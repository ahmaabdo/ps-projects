package com.appspro.rest;


import com.appspro.bsbc.biReport.RestHelper;
import com.appspro.model.PunchData;

import javax.jws.WebMethod;
import javax.jws.WebService;


@WebService
public class PunchController {

    @WebMethod
    public String insertPunch1(PunchData punchData) {

            String result = RestHelper.callPostRest("https://SAMIOICTest-samicloud.integration.ocp.oraclecloud.com:443/ic/api/integration/v1/flows/rest/SAMI_FUSION_OTL/1.0/timeEventRequests",
                                       null, "application/json",
                                       punchData.toString());
            return result;
    }

    //    @WebMethod
    //    public PunchDataResponse insertPunch1(PunchData punchData) {
    //        String json =
    //            getPunch.createJsonPunch(punchData).readEntity(String.class);
    //        return new Gson().fromJson(json, PunchDataResponse.class);
    //    }
}

