package com.appspro.model;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;


public class PunchData {

    public PunchData() {

    }

    private String requestNumber;
    private String sourceId;
    private String requestTimestamp;
    private List<TimeEvents> timeEvents;
    private String personNumber;
    private int intValue;
    private String timeRequestAndEvent;
    

    // Getter Methods


    public String getRequestNumber() {
        return requestNumber;
    }

    public String getSourceId() {
        return sourceId;
    }

    public String getRequestTimestamp() {
        return requestTimestamp;
    }

    // Setter Methods

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public void setRequestTimestamp(String requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public List<TimeEvents> getTimeEvents() {
        return timeEvents;
    }

    public void setTimeEvents(List<TimeEvents> timeEvents) {
        this.timeEvents = timeEvents;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }
    
    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }
    
    public void setTimeRequestAndEvent(String timeRequestAndEvent) {
        this.timeRequestAndEvent = timeRequestAndEvent;
    }

    public String getTimeRequestAndEvent() {
        return timeRequestAndEvent;
    }
    
    public String getValues(){
        if (getIntValue() == 1){
            return  "\r\n             \"supplierDeviceEvent\": \"SAMI_DEVICE_IN\",";
            }
        else {
            return  "\r\n             \"supplierDeviceEvent\": \"SAMI_DEVICE_OUT\",";
            }
        }
    
    public String dateFormate(){
            String s1="";
        try{
            String s = getTimeRequestAndEvent();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                                                        Locale.ENGLISH);
            Date parsedDate = sdf.parse(s);
            SimpleDateFormat print = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            s1=print.format(parsedDate)+"+01:00";
            
        }catch(Exception e){
            e.printStackTrace();
            }
          //  System.out.println(s1);
            return s1;
            
        }
  
    @Override
    public String toString() {
        dateFormate();
        //getIntValue() == "1" ? "SAMI_DEVICE_IN" : "SAMI_DEVICE_OUT"+
        Random ran = new Random();
        int nxt = ran.nextInt() & Integer.MAX_VALUE;
        String s = "" + nxt;
        return "{\r\n       \"requestNumber\": " + '"' + s + '"' +
            ",\r\n       \"sourceId\": \"SAMI_DEVICE\"," +
            "\r\n       \"requestTimestamp\": " +'"' + dateFormate() +'"'+ "," +
            "\r\n       \"timeEvents\": [" +
            "\r\n          {" +
            "\r\n             \"deviceId\": \"SAMI_DEVICE\"," +
            "\r\n             \"eventDateTime\": " +'"' + dateFormate() +'"'+ "," +
            getValues() +
            "\r\n             \"reporterId\": " +'"' +getPersonNumber() +'"'+
            ",\r\n             \"reporterIdType\": \"PERSON\"," +
            "\r\n             \"timeEventAttributes\": [" +
            "\r\n                {" +
            "\r\n                   \"name\": \"PayrollTimeType\"," +
            "\r\n                   \"value\": \"Regular Hours\"" +
            "\r\n                }" +
            "\r\n             ]" +
            "\r\n          }" +
            "\r\n       ]}";
  }


}
