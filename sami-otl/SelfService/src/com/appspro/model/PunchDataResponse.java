package com.appspro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PunchDataResponse {
    public PunchDataResponse() {

    }

    @SerializedName("timeEventRequestId")
    @Expose
    private Long timeEventRequestId;
    @SerializedName("requestNumber")
    @Expose
    private String requestNumber;
    @SerializedName("requestTimestamp")
    @Expose
    private String requestTimestamp;
    @SerializedName("sourceId")
    @Expose
    private String sourceId;
    @SerializedName("timeEvents")
    @Expose
    private List<TimeEvent> timeEvents;
    @SerializedName("links")
    @Expose
    private List<Link> links;


    public Long getTimeEventRequestId() {
        return timeEventRequestId;
    }

    public void setTimeEventRequestId(Long timeEventRequestId) {
        this.timeEventRequestId = timeEventRequestId;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(String requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public List<TimeEvent> getTimeEvents() {
        return timeEvents;
    }

    public void setTimeEvents(List<TimeEvent> timeEvents) {
        this.timeEvents = timeEvents;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
