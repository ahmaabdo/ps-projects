package com.appspro.model;

import java.util.List;

public class TimeEvents {
    public TimeEvents() {
    }
    private String deviceId;
    private String eventDateTime;
    private String supplierDeviceEvent;
    private String reporterId;
    private String reporterIdType;
    private List<TimeEventAttributes> timeEventAttributes;

    // Getter Methods

    public String getDeviceId() {
        return deviceId;
    }

    public String getEventDateTime() {
        return eventDateTime;
    }

    public String getSupplierDeviceEvent() {
        return supplierDeviceEvent;
    }

    public String getReporterId() {
        return reporterId;
    }

    public String getReporterIdType() {
        return reporterIdType;
    }

    // Setter Methods

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setEventDateTime(String eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    public void setSupplierDeviceEvent(String supplierDeviceEvent) {
        this.supplierDeviceEvent = supplierDeviceEvent;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }

    public void setReporterIdType(String reporterIdType) {
        this.reporterIdType = reporterIdType;
    }

    public List<TimeEventAttributes> getTimeEventAttributes() {
        return timeEventAttributes;
    }

    public void setTimeEventAttributes(List<TimeEventAttributes> timeEventAttributes) {
        this.timeEventAttributes = timeEventAttributes;
    }
}
