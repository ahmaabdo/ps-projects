package com.appspro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeEvent {
    public TimeEvent() {
    }
    @SerializedName("timeEventId")
    @Expose
    private Long timeEventId;
    @SerializedName("timeEventRequestId")
    @Expose
    private Long timeEventRequestId;
    @SerializedName("eventDateTime")
    @Expose
    private String eventDateTime;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("supplierDeviceEvent")
    @Expose
    private String supplierDeviceEvent;
    @SerializedName("reporterId")
    @Expose
    private String reporterId;
    @SerializedName("reporterIdType")
    @Expose
    private String reporterIdType;
    @SerializedName("timeEventAttributes")
    @Expose
    private List<TimeEventAttribute> timeEventAttributes;
    @SerializedName("links")
    @Expose
    private List<Link> links;

    public Long getTimeEventId() {
        return timeEventId;
    }

    public void setTimeEventId(Long timeEventId) {
        this.timeEventId = timeEventId;
    }

    public Long getTimeEventRequestId() {
        return timeEventRequestId;
    }

    public void setTimeEventRequestId(Long timeEventRequestId) {
        this.timeEventRequestId = timeEventRequestId;
    }

    public String getEventDateTime() {
        return eventDateTime;
    }

    public void setEventDateTime(String eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSupplierDeviceEvent() {
        return supplierDeviceEvent;
    }

    public void setSupplierDeviceEvent(String supplierDeviceEvent) {
        this.supplierDeviceEvent = supplierDeviceEvent;
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }

    public String getReporterIdType() {
        return reporterIdType;
    }

    public void setReporterIdType(String reporterIdType) {
        this.reporterIdType = reporterIdType;
    }

    public List<TimeEventAttribute> getTimeEventAttributes() {
        return timeEventAttributes;
    }

    public void setTimeEventAttributes(List<TimeEventAttribute> timeEventAttributes) {
        this.timeEventAttributes = timeEventAttributes;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
