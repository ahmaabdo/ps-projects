package com.appspro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Properties {
    public Properties() {
    }
    @SerializedName("changeIndicator")
    @Expose
    private String changeIndicator;

    public String getChangeIndicator() {
        return changeIndicator;
    }

    public void setChangeIndicator(String changeIndicator) {
        this.changeIndicator = changeIndicator;
    }
}
