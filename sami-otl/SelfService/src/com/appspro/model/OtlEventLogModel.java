package com.appspro.model;

public class OtlEventLogModel {
    public OtlEventLogModel() {
    }
    
    int intvalue;
        String timestamp;
        String personnelnr;
        int entranceid;
        String badgenumber;
        
        public OtlEventLogModel( int intvalue , String badgenumber , String timestamp , int entranceid ,  String personnelnr) {
            super();
            this.intvalue = intvalue;
            this.badgenumber = badgenumber;
            this.personnelnr = personnelnr;
            this.entranceid = entranceid;
            this.timestamp = timestamp;
        }
     
        public void setIntvalue(int intvalue) {
            this.intvalue = intvalue;
        }
        public int getIntvalue() {
            return intvalue;
        }
        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
        public String getTimestamp() {
            return timestamp;
        }
        public void setPersonnelnr(String personnelnr) {
            this.personnelnr = personnelnr;
        }
        public String getPersonnelnr() {
            return personnelnr;
        }
        public void setEntranceid(int entranceid) {
            this.entranceid = entranceid;
        }
        public int getEntranceid() {
            return entranceid;
        }
        public void setBadgenumber(String badgenumber) {
            this.badgenumber = badgenumber;
        }
        public String getBadgenumber() {
            return badgenumber;
        }
}

