package com.appspro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeEventAttribute {
    public TimeEventAttribute() {
    }
    @SerializedName("timeEventAttributeId")
    @Expose
    private Long timeEventAttributeId;
    @SerializedName("timeEventId")
    @Expose
    private Long timeEventId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("links")
    @Expose
    private List<Link> links;

    public Long getTimeEventAttributeId() {
        return timeEventAttributeId;
    }

    public void setTimeEventAttributeId(Long timeEventAttributeId) {
        this.timeEventAttributeId = timeEventAttributeId;
    }

    public Long getTimeEventId() {
        return timeEventId;
    }

    public void setTimeEventId(Long timeEventId) {
        this.timeEventId = timeEventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
