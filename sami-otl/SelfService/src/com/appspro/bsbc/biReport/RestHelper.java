/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.biReport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 *
 * @author CPBSLV
 */
public class RestHelper {

    private final String InstanceUrl =
        "https://ejfz-dev1.fa.em2.oraclecloud.com:443";
    private final String orgnizationUrl =
        "//hcmCoreSetupApi/resources/latest/organizations";
    private final String biReportUrl =
        "/xmlpserver/services/PublicReportService";
    private final String employeeServiceUrl =
        "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private final String instanceName =
        "https://ejfz-dev1.fa.em2.oraclecloud.com";

    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public String getBiReportUrl() {
        return biReportUrl;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public static String callPostRest(String serverUrl, String jwttoken,
                                      String contentType, String body) {
        try {
//            System.out.println(body);

            URL url = new URL(serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", contentType);
            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            conn.addRequestProperty("Authorization",
                                    "Basic YW1yby5hbGZhcmVzQGFwcHNwcm8tbWUuY29tOk9yYWNsZUAxMjM0NTY3");

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes());
            os.flush();


            BufferedReader br =
                new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output = br.readLine();


            conn.disconnect();

            return output;
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }

    public String getOrgnizationUrl() {
        return orgnizationUrl;
    }

    public String getEmployeeServiceUrl() {
        return employeeServiceUrl;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getInstanceName() {
        return instanceName;
    }
}
