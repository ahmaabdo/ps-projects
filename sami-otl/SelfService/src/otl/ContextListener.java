package otl;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import scheduler.OtlJob;


public class ContextListener implements ServletContextListener {
    private ServletContext context = null;

    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        System.out.println("----- test listener initiated");
        try {
           // specify the job' s details..
           JobDetail job = JobBuilder.newJob(OtlJob.class).withIdentity("job test")
                                     .build();
           // specify the running period of the job
           Trigger trigger = TriggerBuilder.newTrigger()
                   .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                                      .withIntervalInMinutes(10)
                                                      .repeatForever())
                                                      .build();
           //schedule the job
        //      SchedulerFactory schFactory = new StdSchedulerFactory();
           Scheduler sch = StdSchedulerFactory.getDefaultScheduler();
           sch.start();
           sch.scheduleJob(job, trigger);
            
            System.out.println("----- test job initiated");
           
        } catch (SchedulerException e) {
            System.out.println(e);
        }
    }

    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
    }
}
