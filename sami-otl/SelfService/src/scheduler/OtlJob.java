package scheduler;


import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.appspro.rest.PunchController;

import com.appspro.model.OtlEventLogModel;

import com.appspro.model.PunchData;

import java.sql.PreparedStatement;
import java.sql.SQLException;


public class OtlJob implements Job {

    private Logger log = Logger.getLogger(OtlJob.class);
    Connection conn;
    PreparedStatement stmt;
    ResultSet rs;
    String query;
    String sqlString;

    @Override
    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
        int id_fk;
        List<OtlEventLogModel> listEventLogModel = new ArrayList<OtlEventLogModel>();
        PunchController punchController = new PunchController();
        PunchData punchData = new PunchData();
        try {

//Select * from view_eventlog where CONVERT(VARCHAR(10), timestamp, 120) = CONVERT(VARCHAR(10), GETDATE(), 120) AND personnelnr like '100%' And intvalue != '0'
            conn = DataBase.getConnection();
//            query = "select * from view_eventlog where cast(timestamp As smalldatetime)between dateadd(MINUTE, -10, cast(SYSDATETIME() As smalldatetime)) and SYSDATETIME() AND personnelnr like '100%' And intvalue != '0' order by timestamp desc";
//            query = "\n" + 
//            "SELECT v.id , v.INTVALUE , v.BADGENUMBER, v.TIMESTAMP,v.ENTRANCEID , v.PERSONNELNR  from view_eventlog v\n" + 
//            "EXCEPT\n" + 
//            "SELECT x.id_view_eventlog ,x.INTVALUE2 , x.BADGENUMBER2, x.TIMESTAMP2,x.ENTRANCEID2 , x.PERSONNELNR1  from XX_HR_attendance x";
            query="select id , INTVALUE , BADGENUMBER, TIMESTAMP,ENTRANCEID , PERSONNELNR  from view_eventlog  where cast(timestamp As smalldatetime)between dateadd(MINUTE, -10, cast(SYSDATETIME() As smalldatetime)) and SYSDATETIME() AND (personnelnr like '100%' or personnelnr like '200%' or personnelnr like '400%') And intvalue != '0' \n" + 
            "EXCEPT\n" + 
            "select id_view_eventlog ,INTVALUE2 , BADGENUMBER2, TIMESTAMP2,ENTRANCEID2 , PERSONNELNR1  from XX_HR_attendance";
            stmt = conn.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
            OtlEventLogModel eventLogModel = new OtlEventLogModel();
//                if(rs.getString("PERSONNELNR") != null){
                
                eventLogModel.setIntvalue(rs.getInt("INTVALUE"));
                eventLogModel.setBadgenumber(rs.getString("BADGENUMBER"));
                eventLogModel.setTimestamp(rs.getString("TIMESTAMP"));
                eventLogModel.setEntranceid(rs.getInt("ENTRANCEID"));
                eventLogModel.setPersonnelnr(rs.getString("PERSONNELNR"));
                listEventLogModel.add(eventLogModel);
                
                id_fk = rs.getInt("id");
                
                String query2 = 
                    "insert into XX_HR_attendance(id_view_eventlog,INTVALUE2 , BADGENUMBER2 , TIMESTAMP2 , ENTRANCEID2 , PERSONNELNR1)"+
                    "values (? ,? , ? , ? , ? , ?)";
                
                PreparedStatement preparedStatmnt2 = conn.prepareStatement(query2);
                preparedStatmnt2.setInt(1, id_fk);
                preparedStatmnt2.setInt(2, eventLogModel.getIntvalue());
                preparedStatmnt2.setString(3,eventLogModel.getBadgenumber());
                preparedStatmnt2.setString(4,eventLogModel.getTimestamp());
                preparedStatmnt2.setInt(5,eventLogModel.getEntranceid());
                preparedStatmnt2.setString(6,eventLogModel.getPersonnelnr());
                preparedStatmnt2.execute();  
//                }
//                else{
//                    continue;
//                    }
            }
            for (int i = 0; i < listEventLogModel.size(); i++) {

                punchData.setIntValue(listEventLogModel.get(i).getIntvalue());
                punchData.setTimeRequestAndEvent(listEventLogModel.get(i).getTimestamp());
                punchData.setPersonNumber(listEventLogModel.get(i).getPersonnelnr());
                punchData.toString();
                System.out.println(punchData.toString());
                try {
                    
                    punchController.insertPunch1(punchData);
                    
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            conn.close();

        } catch (SQLException e) {
            System.out.println(e);
        }


    }
}

//            Random ran = new Random();
//            int nxt = ran.nextInt();
//            String s = "" + nxt;
//            System.out.println(s);
//            punchData.setRequestNumber(s);
//            punchData.setRequestTimestamp("2019-01-20T07:30:00.000+01:00");
//            punchData.setSourceId("SAMI_DEVICE");
//            List<TimeEvents> events = new ArrayList<>();
//            TimeEvents times = new TimeEvents();
//            times.setDeviceId("SAMI_DEVICE");
//            times.setEventDateTime("2019-01-20T07:30:00.000+01:00");
//            times.setSupplierDeviceEvent("SAMI_DEVICE_IN");
//            times.setReporterId("1000027");
//            times.setReporterIdType("PERSON");
//            List<TimeEventAttributes> attribute = new ArrayList<>();
//            TimeEventAttributes eventAttributes = new TimeEventAttributes();
//            eventAttributes.setName("PayrollTimeType");
//            eventAttributes.setValue("Regular Hours");
//            attribute.add(eventAttributes);
//            events.add(times);
//            punchData.setTimeEvents(events);
//            times.setTimeEventAttributes(attribute);
