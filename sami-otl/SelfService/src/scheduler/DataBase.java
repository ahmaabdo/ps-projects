package scheduler;
import java.sql.Connection;
import java.sql.DriverManager;

public class DataBase {
    static Connection con = null;

       public static Connection getConnection() {
           try {

               Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
               con = DriverManager.getConnection("jdbc:sqlserver://10.0.17.10:1433;databaseName=aeosdb;user=sa;password=nedap");
               con.setAutoCommit(true);

     //          System.out.println("Opened database successfully");
           } catch (Exception e) {
               System.out.println(e.getMessage());
           }
           return con;
       }
}
