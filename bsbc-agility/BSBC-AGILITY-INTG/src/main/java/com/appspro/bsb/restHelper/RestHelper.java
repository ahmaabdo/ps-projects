/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsb.restHelper;

import com.appspro.bsbc.bean.ItemWLotLogBean;
import com.appspro.bsbc.bean.ReceiptConformationBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.json.JSONObject;

/**
 *
 * @author CPBSLV
 */
public class RestHelper {

    private String InstanceUrl = "https://ehxm.fa.us6.oraclecloud.com:443";
    private String orgnizationUrl = "//hcmCoreSetupApi/resources/latest/organizations";
    private String biReportUrl = "https://ehxm.fa.us6.oraclecloud.com/xmlpserver/services/PublicReportService";
    private String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private String instanceName = "https://ehxm.fa.us6.oraclecloud.com";

    public final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts
                = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }
            }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public static String callPostRest(String serverUrl,
            String contentType, String body, String autString) {
        try {
            System.out.println(body);

            URL url = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", contentType);
            conn.setRequestProperty("Accept", "application/json");
            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            if (autString != null) {
                conn.addRequestProperty("Authorization", "Basic " + autString);
            }

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes("UTF-8"));
            os.close();

            int responseCode = conn.getResponseCode();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            StringBuilder output = new StringBuilder();
            String inputLine;

            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }

            conn.disconnect();
            System.out.println(responseCode);
            if (responseCode == 200) {
                return output.toString();
            } else if (responseCode == 500) {
                return "{\"ReturnStatus\":\"INERTNAL SERVER ERROR\"}";
            } else if (responseCode == 201) {
                JSONObject reponseJson = new JSONObject(output.toString());
                return "{\"ReturnStatus\":\"" + reponseJson.optString("ReturnStatus") + "\",\"ErrorCode\":\"" + reponseJson.optString("ErrorCode") + "\",\"ErrorExplanation\":\"" + reponseJson.optString("ErrorCode") + "\"}";
            } else if(responseCode == 400){
                return conn.getErrorStream().toString();
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }
    
    public static ItemWLotLogBean callPostWLWNRest(String serverUrl, String contentType, String body, String autString) {
        JSONObject res = new JSONObject();
        ItemWLotLogBean itemWLotLogBean = new ItemWLotLogBean();
        try {
            System.out.println("calling ::" + serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", contentType);
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Authorization",
                                          "Basic " + autString);
            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            System.out.println("code:" + connection.getResponseCode());
            InputStream is;
            if (connection.getResponseCode() >= 400) {
                is = connection.getErrorStream();
                System.out.println(connection.getResponseMessage());
                JSONObject respo = new JSONObject(connection.getErrorStream());
                System.out.println(respo.toString());
                itemWLotLogBean.setCodeStatus(Integer.toString(connection.getResponseCode()));
                res.put("status", "error");
            } else {
                res.put("status", "done");
                is = connection.getInputStream();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            StringBuilder response2 = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
                response2.append(inputLine);
                if(connection.getResponseCode()!=201 || connection.getResponseCode()!=200){
//                      System.out.println(response);
                      itemWLotLogBean.setBodyStatus(response.toString());
                      itemWLotLogBean.setCodeStatus(Integer.toString(connection.getResponseCode()));
                }else{
                    System.out.println(response2.toString());
                }
              
             
              //  itemWLotLogBean.setBodyStatus(response.toString());
                res.put("data", itemWLotLogBean.getBodyStatus());
            }
            in.close();
            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                res.put("data", response.toString());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", itemWLotLogBean.getBodyStatus());
            res.put("data", "Internal server error");
        }
        return itemWLotLogBean;
    }
    
     public static ReceiptConformationBean callPostReceiptConfRest(String serverUrl, String contentType, String body, String autString) {
        JSONObject res = new JSONObject();
        ReceiptConformationBean receiptConformationBean = new ReceiptConformationBean();
        List<ReceiptConformationBean> errorMsg = new ArrayList<ReceiptConformationBean>();
        try {
            System.out.println("calling ::" + serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", contentType);
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Authorization",
                                          "Basic " + autString);
            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            System.out.println("code:" + connection.getResponseCode());
            InputStream is;
            if (connection.getResponseCode() >= 400) {
                is = connection.getErrorStream();
                System.out.println(connection.getResponseMessage());
                JSONObject respo = new JSONObject(connection.getErrorStream());
                System.out.println(respo.toString());
                receiptConformationBean.setCodeStatus(Integer.toString(connection.getResponseCode()));
                res.put("status", "error");
            } else {
                res.put("status", "done");
                is = connection.getInputStream();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            StringBuilder response2 = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
                response2.append(inputLine);
                receiptConformationBean.setErrorMessage(response2.toString());
                errorMsg.add(receiptConformationBean);
                if(connection.getResponseCode()!=201 || connection.getResponseCode()!=200){
//                      System.out.println(response);
                      receiptConformationBean.setBodyStatus(response.toString());
                      receiptConformationBean.setCodeStatus(Integer.toString(connection.getResponseCode()));
                }else{
                    System.out.println(response2.toString());
                }
              
             
              //  itemWLotLogBean.setBodyStatus(response.toString());
                res.put("data", receiptConformationBean.getBodyStatus());
            }
            receiptConformationBean.setErrorMessage(errorMsg);
            in.close();
            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                res.put("data", response.toString());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", receiptConformationBean.getBodyStatus());
            res.put("data", "Internal server error");
        }
        return receiptConformationBean;
    }


       public static String callGetRest(String serverUrl) {
        
        try {
              
            URL url = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.addRequestProperty("Authorization", "Basic U0NNX0lNUEw6QlNCQ0AxMjM=");
//            conn.addRequestProperty("REST-Framework-Version","2");
//            conn.setRequestProperty("id", id);

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

}
