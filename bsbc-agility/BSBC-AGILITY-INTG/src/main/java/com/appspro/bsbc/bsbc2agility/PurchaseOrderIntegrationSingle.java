/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsb.biReprot.BIPReports;
import com.appspro.bsb.biReprot.BIReportModel;
import com.appspro.bsbc.SFTPUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author CPBSLV
 */
public class PurchaseOrderIntegrationSingle {

    private final String CONTAINERKEY = "NA";
    private final String STORERKEY = "108815";
    private final String ITEMTYPE = "0";
    private final String ITEMTYPE1 = "1";
    private final String VALUE = "0";

    public static void main(String arp[]) {
        System.out.println(new PurchaseOrderIntegrationSingle().createFile(""));

    }

    public StringBuilder createFile(String salesOrder) {
        //Calling 
        StringBuilder itemLines = new StringBuilder();
        JSONObject json = BIReportModel.runWithParamReportSO(BIPReports.REPORT_NAME.PO_CREATION.getValue(),salesOrder);
        JSONObject dataDS = json.getJSONObject("DATA_DS");
        if (!dataDS.has("G_1")) {
            return null;
        }
        Object jsonTokner = new JSONTokener(dataDS.get("G_1").toString()).nextValue();
        if (jsonTokner instanceof JSONObject) {
            JSONObject g1 = dataDS.getJSONObject("G_1");
            JSONArray arrJson = new JSONArray();
            arrJson.put(g1);
            itemLines = new PurchaseOrderIntegration().checkNullable(arrJson);
        } else {
            JSONArray g1 = dataDS.getJSONArray("G_1");
            itemLines = new PurchaseOrderIntegration().checkNullable(g1);
        }
              DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
        Date date = new Date();
//        return itemLines;
        boolean result = SFTPUtility.SEND_SFTP_FILE(itemLines, "PObsbc_10" + dateFormat.format(date) + ".chr");
        return null;
//        return result;
    }

    
}
