/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.utility;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.quartz.CronScheduleBuilder;
import org.quartz.DateBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Web application lifecycle listener.
 *
 * @author Hp
 */
public class NewServletListener implements ServletContextListener {

    private ServletContext context = null;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        System.out.println("----- listeners initiated");
        try {

            JobDetail jobLot = JobBuilder.newJob(QuartzJobLot.class).build();
            JobDetail jobLotSecond = JobBuilder.newJob(QuartzJobLot.class).build();

            Trigger triggerLot = TriggerBuilder.newTrigger()
                    .withIdentity("job-lot")
                    .withSchedule(
                            CronScheduleBuilder.dailyAtHourAndMinute(23, 50))
                    .forJob(jobLot)
                    .build();
            Trigger triggerLotSecond = TriggerBuilder.newTrigger()
                    .withIdentity("job-lot-second")
                    .withSchedule(
                            CronScheduleBuilder.dailyAtHourAndMinute(12, 50))
                    .forJob(jobLotSecond)
                   .build();
            Scheduler sch = StdSchedulerFactory.getDefaultScheduler();
            sch.start();

            sch.scheduleJob(jobLot, triggerLot);
            sch.scheduleJob(jobLotSecond, triggerLotSecond);
        } catch (SchedulerException e) {
            System.out.println("Error" + e);
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
