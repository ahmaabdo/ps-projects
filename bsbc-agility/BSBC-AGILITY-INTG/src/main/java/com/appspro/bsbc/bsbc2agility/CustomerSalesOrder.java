/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsb.biReprot.BIPReports;
import com.appspro.bsb.biReprot.BIReportModel;
import com.appspro.bsbc.SFTPUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author CPBSLV
 */
public class CustomerSalesOrder {

    private final String STORERKEY = "108815";
    private final String ITEMTYPE = "0";
    private final String ITEMTYPE1 = "1";
    private final String DOCUMENTTYPE = "INV";
    private final String DOCUMENTTYPE1 = "DNC";
    private String priviousInvoiceNumber = "";

    public static void main(String arp[]) {
        System.out.println(new CustomerSalesOrder().createFile());
    }

    public StringBuilder createFile() {
        //Calling 
        StringBuilder itemLines = new StringBuilder();

        JSONObject json = BIReportModel.runWithParamReport(BIPReports.REPORT_NAME.CUSTOMER_SALES_ORDER.getValue());
        JSONObject dataDS = json.getJSONObject("DATA_DS");
        if (!dataDS.has("G_1")) {
            return null;
        }
        Object jsonTokner = new JSONTokener(dataDS.get("G_1").toString()).nextValue();
        if (jsonTokner instanceof JSONObject) {
            JSONObject g1 = dataDS.getJSONObject("G_1");
            JSONArray arrJson = new JSONArray();
            arrJson.put(g1);
            itemLines = new CustomerSalesOrder().checkNullable(arrJson);
        } else {
            JSONArray g1 = dataDS.getJSONArray("G_1");
            itemLines = new CustomerSalesOrder().checkNullable(g1);
        }
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
        Date date = new Date();
//        return itemLines;
//        test.SEND_MAIL(itemLines, "CSsobsbc_10" + dateFormat.format(date));
        boolean result = SFTPUtility.SEND_SFTP_FILE(itemLines, "CSsobsbc_10" + dateFormat.format(date) + ".chr");
        SFTPUtility.SEND_SFTP_FILE_BACKUP(itemLines, "CSsobsbc_10" + dateFormat.format(date) + ".chr");
        return null;
//        return true;
    }

    private StringBuilder createCustomerOrderDetails(String invoiceno, String invoiceDate, String deliveryDate, String consigneekKey, String c_Company, String address1, String address2,
            String address3, String phone1, String fax, String sku, String skuDescription, String orderQty, String expiryDate, String batchNo, String Notes, String orderNo,
            String sourceCode_No, String ExternlinenNo, String itemType, String documentType) {
        StringBuilder itemLine = new StringBuilder();
        if (invoiceno.contains("FIN")) {
            itemLine.append(sourceCode_No);
        } else {
            itemLine.append(invoiceno);
        }

        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(invoiceDate);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(deliveryDate);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((consigneekKey));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((c_Company));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((STORERKEY));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((address1));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((address2));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((address3));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((phone1));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((fax));
        itemLine.append(FacadeIntegration.DELIMITER);
        String skuFormated = sku != null ? sku.replaceAll("-", "") : sku;
        itemLine.append((skuFormated));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((skuDescription));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(orderQty);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((ITEMTYPE1));

        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((expiryDate));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((batchNo));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((Notes));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((orderNo));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append((DOCUMENTTYPE));

        itemLine.append(FacadeIntegration.DELIMITER);
        if (invoiceno.contains("FIN")) {
            itemLine.append(invoiceno);
        } else {
            itemLine.append(sourceCode_No);
        }
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(ExternlinenNo);

        String removeLines = itemLine.toString().replaceAll("\\n", "").replaceAll("\\r", "");
        return new StringBuilder(removeLines).append("\r\n");

    }

    public StringBuilder checkNullable(JSONArray g1) {
        StringBuilder itemLines = new StringBuilder();
        int count = 0;
        for (int i = 0; i < g1.length(); i++) {
            String INVOICENO = "";
            String INVOICEDATE = "";
            String DELIVERYDATE = "";
            String CONSIGNEEKEY = "";
            String C_COMPANY = "";
            String ADDRESS1 = "";
            String ADDRESS2 = "";
            String ADDRESS3 = "";
            String PHONE1 = "";
            String FAX = "";
            String SKU = "";
            String SKUDESCRIPTION = "";
            String ORDEREDQTY = "";
            String EXPIRYDATE = "";
            String BATCHNO = "";
            String NOTES = "";
            String ORDERNO = "";
            String SOURCEDOCNO = "";
            String EXTERNLINENO = "";
            String ITEMTYPE = "";
            String DOCUMENTTYPE = "";

            INVOICENO = (!g1.getJSONObject(i).isNull("INVOICENO") ? g1.getJSONObject(i).get("INVOICENO").toString() : "");
            if (i > 0) {
                priviousInvoiceNumber = (!g1.getJSONObject(i - 1).isNull("INVOICENO") ? g1.getJSONObject(i - 1).get("INVOICENO").toString() : "");
            } else {
                priviousInvoiceNumber = INVOICENO;
            }

            INVOICEDATE = (!g1.getJSONObject(i).isNull("INVOICEDATE") ? g1.getJSONObject(i).get("INVOICEDATE").toString() : "");
            DELIVERYDATE = (!g1.getJSONObject(i).isNull("DELIVERYDATE") ? g1.getJSONObject(i).get("DELIVERYDATE").toString() : "");
            CONSIGNEEKEY = (!g1.getJSONObject(i).isNull("CONSIGNEEKEY") ? g1.getJSONObject(i).get("CONSIGNEEKEY").toString() : "");
            C_COMPANY = (!g1.getJSONObject(i).isNull("C_COMPANY") ? g1.getJSONObject(i).get("C_COMPANY").toString() : "");
            ADDRESS1 = (!g1.getJSONObject(i).isNull("ADDRESS1") ? g1.getJSONObject(i).get("ADDRESS1").toString() : "");
            ADDRESS2 = (!g1.getJSONObject(i).isNull("ADDRESS2") ? g1.getJSONObject(i).get("ADDRESS2").toString() : "");
            ADDRESS3 = (!g1.getJSONObject(i).isNull("ADDRESS3") ? g1.getJSONObject(i).get("ADDRESS3").toString() : "");
            PHONE1 = (!g1.getJSONObject(i).isNull("PHONE1") ? g1.getJSONObject(i).get("PHONE1").toString() : "");
            FAX = (!g1.getJSONObject(i).isNull("FAX") ? g1.getJSONObject(i).get("FAX").toString() : "");
            SKU = (!g1.getJSONObject(i).isNull("SKU") ? g1.getJSONObject(i).get("SKU").toString() : "");
            SKUDESCRIPTION = (!g1.getJSONObject(i).isNull("SKUDESCRIPTION") ? g1.getJSONObject(i).get("SKUDESCRIPTION").toString() : "");
            ORDEREDQTY = (!g1.getJSONObject(i).isNull("ORDEREDQTY") ? g1.getJSONObject(i).get("ORDEREDQTY").toString() : "");
            EXPIRYDATE = (!g1.getJSONObject(i).isNull("EXPIRYDATE") ? g1.getJSONObject(i).get("EXPIRYDATE").toString() : "");
            BATCHNO = (!g1.getJSONObject(i).isNull("BATCHNO") ? g1.getJSONObject(i).get("BATCHNO").toString() : "");
            NOTES = (!g1.getJSONObject(i).isNull("NOTES") ? g1.getJSONObject(i).get("NOTES").toString() : "");
            ORDERNO = (!g1.getJSONObject(i).isNull("ORDERNO") ? g1.getJSONObject(i).get("ORDERNO").toString() : "");
            SOURCEDOCNO = (!g1.getJSONObject(i).isNull("SOURCEDOCNO") ? g1.getJSONObject(i).get("SOURCEDOCNO").toString() : "");
            if (INVOICENO.equals(priviousInvoiceNumber)) {
                count++;
            } else {
                count = 1;
            }
            EXTERNLINENO = (count + "");
            ITEMTYPE = (!g1.getJSONObject(i).isNull("ITEMTYPE") ? g1.getJSONObject(i).get("ITEMTYPE").toString() : "");
            DOCUMENTTYPE = (!g1.getJSONObject(i).isNull("DOCUMENTTYPE") ? g1.getJSONObject(i).get("DOCUMENTTYPE").toString() : "");

            itemLines.append(createCustomerOrderDetails(INVOICENO, INVOICEDATE, DELIVERYDATE, CONSIGNEEKEY, C_COMPANY, ADDRESS1, ADDRESS2,
                    ADDRESS3, PHONE1, FAX, SKU, SKUDESCRIPTION, ORDEREDQTY, EXPIRYDATE, BATCHNO, NOTES, ORDERNO, SOURCEDOCNO, EXTERNLINENO, ITEMTYPE, DOCUMENTTYPE));

        }
        return itemLines;
    }

}
