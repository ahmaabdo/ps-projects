/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.utility;

/**
 *
 * @author Hp
 */
public class Utility {

    public static String GET_NULL_AS_EMPTY(String str) {
        if (str == null) {
            return "";
        }

        return str;
    }

    public static String FORMAT_STRING_WITH_CUTATION(String str) {

        str = GET_NULL_AS_EMPTY(str);
        if (!str.isEmpty()) {
            return "\"" + str + "\"";
        }

        return str;
    }
}
