/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.agility2bsbc;

import com.appspro.bsb.restHelper.RestHelper;
import com.appspro.bsbc.bean.ItemWLotLogBean;
import com.appspro.bsbc.generation.ItemLotQuantity.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.FetchProfile;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author CPBSLV
 */
public class ItemLotQuantity {

    public static void main(String argp[]) {

//        System.out.println(new ItemLotQuantity().insert());
        ItemLotQuantity itemLotQuantity = new ItemLotQuantity();
        itemLotQuantity.getItemData();
    }

    public Message getItemData() {

        FileInputStream fstream = null;
        OutputStream outputStream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        String renamedFile = null;
        boolean check = true;
        Message itemObj = new Message();
        String response = null;
        try {

            try {
                String host = System.getProperty("http.proxyHost");
                String post = System.getProperty("http.proxyPort");
                session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("@BSbc123456");
                if (host != null || post != null) {
                    ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                    session.setProxy(proxy);
                }
                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect();
                sftpChannel = (ChannelSftp) channel;
                sftpChannel.cd("/upload");
                Vector filelist = sftpChannel.ls("/upload/AgilityItemAndLot");
                List<String> fileNameList = new ArrayList<String>();
                String fileName = null;
                for (int i = 0; i < filelist.size(); i++) {
                    ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);
                    if (entry.getFilename().startsWith("IBD_WL")) {
                        fileName = entry.getFilename();
                        fileNameList.add(fileName);
                    }

                }
                if (fileNameList != null && !fileNameList.isEmpty()) {
                    for (int i = 0; i < fileNameList.size(); i++) {
                        fileSFTP = sftpChannel.get("/upload/AgilityItemAndLot/" + fileNameList.get(i));
                        renamedFile = fileNameList.get(i);

                        // Open the file
                        String filename = "OTL";
                        File file = File.createTempFile(filename, ".zip");

                        // write the inputStream to a FileOutputStream
                        outputStream
                                = new FileOutputStream(file);

                        int read = 0;
                        byte[] bytes = new byte[2056];

                        while ((read = fileSFTP.read(bytes)) != -1) {
                            outputStream.write(bytes, 0, read);
                        }

                        if (file != null) {
                            JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);

                            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                            itemObj = (Message) jaxbUnmarshaller.unmarshal(file);
                            buildJsonRequest(itemObj);
                        } else {
                            return null;
                        }
                        Thread.sleep(20
                                * // minutes to sleep
                                60
                                * // seconds to a minute
                                1000);
                        sftpChannel.rename("/upload/AgilityItemAndLot/" + renamedFile,
                                "/upload/LoadedAgilityItemAndLot/" + renamedFile);
                        sftpChannel.cd("/upload/");
                    }

//                sftpChannel.rm(renamedFile);
                } else {
                    check = false;
                }

            } catch (JAXBException ex) {
                Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException ex) {
//            Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        } finally {
            try {
                Thread.sleep(10
                        * // minutes to sleep
                        60
                        * // seconds to a minute
                        1000);
                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                Thread.sleep(5
                        * // minutes to sleep
                        60
                        * // seconds to a minute
                        1000);
                session.setTimeout(1500);
                if (session != null) {
                    session.disconnect();
                }
                                    Thread.sleep(2
                            * // minutes to sleep
                            60
                            * // seconds to a minute
                            1000);
                if (fstream != null) {
                    fstream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JSchException ex) {
                Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }
            renameFile();
        }
        return itemObj;
    }

    public static void renameFile() {

        FileInputStream fstream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        String renamedFile = null;
        boolean check = true;
        try {

            String host = System.getProperty("http.proxyHost");
            String post = System.getProperty("http.proxyPort");
            session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword("@BSbc123456");
            if (host != null || post != null) {
                ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                session.setProxy(proxy);
            }
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftpChannel = (ChannelSftp) channel;
            sftpChannel.cd("/upload");
            Vector filelist = sftpChannel.ls("/upload/AgilityItemAndLot");
            List<String> fileNameList = new ArrayList<String>();
            String fileName = null;
            for (int i = 0; i < filelist.size(); i++) {
                ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);
                if (entry.getFilename().startsWith("IBD_WL")) {
                    fileName = entry.getFilename();
                    fileNameList.add(fileName);
                }

            }
            if (fileNameList != null && !fileNameList.isEmpty()) {
                for (int i = 0; i < fileNameList.size(); i++) {
                    fileSFTP = sftpChannel.get("/upload/AgilityItemAndLot/" + fileNameList.get(i));
                    renamedFile = fileNameList.get(i);

                    sftpChannel.rename("/upload/AgilityItemAndLot/" + renamedFile,
                            "/upload/LoadedAgilityItemAndLot/" + renamedFile);
                    sftpChannel.cd("/upload/");
                }

            } else {
                check = false;
            }

        } catch (JSchException e) {
            e.printStackTrace();
        } //        } catch (ParseException ex) {
        //            Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        catch (SftpException e) {
            e.printStackTrace();
        } finally {
            try {

                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                if (session != null) {
                    session.disconnect();
                }
                if (check & fstream != null) {
                    fstream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

//    public Message getItemData() {
//        Message itemObj = new Message();
//        try {
//
//            File file = SFTPUtility.readSFTPFile("IBD_WL_4cb1f9a8-9145-481c-b94c-e5a87d915063_20180922011244.xml");
//            if (file != null) {
//                JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);
//
//                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//                itemObj = (Message) jaxbUnmarshaller.unmarshal(file);
//            } else {
//                return null;
//            }
//
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//        return itemObj;
//    }
//      
    private final String USER_AGENT = "Mozilla/5.0";

    private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

    public boolean insert() {
        try {
            String url = "https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/AppsproAgility/itemlotquantity/";

            Message message = new ItemLotQuantity().getItemData();
            if (message == null) {
                return false;
            }
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message);
            // System.out.println(jsonInString);
            RestHelper.callPostRest(url, "application/json", jsonInString, null);
            return true;

        } catch (IOException ex) {
            Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String buildJsonRequest(Message itemObj) {
        GetInsertDataDff getInsertDataDff = new GetInsertDataDff();
        ItemWLotLogBean itemWLotLogBean = new ItemWLotLogBean();
        ItemWLotLogBean itemWLotLogBean2 = new ItemWLotLogBean();
        List<Message.MessageDetail.IBD.IBDHeader.IBDDetail> fileSize = itemObj.getMessageDetail().getIBD().getIBDHeader().getIBDDetail();
        for (int ind = 0; ind < fileSize.size(); ind++) {
            String xmlItemNumber = itemObj.getMessageDetail().getIBD().getIBDHeader().getIBDDetail().get(ind).getSKU();
            String lotNumber = itemObj.getMessageDetail().getIBD().getIBDHeader().getIBDDetail().get(ind).getBATCHNUMBER();
            String messageID = itemObj.getMessageHeader().getMessageID();
            String messageType = itemObj.getMessageHeader().getMessageType();
            Long messageDate = itemObj.getMessageHeader().getMessageDate();
            String fileName = messageType.substring(0, 3) + "_" + messageType.substring(3, 5) + "_" + messageID + "_" + messageDate + "17" + ".xml";
            if (xmlItemNumber.length() == 10) {
                xmlItemNumber = xmlItemNumber.substring(0, 4) + "-" + xmlItemNumber.substring(4, 9)
                        + "-" + xmlItemNumber.substring(9, xmlItemNumber.length());
            } else if (xmlItemNumber.length() == 11) {
                xmlItemNumber = xmlItemNumber.substring(0, 4) + "-" + xmlItemNumber.substring(4, 7)
                        + "-" + xmlItemNumber.substring(7, xmlItemNumber.length());
            }
            String agilityLotReportDate = Long.toString(itemObj.getMessageHeader().getMessageDate());
            if (agilityLotReportDate != null && !agilityLotReportDate.isEmpty()) {
                DateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
                DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    agilityLotReportDate = format2.format(format.parse(Long.toString(itemObj.getMessageHeader().getMessageDate())));

                } catch (ParseException ex) {
                    Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            long dqtyonhand = itemObj.getMessageDetail().getIBD().getIBDHeader().getIBDDetail().get(ind).getQTYONHAND();
            long qtyallocated = itemObj.getMessageDetail().getIBD().getIBDHeader().getIBDDetail().get(ind).getQTYALLOCATED();
            long qtyPicked = itemObj.getMessageDetail().getIBD().getIBDHeader().getIBDDetail().get(ind).getQTYPICKED();
            String agilityLotQty = Long.toString(dqtyonhand - qtyallocated - qtyPicked);
            int lineNum = itemObj.getMessageDetail().getIBD().getIBDHeader().getIBDDetail().get(ind).getLINENO();
            // System.out.println(xmlItemNumber);
            //   System.out.println(lotNumber);
            itemWLotLogBean.setLotNumber(lotNumber);
            itemWLotLogBean.setItemNumber(xmlItemNumber);
            itemWLotLogBean.setAgilityLotQty(agilityLotQty);
            itemWLotLogBean.setAgilityLotReportQty(agilityLotReportDate);
            itemWLotLogBean.setLineNumber(lineNum);
            itemWLotLogBean.setFileName(fileName);
            itemWLotLogBean2 = getInsertDataDff.GetAndPostLotData(lotNumber, xmlItemNumber, agilityLotQty, agilityLotReportDate);
            itemWLotLogBean.setPayload(itemWLotLogBean2.getPayload());
            if (itemWLotLogBean2.isStatus() == false) {
//                itemWLotLogBean2.getClass();
                itemWLotLogBean.setPayload(itemWLotLogBean2.getPayload());
                itemWLotLogBean.setUrl(itemWLotLogBean2.getUrl());
                itemWLotLogBean.setBodyStatus(itemWLotLogBean2.getBodyStatus());
                insertErrorLog(itemWLotLogBean);
            }
        }
        return null;
    }

    public boolean insertErrorLog(ItemWLotLogBean itemWLotLogBean) {
        try {
            String url = "https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/AppsproAgility/WithLotErrorLog/";
            String jsonInString = "{\n"
                    + "	\"WL_AGILITY_LOT_QTY\":\"" + itemWLotLogBean.getAgilityLotQty() + '"' + ",\n"
                    + "	\"WL_AGILITY_LOT_REPORT_DATE\":\"" + itemWLotLogBean.getAgilityLotReportQty() + '"' + ",\n"
                    + "	\"WL_ITEM_NUMBER\":\"" + itemWLotLogBean.getItemNumber() + '"' + ",\n"
                    + "	\"WL_LOT_NUMBER\":\"" + itemWLotLogBean.getLotNumber() + '"' + ",\n"
                    + "	\"WL_URL\":\"" + itemWLotLogBean.getUrl() + '"' + ",\n"
                    + "	\"WL_LINE_NUM\":" + itemWLotLogBean.getLineNumber() + ",\n"
                    + "	\"WL_ERROR_MESSAGE\":" + '"' + (itemWLotLogBean.getBodyStatus() == null ? itemWLotLogBean.getBodyStatus() : itemWLotLogBean.getBodyStatus().replaceAll("\"", "'")) + '"' + ",\n"
                    + "	\"WL_FILE_NAME\":" + '"' + itemWLotLogBean.getFileName() + '"' + ",\n"
                    + "	\"WL_PAYLOAD\":" + '"' + (itemWLotLogBean.getPayload() == null ? itemWLotLogBean.getPayload() : itemWLotLogBean.getPayload().replaceAll("\"", "'")) + '"' + "\n"
                    + "}";
            // System.out.println(jsonInString);
            RestHelper.callPostRest(url, "application/json", jsonInString, null);
            return true;

        } catch (Exception ex) {
            Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
