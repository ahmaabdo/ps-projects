/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsb.biReprot.BIPReports;
import com.appspro.bsb.biReprot.BIReportModel;
import com.appspro.bsbc.SFTPUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author Hp
 */
public class ItemMasterIntegrationSingle {

    private final String STORERKEY_BSBC = "108815";
    private final String ABC = "A";
    private final String TARIFFKEY = "0000001895";

    public static void main(String arp[]) {
        new ItemMasterIntegration().createFile();
    }

    public StringBuilder createFile(String invoice) {
        //Calling 
        StringBuilder itemLines = new StringBuilder();
        JSONObject json = BIReportModel.runWithParamReportSO(BIPReports.REPORT_NAME.GET_ITEM_TYPE.getValue(),invoice);
        
        JSONObject dataDS = json.getJSONObject("DATA_DS");
        if (!dataDS.has("G_1")) {
            return null;
        }
        Object jsonTokner = new JSONTokener(dataDS.get("G_1").toString()).nextValue();
        if (jsonTokner instanceof JSONObject) {
            JSONObject g1 = dataDS.getJSONObject("G_1");
            JSONArray arrJson = new JSONArray();
            arrJson.put(g1);
            itemLines = new ItemMasterIntegration().checkNullable(arrJson);
        } else {
            JSONArray g1 = dataDS.getJSONArray("G_1");
            itemLines = new ItemMasterIntegration().checkNullable(g1);
        }

        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
        Date date = new Date();
//        return itemLines;
        boolean result = SFTPUtility.SEND_SFTP_FILE(itemLines, "ITMobsbc_10" + dateFormat.format(date) + ".chr");
        return null;
//        return result;
    }
}
