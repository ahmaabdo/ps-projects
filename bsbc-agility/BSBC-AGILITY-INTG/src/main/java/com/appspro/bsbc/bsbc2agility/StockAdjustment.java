/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsb.biReprot.BIPReports;
import com.appspro.bsb.biReprot.BIReportModel;
import com.appspro.bsbc.SFTPUtility;
import com.appspro.bsbc.utility.Utility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author CPBSLV
 */
public class StockAdjustment {

    private final String STORERKEY = "108815";
    private final String DOCUMENTTYPE = "SAV";

    public static void main(String arp[]) {
        new StockAdjustment().createFile();
    }

    public StringBuilder createFile() {
        //Calling 
        StringBuilder itemLines = new StringBuilder();

        JSONObject json = BIReportModel.runWithParamReport(BIPReports.REPORT_NAME.STUCK_ADJUSTMENT.getValue());

        JSONObject dataDS = json.getJSONObject("DATA_DS");
        if (!dataDS.has("G_1")) {
            return null;
        }
        Object jsonTokner = new JSONTokener(dataDS.get("G_1").toString()).nextValue();
        if (jsonTokner instanceof JSONObject) {
            JSONObject g1 = dataDS.getJSONObject("G_1");
            JSONArray arrJson = new JSONArray();
            arrJson.put(g1);
            itemLines = new StockAdjustment().checkNullable(arrJson);
        } else {
            JSONArray g1 = dataDS.getJSONArray("G_1");
            itemLines = new StockAdjustment().checkNullable(g1);
        }
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
        Date date = new Date();
//        return itemLines;
        boolean result = SFTPUtility.SEND_SFTP_FILE(itemLines, "SAobsbc_10" + dateFormat.format(date) + ".chr");
        return null;
//        return result;
    }

    private StringBuilder createStockAdjustmentDetails(String documentNumber, String documentDate, String sku, String batchNumber, String expireDate, String qty, String detialNum) {
        StringBuilder itemLine = new StringBuilder();
        itemLine.append("\"" + STORERKEY + "\"");
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(documentNumber != null ? documentNumber.replaceAll("#", "") : ""));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(DOCUMENTTYPE));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(documentDate);
        itemLine.append(FacadeIntegration.DELIMITER);
        String skuFormated = sku != null ? sku.replaceAll("-", "").replaceAll("\\|", "~") : sku;
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(skuFormated));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(batchNumber));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(expireDate));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(qty);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(detialNum);
        itemLine.append("\n");
        String removeLines = itemLine.toString().replaceAll("\\n", "").replaceAll("\\r", "");
        return new StringBuilder(removeLines).append("\r\n");

    }

    public StringBuilder checkNullable(JSONArray g1) {
        StringBuilder itemLines = new StringBuilder();

        for (int i = 0; i < g1.length(); i++) {
            String DOCUMENTNUMBER = "";
            String DOCUMENTDATE = "";
            String SKU = "";
            String EXPIRYDATE = "";
            String BATCHNUMBER = "";
            String QTY = "";
            String DETIALNUM = "";

            DOCUMENTNUMBER = (!g1.getJSONObject(i).isNull("DOCUMENTNUMBER") ? g1.getJSONObject(i).get("DOCUMENTNUMBER").toString() : "");
            DOCUMENTDATE = (!g1.getJSONObject(i).isNull("DOCUMENTDATE") ? g1.getJSONObject(i).get("DOCUMENTDATE").toString() : "");
            SKU = (!g1.getJSONObject(i).isNull("SKU") ? g1.getJSONObject(i).get("SKU").toString() : "");
            EXPIRYDATE = (!g1.getJSONObject(i).isNull("EXPIRYDATE") ? g1.getJSONObject(i).get("EXPIRYDATE").toString() : "");
            BATCHNUMBER = (!g1.getJSONObject(i).isNull("BATCHNUMBER") ? g1.getJSONObject(i).get("BATCHNUMBER").toString() : "");
            QTY = (!g1.getJSONObject(i).isNull("QTY") ? g1.getJSONObject(i).get("QTY").toString() : "");
            DETIALNUM = (!g1.getJSONObject(i).isNull("DETIALNUM") ? g1.getJSONObject(i).get("DETIALNUM").toString() : "");

            itemLines.append(createStockAdjustmentDetails(DOCUMENTNUMBER, DOCUMENTDATE, SKU, BATCHNUMBER,
                    EXPIRYDATE, QTY, DETIALNUM));

        }
        return itemLines;
    }
}
