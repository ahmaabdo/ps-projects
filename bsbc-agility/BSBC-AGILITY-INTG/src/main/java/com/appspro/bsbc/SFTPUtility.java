/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc;

import com.appspro.bsbc.agility2bsbc.ItemLotQuantity;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hp
 */
public class SFTPUtility {

    public static boolean SEND_SFTP_FILE(StringBuilder str, String fileName) {

        JSch jsch = new JSch();
        Session session = null;
        InputStream stream = null;
        try {
            stream = new ByteArrayInputStream(str.toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SFTPUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            String host = System.getProperty("http.proxyHost");
            String post = System.getProperty("http.proxyPort");

//          session = jsch.getSession("kwprbsbc", "85.239.8.202", 22);
//            session.setPassword("@bsbc123");
            session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
            session.setPassword("@BSbc123456");
            session.setConfig("StrictHostKeyChecking", "no");
            session.setServerAliveInterval(180000);
            if (host != null && post != null) {
                ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                session.setProxy(proxy);
            }

            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.put(stream, "/upload/Agility/" + fileName);
            //            sftpChannel.rm("/upload/testUpload1.dat");
            //                sftpChannel.rename( "/upload/testUpload1.dat",  "/upload/oweidi.dat");
            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException ex) {
            Logger.getLogger(SFTPUtility.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (SftpException ex) {
            Logger.getLogger(SFTPUtility.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;

    }
    
    public static boolean SEND_SFTP_FILE_BACKUP(StringBuilder str, String fileName) {

        JSch jsch = new JSch();
        Session session = null;
        InputStream stream = null;
        try {
            stream = new ByteArrayInputStream(str.toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SFTPUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            String host = System.getProperty("http.proxyHost");
            String post = System.getProperty("http.proxyPort");

//          session = jsch.getSession("kwprbsbc", "85.239.8.202", 22);
//            session.setPassword("@bsbc123");
            session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
            session.setPassword("@BSbc123456");
            session.setConfig("StrictHostKeyChecking", "no");
            session.setServerAliveInterval(180000);
            if (host != null && post != null) {
                ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                session.setProxy(proxy);
            }

            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.put(stream, "/upload/AgilityBackup/" + fileName);
            //            sftpChannel.rm("/upload/testUpload1.dat");
            //                sftpChannel.rename( "/upload/testUpload1.dat",  "/upload/oweidi.dat");
            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException ex) {
            Logger.getLogger(SFTPUtility.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (SftpException ex) {
            Logger.getLogger(SFTPUtility.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;

    }
    
    public static File readSFTPFile(String fileName) {
        FileInputStream fstream = null;
        OutputStream outputStream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        try {

            String host = System.getProperty("http.proxyHost");
            String post = System.getProperty("http.proxyPort");

//          session = jsch.getSession("kwprbsbc", "85.239.8.202", 22);
//            session.setPassword("@bsbc123");
            session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
            session.setPassword("@BSbc123456");
            session.setConfig("StrictHostKeyChecking", "no");
            session.setServerAliveInterval(180000);
            if (host != null && post != null) {
                ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                session.setProxy(proxy);
            }
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftpChannel = (ChannelSftp) channel;
            fileSFTP = sftpChannel.get("/upload/" + fileName);

            // Open the file
            String filename = "OTL";
            File file = File.createTempFile(filename, ".xml");

            // write the inputStream to a FileOutputStream
            outputStream
                    = new FileOutputStream(file);

            int read = 0;
            byte[] bytes = new byte[2056];

            while ((read = fileSFTP.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            fstream = new FileInputStream(file);
            return file;
//           BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (JSchException e) {
            e.printStackTrace();
            return null;
        } catch (SftpException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                if (session != null) {
                    session.disconnect();
                }
                if (fstream != null) {
                    fstream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
