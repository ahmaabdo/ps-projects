/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsbc.agility2bsbc.ItemLotQuantity;
import com.appspro.bsbc.agility2bsbc.ItemQuantity;
import com.appspro.bsbc.agility2bsbc.ItemTransactions;
import com.appspro.bsbc.agility2bsbc.ReceivingItem;

/**
 *
 * @author Hp
 */
public class FacadeIntegration {

    public static String DELIMITER = "~";

    public static StringBuilder createCustomerSalesOrder() {
        CustomerSalesOrder customerSalesOrder = new CustomerSalesOrder();
        return customerSalesOrder.createFile();
    }

    public static StringBuilder createItemMasterIntegration() {
        ItemMasterIntegration itemMasterIntegration = new ItemMasterIntegration();
        return itemMasterIntegration.createFile();
    }

    public static StringBuilder createMaterialTransfer() {
        MaterialTransfer materialTransfer = new MaterialTransfer();
        return materialTransfer.createFile();
    }

    public static StringBuilder createPOReturnToSupplier() {
        POReturnToSupplier pOReturnToSupplier = new POReturnToSupplier();
        return pOReturnToSupplier.createFile();
    }

    public static StringBuilder createPurchaseOrderIntegration() {
        PurchaseOrderIntegration purchaseOrderIntegration = new PurchaseOrderIntegration();
        return purchaseOrderIntegration.createFile();
    }

    public static StringBuilder createStockAdjustment() {
        StockAdjustment stockAdjustment = new StockAdjustment();
        return stockAdjustment.createFile();
    }

    public static boolean createItemLotQuantity() {
        ItemLotQuantity itemLotQuantity = new ItemLotQuantity();
        return itemLotQuantity.insert();
    }

    public static boolean createItemQuantity() {
        ItemQuantity itemQuantity = new ItemQuantity();
        return itemQuantity.insert();
    }

    public static boolean createItemTransactions() {
        ItemTransactions itemTransactions = new ItemTransactions();
        return itemTransactions.insert();
    }

    public static boolean createReceivingItem() {
        ReceivingItem receivingItem = new ReceivingItem();
        return receivingItem.getItemData() == null ? false : true;
    }

    public static void main(String arp[]) {
        System.out.println("createCustomerSalesOrder = " + FacadeIntegration.createCustomerSalesOrder() != null ? "true" : "false"); //1
        System.out.println("createItemMasterIntegration = " + FacadeIntegration.createItemMasterIntegration()); //2
        System.out.println("createMaterialTransfer = " + FacadeIntegration.createMaterialTransfer()); //3
        System.out.println("createPOReturnToSupplier = " + FacadeIntegration.createPOReturnToSupplier()); //4
        System.out.println("createPurchaseOrderIntegration = " + FacadeIntegration.createPurchaseOrderIntegration()); //5
        System.out.println("createStockAdjustment = " + FacadeIntegration.createStockAdjustment()); //6
//        System.out.println("createItemQuantity = " + FacadeIntegration.createItemQuantity()); //7
//        System.out.println("createItemTransactions = " + FacadeIntegration.createItemTransactions()); //8
//        System.out.println("createItemLotQuantity = " + FacadeIntegration.createItemLotQuantity()); //9
//        System.out.println("createReceivingItem = " + FacadeIntegration.createReceivingItem()); //10
    }

    public static void runSFTP() {
        System.out.println("createCustomerSalesOrder = " + FacadeIntegration.createCustomerSalesOrder() != null ? "true" : "false"); //1
        System.out.println("createItemMasterIntegration = " + FacadeIntegration.createItemMasterIntegration()); //2
        System.out.println("createMaterialTransfer = " + FacadeIntegration.createMaterialTransfer()); //3
        System.out.println("createPOReturnToSupplier = " + FacadeIntegration.createPOReturnToSupplier()); //4
        System.out.println("createPurchaseOrderIntegration = " + FacadeIntegration.createPurchaseOrderIntegration()); //5
        System.out.println("createStockAdjustment = " + FacadeIntegration.createStockAdjustment()); //6
        // System.out.println("createReceivingItem = " + FacadeIntegration.createReceivingItem());
    }

    public static void runSFTPLot() {
        System.out.println("createItemLotQuantity = " + FacadeIntegration.createItemLotQuantity()); //1
    }

    public static void runSFTPItem() {
          System.out.println("createItemQuantity = " + FacadeIntegration.createItemQuantity()); //1
    }
}
