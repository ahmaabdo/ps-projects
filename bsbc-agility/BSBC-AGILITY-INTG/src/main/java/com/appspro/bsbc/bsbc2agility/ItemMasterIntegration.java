/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsb.biReprot.BIPReports;
import com.appspro.bsb.biReprot.BIReportModel;
import com.appspro.bsbc.SFTPUtility;
import com.appspro.bsbc.utility.Utility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author Hp
 */
public class ItemMasterIntegration {

    private final String STORERKEY_BSBC = "108815";
    private final String ABC = "A";
    private final String TARIFFKEY = "0000001895";

    public static void main(String arp[]) {
        new ItemMasterIntegration().createFile();
    }

    public StringBuilder createFile() {
        //Calling 
        StringBuilder itemLines = new StringBuilder();
        JSONObject json = BIReportModel.runWithParamReport(BIPReports.REPORT_NAME.GET_ITEM_TYPE.getValue());
        
        JSONObject dataDS = json.getJSONObject("DATA_DS");
        if (!dataDS.has("G_1")) {
            return null;
        }
        Object jsonTokner = new JSONTokener(dataDS.get("G_1").toString()).nextValue();
        if (jsonTokner instanceof JSONObject) {
            JSONObject g1 = dataDS.getJSONObject("G_1");
            JSONArray arrJson = new JSONArray();
            arrJson.put(g1);
            itemLines = new ItemMasterIntegration().checkNullable(arrJson);
        } else {
            JSONArray g1 = dataDS.getJSONArray("G_1");
            itemLines = new ItemMasterIntegration().checkNullable(g1);
        }

        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
        Date date = new Date();
//        return itemLines;
        boolean result = SFTPUtility.SEND_SFTP_FILE(itemLines, "ITMobsbc_10" + dateFormat.format(date) + ".chr");
         result = SFTPUtility.SEND_SFTP_FILE_BACKUP(itemLines, "ITMobsbc_10" + dateFormat.format(date) + ".chr");
        return null;
//        return result;
    }

    private StringBuilder createItemDetails(String itemCode, String description, String longDescription, String itemGroupType, String EXPIRY, String itemCategory, String businessUnitCode) {
        StringBuilder itemLine = new StringBuilder();

        itemLine.append("\"" + STORERKEY_BSBC + "\"");
        itemLine.append(FacadeIntegration.DELIMITER);
        String skuFormated = itemCode != null ? itemCode.replaceAll("-", "").replaceAll("\\|", "~") : itemCode;
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(skuFormated));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(description));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(longDescription));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(itemGroupType));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(ABC));
        itemLine.append(FacadeIntegration.DELIMITER);

        itemLine.append((EXPIRY));

        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);

        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(TARIFFKEY));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(itemCategory));
        itemLine.append("\n");
        String removeLines = itemLine.toString().replaceAll("\\n", "").replaceAll("\\r", "");
        return new StringBuilder(removeLines).append("\r\n");

    }

    public   StringBuilder checkNullable(JSONArray g1) {
        StringBuilder itemLines = new StringBuilder();

        for (int i = 0; i < g1.length(); i++) {
            String SKU = "";
            String DESCR = "";
            String GROUP = "";
            String EXPIRY = "";
            String ITEMTYPE = "";
            String COMPANY = "";
            String Adescription = "";

            SKU = (!g1.getJSONObject(i).isNull("SKU") ? g1.getJSONObject(i).get("SKU").toString() : "");
            DESCR = (!g1.getJSONObject(i).isNull("DESCR") ? g1.getJSONObject(i).get("DESCR").toString() : "");
            Adescription = (!g1.getJSONObject(i).isNull("Adescription") ? g1.getJSONObject(i).get("Adescription").toString() : "");
            GROUP = (!g1.getJSONObject(i).isNull("GROUP") ? g1.getJSONObject(i).get("GROUP").toString() : "");
            EXPIRY = (!g1.getJSONObject(i).isNull("EXPIRY") ? g1.getJSONObject(i).get("EXPIRY").toString() : "");
            ITEMTYPE = (!g1.getJSONObject(i).isNull("ITEMTYPE") ? g1.getJSONObject(i).get("ITEMTYPE").toString() : "");
            COMPANY = (!g1.getJSONObject(i).isNull("COMPANY") ? g1.getJSONObject(i).get("COMPANY").toString() : "");

            itemLines.append(createItemDetails(SKU, DESCR, Adescription, GROUP, EXPIRY, ITEMTYPE, COMPANY));

        }
        return itemLines;
    }

}
