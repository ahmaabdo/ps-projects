/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsb.biReprot.BIPReports;
import com.appspro.bsb.biReprot.BIReportModel;
import com.appspro.bsbc.SFTPUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author CPBSLV
 */
public class POReturnToSupplierSingle {

    private final String STORERKEY = "108815";
    private final String ITEMTYPE = "0";
    private final String ITEMTYPE1 = "1";
    private final String DOCUMENTTYPE = "INV";
    private final String DOCUMENTTYPE1 = "DNC";

    public static void main(String arp[]) {
        //new POReturnToSupplier().createFile();
        System.out.print(new POReturnToSupplier().createFile());
    }

    public StringBuilder createFile(String invoice) {

        //Calling 
        try {
            StringBuilder itemLines = new StringBuilder();
            JSONObject json = BIReportModel.runWithParamReportSO(BIPReports.REPORT_NAME.PO_RETURN.getValue(),invoice);
            System.out.println(json);
            Object jsonTokner1 = json.opt("DATA_DS");
            if (jsonTokner1 instanceof JSONObject) {

            } else {
                return null;    
            }
            if (json == null) {
                return null;
            }

            if (json.length() <= 0) {
                return null;
            }
            if (json.isNull("DATA_DS")) {
                return null;
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");
            Object jsonTokner = new JSONTokener(dataDS.get("G_3").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.getJSONObject("G_3");
                JSONArray arrJson = new JSONArray();
                arrJson.put(g1);
                itemLines = new POReturnToSupplier().checkNullable(arrJson);
            } else {
                JSONArray g1 = dataDS.getJSONArray("G_3");
                itemLines = new POReturnToSupplier().checkNullable(g1);
            }

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
            Date date = new Date();

//            return itemLines;
            boolean result = SFTPUtility.SEND_SFTP_FILE(itemLines, "RTsobsbc_10" + dateFormat.format(date) + ".chr");
            return null;
//            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

 
}
