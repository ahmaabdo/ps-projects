/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsb.biReprot.BIPReports;
import com.appspro.bsb.biReprot.BIReportModel;
import com.appspro.bsbc.SFTPUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author CPBSLV
 */
public class PurchaseOrderIntegration {

    private final String CONTAINERKEY = "NA";
    private final String STORERKEY = "108815";
    private final String ITEMTYPE = "0";
    private final String ITEMTYPE1 = "1";
    private final String VALUE = "0";

    public static void main(String arp[]) {
        System.out.println(new PurchaseOrderIntegration().createFile());

    }

    public StringBuilder createFile() {
        //Calling 
        StringBuilder itemLines = new StringBuilder();
        JSONObject json = BIReportModel.runReport(BIPReports.REPORT_NAME.PO_CREATION.getValue());
        JSONObject dataDS = json.getJSONObject("DATA_DS");
        if (!dataDS.has("G_1")) {
            return null;
        }
        Object jsonTokner = new JSONTokener(dataDS.get("G_1").toString()).nextValue();
        if (jsonTokner instanceof JSONObject) {
            JSONObject g1 = dataDS.getJSONObject("G_1");
            JSONArray arrJson = new JSONArray();
            arrJson.put(g1);
            itemLines = new PurchaseOrderIntegration().checkNullable(arrJson);
        } else {
            JSONArray g1 = dataDS.getJSONArray("G_1");
            itemLines = new PurchaseOrderIntegration().checkNullable(g1);
        }
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
        Date date = new Date();
//        return itemLines;
        boolean result = SFTPUtility.SEND_SFTP_FILE(itemLines, "PObsbc_10" + dateFormat.format(date) + ".chr");
        return null;
//        return result;
    }

    private StringBuilder createPurchaseOrderDetails(String poNumber, String supplierCODE, String supplierName, String poDate, String sku, String skuDescr, String manufactureSkuCode,
            String qty, String uom, String awb, String location,
            String sectionCode, String poReference, String documentType, String Itemtype) {
        //POV1800488|NA|4101|SIMEON MEDICAL     ( EURO)|18/09/2018|4101000010|SIM.LED 250 JOINT ARM MOBILE WITH ADJ. HEIGHT   #161-0015691|4101000010|1|PC|108815||PWC|1||140|18140014/SM/STOCK - DEMO-INTERNALLY AMENDED|9
        StringBuilder itemLine = new StringBuilder();

        itemLine.append(poNumber != null ? poNumber.replaceAll("POV", "") : null);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(CONTAINERKEY);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(supplierCODE);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(supplierName);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(poDate);
        itemLine.append(FacadeIntegration.DELIMITER);
        String skuFormated = sku != null ? sku.replaceAll("-", "").replaceAll("\\|", "~"): sku;
        itemLine.append((skuFormated));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(skuDescr);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(manufactureSkuCode);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(qty);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(uom);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(STORERKEY);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(awb);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(location);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Itemtype);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(VALUE);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(sectionCode);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(poReference);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(documentType);

        itemLine.append("\n");
        String removeLines = itemLine.toString().replaceAll("\\n", "").replaceAll("\\r", "");
        return new StringBuilder(removeLines).append("\r\n");

    }

    public StringBuilder checkNullable(JSONArray g1) {
        StringBuilder itemLines = new StringBuilder();

        for (int i = 0; i < g1.length(); i++) {
            String PONUMBER = "";
            String SUPPLIERCODE = "";
            String SUPPLIERNAME = "";
            String PODATE = "";
            String SKU = "";
            String SKUDESCR = "";
            String MANUFACTURE_SKUCODE = "";
            String QTY = "";
            String UOM = "";
            String AWB = "";
            String LOCATION = "";
            String SECTIONCODE = "";
            String POREFERENCE = "";
            String DOCUMENTTYPE = "";
            String ITEMTYPE = "";

            PONUMBER = (!g1.getJSONObject(i).isNull("PONUMBER") ? g1.getJSONObject(i).get("PONUMBER").toString() : "");
            SUPPLIERCODE = (!g1.getJSONObject(i).isNull("SUPPLIERCODE") ? g1.getJSONObject(i).get("SUPPLIERCODE").toString() : "");
            SUPPLIERNAME = (!g1.getJSONObject(i).isNull("SUPPLIERNAME") ? g1.getJSONObject(i).get("SUPPLIERNAME").toString() : "");
            PODATE = (!g1.getJSONObject(i).isNull("PODATE") ? g1.getJSONObject(i).get("PODATE").toString() : "");
            SKU = (!g1.getJSONObject(i).isNull("SKU") ? g1.getJSONObject(i).get("SKU").toString() : "");
            SKUDESCR = (!g1.getJSONObject(i).isNull("SKUDESCR") ? g1.getJSONObject(i).get("SKUDESCR").toString() : "");
            MANUFACTURE_SKUCODE = (!g1.getJSONObject(i).isNull("MANUFACTURE-SKUCODE") ? g1.getJSONObject(i).get("MANUFACTURE-SKUCODE").toString() : "");
            QTY = (!g1.getJSONObject(i).isNull("QTY") ? g1.getJSONObject(i).get("QTY").toString() : "");
            UOM = (!g1.getJSONObject(i).isNull("UOM") ? g1.getJSONObject(i).get("UOM").toString() : "");
            AWB = (!g1.getJSONObject(i).isNull("AWB") ? g1.getJSONObject(i).get("AWB").toString() : "");
            LOCATION = (!g1.getJSONObject(i).isNull("LOCATION") ? g1.getJSONObject(i).get("LOCATION").toString() : "");
            SECTIONCODE = (!g1.getJSONObject(i).isNull("SECTIONCODE") ? g1.getJSONObject(i).get("SECTIONCODE").toString() : "");
            POREFERENCE = (!g1.getJSONObject(i).isNull("POREFERENCE") ? g1.getJSONObject(i).get("POREFERENCE").toString() : "");
            DOCUMENTTYPE = (!g1.getJSONObject(i).isNull("DOCUMENTTYPE") ? g1.getJSONObject(i).get("DOCUMENTTYPE").toString() : "");
            ITEMTYPE = (!g1.getJSONObject(i).isNull("ITEMTYPE") ? g1.getJSONObject(i).get("ITEMTYPE").toString() : "");

            itemLines.append(createPurchaseOrderDetails(PONUMBER, SUPPLIERCODE, SUPPLIERNAME, PODATE, SKU, SKUDESCR, MANUFACTURE_SKUCODE,
                    QTY, UOM, AWB, LOCATION, SECTIONCODE, POREFERENCE, DOCUMENTTYPE, ITEMTYPE));

        }
        return itemLines;
    }
}
