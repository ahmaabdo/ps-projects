/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.utility;

import com.appspro.bsbc.bsbc2agility.CustomerSalesOrderSingle;
import com.appspro.bsbc.bsbc2agility.ItemMasterIntegrationSingle;
import com.appspro.bsbc.bsbc2agility.MaterialTransferSingle;
import com.appspro.bsbc.bsbc2agility.POReturnToSupplierSingle;
import com.appspro.bsbc.bsbc2agility.PurchaseOrderIntegrationSingle;
import com.appspro.bsbc.bsbc2agility.StockAdjustmentSingle;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hp
 */
public class SendSingleSalesOrder extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
                
        String salesOrderNumber = request.getParameter("salesOrder"); 
        
        if(salesOrderNumber != null && !salesOrderNumber.isEmpty()) {
            new CustomerSalesOrderSingle().createFile(salesOrderNumber,null);
        }
        
        String poOrderNum = request.getParameter("poOrderNum"); 
        
        if(poOrderNum != null && !poOrderNum.isEmpty()) {
            new PurchaseOrderIntegrationSingle().createFile(poOrderNum);
        }
        
        String itemMaster = request.getParameter("itemMaster"); 
        
        if(itemMaster != null && !itemMaster.isEmpty()) {
            new ItemMasterIntegrationSingle().createFile(itemMaster);
        }
        
        String matrialTransfer = request.getParameter("matrialTransfer"); 
        
        if(matrialTransfer != null && !matrialTransfer.isEmpty()) {
            new MaterialTransferSingle().createFile(matrialTransfer);
        }
        
        String poRetrun = request.getParameter("poRetrun"); 
        
        if(poRetrun != null && !poRetrun.isEmpty()) {
            new POReturnToSupplierSingle().createFile(poRetrun);
        }
        
        String stockAjdusment = request.getParameter("stockAjdusment"); 
        
        if(stockAjdusment != null && !stockAjdusment.isEmpty()) {
            new StockAdjustmentSingle().createFile(stockAjdusment);
        }
        String shipmentNumber = request.getParameter("shipmentNumber"); 
        
        if(shipmentNumber != null && !shipmentNumber.isEmpty()) {
            new CustomerSalesOrderSingle().createFile(null,shipmentNumber);
        }
        response.sendRedirect("sendSingleSalesOrder.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
