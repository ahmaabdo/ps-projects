/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.agility2bsbc;

import com.appspro.bsb.restHelper.RestHelper;
import com.appspro.bsbc.generation.ItemTransactions.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author CPBSLV
 */
public class ItemTransactions {

    public static void main(String argp[]) throws IOException {
        System.out.println(new ItemTransactions().insert());
    }

    public Message getItemData() {

        FileInputStream fstream = null;
        OutputStream outputStream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        String renamedFile = null;
        boolean check = true;
        Message itemObj = new Message();
        String response = null;
        try {

            try {
                String host = System.getProperty("http.proxyHost");
                String post = System.getProperty("http.proxyPort");
                session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("@BSbc123456");
                if (host != null || post != null) {
                    ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                    session.setProxy(proxy);
                }
                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect();
                sftpChannel = (ChannelSftp) channel;
                sftpChannel.cd("/upload");
                Vector filelist = sftpChannel.ls("/upload");
                List<String> fileNameList = new ArrayList<String>();
                String fileName = null;
                for (int i = 0; i < filelist.size(); i++) {
                    ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);
                    if (entry.getFilename().startsWith("Inventory Transaction.xml")) {
                        fileName = entry.getFilename();
                        fileNameList.add(fileName);
                    }

                }
                if (fileNameList != null && !fileNameList.isEmpty()) {
                    for (int i = 0; i < fileNameList.size(); i++) {
                        fileSFTP = sftpChannel.get("/upload/" + fileNameList.get(i));
                        renamedFile = fileNameList.get(i);

                        // Open the file
                        String filename = "OTL";
                        File file = File.createTempFile(filename, ".zip");

                        // write the inputStream to a FileOutputStream
                        outputStream
                                = new FileOutputStream(file);

                        int read = 0;
                        byte[] bytes = new byte[2056];

                        while ((read = fileSFTP.read(bytes)) != -1) {
                            outputStream.write(bytes, 0, read);
                        }

                        if (file == null) {
                            return null;
                        }
                        JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);
                        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                        itemObj = (Message) jaxbUnmarshaller.unmarshal(file);
//
//                        sftpChannel.rename("/upload/" + renamedFile,
//                                "/upload/AgilityLotAndWithOutLot/" + renamedFile);
//                        sftpChannel.cd("/upload/");
                    }

//                sftpChannel.rm(renamedFile);
                } else {
                    check = false;
                }

            } catch (JAXBException ex) {
                Logger.getLogger(ReceivingItem.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReceivingItem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReceivingItem.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException ex) {
//            Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        } finally {
            try {

                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                if (session != null) {
                    session.disconnect();
                }
                if (fstream != null) {
                    fstream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ItemQuantity.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }
        }
        return itemObj;
    }

//    public Message getItemData() {
//        Message itemObj = new Message();
//        try {
//
//            File file = SFTPUtility.readSFTPFile("Inventory Transaction.xml");
//            if (file == null) {
//                return null;
//            }
//            JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);
//
//            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//            itemObj = (Message) jaxbUnmarshaller.unmarshal(file);
//
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//        return itemObj;
//    }
    private final String USER_AGENT = "Mozilla/5.0";

    private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

    public boolean insert() {
        try {
            String url = "https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/AppsproAgility/itemtransactions";
            Message message = new ItemTransactions().getItemData();
            if (message == null) {
                return true;
            }
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message);

            RestHelper.callPostRest(url, " text/plain", jsonInString, null);
            return true;

        } catch (IOException ex) {
            Logger.getLogger(ItemTransactions.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
