///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.appspro.bsbc.mail;
//
//import com.appspro.bsbc.bsbc2agility.CustomerSalesOrderSingle;
//import com.appspro.bsbc.bsbc2agility.FacadeIntegration;
//import com.appspro.bsbc.bsbc2agility.PurchaseOrderIntegrationSingle;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Properties;
//import java.util.TimeZone;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.activation.DataHandler;
//import javax.activation.DataSource;
//import javax.activation.FileDataSource;
//import javax.mail.BodyPart;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.Multipart;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
////import oracle.security.jps.service.credstore.CredentialStore;
////import oracle.security.jps.service.credstore.CredentialFactory;
////import oracle.security.jps.service.credstore.Credential;
////import oracle.security.jps.service.credstore.PasswordCredential;
//
//public class CredentialStoreClassTest {
////
////    public void CredentialStoreTest() {
////        try {
////
////            CredentialStore credentialStore = oracle.security.jps.service.JpsServiceLocator.getServiceLocator().lookup(CredentialStore.class);
////
////            String map = "user.custom.map";
////            String mykey = "AppsProMail";
////            try {
////                System.out.println("Creating map " + map);
////                credentialStore.setCredential(map, mykey, CredentialFactory.newPasswordCredential("AppsPro.rest", "Bsbc@1234".toCharArray()));
////                System.out.println("Password set");
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////            try {
////                System.out.println("Accessing credential");
////                Credential cred = credentialStore.getCredential(map, mykey);
////                System.out.println("Password got:" + cred);
////                if (cred != null) {
////                    System.out.println("Password got:" + new String(((PasswordCredential) cred).getPassword()));
////                }
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////        } catch (Exception eg) {
////            eg.printStackTrace();
////        }
////    }
////
////    public static void SEND_MAIL() {
////        // To send email in JCSSX, you do not need to specify any property. You only need to provide the user id
////        // and password of a user with the Java_Notification_User custom role.
//////        Properties props = newProperties();
////
////        //You can specify the user and password by providing an authenticator when creating the session, like this:
////        /*Session session = Session.getInstance(props,
////                newjavax.mail.Authenticator() {
////                      protectedPasswordAuthentication 
////getPasswordAuthentication() {
////                      return new
////PasswordAuthentication(uname, pwd);
////                     }
////                }
////   );*/
////        // If you do not specify an authenticator for the session, you can provide the user and password when
////        // connecting the transport
////        CredentialStoreClassTest test = new CredentialStoreClassTest();
////        Properties props = new Properties();
////        Session session = Session.getInstance(props);
////        try {
////            InternetAddress[] recipientAddressCC = {new InternetAddress("abdullah.oweidi@appspro-me.com")};
////            InternetAddress[] recipientAddressTO = {new InternetAddress("IT@sultan.com.kw")};
////            MimeMessage msg = new MimeMessage(session);
////            msg.setFrom(new InternetAddress("abdullah.oweidi@appspro-me.com"));
////            msg.addRecipients(Message.RecipientType.CC, recipientAddressCC);
////            msg.addRecipients(Message.RecipientType.TO, recipientAddressTO);
////            msg.setSubject("BSBC Agility Integration Files");
////            msg.setSentDate(new Date());
////            Multipart multipart = new MimeMultipart();
////            MimeBodyPart textPart = new MimeBodyPart();
////            textPart.setText("Dears,\n PFA");
////            multipart.addBodyPart(textPart);
////            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
////            dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kuwait"));
////            Date date = new Date();
////
////            StringBuilder stringBuilderSO = FacadeIntegration.createCustomerSalesOrder();
////            StringBuilder stringBuilderIMI = FacadeIntegration.createItemMasterIntegration();
////            StringBuilder stringBuilderMT = FacadeIntegration.createMaterialTransfer();
////            StringBuilder stringBuilderPOR = FacadeIntegration.createPOReturnToSupplier();
////            StringBuilder stringBuilderPOI = FacadeIntegration.createPurchaseOrderIntegration();
////            StringBuilder stringBuilderSA = FacadeIntegration.createStockAdjustment();
////
////            if (stringBuilderSO != null) {
////                addAttachment(multipart, stringBuilderSO, "CSsobsbc_10" + dateFormat.format(date) + ".chr");
////            }
////            if (stringBuilderIMI != null) {
////                addAttachment(multipart, stringBuilderIMI, "ITMobsbc_10" + dateFormat.format(date) + ".chr");
////            }
////            if (stringBuilderMT != null) {
////                addAttachment(multipart, stringBuilderMT, "STobsbc_10" + dateFormat.format(date) + ".chr");
////            }
////            if (stringBuilderPOR != null) {
////                addAttachment(multipart, stringBuilderPOR, "RTsobsbc_10" + dateFormat.format(date) + ".chr");
////            }
////            if (stringBuilderPOI != null) {
////                addAttachment(multipart, stringBuilderPOI, "PObsbc_10" + dateFormat.format(date) + ".chr");
////            }
////            if (stringBuilderSA != null) {
////                addAttachment(multipart, stringBuilderSA, "SAobsbc_10" + dateFormat.format(date) + ".chr");
////            }
////
////            msg.setContent(multipart);
////            Transport transport = session.getTransport("smtp");
////            // Provide the user and password if you do not specify an authenticator when creating the session
////            transport.connect("AppsPro.rest", "Bsbc@1234");
////            // If you provide an authenticator when creating the session, you do not need user and password in next line
////            //transport.connect();
////            transport.sendMessage(msg, recipientAddressTO);
////        } catch (MessagingException e) {
////            e.printStackTrace();
////        } catch (IOException ex) {
////            Logger.getLogger(CredentialStoreClassTest.class.getName()).log(Level.SEVERE, null, ex);
////        }
////    }
////
////    public static void SEND_MAIL_SINGLE(String salesOrderNumber) {
////
////        CredentialStoreClassTest test = new CredentialStoreClassTest();
////        Properties props = new Properties();
////        Session session = Session.getInstance(props);
////        try {
////            InternetAddress[] recipientAddressCC = {new InternetAddress("abdullah.oweidi@appspro-me.com")};
////            InternetAddress[] recipientAddressTO = {new InternetAddress("IT@sultan.com.kw")};
////            MimeMessage msg = new MimeMessage(session);
////            msg.setFrom(new InternetAddress("abdullah.oweidi@appspro-me.com"));
//////            msg.addRecipients(Message.RecipientType.CC, recipientAddressCC);
////            msg.addRecipients(Message.RecipientType.TO, recipientAddressTO);
////            msg.setSubject("BSBC Agility Integration Files");
////            msg.setSentDate(new Date());
////            Multipart multipart = new MimeMultipart();
////            MimeBodyPart textPart = new MimeBodyPart();
////            textPart.setText("Dears,\n PFA");
////            multipart.addBodyPart(textPart);
////            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
////            Date date = new Date();
////            CustomerSalesOrderSingle sc = new CustomerSalesOrderSingle();
////
////            StringBuilder stringBuilderSO = sc.createFile(salesOrderNumber);;
////
////            if (stringBuilderSO != null) {
////                addAttachment(multipart, stringBuilderSO, "CSsobsbc_10" + dateFormat.format(date) + ".chr");
////            }
////
////            msg.setContent(multipart);
////            Transport transport = session.getTransport("smtp");
////            // Provide the user and password if you do not specify an authenticator when creating the session
////            transport.connect("AppsPro.rest", "Bsbc@1234");
////            // If you provide an authenticator when creating the session, you do not need user and password in next line
////            //transport.connect();
////            transport.sendMessage(msg, recipientAddressTO);
////        } catch (MessagingException e) {
////            e.printStackTrace();
////        } catch (IOException ex) {
////            Logger.getLogger(CredentialStoreClassTest.class.getName()).log(Level.SEVERE, null, ex);
////        }
////    }
////
////    public static void SEND_MAIL_PO_SINGLE(String salesOrderNumber) {
////
////        CredentialStoreClassTest test = new CredentialStoreClassTest();
////        Properties props = new Properties();
////        Session session = Session.getInstance(props);
////        try {
////            InternetAddress[] recipientAddressCC = {new InternetAddress("abdullah.oweidi@appspro-me.com")};
////            InternetAddress[] recipientAddressTO = {new InternetAddress("IT@sultan.com.kw")};
////            MimeMessage msg = new MimeMessage(session);
////            msg.setFrom(new InternetAddress("abdullah.oweidi@appspro-me.com"));
////            msg.addRecipients(Message.RecipientType.CC, recipientAddressCC);
////            msg.addRecipients(Message.RecipientType.TO, recipientAddressTO);
////            msg.setSubject("BSBC Agility Integration Files");
////            msg.setSentDate(new Date());
////            Multipart multipart = new MimeMultipart();
////            MimeBodyPart textPart = new MimeBodyPart();
////            textPart.setText("Dears,\n PFA");
////            multipart.addBodyPart(textPart);
////            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
////            Date date = new Date();
////            PurchaseOrderIntegrationSingle sc = new PurchaseOrderIntegrationSingle();
////
////            StringBuilder stringBuilderSO = sc.createFile(salesOrderNumber);;
////
////            if (stringBuilderSO != null) {
////                addAttachment(multipart, stringBuilderSO, "PObsbc_10" + dateFormat.format(date) + ".chr");
////            }
////
////            msg.setContent(multipart);
////            Transport transport = session.getTransport("smtp");
////            // Provide the user and password if you do not specify an authenticator when creating the session
////            transport.connect("AppsPro.rest", "Bsbc@1234");
////            // If you provide an authenticator when creating the session, you do not need user and password in next line
////            //transport.connect();
////            transport.sendMessage(msg, recipientAddressTO);
////        } catch (MessagingException e) {
////            e.printStackTrace();
////        } catch (IOException ex) {
////            Logger.getLogger(CredentialStoreClassTest.class.getName()).log(Level.SEVERE, null, ex);
////        }
////    }
////
////    private static void addAttachment(Multipart multipart, StringBuilder stringBuilder, String filename) throws MessagingException, IOException {
////        File file = File.createTempFile(filename, ".chr");
////        BufferedWriter writer = null;
////        try {
////            writer = new BufferedWriter(new FileWriter(file));
////            writer.write(stringBuilder.toString());
////        } finally {
////            if (writer != null) {
////                writer.close();
////            }
////        }
////        DataSource source = new FileDataSource(file);
////        BodyPart messageBodyPart = new MimeBodyPart();
////        messageBodyPart.setDataHandler(new DataHandler(source));
////        messageBodyPart.setFileName(filename);
////        multipart.addBodyPart(messageBodyPart);
////    }
//
//            StringBuilder stringBuilderSO = sc.createFile(salesOrderNumber,null);
//
//            if (stringBuilderSO != null) {
//                addAttachment(multipart, stringBuilderSO, "CSsobsbc_10" + dateFormat.format(date) + ".chr");
//            }
//
//            msg.setContent(multipart);
//            Transport transport = session.getTransport("smtp");
//            // Provide the user and password if you do not specify an authenticator when creating the session
//            transport.connect("AppsPro.rest", "Bsbc@1234");
//            // If you provide an authenticator when creating the session, you do not need user and password in next line
//            //transport.connect();
//            transport.sendMessage(msg, recipientAddressTO);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        } catch (IOException ex) {
//            Logger.getLogger(CredentialStoreClassTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public static void SEND_MAIL_PO_SINGLE(String salesOrderNumber) {
//
//        CredentialStoreClassTest test = new CredentialStoreClassTest();
//        Properties props = new Properties();
//        Session session = Session.getInstance(props);
//        try {
//            InternetAddress[] recipientAddressCC = {new InternetAddress("abdullah.oweidi@appspro-me.com")};
//            InternetAddress[] recipientAddressTO = {new InternetAddress("IT@sultan.com.kw")};
//            MimeMessage msg = new MimeMessage(session);
//            msg.setFrom(new InternetAddress("abdullah.oweidi@appspro-me.com"));
//            msg.addRecipients(Message.RecipientType.CC, recipientAddressCC);
//            msg.addRecipients(Message.RecipientType.TO, recipientAddressTO);
//            msg.setSubject("BSBC Agility Integration Files");
//            msg.setSentDate(new Date());
//            Multipart multipart = new MimeMultipart();
//            MimeBodyPart textPart = new MimeBodyPart();
//            textPart.setText("Dears,\n PFA");
//            multipart.addBodyPart(textPart);
//            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
//            Date date = new Date();
//            PurchaseOrderIntegrationSingle sc = new PurchaseOrderIntegrationSingle();
//
//            StringBuilder stringBuilderSO = sc.createFile(salesOrderNumber);;
//
//            if (stringBuilderSO != null) {
//                addAttachment(multipart, stringBuilderSO, "PObsbc_10" + dateFormat.format(date) + ".chr");
//            }
//
//            msg.setContent(multipart);
//            Transport transport = session.getTransport("smtp");
//            // Provide the user and password if you do not specify an authenticator when creating the session
//            transport.connect("AppsPro.rest", "Bsbc@1234");
//            // If you provide an authenticator when creating the session, you do not need user and password in next line
//            //transport.connect();
//            transport.sendMessage(msg, recipientAddressTO);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        } catch (IOException ex) {
//            Logger.getLogger(CredentialStoreClassTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    private static void addAttachment(Multipart multipart, StringBuilder stringBuilder, String filename) throws MessagingException, IOException {
//        File file = File.createTempFile(filename, ".chr");
//        BufferedWriter writer = null;
//        try {
//            writer = new BufferedWriter(new FileWriter(file));
//            writer.write(stringBuilder.toString());
//        } finally {
//            if (writer != null) {
//                writer.close();
//            }
//        }
//        DataSource source = new FileDataSource(file);
//        BodyPart messageBodyPart = new MimeBodyPart();
//        messageBodyPart.setDataHandler(new DataHandler(source));
//        messageBodyPart.setFileName(filename);
//        multipart.addBodyPart(messageBodyPart);
//    }
//}
