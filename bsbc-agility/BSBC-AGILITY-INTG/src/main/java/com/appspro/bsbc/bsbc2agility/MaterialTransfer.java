/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bsbc2agility;

import com.appspro.bsb.biReprot.BIPReports;
import com.appspro.bsb.biReprot.BIReportModel;
import com.appspro.bsbc.SFTPUtility;
import com.appspro.bsbc.utility.Utility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author CPBSLV
 */
public class MaterialTransfer {

    private final String STORERKEY = "108815";

    public static void main(String arp[]) {
        new MaterialTransfer().createFile();
    }

    public StringBuilder createFile() {
        //Calling 
        StringBuilder itemLines = new StringBuilder();
        JSONObject json = BIReportModel.runWithParamReport(BIPReports.REPORT_NAME.MATERIAL_TRANSFER.getValue());
        JSONObject dataDS = json.getJSONObject("DATA_DS");
        if (!dataDS.has("G_1")) {
            return null;
        }
        Object jsonTokner = new JSONTokener(dataDS.get("G_1").toString()).nextValue();
        if (jsonTokner instanceof JSONObject) {
            JSONObject g1 = dataDS.getJSONObject("G_1");
            JSONArray arrJson = new JSONArray();
            arrJson.put(g1);
            itemLines = new MaterialTransfer().checkNullable(arrJson);
        } else {
            JSONArray g1 = dataDS.getJSONArray("G_1");
            itemLines = new MaterialTransfer().checkNullable(g1);
        }
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
        Date date = new Date();
        
//        return itemLines;
        boolean result = SFTPUtility.SEND_SFTP_FILE(itemLines, "STobsbc_10" + dateFormat.format(date) + ".chr");
        return null;
//        return result;
    }

    private StringBuilder createMaterialTransferDetails(String documentNumber, String documentType, String documentDate, String sourceLocation, String destinationLocation, String sku, String expiryDate, String batchNo,
            String qty, String ExternlinerNo) {
         StringBuilder itemLine = new StringBuilder();
        itemLine.append("\"" + STORERKEY + "\"");
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(documentNumber != null ? documentNumber.replaceAll("#", "") : ""));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(documentType));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(documentDate);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(sourceLocation));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(destinationLocation));
        itemLine.append(FacadeIntegration.DELIMITER);
        String skuFormated = sku != null ? sku.replaceAll("-", "").replaceAll("\\|", "~") : sku;
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(skuFormated));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(expiryDate));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(Utility.FORMAT_STRING_WITH_CUTATION(batchNo));
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(qty);
        itemLine.append(FacadeIntegration.DELIMITER);
        itemLine.append(ExternlinerNo);
        itemLine.append("\n");
        String removeLines = itemLine.toString().replaceAll("\\n", "").replaceAll("\\r", "");
        return new StringBuilder(removeLines).append("\r\n");

    }

    public StringBuilder checkNullable(JSONArray g1) {
        StringBuilder itemLines = new StringBuilder();

        for (int i = 0; i < g1.length(); i++) {
            String DOCUMENTNUMBER = "";
            String DOCUMENTTYPE = "";
            String DOCUMENTDATE = "";
            String SOURCELOCATION = "";
            String DESTINATIONLOCATION = "";
            String SKU = "";
            String EXPIRYDATE = "";
            String BATCHNUMBER = "";
            String QTY = "";
            String EXTERNLINENO = "";

            DOCUMENTNUMBER = (!g1.getJSONObject(i).isNull("DOCUMENTNUMBER") ? g1.getJSONObject(i).get("DOCUMENTNUMBER").toString() : "");
            DOCUMENTTYPE = (!g1.getJSONObject(i).isNull("DOCUMENTTYPE") ? g1.getJSONObject(i).get("DOCUMENTTYPE").toString() : "");
            DOCUMENTDATE = (!g1.getJSONObject(i).isNull("DOCUMENTDATE") ? g1.getJSONObject(i).get("DOCUMENTDATE").toString() : "");
            SOURCELOCATION = (!g1.getJSONObject(i).isNull("SOURCELOCATION") ? g1.getJSONObject(i).get("SOURCELOCATION").toString() : "");
            DESTINATIONLOCATION = (!g1.getJSONObject(i).isNull("DESTINATIONLOCATION") ? g1.getJSONObject(i).get("DESTINATIONLOCATION").toString() : "");
            SKU = (!g1.getJSONObject(i).isNull("SKU") ? g1.getJSONObject(i).get("SKU").toString() : "");
            EXPIRYDATE = (!g1.getJSONObject(i).isNull("EXPIRYDATE") ? g1.getJSONObject(i).get("EXPIRYDATE").toString() : "");
            BATCHNUMBER = (!g1.getJSONObject(i).isNull("BATCHNUMBER") ? g1.getJSONObject(i).get("BATCHNUMBER").toString() : "");
            QTY = (!g1.getJSONObject(i).isNull("QTY") ? g1.getJSONObject(i).get("QTY").toString() : "");
            EXTERNLINENO = (i + 1) + "";

            itemLines.append(createMaterialTransferDetails(DOCUMENTNUMBER, DOCUMENTTYPE, DOCUMENTDATE, SOURCELOCATION, DESTINATIONLOCATION, SKU, EXPIRYDATE,
                    BATCHNUMBER, QTY, EXTERNLINENO));

        }
        return itemLines;
    }
}
