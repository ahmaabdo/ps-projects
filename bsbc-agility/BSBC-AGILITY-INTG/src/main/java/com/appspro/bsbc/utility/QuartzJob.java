/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.utility;

import com.appspro.bsbc.bsbc2agility.FacadeIntegration;
import java.util.Date;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author Hp
 */
public class QuartzJob implements Job {

    public static boolean FIRST_TIME = true;

    @Override
    public void execute(JobExecutionContext context)
            throws JobExecutionException {
        System.out.println("here" + new Date());
          FacadeIntegration.runSFTPLot();
        System.out.println("END" + new Date());

    }

    public static boolean runJobs() {

        System.out.println("RUN JOB OF lOT!!!!!!!");
        System.out.println("here" + new Date());
        FacadeIntegration.runSFTPLot();
        System.out.println("END" + new Date());
        // Quartz 1.6.3
        // JobDetail job = new JobDetail();
        // job.setName("dummyJobName");
//        // job.setJobClass(HelloJob.class);
//        try {
//            System.out.println("Static Block" + new Date());
//            JobDetail job = JobBuilder.newJob(QuartzJob.class)
//                    .withIdentity("ReadEmployeeDataJob", "group1").build();
//
//            //Quartz 1.6.3
//            // SimpleTrigger trigger = new SimpleTrigger();
//            // trigger.setStartTime(new Date(System.currentTimeMillis() + 1000));
//            // trigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
//            // trigger.setRepeatInterval(30000);
//            //0 15 10 ? * *
//            // Trigger the job to run on the next round minute
//            Trigger trigger = TriggerBuilder
//                    .newTrigger()
//                    .withIdentity("ReadEmployeeDataTrigger", "group1")
//                    .withSchedule(
//                            CronScheduleBuilder.cronSchedule("0 0/30 5-15 ? * SUN,MON,TUE,WED,THU,SAT *"))
//                    .forJob(job)
//                    .build();
//
//            // schedule it
//            Scheduler scheduler;
//
//            scheduler = new StdSchedulerFactory().getScheduler();
//            scheduler.addJob(job, true);
//            scheduler.scheduleJob(trigger);
//            scheduler.scheduleJob(trigger1);
//            scheduler.scheduleJob(trigger2);
//            scheduler.start();

//            scheduler.scheduleJob(job, trigger1);
//        } catch (SchedulerException ex) {
//            ex.printStackTrace();
//            return false;
//        }
        return true;
    }

    public static void main(String args[]) {
        FacadeIntegration.runSFTPItem();
    }
}
