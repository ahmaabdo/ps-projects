//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.10.29 at 01:31:47 PM AST 
//
package com.appspro.bsbc.generation.ItemQuantity;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MessageHeader">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MessageID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MessageDate" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="MessageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ProjectID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Sender">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CompanyID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SystemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Receiver">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CompanyID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SystemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="MessageDetail">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IBD">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IBDHeader">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="STORERKEY" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="DOCUMENTTIME" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;element name="IBDDetail" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="LINENO" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                                 &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="SKUDESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="QTYONHAND" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                                 &lt;element name="QTYALLOCATED" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                                 &lt;element name="QTYPICKED" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                                 &lt;element name="OPENQTY" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messageHeader",
    "messageDetail"
})
@XmlRootElement(name = "Message")
public class Message {

    @XmlElement(name = "MessageHeader", required = true)
    protected Message.MessageHeader messageHeader;
    @XmlElement(name = "MessageDetail", required = true)
    protected Message.MessageDetail messageDetail;

    /**
     * Gets the value of the messageHeader property.
     *
     * @return possible object is {@link Message.MessageHeader }
     *
     */
    public Message.MessageHeader getMessageHeader() {
        return messageHeader;
    }

    /**
     * Sets the value of the messageHeader property.
     *
     * @param value allowed object is {@link Message.MessageHeader }
     *
     */
    public void setMessageHeader(Message.MessageHeader value) {
        this.messageHeader = value;
    }

    /**
     * Gets the value of the messageDetail property.
     *
     * @return possible object is {@link Message.MessageDetail }
     *
     */
    public Message.MessageDetail getMessageDetail() {
        return messageDetail;
    }

    /**
     * Sets the value of the messageDetail property.
     *
     * @param value allowed object is {@link Message.MessageDetail }
     *
     */
    public void setMessageDetail(Message.MessageDetail value) {
        this.messageDetail = value;
    }

    /**
     * <p>
     * Java class for anonymous complex type.
     *
     * <p>
     * The following schema fragment specifies the expected content contained
     * within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IBD">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IBDHeader">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="STORERKEY" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="DOCUMENTTIME" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                             &lt;element name="IBDDetail" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="LINENO" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                                       &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="SKUDESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="QTYONHAND" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                                       &lt;element name="QTYALLOCATED" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                                       &lt;element name="QTYPICKED" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                                       &lt;element name="OPENQTY" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ibd"
    })
    public static class MessageDetail {

        @XmlElement(name = "IBD", required = true)
        protected Message.MessageDetail.IBD ibd;

        /**
         * Gets the value of the ibd property.
         *
         * @return possible object is {@link Message.MessageDetail.IBD }
         *
         */
        public Message.MessageDetail.IBD getIBD() {
            return ibd;
        }

        /**
         * Sets the value of the ibd property.
         *
         * @param value allowed object is {@link Message.MessageDetail.IBD }
         *
         */
        public void setIBD(Message.MessageDetail.IBD value) {
            this.ibd = value;
        }

        /**
         * <p>
         * Java class for anonymous complex type.
         *
         * <p>
         * The following schema fragment specifies the expected content
         * contained within this class.
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IBDHeader">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="STORERKEY" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="DOCUMENTTIME" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                   &lt;element name="IBDDetail" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="LINENO" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                             &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="SKUDESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="QTYONHAND" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                             &lt;element name="QTYALLOCATED" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                             &lt;element name="QTYPICKED" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                             &lt;element name="OPENQTY" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ibdHeader"
        })
        public static class IBD {

            @XmlElement(name = "IBDHeader", required = true)
            protected Message.MessageDetail.IBD.IBDHeader ibdHeader;

            /**
             * Gets the value of the ibdHeader property.
             *
             * @return possible object is
             *     {@link Message.MessageDetail.IBD.IBDHeader }
             *
             */
            public Message.MessageDetail.IBD.IBDHeader getIBDHeader() {
                return ibdHeader;
            }

            /**
             * Sets the value of the ibdHeader property.
             *
             * @param value allowed object is
             *     {@link Message.MessageDetail.IBD.IBDHeader }
             *
             */
            public void setIBDHeader(Message.MessageDetail.IBD.IBDHeader value) {
                this.ibdHeader = value;
            }

            /**
             * <p>
             * Java class for anonymous complex type.
             *
             * <p>
             * The following schema fragment specifies the expected content
             * contained within this class.
             *
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="STORERKEY" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="DOCUMENTTIME" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *         &lt;element name="IBDDetail" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="LINENO" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *                   &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="SKUDESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="QTYONHAND" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *                   &lt;element name="QTYALLOCATED" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *                   &lt;element name="QTYPICKED" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *                   &lt;element name="OPENQTY" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "storerkey",
                "documenttime",
                "ibdDetail"
            })
            public static class IBDHeader {

                @XmlElement(name = "STORERKEY")
                protected int storerkey;
                @XmlElement(name = "DOCUMENTTIME")
                protected long documenttime;
                @XmlElement(name = "IBDDetail")
                protected List<Message.MessageDetail.IBD.IBDHeader.IBDDetail> ibdDetail;

                /**
                 * Gets the value of the storerkey property.
                 *
                 */
                public int getSTORERKEY() {
                    return storerkey;
                }

                /**
                 * Sets the value of the storerkey property.
                 *
                 */
                public void setSTORERKEY(int value) {
                    this.storerkey = value;
                }

                /**
                 * Gets the value of the documenttime property.
                 *
                 */
                public long getDOCUMENTTIME() {
                    return documenttime;
                }

                /**
                 * Sets the value of the documenttime property.
                 *
                 */
                public void setDOCUMENTTIME(long value) {
                    this.documenttime = value;
                }

                /**
                 * Gets the value of the ibdDetail property.
                 *
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object. This is
                 * why there is not a <CODE>set</CODE> method for the ibdDetail
                 * property.
                 *
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getIBDDetail().add(newItem);
                 * </pre>
                 *
                 *
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Message.MessageDetail.IBD.IBDHeader.IBDDetail }
                 *
                 *
                 */
                public List<Message.MessageDetail.IBD.IBDHeader.IBDDetail> getIBDDetail() {
                    if (ibdDetail == null) {
                        ibdDetail = new ArrayList<Message.MessageDetail.IBD.IBDHeader.IBDDetail>();
                    }
                    return this.ibdDetail;
                }

                /**
                 * <p>
                 * Java class for anonymous complex type.
                 *
                 * <p>
                 * The following schema fragment specifies the expected content
                 * contained within this class.
                 *
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="LINENO" type="{http://www.w3.org/2001/XMLSchema}short"/>
                 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="SKUDESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="QTYONHAND" type="{http://www.w3.org/2001/XMLSchema}short"/>
                 *         &lt;element name="QTYALLOCATED" type="{http://www.w3.org/2001/XMLSchema}short"/>
                 *         &lt;element name="QTYPICKED" type="{http://www.w3.org/2001/XMLSchema}short"/>
                 *         &lt;element name="OPENQTY" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "lineno",
                    "sku",
                    "skudescr",
                    "qtyonhand",
                    "qtyallocated",
                    "qtypicked",
                    "openqty"
                })
                public static class IBDDetail {

                    @XmlElement(name = "LINENO")
                    protected short lineno;
                    @XmlElement(name = "SKU", required = true)
                    protected String sku;
                    @XmlElement(name = "SKUDESCR", required = true)
                    protected String skudescr;
                    @XmlElement(name = "QTYONHAND")
                    protected Long qtyonhand;
                    @XmlElement(name = "QTYALLOCATED")
                    protected Long qtyallocated;
                    @XmlElement(name = "QTYPICKED")
                    protected int qtypicked;
                    @XmlElement(name = "OPENQTY")
                    protected byte openqty;

                    /**
                     * Gets the value of the lineno property.
                     *
                     */
                    public short getLINENO() {
                        return lineno;
                    }

                    /**
                     * Sets the value of the lineno property.
                     *
                     */
                    public void setLINENO(short value) {
                        this.lineno = value;
                    }

                    /**
                     * Gets the value of the sku property.
                     *
                     * @return possible object is {@link String }
                     *
                     */
                    public String getSKU() {
                        return sku;
                    }

                    /**
                     * Sets the value of the sku property.
                     *
                     * @param value allowed object is {@link String }
                     *
                     */
                    public void setSKU(String value) {
                        this.sku = value;
                    }

                    /**
                     * Gets the value of the skudescr property.
                     *
                     * @return possible object is {@link String }
                     *
                     */
                    public String getSKUDESCR() {
                        return skudescr;
                    }

                    /**
                     * Sets the value of the skudescr property.
                     *
                     * @param value allowed object is {@link String }
                     *
                     */
                    public void setSKUDESCR(String value) {
                        this.skudescr = value;
                    }

                    /**
                     * Gets the value of the qtyonhand property.
                     *
                     */
                    public Long getQTYONHAND() {
                        return qtyonhand;
                    }

                    /**
                     * Sets the value of the qtyonhand property.
                     *
                     */
                    public void setQTYONHAND(Long value) {
                        this.qtyonhand = value;
                    }

                    /**
                     * Gets the value of the qtyallocated property.
                     *
                     */
                    public Long getQTYALLOCATED() {
                        return qtyallocated;
                    }

                    /**
                     * Sets the value of the qtyallocated property.
                     *
                     */
                    public void setQTYALLOCATED(Long value) {
                        this.qtyallocated = value;
                    }

                    /**
                     * Gets the value of the qtypicked property.
                     *
                     */
                    public int getQTYPICKED() {
                        return qtypicked;
                    }

                    /**
                     * Sets the value of the qtypicked property.
                     *
                     */
                    public void setQTYPICKED(int value) {
                        this.qtypicked = value;
                    }

                    /**
                     * Gets the value of the openqty property.
                     *
                     */
                    public byte getOPENQTY() {
                        return openqty;
                    }

                    /**
                     * Sets the value of the openqty property.
                     *
                     */
                    public void setOPENQTY(byte value) {
                        this.openqty = value;
                    }

                }

            }

        }

    }

    /**
     * <p>
     * Java class for anonymous complex type.
     *
     * <p>
     * The following schema fragment specifies the expected content contained
     * within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MessageID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MessageDate" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="MessageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ProjectID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Sender">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CompanyID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SystemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Receiver">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CompanyID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SystemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "messageID",
        "messageDate",
        "messageType",
        "projectID",
        "sender",
        "receiver"
    })
    public static class MessageHeader {

        @XmlElement(name = "MessageID", required = true)
        protected String messageID;
        @XmlElement(name = "MessageDate")
        protected long messageDate;
        @XmlElement(name = "MessageType", required = true)
        protected String messageType;
        @XmlElement(name = "ProjectID", required = true)
        protected String projectID;
        @XmlElement(name = "Sender", required = true)
        protected Message.MessageHeader.Sender sender;
        @XmlElement(name = "Receiver", required = true)
        protected Message.MessageHeader.Receiver receiver;

        /**
         * Gets the value of the messageID property.
         *
         * @return possible object is {@link String }
         *
         */
        public String getMessageID() {
            return messageID;
        }

        /**
         * Sets the value of the messageID property.
         *
         * @param value allowed object is {@link String }
         *
         */
        public void setMessageID(String value) {
            this.messageID = value;
        }

        /**
         * Gets the value of the messageDate property.
         *
         */
        public long getMessageDate() {
            return messageDate;
        }

        /**
         * Sets the value of the messageDate property.
         *
         */
        public void setMessageDate(long value) {
            this.messageDate = value;
        }

        /**
         * Gets the value of the messageType property.
         *
         * @return possible object is {@link String }
         *
         */
        public String getMessageType() {
            return messageType;
        }

        /**
         * Sets the value of the messageType property.
         *
         * @param value allowed object is {@link String }
         *
         */
        public void setMessageType(String value) {
            this.messageType = value;
        }

        /**
         * Gets the value of the projectID property.
         *
         * @return possible object is {@link String }
         *
         */
        public String getProjectID() {
            return projectID;
        }

        /**
         * Sets the value of the projectID property.
         *
         * @param value allowed object is {@link String }
         *
         */
        public void setProjectID(String value) {
            this.projectID = value;
        }

        /**
         * Gets the value of the sender property.
         *
         * @return possible object is {@link Message.MessageHeader.Sender }
         *
         */
        public Message.MessageHeader.Sender getSender() {
            return sender;
        }

        /**
         * Sets the value of the sender property.
         *
         * @param value allowed object is {@link Message.MessageHeader.Sender }
         *
         */
        public void setSender(Message.MessageHeader.Sender value) {
            this.sender = value;
        }

        /**
         * Gets the value of the receiver property.
         *
         * @return possible object is {@link Message.MessageHeader.Receiver }
         *
         */
        public Message.MessageHeader.Receiver getReceiver() {
            return receiver;
        }

        /**
         * Sets the value of the receiver property.
         *
         * @param value allowed object is
         *     {@link Message.MessageHeader.Receiver }
         *
         */
        public void setReceiver(Message.MessageHeader.Receiver value) {
            this.receiver = value;
        }

        /**
         * <p>
         * Java class for anonymous complex type.
         *
         * <p>
         * The following schema fragment specifies the expected content
         * contained within this class.
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CompanyID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SystemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "companyID",
            "siteID",
            "systemID"
        })
        public static class Receiver {

            @XmlElement(name = "CompanyID", required = true)
            protected String companyID;
            @XmlElement(name = "SiteID", required = true)
            protected String siteID;
            @XmlElement(name = "SystemID", required = true)
            protected String systemID;

            /**
             * Gets the value of the companyID property.
             *
             * @return possible object is {@link String }
             *
             */
            public String getCompanyID() {
                return companyID;
            }

            /**
             * Sets the value of the companyID property.
             *
             * @param value allowed object is {@link String }
             *
             */
            public void setCompanyID(String value) {
                this.companyID = value;
            }

            /**
             * Gets the value of the siteID property.
             *
             * @return possible object is {@link String }
             *
             */
            public String getSiteID() {
                return siteID;
            }

            /**
             * Sets the value of the siteID property.
             *
             * @param value allowed object is {@link String }
             *
             */
            public void setSiteID(String value) {
                this.siteID = value;
            }

            /**
             * Gets the value of the systemID property.
             *
             * @return possible object is {@link String }
             *
             */
            public String getSystemID() {
                return systemID;
            }

            /**
             * Sets the value of the systemID property.
             *
             * @param value allowed object is {@link String }
             *
             */
            public void setSystemID(String value) {
                this.systemID = value;
            }

        }

        /**
         * <p>
         * Java class for anonymous complex type.
         *
         * <p>
         * The following schema fragment specifies the expected content
         * contained within this class.
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CompanyID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SystemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "companyID",
            "siteID",
            "systemID"
        })
        public static class Sender {

            @XmlElement(name = "CompanyID", required = true)
            protected String companyID;
            @XmlElement(name = "SiteID", required = true)
            protected String siteID;
            @XmlElement(name = "SystemID", required = true)
            protected String systemID;

            /**
             * Gets the value of the companyID property.
             *
             * @return possible object is {@link String }
             *
             */
            public String getCompanyID() {
                return companyID;
            }

            /**
             * Sets the value of the companyID property.
             *
             * @param value allowed object is {@link String }
             *
             */
            public void setCompanyID(String value) {
                this.companyID = value;
            }

            /**
             * Gets the value of the siteID property.
             *
             * @return possible object is {@link String }
             *
             */
            public String getSiteID() {
                return siteID;
            }

            /**
             * Sets the value of the siteID property.
             *
             * @param value allowed object is {@link String }
             *
             */
            public void setSiteID(String value) {
                this.siteID = value;
            }

            /**
             * Gets the value of the systemID property.
             *
             * @return possible object is {@link String }
             *
             */
            public String getSystemID() {
                return systemID;
            }

            /**
             * Sets the value of the systemID property.
             *
             * @param value allowed object is {@link String }
             *
             */
            public void setSystemID(String value) {
                this.systemID = value;
            }

        }

    }

}
