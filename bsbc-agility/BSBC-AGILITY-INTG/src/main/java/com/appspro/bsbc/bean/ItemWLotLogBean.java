/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.bean;

/**
 *
 * @author DELL
 */
public class ItemWLotLogBean {

    private String url;
    private String lotNumber;
    private String itemNumber;
    private String agilityLotQty;
    private String agilityLotReportQty;
    private int lineNumber;
    private boolean status;
    private String bodyStatus;
    private String codeStatus;
    private String payload;
    private String fileName;
    private String agilityItemQty;
    private String agilityItemReportQty;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getAgilityLotQty() {
        return agilityLotQty;
    }

    public void setAgilityLotQty(String agilityLotQty) {
        this.agilityLotQty = agilityLotQty;
    }

    public String getAgilityLotReportQty() {
        return agilityLotReportQty;
    }

    public void setAgilityLotReportQty(String agilityLotReportQty) {
        this.agilityLotReportQty = agilityLotReportQty;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getBodyStatus() {
        return bodyStatus;
    }

    public void setBodyStatus(String bodyStatus) {
        this.bodyStatus = bodyStatus;
    }

    public String getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(String codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getAgilityItemQty() {
        return agilityItemQty;
    }

    public void setAgilityItemQty(String agilityItemQty) {
        this.agilityItemQty = agilityItemQty;
    }

    public String getAgilityItemReportQty() {
        return agilityItemReportQty;
    }

    public void setAgilityItemReportQty(String agilityItemReportQty) {
        this.agilityItemReportQty = agilityItemReportQty;
    }
    
    
    
}
