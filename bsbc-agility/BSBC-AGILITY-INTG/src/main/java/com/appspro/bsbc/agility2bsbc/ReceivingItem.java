/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.agility2bsbc;

import com.appspro.bsbc.SFTPUtility;
import com.appspro.bsbc.generation.receivingitem.Message;
import com.appspro.bsb.restHelper.RestHelper;
import com.appspro.bsbc.bean.ReceiptConformationBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author CPBSLV
 */
public class ReceivingItem {

    public static void main(String argp[]) throws JsonProcessingException {
        //System.out.println(new ReceivingItem().getItemData());
//        System.out.println(getItemUOM("1152-10000-0"));
        ReceivingItem.getItemData();
    }

    public static ReceiptConformationBean getItemData() {
        FileInputStream fstream = null;
        OutputStream outputStream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        String renamedFile = null;
        boolean check = true;
        Message itemObj = new Message();
        String response = null;
        ReceivingItem receivingItem = new ReceivingItem();
        ReceiptConformationBean receiptConformationBean = new ReceiptConformationBean();
        ReceiptConformationBean receiptConformationBean2 = new ReceiptConformationBean();
        try {

            try {
                String host = System.getProperty("http.proxyHost");
                String post = System.getProperty("http.proxyPort");
                session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("@BSbc123456");
                if (host != null || post != null) {
                    ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                    session.setProxy(proxy);
                }
                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect();
                sftpChannel = (ChannelSftp) channel;
                sftpChannel.cd("/upload");
                Vector filelist = sftpChannel.ls("/upload");
                List<String> fileNameList = new ArrayList<String>();
                String fileName = null;
                for (int i = 0; i < filelist.size(); i++) {
                    ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);
                    if (entry.getFilename().startsWith("RCCONFIRM")
                            || entry.getFilename().startsWith("RCCONFIRM_RK-")) {
                        fileName = entry.getFilename();
                        fileNameList.add(fileName);
                    }

                }
                if (fileNameList != null && !fileNameList.isEmpty()) {
                    for (int i = 0; i < fileNameList.size(); i++) {
                        fileSFTP = sftpChannel.get("/upload/" + fileNameList.get(i));
                        renamedFile = fileNameList.get(i);

                        // Open the file
                        String filename = "OTL";
                        File file = File.createTempFile(filename, ".zip");

                        // write the inputStream to a FileOutputStream
                        outputStream
                                = new FileOutputStream(file);

                        int read = 0;
                        byte[] bytes = new byte[2056];

                        while ((read = fileSFTP.read(bytes)) != -1) {
                            outputStream.write(bytes, 0, read);
                        }

                        JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);

                        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                        itemObj = (Message) jaxbUnmarshaller.unmarshal(file);
                        List<Message.MessageDetail.IDC.IDCHeader.IDCDetail> fileSize = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail();
                        for (int ind = 0; ind < fileSize.size(); ind++) {
                            String createReciptRequestDMG = buildJsonRequest(itemObj, "DMG", ind);

                             System.out.println(createReciptRequestDMG);
                            if (createReciptRequestDMG != null) {
                                receiptConformationBean2 = RestHelper.callPostReceiptConfRest("https://ehxm.fa.us6.oraclecloud.com:443/fscmRestApi/resources/11.13.17.11/receipts", "application/json", createReciptRequestDMG, "U0NNX0lNUEw6QlNCQ0AxMjM=");
                                 JSONObject jsonObj = new JSONObject(receiptConformationBean2.getErrorMessage());
                              String errorExplanation= jsonObj.get("ErrorExplanation").toString();
                                receiptConformationBean.setCodeStatus(receiptConformationBean2.getCodeStatus());
                                if (!receiptConformationBean2.getCodeStatus().equals("200") && !receiptConformationBean2.getCodeStatus().equals("201")) {
                                    receiptConformationBean.setStatus(false);
                                    receiptConformationBean.setBodyStatus(receiptConformationBean2.getBodyStatus());
                                    // System.out.println(receiptConformationBean.getBodyStatus());
                                } else {
                                    receiptConformationBean.setStatus(true);
                                    receiptConformationBean.setItemNumber(receiptConformationBean.getItemNumber());

                                }
                                if (receiptConformationBean.isStatus() == false || errorExplanation != null) {
                                    receiptConformationBean.setFileName(renamedFile);
                                    String xmlItemNumber = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getSKU();
                                    String externReceiptKey = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getEXTERNRECEIPTKEY();
                                    byte receiptNumber = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getRECEIPTLINENUMBER();
                                    String receipt = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getRECEIPTKEY();
                                    String receiptLineNumber = Byte.toString(receiptNumber);
                                    String supplierNo = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getVENDORCODE();
                                    String supplierName = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getVENDORDESCRIPTION();
                                    String supplierInvNo = Integer.toString(itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getSUPINVNO());
                                    String payload = createReciptRequestDMG;
                                    receiptConformationBean.setItemNumber(xmlItemNumber);
                                    receiptConformationBean.setPovNo(externReceiptKey);
                                    receiptConformationBean.setLineNo(receiptLineNumber);
                                    receiptConformationBean.setReceipt(receipt);
                                    receiptConformationBean.setSupplierNo(supplierNo);
                                    receiptConformationBean.setSupplierName(supplierName);
                                    receiptConformationBean.setSupplierInvNo(supplierInvNo);
//                                    JSONObject json = new JSONObject(createReciptRequestDMG);
                                    receiptConformationBean.setBodyStatus(receiptConformationBean2.getBodyStatus());
                                    receiptConformationBean.setPayload(payload);
//                                    receiptConformationBean.setItemNumber(json.);
                                    insertErrorLog(receiptConformationBean);
                                }
                            }

                            String createReciptRequestRCV = buildJsonRequest(itemObj, "RCV", ind);
                              System.out.println(createReciptRequestRCV);
                            if (createReciptRequestRCV != null) {
                                receiptConformationBean2 = RestHelper.callPostReceiptConfRest("https://ehxm.fa.us6.oraclecloud.com:443/fscmRestApi/resources/11.13.17.11/receipts", "application/json", createReciptRequestRCV, "U0NNX0lNUEw6QlNCQ0AxMjM=");
                               JSONObject jsonObj = new JSONObject(receiptConformationBean2.getErrorMessage());
                              String errorExplanation= jsonObj.get("ErrorExplanation").toString();
                               System.out.println(errorExplanation);
                                receiptConformationBean.setCodeStatus(receiptConformationBean2.getCodeStatus());
                                if (!receiptConformationBean2.getCodeStatus().equals("200") && !receiptConformationBean2.getCodeStatus().equals("201")) {
                                    receiptConformationBean.setStatus(false);
                                    receiptConformationBean.setBodyStatus(receiptConformationBean2.getBodyStatus());
                                    //    System.out.println(receiptConformationBean.getBodyStatus());
                                } else {
                                    receiptConformationBean.setStatus(true);
                                }
                                if (receiptConformationBean.isStatus() == false || errorExplanation != null) {
                                    receiptConformationBean.setFileName(renamedFile);
                                    String xmlItemNumber = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getSKU();
                                    String externReceiptKey = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getEXTERNRECEIPTKEY();
                                    byte receiptNumber = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getRECEIPTLINENUMBER();
                                    String receipt = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getRECEIPTKEY();
                                    String receiptLineNumber = Byte.toString(receiptNumber);
                                    String supplierNo = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getVENDORCODE();
                                    String supplierName = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getVENDORDESCRIPTION();
                                    String supplierInvNo = Integer.toString(itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getSUPINVNO());
                                    String payload = createReciptRequestRCV;
                                    receiptConformationBean.setItemNumber(xmlItemNumber);
                                    receiptConformationBean.setPovNo(externReceiptKey);
                                    receiptConformationBean.setLineNo(receiptLineNumber);
                                    receiptConformationBean.setReceipt(receipt);
                                    receiptConformationBean.setSupplierNo(supplierNo);
                                    receiptConformationBean.setSupplierName(supplierName);
                                    receiptConformationBean.setSupplierInvNo(supplierInvNo);
                                    receiptConformationBean.setBodyStatus(receiptConformationBean2.getBodyStatus());
                                    receiptConformationBean.setPayload(payload);
                                    receiptConformationBean.setErrorMessage(errorExplanation);
                                    insertErrorLog(receiptConformationBean);
                                }
                            }
//                        sftpChannel.rename("/upload/" + renamedFile,
//                                "/upload/LoadedAgility/" + renamedFile);
//                        sftpChannel.cd("/upload/");
                        }
                        return receiptConformationBean;
                    }

//                sftpChannel.rm(renamedFile);
                } else {
                    check = false;
                }

            } catch (JAXBException ex) {
                Logger.getLogger(ReceivingItem.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReceivingItem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReceivingItem.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException ex) {
//            Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        } finally {
            try {
                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                if (session != null) {
                    session.disconnect();
                }
                if (fstream != null) {
                    fstream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReceivingItem.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }
        }
        return receiptConformationBean;
    }

//    public String getItemData() {
//        Message itemObj = new Message();
//        String response = null;
//        try {
//
//            File file = SFTPUtility.readSFTPFile("RCCONFIRM_RK-000085965620190504110030.XML");
//            if (file == null) {
//                return null;
//            }
//            JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);
//
//            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//            itemObj = (Message) jaxbUnmarshaller.unmarshal(file);
//
//            String createReciptRequestDMG = buildJsonRequest(itemObj, "DMG");
//            System.out.println(createReciptRequestDMG);
//            if (createReciptRequestDMG != null) {
//                response = RestHelper.callPostRest("https://ehxm-test.fa.us6.oraclecloud.com:443/fscmRestApi/resources/11.13.17.11/receipts", "application/json", createReciptRequestDMG, "U0NNX0lNUEw6QlNCQ0AxMjM=");
//            }
//
//            String createReciptRequestRCV = buildJsonRequest(itemObj, "RCV");
//            System.out.println(createReciptRequestRCV);
//            if (createReciptRequestRCV != null) {
//                response = RestHelper.callPostRest("https://ehxm-test.fa.us6.oraclecloud.com:443/fscmRestApi/resources/11.13.17.11/receipts", "application/json", createReciptRequestRCV, "U0NNX0lNUEw6QlNCQ0AxMjM=");
//            }
//
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//        return response;
//    }
    private static String buildJsonRequest(Message itemObj, String type, int ind) {
        ReceiptConformationBean receiptConformationBean = new ReceiptConformationBean();
        String xmlItemNumber = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getSKU();
        //String xmlItemNumber = "1152-10000-0";
        if (xmlItemNumber.length() == 10) {
            xmlItemNumber = xmlItemNumber.substring(0, 4) + "-" + xmlItemNumber.substring(4, 9)
                    + "-" + xmlItemNumber.substring(9, xmlItemNumber.length());
            //     System.out.println(xmlItemNumber);
        } else if (xmlItemNumber.length() == 11) {
            xmlItemNumber = xmlItemNumber.substring(0, 4) + "-" + xmlItemNumber.substring(4, 7)
                    + "-" + xmlItemNumber.substring(7, xmlItemNumber.length());
               System.out.println(xmlItemNumber);
        }
        String url = "https://ehxm.fa.us6.oraclecloud.com//fscmRestApi/resources/latest/items?q=ItemNumber=" + "\'" + xmlItemNumber + "\'" + "&onlyData=true&fields=ItemNumber,DimensionUOMCode,DimensionUOMValue,PrimaryUOMCode,PrimaryUOMValue";
        JSONObject jsonObj = getItemUOM(url);
        String externReceiptKey = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getEXTERNRECEIPTKEY();
        byte receiptNumber = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getRECEIPTLINENUMBER();
        String receipt = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getRECEIPTKEY();
        String receiptLineNumber = Byte.toString(receiptNumber);
        String supplierNo = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getVENDORCODE();
        String supplierName = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getVENDORDESCRIPTION();
        String supplierInvNo = Integer.toString(itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getSUPINVNO());
        receiptConformationBean.setItemNumber(xmlItemNumber);
        receiptConformationBean.setPovNo(externReceiptKey);
        receiptConformationBean.setLineNo(receiptLineNumber);
        receiptConformationBean.setReceipt(receipt);
        receiptConformationBean.setSupplierNo(supplierNo);
        receiptConformationBean.setSupplierName(supplierName);
        receiptConformationBean.setSupplierInvNo(supplierInvNo);
        if (jsonObj != null) {
            String itemQuantity = null;
            if (type.equals("RCV") && itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getQTYRECEIVED() == 0) {
                return null;
            }

            if (type.equals("DMG") && itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getDAMAGEDQTY() == 0) {
                return null;
            }
            if (itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getQTYRECEIVED() != 0 && type.equals("RCV")) {
                itemQuantity = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getQTYRECEIVED() + "";
            } else if (itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getDAMAGEDQTY() != 0 && type.equals("DMG")) {
                itemQuantity = itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getDAMAGEDQTY() + "";
            }

            JSONObject header = new JSONObject();
            JSONArray receiptHeaders = new JSONArray();
            JSONObject headerDetails = new JSONObject();
            headerDetails.put("ReceiptNumber", itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getRECEIPTKEY());
            headerDetails.put("OrganizationCode", "PWC");
//                headerDetails.put("VendorName", itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getVENDORDESCRIPTION());
            headerDetails.put("VendorNumber", itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getVENDORCODE());
            headerDetails.put("EmployeeId", "300000003978145");
            headerDetails.put("BusinessUnit", "BSBC");
            headerDetails.put("ReceiptSourceCode", "VENDOR");

            JSONArray receiptLines = new JSONArray();
            JSONObject receiptLinesDetails = new JSONObject();

            JSONArray lotItemLots = new JSONArray();
            JSONObject lotItemLotsDetails = new JSONObject();

            if (itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getBATCHNUMBER() != null && !itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getBATCHNUMBER().isEmpty()) {
                lotItemLotsDetails.put("LotNumber", itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getBATCHNUMBER());
                lotItemLotsDetails.put("TransactionQuantity", itemQuantity);
                if (itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getEXPIRYDATE() != null && !itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getEXPIRYDATE().isEmpty()) {
                    DateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
                    DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        lotItemLotsDetails.put("LotExpirationDate", format2.format(format.parse(itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getEXPIRYDATE())));

                    } catch (ParseException ex) {
                        Logger.getLogger(ReceivingItem.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                lotItemLots.put(lotItemLotsDetails);
            }

            receiptLinesDetails.put("BusinessUnit", "BSBC");
            if (type.equals("RCV")) {
                receiptLinesDetails.put("Subinventory", "PWC");
            } else {
                receiptLinesDetails.put("Subinventory", "SR");
            }
            receiptLinesDetails.put("DestinationTypeCode", "INVENTORY");
            receiptLinesDetails.put("DocumentLineNumber", itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getRECEIPTLINENUMBER());
            receiptLinesDetails.put("DocumentNumber", "POV" + itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getEXTERNRECEIPTKEY());
            receiptLinesDetails.put("ItemNumber", xmlItemNumber);
            receiptLinesDetails.put("PrimaryQuantity", itemObj.getMessageDetail().getIDC().getIDCHeader().getIDCDetail().get(ind).getTOTALEXPECTEDQTY());
            receiptLinesDetails.put("EmployeeId", "300000003978145");
            receiptLinesDetails.put("OrganizationCode", "PWC");
            receiptLinesDetails.put("Quantity", itemQuantity);

            receiptLinesDetails.put("UOMCode", jsonObj.optString("DimensionUOMCode").isEmpty() ? jsonObj.optString("PrimaryUOMCode") : jsonObj.optString("DimensionUOMCode"));
            receiptLinesDetails.put("UnitOfMeasure", jsonObj.get("DimensionUOMValue"));
            receiptLinesDetails.put("ReceiptSourceCode", "VENDOR");
            receiptLinesDetails.put("SoldtoLegalEntity", "Bader Sultan & Brothers Co. W.L.L.");
            receiptLinesDetails.put("SourceDocumentCode", "PO");
            receiptLinesDetails.put("AutoTransactCode", "DELIVER");
            receiptLinesDetails.put("TransactionType", "RECEIVE");
            receiptLinesDetails.put("lotItemLots", lotItemLots);
//        receiptLinesDetails.put("ASNLineNumber", 2);

            receiptLines.put(receiptLinesDetails);
            headerDetails.put("receiptLines", receiptLines);
            receiptHeaders.put(headerDetails);
            header.put("receiptHeaders", receiptHeaders);
              System.out.println(header);
            return header.toString();
        }
        return null;
    }

    public static boolean insertErrorLog(ReceiptConformationBean receiptConformationBean) {
        try {
            String url = "https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/AppsproAgility/RcConfirmRkErrroLog/";
            String jsonInString = "{\n"
                    + "	\"RC_POV_NO\":\"" + receiptConformationBean.getPovNo() + '"' + ",\n"
                    + "	\"RC_LINE_NO\":\"" + receiptConformationBean.getLineNo() + '"' + ",\n"
                    + "	\"RC_ITEM_NUMBER\":\"" + receiptConformationBean.getItemNumber() + '"' + ",\n"
                    + "	\"RC_RECEIPT\":\"" + receiptConformationBean.getReceipt() + '"' + ",\n"
                    + "	\"RC_SUPPLIER_NO\":" + receiptConformationBean.getSupplierNo() + ",\n"
                    + "	\"RC_SUPPLIER_NAME\":" + '"' + receiptConformationBean.getSupplierName() + '"' + ",\n"
                    + "	\"RC_SUPPLIER_INV_NO\":" + '"' + receiptConformationBean.getSupplierInvNo() + '"' + ",\n"
                    + "	\"RC_FILE_NAME\":" + '"' + receiptConformationBean.getFileName() + '"' + ",\n"
                    + "	\"RC_ERROR_MESSAGE\":" + '"' + receiptConformationBean.getErrorMessage() + '"' + ",\n"
                    + "	\"RC_PAYLOAD\":" + '"' + (receiptConformationBean.getPayload() == null ?  receiptConformationBean.getPayload() : receiptConformationBean.getPayload().replaceAll("\"", "'"))+ '"' + "\n"
                    + "}";
            //System.out.println(jsonInString);
            RestHelper.callPostRest(url, "application/json", jsonInString, null);
            return true;

        } catch (Exception ex) {
            Logger.getLogger(ItemLotQuantity.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private static JSONObject getItemUOM(String itemID) {
        RestHelper service = new RestHelper();
        JSONObject jsonObj = new JSONObject(service.callGetRest(itemID));

        int responseCount = jsonObj.getInt("count");
        System.out.println("Count=" + jsonObj.getInt("count"));

        if (responseCount > 0) {
            JSONArray items = jsonObj.getJSONArray("items");
            JSONObject itemsObj = items.getJSONObject(0);

            return itemsObj;
        }

        return null;

    }
}
