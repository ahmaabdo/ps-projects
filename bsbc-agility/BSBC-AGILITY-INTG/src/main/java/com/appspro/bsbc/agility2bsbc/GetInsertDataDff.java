/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.agility2bsbc;

import com.appspro.bsb.restHelper.RestHelper;
import com.appspro.bsbc.bean.ItemWLotLogBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author DELL
 */
public class GetInsertDataDff {

    public ItemWLotLogBean GetAndPostLotData(String lotNumber, String itemNumber, String agilityLotQty, String agilityLotReportDate) {
        ItemWLotLogBean itemWLotLogBean = new ItemWLotLogBean();
        ItemWLotLogBean itemWLotLogBean2 = new ItemWLotLogBean();
        ItemLotQuantity itemLotQuantity = new ItemLotQuantity();
        try {
            JSONObject jsonObj = new JSONObject(RestHelper.callGetRest("https://ehxm.fa.us6.oraclecloud.com:443//fscmRestApi/resources/11.13.18.05/inventoryItemLots?q=LotNumber=" + lotNumber + ";ItemNumber=" + itemNumber));
            int count = Integer.parseInt(jsonObj.get("count").toString());
        //    System.out.println(count);
            JSONArray items = jsonObj.getJSONArray("items");
            if (count >= 1) {
                for (int i = 0; i < items.length(); ++i) {
                    JSONObject rec = items.getJSONObject(i);
                    String organizationCode = rec.getString("OrganizationCode");
                    String lotNum = rec.getString("LotNumber");
                    String itemNumberLot = rec.getString("ItemNumber");
                    if (organizationCode.equalsIgnoreCase("PWC") && lotNum.equals(lotNumber) && itemNumberLot.equals(itemNumber)) {
                        JSONArray links = rec.getJSONArray("links");
                        for (int j = 0; j < links.length(); j++) {
                            JSONObject linkObj = links.getJSONObject(j);
                            String rel = linkObj.getString("rel");
                            String name = linkObj.getString("name");
                            if (rel.equalsIgnoreCase("child") && name.equalsIgnoreCase("lotStandardDFF")) {
                                String href = linkObj.getString("href");
                                itemWLotLogBean.setUrl(href);
                                String payload = "{\n" + "\"agilityLotQty\":" + '"' + agilityLotQty + '"' + ",\n" + "\"agilityLotReportDate\":" + '"' + agilityLotReportDate + '"' + "\n" + "}";
//                            if (RestHelper.callPostWLWNRest(href, "application/json", payload, "U0NNX0lNUEw6QlNCQ0AxMjM=").getCodeStatus().equals("400")) {
//                                itemWLotLogBean.setStatus(false);
//                                return itemWLotLogBean;
//                            }
                                itemWLotLogBean2 = RestHelper.callPostWLWNRest(href, "application/json", payload, "U0NNX0lNUEw6QlNCQ0AxMjM=");
                                itemWLotLogBean.setCodeStatus(itemWLotLogBean2.getCodeStatus());
                                //  itemWLotLogBean.setPayload(payload);
//                            itemWLotLogBean.setBodyStatus(itemWLotLogBean2.getBodyStatus());
                                if (!itemWLotLogBean2.getCodeStatus().equals("200") && !itemWLotLogBean2.getCodeStatus().equals("201")) {
                                    itemWLotLogBean.setStatus(false);
                                    itemWLotLogBean.setPayload(payload);
                                    itemWLotLogBean.setBodyStatus(itemWLotLogBean2.getBodyStatus());
                                  //  System.out.println(itemWLotLogBean.getBodyStatus());
                                    return itemWLotLogBean;

                                }
                            }
                        }
                    }
                }
            } else {
                itemWLotLogBean.setUrl(itemWLotLogBean.getUrl());
                itemWLotLogBean.setStatus(false);
                itemWLotLogBean.setBodyStatus("Item Does not Exist Or Lot Number not Defined");
                return itemWLotLogBean;
            }
            itemWLotLogBean.setStatus(true);
            return itemWLotLogBean;
        } catch (JSONException ex) {
            Logger.getLogger(GetInsertDataDff.class.getName()).log(Level.SEVERE, null, ex);
            itemWLotLogBean.setStatus(false);
            return itemWLotLogBean;
        } catch (Exception ex) {
            Logger.getLogger(GetInsertDataDff.class.getName()).log(Level.SEVERE, null, ex);
//            itemWLotLogBean.setStatus(false);
//            itemWLotLogBean.setBodyStatus(itemWLotLogBean2.getBodyStatus());

            return itemWLotLogBean;
        }
    }

    public ItemWLotLogBean GetAndPostItemData(String itemNumber, String agilityItemQtyReportDate, String agilityItemQty) {
        ItemWLotLogBean itemWLotLogBean = new ItemWLotLogBean();
        ItemWLotLogBean itemWLotLogBean2 = new ItemWLotLogBean();
        try {
            JSONObject jsonObj = new JSONObject(RestHelper.callGetRest("https://ehxm.fa.us6.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/itemsV2?q=ItemNumber=" + itemNumber + ";OrganizationCode=PWC"));
            int count = Integer.parseInt(jsonObj.get("count").toString());
           // System.out.println(count);
            JSONArray items = jsonObj.getJSONArray("items");
            if (count >= 1) {
                for (int i = 0; i < items.length(); ++i) {
                    JSONObject rec = items.getJSONObject(i);
                    JSONArray links = rec.getJSONArray("links");
                    for (int j = 0; j < links.length(); j++) {
                        JSONObject linkObj = links.getJSONObject(j);
                        String rel = linkObj.getString("rel");
                        String name = linkObj.getString("name");
                        if (rel.equalsIgnoreCase("child") && name.equalsIgnoreCase("ItemDFF")) {
                            String href = linkObj.getString("href");
                            itemWLotLogBean.setUrl(href);
                            String payload = "{\n" + "\"agilityItemQtyReportDate\":" + '"' + agilityItemQtyReportDate + '"' + ",\n" + "    \"agilityItemQty\":" + '"' + agilityItemQty + '"' + "\n" + "}";
                            itemWLotLogBean2 = RestHelper.callPostWLWNRest(href, "application/json", payload, "U0NNX0lNUEw6QlNCQ0AxMjM=");
                            itemWLotLogBean.setCodeStatus(itemWLotLogBean2.getCodeStatus());
                            if (!itemWLotLogBean2.getCodeStatus().equals("200") && !itemWLotLogBean2.getCodeStatus().equals("201")) {
                                itemWLotLogBean.setStatus(false);
                                itemWLotLogBean.setPayload(payload);
                                itemWLotLogBean.setBodyStatus(itemWLotLogBean2.getBodyStatus());
                          //      System.out.println(itemWLotLogBean.getBodyStatus());
                                return itemWLotLogBean;

                            }
                        }
                    }
                }
            } else {
                itemWLotLogBean.setUrl(itemWLotLogBean.getUrl());
                itemWLotLogBean.setStatus(false);
                itemWLotLogBean.setBodyStatus("Item Does not Exist");
                return itemWLotLogBean;
            }
            itemWLotLogBean.setStatus(true);
            return itemWLotLogBean;
        } catch (JSONException ex) {
            Logger.getLogger(GetInsertDataDff.class.getName()).log(Level.SEVERE, null, ex);
            itemWLotLogBean.setStatus(false);
            return itemWLotLogBean;
        } catch (Exception ex) {
            Logger.getLogger(GetInsertDataDff.class.getName()).log(Level.SEVERE, null, ex);
            return itemWLotLogBean;
        }
    }

    public static void main(String[] args) {
        GetInsertDataDff service = new GetInsertDataDff();
        //service.GetItemData();
//        service.GetAndPostLotData(300000053500708L, "111111", "2011-11-11");
//        service.GetAndPostItemData(300000053500708L, "2012-12-22", "1111");
    }

}
