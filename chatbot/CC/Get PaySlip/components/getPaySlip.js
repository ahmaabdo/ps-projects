var request = require('request');
var dateFormat = require('dateformat');
require('hijri-date');

module.exports = {
  metadata: () => ({
    name: 'paySlipAsPdfNew4',
    properties: {
      PersonId: { required: true, type: 'string' },
      fromDate: { required: true, type: 'string' },
      toDate: { required: true, type: 'string' }
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();
    var { fromDate } = conversation.properties();
    var { toDate } = conversation.properties();



    // Format The Date in Multiple Formats 
    switch (fromDate.charAt(1)) {

      case '-': var reversedDate = fromDate.split('-').reverse().join('-'); break;
      case '/': var reversedDate = fromDate.split('/').reverse().join('/'); break;
      default: switch (fromDate.charAt(2)) {
        case '-': var reversedDate = fromDate.split('-').reverse().join('-'); break;
        case '/': var reversedDate = fromDate.split('/').reverse().join('/'); break;

      }

    }

    if(parseInt(reversedDate.substr(0,4)) < 2000 ){ 
    var formattedFromDate = dateFormat(reversedDate+"/1", "dd-mm-yyyy");
    var splitFormattedStartDate = formattedFromDate.split('-');
    
      var HijjDate = new HijriDate(parseInt(splitFormattedStartDate[2]), parseInt(splitFormattedStartDate[1]), parseInt(splitFormattedStartDate[0]));
      var gregStartDate = dateFormat(HijjDate.toGregorian(), "dd-mmm-yyyy");



  }
    else {

      var gregStartDate = dateFormat(reversedDate+"/1", "dd-mmm-yyyy");

    }

    // End of Fromat for the start Date
    // Format The endDate in Multiple Formats 
    switch (toDate.charAt(1)) {

      case '-': var reversedDate = toDate.split('-').reverse().join('-'); break;
      case '/': var reversedDate = toDate.split('/').reverse().join('/'); break;
      default: switch (toDate.charAt(2)) {
        case '-': var reversedDate = toDate.split('-').reverse().join('-'); break;
        case '/': var reversedDate = toDate.split('/').reverse().join('/');; break;

      }

    }
    if(parseInt(reversedDate.substr(0,4)) < 2000 ){ 
      var formattedEndDate = dateFormat(reversedDate+"/1", "dd-mm-yyyy");
      var splitFormattedEndDate = formattedEndDate.split('-');
      
        var HijjDate = new HijriDate(parseInt(splitFormattedEndDate[2]), parseInt(splitFormattedEndDate[1]), parseInt(splitFormattedEndDate[0]));
        var gregEndDate = dateFormat(HijjDate.toGregorian(), "dd-mmm-yyyy");
  
  
  
      }
      else {
  
        var gregEndDate = dateFormat(reversedDate+"/1", "dd-mmm-yyyy");
  
      }
    // End of Fromat for the End Date


    // var username = 'PAASUser';
    // var password = 'Paas@321';
    // var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    // var reqUrl = `http://hrbot.appspro-me.com:7007/hrbot/wepapi/main/payslip/${PersonId}?fromDate=${fromDate}&toDate=${toDate}`;
    var reqUrl = `https://pdfpayslip.herokuapp.com/products/${PersonId}&${gregStartDate}&${gregEndDate}`;


    var options = {
      url: reqUrl,
      headers: {
        // "Authorization": auth
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);
      conversation.variable("paySlipLink",jsonBody.link).transition() ;       
      done();
    });

  }
};


