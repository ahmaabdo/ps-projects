var request = require('request');
var dateFormat = require('dateformat');

module.exports = {
  metadata: () => ({
    name: 'paySlipListNew4',
    properties: {
      PersonId: { required: true, type: 'string' },
      fromDate: { required: true, type: 'string' },
      toDate: { required: true, type: 'string' }
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();
    var { fromDate } = conversation.properties();
    var { toDate } = conversation.properties();


    var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    // Format The Date in Multiple Formats 
    switch (fromDate.charAt(1)) {

      case '-': var reversedDate = fromDate.split('-').reverse().join('-'); break;
      case '/': var reversedDate = fromDate.split('/').reverse().join('/'); break;
      default: switch (fromDate.charAt(2)) {
        case '-': var reversedDate = fromDate.split('-').reverse().join('-'); break;
        case '/': var reversedDate = fromDate.split('/').reverse().join('/'); break;

      }

    }

    if(parseInt(reversedDate.substr(0,4)) < 2000 ){ 
    var formattedFromDate = dateFormat(reversedDate+"/1", "dd-mm-yyyy");
    var splitFormattedStartDate = formattedFromDate.split('-');
    
      var HijjDate = new HijriDate(parseInt(splitFormattedStartDate[2]), parseInt(splitFormattedStartDate[1]), parseInt(splitFormattedStartDate[0]));
      var gregStartDate = dateFormat(HijjDate.toGregorian(), "dd-mmm-yyyy");



  }
    else {

      var gregStartDate = dateFormat(reversedDate+"/1", "dd-mmm-yyyy");

    }

    // End of Fromat for the start Date
    // Format The endDate in Multiple Formats 
    switch (toDate.charAt(1)) {

      case '-': var reversedDate = toDate.split('-').reverse().join('-'); break;
      case '/': var reversedDate = toDate.split('/').reverse().join('/'); break;
      default: switch (toDate.charAt(2)) {
        case '-': var reversedDate = toDate.split('-').reverse().join('-'); break;
        case '/': var reversedDate = toDate.split('/').reverse().join('/');; break;

      }

    }
    if(parseInt(reversedDate.substr(0,4)) < 2000 ){ 
      var formattedEndDate = dateFormat(reversedDate+"/1", "dd-mm-yyyy");
      var splitFormattedEndDate = formattedEndDate.split('-');
      
        var HijjDate = new HijriDate(parseInt(splitFormattedEndDate[2]), parseInt(splitFormattedEndDate[1]), parseInt(splitFormattedEndDate[0]));
        var gregEndDate = dateFormat(HijjDate.toGregorian(), "dd-mmm-yyyy");
  
  
  
      }
      else {
  
        var gregEndDate = dateFormat(reversedDate+"/1", "dd-mmm-yyyy");
  
      }
    // End of Fromat for the End Date

    var reqUrl = `http://hrbot.appspro-me.com:7007/hrbot/wepapi/main/payslip/${PersonId}?fromDate=${gregStartDate}&toDate=${gregEndDate}`;


    var options = {
      url: reqUrl,
      headers: {
        "Authorization": auth
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);
      var employee = jsonBody.DATA[0];
      var totalEarnings =  0 ; 
      var totalDeductions = 0 ; 

      var Payment_NAME = "Uknown";
      var result = jsonBody.RESULT + " ";
      
      let slip = "" ; 
      for(let i =0;i<jsonBody.DATA.length;i++){
      if (jsonBody) {
        Payment_NAME = jsonBody.DATA[i].ELEMENT_NAME + " ";
        var classification = jsonBody.DATA[i].CLASSIFICATION_NAME + " ";
        var Value = jsonBody.DATA[i].RESULT_VALUE + " ";
        if(classification == "Earnings "){

          totalEarnings += parseFloat(Value); 
        }
        else{
          totalDeductions += parseFloat(Value) ; 

        }
      }

      slip += i+1 + '\n'+'Name: ' + Payment_NAME + '\n' + 'Classification: ' + classification + '\n' + 
      'Value: ' + Value + '\n' + '------------------- \n ' ; 
      


    }
    conversation
    .variable('slip',slip)
    
    .transition();




var total = totalEarnings - totalDeductions ; 



conversation.variable('total','Total Earnings: '+totalEarnings + '\n' + 'Total Deductions: '+totalDeductions
+ '\n' + 'Total : ' + total)
            
            .transition();


      done();
    });

  }
};


