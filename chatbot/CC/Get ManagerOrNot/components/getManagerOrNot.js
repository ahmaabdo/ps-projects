var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'getManagerOrNot',
    properties: {
      PersonId: { required: true, type: 'string' },
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();


    /*var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");*/

    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    var reqUrl = `http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/getSUPERVISOREMPNO/${PersonId}`;
    //var reqUrl = `https://pdfnodegen.herokuapp.com/products/${PersonId}`;


    var options = {
      url: reqUrl,
      headers: {
       // "Authorization": no 
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);
    
      
      


 

        

      if(Object.keys(jsonBody).length > 1){
      conversation.variable('Manager','Manager').transition();
      }
      else{
        conversation.variable('Manager','Employee').transition();


      }
      
      done();
    });

  }
};


