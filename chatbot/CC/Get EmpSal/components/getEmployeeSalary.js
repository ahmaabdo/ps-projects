var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'getEmployeeSalary',
    properties: {
      PersonId: { required: true, type: 'string' },
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();


    /*var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");*/

    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    var reqUrl = `http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/getSalary/${PersonId}`;


    var options = {
      url: reqUrl,
      headers: {
       // "Authorization": no 
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);

      var employee = jsonBody[0];
      var emp_salary = "Uknown";
      var emp_annualSalary = "uknown";
      var emp_currncy=" " ; 
      if (employee && employee.SALARY){
        emp_salary = employee.SALARY + " ";
        emp_annualSalary=employee.ANNUALIZED_SALARY + " ";
        emp_currncy=employee.CURRENCY;
             }

      conversation
      .variable('sal', 'Your Salary is : '+ emp_salary +emp_currncy + '\n' + 'Your Annual Salary is : '+emp_annualSalary  + emp_currncy )
      
        
        .transition();

      done();
    });

  }
};


