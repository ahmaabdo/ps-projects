var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'getEmpsList',
    properties: {
      PersonId: { required: true, type: 'string' },
      otlPersonName: { required: true, type: 'string'},
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();
var {otlPersonName} = conversation.properties(); 

    /*var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");*/

    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    var reqUrl = `http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/getSUPERVISOREMPNO/${PersonId}`;
    //var reqUrl = `https://pdfnodegen.herokuapp.com/products/${PersonId}`;


    var options = {
      url: reqUrl,
      headers: {
       // "Authorization": no 
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);
    
     var PersonIDS  = " ";
      jsonBody.forEach(element => {
        if(element.PERSON_ID == PersonId){return;}
        if(element.FULL_NAME.includes(`${otlPersonName}`)){
PersonIDS += "رقم الموظف : "+element.PERSON_ID +"\n"+ "اسم الموظف : "+element.FULL_NAME+"\n ------------------- \n";
        }
        
      });
      if(PersonIDS != " "){
      conversation.reply(PersonIDS).transition();

      conversation.keepTurn(true);
      }
      else{

        conversation.reply("لا يوجد موظف بهذا الاسم").transition();
      }
        

      
      
      done();
    });

  }
};


