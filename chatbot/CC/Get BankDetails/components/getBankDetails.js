var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'getBankDetails',
    properties: {
      PersonId: { required: true, type: 'string' },
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();


    /*var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");*/

    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    var reqUrl = `http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/getBankDet/${PersonId}`;


    var options = {
      url: reqUrl,
      headers: {
       // "Authorization": no 
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);

      var employee = jsonBody[0];
      
      if (employee){
        var bank_name = employee.bank_name + " ";
        var bank_sgm4=employee.SEGMENT4 + " ";
        
       
      }

      conversation
      .variable('bankDets','This is your Bank Details : ' + ' , \n' + 'Bank: '+bank_name + ' , \n'+'Bank IBAN: '+bank_sgm4)
      
        .transition();

      done();
    });

  }
};


