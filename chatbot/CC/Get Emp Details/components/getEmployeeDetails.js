var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'getEmployeeDetails',
    properties: {
      PersonId: { required: true, type: 'string' },
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();


    /*var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");*/

    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    var reqUrl = `http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/getemployee/${PersonId}`;


    var options = {
      url: reqUrl,
      headers: {
       // "Authorization": no 
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);

      var employee = jsonBody[0];
      if (employee && employee.FIRST_NAME){

       var emp_Fname =employee.FIRST_NAME+ " ";
       var emp_Lname =employee.LAST_NAME+ " ";
       var emp_Num   =employee.EMPLOYEE_NUMBER+ " ";
       var empGender = ((employee.SEX === "M") ? 'Male' : 'female');

       var emp_email =employee.EMAIL_ADDRESS+ " "; 
       var Empinfo = 'This is your information: \n'+'Your First Name is:'+emp_Fname + '.\n' + 'Your Last Name is: '+emp_Lname+'. \n'+ 'Your Number is: '+emp_Num + '. \n'+'Your Gender is: '+empGender + '. \n'+ 'Your Email is: '+emp_email;
       conversation.variable("dets",Empinfo).transition();
      }else{
        conversation.variable("dets","I can't Get Your Information").transition();
      }

      
     

      done();
    });

  }
};


