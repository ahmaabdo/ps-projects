
var request = require('request');
var dateFormat = require('dateformat');
require('hijri-date');



module.exports = {
  metadata: () => ({
    name: 'createAbsence',
    properties: {
      empNum: { required: true, type: 'string' },
      //GroupId: { required: true, type: 'string' },
      AbsenceAttendanceTypeId: { required: true, type: 'string' },
      startDate: { required: true, type: 'string' },
      endDate: { required: true, type: 'string' }
      // absenceDays: { required: true, type: 'string' }
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { empNum } = conversation.properties();
    //var { GroupId } = conversation.properties();
    var { AbsenceAttendanceTypeId } = conversation.properties();
    var { startDate } = conversation.properties();
    var { endDate } = conversation.properties();
    //var { absenceDays } = conversation.properties();
    // Format The Date in Multiple Formats 
    switch (startDate.charAt(1)) {

      case '-': var reversedDate = startDate.split('-').reverse().join('-'); break;
      case '/': var reversedDate = startDate.split('/').reverse().join('/'); break;
      default: switch (startDate.charAt(2)) {
        case '-': var reversedDate = startDate.split('-').reverse().join('-'); break;
        case '/': var reversedDate = startDate.split('/').reverse().join('/'); break;

      }

    }
    if(parseInt(reversedDate.substr(0,4)) < 2000 ){ 
    var formattedFromDate = dateFormat(reversedDate, "dd-mm-yyyy");
    var splitFormattedStartDate = formattedFromDate.split('-');
    
      var HijjDate = new HijriDate(parseInt(splitFormattedStartDate[2]), parseInt(splitFormattedStartDate[1]), parseInt(splitFormattedStartDate[0]));
      var gregStartDate = dateFormat(HijjDate.toGregorian(), "dd-mmm-yyyy");



  }
    else {

      var gregStartDate = dateFormat(reversedDate, "dd-mmm-yyyy");

    }



    // End of Fromat for the start Date
    // Format The endDate in Multiple Formats 
    switch (endDate.charAt(1)) {

      case '-': var reversedDate = endDate.split('-').reverse().join('-'); break;
      case '/': var reversedDate = endDate.split('/').reverse().join('/'); break;
      default: switch (endDate.charAt(2)) {
        case '-': var reversedDate = endDate.split('-').reverse().join('-'); break;
        case '/': var reversedDate = endDate.split('/').reverse().join('/');; break;

      }

    }
    if(parseInt(reversedDate.substr(0,4)) < 2000 ){ 
    var formattedEndDate = dateFormat(reversedDate, "dd-mm-yyyy");
    var splitFormattedEndDate = formattedEndDate.split('-');
    
      var HijjDate = new HijriDate(parseInt(splitFormattedEndDate[2]), parseInt(splitFormattedEndDate[1]), parseInt(splitFormattedEndDate[0]));
      var gregEndDate = dateFormat(HijjDate.toGregorian(), "dd-mmm-yyyy");



    }
    else {

      var gregEndDate = dateFormat(reversedDate, "dd-mmm-yyyy");

    }
    // End of Fromat for the End Date

    var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");



    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    // var reqUrl = `http://hrbot.appspro-me.com:7007/hrbot/wepapi/main/create-absence?v_person_id=${PersonId}&v_business_group_id=${GroupId}&v_absence_attendance_type_id=${AbsenceAttendanceTypeId}&v_date_start=${startDate}&v_date_end=${endDate}&io_absence_days=${absenceDays}`;
    var reqUrl = `http://hrbot.appspro-me.com:7007/hrbot/wepapi/main/create-absence?v_emp_num=${empNum}&v_business_group_id=0&v_absence_attendance_type_id=${AbsenceAttendanceTypeId}&v_date_start=${gregStartDate}&v_date_end=${gregEndDate}`;

    var options = {
      url: reqUrl,

      headers: {
        "content-type": "application/x-www-form-urlencoded",
        //"Authorization": auth
      }
    };

    request.post(options, (err, res, body) => {



      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);


      var absence = jsonBody;
      var statues = "Uknown";
      var absence_ID = "Uknown";

      statues = absence.RESULT + " ";
      absence_ID = absence.ABSENCE_ATTENDANCE_ID + "";

      if (absence.RESULT === "SUCESS") {
        conversation.variable("CreateAbsStatus", "Your furlough request submitted successfully");
        conversation.variable("CreateAbsID", "Your furlough request Number: " + absence_ID).transition();
      } else {
        conversation.variable("CreateAbsStatus", "Your request hasn't  been submitted, Please try again");
        conversation.variable("CreateAbsID", " ").transition();
      }

      done();
    });
  }
};


