var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'getEmployeeAbsence',
    properties: {
      PersonId: { required: true, type: 'string' },
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();


    /*var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");*/

    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    var reqUrl = `http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/getEmployeeABSENCE/${PersonId}`;


    var options = {
      url: reqUrl,
      headers: {
       // "Authorization": no 
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);
      conversation.variable("absences",'This is your Absences : ');
      var empAbsence , empdate,trimEmpDate,trimmedEmpDate , empdays ,employee,leaveStart,leaveEnd,trimStart,trimEnd,trimedStart,trimedEnd ;
      
      var arrMaxindex = jsonBody.length-1; //30 -> 29 ->26
      //for(var i = arrMaxindex-3 ; i<arrMaxindex&&i>=0 ;i++){

          employee = jsonBody[arrMaxindex];
          empAbsence = employee.AbsenceName + " ";
          empdate = employee.CreatedDate + " ";
          empdays = employee.TotalDays + " ";
          leaveStart = employee.LeaveStart + " "; 
          leaveEnd = employee.LeaveEnd + " "; 
          trimStart = leaveStart.split(' ');
          trimedStart = trimStart[0];
          trimEnd = leaveEnd.split(' ');
          trimedEnd = trimEnd[0];
          trimEmpDate = empdate.split(' ');
          trimmedEmpDate = trimEmpDate[0];

         conversation.variable("absences1" ,"furlough type: "+empAbsence + "\n" +'Total Days: '+ empdays + "Day" + '\n'+'Start Date: '+trimedStart + '\n' + 'End Date: '+ trimedEnd );  

          employee = jsonBody[arrMaxindex-1];
          empAbsence = employee.AbsenceName + " ";
          empdate = employee.CreatedDate + " ";
          empdays = employee.TotalDays + " ";
          leaveStart = employee.LeaveStart + " "; 
          leaveEnd = employee.LeaveEnd + " ";
          trimStart = leaveStart.split(' ');
          trimedStart = trimStart[0];
          trimEnd = leaveEnd.split(' ');
          trimedEnd = trimEnd[0];

          conversation.variable("absences2" ,"furlough type: "+empAbsence + "\n" +'Total Days: '+ empdays +"Day"+ '\n'+'Start Date: '+trimedStart + '\n' + 'End Date: '+ trimedEnd); 

          employee = jsonBody[arrMaxindex-2];
          empAbsence = employee.AbsenceName + " ";
          empdate = employee.CreatedDate + " ";
          empdays = employee.TotalDays + " ";
          leaveStart = employee.LeaveStart + " "; 
          leaveEnd = employee.LeaveEnd + " ";
          trimStart = leaveStart.split(' ');
          trimedStart = trimStart[0];
          trimEnd = leaveEnd.split(' ');
          trimedEnd = trimEnd[0];

          conversation.variable("absences3" ,"furlough type: "+empAbsence + "\n" +'Total Days: '+ empdays +"Day"+ '\n'+'Start Date: '+trimedStart + '\n' + 'End Date: '+ trimedEnd).transition(); 
     done();
    });

  }
};


