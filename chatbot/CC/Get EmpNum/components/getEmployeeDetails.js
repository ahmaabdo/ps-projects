var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'getEmpNum',
    properties: {
      PersonId: { required: true, type: 'string' },
    },

    //supportedActions: ['getAbsence', 'getSalary']
  }),
  invoke: (conversation, done) => {

    var { PersonId } = conversation.properties();


    /*var username = 'PAASUser';
    var password = 'Paas@321';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");*/

    // var reqUrl = "https://ejfz.fa.em2.oraclecloud.com:443/hcmCoreApi/resources/latest/emps/?q=WorkMobilePhoneNumber=1972666";
    var reqUrl = `http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/getemployee/${PersonId}`;


    var options = {
      url: reqUrl,
      headers: {
       // "Authorization": no 
      }
    };

    request(options, (err, res, body) => {

      conversation.logger().info(err);
      var jsonBody = JSON.parse(body);

      var employee = jsonBody[0];
      var emp_name = "Uknown";
      if (employee ){
        emp_name = employee.FIRST_NAME + " ";
       
       var emp_Num=employee.EMPLOYEE_NUMBER + " ";
       
      }

      conversation.variable("empNum",emp_Num)
  
        
        
        
        .transition();

      done();
    });

  }
};


