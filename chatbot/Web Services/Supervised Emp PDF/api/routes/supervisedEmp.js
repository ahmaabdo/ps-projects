var path = require('path');
var request = require('request');
const express = require('express');
var pdf = require('pdfkit') ;
var fs = require('fs');
const router = express.Router();

var staticPath = path.join(__dirname, '/app');
var app = express();

router.get('/:empid', (req,res,next)=>{

    const empid =req.params.empid;
    var reqUrl =`http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/getSUPERVISOREMPNO/${empid}`;
    var options = {url: reqUrl,headers: {}};
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    console.log(date);
    var pdfFileName = 'file_'+empid+'_'+date;
    var pdfOutPath ='https://suppdfgen.herokuapp.com/'+pdfFileName+'.pdf';

    request(options, (err, res, body) => {
        var jsonBody = JSON.parse(body);
        var employee = jsonBody[0];
        var emp_fullName;
        var  emp_pos;
        var emp_id;
        const doc = new pdf ;
         doc.pipe(fs.createWriteStream('app/'+pdfFileName+'.pdf'));
         doc.registerFont('Arabic1','fonts/STC-Regular.ttf');
         doc.font('Arabic1');
         doc.image('img/mcs.jpg', 500, 15, {fit: [100, 100]}).rect(500, 15, 100, 100);
         doc.moveDown();
         doc.moveDown();
         doc.moveDown();

         for(let i = 0 ; i<jsonBody.length;i++){
            var employee = jsonBody[i];
            var emp_fullName = employee.FULL_NAME + " ";
            var  emp_pos = employee.POSITION_NAME+ " ";
            var emp_id = employee.PERSON_ID + " ";
            var R_empFullName = emp_fullName.split(" ").reverse().join(" ");
            var R_empPos= emp_pos.split(" ").reverse().join(" ");
            doc.text('Employee Name: '+ R_empFullName );
            doc.moveDown();
            doc.text('Employee ID: '+ emp_id );
            doc.moveDown();
            doc.text('Employee Position: '+ R_empPos);
            doc.moveDown();
            doc.text('----------------------------------------------------------------------');
            }
            doc.end() ;
      });
      res.status(200).json({
        message: 'your PDF link',
        link:pdfOutPath,
        id_r: empid
    });
    
});
var staticPath = path.join(__dirname, '/app');
app.use(express.static(staticPath));

module.exports = router;