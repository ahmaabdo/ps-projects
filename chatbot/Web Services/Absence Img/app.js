const express = require('express');
var path = require('path');
var fs = require('fs');
const app = express();

const absenceImgRouts = require('./api/routes/absenceImg');

app.use('/absenceImg',absenceImgRouts);


module.exports = app;

var staticPath = path.join(__dirname, '/app');
app.use(express.static(staticPath));

