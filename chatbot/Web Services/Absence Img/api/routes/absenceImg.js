var path = require('path');
var request = require('request');
const express = require('express');
var pdf = require('pdfkit');
var plotly = require('plotly')('raizo1994', 'Knre49kJ3MGdkcfO5FZV');
var fs = require('fs');
const router = express.Router();

var staticPath = path.join(__dirname, '/app');
var app = express();

router.get('/:empId', (req, res, next) => {
    //request options to get absences details
    const empid = req.params.empId;
    var reqUrl = `http://hrbot.appspro-me.com:7007/AshrafTest-Model-context-root/jersey/testclass/absencesBalance/${empid}`;
    var options = { url: reqUrl, headers: {} };

    // for naming the img file
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    console.log(date);
    var imgFileName = 'file_' + empid + '_' + date;
    var imgOutPath = 'https://link.herokuapp.com/' + imgFileName + '.png';

    request(options, (err, res, body) => {
        var jsonBody = JSON.parse(body);
        var empName = "Uknown";
        var empPDayesTaken = "";
        var empTotal = "";
        var empRemaining = "";

        empPDayesTaken = jsonBody[0].days_taken + "";
        empTotal = jsonBody[0].total + "";
        empRemaining = jsonBody[0].remaining + "";
        toatalDaysLable = "Total Days = "+empTotal+" Days";
        console.log(jsonBody)
        
        var xvalue = ["Taken", 'remaining'];
        var yvalue = [empPDayesTaken, empRemaining];
        var trace1 = {
            x: xvalue, lable: 'Days',
            y: yvalue,
            text: yvalue.map(String),
            textposition: 'auto',
            hoverinfo: 'none',
            marker: {
                color: ["rgb(214, 48, 49)", "rgb(0, 184, 148)"],
                opacity: 1
            },


            type: "bar"
        };
        var layout = {
            title: "Your Absence Balance",
            xaxis: { tickangle: -45 },
            yaxis: { title: toatalDaysLable }
        };
        var figure = { 'data': [trace1], layout: layout };

        var imgOpts = {
            format: 'png',
            width: 1000,
            height: 500


        };


        plotly.getImage(figure, imgOpts, function (error, imageStream) {
            if (error) return console.log(error);

            imageStream.pipe(fs.createWriteStream('app/'+imgFileName + '.png'));
            // var fileStream = fs.createWriteStream('app/'+imgFileName + '.png');
            // // imageStream.pipe(fileStream);
        });

    });
    res.status(200).json({
        message: 'your img link',
        link: imgOutPath,
        id_r: empid
    });

});
var staticPath = path.join(__dirname, '/app');
app.use(express.static(staticPath));

module.exports = router;