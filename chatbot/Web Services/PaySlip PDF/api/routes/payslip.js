var path = require('path');
var request = require('request');
const express = require('express');
var pdf = require('pdfkit') ;
var fs = require('fs');
const router = express.Router();

var staticPath = path.join(__dirname, '/app');
var app = express();

router.get('/:personId&:fDate&:tDate', (req,res,next)=>{

    const PersonId =req.params.personId;
    const fromDate =req.params.fDate;
    const toDate =req.params.tDate;


    var reqUrl = `http://hrbot.appspro-me.com:7007/hrbot/wepapi/main/payslip/${PersonId}?fromDate=${fromDate}&toDate=${toDate}`;

    var options = {url: reqUrl,headers: {}};
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    console.log(date);
    var pdfFileName = 'file_'+PersonId+'_'+date;
    var pdfOutPath ='https://pdfpayslip.herokuapp.com/'+pdfFileName+'.pdf';

    request(options, (err, res, body) => {
        var jsonBody = JSON.parse(body);
        var employee = jsonBody.DATA[0];
        var totalEarnings =  0 ; 
        var totalDeductions = 0 ; 
  
        var Payment_NAME = "Uknown";
        // var result = jsonBody.RESULT + " ";
        // let slip = "" ; 

        const doc = new pdf ;
         doc.pipe(fs.createWriteStream('app/'+pdfFileName+'.pdf'));
         doc.registerFont('Arabic1','fonts/STC-Regular.ttf');
         doc.font('Arabic1');
         doc.image('img/mcs.jpg', 500, 15, {fit: [100, 100]}).rect(500, 15, 100, 100);
         doc.moveDown();
         doc.moveDown();
         doc.moveDown();
         doc.moveDown();
         doc.moveDown();

        for(let i =0;i<jsonBody.DATA.length;i++){
            if (jsonBody) {
              Payment_NAME = jsonBody.DATA[i].ELEMENT_NAME + " ";
              var classification = jsonBody.DATA[i].CLASSIFICATION_NAME + " ";
              var Value = jsonBody.DATA[i].RESULT_VALUE + " ";
              if(classification == "Earnings "){
      
                totalEarnings += parseFloat(Value); 
              }
              else{
                totalDeductions += parseFloat(Value) ; 
      
              }
            }
            var orderNum = i + 1 ;
            var spaceVar = "  "
            var LspaceVar = "       "
            var dashsep =   "--------------------------------------------------------------------------------------------"
            doc.text(LspaceVar +orderNum + spaceVar + 'الرقم: ', {align: 'right'});
            doc.text( LspaceVar +Payment_NAME+spaceVar+'الإسم: '+LspaceVar+LspaceVar +classification+spaceVar+ 'التصنيف: ' , {align: 'right'});
            doc.text( LspaceVar +Value +spaceVar+'القيمة:  ' , {align: 'right'});
            doc.text(dashsep, {align: 'right'});
          }
          var total = totalEarnings - totalDeductions ; 

            doc.moveDown();
            doc.text( LspaceVar +totalEarnings +spaceVar+" : "+'الأرباح مجموع '+LspaceVar+LspaceVar+totalDeductions+spaceVar+ " : "+'الخصومات مجموع ' , {align: 'right'});
            doc.text( LspaceVar +total+spaceVar+" : "+'النهائي المجموع ', {align: 'right'});

          doc.end() ;
      });
      res.status(200).json({
        message: 'your PDF link',
        link:pdfOutPath,
        id_r: PersonId
    });
    
});
var staticPath = path.join(__dirname, '/app');
app.use(express.static(staticPath));

module.exports = router;