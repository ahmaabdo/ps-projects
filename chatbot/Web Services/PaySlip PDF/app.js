const express = require('express');
var path = require('path');
var pdf = require('pdfkit') ;
var fs = require('fs');
const app = express();

const payslipRouts = require('./api/routes/payslip');

app.use('/payslip',payslipRouts);


module.exports = app;

var staticPath = path.join(__dirname, '/app');
app.use(express.static(staticPath));

