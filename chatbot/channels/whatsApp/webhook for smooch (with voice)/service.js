const OracleBot = require('@oracle/bots-node-sdk');
const { WebhookClient, WebhookEvent } = OracleBot.Middleware;
const Smooch = require('smooch-core');
const bodyParser = require('body-parser');
const speech = require('@google-cloud/speech');
const http = require('http');
const https = require('https');
const fs = require('fs');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);
var request = require('sync-request');
var request2 = require('request');
const stream = require('stream');
require('dotenv').config();

//appuserId will be set automaticaly
var appUserId = ' ';
const KEY_ID = 'app_5dd3c04c5b94d700107f1920';
const SECRET = 'UNyITXJzeVRU-I6AC2XMJc00PVyAlkE4iCLzZ-Xi_oAU-lJyhv5cGuoeFyy3fr1moUf3VLiQGtvNxk8TGg02GQ';

// Add webhook Info form smooch App settings
const smooch = new Smooch({
  keyId: KEY_ID,
  secret: SECRET,
  scope: 'app'
});


module.exports = (app) => {
  const logger = console;

  OracleBot.init(app, {
    logger,
  });

// Add webhook Info form ODA Channle settings
  const webhook = new WebhookClient({
    channel: {
      url: 'https://botv2frk1I0165H0930AFbots-mpaasocimt.botmxp.ocp.oraclecloud.com:443/connectors/v1/tenants/idcs-6d466372210e4300bb31f4db15e8e96c/listeners/webhook/channels/58dddb9a-a53c-4a78-ad25-e877e02d7ce0',
      secret: 'IcfofRTNd2B2FNXuJBwiTLFxo8l6XQ6y'
    }
  });
 


   var downloadFile = function (url,res2) {
    var useUrl = url;


    var r = request2.get(useUrl, function (err, res, body) {
      console.log(r.uri.href);
      var lastUrl =r.uri.href;
      const request_2 = https.get(lastUrl,
  
      function (response) {
  
        var stream = response.pipe(fs.createWriteStream('./in.mp3'));
  
        stream.on('finish', function () {
          ffmpeg('./in.mp3')
            .toFormat('flac')
            .audioFrequency(16000)
            .audioChannels(1).save('./done.flac').on('end', () => {
              console.log('Processing finished !');
  
              main(res2).catch(console.error);
            });;
  
          console.log('2-file from convert:');
          console.log('2-file from convert:');
  
  
        });
  
  
  
      });
    });
  
  
  }
  
  var convertFile = function (url,res) {
    
    downloadFile(url,res);
  
  }

  async function main(res) {
  
    const client = new speech.SpeechClient();
  
    const file = fs.readFileSync('./done.flac');
  
    fs.readFile('./done.flac', function (err, data) {
      //     // console.log(err);
      //     // console.log(data);
      const audioBytes = file.toString('base64');
      // The audio file's encoding, sample rate in hertz, and BCP-47 language code
      const audio = {
        content: audioBytes,
      };
  
      const config = {
        encoding: 'flac',
        sampleRateHertz: 16000,
        languageCode: 'ar-XA',
        speechContext: {
          phrases: ['amazus']
        }
      };
      const request = {
        audio: audio,
        config: config,
      };
  
      // Detects speech in the audio file
      client
        .recognize(request)
        .then(data => {
          const response = data[0];
          const transcription = response.results
            .map(result => result.alternatives[0].transcript)
            .join('\n');
          message = transcription;
          console.log(`1- Transcription: ${transcription}`);
          textRecognized = transcription;
          const MessageModel = webhook.MessageModel();
          messageFVoice = {
            userId: '64b463725262fe680304bed1',
            messagePayload: MessageModel.textConversationMessage(textRecognized)
          };
          logger.info('Message voice to bot:', messageFVoice.messagePayload);
          webhook.send(messageFVoice).then(res.status(400).end());
          logger.info('------------------------------------> End <------------------------------------');
        })
        .catch(err => {
          console.error('ERROR:', err);
        });
  
    });
  
  
  
  
  
  }



  webhook.on(WebhookEvent.ERROR, console.error);
  
// Message form ODA sent to Whatsapp
  app.post('/test/message1', (req, res) => {
    
    logger.info('Message type from WA to ODA :', req.body.messagePayload.type);
    logger.info('Message from WA to ODA to ODA:', req.body.messagePayload);

// smooch message Template will be sent from here 
//first we check if the message is text or attachment

    if(req.body.messagePayload.type==='text'){
    smooch.appUsers.sendMessage(appUserId,{
      type: 'text',
      text: req.body.messagePayload.text,
      role: 'appMaker'
  });
    }else if(req.body.messagePayload.type==='attachment'){

      // if the message is attachment then we need to check of what kind of Attachment ( file - image - video)
        if(req.body.messagePayload.attachment.type==='image'){
          smooch.appUsers.sendMessage(appUserId,{
            type: 'image',
            mediaUrl: req.body.messagePayload.attachment.url,
            role: 'appMaker'
        });

        }else if(req.body.messagePayload.attachment.type==='file'){
          smooch.appUsers.sendMessage(appUserId,{
            type: 'file',
            mediaUrl: req.body.messagePayload.attachment.url,
            role: 'appMaker'
        });

        }else if(req.body.messagePayload.attachment.type==='video'){
          smooch.appUsers.sendMessage(appUserId,{
            type: 'file',
            mediaUrl: req.body.messagePayload.attachment.url,
            role: 'appMaker'
        });

        }

    }else{
      smooch.appUsers.sendMessage(appUserId,{
        type: 'text',
        text: 'this message type isnot supported on whatsapp!!',
        role: 'appMaker'
    });
    }

   });
 
   app.post('/bot/message', webhook.receiver());


  // Message form WA sent to ODA  
  app.post('/test/message', (req, res) => {
    var MediaUrl0 = req.body.messages[0].mediaUrl;
    var msgType = req.body.messages[0].type;
    var msgMediaType = req.body.messages[0].mediaType;
    const { user, text } = req.body;
    appUserId = req.body.messages[0].authorId;
    console.log('---------------- app user Id: '+appUserId+' ----------------');
    
   logger.info('Message from appUserId:', req.body);
    const MessageModel = webhook.MessageModel();

    //check Message Type form WA sent to ODA then send the message to ODA
    if(msgType==='file'||msgMediaType==='audio/ogg; codecs=opus'){
      console.log(' ----------------- the message is audio ----------------- ');

      //Function to convert voice from WA to Text then to return the text and send it to ODA
      convertFile(MediaUrl0,res);
    }else{

      //the message is text so it will be sent as it is to ODA
      console.log('----------------- the message is not audio -----------------');
      const message = {
        userId: '64b463725262fe680304bed1',
        messagePayload: MessageModel.textConversationMessage(req.body.messages[0].text)
      };
      logger.info('Message to bot:', message.messagePayload);
      webhook.send(message).then(() => res.send('ok'), e => res.status(400).end());
    }


  });
}