const express = require('express');
const service = require('./service');
const pkg = require('./package.json');
const Smooch = require('smooch-core');
const logger = console;
const app = express();
service(app);

const server = app.listen(process.env.PORT || 3000, () => {
  logger.info(`${pkg.name} service online\n`);
  //logger.info(Smooch.integrations);
  // Smooch.integrations.list({ appId: 'act_5d3f15d3f4fefe00107895a3' }).then((response) => {
  //   // async code
  //   logger.info('for photo:', "test");
  // });
//   s
});

module.exports = server;
