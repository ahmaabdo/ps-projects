
// you should install this modules first:

// npm install --save @google-cloud/speech
// npm install --save fluent-ffmpeg
// npm install --save @ffmpeg-installer/ffmpeg

const speech = require('@google-cloud/speech');
const http = require('http');
const https = require('https');
const fs = require('fs');
var request = require('sync-request');
const stream = require('stream');

// @ffmpeg is a converter to convert the audio file to .Flac with desired audioFrequency
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);

// to set the credentials of the google cloud speech recognition
require('dotenv').config();

// the name of the output file that will be converted
const fileName = './done.flac';

// the name of the file that will be dwonloded then converted file
let file2Con = './in.mp3';

// the link of the voice or the audio file that will be used
var theUrl ="https://s3-external-1.amazonaws.com/media.twiliocdn.com/ACfb482f9279587f2b86baaccf71a2f693/4723f7ddca95beb8c6ae48bcff788c1a";



// function of converting from google cloud
var downloadFile = function (url) {
  console.log('1- start download the Audio File\n');
  var useUrl=url;
  const file_in = fs.createWriteStream('./in.mp3');
  const request_2 = https.get(useUrl,

    function (response) {

      var stream = response.pipe(file_in);
      console.log('2- start Convert the Audio File');
      stream.on('finish', function () {
        ffmpeg('./in.mp3')
          .toFormat('flac')
          .audioFrequency(16000)
          .audioChannels(1).save('./done.flac').on('end', () => {
            console.log('Processing finished ! file converted\n');
            main().catch(console.error);
          });;
      });



    });


}

// function of converting from google cloud
async function main() {
  console.log('3- start recognize the speech from the Audio File');
  const client = new speech.SpeechClient();

  const file = fs.readFileSync('./done.flac');

  fs.readFile('./done.flac', function (err, data) {
    //     // console.log(err);
    //     // console.log(data);
    const audioBytes = file.toString('base64');
    // The audio file's encoding, sample rate in hertz, and BCP-47 language code
    const audio = {
      content: audioBytes,
    };

    const config = {
      encoding: 'flac',
      sampleRateHertz: 16000,
      languageCode: 'en-US',
      speechContext: {
        phrases: ['amazus']
      }
    };
    const request = {
      audio: audio,
      config: config,
    };

    // Detects speech in the audio file
    client
      .recognize(request)
      .then(data => {
        const response = data[0];
        const transcription = response.results
          .map(result => result.alternatives[0].transcript)
          .join('\n');
        message = transcription;
        console.log(`\n THE OUTPUT: Transcription: ${transcription}`);
      })
      .catch(err => {
        console.error('ERROR:', err);
      });

  });





}

//this function will download the file 
// then will convert it to the best type to be used by speech recognition
// then will recognize the speech and return with a text
downloadFile(theUrl);

