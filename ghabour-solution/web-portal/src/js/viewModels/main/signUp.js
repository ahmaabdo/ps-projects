define(['knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmessaging', 'ojs/ojlabel',
    'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojformlayout', 'ojs/ojvalidationgroup','ojs/ojinputnumber'],
        function (ko, app, services, commonhelper, Message) {

            function SignUpViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;

                self.label = ko.observable();
                self.userName = ko.observable();
                self.password = ko.observable();
                self.passwordCheck = ko.observable();
                self.confirmPasswordCheck = ko.observable();
                self.confirmPassword = ko.observable();
                self.currentRawPassValue = ko.observable();
                self.currentPassValue = ko.observable();
                self.firstName = ko.observable();
                self.lastName = ko.observable();
                self.email = ko.observable();
                self.carNumber=ko.observable();
                self.mobileNumber = ko.observable();
                self.engineNumber = ko.observable();
                self.nationalId = ko.observable();
                self.chassisNumber = ko.observable();
                self.errorMsgEmail = ko.observable();
                self.errorMsgPhone = ko.observable();
                self.errorMsgPass = ko.observable();
                self.errorMsgUsername = ko.observable();
                self.groupValid = ko.observable();
                self.isUserLoggedIn = ko.observable(app.userLoggedIn());

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                window.gapi.load('client:auth2', function () {
                    window.gapi.auth2.init(app.googlecredentials).then(function (googleAuth) {
                        self.googleAuth = googleAuth;
                        self.isUserLoggedIn(googleAuth.isSignedIn.get());
                    });
                });
                self.continueWithGoogle = function () {
                    app.continueWithGoogle();
                };
                self.emailValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
                                hint: self.label().emailHint(),
                                messageDetail: self.label().emailmessageDetail()
                            }
                        }];
                });
                self.usernameValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9]{3,}",
                                hint: self.label().usernameHint(),
                                messageDetail: self.label().usernameMessageDetail()
                            }
                        }];
                });
                self.mobileValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[0]+[1]+[0-9]{9,9}",
                                hint: self.label().phoneHint(),
                                messageDetail: self.label().phoneMessageDetail()
                            }
                        }];
                });

                self.validatorPass = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9]{8,}",
                                hint: self.label().passHint(),
                                messageDetail: self.label().passMessageDetail()
                            }
                        }];
                });
                
                self.clearFields = function () {
                    self.userName('');
                    self.password('');
                    self.confirmPassword('');
                    self.firstName('');
                    self.lastName('');
                    self.email('');
                    self.mobileNumber('');
                    self.chassisNumber('');
                    self.engineNumber('');
                    self.nationalId('');
                };
                self.usernameChangedHandler = function (event) {
                    if (event) {
                        var username = event.detail.value;
                        var payload = {
                            "username": username
                        };
                        services.postGeneric("users/is-username-valid", payload).then(data => {
                            if (data.valid == "false" && username) {
                                self.errorMsgUsername([{
                                        summary: self.label().usernameValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } else {
                                self.errorMsgUsername([]);
                            }
                        }, app.failCbFn);
                    }
                };
                self.emailChangedHandler = function (event) {
                    if (event) {
                        var email = event.detail.value;
                        var payload = {
                            "email": email
                        };
                        services.postGeneric("users/is-email-valid", payload).then(data => {
                            if (data.valid == "false" && email) {
                                self.errorMsgEmail([{
                                        summary: self.label().emailValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } else {
                                self.errorMsgEmail([]);
                            }
                        }, app.failCbFn);
                    }
                };
                self.phoneChangedHandler = function (event) {
                    if (event) {
                        var phone = event.detail.value;
                        var payload = {
                            "phone": phone
                        };
                        services.postGeneric("users/is-phone-valid", payload).then(data => {
                            if (data.valid == "false" && phone) {
                                self.errorMsgPhone([{
                                        summary: self.label().phoneValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } else {
                                self.errorMsgPhone([]);
                            }
                        }, app.failCbFn);
                    }
                };

                self.createAccount = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    if(self.password()==self.confirmPassword())
                    {
                        app.loading(true);
                        var payload = {
                            "username": self.userName(),
                            "password": self.password(),
                            "firstName": self.firstName(),
                            "lastName": self.lastName(),
                            "email": self.email(),
                            "phone": self.mobileNumber(),
                            "chassisNumber": self.chassisNumber(),
                            "engineNumber": self.engineNumber(),
                            "nationalId": self.nationalId()
                        };
                        services.postGeneric(commonhelper.addUser, payload).then(data => {
                            app.loading(false);
                            app.createMessage("confirmation", self.label().successMsg());
                            app.login(self.userName().toLowerCase(), self.password());
                        }, app.failCbFn);
                    } else {
                        app.loading(false);
                        app.createMessage("error", self.label().invalidPass());
                    }
                };

                self.logIn = function () {
                    oj.Router.rootInstance.go('logIn');
                };
                
                ko.computed(()=>{
                    self.passChangedHandler=function(){
                     if (self.passwordCheck() == self.confirmPasswordCheck()) {
                                self.errorMsgPass([]);
                            } else {
                                self.errorMsgPass([{
                                        summary: self.label().invalidPass(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } 
                };
                });
                self.connected = function () {
                    app.headerVisible(false);
                    app.loading(false);
                    initTranslations();
                    var pass,confirm;
                    document.getElementById("password").addEventListener('rawValueChanged', function (event) {
                        pass = event.detail.value;
                        self.passwordCheck(pass);
                    });
                    document.getElementById("confirm-pass").addEventListener('rawValueChanged', function (event) {
                            confirm = event.detail.value;
                            self.confirmPasswordCheck(confirm);
                        });
                    self.clearFields();
                };

                function initTranslations() {
                    self.label({
                        continueWithGoogle: ko.observable(getTranslation('main.continueWithGoogle')),
                        carNumberLbl: ko.observable(getTranslation('main.carNumberLbl')),
                        engineNumberLbl: ko.observable(getTranslation('main.engineNumberLbl')),
                        nationalIdLbl: ko.observable(getTranslation('main.nationalIdLbl')),
                        usernameValid: ko.observable(getTranslation('main.usernameValid')),
                        phoneValid: ko.observable(getTranslation('main.phoneValid')),
                        emailValid: ko.observable(getTranslation('main.emailValid')),
                        phoneHint: ko.observable(getTranslation('main.phoneHint')),
                        phoneMessageDetail: ko.observable(getTranslation('main.phoneMessageDetail')),
                        passHint: ko.observable(getTranslation('main.passHint')),
                        passMessageDetail: ko.observable(getTranslation('main.passMessageDetail')),
                        successMsg: ko.observable(getTranslation('main.successMsg')),
                        placeholderUsername: ko.observable(getTranslation('main.placeholderUsername')),
                        placeholderFirstname: ko.observable(getTranslation('main.placeholderFirstname')),
                        placeholderEngine: ko.observable(getTranslation('main.engineNumberLbl')),
                        placeholderNational: ko.observable(getTranslation('main.nationalIdLbl')),
                        placeholderLastname: ko.observable(getTranslation('main.placeholderLastname')),
                        placeholderPassword: ko.observable(getTranslation('main.placeholderPassword')),
                        placeholderConfPassword: ko.observable(getTranslation('main.placeholderConfPassword')),
                        placeholderEmail: ko.observable(getTranslation('main.placeholderEmail')),
                        placeholderPhone: ko.observable(getTranslation('main.placeholderPhone')),
                        placeholderChassis: ko.observable(getTranslation('main.placeholderChassis')),
                        invalidPass: ko.observable(getTranslation('main.invalidPass')),
                        usernameHint: ko.observable(getTranslation('main.usernameHint')),
                        usernameMessageDetail: ko.observable(getTranslation('main.usernameMessageDetail')),
                        emailHint: ko.observable(getTranslation('main.emailHint')),
                        emailmessageDetail: ko.observable(getTranslation('main.emailmessageDetail')),
                        createNewAccountLbl: ko.observable(getTranslation('main.createNewAccount')),
                        userNameLbl: ko.observable(getTranslation('main.userName')),
                        passwordLbl: ko.observable(getTranslation('main.password')),
                        confirmPasswordLbl: ko.observable(getTranslation('main.confirmPassword')),
                        emailLbl: ko.observable(getTranslation('main.email')),
                        firstNameLbl: ko.observable(getTranslation('main.firstName')),
                        lastNameLbl: ko.observable(getTranslation('main.lastName')),
                        mobileNumberLbl: ko.observable(getTranslation('main.mobileNumber')),
                        chassisNumberLbl: ko.observable(getTranslation('main.chassisNumber')),
                        createAccountLbl: ko.observable(getTranslation('main.createAccount')),
                        alreadyUserLbl: ko.observable(getTranslation('main.alreadyUser')),
                        logInLbl: ko.observable(getTranslation('main.login'))
                    });
                }
            }

            return new SignUpViewModel();
        }
);
