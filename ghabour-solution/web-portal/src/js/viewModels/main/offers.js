define(['knockout', 'appController', 'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojinputtext'],
        function (ko, app) {

            function OffersViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observableArray([]);
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.signIn = function () {
                    oj.Router.rootInstance.go('logIn');
                };
                self.signUp = function () {
                    oj.Router.rootInstance.go('signUp');
                };

                self.connected = function () {
                    app.headerVisible(false);
                    app.loading(false);
                    initTranslations();
                };

                function initTranslations() {
                    self.label = {
                        offersLbl: ko.observable(getTranslation('main.offers')),
                        signInLbl: ko.observable(getTranslation('main.signIn')),
                        signUpLbl: ko.observable(getTranslation("main.signUp")),
                        offer: ko.observable(getTranslation("main.offer"))
                    };
                }
            }

            return new OffersViewModel();
        }
);
