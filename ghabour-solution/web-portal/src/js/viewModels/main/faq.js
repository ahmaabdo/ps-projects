define(['knockout', 'appController', 'config/services', 'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojinputtext',
    'ojs/ojbutton', 'ojs/ojconveyorbelt', 'ojs/ojcollapsible'],
        function (ko, app, services) {

            function faqViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.currentLocal = ko.observable(app.getLocale());
                self.faqQuestions = ko.observableArray([]);
                self.data = ko.observableArray([]);
                self.dataArr = ko.observableArray([]);
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.filterFaq();
                        initTranslations();
                    }
                });

                self.filterFaq = function (event) {
                    self.faqQuestions([]);
                    if (event) {
                        var type = event.type;
                        var filterType = self.dataArr().filter(e => e.type == type);
                        if (filterType) {
                            if (app.getLocale() == 'ar') {
                                for (var i in filterType) {
                                    self.faqQuestions.push({question: filterType[i].quesAr, answer: filterType[i].answerAr});
                                }
                            } else {
                                for (var i in filterType) {
                                    self.faqQuestions.push({question: filterType[i].quesEn, answer: filterType[i].answerEn});
                                }
                            }
                        }
                    }
                };

                ko.computed(() => {
                    self.dataArr([]);
                    self.data([]);
                    if (app.faqData()) {
                        self.dataArr(app.faqData());
                        self.unique = ko.computed(function () {
                            var sortedItems = app.faqData().concat().sort(function (left, right) {
                                return left.type == right.type ? 0 : (left.type < right.type ? -1 : 1);
                            });

                            var type;

                            for (var i = 0; i < sortedItems.length; i++) {
                                if (!type || type != sortedItems[i].type) {
                                    type = sortedItems[i].type;
                                } else {
                                    sortedItems.splice(i, 1);
                                    i--;
                                }
                            }
                            return sortedItems;
                        });
                        self.data(self.unique());
                    }
                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                };

                function initTranslations() {
                    self.currentLocal(app.getLocale());
                    app.headerTitle(getTranslation('main.faq'));
                    self.label({
                        canHelpTodayLbl: ko.observable(getTranslation('main.canHelpToday')),
                        canHelpLbl: ko.observable(getTranslation('main.canHelp')),
                        chooseCategoryLbl: ko.observable(getTranslation('main.chooseCatagory')),
                        maintenanceLbl: ko.observable(getTranslation('main.maintenance')),
                        complainLbl: ko.observable(getTranslation('main.complain')),
                        testDriveLbl: ko.observable(getTranslation('main.testDrive')),
                        reservationPaymentLbl: ko.observable(getTranslation('main.reservationPayment'))
                    });
                }
            }
            return new faqViewModel();
        }
);
