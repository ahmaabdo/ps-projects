define(['knockout', 'ojs/ojcore', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojlabel', 'ojs/ojmessages'
            , 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojvalidationgroup'],
        function (ko, oj, app, services, commonhelper) {

            function LoginViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.userName = ko.observable();
                self.password = ko.observable();
                self.groupValid = ko.observable();

                self.isUserLoggedIn = ko.observable(app.userLoggedIn());
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                window.gapi.load('client:auth2', function () {
                    window.gapi.auth2.init(app.googlecredentials).then(function (googleAuth) {
                        self.googleAuth = googleAuth;
                        self.isUserLoggedIn(googleAuth.isSignedIn.get());
                    });
                });


                self.logIn = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    app.login(self.userName().toLowerCase(), self.password());
                };
                self.continueWithGoogle = function () {
                    app.continueWithGoogle();
                };
                self.fbLogin=function(){
                    app.checkLoginState();
                };

                self.connected = function () {
                    app.headerVisible(false);
                    app.loading(false);
                    initTranslations();
                };

                function initTranslations() {
                    self.label({
                        title: ko.observable(getTranslation('main.signIn')),
                        userName: ko.observable(getTranslation('main.userName')),
                        password: ko.observable(getTranslation('main.password')),
                        logIn: ko.observable(getTranslation('main.login')),
                        continueWithGoogle: ko.observable(getTranslation('main.continueWithGoogle')),
                        forgetPassword: ko.observable(getTranslation('main.forgetPassword')),
                        haveAccount: ko.observable(getTranslation('main.haveAccount')),
                        signUp: ko.observable(getTranslation('main.signUp'))
                    });
                }

            }


            return new LoginViewModel();
        }
);
