define(['knockout', 'appController', 'config/services', 'ojs/ojarraydataprovider', 'ojs/ojlistdataproviderview', 'ojs/ojlabel',
    'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox'
            , 'ojs/ojlistview', 'ojs/ojselectcombobox'],
        function (ko, app, services, ArrayDataProvider, ListDataProviderView) {

            function LocationViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var loading = '<div id="iframe_loading" class="loading-section"> <div class="loading"> <div class="lds-dual-ring"></div> <div class="text"> <span class="loading-text"></span> <span class="dots">...</span> </div> </div> </div>';
                self.label = ko.observable();
                var newFrameLink, newContent;
                self.data = ko.observableArray([]);
                self.selectListner = ko.observableArray([]);
                self.search = ko.observable();
                self.latitude = ko.observable();
                self.longitude = ko.observable();

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.handleValueChanged = function () {
                    self.search(document.getElementById("text-input10").rawValue);
                };

                self.dataProvider = ko.computed(function () {
                    var filterRegEx = new RegExp(self.search(), 'i');
                    var filterCriterion = {
                        op: '$or',
                        criteria: [
                            {op: '$regex', value: {locationName: filterRegEx}},
                            {op: '$regex', value: {city: filterRegEx}},
                            {op: '$regex', value: {address: filterRegEx}}
                            ]
                    };
                    return new ListDataProviderView(new ArrayDataProvider(self.data, {keyAttributes: 'value'}), {filterCriterion: filterCriterion});
                });

                self.searchAction = function () {
                    if(self.selectListner().length>0){
                    if (self.latitude() && self.longitude()) {
                        var location = "https://www.google.com/maps/search/?api=1&query=" + self.latitude() + "," + self.longitude();
                        window.open(location);
                    } else {
                        app.createMessage("error", self.label().locationErr());
                    }
                }
                };

                ko.computed(() => {
                    if (self.selectListner().length > 0 && self.data().length > 0) {

                        var filteredArr = self.data().filter(e => {
                            return e.value == self.selectListner()[0];
                        });

                        if (filteredArr.length > 0) {
                            self.latitude(filteredArr[0].latitude);
                            self.longitude(filteredArr[0].longitude);
                            //Refreshing embeded map view
                            newFrameLink = "https://maps.google.com/maps?q=" + self.latitude() + "," + self.longitude() + "&hl=en;z=14&amp;output=embed";
                            newContent = '<iframe id="maps_iframe"  src="' + newFrameLink + '" width="600" height="500" id="gmap_canvas" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">';
                            $('#gmap_container').append(loading);
                            $('#maps_iframe').replaceWith(newContent);
                            $("#maps_iframe").on('load', function () {
                                $("#iframe_loading").remove();
                            });
                        } 
                    }
                });
                ko.computed(() => {
                    var data = app.locationData();
                    if (data) {
                        var brandLocation=data.filter(e => e.brand == "Chery");
                        app.loading(false);
                        self.data([]);
                        for (var i in brandLocation) {
                            if (app.getLocale() == 'ar') {
                                if (brandLocation[i].locationNameAr && brandLocation[i].addressAr && brandLocation[i].cityAr) {
                                    brandLocation[i].locationName = brandLocation[i].locationNameAr;
                                    brandLocation[i].address = brandLocation[i].addressAr;
                                    brandLocation[i].city = brandLocation[i].cityAr;
                                }
                            }
                            self.data.push({locationName: brandLocation[i].locationName, city: brandLocation[i].city,
                                address: brandLocation[i].address, brand: brandLocation[i].brand, latitude: brandLocation[i].latitude, longitude: brandLocation[i].longitude, value: brandLocation[i].locationName});
                        }
                    }
                });

                self.connected = function () {
                    app.headerVisible(true);
                    initTranslations();
//                    app.loading(true);
                    initTranslations();
                };


                function initTranslations() {
                    app.headerTitle(getTranslation('main.location'));
                    self.label ( {
                        searchBox: ko.observable(getTranslation('main.searchLbl')),
                        canHelpLbl: ko.observable(getTranslation('main.canHelp')),
                        signInLbl: ko.observable(getTranslation('main.signIn')),
                        signUpLbl: ko.observable(getTranslation("main.signUp")),
                        ghabbourLocationLbl: ko.observable(getTranslation("main.ghabbourlocation")),
                        findNearestLbl: ko.observable(getTranslation("main.findNearestBranch")),
                        searchLbl: ko.observable(getTranslation('main.search')),
                        cityLbl: ko.observable(getTranslation('main.city')),
                        locationErr: ko.observable(getTranslation('main.locationErr')),
                        locationLbl: ko.observable(getTranslation('main.location')),
                        branchLbl: ko.observable(getTranslation('main.branch'))
                    });
                }
            }

            return new LocationViewModel();
        }
);
