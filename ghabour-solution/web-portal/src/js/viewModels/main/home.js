define([],
        function () {
            function HomeViewModel() {
                this.connected = function () {
                    oj.Router.rootInstance.go('dashboard');
                };
            }

            return new HomeViewModel();
        }
);
