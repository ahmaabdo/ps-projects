define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojvalidation-base', 'ojs/ojarraydataprovider', 'ojs/ojpagingdataproviderview', 'ojs/ojpagingcontrol', 'ojs/ojconveyorbelt', 'ojs/ojbutton'],
        function (oj, ko, app, services, commonhelper, ojvalbase, ArrayDataProvider, PagingDataProviderView) {
            function historyContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var lastBrand = null;
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.isDisabled = ko.observable(true);
//                self.historyVisible = ko.observable(false);
                self.dataStatus = ko.observable("loading");
                self.dataprovider = ko.observableArray([]);
//                self.jsonDataStr = ko.observableArray([]);
                self.data = ko.observableArray();

                self.fieldsValue = {
                    chassisNumber: ko.observable(),
                    carModel: ko.observable(''),
                    engineNumber: ko.observable(""),
                    plateNumber: ko.observable(),
                    vinNumber: ko.observable(),
                    eliteVehicle: ko.observable(),
                    vehicleType: ko.observable()
                };

//                self.historyAction = function (event) {
//                    lastBrand = event;
//                    if (event) {
//                        var brand = event.brand;

                self.historyAction = function (brand) {
                    if (brand) {
                        var filter = self.data().filter(e => e.Brand == brand);
                        if (filter.length > 0) {
                            Object.keys(filter).forEach(function (dataKey) {
                                for (var i in filter[dataKey]) {
                                    if (filter[dataKey][i] == 'null') {
                                        filter[dataKey][i] = app.label().notAvailable();
                                    }
                                }
                            });
                            self.dataprovider(filter);
                            self.dataStatus('dataAvailable');
//                            self.historyVisible(true);
                        } else {
                            self.dataStatus('noData');
                            self.dataprovider([]);
                        }
                    }

                };

                ko.computed(() => {
                    var data = app.vehicleData();

                    if (!data) {
                        self.dataStatus('loading');
                    } else {
                        if (data.length < 1) {
                            self.dataStatus('noData');
                        } else {
//                            self.dataStatus('dataAvailable');
                            self.data(data);
                            self.historyAction("Hyundai");


//                            var brands = app.vehicleData().filter((arr, index, self) =>
//                                index == self.findIndex((t) => (t.Brand === arr.Brand)));
//                            for (var i in brands) {
//                                self.jsonDataStr.push({id: i, brand: brands[i].Brand == "null" ? self.label().notAvailable() : brands[i].Brand});

                        }
                    }

                });

                self.connected = function () {
                    initTranslations();
                    app.loading(false);
//                    self.historyVisible(false);
                };

                function initTranslations() {
                    app.headerTitle(getTranslation('dashboard.vehicleHistory'));
                    self.label({
                        load: ko.observable(getTranslation("history.load")),
                        noDataText: ko.observable(getTranslation("history.noDataText")),
                        notAvailable: ko.observable(getTranslation("history.notAvailable")),
                        title: ko.observable(getTranslation("vehicleHistory.title")),
                        history: ko.observable(getTranslation("history.history")),
                        category: ko.observable(getTranslation("history.category")),
                        chooseCar: ko.observable(getTranslation("history.chooseCar")),
                        eliteVehicle: ko.observable(getTranslation("history.eliteVehicle")),
                        chassisNumber: ko.observable(getTranslation("history.chassisNumber")),
                        carModel: ko.observable(getTranslation("history.carModel")),
                        engineNumber: ko.observable(getTranslation("history.engineNumber")),
                        plateNumber: ko.observable(getTranslation("history.plateNumber")),
                        vinNumber: ko.observable(getTranslation("history.vinNumber")),
                        vehicleType: ko.observable(getTranslation("history.vehicleType"))
                    });
//                    self.historyAction(lastBrand);
                }

            }

            return historyContentViewModel;
        });