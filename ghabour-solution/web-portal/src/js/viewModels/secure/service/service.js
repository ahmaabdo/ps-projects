define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojarraydataprovider', 'ojs/ojpagingdataproviderview', 'ojs/ojbutton', 'ojs/ojinputtext',
    'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojselectcombobox', 'ojs/ojvalidationgroup', 'ojs/ojpagingcontrol'],
        function (oj, ko, app, services, commonhelper, ArrayDataProvider, PagingDataProviderView) {
            function serviceContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.name = ko.observable("");
                self.columns = ko.observable();
                self.avatarSize = ko.observable("xxs");
                self.isDisabled = ko.observable(true);
                self.selection = ko.observable();
                self.dataprovider = ko.observableArray();
                self.groupValid = ko.observable();
                self.shimmerCards = ko.observableArray(new Array(10));
                self.dataStatus = ko.observable("loading");
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.fieldsValue = {
                    rsaLocation: ko.observable(""),
                    firstValue: ko.observable(),
                    firstValueArr: ko.observableArray([]),
                    secondValue: ko.observable(),
                    secValueArr: ko.observableArray([]),
                    serviceDetail: ko.observable("")
                };

                self.fieldsOption = {
                    rsaRequired: ko.observable(false),
                    secValueRequired: ko.observable(false),
                    serviceDetailRequired: ko.observable(true)
                };

                ko.computed(() => {
                    if (app.userData()) {
                        self.dataStatus("noDataAvailable");
                        if (app.userData().serviceData) {
                            if (app.userData().serviceData.length > 0) {
                                self.dataprovider([]);
                                self.dataStatus("dataRetrieved");
                                setTimeout(() => {
                                    for (var i in app.userData().serviceData) {
                                        if (app.userData().serviceData[i].firstVal == "Road Assistance") {
                                            app.userData().serviceData[i].flagRsa = true;
                                            app.userData().serviceData[i].flagSec = false;
                                        } else {
                                            app.userData().serviceData[i].flagRsa = false;
                                            app.userData().serviceData[i].flagSec = true;
                                        }
                                        if (app.userData().serviceData[i].firstVal !== "Road Assistance" && !app.userData().serviceData[i].secondValue) {
                                            app.userData().serviceData[i].flagSec = false;
                                            app.userData().serviceData[i].flagRsa = false;
                                        }
                                        if (app.userData().serviceData[i].rsaLocation == "Not Available" || !app.userData().serviceData[i].rsaLocation) {
                                            app.userData().serviceData[i].flagRsa = false;
                                        }
                                    }
                                    self.dataprovider(app.userData().serviceData);
                                }, 0);
                            }
                        }
                    }
                });

                ko.computed(() => {
                    var data = app.serviceData();
                    self.fieldsValue.firstValueArr([]);
                    if (data) {
                        if (localStorage.getItem("selectedLanguage") == 'ar')
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupNameAr, dependsObj: data[i].dependsObj});
                        else
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupName, dependsObj: data[i].dependsObj});
                    }
                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                };

                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));

                self.selectServiceHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    var firstValSelected = self.fieldsValue.firstValueArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        self.fieldsOption.rsaRequired(firstValSelected.rsaRequired);
                        var dependsObj = firstValSelected.dependsObj;
                        self.fieldsValue.secValueArr([]);
                        self.fieldsOption.secValueRequired("");
                        if (dependsObj) {
                            self.fieldsOption.secValueRequired(true);
                            if (localStorage.getItem("selectedLanguage") == 'ar')
                                for (var i in dependsObj)
                                    self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupNameAr});
                            else
                                for (var i in dependsObj)
                                    self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupName});
                        } else {
                            self.fieldsOption.secValueRequired(false);
                        }
                    }
                };


                self.sendAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    app.loading(true);
                    self.dataStatus("loading");
                    var payload = {
                        "personId": 12,
                        "firstValue": self.fieldsValue.firstValue(),
                        "secValue": self.fieldsValue.secondValue(),
                        "rsaLocation": self.fieldsValue.rsaLocation(),
                        "detailsValue": self.fieldsValue.serviceDetail()
                    };
                    services.postGeneric("serviceService", JSON.stringify(payload)).then(data => {
                        app.loading(false);
                        if (data.status == 'done') {
                            self.fieldsValue.rsaLocation("");
                            self.fieldsValue.serviceDetail("");
                            app.createMessage("confirmation", self.label().success());
                            app.getUserData();
                        } else {
                            app.failCbFn();
                            self.dataStatus("noDataAvailable");
                        }
                    }, err => {
                        self.dataStatus("noDataAvailable");
                        app.failCbFn();
                    });
                };

                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));

                function initTranslations() {
                    app.headerTitle(getTranslation('dashboard.service'));
                    self.label({
                        success: ko.observable(getTranslation('service.success')),
                        placeholder: ko.observable(getTranslation("main.placeholder")),
                        status: ko.observable(getTranslation("main.status")),
                        noDataAvailableTxt: ko.observable(getTranslation("main.noDataAvailableTxt")),
                        notAvailable: ko.observable(getTranslation("history.notAvailable")),
                        upcomingService: ko.observable(getTranslation('service.upcomingService')),
                        upcomingText: ko.observable(getTranslation('service.upcomingText')),
                        pastService: ko.observable(getTranslation('service.pastService')),
                        pastText: ko.observable(getTranslation('service.pastText')),
                        title: ko.observable(getTranslation('service.title')),
                        serviceType: ko.observable(getTranslation('service.serviceType')),
                        rsaLocation: ko.observable(getTranslation('service.rsaLocation')),
                        rsaLocationHistory: ko.observable(getTranslation('service.rsaLocationHistory')),
                        secValueLbl: ko.observable(getTranslation('service.serviceCategory')),
                        serviceDetail: ko.observable(getTranslation("service.serviceDetail")),
                        send: ko.observable(getTranslation("complain.save")),
                        describeService: ko.observable(getTranslation("service.describeService")),
                        serviceHistory: ko.observable(getTranslation("service.serviceHistory")),
                        owner: ko.observable(getTranslation("history.owner")),
                        chassisNumber: ko.observable(getTranslation('maintenance.chassisNumber')),
                        serviceCenter: ko.observable(getTranslation('maintenance.serviceCenter')),
                        date: ko.observable(getTranslation('maintenance.date')),
                        chooseDetail: ko.observable(getTranslation('maintenance.chooseDetail'))
                    });
                }
            }

            return serviceContentViewModel;
        });
