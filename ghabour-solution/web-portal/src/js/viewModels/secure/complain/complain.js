define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojarraydataprovider',
    'ojs/ojpagingdataproviderview', 'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojinputtext',
    'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojselectcombobox', 'ojs/ojpagingcontrol', 'ojs/ojvalidationgroup'],
        function (oj, ko, app, services, commonhelper, ArrayDataProvider, PagingDataProviderView) {
            function complainContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.name = ko.observable("Mohammed");
                self.columns = ko.observable();
                self.avatarSize = ko.observable("xxs");
                self.isDisabled = ko.observable(true);
                self.selection = ko.observable();
                self.dataprovider = ko.observableArray();
                self.shimmerCards = ko.observableArray(new Array(10));
                self.dataStatus = ko.observable("loading");
                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));
                self.groupValid = ko.observable();
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.fieldsOption = {
                    secondValueDisplay: ko.observable(false),
                    secondValueRequired: ko.observable(false)
                };

                self.fieldsValue = {
                    firstValue: ko.observable(),
                    firstValueArr: ko.observableArray([]),
                    secondValue: ko.observable(),
                    secValueArr: ko.observableArray([]),
                    complaintLetter: ko.observable("")
                };

                ko.computed(() => {
                    self.dataStatus("noDataAvailable");
                    if (app.userData()) {
                        if (app.userData().complaintData) {
                            if (app.userData().complaintData.length > 0) {
                                self.dataprovider([]);
                                self.dataStatus("dataRetrieved");
                                setTimeout(() => {
                                    self.dataprovider(app.userData().complaintData);
                                }, 0);
                            }
                        }
                    }
                });

                ko.computed(() => {
                    var data = app.complainData();
                    //Get complain LOVs data
                    self.fieldsValue.firstValueArr([]);
                    if (data) {
                        if (localStorage.getItem("selectedLanguage") == 'ar')
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupNameAr, dependsObj: data[i].dependsObj});
                        else
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupName, dependsObj: data[i].dependsObj});
                    }
                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                };

                self.submitAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    app.loading(true);
                    self.dataStatus("loading");
                    var payload = {
                        "personId": 12,
                        "firstValue": self.fieldsValue.firstValue(),
                        "secValue": self.fieldsValue.secondValue(),
                        "detailsValue": self.fieldsValue.complaintLetter()
                    };

                    services.postGeneric("complainService", JSON.stringify(payload)).then(data => {
                        app.loading(false);
                        if (data.status == 'done') {
                            self.fieldsValue.complaintLetter("");
                            app.createMessage("confirmation", self.label().success());
                            //Refresh/Get all user complains here...
                            app.getUserData();
                        } else {
                            app.failCbFn();
                            self.dataStatus("noDataAvailable");
                        }
                    }, err => {
                        self.dataStatus("noDataAvailable");
                        app.failCbFn();
                    });
                };

                self.selectComplainHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    var firstValSelected = self.fieldsValue.firstValueArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        var dependsObj = firstValSelected.dependsObj;
                        self.fieldsValue.secValueArr([]);
                        self.fieldsOption.secondValueRequired("");
                        if (dependsObj) {
                            self.fieldsOption.secondValueRequired(true);
                            self.fieldsOption.secondValueDisplay(true);
                            if (localStorage.getItem("selectedLanguage") == 'ar')
                                for (var i in dependsObj)
                                    self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupNameAr});
                            else
                                for (var i in dependsObj)
                                    self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupName});
                        } else {
                            self.fieldsOption.secondValueRequired(false);
                            self.fieldsOption.secondValueDisplay(false);
                        }
                    }
                };

                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));

                function initTranslations() {
                    app.headerTitle(getTranslation('complain.title'));
                    self.label({
                        success: ko.observable(getTranslation('complain.success')),
                        placeholder: ko.observable(getTranslation("main.placeholder")),
                        status: ko.observable(getTranslation("main.status")),
                        noDataAvailableTxt: ko.observable(getTranslation("main.noDataAvailableTxt")),
                        notAvailable: ko.observable(getTranslation("history.notAvailable")),
                        upcomingHistory: ko.observable(getTranslation('complain.upcomingHistory')),
                        historyParag: ko.observable(getTranslation('complain.historyParag')),
                        pastParag: ko.observable(getTranslation('complain.pastParag')),
                        pastComplain: ko.observable(getTranslation('complain.pastComplain')),
                        complainType: ko.observable(getTranslation('complain.complainType')),
                        secValueLbl: ko.observable(getTranslation('complain.complaintCategory')),
                        complaintLetter: ko.observable(getTranslation('complain.complaintLetter')),
                        submit: ko.observable(getTranslation("main.submit")),
                        describeComplain: ko.observable(getTranslation("complain.describeComplain")),
                        complainHistory: ko.observable(getTranslation("complain.complainHistory")),
                        owner: ko.observable(getTranslation("history.owner")),
                        chassisNumber: ko.observable(getTranslation('maintenance.chassisNumber')),
                        serviceCenter: ko.observable(getTranslation('maintenance.serviceCenter')),
                        date: ko.observable(getTranslation('maintenance.date')),
                        chooseDetail: ko.observable(getTranslation('maintenance.chooseDetail'))
                    });


                }


            }

            return complainContentViewModel;
        });
