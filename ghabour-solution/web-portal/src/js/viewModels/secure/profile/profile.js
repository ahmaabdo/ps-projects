define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'ojs/ojvalidation-base', 'ojs/ojmessaging',
    'ojs/ojfilepicker', 'ojs/ojavatar', 'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojformlayout'
            , 'ojs/ojvalidationgroup'],
        function (oj, ko, app, services, ojvalbase, Message) {

            function profileContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.columns = ko.observable();
                self.avatarSize = ko.observable("xl");
                self.errorMsgEmail = ko.observable();
                self.errorMsgPhone = ko.observable();
                self.errorMsgPass = ko.observable();
                self.currentRawPassValue = ko.observable();
                self.currentPassValue = ko.observable();
                self.groupValid = ko.observable();
                var dataFiles = {};
                var profile;
                self.image = ko.observable();
                self.koArray = ko.observableArray([]);
                self.values = {
                    name: ko.observable(),
                    email: ko.observable(),
                    firstName: ko.observable(),
                    chassisNumber: ko.observable(),
                    lastName: ko.observable(),
                    phone: ko.observable(),
                    currentPass: ko.observable(),
                    newPass: ko.observable(),
                    confirmPass: ko.observable()
                };

                self.visibleForm = {
                    viewProfile: ko.observable(true),
                    editProfile: ko.observable(false),
                    editPassword: ko.observable(false)
                };
                self.emailValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
                                hint: self.label().emailHint(),
                                messageDetail: self.label().emailmessageDetail()
                            }
                        }];
                });
                self.validator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9 ]{3,}",
                                hint: self.label().nameHint(),
                                messageDetail: self.label().nameMessageDetail()
                            }
                        }];
                });
                self.validatorPass = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9 ]{8,}",
                                hint: self.label().passHint(),
                                messageDetail: self.label().passMessageDetail()
                            }
                        }];
                });
                self.mobileValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[0]+[1]+[0-9]{9,9}",
                                hint: self.label().phoneHint(),
                                messageDetail: self.label().phoneMessageDetail()
                            }
                        }];
                });
                self.addVehicleAction=function(){
                    oj.Router.rootInstance.go('addVehicle');
                };
                 self.emailChangedHandler= function(event){
                    if(event){
                    var email=event.detail.value;
                    console.log(email,"  ",profile.email)
                    var payload = {
                            "email": email
                        };
                        services.postGeneric("users/is-email-valid", payload).then(data => {
                            if (data.valid == "false" && email && email != profile.email) {
                                self.errorMsgEmail([{
                                        summary: self.label().emailValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } else {
                                self.errorMsgEmail([]);
                            }
                        }, app.failCbFn);
                    }
                };
                self.phoneChangedHandler = function (event) {
                    if (event) {
                        var phone = event.detail.value;
                        var payload = {
                            "phone": phone
                        };
                        services.postGeneric("users/is-phone-valid", payload).then(data => {
                            if (data.valid == "false" && phone && phone != profile.phone) {
                                self.errorMsgPhone([{
                                        summary: self.label().phoneValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } else {
                                self.errorMsgPhone([]);
                            }
                        }, app.failCbFn);
                    }
                };

                self.selectListener = function (event) {
                    var files = event.detail.files;
                    console.log(files);
                    if (files.length > 0) {
                        if (self.koArray().length == 1) {
                            self.koArray([]);
                        }
                        if((files[0].size/1024/1024) <= 2.5 && files[0].type.includes('image')){
                        for (var i = 0; i < files.length; i++) {
                            function getBase64(file) {
                                return new Promise((resolve, reject) => {
                                    const reader = new FileReader();
                                    reader.readAsDataURL(file);
                                    reader.onload = () => resolve(reader.result);
                                    reader.onerror = error => reject(error);
                                });
                            }
                            dataFiles.name = files[i].name;
                            dataFiles.id = i + 1;
                            getBase64(files[i]).then(function (data) {
                                dataFiles.data = (data);
                                self.attachmentId = self.attachmentId + 1;
                                dataFiles.id = (self.attachmentId);

                                self.koArray.push({
                                    "name": dataFiles.name,
                                    "SS_id": profile.id,
                                    "data": dataFiles.data


                                });

                                self.image(self.koArray()[0].data);
                            }
                            );
                        }
                }else{
                    app.createMessage("error", self.label().attachErr());
                }
                    }
                };
                self.editAction = function () {
                    self.visibleForm.viewProfile(false);
                    self.visibleForm.editProfile(true);
                };
                self.editFormAction = function () {
                    app.loading(true);
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    self.getSuccs = function (data) {
                        app.loading(false);
                        app.login(profile.username, profile.password);
                        self.visibleForm.viewProfile(true);
                        self.visibleForm.editProfile(false);
                    };

                    var payload = {
                        "id": profile.id,
                        "email": self.values.email(),
                        "firstName": self.values.firstName(),
                        "lastName": self.values.lastName(),
                        "phone": self.values.phone(),
                        "chassisNumber": self.values.chassisNumber()
                    };

                    services.postGeneric("attachment/editImg", self.koArray()).then(data => {
                        app.loading(false);
//                        app.getProfilePicture();
                    }, app.failCbFn);

                    services.postGeneric("users/editUserInfo", payload).then(self.getSuccs, app.failCbFn);

                };
                self.changePassAction = function () {
                    self.visibleForm.viewProfile(false);
                    self.visibleForm.editProfile(false);
                    self.visibleForm.editPassword(true);
                };
                self.reset = function () {
                    var tracker2 = document.getElementById("tracker2");
                    if (tracker2.valid != "valid") {
                        tracker2.showMessages();
                        tracker2.focusOn("@firstInvalidShown");
                        return;
                    }
                    if (self.values.newPass() == self.values.confirmPass()) {
                        app.loading(true);
                        self.getSuccs = function () {
                            app.loading(false);
                            console.log("pass");
                            self.visibleForm.viewProfile(true);
                            self.visibleForm.editProfile(false);
                            self.visibleForm.editPassword(false);
                        };

                        var payload = {
                            "id": profile.id,
                            "password": self.values.newPass()
                        };
                        services.postGeneric("users/editPass", payload).then(self.getSuccs, app.failCbFn);
                    }
                };
                self.backAction = function () {
                    if (self.visibleForm.viewProfile()) {
                        oj.Router.rootInstance.go('dashboard');
                    } else if (self.visibleForm.editProfile()) {
                        self.visibleForm.viewProfile(true);
                        self.visibleForm.editProfile(false);
                        self.visibleForm.editPassword(false);
                    } else {
                        self.visibleForm.viewProfile(true);
                        self.visibleForm.editProfile(false);
                        self.visibleForm.editPassword(false);
                    }
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                ko.computed(()=>{
                    self.passChangedHandler=function(){
                     if (self.values.newPass() == self.values.confirmPass()) {
                                self.errorMsgPass([]);
                            } else {
                                self.errorMsgPass([{
                                        summary: self.label().invalidPass(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } 
                };
                });
                ko.computed(()=>{
                     if (app.profileImg()) {
                        self.image(app.profileImg());
                    } else {
                        self.image('');
                    }
                });
                self.connected = function () {
                    app.headerVisible(true);
                    app.loading(false);
                    initTranslations();
                    var pass,confirm;
                    document.getElementById("text-input7").addEventListener('rawValueChanged', function (event) {
                        pass = event.detail.value;
                        self.values.newPass(pass);
                    });
                    document.getElementById("text-input8").addEventListener('rawValueChanged', function (event) {
                            confirm = event.detail.value;
                        self.values.confirmPass(confirm);
                    });
                    profile = JSON.parse(localStorage.getItem("personDetails"));
                    self.values.name(profile.firstName + " " + profile.lastName);
                    self.values.firstName(profile.firstName);
                    self.values.lastName(profile.lastName);
                    self.values.email(profile.email);
                    self.values.chassisNumber(profile.chassisNumber);
                    self.values.phone(profile.phone);
                    self.initials = ojvalbase.IntlConverterUtils.getInitials(self.values.firstName(), self.values.lastName());
                };

                function initTranslations() {
                    app.headerTitle(getTranslation('profile.title'));
                    self.label({
                        forgetPassword: ko.observable(getTranslation('main.forgetPassword')),
                        addVehicle: ko.observable(getTranslation('profile.addVehicle')),
                        vehicles: ko.observable(getTranslation('profile.vehicles')),
                        invalidPass: ko.observable(getTranslation('main.invalidPass')),
                        phoneHint: ko.observable(getTranslation('main.phoneHint')),
                        phoneMessageDetail: ko.observable(getTranslation('main.phoneMessageDetail')),
                        passHint: ko.observable(getTranslation('main.passHint')),
                        passMessageDetail: ko.observable(getTranslation('main.passMessageDetail')),
                        attachErr:ko.observable(getTranslation('main.attachErr')),
                        phoneValid: ko.observable(getTranslation('main.phoneValid')),
                        emailValid: ko.observable(getTranslation('main.emailValid')),
                        placeholder: ko.observable(getTranslation('profile.placeholder')),
                        nameHint: ko.observable(getTranslation('main.usernameHint')),
                        nameMessageDetail: ko.observable(getTranslation('main.usernameMessageDetail')),
                        emailHint: ko.observable(getTranslation('main.emailHint')),
                        emailmessageDetail: ko.observable(getTranslation('main.emailmessageDetail')),
                        name: ko.observable(getTranslation('profile.name')),
                        email: ko.observable(getTranslation('profile.email')),
                        chassisNumber: ko.observable(getTranslation('profile.chassisNumber')),
                        phone: ko.observable(getTranslation('profile.phone')),
                        edit: ko.observable(getTranslation('profile.edit')),
                        save: ko.observable(getTranslation('main.save')),
                        changePass: ko.observable(getTranslation('profile.changePass')),
                        back: ko.observable(getTranslation('profile.back')),
                        firstName: ko.observable(getTranslation('profile.firstName')),
                        lastName: ko.observable(getTranslation('profile.lastName')),
                        currentPass: ko.observable(getTranslation('profile.currentPass')),
                        newPass: ko.observable(getTranslation('profile.newPass')),
                        confirmPass: ko.observable(getTranslation('profile.confirmPass')),
                        image: ko.observable(getTranslation('profile.image'))
                    });
                }
            }

            return profileContentViewModel;
        });
