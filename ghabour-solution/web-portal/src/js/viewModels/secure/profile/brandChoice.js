define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojbutton', 'ojs/ojlabel', 'ojs/ojcheckboxset', 'ojs/ojbutton'],
        function (oj, ko, app, services, commonhelper) {

            function brandChoiceContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.rememberChoice = ko.observableArray([]);
                self.confirmBtnStatus = ko.observable(true);
                self.selectedBrand = ko.observable("");
                self.carsBrandArr = ko.observable(app.globalCarsBrandArr());

                self.carBrandsClickAction = function (event) {
                    self.selectedBrand(event.brandName);
                };

                ko.computed(() => {
                    if (self.selectedBrand())
                        self.confirmBtnStatus(false);
                    else
                        self.confirmBtnStatus(true);
                });

                self.confirmChoice = function () {
                    if (self.rememberChoice()[0]) {
                        app.loading(true);
                        var payload = {
                            "id": app.personDetails().id,
                            "defaultBrand": self.selectedBrand()
                        };

                        services.postGeneric("users/editDefaultBrand", payload).then(() => {
                            app.brandThemeConfiguration(self.selectedBrand());
                            oj.Router.rootInstance.go('dashboard');

                            //Update user data in local storage
                            var personDetails = JSON.parse(localStorage.getItem("personDetails"));
                            personDetails.defaultBrand = self.selectedBrand();
                            localStorage.setItem("personDetails", JSON.stringify(personDetails));
                            app.personDetails().defaultBrand = self.selectedBrand();

                            app.loading(false);
                        }, app.failCbFn);
                    } else {
                        app.brandThemeConfiguration(self.selectedBrand());
                        oj.Router.rootInstance.go('dashboard');
                    }

                };

                self.skipAction = function () {
                    app.loading(true);

                    var payload = {
                        "id": app.personDetails().id,
                        "defaultBrand": "Ghabbour"
                    };

                    services.postGeneric("users/editDefaultBrand", payload).then(() => {

                        app.brandThemeConfiguration("Ghabbour");
                        oj.Router.rootInstance.go('dashboard');
                        //Update user data in local storage
                        var personDetails = JSON.parse(localStorage.getItem("personDetails"));
                        personDetails.defaultBrand = "Ghabbour";
                        localStorage.setItem("personDetails", JSON.stringify(personDetails));
                        app.personDetails().defaultBrand = self.selectedBrand();
                        
                        app.loading(false);
                    }, app.failCbFn);
                };

                self.connected = function () {
                    self.confirmBtnStatus(true);
                    initTranslations();
                    app.loading(false);
                };

                function initTranslations() {
                    app.headerVisible(true);
                    app.headerTitle(getTranslation(''));
                    self.label ({
                        chooseCar: ko.observable(getTranslation("vehicleHistory.chooseCar")),
                        confirmChoiceLbl: ko.observable(getTranslation("resetPassword.confirm")),
                        rememberLbl: ko.observable(getTranslation("main.rememberLbl")),
                        skipActionLbl: ko.observable(app.label().skipActionLbl())
                    });
                }
            }

            return brandChoiceContentViewModel;
        });
