define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojarraydataprovider',
    'ojs/ojpagingdataproviderview', 'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojinputtext',
    'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojselectcombobox', 'ojs/ojpagingcontrol', 'ojs/ojvalidationgroup'],
        function (oj, ko, app, services, commonhelper, ArrayDataProvider, PagingDataProviderView) {
            function TestDriveContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.name = ko.observable("Mohammed");
                self.columns = ko.observable();
                self.avatarSize = ko.observable("xxs");
                self.isDisabled = ko.observable(true);
                self.selection = ko.observable();
                self.dataprovider = ko.observableArray();
                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));
                self.groupValid = ko.observable();
                self.testDriveData = ko.observable();
                self.showroomData = ko.observable();
                self.brandData = ko.observable();
                self.modelData = ko.observable();
                self.dateData = ko.observable();
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.fieldsValue = {
                    showroom: ko.observable(),
                    brand: ko.observable(),
                    model: ko.observable(),
                    date: ko.observable(),
                    time: ko.observable(),
                    showroomArr: ko.observableArray([]),
                    brandArr: ko.observableArray([]),
                    modelArr: ko.observableArray([]),
                    dateArr: ko.observableArray([]),
                    timeArr: ko.observableArray([])
                };

//                ko.computed(p => {
//                    if (app.userData()) {
//                        self.dataprovider([]);
//                        setTimeout(p => {
//                            self.dataprovider(app.userData().testDriveData);
//                        }, 0);
//                    }
//                });

                ko.computed(() => {
                    self.fieldsValue.showroomArr([]);
                    if (app.testDriveData()) {
                        self.testDriveData(app.testDriveData());
                        self.uniqueShowroom = ko.computed(function () {
                            var sortedItems = app.testDriveData().concat().sort(function (left, right) {
                                return left.ShowRoom == right.ShowRoom ? 0 : (left.ShowRoom < right.ShowRoom ? -1 : 1)
                            });

                            var type;

                            for (var i = 0; i < sortedItems.length; i++) {
                                if (!type || type != sortedItems[i].ShowRoom) {
                                    type = sortedItems[i].ShowRoom;
                                } else {
                                    sortedItems.splice(i, 1);
                                    i--;
                                }
                            }
                            return sortedItems;
                        });
                        for (var i in self.uniqueShowroom()) {
                            self.fieldsValue.showroomArr.push({value: self.uniqueShowroom()[i].ShowRoom, label: self.uniqueShowroom()[i].ShowRoom, dependsObj: self.uniqueShowroom()[i].Brand});

                        }
                    }
                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    //Get testDrive LOVs data
                };

                self.submitAction = function () {
                };

                self.showroomHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    self.showroomData(self.testDriveData().filter(e => e.ShowRoom == selectedVal));
                    var firstValSelected = self.fieldsValue.showroomArr().find(p => p.value == selectedVal);
                    self.fieldsValue.brandArr([]);
                    if (firstValSelected) {
                        if (self.showroomData()) {
                            self.uniqueBrand = ko.computed(function () {
                                var sortedItems = self.showroomData().concat().sort(function (left, right) {
                                    return left.Brand == right.Brand ? 0 : (left.Brand < right.Brand ? -1 : 1)
                                });

                                var type;

                                for (var i = 0; i < sortedItems.length; i++) {
                                    if (!type || type != sortedItems[i].Brand) {
                                        type = sortedItems[i].Brand;
                                    } else {
                                        sortedItems.splice(i, 1);
                                        i--;
                                    }
                                }
                                return sortedItems;
                            });
                            for (var i in self.uniqueBrand())
                                self.fieldsValue.brandArr.push({value: self.uniqueBrand()[i].Brand, label: self.uniqueBrand()[i].Brand});
                        }
                    }
                };

                self.brandHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    self.brandData(self.showroomData().filter(e => e.Brand == selectedVal));
                    self.fieldsValue.modelArr([]);
                    var firstValSelected = self.fieldsValue.brandArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        if (self.brandData()) {
                            self.uniqueModel = ko.computed(function () {
                                var sortedItems = self.brandData().concat().sort(function (left, right) {
                                    return left.Model == right.Model ? 0 : (left.Model < right.Model ? -1 : 1)
                                });

                                var type;

                                for (var i = 0; i < sortedItems.length; i++) {
                                    if (!type || type != sortedItems[i].Model) {
                                        type = sortedItems[i].Model;
                                    } else {
                                        sortedItems.splice(i, 1);
                                        i--;
                                    }
                                }
                                return sortedItems;
                            });
                            for (var i in self.uniqueModel())
                                self.fieldsValue.modelArr.push({value: self.uniqueModel()[i].Model, label: self.uniqueModel()[i].Model});
                        }
                    }
                };

                self.modelHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    self.modelData(self.brandData().filter(e => e.Model == selectedVal));
                    self.fieldsValue.dateArr([]);
                    var firstValSelected = self.fieldsValue.modelArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        if (self.modelData()) {
                            self.uniqueDate = ko.computed(function () {
                                var sortedItems = self.modelData().concat().sort(function (left, right) {
                                    return left.Date == right.Date ? 0 : (left.Date < right.Date ? -1 : 1)
                                });

                                var type;

                                for (var i = 0; i < sortedItems.length; i++) {
                                    if (!type || type != sortedItems[i].Date) {
                                        type = sortedItems[i].Date;
                                    } else {
                                        sortedItems.splice(i, 1);
                                        i--;
                                    }
                                }
                                return sortedItems;
                            });
                            for (var i in self.uniqueDate())
                                self.fieldsValue.dateArr.push({value: self.uniqueDate()[i].Date, label: self.uniqueDate()[i].Date});
                        }
                    }
                };
                self.dateHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    self.dateData(self.modelData().filter(e => e.Date == selectedVal));
                    self.fieldsValue.timeArr([]);
                    var firstValSelected = self.fieldsValue.dateArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        if (self.dateData()) {
                            self.uniqueTime = ko.computed(function () {
                                var sortedItems = self.dateData().concat().sort(function (left, right) {
                                    return left.Time == right.Time ? 0 : (left.Time < right.Time ? -1 : 1)
                                });

                                var type;

                                for (var i = 0; i < sortedItems.length; i++) {
                                    if (!type || type != sortedItems[i].Time) {
                                        type = sortedItems[i].Time;
                                    } else {
                                        sortedItems.splice(i, 1);
                                        i--;
                                    }
                                }
                                return sortedItems;
                            });
                            for (var i in self.uniqueTime())
                                self.fieldsValue.timeArr.push({value: self.uniqueTime()[i].Time, label: self.uniqueTime()[i].Time});
                        }
                    }
                };


                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));

                function initTranslations() {
                    app.headerTitle(getTranslation('dashboard.testDrive'));
                    self.label({
                        showroom: ko.observable(getTranslation('testDrive.showroom')),
                        brand: ko.observable(getTranslation('testDrive.brand')),
                        model: ko.observable(getTranslation('products.model')),
                        time: ko.observable(getTranslation('testDrive.time')),
                        submit: ko.observable(getTranslation("main.submit")),
                        describeComplain: ko.observable(getTranslation("complain.describeComplain")),
                        complainHistory: ko.observable(getTranslation("complain.complainHistory")),
                        date: ko.observable(getTranslation('maintenance.date'))
                    });


                }


            }

            return TestDriveContentViewModel;
        });
