define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'hammerjs', 'ojs/ojarraydataprovider', 'ojs/ojpagingdataproviderview', 'ojs/ojarraydataprovider',
    'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojinputtext', 'ojs/ojlabel',
    'ojs/ojformlayout', 'ojs/ojselectcombobox', 'ojs/ojvalidationgroup', 'ojs/ojpagingcontrol',
    'ojs/ojoffcanvas', 'ojs/ojarraydataprovider', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojavatar', 'ojs/ojnavigationlist'],
        function (oj, ko, app, services, commonhelper, Hammer, ArrayDataProvider, PagingDataProviderView) {
            function inquiryContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.name = ko.observable("Mohammed");
                self.columns = ko.observable();
                self.isDisabled = ko.observable(true);
                self.selection = ko.observable();
                self.dataprovider = ko.observableArray();
                self.groupValid = ko.observable();
                self.shimmerCards = ko.observableArray(new Array(10));
                self.dataStatus = ko.observable("loading");
                self.pcRequired = ko.observable(false);
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.pcOrBajajVisible = ko.observable(false);

                self.fieldsValue = {
                    inquiryDetail: ko.observable(""),
                    firstValue: ko.observable(''),
                    firstValueArr: ko.observableArray([]),
                    secondValue: ko.observable(''),
                    secValueArr: ko.observableArray([])
                };

                ko.computed(() => {
                    self.dataStatus("noDataAvailable");
                    if (app.userData()) {
                        if (app.userData().inquiryData) {
                            if (app.userData().inquiryData.length > 0) {
                                self.dataprovider([]);
                                self.dataStatus("dataRetrieved");
                                setTimeout(() => {
                                    self.dataprovider(app.userData().inquiryData);
                                }, 0);
                            }
                        }
                    }
                });

                ko.computed(() => {
                    var data = app.inquiryData();
                    self.fieldsValue.firstValueArr([]);
                    if (data) {
                        if (localStorage.getItem("selectedLanguage") == 'ar')
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupNameAr, dependsObj: data[i].dependsObj});
                        else
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupName, dependsObj: data[i].dependsObj});
                    }
                });


                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                };

                self.sendAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    app.loading(true);
                    self.dataStatus("loading");
                    var payload = {
                        "personId": 12,
                        "firstValue": self.fieldsValue.firstValue(),
                        "secValue": self.fieldsValue.secondValue(),
                        "detailsValue": self.fieldsValue.inquiryDetail()
                    };

                    services.postGeneric("inquiryService", JSON.stringify(payload)).then(data => {
                        app.loading(false);
                        if (data.status == 'done') {
                            self.fieldsValue.inquiryDetail("");
                            app.createMessage("confirmation", self.label().success());
                            app.getUserData();
                        } else {
                            app.failCbFn();
                            self.dataStatus("noDataAvailable");
                        }
                    }, err => {
                        self.dataStatus("noDataAvailable");
                        app.failCbFn();
                    });
                };

                self.selectInquiryHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    var firstValSelected = self.fieldsValue.firstValueArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        var dependsObj = firstValSelected.dependsObj;
                        self.fieldsValue.secValueArr([]);
                        if (dependsObj) {
                            self.pcRequired(true);
                            self.pcOrBajajVisible(true);
                            for (var i in dependsObj)
                                self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupName});
                        } else {
                            self.pcRequired(false);
                            self.pcOrBajajVisible(false);
                        }
                    }
                };

                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));

                function initTranslations() {
                    app.headerTitle(getTranslation('inquiry.title'));
                    self.label({
                        success: ko.observable(getTranslation('inquiry.success')),
                        placeholder: ko.observable(getTranslation("main.placeholder")),
                        status: ko.observable(getTranslation("main.status")),
                        noDataAvailableTxt: ko.observable(getTranslation("main.noDataAvailableTxt")),
                        notAvailable: ko.observable(getTranslation("history.notAvailable")),
                        upcomingInquiry: ko.observable(getTranslation('inquiry.upcomingInquiry')),
                        upcomingText: ko.observable(getTranslation('inquiry.upcomingText')),
                        pastInquiry: ko.observable(getTranslation('inquiry.pastInquiry')),
                        pastText: ko.observable(getTranslation("inquiry.pastText")),
                        inquiryType: ko.observable(getTranslation('inquiry.inquiryType')),
                        pcOrBajaj: ko.observable(getTranslation('inquiry.pcOrBajaj')),
                        inquiryDetail: ko.observable(getTranslation("inquiry.inquiryDetail")),
                        send: ko.observable(getTranslation("complain.save")),
                        describeInquiry: ko.observable(getTranslation("inquiry.describeInquiry")),
                        inquiryHistory: ko.observable(getTranslation("inquiry.inquiryHistory")),
                        owner: ko.observable(getTranslation("history.owner")),
                        chassisNumber: ko.observable(getTranslation('maintenance.chassisNumber')),
                        serviceCenter: ko.observable(getTranslation('maintenance.serviceCenter')),
                        date: ko.observable(getTranslation('maintenance.date')),
                        chooseDetail: ko.observable(getTranslation('maintenance.chooseDetail'))
                    });
                }
            }

            return inquiryContentViewModel;
        });
