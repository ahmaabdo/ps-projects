define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojfilmstrip'],
        function (oj, ko, app, services) {
            function dashboardContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.image = ko.observable();
                self.label = ko.observable();
                self.carsModelArr = ko.observableArray([]);
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.routeToPage = function (page) {
                    if (app.userLoggedIn())
                        oj.Router.rootInstance.go(page);
                    else
                        oj.Router.rootInstance.go('logIn');
                };
                self.routeToPageVehicle = function (page) {
                    if (!app.userLoggedIn()) {
                        oj.Router.rootInstance.go('logIn');
                    } else {
                        if (app.vehicleFlag() == 'T' && app.phoneFlag()=='T') {
                            oj.Router.rootInstance.go(page);
                        } else if(app.vehicleFlag() !== 'T'){
                            oj.Router.rootInstance.go('addVehicle');
                        }else if(app.phoneFlag()!=='T'){
                            oj.Router.rootInstance.go('phoneScreen');
                        }
                    }
                };

                self.newproduct = function () {
                    oj.Router.rootInstance.go('products');
                };

                self.location = function () {
                    oj.Router.rootInstance.go('ourLocation');
                };
                self.carModelClickAction=function(event){
                    if (event.Name_c && app.productData()) {
                        oj.Router.rootInstance.store(app.productData().Products.filter(e => e.Model_c == event.Name_c));
                        oj.Router.rootInstance.go('newproduct');
                        if (app.productData().Products.filter(e => e.Model_c == event.Name_c).length == 0) {
                            app.createMessage("error", "No data available");
                            self.dataStatus("noDataAvailable");
                        }
                    }
                };
                function arrayBufferToBase64(buffer) {
                    return new Promise((resolve, reject) => {
                        const reader = new FileReader();
                        var blob = new Blob([buffer], {type: "image/png"});
                        reader.readAsDataURL(blob);
                        reader.onload = () => resolve(reader.result.substr(reader.result.indexOf(',') + 1));
                        reader.onerror = error => reject(error);
                    });
                }
                var dataLoaded = false;
                ko.computed(() => {
                    if (app.productData() && !dataLoaded) {
                        dataLoaded = true;
                        self.carsModelArr([]);
                        for (var x = 0; x < app.productData().Models.length; x++) {
                            if (app.productData().Models[x].Brand_c == "CHERY") {
                                app.productData().Models[x].img = "css/images/car_placeholder.png";
                                self.carsModelArr.push(app.productData().Models[x]);
                            }
                        }
                        console.log(self.carsModelArr());
                    } 
                });
                ko.computed(()=>{
                   if (app.modelData()) {
                        console.log("modelData")
                        console.log(app.modelData());
                        var data = self.carsModelArr();
                        data.forEach(e => {
                            if (app.modelData().find(x => x.model == e.Name_c)) {
                                arrayBufferToBase64(new Uint8Array(app.modelData().find(x => x.model == e.Name_c).image)).then(imageData => {
                                    e.img = imageData.replace("data/image/png/base64/", "data:image/png;base64,");
                                });
                            }
                        });
                        self.carsModelArr(data);
                        console.log(self.carsModelArr());
                    }
                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                };

                self.transitionCompleted = function () {
                    if (!app.isDashboardState()) {
                        setTimeout(() => {
                            $('html, body').animate({scrollTop: $(document).height()}, 'slow');
                        }, 150);
                        app.isDashboardState(true);
                    }
                };

                function initTranslations() {
                    app.headerTitle(getTranslation('main.dashboard'));
                    self.label({
                        shoppingTool: ko.observable(getTranslation('dashboard.shoppingTool')),
                        findDealerInPlace: ko.observable(getTranslation('dashboard.findDealerInPlace')),
                        findDealer: ko.observable(getTranslation('dashboard.findDealer')),
                        requestTestDrive: ko.observable(getTranslation("dashboard.requestTestDrive")),
                        elentra: ko.observable(getTranslation("dashboard.elentra")),
                        elantraParag: ko.observable(getTranslation("dashboard.elantraParag")),
                        learnMore: ko.observable(getTranslation("dashboard.learnMore")),
                        models: ko.observable(getTranslation("dashboard.models")),
                        tucson: ko.observable(getTranslation("dashboard.tucson")),
                        creta: ko.observable(getTranslation("dashboard.creta")),
                        i10: ko.observable(getTranslation("dashboard.i10")),
                        newTucsan: ko.observable(getTranslation("dashboard.newTucsan")),
                        carDetail: ko.observable(getTranslation("dashboard.carDetail")),
                        carDetail2: ko.observable(getTranslation("dashboard.carDetail2")),
                        location: ko.observable(getTranslation('main.location')),
                        products: ko.observable(getTranslation('dashboard.products')),
                        maintenance: ko.observable(getTranslation('dashboard.maintenance')),
                        complain: ko.observable(getTranslation("dashboard.complain")),
                        inquiry: ko.observable(getTranslation("dashboard.inquiry")),
                        service: ko.observable(getTranslation("dashboard.service")),
                        vehicleHistory: ko.observable(getTranslation("dashboard.vehicleHistory")),
                        promotions: ko.observable(getTranslation("dashboard.promotions")),
                        testDrive: ko.observable(getTranslation("dashboard.testDrive")),
                        reservationPayment: ko.observable(getTranslation("dashboard.reservationPayment")),
                        fqa: ko.observable(getTranslation("main.faq")),
                        help: ko.observable(getTranslation("dashboard.help")),
                        chooseCategory: ko.observable(getTranslation("dashboard.chooseCategory"))
                    });
                }

            }
            return dashboardContentViewModel;
        });
