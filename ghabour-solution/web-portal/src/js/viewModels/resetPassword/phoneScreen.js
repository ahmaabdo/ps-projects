define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmessaging',
    'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojvalidationgroup'
], function (oj, ko, app, services, commonhelper, Message) {
    function phoneScreenContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
        self.columns = ko.observable();
        self.label = ko.observable();
        self.errorMsgPhone = ko.observable();
        self.errorMsgPass = ko.observable();
        self.groupValid = ko.observable();
        self.groupValid = ko.observable();
//        var retrieved = oj.Router.rootInstance.retrieve();
        var retrieved=JSON.parse(localStorage.getItem("personDetails"));
        self.fields = {
            phone: ko.observable(),
            code: ko.observable()
        };

        self.visibleForm = {
            emailForm: ko.observable(true),
            codeForm: ko.observable(false)
        };
        self.mobileValidator = ko.pureComputed(function () {
            return [{
                    type: 'regExp',
                    options: {
                        pattern: "[0]+[1]+[0-9]{9,9}",
                        hint: self.label().phoneHint(),
                        messageDetail: self.label().phoneMessageDetail()
                    }
                }];
        });

        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });
        self.confirmEmail = function () {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid") {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            self.visibleForm.emailForm(false);
            self.visibleForm.codeForm(true);
        };
        self.code = function () {
            //check if written code is true then continue
            var payload = {
                "phone": self.fields.phone(),
                "id": retrieved.id,
                "firstName": retrieved.firstName,
                "lastName": retrieved.lastName
            };
            services.postGeneric("users/updatePhoneGoogle", payload).then(data => {
                console.log(data);
                app.createMessage("confirmation", "Phone added Successfully!!");
                app.login(retrieved.username.toLowerCase(), retrieved.password);
            }, app.failCbFn);
        };

        self.connected = function () {
            app.headerVisible(false);
            app.loading(false);
            initTranslations();
            console.log(retrieved)
        };

        function initTranslations() {
            self.label({
                phoneResetValid: ko.observable(getTranslation('main.phoneResetValid')),
                phoneHint: ko.observable(getTranslation('main.phoneHint')),
                phoneMessageDetail: ko.observable(getTranslation('main.phoneMessageDetail')),
                placeholderPhone: ko.observable(getTranslation('main.placeholderPhone')),
                phone: ko.observable(getTranslation('profile.phone')),
                titleEmail: ko.observable(getTranslation('title.phoneScreenTitle')),
                titleCode: ko.observable(getTranslation('resetPassword.titleCode')),
                confirm: ko.observable(getTranslation('resetPassword.confirm')),
                codeLbl: ko.observable(getTranslation('resetPassword.codeLbl')),
                code: ko.observable(getTranslation('resetPassword.code'))
            });
        }
    }

    return phoneScreenContentViewModel;
});
