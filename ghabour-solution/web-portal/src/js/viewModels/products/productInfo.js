define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper'
            , 'ojs/ojlabel', 'ojs/ojlistview', 'ojs/ojlabel', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojfilmstrip'
], function (oj, ko, app, services, commonhelper) {
    function productInfoContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
        self.label = ko.observable();
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });
        self.carName = ko.observable();
        self.koArray=ko.observableArray([]);
        self.productsRetrieved = ko.observableArray([]);

        self.connected = function () {
            app.loading(false);
            app.headerVisible(true);
            initTranslations();
            self.koArray([]);
            var image=localStorage.getItem("moveData");
            var imageData=JSON.parse(image);
            var imageDataImage=imageData.image;
            if(imageDataImage.length == 0){
                self.koArray([]);
            }else{
                self.koArray.push({image:imageData.image});
            }
            self.productsRetrieved(imageData);            
        };
        self.backAction = function () {
            oj.Router.rootInstance.go('products');
        };

        function initTranslations() {
            app.headerTitle(getTranslation('products.productInfo'));
            self.label ({
                requestTestDrive: ko.observable(getTranslation("dashboard.requestTestDrive")),
                reservationPayment: ko.observable(getTranslation("dashboard.reservationPayment")),
                keylessEntry:ko.observable(getTranslation("productInfo.keylessEntry")),
                smartKey:ko.observable(getTranslation("productInfo.smartKey")),
                remoteEngineStart:ko.observable(getTranslation("productInfo.remoteEngineStart")),
                superVisionCluster:ko.observable(getTranslation("productInfo.superVisionCluster")),
                manualLeveling:ko.observable(getTranslation("productInfo.manualLeveling")),
                powerOutlet:ko.observable(getTranslation("productInfo.powerOutlet")),
                dms:ko.observable(getTranslation("productInfo.dms")),
                cruiseControl:ko.observable(getTranslation("productInfo.cruiseControl")),
                tiltAndTelSteerWheel:ko.observable(getTranslation("productInfo.tiltAndTelSteerWheel")),
                rearPrivClass:ko.observable(getTranslation("productInfo.rearPrivClass")),
                fullSolarGlass:ko.observable(getTranslation("productInfo.fullSolarGlass")),
                outFoldMirror:ko.observable(getTranslation("productInfo.outFoldMirror")),
                outMirror:ko.observable(getTranslation("productInfo.outMirror")),
                outDoorHandler:ko.observable(getTranslation("productInfo.outDoorHandler")),
                manAc:ko.observable(getTranslation("productInfo.manAc")),
                autoAc:ko.observable(getTranslation("productInfo.autoAc")),
                rearAirVent:ko.observable(getTranslation("productInfo.rearAirVent")),
                glovCoolBox:ko.observable(getTranslation("productInfo.glovCoolBox")),
                panoSlidRoof:ko.observable(getTranslation("productInfo.panoSlidRoof")),
                autoLigthSensor:ko.observable(getTranslation("productInfo.autoLigthSensor")),
                frontConvFogLamp:ko.observable(getTranslation("productInfo.frontConvFogLamp")),
                ledPosLight:ko.observable(getTranslation("productInfo.ledPosLight")),
                dedicatedDrl:ko.observable(getTranslation("productInfo.dedicatedDrl")),
                rearLedCompLamp:ko.observable(getTranslation("productInfo.rearLedCompLamp")),
                premClothSeats:ko.observable(getTranslation("productInfo.premClothSeats")),
                leatherSeatCover:ko.observable(getTranslation("productInfo.leatherSeatCover")),
                leatherSeatCoverRed:ko.observable(getTranslation("productInfo.leatherSeatCoverRed")),
                leatherWrapedWheel:ko.observable(getTranslation("productInfo.leatherWrapedWheel")),
                manAdjustDriverSeat:ko.observable(getTranslation("productInfo.manAdjustDriverSeat")),
                powAdjustDriverSeat:ko.observable(getTranslation("productInfo.powAdjustDriverSeat")),
                passPowerSeat:ko.observable(getTranslation("productInfo.passPowerSeat")),
                frontVent:ko.observable(getTranslation("productInfo.frontVent")),
                rearArmRest:ko.observable(getTranslation("productInfo.rearArmRest")),
                rearSpliteSeat:ko.observable(getTranslation("productInfo.rearSpliteSeat")),
                rearAdjustableHeadRest:ko.observable(getTranslation("productInfo.rearAdjustableHeadRest")),
                chromedInAndOutDoorHandler:ko.observable(getTranslation("productInfo.chromedInAndOutDoorHandler")),
                doorBeltLine:ko.observable(getTranslation("productInfo.doorBeltLine")),
                roofRack:ko.observable(getTranslation("productInfo.roofRack")),
                rearSpoiler:ko.observable(getTranslation("productInfo.rearSpoiler")),
                chromedFrontGrill:ko.observable(getTranslation("productInfo.chromedFrontGrill")),
                autoDefogSys:ko.observable(getTranslation("productInfo.autoDefogSys")),
                ledHeadLamps:ko.observable(getTranslation("productInfo.ledHeadLamps")),
                headLampWashers:ko.observable(getTranslation("productInfo.headLampWashers")),
                powerTailGate:ko.observable(getTranslation("productInfo.powerTailGate")),
                autoRainSensor:ko.observable(getTranslation("productInfo.autoRainSensor")),
                engineImmobilizer:ko.observable(getTranslation("productInfo.engineImmobilizer")),
                speedSensing:ko.observable(getTranslation("productInfo.speedSensing")),
                dualFrontAirBag:ko.observable(getTranslation("productInfo.dualFrontAirBag")),
                sideAndCurtainAirBag:ko.observable(getTranslation("productInfo.sideAndCurtainAirBag")),
                abs:ko.observable(getTranslation("productInfo.abs")),
                ebd:ko.observable(getTranslation("productInfo.ebd")),
                ess:ko.observable(getTranslation("productInfo.ess")),
                esc:ko.observable(getTranslation("productInfo.esc")),
                vsm:ko.observable(getTranslation("productInfo.vsm")),
                bas:ko.observable(getTranslation("productInfo.bas")),
                hac:ko.observable(getTranslation("productInfo.hac")),
                dbc:ko.observable(getTranslation("productInfo.dbc")),
                epb:ko.observable(getTranslation("productInfo.epb")),
                seatBeltReminder:ko.observable(getTranslation("productInfo.seatBeltReminder")),
                insideEcmMirror:ko.observable(getTranslation("productInfo.insideEcmMirror")),
                rearParkingAssist:ko.observable(getTranslation("productInfo.rearParkingAssist")),
                frontAndRearParkingSensor:ko.observable(getTranslation("productInfo.frontAndRearParkingSensor")),
                blindSpotDetectSys:ko.observable(getTranslation("productInfo.blindSpotDetectSys")),
                rearViewCamera:ko.observable(getTranslation("productInfo.rearViewCamera")),
                allAroundViewMon:ko.observable(getTranslation("productInfo.allAroundViewMon")),
                multiFuncSteerWheel:ko.observable(getTranslation("productInfo.multiFuncSteerWheel")),
                radio5:ko.observable(getTranslation("productInfo.radio5")),
                audioTouchscreen:ko.observable(getTranslation("productInfo.audioTouchscreen")),
                bluetooth:ko.observable(getTranslation("productInfo.bluetooth")),
                noOfSpeakers:ko.observable(getTranslation("productInfo.noOfSpeakers")),
                frontUsb:ko.observable(getTranslation("productInfo.frontUsb")),
                wirlessCharger:ko.observable(getTranslation("productInfo.wirlessCharger")),
                rearUsbInput:ko.observable(getTranslation("productInfo.rearUsbInput")),
                mdbs:ko.observable(getTranslation("productInfo.mdbs")),
                alloyWheels16:ko.observable(getTranslation("productInfo.alloyWheels16")),
                alloyWheels17:ko.observable(getTranslation("productInfo.alloyWheels17")),
                alloyWheels19:ko.observable(getTranslation("productInfo.alloyWheels19")),
                tirePressureMonSys:ko.observable(getTranslation("productInfo.tirePressureMonSys")),
                fullSpareAlloyWheel:ko.observable(getTranslation("productInfo.fullSpareAlloyWheel")),
                speedDualClutchTrans:ko.observable(getTranslation("productInfo.speedDualClutchTrans")),
                speedAutoTrans:ko.observable(getTranslation("productInfo.speedAutoTrans")),
                fuelTankCapacity:ko.observable(getTranslation("productInfo.fuelTankCapacity")),
                cylinder:ko.observable(getTranslation("productInfo.cylinder")),
                valveSys:ko.observable(getTranslation("productInfo.valveSys")),
                maxTorque:ko.observable(getTranslation("productInfo.maxTorque")),
                maxPower:ko.observable(getTranslation("productInfo.maxPower")),
                enginCapasity:ko.observable(getTranslation("productInfo.enginCapasity")),
                rear:ko.observable(getTranslation("productInfo.rear")),
                front:ko.observable(getTranslation("productInfo.front")),
                grossVehicleWeight:ko.observable(getTranslation("productInfo.grossVehicleWeight")),
                curbWeigth:ko.observable(getTranslation("productInfo.curbWeigth")),
                luggageCapasity:ko.observable(getTranslation("productInfo.luggageCapasity")),
                groundClearance:ko.observable(getTranslation("productInfo.groundClearance")),
                wheelBase:ko.observable(getTranslation("productInfo.wheelBase")),
                heigth:ko.observable(getTranslation("productInfo.heigth")),
                width:ko.observable(getTranslation("productInfo.width")),
                length:ko.observable(getTranslation("productInfo.length")),
                entertainmentSysLbl:ko.observable(getTranslation("productInfo.entertainmentSysLbl")),
                optionListLbl:ko.observable(getTranslation("productInfo.optionListLbl")),
                saftySecurityLbl:ko.observable(getTranslation("productInfo.saftySecurityLbl")),
                steerWheelsLbl:ko.observable(getTranslation("productInfo.steerWheelsLbl")),
                powerPerformanceLbl:ko.observable(getTranslation("productInfo.powerPerformanceLbl")),
                modelLbl:ko.observable(getTranslation("productInfo.modelLbl")),
                suspensionLbl:ko.observable(getTranslation("productInfo.suspensionLbl")),
                weigthLbl:ko.observable(getTranslation("productInfo.weigthLbl")),
                dimentionsLbl:ko.observable(getTranslation("productInfo.dimentionsLbl")),
                categoryName: ko.observable(getTranslation("products.categoryName")),
                currency: ko.observable(getTranslation("products.currency")),
                model: ko.observable(getTranslation("products.model"))
            });
        }
    }

    return productInfoContentViewModel;
});
