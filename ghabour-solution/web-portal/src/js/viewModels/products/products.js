define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojarraydataprovider'
            , 'ojs/ojlistdataproviderview', 'ojs/ojbutton', 'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojarraytabledatasource',
    'ojs/ojinputtext', 'ojs/ojselectcombobox'],
        function (oj, ko, app, services, commonhelper, ArrayDataProvider, ListDataProviderView) {

            function productsContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.productId = ko.observable();
                self.search = ko.observable();
                self.selectListner = ko.observableArray([]);
                self.images = ko.observableArray([]);
                self.searchCar = ko.observableArray([]);
                self.searchArr = ko.observableArray([]);
                self.carsModelArr = ko.observableArray([]);
                self.carsProductArr = ko.observableArray([]);
                self.productData = ko.observableArray();
                self.dataStatus = ko.observable("loading");
                self.shimmerCards = ko.observableArray(new Array(4));
                self.searchDataProvider = new ArrayDataProvider(self.searchArr, {keyAttributes: 'value'});

                self.carModelsClickAction = function (event) {
                    self.dataStatus("loading");
                    var currentItem = event.srcElement.currentItem;
                    var modelProduct = event.srcElement.data.dataProvider.data._latestValue[currentItem].Name_c;
                    if (modelProduct && self.productData()) {
                        self.carsProductArr(self.productData().Products.filter(e => e.Model_c == modelProduct));
                        oj.Router.rootInstance.store(self.carsProductArr());
                        oj.Router.rootInstance.go('newproduct');
                        if (self.carsProductArr().length == 0) {
                            app.createMessage("error", "No data available");
                            self.dataStatus("noDataAvailable");
                        }
                    }
                };

                self.clearClick = function () {
                    self.search('');
                    return true;
                };

                self.handleValueChanged = function () {
                    self.search(document.getElementById("text-input10").rawValue);
                };

                self.dataProvider = ko.computed(function () {
                    var filterRegEx = new RegExp(self.search(), 'i');
                    var filterCriterion = {
                        op: '$or',
                        criteria: [
                            {op: '$regex', value: {Name_c: filterRegEx}}
                        ]
                    };
                    var arrayDataProvider = new ArrayDataProvider(self.searchCar, {keyAttributes: 'value'});
                    return new ListDataProviderView(arrayDataProvider, {filterCriterion: filterCriterion});
                });

                self.carBrandsClickAction = function (brand) {
                    self.searchArr([]);
                    if (brand && self.productData()) {
                        self.carsModelArr([]);
                        self.carsModelArr(self.productData().Models.filter(e => e.Brand_c == brand));
                        self.searchCar(self.carsModelArr());
                        for (var i in self.carsModelArr()) {
                            self.searchArr.push({"value": self.carsModelArr()[i].Name_c});
                        }
                    }
                };
                function arrayBufferToBase64(buffer) {
                    return new Promise((resolve, reject) => {
                        const reader = new FileReader();
                        var blob = new Blob([buffer], {type: "image/png"});
                        reader.readAsDataURL(blob);
                        reader.onload = () => resolve(reader.result.substr(reader.result.indexOf(',') + 1));
                        reader.onerror = error => reject(error);
                    });
                }
                var dataLoaded = false;
                ko.computed(() => {
                    if (app.productData() && !dataLoaded) {
                        dataLoaded = true;
                        self.productData(app.productData());
                        self.carBrandsClickAction("CHERY");
                        self.carsModelArr([]);
                        for (var x = 0; x < app.productData().Models.length; x++) {
                            if (app.productData().Models[x].Brand_c == "CHERY") {
                                app.productData().Models[x].img = "css/images/car_placeholder.png";
                                self.carsModelArr.push(app.productData().Models[x]);
                            }
                        }
                        self.dataStatus("dataRetrieved");
                    } 
                });
                ko.computed(()=>{
                   if (app.modelData()) {
                        self.images([]);
                        self.dataStatus("dataRetrieved");
                        var data = self.carsModelArr();
                        data.forEach(e => {
                            if (app.modelData().find(x => x.model == e.Name_c)) {
                                arrayBufferToBase64(new Uint8Array(app.modelData().find(x => x.model == e.Name_c).image)).then(imageData => {
                                    e.img = imageData.replace("data/image/png/base64/", "data:image/png;base64,");
                                });
                            }
                        });
                        self.carsModelArr(data);
                    }
                });
                self.connected = function () {
                    app.loading(false);
                    app.headerVisible(true);
                    initTranslations();

                };


                function initTranslations() {
                    app.headerTitle(getTranslation('dashboard.products'));
                    self.label({
                        load: ko.observable(getTranslation("history.load")),
                        searchLbl: ko.observable(getTranslation("main.searchLbl")),
                        noThingTxt: ko.observable(getTranslation("main.noThingTxt")),
                        emptyTxt: ko.observable(getTranslation("main.noDataAvailableTxt")),
                        hyundai: ko.observable(getTranslation("vehicleHistory.hyundai")),
                        title: ko.observable(getTranslation("products.title")),
                        noDataText: ko.observable(getTranslation("history.noDataText")),
                        productInfo: ko.observable(getTranslation("products.productInfo")),
                        categoryName: ko.observable(getTranslation("products.categoryName")),
                        currency: ko.observable(getTranslation("products.currency")),
                        model: ko.observable(getTranslation("products.model")),
                        chooseCar: ko.observable(getTranslation("vehicleHistory.chooseCar")),
                        category: ko.observable(getTranslation("history.category")),
                        chooseCategory: ko.observable(getTranslation("vehicleHistory.chooseCategory"))
                    });
                }
            }

            return productsContentViewModel;
        });
