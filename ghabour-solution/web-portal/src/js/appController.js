define(['knockout', 'ojs/ojmodule-element-utils', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils',
    'ojs/ojrouter', 'ojs/ojarraydataprovider', 'config/services', 'ojs/ojvalidation-base'
            , 'ojs/ojmodule-element', 'ojs/ojknockout', 'ojs/ojmessages', 'ojs/ojavatar',
    'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojnavigationlist'],
        function (ko, moduleUtils, ResponsiveUtils, ResponsiveKnockoutUtils, Router, ArrayDataProvider, services, ojvalbase) {
            function ControllerViewModel() {
                var self = this;
                const getTranslation = oj.Translations.getTranslatedString;
                var selectedLanguage = localStorage.getItem("selectedLanguage");
                var cssPath = "css/images/";
                var loader = $('#loader');
                // Define if the user is logged in
                self.googlecredentials = {
                    "client_id": "812547011930-lt8q1o70te97em22uv06ghfatu5ot489.apps.googleusercontent.com",
                    "scope": "profile email"
                };
                self.vehicleFlag=ko.observable();
                self.phoneFlag=ko.observable();
                // Username
                self.userLogin = ko.observable("");
                self.isDashboardState = ko.observable(true);
                self.currentYear = ko.observable(new Date().getFullYear());
                // User Profile Picture
                self.imagePath = ko.observable("");
                self.refreshViewForLanguage = ko.observable(false);
                self.label = ko.observable();
                self.userLoggedIn = ko.observable(false);
                self.lang = ko.observable();
                self.personDetails = ko.observable();
                self.userData = ko.observable();
                self.vehicleData = ko.observable();
                self.productData = ko.observable();
                self.modelData = ko.observable();
                self.faqData = ko.observable();
                self.testDriveData = ko.observable();
                self.complainData = ko.observable();
                self.inquiryData = ko.observable();
                self.serviceData = ko.observable();
                self.locationData=ko.observable();
                self.image = ko.observable();
                self.name = ko.observable();
                self.profileImg = ko.observable();
                self.userPicture = ko.observable();
                self.messages = ko.observableArray("");
                self.logoPath = ko.observable("css/images/logo.png");
                self.messagesDataprovider = new ArrayDataProvider(self.messages);
                //Header config
                self.headerVisible = ko.observable(false);
                self.headerTitle = ko.observable();
                self.navListData = ko.observable();
                self.navListDataArray = ko.observableArray();
                self.navListDataArray.push({name: '', id: 'setup',
                    iconClass: 'oj-navigationlist-item-icon fa fa-cog'});
                self.navListDataArray.push({name: '', id: 'notificationScreen',
                    iconClass: 'oj-navigationlist-item-icon fa fa-bell'});
                self.navListData(new ArrayDataProvider(self.navListDataArray(), {keyAttributes: 'id'}));

                self.globalCarsBrandArr = ko.observableArray([
                    {id: 1, brandName: "Hyundai", brandIcon: cssPath + "Hyundai.png", mainColor: "#002c5f",
                        secondaryColor: "#33b3d2", backgroundImg: "url(images/bg-hyundai.png)"},
                    {id: 2, brandName: "Chery", brandIcon: cssPath + "Chery.png", mainColor: "#ca0100",
                        secondaryColor: "#c54a49", backgroundImg: "url(images/bg-chery.png)"},
                    {id: 3, brandName: "Geely", brandIcon: cssPath + "Geely.png", mainColor: "#0067b2",
                        secondaryColor: "#0267b1", backgroundImg: "url(images/bg-geely.png)"},
                    {id: 4, brandName: "Mazda", brandIcon: cssPath + "Mazda.png", mainColor: "#1087cd",
                        secondaryColor: "#4d9ece", backgroundImg: "url(images/bg-mazda.png)"},
                    {id: 5, brandName: "Ghabbour", brandIcon: cssPath + "logo.png", mainColor: "#175591",
                        secondaryColor: "#00A1DB", backgroundImg: "url(images/bck-cutting.png)"}
                ]);

                self.brandThemeConfiguration = function (brandName) {
                    if (!brandName)
                        brandName = "Ghabbour";
                    var event = self.globalCarsBrandArr().filter(e => e.brandName == brandName)[0];
                    self.logoPath(event.brandIcon);
                    document.body.style.setProperty('--main-color', event.mainColor);
                    document.body.style.setProperty('--secondary-color', event.secondaryColor);
                    document.body.style.setProperty('--dashboard-img', event.backgroundImg);
                };

                self.brandThemeConfiguration("Chery");

                window.gapi.load('client:auth2', function () {
                    window.gapi.auth2.init(self.googlecredentials).then(function (googleAuth) {
                        self.googleAuth = googleAuth;
                    });
                });

                self.googleDisconnect = function (data, event) {
                    window.gapi.auth2.getAuthInstance().disconnect().then(
                            function () {
                                self.userLogin('Unknown');
                                self.imagePath('');
                            },
                            function (e) {
                                console.log(e);
                            });
                };

                self.checkLoginState = function () {
                    //facebook init to open dialog for fb sign in
                    FB.init({
                        appId: '599883990841573',
                        status: true,
                        cookie: true,
                        xfbml: true,
                        version: 'v5.0'
                    });
                    (function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id))
                            return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "https://connect.facebook.net/en_US/sdk.js#version=v5.0&appId=599883990841573&status=true&cookie=true&xfbml=true";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    //after user login with fb account 
                    FB.login(function (response) {
                        console.log(response.status);
                        if (response.status == 'connected') {
                            self.fbIdToken = response.authResponse.accessToken;
                            testAPI();
                        } else {
                            console.log('User cancelled login or did not fully authorize.');
                        }
                    }, {
                        scope: 'email',
                        return_scopes: true
                    });
                    function testAPI() {
                        FB.api('/me',
                                {fields: "id,picture,email,first_name,gender,hometown,location,last_name,name,timezone,work"},
                                function (response) {
                                    console.log(response);
                                    var payload = {
                                        "username": response.name,
                                        "firstName": response.first_name,
                                        "email": response.email,
                                        "lastName": response.last_name,
                                        "fid": response.id,
                                        "fbIdToken": self.fbIdToken
//                                    "profileImg": response.picture.data
                                    };
                                    self.getDataFromContinue(response, payload);
                                });
                        oj.Router.rootInstance.go('dashboard');
                    }
                };

                self.continueWithGoogle = function () {
                    window.gapi.auth2.getAuthInstance().signIn().then(
                            function () {
                                var profile = self.googleAuth.currentUser.get().getBasicProfile();
                                self.id_token = self.googleAuth.currentUser.get().getAuthResponse().id_token;
                                var payload = {
                                    "username": profile.getName(),
                                    "firstName": profile.getGivenName(),
                                    "email": profile.getEmail(),
                                    "lastName": profile.getFamilyName(),
                                    "googleId": profile.getId(),
                                    "googleIdToken": self.id_token,
                                    "profileImg": profile.getImageUrl()
                                };
                                self.getDataFromContinue(profile, payload);
                            },
                            function (e) {
                                self.userLoggedIn(false);
                                console.log(e);
                            });
                };

                self.getDataFromContinue = function (outData, payload) {
                    self.loading(true);
                    services.postGeneric("users/isContinueWithGoogle", payload).then(data => {
                        console.log(data);
                        self.loading(false);
                        if (data.length > 0) {
                            self.userLoggedIn(true);
                            localStorage.setItem("personDetails", JSON.stringify(data[0]));
                            self.personDetails(JSON.parse(localStorage.getItem("personDetails")));
                            self.name(self.personDetails().username);
                            if (data.length > 0 && outData) {
                                if (data[0].username && data[0].password) {
                                    if (!data[0].phone) {
                                        oj.Router.rootInstance.go('phoneScreen');
                                    } else
                                        self.login(data[0].username.toLowerCase(), data[0].password);
                                }
                            }
                        } else {
                            self.createMessage("error", "Can't Sign in Account");
                        }
                    }, self.failCbFn);
                };


                self.fbLogout = function () {
                    FB.getLoginStatus(function (response) {
                        console.log(response.status);
                        if (response.status === 'connected') {
                            FB.logout(function (response) {
//                                FB.Auth.setAuthResponse(null, 'unknown');
//                                document.location.reload();
                            });
                        }
                    });
                };

                self.menuItemAction = function () {
                    var selected = event.target.value;
                    if (selected == "Logout") {
                        self.userLoggedIn(false);
                        localStorage.removeItem("personDetails");
                        if (self.id_token) {
                            self.googleDisconnect();
                            self.googleSignOut();
                        } else if (self.fbIdToken)
                            self.fbLogout();
                        oj.Router.rootInstance.go('dashboard');
                    } else if (selected == "Account setting") {
                        oj.Router.rootInstance.go('profile');
                    } else if (selected == "Brand") {
                        self.label().skipActionLbl(getTranslation("main.setDefaultActionLbl"));
                        oj.Router.rootInstance.go('brandChoice');
                    }
                };

                self.googleSignOut = function (data, event) {
                    window.gapi.auth2.getAuthInstance().signOut().then(
                            function () {
                                self.userLogin('Unknown');
                                self.imagePath('');
                            },
                            function (e) {
                                console.log(e);
                            });
                };

                $(window).on('scroll', function () {
                    if (Math.round($(window).scrollTop()) > 400) {
                        $('header').addClass('navbar-fixed-top');
                    } else {
                        $('header').removeClass('navbar-fixed-top');
                    }
                });

                self.loading = function (enabled) {
                    enabled ? loader.show() : loader.hide();
                };

                self.setLocale = function (lang) {
                    oj.Config.setLocale(lang,
                            function () {
                                self.refreshViewForLanguage(!self.refreshViewForLanguage());
                                $("html").attr("lang", lang);
                                if (lang == 'ar') {
                                    $("html").attr("dir", "rtl");
                                } else {
                                    $("html").attr("dir", "ltr");
                                }
                                initTranslations();
                                if (!self.userData()) {
                                    self.initRouter();
                                }
                            }
                    );
                };

                self.getLocale = function () {
                    return oj.Config.getLocale();
                };

                self.switchLanguage = function () {
                    if (self.getLocale() == "ar") {
                        self.setLocale("en-US");
                        self.lang("en-US");
                        localStorage.setItem("selectedLanguage", "en-US");
                    } else {
                        self.lang("ar");
                        self.setLocale("ar");
                        localStorage.setItem("selectedLanguage", "ar");
                    }
                };


                (async function () {
                    console.log('Getting General Data...');

                    await services.getGeneric("complainService/complainsType").then(data => {
                        if (data) {
                            self.complainData(data);
                        }
                    }, {});

                    await services.getGeneric("inquiryService/inquiryType").then(data => {
                        if (data) {
                            self.inquiryData(data);
                        }
                    }, {});

                    await services.getGeneric("testDriveService/testDriveType").then(data => {
                        if (data) {
                            self.testDriveData(data);
                        }
                    }, {});

                    await services.getGeneric("FAQService").then(data => {
                        if (data) {
                            self.faqData(data);
                        }
                    }, {});

                    await services.getGeneric("productsService").then(data => {
                        if (data) {
                            self.productData(data);
                        }
                    }, {});

//                    await services.getGeneric("modelService").then(data => {
//                        if (data) {
//                            self.modelData(data);
//                        }
//                    }, {});
                    await services.getGeneric("LocationService").then(data => {
                        if (data) {
                            self.locationData(data);
                        }
                    }, {});

                    await services.getGeneric("serviceService/serviceType").then(data => {
                        if (data) {
                            self.serviceData(data);
                        }
                    }, {});

                    console.log('Getting General Data Done.');
                })();

                self.createMessage = function (severity, summary) {
                    self.loading(false);
                    self.messages([]);
                    self.messages.push({
                        severity: severity,
                        summary: summary,
                        autoTimeout: 0
                    });
                };

                if (selectedLanguage) {
                    self.setLocale(selectedLanguage);
                } else {
                    localStorage.setItem("selectedLanguage", "en-US");
                    self.setLocale(localStorage.getItem("selectedLanguage"));
                }

                self.failCbFn = function () {
                    self.loading(false);
                    self.createMessage("error", self.label().serviceErrMsg());
                };

                self.routeToPage = function (page) {
                    oj.Router.rootInstance.go(page);
                };

                self.ourServices = function () {
                    if (self.router.currentState()) {
                        if (self.router.currentState()._id != 'dashboard') {
                            self.isDashboardState(false);
                            oj.Router.rootInstance.go('dashboard');
                        } else {
                            $('html, body').animate({scrollTop: $(document).height()}, 'slow');
                        }
                    } else {
                        $('html, body').animate({scrollTop: $(document).height()}, 'slow');
                    }
                };

                self.getUserData = async () => {
                    if (self.personDetails()) {
                        var personId = self.personDetails().id.toString();
                        console.log(`Person ID = ${personId}`);
                        await services.getGeneric("users/getUserData/12").then(data => {
                            if (data) {
                                Object.keys(data).forEach(function (dataKey) {
                                    for (var i in data[dataKey]) {
                                        var obj = data[dataKey][i];
                                        Object.keys(obj).forEach(function (key) {
                                            if (obj[key] == 'null') {
                                                obj[key] = self.label().notAvailable();
                                            }
                                        });
                                    }
                                });
                                self.userData(data);
                            }
                        }, {});


                        await services.getGeneric("vehicleHistory/12").then(data => {
                            if (data) {
                                self.vehicleData(data);
                            }
                        }, {});

                        await services.getGeneric("attachment/" + personId).then(data => {
                            if (data.length > 0) {
                                self.image(data[0]);
                                self.personDetails().image = self.image();
                                self.profileImg(data[0].attachment);
                            } else {
                                self.personDetails().image = null;
                                self.profileImg('');
                            }
                        }, {});

                    }
                };

                self.changeLang = function () {
                    if (!self.lang()) {
                        self.lang("en-US");
                    }
                    var payload = {
                        "id": self.personDetails().id,
//                        "id": 12,
                        "defaultLang": self.lang()
                    };
                    services.postGeneric("users/editDefaultLang", payload);
                };
                self.updateVehicle=function(flag){
                    self.vehicleFlag(flag);
                    var payload={
                        "id":self.personDetails().id,
                        "vehicleFlag":flag
                    };
                    services.postGeneric("users/editVehicleFlag", payload);
                };


                self.login = function (userName, password) {
                    self.loading(true);
                    services.getGeneric("users/getUserDetails/" + userName + "/" + password).then(data => {
                        if (data[0])
                        {
                            if(data[0].vehicleFlag=="T"){
                                oj.Router.rootInstance.go("dashboard");
                            }else{
                                oj.Router.rootInstance.go("addVehicleLogin");
                            }
                            if(data[0].phone){
                                self.phoneFlag("T");
                            }else{
                                self.phoneFlag("F");
                            }
                            self.userLoggedIn(true);
                            data[0].image=self.profileImg();
                            localStorage.setItem("personDetails", JSON.stringify(data[0]));
                            self.personDetails(data[0]);
                            self.vehicleFlag(data[0].vehicleFlag);
                            self.name(self.personDetails().username);
                            localStorage.setItem("selectedLanguage", self.personDetails().defaultLang);
                            self.setLocale(self.personDetails().defaultLang);                            
                            self.loading(false);

                            self.getUserData();
                        } else {
                            self.loading(false);
                            self.createMessage("error", self.label().passErrorMsg());
                            return;
                        }
                    }, err => {
                        self.loading(false);
                        self.createMessage("error", self.label().serviceErrMsg());
                    });
                };

                // Media queries for repsonsive layouts
                var smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                self.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);

                initTranslations();
                // Router setup
                self.router = Router.rootInstance;
                self.initRouter = function () {
                    self.router.configure({
                        'home': {label: self.label().home(), value: "main/home", title: self.label().home()},
                        'logIn': {label: self.label().login(), value: "main/logIn", title: self.label().login()},
                        'brandChoice': {label: self.label().brandChoice(), value: "secure/profile/brandChoice", title: self.label().brandChoice()},
                        'signUp': {label: self.label().signup(), value: "main/signUp", title: self.label().signup()},
                        'faq': {label: self.label().faq(), value: "main/faq", title: self.label().faq()},
                        'ourLocation': {label: self.label().ourLocation(), value: "main/ourLocation", title: self.label().ourLocation()},
                        'offers': {label: self.label().offers(), value: "main/offers", title: self.label().offers()},
                        'dashboard': {label: self.label().dashboard(), value: "secure/dashboard/dashboard", title: self.label().dashboard(), isDefault: true},
                        'maintenance': {label: self.label().maintenance(), value: "secure/maintenance/maintenance", title: self.label().maintenance(),
                            canEnter: () => {
                                return (self.userLoggedIn()&& self.vehicleFlag()=='T' &&self.phoneFlag()=='T');
                            }},
                        'complain': {label: self.label().complain(), value: "secure/complain/complain", title: self.label().complain(),
                            canEnter: () => {
                                return (self.userLoggedIn()&& self.vehicleFlag()=='T' &&self.phoneFlag()=='T');
                            }},
                        'testDrive': {label: self.label().testDrive(), value: "secure/testDrive/testDrive", title: self.label().testDrive(),
                            canEnter: () => {
                                return self.userLoggedIn();
                            }},
                        'vehicleHistory': {label: self.label().vehicleHistory(), value: "secure/vehicleHistory/vehicleHistory", title: self.label().vehicleHistory(),
                            canEnter: () => {
                                return self.userLoggedIn();
                            }},
                        'promotions': {label: self.label().promotions(), value: "promotions/promotions", title: self.label().promotions()},
                        'resetPassword': {label: self.label().resetPassword(), value: "resetPassword/resetPassword", title: self.label().resetPassword()},
                        'phoneScreen': {label: self.label().phoneScreen(), value: "resetPassword/phoneScreen", title: self.label().phoneScreen()},
                        'inquiry': {label: self.label().inquiry(), value: "secure/inquiry/inquiry", title: self.label().inquiry(),
                            canEnter: () => {
                                return (self.userLoggedIn()&& self.vehicleFlag()=='T' &&self.phoneFlag()=='T');
                            }},
                        'service': {label: self.label().service(), value: "secure/service/service", title: self.label().service(),
                            canEnter: () => {
                                return (self.userLoggedIn()&& self.vehicleFlag()=='T' &&self.phoneFlag()=='T');
                            }},
                        'profile': {label: self.label().profile(), value: "secure/profile/profile", title: self.label().profile(),
                            canEnter: () => {
                                return self.userLoggedIn();
                            }},
                        'products': {label: self.label().products(), value: "products/products", title: self.label().products()},
                        'productInfo': {label: self.label().productInfo(), value: "products/productInfo", title: self.label().productInfo()},
                        'newproduct': {label: self.label().productInfo(), value: "products/newproduct", title: self.label().productInfo()},
                        'reservationPayment': {label: self.label().reservationPayment(), value: "secure/reservationPayment/reservationPayment", title: self.label().reservationPayment(),
                            canEnter: () => {
                                return self.userLoggedIn();
                            }},
                        'addVehicle': {label: self.label().addVehicle(), value: "secure/profile/addVehicle", title: self.label().addVehicle(),
                            canEnter: () => {
                                return self.userLoggedIn();
                            }},
                        'addVehicleLogin': {label: self.label().addVehicle(), value: "resetPassword/addVehicleLogin", title: self.label().addVehicle(),
                            canEnter: () => {
                                return self.userLoggedIn();
                            }}
                    });
                };
                self.initRouter();
                Router.defaults['urlAdapter'] = new Router.urlParamAdapter();

                //refresh view when language change
                self.refreshData = ko.computed(function () {
                    if (self.refreshViewForLanguage()) {
                        self.setLocale(localStorage.getItem("selectedLanguage"));
                    }
                });

                ko.computed(() => {
                    if (localStorage.getItem("personDetails")) {
                        console.log('computed for person details');
                        self.userLoggedIn(true);
                        try {
                            self.personDetails(JSON.parse(localStorage.getItem("personDetails")));
                            self.getUserData();
                            self.name(self.personDetails().username);

                            self.vehicleFlag(self.personDetails().vehicleFlag);
                            if(self.personDetails().phone){
                                self.phoneFlag("T");
                            }else{
                                self.phoneFlag("F");
                            }
                            //Handling user picture
                            if (JSON.parse(localStorage.getItem("personDetails")).image == null) {
                                self.profileImg('');
                            } else {
                                self.profileImg(self.personDetails().image.attachment);
                            }
                        } catch (e) {
                            console.log(e);
                        }

                    } else {
                        self.userLoggedIn(false);
//                        window.history.pushState({}, document.title, "/" + "");
                    }
                });

                self.switchLangAction = function () {
                    self.switchLanguage();
                    self.changeLang();
                };

                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});
                self.loadModule = function () {
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath})
                        ]);
                        masterPromise.then(
                                function (values) {
                                    window.scrollTo(0, 0);
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                }
                        );
                    });
                };

                function initTranslations() {
                    self.label({
                        homeLbl: ko.observable(getTranslation('main.home')),
                        productsLbl: ko.observable(getTranslation('dashboard.products')),
                        promotionsLbl: ko.observable(getTranslation('dashboard.promotions')),
                        faqLbl: ko.observable(getTranslation('main.faq')),
                        loginLbl: ko.observable(getTranslation('main.login')),
                        signupLbl: ko.observable(getTranslation('main.signUp')),
                        brandChoiceLbl: ko.observable(getTranslation('main.brandChoice')),
                        accountSettingLbl: ko.observable(getTranslation('complain.accountSetting')),
                        language: ko.observable(getTranslation('main.switchLang')),
                        productInfo: ko.observable(getTranslation('title.productInfo')),
                        addVehicle: ko.observable(getTranslation('title.addVehicle')),
                        accountSetting: ko.observable(getTranslation('title.accountSetting')),
                        logout: ko.observable(getTranslation('complain.logout')),
                        notAvailable: ko.observable(getTranslation("history.notAvailable")),
                        passErrorMsg: ko.observable(getTranslation('complain.passErrorMsg')),
                        serviceErrMsg: ko.observable(getTranslation('complain.serviceErrMsg')),
                        home: ko.observable(getTranslation('title.home')),
                        login: ko.observable(getTranslation('title.logIn')),
                        signup: ko.observable(getTranslation("title.signUp")),
                        dashboard: ko.observable(getTranslation('title.dashboard')),
                        skipActionLbl: ko.observable(getTranslation("main.skipActionLbl")),
                        maintenance: ko.observable(getTranslation('title.maintenance')),
                        complain: ko.observable(getTranslation('title.complain')),
                        service: ko.observable(getTranslation('title.service')),
                        inquiry: ko.observable(getTranslation("title.inquiry")),
                        vehicleHistory: ko.observable(getTranslation('title.vehicleHistory')),
                        products: ko.observable(getTranslation('title.products')),
                        profile: ko.observable(getTranslation('title.profile')),
                        offers: ko.observable(getTranslation('title.offers')),
                        ourLocation: ko.observable(getTranslation("title.ourLocation")),
                        ourServices: ko.observable(getTranslation("main.ourServices")),
                        faq: ko.observable(getTranslation('title.faq')),
                        brandChoice: ko.observable(getTranslation('title.brandChoice')),
                        promotions: ko.observable(getTranslation('title.promotions')),
                        resetPassword: ko.observable(getTranslation("dashboard.resetPassword")),
                        testDrive: ko.observable(getTranslation('title.testDrive')),
                        switchLangLbl: ko.observable(getTranslation("main.switchLang")),
                        reservationPayment: ko.observable(getTranslation('title.reservationPayment')),
                        phoneScreen: ko.observable(getTranslation('title.phoneScreen'))
                    });
                }

            }

            return new ControllerViewModel();
        }
);
