define(['jquery'], function ($) {

    function serviceConfig() {

        var self = this;
        var authCode = "Basic Y3hfaW50ZWdyYXRpb246QXBwc1Byb0AyMDE5";
        self.contentTypeApplicationJSON = 'application/json';
        self.contentTypeApplicationXWWw = 'application/x-www-form-urlencoded; charset=UTF-8';
        self.headers = {};
        self.callGetService = function (serviceUrl, contentType, asynchronized, headers) {
            var defer = $.Deferred();
            $.ajax({
                type: "GET",
                async: asynchronized, 
                url: serviceUrl,
                headers: headers,
                contentType: contentType,
                // async : false, 
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.callPostService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }
            var defer = $.Deferred();
            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                    console.log(thrownError)
                }
            });
            return $.when(defer);
        };
        self.callPutService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }

            var defer = $.Deferred();
            $.ajax({
                type: "PUT",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
    }

    return new serviceConfig();
});