package com.appspro.fusionsshr.bean;

public class ProductBean {
    private int id;
    //    @Lob
    private byte[] image;
    private String productId;
    private String active;
    private String name;
    private String model;
    private String brand;
    private String modelYear;

    
    public void setActive(String active) {
        this.active = active;
    }

    public String getActive() {
        return active;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getModelYear() {
        return modelYear;
    }
}
