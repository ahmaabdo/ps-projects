package com.appspro.fusionsshr.bean;

public class FaqBean {
    private int id;
    private String quesAr;
    private String quesEn;
    private String answerAr;
    private String answerEn;
    private String type;
    private String lookupTypeAr;
    private String lookupTypeEn;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setQuesAr(String quesAr) {
        this.quesAr = quesAr;
    }

    public String getQuesAr() {
        return quesAr;
    }

    public void setQuesEn(String quesEn) {
        this.quesEn = quesEn;
    }

    public String getQuesEn() {
        return quesEn;
    }

    public void setAnswerAr(String answerAr) {
        this.answerAr = answerAr;
    }

    public String getAnswerAr() {
        return answerAr;
    }

    public void setAnswerEn(String answerEn) {
        this.answerEn = answerEn;
    }

    public String getAnswerEn() {
        return answerEn;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setLookupTypeAr(String lookupTypeAr) {
        this.lookupTypeAr = lookupTypeAr;
    }

    public String getLookupTypeAr() {
        return lookupTypeAr;
    }

    public void setLookupTypeEn(String lookupTypeEn) {
        this.lookupTypeEn = lookupTypeEn;
    }

    public String getLookupTypeEn() {
        return lookupTypeEn;
    }
}
