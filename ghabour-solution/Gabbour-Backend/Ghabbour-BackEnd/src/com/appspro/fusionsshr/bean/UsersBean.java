package com.appspro.fusionsshr.bean;

public class UsersBean {
    private int id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String chassisNumber;
    private String engineNumber;
    private String nationalId;
    private String defaultBrand;
    private String defaultLang;
    private String  vehicleFlag;
    private String googleId;
    private String googleIdToken;
    private String profileImg;
    private String fbIdToken;
    private String fId;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setDefaultBrand(String defaultBrand) {
        this.defaultBrand = defaultBrand;
    }

    public String getDefaultBrand() {
        return defaultBrand;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleIdToken(String googleIdToken) {
        this.googleIdToken = googleIdToken;
    }

    public String getGoogleIdToken() {
        return googleIdToken;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setFbIdToken(String fbIdToken) {
        this.fbIdToken = fbIdToken;
    }

    public String getFbIdToken() {
        return fbIdToken;
    }

    public void setFId(String fId) {
        this.fId = fId;
    }

    public String getFId() {
        return fId;
    }

    public void setVehicleFlag(String vehicleFlag) {
        this.vehicleFlag = vehicleFlag;
    }

    public String getVehicleFlag() {
        return vehicleFlag;
    }
}
