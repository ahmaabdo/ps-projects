package com.appspro.fusionsshr.bean;

public class AttachmentBean {
    private int id;
    private String SS_id;
    //    @Lob
    private String attachment;
    private String name;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setSS_id(String SS_id) {
        this.SS_id = SS_id;
    }

    public String getSS_id() {
        return SS_id;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
