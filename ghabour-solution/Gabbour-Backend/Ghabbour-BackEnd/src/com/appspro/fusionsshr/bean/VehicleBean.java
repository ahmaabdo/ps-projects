package com.appspro.fusionsshr.bean;

public class VehicleBean {
    private int id;
    private int primaryContact;
    private String model;
    private String chassisNumber;
    private String motor;
    private String phone;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getMotor() {
        return motor;
    }

    public void setPrimaryContact(int primaryContact) {
        this.primaryContact = primaryContact;
    }

    public int getPrimaryContact() {
        return primaryContact;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }
}
