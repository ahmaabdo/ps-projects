package com.appspro.fusionsshr.bean;

public class LocationsBean {
    private int id;
    private String locationName;
    private String city;
    private String address;
    private String brand;
    private String latitude;
    private String longitude;
    private String locationNameAr;
    private String cityAr;
    private String addressAr;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLocationNameAr(String locationNameAr) {
        this.locationNameAr = locationNameAr;
    }

    public String getLocationNameAr() {
        return locationNameAr;
    }

    public void setCityAr(String cityAr) {
        this.cityAr = cityAr;
    }

    public String getCityAr() {
        return cityAr;
    }

    public void setAddressAr(String addressAr) {
        this.addressAr = addressAr;
    }

    public String getAddressAr() {
        return addressAr;
    }
}
