package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.VehicleBean;

import static common.restHelper.RestHelper.getSchema_Name;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class VehicleDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ArrayList<VehicleBean> getAllVehicles(int id) {
        ArrayList<VehicleBean> vehicleList = new ArrayList<VehicleBean>();
        connection = AppsproConnection.getConnection();
        String query =
            "select * FROM " + getSchema_Name() + ".XXX_USER_VEHICLE where primary_contact=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                VehicleBean bean = new VehicleBean();
                bean.setId(rs.getInt("ID"));
                bean.setPrimaryContact(rs.getInt("primary_contact"));
                bean.setModel(rs.getString("MODEL"));
                bean.setMotor(rs.getString("MOTOR"));
                bean.setChassisNumber(rs.getString("CHASSIS_NUMBER"));
                vehicleList.add(bean);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return vehicleList;
    }

    public VehicleBean insertVehicle(String body) {
        connection = AppsproConnection.getConnection();
        VehicleBean vehicleBean =new VehicleBean();
        String query = null;
        JSONObject bean = new JSONObject(body);
        query =
                "insert into " + getSchema_Name() + ".XXX_USER_VEHICLE (motor,model,chassis_number,primary_contact,phone) VALUES(?,?,?,?,?)";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.get("motor").toString());
            ps.setString(2, bean.get("model").toString());
            ps.setString(3, bean.get("chassisNumber").toString());
            ps.setInt(4, bean.getInt("primaryContact"));
            ps.setString(5, bean.get("phone").toString());

            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return vehicleBean;

    }
    public String deleteVehicle(String body) {
            String del="{}";
            VehicleBean obj = new VehicleBean();
            List<JSONObject> beanList = new ArrayList<JSONObject>();
            JSONArray arr = new JSONArray(body);
            JSONObject jsonObjInput;
            for (int i = 0; i < arr.length(); i++) {
                jsonObjInput = arr.getJSONObject(i);
                beanList.add(jsonObjInput);
            }
            try {
                connection = AppsproConnection.getConnection();
                String query="DELETE FROM  "+ " " + getSchema_Name() + ".XXX_USER_VEHICLE WHERE ID=?";
                ps = connection.prepareStatement(query);
                for (JSONObject bean : beanList) {
                ps.setString(1,bean.get("id").toString());
                ps.executeUpdate();
                }
                del="{\"state\":\"del\"}";
            } catch (Exception e) {
               e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
            return del;
        }
}
