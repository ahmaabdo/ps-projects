package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.LocationsBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

public class LocationsDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public ArrayList<LocationsBean> getAllLocations() {
        ArrayList<LocationsBean> locationList = new ArrayList<LocationsBean>();
        connection = AppsproConnection.getConnection();
        String query = "select * FROM " + getSchema_Name() +".XXX_LOCATIONS";
        try {
            ps = connection.prepareStatement(query);
    //            ps.setString(1, username);
    //            ps.setString(2, EncryptionUtilities.desEncrypt(password));
            rs = ps.executeQuery();

            while (rs.next()) {
                LocationsBean bean = new LocationsBean();
                bean.setId(rs.getInt("ID"));
                bean.setLocationName(rs.getString("LOCATION_NAME"));
                bean.setCity(rs.getString("CITY"));
                bean.setAddress(rs.getString("ADDRESS"));
                bean.setBrand(rs.getString("BRAND"));
                bean.setLatitude(rs.getString("LATITUDE"));
                bean.setLongitude(rs.getString("LONGITUDE"));
                bean.setLocationNameAr(rs.getString("LOCATION_NAME_AR"));
                bean.setCityAr(rs.getString("CITY_AR"));
                bean.setAddressAr(rs.getString("ADDRESS_AR"));
                locationList.add(bean);
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        
        return locationList;

    }
}
