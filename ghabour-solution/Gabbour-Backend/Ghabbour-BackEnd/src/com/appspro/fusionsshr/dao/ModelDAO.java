package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ModelBean;

import static common.restHelper.RestHelper.getSchema_Name;
import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import org.json.JSONArray;

public class ModelDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public ArrayList<ModelBean> getImage(String model) {
        ArrayList<ModelBean> beanList = new ArrayList<ModelBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_MODEL_ATTACH where model=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, model);
            rs = ps.executeQuery();
            while (rs.next()) {
                ModelBean bean = new ModelBean();
                bean.setId(rs.getInt("ID"));
                bean.setImage(rs.getBytes("ATTACHMENT"));
                bean.setName(rs.getString("NAME"));
                bean.setModel(rs.getString("model"));
                bean.setBrand(rs.getString("brand"));
                beanList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return beanList;
    }
    public ArrayList<ModelBean> getModels() {
        ArrayList<ModelBean> beanList = new ArrayList<ModelBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_MODEL_ATTACH";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ModelBean bean = new ModelBean();
                bean.setId(rs.getInt("ID"));
                bean.setImage(rs.getBytes("ATTACHMENT"));
                bean.setName(rs.getString("NAME"));
                bean.setModel(rs.getString("model"));
                bean.setBrand(rs.getString("brand"));
                beanList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        System.out.println(new JSONArray(beanList));
        return beanList;
    }
}
