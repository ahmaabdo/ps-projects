package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.UsersBean;


import com.appspro.fusionsshr.handler.GoogleAccountAuth;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import utilities.EncryptionUtilities;


public class UsersDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public static String randomAlphaNumeric(int count) {
    StringBuilder builder = new StringBuilder();
    while (count-- != 0) {
    int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
    builder.append(ALPHA_NUMERIC_STRING.charAt(character));
    }
    return builder.toString();
    }

    public ArrayList<UsersBean> getAllUsers(String username, String password) {
        ArrayList<UsersBean> usersList = new ArrayList<UsersBean>();
        connection = AppsproConnection.getConnection();
        String query = "select * FROM " + getSchema_Name() +".XXX_USERS WHERE LOWER(username) = ? AND password = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, username.toLowerCase());
            ps.setString(2, EncryptionUtilities.desEncrypt(password));
            rs = ps.executeQuery();

            while (rs.next()) {
                UsersBean bean = new UsersBean();
                bean.setId(rs.getInt("ID"));
                bean.setUsername(rs.getString("USERNAME").toLowerCase());
                bean.setPassword(EncryptionUtilities.desDecrypt(rs.getString("PASSWORD")));
                bean.setFirstName(rs.getString("FIRSTNAME"));
                bean.setLastName(rs.getString("LASTNAME"));
                bean.setEmail(rs.getString("EMAIL"));
                bean.setPhone(rs.getString("PHONE"));
                bean.setChassisNumber(rs.getString("CHASSISNUMBER"));
                bean.setNationalId(rs.getString("NATIONAL_ID"));
                bean.setEngineNumber(rs.getString("ENGINE_NUMBER"));
                bean.setDefaultBrand(rs.getString("DEFAULT_BRAND"));
                bean.setDefaultLang(rs.getString("DEFAULT_LANG"));
                bean.setVehicleFlag(rs.getString("VEHICLE_FLAG"));
                usersList.add(bean);
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        
        return usersList;

    }

    public UsersBean getUserById(int id) {
        connection = AppsproConnection.getConnection();
        UsersBean bean = new UsersBean();
        String query = "select * FROM "+ " " + getSchema_Name() +".XXX_USERS where ID=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean.setId(rs.getInt("ID"));
                bean.setUsername(rs.getString("USERNAME"));
                bean.setFirstName(rs.getString("FIRSTNAME"));
                bean.setLastName(rs.getString("LASTNAME"));
                bean.setEmail(rs.getString("EMAIL"));
                bean.setPhone(rs.getString("PHONE"));
                bean.setChassisNumber(rs.getString("CHASSISNUMBER"));
                bean.setNationalId(rs.getString("NATIONAL_ID"));
                bean.setEngineNumber(rs.getString("ENGINE_NUMBER"));
                bean.setDefaultBrand(rs.getString("DEFAULT_BRAND"));
                bean.setDefaultLang(rs.getString("DEFAULT_LANG"));
                bean.setVehicleFlag(rs.getString("VEHICLE_FLAG"));
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        
        return bean;

    }
    public UsersBean getUserByEmail(String email) {
        connection = AppsproConnection.getConnection();
        UsersBean bean = new UsersBean();
        String query = "select * FROM "+ " " + getSchema_Name() +".XXX_USERS where email=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean.setId(rs.getInt("ID"));
                bean.setUsername(rs.getString("USERNAME"));
                bean.setPassword(EncryptionUtilities.desDecrypt(rs.getString("PASSWORD")));
                bean.setFirstName(rs.getString("FIRSTNAME"));
                bean.setLastName(rs.getString("LASTNAME"));
                bean.setEmail(rs.getString("EMAIL"));
                bean.setPhone(rs.getString("PHONE"));
                bean.setChassisNumber(rs.getString("CHASSISNUMBER"));
                bean.setNationalId(rs.getString("NATIONAL_ID"));
                bean.setEngineNumber(rs.getString("ENGINE_NUMBER"));
                bean.setDefaultBrand(rs.getString("DEFAULT_BRAND"));
                bean.setDefaultLang(rs.getString("DEFAULT_LANG"));
                bean.setVehicleFlag(rs.getString("VEHICLE_FLAG"));
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        
        return bean;

    }

    public UsersBean insertOrUpdateUsers(UsersBean bean, String transactionType, String primaryContactId) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
            
            if (transactionType.equals("ADD")) {
                
                query = "insert into " + getSchema_Name() + ".XXX_USERS (USERNAME,PASSWORD,FIRSTNAME,LASTNAME,EMAIL,PHONE,CHASSISNUMBER,PRIMARYCONTACT,ENGINE_NUMBER,NATIONAL_ID) VALUES(?,?,?,?,?,?,?,?,?,?)";
                
                ps = connection.prepareStatement(query);
                
                ps.setString(1, bean.getUsername().toLowerCase());
                ps.setString(2, EncryptionUtilities.desEncrypt(bean.getPassword()));
                ps.setString(3, bean.getFirstName());
                ps.setString(4, bean.getLastName());
                ps.setString(5, bean.getEmail());
                ps.setString(6, bean.getPhone());
                ps.setString(7, bean.getChassisNumber());
                ps.setInt(8, Integer.parseInt(primaryContactId));
                ps.setString(9, bean.getEngineNumber());
                ps.setString(10, bean.getNationalId());
            } else if (transactionType.equals("EDIT")) {
                query =
                        "UPDATE  " + " " + getSchema_Name() + ".XXX_USERS set PASSWORD=? WHERE (EMAIL=?) or (PHONE=?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, EncryptionUtilities.desEncrypt(bean.getPassword()));
                ps.setString(2, bean.getEmail());
                ps.setString(3, bean.getPhone());
            }
            
            ps.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
        
    }
    public UsersBean insertGoogle(UsersBean bean, String transactionType, String primaryContactId) {
        String pass=randomAlphaNumeric(8);
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
            boolean validIdToken=false;
            GoogleAccountAuth google=new GoogleAccountAuth();
            String googleId=bean.getGoogleId();
            String googleIdToken=bean.getGoogleIdToken();
            String fbIdToken=bean.getFbIdToken();
            System.out.println(googleId);
            System.out.println("Id Token:  "+googleIdToken);
            if (googleIdToken!=null){
                 validIdToken=google.tokenSignIn("812547011930-lt8q1o70te97em22uv06ghfatu5ot489.apps.googleusercontent.com","715831709945-d6a1glp8f183n6qo2i1uf1p382294afn.apps.googleusercontent.com",googleIdToken);
                System.out.println("validIdToken:  "+validIdToken);
            }
            if(validIdToken || fbIdToken!=null){
            if (transactionType.equals("ADD")) {
                
                query = "insert into " + getSchema_Name() + ".XXX_USERS (USERNAME,PASSWORD,FIRSTNAME,LASTNAME,EMAIL,PHONE,CHASSISNUMBER,PRIMARYCONTACT,ENGINE_NUMBER,NATIONAL_ID) VALUES(?,?,?,?,?,?,?,?,?,?)";
                
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getUsername().toLowerCase());
                ps.setString(2, EncryptionUtilities.desEncrypt(pass));
                ps.setString(3, bean.getFirstName());
                ps.setString(4, bean.getLastName());
                ps.setString(5, bean.getEmail());
                ps.setString(6, "");
                ps.setString(7, "1234");
                ps.setInt(8, Integer.parseInt(primaryContactId));
                ps.setString(9, "1234");
                ps.setString(10, "1234");
            } 
            ps.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
        
    }
    public UsersBean UpdateContinueWithGooglePhone(UsersBean bean) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_USERS set PHONE=? where id=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getPhone());
            ps.setInt(2, bean.getId());
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;

    }
    public UsersBean UpdateUser(UsersBean bean) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_USERS set FIRSTNAME=?  ,LASTNAME=? ,EMAIL=?,PHONE=?,CHASSISNUMBER=?,ENGINE_NUMBER=?,NATIONAL_ID=? where id=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getFirstName());
            ps.setString(2, bean.getLastName());
            ps.setString(3, bean.getEmail());
            ps.setString(4, bean.getPhone());
            ps.setString(5, bean.getChassisNumber());
            ps.setString(6, bean.getEngineNumber());
            ps.setString(7, bean.getNationalId());
            ps.setInt(8, bean.getId());


            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;

    }
     public UsersBean forgetPass(UsersBean bean) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_USERS set PASSWORD=? WHERE (EMAIL=?) or (PHONE=?)";
            ps = connection.prepareStatement(query);
            ps.setString(1,
                         EncryptionUtilities.desEncrypt(bean.getPassword()));
            ps.setString(2, bean.getEmail());
            ps.setString(3, bean.getPhone());


            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;

    }

    public UsersBean UpdateUserPassword(UsersBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_USERS set PASSWORD=? WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, EncryptionUtilities.desEncrypt(bean.getPassword()));
            ps.setInt(2, bean.getId());


            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;

    }
    
    public UsersBean UpdateUserDefaultBrand(UsersBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query ="UPDATE " + getSchema_Name() + ".XXX_USERS set DEFAULT_BRAND=? WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getDefaultBrand());
            ps.setInt(2, bean.getId());


            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;

    }
    public UsersBean UpdateUserVehicleFlag(UsersBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query ="UPDATE " + getSchema_Name() + ".XXX_USERS set VEHICLE_FLAG=? WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getVehicleFlag());
            ps.setInt(2, bean.getId());


            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;

    }
    public UsersBean UpdateUserDefaultLang(UsersBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query ="UPDATE " + getSchema_Name() + ".XXX_USERS set DEFAULT_LANG=? WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getDefaultLang());
            ps.setInt(2, bean.getId());


            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;

    }
    public String isValidUsername(UsersBean bean) {
        connection = AppsproConnection.getConnection();
        String valid="{\"valid\":\"true\"}";
        String query = "select * FROM "+ " " + getSchema_Name() +".XXX_USERS where username=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getUsername());
            rs = ps.executeQuery();

            while (rs.next()) {
                valid="{\"valid\":\"false\"}";
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        
        return valid;

    }
    public String isValidMail(UsersBean bean) {
        connection = AppsproConnection.getConnection();
        String valid="{\"valid\":\"true\"}";
        String query = "select * FROM "+ " " + getSchema_Name() +".XXX_USERS where email=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getEmail());
            rs = ps.executeQuery();

            while (rs.next()) {
                valid="{\"valid\":\"false\"}";
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return valid;

    }
    public String isValidPhone(UsersBean bean) {
        connection = AppsproConnection.getConnection();
        String valid="{\"valid\":\"true\"}";
        String query = "select * FROM "+ " " + getSchema_Name() +".XXX_USERS where phone=?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getPhone());
            rs = ps.executeQuery();

            while (rs.next()) {
                valid="{\"valid\":\"false\"}";
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        
        return valid;

    }
    public String isContinueWithGoogleExist(UsersBean bean) {
        System.out.println(bean.getEmail());
        connection = AppsproConnection.getConnection();
        String valid="{\"valid\":\"false\"}";
        String query = "select * FROM "+ " " + getSchema_Name() +".XXX_USERS where (EMAIL=? and FIRSTNAME=? and LASTNAME=?)";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getEmail());
            ps.setString(2, bean.getFirstName());
            ps.setString(3, bean.getLastName());
            rs = ps.executeQuery();

            while (rs.next()) {
                valid="{\"valid\":\"true\"}";
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return valid;

    }
}
