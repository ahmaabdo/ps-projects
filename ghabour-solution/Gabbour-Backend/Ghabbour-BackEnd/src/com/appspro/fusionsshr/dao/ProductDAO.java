package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ProductBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

public class ProductDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public ArrayList<ProductBean> getImages(String model) {
        ArrayList<ProductBean> beanList = new ArrayList<ProductBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_PRODUCT_ATTACHMENT where Model=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, model);
            rs = ps.executeQuery();
            while (rs.next()) {
                ProductBean bean = new ProductBean();
                bean.setId(rs.getInt("ID"));
                bean.setImage(rs.getBytes("IMAGE"));
                bean.setActive(rs.getString("ACTIVE"));
                bean.setName(rs.getString("NAME"));
                bean.setBrand(rs.getString("Brand"));
                bean.setModel(rs.getString("Model"));
                beanList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return beanList;
    }
    public ArrayList<ProductBean> getProductId(String model) {
        ArrayList<ProductBean> beanList = new ArrayList<ProductBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_PRODUCT where MODEL_YEAR=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, model);
            rs = ps.executeQuery();
            while (rs.next()) {
                ProductBean bean = new ProductBean();
                bean.setId(rs.getInt("ID"));
                bean.setBrand(rs.getString("BRAND"));
                bean.setModel(rs.getString("MODEL"));
                bean.setModelYear(rs.getString("MODEL_YEAR"));
                beanList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return beanList;
    }
}
