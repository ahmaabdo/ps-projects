package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.ModelBean;
import com.appspro.fusionsshr.bean.ProductBean;
import com.appspro.fusionsshr.dao.ModelDAO;
import com.appspro.fusionsshr.dao.ProductDAO;

import common.restHelper.GenricHandler;
import common.restHelper.RestHelper;

import java.util.ArrayList;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProductsHandler extends GenricHandler {

    ProductDAO dao=new ProductDAO();
    public JSONObject getProductsFromSaaS() {
        JSONObject response = new JSONObject();
        String crmRestUrl = RestHelper.getInstance().getInstanceUrl() + RestHelper.getInstance().getCrmRestUrl();
        int brandOffset = 0;
        int modelOffset = 0;
        int productsOffset = 0;
        boolean hasMoreBrands = false;
        boolean hasMoreModels = false;
        boolean hasMoreProducts = false;
        
        //Car Brands
        do {
            String brandsURL = crmRestUrl + RestHelper.getInstance().getBrandURL() + "?limit=500&offset=" + brandOffset;

            try {
                JSONObject data = httpGET(brandsURL).getJSONObject("data");
                hasMoreBrands = data.getBoolean("hasMore");
                JSONArray items = data.getJSONArray("items");
                brandOffset += 500;
                JSONArray brandsArr = new JSONArray();

                for (int i = 0; i < items.length(); i++) {
                    JSONObject obj = new JSONObject(items.get(i).toString());
                    JSONObject newObj = new JSONObject();
                    if(!obj.get("BrandName_c").toString().equals("null") && !obj.get("BrandName_c").toString().equals(null)){
                    
                        newObj.put("BrandName_c", obj.get("RecordName").toString().trim());
                        newObj.put("CurrencyCode", obj.get("CurrencyCode").toString().trim());
                        newObj.put("CurcyConvRateType", obj.get("CurcyConvRateType").toString().trim());
                        newObj.put("CorpCurrencyCode", obj.get("CorpCurrencyCode").toString().trim());
                        newObj.put("BranchCode_c", obj.get("BranchCode_c").toString().trim());
                        
                        brandsArr.put(newObj);
                    }
                }

                response.put("Brands", brandsArr);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (hasMoreBrands);
        
        //Car Models
        do {
            String modelsURL = crmRestUrl + RestHelper.getInstance().getCarModelURL() + "?limit=500&offset=" + modelOffset;

            try {
                JSONObject data = httpGET(modelsURL).getJSONObject("data");
                hasMoreModels = data.getBoolean("hasMore");
                JSONArray items = data.getJSONArray("items");
                modelOffset += 500;
                JSONArray modelsArr = new JSONArray();

                for (int i = 0; i < items.length(); i++) {
                    JSONObject obj = new JSONObject(items.get(i).toString());
                    ArrayList<ModelBean> list = new ArrayList<ModelBean>();
                    JSONObject newObj = new JSONObject();
                    
                    newObj.put("Brand_c", obj.get("Brand_c").toString().trim());
                    newObj.put("Name_c", obj.get("Name_c").toString().trim());
                    newObj.put("CurrencyCode", obj.get("CurrencyCode").toString().trim());
                    newObj.put("CurcyConvRateType", obj.get("CurcyConvRateType").toString().trim());
                    newObj.put("CorpCurrencyCode", obj.get("CorpCurrencyCode").toString().trim());
                    newObj.put("Brand_Id_Brand_Car_Model", obj.get("Brand_Id_Brand_Car_Model").toString().trim());
                    modelsArr.put(i,newObj);
                }

                response.put("Models", modelsArr);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (hasMoreModels);
        
        //Car Products
        do {
            String productsURL = crmRestUrl + RestHelper.getInstance().getProductCategoryURL() + "?limit=500&offset=" + productsOffset;

            try {
                JSONObject data = httpGET(productsURL).getJSONObject("data");
                hasMoreProducts = data.getBoolean("hasMore");
                JSONArray items = data.getJSONArray("items");
                productsOffset += 500;
                JSONArray productsArr = new JSONArray();

                for (int i = 0; i < items.length(); i++) {
                    JSONObject obj = new JSONObject(items.get(i).toString());
                    JSONObject newObj = new JSONObject();
                    
                    newObj.put("Model_c", obj.get("Model_c").toString().trim());
                    newObj.put("CategoryName_c", obj.get("CategoryName_c").toString().trim());
                    newObj.put("CurrencyCode", obj.get("CurrencyCode").toString().trim());
                    newObj.put("CurcyConvRateType", obj.get("CurcyConvRateType").toString().trim());
                    newObj.put("CorpCurrencyCode", obj.get("CorpCurrencyCode").toString().trim());
                    
                    productsArr.put(i,newObj);
                }

                response.put("Products", productsArr);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (hasMoreProducts);

        return response;
    }
    
    
    
    public JSONObject getAllProducts() throws Exception {

        JSONObject respone = null;
        //// Caching
        CacheManager cm = CacheManager.getInstance();

        Cache cache = cm.getCache("ProductsCache");
        Element ele = cache.get("ProductsType");

        if (ele == null) {
            respone = getProductsFromSaaS();
            cache.put(new Element("ProductsType", respone.toString()));
        } else {
            String output =
                (ele == null ? null : ele.getObjectValue().toString());
            respone = new JSONObject(output);
        }

        return respone;
    }
    public JSONArray getAllImages(String model){

            ArrayList<ProductBean> list = new ArrayList<ProductBean>();
            list = dao.getImages(model);

            return new JSONArray(list);
        }
    public JSONArray getProductId(String model){

            ArrayList<ProductBean> list = new ArrayList<ProductBean>();
            list = dao.getProductId(model);

            return new JSONArray(list);
        }


}
