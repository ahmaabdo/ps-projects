package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.ModelBean;
import com.appspro.fusionsshr.dao.ModelDAO;

import common.restHelper.GenricHandler;

import java.util.ArrayList;

import org.json.JSONArray;

public class ModelHandler extends GenricHandler{
    ModelDAO dao=new ModelDAO();
    public JSONArray getAllImages(){

            ArrayList<ModelBean> list = new ArrayList<ModelBean>();
            list = dao.getModels();

            return new JSONArray(list);
        }
    public JSONArray getModelImg(String model){

            ArrayList<ModelBean> list = new ArrayList<ModelBean>();
            list = dao.getImage(model);

            return new JSONArray(list);
        }
}
