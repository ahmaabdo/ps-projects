package com.appspro.fusionsshr.handler;

import common.restHelper.GenricHandler;
import common.restHelper.RestHelper;

import org.json.JSONArray;
import org.json.JSONObject;

public class MaintenaceHandler extends GenricHandler {
    public JSONArray getEventArr() {

        JSONArray responseArr = new JSONArray();
        String eventURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20ID,bay,start_date,end_date,text,estimated_hour%20from%20CO.events";

        try {
            int idColNum = 0;
            int bayColNum = 0;
            int startDateColNum = 0;
            int endDateColNum = 0;
            int textColNum = 0;
            int estimatedHourColNum = 0;

            JSONObject eventObj =
                httpGET(eventURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames = eventObj.getJSONArray("columnNames");
            JSONArray rows = eventObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("bay"))
                    bayColNum = i;
                if (columnNames.get(i).equals("start_date"))
                    startDateColNum = i;
                if (columnNames.get(i).equals("end_date"))
                    endDateColNum = i;
                if (columnNames.get(i).equals("text"))
                    textColNum = i;
                if (columnNames.get(i).equals("estimated_hour"))
                    estimatedHourColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("bay",
                            rows.getJSONArray(i).get(bayColNum).toString().split("-")[0]);
                    obj.put("start_date",
                            rows.getJSONArray(i).get(startDateColNum).toString().split("-")[1]);
                    obj.put("end_date",
                            rows.getJSONArray(i).get(endDateColNum).toString().split("-")[0]);
                    obj.put("text",
                            rows.getJSONArray(i).get(textColNum).toString());
                    obj.put("estimated_hour",
                            rows.getJSONArray(i).get(estimatedHourColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("bay", rows.getJSONArray(i).get(bayColNum));
                    obj.put("start_date",
                            rows.getJSONArray(i).get(startDateColNum));
                    obj.put("end_date",
                            rows.getJSONArray(i).get(endDateColNum));
                    obj.put("text", rows.getJSONArray(i).get(textColNum));
                    obj.put("estimated_hour",
                            rows.getJSONArray(i).get(estimatedHourColNum));

                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }

    public JSONArray getBranchesArr() {

        JSONArray responseArr = new JSONArray();
        String branchesURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20id,code,name,active,capacity,recieption_period,delivery_period,time_slot%20from%20Config.Branch";

        try {
            int idColNum = 0;
            int codeColNum = 0;
            int nameColNum = 0;
            int activeColNum = 0;
            int capacityColNum = 0;
            int recieptionPeriodColNum = 0;
            int deliveryPeriodColNum = 0;
            int timeSlotColNum = 0;

            JSONObject branchesObj =
                httpGET(branchesURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames = branchesObj.getJSONArray("columnNames");
            JSONArray rows = branchesObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("code"))
                    codeColNum = i;
                if (columnNames.get(i).equals("name"))
                    nameColNum = i;
                if (columnNames.get(i).equals("active"))
                    activeColNum = i;
                if (columnNames.get(i).equals("capacity"))
                    capacityColNum = i;
                if (columnNames.get(i).equals("recieption_period"))
                    recieptionPeriodColNum = i;
                if (columnNames.get(i).equals("delivery_period"))
                    deliveryPeriodColNum = i;
                if (columnNames.get(i).equals("time_slot"))
                    timeSlotColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("code",
                            rows.getJSONArray(i).get(codeColNum).toString().split("-")[0]);
                    obj.put("name",
                            rows.getJSONArray(i).get(nameColNum).toString().split("-")[1]);
                    obj.put("active",
                            rows.getJSONArray(i).get(activeColNum).toString().split("-")[0]);
                    obj.put("capacity",
                            rows.getJSONArray(i).get(capacityColNum).toString().split("-")[1]);
                    obj.put("recieption_period",
                            rows.getJSONArray(i).get(recieptionPeriodColNum).toString().split("-")[0]);
                    obj.put("delivery_period",
                            rows.getJSONArray(i).get(deliveryPeriodColNum).toString().split("-")[1]);
                    obj.put("time_slot",
                            rows.getJSONArray(i).get(timeSlotColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("code", rows.getJSONArray(i).get(codeColNum));
                    obj.put("name", rows.getJSONArray(i).get(nameColNum));
                    obj.put("active", rows.getJSONArray(i).get(activeColNum));
                    obj.put("capacity",
                            rows.getJSONArray(i).get(capacityColNum));
                    obj.put("recieption_period",
                            rows.getJSONArray(i).get(recieptionPeriodColNum));
                    obj.put("delivery_period",
                            rows.getJSONArray(i).get(deliveryPeriodColNum));
                    obj.put("time_slot",
                            rows.getJSONArray(i).get(timeSlotColNum));


                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }

    public JSONArray getVehiclesArr() {

        JSONArray responseArr = new JSONArray();
        String vehiclesURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20id,model,plate_number,chassis_number,vin_number,engine_number%20from%20package.Vehicle%20where%20id%20=1";

        try {
            int idColNum = 0;
            int modelColNum = 0;
            int plateNumberColNum = 0;
            int chassisNumberColNum = 0;
            int vinNumberColNum = 0;
            int engineNumberColNum = 0;

            JSONObject vehiclesObj =
                httpGET(vehiclesURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames = vehiclesObj.getJSONArray("columnNames");
            JSONArray rows = vehiclesObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("Model"))
                    modelColNum = i;
                if (columnNames.get(i).equals("plate_number"))
                    plateNumberColNum = i;
                if (columnNames.get(i).equals("chassis_number"))
                    chassisNumberColNum = i;
                if (columnNames.get(i).equals("vin_number"))
                    vinNumberColNum = i;
                if (columnNames.get(i).equals("engine_number"))
                    engineNumberColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("Model",
                            rows.getJSONArray(i).get(modelColNum).toString().split("-")[0]);
                    obj.put("plate_number",
                            rows.getJSONArray(i).get(plateNumberColNum).toString().split("-")[1]);
                    obj.put("chassis_number",
                            rows.getJSONArray(i).get(chassisNumberColNum).toString().split("-")[0]);
                    obj.put("vin_number",
                            rows.getJSONArray(i).get(vinNumberColNum).toString().split("-")[1]);
                    obj.put("engine_number",
                            rows.getJSONArray(i).get(engineNumberColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("Model", rows.getJSONArray(i).get(modelColNum));
                    obj.put("plate_number",
                            rows.getJSONArray(i).get(plateNumberColNum));
                    obj.put("chassis_number",
                            rows.getJSONArray(i).get(chassisNumberColNum));
                    obj.put("vin_number",
                            rows.getJSONArray(i).get(vinNumberColNum));
                    obj.put("engine_number",
                            rows.getJSONArray(i).get(engineNumberColNum));
                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }

    public JSONArray getOperationsArr() {

        JSONArray responseArr = new JSONArray();
        String operationURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20id,code,description,operation_category,bay_type_groups,splitted_by_break,user_must_enter_note%20from%20Config.Operation";

        try {
            int idColNum = 0;
            int codeColNum = 0;
            int descriptionColNum = 0;
            int operationCategoryColNum = 0;
            int bayTypeGroupsColNum = 0;
            int splittedByBreakColNum = 0;
            int userMustEnterNoteColNum = 0;

            JSONObject operationObj =
                httpGET(operationURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames = operationObj.getJSONArray("columnNames");
            JSONArray rows = operationObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("code"))
                    codeColNum = i;
                if (columnNames.get(i).equals("description"))
                    descriptionColNum = i;
                if (columnNames.get(i).equals("operation_category"))
                    operationCategoryColNum = i;
                if (columnNames.get(i).equals("bay_type_groups"))
                    bayTypeGroupsColNum = i;
                if (columnNames.get(i).equals("splitted_by_break"))
                    splittedByBreakColNum = i;
                if (columnNames.get(i).equals("user_must_enter_note"))
                    userMustEnterNoteColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("code",
                            rows.getJSONArray(i).get(codeColNum).toString().split("-")[0]);
                    obj.put("description",
                            rows.getJSONArray(i).get(descriptionColNum).toString().split("-")[1]);
                    obj.put("operation_category",
                            rows.getJSONArray(i).get(operationCategoryColNum).toString().split("-")[0]);
                    obj.put("bay_type_groups",
                            rows.getJSONArray(i).get(bayTypeGroupsColNum).toString().split("-")[1]);
                    obj.put("splitted_by_break",
                            rows.getJSONArray(i).get(splittedByBreakColNum).toString().split("-")[0]);
                    obj.put("user_must_enter_note",
                            rows.getJSONArray(i).get(userMustEnterNoteColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("code", rows.getJSONArray(i).get(codeColNum));
                    obj.put("description",
                            rows.getJSONArray(i).get(descriptionColNum));
                    obj.put("operation_category",
                            rows.getJSONArray(i).get(operationCategoryColNum));
                    obj.put("bay_type_groups",
                            rows.getJSONArray(i).get(bayTypeGroupsColNum));
                    obj.put("splitted_by_break",
                            rows.getJSONArray(i).get(splittedByBreakColNum));
                    obj.put("user_must_enter_note",
                            rows.getJSONArray(i).get(userMustEnterNoteColNum));
                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }

    public JSONArray getWorkingHoursByBranchArr() {

        JSONArray responseArr = new JSONArray();
        String workingHoursByBranchURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20Day,BreakStartTime,BreakEndTime,StartDate,EndDate,StartTime,EndTime,Type%20from%20Config.WorkingDays%20where%20branch=4";

        try {
            int idColNum = 0;
            int dayColNum = 0;
            int breakStartTimeColNum = 0;
            int breakEndTimeColNum = 0;
            int startDateColNum = 0;
            int endDateColNum = 0;
            int startTimeColNum = 0;
            int endTimeColNum = 0;
            int typeColNum = 0;

            JSONObject workingHoursByBranchObj =
                httpGET(workingHoursByBranchURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames =
                workingHoursByBranchObj.getJSONArray("columnNames");
            JSONArray rows = workingHoursByBranchObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("Day"))
                    dayColNum = i;
                if (columnNames.get(i).equals("BreakStartTime"))
                    breakStartTimeColNum = i;
                if (columnNames.get(i).equals("BreakEndTime"))
                    breakEndTimeColNum = i;
                if (columnNames.get(i).equals("StartDate"))
                    startDateColNum = i;
                if (columnNames.get(i).equals("EndDate"))
                    endDateColNum = i;
                if (columnNames.get(i).equals("StartTime"))
                    startTimeColNum = i;
                if (columnNames.get(i).equals("EndTime"))
                    endTimeColNum = i;
                if (columnNames.get(i).equals("Type"))
                    typeColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("Day",
                            rows.getJSONArray(i).get(dayColNum).toString().split("-")[0]);
                    obj.put("BreakStartTime",
                            rows.getJSONArray(i).get(breakStartTimeColNum).toString().split("-")[1]);
                    obj.put("BreakEndTime",
                            rows.getJSONArray(i).get(breakEndTimeColNum).toString().split("-")[0]);
                    obj.put("StartDate",
                            rows.getJSONArray(i).get(startDateColNum).toString().split("-")[1]);
                    obj.put("EndDate",
                            rows.getJSONArray(i).get(endDateColNum).toString().split("-")[0]);
                    obj.put("StartTime",
                            rows.getJSONArray(i).get(startTimeColNum).toString().split("-")[0]);
                    obj.put("EndTime",
                            rows.getJSONArray(i).get(endTimeColNum).toString().split("-")[0]);
                    obj.put("Type",
                            rows.getJSONArray(i).get(typeColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("Day", rows.getJSONArray(i).get(dayColNum));
                    obj.put("BreakStartTime",
                            rows.getJSONArray(i).get(breakStartTimeColNum));
                    obj.put("BreakEndTime",
                            rows.getJSONArray(i).get(breakEndTimeColNum));
                    obj.put("StartDate",
                            rows.getJSONArray(i).get(startDateColNum));
                    obj.put("EndDate",
                            rows.getJSONArray(i).get(endDateColNum));
                    obj.put("StartTime",
                            rows.getJSONArray(i).get(startTimeColNum));
                    obj.put("EndTime",
                            rows.getJSONArray(i).get(endTimeColNum));
                    obj.put("Type", rows.getJSONArray(i).get(typeColNum));
                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }

    public JSONArray getcapacitySlotByWorkingDayArr() {

        JSONArray responseArr = new JSONArray();
        String capacitySlotURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20StartTime,endtime,Capacity,WorkingDays%20from%20Config.CapacitySlot%20where%20workingDays=3";

        try {
            int idColNum = 0;
            int startTimeColNum = 0;
            int endTimeColNum = 0;
            int capacityColNum = 0;
            int workingDaysColNum = 0;

            JSONObject capacitySlotObj =
                httpGET(capacitySlotURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames =
                capacitySlotObj.getJSONArray("columnNames");
            JSONArray rows = capacitySlotObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("StartTime"))
                    startTimeColNum = i;
                if (columnNames.get(i).equals("EndTime"))
                    endTimeColNum = i;
                if (columnNames.get(i).equals("Capacity"))
                    capacityColNum = i;
                if (columnNames.get(i).equals("WorkingDays"))
                    workingDaysColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("Capacity",
                            rows.getJSONArray(i).get(capacityColNum).toString().split("-")[0]);
                    obj.put("WorkingDays",
                            rows.getJSONArray(i).get(workingDaysColNum).toString().split("-")[0]);
                    obj.put("StartTime",
                            rows.getJSONArray(i).get(startTimeColNum).toString().split("-")[0]);
                    obj.put("EndTime",
                            rows.getJSONArray(i).get(endTimeColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("Capacity",
                            rows.getJSONArray(i).get(capacityColNum));
                    obj.put("WorkingDays",
                            rows.getJSONArray(i).get(workingDaysColNum));
                    obj.put("StartTime",
                            rows.getJSONArray(i).get(startTimeColNum));
                    obj.put("EndTime",
                            rows.getJSONArray(i).get(endTimeColNum));
                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }

    public JSONArray getmodelOperationArr() {

        JSONArray responseArr = new JSONArray();
        String modelOperationURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20hours_standard,hours_production,hours_billing,labor_price,max_start_time,id%20from%20Config.ModelOperation%20where%20model=2%20and%20operation=10";

        try {
            int idColNum = 0;
            int hoursStandardColNum = 0;
            int hoursProductionColNum = 0;
            int hoursBillingColNum = 0;
            int laborPriceColNum = 0;
            int maxStartTimeColNum = 0;
            JSONObject modelOperationObj =
                httpGET(modelOperationURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames =
                modelOperationObj.getJSONArray("columnNames");
            JSONArray rows = modelOperationObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("hours_standard"))
                    hoursStandardColNum = i;
                if (columnNames.get(i).equals("hours_production"))
                    hoursProductionColNum = i;
                if (columnNames.get(i).equals("hours_billing"))
                    hoursBillingColNum = i;
                if (columnNames.get(i).equals("labor_price"))
                    laborPriceColNum = i;
                if (columnNames.get(i).equals("max_start_time"))
                    maxStartTimeColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("hours_standard",
                            rows.getJSONArray(i).get(hoursStandardColNum).toString().split("-")[0]);
                    obj.put("hours_production",
                            rows.getJSONArray(i).get(hoursProductionColNum).toString().split("-")[0]);
                    obj.put("hours_billing",
                            rows.getJSONArray(i).get(hoursBillingColNum).toString().split("-")[0]);
                    obj.put("labor_price",
                            rows.getJSONArray(i).get(laborPriceColNum).toString().split("-")[0]);
                    obj.put("max_start_time",
                            rows.getJSONArray(i).get(maxStartTimeColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("hours_standard",
                            rows.getJSONArray(i).get(hoursStandardColNum));
                    obj.put("hours_production",
                            rows.getJSONArray(i).get(hoursProductionColNum));
                    obj.put("hours_billing",
                            rows.getJSONArray(i).get(hoursBillingColNum));
                    obj.put("labor_price",
                            rows.getJSONArray(i).get(laborPriceColNum));
                    obj.put("max_start_time",
                            rows.getJSONArray(i).get(maxStartTimeColNum));
                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }

    public JSONArray getPartsArr() {

        JSONArray responseArr = new JSONArray();
        String partsURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20Parts.id,Parts.part_description,Parts.price,Parts.part_number,ModelOperation.labor_price%20from%20Config.ModelOperParts%20where%20modelOperation=2";

        try {
            int idColNum = 0;
            int partDescriptionColNum = 0;
            int partNumberColNum = 0;
            int priceColNum = 0;
            int laborPriceColNum = 0;
            JSONObject partsObj =
                httpGET(partsURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames = partsObj.getJSONArray("columnNames");
            JSONArray rows = partsObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("part_description"))
                    partDescriptionColNum = i;
                if (columnNames.get(i).equals("part_number"))
                    partNumberColNum = i;
                if (columnNames.get(i).equals("labor_price"))
                    laborPriceColNum = i;
                if (columnNames.get(i).equals("price"))
                    priceColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("part_description",
                            rows.getJSONArray(i).get(partDescriptionColNum).toString().split("-")[0]);
                    obj.put("part_number",
                            rows.getJSONArray(i).get(partNumberColNum).toString().split("-")[0]);
                    obj.put("price",
                            rows.getJSONArray(i).get(priceColNum).toString().split("-")[0]);
                    obj.put("labor_price",
                            rows.getJSONArray(i).get(laborPriceColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("part_description",
                            rows.getJSONArray(i).get(partDescriptionColNum));
                    obj.put("part_number",
                            rows.getJSONArray(i).get(partNumberColNum));
                    obj.put("price", rows.getJSONArray(i).get(priceColNum));
                    obj.put("labor_price",
                            rows.getJSONArray(i).get(laborPriceColNum));
                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }

    public JSONArray getConsumablesArr() {

        JSONArray responseArr = new JSONArray();
        String consumablesURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults/?query=select%20Consumble.id,Consumble.code,Consumble.category,Consumble.name,Consumble.price,Consumble.ModelOperation,Consumble.category.name,ModelOperation.labor_price%20from%20Config.ModelOperConsumble%20where%20modelOperation=2";

        try {
            int idColNum = 0;
            int codeColNum = 0;
            int categoryColNum = 0;
            int nameColNum = 0;
            int priceColNum = 0;
            int ModelOperationColNum = 0;
            int nameArColNum = 0;
            int laborPriceColNum = 0;
            JSONObject consumablesObj =
                httpGET(consumablesURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames = consumablesObj.getJSONArray("columnNames");
            JSONArray rows = consumablesObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("code"))
                    codeColNum = i;
                if (columnNames.get(i).equals("category"))
                    categoryColNum = i;
                if (columnNames.get(i).equals("name"))
                    nameColNum = i;
                if (columnNames.get(i).equals("labor_price"))
                    laborPriceColNum = i;
                if (columnNames.get(i).equals("price"))
                    priceColNum = i;
                if (columnNames.get(i).equals("ModelOperation"))
                    ModelOperationColNum = i;
                if (columnNames.get(i).equals("Name"))
                    nameArColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));

                try {
                    obj.put("code",
                            rows.getJSONArray(i).get(codeColNum).toString().split("-")[0]);
                    obj.put("category",
                            rows.getJSONArray(i).get(categoryColNum).toString().split("-")[0]);
                    obj.put("name",
                            rows.getJSONArray(i).get(nameColNum).toString().split("-")[0]);
                    obj.put("price",
                            rows.getJSONArray(i).get(priceColNum).toString().split("-")[0]);
                    obj.put("ModelOperation",
                            rows.getJSONArray(i).get(ModelOperationColNum).toString().split("-")[0]);
                    obj.put("labor_price",
                            rows.getJSONArray(i).get(laborPriceColNum).toString().split("-")[0]);
                    obj.put("Name",
                            rows.getJSONArray(i).get(nameArColNum).toString().split("-")[0]);
                } catch (ArrayIndexOutOfBoundsException ae) {
                    obj.put("code", rows.getJSONArray(i).get(codeColNum));
                    obj.put("category",
                            rows.getJSONArray(i).get(categoryColNum));
                    obj.put("name", rows.getJSONArray(i).get(nameColNum));
                    obj.put("price", rows.getJSONArray(i).get(priceColNum));
                    obj.put("ModelOperation",
                            rows.getJSONArray(i).get(ModelOperationColNum));
                    obj.put("labor_price",
                            rows.getJSONArray(i).get(laborPriceColNum));
                    obj.put("Name", rows.getJSONArray(i).get(nameArColNum));
                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }
}
