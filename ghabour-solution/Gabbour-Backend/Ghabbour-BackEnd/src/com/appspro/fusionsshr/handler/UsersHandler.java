package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.UsersBean;
import com.appspro.fusionsshr.dao.UsersDAO;

import com.appspro.fusionsshr.rest.RightNowService;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.restHelper.GenricHandler;
import common.restHelper.RestHelper;

import java.util.ArrayList;

import javax.ws.rs.core.Response;

import org.codehaus.stax2.ri.typed.ValueEncoderFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.EncryptionUtilities;

public class UsersHandler extends GenricHandler  {
     UsersDAO dao =new UsersDAO();
     
    public JSONArray getAllUsers(String username,String password){
            ArrayList<UsersBean> list = new ArrayList<UsersBean>();
            list = dao.getAllUsers(username,password);
            return new JSONArray(list);
        }
    public JSONObject getUserById(int id){
            UsersBean bean = new UsersBean();
            bean = dao.getUserById(id);
            return new JSONObject(bean);
        }
    
    public JSONObject isValidUsername(String body){
            String valid=null;
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                valid = dao.isValidUsername(bean);
                  return new JSONObject(valid);
              } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONObject isValidMail(String body){
            String valid=null;
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                valid = dao.isValidMail(bean);
                  return new JSONObject(valid);
              } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONObject isValidPhone(String body){
            String valid=null;
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                valid = dao.isValidPhone(bean);
                  return new JSONObject(valid);
              } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONArray isContinueWithGoogle(String body) {
        String valid = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            UsersBean bean;
            UsersBean getbean;
            String userLogin = "";
            bean = mapper.readValue(body, UsersBean.class);
            valid = dao.isContinueWithGoogleExist(bean);
            String exist = new JSONObject(valid).get("valid").toString();
            System.out.println("exist"+exist);
            if (exist.equalsIgnoreCase("true")) {
                getbean= dao.getUserByEmail(bean.getEmail());
                userLogin=getAllUsers(getbean.getUsername(),getbean.getPassword()).toString();
            } else {
                JSONObject user = insertUserByGoogle(body, "ADD");
                getbean = dao.getUserByEmail(bean.getEmail());
                if(getbean.getUsername()!=null && getbean.getPassword()!=null){
                userLogin =
                        getAllUsers(getbean.getUsername(), getbean.getPassword()).toString();
                }
            }
            System.out.println("userCreated===>" + userLogin);
            if (!userLogin.equalsIgnoreCase("")) {
                return new JSONArray(userLogin);
            } else {
                return new JSONArray();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }
    public JSONObject insertUser(String body , String transactionType){
            //Adding contact primary id to local db
            JSONObject bodyObject = new JSONObject(body);
            String primaryContactId = "0";
            String contactPayloadString =
                "{ \"title\": \"Amr Matter5\", \"name\": { \"first\": \"" +
                bodyObject.get("firstName") + "\", \"last\": \"" +
                bodyObject.get("lastName") +
                "\" } , \"address\": { \"city\": \"Cairo\", \"postalCode\": \"02455\", \"stateOrProvince\": { \"lookupName\": \"Cairo\" }, \"street\": \"5Kafour St\", \"country\": { \"id\":2 } } , \"contactType\": { \"lookupName\": \"Prospect\" } }";

            Response response =
                RightNowService.getInstance().RightNowResponse("{ \"restMethod\":\"POST\", \"url\":\"contacts\", \"payload\":" + contactPayloadString + " }");

            try {
                JSONObject object = new JSONObject(response.getEntity().toString());
                primaryContactId = (object.getJSONObject("data").get("id")).toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            //If there's an error with rightnow api primaryContactId will be equaled to 0 and ContactErrorCode will return
            if(primaryContactId == "0"){
                return  new JSONObject("ContactErrorCode");
            }

            //Adding user info to the db
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                list = dao.insertOrUpdateUsers(bean, transactionType,primaryContactId);
                return new JSONObject(list);
              } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONObject insertUserByGoogle(String body , String transactionType){
            //Adding contact primary id to local db
            JSONObject bodyObject = new JSONObject(body);
            String primaryContactId = "0";
            String contactPayloadString =
                "{ \"title\": \"Amr Matter5\", \"name\": { \"first\": \"" +
                bodyObject.get("firstName") + "\", \"last\": \"" +
                bodyObject.get("lastName") +
                "\" } , \"address\": { \"city\": \"Cairo\", \"postalCode\": \"02455\", \"stateOrProvince\": { \"lookupName\": \"Cairo\" }, \"street\": \"5Kafour St\", \"country\": { \"id\":2 } } , \"contactType\": { \"lookupName\": \"Prospect\" } }";

            Response response =
                RightNowService.getInstance().RightNowResponse("{ \"restMethod\":\"POST\", \"url\":\"contacts\", \"payload\":" + contactPayloadString + " }");

            try {
                JSONObject object = new JSONObject(response.getEntity().toString());
                primaryContactId = (object.getJSONObject("data").get("id")).toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            //If there's an error with rightnow api primaryContactId will be equaled to 0 and ContactErrorCode will return
            if(primaryContactId == "0"){
                return  new JSONObject("ContactErrorCode");
            }

            //Adding user info to the db
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                list = dao.insertGoogle(bean, transactionType,primaryContactId);
                return new JSONObject(list);
              } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONObject updateUserGooglePhone(String body){
            //Adding contact primary id to local db
            JSONObject bodyObject = new JSONObject(body);
            String primaryContactId = "0";
            String contactPayloadString =
                "{ \"title\": \"Amr Matter5\", \"name\": { \"first\": \"" +
                bodyObject.get("firstName") + "\", \"last\": \"" +
                bodyObject.get("lastName") +
                "\" } , \"address\": { \"city\": \"Cairo\", \"postalCode\": \"02455\", \"stateOrProvince\": { \"lookupName\": \"Cairo\" }, \"street\": \"5Kafour St\", \"country\": { \"id\":2 } } , \"contactType\": { \"lookupName\": \"Prospect\" } }";

            Response response =
                RightNowService.getInstance().RightNowResponse("{ \"restMethod\":\"POST\", \"url\":\"contacts\", \"payload\":" + contactPayloadString + " }");

            try {
                JSONObject object = new JSONObject(response.getEntity().toString());
                primaryContactId = (object.getJSONObject("data").get("id")).toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            //If there's an error with rightnow api primaryContactId will be equaled to 0 and ContactErrorCode will return
            if(primaryContactId == "0"){
                return  new JSONObject("ContactErrorCode");
            }

            //Adding user info to the db
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                list = dao.UpdateContinueWithGooglePhone(bean);
                return new JSONObject(list);
              } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONObject updateUser(String body){
            //Adding contact primary id to local db
            JSONObject bodyObject = new JSONObject(body);
            String primaryContactId = "0";
            String contactPayloadString =
                "{ \"title\": \"Amr Matter5\", \"name\": { \"first\": \"" +
                bodyObject.get("firstName") + "\", \"last\": \"" +
                bodyObject.get("lastName") +
                "\" } , \"address\": { \"city\": \"Cairo\", \"postalCode\": \"02455\", \"stateOrProvince\": { \"lookupName\": \"Cairo\" }, \"street\": \"5Kafour St\", \"country\": { \"id\":2 } } , \"contactType\": { \"lookupName\": \"Prospect\" } }";

            Response response =
                RightNowService.getInstance().RightNowResponse("{ \"restMethod\":\"POST\", \"url\":\"contacts\", \"payload\":" + contactPayloadString + " }");

            try {
                JSONObject object = new JSONObject(response.getEntity().toString());
                primaryContactId = (object.getJSONObject("data").get("id")).toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            //If there's an error with rightnow api primaryContactId will be equaled to 0 and ContactErrorCode will return
            if(primaryContactId == "0"){
                return  new JSONObject("ContactErrorCode");
            }

            //Adding user info to the db
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                list = dao.UpdateUser(bean);
                return new JSONObject(list);
              } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONObject changeUserPass(String body){
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                list = dao.UpdateUserPassword(bean);
                return new JSONObject(list);
            } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        } 
    
    public JSONObject userDefaultBrand(String body){
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean = mapper.readValue(body, UsersBean.class);
                list = dao.UpdateUserDefaultBrand(bean);
                return new JSONObject(list);
            } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONObject userVehicleFlag(String body){
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean = mapper.readValue(body, UsersBean.class);
                list = dao.UpdateUserVehicleFlag(bean);
                return new JSONObject(list);
            } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    public JSONObject userDefaultLang(String body){
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean = mapper.readValue(body, UsersBean.class);
                list = dao.UpdateUserDefaultLang(bean);
                return new JSONObject(list);
            } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    
    public JSONObject forgetPass(String body){
            UsersBean list = new UsersBean();
            try {
                ObjectMapper mapper = new ObjectMapper();
                UsersBean bean;
                bean = mapper.readValue(body, UsersBean.class);
                list = dao.forgetPass(bean);
                return new JSONObject(list);
            } catch (Exception e) {
                e.printStackTrace();
                return new JSONObject("UnknownErrorCode");
            }
        }
    
    
    public JSONObject getUserData(String userId) throws Exception {
        
        String complaintTypeURL =
                    RestHelper.getRIGHT_NOW_URL() +  "/queryResults?query=select%20incidents.id," +
                    //....Complaint....
                    //Complaint type
                        "incidents.CustomFields.c.complaint_type.lookupName%20'complaint_type'," +
                    //Complaint values
                        "incidents.CustomFields.Incident.Branch.name%20'servicecenter'," +
                        "incidents.CustomFields.c.outlets.lookupName%20'outlets'," +
                        "incidents.CustomFields.Incident.Showroom.name%20'showrooms'," +
                        "incidents.CustomFields.c.dealers.lookupName%20'dealers'," +
                        "incidents.CustomFields.c.location_360.lookupName%20'location_360'," +
                        //Complaint details
                        "incidents.CustomFields.c.complaint_details%20'complaint_details'," +
                    
                        //....Inquiry....
                        //Inquiry type
                        "incidents.CustomFields.c.inquirytype.lookupName%20'inquirytype'," +
                        //Inquiry values
                        "incidents.CustomFields.c.pcorbajaj.lookupName%20'pcorbajaj'," +
                        //Inquiry details
                        "incidents.CustomFields.c.inquiry_details%20'inquiry_details'," +
                    
                        //....Service....
                        //Service type
                        "incidents.CustomFields.c.servicetype.lookupName%20'servicetype'," +
                        //Service RSA Location
                        "incidents.CustomFields.c.rsa_location_text%20'rsa_location_text'," +
                        //Service values
                        "incidents.CustomFields.c.after_sales_benefits.lookupName%20'after_sales_benefits'," +
                        "incidents.CustomFields.c.zones.lookupName%20'zones'," +
                        //Service details
                         "incidents.CustomFields.c.servicedetails%20'servicedetails'," +
                    
                        //Status
                         "incidents.statusWithType.status.lookupName%20'status'" +
                    "%20from%20incidents%20where%20incidents.PrimaryContact.Contact="+Integer.parseInt(userId)+"%20ORDER%20BY%20ID%20DESC";
                
        JSONObject responseObj = new JSONObject();                
        JSONArray complaintArr = new JSONArray();
        JSONArray inquiryArr = new JSONArray();
        JSONArray serviceArr = new JSONArray();

        try {
            JSONObject userDataURL = httpGET(complaintTypeURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray rows = userDataURL.getJSONArray("rows");
            JSONArray columnNames = userDataURL.getJSONArray("columnNames");
            JSONArray responseArr = new JSONArray();

            for (int i = 0; i < rows.length(); i++) {
                JSONArray rowVals = rows.getJSONArray(i);
                JSONObject obj = new JSONObject();
                
                for (int x = 0; x < rowVals.length(); x++) {
                    obj.put(columnNames.get(x).toString(), rowVals.get(x).toString());
                }
                responseArr.put(i, obj);
            }

            for (int i = 0; i < responseArr.length(); i++) {
                JSONObject complaintObj = new JSONObject();
                JSONObject inquiryObj = new JSONObject();
                JSONObject serviceObj = new JSONObject();
                
                //Complain data
                if(!responseArr.getJSONObject(i).get("complaint_type").equals("null")){
                    complaintObj.put("firstVal", responseArr.getJSONObject(i).get("complaint_type"));
                    complaintObj.put("detailsValue", responseArr.getJSONObject(i).get("complaint_details"));
                    complaintObj.put("statusCode", responseArr.getJSONObject(i).get("status"));
                    
                    String[] complaintSecVals = {"servicecenter", "outlets", "showrooms", "dealers", "location_360"};
                    for (int x = 0; x < complaintSecVals.length; x++) {
                        if (!responseArr.getJSONObject(i).get(complaintSecVals[x]).toString().equals("null"))
                            complaintObj.put("secondValue", responseArr.getJSONObject(i).get(complaintSecVals[x]));
                    }
                    complaintObj.put("flagSec", complaintObj.has("secondValue"));

                    complaintArr.put(complaintObj);
                }else if(!responseArr.getJSONObject(i).get("inquirytype").equals("null")){
                    //Inquiry data
                    inquiryObj.put("firstVal", responseArr.getJSONObject(i).get("inquirytype"));
                    if (!responseArr.getJSONObject(i).get("pcorbajaj").toString().equals("null"))
                        inquiryObj.put("secondValue", responseArr.getJSONObject(i).get("pcorbajaj"));
                    inquiryObj.put("detailsValue", responseArr.getJSONObject(i).get("inquiry_details"));
                    inquiryObj.put("statusCode", responseArr.getJSONObject(i).get("status"));
                    inquiryObj.put("flagSec", inquiryObj.has("secondValue"));

                    inquiryArr.put(inquiryObj);
                }else if(!responseArr.getJSONObject(i).get("servicetype").equals("null")){
                    //Service data
                    serviceObj.put("firstVal", responseArr.getJSONObject(i).get("servicetype"));
                    serviceObj.put("detailsValue", responseArr.getJSONObject(i).get("servicedetails"));
                    serviceObj.put("statusCode", responseArr.getJSONObject(i).get("status"));
                    serviceObj.put("flagSec", serviceObj.has("secondValue"));
                    
                    if (!responseArr.getJSONObject(i).get("rsa_location_text").toString().equals("Enter the RSA Location here"))
                        serviceObj.put("rsaLocation", responseArr.getJSONObject(i).get("rsa_location_text"));

                    serviceObj.put("flagRsa", serviceObj.has("rsaLocation"));
                    
                    String[] serviceSecVals = {"after_sales_benefits", "zones"};
                    for (int x = 0; x < serviceSecVals.length; x++) {
                        if (!responseArr.getJSONObject(i).get(serviceSecVals[x]).toString().equals("null"))
                            serviceObj.put("secondValue", responseArr.getJSONObject(i).get(serviceSecVals[x]));
                    }
                    serviceArr.put(serviceObj);
                }
            }
            
            responseObj.put("complaintData", complaintArr);
            responseObj.put("inquiryData", inquiryArr);
            responseObj.put("serviceData", serviceArr);
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseObj;
    }
    
}
