package com.appspro.fusionsshr.handler;
import common.restHelper.GenricHandler;
import common.restHelper.RestHelper;
import org.json.JSONArray;
import org.json.JSONObject;
public class VehicleHistoryHandler extends GenricHandler {
    public JSONArray getVehiclesByUserId(String userId) {
        JSONArray responseArr = new JSONArray();
        String queryURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults?query=select%20*%20,package.Vehicle.Model.lookupName'CarModel'," +
            "package.Vehicle.Elite_Vehicle.lookupName'Elite_Vehicle'," +
            "package.Vehicle.Model.Brand.name'Brand'%20from%20package.Vehicle%20where%20Contact.ID="+userId;
        try {
            
            JSONObject itemsObj = httpGET(queryURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames = itemsObj.getJSONArray("columnNames");
            JSONArray rows = itemsObj.getJSONArray("rows");
            
            for (int i = 0; i < rows.length(); i++) {
                JSONArray rowVals = rows.getJSONArray(i);
                JSONObject obj = new JSONObject();
                
                for (int x = 0; x < rowVals.length(); x++) {
                    obj.put(columnNames.get(x).toString(), rowVals.get(x).toString());
                }
                responseArr.put(obj);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseArr;
    }
}