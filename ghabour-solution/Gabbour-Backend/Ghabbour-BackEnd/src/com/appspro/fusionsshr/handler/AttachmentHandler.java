package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.AttachmentBean;

import com.appspro.fusionsshr.dao.AttachmentDAO;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class AttachmentHandler {
    
    AttachmentDAO dao = new AttachmentDAO();
    public JSONArray getAllAttachment(String ss_id){

            ArrayList<AttachmentBean> list = new ArrayList<AttachmentBean>();
            list = dao.getAttachment(ss_id);

            return new JSONArray(list);
        }
    public JSONObject insertAttachment(String body){

            AttachmentBean list = new AttachmentBean();
            try {
                list = dao.insertAttachment(body);
                return new JSONObject(list);
            } catch (Exception e) {
                return new JSONObject(list);
            }
        }
    public JSONObject updateAttachment(String body){

            AttachmentBean list = new AttachmentBean();
            try {
                list = dao.updateAttachment(body);
                return new JSONObject(list);
            } catch (Exception e) {
                return new JSONObject(list);
            }
        }
}
