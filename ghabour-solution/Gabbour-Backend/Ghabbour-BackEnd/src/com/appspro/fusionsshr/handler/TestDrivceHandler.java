package com.appspro.fusionsshr.handler;

import common.restHelper.GenricHandler;
import common.restHelper.RestHelper;

import org.json.JSONArray;
import org.json.JSONObject;

public class TestDrivceHandler extends GenricHandler{
    public JSONArray getTestDriveArr() {

         JSONArray responseArr = new JSONArray();
         String testDriveTypeURL =
             RestHelper.getRIGHT_NOW_URL() + "/queryResults?query=select%20*,Test_Drive.TestDriveSchedule.TestDriveAdmin.Showroom.name%20\'ShowRoom\'%20,Test_Drive.TestDriveSchedule.TestDriveAdmin.TestDriveModel.Model.Brand.name%20'Brand',Test_Drive.TestDriveSchedule.TestDriveAdmin.TestDriveModel.Model.code%20'Model'from%20Test_Drive.TestDriveSchedule%20where%20Test_Drive.TestDriveSchedule.Scheduled%20!=1";
         try {
             int idColNum = 0;
             int dateColNum = 0;
             int timeColNum=0;
             int showroomColNum = 0;
             int brandColNum = 0;
             int modelColNum=0;

             JSONObject testDriveTypeObj =
                 httpGET(testDriveTypeURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
             JSONArray columnNames =
                 testDriveTypeObj.getJSONArray("columnNames");
             JSONArray rows = testDriveTypeObj.getJSONArray("rows");

             for (int i = 0; i < columnNames.length(); i++) {
                 if (columnNames.get(i).equals("id"))
                     idColNum = i;
                 if (columnNames.get(i).equals("Date"))
                     dateColNum = i;
                 if (columnNames.get(i).equals("Time"))
                     timeColNum = i;
                 if (columnNames.get(i).equals("ShowRoom"))
                     showroomColNum = i;
                 if (columnNames.get(i).equals("Brand"))
                     brandColNum = i;
                 if (columnNames.get(i).equals("Model"))
                     modelColNum = i;
             }

             for (int i = 0; i < rows.length(); i++) {
                 JSONObject obj = new JSONObject();
                 obj.put("id", rows.getJSONArray(i).get(idColNum));

                 try{
                     obj.put("Date", rows.getJSONArray(i).get(dateColNum).toString().split("-")[0]);
                     obj.put("Time", rows.getJSONArray(i).get(timeColNum).toString().split("-")[1]);
                     obj.put("ShowRoom", rows.getJSONArray(i).get(showroomColNum).toString().split("-")[0]);
                     obj.put("Brand", rows.getJSONArray(i).get(brandColNum).toString().split("-")[1]);
                     obj.put("Model", rows.getJSONArray(i).get(modelColNum).toString().split("-")[0]);
                 }catch(ArrayIndexOutOfBoundsException ae){
                     obj.put("Date", rows.getJSONArray(i).get(dateColNum));
                     obj.put("Time", rows.getJSONArray(i).get(timeColNum));
                     obj.put("ShowRoom", rows.getJSONArray(i).get(showroomColNum));
                     obj.put("Brand", rows.getJSONArray(i).get(brandColNum));
                     obj.put("Model", rows.getJSONArray(i).get(modelColNum));
                     
                 }
                 responseArr.put(i, obj);
             }
         } catch (Exception e) {
             e.printStackTrace();
         }
         return responseArr;
     }
   
}
