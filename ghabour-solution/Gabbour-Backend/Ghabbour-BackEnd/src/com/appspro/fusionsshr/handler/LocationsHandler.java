package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.LocationsBean;
import com.appspro.fusionsshr.dao.LocationsDAO;

import common.restHelper.GenricHandler;

import java.util.ArrayList;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.json.JSONArray;

public class LocationsHandler extends GenricHandler{
    LocationsDAO dao = new LocationsDAO();
    public JSONArray getAllLocations() {
        ArrayList<LocationsBean> list = new ArrayList<LocationsBean>();
        list = dao.getAllLocations();
        return new JSONArray(list);
    }
    public JSONArray getLocationsType() throws Exception {

        JSONArray respone = null;
        //// Caching
        CacheManager cm = CacheManager.getInstance();

        Cache cache = cm.getCache("LocationCache");
        Element ele = cache.get("LocationType");

        if (ele == null) {
            respone = getAllLocations();
            cache.put(new Element("LocationType", respone.toString()));
        } else {
            String output =
                (ele == null ? null : ele.getObjectValue().toString());
            respone = new JSONArray(output);
        }

        return respone;
    }
}
