package com.appspro.fusionsshr.handler;

import java.io.IOException;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.util.Preconditions;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

import com.google.api.client.json.jackson2.JacksonFactory;

import java.security.GeneralSecurityException;

import com.google.api.client.util.Beta;
import com.google.api.client.util.Clock;

import java.security.PublicKey;

import java.util.Arrays;
import java.util.Collections;

public class GoogleAccountAuth {
    public boolean tokenSignIn(String CLIENT_ID1,String CLIENT_ID2,
                               String idTokenString) throws GeneralSecurityException,
                                                            IOException {
        HttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory jsonFactory = new JacksonFactory().getDefaultInstance();
        boolean emailVerified = false;
        GoogleIdTokenVerifier verifier =
            new GoogleIdTokenVerifier.Builder(transport,
                                              jsonFactory)
            .setAudience(Arrays.asList(CLIENT_ID1,CLIENT_ID2))
            .setIssuer("https://accounts.google.com")
            .build();
        GoogleIdToken idToken = verifier.verify(idTokenString);
        if (idToken != null) {
            Payload payload = idToken.getPayload();
            emailVerified = Boolean.valueOf(payload.getEmailVerified());
        } else {
            System.out.println("Invalid ID token.");
        }
        return emailVerified;
    }
}
