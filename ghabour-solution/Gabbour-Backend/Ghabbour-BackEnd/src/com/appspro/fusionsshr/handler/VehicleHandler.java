package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.AttachmentBean;
import com.appspro.fusionsshr.bean.VehicleBean;
import com.appspro.fusionsshr.dao.VehicleDAO;

import common.restHelper.GenricHandler;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class VehicleHandler extends GenricHandler{
    VehicleDAO dao=new VehicleDAO();
    public JSONArray getAllVehicles(int id){
            ArrayList<VehicleBean> list = new ArrayList<VehicleBean>();
            list = dao.getAllVehicles(id);
            return new JSONArray(list);
        }
    public JSONObject insertVehicle(String body){

            VehicleBean list = new VehicleBean();
            try {
                list = dao.insertVehicle(body);
                return new JSONObject(list);
            } catch (Exception e) {
                return new JSONObject(list);
            }
        }
    
    public JSONObject deleteVehicle(String body){
        String list="";
        try {
             list = dao.deleteVehicle(body);
            return new JSONObject(list);
        } catch (Exception e) {
            return new JSONObject(list);
        }
    }
    
}
