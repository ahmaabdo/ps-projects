package com.appspro.fusionsshr.handler;

import common.restHelper.GenricHandler;
import common.restHelper.RestHelper;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.json.JSONArray;
import org.json.JSONObject;

public class InquiryHandler extends GenricHandler{
    public JSONArray getInquiryArr() {

         JSONArray responseArr = new JSONArray();
         String inquiryTypeURL =
             RestHelper.getRIGHT_NOW_URL() + "/queryResults?query=select%20*%20from%20Incident.inquiry_type";

         try {
             int idColNum = 0;
             int lookupNameColNum = 0;

             JSONObject inquiryTypeObj =
                 httpGET(inquiryTypeURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
             JSONArray columnNames =
                 inquiryTypeObj.getJSONArray("columnNames");
             JSONArray rows = inquiryTypeObj .getJSONArray("rows");

             for (int i = 0; i < columnNames.length(); i++) {
                 if (columnNames.get(i).equals("id"))
                     idColNum = i;
                 if (columnNames.get(i).equals("lookupName"))
                     lookupNameColNum = i;
             }

             for (int i = 0; i < rows.length(); i++) {
                 JSONObject obj = new JSONObject();
                 obj.put("id", rows.getJSONArray(i).get(idColNum));
                 
                 try{
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[0]);
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[1]);
                 }catch(ArrayIndexOutOfBoundsException ae){
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum));
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum));
                 }

                 switch (rows.getJSONArray(i).get(lookupNameColNum).toString()) {
                     case "After Sales General Info":
                         obj.put("dependsObj", getDepends("PCorBAJAJ"));
                         break;
                     default:
                 }

                 responseArr.put(i, obj);
             }
         } catch (Exception e) {
             e.printStackTrace();
         }

         return responseArr;
     }
    public JSONArray getDepends(String URL) {

         JSONArray responseArr = new JSONArray();
         String inquiryTypeURL =
             RestHelper.getRIGHT_NOW_URL() + "/queryResults?query=select%20*%20from%20Incident." + URL;

         try {
             int idColNum = 0;
             int lookupNameColNum = 0;

             JSONObject inquiryTypeObj =
                 httpGET(inquiryTypeURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
             JSONArray columnNames =
                 inquiryTypeObj.getJSONArray("columnNames");
             JSONArray rows = inquiryTypeObj.getJSONArray("rows");

             for (int i = 0; i < columnNames.length(); i++) {
                 if (columnNames.get(i).equals("id"))
                     idColNum = i;
                 if (columnNames.get(i).equals("lookupName"))
                     lookupNameColNum = i;
             }

             for (int i = 0; i < rows.length(); i++) {
                 JSONObject obj = new JSONObject();
                 obj.put("id", rows.getJSONArray(i).get(idColNum));
                 try{
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[0]);
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[1]);
                 }catch(ArrayIndexOutOfBoundsException ae){
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum));
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum));
                 }
                 responseArr.put(i, obj);
             }
         } catch (Exception e) {
             e.printStackTrace();
         }

         return responseArr;
     }
    public JSONArray getInquiryType() throws Exception {

        JSONArray respone = null;
        //// Caching
        CacheManager cm = CacheManager.getInstance();

        Cache cache = cm.getCache("InquiryServiceCache");
        Element ele = cache.get("InquiryType");

        if (ele == null) {
            respone = getInquiryArr();
            cache.put(new Element("InquiryType", respone.toString()));
        } else {
            String output =
                (ele == null ? null : ele.getObjectValue().toString());
            respone = new JSONArray(output);
        }

        return respone;
    }
    public JSONObject addInquiry(String body) throws Exception {

        String lookupName = "lookupName";

        JSONObject bodyObj = new JSONObject(body);
        int personId = Integer.parseInt(bodyObj.get("personId").toString());
        String inquiryType = bodyObj.get("firstValue").toString();
        String lookupValue = bodyObj.get("secValue").toString();
        String inquiryDetails = bodyObj.get("detailsValue").toString();

        //////////////////////////// Sample payload ////////////////////////////
//        {
        //        "primaryContact": {
        //        "id" : 13
        //        }   ,
        //        "customFields": {
        //        "c": {
        //              "incidenttype": {
        //                        "lookupName": "Inqury"
        //                    },
        //                      "inquirytype": {
        //                        "lookupName": "After Sales General Info"}
        //                    ,
        //
        //                "pcorbajaj": {
        //                        "lookupName": "PC"
        //                    },
        //          "inquiry_details": "test"
        //         }
        //        },
        //            "statusWithType": {
        //                "status":{
        //        "lookupName": "Waiting"}
        //
        //            },
        //         "subject": "test"
        //        }

        //Main payload
        JSONObject payload = new JSONObject();

        JSONObject primaryContact = new JSONObject();
        primaryContact.put("id", personId);

        JSONObject customFields = new JSONObject();

        JSONObject c = new JSONObject();
        JSONObject incidenttype = new JSONObject();
        incidenttype.put(lookupName, "Inqury");
        JSONObject inquiry_type = new JSONObject();
        inquiry_type.put(lookupName, inquiryType);
        JSONObject inquiry_type_depend = new JSONObject();
        inquiry_type_depend.put(lookupName, lookupValue);

        c.put("incidenttype", incidenttype);
        c.put("inquirytype", inquiry_type);
        c.put("inquiry_details", inquiryDetails);

        JSONObject statusWithType = new JSONObject();
        JSONObject status = new JSONObject();
        status.put(lookupName, "Waiting");
        statusWithType.put("status", status);

        switch (inquiryType) {
            case "After Sales General Info":
                c.put("pcorbajaj", inquiry_type_depend);
                break;
            default:
                System.out.println("default");
        }

        customFields.put("c", c);
        payload.put("primaryContact", primaryContact);
        payload.put("customFields", customFields);
        payload.put("statusWithType", statusWithType);
        payload.put("subject", "test");

        System.out.println(payload);
        
        return httpPOST(RestHelper.getRIGHT_NOW_URL() + RestHelper.getRIGHT_NOW_INCIDENTS(), payload.toString());
        
    }
}
