package com.appspro.fusionsshr.handler;

import common.restHelper.GenricHandler;
import common.restHelper.RestHelper;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.json.JSONArray;
import org.json.JSONObject;

public class ServiceHandler  extends GenricHandler{
    public JSONArray getServicesArr() {

         JSONArray responseArr = new JSONArray();
         String serviceTypeURL =
             RestHelper.getRIGHT_NOW_URL() + "/queryResults?query=select%20*%20from%20Incident.Service_Types";

         try {
             int idColNum = 0;
             int lookupNameColNum = 0;

             JSONObject serviceTypeObj =
                 httpGET(serviceTypeURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
             JSONArray columnNames =
                 serviceTypeObj.getJSONArray("columnNames");
             JSONArray rows = serviceTypeObj.getJSONArray("rows");

             for (int i = 0; i < columnNames.length(); i++) {
                 if (columnNames.get(i).equals("id"))
                     idColNum = i;
                 if (columnNames.get(i).equals("lookupName"))
                     lookupNameColNum = i;
             }

             for (int i = 0; i < rows.length(); i++) {
                 JSONObject obj = new JSONObject();
                 obj.put("id", rows.getJSONArray(i).get(idColNum));
                 try{
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[0]);
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[1]);
                 }catch(ArrayIndexOutOfBoundsException ae){
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum));
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum));
                 }

                 switch (rows.getJSONArray(i).get(lookupNameColNum).toString()) {
                     case "After Sales Benefits":
                         obj.put("dependsObj", getDepends("AfterSalesBenefits"));
                         break;
                     case "Elite Door to Door":
                         obj.put("dependsObj", getDepends("Zones"));
                         break;
                     default:
                 }

                 obj.put("rsaLocation", rows.getJSONArray(i).get(lookupNameColNum).toString().equals("Road Assistance"));

                 responseArr.put(i, obj);
             }
         } catch (Exception e) {
             e.printStackTrace();
         }

         return responseArr;
     }
    public JSONArray getDepends(String URL) {

         JSONArray responseArr = new JSONArray();
         String serviceTypeURL =
             RestHelper.getRIGHT_NOW_URL() + "/queryResults?query=select%20*%20from%20Incident." + URL;

         try {
             int idColNum = 0;
             int lookupNameColNum = 0;

             JSONObject serviceTypeObj =
                 httpGET(serviceTypeURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
             JSONArray columnNames =
                 serviceTypeObj.getJSONArray("columnNames");
             JSONArray rows = serviceTypeObj.getJSONArray("rows");

             for (int i = 0; i < columnNames.length(); i++) {
                 if (columnNames.get(i).equals("id"))
                     idColNum = i;
                 if (columnNames.get(i).equals("lookupName"))
                     lookupNameColNum = i;
             }

             for (int i = 0; i < rows.length(); i++) {
                 JSONObject obj = new JSONObject();
                 obj.put("id", rows.getJSONArray(i).get(idColNum));
                 try{
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[0]);
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[1]);
                 }catch(ArrayIndexOutOfBoundsException ae){
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum));
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum));
                 }
                 responseArr.put(i, obj);
             }
         } catch (Exception e) {
             e.printStackTrace();
         }

         return responseArr;
     }
    public JSONArray getServiceType() throws Exception {

        JSONArray respone = null;
        //// Caching
        CacheManager cm = CacheManager.getInstance();

        Cache cache = cm.getCache("ServiceServiceCache");
        Element ele = cache.get("ServiceType");

        if (ele == null) {
            respone = getServicesArr();
            cache.put(new Element("ServiceType", respone.toString()));
        } else {
            String output =
                (ele == null ? null : ele.getObjectValue().toString());
            respone = new JSONArray(output);
        }

        return respone;
    }
    public JSONObject addService(String body) throws Exception {

        String lookupName = "lookupName";

        JSONObject bodyObj = new JSONObject(body);
        int personId = Integer.parseInt(bodyObj.get("personId").toString());
        String serviceType = bodyObj.get("firstValue").toString();
        String lookupValue = bodyObj.get("secValue").toString();
        String rsaLocation = bodyObj.get("rsaLocation").toString();
        String serviceDetails = bodyObj.get("detailsValue").toString();

        //////////////////////////// Sample payload ////////////////////////////
//        {
// "primaryContact": {
        // "id" : 11
        // }   ,
        // "customFields": {
        // "c": {
        // 	     "incidenttype": {"lookupName": "Service"},
        //          "servicetype": {"lookupName": "Road Assistance"},
        //           "rsa_location_text": "Location One",
        //           "after_sales_benefits": {"lookupName": "Elite Membership"},
        //           "zones": {"lookupName": "Zone One"},
        //           "servicedetails":"xx"
        //  }
        // },
        //     "statusWithType": {
        //         "status": {
        //             "lookupName": "Waiting"
        //         }
        //
        //     },
        //  "subject": "test"
        // }


        //Main payload
        JSONObject payload = new JSONObject();

        JSONObject primaryContact = new JSONObject();
        primaryContact.put("id", personId);

        JSONObject customFields = new JSONObject();

        JSONObject c = new JSONObject();
        JSONObject incidenttype = new JSONObject();
        incidenttype.put(lookupName, "Service");
        JSONObject service_type = new JSONObject();
        service_type.put(lookupName, serviceType);
        JSONObject service_type_depend = new JSONObject();
        service_type_depend.put(lookupName, lookupValue);

        c.put("incidenttype", incidenttype);
        c.put("servicetype", service_type);
        c.put("servicedetails", serviceDetails);

        JSONObject statusWithType = new JSONObject();
        JSONObject status = new JSONObject();
        status.put(lookupName, "Waiting");
        statusWithType.put("status", status);

        switch (serviceType) {
            case "Road Assistance":
                c.put("rsa_location_text", rsaLocation);
                break;
            case "After Sales Benefits":
                c.put("after_sales_benefits", service_type_depend);
                break;
            case "Elite Door to Door":
                c.put("zones", service_type_depend);
                break;
            default:
                System.out.println("default");
        }

        customFields.put("c", c);
        payload.put("primaryContact", primaryContact);
        payload.put("customFields", customFields);
        payload.put("statusWithType", statusWithType);
        payload.put("subject", "test");

        System.out.println(payload);
        
        return httpPOST(RestHelper.getRIGHT_NOW_URL() + RestHelper.getRIGHT_NOW_INCIDENTS(), payload.toString());
        
    }
}
