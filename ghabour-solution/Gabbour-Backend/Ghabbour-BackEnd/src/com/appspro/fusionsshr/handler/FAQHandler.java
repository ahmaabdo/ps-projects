package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.FaqBean;

import com.appspro.fusionsshr.dao.FaqDAO;

import common.restHelper.GenricHandler;

import java.util.ArrayList;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.json.JSONArray;

public class FAQHandler extends GenricHandler{
   FaqDAO dao=new FaqDAO();
    public JSONArray getAllFaqs(){
            ArrayList<FaqBean> list = new ArrayList<FaqBean>();
            list = dao.getAllFaqs();
            return new JSONArray(list);
        }
    public JSONArray getFaqsType() throws Exception {

        JSONArray respone = null;
        //// Caching
        CacheManager cm = CacheManager.getInstance();

        Cache cache = cm.getCache("FaqCache");
        Element ele = cache.get("FaqType");

        if (ele == null) {
            respone = getAllFaqs();
            cache.put(new Element("FaqType", respone.toString()));
        } else {
            String output =
                (ele == null ? null : ele.getObjectValue().toString());
            respone = new JSONArray(output);
        }

        return respone;
    }
}
