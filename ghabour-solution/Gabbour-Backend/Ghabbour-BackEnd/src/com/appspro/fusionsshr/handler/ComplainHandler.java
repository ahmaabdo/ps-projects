package com.appspro.fusionsshr.handler;

import common.restHelper.GenricHandler;

import common.restHelper.RestHelper;

import java.net.URL;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.json.JSONArray;
import org.json.JSONObject;

public class ComplainHandler extends GenricHandler {
    
    public JSONArray getComplaintsArr() {

         JSONArray responseArr = new JSONArray();
         String complaintTypeURL =
             RestHelper.getRIGHT_NOW_URL() + "/queryResults?query=select%20*%20from%20Incident.complaint_type";

         try {
             int idColNum = 0;
             int lookupNameColNum = 0;

             JSONObject complaintTypeObj =
                 httpGET(complaintTypeURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
             JSONArray columnNames =
                 complaintTypeObj.getJSONArray("columnNames");
             JSONArray rows = complaintTypeObj.getJSONArray("rows");

             for (int i = 0; i < columnNames.length(); i++) {
                 if (columnNames.get(i).equals("id"))
                     idColNum = i;
                 if (columnNames.get(i).equals("lookupName"))
                     lookupNameColNum = i;
             }

             for (int i = 0; i < rows.length(); i++) {
                 JSONObject obj = new JSONObject();
                 obj.put("id", rows.getJSONArray(i).get(idColNum));

                 try{
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[0]);
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[1]);
                 }catch(ArrayIndexOutOfBoundsException ae){
                     obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum));
                     obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum));
                 }

                 switch (rows.getJSONArray(i).get(lookupNameColNum).toString()) {
                     case "Service Center Complaint":
                         obj.put("dependsObj", getDepends("Service_Centers"));
                         break;
                     case "Parts Complaint":
                         obj.put("dependsObj", getDepends("Outlets"));
                         break;
                     case "Sales Complaint":
                         obj.put("dependsObj", getDepends("Showrooms"));
                         break;
                     case "Distributor Complaint":
                         obj.put("dependsObj", getDepends("Dealers"));
                         break;
                     case "360 Complaint":
                         obj.put("dependsObj", getDepends("Location360"));
                         break;
                     default:
                 }

                 responseArr.put(i, obj);
             }
         } catch (Exception e) {
             e.printStackTrace();
         }

         return responseArr;
     }
    
   public JSONArray getDepends(String URL) {

        JSONArray responseArr = new JSONArray();
        String complaintTypeURL =
            RestHelper.getRIGHT_NOW_URL() + "/queryResults?query=select%20*%20from%20Incident." + URL;

        try {
            int idColNum = 0;
            int lookupNameColNum = 0;

            JSONObject complaintTypeObj =
                httpGET(complaintTypeURL).getJSONObject("data").getJSONArray("items").getJSONObject(0);
            JSONArray columnNames =
                complaintTypeObj.getJSONArray("columnNames");
            JSONArray rows = complaintTypeObj.getJSONArray("rows");

            for (int i = 0; i < columnNames.length(); i++) {
                if (columnNames.get(i).equals("id"))
                    idColNum = i;
                if (columnNames.get(i).equals("lookupName"))
                    lookupNameColNum = i;
            }

            for (int i = 0; i < rows.length(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", rows.getJSONArray(i).get(idColNum));
                try{
                    obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[0]);
                    obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum).toString().split("-")[1]);
                }catch(ArrayIndexOutOfBoundsException ae){
                    obj.put("lookupName", rows.getJSONArray(i).get(lookupNameColNum));
                    obj.put("lookupNameAr", rows.getJSONArray(i).get(lookupNameColNum));
                }
                responseArr.put(i, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseArr;
    }
   
    public JSONArray getComplainType() throws Exception {

        JSONArray respone = null;
        //// Caching
        CacheManager cm = CacheManager.getInstance();

        Cache cache = cm.getCache("ComplainServiceCache");
        Element ele = cache.get("ComplainType");

        if (ele == null) {
            respone = getComplaintsArr();
            cache.put(new Element("ComplainType", respone.toString()));
        } else {
            String output =
                (ele == null ? null : ele.getObjectValue().toString());
            respone = new JSONArray(output);
        }

        return respone;
    }
    public JSONObject addNewComplain(String body) throws Exception {

        String lookupName = "lookupName";

        JSONObject bodyObj = new JSONObject(body);
        int personId = Integer.parseInt(bodyObj.get("personId").toString());
        String complaintType = bodyObj.get("firstValue").toString();
        String lookupValue = bodyObj.get("secValue").toString();
        String complaintDetails = bodyObj.get("detailsValue").toString();

        //////////////////////////// Sample payload ////////////////////////////
        //        {
        //            "primaryContact": {
        //                "id": 14
        //            },
        //            "customFields": {
        //                "c": {
        //                    "incidenttype": {
        //                        "lookupName": "Complaint"
        //                    },
        //                    "complaint_type": {
        //                        "lookupName": "Service Center Complaint"
        //                    },
        //                    "servicecenter": { "lookupName": "Service Center One" },
        //                    "outlets": { "lookupName": "OutLet One" },
        //                    "showrooms": { "lookupName": "Showroom One" },
        //                    "dealers": { "lookupName": "Dealer One" },
        //                    "location_360": { "lookupName": "Location One" },
        //                    "complaint_details": "complaint description field"
        //                }
        //            },
        //            "statusWithType": {
        //                "status": {
        //                    "lookupName": "Waiting"
        //                }
        //            },
        //            "subject": "test"
        //        }

        //Main payload
        JSONObject payload = new JSONObject();

        JSONObject primaryContact = new JSONObject();
        primaryContact.put("id", personId);

        JSONObject customFields = new JSONObject();

        JSONObject c = new JSONObject();
        JSONObject incidenttype = new JSONObject();
        incidenttype.put(lookupName, "Complaint");
        JSONObject complaint_type = new JSONObject();
        complaint_type.put(lookupName, complaintType);
        JSONObject complaint_type_depend = new JSONObject();
        complaint_type_depend.put(lookupName, lookupValue);

        c.put("incidenttype", incidenttype);
        c.put("complaint_type", complaint_type);
        c.put("complaint_details", complaintDetails);

        JSONObject statusWithType = new JSONObject();
        JSONObject status = new JSONObject();
        status.put(lookupName, "Waiting");
        statusWithType.put("status", status);

        switch (complaintType) {
            case "Service Center Complaint":
                c.put("servicecenter", complaint_type_depend);
                break;
            case "Parts Complaint":
                c.put("outlets", complaint_type_depend);
                break;
            case "Sales Complaint":
                c.put("showrooms", complaint_type_depend);
                break;
            case "Distributor Complaint":
                c.put("dealers", complaint_type_depend);
                break;
            case "360 Complaint":
                c.put("location_360", complaint_type_depend);
                break;
            default:
        }

        customFields.put("c", c);
        payload.put("primaryContact", primaryContact);
        payload.put("customFields", customFields);
        payload.put("statusWithType", statusWithType);
        payload.put("subject", "test");
        
        System.out.println(payload);

        return httpPOST(RestHelper.getRIGHT_NOW_URL() + RestHelper.getRIGHT_NOW_INCIDENTS(), payload.toString());
        
    }
}
