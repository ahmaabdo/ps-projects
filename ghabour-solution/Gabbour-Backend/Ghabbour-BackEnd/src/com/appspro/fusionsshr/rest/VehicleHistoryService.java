package com.appspro.fusionsshr.rest;
import com.appspro.fusionsshr.handler.VehicleHistoryHandler;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
@Path("/vehicleHistory")
public class VehicleHistoryService {
    
    private VehicleHistoryHandler handler = new VehicleHistoryHandler();
    @GET
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    public Response getVehicle(@PathParam("userId") String userId) {
        JSONArray reponse = null;
        try {
            reponse = handler.getVehiclesByUserId(userId);
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}