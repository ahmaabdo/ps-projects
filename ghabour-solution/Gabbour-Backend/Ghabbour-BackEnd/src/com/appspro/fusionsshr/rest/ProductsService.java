package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.ComplainHandler;

import com.appspro.fusionsshr.handler.ProductsHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/productsService")
public class ProductsService {

    private ProductsHandler handler = new ProductsHandler();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllProducts() {
        JSONObject reponse = null;
        try {
            reponse = handler.getAllProducts();
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
    @GET
    @Path("/{model}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allImages(@PathParam("model")
        String model) {
        return handler.getAllImages(model).toString();
    }
    @GET
    @Path("model/{model}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getProductId(@PathParam("model")
       String model) {
        return handler.getProductId(model).toString();
    }
}
