package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.FAQHandler;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/FAQService")
public class FAQService {
    FAQHandler handler=new FAQHandler();
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFAQType() {
        JSONArray reponse = null;
        try {
            reponse = handler.getFaqsType();
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}
