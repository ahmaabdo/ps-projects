package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.AttachmentBean;

import com.appspro.fusionsshr.dao.AttachmentDAO;

import com.appspro.fusionsshr.handler.AttachmentHandler;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("attachment")
public class AttachmentRest {
    AttachmentHandler handler = new AttachmentHandler();

    @POST
    @Path("/addImg")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertAttachment(String body) {
        return handler.insertAttachment(body).toString();
    }
    @GET
    @Path("/{ss_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allAttachment(@PathParam("ss_id")
        String ss_id) {
        return handler.getAllAttachment(ss_id).toString();
    }
    @POST
    @Path("/editImg")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateAttachment(String body) {
        return handler.updateAttachment(body).toString();
    }
}
