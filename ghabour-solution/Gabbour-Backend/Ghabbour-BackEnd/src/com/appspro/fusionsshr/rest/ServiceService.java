package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.ServiceHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/serviceService")
public class ServiceService {
    private ServiceHandler handler = new ServiceHandler();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNewInquiry(String body) {

        JSONObject reponse = null;
        try {
            reponse = handler.addService(body);
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
    @GET
    @Path("/serviceType")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServiceType() {
        JSONArray reponse = null;
        try {
            reponse = handler.getServiceType();
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}
