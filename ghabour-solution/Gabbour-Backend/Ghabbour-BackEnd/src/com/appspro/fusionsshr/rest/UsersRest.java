package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.UsersHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsersRest {
    UsersHandler handler=new UsersHandler();
    @GET
    @Path("/getUserDetails/{username}/{password}")
    public String getUserDetails(@PathParam("username" )String username, @PathParam("password")String password) {
        return handler.getAllUsers(username,password).toString();
    }
    
    @GET
    @Path("/getUser/{id}")
    public String getUser( @PathParam("id" )int id) {
        return handler.getUserById(id).toString();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/is-username-valid")
    public String getUserName(String username) {
        return  handler.isValidUsername(username).toString();
    }
    @POST
    @Path("/forgetPass")
    public String forgetPassword(String body) {
        return handler.forgetPass(body).toString();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/is-email-valid")
    public String getUserEmail(String email) {
        return  handler.isValidMail(email).toString();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/isContinueWithGoogle")
    public String getContinueWithGoogle(String email) {
        return  handler.isContinueWithGoogle(email).toString();
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/is-phone-valid")
    public String getUserPhone(String phone) {
        return handler.isValidPhone(phone).toString();
    }

    @POST
    @Path("/{transactionType}")
    public String insertOrUpdateUser( String body, @PathParam("transactionType")
        String transactionType) {
        return handler.insertUser(body,transactionType).toString();
    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/updatePhoneGoogle")
    public String UpdateUserGoogle( String body) {
        return handler.updateUserGooglePhone(body).toString();
    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/editUserInfo")
    public String UpdateUser( String body) {
        return handler.updateUser(body).toString();
    }

    @POST
    @Path("/editPass")
    public String UpdateUserPassword(String body) {
        return handler.changeUserPass(body).toString();
    }
    
    @POST
    @Path("/editDefaultBrand")
    public String UpdateUserDefaultBrand(String body) {
        return handler.userDefaultBrand(body).toString();
    }
    @POST
    @Path("/editVehicleFlag")
    public String UpdateUserVehicleFlag(String body) {
        return handler.userVehicleFlag(body).toString();
    }
    
    @POST
    @Path("/editDefaultLang")
    public String UpdateUserDefaultLang(String body) {
        return handler.userDefaultLang(body).toString();
    }
    
    @GET
    @Path("/getUserData/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserData(@PathParam("userId") String userId) {

        JSONObject reponse = null;
        try {
            reponse = handler.getUserData(userId);
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}
