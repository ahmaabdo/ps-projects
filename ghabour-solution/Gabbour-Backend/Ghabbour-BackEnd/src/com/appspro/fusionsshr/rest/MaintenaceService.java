package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.MaintenaceHandler;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("/maintenaceService")
public class MaintenaceService {
    private MaintenaceHandler handler = new MaintenaceHandler();

    @GET
    @Path("/events")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEvents() {
        JSONArray reponse = null;
        try {
            reponse = handler.getPartsArr();
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

}
