package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.ComplainHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/complainService")
public class ComplainService {

    private ComplainHandler handler = new ComplainHandler();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNewComplain(String body) {

        JSONObject reponse = null;
        try {
            reponse = handler.addNewComplain(body);
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/complainsType")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getComplainsType() {
        JSONArray reponse = null;
        try {
            reponse = handler.getComplainType();
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}
