package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.ComplainHandler;

import com.appspro.fusionsshr.handler.TestDrivceHandler;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("/testDriveService")
public class TestDrivceService {
    private TestDrivceHandler handler = new TestDrivceHandler();
    @GET
    @Path("/testDriveType")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getComplainsType() {
        JSONArray reponse = null;
        try {
            reponse = handler.getTestDriveArr();
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}
