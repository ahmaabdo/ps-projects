package com.appspro.fusionsshr.rest;

import common.restHelper.RestHelper;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import org.json.JSONObject;

@Path("/customRest")
public class RightNowService {

    private static RightNowService instance;

    public static RightNowService getInstance() {
        if (instance == null)
            instance = new RightNowService();
        return instance;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response RightNowResponse(String body) {
        JSONObject res = new JSONObject();
        try {
            JSONObject jsonBody = new JSONObject(body);

            String url = jsonBody.get("url").toString();
            String methodType = jsonBody.get("restMethod").toString();
            String payload = jsonBody.get("payload").toString();
//            String hasMore = "";

            String serverUrl =
                (RestHelper.getInstance().getRIGHT_NOW_URL() + "/" +
                 url).replace(" ", "%20");

            System.out.println(serverUrl);
            res = RestHelper.getInstance().callRest(serverUrl, methodType, payload);
            
//            try {
//                hasMore = res.getJSONObject("data").get("hasMore").toString();
//                if (hasMore == "true") {
//                } else if (hasMore == "false") {
//                }
//            } catch (JSONException je) {
//            }
            
            return Response.ok(res.toString()).build();

        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }

    }
}
