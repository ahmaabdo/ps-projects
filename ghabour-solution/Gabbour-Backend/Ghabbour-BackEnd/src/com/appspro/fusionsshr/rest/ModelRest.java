package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.handler.ModelHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/modelService")

public class ModelRest {
    private ModelHandler handler = new ModelHandler();
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allImages() {
        return handler.getAllImages().toString();
    }
    @GET
    @Path("model")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getModel(@PathParam("model")
        String model) {
        return handler.getModelImg(model).toString();
    }
}
