package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.VehicleHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/myVehicle")

public class VehicleRest {
    private VehicleHandler handler=new VehicleHandler();
    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allVehicles(@PathParam("id" )int id) {
        return handler.getAllVehicles(id).toString();
    }
    @POST
    @Path("/addVehicle")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertVehicle(String body) {
        return handler.insertVehicle(body).toString();
    }
    @POST
    @Path("/getPhone")
    @Consumes(MediaType.APPLICATION_JSON)

    public String getPhone(String body) {
        return "01204990009";
    }
    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteVehicle(String body) {
        return handler.deleteVehicle(body).toString();
    }
}
