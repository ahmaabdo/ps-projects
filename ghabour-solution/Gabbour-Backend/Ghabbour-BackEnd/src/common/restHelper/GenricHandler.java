package common.restHelper;

import org.json.JSONObject;

public class GenricHandler {
    
    private RestHelper restHelper = new RestHelper();


    public JSONObject httpGET(String serverUrl) throws Exception {
        return restHelper.callRest(serverUrl, "GET", "null");
    }

    public JSONObject httpPOST(String serverUrl,
                               String body) throws Exception {
        return restHelper.callRest(serverUrl, "POST", body);
    }
}
