package common.biPReports.sync;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;



public class Listener implements ServletContextListener {
    private ServletContext context = null;

    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        
    }

    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
    }
}
