package common.biPReports.sync;

public class MasterDataReportJob {
    
}
//import com.appspro.db.AppsproConnection;
//import com.appspro.fusionsshr.bean.LookUpBean;
//import com.appspro.fusionsshr.rest.BIReportService;
//
//import com.appspro.fusionsshr.rest.RightNowService;
//
//import static common.restHelper.RestHelper.getSchema_Name;
//import java.io.UnsupportedEncodingException;
//
//import java.sql.CallableStatement;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.ws.rs.core.Response;
//
//import org.apache.commons.codec.binary.Base64;
//
//import org.apache.commons.codec.binary.StringUtils;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import org.quartz.Job;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//
//public class MasterDataReportJob extends AppsproConnection implements Job {
//
////    PaaSReportsModel paaSBean = new PaaSReportsModel();
//    BIReportService biReport = new BIReportService();
//    Base64 base64 = new Base64();
//    Connection connection;
//    PreparedStatement ps;
//    CallableStatement cs;
//    ResultSet rs;
//
//    public LookUpBean addLookUps(LookUpBean bean) {
//        try {
//            connection = AppsproConnection.getConnection();
//            String query = "BEGIN\n" +
//                "UPDATE " + getSchema_Name() +
//                ".XXX_MASTER_DATA_LOOKUPS SET UPDATED_DATE=sysdate WHERE VALUE=?;\n" +
//                "IF SQL%ROWCOUNT = 0 THEN  \n" +
//                "RAISE NO_DATA_FOUND;\n" +
//                "END IF;\n" +
//                "EXCEPTION\n" +
//                "  WHEN NO_DATA_FOUND THEN\n" +
//                "INSERT INTO " + getSchema_Name() +
//                ".XXX_MASTER_DATA_LOOKUPS (LOOKUP_NAME,VALUE,LABEL_EN,LABEL_AR,DEPEND_VALUE,CREATION_DATE,UPDATED_DATE) VALUES(?,?,?,?,?,sysdate,sysdate);\n" +
//                "END;";
//            ps = connection.prepareStatement(query);
//
//            ps.setString(1, bean.getValue());
//            ps.setString(2, bean.getLookupName());
//            ps.setString(3, bean.getValue());
//            ps.setString(4, bean.getLabelEn());
//            ps.setString(5, bean.getLabelAr());
//            ps.setString(6, bean.getDependValue());
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            closeResources(connection, ps, rs);
//        }
//        return bean;
//    }
//
////    public ArrayList<SummaryReportBean> getAllReports() {
////
////        ArrayList<SummaryReportBean> SummaryReportList =
////            new ArrayList<SummaryReportBean>();
////        try {
////            connection = AppsproConnection.getConnection();
////            String query = "select * from " + getSchema_Name() + ".XXX_Report";
////            ps = connection.prepareStatement(query);
////            rs = ps.executeQuery();
////            while (rs.next()) {
////                SummaryReportBean summaryReport = new SummaryReportBean();
////                summaryReport.setReport_Name(rs.getString("REPORT_NAME"));
////                summaryReport.setParameter1(rs.getString("PARAMETER1"));
////                summaryReport.setParameter2(rs.getString("PARAMETER2"));
////                summaryReport.setParameter3(rs.getString("PARAMETER3"));
////                summaryReport.setParameter4(rs.getString("PARAMETER4"));
////                summaryReport.setParameter5(rs.getString("PARAMETER5"));
////                SummaryReportList.add(summaryReport);
////            }
////
////        } catch (Exception e) {
////            e.printStackTrace();
////        } finally {
////            closeResources(connection, ps, rs);
////        }
////        return SummaryReportList;
////    }
//
//    @Override
//    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
//        try {
//            //JOB code starts here...
//            InsertMasterData();
//        } catch (Exception e) {
//            System.out.println("Error: No data found...");
//            e.printStackTrace();
//        }
//    }
//
////    public void InsertStaticReport(String reportBody, String reportName,
////                                   String param1) {
////        String reportResponse = biReport.getBiReport(reportBody);
////            paaSBean.setReportName(reportName);
////            paaSBean.setParam1(param1);
////            paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes())));
////            addPaaSReport(paaSBean);
////    }
//
//    public void InsertMasterData() throws UnsupportedEncodingException {
////        
////        
////        List<SummaryReportBean> reportBeanArr = new ArrayList<>();
////        reportBeanArr = getAllReports();
////        JSONArray reportJSONArr = new JSONArray(reportBeanArr);
//        
////        String url = "select * from Incident.complaint_type";
////        Response response =
////            RightNowService.getInstance().RightNowResponse("{ \"restMethod\":\"GET\", \"url\":+" + url + "+, \"payload\":\"null\" }");
////        
////        try {
////            JSONObject responseBody = new JSONObject(response.getEntity().toString());
////            JSONArray responseArr = new JSONArray(responseBody.getJSONArray("items"));
////
////            for (int i = 0; i < responseArr.length(); i++) {
////                
////                
////                }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        JSONArray eitNameArr =
////            new JSONArray(biReport.getBiReport("{\"reportName\":\"EitNameReport\",\"LANG\":\"US\"}"));
////
////        for (int i = 0; i < eitNameArr.length(); i++) {
////            JSONObject mObject = eitNameArr.getJSONObject(i);
////            String eitDataNameBody =
////                "{\"reportName\":\"EIT Report\",\"LANG\":\"US\", \"Structure_Code\":\"" +
////                mObject.optString("DESCRIPTIVE_FLEX_CONTEXT_CODE") + "\"}";
////
////            JSONArray eitDataNameArr =
////                new JSONArray(biReport.getBiReport(eitDataNameBody));
////            for (int j = 0; j < eitDataNameArr.length(); j++) {
////                JSONObject eitDataObject = eitDataNameArr.getJSONObject(j);
////                String inDependentReport =
////                    "{\"reportName\":\"inDependentReport\", \"valueSet\":\"" +
////                    eitDataObject.optString("FLEX_value_SET_NAME") +
////                    "\",\"langCode\":\"US\"}";
////                System.out.println(inDependentReport);
////                String reportResponse =
////                    biReport.getBiReport(inDependentReport);
////                    paaSBean.setReportName("inDependentReport");
////                    paaSBean.setParam1(eitDataObject.optString("FLEX_value_SET_NAME"));
////                    paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes("UTF-8")),"UTF-8"));
////                   // paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes())));
////                    addPaaSReport(paaSBean);
////            }
////        }
//
////        for (int i = 0; i < reportJSONArr.length(); i++) {
////            JSONObject object = reportJSONArr.getJSONObject(i);
////            boolean isParamsEmpty =
////                object.optString("parameter1").isEmpty() && object.optString("parameter2").isEmpty() &&
////                object.optString("parameter3").isEmpty() &&
////                object.optString("parameter4").isEmpty() &&
////                object.optString("parameter5").isEmpty();
////
////            if (isParamsEmpty) {
////                try {
////                    String body =
////                        "{\"reportName\":\"" + object.optString("report_Name") +
////                        "\"}";
////
////                    String reportResponse = biReport.getBiReport(body);
////                        paaSBean.setReportName(object.optString("report_Name"));
////                        paaSBean.setParam1(object.optString("report_Name"));
////                        paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes("UTF-8")),"UTF-8"));
////                    //  paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes())));
////                        addPaaSReport(paaSBean);
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////            }
////        }
//
//
////        JSONArray personIdArr =
////            new JSONArray(biReport.getBiReport("{\"reportName\":\"PERSON_ID_REPORT\"}"));
////    
////        
////        for (int i = 0; i < reportJSONArr.length(); i++) {
////            JSONObject mObject2 = personIdArr.getJSONObject(i);
////            JSONObject object = reportJSONArr.getJSONObject(i);
////            boolean isParamsEmpty =
////                object.optString("parameter1").equalsIgnoreCase("personId")&& object.optString("parameter2").isEmpty();
////
////            if (isParamsEmpty) {
////                try {
////                        for(int j = 0 ; j < personIdArr.length();j++) {
////                        String body = "{\"reportName\":\""+object.optString("report_Name")+"\", \"personId\":\"" + personIdArr.getJSONObject(j).optString("value") + "\"}";
////                        System.out.println("report body " +body);
////                                String reportResponse = biReport.getBiReport(body);
////
////                                   paaSBean.setReportName(object.optString("report_Name"));
////                                   paaSBean.setParam1(personIdArr.getJSONObject(j).optString("value"));
////                                   paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes("UTF-8")),"UTF-8"));
////                               //  paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes())));
////                                   addPaaSReport(paaSBean);
////                                                
////                    }
////                
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////            }
////        }
//        
//        
////        JSONArray countiresArr =
////            new JSONArray(biReport.getBiReport("{\"reportName\":\"inDependentReport\" , \"valueSet\":\"XXX_HR_TA_COUNTIRES\",\"langCode\":\"US\"}"));
////			
////        for (int x = 0; x < countiresArr.length(); x++) {  
////		
////                    JSONObject DecObject = countiresArr.getJSONObject(x);
////	   
////                   try {
////                        String body = "{\"reportName\":\"XXX_HR_TA_CITEIS\" , \"toCountry\":\"" + countiresArr.getJSONObject(x).optString("FLEX_value") + "\"}";
////                        System.out.println("report body" +body);
////                                String reportResponse = biReport.getBiReport(body);
////
////                                 paaSBean.setReportName("XXX_HR_TA_CITEIS");
////                                 paaSBean.setParam1(countiresArr.getJSONObject(x).optString("FLEX_value"));
////                                 paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes("UTF-8")),"UTF-8"));
////                             //  paaSBean.setResponse(new String(base64.encode(reportResponse.getBytes())));
////                                addPaaSReport(paaSBean);
////                                                
////                    
////                
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////			
////		
////		}
//        
////    
////          InsertStaticReport("{\"reportName\":\"inDependentReport\" , \"valueSet\":\"XXX_HR_TRAVEL_DECTINATION\",\"langCode\":\"US\"}",
////                           "inDependentReport", "XXX_HR_TRAVEL_DECTINATION");
////        
////          InsertStaticReport("{\"reportName\":\"XXX_HR_TA_CITEIS\" }","XXX_HR_TA_CITEIS", "XXX_HR_TA_CITEIS");
////        
//
//
//    }
//
////    public static void main(String... args) throws UnsupportedEncodingException {
////        PaaSReportJob paas = new PaaSReportJob();
////        paas.InsertReportsIntoPaaS();
////    }

//}