/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common.biPReports;


import com.appspro.db.CommonConfigReader;

import common.restHelper.RestHelper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;


/**
 *
 * @author user
 */
public class BIReportModel extends RestHelper {

    private static BIReportModel instance = null;
    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public static BIReportModel getInstance() {
        if(instance == null)
            instance = new BIReportModel();
        return instance;
    }


    public static enum REPORT_NAME {
        EXAMPLE("EXAMPLE");

        private String value;

        private REPORT_NAME(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum ATTRIBUTE_TEMPLATE {

        ATTRIBUTE_TEMPLATE_XML("DEFAULT"),
        ATTRIBUTE_TEMPLATE_PDF("PDF");
        private String value;

        private ATTRIBUTE_TEMPLATE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum ATTRIBUTE_FORMAT {

        ATTRIBUTE_FORMAT_XML("xml"),
        ATTRIBUTE_FORMAT_PDF("pdf"); //keep in small caps
        private String value;

        private ATTRIBUTE_FORMAT(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

   
    private String reportAbsolutePath;
    private String attributeFormat;
    private String attributeTemplate;
    private String parameters;
    private Map<String, String> paramMap = new HashMap<String, String>();
    private String soapRequest = null;
    private String username = CommonConfigReader.getValue("USER_NAME");
    private String password = CommonConfigReader.getValue("PASSWORD");
    private String segmentName;
    public static List<BIReportModel> THREAD_REPORT =
        new ArrayList<BIReportModel>();
    public static int THREAD_REPORT_COUNTER = -1;
    public static List<String> THREAD_REPORT_RESPONSE =
        new ArrayList<String>();
    //     private String username = "mahmoud.essam@appspro-me.com";
    //     private String password = "Welcome@123";

    public void setParamMap(Map<String, String> paramMap) {
        this.paramMap = paramMap;
    }

    public Map<String, String> getParamMap() {
        return paramMap;
    }

    public void setReportAbsolutePath(String reportAbsolutePath) {
        this.reportAbsolutePath = reportAbsolutePath;
    }

    public String getReportAbsolutePath() {
        return reportAbsolutePath;
    }

    public String getParameters() {
        Iterator it = paramMap.entrySet().iterator();
        parameters = "";
        while (it.hasNext()) {
            Map.Entry parameter = (Map.Entry)it.next();
            parameters = parameters + "             <pub:item>\n" +
                    "                <pub:name>" + parameter.getKey() +
                    "</pub:name>\n" +
                    "                 <pub:values>\n" +
                    "                    <pub:item>" + parameter.getValue() +
                    "</pub:item>\n" +
                    "                  </pub:values>\n" +
                    "             </pub:item>\n";
        }
        return parameters;
    }

    public String getSoapRequest() {
        this.soapRequest =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pub=\"http://xmlns.oracle.com/oxp/service/PublicReportService\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <pub:runReport>\n" +
                "         <pub:reportRequest>\n" +
                "            <pub:attributeLocale>en-US</pub:attributeLocale>\n" +
                "            <pub:attributeTemplate>" +
                this.attributeTemplate + "</pub:attributeTemplate>\n" +
                "            <pub:attributeFormat>" + this.attributeFormat +
                "</pub:attributeFormat>\n" +
                "            <pub:reportAbsolutePath>/Custom/SAAS Extension Reports/Reports/" +
                reportAbsolutePath + ".xdo</pub:reportAbsolutePath>\n" +
                "<pub:parameterNameValues>\n" +
                " \n" +
                getParameters() + "             </pub:parameterNameValues>\n" +
                " \n" +
                "         </pub:reportRequest>\n" +
                "         <pub:userID>" + this.username + "</pub:userID>\n" +
                "         <pub:password>" + this.password +
                "</pub:password>\n" +
                "      </pub:runReport>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        return soapRequest;
    }

    public byte[] executeReportsPDF() {
        String output = "";
        try {
            System.setProperty("DUseSunHttpHandler", "true");

            byte[] buffer = new byte[getSoapRequest().length()];
            buffer = getSoapRequest().getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            //TO_DO
            java.net.URL url =
                new URL(null, null/*this.getBiReportUrl()*/, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                //TO_DO
                //trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                //https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";
            //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            InputStream is = http.getInputStream();
            //TO_DO
            String responseXML = null;//getStringFromInputStream(is);

            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(responseXML));

            Document doc = builder.parse(src);
            String data =
                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            } else {
                output = data;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.getBytes();
    }

    public void setAttributeFormat(String attributeFormat) {
        this.attributeFormat = attributeFormat;
    }

    public void setAttributeTemplate(String attributeTemplate) {
        this.attributeTemplate = attributeTemplate;
    }

    public static JSONObject runReport(String reportName,
                                       Map<String, String> paramMap) {
        System.out.println(paramMap);
        BIReportModel biPReports = new BIReportModel();
        biPReports.setAttributeFormat(BIReportModel.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIReportModel.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
        biPReports.setReportAbsolutePath(reportName);

        biPReports.setParamMap(paramMap);

        String fresponse = biPReports.executeReports();
        //  System.out.print(fresponse);
        JSONObject xmlJSONObj = null;

        try {
            xmlJSONObj = XML.toJSONObject(fresponse.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(xmlJSONObj);

        return xmlJSONObj;
    }

    public String executeReports() {
//        String output = "";
//        try {
//            System.out.println("Invoke service using direct HTTP call with Basic Auth");
//
//            Document doc =
//                new RestHelper().httpPost(null/*this.getBiReportUrl()*/, getSoapRequest());
//
//            doc.getDocumentElement().normalize();
//
//            String data =
//                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
//            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
//                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
//            } else {
//                output = data;
//            }
//
//            return output;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return null;
    }

    public String executeReportsThread() {
//        String output = "";
//        try {
//            System.out.println("Invoke service using direct HTTP call with Basic Auth");
//
//            Document doc =
//                new RestHelper().httpPost(this.getBiReportUrl(), getSoapRequest());
//
//            doc.getDocumentElement().normalize();
//
//            String data =
//                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
//            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
//                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
//            } else {
//                output = data;
//            }
//
//            return segmentName + "SPLIT" + output;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        return segmentName + "SPLIT" + output;
        return null;
    }

    public List<String> runMultipleReports() throws InterruptedException,
                                                    ExecutionException {

        Map<String, String> responesMap = new HashMap<String, String>();

        System.out.println(new Date());
        int size = THREAD_REPORT.size();
        if (size < 1) {
            return new ArrayList<String>();
        }
        ExecutorService threads = Executors.newFixedThreadPool(size);
        List<Callable<String>> torun = new ArrayList(size);
        final CountDownLatch countdown = new CountDownLatch(size);
        for (int i = 0; i < size; i++) {
            torun.add(new Callable<String>() {
                    public String call() throws InterruptedException {
                        countdown.countDown();
                        countdown.await();
                        THREAD_REPORT_COUNTER++;
                        return THREAD_REPORT.get(THREAD_REPORT_COUNTER).executeReportsThread();
                    }
                });
        }

        // all tasks executed in different threads, at 'once'.
        List<Future<String>> futures = threads.invokeAll(torun);

        // no more need for the threadpool
        threads.shutdown();
        // check the results of the tasks...throwing the first exception, if any.
        for (Future<String> fut : futures) {
            THREAD_REPORT_RESPONSE.add(fut.get());
        }
        countdown.await();
        System.out.println(new Date());
        //        threads.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        //check the threadpool is now in fact complete
        if (!threads.isShutdown()) {
            // something went wrong... our accounting is off...
        }

        System.out.println(new Date());
        return THREAD_REPORT_RESPONSE;
    }

}
