define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils',
    'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojinputtext'],
        function (oj, ko, app, moduleUtils) {

            function OffersViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;

                self.labels = ko.observableArray([]);

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true,true, 'landing');
                };

                self.transitionCompleted = function () {
                    app.animationSwitcher();
                };
                
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {
                    app.headerTitle(getTranslation('main.offers'));
                    self.labels = {
                        offersLbl: ko.observable(getTranslation('main.offers'))};
                }
            }

            return new OffersViewModel();
        }
);
