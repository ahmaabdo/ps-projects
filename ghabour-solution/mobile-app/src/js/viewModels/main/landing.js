define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton'],
        function (oj, ko, app, moduleUtils) {

            function LandingViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable();

                self.signInClickAction = function () {
                    oj.Router.rootInstance.go('login');
                };
                self.faqsClickAction = function () {
                    oj.Router.rootInstance.go('faqs');
                };
                self.locationsClickAction = function () {
                    oj.Router.rootInstance.go('ourLocation');
                };
                self.productsClickAction = function () {
                    oj.Router.rootInstance.go('products');
                };
                self.switchLangAction = function () {
                    app.switchLanguage();
                    app.changeLang();
                };
                self.connected = function () {
                    // Implement if needed
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(false, false,false, null);
                };

                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                    app.globalHeaderConfig(false, false,false, null);
                };


                app.globalHeaderConfig(false, false,false, null);
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {
                    app.globalHeaderConfig(false, false,false, null);
                    app.headerTitle(getTranslation(''));
                    self.labels({
                        big: ko.observable(getTranslation("main.big")),
                        bigOffer: ko.observable(getTranslation("main.bigOffer")),
                        signInlbl: ko.observable(getTranslation("main.signIn")),
                        faqLbl: ko.observable(getTranslation("main.faq")),
                        ourLocationsLbl: ko.observable(getTranslation("main.ourLocation")),
                        productLbl: ko.observable(getTranslation("main.products")),
                        languageSwitchLbl: ko.observable(getTranslation("main.language"))});
                }
            }

            return new LandingViewModel();
        }
);
