define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'config/services', 'ojs/ojmessages',
    'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojinputtext'],
        function (oj, ko, app, moduleUtils, services) {

            function LoginViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable();
                self.userName = ko.observable();
                self.password = ko.observable();
                self.isUserLoggedIn = ko.observable(false);
                self.imagePath = ko.observable("");
                
                self.continueWithGoogle = function (data, event) {
                    app.continueWithGoogle();
                };
                self.fbLogin=function(){
                    app.checkLoginState();
                };
                //****************************************************//

                self.loginClickAction = function () {
                    app.login(self.userName().toLowerCase(), self.password());
                };

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true,true, 'landing');
                };


                /***************** refresh for language ******************/
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };

                function initTranslations() {
                    app.headerTitle(getTranslation('main.signIn'));
                    self.labels({
                        continueWithGoogle: ko.observable(getTranslation('main.continueWithGoogle')),
                        title: ko.observable(getTranslation('main.signIn')),
                        userNameLbl: ko.observable(getTranslation('main.userName')),
                        passwordLbl: ko.observable(getTranslation('main.password')),
                        forgetPasswordLbl: ko.observable(getTranslation('main.forgetPassword')),
                        haveAccountLbl: ko.observable(getTranslation('main.haveAccount')),
                        signUpLbl: ko.observable(getTranslation('main.signUp')),
                        signInLbl: ko.observable(getTranslation('main.signIn'))});
                }
            }


            return new LoginViewModel();
        }
);
