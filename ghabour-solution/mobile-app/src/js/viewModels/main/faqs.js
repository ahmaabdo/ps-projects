define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'config/services',
    'ojs/ojbutton', 'ojs/ojconveyorbelt', 'ojs/ojcollapsible'],
        function (oj, ko, app, moduleUtils, services) {

            function FAQsViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observableArray([]);
                self.currentLocal = ko.observable(app.getLocale());
                self.faqQuestions = ko.observableArray([]);
                self.data = ko.observableArray([]);
                self.dataArr = ko.observableArray([]);


                self.connected = function () {
                    self.data([]);
                    self.dataArr([]);
                    app.loading(true);
                    initTranslations();
                    app.globalHeaderConfig(true, true, true, 'dashboard');
                        self.dataArr(app.faqData());
                        if (app.faqData()) {
                            app.loading(false);
                            self.unique = ko.computed(function () {
                                var sortedItems = app.faqData().concat().sort(function (left, right) {
                                    return left.type == right.type ? 0 : (left.type < right.type ? -1 : 1)
                                });

                                var type;

                                for (var i = 0; i < sortedItems.length; i++) {
                                    if (!type || type != sortedItems[i].type) {
                                        type = sortedItems[i].type;
                                    } else {
                                        sortedItems.splice(i, 1);
                                        i--;
                                    }
                                }
                                return sortedItems;
                            });
                            self.data(self.unique());
                        }
                };
                self.filterFaq = function (event) {
                    self.faqQuestions([]);
                    if(event){
                    var type = event.type;
                    var filterType = self.dataArr().filter(e => e.type == type);
                    if (filterType) {
                        if (localStorage.getItem("selectedLanguage") == 'ar') {
                            for (var i in filterType)
                                self.faqQuestions.push({question: filterType[i].quesAr, answer: filterType[i].answerAr, type: filterType[i].lookupTypeAr});
                        } else {
                            for (var i in filterType)
                                self.faqQuestions.push({question: filterType[i].quesEn, answer: filterType[i].answerEn, type: filterType[i].lookupTypeEn});
                        }
                    }
                }
                };

                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {
                    self.currentLocal(app.getLocale());
                    app.headerTitle(getTranslation('main.faq'));
                    self.labels = {
                        carsLbl: ko.observable(getTranslation('main.cars')),
                        appLbl: ko.observable(getTranslation('main.app')),
                        complainLbl: ko.observable(getTranslation('main.complain')),
                        maintenanceLbl: ko.observable(getTranslation('main.maintenance')),
                        vehicleLbl: ko.observable(getTranslation('main.vehicle'))
                    };
                    self.filterFaq();
                }
            }

            return new FAQsViewModel();
        }
);
