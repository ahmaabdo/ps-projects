define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojmessaging', 'ojs/ojbutton',
    'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojformlayout', 'ojs/ojvalidationgroup','ojs/ojinputnumber'],
        function (oj, ko, app, services, commonhelper, moduleUtils, ArrayDataProvider, Message) {

            function SignUpViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observableArray([]);
                self.errorMsgEmail = ko.observable();
                self.errorMsgPhone = ko.observable();
                self.errorMsgUsername = ko.observable();
                self.errorMsgPass = ko.observable();
                self.currentRawPassValue = ko.observable();
                self.currentPassValue = ko.observable();
                self.groupValid = ko.observable();
                self.userName = ko.observable("");
                self.password = ko.observable("");
                self.confirmPassword = ko.observable("");
                self.firstName = ko.observable("");
                self.lastName = ko.observable("");
                self.email = ko.observable("");
                self.engineNumber = ko.observable();
                self.nationalId = ko.observable();
                self.mobileNumber = ko.observable("");
                self.chassisNumber = ko.observable("");
                self.emailValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
                                hint: self.labels.emailHint(),
                                messageDetail: self.labels.emailmessageDetail()
                            }
                        }];
                });

                self.usernameValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9]{3,}",
                                hint: self.labels.usernameHint(),
                                messageDetail: self.labels.usernameMessageDetail()
                            }
                        }];
                });

                self.validatorPass = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9 ]{8,}",
                                hint: self.labels.passHint(),
                                messageDetail: self.labels.passMessageDetail()
                            }
                        }];
                });
                self.mobileValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[0]+[1]+[0-9]{9,9}",
                                hint: self.labels.phoneHint(),
                                messageDetail: self.labels.phoneMessageDetail()
                            }
                        }];
                });

                self.usernameChangedHandler = function (event) {
                    if (event) {
                        var username = event.detail.value;
                        var payload = {
                            "username": username
                        };
                        services.postGeneric("users/is-username-valid", payload).then(data => {
                            if (data.valid == "false" && username) {
                                self.errorMsgUsername([{
                                        summary: self.labels.usernameValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } else {
                                self.errorMsgUsername([]);
                            }
                        }, app.failCbFn);
                    }
                };

                self.emailChangedHandler = function (event) {
                    if (event) {
                        var email = event.detail.value;
                        var payload = {
                            "email": email
                        };
                        services.postGeneric("users/is-email-valid", payload).then(data => {
                            if (data.valid == "false" && email) {
                                self.errorMsgEmail([{
                                        summary: self.labels.emailValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } else {
                                self.errorMsgEmail([]);
                            }
                        }, app.failCbFn);
                    }
                };

                self.phoneChangedHandler = function (event) {
                    if (event) {
                        var phone = event.detail.value;
                        var payload = {
                            "phone": phone
                        };
                        services.postGeneric("users/is-phone-valid", payload).then(data => {
                            if (data.valid == "false" && phone) {
                                self.errorMsgPhone([{
                                        summary: self.labels.phoneValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } else {
                                self.errorMsgPhone([]);
                            }
                        }, app.failCbFn);
                    }
                };

                self.createAccountAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    if(self.password()==self.confirmPassword())
                    {
                    self.getSuccs = function () {
                        app.loading(false);
                        app.createMessage("confirmation", self.label.success());
                        app.login(self.userName().toLowerCase(), self.password());
                    };

                        app.loading(true);
                        var payload = {
                            "username": self.userName(),
                            "password": self.password(),
                            "firstName": self.firstName(),
                            "lastName": self.lastName(),
                            "email": self.email(),
                            "phone": self.mobileNumber(),
                            "chassisNumber": self.chassisNumber(),
                            "engineNumber": self.engineNumber(),
                            "nationalId": self.nationalId()
                        };
                        services.postGeneric(commonhelper.addUser, payload).then(self.getSuccs, app.failCbFn);
                    } else {
                        app.loading(false);
                        app.createMessage("error", self.label.invalidPass());
                    }

                };
                self.logInAction = function () {
                    oj.Router.rootInstance.go('login');
                };
                ko.computed(()=>{
                    self.passChangedHandler=function(){
                     if (self.password() == self.confirmPassword()) {
                                self.errorMsgPass([]);
                            } else {
                                self.errorMsgPass([{
                                        summary: self.label.invalidPass(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } 
                };
                });
                self.continueWithGoogle = function (data, event) {
                    app.continueWithGoogle();
                };
                self.fbLogin=function(){
                    app.checkLoginState();
                };

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true,true, 'landing');
                    var pass,confirm;
                    document.getElementById("password").addEventListener('rawValueChanged', function (event) {
                        pass = event.detail.value;
                        self.password(pass);
                    });
                    document.getElementById("confirm-pass").addEventListener('rawValueChanged', function (event) {
                            confirm = event.detail.value;
                            self.confirmPassword(confirm);
                        });
                };


                self.transitionCompleted = function () {
                    app.animationSwitcher();
                };


                /***************** refresh for language ******************/
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });


                function initTranslations() {
                    app.headerTitle(getTranslation('main.signUp'));
                    self.labels = {
                        continueWithGoogle: ko.observable(getTranslation('main.continueWithGoogle')),
                        placeholderEngine: ko.observable(getTranslation('main.engineNumberLbl')),
                        placeholderNational: ko.observable(getTranslation('main.nationalIdLbl')),
                        engineNumberLbl: ko.observable(getTranslation('main.engineNumberLbl')),
                        nationalIdLbl: ko.observable(getTranslation('main.nationalIdLbl')),
                        success:ko.observable(getTranslation('main.success')),
                        invalidPass: ko.observable(getTranslation('main.invalidPass')),
                        phoneHint: ko.observable(getTranslation('main.phoneHint')),
                        phoneMessageDetail: ko.observable(getTranslation('main.phoneMessageDetail')),
                        passHint: ko.observable(getTranslation('main.passHint')),
                        passMessageDetail: ko.observable(getTranslation('main.passMessageDetail')),
                        usernameValid: ko.observable(getTranslation('main.usernameValid')),
                        phoneValid: ko.observable(getTranslation('main.phoneValid')),
                        emailValid: ko.observable(getTranslation('main.emailValid')),
                        placeholderUsername: ko.observable(getTranslation('main.placeholderUsername')),
                        placeholderFirstname: ko.observable(getTranslation('main.placeholderFirstname')),
                        placeholderLastname: ko.observable(getTranslation('main.placeholderLastname')),
                        placeholderPassword: ko.observable(getTranslation('main.placeholderPassword')),
                        placeholderConfPassword: ko.observable(getTranslation('main.placeholderConfPassword')),
                        placeholderEmail: ko.observable(getTranslation('main.placeholderEmail')),
                        placeholderPhone: ko.observable(getTranslation('main.placeholderPhone')),
                        placeholderChassis: ko.observable(getTranslation('main.placeholderChassis')),
                        usernameHint: ko.observable(getTranslation('main.usernameHint')),
                        usernameMessageDetail: ko.observable(getTranslation('main.usernameMessageDetail')),
                        emailHint: ko.observable(getTranslation('main.emailHint')),
                        emailmessageDetail: ko.observable(getTranslation('main.emailmessageDetail')),
                        createNewAccountLbl: ko.observable(getTranslation('main.createNewAccount')),
                        userName: ko.observable(getTranslation('main.userName')),
                        password: ko.observable(getTranslation('main.password')),
                        confirmPassword: ko.observable(getTranslation('main.confirmPassword')),
                        emailLbl: ko.observable(getTranslation('main.email')),
                        firstNameLbl: ko.observable(getTranslation('main.firstName')),
                        lastNameLbl: ko.observable(getTranslation('main.lastName')),
                        mobileNumberLbl: ko.observable(getTranslation('main.mobileNumber')),
                        chassisNumberLbl: ko.observable(getTranslation('history.chassisNumber')),
                        createAccountLbl: ko.observable(getTranslation('main.createAccount'))};
                }
            }

            return new SignUpViewModel();
        }
);
