define(['ojs/ojcore', 'knockout', 'appController', 'config/services',
    'util/commonhelper', 'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider',
    'ojs/ojpagingdataproviderview', 'ojs/ojpagingcontrol', 'ojs/ojlabel',
    'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojarraytabledatasource'],
        function (oj, ko, app, services, commonhelper, moduleUtils, ArrayDataProvider, PagingDataProviderView) {

            function OurLocationViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var lastBrand = null;
                self.label = ko.observable();
                self.dataprovider = ko.observableArray([]);
                self.data = ko.observableArray();
                self.jsonDataStr = ko.observableArray([]);
                self.dataStatus = ko.observable("loading");
                self.vehiclesDataProvider = ko.observable(new oj.ArrayTableDataSource(self.jsonDataStr));
                self.shimmerCards = ko.observableArray(new Array(4));
                self.visibleForm = {
                    load: ko.observable(true),
                    vehicleHistory: ko.observable(false),
                    noData: ko.observable(false),
                    history: ko.observable(false)
                };
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });


                //Static Data: Remove this section

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                self.jsonDataStr.push({id: "0", brand: "Hyundai"}, {id: "1", brand: "Geely"}, {id: "2", brand: "Mazda"});
//                self.data.push({"Brand": "Hyundai",
//                    "Incident1": "null",
//                    "Model": "4",
//                    "CarModel": "c",
//                    "createdTime": "2019-10-09T07:12:50Z",
//                    "Incident": "null",
//                    "Contact": "12",
//                    "DriverName": "null",
//                    "DriverPhone": "null",
//                    "plate_number": "م ل ي‚ 5942",
//                    "Color": "null",
//                    "Driver": "null",
//                    "Elite_Vehicle": "No",
//                    "id": "5",
//                    "lookupName": "م ل ث‚ 5942",
//                    "engine_number": "33917",
//                    "Vehicle_type": "null",
//                    "UpdatedByAccount": "2",
//                    "Opportunity": "null",
//                    "vin_number": "602231339",
//                    "Date": "2019-10-31",
//                    "Problem_Summary": "null",
//                    "Service_Advisor": "null",
//                    "by_Vehicle": "null",
//                    "INSTALL_DATE": "null",
//                    "QUANTITY": "null",
//                    "JOB_TYPE": "null",
//                    "PHONE_NUMBER": "null",
//                    "GROUP_NAME": "null",
//                    "ITEM_CODE": "null",
//                    "ITEM_DESC": "null",
//                    "UNIT_PRICE": "null",
//                    "NET_AMOUNT": "null",
//                    "TASK_STATUS": "null",
//                    "INCIDENT_NUMBER": "null",
//                    "INCIDENT_DATE": "null",
//                    "KM": "null",
//                    "CUSTOMER_NUMBER": "null",
//                    "CUSTOMER_NAME": "null",
//                    "TASK_NAME": "null",
//                    "TASK_DESCRIPTION": "null",
//                    "SER_ADVISOR": "null",
//                    "chassis_number": "22222",
//                    "updatedTime": "2019-10-31T12:07:01Z",
//                    "CreatedByAccount": "2"
//                }, {"Brand": "Geely",
//                    "Incident1": "null",
//                    "Model": "4",
//                    "CarModel": "AZERA",
//                    "createdTime": "2019-10-09T07:12:50Z",
//                    "Incident": "null",
//                    "Contact": "12",
//                    "DriverName": "null",
//                    "DriverPhone": "null",
//                    "plate_number": "ب ر‚ 5942",
//                    "Color": "null",
//                    "Driver": "null",
//                    "Elite_Vehicle": "No",
//                    "id": "5",
//                    "lookupName": "ب خ ق‚ 5942",
//                    "engine_number": "33917",
//                    "Vehicle_type": "null",
//                    "UpdatedByAccount": "2",
//                    "Opportunity": "null",
//                    "vin_number": "602231339",
//                    "chassis_number": "602231",
//                    "Date": "2019-10-31",
//                    "Problem_Summary": "null",
//                    "Service_Advisor": "null",
//                    "by_Vehicle": "null",
//                    "INSTALL_DATE": "null",
//                    "QUANTITY": "null",
//                    "JOB_TYPE": "null",
//                    "PHONE_NUMBER": "null",
//                    "GROUP_NAME": "null",
//                    "ITEM_CODE": "null",
//                    "ITEM_DESC": "null",
//                    "UNIT_PRICE": "null",
//                    "NET_AMOUNT": "null",
//                    "TASK_STATUS": "null",
//                    "INCIDENT_NUMBER": "null",
//                    "INCIDENT_DATE": "null",
//                    "KM": "null",
//                    "CUSTOMER_NUMBER": "null",
//                    "CUSTOMER_NAME": "null",
//                    "TASK_NAME": "null",
//                    "TASK_DESCRIPTION": "null",
//                    "SER_ADVISOR": "null",
//                    "updatedTime": "2019-10-31T12:07:01Z",
//                    "CreatedByAccount": "2"
//                }, {"Brand": "Mazda",
//                    "Incident1": "null",
//                    "Model": "4",
//                    "CarModel": "CX-5",
//                    "createdTime": "2019-10-09T07:12:50Z",
//                    "Incident": "null",
//                    "Contact": "12",
//                    "DriverName": "null",
//                    "DriverPhone": "null",
//                    "plate_number": "ض غ‚ 5942",
//                    "Color": "null",
//                    "Driver": "null",
//                    "Elite_Vehicle": "No",
//                    "id": "5",
//                    "lookupName": "ش ث ى‚ 432",
//                    "engine_number": "43132",
//                    "Vehicle_type": "null",
//                    "UpdatedByAccount": "2",
//                    "Opportunity": "null",
//                    "vin_number": "43215",
//                    "chassis_number": "43215",
//                    "updatedTime": "2019-10-31T12:07:01Z",
//                    "Date": "2019-10-31",
//                    "Problem_Summary": "null",
//                    "Service_Advisor": "null",
//                    "by_Vehicle": "null",
//                    "INSTALL_DATE": "null",
//                    "QUANTITY": "null",
//                    "JOB_TYPE": "null",
//                    "PHONE_NUMBER": "null",
//                    "GROUP_NAME": "null",
//                    "ITEM_CODE": "null",
//                    "ITEM_DESC": "null",
//                    "UNIT_PRICE": "null",
//                    "NET_AMOUNT": "null",
//                    "TASK_STATUS": "null",
//                    "INCIDENT_NUMBER": "null",
//                    "INCIDENT_DATE": "null",
//                    "KM": "null",
//                    "CUSTOMER_NUMBER": "null",
//                    "CUSTOMER_NAME": "null",
//                    "TASK_NAME": "null",
//                    "TASK_DESCRIPTION": "null",
//                    "SER_ADVISOR": "null",
//                    "CreatedByAccount": "2"}
//                );
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//                self.handleCurrentItemChanged = function (event) {
////                    var brand = event.detail.item.innerText;
//                    var brand = event;
//                    var filter = self.data().filter(e => e.Brand == brand);
//                    if (filter.length > 0 && brand) {
//                        //Static data -- remove this
//                        ///////
//                        Object.keys(filter).forEach(function (dataKey) {
//                            for (var i in filter[dataKey]) {
//                                if (filter[dataKey][i] == 'null') {
//                                    filter[dataKey][i] = self.label().notAvailable();
//                                }
//                            }
//                        });
//                        ///////
//                        self.dataprovider(filter);
//                    } else {
//                        self.dataprovider([]);
//                    }
//                    self.visibleForm.vehicleHistory(true).history(true).load(false).noData(false);
//                };
                self.historyAction = function (event) {
//                    var brand = event.detail.item.innerText;
                    var brand = event;
                    var filter = self.data().filter(e => e.Brand == brand);
                    if (filter.length > 0 && brand) {
                        //Static data -- remove this
                        ///////
                        Object.keys(filter).forEach(function (dataKey) {
                            for (var i in filter[dataKey]) {
                                if (filter[dataKey][i] == 'null') {
                                    filter[dataKey][i] = self.label().notAvailable();
                                }
                            }
                        });
                        ///////
                        self.dataprovider(filter);
                        self.dataStatus('dataAvailable');
                    } else {
                        self.dataprovider([]);
                        self.dataStatus('noData');
                    }
//                    self.visibleForm.vehicleHistory(true).history(true).load(false).noData(false);

                };
                ko.computed(() => {
                    var data = app.vehicleData();
                    console.log(data)
                    if (!data) {
                        self.dataStatus('loading');
                    } else {
                        if (data.length < 1) {
                            self.dataStatus('noData');
                        } else {
//                            self.dataStatus('dataAvailable');
                            self.data(data);
                            self.historyAction("Hyundai");


//                            var brands = app.vehicleData().filter((arr, index, self) =>
//                                index == self.findIndex((t) => (t.Brand === arr.Brand)));
//                            for (var i in brands) {
//                                self.jsonDataStr.push({id: i, brand: brands[i].Brand == "null" ? self.label().notAvailable() : brands[i].Brand});

                        }
                    }

                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true, true, 'dashboard');
//                    self.visibleForm.vehicleHistory(false);
//                    self.visibleForm.history(false);
//                    //Static data -- Uncomment below lines
//                    self.dataprovider([]);
//                    self.jsonDataStr([]);
//                    var data=app.vehicleData();
//                        if (data) {
//                            self.visibleForm.vehicleHistory(true);
//                            self.visibleForm.load(false);
//                            self.data(data);
//
//                            var brands = app.vehicleData().filter((arr, index, self) =>
//                                index == self.findIndex((t) => (t.Brand === arr.Brand)));
//                            for (var i in brands) {
//                                self.jsonDataStr.push({id: i, brand: brands[i].Brand == "null" ? self.label().notAvailable() : brands[i].Brand});
//                            }
//
//                        } else {
//
//                            //Static data -- Remove below 2 lines and Uncomment below lines
//                            self.visibleForm.noData(true);
//                            self.visibleForm.vehicleHistory(false);
//                            self.visibleForm.history(false);
//                            self.visibleForm.load(false);
//                        }
                };

                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };


                function initTranslations() {
                    app.headerTitle(getTranslation('main.vehicleHistory'));
                    self.label ({
                        load: ko.observable(getTranslation('history.load')),
                        history: ko.observable(getTranslation('history.history')),
                        noDataText: ko.observable(getTranslation('history.noDataText')),
                        notAvailable: ko.observable(getTranslation("history.notAvailable")),
                        eliteVehicle: ko.observable(getTranslation("history.eliteVehicle")),
                        chassisNumber: ko.observable(getTranslation("history.chassisNumber")),
                        carModel: ko.observable(getTranslation("history.carModel")),
                        engineNumber: ko.observable(getTranslation("history.engineNumber")),
                        noThingTxt: ko.observable(getTranslation("main.noThingTxt")),
                        emptyTxt: ko.observable(getTranslation("main.noDataAvailableTxt")),
                        plateNumber: ko.observable(getTranslation("history.plateNumber")),
                        vinNumber: ko.observable(getTranslation("history.vinNumber")),
                        vehicleType: ko.observable(getTranslation("history.vehicleType"))
                    });
                }
            }

            return new OurLocationViewModel();
        }
);
