define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojarraytabledatasource'],
        function (oj, ko, app, moduleUtils) {

            function LandingViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observableArray([]);
                self.headerName = ko.observable();
                self.dataProvider = ko.observable();
                self.jsonDataStr = ko.observableArray([]);
                self.dataProvider(new oj.ArrayTableDataSource(self.jsonDataStr, {idAttributes: 'id'}));

                self.connected = function () {
                    // Implement if needed
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true, 'vehicleHistory');
                    var retrievedData = oj.Router.rootInstance.retrieve();
                    self.jsonDataStr([]);
                    self.jsonDataStr.push({
                        id: retrievedData.id,
                        name: retrievedData.name,
                        icon: 'car.jpg',
                        maintenanceType: self.labels.maintenanceTypeLbl() + ' : ' + 'Open',
                        itemsAsOperation: self.labels.itemsAsOperationLbl() + ' fix front tires',
                        dateOfVisit: self.labels.dateOfVisitLbl() + ' 02/09/2019',
                        comments: self.labels.commentsLbl() + ' test'
                    });
                    self.jsonDataStr.push({
                        id: retrievedData.id,
                        name: retrievedData.name,
                        icon: 'car.jpg',
                        maintenanceType: self.labels.maintenanceTypeLbl() + ' : ' + 'Open',
                        itemsAsOperation: self.labels.itemsAsOperationLbl() + ' fix front tires',
                        dateOfVisit: self.labels.dateOfVisitLbl() + ' 02/09/2019',
                        comments: self.labels.commentsLbl() + ' test'
                    });
                    self.jsonDataStr.push({
                        id: retrievedData.id,
                        name: retrievedData.name,
                        icon: 'car.jpg',
                        maintenanceType: self.labels.maintenanceTypeLbl() + ' : ' + 'Open',
                        itemsAsOperation: self.labels.itemsAsOperationLbl() + ' fix front tires',
                        dateOfVisit: self.labels.dateOfVisitLbl() + ' 02/09/2019',
                        comments: self.labels.commentsLbl() + ' test'
                    });

                };


                self.handleCurrentItemChanged = function (event) {
                    oj.Router.rootInstance.store(self.jsonDataStr()[event.detail.value[0] - 1]);
                    oj.Router.rootInstance.go('carHistory');

                };

                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                
                function initTranslations() {
                    app.headerTitle(getTranslation('main.cars'));
                    self.labels = ({
                        maintenanceTypeLbl: ko.observable(getTranslation("maintenance.maintenanceType")),
                        itemsAsOperationLbl: ko.observable(getTranslation("cars.itemsAsOperation")),
                        dateOfVisitLbl: ko.observable(getTranslation("cars.dateOfVisit")),
                        commentsLbl: ko.observable(getTranslation("cars.comments"))
                    });

                }
            }

            return new LandingViewModel();
        }
);
