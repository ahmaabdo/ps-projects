define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojarraytabledatasource'],
        function (oj, ko, app, moduleUtils) {

            function LandingViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observableArray([]);
                self.headerName = ko.observable();
                self.carName = ko.observable();
                self.jsonDataStr = ko.observableArray([]);
                self.connected = function () {
                    // Implement if needed
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true, 'cars');
                    var retrievedData = oj.Router.rootInstance.retrieve();
                    self.jsonDataStr([]);
                    self.carName(retrievedData.name);
                    self.jsonDataStr.push({
                        id: retrievedData.id,
                        owner: self.labels.ownerLbl() + ' : ' + 'eslam',
                        chassisNumber: self.labels.chassisNumberLbl() + ' : ' + '1538424',
                        serviceCenter: self.labels.serviceCenterLbl() + ' : ' + 'center',
                        date: self.labels.dateLbl() + ' : ' + '2/9/2019'
                    });
                    self.jsonDataStr.push({
                        id: retrievedData.id,
                        owner: self.labels.ownerLbl() + ' : ' + 'eslam',
                        chassisNumber: self.labels.chassisNumberLbl() + ' : ' + '1538424',
                        serviceCenter: self.labels.serviceCenterLbl() + ' : ' + 'center',
                        date: self.labels.dateLbl() + ' : ' + '2/9/2019'
                    });
                };


                self.handleCurrentItemChanged = function (event) {
                };

                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {
                    app.headerTitle(getTranslation('main.carHistory'));
                    self.labels = {
                        upcomingLbl: ko.observable(getTranslation('maintenance.upcoming')),
                        pastLbl: ko.observable(getTranslation('maintenance.past')),
                        ownerLbl: ko.observable(getTranslation('maintenance.owner')),
                        chassisNumberLbl: ko.observable(getTranslation('main.chassisNumber')),
                        serviceCenterLbl: ko.observable(getTranslation('maintenance.serviceCenter')),
                        dateLbl: ko.observable(getTranslation('maintenance.date')),
                        addLbl: ko.observable(getTranslation('maintenance.add'))};

                }
            }
            return new LandingViewModel();
        }
);


