define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojpagingdataproviderview',
    'ojs/ojpagingcontrol', 'ojs/ojvalidationgroup', 'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojinputtext',
    'ojs/ojselectcombobox'],
        function (oj, ko, app, services, commonhelper, moduleUtils, ArrayDataProvider, PagingDataProviderView) {
            function inquiryContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable();
                self.groupValid = ko.observable();
                self.dataprovider = ko.observableArray([]);
                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));
                self.btnVal = ko.observable("Add");
                self.mainBtnsVals = [];
                self.pcOrBajajVisible = ko.observable(false);
                self.pcRequired = ko.observable(false);
                self.historyStatus = ko.observable("loading");
                self.shimmerCards = ko.observableArray(new Array(10));

                self.fieldsValue = {
                    inquiryDetail: ko.observable(""),
                    firstValue: ko.observable(''),
                    firstValueArr: ko.observableArray([]),
                    secondValue: ko.observable(''),
                    secValueArr: ko.observableArray([])
                };

                ko.computed(() => {
                    self.historyStatus("noDataAvailable");
                    if (app.userData()) {
                        if (app.userData().inquiryData) {
                            if (app.userData().inquiryData.length > 0) {
                                self.dataprovider([]);
                                setTimeout(() => {
                                    self.dataprovider(app.userData().inquiryData);
                                    self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));
                                    self.historyStatus("loaded");
                                }, 0);
                            }
                        }
                    }
                });
                ko.computed(()=>{
                    var data = app.inquiryData();
                    self.fieldsValue.firstValueArr([]);
                    if (data) {
                        if (localStorage.getItem("selectedLanguage") == 'ar')
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupNameAr, dependsObj: data[i].dependsObj});
                        else
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupName, dependsObj: data[i].dependsObj});
                    }
                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true, true, 'dashboard');
                    };

                self.selectInquiryHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    var firstValSelected = self.fieldsValue.firstValueArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        var dependsObj = firstValSelected.dependsObj;
                        self.fieldsValue.secValueArr([]);
                        if (dependsObj) {
                            self.pcRequired(true);
                            self.pcOrBajajVisible(true);
                            for (var i in dependsObj)
                                self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupName});
                        } else {
                            self.pcRequired(false);
                            self.pcOrBajajVisible(false);
                        }
                    }
                };

                self.submitAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    app.loading(true);
                    var payload = {
                        "personId": 12,
                        "firstValue": self.fieldsValue.firstValue(),
                        "secValue": self.fieldsValue.secondValue(),
                        "detailsValue": self.fieldsValue.inquiryDetail()
                    };

                    services.postGeneric("inquiryService", JSON.stringify(payload)).then(data => {
                        app.loading(false);
                        if (data.status == 'done') {
                            self.fieldsValue.inquiryDetail("");
                            app.createMessage("confirmation", self.labels().success());
                            app.getUserData();
                        } else {
                            app.failCbFn();
                            self.historyStatus("noDataAvailable");
                        }
                    }, err => {
                        self.historyStatus("noDataAvailable");
                        app.failCbFn();
                    });
                };


                self.transitionCompleted = function () {
                    app.animationSwitcher();
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                /**********************************************************/
                function initTranslations() {
                    app.headerTitle(getTranslation('main.inquiry'));
                    self.labels({
                        success: ko.observable(getTranslation('inquiry.success')),
                        inquiryDetail: ko.observable(getTranslation('main.description')),
                        status: ko.observable(getTranslation("main.status")),
                        secValueLbl: ko.observable(getTranslation('inquiry.pcOrBajaj')),
                        notAvailable: ko.observable(getTranslation("complain.notAvailable")),
                        inquiryType: ko.observable(getTranslation('inquiry.inquiryType')),
                        sendLbl: ko.observable(getTranslation('complain.send')),
                        noThingTxt: ko.observable(getTranslation("main.noThingTxt")),
                        emptyTxt: ko.observable(getTranslation("main.emptyTxt")),
                        placeholder: ko.observable(getTranslation("main.placeholder")),
                        addLbl: ko.observable(getTranslation("inquiry.addLbl")),
                        historyLbl: ko.observable(getTranslation("main.history"))
                    });
                    self.mainBtnsVals = [
                        {id: 'Add', label: self.labels().addLbl},
                        {id: 'History', label: self.labels().historyLbl}
                    ];
                }

            }

            return inquiryContentViewModel;
        });
