define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojpagingdataproviderview',
    'ojs/ojpagingcontrol', 'ojs/ojvalidationgroup', 'ojs/ojlabel', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojselectcombobox'],
        function (oj, ko, app, services, commonhelper, moduleUtils, ArrayDataProvider, PagingDataProviderView) {
            function serviceContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable();
                self.dataprovider = ko.observableArray([]);
                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));
                self.btnVal = ko.observable("Add");
                self.mainBtnsVals = [];
                self.historyStatus = ko.observable("loading");
                self.shimmerCards = ko.observableArray(new Array(10));

                self.groupValid = ko.observable();

                self.fieldsValue = {
                    rsaLocation: ko.observable(""),
                    firstValue: ko.observable(''),
                    firstValueArr: ko.observableArray([]),
                    secondValue: ko.observable(''),
                    secValueArr: ko.observableArray([]),
                    serviceDetail: ko.observable("")
                };

                self.fieldsOption = {
                    rsaRequired: ko.observable(false),
                    secValueRequired: ko.observable(false),
                    serviceDetailRequired: ko.observable(true)
                };

                ko.computed(() => {
                    if (app.userData()) {
                        self.historyStatus("noDataAvailable");
                        if (app.userData().serviceData) {
                            if (app.userData().serviceData.length > 0) {
                                self.dataprovider([]);
                                setTimeout(() => {
                                    for (var i in app.userData().serviceData){
                                        if(app.userData().serviceData[i].firstVal == "Road Assistance"){
                                            app.userData().serviceData[i].flagRsa=true;
                                            app.userData().serviceData[i].flagSec=false;
                                        }else{
                                            app.userData().serviceData[i].flagRsa=false;
                                            app.userData().serviceData[i].flagSec=true;
                                        }
                                        if(app.userData().serviceData[i].firstVal !== "Road Assistance"&&!app.userData().serviceData[i].secondValue){
                                            app.userData().serviceData[i].flagSec=false;
                                            app.userData().serviceData[i].flagRsa=false;
                                        }
                                        if(app.userData().serviceData[i].rsaLocation=="Not Available"||!app.userData().serviceData[i].rsaLocation){
                                            app.userData().serviceData[i].flagRsa=false;
                                        }
                                    }
                                    self.dataprovider(app.userData().serviceData);
                                    self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));
                                    self.historyStatus("loaded");
                                }, 0);
                            }
                        }
                    }
                });
                
                ko.computed(()=>{
                    var data = app.serviceData();
                    self.fieldsValue.firstValueArr([]);
                    if (data) {
                        if (localStorage.getItem("selectedLanguage") == 'ar')
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupNameAr, dependsObj: data[i].dependsObj});
                        else
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupName, dependsObj: data[i].dependsObj});
                    }else{
                        data = app.serviceData();
                    }
                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true, true, 'dashboard');
                    };
                self.selectServiceHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    var firstValSelected = self.fieldsValue.firstValueArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        var dependsObj = firstValSelected.dependsObj;
                        self.fieldsOption.rsaRequired(firstValSelected.rsaRequired);
                        self.fieldsValue.secValueArr([]);
                        self.fieldsOption.secValueRequired("");
                        if (dependsObj) {
                            self.fieldsOption.secValueRequired(true);
                            if (localStorage.getItem("selectedLanguage") == 'ar')
                                for (var i in dependsObj)
                                    self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupNameAr});
                            else
                                for (var i in dependsObj)
                                    self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupName});
                        } else {
                            self.fieldsOption.secValueRequired(false);
                        }
                    }
                };


                self.sendAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    app.loading(true);
                    var payload = {
                        "personId": 12,
                        "firstValue": self.fieldsValue.firstValue(),
                        "secValue": self.fieldsValue.secondValue(),
                        "rsaLocation": self.fieldsValue.rsaLocation(),
                        "detailsValue": self.fieldsValue.serviceDetail()
                    };

                    self.historyStatus("loading");
                    services.postGeneric("serviceService", JSON.stringify(payload)).then(data => {
                        app.loading(false);
                        if (data.status == 'done') {
                            self.fieldsValue.serviceDetail("");
                            app.createMessage("confirmation", self.labels().success());
                            app.getUserData();
                        } else {
                            app.failCbFn();
                            self.historyStatus("noDataAvailable");
                        }
                    }, err => {
                        self.historyStatus("noDataAvailable");
                        app.failCbFn();
                    });
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };
                /**********************************************************/
                function initTranslations() {
                    app.headerTitle(getTranslation('main.service'));
                    self.labels({
                        success: ko.observable(getTranslation('service.success')),
                        placeholder: ko.observable(getTranslation("main.placeholder")),
                        status: ko.observable(getTranslation("main.status")),
                        notAvailable: ko.observable(getTranslation("complain.notAvailable")),
                        serviceType: ko.observable(getTranslation('service.serviceType')),
                        rsaLocation: ko.observable(getTranslation('service.rsaLocation')),
                        rsaLocationHistory: ko.observable(getTranslation('service.rsaLocationHistory')),
                        secValueLbl: ko.observable(getTranslation('service.serviceCategory')),
                        serviceDetail: ko.observable(getTranslation('main.description')),
                        noThingTxt: ko.observable(getTranslation("main.noThingTxt")),
                        emptyTxt: ko.observable(getTranslation("main.emptyTxt")),
                        sendLbl: ko.observable(getTranslation('complain.send')),
                        addLbl: ko.observable(getTranslation("service.addLbl")),
                        historyLbl: ko.observable(getTranslation("main.history"))
                    });
                    self.mainBtnsVals = [
                        {id: 'Add', label: self.labels().addLbl},
                        {id: 'History', label: self.labels().historyLbl}
                    ];
                }
            }

            return serviceContentViewModel;
        });
