define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils',
    , 'config/services', 'util/commonhelper', 'ojs/ojarraydataprovider', 'ojs/ojlistdataproviderview', 'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojinputtext',
    'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojselectcombobox', 'ojs/ojpagingcontrol', 'ojs/ojvalidationgroup', 'ojs/ojmessaging'
            , 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojlistview'],
        function (oj, ko, app, moduleUtils, services, commonhelper, ArrayDataProvider, ListDataProviderView) {

            function MaintenanceViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;

                self.label = ko.observable();
                self.name = ko.observable("Mohammed");
                self.columns = ko.observable();
                self.avatarSize = ko.observable("xxs");
                self.isDisabled = ko.observable(true);
                self.selection = ko.observable();
                self.data = ko.observableArray([]);
                self.dataprovider = new oj.ListDataProviderView(new oj.ArrayDataProvider(self.data));
                self.groupValid = ko.observable();
                self.value = ko.observable();
                self.displayDate = ko.observable();
                self.brand = ko.observable();
                self.price = ko.observable();
                self.estTime = ko.observable();
                self.time = ko.observable('T08:00:00');
                self.toTime = ko.observable();
                self.addTrue = ko.observable(true);
                self.timePicker = {
                    timeIncrement: '00:20:00:00'
                };
                self.datePicker = {
                };


                self.dayFormatter =
                        ko.observable(function (dateInfo) {
                            console.log(dateInfo);
                            var month = dateInfo["month"],
                                    date = dateInfo["date"],
                                    fullYear = dateInfo["fullYear"];

                            var friday = getFri(month, fullYear);
                            var saturday = getSat(month, fullYear);

                            for (var i in friday)
                                if (date == friday[i]) {
                                    return {disabled: true};
                                }

                            for (var i in saturday)
                                if (date == saturday[i]) {
                                    return {disabled: true};
                                }


                        });



                var getFri = function (month, year) {
                    var d = new Date();
                    var getTot = daysInMonth(month, year);
//                    console.log(daysInMonth(d.getMonth(), d.getFullYear()));
                    var fri = new Array();
                    var sat = new Array();

                    for (var i = 1; i <= getTot; i++) {
                        var newDate = new Date(d.getFullYear(), d.getMonth(), i);
                        if (newDate.getDay() == 5) {
                            fri.push(i);
                        }
                        if (newDate.getDay() == 6) {
                            sat.push(i);
                        }

                    }
//                    console.log(fri);
//                    console.log(sat);
                    function daysInMonth(month, year) {
                        return new Date(year, month, 0).getDate();
                    }
                    return fri;
                };

                var getSat = function (month, year) {
                    var d = new Date();
                    var getTot = daysInMonth(month, year);
//                    console.log(daysInMonth(d.getMonth(), d.getFullYear()));
                    var fri = new Array();
                    var sat = new Array();

                    for (var i = 1; i <= getTot; i++) {
                        var newDate = new Date(d.getFullYear(), d.getMonth(), i);
                        if (newDate.getDay() == 5) {
                            fri.push(i);
                        }
                        if (newDate.getDay() == 6) {
                            sat.push(i);
                        }

                    }
//                    console.log(fri);
//                    console.log(sat);
                    function daysInMonth(month, year) {
                        return new Date(year, month, 0).getDate();
                    }
                    return sat;
                };
//                getDay();

                self.dayMetaData =
                        ko.observable({
                            //Year Level
                            '*':
                                    {
                                        //Month Level
                                        "*":
                                                {
                                                    //Day Level
                                                    "*":
                                                            {
                                                                disabled: true
                                                            }
                                                }
                                    }
                        });


                self.min = ko.observable('T08:00:00');
                self.max = ko.observable('T17:00:00');
                self.label = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.fieldsOption = {
                    secondValueDisplay: ko.observable(true),
                    secondValueRequired: ko.observable(false)
                };

                self.fieldsValue = {
                    operationDetail: ko.observable(),
                    vehicle: ko.observable(),
                    vehicleArr: ko.observableArray([]),
                    branch: ko.observable(),
                    branchArr: ko.observableArray([]),
                    operation: ko.observable(),
                    operationArr: ko.observableArray([]),
                    time: ko.observable(),
                    timeArr: ko.observableArray([]),
                    comment: ko.observable("")
                };

                ko.computed(p => {
                    if (self.value()) {
                        var date = new Date(self.value());
                        var dayOrder = date.getDay();
                        if (dayOrder == 5 || dayOrder == 6) {
                            app.createMessage("error", self.label().holidayErrMsg());
                            self.value('');
                        }
                        if (self.time() && self.value()) {
                            var dt = new Date(self.value() + self.time());
                            var t = 0;
                            if (self.fieldsValue.operation() == "5k") {
                                t = dt.getHours() + 2;
                                dt.setHours(t);
                                if (dt.getMinutes() == 0 || dt.getMilliseconds() == 0) {
                                    self.toTime("T" + dt.getHours() + ":" + "00" + ":" + "00");
                                } else {
                                    self.toTime("T" + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getMilliseconds());
                                }
                            } else if (self.fieldsValue.operation() == "1K") {
                                t = dt.getHours() + 1;
                                dt.setHours(t);
                                if (dt.getMinutes() == 0 || dt.getMilliseconds() == 0) {
                                    self.toTime("T" + dt.getHours() + ":" + "00" + ":" + "00");
                                } else {
                                    self.toTime("T" + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getMilliseconds());
                                }
                            } else {
                                t = dt.getHours() + 3;
                                dt.setHours(t);
                                if (dt.getMinutes() == 0 || dt.getMilliseconds() == 0) {
                                    self.toTime("T" + dt.getHours() + ":" + "00" + ":" + "00");
                                } else {
                                    self.toTime("T" + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getMilliseconds());
                                }
                            }
                        }
                    }
                });

                self.connected = function () {
                    app.globalHeaderConfig(true, true,true, 'dashboard');
                    app.loading(false);
                    initTranslations();
                    var retrievedVehicle = [
                        {Brand: "Hyundai", CarModel: "AZERA"},
                        {Brand: "Hyundai", CarModel: "ACCENT(2006)"}
                    ];
                    if (retrievedVehicle) {
                        self.brand(retrievedVehicle[0].Brand);
                        for (var i in retrievedVehicle) {
                            self.fieldsValue.vehicleArr.push({label: retrievedVehicle[i].CarModel, value: retrievedVehicle[i].CarModel});
                        }
                    }
//                    services.getGeneric("maintenaceService/events").then(data => {
//                        if (data) {
//                            console.log(data);
//                        }
//                    }, app.failCbFn);
                    self.fieldsValue.branchArr.push({label: "TD_Abo Rawash", value: "TD_Abo Rawash"},
                            {label: "HYN Abo Rawash SC", value: "HYN Abo Rawash SC"});
                    self.fieldsValue.operationArr.push({label: "5k", value: "5k", takenTime: 2},
                            {label: "1K", value: "1K", takenTime: 1}, {label: "طقم دبرياج", value: "طقم دبرياج", takenTime: 3});
                };
                self.addAction = function () {
                    var time = self.time();
                    var toTime = self.toTime();
                    self.data.push({vehicle: self.fieldsValue.vehicle(), branch: self.fieldsValue.branch()
                        , operation: self.fieldsValue.operation(), comment: self.fieldsValue.comment(),
                        date: self.value(), time: "From " + time.replace('T','') + " To " + toTime.replace('T',''), price: self.price(), estTime: self.estTime()});
                    self.addTrue(false);
                };
                self.submitAction = function () {
                    app.createMessage("confirmation", self.label().successMsg());
                };

                self.selectOperationHandler = function (event) {
                    self.fieldsValue.timeArr([]);
                    var selectedVal = event['detail'].value;
                    if (selectedVal == "5k") {
                        self.price("550.0 LE");
                        self.estTime("02:00 Hours");
                    } else if (selectedVal == "1K") {
                        self.price("350.0 LE");
                        self.estTime("01:00 Hours");
                    } else {
                        self.price("250.0 LE");
                        self.estTime("03:00 Hours");
                    }
                };
                function initTranslations() {
                    app.headerTitle(getTranslation('main.maintenance'));
                    self.label({
                        estTime: ko.observable(getTranslation('maintenance.estTime')),
                        successMsg: ko.observable(getTranslation('maintenance.successMsg')),
                        add: ko.observable(getTranslation("maintenance.add")),
                        price: ko.observable(getTranslation('maintenance.price')),
                        details: ko.observable(getTranslation('maintenance.details')),
                        vehicle: ko.observable(getTranslation('maintenance.vehicle')),
                        holidayErrMsg: ko.observable(getTranslation('maintenance.holidayErrMsg')),
                        branch: ko.observable(getTranslation('maintenance.branch')),
                        operation: ko.observable(getTranslation('maintenance.operation')),
                        comment: ko.observable(getTranslation('maintenance.comment')),
                        date: ko.observable(getTranslation('maintenance.date')),
                        time: ko.observable(getTranslation('testDrive.time')),
                        submit: ko.observable(getTranslation("main.submit"))
                    });


                }
            }

            return new MaintenanceViewModel();
        }
);
