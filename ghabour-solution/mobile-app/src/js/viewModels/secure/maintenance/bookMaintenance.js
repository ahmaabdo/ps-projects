define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton', 'ojs/ojlabel',
    'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata'],
        function (oj, ko, app, moduleUtils) {

            function BookMaintenanceViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observableArray([]);
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'
                }));
                self.currentDate = ko.observable(formatDate(new Date()));

                self.bookAction = function () {

                };

                self.connected = function () {
                    // Implement if needed
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true, 'maintenance');
                };

                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };


                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function formatDate(date) {
                    var month = '' + (date.getMonth() + 1), day = '' + date.getDate(), year = date.getFullYear();
                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;
                    return [year, month, day].join('-');
                }

                function initTranslations() {
                    app.headerTitle(getTranslation('main.bookMaintenance'));
                    self.labels = {
                        nearestLocationLbl: ko.observable(getTranslation('maintenance.nearestLocation')),
                        userNameLbl: ko.observable(getTranslation('main.userName')),
                        passwordLbl: ko.observable(getTranslation('main.password')),
                        bookLbl: ko.observable(getTranslation('maintenance.book')),
                        carInfoLbl: ko.observable(getTranslation('maintenance.carInfo')),
                        carModelLbl: ko.observable(getTranslation('maintenance.carModel')),
                        carTypeLbl: ko.observable(getTranslation('maintenance.carType')),
                        chassisNumberLbl: ko.observable(getTranslation('main.chassisNumber')),
                        maintenanaceInfoLbl: ko.observable(getTranslation('maintenance.maintenanceInfo')),
                        customerInfoLbl: ko.observable(getTranslation('maintenance.customerInfo')),
                        maintenanceTypeLbl: ko.observable(getTranslation('maintenance.maintenanceType')),
                        serviceCenterLbl: ko.observable(getTranslation('maintenance.serviceCenter'))};
                }
            }

            return new BookMaintenanceViewModel();
        }
);
