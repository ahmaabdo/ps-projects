define(['ojs/ojcore', 'knockout', 'appController', 'config/services',
    'util/commonhelper', 'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider',
    'ojs/ojpagingdataproviderview', 'ojs/ojvalidationgroup', 'ojs/ojpagingcontrol',
    'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox'],
        function (oj, ko, app, services, commonhelper, moduleUtils, ArrayDataProvider, PagingDataProviderView) {

            function ComplainViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.dataprovider = ko.observableArray([]);
                self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));
                self.btnVal = ko.observable("Add");
                self.mainBtnsVals = ko.observableArray([]);
                self.groupValid = ko.observable();
                self.historyStatus = ko.observable("loading");
                self.shimmerCards = ko.observableArray(new Array(10));
                self.fieldsOption = {
                    secondValueDisplay: ko.observable(false),
                    secondValueRequired: ko.observable(false)
                };
                self.fieldsValue = {
                    firstValue: ko.observable(''),
                    firstValueArr: ko.observableArray([]),
                    secondValue: ko.observable(''),
                    secValueArr: ko.observableArray([]),
                    complaintLetter: ko.observable("")
                };
                ko.computed(()=>{
                    var data = app.complainData();
                    //Get complain LOVs data
                    self.fieldsValue.firstValueArr([]);
                    if (data) {
                        if (localStorage.getItem("selectedLanguage") == 'ar')
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupNameAr, dependsObj: data[i].dependsObj});
                        else
                            for (var i in data)
                                self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupName, dependsObj: data[i].dependsObj});
                    }
                });


                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true, true, 'dashboard');
                };
                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };

                ko.computed(() => {
                    if (app.userData()) {
                        self.historyStatus("noDataAvailable");
                        if (app.userData().complaintData) {
                            if (app.userData().complaintData.length > 0) {
                                self.dataprovider([]);
                                setTimeout(() => {
                                    self.dataprovider(app.userData().complaintData);
                                    self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));
                                    self.historyStatus("loaded");
                                }, 0);
                            }
                        }
                    }
                });


                self.submitAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    app.loading(true);
                    var payload = {
                        "personId": 12,
                        "firstValue": self.fieldsValue.firstValue(),
                        "secValue": self.fieldsValue.secondValue(),
                        "detailsValue": self.fieldsValue.complaintLetter()
                    };
                    self.historyStatus("loading");
                    services.postGeneric("complainService", JSON.stringify(payload)).then(data => {
                        app.loading(false);
                        if (data.status == 'done') {
                            self.fieldsValue.complaintLetter("");
                            app.createMessage("confirmation", self.label().success());
                            app.getUserData();
                        } else {
                            app.failCbFn();
                            self.historyStatus("noDataAvailable");
                        }
                    }, err => {
                        self.historyStatus("noDataAvailable");
                        app.failCbFn();
                    });
                };

                self.selectComplainHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    var firstValSelected = self.fieldsValue.firstValueArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        var dependsObj = firstValSelected.dependsObj;
                        self.fieldsValue.secValueArr([]);
                        self.fieldsOption.secondValueRequired("");
                        if (dependsObj) {
                            self.fieldsOption.secondValueRequired(true);
                            self.fieldsOption.secondValueDisplay(true);

                            if (localStorage.getItem("selectedLanguage") == 'ar')
                                for (var i in dependsObj)
                                    self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupNameAr});
                            else
                                for (var i in dependsObj)
                                    self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupName});
                        } else {
                            self.fieldsOption.secondValueRequired(false);
                            self.fieldsOption.secondValueDisplay(false);
                        }
                    }
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {
                    app.headerTitle(getTranslation('main.complain'));
                    self.label({
                        success: ko.observable(getTranslation("complain.success")),
                        notAvailable: ko.observable(getTranslation("complain.notAvailable")),
                        status: ko.observable(getTranslation("main.status")),
                        complainType: ko.observable(getTranslation('complain.complainType')),
                        secValueLbl: ko.observable(getTranslation('complain.complaintCategory')),
                        complaintLetter: ko.observable(getTranslation('complain.complaintLetter')),
                        complaintLetterLbl: ko.observable(getTranslation('main.description')),
                        submit: ko.observable(getTranslation("main.submit")),
                        placeholder: ko.observable(getTranslation("main.placeholder")),
                        noThingTxt: ko.observable(getTranslation("main.noThingTxt")),
                        emptyTxt: ko.observable(getTranslation("main.emptyTxt")),
                        addLbl: ko.observable(getTranslation("complain.addLbl")),
                        historyLbl: ko.observable(getTranslation("main.history"))
                    });
                    self.mainBtnsVals([
                        {id: 'Add', label: self.label().addLbl},
                        {id: 'History', label: self.label().historyLbl}
                    ]);
                }
            }

            return new ComplainViewModel();
        });
