define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton'],
        function (oj, ko, app, moduleUtils) {

            function DashboardViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable();
                self.isUserLoggedIn = ko.observable(false);
                self.imagePath = ko.observable("");

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.routeToPageVehicle = function (page) {
                    if (!app.userLoggedIn()) {
                        oj.Router.rootInstance.go('logIn');
                    } else {
                        if (app.vehicleFlag() == 'T' && app.phoneFlag()=='T') {
                            oj.Router.rootInstance.go(page);
                        } else if(app.vehicleFlag() !== 'T'){
                            oj.Router.rootInstance.go('addVehicle');
                        }else if(app.phoneFlag()!=='T'){
                            oj.Router.rootInstance.go('phoneScreen');
                        }
                    }
                };
                // Header Config
                self.headerConfig = ko.observable({'view': [], 'viewModel': null});
                moduleUtils.createView({'viewPath': 'views/header.html'}).then(function (view) {
                    self.headerConfig({'view': view, 'viewModel': new app.getHeaderModel()});
                });

                self.maintenanceClickAction = function () {
                    oj.Router.rootInstance.go('maintenance');
                };
                self.products = function () {
                    oj.Router.rootInstance.go('products');
                };
                self.complainClickAction = function () {
                    oj.Router.rootInstance.go('complain');
                };
                self.testDriveClickAction = function () {
                    oj.Router.rootInstance.go('testDrive');
                };
                self.reservationPaymentClickAction = function () {
                    oj.Router.rootInstance.go('reservationPayment');
                };
                self.vehicleHistoryClickAction = function () {
                    oj.Router.rootInstance.go('vehicleHistory');
                };
                self.promotionsClickAction = function () {
                    oj.Router.rootInstance.go('promotions');
                };
                self.faqsClickAction = function () {
                    oj.Router.rootInstance.go('faqs');
                };
                self.profileClickAction = function () {
                    oj.Router.rootInstance.go('profile');
                };
                self.logoutAction = function () {
//                    app.fbLogout();
                    oj.Router.rootInstance.go('login');
                    localStorage.removeItem("personDetails");
//                    window.plugins.googleplus.logout(
//                            function (msg) {
//                                self.isUserLoggedIn(false);
//                                self.imagePath("");
//                            }
//                    );
//                    app.googleDisconnect();
//                    app.handleActivated();
                    app.userLoggedIn(false);
                };
                self.inquiryClickAction = function () {
                    oj.Router.rootInstance.go('inquiry');
                };
                self.serviceClickAction = function () {
                    oj.Router.rootInstance.go('service');
                };
                self.locationClickAction = function () {
                    oj.Router.rootInstance.go('ourLocation');
                };
                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(false, false,false, null);
                };
                self.switchLangAction = function () {
                    app.switchLanguage();
                    app.changeLang();
                };


                self.transitionCompleted = function () {
                    // Implement if needed
                    app.animationSwitcher();
                };

                app.globalHeaderConfig(false,false, false, null);
                function initTranslations() {
                    self.labels({
                        language: ko.observable(getTranslation('main.language')),
                        location: ko.observable(getTranslation('main.location')),
                        header: ko.observable(getTranslation("main.header")),
                        dashboard: ko.observable(getTranslation('main.dashboard')),
                        parag: ko.observable(getTranslation("main.parag")),
                        products: ko.observable(getTranslation('main.products')),
                        inquiry: ko.observable(getTranslation("main.inquiry")),
                        service: ko.observable(getTranslation("main.service")),
                        maintenanceLbl: ko.observable(getTranslation('main.maintenance')),
                        complainLbl: ko.observable(getTranslation('main.complain')),
                        testDriveLbl: ko.observable(getTranslation('main.testDrive')),
                        reservationPaymentLbl: ko.observable(getTranslation('main.reservationPayment')),
                        vehicleHistoryLbl: ko.observable(getTranslation('main.vehicleHistory')),
                        promotionsLbl: ko.observable(getTranslation('main.promotions')),
                        faqLbl: ko.observable(getTranslation('main.faq')),
                        logout: ko.observable(getTranslation('main.logout')),
                        profileLbl: ko.observable(getTranslation('profile.title'))
                    });
                }
            }

            return new DashboardViewModel();
        }
);
