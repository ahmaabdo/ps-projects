define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojlistdataproviderview', 'ojs/ojinputtext', 'ojs/ojlistview', 'ojs/ojbutton', 
    'ojs/ojlabel', 'ojs/ojarraytabledatasource', 'ojs/ojselectcombobox'],
        function (oj, ko, app, services, commonhelper, moduleUtils, ArrayDataProvider, ListDataProviderView) {

            function productsContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.productId=ko.observable();
                self.product=ko.observable();
                self.search=ko.observable();
                self.images=ko.observableArray([]);
                self.searchCar=ko.observableArray([]);
                self.searchArr=ko.observableArray([]);
                self.event=ko.observable();
                self.koArray=ko.observableArray([]);
                self.dataStatus = ko.observable("loading");
                self.carsModelArr = ko.observableArray([]);
                self.carsProductArr = ko.observableArray([]);
                self.productData = ko.observableArray();
                self.shimmerCards = ko.observableArray(new Array(4));
                self.searchDataProvider = new ArrayDataProvider(self.searchArr, { keyAttributes: 'value' });
                
                self.carModelsClickAction = function (event) {
//                    self.dataStatus("loading");
                    var currentItem=event.srcElement.currentItem;
                    var modelProduct = event.srcElement.data.dataProvider.data._latestValue[currentItem].Name_c;
                    if (modelProduct && self.productData()) {
                        self.carsProductArr([]);
                        self.carsProductArr(self.productData().Products.filter(e => e.Model_c == modelProduct));
                        oj.Router.rootInstance.store(self.carsProductArr());
                        console.log(self.carsProductArr());
                        oj.Router.rootInstance.go('productInfo');
                        if (self.carsProductArr().length == 0)
                        {
                            app.createMessage("error", "No data available");
                            self.dataStatus("noDataAvailable");
                        }
                    }
                };

                self.carBrandsClickAction = function (brand) {
                    self.searchArr([]);
                    console.log(self.productData());
                    if (brand && app.productData().length>0) {
                        self.carsModelArr([]);
                        self.carsModelArr(app.productData().Models.filter(e => e.Brand_c == brand));
                        self.searchCar(self.carsModelArr());
                        for(var i in self.carsModelArr()){
                            self.searchArr.push({"value":self.carsModelArr()[i].Name_c});
                        }
                    }
                };
                self.handleValueChanged = function () {
                    self.search(document.getElementById("text-input10").rawValue);
                };

                self.dataProvider = ko.computed(function () {
                    var filterRegEx = new RegExp(self.search(), 'i');
                    var filterCriterion = {
                        op: '$or',
                        criteria: [
                            {op: '$regex', value: {Name_c: filterRegEx}}
                            ]
                    };
                    var arrayDataProvider = new ArrayDataProvider(self.searchCar, {keyAttributes: 'value'});
                    return new ListDataProviderView(arrayDataProvider, {filterCriterion: filterCriterion});
                });
 
                function arrayBufferToBase64(buffer) {
                    return new Promise((resolve, reject) => {
                        const reader = new FileReader();
                        var blob = new Blob([buffer], {type: "image/png"});
                        reader.readAsDataURL(blob);
                        reader.onload = () => resolve(reader.result.substr(reader.result.indexOf(',') + 1));
                        reader.onerror = error => reject(error);
                    });
                }
                var dataLoaded = false;
                ko.computed(() => {
                    if (app.productData() && !dataLoaded) {
                        dataLoaded = true;
                        self.productData(app.productData());
                        self.carBrandsClickAction("CHERY");
                        self.carsModelArr([]);
                        for (var x = 0; x < app.productData().Models.length; x++) {
                            if (app.productData().Models[x].Brand_c == "CHERY") {
                                app.productData().Models[x].img = "css/images/car_placeholder.png";
                                self.carsModelArr.push(app.productData().Models[x]);
                            }
                        }
                        self.searchCar(self.carsModelArr());
                        console.log(self.carsModelArr());
                        self.dataStatus("dataRetrieved");
                    } 
                });
//                ko.computed(()=>{
//                   if (app.modelData()) {
//                        self.images([]);
//                        self.dataStatus("dataRetrieved");
//                        var data = self.carsModelArr();
//                        data.forEach(e => {
//                            if (app.modelData().find(x => x.model == e.Name_c)) {
//                                arrayBufferToBase64(new Uint8Array(app.modelData().find(x => x.model == e.Name_c).image)).then(imageData => {
//                                    e.img = imageData.replace("data/image/png/base64/", "data:image/png;base64,");
//                                });
//                            }
//                        });
//                        self.carsModelArr(data);
//                    }
//                });
                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    app.globalHeaderConfig(true, true, true, 'dashboard');
                };

                self.transitionCompleted = function () {
                    app.animationSwitcher();
                };
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {
                    app.headerTitle(getTranslation('main.products'));
                    self.label({
                        searchLbl:ko.observable(getTranslation("main.searchLbl")),
                        hyundai: ko.observable(getTranslation("vehicleHistory.hyundai")),
                        title: ko.observable(getTranslation("products.title")),
                        productInfo: ko.observable(getTranslation("products.productInfo")),
                        categoryName: ko.observable(getTranslation("products.categoryName")),
                        currency: ko.observable(getTranslation("products.currency")),
                        model: ko.observable(getTranslation("products.model")),
                        noThingTxt: ko.observable(getTranslation("main.noThingTxt")),
                        emptyTxt: ko.observable(getTranslation("main.noDataAvailableTxt")),
                        chooseCar: ko.observable(getTranslation("vehicleHistory.chooseCar")),
                        category: ko.observable(getTranslation("products.category")),
                        chooseCategory: ko.observable(getTranslation("vehicleHistory.chooseCategory"))
                    });
                }
            }

            return new productsContentViewModel();
        });
