define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'ojs/ojformlayout', 'ojs/ojinputtext', 'ojs/ojbutton', 'ojs/ojvalidationgroup'
], function (oj, ko, app, services) {
    function addVehicleLoginContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.groupValid = ko.observable();
                self.model = ko.observable();
                self.chassNumber = ko.observable();
                self.motor = ko.observable();
                self.code = ko.observable();
                self.phone = ko.observable();
                self.lastDigitPhone=ko.observable();
                self.created=ko.observable("F");
                self.visibleForm=ko.observable('addVehicle');
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                self.dataprovider = ko.observableArray([]);
                
                self.addAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    app.loading(true);
                    var payload = {
                        "motor": self.motor(),
                        "model": self.model(),
                        "chassisNumber": self.chassNumber()
                    };
                    //web service to get phone
                    services.postGeneric("myVehicle/getPhone", JSON.stringify(payload)).then(data => {
                        app.loading(false);
                        console.log(data);
                        if(data){
                            self.visibleForm('code');
                            self.phone(data);
                            self.lastDigitPhone(data.slice(-2));
                            console.log(self.lastDigitPhone());
                        }else{
                            app.createMessage("error", "Phone Needed");
                        }
                    }, err => {
                        app.failCbFn();
                    });
                };
                self.codeAction=function(){
                    //confirm code then add
                    app.loading(true);
                    var payload = {
                        "primaryContact":app.personDetails().id,
                        "motor": self.motor(),
                        "model": self.model(),
                        "chassisNumber": self.chassNumber(),
                        "phone":self.phone()
                    };
                    services.postGeneric("myVehicle/addVehicle", JSON.stringify(payload)).then(data => {
                        app.loading(false);
                        console.log(data);
                        if(data){
                            app.createMessage("confirmation", "Vehicle Added Successfully");
                            self.motor("");
                            self.model("");
                            self.chassNumber("");
                            self.created("T");
                            app.updateVehicle(self.created());
                            oj.Router.rootInstance.go("dashboard");
                        }
                            
                    }, err => {
                        app.failCbFn();
                    });
                };
                self.skipAction = function () {
                    oj.Router.rootInstance.go('dashboard');
                };
                self.backAction = function () {
                    oj.Router.rootInstance.go('login');
                };
                self.resendCodeAction=function(){

                };
                self.callAction = function () {
                };

                self.connected = function () {
                    app.globalHeaderConfig(true, true,true, 'profile');
                    app.loading(false);
                    initTranslations();
                };
                function initTranslations() {
                    app.headerTitle(getTranslation('profile.vehicles'));
                    self.label ({
                        callCenter:ko.observable(getTranslation('profile.callCenter')),
                        tryAgain:ko.observable(getTranslation('profile.tryAgain')),
                        resendCode:ko.observable(getTranslation('profile.resendCode')),
                        call:ko.observable(getTranslation('profile.call')),
                        enterCode:ko.observable(getTranslation('profile.enterCode')),
                        notRecieved:ko.observable(getTranslation('profile.notRecieved')),
                        titleCode: ko.observable(getTranslation('profile.titleCode')),
                        back: ko.observable(getTranslation('profile.back')),
                        history: ko.observable(getTranslation("history.history")),
                        submit: ko.observable(getTranslation("main.submit")),
                        title: ko.observable(getTranslation('profile.addVehicle')),
                        model: ko.observable(getTranslation('products.model')),
                        chassNumber: ko.observable(getTranslation('profile.chassisNumber')),
                        motor: ko.observable(getTranslation('main.motor')),
                        codeLbl: ko.observable(getTranslation('resetPassword.codeLbl')),
                        code: ko.observable(getTranslation('resetPassword.code')),
                        skipActionLbl: ko.observable(app.label().skipActionLbl())
                    });
                }
    }
    
    return addVehicleLoginContentViewModel;
});
