define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojmodule-element-utils', 'ojs/ojmessaging', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojvalidationgroup'],
        function (oj, ko, app, services, commonhelper, moduleUtils, Message) {
            function resetPasswordContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.columns = ko.observable();
                self.label = ko.observableArray([]);
                self.currentRawPassValue = ko.observable();
                self.currentPassValue = ko.observable();
                self.errorMsgEmail = ko.observable();
                self.errorMsgPhone = ko.observable();
                self.errorMsgPass = ko.observable();
                self.groupValid = ko.observable();
                self.confirmTrue=ko.observable(true);

                self.fields = {
                    phone:ko.observable(),
                    email: ko.observable(),
                    code: ko.observable(),
                    newPass: ko.observable(),
                    confirmPass: ko.observable()
                };

                self.visibleForm = {
                    showLabelPhone:ko.observable(true),
                    showLabelEmail:ko.observable(false),
                    showPhone:ko.observable(false),
                    showEmail:ko.observable(true),
                    emailForm: ko.observable(true),
                    codeForm: ko.observable(false),
                    resetForm: ko.observable(false)
                };

                self.confirmEmail = function () {
                    self.visibleForm.emailForm(false);
                    self.visibleForm.codeForm(true);
                };

                self.code = function () {
                    self.visibleForm.emailForm(false);
                    self.visibleForm.codeForm(false);
                    self.visibleForm.resetForm(true);
                };
                self.emailValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
                                hint: self.label.emailHint(),
                                messageDetail: self.label.emailmessageDetail()
                            }
                        }];
                });
                self.mobileValidator = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[0]+[1]+[0-9]{9,9}",
                                hint: self.label.phoneHint(),
                                messageDetail: self.label.phoneMessageDetail()
                            }
                        }];
                });
                self.validatorPass = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: "[a-zA-Z0-9]{8,}",
                                hint: self.label.passHint(),
                                messageDetail: self.label.passMessageDetail()
                            }
                        }];
                });
                self.emailChangedHandler = function (event) {
                    app.loading(true);
                    if (event) {
                        var email = event.detail.value;
                        var payload = {
                            "email": email
                        };
                        services.postGeneric("users/is-email-valid", payload).then(data => {
                            if(data){
                                app.loading(false);
                            if (data.valid == "false" && email) {
                               self.errorMsgEmail([]);
                               self.confirmTrue(false);
                            } else {
                                self.confirmTrue(true);
                                 self.errorMsgEmail([{
                                        summary: self.label.emailResetValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            }
                        }
                        }, app.failCbFn);
                    }
                };
                self.phoneChangedHandler = function (event) {
                    app.loading(true);
                    if (event) {
                        var phone = event.detail.value;
                        var payload = {
                            "phone": phone
                        };
                        services.postGeneric("users/is-phone-valid", payload).then(data => {
                            if(data){
                                app.loading(false);
                            if (data.valid == "false" && phone) {
                                self.errorMsgPhone([]);
                                self.confirmTrue(false);
                            } else {
                                self.confirmTrue(true);
                                self.errorMsgPhone([{
                                        summary: self.label.phoneResetValid(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            }
                        }
                        }, app.failCbFn);
                    }
                };

                self.reset = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
//                    app.loading(true);
                    self.getSuccs = function () {
                        app.loading(false);
                        oj.Router.rootInstance.go('logIn');
                    };
                    if (self.fields.newPass() == self.fields.confirmPass()) {
                        app.loading(true);
                        var payload = {
                            "phone":self.fields.phone(),
                            "email": self.fields.email(),
                            "password": self.fields.newPass()
                        };
                        services.postGeneric("users/forgetPass", payload).then(self.getSuccs, app.failCbFn);
                    }

                };
                self.byPhone = function () {
                    if (self.visibleForm.showPhone() && self.visibleForm.showLabelEmail()) {
                        self.confirmTrue(true);
                        self.visibleForm.showPhone(false);
                        self.visibleForm.showLabelPhone(true);
                        self.visibleForm.showLabelEmail(false);
                        self.visibleForm.showEmail(true);
                    } else {
                        self.confirmTrue(true);
                        self.visibleForm.showPhone(true);
                        self.visibleForm.showLabelPhone(false);
                        self.visibleForm.showLabelEmail(true);
                        self.visibleForm.showEmail(false);
                    }
                };
                ko.computed(()=>{
                    self.passChangedHandler=function(){
                     if (self.fields.newPass() == self.fields.confirmPass()) {
                                self.errorMsgPass([]);
                            } else {
                                self.errorMsgPass([{
                                        summary: self.label.invalidPass(),
                                        detail: "", severity: Message.SEVERITY_TYPE.ERROR
                                    }]);
                            } 
                };
                });

                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    if (!app.isUserLoggedIn()) {
                        app.globalHeaderConfig(true, true, true, 'login');
                    }else{
                        app.globalHeaderConfig(true, true, true, 'dashboard');
                    }
                    var pass,confirm;
                    document.getElementById("password").addEventListener('rawValueChanged', function (event) {
                        pass = event.detail.value;
                        self.fields.newPass(pass);
                    });
                    document.getElementById("confirm-pass").addEventListener('rawValueChanged', function (event) {
                            confirm = event.detail.value;
                            self.fields.confirmPass(confirm);
                        });
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.transitionCompleted = function () {
                    app.animationSwitcher();
                };
                function initTranslations() {
                    app.headerTitle(getTranslation('resetPassword.title'));
                    self.label = {
                        phoneResetValid: ko.observable(getTranslation('main.phoneResetValid')),
                        emailResetValid: ko.observable(getTranslation('main.emailResetValid')),
                        phoneHint: ko.observable(getTranslation('main.phoneHint')),
                        phoneMessageDetail: ko.observable(getTranslation('main.phoneMessageDetail')),
                        passHint: ko.observable(getTranslation('main.passHint')),
                        passMessageDetail: ko.observable(getTranslation('main.passMessageDetail')),
                        placeholderEmail: ko.observable(getTranslation('main.placeholderEmail')),
                        placeholderPhone: ko.observable(getTranslation('main.placeholderPhone')),
                        invalidPass: ko.observable(getTranslation('main.invalidPass')),
                        emailHint: ko.observable(getTranslation('main.emailHint')),
                        emailmessageDetail: ko.observable(getTranslation('main.emailmessageDetail')),
                        changeByEmail:ko.observable(getTranslation('resetPassword.changeByEmail')),
                        changeByPhone:ko.observable(getTranslation('resetPassword.changeByPhone')),
                        phone:ko.observable(getTranslation('profile.phone')),
                        title: ko.observable(getTranslation('resetPassword.title')),
                        email: ko.observable(getTranslation('main.email')),
                        confirmEmail: ko.observable(getTranslation('resetPassword.confirmEmail')),
                        codeLbl: ko.observable(getTranslation('resetPassword.codeLbl')),
                        code: ko.observable(getTranslation('resetPassword.code')),
                        newPass: ko.observable(getTranslation('resetPassword.newPass')),
                        confirmPass: ko.observable(getTranslation('resetPassword.confirmPass')),
                        reset: ko.observable(getTranslation('resetPassword.reset'))
                    };
                }
            }

            return resetPasswordContentViewModel;
        });
