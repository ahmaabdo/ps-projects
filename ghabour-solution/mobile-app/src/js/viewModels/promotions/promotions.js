define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils'],
        function (oj, ko, app, moduleUtils) {

            function OurLocationViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observableArray([]);

                self.connected = function () {
                    // Implement if needed
                    app.loading(false);       
                    initTranslations();
                    app.globalHeaderConfig(true, true,true, 'dashboard');
                };


                self.transitionCompleted = function () {
                    app.animationSwitcher();
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                    app.headerTitle(getTranslation('main.promotions'));
                    self.labels = {};
                }
            }

            return new OurLocationViewModel();
        }
);
