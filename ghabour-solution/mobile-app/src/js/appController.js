define(['knockout', 'ojs/ojrouter', 'ojs/ojthemeutils', 'ojs/ojmodule-element-utils', 'ojs/ojmoduleanimations',
    'ojs/ojknockouttemplateutils', 'ojs/ojarraydataprovider', 'config/services', 'ojs/ojknockout', 'ojs/ojmodule-element', 'ojs/ojmessages'],
        function (ko, Router, ThemeUtils, moduleUtils, ModuleAnimations, KnockoutTemplateUtils, ArrayDataProvider, services) {
            function ControllerViewModel() {
                var self = this;
                self.KnockoutTemplateUtils = KnockoutTemplateUtils;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observable();
                self.userLoggedIn = ko.observable(false);
                self.image = ko.observable();
                self.lang = ko.observable();
                self.vehicleFlag = ko.observable("T");
                self.phoneFlag = ko.observable("T");
                self.headerTitle = ko.observable("Dashboard");
                self.switcherCallBack = ko.observable('coverStart');
                self.languageSwitchLbl = ko.observable();
                var win = window;
                var loader = $('#loader');
                self.headerConfig = {
                    islangBtnVisible: ko.observable(false),
                    isHeaderVisible: ko.observable(false),
                    isBackBtnVisible: ko.observable(false),
                    parent: ko.observable(null)
                };
                self.globalHeaderConfig = function (headerVisible, backBtnVisible, langBtnVisible, p) {
                    self.headerConfig.isHeaderVisible(headerVisible).isBackBtnVisible(backBtnVisible).islangBtnVisible(langBtnVisible).parent(p);
                    win.scrollTo(0, 0);
                    try {
                        self.router._stateId() == 'landing' || self.router._stateId() == 'dashboard' ?
                                StatusBar.backgroundColorByHexString("#0b0a09") : StatusBar.backgroundColorByHexString("#002c5f");
                    } catch (e) {
                    }
                };

                self.loading = function (enabled) {
                    enabled ? loader.show() : loader.hide();
                };

                self.refreshViewForLanguage = ko.observable(false);
                self.personDetails = ko.observable("");
                self.messages = ko.observableArray("");
                self.userData = ko.observable();
                self.vehicleData = ko.observable();
                self.productData = ko.observable();
                self.locationData = ko.observable();
                self.faqData = ko.observable();
                self.testDriveData = ko.observable();
                self.complainData = ko.observable();
                self.inquiryData = ko.observable();
                self.serviceData = ko.observable();
                self.messagesDataprovider = new ArrayDataProvider(self.messages);
                self.imagePath = ko.observable("");

                self.continueWithGoogle = function (data, event) {
                    window.plugins.googleplus.login(
                            {
                                'scopes': 'profile email',
                                'webClientId': '715831709945-d6a1glp8f183n6qo2i1uf1p382294afn.apps.googleusercontent.com',
                                'offline': true
                            },
                            function (obj) {
                                console.log(obj)
                                self.userLoggedIn(true);
                                var payload = {
                                    "username": obj.displayName,
                                    "firstName": obj.givenName,
                                    "email": obj.email,
                                    "lastName": obj.familyName,
                                    "googleId": obj.userId,
                                    "googleIdToken": obj.idToken,
                                    "profileImg": obj.imageUrl
                                };
                                self.getDataFromContinue(obj, payload);
                            },
                            function (msg) {
                                self.userLoggedIn(false);
                                self.imagePath("");
                            }
                    );
                };

                self.handleActivated = function (info) {
                    window.plugins.googleplus.trySilentLogin(
                            {
                                'scopes': 'profile email',
                                'webClientId': '715831709945-d6a1glp8f183n6qo2i1uf1p382294afn.apps.googleusercontent.com',
                                'offline': true
                            },
                            function (obj) {
                                self.userLoggedIn(true);
                                self.imagePath(obj.imageUrl);
                                self.userObject = obj;
                            },
                            function (msg) {
                                self.userLoggedIn(false);
                                self.imagePath("");
                            }
                    );
                };
                self.checkLoginState = function () {
                    //facebook init to open dialog for fb sign in
                    FB.init({
                        appId: '599883990841573',
                        status: true,
                        cookie: true,
                        xfbml: true,
                        version: 'v5.0'
                    });
                    (function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id))
                            return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "https://connect.facebook.net/en_US/sdk.js#version=v5.0&appId=599883990841573&status=true&cookie=true&xfbml=true";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));

                    //after user login with fb account 
                    FB.login(function (response) {
                        if (response.status == 'connected') {
                            self.fbIdToken = response.authResponse.accessToken;
                            testAPI();
                        } else {
                            console.log('User cancelled login or did not fully authorize.');
                        }
                    }, {
                        scope: 'email',
                        return_scopes: true
                    });
                    function testAPI() {
                        FB.api('/me',
                                {fields: "id,picture,email,first_name,gender,hometown,location,last_name,name,timezone,work"},
                                function (response) {
                                    console.log(response);
                                    var payload = {
                                        "username": response.name,
                                        "firstName": response.first_name,
                                        "email": response.email,
                                        "lastName": response.last_name,
                                        "fid": response.id,
                                        "fbIdToken": self.fbIdToken
//                                    "profileImg": response.picture.data
                                    };
                                    self.getDataFromContinue(response, payload);
                                });
                        oj.Router.rootInstance.go('dashboard');
                    }
                };
                self.getDataFromContinue = function (outData, payload) {
                    self.loading(true);
                    services.postGeneric("users/isContinueWithGoogle", payload).then(data => {
                        console.log(data);
                        self.loading(false);
                        if (data.length > 0) {
                            localStorage.setItem("personDetails", JSON.stringify(data[0]));
                            self.personDetails(JSON.parse(localStorage.getItem("personDetails")));
                            self.name(self.personDetails().username);
                            if (data.length > 0 && outData) {
                                if (data[0].username && data[0].password) {
                                    if (!data[0].phone) {
                                        oj.Router.rootInstance.go('phoneScreen');
                                    } else
                                        self.login(data[0].username.toLowerCase(), data[0].password);
                                }
                            }
                        } else {
                            self.createMessage("error", "Can't Sign in Account");
                        }
                    }, self.failCbFn);
                };
                self.fbLogout = function () {
                    FB.getLoginStatus(function (response) {
                        console.log(response.status);
                        if (response.status == 'connected') {
                            FB.logout(function (response) {
                            });
                        }
                    });
                };
                self.googleDisconnect = function (data, event) {
                    window.gapi.auth2.getAuthInstance().disconnect().then(
                            function () {
                                self.imagePath('');
                            },
                            function (e) {
                                console.log(e);
                            });
                };

                self.setLocale = function (lang) {
                    oj.Config.setLocale(lang,
                            function () {
                                self.refreshViewForLanguage(!self.refreshViewForLanguage());
                                $("html").attr("lang", lang);
                                if (lang === 'ar') {
                                    $("html").attr("dir", "rtl");
                                } else {
                                    $("html").attr("dir", "ltr");
                                }
                                initTranslations();
                            }
                    );
                };

                self.getLocale = function () {
                    return oj.Config.getLocale();
                };

                self.switchLanguage = function () {
                    if (self.getLocale() == "ar") {
                        self.setLocale("en-US");
                        self.lang("en-US");
                        localStorage.setItem("selectedLanguage", "en-US");
                    } else {
                        self.setLocale("ar");
                        self.lang("ar");
                        localStorage.setItem("selectedLanguage", "ar");
                    }
                    self.animationSwitcher();
                };

                self.animationSwitcher = function () {
                    localStorage.getItem("selectedLanguage") == "ar" ?
                            self.switcherCallBack('revealEnd') : self.switcherCallBack('coverStart');
                };

                var selectedLanguage = localStorage.getItem("selectedLanguage");
                if (selectedLanguage !== null && (selectedLanguage === "en-US" || selectedLanguage === "ar")) {
                    self.setLocale(selectedLanguage);
                }
                var platform = ThemeUtils.getThemeTargetPlatform();

                self.createMessage = function (severity, summary) {
                    self.loading(false);
                    self.messages([]);
                    self.messages.push({
                        severity: severity,
                        summary: summary,
                        autoTimeout: 0
                    });
                };

                self.failCbFn = function () {
                    self.loading(false);
                    self.createMessage("error", self.label().serviceErrMsg());
                };

                self.getUserData = async () => {
                    if (self.personDetails()) {
                        var personId = self.personDetails().id.toString();
                        console.log(`Person ID = ${personId}`);
                        services.getGeneric("users/getUserData/12").then(data => {
                            if (data) {
                                Object.keys(data).forEach(function (dataKey) {
                                    for (var i in data[dataKey]) {
                                        var obj = data[dataKey][i];
                                        Object.keys(obj).forEach(function (key) {
                                            if (obj[key] == 'null')
                                                obj[key] = self.label().notAvailable();
                                        });
                                    }
                                });
                                self.userData(data);
                            }
                        }, {});
                        await services.getGeneric("vehicleHistory/12").then(data => {
                            if (data) {
                                self.vehicleData(data);
                            }
                        }, {});

                        await services.getGeneric("attachment/" + personId).then(data => {
                            if (data.length > 0) {
                                self.image(data[0]);
                                self.personDetails().image = self.image();
//                                self.profileImg(data[0].attachment);
                            } else {
                                self.personDetails().image = null;
//                                self.profileImg('');
                            }
                        }, {});
                    }
                };
//                (async function () {
//                    console.log('Getting General Data...');
//
//                    await services.getGeneric("complainService/complainsType").then(data => {
//                        if (data) {
//                            self.complainData(data);
//                        }
//                    }, {});
//
//                    await services.getGeneric("inquiryService/inquiryType").then(data => {
//                        if (data) {
//                            self.inquiryData(data);
//                        }
//                    }, {});
//
//                    await services.getGeneric("testDriveService/testDriveType").then(data => {
//                        if (data) {
//                            self.testDriveData(data);
//                        }
//                    }, {});
//
//                    await services.getGeneric("FAQService").then(data => {
//                        if (data) {
//                            self.faqData(data);
//                        }
//                    }, {});
//
//                    await services.getGeneric("productsService").then(data => {
//                        if (data) {
//                            self.productData(data);
//                        }
//                    }, {});
//
////                    await services.getGeneric("modelService").then(data => {
////                        if (data) {
////                            self.modelData(data);
////                        }
////                    }, {});
//                    await services.getGeneric("LocationService").then(data => {
//                        if (data) {
//                            self.locationData(data);
//                        }
//                    }, {});
//
//                    await services.getGeneric("serviceService/serviceType").then(data => {
//                        if (data) {
//                            self.serviceData(data);
//                        }
//                    }, {});
//
//                    console.log('Getting General Data Done.');
//                })();
                self.changeLang = function () {
                    self.loading(true);
                    if (!self.lang()) {
                        self.lang("en-US");
                    }
                    var payload = {
                        "id": self.personDetails().id,
                        "defaultLang": self.lang()
                    };
                    services.postGeneric("users/editDefaultLang", payload).then(() => {
                        self.loading(false);
                    }, self.failCbFn);

                };
                self.updateVehicle = function (flag) {
//                    self.vehicleFlag(flag);
                    var payload = {
                        "id": self.personDetails().id,
                        "vehicleFlag": flag
                    };
                    services.postGeneric("users/editVehicleFlag", payload);
                };
                self.login = function (userName, password) {
                    self.loading(true);
                    services.getGeneric("users/getUserDetails/" + userName + "/" + password).then(data => {
                        self.userLoggedIn(true);
                        var attachmentView = function (dataImg) {
                            if (dataImg.length > 0) {
                                self.image(dataImg[0]);
                                data[0].image = self.image();
                            } else {
                                data[0].image = null;
                                self.image('');
                            }
                            if (data[0].phone) {
                                self.phoneFlag("T");
                            } else {
//                                self.phoneFlag("F");
                            }
                            localStorage.setItem("personDetails", JSON.stringify(data[0]));
                            self.personDetails(JSON.parse(localStorage.getItem("personDetails")));
                            localStorage.setItem("selectedLanguage", data[0].defaultLang);
                            self.setLocale(self.personDetails().defaultLang);
//                            self.vehicleFlag(data[0].vehicleFlag);
                            if (data[0].vehicleFlag == "T") {
                                data.find(e => e.username == userName && e.password == password) ?
                                        oj.Router.rootInstance.go('dashboard') : self.createMessage("error", "wrong username or password");
                            } else {
                                data.find(e => e.username == userName && e.password == password) ?
                                        oj.Router.rootInstance.go('addVehicleLogin') : self.createMessage("error", "wrong username or password");
                            }
                            self.getUserData();
                        };
                        //For development purpose only.
                        services.getGeneric("attachment/" + data[0].id.toString()).then(attachmentView, self.failCbFn);
                    }, err => {
                        self.loading(false);
                        self.createMessage("error", self.label().serviceErrMsg());
                    });
                };

                // Router setup
                self.router = Router.rootInstance;

                initTranslations();
                self.router.configure({
                    'landing': {label: self.label().landing(), value: "main/landing", title: self.label().landing(), isDefault: true},
                    'login': {label: self.label().login(), value: "main/login", title: self.label().login()},
                    'signup': {label: self.label().signup(), value: "main/signup", title: self.label().signup()},
                    'faqs': {label: self.label().faq(), value: "main/faqs", title: self.label().faq()},
                    'dashboard': {label: self.label().dashboard(), value: "secure/dashboard/dashboard", title: self.label().dashboard()},
                    'ourLocation': {label: self.label().ourLocation(), value: "main/ourLocation", title: self.label().ourLocation()},
                    'offers': {label: self.label().offers(), value: "main/offers", title: self.label().offers()},
                    'maintenance': {label: self.label().maintenance(), value: "secure/maintenance/maintenance", title: self.label().maintenance(),
                        canEnter: () => {
                            return (self.userLoggedIn() && self.vehicleFlag() == 'T' && self.phoneFlag() == 'T');
                        }},
                    'bookMaintenance': {label: self.label().bookMaintenance(), value: "secure/maintenance/bookMaintenance", title: self.label().bookMaintenance()},
                    'complain': {label: self.label().complain(), value: "secure/complain/complain", title: self.label().complain(),
                        canEnter: () => {
                            return (self.userLoggedIn() && self.vehicleFlag() == 'T' && self.phoneFlag() == 'T');
                        }},
                    'testDrive': {label: self.label().testDrive(), value: "secure/testDrive/testDrive", title: self.label().testDrive(),
                        canEnter: () => {
                            return self.userLoggedIn();
                        }},
                    'reservationPayment': {label: self.label().reservationPayment(), value: "secure/reservationPayment/reservationPayment", title: self.label().reservationPayment(),
                        canEnter: () => {
                            return self.userLoggedIn();
                        }},
                    'vehicleHistory': {label: self.label().vehicleHistory(), value: "secure/vehicleHistory/vehicleHistory", title: self.label().vehicleHistory(), canEnter: () => {
                            return self.userLoggedIn();
                        }},
                    'cars': {label: self.label().cars(), value: "secure/vehicleHistory/cars", title: self.label().cars()},
                    'inquiry': {label: self.label().inquiry(), value: "secure/inquiry/inquiry", title: self.label().inquiry(), canEnter: () => {
                            return (self.userLoggedIn() && self.vehicleFlag() == 'T' && self.phoneFlag() == 'T');
                        }},
                    'service': {label: self.label().service(), value: "secure/service/service", title: self.label().service(), canEnter: () => {
                            return (self.userLoggedIn() && self.vehicleFlag() == 'T' && self.phoneFlag() == 'T');
                        }},
                    'profile': {label: self.label().profile(), value: "secure/profile/profile", title: self.label().profile(), canEnter: () => {
                            return self.userLoggedIn();
                        }},
                    'resetPassword': {label: self.label().resetPassword(), value: "resetPassword/resetPassword", title: self.label().resetPassword()},
                    'phoneScreen': {label: self.label().phoneScreen(), value: "resetPassword/phoneScreen", title: self.label().phoneScreen()},
                    'carHistory': {label: self.label().carHistory(), value: "secure/vehicleHistory/carHistory", title: self.label().carHistory()},
                    'products': {label: self.label().products(), value: "products/products", title: self.label().products()},
                    'productInfo': {label: self.label().productInfo(), value: "products/productInfo", title: self.label().productInfo()},
                    'models': {label: self.label().models(), value: "products/models", title: self.label().models()},
                    'promotions': {label: self.label().promotions(), value: "promotions/promotions", title: self.label().promotions()},
                    'addVehicle': {label: self.label().addVehicle(), value: "secure/profile/addVehicle", title: self.label().addVehicle(),
                        canEnter: () => {
                            return self.userLoggedIn();
                        }},
                    'addVehicleLogin': {label: self.label().addVehicle(), value: "resetPassword/addVehicleLogin", title: self.label().addVehicle(),
                        canEnter: () => {
                            return self.userLoggedIn();
                        }}
                });

                self.backBtnAction = function (parent) {
                    localStorage.getItem("selectedLanguage") == "ar" ?
                            self.switcherCallBack('coverStart') : self.switcherCallBack('revealEnd');
                    parent = self.headerConfig.parent();
                    if (!self.userLoggedIn())
                        oj.Router.rootInstance.go('landing');
//                    else if(self.userLoggedIn()&& self.vehicleFlag()=="F")
//                        oj.Router.rootInstance.go('login');
                    else
                        parent ? oj.Router.rootInstance.go(parent) : oj.Router.rootInstance.go('dashboard');
                };
                Router.defaults['urlAdapter'] = new Router.urlParamAdapter();

                function switcherCallback(context) {
                    return self.switcherCallBack();
                }

                //refresh view when language change
                self.refreshData = ko.computed(function () {
                    if (self.refreshViewForLanguage()) {
                        self.setLocale(localStorage.getItem("selectedLanguage"));
                    }
                });
                //for development case only
//                ko.computed(() => {
//                    self.personDetails(JSON.parse(localStorage.getItem("personDetails")));
//                    self.personDetails() ? window.history.pushState({}, document.title, "/" + "?root=dashboard") : window.history.pushState({}, document.title, "/" + "");
//                    if (!self.userData())
//                        self.getUserData();
//                });

                self.headerModuleConfig = ko.observable({'view': [], 'viewModel': null});
                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});

                self.loadModule = function () {
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath}),
                            moduleUtils.createView({'viewPath': 'views/header.html'}).then(function (view) {
                                self.headerModuleConfig({'view': view, 'viewModel': new self.getHeaderModel()});
                            })
                        ]);
                        masterPromise.then(
                                function (values) {
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                });
                    });
                };

                ko.computed(() => {
                    document.addEventListener("deviceready", onDeviceReady, false);
                    function onDeviceReady() {
                        document.addEventListener("backbutton", function (e) {
                            e.preventDefault();
                            if (self.router._stateId() == 'landing' || self.router._stateId() == 'dashboard') {
                                navigator.app.exitApp();
                            } else {
                                self.backBtnAction(self.headerConfig.parent());
                                return false;
                            }
                        }, false);
                    }
                });

                if (platform == 'ios') {
                    $(".custom-header").css("width", "100%");
                    $(".custom-header").css("height", "3.2rem");
                }

                self.moduleAnimation = ModuleAnimations.switcher(switcherCallback);

                // Header Setup
                self.getHeaderModel = function () {
                    this.pageTitle = "GB";
                    this.transitionCompleted = function () {
                        // Adjust content padding after header bindings have been applied
                        self.adjustContentPadding();
                    };
                };


                self.adjustContentPadding = function () {
                    $(".oj-hybrid-applayout-header-title").css('visibility', 'hidden');
                    var topElem = document.getElementsByClassName('oj-applayout-fixed-top')[0];
                    var contentElem = document.getElementsByClassName('oj-applayout-content')[0];
                    var bottomElem = document.getElementsByClassName('oj-applayout-fixed-bottom')[0];

                    if (topElem)
                        contentElem.style.paddingTop = topElem.offsetHeight + 'px';
                    if (bottomElem)
                        contentElem.style.paddingBottom = bottomElem.offsetHeight + 'px';
                    if (contentElem)
                        contentElem.classList.add('oj-complete');
                };

                function initTranslations() {
                    self.label({
                        skipActionLbl: ko.observable(getTranslation("main.skipActionLbl")),
                        phoneScreen: ko.observable(getTranslation('resetPassword.phoneScreenTitle')),
                        addVehicle: ko.observable(getTranslation('profile.addVehicle')),
                        languageSwitchLbl: ko.observable(getTranslation('main.language')),
                        passErrorMsg: ko.observable(getTranslation('main.passErrorMsg')),
                        notAvailable: ko.observable(getTranslation("complain.notAvailable")),
                        serviceErrMsg: ko.observable(getTranslation('main.serviceErrMsg')),
                        landing: ko.observable(getTranslation('main.landing')),
                        login: ko.observable(getTranslation('main.signIn')),
                        signup: ko.observable(getTranslation("main.signUp")),
                        dashboard: ko.observable(getTranslation('main.dashboard')),
                        maintenance: ko.observable(getTranslation('main.maintenance')),
                        complain: ko.observable(getTranslation('main.complain')),
                        service: ko.observable(getTranslation('main.service')),
                        inquiry: ko.observable(getTranslation("main.inquiry")),
                        vehicleHistory: ko.observable(getTranslation('main.vehicleHistory')),
                        products: ko.observable(getTranslation('main.products')),
                        profile: ko.observable(getTranslation('main.profile')),
                        offers: ko.observable(getTranslation('main.offers')),
                        ourLocation: ko.observable(getTranslation("main.ourLocation")),
                        faq: ko.observable(getTranslation('main.faq')),
                        promotions: ko.observable(getTranslation('main.promotions')),
                        resetPassword: ko.observable(getTranslation("resetPassword.title")),
                        testDrive: ko.observable(getTranslation('main.testDrive')),
                        reservationPayment: ko.observable(getTranslation('main.reservationPayment')),
                        cars: ko.observable(getTranslation('main.cars')),
                        bookMaintenance: ko.observable(getTranslation('maintenance.bookMaintenance')),
                        carHistory: ko.observable(getTranslation("main.carHistory")),
                        productInfo: ko.observable(getTranslation('main.productInfo')),
                        models: ko.observable(getTranslation('main.models'))
                    });
                }
            }

            return new ControllerViewModel();
        }
);
