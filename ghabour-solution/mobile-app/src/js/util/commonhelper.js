define([], function () {

    function commonHelper() {
        var self = this;
        self.getUsers = "users/getUsers";
        self.addUser = "users/ADD";
        self.incidents = "incidents";
        self.contact = "contact";
        self.customRest = "customRest";
        self.get = "GET";
        self.post = "POST";
        self.getPayload = "null";

        self.getInternalRest = function () {
//         var host = "http://127.0.0.1:7101/Ghabour/rest/";
        //Default localhost for android emulator
//        var host = "http://10.0.2.2:7101/Ghabour/rest/";
         var host = "https://hrss.dur.sa/Ghabbour-backend-test/rest/";
//         var host = "https://130.61.214.91/Ghabbour-backend-test/rest/";
            return host;
        };

    }

    return new commonHelper();
});