define({
    "main": {
        "skipActionLbl": "الأختيار لاحقاً",
        "motor":"المحرك",
        "continueWithGoogle":"متابعة باستخدام حساب Google",
        "findNearestBranch": "ابحث عن أقرب فرع من موقعك",
        "locationErr": "حدث خطأ غير معروف أثناء فتح الموقع ، يرجى المحاولة مرة أخرى لاحقًا",
        "searchLbl":"بحث",
        "engineNumberLbl":"رقم المحرك",
        "nationalIdLbl":"الرقم القومى",
        "success":"تم إنشاء الحساب بنجاح",
        "emailResetValid": "البريد الإلكتروني غير صالح.",
        "phoneResetValid": "رقم الهاتف غير صحيح",
        "invalidPass": "كلمة المرور غير متطابقة",
        "phoneHint": "أدخل رقم هاتف صحيح.",
        "phoneMessageDetail": "رقم هاتف غير صالح ، يجب أن يبدأ الهاتف برقم 01",
        "attachErr":"تحقق مما إذا كان الملف صورة و ليس اكبر من 1 ميجا بايت!",
        "phoneValid": "رقم الهاتف موجود بالفعل.",
        "emailValid": "البريد الالكتروني موجود بالفعل.",
        "usernameValid": "اسم المستخدم موجود بالفعل",
        "placeholderUsername": "اسم المستخدم",
        "placeholderFirstname": "الاسم",
        "placeholderLastname": "اسم العائلة",
        "placeholderPassword": "كلمه المرور",
        "save": "حفظ",
        "loadingTxt": "جاري التحميل",
        "placeholderConfPassword": "تأكيد كلمة المرور",
        "placeholderEmail": "عنوان البريد الالكتروني",
        "placeholderPhone": "رقم الهاتف المحمول",
        "placeholderChassis": "رقم الشاسيه",
        "passHint": "أدخل ما لا يقل عن 8 أحرف أو أرقام.",
        "passMessageDetail": "يجب إدخال 8 أحرف أو أرقام على الأقل .",
        "usernameHint": "أدخل ما لا يقل عن 3 أحرف أو أرقام بدون مسافة.",
        "usernameMessageDetail": "يجب إدخال ما لا يقل عن 3 أحرف أو أرقام وعدم وجود مسافة.",
        "emailHint": "أدخل تنسيق بريد إلكتروني صالح",
        "emailmessageDetail": "تنسيق البريد الإلكتروني غير صحيح",
        "placeholder": "اختر قيمة",
        "passErrorMsg": "اسم المستخدم أو كلمة المرور خاطئة",
        "serviceErrMsg": "الخدمة غير متوفرة ، يرجى المحاولة مرة أخرى لاحقًا",
        "big": "عروض غبور",
        "bigOffer": "فقط من اجلك",
        "description": "التفاصيل",
        "header": "غبور اوتو",
        "parag": "أفضل مما تعتقد",
        "appname": "Ghabbour - AR",
        "signIn": "تسجيل الدخول",
        "signUp": "تسجيل حساب",
        "faq": "الاسئله الشائعة",
        "status": "الحالة",
        "noDataAvailableTxt": "لا توجد بيانات متاحةالآن.",
        "ourLocation": "مواقعنا",
        "noThingTxt": "لا توجد بيانات!!",
        "emptyTxt": "لا توجد بيانات لعرضها.",
        "offers": "العروض",
        "userName": "اسم المستخدم",
        "password": "كلمة المرور",
        "confirmPassword": "تأكيد كلمة المرور",
        "email": "عنوان البريد الالكتروني",
        "forgetPassword": "نسيت كلمة المرور",
        "haveAccount": "لاتمتلك حساب؟",
        "createNewAccount": "انشاء حساب جديد",
        "firstName": "الاسم",
        "lastName": "الاسم الاخير",
        "mobileNumber": "رقم الهاتف",
        "createAccount": "انشاء حساب",
        "cars": "سيارات",
        "app": "تطبيق",
        "add": "اضافة",
        "history": "السجل",
        "complain": "شكوي",
        "inquiry": "استفسار",
        "service": "خدمة",
        "maintenance": "صيانة",
        "vehicle": "السيارة",
        "search": "احصل على الاتجاهات",
        "ghabbourlocation": "موقع غبور للسيارات",
        "city": "مدينة",
        "location": "موقعك",
        "branch": "الفرع",
        "placeHolder": "من فضلك اختر قيمة",
        "testDrive": "اختبار قيادة",
        "reservationPayment": "دفع الحجز",
        "vehicleHistory": "سجل سياراتي",
        "promotions": "عروض غبور",
        "submit": "تقديم",
        "products": "المنتجات",
        "language": "English",
        "logout": "تسجيل الخروج",
        "landing": "غبور - الصفحة الرئيسية",
        "dashboard": "الصفحة الرئيسية",
        "profile": "الصفحة الشخصية",
        "productInfo": "معلومات المنتج",
        "carHistory": "تاريخ السيارة",
        "models": "نماذج"
    },
        "maintenance": {
            "estTime":"اجمالى الوقت",
            "branch":"الفرع",
            "operation":"العملية",
            "comment":"تعليق",
            "date":"اليوم",
            "holidayErrMsg":"ليس يومًا صالحًا ، يرجى تحديد يوم آخر",
            "vehicle":"السيارة",
            "details":"التفاصيل",
            "price":"اجمالى السعر",
            "add":"اضافة",
            "successMsg": "تم اضافة الصيانة بنجاح!!",
            "chooseDetail": "اختر للعثور على التفاصيل التي تحتاجها"
        },
    "complain": {
        "success":"تم تقديم الشكوى بنجاح!",
        "complainType": "نوع الشكوى",
        "notAvailable": "غير متاح",
        "addLbl": "اضافة شكوى",
        "complaintCategory": "فئة الشكوى",
        "complaintLetter": "تفاصيل الشكوى",
        "send": "إرسال"
    },
    "inquiry": {
        "success":"تم تقديم الاستفسار بنجاح!",
        "inquiryType": "نوع الاستفسار",
        "pcOrBajaj": "فئة الاستفسار",
        "addLbl": "اضافة استفسار",
        "inquiryDetail": "تفاصيل الاستفسار",
        "describeInquiry": "صف استفسارك",
        "inquiryHistory": "تاريخ الاستفسار",
        "title": "استفسار"
    },
    "service": {
        "success":"تم تقديم الخدمة بنجاح!",
        "serviceType": "نوع الخدمة",
        "rsaLocation": "ادخل الموقع",
        "addLbl": "اضافة خدمة",
        "rsaLocationHistory": "الموقع",
        "serviceCategory": "فئة الخدمات",
        "serviceDetail": "تفاصيل الخدمة",
        "describeService": "صف خدمتك",
        "serviceHistory": "تاريخ الخدمة الخاص بك",
        "title": "خدمة"
    },
        "testDrive": {
            "showroom": "المعارض",
            "brand": "نوع السيارة",
            "time": "الوقت"
        },
    "cars": {
        "itemsAsOperation": "العناصر:",
        "dateOfVisit": "Date of visit:",
        "comments": "تعليقات:"
    },
    "resetPassword": {
        "phoneScreenTitle":"اضافة رقم الهاتف ",
        "changeByEmail":"التغيير عن طريق البريد الإلكتروني",
        "changeByPhone":"التغيير عن طريق الهاتف",
        "confirmEmail": "تأكيد",
        "codeLbl": "الكود",
        "title": "إعادة تعيين كلمة المرور",
        "code": "ادخل الكود",
        "newPass": "كلمة المرور الجديدة",
        "confirmPass": "أكد كلمة المرور",
        "reset": "إعادة تعيين كلمة المرور"
    },
    "profile": {
        "callCenter": "إذا كان لديك أي مشكلة ، يرجى الاتصال على هاتف خدمة عملاء غبور: +20 2 3539 1047",
        "tryAgain": "لم تتلق رسالة نصية؟",
        "resendCode": "حاول ثانية",
        "call": "Ghabbour Call Center",
        "enterCode": "أدخل الرمز الذي تلقيته عبر الرسالة النصية",
        "notRecieved": " ، إذا لم تتلقى رمز التحقق خلال 30 ثانية / دقيقة واحدة",
        "titleCode": "تم إرسال الرمز إلى xxxxxxxxx",
        "delete": "حذف",
        "chassisNumber": "رقم السيارة : ",
        "addVehicle":"أضف سيارة",
        "vehicles":"السيارات",
        "placeholder": "ادخل قيمة",
        "title": "الأعدادات",
        "name": "الاسم  ",
        "email": "البريد الاكترونى  ",
        "phone": "رقم الهاتف : ",
        "edit": "تعديل",
        "changePass": "تغيير كلمة المرور",
        "back": "الرجوع",
        "newPass": "كلمة المرور الجديدة",
        "confirmPass": "تأكيد كلمة المرور",
        "currentPass": "كلمة المرور الحالية",
        "image": "اختر صورة شخصية"
    },
    "history": {
        "load": "جاري التحميل .....",
        "noDataText": "ليس لديك أي سجل سيارات حتى الآن.",
        "notAvailable": "غير متاح",
        "history": "السجل المتاح ",
        "eliteVehicle": "سيارة النخبة",
        "chassisNumber": "رقم الشاسيه  ",
        "carModel": "طراز السيارة ",
        "engineNumber": " رقم المحرك",
        "plateNumber": "رقم لوحة ",
        "vinNumber": "رقم المركبة ",
        "vehicleType": "نوع السيارة "
    },
    "products": {
        "title": "المنتجات",
        "productInfo": "معلومات المنتج",
        "categoryName": "اسم التصنيف",
        "currency": "عملة",
        "model": "موديل",
        "chooseCar": "يرجى اختيار ماركة سيارتك",
        "chooseCategory": "اختر فئة للعثور على المساعدة التي تحتاج إليها",
        "category": "الفئة"
    },
    "vehicleHistory": {
        "geely": "جيلى",
        "hyundai": "هيونداى",
        "mazda": "مازدا",
        "chery": "تشيرى",
        "title": "تاريخ السيارة",
        "chooseCar": "يرجى اختيار ماركة سيارتك",
        "chooseCategory": "اختر فئة للعثور على المساعدة التي تحتاج إليها"
    },
        "productInfo":{
            "dimentionsLbl":"الابعاد",
            "weigthLbl":"الوزن",
            "suspensionLbl":"نظام التعليق",
            "modelLbl":"الموديل",
            "powerPerformanceLbl":"القدرة و الاداء",
            "steerWheelsLbl":"التوجية و الاطارات",
            "saftySecurityLbl":"أنظمة الأمان",
            "optionListLbl":"الكماليات",
            "entertainmentSysLbl":"أنظمة الترفية",
            "length":"الطول الكلى (مم)",
            "width":"العرض الكلى(مم)",
            "heigth":"الارتفاع الكلى(مم)",
            "wheelBase":"قاعدة العجلات(مم)",
            "groundClearance":"ارتفاع المركبة عن الارض(مم)",
            "luggageCapasity":"السعة التخزينية لصندوق الامتعة(لتر)",
            "curbWeigth":"الوزن الفارغ(الاثقل)كج",
            "grossVehicleWeight":"الوزن الاجمالي كج",
            "front":"امامي",
            "rear":"خلفي",
            "enginCapasity":"السعة اللترية(CC)",
            "maxPower":"اقصى قوة حصانية(Hp/Rpm )",
            "maxTorque":"اقصى عزم (Nm /Rpm)",
            "valveSys":"نظام الصبابات",
            "cylinder":"عدد السيلندرات",
            "fuelTankCapacity":"سعة خزان الوقود (لتر)",
            "speedAutoTrans":"ناقل حركة اوتوماتيك 6 سرعات",
            "speedDualClutchTrans":"ناقل حركة ثنائى الاسطوانات 7 سرعات",
            "mdbs": "عجلة قيادة الية المؤازرة كهربائياً",
            "alloyWheels16": "جنوط سبور مقاس \" (215/70 R16)",
            "alloyWheels17": "جنوط سبور مقاس \" (225/60 R17)",
            "alloyWheels19": "جنوط سبور مقاس \" (245/45 R19)",
            "tirePressureMonSys": "نظام مراقبة ضغط الإطارات",
            "fullSpareAlloyWheel": "اطار احتياطي كامل الومنيوم",
            "multiFuncSteerWheel":"عجلة قيادة متعددة المهام",
            "radio5":"نظام سمعي مزود بشاشة 5\"",
            "audioTouchscreen": "شاشة \" 7 LCD ملونة لنظام الترفية",
            "bluetooth": "نظام Bluetooth للهاتف",
            "noOfSpeakers": "عدد السماعات",
            "frontUsb": "مدخل صوت  AUX/USB ",
            "wirlessCharger": "شاحن هوائي للهاتف الجوال",
            "rearUsbInput": "مدخل USB خلفى",
            "engineImmobilizer":"نظام منع ادارة المحرك",
            "speedSensing":"قفل اوتوماتيكي للأبواب حسب السرعة",
            "dualFrontAirBag":"وسائد هوائية امامية",
            "sideAndCurtainAirBag":"وسائد هوائية جانبية",
            "abs":"مكابح مضادة للانغلاق",
            "ebd":"توزيع إلكتروني لقوة المكابح",
            "ess":"اضاءة تحذيرية للوقوف المفاجئ",
            "esc":"برنامج الإتزان الإلكتروني",
            "vsm":"نظام تعزيز اتزان السيارة مع عجلة القيادة",
            "bas":"برنامج تدعيم المكابح",
            "hac":"برنامج التوازن عند المرتفعات ",
            "dbc":"برنامج التوازن عند المنحدرات",
            "epb":"مكابح يد الكترونية",
            "seatBeltReminder":"تحذير عدم ربط أحزمة الامان",
            "insideEcmMirror": "مراَه داخلية زاتية التعتيم مع بوصلة",
            "rearParkingAssist": "جهاز تحذير المسافة الخلفية",
            "frontAndRearParkingSensor": "جهاز تحذير المسافة الامامية و الخلفية",
            "blindSpotDetectSys": "كشف النقط العمياء",
            "rearViewCamera": "كاميرا للرؤية الخلفية",
            "allAroundViewMon": "رؤية 360 ° لمحيط السيارة",
            "keylessEntry": "ريموت كونترول وانذار",
            "smartKey": "مفتاح ذكي و امكانية إدارة المحرك بدون مفتاح",
            "remoteEngineStart": "إمكانية إدارة المحرك عن بعد",
            "superVisionCluster": "عدادات مزودة بشاشة ملونة TFT \"4.2",
            "manualLeveling": "بكرة تحكم فى مستوى إضاءة الفوانيس الأماميه",
            "powerOutlet": "مخرج كهرباء 12V(امامى و فى صندوق الامتعة)",
            "dms": "إمكانية إختيار نوعية القيادة",
            "cruiseControl": "مثبت السرعة",
            "tiltAndTelSteerWheel": "عجلة قيادة قابلة للإمالة و الامتداد",
            "rearPrivClass": "زجاج خلفى داكن اللون",
            "fullSolarGlass": "زجاج حراري",
            "outFoldMirror": "مرايات كهربائية الطى و الضبط مزودة بإشارات جانبية",
            "outMirror": "اضاءة في المرايات الخارجيه عند فتح السيارة",
            "outDoorHandler": "إضاءه فى المقابض الخارجيه عند فتح السيارة",
            "manAc": "تكييف هواء يدوي",
            "autoAc": "تكييف هواء أتوماتيك بتحكم ثنائي في درجة الحرارة و منقى للهواء",
            "rearAirVent": "فتحات تكييف خلفية",
            "glovCoolBox": "فتحة تكييف و إضاءه داخل درج التابلوه",
            "panoSlidRoof": "فتحة سقف بانوراما",
            "autoLigthSensor": "حساس اضاءه أتوماتيك",
            "frontConvFogLamp": "مصابيح ضباب أمامية",
            "ledPosLight": "اضاءة ارشادية LED",
            "dedicatedDrl": "اضاءة امامية خلال النهار LED",
            "rearLedCompLamp": "مصابيح خلفية LED",
            "premClothSeats": "فرش فاخر للمقاعد",
            "leatherSeatCover": "مقاعد مكسوة بالجلد",
            "leatherSeatCoverRed": "مقاعد مكسوة بالجلد *فرش أحمر",
            "leatherWrapedWheel": "مقود و ناقل حركة مكسو بالجلد",
            "manAdjustDriverSeat": "تعديل كرسي السائق فى 6 اتجاهات",
            "powAdjustDriverSeat": "تعديل كرسي السائق كهربائيا فى 10 اتجاهات",
            "passPowerSeat": "تحكم كهربائي في كرسي الراكب الامامي",
            "frontVent": "تبريد و تدفئة للمقاعد الامامية",
            "rearArmRest": "مسند يد خلفى مع حامل اكواب",
            "rearSpliteSeat": "امكانية طي المقاعد الخلفية 60 : 40",
            "rearAdjustableHeadRest": "مخادع رأس خلفية متحركة",
            "chromedInAndOutDoorHandler": "مقابض داخلية و خارجية بطلاء من الكروم",
            "doorBeltLine": "حزام كروم خارجي للابواب",
            "roofRack": "حامل سقف",
            "rearSpoiler": "سبويلر خلفى باضاءة مرتفعة للفرامل LED",
            "chromedFrontGrill": "شبكة كروم",
            "autoDefogSys": "إزالة الضباب اوتوماتيكيا عن الزجاج الامامي",
            "ledHeadLamps": "مصابيح LED امامية",
            "headLampWashers": "رشاشات فوانيس امامية",
            "powerTailGate": "فتح صندوق الامتعة كهربائياً",
            "autoRainSensor": "حساس مطر"
        }
});
