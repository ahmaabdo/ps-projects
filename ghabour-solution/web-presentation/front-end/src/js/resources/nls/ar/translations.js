define({
    "main": {
        "submit":"حفظ",
        "removeBtn": "مسح",
        "download": "تحميل",
        "appname": "Ghabbour",
        "switchLang": "English"
    },"dashboard": {
            "addVehicle": "السيارات",
            "addModel": "موديل السيارات",
            "faq": "الاسئلة الشائعة",
            "location": "الموقع",
            "title":"الصفحة الرئيسية"
        },
    "addVehicle": {
        "pageGuide":"دليل الصفحة",
        "title": "أضف سيارة ",
        "title2": "تصفح السيارات ",
        "title3":"أضف سيارة",
        "brand": "علامة تجارية ",
        "carModel": "نموذج ",
        "trim": "حالة ",
        "modelYear": "سنة الصنع ",
        "saveAndContinue": "حفظ ومتابعة",
        "image": "تحميل صور السيارات",
        "upload": "تحميل",
        "addMoreData": "اضافه المزيد",
        "view": "معاينة"
    }, "locations": {
            "city":"المدينة",
            "locationName": "اسم الموقع",
            "address": "عنوان",
            "cityAr":"المدينة باللغة العربية",
            "locationNameAr": "اسم الموقع باللغة العربية",
            "addressAr": "العنوان باللغة العربية",
            "latitude": "خط العرض",
            "longitude": "خط الطول",
            "brand": "علامة تجارية"
        },
        "faqs": {
            "type": "نوع",
            "lookupTypeEn": "نوع البحث الإنجليزية",
            "lookupTypeAr": "نوع البحث العربية",
            "quesAr": "سؤال باللغة العربية",
            "quesEn": "سؤال باللغة الإنجليزية",
            "answerAr": "الجواب باللغة العربية",
            "answerEn": "الإجابة باللغة الإنجليزية"
        }
}

);
