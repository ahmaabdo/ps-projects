define({
    "root": {
        "main": {
            "removeBtn": "Remove",
            "removeAllBtnLbl": "Remove All",
            "download": "Download All",
            "appname": "Ghabbour",
            "submit":"Submit",
            "switchLang": "العربية"
        },"dashboard": {
            "addVehicle": "Products",
            "addModel": "Models",
            "faq": "FAQ",
            "location": "Location",
            "title":"Dashboard"
        },
        "addVehicle": {
            "pageGuide":"Page Guide",
            "title": "Add/Edit Vehicle Images",
            "title2": "Browse Vehicles ",
            "title3":"Add/Edit Model Images",
            "brand": "Brand ",
            "carModel": "Model ",
            "trim": "Category",
            "modelYear": "Category",
            "saveAndContinue": "Save and Continue",
            "image": "Upload Car Images",
            "upload": "Upload",
            "view": "View",
            "addMoreData": "Add more vehicles",
            "back": "Back"
        }
        , "locations": {
            "city":"City",
            "locationName": "Location Name",
            "address": "Address",
            "cityAr":"City Arabic",
            "locationNameAr": "Location Name Arabic",
            "addressAr": "Address Arabic",
            "latitude": "Latitude",
            "longitude": "Longitude",
            "brand": "Brand"
        },
        "faqs": {
            "type": "Type",
            "lookupTypeEn": "Lookup Type English",
            "lookupTypeAr": "Lookup Type Arabic",
            "quesAr": "Question Arabic",
            "quesEn": "Question English",
            "answerAr": "Answer Arabic",
            "answerEn": "Answer English"
        }
    },
    "ar": true
});
