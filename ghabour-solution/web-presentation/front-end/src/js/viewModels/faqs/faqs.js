define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'ojs/ojpagingdataproviderview', 'ojs/ojarraydataprovider', 'ojs/ojbutton', 'ojs/ojinputtext'
            , 'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojtable', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource'
            , 'ojs/ojdialog', 'ojs/ojvalidationgroup'
], function (oj, ko,app, services, PagingDataProviderView, ArrayDataProvider) {
    function faqsContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
        self.label = ko.observable();
        self.groupValid = ko.observable();
        self.columns = ko.observable(2);
        self.quesAr = ko.observable();
        self.quesEn = ko.observable();
        self.answerAr = ko.observable();
        self.answerEn = ko.observable();
        self.type = ko.observable();
        self.id=ko.observable();
        self.lookupTypeAr = ko.observable();
        self.lookupTypeEn = ko.observable();
        self.columnArray = ko.observableArray();
        self.selectedRowKey=ko.observable();
        self.selectedRow=ko.observable();
        self.selectedIndex=ko.observable();
        self.tableArray = ko.observableArray([]);
        self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.tableArray, {idAttribute: 'id'}));
        self.submit = function () {
            app.loading(true);
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid") {
                app.loading(false);
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            var confirmation="confirmation";
            var msg="Faq added successfully!";
            var payload = {
                    "answerAr": self.answerAr(),
                    "answerEn": self.answerEn(),
                    "quesAr": self.quesAr(),
                    "lookupTypeAr": self.lookupTypeAr() ,
                    "lookupTypeEn": self.lookupTypeEn(),
                    "quesEn": self.quesEn(),
                    "type": self.type()
                };
            services.postGeneric("FAQService", JSON.stringify(payload)).then(data => {
                if (data) {
                    getFaq();
                } else {
                    confirmation = "error";
                    msg = "An unknown error has occurred, Please try again later";
                    app.failCbFn();
                }
            }, app.failCbFn);
            app.loading(false);
            app.createMessage(confirmation, msg);
        };
        self.clear = function () {
            self.quesAr('');
            self.quesEn('');
            self.answerAr('');
            self.answerEn('');
            self.type('');
            self.lookupTypeAr('');
            self.lookupTypeEn('');
            self.id('');
        };
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.selectedRow(self.tableArray()[currentRow['rowIndex']]);
            self.selectedRowKey(currentRow['rowKey']);
            self.selectedIndex(currentRow['rowIndex']);
            for (var i = 0; i < self.tableArray().length; i++) {
                if (self.selectedRow().id == self.tableArray()[i].id) {
                    self.quesAr(self.tableArray()[i].quesAr);
                    self.quesEn(self.tableArray()[i].quesEn);
                    self.answerAr(self.tableArray()[i].answerAr);
                    self.answerEn(self.tableArray()[i].answerEn);
                    self.type(self.tableArray()[i].type);
                    self.lookupTypeAr(self.tableArray()[i].lookupTypeAr);
                    self.lookupTypeEn(self.tableArray()[i].lookupTypeEn);
                    self.id(self.tableArray()[i].id);
                }
            }
            document.getElementById('modalDialog1').open();
        };
        self.close = function (event) {
            document.getElementById('modalDialog1').close();
        };
        self.edit = function () {
            app.loading(true);
            var confirmation = "confirmation";
            var msg = "Faq edited successfully!";
            var payload = {
                "id":self.id(),
                "answerAr": self.answerAr(),
                "answerEn": self.answerEn(),
                "quesAr": self.quesAr(),
                "lookupTypeAr": self.lookupTypeAr(),
                "lookupTypeEn": self.lookupTypeEn(),
                "quesEn": self.quesEn(),
                "type": self.type()
            };
            services.editGeneric("FAQService/edit", JSON.stringify(payload)).then(data => {
                document.getElementById('modalDialog1').close();
                if (data) {
                    self.clear();
                    getFaq();
                } else {
                    confirmation = "error";
                    msg = "An unknown error has occurred, Please try again later";
                    app.failCbFn();
                }
            }, app.failCbFn);
            app.loading(false);
            app.createMessage(confirmation, msg);
        };
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });
        function getFaq() {
            self.tableArray([]);
            services.getGeneric("FAQService").then(data => {
                app.loading(false);
                self.tableArray(data);
            }, app.failCbFn);
        };
        /******************************/
        self.connected = function () {
            app.loading(true);
            initTranslations();
            getFaq();
        };

        function initTranslations() {
            self.label({
                submit: ko.observable(getTranslation('main.submit')),
                quesAr: ko.observable(getTranslation('faqs.quesAr')),
                quesEn: ko.observable(getTranslation('faqs.quesEn')),
                answerAr: ko.observable(getTranslation('faqs.answerAr')),
                answerEn: ko.observable(getTranslation('faqs.answerEn')),
                type: ko.observable(getTranslation('faqs.type')),
                lookupTypeAr: ko.observable(getTranslation('faqs.lookupTypeAr')),
                lookupTypeEn: ko.observable(getTranslation('faqs.lookupTypeEn'))
            });
            self.columnArray([
                {
                    "headerText": '#', "field": "id"
                },
                {
                    "headerText": self.label().quesAr(), "field": "quesAr"
                },
                {
                    "headerText": self.label().quesEn(), "field": "quesEn"
                },
                {
                    "headerText": self.label().answerAr(), "field": "answerAr"
                },
                {
                    "headerText": self.label().answerEn(), "field": "answerEn"
                },
                {
                    "headerText": self.label().type(), "field": "type"
                },
                {
                    "headerText": self.label().lookupTypeAr(), "field": "lookupTypeAr"
                },
                {
                    "headerText": self.label().lookupTypeEn(), "field": "lookupTypeEn"
                }
            ]);
        }
    }

    return faqsContentViewModel;
});
