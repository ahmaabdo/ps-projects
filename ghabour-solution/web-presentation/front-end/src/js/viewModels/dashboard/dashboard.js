define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojformlayout'
], function (oj, ko, app) {
    function dashboardContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
        self.label = ko.observable();
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });
        self.connected = function () {
            initTranslations();
            app.loading(false);
        };
        self.switchLangAction = function () {
            app.switchLanguage();
        };
        self.addVehicle = function () {
            oj.Router.rootInstance.go('addVehicle');
        };
        self.faq = function () {
            oj.Router.rootInstance.go('faq');
        };
        self.location = function () {
            oj.Router.rootInstance.go('locations');
        };
        self.addModel = function () {
            oj.Router.rootInstance.go('addModelVehicle');
        };

        function initTranslations() {
            self.label({
                title: ko.observable(getTranslation('dashboard.title')),
                addVehicle: ko.observable(getTranslation('dashboard.addVehicle')),
                faq: ko.observable(getTranslation("dashboard.faq")),
                location: ko.observable(getTranslation("dashboard.location")),
                addModel: ko.observable(getTranslation("dashboard.addModel"))
            });
        }

        this.labelEdge = ko.pureComputed(function () {

        }, this);
    }

    return dashboardContentViewModel;
});
