define(['knockout', 'appController', 'config/services', 'ojs/ojarraydataprovider', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojlistview', 'ojs/ojlabel', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojformlayout', 'ojs/ojnavigationlist', 'ojs/ojselectcombobox',
    'ojs/ojconveyorbelt', 'ojs/ojfilmstrip', 'ojs/ojdialog', 'ojs/ojvalidationgroup'],
        function (ko, app, services, ArrayDataProvider, commonhelper) {

            function viewVehicleViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var oldSelectedYear;
                self.label = ko.observable();
                self.screenVisible = ko.observable(true);
                self.columns = ko.observable(3);
                self.groupValid = ko.observable();
                self.idProduct = ko.observable();
                self.images = ko.observableArray([]);
                self.show = ko.observableArray([]);
                self.oppNumber = ko.observable();
                self.koArray = ko.observableArray([]);
                self.productData = ko.observableArray();
                self.currentNavArrowPlacement = ko.observable("adjacent");
                self.currentNavArrowVisibility = ko.observable("auto");
                self.fieldsValue = {
                    carModel: ko.observable(''),
                    brand: ko.observable(),
                    modelYear: ko.observable(),
                    brandArr: ko.observableArray([]),
                    modelYearArr: ko.observableArray([]),
                    modelArr: ko.observableArray([])
                };
                self.clear = function () {
                    self.fieldsValue.carModel('');
                    self.fieldsValue.brand('');
                    self.fieldsValue.modelYear('');
                };
                self.dataProvider = new ArrayDataProvider(self.koArray);

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.saveBtnAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    if (self.oppNumber()) {
                        app.loading(true);
                        var payload = {
                            "oppNumber": self.oppNumber(),
                            "Brand_c": self.fieldsValue.brand(),
                            "Model_c": self.fieldsValue.carModel(),
                            "Category_c": self.fieldsValue.modelYear()
                        };
                        services.postGeneric("productsService/addOpp", JSON.stringify(payload)).then(data => {
                            console.log(data);
                            app.loading(false);
                            if (data.status == 'done') {
                                app.createMessage("confirmation", "Opportunity submitted successfully!");
                                self.screenVisible(false);
                            } else {
                                app.createMessage("error", "Couldn't add Opportunity!");
                                self.screenVisible(true);
                            }
                        }, app.failCbFn);
                    } else {
                        app.createMessage("error", "OOP Number is Needed!");
                    }
                };
                self.backAction = function () {
                    self.screenVisible(true);
                };

                self.imageClick = function (event) {
                    self.image(event.data);
                    document.getElementById('modalDialog1').open();
                };

                self.close = function () {
                    document.getElementById('modalDialog1').close();
                };

                self.brandSelectHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    if (selectedVal) {
                        self.fieldsValue.modelArr([]);
                        if (selectedVal && self.productData()) {
                            var model = self.productData().Models.filter(e => e.Brand_c == selectedVal);
                            for (var i in model)
                                self.fieldsValue.modelArr.push({id: i, value: model[i].Name_c, label: model[i].Name_c});
                        }
                    }
                };

                self.modelSelectHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    if (selectedVal) {
                        self.fieldsValue.modelYearArr([]);
                        if (selectedVal && self.productData()) {
                            var product = self.productData().Products.filter(e => e.Model_c == selectedVal);
                            for (var i in product)
                                self.fieldsValue.modelYearArr.push({id: i, value: product[i].CategoryName_c, label: product[i].CategoryName_c});
                        }
                    }

                };

                self.trimChange = function () {
                    self.koArray([]);
                    if (self.fieldsValue.brand() && self.fieldsValue.carModel() && self.fieldsValue.modelYear()) {
                        if (oldSelectedYear != self.fieldsValue.modelYear()) {
                            app.loading(true);
                            var payload = {
                                "brand": self.fieldsValue.brand(),
                                "model": self.fieldsValue.carModel(),
                                "modelYear": self.fieldsValue.modelYear()
                            };
                            services.postGeneric("productsService/getProduct", JSON.stringify(payload)).then(data => {
                                console.log(data);
                                if (data) {
                                    if (data.id == 0)
                                        app.loading(false);
                                    self.idProduct(data.id);
                                } else {
                                    app.failCbFn();
                                }
                            }, app.failCbFn);
                        }
                    }
                    oldSelectedYear = self.fieldsValue.modelYear();
                };

                function arrayBufferToBase64(buffer) {
                    return new Promise((resolve, reject) => {
                        const reader = new FileReader();
                        var blob = new Blob([buffer], {type: "image/png"});
                        reader.readAsDataURL(blob);
                        reader.onload = () => resolve(reader.result.substr(reader.result.indexOf(',') + 1));
                        reader.onerror = error => reject(error);
                    });
                }

                ko.computed(() => {
                    self.koArray([]);
                    if (self.idProduct()) {
                        services.getGeneric("attachment/" + self.idProduct()).then(data => {
                            if (data.length>0) {
                                (async () => {
                                    self.images([]);
                                    for (var i in data)
                                    {
                                        var filename = data[i].name;
                                        var type = (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
                                        if (type[0]) {
                                            var buf = new Uint8Array(data[i].image);
                                            await arrayBufferToBase64(buf).then(function (imageData) {
                                                var dataReplacement = imageData.replace("data/image/", "data:image/").replace("/base64/", ";base64,");
                                                self.images.push({
                                                    "name": data[i].name,
                                                    "data": dataReplacement,
                                                    "id": data[i].id
                                                });

                                            });
                                        }
                                    }
                                    self.koArray(self.images());
                                })();
                            }else{
                                self.koArray().data="../css/images/logo.png";
                                self.koArray().name="logo";
                                console.log(self.koArray());
                            }
                            app.loading(false);
                        }, err => {
                            app.failCbFn();
                        });
                    }
                });


                self.connected = function () {
                    initTranslations();
                    var urlParams = new URLSearchParams(window.location.search);
                    var myParam = urlParams.get('OOPNumber');
                    self.oppNumber(myParam);
                    self.screenVisible(true);
                    app.loading(true);
                    self.fieldsValue.brandArr([]);
                    services.getGeneric("productsService").then(data => {
                        app.loading(false);
                        if (data) {
                            app.loading(false);
                            self.productData(data);
                            for (var i in data.Brands) {
                                if (data.Brands[i].BrandName_c != "null")
                                    self.fieldsValue.brandArr.push({id: i, value: data.Brands[i].BrandName_c, label: data.Brands[i].BrandName_c});
                            }
                        }
                    }, app.failCbFn);
                };

                function initTranslations() {
                    self.label({
                        title: ko.observable(getTranslation('addVehicle.title2')),
                        brand: ko.observable(getTranslation('addVehicle.brand')),
                        carModel: ko.observable(getTranslation("addVehicle.carModel")),
                        modelYear: ko.observable(getTranslation("addVehicle.modelYear")),
                        trim: ko.observable(getTranslation("addVehicle.trim")),
                        saveAndContinue: ko.observable(getTranslation("addVehicle.saveAndContinue")),
                        addMoreData: ko.observable(getTranslation("addVehicle.addMoreData"))
                    });
                }

                this.labelEdge = ko.pureComputed(function () {

                }, this);
            }


            return new viewVehicleViewModel();
        }
);
