define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'ojs/ojpagingdataproviderview', 'ojs/ojarraydataprovider', 'ojs/ojbutton', 'ojs/ojinputtext'
            , 'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojtable', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource'
            , 'ojs/ojdialog', 'ojs/ojvalidationgroup'
], function (oj, ko, app, services, PagingDataProviderView, ArrayDataProvider) {
    function locationsContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
        self.label = ko.observable();
        self.groupValid = ko.observable();
        self.columns = ko.observable(2);
        self.locationName = ko.observable();
        self.city = ko.observable();
        self.address = ko.observable();
        self.locationNameAr = ko.observable();
        self.cityAr = ko.observable();
        self.addressAr = ko.observable();
        self.brand = ko.observable();
        self.latitude = ko.observable();
        self.longitude = ko.observable();
        self.id=ko.observable();
        self.selectedRowKey=ko.observable();
        self.selectedRow=ko.observable();
        self.selectedIndex=ko.observable();
        self.columnArray = ko.observableArray();
        self.tableArray = ko.observableArray([]);
        self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.tableArray, {idAttribute: 'DepartmentId'}));

        self.submit = function () {
            app.loading(true);
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid") {
                app.loading(false);
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            var confirmation="confirmation";
            var msg="Location added successfully!";
            var payload = {
                    "locationName": self.locationName(),
                    "city": self.city(),
                    "address": self.address(),
                    "locationNameAr": self.locationNameAr(),
                    "cityAr": self.cityAr(),
                    "addressAr": self.addressAr(),
                    "brand": self.brand() ,
                    "latitude": self.latitude(),
                    "longitude": self.longitude()
                };
            services.postGeneric("LocationService", JSON.stringify(payload)).then(data => {
                if (data) {
                    getLocation();
                } else {
                    confirmation = "error";
                    msg = "An unknown error has occurred, Please try again later";
                    app.failCbFn();
                }
            }, app.failCbFn);
            app.loading(false);
            app.createMessage(confirmation, msg);
        };
        self.clear=function(){
            self.locationName('');
            self.city('');
            self.address('');
            self.locationNameAr('');
            self.cityAr('');
            self.addressAr('');
            self.brand('');
            self.latitude('');
            self.longitude('');
            self.id('');
        };
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.selectedRow(self.tableArray()[currentRow['rowIndex']]);
            self.selectedRowKey(currentRow['rowKey']);
            self.selectedIndex(currentRow['rowIndex']);
            for (var i = 0; i < self.tableArray().length; i++) {
                if (self.selectedRow().id == self.tableArray()[i].id) {
                    self.locationName(self.tableArray()[i].locationName);
                    self.city(self.tableArray()[i].city);
                    self.address(self.tableArray()[i].address);
                    self.locationNameAr(self.tableArray()[i].locationNameAr);
                    self.cityAr(self.tableArray()[i].cityAr);
                    self.addressAr(self.tableArray()[i].addressAr);
                    self.brand(self.tableArray()[i].brand);
                    self.latitude(self.tableArray()[i].latitude);
                    self.longitude(self.tableArray()[i].longitude);
                    self.id(self.tableArray()[i].id);
                }
            }
            document.getElementById('modalDialog1').open();
        };
        self.close = function (event) {
            document.getElementById('modalDialog1').close();
        };
        self.edit = function () {
            app.loading(true);
            var confirmation = "confirmation";
            var msg = "Faq edited successfully!";
            var payload = {
                "id": self.id(),
                "locationName": self.locationName(),
                "city": self.city(),
                "address": self.address(),
                "locationNameAr": self.locationNameAr(),
                "cityAr": self.cityAr(),
                "addressAr": self.addressAr(),
                "brand": self.brand(),
                "latitude": self.latitude(),
                "longitude": self.longitude()
            };
            services.editGeneric("LocationService/edit", JSON.stringify(payload)).then(data => {
                document.getElementById('modalDialog1').close();
                if (data) {
                    self.clear();
                    getLocation();
                } else {
                    confirmation = "error";
                    msg = "An unknown error has occurred, Please try again later";
                    app.failCbFn();
                }
            }, app.failCbFn);
            app.loading(false);
            app.createMessage(confirmation, msg);
        };
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });
        
        function getLocation() {
            self.tableArray([]);
            services.getGeneric("LocationService").then(data => {
                app.loading(false);
                self.tableArray(data);
            }, app.failCbFn);
        };

        /******************************/
        self.connected = function () {
            app.loading(true);
            initTranslations();
            getLocation();
        };

        function initTranslations() {
            self.label({
                submit: ko.observable(getTranslation('main.submit')),
                locationName: ko.observable(getTranslation('locations.locationName')),
                city: ko.observable(getTranslation('locations.city')),
                address: ko.observable(getTranslation('locations.address')),
                locationNameAr: ko.observable(getTranslation('locations.locationNameAr')),
                cityAr: ko.observable(getTranslation('locations.cityAr')),
                addressAr: ko.observable(getTranslation('locations.addressAr')),
                brand: ko.observable(getTranslation('locations.brand')),
                latitude: ko.observable(getTranslation('locations.latitude')),
                longitude: ko.observable(getTranslation('locations.longitude'))
            });
            self.columnArray([
                {
                    "headerText": '#', "field": "id"
                },
                {
                    "headerText": self.label().locationName(), "field": "locationName"
                },
                {
                    "headerText": self.label().locationNameAr(), "field": "locationNameAr"
                },
                {
                    "headerText": self.label().city(), "field": "city"
                },
                {
                    "headerText": self.label().cityAr(), "field": "cityAr"
                },
                {
                    "headerText": self.label().address(), "field": "address"
                },
                {
                    "headerText": self.label().addressAr(), "field": "addressAr"
                },
                {
                    "headerText": self.label().brand(), "field": "brand"
                },
                {
                    "headerText": self.label().latitude(), "field": "latitude"
                },
                {
                    "headerText": self.label().longitude(), "field": "longitude"
                }
            ]);
        }
    }

    return locationsContentViewModel;
});
