define(['knockout', 'appController', 'config/services', 'ojs/ojarraydataprovider', 'ojs/ojknockout',
    'ojs/ojfilepicker', 'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojselectcombobox', 'ojs/ojinputtext',
    'ojs/ojformlayout', 'ojs/ojdialog'
], function (ko, app, services) {
    function addModelVehicleContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
                var dataFiles = {};
                var oldSelectedBrand;
                self.label = ko.observable();
                self.columns = ko.observable(3);
                self.image = ko.observable();
                self.show = ko.observable();
                self.idProduct = ko.observable();
                self.isDisabled=ko.observable();
                self.koArray = ko.observableArray([]);
                self.removedArray = ko.observableArray([]);
                self.attachmentId = 0;
                self.productData = ko.observableArray();
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.selectedItemsAttachment = ko.observableArray([]);
                self.fieldsValue = {
                    carModel: ko.observable(''),
                    brand: ko.observable(),
                    brandArr: ko.observableArray([]),
                    modelArr: ko.observableArray([])
                };
                self.clear = function () {
                    self.fieldsValue.carModel('');
                    self.fieldsValue.brand('');
                    self.koArray([]);
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                
                self.removeSelectedAttachment = function () {
                    self.isDisabled(false);
                    if (self.selectedItemsAttachment().length < 1)
                        app.createMessage("error", "Please select an item");
                    self.removedArray.push({id: self.selectedItemsAttachment()[0]});
                    $.each(self.selectedItemsAttachment(), function (index, value) {
                        self.koArray.remove(function (item)
                        {
                            return (item.id == value);
                        });
                    });
                };

                function downloadURI(uri, name) {
                    var link = document.createElement("a");
                    link.setAttribute('href', uri);
                    link.setAttribute('download', name);
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                self.downloadAttachmentViewer = function () {
                    //Option to download all attachments once the method fired
                    for (var i = 0; i < self.koArray().length; i++) {
                        downloadURI(self.koArray()[i].data, self.koArray()[i].name);
                    }
                };

                self.attachmentViewer = function () {
                    if (self.selectedItemsAttachment().length < 1)
                        app.createMessage("error", "Please select an item");
                    $.each(self.selectedItemsAttachment(), function (index, value) {
                        for (var j in self.koArray()) {
                            if (self.koArray()[j].id == self.selectedItemsAttachment()[0]) {
                                self.show(self.koArray()[j].data);
                                document.getElementById('modalDialog1').open();
                            }
                        }
                    });
                };
                self.close = function () {
                    document.getElementById('modalDialog1').close();
                };

                self.selectListener = async function (event) {
                    var files = event.detail.files;
                    if(self.koArray().length==0){
                        self.isDisabled(true);
                    for (var i = 0; i < files.length; i++) {
                        if (files.length > 0) {
                            if (files[i].size / 1024 / 1024 <= 2.5 && files[i].type.includes('image/png')) {
                                function getBase64(file) {
                                    return new Promise((resolve, reject) => {
                                        const reader = new FileReader();
                                        reader.readAsDataURL(file);
                                        reader.onload = () => resolve(reader.result);
                                        reader.onerror = error => reject(error);
                                    });
                                }
                                dataFiles.name = files[i].name;
                                dataFiles.id = i + 1;
                                await getBase64(files[i]).then(function (data) {
                                    dataFiles.data = (data);
                                    self.attachmentId = self.attachmentId - 1;
                                    dataFiles.id = (self.attachmentId);
                                    self.koArray.push({
                                        "name": self.fieldsValue.carModel()+".png",
                                        "data": dataFiles.data,
                                        "id": dataFiles.id
                                    });
                                }
                                );
                            } else if (!(files[i].size / 1024 / 1024 <= 2.5) && files[i].type.includes('image/png')) {
                                app.createMessage("error", "Cannot upload files exceeding 1MB in size");
                            } else if (!(files[i].type.includes('image/png'))) {
                                app.createMessage("error", "Uploaded file is not a valid image. Only PNG is allowed.");
                            } else {
                                app.createMessage("error", "Can only upload png images and not exceeding 1MB in size");
                            }
                        }
                    }
                }else{
                    self.isDisabled(false);
                    app.createMessage("error", "Cannot upload more than one image");
                }
                };
                self.addImage = async function (id) {
                    var confirmation = "confirmation";
                    var msg = "Model images added successfully!";
                    app.loading(true);
                    for (var i in self.koArray()) {
                        var payloadAttach = [{
                                "name": self.koArray()[i].name,
                                "ssId": id,
                                "image": self.koArray()[i].data,
                                "id": self.koArray()[i].id
                            }];
                        await services.postGeneric("attachmentModel/addImg", JSON.stringify(payloadAttach)).then(dataAtt => {
                            if (dataAtt) {
                                confirmation = "confirmation";
                                msg = "Model images added successfully!";
                            } else {
                                confirmation = "error";
                                msg = "An unknown error has occurred, Please try again later";
                                app.failCbFn();
                            }
                        }, app.failCbFn);
                    }
                    app.loading(false);
                    app.createMessage(confirmation, msg);
                };

                self.saveAndContinue = async function () {
                    self.isDisabled(false);
                    if (self.removedArray().length == 0&& self.idProduct() == 0 || self.idProduct() == 0) {
                        app.loading(true);
                        var payload = {
                            "brand": self.fieldsValue.brand(),
                            "model": self.fieldsValue.carModel()
                        };
                        await services.postGeneric("model", JSON.stringify(payload)).then(data => {
                            if (data) {
                                app.loading(false);
                                if (self.koArray()) {
                                    self.addImage(data.id);
                                }
                            } else {
                                app.failCbFn();
                            }
                        }, app.failCbFn);
                    } else {
                        app.loading(true);
                        for (var j in self.removedArray()) {
                            var id = self.removedArray()[j].id;
                            var payload = [{
                                    "id": id
                                }];
                            await services.postGeneric("attachmentModel/delete", JSON.stringify(payload)).then(data => {
                                app.loading(false);

                            }, app.failCbFn);
                        }
                        self.addImage(self.idProduct());
                    }
                };

                self.brandSelectHandler = function (event) {
                    self.isDisabled(false);
                    var selectedVal = event['detail'].value;
                    if (selectedVal) {
                        self.fieldsValue.modelArr([]);
                        if (selectedVal && self.productData()) {
                            var model = self.productData().Models.filter(e => e.Brand_c == selectedVal);
                            for (var i in model)
                                self.fieldsValue.modelArr.push({id: i, value: model[i].Name_c, label: model[i].Name_c});
                        }
                    }
                };

                self.modelSelectHandler = function (event) {
                    self.trimChange();
                };

        self.trimChange = function () {
            self.koArray([]);
            if (self.fieldsValue.brand() && self.fieldsValue.carModel()) {
                if (oldSelectedBrand != self.fieldsValue.carModel()) {
                    app.loading(true);
                    services.getGeneric("attachmentModel").then(data => {
                        self.isDisabled(true);
                        if (data.length>0) {
                            for (var i in data) {
                                if (data[i].model == self.fieldsValue.carModel() && data[i].brand == self.fieldsValue.brand()) {
                                    self.idProduct(data[i].model);
                                }else if(data[i].model !== self.fieldsValue.carModel() && data[i].brand == self.fieldsValue.brand()){
                                    self.idProduct(self.fieldsValue.carModel());
                                }
                            }
                        }
                    }, app.failCbFn);
                }
            }
            oldSelectedBrand = self.fieldsValue.carModel();
        };

                function arrayBufferToBase64(buffer) {
                    return new Promise((resolve, reject) => {
                        const reader = new FileReader();
                        var blob = new Blob([buffer], {type: "image/png"});
                        reader.readAsDataURL(blob);
                        reader.onload = () => resolve(reader.result.substr(reader.result.indexOf(',') + 1));
                        reader.onerror = error => reject(error);
                    });
                }

                ko.computed(() => {
                    self.koArray([]);
                    if (self.idProduct()) {
                        services.getGeneric("attachmentModel/" + self.idProduct()).then(data => {
                            if (data.length>0) {
                                app.loading(false);
                                self.isDisabled(true);
                                (async () => {
                                    for (var i in data) {
                                        var filename = data[i].name;
                                        var type = (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
                                        if (type[0]) {
                                            var buf = new Uint8Array(data[i].attachment);
                                            await arrayBufferToBase64(buf).then(function (imageData) {
                                                var dataReplacement = imageData.replace("data/image/", "data:image/").replace("/base64/", ";base64,");
                                                self.koArray.push({
                                                    "name": data[i].name,
                                                    "data": dataReplacement,
                                                    "id": data[i].id
                                                });
                                            });
                                        } else {
                                            app.createMessage("error", "Found corrupt file: " + data[i].name + "; Automatically removing it in progress...");
                                            app.loading(true);
                                            var payload = [{
                                                    "id": data[i].id
                                                }];
                                            services.postGeneric("attachmentModel/delete", JSON.stringify(payload)).then(data => {
                                                app.loading(false);
                                            }, err => {
                                                app.failCbFn();
                                                app.createMessage("error", "Automatically removing corrupt file name: " + data[i].name + " failed. Please remove it manually.");
                                            });
                                        }
                                    }
                                })();
                            }else{
                                app.loading(false);
                                self.isDisabled(false);
                            }
                            app.loading(false);
                        }, app.failCbFn);
                    }
                });

                self.connected = function () {
                    initTranslations();
                    self.fieldsValue.brandArr([]);
                    services.getGeneric("productsService").then(data => {
                        app.loading(false);
                        if (data) {
                            self.productData(data);
                            for (var i in data.Brands) {
                                if (data.Brands[i].BrandName_c != "null")
                                    self.fieldsValue.brandArr.push({id: i, value: data.Brands[i].BrandName_c, label: data.Brands[i].BrandName_c});
                            }
                        }
                    }, app.failCbFn);
                };
                self.switchLangAction = function () {
                    app.switchLanguage();
                };
                self.pageGuide=function(){
                    oj.Router.rootInstance.go('modelGuide');
                };

                function initTranslations() {
                    self.label({
                        pageGuide:ko.observable(getTranslation('addVehicle.pageGuide')),
                        title: ko.observable(getTranslation('addVehicle.title3')),
                        brand: ko.observable(getTranslation('addVehicle.brand')),
                        carModel: ko.observable(getTranslation("addVehicle.carModel")),
                        modelYear: ko.observable(getTranslation("addVehicle.modelYear")),
                        trim: ko.observable(getTranslation("addVehicle.trim")),
                        image: ko.observable(getTranslation("addVehicle.image")),
                        upload: ko.observable(getTranslation("addVehicle.upload")),
                        view: ko.observable(getTranslation("addVehicle.view")),
                        browse: ko.observable(getTranslation("addVehicle.title2")),
                        saveAndContinue: ko.observable(getTranslation("addVehicle.saveAndContinue")),
                        removeBtnLbl: ko.observable(getTranslation("main.removeBtn")),
                        removeAllBtnLbl: ko.observable(getTranslation("main.removeAllBtnLbl")),
                        downloadLbl: ko.observable(getTranslation("main.download"))
                    });
                }

                this.labelEdge = ko.pureComputed(function () {

                }, this);
    }
    
    return addModelVehicleContentViewModel;
});
