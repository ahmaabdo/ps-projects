define(['knockout', 'ojs/ojmodule-element-utils', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils',
    'ojs/ojrouter', 'ojs/ojarraydataprovider', 'config/services', 'ojs/ojvalidation-base', 'ojs/ojmodule-element',
    'ojs/ojknockout', 'ojs/ojmessages', 'ojs/ojavatar', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojnavigationlist'],
        function (ko, moduleUtils, ResponsiveUtils, ResponsiveKnockoutUtils, Router, ArrayDataProvider, services, ojvalbase) {
            function ControllerViewModel() {
                var self = this;
                const getTranslation = oj.Translations.getTranslatedString;
                var selectedLanguage = localStorage.getItem("selectedLanguage");
                self.refreshViewForLanguage = ko.observable(false);
                self.label = ko.observable();
                self.messages = ko.observableArray("");
                self.messagesDataprovider = new ArrayDataProvider(self.messages);
                self.navListData = ko.observable();
                self.navListDataArray = ko.observableArray();
                self.navListDataArray.push({name: '', id: 'setup',
                    iconClass: 'oj-navigationlist-item-icon fa fa-cog'});
                self.navListDataArray.push({name: '', id: 'notificationScreen',
                    iconClass: 'oj-navigationlist-item-icon fa fa-bell'});
                self.navListData(new ArrayDataProvider(self.navListDataArray(), {keyAttributes: 'id'}));


                self.setLocale = function (lang) {
                    oj.Config.setLocale(lang,
                            function () {
                                self.refreshViewForLanguage(!self.refreshViewForLanguage());
                                $("html").attr("lang", lang);
                                if (lang == 'ar') {
                                    $("html").attr("dir", "rtl");
                                } else {
                                    $("html").attr("dir", "ltr");
                                }
                                initTranslations();
                            }
                    );
                };

                self.getLocale = function () {
                    return oj.Config.getLocale();
                };
                self.loading = function (load) {
                    if (load) {
                        $('#loading').show();
                    } else {
                        $('#loading').hide();
                    }
                };

                self.switchLanguage = function () {
                    if (self.getLocale() == "ar") {
                        self.setLocale("en-US");
                        localStorage.setItem("selectedLanguage", "en-US");
                    } else {
                        self.setLocale("ar");
                        localStorage.setItem("selectedLanguage", "ar");
                    }
                };

                self.createMessage = function (severity, summary) {
                    self.messages([]);
                    self.messages.push({
                        severity: severity,
                        summary: summary,
                        autoTimeout: 12000
                    });
                };

                if (selectedLanguage) {
                    self.setLocale(selectedLanguage);
                } else {
                    localStorage.setItem("selectedLanguage", "en-US");
                    self.setLocale(localStorage.getItem("selectedLanguage"));
                }

                self.failCbFn = function () {
                    self.loading(false);
                    console.log('error');
                    self.createMessage("error", "An unknown error has occurred, Please try again later");
                };


                // Media queries for repsonsive layouts
                var smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                self.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);

                // Router setup
                self.router = Router.rootInstance;
                self.router.configure({
                    'dashboard': {label: 'Dashboard', value: "dashboard/dashboard", title: "Dashboard", isDefault: true},
                    'addModelVehicle': {label: 'Add/Edit Model Images', value: "addModelVehicle/addModelVehicle", title: "Add/Edit Model Images"},
                    'faq': {label: 'Faqs', value: "faqs/faqs", title: "Faqs"},
                    'locations': {label: 'Locations', value: "locations/locations", title: "Locations"},
                    'addVehicle': {label: 'Add/Edit Vehicle Images', value: "main/addVehicle", title: "Add/Edit Vehicle Images"},
                    'viewVehicle': {label: 'Browse Vehicles', value: "main/viewVehicle", title: "Browse Vehicles"},
                    'productGuide': {label: 'Product Guide', value: "main/productGuide", title: "Product Guide"},
                    'modelGuide': {label: 'Model Guide', value: "addModelVehicle/modelGuide", title: "Model Guide"}
                });
                Router.defaults['urlAdapter'] = new Router.urlParamAdapter();

                //refresh view when language change
                self.refreshData = ko.computed(function () {
                    if (self.refreshViewForLanguage()) {
                        self.setLocale(localStorage.getItem("selectedLanguage"));
                    }
                });

                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});
                self.loadModule = function () {
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath})
                        ]);
                        masterPromise.then(
                                function (values) {
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                }
                        );
                    });
                };

                function initTranslations() {
                    self.label = {};
                }
                initTranslations();

            }

            return new ControllerViewModel();
        }
);
