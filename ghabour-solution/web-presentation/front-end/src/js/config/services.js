define(['config/serviceconfig', 'util/commonhelper'], function (serviceConfig, commonHelper) {

    function services() {

        var self = this;
        var restPath = commonHelper.getInternalRest();
        var headers = {
             Authorization: "QURNSU46QURNSU4="
        };
        self.getGeneric = function (serviceName) {
            var serviceURL = restPath + serviceName;

            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.postGeneric = function (serviceName, payload) {
            return serviceConfig.callPostService(restPath + serviceName, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.editGeneric = function (serviceName, payload) {
            return serviceConfig.callPutService(restPath + serviceName, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
    }

    return new services();
});