package common.restHelper;


import com.appspro.db.CommonConfigReader;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.security.cert.CertificateException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.util.Scanner;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.joda.time.Chronology;
import org.joda.time.LocalDate;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;


public class RestHelper {
    
    private static RestHelper instance;

    public static RestHelper getInstance() {
        if (instance == null)
            instance = new RestHelper();
        return instance;
    }
    
    private String InstanceUrl =
        CommonConfigReader.getValue("InstanceUrl"); //;"https://ccmf-test.fa.em3.oraclecloud.com:443";
    private String orgnizationUrl =
        "//hcmCoreSetupApi/resources/latest/organizations";
    private String biReportUrl =
        CommonConfigReader.getValue("biReportUrl"); //"https://ejcp-test.fa.em2.oraclecloud.com/xmlpserver/services/PublicReportService";
    private String SecurityService =
        CommonConfigReader.getValue("SecurityService");
    private String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private String instanceName =
        CommonConfigReader.getValue("instanceName"); //"ejcp-test.fa.em2.oraclecloud.com";
    private String crmRestUrl = "/crmRestApi/resources/latest/";
    private String crmRestOppUrl = "/crmRestApi/resources/11.13.18.05/opportunities/";
    private String crmRestOppUrl2 = "/child/OPPVechicelsCollection_c";
    private String brandURL = "Brand_c";
    private String carModelURL = "CarModel_c";
    private String productCategoryURL = "ProductCategory_c";
    public static String USER_NAME =
        CommonConfigReader.getValue("USER_NAME"); //"mahmoud.essam@appspro-me.com";
    public static String PASSWORD = CommonConfigReader.getValue("PASSWORD");
    public static String Schema_Name =
        CommonConfigReader.getValue("Schema_Name");
    public final static String SAAS_URL =
        "https://" + CommonConfigReader.getValue("instanceName");
    public final static String STATIC_INSTANCE_NAME =
        "ejfz-test.fa.em2.oraclecloud.com";

    private static String RIGHT_NOW_URL = CommonConfigReader.getValue("rightNowUrl");
    private static String RIGHT_NOW_USER = CommonConfigReader.getValue("rightNowUserName");
    private static String RIGHT_NOW_PW = CommonConfigReader.getValue("rightNowPassword");
    private static String RIGHT_NOW_INCIDENTS = "/incidents/";
    private static String RIGHT_NOW_CONTACTS = "/contacts/";


    public static String getSchema_Name() {
        return Schema_Name;
    }

    public static String getRightNowAuth() {
        byte[] message = (RIGHT_NOW_USER + ":" + RIGHT_NOW_PW).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }


    public void setCrmRestUrl(String crmRestUrl) {
        this.crmRestUrl = crmRestUrl;
    }

    public String getCrmRestUrl() {
        return crmRestUrl;
    }
    
    public String getInstanceName() {
        return instanceName;
    }

    public String getEmployeeUrl() {
        return InstanceUrl + employeeServiceUrl;
    }

    public String getEmployeeRestEndPointUrl() {
        return employeeServiceUrl;
    }

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getOrgnizationUrl() {
        return orgnizationUrl;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }
    
    public static String getRIGHT_NOW_URL() {
        return RIGHT_NOW_URL;
    }

    public static String getRIGHT_NOW_USER() {
        return RIGHT_NOW_USER;
    }

    public static String getRIGHT_NOW_PW() {
        return RIGHT_NOW_PW;
    }
    
    public static String getRIGHT_NOW_INCIDENTS() {
        return RIGHT_NOW_INCIDENTS;
    }

    public static String getRIGHT_NOW_CONTACTS() {
        return RIGHT_NOW_CONTACTS;
    }

    public void setBrandURL(String brandURL) {
        this.brandURL = brandURL;
    }

    public String getBrandURL() {
        return brandURL;
    }

    public void setCarModelURL(String carModelURL) {
        this.carModelURL = carModelURL;
    }

    public String getCarModelURL() {
        return carModelURL;
    }

    public void setProductCategoryURL(String productCategoryURL) {
        this.productCategoryURL = productCategoryURL;
    }

    public String getProductCategoryURL() {
        return productCategoryURL;
    }
    
    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }


    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    
    public String callSaaS(String serverUrl,String restFrameWorkVersion){
        String newurl=serverUrl.replaceAll(" ","%20");
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        try {
            URL url
                    = new URL(null, newurl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction
                    = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
          //  connection.setRequestProperty("REST-Framework-Version", "3");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());
            connection.setRequestProperty("REST-Framework-Version", "2");
            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
          return response.toString() ;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

  

    public Document httpPost(String destUrl,
                             String postData) throws Exception {
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + http.getResponseCode() +
                           "; connection response: " +
                           http.getResponseMessage());

        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);


            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();


            return doc;
        } else {
            System.out.println("Failed");
        }


        return null;
    }

    public static String getAuth() {
        byte[] message = (USER_NAME + ":" + PASSWORD).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

    public void setSecurityService(String SecurityService) {
        this.SecurityService = SecurityService;
    }

    public String getSecurityService() {
        return SecurityService;
    }
    public String getEmailBody(String reqType, String approvalName, String reqName ,String BeneficiaryName ){
        String body = "";
        if(reqType.equals("FYI")){
            body = "<h4> Dear "+ approvalName+",</h4>" + " <p>Kindly review for your information the below details: </p>" +
                "<p>Request Name: " + reqName + ",</p><p>  Beneficiary: " + BeneficiaryName +"</p>";
                
        }else if(reqType.equals("FYA")){
            body = "<h4> Dear "+ approvalName+", </h4>" + 
                   "<p>Your Action required for the  following:-  </p>" +
                "<p>Request Name :  " + reqName + "</p> <p> Beneficiary: " + BeneficiaryName +
                " ,</p> <p>Please connect to <a href=\\\"https://HCM.HHA.com.sa/\\\">https://HCM.HHA.com.sa </a></p>";
        }
        return body ; 
    }
    public String getEmailSubject(String reqType ,String reqName , String BeneficiaryName ){
        String subject = "";
        if(reqType.equals("FYI")){
            subject = "FYI  Approval of  "+  reqName +"for  " + BeneficiaryName ;
        }else if(reqType.equals("FYA")){
            subject = "Action Required   Approval of  "+  reqName +"for  " + BeneficiaryName ;
        }          
        return subject ; 
    }
    
    public String httpPostHCMDataLoad(String destUrl,
                             String postData) throws Exception {
        System.out.println(postData);
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + http.getResponseCode() +
                           "; connection response: " +
                           http.getResponseMessage());

        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<result>"), response.indexOf("</result>") +
                                           2);
            
            System.out.println(response);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);
            NodeList customAction = doc.getElementsByTagName("result");
            response = customAction.item(0).getChildNodes().item(0).getTextContent();
            System.out.println(response);
            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();


            return response;
        } else {
            System.out.println("Failed");
        }


        return null;
    }
    
    public String httpPostHCMDataLoadStatus(String destUrl,
                             String postData) throws Exception {
        System.out.println(postData);
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + http.getResponseCode() +
                           "; connection response: " +
                           http.getResponseMessage());

        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);


            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();


            return response;
        } else {
            System.out.println("Failed");
        }


        return null;
    }
    
    
    public JSONObject callSaaSRest(String serverUrl, String method, String body) {
        JSONObject res = new JSONObject();
        System.setProperty("DUseSunHttpHandler", "true");
        try {

            HttpsURLConnection https = null;
            HttpURLConnection connection = null;

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);


            if (method.equalsIgnoreCase("PATCH")) {
                connection.setRequestProperty("X-HTTP-Method-Override",
                                              "PATCH");
                connection.setRequestMethod("POST");
            } else {
                connection.setRequestMethod(method);
            }

            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("SOAPAction", "");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());
            
            if (!body.equalsIgnoreCase("null")) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int code = connection.getResponseCode();

            InputStream is;
            if (code >= 400) {
                is = connection.getErrorStream();
                res.put("status", "error");
            } else {
                res.put("status", "done");
                is = connection.getInputStream();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                res.put("data", response.toString());
            }

            if (code >= 400)
                System.out.println(",Error:" + res.get("data").toString());
            else
                System.out.println();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", "Internal server error");
        }

        return res;
    }


    public JSONObject callRest(String serverUrl, String method, String body) {
        System.out.println(serverUrl);
        JSONObject res = new JSONObject();
        System.setProperty("DUseSunHttpHandler", "true");
        try {

            HttpsURLConnection https = null;
            HttpURLConnection connection = null;

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);


            if (method.equalsIgnoreCase("PATCH")) {
                connection.setRequestProperty("X-HTTP-Method-Override",
                                              "PATCH");
                connection.setRequestMethod("POST");
            } else {
                connection.setRequestMethod(method);
            }

            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("SOAPAction", "");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Authorization",
                                          "Basic " + getRightNowAuth());
            
            if (!body.equalsIgnoreCase("null")) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int code = connection.getResponseCode();

            InputStream is;
            if (code >= 400) {
                is = connection.getErrorStream();
                res.put("status", "error");
            } else {
                res.put("status", "done");
                is = connection.getInputStream();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                res.put("data", response.toString());
            }

            if (code >= 400)
                System.out.println(",Error:" + res.get("data").toString());
            else
                System.out.println();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", "Internal server error");
        }

        return res;
    }

    public void setCrmRestOppUrl(String crmRestOppUrl) {
        this.crmRestOppUrl = crmRestOppUrl;
    }

    public String getCrmRestOppUrl() {
        return crmRestOppUrl;
    }

    public void setCrmRestOppUrl2(String crmRestOppUrl2) {
        this.crmRestOppUrl2 = crmRestOppUrl2;
    }

    public String getCrmRestOppUrl2() {
        return crmRestOppUrl2;
    }
}
