package common.restHelper;



import java.io.IOException;
import java.io.StringReader;

import java.util.Map;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.BindingProvider;

import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceFeature;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import weblogic.wsee.jws.jaxws.owsm.SecurityPoliciesFeature;

public class HCMDataLoaderHelper {
    
    private String soapRequest = null;
    private String url = null;
    RestHelper restHelper = new RestHelper();
    
    public String getImportAndLoadDataPayload(String contentId, String parameters){
        
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/common/dataLoader/core/dataLoaderIntegrationService/types/\">\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:importAndLoadData>\n" + 
        "         <typ:ContentId>"+contentId+"</typ:ContentId>\n" + 
        "         <typ:Parameters>"+parameters+"</typ:Parameters>\n" + 
        "      </typ:importAndLoadData>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>";
        
        return soapRequest;
    }
    
    public String getDataSetStatusPayload(String parameters){
        
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/common/dataLoader/core/dataLoaderIntegrationService/types/\">\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:getDataSetStatus>\n" + 
        "         <typ:Parameters>"+parameters+"</typ:Parameters>\n" + 
        "      </typ:getDataSetStatus>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>";
        
        return soapRequest;
    }
    
    public String importAndLoadData(String contentId, String parameters){
        this.url = restHelper.getInstanceUrl()+"/hcmService/HCMDataLoader";
        String response = "";
        try {
            response = restHelper.httpPostHCMDataLoad(url, getImportAndLoadDataPayload(contentId, parameters));
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    
    public String getDataSetStatus(String parameters){
        this.url = restHelper.getInstanceUrl()+"/hcmService/HCMDataLoader";
        String response = "";
        try {
            response = restHelper.httpPostHCMDataLoadStatus(url, getDataSetStatusPayload(parameters));
            System.out.println("response Status = " + response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    
    public String loadDataService(String id, String path, String hdlId) {
        String status = "Internal Error";

        // Configure security feature
        //SecurityPoliciesFeature securityFeatures = new SecurityPoliciesFeature(new String[] {"oracle/wss11_username_token_with_message_protection_client_policy" });
        SecurityPoliciesFeature securityFeatures =
            new SecurityPoliciesFeature(new String[] { "oracle/wss_username_token_over_ssl_client_policy" });
        String uuid = "uuid:" + UUID.randomUUID();

        String contentId = id;
        String parameters = "";
        String response = "Failed to invoke service";
        
            System.out.println("Entered try block and going to invoke service....");
            response = importAndLoadData(contentId, parameters);
            System.out.println("The response received from the server is ...");
            System.out.println(response);
            //            HDLHelper.updateHDLFile(response, hdlId,
            //                                    "Calling Load Data Service",id);
            //get Status of load submitted...
            Long processId = Long.valueOf(response);
            String loadStatus = getStatus(processId, hdlId);
            System.out.println("File load status: " + loadStatus);
            status = response;
        return status;
    }
    
    private String getStatus(Long id,
                             String hdlId) {
        String status = "Errored out";
        boolean loadstatus = false;
        try {
            loadstatus = invokeGetDataSetStatus(id, hdlId);
            status = "" + loadstatus;
        } catch (IOException e) {
        } catch (SAXException e) {
        } catch (ParserConfigurationException e) {
        }
        System.out.println("\n\n Load Completed for Data Set with UCM Content Id " +
                           id + " with status " + loadstatus);
        return status;
    }

    private boolean invokeGetDataSetStatus(Long processId,
                                           String hdlId) throws ParserConfigurationException,
                                                                SAXException,
                                                                IOException {
        boolean resp = true;
        boolean firstTimeInprogress = true;
        try {


            String response = getDataSetStatus("ProcessId=" + processId);
            DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document =
                builder.parse(new InputSource(new StringReader(response)));
            NodeList flowList =
                document.getElementsByTagName("DATA_SET_STATUS");
            NodeList childList;

            for (int i = 0; i < flowList.getLength(); i++) {
                childList = flowList.item(i).getChildNodes();
                for (int j = 0; j < childList.getLength(); j++) {
                    Node childNode = childList.item(j);
                    if ("DATA_SET".equals(childNode.getNodeName())) {
                        NodeList dataSeteChildNodes =
                            childNode.getChildNodes();
                        {
                            for (int k = 0; k < dataSeteChildNodes.getLength();
                                 k++) {
                                {
                                    Node childDSNode =
                                        dataSeteChildNodes.item(k);
                                    if ("STATUS".equals(childDSNode.getNodeName())) {
                                        String status =
                                            dataSeteChildNodes.item(k).getTextContent().trim();
                                        System.out.println("Current Status of the Data Set " +
                                                           dataSeteChildNodes.item(k).getTextContent().trim());
                                        if ("NOT_STARTED".equals(dataSeteChildNodes.item(k).getTextContent().trim())) {
                                            //                                            HDLHelper.updateHDLFile(processId.toString(),
                                            //                                                                    hdlId,
                                            //                                                                    status,null);
                                        } else if ("IN_PROGRESS".equals(dataSeteChildNodes.item(k).getTextContent().trim()) &&
                                                   firstTimeInprogress) {
                                            //                                            HDLHelper.updateHDLFile(processId.toString(),
                                            //                                                                    hdlId,
                                            //                                                                    status,null);
                                        }

                                        if ("COMPLETED".equals(status) ||
                                            "ERROR".equals(status)) {
                                            System.out.println("Data load status in interface tables :" +
                                                               status);

                                            //                                            HDLHelper.updateHDLFile(processId.toString(),
                                            //                                                                    hdlId,
                                            //                                                                    status,null);
                                            return true;
                                        } else {
                                            try {
                                                System.out.println("Waiting for " +
                                                                   (6000 /
                                                                    1000) +
                                                                   " secs, to recalculate the status ... ");
                                                Thread.sleep(6000);
                                                invokeGetDataSetStatus(processId,
                                                                       hdlId);
                                                resp = true;
                                            } catch (InterruptedException e) {
                                                System.out.println("\n\nException while thread waiting....Program Exit");
                                                resp = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }
    
    public static void main(String [] args){
        
        HCMDataLoaderHelper obj = new HCMDataLoaderHelper();
        
        obj.loadDataService("11", "TEST", "122");
    }
}
