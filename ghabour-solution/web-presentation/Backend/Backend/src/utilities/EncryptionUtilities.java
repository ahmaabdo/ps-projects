package utilities;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class EncryptionUtilities {

    static private Cipher ecipher;
    static private Cipher dcipher;
    static private SecretKey key;
    static private String keyStr = "AppsproCommKey";
    static private String keyStr2 = "P@$$w0rdOW";
    static private boolean ZASitekey;

    private EncryptionUtilities() {
    }

    public static String convertCharset(String text, String charset) {
        String temp = text;
        if (text != null && !text.isEmpty()) {
            try {
                temp = new String(text.getBytes(charset));
            } catch (UnsupportedEncodingException e) {
               e.printStackTrace();
            }
        }

        return temp;
    }

    private static void generateSecretKey() {

        try {
            byte[] secret;
            if (ZASitekey) {
                secret = keyStr2.getBytes("UTF8");
            } else {
                secret = keyStr.getBytes("UTF8");
            }

            SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
            DESKeySpec desSpec = new DESKeySpec(secret);
            key = skf.generateSecret(desSpec);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String desEncrypt(String str) {
        ZASitekey = false;
        try {
            if (ecipher == null) {
                generateSecretKey();
                ecipher = Cipher.getInstance("DES");
                ecipher.init(Cipher.ENCRYPT_MODE, key);
            }
            byte[] utf8 = str.getBytes("UTF8");
            byte[] enc = ecipher.doFinal(utf8);
            return new sun.misc.BASE64Encoder().encode(enc);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return null;
    }

    public static String desDecrypt(String str) {
        ZASitekey = false;
        try {
            if (dcipher == null) {
                generateSecretKey();
                dcipher = Cipher.getInstance("DES");
                dcipher.init(Cipher.DECRYPT_MODE, key);
            }
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
            byte[] utf8 = dcipher.doFinal(dec);
            return new String(utf8, "UTF8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return null;
    }

}
