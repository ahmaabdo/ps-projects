/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;

import javax.sql.DataSource;

public class AppsproConnection {

    protected static String driverName; // = CommonConfigReader.getValue("DB_DRIVER") ;
    protected static String serverName; // = CommonConfigReader.getValue("DB_SERVER_NAME") ;
    protected static String portNumber; // = CommonConfigReader.getValue("DB_PORT") ;
    protected static String sid; // = CommonConfigReader.getValue("DB_SID") ;
    protected static String username; // = CommonConfigReader.getValue("DB_USERNAME") ;
    protected static String password; // = CommonConfigReader.getValue("DB_PASSWORD") ;
    private static String schemaName = "DEV";

    public static Connection getConnection(boolean commit) {
        if (CommonConfigReader.getValue("CONNECTION_TYPE").equalsIgnoreCase("JDBC")) {
            return getJDBCConnection(commit);
        } else {
            return getDSConnection(commit);
        }
    }

    public static Connection getConnection() {
        if (CommonConfigReader.getValue("CONNECTION_TYPE").equalsIgnoreCase("JDBC")) {
            return getJDBCConnection();
        } else {
            return getDSConnection();
        }
    }

    private static Connection getJDBCConnection(boolean autoCommit) {

        driverName = CommonConfigReader.getValue("DB_DRIVER");
        serverName = CommonConfigReader.getValue("DB_SERVER_NAME");
        portNumber = CommonConfigReader.getValue("DB_PORT");
        sid = CommonConfigReader.getValue("DB_SID");
        username = CommonConfigReader.getValue("DB_USERNAME");
        password = CommonConfigReader.getValue("DB_PASSWORD");

        Connection connection = null;

        try {
            Class.forName(driverName);
            connection
                    = DriverManager.getConnection("jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid, username,
                            password);
            connection.setAutoCommit(autoCommit);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    private static Connection getJDBCConnection() {
        driverName = CommonConfigReader.getValue(schemaName + "_" + "DB_DRIVER");
        serverName = CommonConfigReader.getValue(schemaName + "_" + "DB_SERVER_NAME");
        portNumber = CommonConfigReader.getValue(schemaName + "_" + "DB_PORT");
        sid = CommonConfigReader.getValue(schemaName + "_" + "DB_SID");
        username = CommonConfigReader.getValue(schemaName + "_" + "DB_USERNAME");
        password = CommonConfigReader.getValue(schemaName + "_" + "DB_PASSWORD");

        Connection connection = null;

        try {
            String connectionUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(HOST=" + serverName + ")(PORT=" + portNumber + ")(PROTOCOL=tcp))(CONNECT_DATA=(SERVICE_NAME=" + sid + ")))";
            Class.forName(driverName);
            connection
                    = DriverManager.getConnection(connectionUrl, username,
                            password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    private static Connection getDSConnection() {
        return getDSConnection(CommonConfigReader.getValue("DS_NAME"));
    }

    private static Connection getDSConnection(boolean autoCommit) {
        Connection connection = getDSConnection(CommonConfigReader.getValue("DS_NAME"));
        try {
            connection.setAutoCommit(autoCommit);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    private static Connection getDSConnection(String dsName) {
        DataSource dataSource = null;
        Connection conn = null;
        try {
            dataSource = ServiceLocator.getDataSource(dsName);
            conn = dataSource.getConnection();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (NamingException ex) {
            Logger.getLogger(AppsproConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

        return conn;
    }

    public void closeResources(Connection connection, Statement stmt, ResultSet rs) {
        closeResources(rs);
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, PreparedStatement stmt, ResultSet rs) {
        closeResources(rs);
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, Statement stmt) {
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, PreparedStatement stmt) {
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Statement stmt, ResultSet rs) {
        closeResources(rs);
        closeResources(stmt);
    }

    public void closeResources(PreparedStatement stmt, ResultSet rs) {
        closeResources(rs);
        closeResources(stmt);
    }

    public void closeResources(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO
        }
    }

    public void closeResources(PreparedStatement preparedStatement) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO
        }
    }

    public void closeResources(Statement stmt) {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO
        }
    }

    public void closeResources(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO
        }
    }

    public String getGeneratedSequence(Statement stmt) {
        ResultSet rs = null;
        try {
            rs = stmt.getGeneratedKeys();
            rs.next();
            return rs.getString(1);
        } catch (SQLException sqlExp) {
            sqlExp.printStackTrace();
        } finally {
            closeResources(rs);
        }

        return null;
    }

    protected Integer getSeq(String seqName, Connection connection) throws SQLException {
        String query = "SELECT " + seqName + ".NEXTVAL AS SEQ_ID FROM DUAL";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.next()) {
            return rs.getInt("SEQ_ID");
        }
        return null;
    }

}
