package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.ModelBean;
import com.appspro.fusionsshr.dao.ModelDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.restHelper.GenricHandler;

import org.json.JSONObject;

public class ModelHandler extends GenricHandler{
    public JSONObject insertModel(String body) {

        int list = 0;
        ModelDAO dao = new ModelDAO();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ModelBean bean;
            bean = mapper.readValue(body, ModelBean.class);
            list = dao.insertModel(bean);
            String id = "{\"id\":" + list + "}";
            return (new JSONObject(id));
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject("UnknownErrorCode");
        }
    }

    public JSONObject getModel(String body) {

        ModelBean list = new ModelBean();
        ModelDAO dao = new ModelDAO();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ModelBean bean;
            bean = mapper.readValue(body, ModelBean.class);
            list = dao.getModel(bean);
            return new JSONObject(list);
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject("UnknownErrorCode");
        }
    }

}
