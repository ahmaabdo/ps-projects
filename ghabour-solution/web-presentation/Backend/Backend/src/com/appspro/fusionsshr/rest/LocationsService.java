package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.LocationsHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/LocationService")
public class LocationsService {
    LocationsHandler handler=new LocationsHandler();
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getLocations() {
        return handler.getAllLocations().toString();
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertLocation(String body) {
        return handler.insertLocation(body).toString();
    }
    @PUT
    @Path("/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateLocation(String body) {
        return handler.updateLocation(body).toString();
    }
}
