package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.FaqBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import utilities.EncryptionUtilities;

public class FaqDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ArrayList<FaqBean> getAllFaqs() {
        ArrayList<FaqBean> faqList = new ArrayList<FaqBean>();
        connection = AppsproConnection.getConnection();
        String query = "select * FROM " + getSchema_Name() + ".XXX_FAQS";
        try {
            ps = connection.prepareStatement(query);
//            ps.setString(1, username);
//            ps.setString(2, EncryptionUtilities.desEncrypt(password));
            rs = ps.executeQuery();

            while (rs.next()) {
                FaqBean bean = new FaqBean();
                bean.setId(rs.getInt("ID"));
                bean.setQuesAr(rs.getString("QUES_AR"));
                bean.setQuesEn(rs.getString("QUES_EN"));
                bean.setAnswerAr(rs.getString("ANSWER_AR"));
                bean.setAnswerEn(rs.getString("ANSWER_EN"));
                bean.setType(rs.getString("TYPE"));
                bean.setLookupTypeAr(rs.getString("LOOKUP_TYPE_AR"));
                bean.setLookupTypeEn(rs.getString("LOOKUP_TYPE_EN"));
                faqList.add(bean);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return faqList;

    }

    public FaqBean insertFaq(FaqBean bean) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "insert into " + getSchema_Name() + ".XXX_FAQS (QUES_AR,QUES_EN,ANSWER_AR,ANSWER_EN,TYPE,LOOKUP_TYPE_AR,LOOKUP_TYPE_EN) VALUES(?,?,?,?,?,?,?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, bean.getQuesAr());
            ps.setString(2, bean.getQuesEn());
            ps.setString(3, bean.getAnswerAr());
            ps.setString(4, bean.getAnswerEn());
            ps.setString(5, bean.getType());
            ps.setString(6, bean.getLookupTypeAr());
            ps.setString(7, bean.getLookupTypeEn());
            
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    public FaqBean updateFaq(FaqBean bean) {
        
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "Update " + getSchema_Name() + ".XXX_FAQS set QUES_AR =? ,QUES_EN=?,ANSWER_AR=?,ANSWER_EN=?,TYPE=?,LOOKUP_TYPE_AR=?,LOOKUP_TYPE_EN=? where id=?";

            ps = connection.prepareStatement(query);
           
            ps.setString(1, bean.getQuesAr());
            ps.setString(2, bean.getQuesEn());
            ps.setString(3, bean.getAnswerAr());
            ps.setString(4, bean.getAnswerEn());
            ps.setString(5, bean.getType());
            ps.setString(6, bean.getLookupTypeAr());
            ps.setString(7, bean.getLookupTypeEn());
            ps.setInt(8,bean.getId());
            
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
}
