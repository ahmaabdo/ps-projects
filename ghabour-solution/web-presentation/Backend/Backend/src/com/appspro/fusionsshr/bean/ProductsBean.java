package com.appspro.fusionsshr.bean;

public class ProductsBean {
    private int id;
    private String brand;
    private String model;
    private String modelYear;
    private String trim;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setTrim(String trim) {
        this.trim = trim;
    }

    public String getTrim() {
        return trim;
    }
}
