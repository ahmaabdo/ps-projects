package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ModelAttachmentBean;
import static common.restHelper.RestHelper.getSchema_Name;

import com.bea.common.security.utils.encoders.BASE64Decoder;

import common.restHelper.RestHelper;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class ModelAttachmentDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public ModelAttachmentBean insertAttachment(String body) {

        ModelAttachmentBean obj = new ModelAttachmentBean();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(body);
        byte[] decodeByte ;
        JSONObject jsonObjInput;
        for (int i = 0; i < arr.length(); i++) {
            jsonObjInput = arr.getJSONObject(i);
            if(Integer.parseInt(jsonObjInput.get("id").toString()) < 0   ){
            beanList.add(jsonObjInput);
            }
        }
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "INSERT INTO  " + " " + getSchema_Name() + ".XXX_MODEL_ATTACH(ATTACHMENT,MODEL,BRAND,NAME) VALUES(?,?,?,?)";
            Blob blob = connection.createBlob();
            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
                BASE64Decoder decoder = new BASE64Decoder();
                decodeByte = decoder.decodeBuffer(bean.get("image").toString());
                blob.setBytes(1,decodeByte);
                ps.setBlob(1, blob);
                ps.setString(2, bean.get("model").toString());
                ps.setString(3, bean.get("brand").toString());
                ps.setString(4, bean.get("name").toString());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return obj;

    }
    public ArrayList<ModelAttachmentBean> getAttachment(String model) {
        ArrayList<ModelAttachmentBean> beanList = new ArrayList<ModelAttachmentBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_MODEL_ATTACH where model=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, model);
            rs = ps.executeQuery();
            while (rs.next()) {
                ModelAttachmentBean bean = new ModelAttachmentBean();
                bean.setId(rs.getInt("ID"));
                bean.setAttachment(rs.getBytes("ATTACHMENT"));
                bean.setName(rs.getString("NAME"));
                bean.setModel(rs.getString("model"));
                bean.setBrand(rs.getString("brand"));
                beanList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return beanList;
    }
    public ArrayList<ModelAttachmentBean> getAttachments() {
        ArrayList<ModelAttachmentBean> beanList = new ArrayList<ModelAttachmentBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_MODEL_ATTACH";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ModelAttachmentBean bean = new ModelAttachmentBean();
                bean.setId(rs.getInt("ID"));
                bean.setAttachment(rs.getBytes("ATTACHMENT"));
                bean.setName(rs.getString("NAME"));
                bean.setModel(rs.getString("model"));
                bean.setBrand(rs.getString("brand"));
                beanList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return beanList;
    }
    public String deleteAttachment(String body) {
            String del="{}";
            ModelAttachmentBean obj = new ModelAttachmentBean();
            List<JSONObject> beanList = new ArrayList<JSONObject>();
            JSONArray arr = new JSONArray(body);
            JSONObject jsonObjInput;
            for (int i = 0; i < arr.length(); i++) {
                jsonObjInput = arr.getJSONObject(i);
                beanList.add(jsonObjInput);
            }
            try {
                connection = AppsproConnection.getConnection();
                String query="DELETE FROM  "+ " " + getSchema_Name() + ".XXX_MODEL_ATTACH WHERE ID=?";
                ps = connection.prepareStatement(query);
                for (JSONObject bean : beanList) {
                ps.setString(1,bean.get("id").toString());
                ps.executeUpdate();
                }
                del="{\"state\":\"del\"}";
            } catch (Exception e) {
               e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
            return del;
        }
}
