package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.LocationsBean;
import com.appspro.fusionsshr.dao.LocationsDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.restHelper.GenricHandler;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class LocationsHandler extends GenricHandler{
    LocationsDAO dao = new LocationsDAO();
    public JSONArray getAllLocations() {
        ArrayList<LocationsBean> list = new ArrayList<LocationsBean>();
        list = dao.getAllLocations();
        return new JSONArray(list);
    }
    public JSONObject insertLocation(String body) {

        LocationsBean list = new LocationsBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            LocationsBean bean;
            bean = mapper.readValue(body, LocationsBean.class);
            list = dao.insertLocation(bean);
            return new JSONObject(list);
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject("UnknownErrorCode");
        }
    }
    public JSONObject updateLocation(String body) {

        LocationsBean list = new LocationsBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            LocationsBean bean;
            bean = mapper.readValue(body, LocationsBean.class);
            list = dao.updateLocation(bean);
            return new JSONObject(list);
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject("UnknownErrorCode");
        }
    }
}
