package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ModelBean;
import static common.restHelper.RestHelper.getSchema_Name;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ModelDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public int insertModel(ModelBean bean) {
        int modelId=0;
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
                            
                query = "insert into " + getSchema_Name() + ".XXX_MODEL (BRAND,MODEL) VALUES(?,?)";
                
                ps = connection.prepareStatement(query);
                
                ps.setString(1, bean.getBrand());
                ps.setString(2, bean.getModel());

                ps.executeUpdate();
                modelId= getModel(bean).getId();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return modelId;
    }
    
    public ModelBean getModel(ModelBean bean) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
                            
                query = "select id from " + getSchema_Name() + ".XXX_MODEL where brand=? and model=?";

            ps = connection.prepareStatement(query);

            ps.setString(1, bean.getBrand());
            ps.setString(2, bean.getModel());
            rs = ps.executeQuery();
            while (rs.next()) {
                    bean.setId(rs.getInt("ID"));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
        
    }
}
