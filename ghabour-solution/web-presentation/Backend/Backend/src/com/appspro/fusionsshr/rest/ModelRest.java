package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.ModelHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/model")


public class ModelRest {
    private ModelHandler handler = new ModelHandler();
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String insertModel(String body) {
        return handler.insertModel(body).toString();
    }
    @POST
    @Path("/getModel")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getModel(String body) {
        return handler.getModel(body).toString();
    }
}
