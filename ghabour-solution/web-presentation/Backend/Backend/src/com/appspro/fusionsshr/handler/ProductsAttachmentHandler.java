package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.ProductsAttachmentBean;
import com.appspro.fusionsshr.dao.ProductsAttachmentDAO;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProductsAttachmentHandler {
    ProductsAttachmentDAO dao = new ProductsAttachmentDAO();
    public JSONObject insertAttachment(String body){

            ProductsAttachmentBean list = new ProductsAttachmentBean();
            try {
                list = dao.insertAttachment(body);
                return new JSONObject(list);
            } catch (Exception e) {
                return new JSONObject(list);
            }
        }
    public JSONObject deleteAttachment(String body){
            String list="";
//            ProductsAttachmentBean list = new ProductsAttachmentBean();
            try {
                 list = dao.deleteAttachment(body);
                return new JSONObject(list);
            } catch (Exception e) {
                return new JSONObject(list);
            }
        }
    public JSONArray getAllAttachment(int productId){

            ArrayList<ProductsAttachmentBean> list = new ArrayList<ProductsAttachmentBean>();
            list = dao.getAttachment(productId);

            return new JSONArray(list);
        }
}
