package com.appspro.fusionsshr.handler;

import com.appspro.fusionsshr.bean.ModelAttachmentBean;

import com.appspro.fusionsshr.dao.ModelAttachmentDAO;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class ModelAttachmentHandler {
    ModelAttachmentDAO dao=new ModelAttachmentDAO();
    public JSONObject insertAttachment(String body){

            ModelAttachmentBean list = new ModelAttachmentBean();
            try {
                list = dao.insertAttachment(body);
                return new JSONObject(list);
            } catch (Exception e) {
                return new JSONObject(list);
            }
        }
    public JSONObject deleteAttachment(String body){
            String list="";
            try {
                 list = dao.deleteAttachment(body);
                return new JSONObject(list);
            } catch (Exception e) {
                return new JSONObject(list);
            }
        }
    public JSONArray getAllAttachment(String model){

            ArrayList<ModelAttachmentBean> list = new ArrayList<ModelAttachmentBean>();
            list = dao.getAttachment(model);

            return new JSONArray(list);
        }
    public JSONArray getAllAttachments(){

            ArrayList<ModelAttachmentBean> list = new ArrayList<ModelAttachmentBean>();
            list = dao.getAttachments();

            return new JSONArray(list);
        }
}
