package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.ProductsAttachmentHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("attachment")
public class ProductsAttachmentRest {
    ProductsAttachmentHandler handler = new ProductsAttachmentHandler();
    @POST
    @Path("/addImg")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertAttachment(String body) {
        return handler.insertAttachment(body).toString();
    }
    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateAttachment(String body) {
        return handler.deleteAttachment(body).toString();
    }
    @GET
    @Path("/{productId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allAttachment(@PathParam("productId")
        int productId) {
        return handler.getAllAttachment(productId).toString();
    }
}
