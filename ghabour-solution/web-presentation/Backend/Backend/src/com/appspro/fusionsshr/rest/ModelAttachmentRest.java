package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.ModelAttachmentHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("attachmentModel")
public class ModelAttachmentRest {
    ModelAttachmentHandler handler = new ModelAttachmentHandler();
    @POST
    @Path("/addImg")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertAttachment(String body) {
        return handler.insertAttachment(body).toString();
    }
    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateAttachment(String body) {
        return handler.deleteAttachment(body).toString();
    }
    @GET
    @Path("/{model}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allAttachment(@PathParam("model")
        String model) {
        return handler.getAllAttachment(model).toString();
    }
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allAttachments() {
        return handler.getAllAttachments().toString();
    }
}
