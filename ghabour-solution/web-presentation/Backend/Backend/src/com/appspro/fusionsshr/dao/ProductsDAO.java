package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ProductsBean;
import static common.restHelper.RestHelper.getSchema_Name;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class ProductsDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public int insertProduct(ProductsBean bean) {
        int productId=0;
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
                            
                query = "insert into " + getSchema_Name() + ".XXX_PRODUCT (BRAND,MODEL,MODEL_YEAR) VALUES(?,?,?)";
                
                ps = connection.prepareStatement(query);
                
                ps.setString(1, bean.getBrand());
                ps.setString(2, bean.getModel());
                ps.setString(3, bean.getModelYear());

                ps.executeUpdate();
                productId= getProduct(bean).getId();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return productId;
    }
    
    public ProductsBean getProduct(ProductsBean bean) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
                            
                query = "select id from " + getSchema_Name() + ".XXX_PRODUCT where brand=? and model=? and model_year=?";

            ps = connection.prepareStatement(query);

            ps.setString(1, bean.getBrand());
            ps.setString(2, bean.getModel());
            ps.setString(3, bean.getModelYear());
            rs = ps.executeQuery();
            while (rs.next()) {
                    bean.setId(rs.getInt("ID"));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
        
    }
    
}
