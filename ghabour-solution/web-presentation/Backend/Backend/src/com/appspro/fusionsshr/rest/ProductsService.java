package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.ProductsHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

@Path("/productsService")
public class ProductsService {

    private ProductsHandler handler = new ProductsHandler();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllProducts() {
        JSONObject reponse = null;
        try {
            reponse = handler.getAllProducts();
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
    @POST
    @Path("/addOpp")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNewOpp(String body) {

        JSONObject reponse = null;
        try {
            reponse = handler.insertProductsOpportunity(body);
            return Response.ok(reponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String insertProduct(String body) {
        return handler.insertProduct(body).toString();
    }
    @POST
    @Path("/getProduct")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getProduct(String body) {
        return handler.getProduct(body).toString();
    }
}
