package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.handler.FAQSHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("/FAQService")
public class FaqService {
    FAQSHandler handler = new FAQSHandler();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getFaqs() {
        return handler.getAllFaqs().toString();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertFaq(String body) {
        return handler.insertFaq(body).toString();
    }
    @PUT
    @Path("/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateFaq(String body) {
        return handler.updateFaq(body).toString();
    }
}
