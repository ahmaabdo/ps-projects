package com.appspro.fusionsshr.bean;

import java.sql.Blob;

public class ProductsAttachmentBean {
    private int id;
    //    @Lob
    private byte[] image;
    private String productId;
    private String active;
    private String name;

  
    public void setActive(String active) {
        this.active = active;
    }

    public String getActive() {
        return active;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
