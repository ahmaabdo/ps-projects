package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ProductsAttachmentBean;


import com.bea.common.security.utils.encoders.BASE64Decoder;

import common.restHelper.RestHelper;

import java.sql.Blob;

import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProductsAttachmentDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ProductsAttachmentBean insertAttachment(String body) {

        ProductsAttachmentBean obj = new ProductsAttachmentBean();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(body);
        byte[] decodeByte ;
        JSONObject jsonObjInput;
        for (int i = 0; i < arr.length(); i++) {
            jsonObjInput = arr.getJSONObject(i);
            if(Integer.parseInt(jsonObjInput.get("id").toString()) < 0   ){
            beanList.add(jsonObjInput);
            }
        }
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "INSERT INTO  " + " " + getSchema_Name() + ".XXX_PRODUCT_ATTACHMENT(IMAGE,PRODUCT_ID,ACTIVE,NAME) VALUES(?,?,?,?)";
            Blob blob = connection.createBlob();
            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
                BASE64Decoder decoder = new BASE64Decoder();
                decodeByte = decoder.decodeBuffer(bean.get("image").toString());
                blob.setBytes(1,decodeByte);
                ps.setBlob(1, blob);
                ps.setInt(2, bean.getInt("productId"));
                ps.setString(3, bean.get("active").toString());
                ps.setString(4, bean.get("name").toString());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return obj;

    }
    public ArrayList<ProductsAttachmentBean> getAttachment(int productId) {
        ArrayList<ProductsAttachmentBean> beanList = new ArrayList<ProductsAttachmentBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_PRODUCT_ATTACHMENT where PRODUCT_ID=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, productId);
            rs = ps.executeQuery();
            while (rs.next()) {
                ProductsAttachmentBean bean = new ProductsAttachmentBean();
                bean.setId(rs.getInt("ID"));
                bean.setImage(rs.getBytes("IMAGE"));
                bean.setActive(rs.getString("ACTIVE"));
                bean.setName(rs.getString("NAME"));
                beanList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return beanList;
    }
    public String deleteAttachment(String body) {
            String del="{}";
            ProductsAttachmentBean obj = new ProductsAttachmentBean();
            List<JSONObject> beanList = new ArrayList<JSONObject>();
            JSONArray arr = new JSONArray(body);
            JSONObject jsonObjInput;
            for (int i = 0; i < arr.length(); i++) {
                jsonObjInput = arr.getJSONObject(i);
                beanList.add(jsonObjInput);
            }
            try {
                connection = AppsproConnection.getConnection();
                String query="DELETE FROM  "+ " " + getSchema_Name() + ".XXX_PRODUCT_ATTACHMENT WHERE ID=?";
                ps = connection.prepareStatement(query);
                for (JSONObject bean : beanList) {
                ps.setString(1,bean.get("id").toString());
                ps.executeUpdate();
                }
                del="{\"state\":\"del\"}";
            } catch (Exception e) {
               e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
            return del;
        }
}
