package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.LocationsBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import utilities.EncryptionUtilities;

public class LocationsDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public ArrayList<LocationsBean> getAllLocations() {
        ArrayList<LocationsBean> locationList = new ArrayList<LocationsBean>();
        connection = AppsproConnection.getConnection();
        String query = "select * FROM " + getSchema_Name() +".XXX_LOCATIONS";
        try {
            ps = connection.prepareStatement(query);
//            ps.setString(1, username);
//            ps.setString(2, EncryptionUtilities.desEncrypt(password));
            rs = ps.executeQuery();

            while (rs.next()) {
                LocationsBean bean = new LocationsBean();
                bean.setId(rs.getInt("ID"));
                bean.setLocationName(rs.getString("LOCATION_NAME"));
                bean.setCity(rs.getString("CITY"));
                bean.setAddress(rs.getString("ADDRESS"));
                bean.setLocationNameAr(rs.getString("LOCATION_NAME_AR"));
                bean.setCityAr(rs.getString("CITY_AR"));
                bean.setAddressAr(rs.getString("ADDRESS_AR"));
                bean.setBrand(rs.getString("BRAND"));
                bean.setLatitude(rs.getString("LATITUDE"));
                bean.setLongitude(rs.getString("LONGITUDE"));
                locationList.add(bean);
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        
        return locationList;

    }
    public LocationsBean updateLocation(LocationsBean bean) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            query =
            "Update " + getSchema_Name() + ".XXX_LOCATIONS set LOCATION_NAME =? ,CITY=?,ADRRESS=?,BRAND=?,LATITUDE=?,LONGITUDE=?,LOCATION_NAME_AR =? ,CITY_AR=?,ADRRESS_AR=? where id=?";

            ps = connection.prepareStatement(query);

            ps.setString(1, bean.getLocationName());
            ps.setString(2, bean.getCity());
            ps.setString(3, bean.getAddress());
            ps.setString(4, bean.getBrand());
            ps.setString(5, bean.getLatitude());
            ps.setString(6, bean.getLongitude());
            ps.setString(7, bean.getLocationNameAr());
            ps.setString(8, bean.getCityAr());
            ps.setString(9, bean.getAddressAr());
            ps.setInt(10,bean.getId());
            
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    public LocationsBean insertLocation(LocationsBean bean) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "insert into " + getSchema_Name() + ".XXX_LOCATIONS (LOCATION_NAME,CITY,ADDRESS,BRAND,LATITUDE,LONGITUDE,LOCATION_NAME_AR,CITY_AR,ADDRESS_AR) VALUES(?,?,?,?,?,?,?,?,?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, bean.getLocationName());
            ps.setString(2, bean.getCity());
            ps.setString(3, bean.getAddress());
            ps.setString(4, bean.getBrand());
            ps.setString(5, bean.getLatitude());
            ps.setString(6, bean.getLongitude());
            ps.setString(7, bean.getLocationNameAr());
            ps.setString(8, bean.getCityAr());
            ps.setString(9, bean.getAddressAr());
            
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
}
