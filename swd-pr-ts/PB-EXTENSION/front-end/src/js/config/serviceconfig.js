define(['jquery'], function ($) {

    function serviceConfig() {

        var self = this;
        var authCode = "Basic bmlkYWxAc3VsdGFuLmNvbS5rdwo6QnNiY";
        self.contentTypeApplicationJSON = 'application/json';
        self.contentTypeApplicationXWWw = 'application/x-www-form-urlencoded; charset=UTF-8';
        self.headers = {};

        self.callAddFileService = function (serviceUrl, payload, headers) {
            var defer = $.Deferred();
            // headers.Authorization = paasAuth;

            $.ajax({
                type: "POST",
                async: false,
                headers: headers,
                url: serviceUrl,
                cache: false,
                contentType: false,
                processData: false,
                data: payload,

                success: function (data) {

                    defer.resolve(data);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr, thrownError);
                }
            });
            return $.when(defer);
        };

        self.callGetService = function (serviceUrl, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            $.ajax({
                type: "GET",
                async: asynchronized,
                url: serviceUrl,
                headers: headers,
                contentType: contentType,
                // async : false, 
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callDeleteService = function (serviceUrl, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            $.ajax({
                type: "DELETE",
                async: asynchronized,
                url: serviceUrl,
                headers: headers,
                contentType: contentType,
                // async : false, 
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callPostService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }
            var defer = $.Deferred();
            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                    console.log(thrownError);
                }
            });
            return $.when(defer);
        };


        self.callPutService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }
            var defer = $.Deferred();
            $.ajax({
                type: "PUT",
                //                dataType: 'jsonp',  
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };


        self.callGetApexService = function (serviceUrl, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            headers.Authorization = authCode;
            $.ajax({
                type: "GET",
                url: serviceUrl,
                headers: headers,
                async: false,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callPostApexService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            headers.Authorization = authCode;
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }

            var defer = $.Deferred();
            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callPutApexService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            headers.Authorization = authCode;
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }

            var defer = $.Deferred();
            $.ajax({
                type: "PUT",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.callDeleteApexService = function (serviceUrl, payload, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            headers.Authorization = authCode;
            $.ajax({
                type: "DELETE",
                async: asynchronized,
                url: serviceUrl,
                headers: headers,
                data: payload,
                contentType: contentType,
                success: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });

            return $.when(defer);
        };

        self.callPostServiceUncodeed = function (serviceUrl, payload, contentType, asynchronized, headers) {

            var defer = $.Deferred();
            $.ajax({
                type: "POST",
                async: false,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payload,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
    }

    return new serviceConfig();
});