    define(['config/serviceconfig'], function (serviceConfig) {

    function services() {

        var self = this;
        var biReportServletPath = "report/commonbireport";
        var headers = {
            // "Authorization": "Basic bmlkYWxAc3VsdGFuLmNvbS5rdzpCc2JjT3JhQDE5"
        };


      var restPath = "https://144.21.71.46:7002/ELSEWEDY-PB-context-root-TEST/webapi/";
//         var restPath = "http://127.0.0.1:7101/ELSEWEDY-PB-context-root-TEST/webapi/";
       
        self.authenticate = function (payload) {
            var serviceURL = restPath + "login";
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.getSaasGeneric = function (serviceName) {
            var serviceURL = restPath + serviceName;
//            var headers = {
//                "Authorization": "Basic QXBwc3Byby5wcm9qZWN0czpBaG1lZDEyMw=="
//            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

         self.getSaasLinksGeneric = function (serviceName) {
            var serviceURL = serviceName;
            var headers = {
                "Authorization": "Basic QXBwc3Byby5wcm9qZWN0czpBaG1lZDEyMw=="
            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };
        
        
        self.getSaasGeneric2 = function (serviceName) {
            var serviceURL = restPath4 + serviceName;
            var headers = {
                "Authorization": "Basic QXBwc3Byby5wcm9qZWN0czpBaG1lZDEyMw=="
            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.getGeneric = function (serviceName, header) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, header, true);
        };
        
        self.postGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.getAllEmployees = function (serviceName) {
            var serviceURL = restPath2 + serviceName;
            var headers = {
                "Authorization": "Basic QXBwc1Byby1IQ006QXBwc3Byb0AxMjM="
            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.getGenericReport = function (payload) {
            var serviceURL = restPath + biReportServletPath;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.searcheGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
                "content-type": "application/x-www-form-urlencoded"
            };
            return serviceConfig.callPostServiceUncodeed(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
        };

        self.editGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPutService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.postSaasGeneric = function (serviceName, payload) {
            var serviceURL = restPath3 + serviceName;

            var headers = {
                "Authorization": "Basic RklOLlVTRVI6QXBwc3Byb0AxMjM="
            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.postSaasPrGeneric = function (serviceName, payload) {

            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        
        self.addFile = function (serviceName, payload) {
            var headers = {
          };
            var serviceURL = restPath + serviceName;
          return serviceConfig.callAddFileService(serviceURL,payload,headers);
      };

        self.addGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.addGenericSaas = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };

        self.deleteGeneric = function (serviceName, headers) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callDeleteService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

    }

    return new services();
});