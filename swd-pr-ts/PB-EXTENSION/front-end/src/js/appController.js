/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'config/services', 'util/commonhelper', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
    'ojs/ojoffcanvas', 'ojs/ojbutton', 'jquery', 'ojs/ojarraytabledatasource','ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojmessages', 'ojs/ojdialog'],
    function (oj, ko, moduleUtils, services, commonhelper) {
        function ControllerViewModel() {
            var self = this;
            var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
            var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);

            self.username = ko.observable();
            self.password = ko.observable();
            self.userLogin = ko.observable();
            self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
            self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
            self.empGrade = ko.observable();
            self.empDepartment = ko.observable();
            self.empNumber = ko.observable();
            self.empJob = ko.observable();
            self.empName = ko.observable();
            self.jwt = ko.observable();
            self.usersArr = ko.observableArray();
            self.locationArr = ko.observableArray();
            self.vendorSiteArr = ko.observableArray();
            self.vendorArr = ko.observableArray();
            self.customerNameArr = ko.observableArray();
            self.loginUser = ko.observable();
            self.invoiceNum = ko.observableArray();
            self.buyerNameArr = ko.observableArray();
            self.businessUnitArr = ko.observableArray();
            self.invoiceNumArr = ko.observableArray();
            self.purchaseOrdersArrPoHeader = ko.observableArray();
            self.purchaseOrdersArrPoNum = ko.observableArray();
            self.vendorSiteIndepArr = ko.observableArray();
            self.vendorNameVal = ko.observable();
            self.messagesDataProvider = ko.observableArray();
            self.tProcurmentValue=ko.observable();
            self.totalService=ko.observable();


            self.loading = function (load) {
                if (load) {
                    $('#loading').show();
                } else {
                    $('#loading').hide();
                }
            }

            self.translate = function (key) {
                return oj.Translations.getTranslatedString(key);
            }
            // Search Function for lables 
            self.searchArray = function (nameKey, searchArray) {
                for (var i = 0; i < searchArray.length; i++) {
                    if (searchArray[i].value == nameKey) {
                        return searchArray[i].desc;
                    }
                }
            };
            if (sessionStorage.empName != "null") {
                self.empName(sessionStorage.empName);
            } else {
                self.empName('');
            }
            if (sessionStorage.empJob != "null") {
                self.empJob(sessionStorage.empJob);
            } else {
                self.empJob('');
            }
            if (sessionStorage.empNumber != "null") {
                self.empNumber(sessionStorage.empNumber);
            } else {
                self.empNumber('');

            }
            if (sessionStorage.empDepartment != "null") {
                self.empDepartment(sessionStorage.empDepartment);
            } else {
                self.empDepartment('');
            }
            if (sessionStorage.empGrade != "null") {
                self.empGrade(sessionStorage.empGrade);
            } else {
                self.empGrade('');
            }



            // self.globalSupplierNameArr = ko.observableArray();

            //self.getSupplerName();

            self.isUserLoggedIn = ko.observable(false);

            var url = new URL(window.location.href);
            var jwt;

            if (url.searchParams.get("jwt")) {
                jwt = url.searchParams.get("jwt");
                self.jwt(jwt);


                jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
                var username = jwtJSON.sub;
                self.username(username.split('.')[0] + username.split('.')[1]);

                self.userLogin(sessionStorage.Username);

                if (self.isUserLoggedIn() === 'false') {
                    self.isUserLoggedIn(false);
                } else {
                    self.isUserLoggedIn(true);
                }

            } else if (sessionStorage.isUserLoggedIn) {
                self.userLogin(sessionStorage.Username);


                if (self.isUserLoggedIn() === 'false') {
                    self.isUserLoggedIn(false);
                } else {
                    self.isUserLoggedIn(true);
                }

            }


            $(function () {
                $('#username').on('keydown', function (ev) {
                    var mobEvent = ev;
                    var mobPressed = mobEvent.keyCode || mobEvent.which;
                    if (mobPressed == 13) {
                        $('#username').blur();
                        $('#password').focus();
                    }
                    return true;
                });
            });

            //



            self.dialogYes = ko.observable();
            self.dialogOk = ko.observable();
            self.dialogNo = ko.observable();
            self.yes = ko.observable('Yes');
            self.ok = ko.observable('Ok');
            self.no = ko.observable('No');
            self.msg = ko.observable('')
            self.confirmMsg = function (msg, cb) {
                self.msg(msg);
                self.dialogYes(function () {
                    document.querySelector("#GenericYesNoDialog").close();
                    cb(true)
                })
                self.dialogNo(function () {
                    document.querySelector("#GenericYesNoDialog").close();
                    cb(false)
                })
                document.querySelector("#GenericYesNoDialog").open();
            };

            self.infomMsg = function (text, cb) {
                self.msg(text);
                self.dialogOk(function () {
                    document.querySelector("#GenericOkDialog").close();
                    if (cb && typeof cb == 'function')
                        cb(true);
                })

                document.querySelector("#GenericOkDialog").open();
            }

            //close notify in when focus on login input
            $("#userValidation,#passValidation").focusout(function () {
                //                    $(".notifyjs-bootstrap-base").notify().hide();
            });
            //login button call function
            self.validateUser = function () {

                $(".apLoginBtn button").addClass("loading");
                var username = self.username();
                var password = self.password();
                // Here we check null value and show error to fill
                if (!username && !password) {
                    //                        $("#userValidation").notify(
                    //                                "Please enter user name",
                    //                                {position: "top right", autoHide: false, className: 'errorInput'}
                    //                        );
                    //                        $("#passValidation").notify(
                    //                                "Please enter password",
                    //                                {position: "top right", autoHide: false, className: 'errorInput'}
                    //                        );
                } else if (!username) {
                    //                        $("#userValidation").notify(
                    //                                "Please enter user name",
                    //                                {position: "top right", autoHide: false, className: 'errorInput'}
                    //                        );
                } else if (!password) {
                    //                        $("#passValidation").notify(
                    //                                "Please enter user name",
                    //                                {position: "top right", autoHide: false, className: 'errorInput'}
                    //                        );
                } else {

                    $('#loader-overlay').show();
                    //                        req.validateUser(username, password, function (response) {
                    var loginCbFn = function (data) {
                        self.getEmployeeDetails();
                        var parsedData = jQuery.parseJSON(data.replace('\'', '"').replace('\'', '"'));


                        if (parsedData.result == true) {
                            self.messagesDataProvider([]);
                            self.isUserLoggedIn(true);
                            sessionStorage.setItem('username', self.isUserLoggedIn());

                            var userNamelabel = self.username();
                            sessionStorage.setItem('isUserLoggedIn', self.isUserLoggedIn());
                            sessionStorage.setItem('Username', userNamelabel);

                            $('#loader-overlay').hide();


                        } else {
                            $(".apLoginBtn button").removeClass("loading");
                            // If authentication failed
                            // alert(response.Error);
                            //                                $.notify("Wrong user name or passowrd", "error");
                            self.messagesDataProvider([]);
                            self.messagesDataProvider.push(self.createMessage('error', 'Invalid Username or Password'));

                            return;

                        }
                    };
                    self.loginUser(username);
                    var loginFailCbFn = function () {
                        $(".apLoginBtn button").removeClass("loading");
                        self.messagesDataProvider.push(self.createMessage('error', 'Invalid Username or Password'));
                        return;
                    };

                    var payload = {
                        "userName": username,
                        "password": password
                    };
                    services.authenticate(payload).then(loginCbFn, loginFailCbFn);
                    oj.Router.rootInstance.go('budgetSummry');
                    return;
                }
            };


            $(".home-icon").click(function () {
                oj.Router.rootInstance.go('budgetSummry');
            });

            self.isValidDate = function (date) {
                return date instanceof Date && !isNaN(date);
            };
            
            self.convertISOToLocalDate = function (date) {
                var tzoffset = (new Date()).getTimezoneOffset() * 60000;
                return (new Date(new Date(date) - tzoffset)).toISOString().slice(0, -1).substr(0, 10);
            };

            self.getEmployeeDetails = function () {
                var getEmployeeDetailsCbFn = function (data) {

                    sessionStorage.setItem('empName', data.displayName);
                    sessionStorage.setItem('empJob', data.jobName);
                    sessionStorage.setItem('empNumber', data.personNumber);
                    sessionStorage.setItem('empDepartment', data.departmentName);
                    sessionStorage.setItem('empGrade', data.grade);

                    if (sessionStorage.empName != "null") {
                        self.empName(sessionStorage.empName);

                    } else {
                        self.empName('');
                    }
                    if (sessionStorage.empJob != "null") {
                        self.empJob(sessionStorage.empJob);
                    } else {
                        self.empJob('');
                    }
                    if (sessionStorage.empNumber != "null") {
                        self.empNumber(sessionStorage.empNumber);
                    } else {
                        self.empNumber('');

                    }
                    if (sessionStorage.empDepartment != "null") {
                        self.empDepartment(sessionStorage.empDepartment);
                    } else {
                        self.empDepartment('');
                    }
                    if (sessionStorage.empGrade != "null") {
                        self.empGrade(sessionStorage.empGrade);
                    } else {
                        self.empGrade('');
                    }
                };
                var getEmployeeDetailsfailCbFn = function () {
                };
                var UrlPath = "getemployee/" + self.username();

                //services.getGeneric(UrlPath).then(getEmployeeDetailsCbFn, getEmployeeDetailsfailCbFn);
            };

            self.menuItemAction = function (event) {
                // if (event.target.value === 'out') {
                 location.reload();
                self.isUserLoggedIn(false);
                sessionStorage.setItem('username', '');
                sessionStorage.setItem('isUserLoggedIn', '');


                if (self.language() === 'english') {
                    $('html').attr({ 'dir': 'ltr' });
                } else if (self.language() === 'arabic') {
                    $('html').attr({ 'dir': 'rtl' });
                }

            };

            self.loginErrorPopup = function () {
                var popup = document.querySelector('#popup1');
                popup.close('#btnGo');
            };

            startAnimationListener = function (data, event) {
                var ui = event.detail;
                if (!$(event.target).is("#popup1"))
                    return;

                if ("open" === ui.action) {
                    event.preventDefault();
                    var options = { "direction": "top" };
                    oj.AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
                } else if ("close" === ui.action) {
                    event.preventDefault();
                    ui.endCallback();
                }
            };


            // Router setup
            self.router = oj.Router.rootInstance;
            self.router.configure({
                'budgetSummry': { label: 'Projet Budget', value: "budget/budgetSummry", title: "El Sewedy - Project Budget", isDefault: true},
                'invoiceSummary': { label: 'Invoice Summary', value: "invoice/invoiceSummary", title: "El Sewedy - Invoice"},
                'budgetSheetTable': { label: 'Projet Budget Tabke', value: "budget/budgetSheetTable", title: "El Sewedy - Project Version Details"},
                'procurmentPlan': { label: 'Projet Procurment Plan', value: "reports/procurmentPlan", title: "El Sewedy - Reports"},
                'projectOverHead': { label: ' project OverHead', value: "projectOverHead/projectOverHead", title: "El Sewedy - projectOverHead"},
                'costScreen': { label: ' project Cost', value: "costing/costScreen", title: "El Sewedy - project Cost"},
                'cashFlow': { label: ' project cashFlow', value: "cashFlow/cashFlowScreen", title: "El Sewedy - project cashFlow"},
                'costCategory': { label: 'Cost Category', value: "costing/costCategory", title: "El Sewedy - project costCategory"},

            });
            oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();


            self.localeLogin = ko.observable();
            self.unknownErr = ko.observable();
            self.UsernameLabel = ko.observable();
            self.passwordLabel = ko.observable();
            self.loginword = ko.observable();
            self.forgotPasswordLabel = ko.observable();

            self.localeName = ko.observable();
            self.localeJop = ko.observable();
            self.localeempNO = ko.observable();
            self.localeDep = ko.observable();
            self.localeGrade = ko.observable();


            self.language = ko.observable();
            self.languagelable = ko.observable("عربي");
            var storage = window.localStorage;
            var preferedLanguage;

            self.langSelected = function (event) {
                var valueObj = buildValueChange(event['detail']);
                self.setPreferedLanguage(valueObj.value);
            };

            function buildValueChange(valueParam) {
                var valueObj = {};
                if (valueParam.previousValue) {
                    valueObj.value = valueParam.value;
                } else {
                    valueObj.value = valueParam.previousValue;
                }
                return valueObj;
            }

            $(function () {
                storage.setItem('setLang', 'english');
                //                    storage.setItem('setLang', 'arabic');
                preferedLanguage = storage.getItem('setLang');
                self.language(preferedLanguage);
                changeToArabic(preferedLanguage);
                //document.addEventListener("deviceready", selectLanguage, false);
            });

            // Choose prefered language
            function selectLanguage() {
                oj.Config.setLocale('english',
                    function () {
                        $('html').attr({ 'lang': 'en-us', 'dir': 'ltr' });
                        self.localeLogin(oj.Translations.getTranslatedString('Login'));
                        self.unknownErr(oj.Translations.getTranslatedString('unknownErr'));
                        self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                        self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                        self.loginword(oj.Translations.getTranslatedString('login'));
                        self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                        self.localeName(oj.Translations.getTranslatedString('Name'));
                        self.localeJop(oj.Translations.getTranslatedString('JOP'));
                        self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                        self.localeDep(oj.Translations.getTranslatedString('Dep'));
                        self.localeGrade(oj.Translations.getTranslatedString('Grade'));

                    }
                );
            }

            // Set Prefered Language
            self.setPreferedLanguage = function (lang) {
                var selecedLanguage = lang;
                storage.setItem('setLang', selecedLanguage);
                storage.setItem('setTrue', true);
                preferedLanguage = storage.getItem('setLang');

                // Call function to convert arabic
                changeToArabic(preferedLanguage);
            };

            self.createMessage = function (type, summary) {
                return {
                    severity: type,
                    summary: summary,
                    autoTimeout: 0
                };
            };

            function changeToArabic(preferedLanguage) {
                if (preferedLanguage === 'arabic') {

                    oj.Config.setLocale('ar-sa',
                        function () {
                            $('html').attr({ 'lang': 'ar-sa', 'dir': 'rtl' });
                            self.localeLogin(oj.Translations.getTranslatedString('Login'));
                            self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                            self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                            self.loginword(oj.Translations.getTranslatedString('login'));
                            self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                            self.localeName(oj.Translations.getTranslatedString('Name'));
                            self.localeJop(oj.Translations.getTranslatedString('JOP'));
                            self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                            self.localeDep(oj.Translations.getTranslatedString('Dep'));
                            self.localeGrade(oj.Translations.getTranslatedString('Grade'));
                        }
                    );

                } else if (preferedLanguage === 'english') {
                    selectLanguage();


                }
            }
            $('.login-label').toggleClass('user-label-right');
            $('.input100').toggleClass('box-input-text');
            $('.wrap-input100').toggleClass('box-input-text');
            self.changeLanguage = function () {
                //storage.setItem('setLang', 'arabic');
                if (self.language() === 'english') {
                    self.language("arabic");
                    self.setPreferedLanguage(self.language());
                    self.languagelable("English");
                    $('.login-label').toggleClass('user-label-right');
                    $('.login-label').removeClass('user-label-left');
                    $('.input100').toggleClass('box-input-text');
                    $('.wrap-input100').toggleClass('box-input-text');
                } else if (self.language() === 'arabic') {
                    self.language("english");
                    self.setPreferedLanguage(self.language());
                    self.languagelable("عربي");
                    $('.login-label').toggleClass('user-label-left');
                    $('.login-label').removeClass('user-label-right');
                    $('.input100').removeClass('box-input-text');
                    $('.wrap-input100').removeClass('box-input-text');


                }
                //  self.setPreferedLanguage('arabic');
                //  self.languagelable("Arabic");
            };


            self.moduleConfig = ko.observable({ 'view': [], 'viewModel': null });

            // Search Start
            self.keyword = ko.observableArray();
            //Business Unit Input search Start 

            self.searchWord = ko.observable();
            self.tags = [
                { value: "Value 1", label: "Unit 1" },
                { value: "Value 2", label: "Unit 2" },
                { value: "Value 3", label: "Unit 3" },
                { value: "Value 4", label: "Unit 4" },
                { value: "Value 5", label: "Unit 5" },
                { value: "Value 6", label: "Unit 6" },
                { value: "Value 7", label: "Unit 7" }
            ];

            self.tagsDataProvider = new oj.ArrayDataProvider(self.tags, { keyAttributes: 'value' });

            self.search = function (event) {



                var trigger = event.type;
                var term;

                if (trigger === "ojValueUpdated") {
                    // search triggered from input field
                    // getting the search term from the ojValueUpdated event
                    term = event['detail']['value'];
                    trigger += " event";
                } else {
                    // search triggered from end slot
                    // getting the value from the element to use as the search term.
                    term = document.getElementById("search").value;
                    trigger = "click on search button";
                }

            };
            //Business Unit Input search END
            // Search End

            //Global function to format date to the following format : d-MMM-yy -> 2-Feb-20 
            self.formatDate = function (date) {
                return new Date(date).toLocaleDateString('en-GB', {
                    day: 'numeric',
                    month: 'short',
                    year: '2-digit'
                }).split(' ').join('-');

            };
            
            self.dateConverter = ko.observable(oj.Validation.converterFactory('datetime').
                    createConverter(
                            {
                                pattern: "dd-MMM-yyyy"
                            }));

            self.toolbarClassNames = ko.observableArray([]);

            self.toolbarClasses = ko.computed(function () {
                return self.toolbarClassNames().join(" ");
            }, this);
            // toolbar End

            // Related to New Design Add BY Mohamed Yasser  //End

            self.loadModule = function () {
                ko.computed(function () {
                    var name = self.router.moduleConfig.name();
                    var viewPath = 'views/' + name + '.html';
                    var modelPath = 'viewModels/' + name;
                    var masterPromise = Promise.all([
                        moduleUtils.createView({ 'viewPath': viewPath }),
                        moduleUtils.createViewModel({ 'viewModelPath': modelPath })
                    ]);
                    masterPromise.then(
                        function (values) {
                            self.moduleConfig({ 'view': values[0], 'viewModel': values[1] });
                        },
                        function (reason) { }
                    );
                });
            };
            // Navigation setup
            self.navDataSource = new oj.ArrayTableDataSource([], { idAttribute: 'id' });

            // Drawer
            // Close offcanvas on medium and larger screens
            self.mdScreen.subscribe(function () {
                oj.OffcanvasUtils.close(self.drawerParams);
            });
            self.drawerParams = {
                displayMode: 'push',
                selector: '#navDrawer',
                content: '#pageContent'
            };
            // Called by navigation drawer toggle button and after selection of nav drawer item
            self.toggleDrawer = function () {
                return oj.OffcanvasUtils.toggle(self.drawerParams);
            };
            // Add a close listener so we can move focus back to the toggle button when the drawer closes
            $("#navDrawer").on("ojclose", function () {
                $('#drawerToggleButton').focus();
            });

            // Header
            // Application Name used in Branding Area
            self.appName = ko.observable("Nexus");
            // User Info used in Global Navigation area


            // Footer
            function footerLink(name, id, linkTarget) {
                this.name = name;
                this.linkId = id;
                this.linkTarget = linkTarget;
            }
            self.footerLinks = ko.observableArray([
                new footerLink('About Oracle', 'aboutOracle', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
                new footerLink('Contact Us', 'contactUs', 'http://www.oracle.com/us/corporate/contact/index.html'),
                new footerLink('Legal Notices', 'legalNotices', 'http://www.oracle.com/us/legal/index.html'),
                new footerLink('Terms Of Use', 'termsOfUse', 'http://www.oracle.com/us/legal/terms/index.html'),
                new footerLink('Your Privacy Rights', 'yourPrivacyRights', 'http://www.oracle.com/us/legal/privacy/index.html')
            ]);
        }

        $(window).scroll(function () {

            if ($(this).scrollTop() > 130) {
                $('header').addClass("purple");
                $('header').addClass("scrollnav");
                $('header').removeClass("forwhite");
            } else {
                $('header').removeClass("purple");
                $('header').addClass("forwhite");
                $('header').removeClass("scrollnav");
            }
        });

        return new ControllerViewModel();
    }
);
