define([], function () {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function () {
            var host = "http://192.168.1.118:7101/EBSMobile/rest";
            return host;
        };

        self.getPaaSHost = function () {
            var host = "https://apex-hcuk.db.em2.oraclecloudapps.com/apex/xx_selfService/";
            return host;
        };

        self.getAppHost = function () {
            var host = "/HHA-SSHR-BackEnd/";
            return host;
        };
        self.getprojectTaskNum='11.13.18.05/projectFinancialTasks';
        self.getprojectTasks='11.13.18.05/projectCommitments';
        self.getProjectDetails='projects';
        self.getProjectBudgetVersion='projectBudgets/';
        self.createPr="PrSoap/CreatPrReq";
        self.searchurl = 'purchaseRequisition/searchbyelement';
        self.getallRequrl = 'purchaseRequisition/getAllPR';
        self.updaterequest = 'purchaseRequisition/EDIT';
        self.Addrequest = 'purchaseRequisition/ADD';
        self.deletereq = 'purchaseRequisition/delete/';
        self.searchGenerate = 'Generate/searchGeneratePoPp/';
        self.updateGenerate = 'Generate/updateGeneratePoPp/';
        self.getAllGenerate = 'Generate/getAllGeneratePoPp/';
        self.reAssignGenerate = 'Generate/reAssignGeneratePoPp/';
        self.getAllFromStaging = 'Generate/getAllFromStaging/';
        self.updateGeneratePpNum = 'Generate/updateGeneratePpNum/';
        self.updateGeneratePoNum = 'Generate/updateGeneratePoNum/';
        self.getAllEmployessInfo = "latest/emps";
        self.generatePoNumber = "soap/soapRest";
        self.createNewInvoice = "11.13.18.05/invoices";
        self.getAllInvoice = "11.13.18.05/invoices?limit=500";
        self.getAllOrderNumber = "11.13.18.05/purchaseOrders?limit=500";
        self.insertProcurment="procurment/insert";
        self.getAllProcurment="procurment/procurmentPlan";
        self.costID = "masterData/getByCostType";
        self.insertOverHead = "overHead/insert";
        self.getAllRequest = "overHead/getByProjectId";
        self.services = 'services/getServiceByProjectId';
        self.systemCode = 'masterData/getByCostId';
        self.systemType = 'masterData/costIdDetails';
        self.addServices = 'services/newVersion';
        self.getCostCategory = 'masterData/getAllRequest';
        self.insertCostCategory = 'masterData/insert';
        self.uploadServiceToInstance = 'services/uploadServices';
        self.addOptions = 'optionsDes/insert';
        self.getoptions = 'optionsDes/getAllOptionsRequest';
        self.updateStatus = 'overHead/updateOverHeadStatus';

        self.REPORT_DATA_TYPE = {
            UDT: 'UDT', LOOKUP: 'LOOKUP', TERRITORIES: 'TERRITORIES'
        };

        self.FUSE_SERVICE_PARAM = {
            BINDLOOKUPTYPE: self.BTRIP_TYPE + ',' + self.YES_NO + ',' + self.ADV_MONTHS + ',' + self.SA_BANKS + ',' + self.BTRIPDRIVER_TYPE + ',' + self.BTRIPDRIVER_AREA + ',' + self.HRC_YES_NO + ',' + self.NADEC_HR_ID_MAIL_TYPE + ',' + self.NADEC_HR_IDENTIFICATION_LANG + ',' + self.HR_TICKETS_ROUTES + ',' + self.HR_TICKETS_REASONS + ',' + self.HR_TICKETS_CLASS + ',' + self.HR_GRIEVANCE_TYPE + ',' + self.HR_GRIEVANCE_STATUS + ',' + self.HR_MGR_GRIEVANCE_STATUS + ',' + self.HR_HEAD_GRIEVANCE_STATUS + ',' + self.EDU_YEARS + ',' + self.EDU_SEMESTER + ',' + self.HR_ALLOWANCES + ',' + self.NADEC_BTR_ROUTE_TYPE + ',' + self.HR_HEAD_HR_GRIEVANCE_STATUS + ',' + self.NADEC_CAR_INSIDE_LOV + ',' + self.HR_CAR_LOCATION + ',' + self.NADEC_TICKET_RAUTES_NEW_US + ',' + self.HR_NADEC_HOUSING_PERIOD + ',' + self.NADEC_TICKET_REFUND + ',' + self.NADEC_TRIP_DIRECTION_1 + ',' + self.NADEC_TICKET_RAUTES_NEW_US,
            P_TABLE_NAME: 'XXX_HR_REG_BTRIP_DAYS_B,XXX_HR_TRAIN_BTRIP_DAYS_B,XXX_HR_REG_BTRIP_DAYS_A,XXX_HR_TRAIN_BTRIP_DAYS_A,XXX_HR_REG_BTRIP_PERDIEM,XXX_HR_TRAIN_BTRIP_PERDIEM,XXX_HR_REG_BTRIP_TICKET,XXX_HR_TRAIN_BTRIP_TICKET,XXX_HR_PART_OF_EOS_AMT,XXX_HR_GLOBAL_VALUES,XXX_HR_ALLOWANCES_DETAILS,' + self.NADEC_BTR_ROUTE_TYPE, BINDLANGUAGE: 'US'
        };

        self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };

        self.getInternalRest = function () {
            //MOE dEPLOYED Test URL 
            ///Fusion-SSHR-Backend-Jdev-dev
           // var host = "https://144.21.78.176:7002/Fusion-SSHR-Backend-Jdev-prod/resources/";
           var host = "http://127.0.0.1:7101/ELSEWEDY-PR-context-root-TEST/webapi/";
           //var host = "http://127.0.0.1:7101/ELSEWEDY-PR-context-root-TEST/webapi/";
          // var host = "https://144.21.83.54:7002/HHA-SSHR/webapi/";
         // var host = "https://144.21.78.176:7002/HHA-SSSHR-BackEnd/webapi/";
         //var host ="https://hhajcs-a562419.java.em2.oraclecloudapps.com/Fusion-SSHR-Backend-Jdev/rest/";
//         var host = "https://hhajcs-a562419.java.em2.oraclecloudapps.com/Fusion-SSHR-Backend-Jdev-prod/rest/";
        // var host = "https://hhajcs-a562419.java.em2.oraclecloudapps.com/Fusion-SSHR-Backend-Jdev-dev/rest/";
            return host;
        };

    }

    return new commonHelper();
});