/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * projectOverHead module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'xlsx', 'util/commonhelper', 'ojs/ojarraydataprovider',
    'ojs/ojdatetimepicker', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojdialog', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, x, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function projectOverHeadContentViewModel() {
        var self = this;
        self.projectValue = ko.observable();
        self.fCurrancyValue = ko.observable();
        self.fCurrancyValueVal = ko.observable();
        self.fCurrancyValueValPro = ko.observable();
        self.fRate = ko.observable();
        self.fRateVal = ko.observable(" 1.000 ");
        self.fRateValPro = ko.observable(" 1.000 ");
        self.AED = ko.observable();
        self.AEDVal = ko.observable();
        self.AEDValPro = ko.observable();
        self.costId = ko.observable();
        self.Details = ko.observable();
        self.Description = ko.observable();
        self.costDate = ko.observable();
        self.rules = ko.observable();
        self.amount = ko.observable();
        self.value = ko.observable();
        self.Rate = ko.observable();
        self.reSet = ko.observable();
        self.isDisabled = ko.observable(true);
        self.addDisable = ko.observable(false);
        self.rateDisabled = ko.observable(false);
        self.amountDisabled = ko.observable(false);
        self.placeholder = ko.observable();
        self.projectValueVal = ko.observable('');
        self.prcurmentValue = ko.observable();
        self.costIdVal = ko.observable();
        self.detailsVal = ko.observable();
        self.descriptionVal = ko.observable();
        self.rulesVal = ko.observable();
        self.rateVal = ko.observable(0);
        self.valueVal = ko.observable();
        self.amountVal = ko.observable();
        self.DateVal = ko.observable();
        self.Back = ko.observable();
        self.Add = ko.observable();
        self.countCost = ko.observable();
        self.columnArray2 = ko.observableArray();
        self.costIdArr = ko.observableArray();
        self.tableData = ko.observableArray([]);
        self.dataprovider2 = ko.observable();
        self.dataprovider2(new oj.ArrayDataProvider(self.tableData, {keyAttributes: 'projectId'}));
        self.tableData = ko.observableArray();
        self.groupValid = ko.observable();
        self.isRequired = ko.observable(true);
        self.projectNumberValue = ko.observable();
        self.projectVersionValue = ko.observable();
        self.selectedRule = ko.observable();
        self.max = ko.observable(0);
        self.min = ko.observable(0);
        self.step = ko.observable();
        self.bondStartDate = ko.observable();
        self.bondEndDate = ko.observable();
        self.bondRate = ko.observable();
        self.durationOfBond = ko.observable();
        self.durationOfBondVal = ko.observable();
        self.bondVal = ko.observable();
        self.bondStartDateVal = ko.observable();
        self.bondEndDateVal = ko.observable();
        self.bondDisable = ko.observable(true);
        self.bondRateDisable = ko.observable(true);
        self.duBondDisable = ko.observable(true);
        self.selectedDetail = ko.observable();
        self.PlanningElementId=ko.observable();
        self.uploadOverDisabled=ko.observable(true);
        self.rowId =ko.observable();
        self.deleteDisabled=ko.observable(true);
        self.totalOverHead=ko.observable("0.00");
        self.total=ko.observable();
          self.selectedPlanningElementId=ko.observable();
            var Rules = [{label: "Receivable collection bank fee's",
                value: "Receivable collection bank fee's",
                rule: "0.10%-to-0.50% from Project Value"},          
            {label: "PB Fee's",
                value: " PB Fee's",
                rule: " 1.25%-to-1.75% from bond Value per annum"},
             {label: "Procuerment payment  bank fee's",
                value: "Procuerment payment  bank fee's",
                rule: "0.10%-to-0.50% from Procurement Value"
            },
            {label: "Letter of credit & TR bank Fee's",
                value: "Letter of credit & TR bank Fee's",
                rule: "0.0625%/p.m from financed procuerments"},
            {label: "Letter of credit & TR bank Interest's",
                value: "Letter of credit & TR bank Interest's",
                rule: "8.50% per annum "},
            {label: "Special Limit project Facility fee's",
                value: "Special Limit project Facility fee's",
                rule: " "}];
        self.rulesArray = ko.observableArray(Rules);

        self.connected = function () {
           
            var myData = oj.Router.rootInstance.retrieve();
            if (myData) {
                self.projectNumberValue(myData.projectID);
                self.projectVersionValue(myData.versionID);
                
                self.getCostID();
                self.getAllData();              
                getDataDB();
              getAllServices();
            }
        };

        function getAllServices()
        {
            app.loading(true);

            var payload = {
                "projectId": self.projectNumberValue().toString(),
                "versionId": self.projectVersionValue().toString()
            };
            var successEMPCbFn = function (data) {
                app.loading(false);
                var countCost = 0;
                for (var i = 0; i < data.length; i++) {
                    if (typeof (data[i].costs) == "undefined") {
                    } else {
                        countCost = parseInt(data[i].costs.replace(",", '')) + countCost;
                    }
                }
                self.countCost(countCost);
                 var sucess = function (data)
            {
                var Mydata = data.data;
                var cost = [...new Set(Mydata.map(el => el.COST_ID))];
                var totalPriceAED;
                cost.forEach((costs, i) => {
                    var total = Mydata.filter(el => el.COST_ID == cost[i])
                    totalPriceAED = total.map(el => el.TOTAL_COST_AED_DUBAI.replace(/,/g, "")).filter(e => e && e != "null").reduce((p, c) => ((p || 0) - 0) + ((c || 0) - 0));
                });
                var totalProjectValueCalc = parseFloat(totalPriceAED) + parseFloat(self.countCost());              
                self.fCurrancyValueVal(numberWithCommas(totalProjectValueCalc.toFixed(2)));
                self.AEDVal(numberWithCommas(totalProjectValueCalc.toFixed(2)));
            };
            var failcbfn = function (data) {

            };
            services.getGeneric("versions/view/" + self.projectNumberValue() + "/" + self.projectVersionValue()).then(sucess, failcbfn);
                
            };
            var failEMPCbFn = function () {
                app.loading(false);
            };
            services.postGeneric(commonhelper.services, payload).then(successEMPCbFn, failEMPCbFn);
        }

        self.getCostID = function ()
        {
            var getSuccss = function (data) {
                $.each(data, function (index) {
                    self.costIdArr.push({
                        label: data[index].costId,
                        value: data[index].costId,
                        description: data[index].description
                    });
                });
            };
            var projectCostFail = function () {

            };
            var payload = {

                "costType": "Overhead"
            };
            services.addGeneric(commonhelper.costID, payload).then(getSuccss, projectCostFail);
        };

        self.getAllData = function ()
        {
            var payload = {
                "versionId": self.projectVersionValue().toString(),
                "projectId": self.projectNumberValue().toString()
            };

            var getSuccss = function (data) {
                self.tableData([]);
                $.each(data, function (index) {
                    self.tableData.push({
                        costId: data[index].costId,
                        detalis: data[index].detalis,
                        rules: data[index].rules,
                        rate: data[index].rate,
                        amount: data[index].amount,
                        value: data[index].value,
                        costDate: data[index].costDate,
                        description: data[index].description,
                        bondRate: data[index].bondRate,
                        durationofBond: data[index].bondDuration,
                        bondStartDate: data[index].bondStart,
                        bondEndDate: data[index].bondEnd,
                        planningElementID:data[index].planningElementId,
                        id:data[index].id
                    });
                });
                self.dataprovider2(new oj.ArrayDataProvider(self.tableData, {keyAttributes: 'projectId'}));
                //  self.uploadOverDisabled
                
                if(self.tableData().length > 0){
                    self.uploadOverDisabled(false);                    
                }else{
                 self.uploadOverDisabled(true);   
                }
               // var system = self.tableData().filter(e => e.costId == sys[i])
                var allAmount = self.tableData().map(ee => (ee.value | ee.amount)).reduce((p, c) => (p || 0) + (c)||0);          
                self.totalOverHead(allAmount)
            };
            var projectCostFail = function () {

            };
            services.addGeneric(commonhelper.getAllRequest, payload).then(getSuccss, projectCostFail);
        };

        var getDataDB = function (foundFunc)
        {
            var payload = {
                "projectId": self.projectNumberValue().toString(),
                "versionId": self.projectVersionValue().toString()
            };

            var successEMPCbFn = function (data) {
                var countCost = 0;
                for (var i = 0; i < data.data.length; i++) {
                    countCost += parseFloat(data.data[i].afterDistotalCostAED);
                }
                self.fCurrancyValueValPro(numberWithCommas(countCost));
                self.AEDValPro(numberWithCommas(countCost));
            };

            var failEMPCbFn = function () {
            };
            services.postGeneric(commonhelper.getAllProcurment, payload).then(successEMPCbFn, failEMPCbFn);
        };
    
        self.addToTable = function ()
        {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            addToDBTable()
//             app.loading(true);
//            var payload2 = {};
//            payload2.planVersionId = self.projectVersionValue();
//            payload2.data = {
//                "TaskNumber": self.costIdVal(),
//                "ResourceName": "Over Heads",
//                "PlanningAmounts": [
//                    {
//                        "Currency": "AED",
//                        "RawCostAmounts": self.amountVal() == 0 ? self.valueVal() : self.amountVal(),
//                            "RevenueAmounts": self.amountVal() == 0 ? self.valueVal() : self.amountVal()
//                    }
//                ]
//            };
//            var successEMPCbFn = function (data) {
//                if (data[0].status == "error") {
//                    app.loading(false);
//                    app.messagesDataProvider.push({severity: "error", summary:data[0].data.toString(), autoTimeout: 0});
//                    return;
//                } else {
//                    console.log(data)
//                    app.loading(false);
//                    self.PlanningElementId(data[0].data.PlanningElementId)
//                    addToDBTable();
//                }
//            };
//            var failEMPCbFn = function (response) {
//            }
//            services.postGeneric(commonhelper.uploadServiceToInstance, [payload2]).then(successEMPCbFn, failEMPCbFn);
        };
        var addToDBTable = function () {
            app.loading(true)
            var payload2 = {
                "costId": self.costIdVal(),
                "detalis": self.detailsVal(),
                "rules": self.rulesVal(),
                "rate": self.rateVal(),
                "amount": self.amountVal(),
                "value": self.valueVal(),
                "projectId": self.projectNumberValue(),
                "versionId": self.projectVersionValue(),
                "costDate": self.DateVal(),
                "description": self.descriptionVal(),
                "bondRate": self.bondVal(),
                "bondDuration": self.durationOfBondVal(),
                "bondStart": self.bondStartDateVal(),
                "bondEnd": self.bondEndDateVal(),
                //"planningElementId": self.PlanningElementId().toString()    
            };

            var getSuccss = function (data) {
                self.getAllData();
                self.addDisable(true);
                app.messagesDataProvider.push({severity: "confirmation", summary: "Request Added successfully", autoTimeout: 1000});
                app.loading(false);
            };

            var failCbFn = function () {
                app.messagesDataProvider.push({severity: "error", summary: "Request Failed", autoTimeout: 1000});
                app.loading(false);
            };
            services.addGeneric(commonhelper.insertOverHead, payload2).then(getSuccss, failCbFn);
            self.reSetFields();
        }
        self.detailsValueChanged = function (event)
        {
            self.selectedDetail(event.detail.value);
            if (!self.detailsVal() || self.detailsVal().length == 0)
                return;
            self.amountVal(0);
            for (var i = 0; i < Rules.length; i++) {
                if (event.detail.value == Rules[i].value) {
                    self.rulesVal(Rules[i].rule);
                    self.selectedRule(Rules[i].rule);
                }
            }
            if (event.detail.value === "Receivable collection bank fee's") {
                self.min(.10);
                self.max(.50);
                self.step(.1);
                self.rateVal(self.min());
                self.rateDisabled(false);
            }
            if (event.detail.value === "Procuerment payment  bank fee's") {
                self.min(.10);
                self.max(.50);
                self.step(.1);
                self.rateVal(self.min());
                self.rateDisabled(false);
            }
            if (event.detail.value == " PB Fee's") {
                self.min(1.25);
                self.max(1.75);
                self.step(0.1);
                self.rateVal(self.min());
                self.rateDisabled(false);
                self.bondDisable(false);
                self.bondRateDisable(false);
            } else {
                self.bondDisable(true);
                self.bondRateDisable(true);
                self.bondStartDateVal('');
                self.bondEndDateVal('');
                self.bondVal(0);
            }
            if (event.detail.value == "Letter of credit & TR bank Fee's") {
                self.min(.75);
                self.max(.75);
                self.rateVal(.75);
                self.rateDisabled(true);
            }
            if (event.detail.value == "Letter of credit & TR bank Interest's") {
                self.rateDisabled(true);
                self.rateVal(8.50);
            } else if (event.detail.value == "Special Limit project Facility fee's") {
                self.rateDisabled(false);
                self.bondRateDisable(false);
                self.min(1);
                self.max(100);
                self.step(1);
                self.rateVal(0);
            }
        };

        self.amountValueChanged = function (event) {
            if (event.detail.value > 0) {
                self.rateDisabled(true);
                self.rateVal(0);
                self.valueVal(0);
                self.bondStartDateVal('');
                self.bondEndDateVal('');
                self.durationOfBondVal('');
                self.bondDisable(true);
                self.bondRateDisable(true);
                self.bondVal(0);
            } else if (self.selectedDetail() && self.selectedDetail().includes(" PB Fee's")) {
                self.bondDisable(false);
                self.bondRateDisable(false);
                self.rateDisabled(false);
            } else {
                self.rateDisabled(false);
            }
        };

        self.calcDuration = function () {
            if (self.bondStartDateVal() && self.bondEndDateVal()) {
                var duration = DaysBetween(new Date(self.bondStartDateVal()), new Date(self.bondEndDateVal()));
                return  self.durationOfBondVal((duration / 365).toFixed(2));
            }
        };

        self.costValueChanged = function (event) {
            for (var i = 0; i < self.costIdArr().length; i++) {
                if (event.detail.value == self.costIdArr()[i].value) {
                    self.descriptionVal(self.costIdArr()[i].description);
                }
            }
            if (event.detail.value != null) {
                self.addDisable(false);
            }
        };

        function DaysBetween(StartDate, EndDate) {
            const oneDay = 1000 * 60 * 60 * 24;
            const start = Date.UTC(EndDate.getFullYear(), EndDate.getMonth(), EndDate.getDate());
            const end = Date.UTC(StartDate.getFullYear(), StartDate.getMonth(), StartDate.getDate());
            return (start - end) / oneDay;
        }

        self.numberValueChanged = function (event) {
            self.doClc();
        };

        self.tableSelectionListener2 = function (event) {
            
              var data = event.detail;
                var currentRow = data.currentRow;
                     if (!currentRow)
                    return;
                    var row = self.tableData()[currentRow['rowIndex']];
                    self.selectedPlanningElementId(row.planningElementID);
                    self.rowId (row.id);
                   self.deleteDisabled(false);
            
        };
        self.DeleteFields = function(){
               removeOverHead();
        };
        
        var removeOverHead = function () {
                 app.loading(true);
                var deleteSucess = function () {
                    app.messagesDataProvider.push({severity: "info", summary: "Record Deleted", autoTimeout: 0});
                    self.getAllData();
                     self.deleteDisabled(true);
                     app.loading(false);
                };
                services.getGeneric('overHead/deleteDataBaseOverHead/' + self.rowId().toString()).then(deleteSucess);
            };
       
        self.bondEndDateChangeListener = function () {
            self.calcDuration();
        };

        self.bondStartDateChangeListener = function () {
            self.calcDuration();
        };

        self.bondRateChangedListener = function () {
            self.doClc();
        };

        self.durationOFChangeListener = function () {
            self.doClc();
        };

        self.BackBtnAction = function () {
            oj.Router.rootInstance.go('procurmentPlan');
        };

        self.reSetFields = function () {
            self.costIdVal('');
            self.descriptionVal('');
            self.amountVal(0);
            self.rulesVal('');
            self.DateVal('');
            self.placeholder("Please Select");
            self.detailsVal('');
            self.rateVal(parseFloat(0));
            self.doClc();
            self.bondStartDateVal('');
            self.bondEndDateVal('');
            self.durationOfBondVal('');
            self.bondVal(0);
        };

        self.doClc = function () {
            var val = 0;
            if (self.selectedRule() && self.selectedRule().includes("Project Value")) {

                val = (parseFloat(self.rateVal()) * parseFloat(self.fCurrancyValueVal().replace(/,/g,''))) / 100;
            } else if (self.selectedRule() && self.selectedRule().includes("Procurement Value")) {
                val = (parseFloat(self.rateVal()) * parseFloat(self.fCurrancyValueValPro().replace(/,/g,''))) / 100;
            } else if (self.durationOfBondVal() && self.rateVal() && self.bondVal() && self.selectedDetail() && self.selectedDetail().includes(" PB Fee's")) {
                val = parseFloat(self.fCurrancyValueVal().replace(/,/g,'')) * parseFloat((self.bondVal() / 100)) * parseFloat(self.rateVal()) * parseFloat(self.durationOfBondVal())
            } else if (self.rateVal() && self.bondVal() && self.selectedDetail() && self.selectedDetail().includes("Special Limit project Facility fee's")) {
                val = parseFloat(self.fCurrancyValueVal().replace(/,/g,'')) * parseFloat((self.bondVal() / 100)) * parseFloat(self.rateVal());
            } else {
                val = (parseFloat(self.rateVal()) * parseFloat(self.fCurrancyValueVal().replace(/,/g,''))) / 100;
            }
            self.valueVal(parseFloat(val.toFixed(1)));
        };


        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        var getTranslation = oj.Translations.getTranslatedString;
        function initTranslations() {

            self.costId(getTranslation("reports.costId"));
            self.Details(getTranslation("reports.Details"));
            self.rules(getTranslation("reports.rules"));
            self.amount(getTranslation("reports.amount"));
            self.value(getTranslation("reports.value"));
            self.Rate(getTranslation("reports.Rate"));
            self.projectValue(getTranslation("reports.projectValue"));
            self.fCurrancyValue(getTranslation("reports.fCurrancyValue"));
            self.AED(getTranslation("reports.AED"));
            self.prcurmentValue(getTranslation("reports.prcurmentValue"));
            self.fRate(getTranslation("reports.fRate"));
            self.Back(getTranslation("reports.Back"));
            self.Add(getTranslation("reports.Add"));
            self.reSet(getTranslation("reports.reSet"));
            self.Description(getTranslation("reports.Description"));
            self.costDate(getTranslation("reports.costDate"));
            self.placeholder(getTranslation("reports.placeholder"));
            self.bondStartDate(getTranslation("reports.bondStartDate"));
            self.bondEndDate(getTranslation("reports.bondEndDate"));
            self.bondRate(getTranslation("reports.bondRate"));
            self.durationOfBond(getTranslation("reports.durationOfBond"));
            self.total(getTranslation("reports.total"));

            self.columnArray2([
                {
                    "headerText": self.costId(), "field": "costId"
                },
                {
                    "headerText": self.Details(), "field": "detalis"
                },
                {
                    "headerText": self.rules(), "field": "rules"
                },
                {
                    "headerText": self.Rate(), "field": "rate"
                },
                {
                    "headerText": self.amount(), "field": "amount"
                },
                {
                    "headerText": self.value(), "field": "value"
                },
                {
                    "headerText": self.costDate(), "field": "costDate"
                },
                {
                    "headerText": self.Description(), "field": "description"
                },
                {
                    "headerText": self.bondRate(), "field": "bondRate"
                },
                {
                    "headerText": self.durationOfBond(), "field": "durationofBond"
                },
                {
                    "headerText": self.bondStartDate(), "field": "bondStartDate"
                },
                {
                    "headerText": self.bondEndDate(), "field": "bondEndDate"
                },
            ]);
        }
        initTranslations();
    }

    return projectOverHeadContentViewModel;
});
