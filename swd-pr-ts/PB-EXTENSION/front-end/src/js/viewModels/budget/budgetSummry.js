define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojarraydataprovider','ojs/ojformlayout', 'ojs/ojlabel', 'ojs/ojlabel', 'ojs/ojdatetimepicker', 'ojs/ojbutton',
    'ojs/ojtable', 'ojs/ojcheckboxset', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojoffcanvas', 'xlsx', 'ojs/ojfilepicker', 'ojs/ojpopup', 'knockout', 'promise', 'ojs/ojchart',
    'ojs/ojdatagrid', 'ojs/ojarraydatagriddatasource', 'ojs/ojradioset'],
    function (oj, ko, $, app, services, commonhelper) {

        function budgetSummryContentViewModel() {
            var self = this;

            // #region observables
            self.upload = ko.observable('Uload');
            self.preview = ko.observable('Preview');
            self.download = ko.observable('Download');
            self.payload = ko.observableArray([]);
            self.selected_files = ko.observableArray([]);
            self.resourceToBeUpdated = ko.observable({});
            self.MainLbl = ko.observable();
            self.lastVerRecord = ko.observable();
            self.reportBtnDisabled = ko.observable(true);
            self.payloadArr = ko.observableArray([]);
            self.projectsNameArr = ko.observableArray([]);
            self.projectsNumArr = ko.observableArray([]);
            self.projects = ko.observableArray([]);
            self.projectsSearchResult = ko.observableArray([]);
            self.searchDisable = ko.observable(false);
            self.searchLbl = ko.observable();;
            self.CancelLineLbl = ko.observable();
            self.GenePrLbl = ko.observable();
            self.VersionsColumnArray = ko.observableArray();
            self.generateColumnArray = ko.observableArray();
            self.columnArray = ko.observableArray();
            self.BomLnum = ko.observable();
            self.projectStartDate = ko.observable();
            self.projectEndDate = ko.observable();
            self.projName = ko.observable();
            self.LineDesc = ko.observable();
            self.itemCode = ko.observable();
            self.PrOrder = ko.observable();
            self.CurrrentQty = ko.observable();
            self.totalQty = ko.observable();
            self.UOM = ko.observable();
            self.itmCost = ko.observable();
            self.ProjNum = ko.observable();
            self.versionName = ko.observable();
            self.versionStatus = ko.observable();
            self.planType = ko.observable();
            self.revenueAmount = ko.observable();
            self.costAmount = ko.observable();
            self.VersionsArr = ko.observableArray([]);
            self.linesArr = ko.observableArray([]);
            self.projnumVal = ko.observable();
            self.rowData = ko.observable();
            self.RowPlanVersionId = ko.observable();
            self.exportLbl = ko.observable();
            self.dataprovider = new oj.ArrayDataProvider(self.projectsSearchResult, { keyAttributes: 'projectId' });
            self.VersionsArrDataprovider = new oj.ArrayDataProvider(self.VersionsArr, { keyAttributes: 'planVersionId' });
            self.linesArrDataprovider = new oj.ArrayDataProvider(self.linesArr, { keyAttributes: 'PlanningElementId' });

            self.UOMAtrr = ko.observableArray();
            self.currProjName = ko.observable();
            self.linesColumnArray = ko.observableArray();
            self.projectNameVal = ko.observable();
            self.projectNameLbl = ko.observable();
            self.projectNumberLbl = ko.observable();
            self.projectManagerLbl = ko.observable();
            self.projectStatusLbl = ko.observable();
            self.newVersionNameVal = ko.observable();
            self.projectId = ko.observable();
            self.newVersionPlanNameVal = ko.observable('Cost and Revenue Budget');
            self.newVersionStatusVal = ko.observable('Current Working');
            self.versionPlanNames = ko.observableArray([]);
            self.versionPlanStatus = ko.observableArray([{ label: 'Current Working', value: 'Current Working' }]);
            self.versionsNamesOptions = ko.observableArray([]);
            self.versionsNamesOptionsDp = new oj.ArrayDataProvider(self.versionsNamesOptions, { keyAttributes: 'value' });
            self.dataProviderAttachment = new oj.ArrayDataProvider(self.selected_files, {keyAttributes: 'id'});
            self.selectedItemsAttachment = ko.observableArray([]);
            self.selectedProject = ko.observable({});
            self.selectedVersion = ko.observable({});
            self.isUpdateProjectPlan = ko.observableArray([]);
            self.isUpdateSelectedVersion = ko.observableArray([]);
                self.showBudgetTableLbl = ko.observable();
                self.exportBudgetSheetLbl = ko.observable();
                self.deleteVersionLbl = ko.observable();
                self.reportsLbl = ko.observable();
                self.costCategoryLbl = ko.observable();
                self.dyCostId = ko.observableArray();
                self.costCategoryLbl=ko.observable('Cost Category');
                self.projetNumber = ko.observable();
                 self.versionName = ko.observable();
            var model = {
                "FinancialPlanType": ko.observable(),
                "PlanVersionName": ko.observable(),
                "BudgetCreationMethod": "MANUAL",
                "PlanVersionStatus": ko.observable(),
                "ProjectName": ko.observable(),
                "PlanningResources": ko.observableArray([])
            };
            //#endregion

            self.picker_text = ko.observable(app.translate("common.click_or_dragdrop"));
            self.acceptArr = ko.observableArray([]);
            self.acceptArr = ko.pureComputed(function () {
                var accept = ".xls,.xlsx";
                return accept ? accept.split(",") : [];
            }, self);

            self.removePickerBtnAction = function () {
                self.selected_files([]);
                document.querySelector("input[type='file']").value = '';
            };

            function getSelectedRow(tableId, arr) {
                if (!arr || !arr.length)
                    return null;
                var element = document.getElementById(tableId);
                if (!element)
                    return null;
                var currentRow = element.currentRow;
                if (!currentRow)
                    return null;
                var index = currentRow['rowIndex'];
                if (index > arr.length - 1)
                    return null;
                return arr[index];
            }

            self.updateselectedversioncheckboxchangelistener = function (e) {
                if (e.detail.previousValue.length == 0 && !self.selectedVersion().planVersionName) {
                    app.messagesDataProvider.push({ severity: "error", summary: "Please select a version to update", autoTimeout: 0 });
                    setTimeout(() => {
                        self.isUpdateSelectedVersion([]);
                    }, 100);
                }
            };

            self.showBudgetTableBtn = function () {
                if (!self.selectedVersion().planVersionId) {
                    app.messagesDataProvider.push({ severity: "error", summary: "Please select a version to display data", autoTimeout: 0 });
                    return;
                }
                var StoredData={
                    "projectID": self.selectedProject().projectId,
                    "versionID": self.selectedVersion().planVersionId,
                    "projectStartDate":self.projectsSearchResult()[0].projectStartDate
                };
                oj.Router.rootInstance.store(StoredData);
                oj.Router.rootInstance.go('budgetSheetTable');
            };

            self.exportBtnAction = function (e) {
                var row = getSelectedRow("versionsTable", self.VersionsArr());
                if (row) {
                    app.loading(true);
                    services.getGeneric("versions/export/" + self.selectedProject().projectId + "/" + row.planVersionId)
                        .then(excelData => {
                            try {
                                excelData = JSON.parse(excelData);
                            } catch (e) { }
                            app.loading(false);
                            if (excelData.status == 'ERROR') {
                                app.messagesDataProvider.push({ severity: "error", summary: "Error while exporting the Excel Sheet,\n" + excelData.data, autoTimeout: 0 });
                            } else {
                                var href = 'data:application/vnd.ms-excel;base64,' + excelData.data;
                                var link = document.createElement("a");
                                document.body.appendChild(link);
                                link.setAttribute("type", "hidden");
                                link.href = "data:text/plain;base64," + excelData.data;
                                link.download = "book1.xls";
                                link.click();
                                document.body.removeChild(link);
                                saveExcel(excelData);
                            }

                        }, error => {
                            app.loading(false);
                        });
                } else {
                    app.messagesDataProvider.push({ severity: "error", summary: "Please select a version to export", autoTimeout: 0 });

                }
            };

            // function saveExcel(data) {
            //     try {
            //         data = JSON.parse(data);
            //     } catch (e) { }
            //     if (data && data.status == 'OK') {
            //         var arr = data.data;

            //         if (arr && !arr.length) {
            //             try {
            //                 arr = JSON.parse(arr);
            //             } catch (e) { }
            //         }
            //         if (arr.length > 0) {
            //             var headerMappedWithBody = [{ "label": "Pos", "dbkey": "POS" }, { "label": "refrence", "dbkey": "REFERENCE" }, { "label": "costId", "dbkey": "COST_ID" }, { "label": "resourceName", "dbkey": "RESOURCE_NAME" }, { "label": "partNo", "dbkey": "PART_NO" }, { "label": "supplier", "dbkey": "SUPPLIER" }, { "label": "manufacturer", "dbkey": "MANUFACTURER" }, { "label": "startDate", "dbkey": "START_DATE" }, { "label": "finishDate", "dbkey": "FINISH_DATE" }, { "label": "desciption", "dbkey": "DESCRIPTION" },
            //             { "label": "site1", "dbkey": "SITE_1" }, { "label": "site2", "dbkey": "SITE_2" }, { "label": "site3", "dbkey": "SITE_3" }, { "label": "site4", "dbkey": "SITE_4" }, { "label": "site5", "dbkey": "SITE_5" }, { "label": "site6", "dbkey": "SITE_6" }, { "label": "site7", "dbkey": "SITE_7" }, { "label": "site8", "dbkey": "SITE_8" }, { "label": "site9", "dbkey": "SITE_9" }, { "label": "site10", "dbkey": "SITE_10" }, { "label": "site11", "dbkey": "SITE_11" }, { "label": "site12", "dbkey": "SITE_12" }, { "label": "site13", "dbkey": "SITE_13" }, { "label": "site14", "dbkey": "SITE_14" }, { "label": "site15", "dbkey": "SITE_15" }, { "label": "site16", "dbkey": "SITE_16" }, { "label": "site17", "dbkey": "SITE_17" }, { "label": "site18", "dbkey": "SITE_18" }, { "label": "site19", "dbkey": "SITE_19" }, { "label": "site20", "dbkey": "SITE_20" }, { "label": "site21", "dbkey": "SITE_21" }, { "label": "site22", "dbkey": "SITE_22" }, { "label": "site23", "dbkey": "SITE_23" }, { "label": "site24", "dbkey": "SITE_24" }, { "label": "site25", "dbkey": "SITE_25" }, { "label": "site26", "dbkey": "SITE_26" }, { "label": "site27", "dbkey": "SITE_27" }, { "label": "site28", "dbkey": "SITE_28" }, { "label": "site29", "dbkey": "SITE_29" }, { "label": "site30", "dbkey": "SITE_30" }, { "label": "site31", "dbkey": "SITE_31" }, { "label": "site32", "dbkey": "SITE_32" }, { "label": "site33", "dbkey": "SITE_33" }, { "label": "site34", "dbkey": "SITE_34" }, { "label": "site35", "dbkey": "SITE_35" }, { "label": "site36", "dbkey": "SITE_36" }, { "label": "site37", "dbkey": "SITE_37" }, { "label": "site38", "dbkey": "SITE_38" }, { "label": "site39", "dbkey": "SITE_39" }, { "label": "site40", "dbkey": "SITE_40" }, { "label": "site41", "dbkey": "SITE_41" }, { "label": "site42", "dbkey": "SITE_42" }, { "label": "site43", "dbkey": "SITE_43" }, { "label": "site44", "dbkey": "SITE_44" }, { "label": "site45", "dbkey": "SITE_45" }, { "label": "site46", "dbkey": "SITE_46" }, { "label": "site47", "dbkey": "SITE_47" }, { "label": "site48", "dbkey": "SITE_48" }, { "label": "site49", "dbkey": "SITE_49" }, { "label": "site50", "dbkey": "SITE_50" }, { "label": "site51", "dbkey": "SITE_51" }, { "label": "site52", "dbkey": "SITE_52" }, { "label": "site53", "dbkey": "SITE_53" }, { "label": "site54", "dbkey": "SITE_54" }, { "label": "site55", "dbkey": "SITE_55" }, { "label": "site56", "dbkey": "SITE_56" }, { "label": "site57", "dbkey": "SITE_57" }, { "label": "site58", "dbkey": "SITE_58" }, { "label": "site59", "dbkey": "SITE_59" }, { "label": "site60", "dbkey": "SITE_60" }, { "label": "site61", "dbkey": "SITE_61" }, { "label": "site62", "dbkey": "SITE_62" }, { "label": "site63", "dbkey": "SITE_63" }, { "label": "site64", "dbkey": "SITE_64" }, { "label": "site65", "dbkey": "SITE_65" }, { "label": "site66", "dbkey": "SITE_66" }, { "label": "site67", "dbkey": "SITE_67" }, { "label": "site68", "dbkey": "SITE_68" }, { "label": "site69", "dbkey": "SITE_69" }, { "label": "site70", "dbkey": "SITE_70" },
            //             { "label": "unitOfMeasure", "dbkey": "UNIT_OF_MEASURE" }, { "label": "totalQtyHours", "dbkey": "TOTAL_QTY" }, { "label": "BudgetCurrencyAed", "dbkey": "BUDGET_CURRENCY_AED" }, { "label": "foreignCurrency", "dbkey": "FOREIGN_CURRENCY" }, { "label": "conversionRate", "dbkey": "CONVERSION_RATE" }, { "label": "lastBudgetPrice", "dbkey": "LAST_BUDGET_PRICE" }, { "label": "lastBudgetPriceCurrency", "dbkey": "LAST_BUDGET_PRICE_CURRENCY" }, { "label": "unitPriceRate", "dbkey": "UNIT_PRICE" }, { "label": "unitCostAed", "dbkey": "UNIT_COST_AED" }, { "label": "totalCostAed", "dbkey": "TOTAL_COST_AED" }, { label: '', dbkey: '' }, { "label": "lastStandardDiscount", "dbkey": "LAST_STANDARD_DISCOUNT" }, { "label": "standardDiscount", "dbkey": "STANDARD_DISCOUNT" }, { "label": "specialDiscount", "dbkey": "SPECIAL_DISCOUNT" }, { "label": "unitCostAedDiscounted", "dbkey": "UNIT_COST_AED_DISCOUNTED" }, { "label": "totalCostAedDiscounted", "dbkey": "TOTAL_COST_AED_DISCOUNTED" }, { "label": "gpEquipment", "dbkey": "GP_EQUIPMENT" }, { "label": "fixedOverhead", "dbkey": "FIXED_OVERHEAD" }, { "label": "variableOverhead", "dbkey": "VARIABLE_OVERHEAD" }, { "label": "unitCostAedDubai", "dbkey": "UNIT_COST_AED_DUBAI" }, { "label": "summaryTotalPriceAed", "dbkey": "SUMMARY_TOTAL_PRICE_AED" }, { "label": "unitPriceFc", "dbkey": "UNIT_PRICE_FC" }, { "label": "totalPriceFc", "dbkey": "TOTAL_PRICE_FC" }, { "label": "summaryTotalPrice", "dbkey": "SUMMARY_TOTAL_PRICE" }];
            //             var header = headerMappedWithBody.map(e => e.label);
            //             var body = [];
            //             for (x = 0; x < data.data.length; x++) {
            //                 var dbRow = data.data[x];
            //                 var bodyObj = [];
            //                 for (y = 0; y < headerMappedWithBody.length; y++) {
            //                     var mapCol = headerMappedWithBody[y];
            //                     var cell = mapCol ? dbRow[mapCol.dbkey] || "" : "";

            //                     //insert equation instead of values (equations printed as string)
            //                     // if (mapCol.dbkey == 'TOTAL_COST_AED')
            //                     //     bodyObj.push(`=CK${x + 2}*CD${x + 2}`);
            //                     // else if (mapCol.dbkey == 'UNIT_COST_AED_DISCOUNTED')
            //                     //     bodyObj.push(`=CK${x + 2}*(1-CO${x + 2}-CP${x + 2})`);
            //                     // else if (mapCol.dbkey == 'TOTAL_COST_AED_DISCOUNTED')
            //                     //     bodyObj.push(`=CD${x + 2}*CQ${x + 2}`);
            //                     // else if (mapCol.dbkey == 'UNIT_COST_AED_DUBAI')
            //                     //     bodyObj.push(`=CQ${x + 2}/(1-SUM(CS${x + 2}:CU${x + 2}))`);
            //                     // else if (mapCol.dbkey == 'TOTAL_PRICE_FC')
            //                     //     bodyObj.push(`=CY${x + 2}*CD${x + 2}`);
            //                     // else
            //                     bodyObj.push(cell);
            //                 }
            //                 body.push(bodyObj)
            //             }

            //             //save to excel
            //             var sheetData = [];//init rows

            //             sheetData.push([]);//1
            //             sheetData.push([]);//2
            //             sheetData.push(["Project", self.selectedProject().projectName]);//3
            //             sheetData.push(["Customer", ""]);//4
            //             sheetData.push(["Quote No.", ""]);//5
            //             sheetData.push(["Date:", new Date().toJSON().slice(0, 10).replace(/-/g, '-')]);//6
            //             sheetData.push([]);//7
            //             sheetData.push(header);//8 ->header
            //             sheetData.push([]);//9
            //             sheetData.push([]);//10 -> task info //todo
            //             sheetData.push(...body);//11 -> start of body

            //             var filename = "book1";
            //             const book = XLSX.utils.book_new();
            //             const sheet = XLSX.utils.aoa_to_sheet(sheetData);
            //             // sheet.I11.t = 'd';
            //             // sheet.I11.s = {fill:{fgColor: {rgb:"86BC25"}}};
            //             XLSX.utils.book_append_sheet(book, sheet, 'book1');
            //             XLSX.writeFile(book, `${filename}.xls`);

            //         } else {
            //             app.messagesDataProvider.push({ severity: "error", summary: "No data to export", autoTimeout: 0 });
            //         }
            //     } else {
            //         app.infomMsg(data.data);
            //     }
            // }


                self.selectListener = function (event) {

                    var files = event.detail.files;
                    for (var b = 0; b< files.length; b++) {
                        self.selected_files.push({
                            "name": files[b].name
                        });
                        var reader = new FileReader();

                        app.loading(true);
                        reader.onload = function (e) {
                            var data = e.target.result;
                            var workbook = XLSX.read(data, {type: 'binary'});
                            var sheet = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
                            app.loading(false);

                        //    self.payload([]);
                            var data = [];
                            var startReading = false;
                            for (x = 0; x < sheet.length; x++) {
                        var o = sheet[x];
                        var objToPushed = {};
                        

                        if (workbook.SheetNames[1] == 'metadata') {//exported
                            objToPushed = o;
                        } else {
                            //client sheet (__EMPTY exist only on client sheet)
                            if (!startReading && o.__EMPTY && o.__EMPTY.toLowerCase() == 'pos') {
                                startReading = true;
                                continue;
                            }
                            //client sheet (__EMPTY exist only on client sheet)
                            if (!o.__EMPTY_2) {//cost id
                                continue;
                            }

                            if (!startReading)
                                continue;
                            objToPushed = {
                                Pos: parseFloat(o['__EMPTY'].replace(",", "")),//no
                                refrence: o['__EMPTY_1'] || "",
                                costId: o['__EMPTY_2'] || "",
                                resourceName: o['__EMPTY_3'] || "",
                                partNo: o['__EMPTY_4'] || "",
                                supplier: o['__EMPTY_5'] || "",
                                manufacturer: o['__EMPTY_6'] || "",
                                startDate: new Date(o['__EMPTY_7'] || ""),
                                finishDate: new Date(o['__EMPTY_8'] || ""),
                                desciption: o['__EMPTY_9'] || "",
                                site1: o['__EMPTY_10'] || "",
                                site2: o['__EMPTY_11'] || "",
                                site3: o['__EMPTY_12'] || "",
                                site4: o['__EMPTY_13'] || "",
                                site5: o['__EMPTY_14'] || "",
                                site6: o['__EMPTY_15'] || "",
                                site7: o['__EMPTY_16'] || "",
                                site8: o['__EMPTY_17'] || "",
                                site9: o['__EMPTY_18'] || "",
                                site10: o['__EMPTY_19'] || "",
                                site11: o['__EMPTY_20'] || "",
                                site12: o['__EMPTY_21'] || "",
                                site13: o['__EMPTY_22'] || "",
                                site14: o['__EMPTY_23'] || "",
                                site15: o['__EMPTY_24'] || "",
                                site16: o['__EMPTY_25'] || "",
                                site17: o['__EMPTY_26'] || "",
                                site18: o['__EMPTY_27'] || "",
                                site19: o['__EMPTY_28'] || "",
                                site20: o['__EMPTY_29'] || "",
                                site21: o['__EMPTY_30'] || "",
                                site22: o['__EMPTY_31'] || "",
                                site23: o['__EMPTY_32'] || "",
                                site24: o['__EMPTY_33'] || "",
                                site25: o['__EMPTY_34'] || "",
                                site26: o['__EMPTY_35'] || "",
                                site27: o['__EMPTY_36'] || "",
                                site28: o['__EMPTY_37'] || "",
                                site29: o['__EMPTY_38'] || "",
                                site30: o['__EMPTY_39'] || "",
                                site31: o['__EMPTY_40'] || "",
                                site32: o['__EMPTY_41'] || "",
                                site33: o['__EMPTY_42'] || "",
                                site34: o['__EMPTY_43'] || "",
                                site35: o['__EMPTY_44'] || "",
                                site36: o['__EMPTY_45'] || "",
                                site37: o['__EMPTY_46'] || "",
                                site38: o['__EMPTY_47'] || "",
                                site39: o['__EMPTY_48'] || "",
                                site40: o['__EMPTY_49'] || "",
                                site41: o['__EMPTY_50'] || "",
                                site42: o['__EMPTY_51'] || "",
                                site43: o['__EMPTY_52'] || "",
                                site44: o['__EMPTY_53'] || "",
                                site45: o['__EMPTY_54'] || "",
                                site46: o['__EMPTY_55'] || "",
                                site47: o['__EMPTY_56'] || "",
                                site48: o['__EMPTY_57'] || "",
                                site49: o['__EMPTY_58'] || "",
                                site50: o['__EMPTY_59'] || "",
                                site51: o['__EMPTY_60'] || "",
                                site52: o['__EMPTY_61'] || "",
                                site53: o['__EMPTY_62'] || "",
                                site54: o['__EMPTY_63'] || "",
                                site55: o['__EMPTY_64'] || "",
                                site56: o['__EMPTY_65'] || "",
                                site57: o['__EMPTY_66'] || "",
                                site58: o['__EMPTY_67'] || "",
                                site59: o['__EMPTY_68'] || "",
                                site60: o['__EMPTY_69'] || "",
                                site61: o['__EMPTY_70'] || "",
                                site62: o['__EMPTY_71'] || "",
                                site63: o['__EMPTY_72'] || "",
                                site64: o['__EMPTY_73'] || "",
                                site65: o['__EMPTY_74'] || "",
                                site66: o['__EMPTY_75'] || "",
                                site67: o['__EMPTY_76'] || "",
                                site68: o['__EMPTY_77'] || "",
                                site69: o['__EMPTY_78'] || "",
                                site70: o['__EMPTY_79'] || "",
                                unitOfMeasure: o['__EMPTY_80'] || "",
                                totalQtyHours: o['__EMPTY_81'] || "",
                                BudgetCurrencyAed: o['__EMPTY_82'] || "",
                                foreignCurrency: o['__EMPTY_83'] || "",
                                conversionRate: o['__EMPTY_84'] || "",
                                lastBudgetPrice: o['__EMPTY_85'] || "",
                                lastBudgetPriceCurrency: o['__EMPTY_86'] || "",
                                unitPriceRate: o['__EMPTY_87'] || "",
                                unitCostAed: parseFloat((o['__EMPTY_88'] || "0").replace(",", "")),
                                totalCostAed: parseFloat((o['__EMPTY_89'] || "0").replace(/,/g, "")),
                                lastStandardDiscount: parseFloat((o['__EMPTY_91'] || "0").replace(",", "")),
                                standardDiscount: (o['__EMPTY_92'] || "0").replace(",", ""),
                                specialDiscount: (o['__EMPTY_93'] || "0").replace(",", ""),
                                unitCostAedDiscounted: parseFloat((o['__EMPTY_94'] || "0").replace(",", "")),
                                totalCostAedDiscounted: parseFloat((o['__EMPTY_95'] || "0").replace(/,/g, "")),
                                gpEquipment: (o['__EMPTY_96'] || "0").replace(",", ""),
                                fixedOverhead:  parseFloat((o['__EMPTY_97'] || "0").replace(",", "")),
                                variableOverhead:  parseFloat((o['__EMPTY_98'] || "0").replace(",", "")),
                                unitCostAedDubai:  parseFloat((o['__EMPTY_99'] || "0").replace(",", "")),
                                totalCostAedDubai: parseFloat((o['__EMPTY_100'] || "0").replace(/,/g, "")),
                                summaryTotalPriceAed: parseFloat((o['__EMPTY_101'] || "0").replace(",", "")),
//                                unitPriceFc: parseFloat((o['__EMPTY_102'] || "0").replace(",", "")),
//                                totalPriceFc: parseFloat((o['__EMPTY_103'] || "0").replace(",", "")),
                                unitPriceAED: parseFloat((o['__EMPTY_102'] || "0").replace(",", "")),
                                totalPriceAED: parseFloat((o['__EMPTY_103'] || "0").replace(",", "")),
                                make: (o['__EMPTY_104'] || "0").replace(",", ""),
                                currency: (o['__EMPTY_105'] || "0").replace(",", ""),
                                summaryTotalPriceAed: parseFloat((o['__EMPTY_106'] || "0").replace(",", ""))
                            };
                        }

                        Object.keys(objToPushed).forEach(key => {
                            if (key.length == 0)
                                delete objToPushed[key];

                            if (objToPushed[key] && typeof objToPushed[key] == 'string') {
                                objToPushed[key] = objToPushed[key].trim();

                                if (objToPushed[key].includes('%')) {
                                    objToPushed[key] = objToPushed[key].replace('%', '');
                                    try {
                                        objToPushed[key] = parseInt(objToPushed[key]) / 100;
                                    } catch (e) { }
                                }
                            }
                        });

                        if (objToPushed.Pos && objToPushed.Pos != NaN)
                            data.push(objToPushed);
                    }
                    self.payload.push(...data);
                    self.payloadArr.push(...data);
                    model.PlanningResources([]);
                    
                    for (var i = 0; i < self.payload().length; i++) {
                        var objectPayload = {};
                        var objectPlanningAmounts = [{
                            Currency: "AED",
                            Quantity: self.payload()[i].totalQtyHours,
                            EffectiveRawCostRate: self.payload()[i].unitCostAedDiscounted,//unit price fc
                            EffectiveRevenueRate: self.payload()[i].unitCostAedDubai
                        }];

                        objectPayload.TaskNumber = self.payload()[i].costId;
                        objectPayload.ResourceName = self.payload()[i].partNo;
                        objectPayload.PlanningStartDate = self.payload()[i].startDate;
                        objectPayload.PlanningEndDate = self.payload()[i].finishDate;
                        objectPayload.PlanningAmounts = objectPlanningAmounts;

                        model.PlanningResources().push(objectPayload);
                    }
                    if (model.PlanningResources().length == 0) {
                        app.messagesDataProvider.push({ severity: "error", summary: "no resources found in the file you uploaded!", autoTimeout: 0 });
                        self.removePickerBtnAction();
                    }
                };

                reader.onerror = function (ex) {
                };

                reader.readAsBinaryString(files[0]);
            }
            };
            
            self.reportBtn =function()
            {
                var myJsonObject2 = {
                    "projectID": self.selectedProject().projectId,
                    "projectStartDate": self.selectedProject().projectStartDate,
                    "projectEndDate": self.selectedProject().projectEndDate,
                    "versionID": self.selectedVersion().planVersionId,
                    "projectName": self.selectedVersion().projectName ,
                    "CostID":self.dyCostId()  ,
                    "projectNumber":self.projetNumber(),
                    "versionNumber":self.versionName()
                    
                };
                oj.Router.rootInstance.store(myJsonObject2);
                oj.Router.rootInstance.go('procurmentPlan');
                
            };

            self.clearContents = function () {
                self.removePickerBtnAction();
                self.projectsSearchResult([]);
                self.VersionsArr([]);
                self.linesArr([]);
                self.payload([]);
                self.selectedProject({});
            };
            self.addNewBtnAction = function () {
                var payload = {};
                var myModel = {};

                payload.updateSelectedVersion = self.isUpdateSelectedVersion() && self.isUpdateSelectedVersion().length && self.isUpdateSelectedVersion().length > 0;
                myModel.ProjectName = self.projectNameVal();
                myModel.FinancialPlanType = self.newVersionPlanNameVal();
                myModel.PlanningResources = model.PlanningResources;
                if (!payload.updateSelectedVersion) {//add only in case of creation new version
                    myModel.PlanVersionName = self.newVersionNameVal();
                    //myModel.PlanVersionStatus = self.newVersionStatusVal();
                    myModel.BudgetCreationMethod = "MANUAL";
                } else {//update
                    payload.planVersionId = self.selectedVersion().planVersionId;
                }
                myModel = ko.toJSON(myModel);
                try { myModel = JSON.parse(myModel); } catch (e) { }

                if (payload.updateSelectedVersion)
                    myModel.PlanningResources.forEach(e => {
                        var ee = self.linesArr().find(x => x.TaskNumber == e.TaskNumber && x.ResourceName == e.ResourceName)
                        if (ee)
                            e.PlanningElementId = ee.PlanningElementId;
                    });
                if (self.newVersionPlanNameVal() && (self.newVersionNameVal() || payload.updateSelectedVersion) && self.newVersionStatusVal()) {
                    document.getElementById("addnewData").close();
                    
                    payload.data = myModel;
                    payload.payload = self.payload();
                    payload.updatePlan = self.isUpdateProjectPlan() && self.isUpdateProjectPlan().length && self.isUpdateProjectPlan().length > 0;
                    app.loading(true);
                    services.addGeneric('versions/newVersion', JSON.stringify(payload))
                        .then(s => {
                            app.loading(false);
                            if (s && s.length > 0) {
                                app.infomMsg(s, null);
                            } else {
                                self.clearContents();
                                app.messagesDataProvider.push({ severity: "info", summary: "Excel sheet imported successfully!", autoTimeout: 0 });//default time out
                            }
                        }, f => {
                            app.loading(false);
                        });
                } else {
                    app.messagesDataProvider.push({ severity: "error", summary: "please insure that you entered version name, plan name, and version status", autoTimeout: 0 });//default time out
               }
            };
            self.uploadAction = function (e) {
                self.versionPlanNames([
                    { label: 'Cost and Revenue Budget', value: 'Cost and Revenue Budget' },
                    { label: '3WN Cost and Revenue Budget', value: '3WN Cost and Revenue Budget' }
                ]);
                document.getElementById("addnewData").open();
            };

            self.previewBtnAction = function () {
                document.getElementById('excelData').open();
            };

            self.connected = function () {
                app.loading(false);
                initTranslations();


                $(document).ready(function () {
                    $(".oj-combobox-choice").addClass("loading");
                    self.getProjectsDetails();
                });

                setTimeout(() => {
                    // document.getElementById("addnewData").open();
                    // document.getElementById('resourceDialoge').open();
                    // var d = { "data": [{ "UNIT_COST_AED_DISCOUNTED": "9437.58", "COST_ID": "01", "PART_NO": "Sheriff, Ahmed", "TOTAL_COST_AED": "536226.17", "SUMMARY_TOTAL_PRICE_AED": "0", "UNIT_PRICE": "3,027.00", "SUPPLIER": "Scneider Electric", "TOTAL_PRICE_FC": "536226.17", "DESCRIPTION": "Superb quality indoor/outdoor pendant PTZ dome camera with 1080p HD resolution; 20x optical zoom; IVA; PoE; iSCSI/SD; multiple pre-programmed user modes; H.264 quad-streaming (CPP4); clear bubble. Rated IP66", "UNIT_COST_AED_DUBAI": "10724.52", "ID": 5, "LAST_STANDARD_DISCOUNT": "0", "PROJECT_ID": "300000002187836", "GP_EQUIPMENT": "7", "CONVERSION_RATE": "USD", "VARIABLE_OVERHEAD": "5", "FIXED_OVERHEAD": "0", "START_DATE": "2019-11-10", "UNIT_OF_MEASURE": "Nos.", "TOTAL_COST_AED_DISCOUNTED": "471879.03", "SPECIAL_DISCOUNT": "5", "VERSION_ID": "300000002364327", "POS": "1.01", "UNIT_COST_AED": "11103.04", "TOTAL_QTY": "50", "UNIT_PRICE_FC": "10724.52", "STANDARD_DISCOUNT": "10", "FINISH_DATE": "2019-11-10", "RESOURCE_NAME": "Material", "FOREIGN_CURRENCY": "USD" }], "status": "OK" };
                    // saveExcel(d)
                }, 1000);

            };

            ko.computed(_ => {
                self.searchDisable(!self.projectNameVal() && !self.projnumVal());
            });

            self.getProjectsDetails = function () {
                services.getGeneric("projects")
                    .then(data => {
                        self.projects(JSON.parse(JSON.stringify(data, (k,v) => { return v == 'null' ? "No specific data available" : v; })));
                        $(".oj-combobox-choice").removeClass("loading");
                        self.projectsSearchResult([]);
                        self.projectsNameArr(data.map(e => { return { label: e.projectName, value: e.projectName }; }));
                        self.projectsNumArr(data.map(e => { return { label: e.projectNumber, value: e.projectNumber }; }));
                    }, error => {
                        $(".oj-combobox-choice").removeClass("loading");
                    });
            };

            self.projectsTableSelectionListener = function (event) {
                var data = event.detail;
                var currentRow = data.currentRow;
                if (!currentRow)
                    return;
              
                var projectId = currentRow.rowKey;
                var row = self.projectsSearchResult().find(e => e.projectId == projectId);
                  self.projetNumber(row.projectNumber)
                self.selectedProject(row);

                getProjectById(projectId);
            };

            function numberWithCommas(x) {
                if (!x) return 0;

                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            function getProjectById(projectId) {
                app.loading(true);
                self.VersionsArr([]);
                //get versions
                services.getGeneric("versions/" + projectId)
                    .then(versons => {
                        app.loading(false);
                        self.versionsNamesOptions(versons.map(e => {
                            return { label: e.planVersionName + '(' + e.planVersionStatus + ')', value: e.planVersionId };
                        }));

                        versons.forEach(e => {
                            e.PCRawCostAmounts = numberWithCommas(e.PCRawCostAmounts);
                            e.PCRevenueAmounts = numberWithCommas(e.PCRevenueAmounts);
                        });
                        self.VersionsArr.push(...versons);
                        if (versons.length == 0) {
                            app.messagesDataProvider.push({ severity: "error", summary: "No Versions", autoTimeout: 0 });
                        }
                    }, error => {
                        app.loading(false);
                    });
                    
                services.getGeneric("versions/lastRecord/" + projectId).then(lastRecord =>{
                    self.lastVerRecord(lastRecord[0].planVersionId);
                },err=>{});
            }

            self.versionsTableSelectionListener = function (event) {

                var data = event.detail;
                if (!data) return;
                var currentRow = data.currentRow;
                if (!currentRow) return;
             
                var row = self.VersionsArr()[currentRow['rowIndex']];
                   self.versionName(row.planVersionName)
                self.projectId(row.projectId);
                self.getMyData(row.projectId,row.planVersionId);
                self.rowData(row);
                row.planVersionId == self.lastVerRecord() ? self.reportBtnDisabled(false) : self.reportBtnDisabled(true);
                desplayResources(row);
            };
            
            self.getMyData = function (projectID, versionID) {
                    var successEMPCbFn = function (SheetData) {
                        var getSuccss = function (data) {
                            app.loading(true)
                            var x = [... new Set(SheetData.data.map(e => e.COST_ID))]
                            var costIdCheckArray = []; //m
                            var unExistCostID = [];  //z
                            if (data.length == 0) {
                                self.reportBtnDisabled(true);
                                unExistCostID.push(...x)
                                app.messagesDataProvider.push({severity: "error", summary: `Please Add the Project Version Systems From Cost Category `, autoTimeout: 5000});
                                app.loading(false);
                            } else {
                                for (var i = 0; i < x.length; i++) {
                                    for (var j = 0; j < data.length; j++) {
                                        if (x[i] != data[j].costId) {
                                            costIdCheckArray.push(x[i])
                                        }
                                    }
                                    if (costIdCheckArray.length == data.length) {
                                        unExistCostID.push(costIdCheckArray[0]);
                                    } else {
                                        costIdCheckArray = [];
                                    }
                                }
                                if (unExistCostID.length > 0) {
                                    self.reportBtnDisabled(true);
                                    app.loading(false)
                                    app.messagesDataProvider.push({severity: "error", summary: `Please Add this Project Version Systems \"${unExistCostID}\" `, autoTimeout: 0});
                                } else {
                                    app.loading(false)
                                     self.reportBtnDisabled(false);
                                }
                            }
                        };
                        var failCbFn = function () {
                        };
                        var payload = {
                            projectId: self.projectId().toString()
                        }
                        services.addGeneric(commonhelper.getCostCategory, payload).then(getSuccss, failCbFn);
                        app.loading(false);
                    };

                    var failEMPCbFn = function () {
                        app.loading(false);
                    };
                    services.getGeneric("versions/view/" + projectID + "/" + versionID).then(successEMPCbFn, failEMPCbFn);
                };

            function desplayResources(row) {
                if (!row) return;
                self.selectedVersion(row);
                var link = JSON.parse(row.linkes).find(e => e.name == 'PlanningResources').href;
                app.loading(true);
                self.linesArr([]);
                services.getGeneric("projects/callSaas?link=" + link)
                    .then(tasks => {
                var x  =  new Set(...[tasks.items.map(e=>e.TaskNumber)])
                self.dyCostId.push(...x)
                        setTimeout(() => {
                            app.loading(false);
                            self.linesArr(tasks.items);
                        }, 1000);
                    }, error => {
                        app.loading(false);
                    });
            }

            $("body").delegate("#linesTable .oj-table-body-row", "click", (e) => {
                var key = $(e.currentTarget).find(".hiddenkeyHolder").val();
                var row = self.linesArr().find(xx => xx.PlanningElementId == key);
                var href = row.links.find(e => e.name == "PlanningAmounts").href;

                app.loading(true);
                services.getGeneric("projects/callSaas?link=" + href)
                    .then(line => {
                        app.loading(false);
                        var resource = line.items[0];
                        var obj = {};
                        obj.RawCostAmounts = resource.RawCostAmounts;
                        obj.RevenueAmounts = resource.RevenueAmounts;
                        obj.EffectiveRawCostRate = resource.EffectiveRawCostRate;
                        obj.EffectiveRevenueRate = resource.EffectiveRevenueRate;
                        obj.PCRawCostAmounts = resource.PCRawCostAmounts;
                        obj.PCRevenueAmounts = resource.PCRevenueAmounts;
                        obj.Quantity = resource.Quantity;
                        obj.ResourceName = row.ResourceName;
                        obj.TaskName = row.TaskName;
                        obj.planVersionId = self.selectedVersion().planVersionId;
                        obj.projectId = self.selectedProject().projectId;
                        obj.PlanLineId = resource.PlanLineId;
                        obj.PlanningElementId = row.PlanningElementId;
                        self.resourceToBeUpdated(obj);
                        document.getElementById('resourceDialoge').open();
                    }, error => {
                        app.loading(false);
                    });
            });

            self.addVersionBtnAction = function () {
                // newVersion
                self.uploadAction();
            };
            self.costCategoryBtn = function () {
              oj.Router.rootInstance.go('costCategory')
            };


            self.resourceUpdateInputesChangedListener = function (e) {

                var id = e.srcElement.id;
                if (!id) return;

                //-1
                var v5 = self.resourceToBeUpdated().PCRevenueAmounts;
                var v4 = self.resourceToBeUpdated().PCRawCostAmounts;
                var v3 = self.resourceToBeUpdated().Quantity;
                var v2 = self.resourceToBeUpdated().EffectiveRevenueRate;
                var v1 = self.resourceToBeUpdated().EffectiveRawCostRate;

                if (typeof v1 == NaN || typeof v2 == NaN || typeof v3 == NaN || typeof v4 == NaN || typeof v5 == NaN)
                    return;

                if (id != 'v5' && v3 && v2)
                    v5 = v3 * v2;

                if (id == 'v5' && v3)
                    v2 = v5 / v3;


                if (id != 'v4' && v3 && v1)
                    v4 = v3 * v1;

                if (id == 'v4' && v3)
                    v1 = v4 / v3;


                self.resourceToBeUpdated().PCRevenueAmounts = v5;
                self.resourceToBeUpdated().PCRawCostAmounts = v4;
                self.resourceToBeUpdated().Quantity = v3;
                self.resourceToBeUpdated().EffectiveRevenueRate = v2;
                self.resourceToBeUpdated().EffectiveRawCostRate = v1;
                self.resourceToBeUpdated(self.resourceToBeUpdated());
            };

            self.deleteBtnAction = function (e) {
                var element = document.getElementById('versionsTable');
                if (!element) return;
                var currentRow = element.currentRow;
                if (!currentRow) return;

                var row = self.VersionsArr()[currentRow['rowIndex']];

                if (['Working', 'Current Working'].indexOf(row.planVersionStatus) == -1) {
                    app.messagesDataProvider.push({ severity: "error", summary: `Can't Delete a version in \"${row.planVersionStatus}\" status`, autoTimeout: 0 });
                    return;
                }

                app.confirmMsg("Are you sure you want to delete this version?", yes => {
                    if (yes) {
                        app.loading(true);
                        services.deleteGeneric('versions/deleteVersion/' + row.planVersionId).then(RES => {
                            app.loading(false);
                            if (RES && RES.length > 0) {
                                app.infomMsg(RES);
                            } else {
                                app.messagesDataProvider.push({ severity: "info", summary: `Version Deleted Successfully!`, autoTimeout: 0 });
                                // self.clearContents();
                                var versions = self.VersionsArr().filter(e => e.planVersionId != row.planVersionId);
                                self.VersionsArr(versions);
                                self.linesArr([]);
                            }

                        }, error => {
                            app.loading(false);
                        });
                    } else {
                    }
                });
            };

            self.searchBtnAction = function () {
                if (!self.projectNameVal() && !self.projnumVal())
                    return;
                app.loading(true);
                self.projectsSearchResult([]);
                self.clearContents();
                setTimeout(() => {
                    if(self.projectNameVal()){
                         var currentProject = self.projects().filter(e=> e.projectName.toLowerCase().includes(self.projectNameVal().toLowerCase()))
                    }if(self.projnumVal()){
                         var currentProject = self.projects().filter(e=> e.projectName.toLowerCase().includes(self.projnumVal().toLowerCase()))
                    }                  
                   var currentProjectID= currentProject[0].projectId;
                    oj.Router.rootInstance.store(currentProjectID);
                    self.projectsSearchResult(self.projects().filter(e => {
                        return self.projectNameVal() ? e.projectName.toLowerCase().includes(self.projectNameVal().toLowerCase()) : true
                            && self.projnumVal() ? e.projectNumber.toLowerCase().includes(self.projnumVal().toLowerCase()) : true;
                    }));
                    app.loading(false);
                }, 100);

            };
                self.showCostCategory = function () {

                    oj.Router.rootInstance.go('costCategory')
                }
            self.updateResourceDataBtnAction = function (e) {

                if (['Working', 'Current Working'].indexOf(self.selectedVersion().planVersionStatus) == -1) {
                    app.messagesDataProvider.push({ severity: "error", summary: `Can't Update a version in \"${self.selectedVersion().planVersionStatus}\" status`, autoTimeout: 0 });
                    return;
                }

                app.confirmMsg("Do you want to save updates?", ok => {

                    if (ok) {
                        document.getElementById('resourceDialoge').close();
                        app.loading(true);
                        var object = {
                            "effectiveRawCostRate": self.resourceToBeUpdated().EffectiveRawCostRate,
                            "effectiveRevenueRate": self.resourceToBeUpdated().EffectiveRevenueRate,
                            "quantity": self.resourceToBeUpdated().Quantity,
                            "planVersionId": self.resourceToBeUpdated().planVersionId,
                            "planningElementId": self.resourceToBeUpdated().PlanningElementId,
                            "planLineId": self.resourceToBeUpdated().PlanLineId,
                            "projectName": self.selectedProject().projectName
                        };
                        services.addGeneric("versions/updateVersion", object)
                            .then(res => {
                                app.loading(false);
                                if (res && res.length > 0) {
                                    app.infomMsg(res);
                                }
                                else {
                                    app.messagesDataProvider.push({ severity: "info", summary: "Resource Updated Successfully", autoTimeout: 0 });
                                    getProjectById(self.selectedProject().projectId);
                                }
                            }, error => {
                                app.loading(false);
                                app.messagesDataProvider.push({ severity: "error", summary: "Can't save, plz contact us!", autoTimeout: 0 });
                            });
                    }
                });
            };

            function initTranslations() {
                self.MainLbl(app.translate("Purchase.MainLbl"));
                self.searchLbl(app.translate("Purchase.searchLbl"));
                self.BomLnum(app.translate("Purchase.BomLnum"));
                self.LineDesc(app.translate("Purchase.LineDesc"));
                self.PrOrder(app.translate("Purchase.PrOrder"));
                self.CurrrentQty(app.translate("Purchase.Quantity"));
                self.totalQty(app.translate("Purchase.totalQty"));
                self.UOM(app.translate("Purchase.UOM"));
                self.itmCost(app.translate("Purchase.itmCost"));
                self.ProjNum(app.translate("Purchase.ProjNum"));
                self.exportLbl(app.translate("Purchase.exportLbl"));
                self.CancelLineLbl(app.translate("Purchase.CancelLineLbl"));
                self.projectStartDate(app.translate("Purchase.projectStartDate"));
                self.projectEndDate(app.translate("Purchase.projectEndDate"));
                self.projName(app.translate("Purchase.projName"));
                self.GenePrLbl(app.translate("Purchase.GenePrLbl"));
                self.projectNameLbl(app.translate("Purchase.projectNameLbl"));
                self.projectNumberLbl(app.translate("Purchase.projectNumberLbl"));
                self.projectManagerLbl(app.translate("Purchase.projectManagerLbl"));
                self.projectStatusLbl(app.translate("Purchase.projectStatusLbl"));
                self.versionName(app.translate("Purchase.versionName"));
                self.versionStatus(app.translate("Purchase.versionStatus"));
                self.planType(app.translate("Purchase.planType"));
                self.revenueAmount(app.translate("Purchase.revenueAmount"));
                self.costAmount(app.translate("Purchase.costAmount"));
                 self.showBudgetTableLbl(app.translate("reports.showBudgetTableLbl"));
                self.exportBudgetSheetLbl(app.translate("reports.exportBudgetSheetLbl"));
                self.deleteVersionLbl(app.translate("reports.deleteVersionLbl"));
                self.reportsLbl(app.translate("reports.reportsLbl"));
                self.costCategoryLbl(app.translate("reports.costCategoryLbl"));
                self.columnArray([
                    {
                        "headerText": self.BomLnum(), "field": "seq", "template": "serTemplate"
                    }
                    ,
                    {
                        "headerText": self.projName(), "field": "projectName"
                    }
                    ,
                    {
                        "headerText": self.ProjNum(), "field": "projectNumber"
                    }
                    ,
                    {
                        "headerText": self.projectStartDate(), "field": "projectStartDate"
                    }
                    ,
                    {
                        "headerText": self.projectEndDate(), "field": "projectEndDate"
                    }
                    ,
                    {
                        "headerText": self.projectManagerLbl(), "field": "projectManagerName"
                    }
                    ,
                    {
                        "headerText": self.projectStatusLbl(), "field": "projectStatus"
                    }
                ]);
                self.VersionsColumnArray([
                    {
                        "headerText": self.versionName(), "field": "planVersionName"
                    },
                    {
                        "headerText": self.versionStatus(), "field": "planVersionStatus"
                    }
                    ,
                    {
                        "headerText": self.planType(), "field": "financialPlanType"
                    }
                    ,
                    {
                        "headerText": self.revenueAmount(), "field": "PCRevenueAmounts"
                    }
                    ,
                    {
                        "headerText": self.costAmount(), "field": "PCRawCostAmounts"
                    }
                ]);
                self.linesColumnArray([

                    {
                        "headerText": "Serial", "template": "resourceSerTemplate"
                    },
                    {
                        "headerText": "ID", "field": "TaskNumber"
                    }
                    ,
                    {
                        "headerText": "Task", "field": "TaskName"
                    }
                    ,
                    {
                        "headerText": "Resource", "field": "ResourceName"
                    }
                    ,
                    {
                        "headerText": "Start Date", "field": "PlanningStartDate"
                    }
                    ,
                    {
                        "headerText": "End Date", "field": "PlanningEndDate"
                    }
                ]);
            }

        }

        return budgetSummryContentViewModel;
    });
