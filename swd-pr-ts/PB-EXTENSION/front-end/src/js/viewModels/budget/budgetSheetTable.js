define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services','ojs/ojarraydataprovider', 
    'ojs/ojformlayout', 'ojs/ojlabel', 'ojs/ojbutton','ojs/ojtable','ojs/ojselectcombobox'],
    function (oj, ko, $, app, services) {
        function budgetSheetTableContentViewModel() {
            var self = this;

            self.sheetTableData = ko.observableArray([]);
            self.placeholder = ko.observable('Choose The Column Name');
            self.disableHideColumnBtn = ko.observable(true);
            self.disableShowColumnBtn = ko.observable(true);

            self.SheetData = ko.observableArray();
            self.sheetdataprovider = new oj.ArrayDataProvider(self.SheetData);
            var myEvent = [];
            var cols = [{ "headerText": "Pos", "field": "POS", "resizable": "enabled", "visible": true }, { "headerText": "refrence", "field": "refrence", "resizable": "enabled", "visible": true }, { "headerText": "costId", "field": "COST_ID", "resizable": "enabled", "visible": true }, { "headerText": "resourceName", "field": "RESOURCE_NAME", "resizable": "enabled", "visible": true }, { "headerText": "partNo", "field": "PART_NO", "resizable": "enabled", "visible": true }, { "headerText": "supplier", "field": "SUPPLIER", "resizable": "enabled", "visible": true }, { "headerText": "manufacturer", "field": "manufacturer", "resizable": "enabled", "visible": true }, { "headerText": "startDate", "field": "START_DATE", "resizable": "enabled", "visible": true }, { "headerText": "finishDate", "field": "FINISH_DATE", "resizable": "enabled", "visible": true }, { "headerText": "desciption", "field": "DESCRIPTION", "resizable": "enabled", "visible": true }, { "headerText": "site1", "field": "site1", "resizable": "enabled", "visible": true }, { "headerText": "site2", "field": "site2", "resizable": "enabled", "visible": true }, { "headerText": "site3", "field": "site3", "resizable": "enabled", "visible": true }, { "headerText": "site4", "field": "site4", "resizable": "enabled", "visible": true }, { "headerText": "site5", "field": "site5", "resizable": "enabled", "visible": true }, { "headerText": "site6", "field": "site6", "resizable": "enabled", "visible": true }, { "headerText": "site7", "field": "site7", "resizable": "enabled", "visible": true }, { "headerText": "site8", "field": "site8", "resizable": "enabled", "visible": true }, { "headerText": "site9", "field": "site9", "resizable": "enabled", "visible": true }, { "headerText": "site10", "field": "site10", "resizable": "enabled", "visible": true }, { "headerText": "site11", "field": "site11", "resizable": "enabled", "visible": true }, { "headerText": "site12", "field": "site12", "resizable": "enabled", "visible": true }, { "headerText": "site13", "field": "site13", "resizable": "enabled", "visible": true }, { "headerText": "site14", "field": "updatedDate", "resizable": "enabled", "visible": true }, { "headerText": "site15", "field": "site15", "resizable": "enabled", "visible": true }, { "headerText": "site16", "field": "site16", "resizable": "enabled", "visible": true }, { "headerText": "site17", "field": "site17", "resizable": "enabled", "visible": true }, { "headerText": "site18", "field": "site18", "resizable": "enabled", "visible": true }, { "headerText": "site19", "field": "site19", "resizable": "enabled", "visible": true }, { "headerText": "site20", "field": "site20", "resizable": "enabled", "visible": true }, { "headerText": "site21", "field": "site21", "resizable": "enabled", "visible": true }, { "headerText": "site22", "field": "site22", "resizable": "enabled", "visible": true }, { "headerText": "site23", "field": "site23", "resizable": "enabled", "visible": true }, { "headerText": "site24", "field": "site24", "resizable": "enabled", "visible": true }, { "headerText": "site25", "field": "site25", "resizable": "enabled", "visible": true }, { "headerText": "site26", "field": "site26", "resizable": "enabled", "visible": true }, { "headerText": "site27", "field": "site27", "resizable": "enabled", "visible": true }, { "headerText": "site28", "field": "site28", "resizable": "enabled", "visible": true }, { "headerText": "site29", "field": "site29", "resizable": "enabled", "visible": true }, { "headerText": "site30", "field": "site30", "resizable": "enabled", "visible": true }, { "headerText": "site31", "field": "site31", "resizable": "enabled", "visible": true }, { "headerText": "site32", "field": "site32", "resizable": "enabled", "visible": true }, { "headerText": "site33", "field": "site33", "resizable": "enabled", "visible": true }, { "headerText": "site34", "field": "site34", "resizable": "enabled", "visible": true }, { "headerText": "site35", "field": "site35", "resizable": "enabled", "visible": true }, { "headerText": "site36", "field": "site36", "resizable": "enabled", "visible": true }, { "headerText": "site37", "field": "site37", "resizable": "enabled", "visible": true }, { "headerText": "site38", "field": "site38", "resizable": "enabled", "visible": true }, { "headerText": "site39", "field": "site39", "resizable": "enabled", "visible": true }, { "headerText": "site40", "field": "site40", "resizable": "enabled", "visible": true }, { "headerText": "site41", "field": "site41", "resizable": "enabled", "visible": true }, { "headerText": "site42", "field": "site42", "resizable": "enabled", "visible": true }, { "headerText": "site43", "field": "site43", "resizable": "enabled", "visible": true }, { "headerText": "site44", "field": "site44", "resizable": "enabled", "visible": true }, { "headerText": "site45", "field": "site45", "resizable": "enabled", "visible": true }, { "headerText": "site46", "field": "site46", "resizable": "enabled", "visible": true }, { "headerText": "site47", "field": "site47", "resizable": "enabled", "visible": true }, { "headerText": "site48", "field": "site48", "resizable": "enabled", "visible": true }, { "headerText": "site49", "field": "site49", "resizable": "enabled", "visible": true }, { "headerText": "site50", "field": "site50", "resizable": "enabled", "visible": true }, { "headerText": "site51", "field": "site51", "resizable": "enabled", "visible": true }, { "headerText": "site52", "field": "site52", "resizable": "enabled", "visible": true }, { "headerText": "site53", "field": "site53", "resizable": "enabled", "visible": true }, { "headerText": "site54", "field": "site54", "resizable": "enabled", "visible": true }, { "headerText": "site55", "field": "site55", "resizable": "enabled", "visible": true }, { "headerText": "site56", "field": "site56", "resizable": "enabled", "visible": true }, { "headerText": "site57", "field": "site57", "resizable": "enabled", "visible": true }, { "headerText": "site58", "field": "site58", "resizable": "enabled", "visible": true }, { "headerText": "site59", "field": "site59", "resizable": "enabled", "visible": true }, { "headerText": "site60", "field": "site60", "resizable": "enabled", "visible": true }, { "headerText": "site61", "field": "site61", "resizable": "enabled", "visible": true }, { "headerText": "site62", "field": "site62", "resizable": "enabled", "visible": true }, { "headerText": "site63", "field": "site63", "resizable": "enabled", "visible": true }, { "headerText": "site64", "field": "site64", "resizable": "enabled", "visible": true }, { "headerText": "site65", "field": "site65", "resizable": "enabled", "visible": true }, { "headerText": "site66", "field": "site66", "resizable": "enabled", "visible": true }, { "headerText": "site67", "field": "site67", "resizable": "enabled", "visible": true }, { "headerText": "site68", "field": "site68", "resizable": "enabled", "visible": true }, { "headerText": "site69", "field": "site69", "resizable": "enabled", "visible": true }, { "headerText": "site70", "field": "site70", "resizable": "enabled", "visible": true }, { "headerText": "unitOfMeasure", "field": "UNIT_OF_MEASURE", "resizable": "enabled", "visible": true }, { "headerText": "totalQtyHours", "field": "TOTAL_QTY", "resizable": "enabled", "visible": true }, { "headerText": "BudgetCurrencyAed", "field": "BudgetCurrencyAed", "resizable": "enabled", "visible": true }, { "headerText": "foreignCurrency", "field": "FOREIGN_CURRENCY", "resizable": "enabled", "visible": true }, { "headerText": "conversionRate", "field": "CONVERSION_RATE", "resizable": "enabled", "visible": true }, { "headerText": "lastBudgetPrice", "field": "lastBudgetPrice", "resizable": "enabled", "visible": true }, { "headerText": "lastBudgetPriceCurrency", "field": "lastBudgetPriceCurrency", "resizable": "enabled", "visible": true }, { "headerText": "unitPriceRate", "field": "UNIT_PRICE", "resizable": "enabled", "visible": true }, { "headerText": "unitCostAed", "field": "UNIT_COST_AED", "resizable": "enabled", "visible": true }, { "headerText": "totalCostAed", "field": "TOTAL_COST_AED", "resizable": "enabled", "visible": true }, { "headerText": "lastStandardDiscount", "field": "LAST_STANDARD_DISCOUNT", "resizable": "enabled", "visible": true }, { "headerText": "standardDiscount", "field": "STANDARD_DISCOUNT", "resizable": "enabled", "visible": true }, { "headerText": "specialDiscount", "field": "SPECIAL_DISCOUNT", "resizable": "enabled", "visible": true }, { "headerText": "unitCostAedDiscounted", "field": "unitCostAedDiscounted", "resizable": "enabled", "visible": true }, { "headerText": "totalCostAedDiscounted", "field": "UNIT_COST_AED_DISCOUNTED", "resizable": "enabled", "visible": true }, { "headerText": "gpEquipment", "field": "GP_EQUIPMENT", "resizable": "enabled", "visible": true }, { "headerText": "fixedOverhead", "field": "FIXED_OVERHEAD", "resizable": "enabled", "visible": true }, { "headerText": "variableOverhead", "field": "VARIABLE_OVERHEAD", "resizable": "enabled", "visible": true }, { "headerText": "unitCostAedDubai", "field": "UNIT_COST_AED_DUBAI", "resizable": "enabled", "visible": true }, { "headerText": "totalCostAed", "field": "UNIT_COST_AED", "resizable": "enabled", "visible": true }, { "headerText": "summaryTotalPriceAed", "field": "SUMMARY_TOTAL_PRICE_AED", "resizable": "enabled", "visible": true }, { "headerText": "unitPriceFc", "field": "UNIT_PRICE_FC", "resizable": "enabled", "visible": true }, { "headerText": "totalPriceFc", "field": "TOTAL_PRICE_FC", "resizable": "enabled", "visible": true }, { "headerText": "summaryTotalPriceAed", "field": "SUMMARY_TOTAL_PRICE_AED", "resizable": "enabled", "visible": true }];

            self.connected = function () {
                var myData = oj.Router.rootInstance.retrieve();
                if (!myData || !myData.projectID || !myData.versionID) {
                    oj.Router.rootInstance.go('budgetSummry');
                    return;
                }
                self.sheetTableData(cols);
                self.getMyData(myData.projectID, myData.versionID);
            };
            self.BackBtnAction=function(){
                
                oj.Router.rootInstance.go('budgetSummry');
            }
            self.getMyData = function (projectID, versionID) {
                services.getGeneric("versions/view/" + projectID + "/" + versionID)
                    .then(SheetData => {
                        app.loading(false);
                        self.SheetData(SheetData.data);
                    }, error => {
                        app.loading(false);
                    });

            };


            self.changeListValue = function (event) {
                if (event && event.detail && event.detail.value)
                    myEvent = event.detail.value;

                self.disableHideColumnBtn(true);
                self.disableShowColumnBtn(true);

                var isColExist = false;

                for (var i = 0; i < myEvent.length; i++) {
                    var itemInSelectedArr = myEvent[i];//pos => visible->true
                    if (cols.find(e => e.headerText == itemInSelectedArr && e.visible)) {
                        self.disableHideColumnBtn(false);
                        isColExist = true;
                    }
                }
                self.disableShowColumnBtn(isColExist || myEvent.length == 0);

            }
            self.hideColumn = function () {
                colAction(false)
            }
            self.showColumn = function () {
                colAction(true)
            }

            function colAction(show) {
                myEvent.forEach(value => {
                    cols.find(e => e.headerText == value).visible = show;
                });
                self.sheetTableData(cols.filter(e => e.visible));
                self.changeListValue()
            }

        }
        return budgetSheetTableContentViewModel;
    });
