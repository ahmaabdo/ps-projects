/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'xlsx',
    'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojbutton',
    'ojs/ojmessages', 'ojs/ojdialog', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojvalidationgroup',
    'ojs/ojfilepicker'],
        function (oj, ko, $, app, services, commonhelper) {

            function procurmentPlanViewModel() {

                var self = this;

                self.projectNumber = ko.observable();
                self.MainLbl = ko.observable();
                self.isDisabledProNumVal = ko.observable('true');
                self.isDisabledSupplerNumVal = ko.observable('true');
                self.isDisabledSubmitProc = ko.observable(false);
                self.isDisabledImport = ko.observable(true);
                self.overHeadDisabled = ko.observable(false);
                self.projectNumberValue = ko.observable();
                self.projectVersion = ko.observable();
                self.projectVersionValue = ko.observable();
                self.ProcPlanDurationLbl = ko.observable();
                self.prProcessingDateValue = ko.observable();
                self.contingencyTimeValue = ko.observable();
                self.shippingTimeValue = ko.observable();
                self.manufacturingLeadTimeValue = ko.observable();
                self.packingValue = ko.observable();
                self.deliveryValue = ko.observable();
                self.targetDeliveryDateValue = ko.observable();
                self.cabinetAssemblyValue = ko.observable();
                self.preFatValue = ko.observable();
                self.fatValue = ko.observable();
                self.fatPunchClearanceValue = ko.observable();
                self.paymentTermValue = ko.observable();
                self.supplierNumberValue = ko.observable();
                self.prProcessingDatelbl = ko.observable();
                self.contingencyTimeLbl = ko.observable();
                self.shippingTimeLbl = ko.observable();
                self.manufacturingLeadTimeLbl = ko.observable();
                self.packingLbl = ko.observable();
                self.deliveryLbl = ko.observable();
                self.targetDeliveryDateLbl = ko.observable();
                self.cabinetAssemblyLbl = ko.observable();
                self.preFatLbl = ko.observable();
                self.fatLbl = ko.observable();
                self.fatPunchClearanceLbl = ko.observable();
                self.paymentDate = ko.observable();
                self.weekLbl = ko.observable();
                self.dayLbl = ko.observable();
                self.submitProLbl = ko.observable();
                self.servicesLbl = ko.observable();
                self.financesCostLbl = ko.observable();
                self.overHeadLbl = ko.observable();
                self.paymentTermLbl = ko.observable();
                self.costId = ko.observable();
                self.Details = ko.observable();
                self.rules = ko.observable();
                self.amount = ko.observable();
                self.value = ko.observable();
                self.Rate = ko.observable();
                self.backLbl = ko.observable();
                self.itmCost = ko.observable();
                self.ProjNum = ko.observable();
                self.item = ko.observable();
                self.description = ko.observable();
                self.biddingEstimate = ko.observable();
                self.purchaseEstimate = ko.observable();
                self.supplierAED = ko.observable();
                self.qty = ko.observable();
                self.unit = ko.observable();
                self.totalAED = ko.observable();
                self.gpServices = ko.observable();
                self.planType = ko.observable();
                self.revenueAmount = ko.observable();
                self.costAmount = ko.observable();
                self.systemCode = ko.observable();
                self.servicesColumnArray = ko.observableArray([]);
                self.payload = ko.observableArray([]);
                self.selected_files = ko.observableArray([]);
                self.acceptArr = ko.observableArray([]);
                self.saveDatesLbl = ko.observable();
                self.cancelDatesLbl = ko.observable();
                self.exportProLbl = ko.observable();
                self.system = ko.observable();
                self.make = ko.observable();
                self.supplier = ko.observable();
                self.supplierNoLbl = ko.observable();
                self.costId = ko.observable();
                self.currency = ko.observable();
                self.afterDistotalCostFC = ko.observable();
                self.exchangeRate = ko.observable();
                self.afterDistotalCostAED = ko.observable();
                self.beforeDisCostAED = ko.observable();
                self.DisAED = ko.observable();
                self.DisRate = ko.observable();
                self.forecostPRDate = ko.observable();
                self.forecostOrderDate = ko.observable();
                self.manufacturingLeadTime = ko.observable();
                self.contingencyTime = ko.observable();
                self.matReadyWithSupplier = ko.observable();
                self.shippingTime = ko.observable();
                self.ETADSODate = ko.observable();
                self.cabinetAssembly = ko.observable();
                self.PreFAT = ko.observable();
                self.FAT = ko.observable();
                self.FATPunchClearance = ko.observable();
                self.integerationFATDate = ko.observable();
                self.packing = ko.observable();
                self.delivery = ko.observable();
                self.targetDeliveryDate = ko.observable();
                self.PRProcessingTime = ko.observable();
                self.rowData = ko.observable();
                self.saveDateData = ko.observable();
                self.currentRow = ko.observable();
                self.groupValid = ko.observable();
                self.servicesBtnLbl = ko.observable();
                self.costScreenLbl = ko.observable();
                self.data = ko.observableArray([]);
                self.supplierRows = ko.observable();
                self.columnArray = ko.observableArray([]);
                self.columnArray2 = ko.observableArray([]);
                self.procurmentPlanData = ko.observableArray([]);
                self.object = ko.observableArray([]);
                self.SheetData = ko.observableArray();
                self.tableData = ko.observableArray();
                self.dataToInsert = ko.observableArray();
                self.dataSourcePro = ko.observable();
                self.servicesArr = ko.observableArray([]);
                self.linesArr = ko.observableArray([]);
                self.dataprovider2 = ko.observable();
                self.dataSourcePro(new oj.ArrayDataProvider(self.object, {keyAttributes: 'seq'}));
                self.dataprovider2(new oj.ArrayDataProvider(self.tableData, {keyAttributes: 'projectId'}));
                self.servicesArrDataprovider = new oj.ArrayDataProvider(self.servicesArr, {keyAttributes: 'id'});
                self.Description = ko.observable();
                self.costDate = ko.observable();
                self.sequanceLbl = ko.observable();
                self.costIdRetrived = ko.observableArray();
                 self.tableIndex=ko.observable();
              
                 self.dataSysN= ko.observable();
                 self.taskNumberRef=ko.observable();
                 self.serviceSummary=ko.observable();
                 self.ratePerHours=ko.observable();
                 self.totalcost=ko.observable();
                 self.totalHours=ko.observable();
                 self.proNum=ko.observable();
                 self.proVName=ko.observable();
                 self.projectStartDate=ko.observable();
                 self.projectEndDate=ko.observable();
                 self.dateFrom=ko.observable();
                 self.dateTo=ko.observable();
                 self.allTableData=ko.observableArray();
                    self.uploadOverDisabled=ko.observable(false);
                    self.uploadVisable=ko.observable(true);

                self.connected = function ()
                {
                    app.loading(true);
                    self.selected_files([]);
                    var myData = oj.Router.rootInstance.retrieve();
                    if (myData)
                    {
                        self.projectNumberValue(myData.projectID);
                        self.proNum(myData.projectNumber);
                        self.proVName(myData.versionNumber);
                        self.projectVersionValue(myData.versionID);
                        self.costIdRetrived.push(...myData.CostID);
                        self.projectStartDate(myData.projectStartDate);
                        self.projectEndDate(myData.projectEndDate); 
                                         
                    }
                    getDataDB(function (found) {
                        if (found)
                            self.getMyData(myData.projectID, myData.versionID);
                    });
                };

                var getDataDB = function (foundFunc)
                {
                    var payload = {
                        "projectId": self.projectNumberValue().toString(),
                        "versionId": self.projectVersionValue().toString()
                    };
                    var successEMPCbFn = function (data) {
                        if (data.data.length == 0)
                        {
                            foundFunc(true);
                        } else
                        {
                            foundFunc(false);
                            data.data.map(e => {
                                var obj = e;
                                obj.children = [{
                                        "forecostPRDate": e.forecastPrDate,
                                        "prProcessingTime": e.prProcessingTime,
                                        "forecostOrderDate": e.forecostOrderDate,
                                        "manufacturingLeadTime": e.manufactingLeadTime,
                                        "contingencyTime": e.cantingencyTime,
                                        "matReadyWithSupplier": e.matReadyWith,
                                        "shippingTime": e.shippingTime,
                                        "ETADSODate": e.etaDsoDate,
                                        "cabinetAssembly": e.cabinetAssembly,
                                        "PreFAT": e.preFat,
                                        "FAT": e.fat,
                                        "FATPunchClearance": e.fatPunchClearance,
                                        "integerationFATDate": e.integerationFatDate,
                                        "packing": e.packing,
                                        "delivery": e.delivery,
                                        "targetDeliveryDate": e.targetDeliveryDate,
                                        "paymentTerm": e.paymentTerm,
                                        "paymentDate": e.paymentDate
                                    }];
                                return obj;
                            });
                            self.isDisabledSubmitProc(true);
                            self.object(data.data);
                            var countCost = 0;
                            for (var i = 0; i < data.data.length; i++) {
                                countCost += parseFloat(data.data[i].afterDistotalCostAED);
                            }
                            app.loading(false);
                            app.tProcurmentValue(countCost);
                            self.procurmentPlanData(data.data);
                            getAllServices();
                            self.getAllOverHeadData();
                            self.dataSourcePro(new oj.ArrayTableDataSource(self.object(), {keyAttributes: 'seq'}));
                        }
                    };

                    var failEMPCbFn = function () {
                    };
                    services.postGeneric(commonhelper.getAllProcurment, payload).then(successEMPCbFn, failEMPCbFn);
                };


                self.getMyData = function (projectID, versionID) {

                    var successEMPCbFn = function (SheetData) {
                        self.SheetData(SheetData.data);
                        calculationProcPlan(self.SheetData());
                        app.loading(false);
                    };

                    var failEMPCbFn = function () {
                        app.loading(false);
                    };
                    services.getGeneric("versions/view/" + projectID + "/" + versionID).then(successEMPCbFn, failEMPCbFn);
                };

                var calculationProcPlan = function (arr)
                {
                    var names = [...new Set(arr.map(e => e.SUPPLIER))];

                    names.forEach((name, i) => {
                        var rows = arr.filter(e => e.SUPPLIER == name);
                        self.supplierRows(rows[0].COST_ID);
                        var payload = {
                            "costId": rows[0].COST_ID,
                            "projectId":self.projectNumberValue().toString()
                            
                        };              
                        var successEMPCbFn = function (data) {
                         
                            var costAfter = parseInt(rows.map(e => e.TOTAL_COST_AED_DISCOUNTED).reduce((p, c) => (((p - 0) || 0) + ((c - 0) || 0))));
                            var costBefore = parseInt(rows.map(e => e.TOTAL_COST_AED).reduce((p, c) => ((((p - 0) || null) || 0) + (((c - 0) || null) || 0))));
                            var disAED = (costBefore - costAfter);
                            var rate = 1;

                            setTimeout(function () {
                                var obj = {
                                    "seq": (i++).toString(),
                                    "system": data[0].systemCode,
                                    "make": rows[0].MAKE,
                                    "supplier": name,
                                    "costId": rows[0].COST_ID,
                                    "currency": rows[0].CURRENCY,
                                    "afterDistotalCostAED": (costAfter.toFixed(2)).toString(),
                                    "exchangeRate": rate.toString(),
                                    "afterDistotalCostFc": ((costAfter).toFixed(2) / rate).toString(),
                                    "beforeDistotalCostAED": ((costBefore).toFixed(2)).toString(),
                                    "discountAED": disAED.toString(),
                                    "discountRate": disAED > 0 ? (((disAED / costBefore).toFixed(2)) * 100).toString() + ' %' : "0" + ' %',
                                    "projectId": self.projectNumberValue().toString(),
                                    "versionId": self.projectVersionValue().toString(),
                                    "children": []
                                };
                                self.object.push(obj);
                            }, 1000);
                            self.dataSourcePro(new oj.ArrayTableDataSource(self.object, {keyAttributes: 'seq'}));
                        };

                        var failEMPCbFn = function () {
                            app.loading(false);
                        };
                  services.postGeneric(commonhelper.systemCode, payload).then(successEMPCbFn, failEMPCbFn);
                                                                    
                    });
                };
                $("body").delegate("#tablePro .oj-table-body-row", "click", (e) => {
                    var index = $(e.currentTarget).find(".hiddenkeyHolder").val();
                    var row = self.object()[index];
               self.tableIndex(index);
                    if (self.procurmentPlanData().length > 0)
                        return;
                    if (!row)
                        return;
                    self.rowData(row);
                    self.supplierNumberValue(row.supplier);
                    document.querySelector("#ProcPlanDuration").open();
                });



//                self.tableSeleListenerPro = function (event)
//                {
//                    if (self.procurmentPlanData().length > 0)
//                        return;
//                    var data = event.detail;
//                    if (!data)
//                        return;
//                    var currentRow = data.currentRow;
//                    if (!currentRow)
//                        return;
//                    var row = self.object()[currentRow['rowIndex']];
//                    self.currentRow(currentRow['rowIndex']);
//                    self.rowData(row);
//                    self.supplierNumberValue(row.supplier);
//                    document.querySelector("#ProcPlanDuration").open();
//                };

                self.saveDatesBtn = function ()
                {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }

                    var z = self.cabinetAssemblyValue() + self.preFatValue() + self.fatValue() + self.fatPunchClearanceValue();
                    var x = self.packingValue() + self.deliveryValue();
                    var integration = calculationDate(self.targetDeliveryDateValue(), x);

                    var eta = calculationDate(integration, z);

                    var shippingTime = (self.shippingTimeValue() * 7);
                    var mat = calculationDate(eta, shippingTime);

                    var forecast = (self.manufacturingLeadTimeValue() + self.contingencyTimeValue()) * 7;
                    var forecastOrder = calculationDate(mat, forecast);

                    var prTime = self.prProcessingDateValue() * 7;
                    var forecastPR = calculationDate(forecastOrder, prTime);

                    var paymentDate = calculationDatePlus(eta, self.paymentTermValue())

                    var obj = {
                        "prProcessingTime": self.prProcessingDateValue().toString(),
                        "contingencyTime": self.contingencyTimeValue().toString(),
                        "shippingTime": self.shippingTimeValue().toString(),
                        "manufacturingLeadTime": self.manufacturingLeadTimeValue().toString(),
                        "packing": self.packingValue().toString(),
                        "delivery": self.deliveryValue().toString(),
                        "targetDeliveryDate": self.targetDeliveryDateValue().toString(),
                        "cabinetAssembly": self.cabinetAssemblyValue().toString(),
                        "PreFAT": self.preFatValue().toString(),
                        "FAT": self.fatValue().toString(),
                        "FATPunchClearance": self.fatPunchClearanceValue().toString(),
                        "paymentTerm": self.paymentTermValue().toString(),
                        "paymentDate": paymentDate,
                        "integerationFATDate": integration,
                        "forecostPRDate": forecastPR,
                        "forecostOrderDate": forecastOrder,
                        "matReadyWithSupplier": mat,
                        "ETADSODate": eta
                    };
                        
                        if(
                            new Date(app.formatDate(obj.targetDeliveryDate)) < new Date(app.formatDate(self.projectStartDate())) ||
                            new Date(app.formatDate(obj.forecostOrderDate)) < new Date(app.formatDate(self.projectStartDate())) ||
                            new Date(app.formatDate(obj.matReadyWithSupplier)) < new Date(app.formatDate(self.projectStartDate())) ||
                            new Date(app.formatDate(obj.ETADSODate)) < new Date(app.formatDate(self.projectStartDate())) ||
                            new Date(app.formatDate(obj.integerationFATDate)) < new Date(app.formatDate(self.projectStartDate())) ||
                            new Date(app.formatDate(obj.paymentDate)) < new Date(app.formatDate(self.projectStartDate())) ||
                            new Date(app.formatDate(obj.forecostOrderDate)) < new Date(app.formatDate(self.projectStartDate()))
                                ){      
                             app.messagesDataProvider.push({severity: "error", summary: "Out Of Project Date Range", autoTimeout: 0});
                        } 
                        if (self.projectEndDate() !="No specific data available"&&
                            new Date(app.formatDate(obj.targetDeliveryDate)) > new Date(app.formatDate(self.projectEndDate())) ||
                            new Date(app.formatDate(obj.forecostOrderDate)) > new Date(app.formatDate(self.projectEndDate())) ||
                            new Date(app.formatDate(obj.matReadyWithSupplier)) > new Date(app.formatDate(self.projectEndDate())) ||
                            new Date(app.formatDate(obj.ETADSODate)) > new Date(app.formatDate(self.projectEndDate())) ||
                            new Date(app.formatDate(obj.integerationFATDate)) > new Date(app.formatDate(self.projectEndDate())) ||
                            new Date(app.formatDate(obj.paymentDate)) > new Date(app.formatDate(self.projectEndDate())) ||
                            new Date(app.formatDate(obj.forecostOrderDate)) > new Date(app.formatDate(self.projectEndDate()))
                                ){  
                            
                                app.messagesDataProvider.push({severity: "error", summary: "Out Of Project Date Range", autoTimeout: 0});
                        }
                    self.saveDateData(obj);
                     
                    self.object()[ self.tableIndex()].children = []                  
                    self.object().find(e => e.supplier == self.supplierNumberValue()).children.push(self.saveDateData());
                    self.dataSourcePro(new oj.ArrayTableDataSource(self.object, {keyAttributes: 'seq'}));

                    self.dataToInsert.push(self.rowData());
                    document.querySelector("#ProcPlanDuration").close();
                    self.resetFun();
                };

                var calculationDate = function (DateValue, calcul)
                {
                    var d = new Date(DateValue);
                    d.setDate(d.getDate() - calcul);
                    var result = ((d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear());
                    return result;
                };

                var calculationDatePlus = function (DateValue, calcul)
                {
                    var d = new Date(DateValue);
                    d.setDate(d.getDate() + calcul);
                    var result = ((d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear());
                    return result;
                };

                self.resetFun = function ()
                {
                    self.prProcessingDateValue(0);
                    self.contingencyTimeValue(0);
                    self.shippingTimeValue(0);
                    self.manufacturingLeadTimeValue(0);
                    self.packingValue(0);
                    self.deliveryValue(0);
                    self.targetDeliveryDateValue('');
                    self.cabinetAssemblyValue(0);
                    self.preFatValue(0);
                    self.fatValue(0);
                    self.fatPunchClearanceValue(0);
                    self.paymentTermValue(0);
                };

                self.submitProcBtnAction = function ()
                {
                    var isRecordFill = self.object().every(row => Object.values(row).length >= 15 && Object.values(row).every(e => e)/*check row*/
                                && row.children.length > 0
                                && Object.values(row.children[0]).length >= 16
                                && Object.values(row.children[0]).every(e => e)
                    );
                    if (isRecordFill)
                    {
                        app.loading(true);
                        var successEMPCbFn = function (data) {
                            app.loading(false);
                            self.isDisabledSubmitProc(true);
                            self.isDisabledImport(false);
                            self.data().push(data);
                            self.procurmentPlanData(self.data());
                            getAllServices();
                        };

                        var failEMPCbFn = function () {
                        };
                        services.postGeneric(commonhelper.insertProcurment, self.dataToInsert()).then(successEMPCbFn, failEMPCbFn);

                    } else
                    {
                        app.messagesDataProvider.push({severity: "error", summary: "Please fill all record!!!", autoTimeout: 0});
                    }
                };

                self.exportProcBtnAction = function ()
                {
                    var exportedData = [];
                    $.each(self.object(),function(index){                       
                       exportedData .push({
                                       System:self.object()[index].system,
                                               Make:self.object()[index].make,
                                               Supplier:self.object()[index].supplier,
                                               CostID:self.object()[index].costId,
                                               Currency:self.object()[index].currency,
                                               AfterDiscountTotalCostFC:self.object()[index].afterDistotalCostFc,
                                               ExchangeRate:self.object()[index].exchangeRate,
                                               AfterDiscountTotalCostAED:self.object()[index].afterDistotalCostAED,
                                               BeforeDiscontTotalCostAED:self.object()[index].beforeDistotalCostAED,
                                               DiscountAED:self.object()[index].discountAED,
                                               DiscountRate:self.object()[index].discountRate,
                                               ForcostPRDate:self.object()[index].forecastPrDate,
                                               PRProcessingTime:self.object()[index].prProcessingTime,
                                               ForcostOrderDate:self.object()[index].forecostOrderDate,
                                               ManufacturingLeadTime:self.object()[index].manufactingLeadTime,
                                               ContingencyTime:self.object()[index].cantingencyTime,
                                               MatReadyWithSupplier:self.object()[index].matReadyWith,
                                               ShippingTime:self.object()[index].shippingTime,
                                               ETADSODate:self.object()[index].etaDsoDate,
                                               CabientAssemply:self.object()[index].cabinetAssembly,
                                               PreFAT:self.object()[index].preFat,
                                               FAT:self.object()[index].fat,
                                               FATPunchClerance:self.object()[index].fatPunchClearance,
                                               IntegerationFATDate:self.object()[index].integerationFatDate,
                                               Packing:self.object()[index].packing,
                                               Delivery:self.object()[index].delivery,
                                               TargetDeliveryDate:self.object()[index].targetDeliveryDate,
                                               PaymentTerm:self.object()[index].paymentTerm,
                                               PaymentDate:self.object()[index].paymentDate,                                     
                                    });                      
                    });
                    var ws = XLSX.utils.json_to_sheet(exportedData);
                    var wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, "Employee");
                    XLSX.writeFile(wb, "Procurment Plan.xlsx");
                    app.messagesDataProvider.push({severity: "info", summary: "Excel sheet imported successfully!", autoTimeout: 0});
                };
          

                self.addNewBtnAction = function ()
                {
                    var payload = {};
                    payload.payload = self.payload();
                    for (var i in self.payload()) {
                        if (self.payload()[i].Task_Number_Ref == "") {
                            self.payload().splice(i, 1)
                        }
                    }
                    var filterPayload = [];
                    filterPayload = payload.payload
                    app.loading(true);
                    var x = [];
                    var totalTaskAmount = 0;
                    for (var i in self.payload()) {
                        var payload2 = {};
                        if (self.payload()[i].Task_Number_Ref == "76") {
                            totalTaskAmount = parseInt((self.payload()[i].Cost).replace(",", "")) + totalTaskAmount
                        } else {
                            payload2.planVersionId = self.projectVersionValue();
                            payload2.data = {
                                "TaskNumber": self.payload()[i].Task_Number_Ref,
                                "ResourceName": self.payload()[i].Project_Service_Cost_Summary,
                                "PlanningStartDate": "2019-06-01",
                                "PlanningEndDate": "2019-09-01",
                                "PlanningAmounts": [
                                    {
                                        "Currency": "AED",
                                        "Quantity": (self.payload()[i]._Hours_).replace(",", ""),
                                        "EffectiveRawCostRate": self.payload()[i].Rates,
                                        "EffectiveRevenueRate": self.payload()[i].Rates
                                    }
                                ]
                            };
                        }
                        x.push(payload2);
                    }                 
                    var Tasks= [...new Set(self.payload().map(e => e.Task_Number_Ref))];
                    if (Tasks.includes("76")) {
                        var totalFinanceElement = {
                            "planVersionId": self.projectVersionValue(),
                            "data": {
                                "TaskNumber": "76",
                                "ResourceName": "Financial Resources",
                                "PlanningStartDate": "2019-06-01",
                                "PlanningEndDate": "2019-09-01",
                                "PlanningAmounts": [
                                    {
                                        "Currency": "AED",                                                                       
                                        "RawCostAmounts": totalTaskAmount,
                                        "RevenueAmounts": totalTaskAmount
                                    }
                                ]
                            }
                        };
                        x.push(totalFinanceElement)
                    }                  
                    var uploadedPayload = x.filter(e=>e.planVersionId);
                    var successEMPCbFn = function (data) {
                        var errorResponse = data.filter(e => e.status == "error").map(ee => ee.data);
                        if (errorResponse.length > 0) {
                            app.loading(false)
                            app.infomMsg(errorResponse.toString());
                            return;
                           
                        } else {
                           app.loading(false)
                            addServiceToDb(filterPayload)
                        }
                    };
                   services.postGeneric(commonhelper.uploadServiceToInstance, uploadedPayload).then(successEMPCbFn, app.failEMPCbFn);

                    self.removePickerBtnAction();              
                };               
                var addServiceToDb = function (filterPayload) {

                    var successEMPCbFn = function (data) {
                        app.loading(false);
                        if (data && data.length > 0) {
                            app.infomMsg(data, null);
                        } else {
                            getAllServices();
                            app.messagesDataProvider.push({severity: "info", summary: "Excel sheet imported successfully!", autoTimeout: 0});//default time out
                        }
                    };

                    var failEMPCbFn = function () {
                        app.loading(false);
                    };
                    services.postGeneric(commonhelper.addServices, filterPayload).then(successEMPCbFn, failEMPCbFn);
                };
                self.selectListener = function (event)
                {
                    var files = event.detail.files;
                    self.selected_files(files);
                    var reader = new FileReader();

                    app.loading(true);
                    reader.onload = function (e) {
                        var data = e.target.result;
                        var workbook = XLSX.read(data, {type: 'binary'});
                        var sheet = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], {defval: ""});
                        app.loading(false);
                        self.payload(sheet);
                        self.payload().forEach(o => {
                            Object.keys(o).forEach(key => {
                                var val = o[key];
                                delete o[key];
                                key = (key.replace(/ /g, "_").replace("(", "").replace(")", ""));
                                val = (val.replace("%", "").replace("(", "").replace(")", "").replace("$", "").trim());
                                o[key] = val;
                            });
                        });

                        self.payload().map(e => {
                            e.projectId = self.projectNumberValue().toString(), e.projectVersion = self.projectVersionValue().toString()
                        });

                    };

                    reader.onerror = function (ex) {
                    };

                    reader.readAsBinaryString(files[0]);
                };

                ko.computed(c => {
                    if (self.object().length) {
                        localStorage.setItem("procInvData", JSON.stringify({system: self.object()[0].system, targetDate: self.object()[0].targetDeliveryDate}));
                    }
                });

                self.picker_text = ko.observable(app.translate("common.click_or_dragdrop"));

                self.acceptArr = ko.pureComputed(function ()
                {
                    var accept = ".xls,.xlsx";
                    return accept ? accept.split(",") : [];
                }, self);

                self.removePickerBtnAction = function ()
                {
                    self.selected_files([]);
                    document.querySelector("input[type='file']").value = '';
                };

                function getAllServices()
                {
                    app.loading(true);
                    self.servicesArr([]);

                    var payload = {
                        "projectId": self.projectNumberValue().toString(),
                        "versionId": self.projectVersionValue().toString()
                    };

                    var successEMPCbFn = function (data) {
                        app.loading(false);
                        var x = 0;
                        self.servicesArr(data);
                      
                        if (data.length == 0) {
                            self.isDisabledImport(false);
                            self.overHeadDisabled(true);
                            self.uploadVisable(false);

                        } else if (data.length > 0 && self.tableData().length == 0 ) {
                            self.overHeadDisabled(false);
                        }
                    };

                    var failEMPCbFn = function () {
                        app.loading(false);
                    };
                    services.postGeneric(commonhelper.services, payload).then(successEMPCbFn, failEMPCbFn);
                }

                self.getAllOverHeadData = function ()
                {
                    var getSuccss = function (data) {
                        self.allTableData(data);
                      var FinanceData=  data.filter(e=>e.detalis);
                        self.tableData([]);
                        $.each(FinanceData, function (index) {
                            self.tableData.push({
                                costId: FinanceData[index].costId,
                                detalis: FinanceData[index].detalis,
                                rules: FinanceData[index].rules,
                                rate: FinanceData[index].rate,
                                amount: FinanceData[index].amount,
                                value: FinanceData[index].value,
                                costDate: FinanceData[index].costDate,
                                description: FinanceData[index].description,
                                bondrate: "",
                                duration: "",
                                
                            });
                        });
                        self.dataprovider2(new oj.ArrayDataProvider(self.tableData, {keyAttributes: 'projectId'}));
                        if(data.length == 0){
                                self.uploadVisable(false);
                        }
                        var status = [...new Set (data.map(e=>e.status))];                      
                        if(status == "uploaded"){
                            self.uploadOverDisabled(true);
                            self.overHeadDisabled(true); 
                        }                      
                    };
                    var projectCostFail = function () {
                    };
                    var payload = {
                        "versionId": self.projectVersionValue().toString(),
                        "projectId": self.projectNumberValue().toString()
                    };
                    services.addGeneric(commonhelper.getAllRequest, payload).then(getSuccss, projectCostFail);
                };
                
                    //------------- upload total--------***/////
                self.uploadOverAction = function () {
                    app.loading(true);
                    var sys = [...new Set(self.allTableData().map(e => e.costId))];
                    var x = [];
                    for (var i  in sys) {
                        var system = self.allTableData().filter(e => e.costId == sys[i])
                        var allAmount = system.map(ee => (ee.value | ee.amount)).reduce((p, c) => (p || 0) + c);
                        var payload2 = {};
                        payload2.planVersionId = self.projectVersionValue();
                        payload2.data = {
                            "TaskNumber": sys[i],
                            "ResourceName": "Over Heads",
                            "PlanningAmounts": [
                                {
                                    "Currency": "AED",
                                    "RawCostAmounts": allAmount,
                                    "RevenueAmounts": allAmount
                                }
                            ]
                        };
                        x.push(payload2);
                    }
                    var successEMPCbFn = async function (data) {
                        if (data[0].status == "error") {
                            app.loading(false);
                            app.messagesDataProvider.push({severity: "error", summary: data[0].data.toString(), autoTimeout: 0});
                            return;
                        } else {
                            app.loading(false);
                            app.messagesDataProvider.push({severity: "confirmation", summary: "Record Uploaded Successfully", autoTimeout: 0});
                            self.uploadOverDisabled(true);
                            self.overHeadDisabled(true);
                            var updatedIds = self.allTableData().map(e => e.id);
                            for (var i in updatedIds) {
                                var updateSucess = function () {
                                };
                                services.postGeneric(commonhelper.updateStatus, updatedIds[i]).then(updateSucess, app.failEMPCbFn);
                            }
                        }
                    };
                    var failEMPCbFn = function (response) {
                    };
                    services.postGeneric(commonhelper.uploadServiceToInstance, x).then(successEMPCbFn, failEMPCbFn);
                };
              //-----------------------------------------------------------------------------------------------------------------//

                self.cancelDatesBtn = function ()
                {
                    document.querySelector("#ProcPlanDuration").close();
                };

                self.backBtnAction = function ()
                {
                    oj.Router.rootInstance.go('budgetSummry');
                };
                self.overHeadBtn = function ()
                {
                    oj.Router.rootInstance.go('projectOverHead');
                };
                self.costScreendBtn = function ()
                {
                    oj.Router.rootInstance.go('costScreen');
                };


                var getTranslation = oj.Translations.getTranslatedString;

                function initTranslations() {

                    self.projectNumber(getTranslation("reports.projectNumber"));
                    self.projectVersion(getTranslation("reports.projectVersion"));
                    self.exportProLbl(getTranslation("reports.exportProLbl"));
                    self.prProcessingDatelbl(getTranslation("reports.prProcessingDatelbl"));
                    self.PRProcessingTime(getTranslation("reports.PRProcessingTime"));
                    self.ProcPlanDurationLbl(getTranslation("reports.ProcPlanDurationLbl"));
                    self.contingencyTimeLbl(getTranslation("reports.contingencyTimeLbl"));
                    self.shippingTimeLbl(getTranslation("reports.shippingTimeLbl"));
                    self.manufacturingLeadTimeLbl(getTranslation("reports.manufacturingLeadTimeLbl"));
                    self.packingLbl(getTranslation("reports.packingLbl"));
                    self.deliveryLbl(getTranslation("reports.deliveryLbl"));
                    self.targetDeliveryDateLbl(getTranslation("reports.targetDeliveryDateLbl"));
                    self.cabinetAssemblyLbl(getTranslation("reports.cabinetAssemblyLbl"));
                    self.preFatLbl(getTranslation("reports.preFatLbl"));
                    self.fatLbl(getTranslation("reports.fatLbl"));
                    self.fatPunchClearanceLbl(getTranslation("reports.fatPunchClearanceLbl"));
                    self.paymentTermLbl(getTranslation("reports.paymentTermLbl"));
                    self.paymentDate(getTranslation("reports.paymentDate"));
                    self.saveDatesLbl(getTranslation("reports.saveDatesLbl"));
                    self.cancelDatesLbl(getTranslation("reports.cancelDatesLbl"));
                    self.system(getTranslation("reports.system"));
                    self.make(getTranslation("reports.make"));
                    self.supplier(getTranslation("reports.supplier"));
                    self.supplierNoLbl(getTranslation("reports.supplierNoLbl"));
                    self.costId(getTranslation("reports.costId"));
                    self.currency(getTranslation("reports.currency"));
                    self.afterDistotalCostFC(getTranslation("reports.afterDistotalCostFC"));
                    self.exchangeRate(getTranslation("reports.exchangeRate"));
                    self.afterDistotalCostAED(getTranslation("reports.afterDistotalCostAED"));
                    self.beforeDisCostAED(getTranslation("reports.beforeDisCostAED"));
                    self.DisAED(getTranslation("reports.DisAED"));
                    self.DisRate(getTranslation("reports.DisRate"));
                    self.forecostPRDate(getTranslation("reports.forecostPRDate"));
                    self.forecostOrderDate(getTranslation("reports.forecostOrderDate"));
                    self.manufacturingLeadTime(getTranslation("reports.manufacturingLeadTime"));
                    self.contingencyTime(getTranslation("reports.contingencyTime"));
                    self.matReadyWithSupplier(getTranslation("reports.matReadyWithSupplier"));
                    self.shippingTime(getTranslation("reports.shippingTime"));
                    self.ETADSODate(getTranslation("reports.ETADSODate"));
                    self.cabinetAssembly(getTranslation("reports.cabinetAssembly"));
                    self.PreFAT(getTranslation("reports.PreFAT"));
                    self.FAT(getTranslation("reports.FAT"));
                    self.FATPunchClearance(getTranslation("reports.FATPunchClearance"));
                    self.integerationFATDate(getTranslation("reports.integerationFATDate"));
                    self.packing(getTranslation("reports.packing"));
                    self.delivery(getTranslation("reports.delivery"));
                    self.targetDeliveryDate(getTranslation("reports.targetDeliveryDate"));
                    self.MainLbl(getTranslation("reports.MainLbl"));
                    self.weekLbl(getTranslation("reports.weekLbl"));
                    self.dayLbl(getTranslation("reports.dayLbl"));
                    self.submitProLbl(getTranslation("reports.submitProLbl"));
                    self.servicesLbl(getTranslation("reports.servicesLbl"));
                    self.financesCostLbl(getTranslation("reports.financesCostLbl"));
                    self.overHeadLbl(getTranslation("reports.overHeadLbl"));
                    self.costId(getTranslation("reports.costId"));
                    self.Details(getTranslation("reports.Details"));
                    self.rules(getTranslation("reports.rules"));
                    self.amount(getTranslation("reports.amount"));
                    self.value(getTranslation("reports.value"));
                    self.Rate(getTranslation("reports.Rate"));
                    self.servicesBtnLbl(getTranslation("reports.servicesBtnLbl"));
                    self.costScreenLbl(getTranslation("reports.costScreenLbl"));
                    self.item(app.translate("Services.item"));
                    self.description(app.translate("Services.description"));
                    self.biddingEstimate(app.translate("Services.biddingEstimate"));
                    self.purchaseEstimate(app.translate("Services.purchaseEstimate"));
                    self.supplierAED(app.translate("Services.supplierAED"));
                    self.qty(app.translate("Services.qty"));
                    self.unit(app.translate("Services.unit"));
                    self.totalAED(app.translate("Services.totalAED"));
                    self.gpServices(app.translate("Services.gpServices"));
                    self.planType(app.translate("Purchase.planType"));
                    self.revenueAmount(app.translate("Purchase.revenueAmount"));
                    self.costAmount(app.translate("Purchase.costAmount"));
                    self.Description(getTranslation("reports.Description"));
                    self.costDate(getTranslation("reports.costDate"));
                    self.backLbl(getTranslation("reports.backLbl"));
                    self.sequanceLbl(getTranslation("reports.sequanceLbl"));
                    self.taskNumberRef(getTranslation("Services.taskNumberRef"));
                    self.ratePerHours(getTranslation("Services.ratePerHours"));
                    self.totalcost(getTranslation("Services.totalcost"));
                    self.totalHours(getTranslation("Services.totalHours"));
                    self.serviceSummary(getTranslation("Services.serviceSummary"));
                    self.dateFrom(getTranslation("Services.dateFrom"));
                 self.dateTo(getTranslation("Services.dateTo"));
                    
                    self.columnArray([

                        {
                            "headerText": self.sequanceLbl(), "template": "seqTemplate"
                        },
                        {
                            "headerText": self.system(), "field": "system"
                        },
                        {
                            "headerText": self.make(), "field": "make"
                        },
                        {
                            "headerText": self.supplier(), "field": "supplier"
                        },
                        {
                            "headerText": self.costId(), "field": "costId"
                        },
                        {
                            "headerText": self.currency(), "field": "currency"
                        },
                        {
                            "headerText": self.afterDistotalCostFC(), "field": "afterDistotalCostFc"
                        },
                        {
                            "headerText": self.exchangeRate(), "field": "exchangeRate"
                        },
                        {
                            "headerText": self.afterDistotalCostAED(), "field": "afterDistotalCostAED"
                        },
                        {
                            "headerText": self.beforeDisCostAED(), "field": "beforeDistotalCostAED"
                        },
                        {
                            "headerText": self.DisAED(), "field": "discountAED"
                        },
                        {
                            "headerText": self.DisRate(), "field": "discountRate"
                        },
                        {
                            "headerText": self.forecostPRDate(), "field": "children[0].forecostPRDate"
                        },
                        {
                            "headerText": self.PRProcessingTime(), "field": "children[0].prProcessingTime"
                        },
                        {
                            "headerText": self.forecostOrderDate(), "field": "children[0].forecostOrderDate"
                        },
                        {
                            "headerText": self.manufacturingLeadTime(), "field": "children[0].manufacturingLeadTime"
                        },
                        {
                            "headerText": self.contingencyTime(), "field": "children[0].contingencyTime"
                        },
                        {
                            "headerText": self.matReadyWithSupplier(), "field": "children[0].matReadyWithSupplier"
                        },
                        {
                            "headerText": self.shippingTime(), "field": "children[0].shippingTime"
                        },
                        {
                            "headerText": self.ETADSODate(), "field": "children[0].ETADSODate"
                        },
                        {
                            "headerText": self.cabinetAssembly(), "field": "children[0].cabinetAssembly"
                        },
                        {
                            "headerText": self.PreFAT(), "field": "children[0].PreFAT"
                        },
                        {
                            "headerText": self.FAT(), "field": "children[0].FAT"
                        },
                        {
                            "headerText": self.FATPunchClearance(), "field": "children[0].FATPunchClearance"
                        },
                        {
                            "headerText": self.integerationFATDate(), "field": "children[0].integerationFATDate"
                        },
                        {
                            "headerText": self.packing(), "field": "children[0].packing"
                        },
                        {
                            "headerText": self.delivery(), "field": "children[0].delivery"
                        },
                        {
                            "headerText": self.targetDeliveryDate(), "field": "children[0].targetDeliveryDate"
                        },
                        {
                            "headerText": self.paymentTermLbl(), "field": "children[0].paymentTerm"
                        },
                        {
                            "headerText": self.paymentDate(), "field": "children[0].paymentDate"
                        }
                    ]);

                    self.columnArray2([
                        {
                            "headerText": self.costId(), "field": "costId"
                        },
                        {
                            "headerText": self.Details(), "field": "detalis"
                        },
                        {
                            "headerText": self.rules(), "field": "rules"
                        },
                        {
                            "headerText": self.Rate(), "field": "rate"
                        },
                        {
                            "headerText": self.amount(), "field": "amount"
                        },
                        {
                            "headerText": self.value(), "field": "value"
                        },
                        {
                            "headerText": self.costDate(), "field": "costDate"
                        },
                        {
                            "headerText": self.Description(), "field": "description"
                        }
                    ]);

                    self.servicesColumnArray([           
                        {
                            "headerText": self.taskNumberRef(), "field": "taskNumberRef"
                        },
                        {
                            "headerText": self.serviceSummary(), "field": "projectService"
                        },
                        {
                            "headerText":self.ratePerHours(), "field": "rates"
                        },
                        {
                            "headerText": self.totalHours(), "field": "hours"
                        },
                        {
                            "headerText":     self.totalcost(), "field": "costs"
                        },
                        
                        {
                            "headerText": self.dateFrom(), "field": "dateFrom"
                        },
                        {
                            "headerText": self.dateTo(), "field": "dateTo"
                        },
//                        {
//                            "headerText": self.purchaseEstimate(), "field": "purchaseEstimate"
//                        },
//                        {
//                            "headerText": self.supplierAED(), "field": "supplierAED"
//                        },
//                        {
//                            "headerText": self.supplierAED(), "field": "supplierAED_1"
//                        },
//                        {
//                            "headerText": self.supplierAED(), "field": "supplierAED_2"
//                        },
//                        {
//                            "headerText": self.supplierAED(), "field": "supplierAED_3"
//                        },
//                        {
//                            "headerText": self.qty(), "field": "qty"
//                        },
//                        {
//                            "headerText": self.unit(), "field": "unit"
//                        },
//                        {
//                            "headerText": self.totalAED(), "field": "totalAED"
//                        },
//                        {
//                            "headerText": self.gpServices(), "field": "gpServices"
//                        },
//                        {
//                            "headerText": self.totalAED(), "field": "totalAED_1"
//                        }
                    ]);
                }
                initTranslations();
            }

            return procurmentPlanViewModel;
        });

