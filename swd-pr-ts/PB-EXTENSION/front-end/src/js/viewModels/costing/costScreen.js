/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * costScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'xlsx', 'util/commonhelper', 'ojs/ojarraydataprovider',
    'ojs/ojtable', 'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojdialog'
], function (oj, ko, $, app, services, x, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function costScreenContentViewModel() {
        var self = this;
        self.costId = ko.observable();
        self.Details = ko.observable();
        self.rules = ko.observable();
        self.amount = ko.observable();
        self.value = ko.observable();
        self.Rate = ko.observable();
        self.commersialCond=ko.observable();
        self.commersialCond1=ko.observable();
        self.commersialCond2=ko.observable();
        self.commersialCond3=ko.observable();
        self.commersialCond4=ko.observable();
        self.commersialCond5=ko.observable();
        self.commersialCond6=ko.observable();
        self.commersialCond7=ko.observable();
        self.commersialCond8=ko.observable();
        self.commersialCond9=ko.observable();
        self.commersialCond10=ko.observable();
        self.commersialCond10Link=ko.observable();
        self.commersialCond8_1=ko.observable();
        self.commersialCond8_2=ko.observable();
        self.commersialCond8_3=ko.observable();
        self.commersialCond8_4=ko.observable();
        self.commersialCond8_5=ko.observable();
        self.commersialCond9_1=ko.observable();
        self.commersialCond9_2=ko.observable();
        self.commersialCond9_3=ko.observable();
        self.commersialCond9_4=ko.observable();
        self.commersialCond9_5=ko.observable();
        self.commersialCond9_6=ko.observable();
        self.isDisabledProStartVal = ko.observable(true);
        self.isDisabledProEndVal = ko.observable(true);
        self.isDisabledProNameVal = ko.observable(true);
        self.isDisabledProNumVal = ko.observable(true);
        self.isDisabledTotalSalesVal = ko.observable(true);
        self.isDisabledMainOrderVal = ko.observable(true);
        self.isDisabledcostOfSalesVal = ko.observable(true);
        self.isDisabledEquipmentVal = ko.observable(true);
        self.isDisabledOverheadsVal = ko.observable(true);
        self.isDisabledPMVal = ko.observable(true);
        self.isDisabledForecastVal = ko.observable(true);
        self.isDisabledRateVal = ko.observable(true);
        self.projectStartDate = ko.observable();
        self.projectEndDate = ko.observable();
        self.projectStartValue = ko.observable();
        self.projectEndValue = ko.observable();
        self.projectVersionValue = ko.observable();


        self.system = ko.observable();
        self.make = ko.observable();
        self.supplier = ko.observable();
        self.supplierNoLbl = ko.observable();
        self.costId = ko.observable();
        self.currency = ko.observable();
        self.afterDistotalCostFC = ko.observable();
        self.exchangeRate = ko.observable();
        self.afterDistotalCostAED = ko.observable();
        self.beforeDisCostAED = ko.observable();
        self.DisAED = ko.observable();
        self.DisRate = ko.observable();
        self.forecostPRDate = ko.observable();
        self.forecostOrderDate = ko.observable();
        self.manufacturingLeadTime = ko.observable();
        self.contingencyTime = ko.observable();
        self.matReadyWithSupplier = ko.observable();
        self.shippingTime = ko.observable();
        self.ETADSODate = ko.observable();
        self.cabinetAssembly = ko.observable();
        self.PreFAT = ko.observable();
        self.FAT = ko.observable();
        self.FATPunchClearance = ko.observable();
        self.integerationFATDate = ko.observable();
        self.packing = ko.observable();
        self.delivery = ko.observable();
        self.targetDeliveryDate = ko.observable();
        self.PRProcessingTime = ko.observable();
        self.item = ko.observable();
        self.description = ko.observable();
        self.biddingEstimate = ko.observable();
        self.purchaseEstimate = ko.observable();
        self.supplierAED = ko.observable();
        self.qty = ko.observable();
        self.unit = ko.observable();
        self.totalAED = ko.observable();
        self.totalQuotLbl = ko.observable();
        self.projectNameValue = ko.observable();
        self.projectName = ko.observable();
        self.projectNumber = ko.observable();
        self.projectNumberValue = ko.observable();
        self.totalSalesValue = ko.observable();
        self.mainOrderValue = ko.observable();
        self.costOfSalesValue = ko.observable();
        self.equipmentValue = ko.observable();
        self.overheadsValue = ko.observable();
        self.PMValue = ko.observable();
        self.forecastValue = ko.observable();
        self.rateValue = ko.observable();
        self.gpServices = ko.observable();
        self.systemId = ko.observable();
        self.cost = ko.observable();
        self.systemCode = ko.observable();

        self.budgetLbl = ko.observable();
        self.invoiceLbl = ko.observable();
        self.quotationLbl = ko.observable();
        self.cancelDatesLbl = ko.observable();
        self.financeLbl = ko.observable();
        self.totalSalesLbl = ko.observable();
        self.mainOrderLbl = ko.observable();
        self.costOfSalesLbl = ko.observable();
        self.equipmentLbl = ko.observable();
        self.overheadsLbl = ko.observable();
        self.projectStartDateLbl = ko.observable();
        self.PMLbl = ko.observable();
        self.forecastGrossProfitLbl = ko.observable();
        self.rateLbl = ko.observable();
        self.backLbl = ko.observable();
        self.paymentTermLbl = ko.observable();
        self.paymentDate = ko.observable();
        self.totalMaterialLbl = ko.observable();
        self.total = ko.observable();
        self.object = ko.observableArray([]);
        self.columnArray = ko.observableArray();
        self.columnArray2 = ko.observableArray();
        self.columnArraySer = ko.observableArray();
        self.columnArrayOver = ko.observableArray();
        self.columnArrayPro = ko.observableArray();
        self.serviceTabelData = ko.observableArray([]);
        self.servicesArr = ko.observableArray([]);
        self.serArr = ko.observableArray([]);
        self.totalServiceVal = ko.observable('00.00');
        self.totalProcurmentVal = ko.observable('00.00');
        self.totaloverHeadVal = ko.observable('00.00');
        self.totalQuotationVal = ko.observable('00.00');
        self.totalVal = ko.observable('00.00');
        self.totalMaterialVal = ko.observable('00.00');
        self.totalManpowerVal = ko.observable('00.00');

        self.procurmentTabelData = ko.observableArray([]);
        self.dataSourcePro = new oj.ArrayDataProvider(self.procurmentTabelData);

        self.dataSourceSer = new oj.ArrayDataProvider(self.servicesArr);

        self.overHeadTabelData = ko.observableArray();
        self.dataSourceOver = new oj.ArrayTableDataSource(self.overHeadTabelData);

        self.dataSource = new oj.ArrayTableDataSource(self.object);
        self.dataSource22 = new oj.ArrayDataProvider(self.serArr, {keyAttributes: 'id'});
        self.optionDataArray=ko.observableArray();
        self.dataSourceOption = new oj.ArrayTableDataSource(self.optionDataArray, {keyAttributes: 'id'});

            self.cashFlow=ko.observable();
        self.costScreenLbl=ko.observable();
        self.proPlanLbl=ko.observable();
        self.serviceCostLbl=ko.observable();
        self.overHeadLbl=ko.observable();
        self.descriptionSystem=ko.observable();
        self.descriptionManPower=ko.observable();
        self.manpowrTableLbl=ko.observable();
        self.priceAED=ko.observable();
        self.proNumber=ko.observable();
        self.optionLbl=ko.observable();
        self.amountLbl=ko.observable();
        self.optionValue=ko.observable();
        self.optionAmountValue=ko.observable();
       self.selectedIdToDelete=ko.observable();
        self.optionColumsArray=ko.observableArray();
        self.deleteDisable=ko.observable(true);
        
        self.connected = function () {

            var myData = oj.Router.rootInstance.retrieve();

            if (myData) {
                self.projectNumberValue(myData.projectID);
                self.projectVersionValue(myData.versionID);
                self.projectStartValue(myData.projectStartDate);
                self.projectEndValue(myData.projectEndDate);
                self.projectNameValue(myData.projectName);
                self.proNumber(myData.projectNumber);
                self.getProcurment();
                self.getOverHeadData();
                self.getAllServices();
                getOptionRequests();
               
            }
        };

        self.tableSeleListenerPro = function () {

        };
        self.tableSeleListenerSer = function () {

        };
        self.tableSeleListenerOver = function () {

        };
        self.tableSeleListener = function () {

        };
        self.tableSeleListener2 = function () {

        };

        self.budgetBtnAction = function () {
            
            var costOfSales = 0;
            self.equipmentValue(self.totalProcurmentVal());
            self.overheadsValue(self.totaloverHeadVal());
            self.PMValue(self.totalServiceVal());
            var mainOrder = parseInt(self.totalMaterialVal()) + parseInt(self.PMValue().replace(/,/g, ""));
            self.mainOrderValue(numberWithCommas(mainOrder));
            self.totalSalesValue(numberWithCommas(mainOrder));
            costOfSales = parseFloat(self.equipmentValue().replace(/,/g, "")) + parseFloat(self.overheadsValue().replace(/,/g, "")) + parseFloat(self.PMValue().replace(/,/g, ""));
            self.costOfSalesValue(numberWithCommas(costOfSales));
            var forecost = parseFloat(self.mainOrderValue().replace(",", '')) - parseFloat(self.costOfSalesValue().replace(",", ''));
            self.forecastValue(numberWithCommas(forecost));
            var rate = ((parseInt(self.forecastValue().replace(",", '')) / parseInt(self.mainOrderValue().replace(",", ''))) * 100).toFixed(2);
            self.rateValue(numberWithCommas(rate));
            document.querySelector('#budgetDialog').open();
        };

        ko.computed(c => {
            self.equipmentValue(self.totalProcurmentVal());
            self.PMValue(self.totalServiceVal());
            var mainOrder = parseInt(self.equipmentValue().replace(",", '')) + parseInt(self.PMValue().replace(",", ''));
            if (mainOrder) {
                localStorage.setItem("invoiceStoredData", JSON.stringify({procDate: JSON.parse(localStorage.getItem("procInvData")), amountAED: numberWithCommas(mainOrder)}));
            }
        });


        self.getOverHeadData = function () {

            var payload = {
                "versionId": self.projectVersionValue().toString(),
                "projectId": self.projectNumberValue().toString()
            };

            var getSuccss = function (data) {

                if (data.length == 0)
                    return;
                
                var sys = [...new Set(data.map(e => e.costId))]
                sys.forEach((name, i) => {
                    var system = data.filter(e => e.costId == sys[i])

                    var amount = system.length > 1 ? (system.reduce((p, c) => {
                        return (((p.amount || 0) - 0 || 0) + ((p.value || 0) - 0 || 0)) + (((c.amount || 0) - 0 || 0) + ((c.value || 0) - 0 || 0));
                    })) : (system[0].amount == 0 ? system[0].value : system[0].amount);

                    var obj2 =
                            {
                                "systemId": sys[i],
                                "system": system[0].description,
                                "cost": amount
                            };

                    self.overHeadTabelData.push(obj2);
                });

                var countCost = 0;
                countCost = parseInt(self.overHeadTabelData().map(e => e.cost).reduce((p, c) => (((p - 0) || 0) + (c - 0))));
                self.totaloverHeadVal(numberWithCommas(countCost));
            };
            var projectCostFail = function () {

            };
            services.addGeneric(commonhelper.getAllRequest, payload).then(getSuccss, projectCostFail);
        };

        self.getAllServices = function ()
        {
            app.loading(true);
            self.servicesArr([]);
            self.serArr([]);

            var payload = {
                "projectId": self.projectNumberValue().toString(),
                "versionId": self.projectVersionValue().toString()
            };

            var successEMPCbFn = function (data) {
                app.loading(false);

                var x = 0;
                data.forEach(e => e.Item = ++x);

                self.servicesArr(data);
                self.serArr(self.servicesArr());

                var countCost = 0;
                for (var i = 0; i < self.servicesArr().length; i++) {
                    if (typeof (self.servicesArr()[i].costs)!= "undefined")
                    if (isNaN(parseInt(self.servicesArr()[i].costs.replace(",", '')))) {
                    } else {
                        countCost = parseInt(self.servicesArr()[i].costs.replace(",", '')) + countCost;
                    }
                }
                self.totalServiceVal(numberWithCommas(countCost));
                self.totalManpowerVal(countCost);
                if (services.length == 0) {
                    app.messagesDataProvider.push({severity: "error", summary: "No Services Available!", autoTimeout: 0});
                }
            };

            var failEMPCbFn = function () {
                app.loading(false);
            };
            services.postGeneric(commonhelper.services, payload).then(successEMPCbFn, failEMPCbFn);
        };

        self.getProcurment = function ()
        {
            var payload = {
                "projectId": self.projectNumberValue().toString(),
                "versionId": self.projectVersionValue().toString()
            };

            var successEMPCbFn = function (data) {

                var sys = [...new Set(data.data.map(e => e.system))];            
                sys.forEach((name, i) => {
                    var system = data.data.filter(e => e.system == sys[i])
                   // var costId = [...new Set(data.data.filter(e=> e.system == sys[i]).map(e=> e.costId)) ]
                    var amount = system.map(e => e.afterDistotalCostAED).reduce((p, c) => (((p - 0) || 0) + (c - 0)));
                    var obj2 =
                            {
                                "systemId": system[0].costId.includes(".")?system[0].costId.substring(0,system[0].costId.indexOf(".")): system[0].costId,
                                "system": system[0].system,
                                "cost": amount
                            };
                    self.procurmentTabelData.push(obj2);
                });
                var tot2 = self.procurmentTabelData().map(e => parseInt(e.cost)).reduce((p, c) => (p || 0) + c);
                self.totalProcurmentVal(numberWithCommas(tot2.toFixed(1)));
            };
            var failEMPCbFn = function () {
            };
            services.postGeneric(commonhelper.getAllProcurment, payload).then(successEMPCbFn, failEMPCbFn);
        };

        self.getMyData = function () {

            var successEMPCbFn = function (SheetData) {
                app.loading(false);
             // var sys = [...new Set(SheetData.data.map(e => e.COST_ID))]; // seperate cost id
                var sys = [... new Set (SheetData.data.map(e=>e.COST_ID.includes(".")?(e.COST_ID).substring(0,e.COST_ID.indexOf(".")):e.COST_ID))] // parent cost id
                
               
                sys.forEach((name, i) => {
                    var payload = {
                        "costId": sys[i],
                        "projectId":self.projectNumberValue().toString()
                    };
                    var successEMPCbFn = function (data) {
                        app.loading(false);
                        var system = SheetData.data.filter(e =>e.COST_ID.includes(".")?(e.COST_ID).substring(0,e.COST_ID.indexOf("."))== sys[i]:e.COST_ID == sys[i]); // parent cost id
                     // var system = SheetData.data.filter(e => e.COST_ID == sys[i]);// seperate cost id
                        var amount = system.map(e => e.TOTAL_COST_AED_DUBAI).reduce((p, c) => ((((p - 0) || null) || 0) + ((c - 0) || 0)).toFixed(1));
                        var obj =
                                {
                                    "seq": sys[i],
                                    "system": data[0].systemCode,
                                    "amount": amount
                                };
                        self.object.push(obj);

                        var totalAmount = self.object().map(e => e.amount).reduce((p, c) => ((((p - 0) || null) || 0) + ((c - 0) || 0)));
                 
                        self.totalMaterialVal(Math.round(totalAmount));
                    };
                    var failEMPCbFn = function () {
                        app.loading(false);
                    };
                    services.postGeneric(commonhelper.systemCode, payload).then(successEMPCbFn, failEMPCbFn);
                });
            };
            var failEMPCbFn = function () {
                app.loading(false);
            };
            services.getGeneric("versions/view/" + self.projectNumberValue() + "/" + self.projectVersionValue()).then(successEMPCbFn, failEMPCbFn);
        };
        self.invoiceBtnAction = function () {
            oj.Router.rootInstance.go('invoiceSummary');
        };
        
        self.quotationBtnAction = function () {
            self.object([]);
             self.getMyData();
            document.querySelector('#quatationDialog').open();        
        };
        
        self.backBtnAction = function () {
            oj.Router.rootInstance.go('procurmentPlan');
        };

        self.cancelDatesBtn = function () {
            document.querySelector('#budgetDialog').close();
        };

        self.cancelQuotationBtn = function () {
            document.querySelector('#quatationDialog').close();
        };

        self.BackBtnAction = function () {
            oj.Router.rootInstance.go('procurmentPlan');
        };
        self.cashFlowAction  = function () {

            oj.Router.rootInstance.go('cashFlow');
        };


        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        self.BackBtnAction = function () {

        };
        var getOptionRequests = function () {

            var payload = {
                "projectVersion": self.projectVersionValue().toString()
            };
            var getSuccss = function (data) {
                self.optionDataArray(data)
            };
            services.addGeneric(commonhelper.getoptions, payload).then(getSuccss, app.failEMPCbFn);
        }
        self.resetConditoinField=function(){
             self.optionValue('')
             self.optionAmountValue(0)
        }
        self.addCondition = function () {
            var payload = {

                "projectVersion": self.projectVersionValue().toString(),
                "options": self.optionValue(),
                "amount": self.optionAmountValue().toString(),
            };
            var getSuccss = function (data) {
                getOptionRequests();
                app.messagesDataProvider.push({severity: "confirmation", summary: "Option Added", autoTimeout: 0});
                self.resetConditoinField();
            };
            services.addGeneric(commonhelper.addOptions, payload).then(getSuccss, app.failEMPCbFn);
        };
        self.deletecondition = function () {
            var deleteSucess = function (data) {
                app.messagesDataProvider.push({severity: "confirmation", summary: "Option Deleted", autoTimeout: 0});
                getOptionRequests();
                self.deleteDisable(true);
                self.resetConditoinField();
            };
            services.getGeneric('optionsDes/deleteOption/' + self.selectedIdToDelete().toString()).then(deleteSucess);
        };
        self.optionTableSelection = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            if (!currentRow)
                return;
            var row = self.optionDataArray()[currentRow['rowIndex']];
            self.selectedIdToDelete(row.id)
            self.deleteDisable(false);

        };

        var getTranslation = oj.Translations.getTranslatedString;
        function initTranslations() {
            self.commersialCond(getTranslation("costing.commersialCond"));
            self.commersialCond1(getTranslation("costing.commersialCond1"));
            self.commersialCond2(getTranslation("costing.commersialCond2"));
            self.commersialCond3(getTranslation("costing.commersialCond3"));
            self.commersialCond4(getTranslation("costing.commersialCond4"));
            self.commersialCond5(getTranslation("costing.commersialCond5"));
            self.commersialCond6(getTranslation("costing.commersialCond6"));
            self.commersialCond7(getTranslation("costing.commersialCond7"));
            self.commersialCond8(getTranslation("costing.commersialCond8"));
            self.commersialCond8_1(getTranslation("costing.commersialCond8_1"));
            self.commersialCond8_2(getTranslation("costing.commersialCond8_2"));
            self.commersialCond8_3(getTranslation("costing.commersialCond8_3"));
            self.commersialCond8_4(getTranslation("costing.commersialCond8_4"));
            self.commersialCond8_5(getTranslation("costing.commersialCond8_5"));
            self.commersialCond9(getTranslation("costing.commersialCond9"));
            self.commersialCond9_1(getTranslation("costing.commersialCond9_1"));
            self.commersialCond9_2(getTranslation("costing.commersialCond9_2"));
            self.commersialCond9_3(getTranslation("costing.commersialCond9_3"));
            self.commersialCond9_4(getTranslation("costing.commersialCond9_4"));
            self.commersialCond9_5(getTranslation("costing.commersialCond9_5"));
            self.commersialCond9_6(getTranslation("costing.commersialCond9_6"));
            self.commersialCond10(getTranslation("costing.commersialCond10"));
            self.commersialCond10Link(getTranslation("costing.commersialCond10Link"));
            self.projectNumber(getTranslation("reports.projectNumber"));
            self.projectName(getTranslation("reports.projectName"));
            self.projectStartDateLbl(getTranslation("reports.projectStartDateLbl"));
            self.projectEndDate(getTranslation("reports.projectEndDate"));
            self.financeLbl(getTranslation("costing.financeLbl"));
            self.totalSalesLbl(getTranslation("costing.totalSalesLbl"));
            self.mainOrderLbl(getTranslation("costing.mainOrderLbl"));
            self.costOfSalesLbl(getTranslation("costing.costOfSalesLbl"));
            self.equipmentLbl(getTranslation("costing.equipmentLbl"));
            self.overheadsLbl(getTranslation("costing.overheadsLbl"));
            self.PMLbl(getTranslation("costing.PMLbl"));
            self.forecastGrossProfitLbl(getTranslation("costing.forecastGrossProfitLbl"));
            self.rateLbl(getTranslation("costing.rateLbl"));
            self.system(getTranslation("reports.system"));
            self.make(getTranslation("reports.make"));
            self.supplier(getTranslation("reports.supplier"));
            self.supplierNoLbl(getTranslation("reports.supplierNoLbl"));
            self.costId(getTranslation("reports.costId"));
            self.currency(getTranslation("reports.currency"));
            self.afterDistotalCostFC(getTranslation("reports.afterDistotalCostFC"));
            self.exchangeRate(getTranslation("reports.exchangeRate"));
            self.afterDistotalCostAED(getTranslation("reports.afterDistotalCostAED"));
            self.beforeDisCostAED(getTranslation("reports.beforeDisCostAED"));
            self.DisAED(getTranslation("reports.DisAED"));
            self.DisRate(getTranslation("reports.DisRate"));
            self.forecostPRDate(getTranslation("reports.forecostPRDate"));
            self.forecostOrderDate(getTranslation("reports.forecostOrderDate"));
            self.manufacturingLeadTime(getTranslation("reports.manufacturingLeadTime"));
            self.contingencyTime(getTranslation("reports.contingencyTime"));
            self.matReadyWithSupplier(getTranslation("reports.matReadyWithSupplier"));
            self.shippingTime(getTranslation("reports.shippingTime"));
            self.ETADSODate(getTranslation("reports.ETADSODate"));
            self.cabinetAssembly(getTranslation("reports.cabinetAssembly"));
            self.PreFAT(getTranslation("reports.PreFAT"));
            self.FAT(getTranslation("reports.FAT"));
            self.FATPunchClearance(getTranslation("reports.FATPunchClearance"));
            self.integerationFATDate(getTranslation("reports.integerationFATDate"));
            self.packing(getTranslation("reports.packing"));
            self.delivery(getTranslation("reports.delivery"));
            self.targetDeliveryDate(getTranslation("reports.targetDeliveryDate"));
            self.paymentTermLbl(getTranslation("reports.paymentTermLbl"));
            self.paymentDate(getTranslation("reports.paymentDate"));
            self.costId(getTranslation("reports.costId"));
            self.Details(getTranslation("reports.Details"));
            self.rules(getTranslation("reports.rules"));
            self.amount(getTranslation("reports.amount"));
            self.value(getTranslation("reports.value"));
            self.Rate(getTranslation("reports.Rate"));
            self.total(getTranslation("reports.total"));
            self.totalQuotLbl(getTranslation("costing.totalQuotLbl"));
            self.budgetLbl(getTranslation("costing.budgetLbl"));
            self.invoiceLbl(getTranslation("costing.invoiceLbl"));
            self.quotationLbl(getTranslation("costing.quotationLbl"));
            self.cancelDatesLbl(getTranslation("reports.cancelDatesLbl"));
            self.item(app.translate("Services.item"));
            self.description(app.translate("Services.description"));
            self.biddingEstimate(app.translate("Services.biddingEstimate"));
            self.purchaseEstimate(app.translate("Services.purchaseEstimate"));
            self.supplierAED(app.translate("Services.supplierAED"));
            self.qty(app.translate("Services.qty"));
            self.unit(app.translate("Services.unit"));
            self.totalAED(app.translate("Services.totalAED"));
            self.gpServices(app.translate("Services.gpServices"));
            self.backLbl(app.translate("reports.backLbl"));
            self.systemId(app.translate("costing.systemId"));
            self.cost(app.translate("costing.cost"));
            self.cashFlow(app.translate("costing.cashFlow"));
            self.costScreenLbl(app.translate("costing.costScreenLbl"));
            self.proPlanLbl(app.translate("costing.proPlanLbl"));
            self.serviceCostLbl(app.translate("costing.serviceCostLbl"));
            self.overHeadLbl(app.translate("costing.overHeadLbl"));
            self.descriptionSystem(app.translate("costing.descriptionSystem"));
            self.descriptionManPower(app.translate("costing.descriptionManPower"));
            self.manpowrTableLbl(app.translate("costing.manpowrTableLbl"));
            self.priceAED(app.translate("costing.priceAED"));
            self.optionLbl(app.translate("costing.optionLbl"));
            self.amountLbl(app.translate("costing.amountLbl"));
         


            self.columnArrayPro([

                {
                    "headerText": self.systemId(), "field": "systemId"
                },
                {
                    "headerText": self.system(), "field": "system"
                },
                {
                    "headerText": self.cost(), "field": "cost"
                }

            ]);
//            self.columnArrayPro([
//                {
//                    "headerText": self.system(), "field": "system"
//                },
//                {
//                    "headerText": self.make(), "field": "make"
//                },
//                {
//                    "headerText": self.supplier(), "field": "supplier"
//                },
//                {
//                    "headerText": self.costId(), "field": "costId"
//                },
//                {
//                    "headerText": self.currency(), "field": "currency"
//                },
//                {
//                    "headerText": self.afterDistotalCostFC(), "field": "afterDistotalCostFc"
//                },
//                {
//                    "headerText": self.exchangeRate(), "field": "exchangeRate"
//                },
//                {
//                    "headerText": self.afterDistotalCostAED(), "field": "afterDistotalCostAED"
//                },
//                {
//                    "headerText": self.beforeDisCostAED(), "field": "beforeDistotalCostAED"
//                },
//                {
//                    "headerText": self.DisAED(), "field": "discountAED"
//                },
//                {
//                    "headerText": self.DisRate(), "field": "discountRate"
//                },
//                {
//                    "headerText": self.forecostPRDate(), "field": "forecastPrDate"
//                },
//                {
//                    "headerText": self.PRProcessingTime(), "field": "prProcessingTime"
//                },
//                {
//                    "headerText": self.forecostOrderDate(), "field": "forecostOrderDate"
//                },
//                {
//                    "headerText": self.manufacturingLeadTime(), "field": "manufactingLeadTime"
//                },
//                {
//                    "headerText": self.contingencyTime(), "field": "cantingencyTime"
//                },
//                {
//                    "headerText": self.matReadyWithSupplier(), "field": "matReadyWith"
//                },
//                {
//                    "headerText": self.shippingTime(), "field": "shippingTime"
//                },
//                {
//                    "headerText": self.ETADSODate(), "field": "etaDsoDate"
//                },
//                {
//                    "headerText": self.cabinetAssembly(), "field": "cabinetAssembly"
//                },
//                {
//                    "headerText": self.PreFAT(), "field": "preFat"
//                },
//                {
//                    "headerText": self.FAT(), "field": "fat"
//                },
//                {
//                    "headerText": self.FATPunchClearance(), "field": "fatPunchClearance"
//                },
//                {
//                    "headerText": self.integerationFATDate(), "field": "integerationFatDate"
//                },
//                {
//                    "headerText": self.packing(), "field": "packing"
//                },
//                {
//                    "headerText": self.delivery(), "field": "delivery"
//                },
//                {
//                    "headerText": self.targetDeliveryDate(), "field": "targetDeliveryDate"
//                }
//                ,
//                {
//                    "headerText": self.paymentTermLbl(), "field": "paymentTerm"
//                },
//                {
//                    "headerText": self.paymentDate(), "field": "paymentDate"
//                }
//            ]);
            self.columnArraySer([

                {
                    "headerText": self.item(), "field": "Item"
                },
                {
                    "headerText": self.system(), "field": "projectService"
                },
                {
                    "headerText": self.cost(), "field": "costs"
                }

//                {
//                    "headerText": self.item(), "field": "Item"
//                },
//                {
//                    "headerText": self.description(), "field": "description"
//                },
//                {
//                    "headerText": self.biddingEstimate(), "field": "biddingEstimate"
//                },
//                {
//                    "headerText": self.purchaseEstimate(), "field": "purchaseEstimate"
//                },
//                {
//                    "headerText": self.supplierAED(), "field": "supplierAED"
//                },
//                {
//                    "headerText": self.supplierAED(), "field": "supplierAED_1"
//                },
//                {
//                    "headerText": self.supplierAED(), "field": "supplierAED_2"
//                },
//                {
//                    "headerText": self.supplierAED(), "field": "supplierAED_3"
//                },
//                {
//                    "headerText": self.qty(), "field": "qty"
//                },
//                {
//                    "headerText": self.unit(), "field": "unit"
//                },
//                {
//                    "headerText": self.totalAED(), "field": "totalAED"
//                },
//                {
//                    "headerText": self.gpServices(), "field": "gpServices"
//                },
//                {
//                    "headerText": self.totalAED(), "field": "totalAED_1"
//                }
            ]);

            self.columnArrayOver([

                {
                    "headerText": self.systemId(), "field": "systemId"
                },
                {
                    "headerText": self.system(), "field": "system"
                },
                {
                    "headerText": self.cost(), "field": "cost"
                }
//                {
//                    "headerText": self.costId(), "field": "costId"
//                },
//                {
//                    "headerText": self.Details(), "field": "detalis"
//                },
//                {
//                    "headerText": self.rules(), "field": "rules"
//                },
//                {
//                    "headerText": self.Rate(), "field": "rate"
//                },
//                {
//                    "headerText": self.amount(), "field": "amount"
//                },
//                {
//                    "headerText": self.value(), "field": "value"
//                }
            ]);

            self.columnArray([
                {
                    "headerText": self.item(), "field": "seq"
                },
                {
                    "headerText": self.descriptionSystem(), "field": "system"
                },
                {
                    "headerText": self.priceAED(), "field": "amount"
                }
            ]);

            self.columnArray2([
                {
                    "headerText": self.item(), "field": "Item"
                },
                {
                    "headerText": self.descriptionManPower(), "field": "projectService"
                },
                {
                    "headerText": self.priceAED(), "field": "costs"
                },
            ]);
        self.optionColumsArray([
                {
                    "headerText": "Option", "field": "options"
                },
                {
                    "headerText": "Amount", "field": "amount"
                },             
            ]);


        }
        initTranslations();
    }

    return costScreenContentViewModel;
});
