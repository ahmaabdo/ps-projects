/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * costCategory module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog',
    'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, services, commonhelper) {
            /**
             * The view model for the main content view template
             */
            function costCategoryContentViewModel() {
                var self = this;

                self.backBtnLbl = ko.observable();
                self.MainLbl = ko.observable();
                self.addLbl = ko.observable();
                self.searchLbl = ko.observable();
                self.costId = ko.observable();
                self.systemCode = ko.observable();
                self.description = ko.observable();
                self.enable = ko.observable();
                self.costType = ko.observable();
                self.isDisabledDesc = ko.observable(false)
                self.searchDisable = ko.observable(false)
                self.costCategoryColumnArray = ko.observableArray();
                self.categoryArr = ko.observableArray([]);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.categoryArr,{idAttribute: 'seq'}));
                self.groupValid = ko.observable();
                self.isRequired = ko.observable(false);
                self.costTypeArr = ko.observableArray([]);
                self.arrayData = ko.observableArray();
                self.masterData = ko.observableArray();  
     
                self.costIdVal = ko.observable('');
                self.systemCodeVal = ko.observable('');
                self.costTypeVal = ko.observable();
                self.descriptionVal = ko.observable('');
                self.isDisabled = ko.observable(false);
                self.placeholder = ko.observable();
                self.projectId= ko.observable();
                var costArray = [{
                   label: "Manpower",value: "Manpower"}, { label: "Material",value: "Material"}
                  ,{label: "Overhead",value: "Overhead"}];
                    self.costTypeArr(costArray);
                self.connected = function () {

                    app.loading(false);
                    var RetrivedData= oj.Router.rootInstance.retrieve();
                    self.projectId(RetrivedData);            
                    getAllRecords();
                    
                };
   //----------------------------------Get All Data -----------------------------------------
                var getAllRecords = function () {
                    var getSuccss = function (data) {
                        if(data.length == 0){
                                self.searchDisable(true);
              app.messagesDataProvider.push({severity: "warning", summary: "No System Added ", autoTimeout: 0});
                        }else{                           
                      self.searchDisable(false)
                        var seq = 0;
                        data.forEach(e =>
                            e.seq = ++seq)
                        self.arrayData().push(...data);
                        self.categoryArr(data)
                        self.masterData(data);
                        }
                    };
                    var failCbFn = function () {

                    };
                    var payload={
                        projectId:self.projectId().toString()
                    }
                    services.addGeneric(commonhelper.getCostCategory,payload).then(getSuccss, failCbFn);
                };
                self.tableSelectionListener = function (event) {

                };
        //----------------------------------Search Button----------------------------------------
                self.searchBtnAction = function () {                 
                    self.categoryArr([])
//                    cond = function (e) {
//                        var res =
//                               (self.costIdVal() ? e.costId === self.costIdVal() : true)
//                                || (self.systemCodeVal() ? e.systemCode === self.systemCodeVal() : true)
//                                || (self.costTypeVal() ? e.costType === self.costTypeVal() : true)
//                                || (self.descriptionVal() ? e.description === self.descriptionVal() : true)
//
//                        return res;
//                    };
                     var arrSearch = self.arrayData().filter(e =>
                        (self.costIdVal() ? e.costId === self.costIdVal() : true)
                                && (self.systemCodeVal() ? e.systemCode === self.systemCodeVal() : true)
                                && (self.costTypeVal() ? e.costType === self.costTypeVal() : true)
                    );
                    //var arrSearch = self.arrayData().filter(e => cond(e));
                    self.categoryArr(arrSearch);
                };
      //----------------------------------Add Button-----------------------------------
                self.addBtnAction = function () {
                    if(self.systemCodeVal() == undefined || self.costIdVal() == undefined ||self.systemCodeVal() == ''){
                         app.messagesDataProvider.push({severity: "error", summary: "Please Fill Fields", autoTimeout: 0});
                         return
                    }
                    self.isRequired(true);
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var idArray = self.categoryArr().map(e => e.costId);
                    var systemCodeArray = self.categoryArr().map(e => e.systemCode);
//                    for (var i in self.categoryArr()) {
//                        if (self.costIdVal() == idArray[i]) {  
//                            app.messagesDataProvider.push({severity: "error", summary: "Cost Id already exist", autoTimeout: 0});
//                            return;
//                        } else if (self.systemCodeVal()) {
//                            app.messagesDataProvider.push({severity: "error", summary: "system code already exist", autoTimeout: 0});
//                            return;
//                        }
//                    }
                    app.loading(true);
                    var getSuccss = function (data) {
                        app.loading(false);
                       app.messagesDataProvider.push({severity: "info", summary: "Record added successfully ", autoTimeout: 0});
                        self.costTypeVal('');
                        self.systemCodeVal('');
                        self.descriptionVal('');
                        self.costIdVal('');
                        self.placeholder("Please Select");
                        getAllRecords();
                    };
                    var failCbFn = function () {
                         app.messagesDataProvider.push({severity: "info", summary: "Record Not Added  ", autoTimeout: 0});
                    };
                    var payload = {
                        "costType": self.costTypeVal(),
                        "systemCode": self.systemCodeVal(),
                        "description": self.descriptionVal(),
                        "costId": self.costIdVal(),
                        "projectId":self.projectId()
                    };   
                    services.addGeneric(commonhelper.insertCostCategory, payload).then(getSuccss, failCbFn);
                };
          //--------------------------------------------Reset and Back------------------------------
                self.backBtnAction = function () {
                oj.Router.rootInstance.go('budgetSummry');
                };
                self.resetButtonAction = function () {
                    self.costTypeVal('')
                    self.systemCodeVal('')
                    self.descriptionVal('')
                    self.costIdVal('')
                    self.placeholder("Please Select");
                    self.categoryArr([])
                    self.categoryArr(self.masterData())

                };
                function initTranslations() {
                    self.MainLbl(app.translate("costCategory.MainLbl"));
                    self.addLbl(app.translate("costCategory.addLbl"));
                    self.searchLbl(app.translate("costCategory.searchLbl"));
                    self.backBtnLbl(app.translate("costCategory.backBtnLbl"));
                    self.costId(app.translate("costCategory.costId"));
                    self.systemCode(app.translate("costCategory.systemCode"));
                    self.description(app.translate("costCategory.description"));
                    self.enable(app.translate("costCategory.enable"));
                    self.costType(app.translate("costCategory.costType"));
                    self.placeholder(app.translate("costCategory.placeholder"));

                    self.costCategoryColumnArray([
                        {
                            "headerText": self.costId(), "field": "costId"
                        },
                        {
                            "headerText": self.systemCode(), "field": "systemCode"
                        },
                        {
                            "headerText": self.description(), "field": "description"
                        },
                        {
                            "headerText": self.enable(), "field": "rate"
                        },
                        {
                            "headerText": self.costType(), "field": "costType"
                        }

                    ]);
                }
                initTranslations();
            }

            return costCategoryContentViewModel;
        });
