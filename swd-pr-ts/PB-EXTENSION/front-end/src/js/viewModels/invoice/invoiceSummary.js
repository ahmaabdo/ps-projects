define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'ojs/ojdatetimepicker',
    'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojtable', 'ojs/ojdialog', 'ojs/ojselectcombobox'],
        function (oj, ko, $, app, services) {

            function invoiceSummaryViewModel() {
                var self = this;
                self.MainLbl = ko.observable();
                self.invoiceColumnArray = ko.observableArray();
                self.number = ko.observable();
                self.milestone = ko.observable();
                self.invData = ko.observable();
                self.deleteBtnStatus = ko.observable(true);
                self.pmtTerm = ko.observable();
                self.collectionDate = ko.observable();
                self.selectedRow = ko.observable();
                self.percent = ko.observable();
                self.milestonePercent = ko.observable();
                self.amountAED = ko.observable();
                self.invoiceBtnCreateLbl = ko.observable();
                self.invoiceDeleteBtnLbl = ko.observable();
                self.cancleDialogLbl = ko.observable();
                self.createDialogLbl = ko.observable();
                self.deleteConfirmMsg = ko.observable();
                self.deleteInvoiceLbl = ko.observable();
                self.backBtnLbl = ko.observable();
                self.invoiceRetrievedData = ko.observable(JSON.parse(localStorage.getItem('invoiceStoredData')));
                self.invoiceArr = ko.observableArray([]);
                self.dateConverter = ko.observable(app.dateConverter());

                self.mileStonesArr = ko.observableArray([
                    {value: "material", label: "Material delivery and inspection approval from Engineer/Consultants", content: "% against Material delivery and inspection approval from Engineer/Consultants - "},
                    {value: "installation", label: "Installation and approval from the Engineer", content: "% on Installation and approval from the Engineer - "},
                    {value: "testing", label: "Testing & Commissioning", content: "% on Testing & Commissioning - "},
                    {value: "handingOver", label: "Handing Over systems to Engineer", content: "% on Handing Over systems to Engineer - "}
                ]);

                self.invoiceData = {
                    srNo: ko.observable(),
                    milestoneDes: ko.observable(),
                    invDate: ko.observable(app.convertISOToLocalDate(new Date())),
                    collectionDate: ko.observable(app.convertISOToLocalDate(new Date())),
                    pmtTerm: ko.observable(0),
                    invDateDays: ko.observable(0),
                    percent: ko.observable(100),
                    amountAED: ko.observable(),
                    projectVer: ko.observable(),
                    projectId: ko.observable()
                };

                function clearContent() {
                    self.invoiceData.pmtTerm(0);
                    self.invoiceData.invDateDays(0);
                    self.invoiceData.percent(100);
                }

                ko.computed(parm => {
                    if (self.invoiceArr().length) {
                        self.invoiceData.srNo(Number(self.invoiceArr()[self.invoiceArr().length - 2].srNo) + 1);
                        self.invoiceData.invDate(app.convertISOToLocalDate(self.invoiceArr()[self.invoiceArr().length - 2].invDate));
                        (self.invoiceRetrievedData().amountAED).includes(",") ?
                                self.invoiceData.amountAED(Number((self.invoiceRetrievedData().amountAED).replace(",", ""))) : self.invoiceData.amountAED(Number((self.invoiceRetrievedData().amountAED)));
                    } else {
                        self.invoiceData.srNo(1);

                        if (self.invoiceRetrievedData()) {
                            self.invoiceData.invDate(app.isValidDate(new Date(self.invoiceRetrievedData().procDate.targetDate)) ?
                                    app.convertISOToLocalDate(new Date(self.invoiceRetrievedData().procDate.targetDate)) : app.convertISOToLocalDate(new Date()));

                            (self.invoiceRetrievedData().amountAED).includes(",") ?
                                    self.invoiceData.amountAED(Number((self.invoiceRetrievedData().amountAED).replace(",", ""))) : self.invoiceData.amountAED(Number((self.invoiceRetrievedData().amountAED)));
                        } else {
                            self.invoiceData.amountAED(0);
                            self.invoiceData.invDate(app.convertISOToLocalDate(new Date()));
                        }
                    }

                    self.invoiceData.invDate(app.convertISOToLocalDate(calcDate(self.invoiceData.invDate(), self.invoiceData.invDateDays())));
                    self.invoiceData.collectionDate(app.convertISOToLocalDate(calcDate(self.invoiceData.invDate(), self.invoiceData.pmtTerm())));
                    self.invoiceData.amountAED(parseInt((Number(self.invoiceData.amountAED()) * Number(self.invoiceData.percent() / 100))));

                });

                self.invoiceArrDataprovider = new oj.ArrayDataProvider(self.invoiceArr);

                self.tableSelectionListener = function (event) {
                    if (event.detail.currentRow) {
                        var currentRow = event.detail.currentRow.rowIndex;
                        self.selectedRow(self.invoiceArr()[currentRow]);
                        self.selectedRow().id ? self.deleteBtnStatus(false) : self.deleteBtnStatus(true);
                    }
                };

                self.openDeleteDialogBtn = function () {
                    document.querySelector('#invoiceDeleteDialog').open();
                };

                self.openDialogBtn = function () {
                    document.querySelector('#invoiceDialog').open();
                };

                self.closeDialogBtn = function () {
                    document.querySelector('#invoiceDeleteDialog').close();
                    document.querySelector('#invoiceDialog').close();
                    clearContent();
                };

                self.backBtnAction = function () {
                    oj.Router.rootInstance.go('costScreen');
                };

                self.addNewInvoiceBtn = function () {
                    var percentVal = self.invoiceData.percent();

                    for (var i = 1; i < self.invoiceArr().length - 1; i++) {
                        percentVal += Number((self.invoiceArr()[i].percent).replace('%', ''));
                        if (percentVal > 100)
                            return app.messagesDataProvider.push(app.createMessage('error', 'Sum of the percentage cannot be more than 100%'));

                    }

                    for (var i = 1; i < self.invoiceArr().length - 1; i++) {
                        if (self.invoiceArr()[i].milestoneVal)
                            if (self.invoiceArr()[i].milestoneVal == self.invoiceData.milestoneDes())
                                return app.messagesDataProvider.push(app.createMessage('error', self.mileStonesArr().find(val => val.value == self.invoiceArr()[i].milestoneVal).label + ' Already exists!'));
                    }


                    document.querySelector('#invoiceDialog').close();
                    app.loading(true);
                    services.addGeneric("invoice/newInvoice", ko.toJSON(self.invoiceData)).then(inv => {
                        getAllInvoices();
                        clearContent();
                    }, err => {
                        app.messagesDataProvider.push({severity: "error", summary: app.unknownErr(), autoTimeout: 0});
                        app.loading(false);
                        clearContent();
                    });

                };

                self.deleteInvoiceBtn = function () {
                    app.loading(true);
                    self.invoiceArr([]);
                    document.querySelector('#invoiceDeleteDialog').close();
                    if (self.selectedRow().id) {
                        services.getGeneric("invoice/delete/" + self.selectedRow().id).then(invoice => {
                            getAllInvoices();
                            app.messagesDataProvider.push(app.createMessage('confirmation', "Successfully deleted"));
                        }, err => {
                            app.messagesDataProvider.push(app.createMessage('confirmation', "Successfully deleted"));
                            getAllInvoices();
                        });
                    }
                };

                function getAllInvoices() {
                    app.loading(true);
                    self.invoiceArr([]);

                    services.getGeneric(`invoice/${self.invoiceData.projectVer()}/${self.invoiceData.projectId()}`).then(invoice => {
                        app.loading(false);
                        var calculatedPercent = 0, calculatedAmount = 0;

                        for (var i in invoice) {
                            invoice[i].milestoneVal = invoice[i].milestoneDes;
                            invoice[i].milestoneDes = invoice[i].percent + self.mileStonesArr().find(val => val.value == invoice[i].milestoneDes).content + invoice[i].pmtTerm + ' days TT';
                            invoice[i].invDate = app.formatDate(invoice[i].invDate);
                            invoice[i].collectionDate = calcDate(invoice[i].invDate, Number(invoice[i].pmtTerm));
                            calculatedAmount += Number(invoice[i].amountAED);
                            calculatedPercent += Number(invoice[i].percent);
                            invoice[i].percent += '%';
                            invoice[i].ttHeader = 'TT';
                        }
                        
                        if (invoice.length > 0) {
                            invoice.splice(0, 0, {
                               // milestoneDes: self.invoiceRetrievedData().procDate.system + " Delivery"
                            });
                            invoice.push({
                                amountAED: calculatedAmount,
                                milestoneDes: "TOTAL",
                                percent: calculatedPercent + "%"
                            });
                        }
                        
                        self.invoiceArr.push(...invoice);
                    }, err => {
                        app.messagesDataProvider.push({severity: "error", summary: app.unknownErr(), autoTimeout: 0});
                        app.loading(false);
                    });
                }


                function calcDate(date, num) {
                    var mDate = new Date(date);
                    return app.formatDate(mDate.setDate(mDate.getDate() + num));
                }

                self.connected = function () {
                    var retrievedData = oj.Router.rootInstance.retrieve();
                    if (retrievedData) {
                        self.invoiceData.projectVer(retrievedData.versionID);
                        self.invoiceData.projectId(retrievedData.projectID);
                    }
                    
                    initTranslations();
                    getAllInvoices();
                    clearContent();
                };

                function initTranslations() {
                    self.MainLbl(app.translate("Purchase.InvoiceLbl"));
                    self.number(app.translate("Invoice.number"));
                    self.milestone(app.translate("Invoice.milestone"));
                    self.invData(app.translate("Invoice.invData"));
                    self.pmtTerm(app.translate("Invoice.pmtTerm"));
                    self.collectionDate(app.translate("Invoice.collectionDate"));
                    self.percent(app.translate("Invoice.percent"));
                    self.milestonePercent(app.translate("Invoice.milestonePercent"));
                    self.amountAED(app.translate("Invoice.amountAED"));
                    self.invoiceBtnCreateLbl(app.translate("Invoice.Create"));
                    self.invoiceDeleteBtnLbl(app.translate("buttoms.deleteBtn"));
                    self.cancleDialogLbl(app.translate("buttoms.cancleBtn"));
                    self.deleteConfirmMsg(app.translate("confirmation message"));
                    self.deleteInvoiceLbl(app.translate("Are you sure Delete Record ?"));
                    self.createDialogLbl(app.translate("Add"));
                    self.backBtnLbl(app.translate("Purchase.backLbl"));

                    self.invoiceColumnArray([
                        {
                            "headerText": self.number(), "field": "srNo"
                        },
                        {
                            "headerText": self.milestone(), "field": "milestoneDes"
                        },
                        {
                            "headerText": self.invData(), "field": "invDate"
                        },
                        {
                            "headerText": self.pmtTerm(), "field": "pmtTerm"
                        },
                        {
                            "headerText": "", "field": "ttHeader"
                        },
                        {
                            "headerText": self.collectionDate(), "field": "collectionDate"
                        },
                        {
                            "headerText": self.percent(), "field": "percent"
                        },
                        {
                            "headerText": self.amountAED(), "field": "amountAED"
                        },
                        {
                            "headerText": self.amountAED(), "field": "amountAED"
                        }
                    ]);
                }

            }

            return invoiceSummaryViewModel;
        });
