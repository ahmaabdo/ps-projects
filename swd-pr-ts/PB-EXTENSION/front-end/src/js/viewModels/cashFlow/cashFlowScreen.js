/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * cashFlowScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'xlsx', 'util/commonhelper', 'ojs/ojarraydataprovider', 'ojs/ojtable', 'ojs/ojbutton', 'ojs/ojmessages'
], function (oj, ko, $, app, services, x, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function cashFlowScreenContentViewModel() {
        var self = this;
        self.invoiceDetalies = ko.observableArray([]);
        self.costTocomeArray = ko.observableArray();
        self.totalArray = ko.observableArray();
        self.data = ko.observableArray();
        self.dataSourceoInVDetails = new oj.ArrayDataProvider(self.invoiceDetalies);
        self.dataSourceCostToCome = new oj.ArrayDataProvider(self.data,{idAttribute:"supplier"});
        self.dataSourceTotal = new oj.ArrayDataProvider(self.totalArray);
        self.columnArrayInvoice = ko.observableArray();
        self.srNo = ko.observableArray();
        self.invoiceRetrievedData = ko.observable(JSON.parse(localStorage.getItem('invoiceStoredData')));
        self.columnArrayCost = ko.observableArray();
        self.columnArrayTotal = ko.observableArray();
        self.cashFlowForCast = ko.observable();
        self.totalInFlow = ko.observable();
        self.costId = ko.observable();
        self.receiptDates = ko.observable();
        self.amount = ko.observable();
        self.Contingency = ko.observable();
        self.startDate = ko.observable();
        self.total = ko.observable();
        self.backLbl = ko.observable();
        self.projectNumberValue = ko.observable();
        self.projectVersionValue = ko.observable();
        self.allDate = ko.observableArray();
        self.overHeadTabelData = ko.observableArray();
        self.endDateChange = ko.observable();
        var pStartDate;
                var pEndDate;
        self.selectedDateMonthsArr = ko.observableArray();
        self.totalOutFlowArray = ko.observableArray();
        self.totalAmountAED = ko.observableArray();
        self.servicesData = ko.observableArray();
        self.data2 = ko.observableArray();
        self.totalOutFlow = ko.observableArray();
        self.totalInFlowVal = ko.observableArray();
        self.tInArray = ko.observableArray();
        self.tOutArray = ko.observableArray();


        self.tableTotalArray = ko.observableArray();
        self.firstTableHeader = ko.observable();
        self.costToComeLbl = ko.observable();
        self.totalLbl = ko.observable();
        self.connected = function ()
        {
            var RetrivedData = oj.Router.rootInstance.retrieve();
            self.projectNumberValue(RetrivedData.projectID)
            self.projectVersionValue(RetrivedData.versionID)
            pStartDate = new Date (RetrivedData.projectStartDate).toDateString();
            var startDate = new Date(pStartDate).toDateString();
            var formateStartDate = startDate.slice(3, 8).concat(new Date(startDate).getFullYear());
            self.selectedDateMonthsArr().push(formateStartDate.trim())
            self.startDate(startDate);
            pEndDate = RetrivedData.projectEndDate;
            if (pEndDate == "No specific data available") {
                var isEndDate = new Date("2021-02-01");
                self.endDateChange(isEndDate);
            } else {
                var isEndDate = pEndDate;
                self.endDateChange(isEndDate);
            }
            var start = new Date(pStartDate);
            var StartDateTable = start.toDateString();
            var StartDateTable = StartDateTable.slice(3, 8).concat(new Date(StartDateTable).getFullYear());
            var end = new Date("2021-02-01");
            var count = monthDiff(start, end)
            self.getMaterial(count,pStartDate);
            self.getAllServices(count,pStartDate);
            self.getAllOverHeadData();
            initTranslations();
            getAllInvoices();
        };
        self.mileStonesArr = ko.observableArray([
            {value: "material", label: "Material delivery and inspection approval from Engineer/Consultants", content: "% against Material delivery and inspection approval from Engineer/Consultants - "},
            {value: "installation", label: "Installation and approval from the Engineer", content: "% on Installation and approval from the Engineer - "},
            {value: "testing", label: "Testing & Commissioning", content: "% on Testing & Commissioning - "},
            {value: "handingOver", label: "Handing Over systems to Engineer", content: "% on Handing Over systems to Engineer - "}
        ]);
        function getAllInvoices() {
            app.loading(true);
            self.invoiceDetalies([]);
            services.getGeneric(`invoice/${self.projectVersionValue()}/${self.projectNumberValue()}`).then(invoice => {
                for (var x in invoice) {
                    invoice[x].milestoneDes = invoice[x].percent + self.mileStonesArr().find(val => val.value == invoice[x].milestoneDes).content + invoice[x].pmtTerm + ' days TT';
                    invoice[x].collectionDate = calcDate(invoice[x].invDate, Number(invoice[x].pmtTerm));
                    invoice[x].amountAED = parseInt((Number(invoice[x].amountAED) * Number(invoice[x].percent)) / 100);
                    for (var i in self.columnArrayInvoice()) {
                        var headerDate = new Date(self.columnArrayInvoice()[i].headerText);
                        var column;
                        if (app.isValidDate(headerDate))
                            column = monthDiff(new Date(app.convertISOToLocalDate(headerDate)), new Date(app.convertISOToLocalDate(invoice[x].collectionDate))) == 0 ? self.columnArrayInvoice()[i].field : null;
                        if (column)
                            invoice[x][column] = invoice[x].amountAED;
                    }
                }
                if (invoice.length > 0) {
                    invoice.splice(0, 0, {
                        milestoneDes: "Initial Order"
                    });
                }
                self.invoiceDetalies.push(...invoice);
                var totalAmount = self.invoiceDetalies().filter(e => e.amountAED).map(e => e.amountAED).reduce((p, c) => ((p||0) + (c||0)));
                var everyMonthAmount = [];
                var monthesVal = [];
                var calcInFlow = [];
                var result = [];
                for (var i in self.columnArrayInvoice()) {
                    everyMonthAmount = self.invoiceDetalies().filter(e => e[self.selectedDateMonthsArr()[i]?self.selectedDateMonthsArr()[i].trim():null]).map(e => e[self.selectedDateMonthsArr()[i].trim()]).map(e => ({[self.selectedDateMonthsArr()[i].trim()]: e}));
                    if (everyMonthAmount.length > 0) {
                        monthesVal.push(...everyMonthAmount);
                        calcInFlow = everyMonthAmount.filter(e => e[self.selectedDateMonthsArr()[i].trim()]).map(e => e[self.selectedDateMonthsArr()[i].trim()]);
                        var calc = [calcInFlow.reduce((p, c) => (p || 0) + (c || 0))];
                        result.push(...calc.map(e => ({[self.selectedDateMonthsArr()[i].trim()]: e})));
                    }
                }
                var obj = {};
                result.forEach(e => {
                    obj = {...obj, ...e, milestoneDes: "TOTAL IN FLOW", amountAED: totalAmount};
                });               
                self.totalInFlowVal(obj);
                self.invoiceDetalies.push(obj);
            }, err => {
                app.loading(false);
            });
        }

        function monthDiff(dateFrom, dateTo) {
            return dateTo.getMonth() - dateFrom.getMonth() +
                    (12 * (dateTo.getFullYear() - dateFrom.getFullYear()))
        }
        function calcDate(date, num) {
            var mDate = new Date(date);
            return app.formatDate(mDate.setDate(mDate.getDate() + num));
        }

        
        var dates = {};
        self.getMaterial = function (count,start)
        {
            var payload = {
                "projectId": self.projectNumberValue().toString(),
                "versionId": self.projectVersionValue().toString()
            };
            var StartDateMatrial = new Date (start)
            var  counter = StartDateMatrial.getMonth();
            var successEMPCbFn = function (data) {
                for (var i = 0; i < count; i++)
                {
                    var selectedDate = new Date(StartDateMatrial.setMonth(StartDateMatrial.getMonth() + counter)).toDateString();
                    var selectedDateMonths = selectedDate.slice(3, 8).concat(new Date(selectedDate).getFullYear());
                    counter++
                    dates[selectedDateMonths.trim()] = "";
                }
                var newData = data.data.map(el => {
                    dates = [];
                    var yy = (new Date(el.paymentDate).toDateString().slice(3, 8).concat(new Date(el.paymentDate).getFullYear())).trim();
                    dates[yy] = el.afterDistotalCostAED;
                    var x = {...el, ...dates};
                    return x;
                });
                for (var i; i < self.selectedDateMonthsArr().length; i++) {
                    var supplierMonthsamount = newData.filter(e => e[self.selectedDateMonthsArr()[i] ? self.selectedDateMonthsArr()[i].trim() : null]).map(e => e[self.selectedDateMonthsArr()[i].trim()]).map(e => ({[self.selectedDateMonthsArr()[i].trim()]: e}));
                    if (supplierMonthsamount.length > 0) {
                        self.totalOutFlowArray().push(...supplierMonthsamount);
                    }
                }
                var totalOfMaterial = newData.map(el => el.afterDistotalCostAED).reduce((p, c) => ((p - 0) || 0) + (c - 0));
                self.totalAmountAED().push(totalOfMaterial);
                var obj =
                        {
                            "supplier": "Equipment / Material",
                            "afterDistotalCostAED": totalOfMaterial
                        };
                self.data(newData);
                self.data.push(obj);
            };
            var failEMPCbFn = function () {
            };
            services.postGeneric(commonhelper.getAllProcurment, payload).then(successEMPCbFn, failEMPCbFn);
        };
        self.getAllOverHeadData = function ()
        {
            var payload = {
                "versionId": self.projectVersionValue().toString(),
                "projectId": self.projectNumberValue().toString()
            };
            var getSuccss = function (data)
            {
                var sys = [...new Set(data.map(e => e.costId))]; // 05,05,05 2/10/2019 oct 2019
                sys.forEach((name, i) => {
                    var system = data.filter(e => e.costId == sys[i])
                    var amount = system.map(e => ((e.amount - 0) || 0) + ((e.value - 0) || 0)).reduce((p, c) => ((p || 0) + c)).toFixed(2);
                    var obj2 =
                            {
                                "costId": sys[i], // 05 
                                "supplier": system[0].description,
                                "afterDistotalCostAED": amount,
                                "children": []
                            };
                    var selectedDateMonths;
                    for (var i in system)
                    {
                        var xxx = new Date(system[i].costDate);
                        var selectedDate = new Date(xxx.setMonth(xxx.getMonth() + 1)).toDateString();
                        selectedDateMonths = selectedDate.slice(3, 8).concat(new Date(selectedDate).getFullYear());
                        var obj =
                                {
                                    "costDate": selectedDateMonths,
                                    "value": system[i].value > 1 ? system[i].value : system[i].amount
                                };
                        obj2.children.push(obj);
                    }
                    var sys2 = [...new Set(obj2.children.map(e => e.costDate))]; //oct 2019 ,581415  oct 2020 ,581415  , sep 2021 556585
                    for (var j in sys2)
                    {
                        var amount = (obj2.children.filter(e => e.costDate == sys2[j]).map(el => el.value).reduce((p, c) => ((p - 0) || 0) + (c - 0)) - 0).toFixed(2);
                        obj2[obj2.children[j].costDate.trim()] = amount;
                    }

                    var x = {...obj2, ...obj2.children[2]};
                    self.data.push(x);
                    self.data2().push(x);
                 });

                    var y = [];
                    for (var k in self.data2())
                    {
                        var mapppedData = self.data2()[k].children.map(el => el.value);
                        y.push(...mapppedData);
                    }
                    var totalOfMaterial = y.reduce((p, c) => ((p - 0) || 0) + (c - 0));
                    var obj3 =
                            {
                                "supplier": "Project Overhead",
                                "afterDistotalCostAED": Math.round(totalOfMaterial)
                            };
                    self.data.push(obj3);
                    
                for (var i in self.selectedDateMonthsArr()) {
                    var overHeadAmount = self.data2().filter(e => e[self.selectedDateMonthsArr()[i] ? self.selectedDateMonthsArr()[i].trim() : null]).map(e => e[self.selectedDateMonthsArr()[i].trim()]).map(e => ({[self.selectedDateMonthsArr()[i].trim()]: e}));              
                 if (overHeadAmount.length > 0) {
                        self.totalOutFlowArray().push(...overHeadAmount);
                    }
                }
             
            };
            var projectCostFail = function () {

            };
            services.postGeneric(commonhelper.getAllRequest, payload).then(getSuccss, projectCostFail);
        };

        self.getAllServices = function (count,start)
        {
            var payload = {
                "projectId": self.projectNumberValue().toString(),
                "versionId": self.projectVersionValue().toString()
            };
            var successEMPCbFn = function (data) {
                
                app.loading(false);
                self.servicesData(data);
                setTimeout(function () {
                    systemCode(function (data2) {
                       var  counter = new Date(start).getMonth();
                        for (var i = 0; i < count; i++)
                        {
                            var selectedDate = new Date(new Date(start).setMonth(counter)).toDateString();
                            var selectedDateMonths = selectedDate.slice(3, 8).concat(new Date(selectedDate).getFullYear());                        
                            counter++
                            dates[selectedDateMonths.trim()] = "";
                            self.selectedDateMonthsArr.push(selectedDateMonths);              
                        }
                         var data2 = data.map(el =>
                            ({
                                supplier: el.projectService,
                                afterDistotalCostAED: el.costs,
                                costId: el.taskNumberRef,
                                dateFrom:el.dateFrom?el.dateFrom:null,
                                dateTo:el.dateTo?el.dateTo:null,
                                period:monthDiff(new Date(el.dateFrom),new Date(el.dateTo))
                                //[date[i]] :parseInt(el.costs)/(monthDiff(new           
                            }));
                       var counter = 0
                        for (var z in data2) {                           
                            var Start = new Date(data2[z].dateFrom);
                            var date = []
                            counter=Start.getMonth();                         
                            for (var i = 0; i <= data2[z].period; i++) {
                                var firstformate = new Date(Start.setMonth(counter)).toDateString();
                                var arrDates = firstformate.slice(3, 8).concat(new Date(firstformate).getFullYear());
                                date.push(arrDates) 
                                for (var k in date) {
                                    var month = date[k];
                                    if (month)
                                        data2[z][month.trim()] = Math.round( parseInt((data2[z].afterDistotalCostAED).replace(",", "")) / (monthDiff(new Date(data2[z].dateFrom), new Date(data2[z].dateTo))));
                                }
                                 counter++
                            }
                          }                                                                       
//                        var newData = data2.map(el => {
//                            dates = [];
//                            var x = {...el, ...dates};
//                            return x;
//                        });
                        var totalOfMaterial = data2.map(el =>  typeof (el.afterDistotalCostAED)!= "undefined"?el.afterDistotalCostAED.replace(",", "") :false).reduce((p, c) => ((p - 0) || 0) + (c - 0));

                        var eMonTotal = [];
                        for (var i; i < self.selectedDateMonthsArr().length; i++)
                        {
                            // calculate service monthes  //
                        
                            var service = data2.filter(e => e[self.selectedDateMonthsArr()[i].trim()]).map(e => e[self.selectedDateMonthsArr()[i].trim()]).map(e => ({[self.selectedDateMonthsArr()[i].trim()]: e}));
                        
                            if (service.length > 0) {
                                self.totalOutFlowArray().push(...service);                         
                            }
                           
                            var evreyMonthTotal = self.totalOutFlowArray().filter(e => e[self.selectedDateMonthsArr()[i] ? self.selectedDateMonthsArr()[i].trim() : null]).map(e => e[self.selectedDateMonthsArr()[i].trim()] - 0)
                        
                            if (evreyMonthTotal.length > 0) {
                                var calc = [Math.round(evreyMonthTotal.reduce((p, c) => (p + c)))];
                                eMonTotal.push(...calc.map(e => ({[self.selectedDateMonthsArr()[i].trim()]: e})));
                            }
                        }

                        self.totalAmountAED().push(totalOfMaterial);
                        var Amount = self.totalAmountAED().reduce((p, c) => p + c).toFixed(2);
                        var obj = {};
                        eMonTotal.forEach(e => {
                            obj = {...obj, ...e, description: "TOTAL OUT FLOW", amount: Amount};
                        });
                        self.totalOutFlow(obj);
                        //--Calculate NetCash--//
                        var res = [];
                        for (var i in self.totalOutFlow()) {                         
                            for (var j in self.totalInFlowVal()) {
                                if (i == j ) {                                    
                                    self.tInArray.push({[j]: self.totalInFlowVal()[i]});
                                    self.tOutArray.push({[j]: self.totalOutFlow()[i]});
                                } 
//                                if (i && j ){
//                                    if(self.totalInFlowVal()[i]  != undefined ){   
//                                     //  console.log({[j]: self.totalInFlowVal()[j]}); 
//                                   // console.log({[i]: self.totalOutFlow()[i]});
//                                    }  
//                                }
                            }
                        }                     
                        for (var g = 0; g < self.tInArray().length; g++) {
                            for (var i in self.totalOutFlow()) {
                                for (var j in self.totalInFlowVal()) {
                                    if (i == j) {
                                        if (self.tInArray()[g][j] != undefined) {
                                            var subtractionNetCash = [(self.tInArray()[g][j]) - (self.tOutArray()[g][j])];
                                            res.push(...subtractionNetCash.map(e => ({[j]: e.toFixed(2)})));
                                        }
                                    }
                                }
                            }
                        }   
                        var netCash = { description: "NET CASHFLOW"};
                        res.forEach(e => {
                            netCash = {...netCash, ...e};
                        });
                                    self.tableTotalArray(obj)
                        var obj2 = {};
                        for (var i = 0; i < self.selectedDateMonthsArr().length; i++) {            
                            var trimData = self.selectedDateMonthsArr()[i - 1] ? self.selectedDateMonthsArr()[i - 1].trim() : 0
                            obj2[self.selectedDateMonthsArr()[i].trim()] = self.tableTotalArray()[trimData]
                        }
                        var obj3 = {}
                        for (var i = 0; i < self.selectedDateMonthsArr().length; i++) {
                            var trimData = self.selectedDateMonthsArr()[i] ? self.selectedDateMonthsArr()[i].trim() : 0
                            obj3[self.selectedDateMonthsArr()[i].trim()] = (obj[trimData] ? obj[trimData] : 0) + (obj2[trimData] ? obj2[trimData] : 0)
                        } 
                        obj3.description = "CLOSING BALANCE";
                        obj2.description = "OPENING BALANCE";
                        self.totalArray.push(obj, obj2, obj3);
//                                self.totalArray([obj,obj2]);                                            
                        var obj =
                                {
                                    "supplier": "PM & Engineering",
                                    "afterDistotalCostAED": totalOfMaterial
                                };
                        self.data.push(...data2);
                        self.data.push(obj);
                        app.loading(false);
                    }, 2500);
                });

            };
            var failEMPCbFn = function () {
                app.loading(false);
            };
            services.postGeneric(commonhelper.services, payload).then(successEMPCbFn, failEMPCbFn);
        };
        function systemCode(func)
        {
                app.loading(true);
         app.messagesDataProvider.push({severity: "info", summary: "Loading Services", autoTimeout: 0});
            var payload = self.servicesData().map(el => {
                var obj = {
                    "desc": el.description
                };
                return obj;
            });
            var successEMPCbFn = function (data) {
                app.loading(false);
                func(data);
            };
            var failEMPCbFn = function () {
                app.loading(false);
            };
            services.postGeneric(commonhelper.systemType, payload).then(successEMPCbFn, failEMPCbFn);
        }

        self.backBtnAction = function ()
        {
            oj.Router.rootInstance.go('costScreen');
        };
        var getTranslation = oj.Translations.getTranslatedString;
        function  initTranslations (){

        self.cashFlowForCast(getTranslation("cashFlow.cashFlowForCast"));
        self.srNo(getTranslation("Invoice.number"));
        self.totalInFlow(getTranslation("cashFlow.totalInFlow"));
        self.receiptDates(getTranslation("cashFlow.receiptDates"));
        self.amount(getTranslation("cashFlow.amount"));
        self.total(getTranslation("cashFlow.total"));
        self.costId(getTranslation("cashFlow.costId"));
        self.Contingency(getTranslation("cashFlow.Contingency"));
        self.backLbl(app.translate("reports.backLbl"));
         self.firstTableHeader(app.translate("cashFlow.firstTableHeader"));
        self.costToComeLbl(app.translate("cashFlow.costToComeLbl"));
        self.totalLbl(app.translate("cashFlow.totalLbl"));
        var start = new Date(pStartDate);
        var StartDateTable = start.toDateString();
        var StartDateTable = StartDateTable.slice(3, 8).concat(new Date(StartDateTable).getFullYear());
        var end = new Date(self.endDateChange());
        var count = monthDiff(start, end);
        var columnArray = [
            {
                "headerText": self.srNo(), "field": "srNo"
            }
            ,
            {
                "headerText": self.cashFlowForCast(), "field": "milestoneDes"
            }
            ,
            {
                "headerText": self.receiptDates(), "field": "collectionDate"
            }
            ,
            {
                "headerText": self.amount(), "field": "amountAED"
            },
            {
                "headerText": StartDateTable, "field": StartDateTable.trim()
            }
        ];
        var columnArraySecond = [
            {
                "headerText": self.totalInFlow(), "field": "supplier"
            }
            ,
            {
                "headerText": self.costId(), "field": "costId"
            }
            ,
            {
                "headerText": self.amount(), "field": "afterDistotalCostAED"
            },
            {
                "headerText": StartDateTable, "field": StartDateTable.trim()
            }

        ];
        var columnArraythird = [
            {
                "headerText": "Description", "field": "description"
            }
            ,
            {
                "headerText": self.amount(), "field": "amount"
            },
            {
                "headerText": StartDateTable, "field": StartDateTable.trim()
            }

        ];
        for (var i = 0; i < count; i++)
        {
            var selectedDate = new Date(start.setMonth(start.getMonth() + 1)).toDateString();
            var selectedDateMonths = selectedDate.slice(3, 8).concat(new Date(selectedDate).getFullYear());
            self.selectedDateMonthsArr().push(selectedDateMonths);
            columnArray.push(
                    {
                        "headerText": selectedDateMonths, "field": selectedDateMonths.trim()
                    }
            );
            columnArraySecond.push(
                    {
                        "headerText": selectedDateMonths, "field": selectedDateMonths.trim()
                    }
            );
            columnArraythird.push(
                    {
                        "headerText": selectedDateMonths, "field": selectedDateMonths.trim()
                    }
            );
        }       
        //fileds after Dates //
        columnArray.push(
                {
                    "headerText": self.Contingency(), "field": "test1"
                },
                {
                    "headerText": self.total(), "field": "amountAED"
                }
        );
        columnArraySecond.push(
                {
                    "headerText": self.Contingency(), "field": "test1"
                },
                {
                    "headerText": self.total(), "field": "test2", "className": "oj-helper-text-align-end oj-read-only"
                }

        );
        columnArraythird.push(
                {
                    "headerText": self.Contingency(), "field": "test1"
                },
                {
                    "headerText": self.total(), "field": "test2"
                }

        );
        //End of  fileds after Dates //
        self.columnArrayInvoice(columnArray);
        self.columnArrayCost(columnArraySecond);
        self.columnArrayTotal(columnArraythird);
    
    }
        initTranslations();
        }


    return cashFlowScreenContentViewModel;
});
