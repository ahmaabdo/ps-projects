package com.appspro.nexuscc.rest;

import com.appspro.nexuscc.bean.InvoiceBean;

import com.appspro.nexuscc.dao.InvoiceDao;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/invoice")
public class InvoiceRest {

    @POST
    @Path("/newInvoice")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response insertInvoice(String body) {
        JSONObject json = new JSONObject(body);
        System.out.println(json);
        InvoiceDao.getInstance().createNewInvoice(json.toString());
        return Response.ok("").status(200).build();
    }


    @GET
    @Path("/{projectVer}/{projectId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getInvoices(@PathParam("projectVer") String projectVer, @PathParam("projectId") String projectId) {
        try {
            List<InvoiceBean> allInvoice =
                InvoiceDao.getInstance().getAllInvoice(projectVer, projectId);
            return new JSONArray(allInvoice).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GET
    @Path("/delete/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteInvoice(@PathParam("id") String id) {
        try {
            return InvoiceDao.getInstance().deleteAnInvoice(id);
        } catch (Exception e) {
            return e.getMessage();
        }
    }


}
