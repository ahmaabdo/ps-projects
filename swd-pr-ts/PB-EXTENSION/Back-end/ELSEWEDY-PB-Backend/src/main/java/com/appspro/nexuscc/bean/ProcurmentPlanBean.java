package com.appspro.nexuscc.bean;

import org.json.JSONArray;

public class ProcurmentPlanBean {
    
    private String system;
    private String make;
    private String supplier;
    private String costId;
    private String currency;
    private String afterDistotalCostFc;
    private String exchangeRate;
    private String afterDistotalCostAED;
    private String beforeDistotalCostAED;
    private String discountAED;
    private String discountRate;
    private String forecastPrDate;
    private String prProcessingTime;
    private String forecostOrderDate;
    private String manufactingLeadTime;
    private String matReadyWith;
    private String shippingTime;
    private String etaDsoDate;
    private String cabinetAssembly;
    private String preFat;
    private String fat;
    private String fatPunchClearance;
    private String integerationFatDate;
    private String packing;
    private String delivery;
    private String targetDeliveryDate;
    private String cantingencyTime;
    private String projectId;
    private String versionId;
    private String paymentDate;
    private String paymentTerm;
    public JSONArray child = new JSONArray();
    private String seq;


    public void setSystem(String system) {
        this.system = system;
    }

    public String getSystem() {
        return system;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getMake() {
        return make;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setCostId(String costId) {
        this.costId = costId;
    }

    public String getCostId() {
        return costId;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setAfterDistotalCostFc(String afterDistotalCostFc) {
        this.afterDistotalCostFc = afterDistotalCostFc;
    }

    public String getAfterDistotalCostFc() {
        return afterDistotalCostFc;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setAfterDistotalCostAED(String afterDistotalCostAED) {
        this.afterDistotalCostAED = afterDistotalCostAED;
    }

    public String getAfterDistotalCostAED() {
        return afterDistotalCostAED;
    }

    public void setBeforeDistotalCostAED(String beforeDistotalCostAED) {
        this.beforeDistotalCostAED = beforeDistotalCostAED;
    }

    public String getBeforeDistotalCostAED() {
        return beforeDistotalCostAED;
    }

    public void setDiscountAED(String discountAED) {
        this.discountAED = discountAED;
    }

    public String getDiscountAED() {
        return discountAED;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate;
    }

    public String getDiscountRate() {
        return discountRate;
    }

    public void setForecastPrDate(String forecastPrDate) {
        this.forecastPrDate = forecastPrDate;
    }

    public String getForecastPrDate() {
        return forecastPrDate;
    }

    public void setPrProcessingTime(String prProcessingTime) {
        this.prProcessingTime = prProcessingTime;
    }

    public String getPrProcessingTime() {
        return prProcessingTime;
    }

    public void setForecostOrderDate(String forecostOrderDate) {
        this.forecostOrderDate = forecostOrderDate;
    }

    public String getForecostOrderDate() {
        return forecostOrderDate;
    }

    public void setManufactingLeadTime(String manufactingLeadTime) {
        this.manufactingLeadTime = manufactingLeadTime;
    }

    public String getManufactingLeadTime() {
        return manufactingLeadTime;
    }

    public void setMatReadyWith(String matReadyWith) {
        this.matReadyWith = matReadyWith;
    }

    public String getMatReadyWith() {
        return matReadyWith;
    }

    public void setShippingTime(String shippingTime) {
        this.shippingTime = shippingTime;
    }

    public String getShippingTime() {
        return shippingTime;
    }

    public void setEtaDsoDate(String etaDsoDate) {
        this.etaDsoDate = etaDsoDate;
    }

    public String getEtaDsoDate() {
        return etaDsoDate;
    }

    public void setCabinetAssembly(String cabinetAssembly) {
        this.cabinetAssembly = cabinetAssembly;
    }

    public String getCabinetAssembly() {
        return cabinetAssembly;
    }

    public void setPreFat(String preFat) {
        this.preFat = preFat;
    }

    public String getPreFat() {
        return preFat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getFat() {
        return fat;
    }

    public void setFatPunchClearance(String fatPunchClearance) {
        this.fatPunchClearance = fatPunchClearance;
    }

    public String getFatPunchClearance() {
        return fatPunchClearance;
    }

    public void setIntegerationFatDate(String integerationFatDate) {
        this.integerationFatDate = integerationFatDate;
    }

    public String getIntegerationFatDate() {
        return integerationFatDate;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getPacking() {
        return packing;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setTargetDeliveryDate(String targetDeliveryDate) {
        this.targetDeliveryDate = targetDeliveryDate;
    }

    public String getTargetDeliveryDate() {
        return targetDeliveryDate;
    }

    public void setCantingencyTime(String cantingencyTime) {
        this.cantingencyTime = cantingencyTime;
    }

    public String getCantingencyTime() {
        return cantingencyTime;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getVersionId() {
        return versionId;
    }
    
    

    public void setChild(JSONArray child) {
        this.child = child;
    }

    public JSONArray getChild() {
        return child;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSeq() {
        return seq;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }
}
