package com.appspro.nexuscc.rest;

import com.appspro.nexuscc.bean.MasterDataBean;
import com.appspro.nexuscc.bean.OptionBean;
import com.appspro.nexuscc.dao.MasterDataDao;
import com.appspro.nexuscc.dao.OptionDao;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;
@Path("/optionsDes")
public class OptionRest {
    
    OptionDao service = new OptionDao();
    
    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
        OptionBean list = new OptionBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            OptionBean bean = mapper.readValue(body, OptionBean.class);
            list = service.addOption(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @POST
    @Path("/getAllOptionsRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllRequest(String data) {
        List<OptionBean> list = new ArrayList<OptionBean>();
        JSONObject o = new JSONObject(data);
        String projectVersion =
           o.has("projectVersion") ? o.getString("projectVersion") : null;
        list = service.getallrequest(projectVersion);
        return new JSONArray(list).toString();

    }
    @GET
    @Path("/deleteOption/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getdelete(@PathParam("id")
           String id) {

        try {
            service.removeElementFromDb(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
