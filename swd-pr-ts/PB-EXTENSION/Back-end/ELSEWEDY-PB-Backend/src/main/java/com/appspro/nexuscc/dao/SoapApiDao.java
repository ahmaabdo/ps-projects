package com.appspro.nexuscc.dao;

import org.json.JSONObject;

public class SoapApiDao {
    private static SoapApiDao instance = null;
    public static SoapApiDao getInstance(){
        if(instance == null)
            instance = new SoapApiDao();
        return instance;
    }
    
    public JSONObject updateProjectPlan(JSONObject data){
        JSONObject res = new JSONObject();
        String template = 
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/projects/control/budgetsAndForecasts/projPlanServiceV3/types/\" xmlns:proj=\"http://xmlns.oracle.com/apps/projects/control/budgetsAndForecasts/projPlanServiceV3/\">" +
        "  <soapenv:Header/>"+
        "   <soapenv:Body>"+
        "      <typ:processProjectPlan>"+
        "         <typ:projPlanList>"+
        "            <proj:ProjectId>"+data.get("ProjectId").toString()+"</proj:ProjectId>"+
        "            <proj:PlanVersionId>"+data.get("PlanVersionId").toString()+"</proj:PlanVersionId>"+
//        "            <proj:PmProductCode>?</proj:PmProductCode>"+
//        "            <proj:ProjectReferenceId>?</proj:ProjectReferenceId>"+
//        "            <proj:PlanIntegrationAsmt>"+
//        "               <proj:ProjectId>?</proj:ProjectId>"+
//        "               <proj:PlanningElementId>?</proj:PlanningElementId>"+
//        "               <proj:PlanLineId>?</proj:PlanLineId>"+
//        "               <proj:PmProjectSrcReference>?</proj:PmProjectSrcReference>"+
//        "               <proj:PmProductCode>?</proj:PmProductCode>"+
//        "               <proj:PmTaskSrcReference>?</proj:PmTaskSrcReference>"+
//        "               <proj:PmTaskAssgnSrcReference>?</proj:PmTaskAssgnSrcReference>"+
//        "               <proj:PlanVersionId>?</proj:PlanVersionId>"+
//        "               <proj:TaskId>?</proj:TaskId>"+
//        "               <proj:RbsElementId>?</proj:RbsElementId>"+
//        "               <proj:TransactionCurrencyCode>?</proj:TransactionCurrencyCode>"+
//        "               <proj:ProjElementVersionId>?</proj:ProjElementVersionId>"+
//        "               <proj:RaTaskDatesSame>?</proj:RaTaskDatesSame>"+
//        "               <proj:AssignmentDescription>?</proj:AssignmentDescription>"+
//        "               <proj:ScheduledDelay>?</proj:ScheduledDelay>"+
//        "               <proj:UnplannedFlag>?</proj:UnplannedFlag>"+
//        "               <proj:PlanningStartDate>?</proj:PlanningStartDate>"+
//        "               <proj:PlanningEndDate>?</proj:PlanningEndDate>"+
//        "               <proj:TotalQuantity unitCode=\"?\">?</proj:TotalQuantity>"+
//        "               <proj:TotalTCRawCost currencyCode=\"?\">?</proj:TotalTCRawCost>"+
//        "               <proj:TotalTCBrdndCost currencyCode=\"?\">?</proj:TotalTCBrdndCost>"+
//        "               <proj:TCRawCostRateOverride>?</proj:TCRawCostRateOverride>"+
//        "               <proj:TCBrdndCostRateOverride>?</proj:TCBrdndCostRateOverride>"+
//        "               <proj:ErrorFlag>?</proj:ErrorFlag>"+
//        "               <proj:AttributeCategory>?</proj:AttributeCategory>"+
//        "               <proj:ResourceClassCode>?</proj:ResourceClassCode>"+
//        "               <proj:IntegrationResourceType>?</proj:IntegrationResourceType>"+
//        "               <proj:UnitOfMeasure>?</proj:UnitOfMeasure>"+
        "            </proj:PlanIntegrationAsmt>"+
        "         </typ:projPlanList>"+
        "      </typ:processProjectPlan>"+
        "   </soapenv:Body>"+
        "</soapenv:Envelope>";
        
        return res;
    }
}
