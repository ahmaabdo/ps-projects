package com.appspro.nexuscc.bean;

public class UploadAssignBean {

    private int id;
    private String srnNumber;
    private String airCraft;
    private String dep;
    private String arr;
    private String itemService;
    private String deptIcap;
    private String arrIco;
    private String customerName;
    private String flightType;
    private String cctrnNum;
    private String services;
    private String paymentMethod;
    private String sector;
    private String cardNumber;
    private String cardHolder;
    private String currency;
    private String foreignCurrency;
    private String sarAmount;
    private String vendorName;
    private String buyerName;
    private String assignedToUser;
    private String managerId;
    private String poNumber;
    private String ppNumber;
    private String trxDate;
    private String location;
    private String vendorSite;
    private String email;
    private String busniessUnit;
    private String invoiceNumber;
    private String orderNumber;



    public UploadAssignBean() {
        super();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setSrnNumber(String srnNumber) {
        this.srnNumber = srnNumber;
    }

    public String getSrnNumber() {
        return srnNumber;
    }

    public void setAirCraft(String airCraft) {
        this.airCraft = airCraft;
    }

    public String getAirCraft() {
        return airCraft;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getDep() {
        return dep;
    }

    public void setArr(String arr) {
        this.arr = arr;
    }

    public String getArr() {
        return arr;
    }

    public void setItemService(String itemService) {
        this.itemService = itemService;
    }

    public String getItemService() {
        return itemService;
    }

    public void setDeptIcap(String deptIcap) {
        this.deptIcap = deptIcap;
    }

    public String getDeptIcap() {
        return deptIcap;
    }

    public void setArrIco(String arrIco) {
        this.arrIco = arrIco;
    }

    public String getArrIco() {
        return arrIco;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    public String getFlightType() {
        return flightType;
    }

    public void setCctrnNum(String cctrnNum) {
        this.cctrnNum = cctrnNum;
    }

    public String getCctrnNum() {
        return cctrnNum;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getServices() {
        return services;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSector() {
        return sector;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setForeignCurrency(String foreignCurrency) {
        this.foreignCurrency = foreignCurrency;
    }

    public String getForeignCurrency() {
        return foreignCurrency;
    }

    public void setSarAmount(String sarAmount) {
        this.sarAmount = sarAmount;
    }

    public String getSarAmount() {
        return sarAmount;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setAssignedToUser(String assignedToUser) {
        this.assignedToUser = assignedToUser;
    }

    public String getAssignedToUser() {
        return assignedToUser;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPpNumber(String ppNumber) {
        this.ppNumber = ppNumber;
    }

    public String getPpNumber() {
        return ppNumber;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getTrxDate() {
        return trxDate;
    }
   
    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setVendorSite(String vendorSite) {
        this.vendorSite = vendorSite;
    }

    public String getVendorSite() {
        return vendorSite;
    }


    public void setBusniessUnit(String busniessUnit) {
        this.busniessUnit = busniessUnit;
    }

    public String getBusniessUnit() {
        return busniessUnit;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }


    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }
}
