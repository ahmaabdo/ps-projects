package com.appspro.nexuscc.dao;

import biPReports.RestHelper;

import static biPReports.RestHelper.getSchema_Name;

import com.appspro.db.AppsproConnection;
import com.appspro.nexuscc.bean.ServicesBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class ServicesDao extends AppsproConnection {
    Connection connection = null;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper restHelper = new RestHelper();
    private static ServicesDao instance;

    public static ServicesDao getInstance() {
        if (instance == null)
            instance = new ServicesDao();

        return instance;
    }


//    public ArrayList<ServicesBean> getAllServices() {
//        ArrayList<ServicesBean> list = new ArrayList<ServicesBean>();
//
//        try {
//            String query = null;
//            query = "SELECT * FROM " + getSchema_Name() + ".XXX_SERVICES";
//
//            connection = AppsproConnection.getConnection();
//            ps = connection.prepareStatement(query);
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                ServicesBean bean = new ServicesBean();
//
//                bean.setId(rs.getInt("ID"));
//                bean.setItem(rs.getString("ITEM"));
//                bean.setDescription(rs.getString("DESCRIPTION"));
//                bean.setBiddingEstimate(rs.getString("BIDDING_ESTIMATE"));
//                bean.setPurchaseEstimate(rs.getString("PURCHASE_ESTIMATE"));
//                bean.setSupplierAED(rs.getString("SUPPLIER_AED"));
//                bean.setSupplierAED_1(rs.getString("SUPPLIER_AED_1"));
//                bean.setSupplierAED_2(rs.getString("SUPPLIER_AED_2"));
//                bean.setSupplierAED_3(rs.getString("SUPPLIER_AED_3"));
//                bean.setQty(rs.getString("QTY"));
//                bean.setUnit(rs.getString("UNIT"));
//                bean.setTotalAED(rs.getString("TOTAL_AED"));
//                bean.setTotalAED_1(rs.getString("TOTAL_AED_1"));
//                bean.setGpServices(rs.getString("GP_SERVICES"));
//
//                list.add(bean);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            closeResources(connection, ps, rs);
//        }
//
//        return list;
//    }
    
      public ArrayList<ServicesBean> getAllServices(String projectId , String versionId) {
        ArrayList<ServicesBean> list = new ArrayList<ServicesBean>();
        try {
            String query = null;
            query = "SELECT * FROM " + getSchema_Name() + ".XXX_SERVICES Where PROJECT_NUMBER =? AND PROJECT_VERSION =?";
            connection = AppsproConnection.getConnection();
            ps = connection.prepareStatement(query);
            ps.setString(1, projectId);
            ps.setString(2, versionId);  
            rs = ps.executeQuery();
            while (rs.next()) {
                ServicesBean bean = new ServicesBean();
                bean.setId(rs.getInt("ID"));
                bean.setTaskNumberRef(rs.getString("TASK_ID_REF"));
                bean.setProjectService(rs.getString("PROJECT_SERVICE"));
                bean.setRates(rs.getString("RATES"));
                bean.setHours(rs.getString("HOURS"));
                bean.setCosts(rs.getString("COST"));
                bean.setDateFrom(rs.getString("DATE_FROM"));
                bean.setDateTo(rs.getString("DATE_TO"));
                list.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public JSONObject getDataToBeExported() {
        JSONObject response = new JSONObject();
        response.put("status", "OK");

        JSONArray res = new JSONArray();
        String query = "SELECT * FROM " + getSchema_Name() + ".XXX_SERVICES";
        try {
            connection = AppsproConnection.getConnection();
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();

            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                System.out.println(numColumns);
                JSONObject obj = new JSONObject();
                for (int i = 1; i <= numColumns; i++) {
                    String columnName = rsmd.getColumnName(i);
                    obj.put(columnName, rs.getObject(columnName));
                }
                res.put(obj);
            }
            response.put("data", res);
        } catch (SQLException e) {
            response.put("status", "ERROR");
            response.put("data", e.getMessage());
            e.printStackTrace();
        }
        System.out.println(response);
        return response;
    }


    public String createDataFromExcelSheet(String body) {
        try {
            String query = null;
            JSONArray jsonArray = new JSONArray(body);
            connection = AppsproConnection.getConnection();
            query =
                " INSERT INTO  XXX_SERVICES (TASK_ID_REF, PROJECT_SERVICE, RATES, HOURS,COST, PROJECT_NUMBER, PROJECT_VERSION,DATE_FROM,DATE_TO)\n" + 
                "     VALUES (?,?,?,?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = jsonArray.getJSONObject(i);
                ps.setString(1, item.get("Task_Number_Ref").toString());
                ps.setString(2, item.get("Project_Service_Cost_Summary").toString());
                ps.setString(3, item.get("Rates").toString());
                ps.setString(4, item.get("_Hours_").toString());
                ps.setString(5, item.get("Cost").toString());
                ps.setString(6, item.get("projectId").toString());
                ps.setString(7, item.get("projectVersion").toString());
                ps.setString(8, item.get("from").toString());
                ps.setString(9, item.get("to").toString());

                ps.executeUpdate();
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return "";
    }

    public JSONArray createPlanedResourse(String body) {

        JSONArray jsonobj = new JSONArray(body);
        Long palnversionId = jsonobj.getJSONObject(0).getLong("planVersionId");
        String jsonobj2 = "";
        JSONObject returnData = new JSONObject();
        JSONArray responseCollect = new JSONArray();
        for (int i = 0; i < jsonobj.length(); i++) {
            JSONObject arr = jsonobj.getJSONObject(i);
            jsonobj2 = arr.getJSONObject("data").toString();
            String serverUrl =
                restHelper.getInstanceUrl() + restHelper.getProjectBudgetLinesURL() +
                palnversionId + restHelper.getProjectBudgetLineResoursURL();
            returnData =
                    RestHelper.getInstance().callRest(serverUrl, "POST", jsonobj2);
            responseCollect.put(returnData);
           
        }  
        return  responseCollect;
    }
}
