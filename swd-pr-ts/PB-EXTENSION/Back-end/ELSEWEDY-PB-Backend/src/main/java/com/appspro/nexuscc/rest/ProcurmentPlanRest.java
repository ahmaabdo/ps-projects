package com.appspro.nexuscc.rest;

import com.appspro.nexuscc.bean.ProcurmentPlanBean;
import com.appspro.nexuscc.dao.ProcurmentPlanDao;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("procurment")
public class ProcurmentPlanRest {

    ProcurmentPlanDao service = new ProcurmentPlanDao();

    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
        ProcurmentPlanBean list = new ProcurmentPlanBean();
        ProcurmentPlanBean bean = new ProcurmentPlanBean();
        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length() ; i++) {
                
                JSONObject object = arr.getJSONObject(i);
                
                bean.setDiscountAED(object.getString("discountAED"));
                bean.setDiscountRate(object.getString("discountRate"));
                bean.setAfterDistotalCostAED(object.getString("afterDistotalCostAED"));
                bean.setAfterDistotalCostFc(object.getString("afterDistotalCostFc"));
                bean.setBeforeDistotalCostAED(object.getString("beforeDistotalCostAED"));
                bean.setCostId(object.getString("costId"));
                bean.setExchangeRate(object.getString("exchangeRate"));
                bean.setSeq(object.getString("seq"));
                bean.setMake(object.getString("make"));
                bean.setCurrency(object.getString("currency"));
                bean.setChild(object.getJSONArray("children"));
                bean.setSupplier(object.getString("supplier"));
                bean.setSystem(object.getString("system"));
                bean.setProjectId(object.getString("projectId"));
                bean.setVersionId(object.getString("versionId"));
                
                list = service.insertToProcurment(bean);
            }
           
            
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    
    @POST
    @Path("/procurmentPlan")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllProcurment (String body) {
    
        JSONObject obj = new JSONObject(body);
        JSONObject res = new JSONObject();
        
        String projectId = obj.getString("projectId");
        String versionId = obj.getString("versionId");
        
        res = service.getAllProcurment(projectId, versionId);
        
        return Response.ok(res.toString()).build();
//        return Response.status(Status.OK).entity(res.toString()).build();
        
    }
}
