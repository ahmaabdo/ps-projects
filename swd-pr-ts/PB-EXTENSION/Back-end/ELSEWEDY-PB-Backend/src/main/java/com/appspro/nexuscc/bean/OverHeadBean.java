package com.appspro.nexuscc.bean;

public class OverHeadBean {
    private String id;
    private String costId;
    private String detalis;
    private String rules;
    private String rate;
    private String amount;
    private String value;
    private String bondRate;
    private String projectId;
    private String versionId;
    private String durationOfBondRate;
    private String costDate;
    private String description;
    private String bondDuration;
    private String bondStart;
    private String bondEnd;
    private String planningElementId;
    private String status;

    public void setCostId(String costId) {
        this.costId = costId;
    }

    public String getCostId() {
        return costId;
    }

    public void setDetalis(String detalis) {
        this.detalis = detalis;
    }

    public String getDetalis() {
        return detalis;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getRules() {
        return rules;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }

    public void setAmount(String Amount) {
        this.amount = Amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setBondRate(String bondRate) {
        this.bondRate = bondRate;
    }

    public String getBondRate() {
        return bondRate;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setCostDate(String costDate) {
        this.costDate = costDate;
    }

    public String getCostDate() {
        return costDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setBondDuration(String bondDuration) {
        this.bondDuration = bondDuration;
    }

    public String getBondDuration() {
        return bondDuration;
    }

    public void setBondStart(String bondStart) {
        this.bondStart = bondStart;
    }

    public String getBondStart() {
        return bondStart;
    }

    public void setBondEnd(String bondEnd) {
        this.bondEnd = bondEnd;
    }

    public String getBondEnd() {
        return bondEnd;
    }

    public void setPlanningElementId(String planningElementId) {
        this.planningElementId = planningElementId;
    }

    public String getPlanningElementId() {
        return planningElementId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
