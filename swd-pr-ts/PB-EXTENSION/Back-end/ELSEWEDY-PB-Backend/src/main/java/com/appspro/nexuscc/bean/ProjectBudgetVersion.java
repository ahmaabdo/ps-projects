package com.appspro.nexuscc.bean; 

public class ProjectBudgetVersion {

private String financialPlanType;
private String planningAmounts;
private long planVersionId;
private Integer planVersionNumber;
private String planVersionName;
private Object planVersionDescription;
private String planVersionStatus;
private Long projectId;
private String projectNumber;
private String projectName;
private Object budgetCreationMethod;
private Object sourcePlanVersionId;
private Object budgetGenerationSource;
private Object sourcePlanType;
private Object sourcePlanVersionStatus;
private Object sourcePlanVersionNumber;
private Object copyAdjustmentPercentage;
private Double pCRawCostAmounts;
private Double pCBurdenedCostAmounts;
private Integer pCRevenueAmounts;
private Double pFCRawCostAmounts;
private Double pFCBurdenedCostAmounts;
private Integer pFCRevenueAmounts;
private Object deferFinancialPlanCreation;
private Object administrativeCode;
private String linkes;

public String getFinancialPlanType() {
return financialPlanType;
}

public void setFinancialPlanType(String financialPlanType) {
this.financialPlanType = financialPlanType;
}

public String getPlanningAmounts() {
return planningAmounts;
}

public void setPlanningAmounts(String planningAmounts) {
this.planningAmounts = planningAmounts;
}

public long getPlanVersionId() {
return planVersionId;
}

public void setPlanVersionId(long planVersionId) {
this.planVersionId = planVersionId;
}

public Integer getPlanVersionNumber() {
return planVersionNumber;
}

public void setPlanVersionNumber(Integer planVersionNumber) {
this.planVersionNumber = planVersionNumber;
}

public String getPlanVersionName() {
return planVersionName;
}

public void setPlanVersionName(String planVersionName) {
this.planVersionName = planVersionName;
}

public Object getPlanVersionDescription() {
return planVersionDescription;
}

public void setPlanVersionDescription(Object planVersionDescription) {
this.planVersionDescription = planVersionDescription;
}

public String getPlanVersionStatus() {
return planVersionStatus;
}

public void setPlanVersionStatus(String planVersionStatus) {
this.planVersionStatus = planVersionStatus;
}

public Long getProjectId() {
return projectId;
}

public void setProjectId(Long projectId) {
this.projectId = projectId;
}

public String getProjectNumber() {
return projectNumber;
}

public void setProjectNumber(String projectNumber) {
this.projectNumber = projectNumber;
}

public String getProjectName() {
return projectName;
}

public void setProjectName(String projectName) {
this.projectName = projectName;
}

public Object getBudgetCreationMethod() {
return budgetCreationMethod;
}

public void setBudgetCreationMethod(Object budgetCreationMethod) {
this.budgetCreationMethod = budgetCreationMethod;
}

public Object getSourcePlanVersionId() {
return sourcePlanVersionId;
}

public void setSourcePlanVersionId(Object sourcePlanVersionId) {
this.sourcePlanVersionId = sourcePlanVersionId;
}

public Object getBudgetGenerationSource() {
return budgetGenerationSource;
}

public void setBudgetGenerationSource(Object budgetGenerationSource) {
this.budgetGenerationSource = budgetGenerationSource;
}

public Object getSourcePlanType() {
return sourcePlanType;
}

public void setSourcePlanType(Object sourcePlanType) {
this.sourcePlanType = sourcePlanType;
}

public Object getSourcePlanVersionStatus() {
return sourcePlanVersionStatus;
}

public void setSourcePlanVersionStatus(Object sourcePlanVersionStatus) {
this.sourcePlanVersionStatus = sourcePlanVersionStatus;
}

public Object getSourcePlanVersionNumber() {
return sourcePlanVersionNumber;
}

public void setSourcePlanVersionNumber(Object sourcePlanVersionNumber) {
this.sourcePlanVersionNumber = sourcePlanVersionNumber;
}

public Object getCopyAdjustmentPercentage() {
return copyAdjustmentPercentage;
}

public void setCopyAdjustmentPercentage(Object copyAdjustmentPercentage) {
this.copyAdjustmentPercentage = copyAdjustmentPercentage;
}

public Double getPCRawCostAmounts() {
return pCRawCostAmounts;
}

public void setPCRawCostAmounts(Double pCRawCostAmounts) {
this.pCRawCostAmounts = pCRawCostAmounts;
}

public Double getPCBurdenedCostAmounts() {
return pCBurdenedCostAmounts;
}

public void setPCBurdenedCostAmounts(Double pCBurdenedCostAmounts) {
this.pCBurdenedCostAmounts = pCBurdenedCostAmounts;
}

public Integer getPCRevenueAmounts() {
return pCRevenueAmounts;
}

public void setPCRevenueAmounts(Integer pCRevenueAmounts) {
this.pCRevenueAmounts = pCRevenueAmounts;
}

public Double getPFCRawCostAmounts() {
return pFCRawCostAmounts;
}

public void setPFCRawCostAmounts(Double pFCRawCostAmounts) {
this.pFCRawCostAmounts = pFCRawCostAmounts;
}

public Double getPFCBurdenedCostAmounts() {
return pFCBurdenedCostAmounts;
}

public void setPFCBurdenedCostAmounts(Double pFCBurdenedCostAmounts) {
this.pFCBurdenedCostAmounts = pFCBurdenedCostAmounts;
}

public Integer getPFCRevenueAmounts() {
return pFCRevenueAmounts;
}

public void setPFCRevenueAmounts(Integer pFCRevenueAmounts) {
this.pFCRevenueAmounts = pFCRevenueAmounts;
}

public Object getDeferFinancialPlanCreation() {
return deferFinancialPlanCreation;
}

public void setDeferFinancialPlanCreation(Object deferFinancialPlanCreation) {
this.deferFinancialPlanCreation = deferFinancialPlanCreation;
}

public Object getAdministrativeCode() {
return administrativeCode;
}

public void setAdministrativeCode(Object administrativeCode) {
this.administrativeCode = administrativeCode;
}

    public void setLinkes(String linkes) {
        this.linkes = linkes;
    }

    public String getLinkes() {
        return linkes;
    }
}
