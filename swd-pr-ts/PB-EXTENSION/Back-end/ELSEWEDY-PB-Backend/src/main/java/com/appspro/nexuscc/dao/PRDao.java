package com.appspro.nexuscc.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.nexuscc.bean.PRBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

public class PRDao extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;

    public List<PRBean> getcoulmn() {


        ArrayList<PRBean> list = new ArrayList<PRBean>();
        try {
            connection = AppsproConnection.getConnection();
            String sql = "select BOM_LINENUMBER from xx_sewedy_pr ";
            PreparedStatement pre;
            PRBean bean = new PRBean();
            pre = connection.prepareStatement(sql);       
            ResultSet rs = pre.executeQuery();

            while (rs.next()) {


                bean.setBomLineNumber(rs.getString("BOM_LINENUMBER"));
                list.add(bean);

                ObjectMapper mapper = new ObjectMapper();
                String s;
                s = mapper.writeValueAsString(bean);
            }
        } catch (Exception x) {

        }
        return list;
    }


    public PRBean getsearchbyid(String PROJECTNUMBER) {


        connection = AppsproConnection.getConnection();
        String sql = "select * from xx_sewedy_pr where PROJECTNUMBER =? ";
        PreparedStatement pre;
        PRBean bean = new PRBean();
        try {
            pre = connection.prepareStatement(sql);
            pre.setString(1, PROJECTNUMBER);
            ResultSet rs = pre.executeQuery();

            while (rs.next()) {

                bean.setId(rs.getInt("ID"));
                bean.setBomLineNumber(rs.getString("BOM_LINENUMBER"));
                bean.setLineDescription(rs.getString("LINE_DESCRIPTION"));
                bean.setItemCode(rs.getString("ITEMCODE"));
                bean.setPreviousOrdered(rs.getString("PREVIOUS_ORDERED"));
                bean.setCurrentQuantity(rs.getString("CURRENT_QUANTITY"));
                bean.setTotalQuantity(rs.getString("TOTAL_QUANTITY"));
                bean.setUom(rs.getString("UOM"));
                bean.setItemCost(rs.getString("ITEMCOST"));
                bean.setNeedByDate(rs.getString("NEEDBYDATE"));
                bean.setProjectNumber(rs.getString("PROJECTNUMBER"));
                bean.setTaskNumber(rs.getString("TASKNUMBER"));
                bean.setExpenditureOrganiz(rs.getString("EXPEND_ORGANIZ"));
                bean.setExpenditureType(rs.getString("EXPEND_TYPE"));
                bean.setExpenditureDate(rs.getString("EXPEND_DATE"));
                //                    list.add(bean);

                ObjectMapper mapper = new ObjectMapper();
                String s;
                s = mapper.writeValueAsString(bean);
            }
        } catch (Exception x) {

        }
        return bean;
    }


    public List<PRBean> getallrequest() {
        ArrayList<PRBean> list = new ArrayList<PRBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM xx_sewedy_pr";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                PRBean bean = new PRBean();
                bean.setId(rs.getInt("ID"));
                bean.setBomLineNumber(rs.getString("BOM_LINENUMBER"));
                bean.setLineDescription(rs.getString("LINE_DESCRIPTION"));
                bean.setItemCode(rs.getString("ITEMCODE"));
                bean.setPreviousOrdered(rs.getString("PREVIOUS_ORDERED"));
                bean.setCurrentQuantity(rs.getString("CURRENT_QUANTITY"));
                bean.setTotalQuantity(rs.getString("TOTAL_QUANTITY"));
                bean.setUom(rs.getString("UOM"));
                bean.setItemCost(rs.getString("ITEMCOST"));
                bean.setNeedByDate(rs.getString("NEEDBYDATE"));
                bean.setProjectNumber(rs.getString("PROJECTNUMBER"));
                bean.setTaskNumber(rs.getString("TASKNUMBER"));
                bean.setExpenditureOrganiz(rs.getString("EXPEND_ORGANIZ"));
                bean.setExpenditureType(rs.getString("EXPEND_TYPE"));
                bean.setExpenditureDate(rs.getString("EXPEND_DATE"));
                list.add(bean);

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;


    }

    public PRBean insertOrUpdatePurchaseReq(PRBean bean,
                                            String transactionType) {
        try {

            connection = AppsproConnection.getConnection();
            if (transactionType.equals("ADD")) {

                String query =
                    "INSERT INTO xx_sewedy_pr (BOM_LINENUMBER,LINE_DESCRIPTION,ITEMCODE,PREVIOUS_ORDERED,CURRENT_QUANTITY,TOTAL_QUANTITY,UOM,\n" +
                    "ITEMCOST,NEEDBYDATE,PROJECTNUMBER,TASKNUMBER,EXPEND_ORGANIZ,EXPEND_TYPE,EXPEND_DATE)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getBomLineNumber());
                ps.setString(2, bean.getLineDescription());
                ps.setString(3, bean.getItemCode());
                ps.setString(4, bean.getPreviousOrdered());
                ps.setString(5, bean.getCurrentQuantity());
                ps.setString(6, bean.getTotalQuantity());
                ps.setString(7, bean.getUom());
                ps.setString(8, bean.getItemCost());
                ps.setString(9, bean.getNeedByDate());
                ps.setString(10, bean.getProjectNumber());
                ps.setString(11, bean.getTaskNumber());
                ps.setString(12, bean.getExpenditureOrganiz());
                ps.setString(13, bean.getExpenditureType());
                ps.setString(14, bean.getExpenditureDate());
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {

                String query =
                    "update xx_sewedy_pr set BOM_LINENUMBER=?,LINE_DESCRIPTION=?,ITEMCODE=?,PREVIOUS_ORDERED=?,CURRENT_QUANTITY=?,TOTAL_QUANTITY=?,UOM=?,\n" +
                    "ITEMCOST=?,NEEDBYDATE=?,PROJECTNUMBER=?,TASKNUMBER=?,EXPEND_ORGANIZ=?,EXPEND_TYPE=?,EXPEND_DATE=? where id = ?";

                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getBomLineNumber());
                ps.setString(2, bean.getLineDescription());
                ps.setString(3, bean.getItemCode());
                ps.setString(4, bean.getPreviousOrdered());
                ps.setString(5, bean.getCurrentQuantity());
                ps.setString(6, bean.getTotalQuantity());
                ps.setString(7, bean.getUom());
                ps.setString(8, bean.getItemCost());
                ps.setString(9, bean.getNeedByDate());
                ps.setString(10, bean.getProjectNumber());
                ps.setString(11, bean.getTaskNumber());
                ps.setString(12, bean.getExpenditureOrganiz());
                ps.setString(13, bean.getExpenditureType());
                ps.setString(14, bean.getExpenditureDate());
                ps.setInt(15, bean.getId());
                ps.executeUpdate();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps);
        }
        return bean;
    }

    public String getDeletePr(String id) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult = 0;

        try {
            query = "DELETE FROM xx_sewedy_pr WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            checkResult = ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }


    public List<PRBean> searchsecond(String projectNumber, String taskNumber,
                                     String expenditureOrganiz) {
        List<PRBean> list = new ArrayList();
        Connection connection;
        String sql = "", conditions = "";
        try {
            connection = AppsproConnection.getConnection();

            if (projectNumber != null) {
                conditions += " and PROJECTNUMBER = \'" + projectNumber + "\'";
            }
            if (taskNumber != null) {
                conditions += " and TASKNUMBER like \'%" + taskNumber + "%\'";
            }
            if (expenditureOrganiz != null) {
                conditions +=
                        " and EXPEND_ORGANIZ like \'%" + expenditureOrganiz +
                        "%\'";

            }


            sql =
"select * from xx_sewedy_pr where 1=1 " + conditions + " order by id";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {

                PRBean bean = new PRBean();
                bean.setId(rs.getInt("ID"));
                bean.setBomLineNumber(rs.getString("BOM_LINENUMBER"));
                bean.setLineDescription(rs.getString("LINE_DESCRIPTION"));
                bean.setItemCode(rs.getString("ITEMCODE"));
                bean.setPreviousOrdered(rs.getString("PREVIOUS_ORDERED"));
                bean.setCurrentQuantity(rs.getString("CURRENT_QUANTITY"));
                bean.setTotalQuantity(rs.getString("TOTAL_QUANTITY"));
                bean.setUom(rs.getString("UOM"));
                bean.setItemCost(rs.getString("ITEMCOST"));
                bean.setNeedByDate(rs.getString("NEEDBYDATE"));
                bean.setProjectNumber(rs.getString("PROJECTNUMBER"));
                bean.setTaskNumber(rs.getString("TASKNUMBER"));
                bean.setExpenditureOrganiz(rs.getString("EXPEND_ORGANIZ"));
                bean.setExpenditureType(rs.getString("EXPEND_TYPE"));
                bean.setExpenditureDate(rs.getString("EXPEND_DATE"));
                list.add(bean);
            }
        }

        catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


}
