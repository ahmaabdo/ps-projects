
package com.appspro.nexuscc.bean;


public class SubProject {

    private String no;
    private String costId;
    private String partNo;
    private String supplier;
    private String make;
    private String desciption;
    private String unit;
    private Double totalQty;
    private String currency;
    private Double unitPrice;
    private Double unitCostAed;
    private Double standardDiscount;
    private Double specialDiscount;
    private Double gpEquipment;
    private Double fixedOverhead;
    private Double variableOverhead;
    private Double unitCostAedDubai;
    private Double totalCostAed;
    private Double summaryTotalPriceAed;
    private Double unitPriceAed;
    private Double totalPriceAed;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getCostId() {
        return costId;
    }

    public void setCostId(String costId) {
        this.costId = costId;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(Double totalQty) {
        this.totalQty = totalQty;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getUnitCostAed() {
        return unitCostAed;
    }

    public void setUnitCostAed(Double unitCostAed) {
        this.unitCostAed = unitCostAed;
    }

    public Double getStandardDiscount() {
        return standardDiscount;
    }

    public void setStandardDiscount(Double standardDiscount) {
        this.standardDiscount = standardDiscount;
    }

    public Double getSpecialDiscount() {
        return specialDiscount;
    }

    public void setSpecialDiscount(Double specialDiscount) {
        this.specialDiscount = specialDiscount;
    }

    public Double getGpEquipment() {
        return gpEquipment;
    }

    public void setGpEquipment(Double gpEquipment) {
        this.gpEquipment = gpEquipment;
    }

    public Double getFixedOverhead() {
        return fixedOverhead;
    }

    public void setFixedOverhead(Double fixedOverhead) {
        this.fixedOverhead = fixedOverhead;
    }

    public Double getVariableOverhead() {
        return variableOverhead;
    }

    public void setVariableOverhead(Double variableOverhead) {
        this.variableOverhead = variableOverhead;
    }

    public Double getUnitCostAedDubai() {
        return unitCostAedDubai;
    }

    public void setUnitCostAedDubai(Double unitCostAedDubai) {
        this.unitCostAedDubai = unitCostAedDubai;
    }

    public Double getTotalCostAed() {
        return totalCostAed;
    }

    public void setTotalCostAed(Double totalCostAed) {
        this.totalCostAed = totalCostAed;
    }

    public Double getSummaryTotalPriceAed() {
        return summaryTotalPriceAed;
    }

    public void setSummaryTotalPriceAed(Double summaryTotalPriceAed) {
        this.summaryTotalPriceAed = summaryTotalPriceAed;
    }

    public Double getUnitPriceAed() {
        return unitPriceAed;
    }

    public void setUnitPriceAed(Double unitPriceAed) {
        this.unitPriceAed = unitPriceAed;
    }

    public Double getTotalPriceAed() {
        return totalPriceAed;
    }

    public void setTotalPriceAed(Double totalPriceAed) {
        this.totalPriceAed = totalPriceAed;
    }

}
