package com.appspro.nexuscc.rest;

import com.appspro.nexuscc.bean.OverHeadBean;
import com.appspro.nexuscc.bean.ServicesBean;

import com.appspro.nexuscc.dao.OverHeadDao;
import com.appspro.nexuscc.dao.ServicesDao;

import com.appspro.nexuscc.excel.ExcelHelper;

import java.util.ArrayList;
import java.util.List;

import javax.mail.Service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/services")
public class ServicesRest {


    @POST
    @Path("/newVersion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createNewVersion(String excelJSON) {
            ServicesDao.getInstance().createDataFromExcelSheet(excelJSON);
            return Response.ok("").status(200).build();
    }


//    @GET
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public String getProjectBudgetVersion(){
//        try {
//            List<ServicesBean> allServices =
//                ServicesDao.getInstance().getAllServices();
//            return new JSONArray(allServices).toString();
//        } catch (Exception e) {
//            return e.getMessage();
//        }
//    }
    
   @POST
   @Path("/getServiceByProjectId")
   @Produces(MediaType.APPLICATION_JSON)
   public String findbyid(String data) {
       ArrayList<ServicesBean> list = new ArrayList<ServicesBean>();
       try {
           JSONObject o = new JSONObject(data);
           String projectId =
               o.has("projectId") ? o.getString("projectId") : null;
           String versionId =
               o.has("versionId") ? o.getString("versionId") : null;
           list =   ServicesDao.getInstance().getAllServices(projectId, versionId);
       } catch (Exception e) {
           e.printStackTrace();
       }
       return new JSONArray(list).toString();
   }

    @GET
    @Path("/exportServices")
    @Produces("application/xls")
    public Response doDownload() {
        JSONObject data;
        try {
            data = ServicesDao.getInstance().getDataToBeExported();
            String b64 = ExcelHelper.getInstance().exportServices(data);
            data = new JSONObject();
            data.put("status", "OK");
            data.put("data", b64);
            return Response.ok(data.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            data = new JSONObject();
            data.put("status", "ERROR");
            data.put("data", e.getMessage());
            return Response.ok(data.toString()).build();
        }
    }
    @POST
    @Path("/uploadServices")
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadServiecs(String sheetBody){
        Long planVersionId;
       JSONArray json = new JSONArray(sheetBody);
       planVersionId =  json.getJSONObject(0).getLong("planVersionId");
         String body = sheetBody.toString();
        JSONArray returnData =ServicesDao.getInstance().createPlanedResourse(body);     
           return Response.ok(returnData.toString()).status(200).build();
        }

}

