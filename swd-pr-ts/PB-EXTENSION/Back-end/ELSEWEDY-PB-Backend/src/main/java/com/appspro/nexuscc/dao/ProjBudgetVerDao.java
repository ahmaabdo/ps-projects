package com.appspro.nexuscc.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.nexuscc.bean.ProjectBean;
import com.appspro.nexuscc.bean.ProjectBudgetVersion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProjBudgetVerDao extends AppsproConnection {

    private static ProjBudgetVerDao instance = null;

    public static ProjBudgetVerDao getInstance() {
        if (instance == null)
            instance = new ProjBudgetVerDao();

        return instance;
    }

    public ArrayList<ProjectBudgetVersion> getProjectBudgetVersions(String projectId) throws UnsupportedEncodingException,
                                                                                             MalformedURLException,
                                                                                             IOException {
        ArrayList<ProjectBudgetVersion> projectBudgetVersionsList =
            new ArrayList();

        String serverUrl =
            RestHelper.getInstance().getInstanceUrl() + RestHelper.getInstance().getProjectBudgetURL() +
            "?q=ProjectId=" + projectId;
        try {

            JSONObject obj =
                RestHelper.getInstance().callRest(serverUrl, "GET", null);
            JSONArray arr = obj.getJSONObject("data").getJSONArray("items");

            for (int i = 0; i < arr.length(); i++) {
                ProjectBudgetVersion bean = new ProjectBudgetVersion();
                JSONObject proObj = arr.getJSONObject(i);
                bean.setPlanVersionName(proObj.getString("PlanVersionName"));
                bean.setPlanVersionId(proObj.getLong("PlanVersionId"));
                bean.setPlanVersionStatus(proObj.getString("PlanVersionStatus"));
                bean.setFinancialPlanType(proObj.getString("FinancialPlanType"));
                bean.setProjectName(proObj.getString("ProjectName"));
                bean.setProjectId(proObj.getLong("ProjectId"));
                bean.setLinkes(proObj.get("links").toString());
                if (proObj.get("PCRevenueAmounts") != JSONObject.NULL)
                    bean.setPCRevenueAmounts(proObj.getInt("PCRevenueAmounts"));
                if (proObj.get("PCRawCostAmounts") != JSONObject.NULL)
                    bean.setPCRawCostAmounts(proObj.getDouble("PCRawCostAmounts"));
                projectBudgetVersionsList.add(bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return projectBudgetVersionsList;
    }


}
