package com.appspro.nexuscc.bean;

public class InvoiceBean {

    private int id;
    private String srNo;
    private String milestoneDes;
    private String invDate;
    private String pmtTerm;
    private String invDateDays;
    private String percent;
    private String amountAED;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setMilestoneDes(String milestoneDes) {
        this.milestoneDes = milestoneDes;
    }

    public String getMilestoneDes() {
        return milestoneDes;
    }

    public void setInvDate(String invDate) {
        this.invDate = invDate;
    }

    public String getInvDate() {
        return invDate;
    }

    public void setPmtTerm(String pmtTerm) {
        this.pmtTerm = pmtTerm;
    }

    public String getPmtTerm() {
        return pmtTerm;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getPercent() {
        return percent;
    }

    public void setAmountAED(String amountAED) {
        this.amountAED = amountAED;
    }

    public String getAmountAED() {
        return amountAED;
    }

    public void setInvDateDays(String invDateDays) {
        this.invDateDays = invDateDays;
    }

    public String getInvDateDays() {
        return invDateDays;
    }
}
