package com.appspro.nexuscc.bean;

public class ProjectBean {

        private Long projectId;
        private String owningOrganizationName;
        private String projectName;
        private String projectNumber;
        private String projectManagerName;
        private String projectStatus;
        private String projectStartDate;
        private String projectEndDate;
        private String linkes;


    public void setOwningOrganizationName(String owningOrganizationName) {
        this.owningOrganizationName = owningOrganizationName;
    }

    public String getOwningOrganizationName() {
        return owningOrganizationName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectManagerName(String projectManagerName) {
        this.projectManagerName = projectManagerName;
    }

    public String getProjectManagerName() {
        return projectManagerName;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setLinkes(String linkes) {
        this.linkes = linkes;
    }

    public String getLinkes() {
        return linkes;
    }

    public void setProjectStartDate(String projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public String getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectEndDate(String projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public String getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectId() {
        return projectId;
    }
}
