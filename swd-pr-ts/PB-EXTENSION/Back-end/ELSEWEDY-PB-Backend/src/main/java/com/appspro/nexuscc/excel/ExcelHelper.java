package com.appspro.nexuscc.excel;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.hssf.util.HSSFCellUtil;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.json.JSONArray;
import org.json.JSONObject;


public class ExcelHelper {
    private static ExcelHelper instance = null;

    public static ExcelHelper getInstance() {
        if (instance == null)
            instance = new ExcelHelper();
        return instance;
    }
    public String exportServices(JSONObject obj) {

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet realSheet = workbook.createSheet("Sheet xls");
       
        Row header = realSheet.createRow(0);
        header.setHeight((short)700);

        CellStyle style = workbook.createCellStyle();
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        
        String[] arrHeader = {"Item", "Description","Bidding Estimate","Purchase Estimate",
                              "Supplier AED","Supplier AED","Supplier AED","Supplier AED",
                              "Qty","Unit","Total (AED)","GP_Services","Total (AED)"};

        for (int x = 0; x < arrHeader.length; x++) {
            header.createCell(x).setCellValue(arrHeader[x]);
        }
        

        //loop over rows
        JSONArray rows = obj.getJSONArray("data");
        for (int x = 0; x < rows.length(); x++) {
            int c_row_index = x + 1;
            HSSFRow row = realSheet.createRow(c_row_index);

            JSONObject rowData = rows.getJSONObject(x);
            row.createCell(0).setCellValue(rowData.get("ITEM").toString());
            row.createCell(1).setCellValue(rowData.get("DESCRIPTION").toString());
            row.createCell(2).setCellValue(rowData.get("BIDDING_ESTIMATE").toString());
            row.createCell(3).setCellValue(rowData.get("PURCHASE_ESTIMATE").toString());
            row.createCell(4).setCellValue(rowData.get("SUPPLIER_AED").toString());
            row.createCell(5).setCellValue(rowData.get("SUPPLIER_AED_1").toString());
            row.createCell(6).setCellValue(rowData.get("SUPPLIER_AED_2").toString());
            row.createCell(7).setCellValue(rowData.get("SUPPLIER_AED_3").toString());
            row.createCell(8).setCellValue(rowData.get("QTY").toString());
            row.createCell(9).setCellValue(rowData.get("UNIT").toString());
            row.createCell(10).setCellValue(rowData.get("TOTAL_AED").toString());
            row.createCell(11).setCellValue(rowData.get("GP_SERVICES").toString());
            row.createCell(12).setCellValue(rowData.get("TOTAL_AED_1").toString());
            
        }

        for (int x = 0; x < header.getLastCellNum(); x++) {
            realSheet.autoSizeColumn(x);
        }

        realSheet.setColumnWidth(9, 5000);

        String b64 = workBookToString(workbook);
        return b64;
    }

    public String exportVersionData(JSONObject obj) {
        if (obj.get("status").toString().equalsIgnoreCase("error"))
            return obj.toString();


        HSSFWorkbook workbook = new HSSFWorkbook();


        HSSFSheet realSheet = workbook.createSheet("Sheet xls");
        HSSFSheet hiddenSheet = workbook.createSheet("metadata");
        Row rowmeta = hiddenSheet.createRow(0);
        rowmeta.createCell(0).setCellValue("created");
        workbook.setSheetHidden(1, true);
        //        Row row1 = realSheet.createRow(0);
        //        row1.createCell(0).setCellValue("Project");
        //        row1.createCell(1).setCellValue("Project Name");
        //
        //        Row row2 = realSheet.createRow(1);
        //        row2.createCell(0).setCellValue("Customer");
        //        row2.createCell(1).setCellValue("Customer Name");
        //
        //        Row row3 = realSheet.createRow(2);
        //        row3.createCell(0).setCellValue("Quote No.");
        //        row3.createCell(1).setCellValue("");
        //
        //        Row row4 = realSheet.createRow(3);
        //        row4.createCell(0).setCellValue("Date");
        //        row4.createCell(1).setCellValue("1993-11-10");


        Row header = realSheet.createRow(0);
        header.setHeight((short)700);


        String headerMappedWithBody =
            "[{ \"label\": \"Pos\", \"dbkey\": \"POS\" }, { \"label\": \"refrence\", \"dbkey\": \"REFERENCE\" }, { \"label\": \"costId\", \"dbkey\": \"COST_ID\" }, { \"label\": \"resourceName\", \"dbkey\": \"RESOURCE_NAME\" }, { \"label\": \"partNo\", \"dbkey\": \"PART_NO\" }, { \"label\": \"supplier\", \"dbkey\": \"SUPPLIER\" }, { \"label\": \"manufacturer\", \"dbkey\": \"MANUFACTURER\" }, { \"label\": \"startDate\", \"dbkey\": \"START_DATE\" }, { \"label\": \"finishDate\", \"dbkey\": \"FINISH_DATE\" }, { \"label\": \"desciption\", \"dbkey\": \"DESCRIPTION\" },\n" +
            "                        { \"label\": \"site1\", \"dbkey\": \"SITE_1\" }, { \"label\": \"site2\", \"dbkey\": \"SITE_2\" }, { \"label\": \"site3\", \"dbkey\": \"SITE_3\" }, { \"label\": \"site4\", \"dbkey\": \"SITE_4\" }, { \"label\": \"site5\", \"dbkey\": \"SITE_5\" }, { \"label\": \"site6\", \"dbkey\": \"SITE_6\" }, { \"label\": \"site7\", \"dbkey\": \"SITE_7\" }, { \"label\": \"site8\", \"dbkey\": \"SITE_8\" }, { \"label\": \"site9\", \"dbkey\": \"SITE_9\" }, { \"label\": \"site10\", \"dbkey\": \"SITE_10\" }, { \"label\": \"site11\", \"dbkey\": \"SITE_11\" }, { \"label\": \"site12\", \"dbkey\": \"SITE_12\" }, { \"label\": \"site13\", \"dbkey\": \"SITE_13\" }, { \"label\": \"site14\", \"dbkey\": \"SITE_14\" }, { \"label\": \"site15\", \"dbkey\": \"SITE_15\" }, { \"label\": \"site16\", \"dbkey\": \"SITE_16\" }, { \"label\": \"site17\", \"dbkey\": \"SITE_17\" }, { \"label\": \"site18\", \"dbkey\": \"SITE_18\" }, { \"label\": \"site19\", \"dbkey\": \"SITE_19\" }, { \"label\": \"site20\", \"dbkey\": \"SITE_20\" }, { \"label\": \"site21\", \"dbkey\": \"SITE_21\" }, { \"label\": \"site22\", \"dbkey\": \"SITE_22\" }, { \"label\": \"site23\", \"dbkey\": \"SITE_23\" }, { \"label\": \"site24\", \"dbkey\": \"SITE_24\" }, { \"label\": \"site25\", \"dbkey\": \"SITE_25\" }, { \"label\": \"site26\", \"dbkey\": \"SITE_26\" }, { \"label\": \"site27\", \"dbkey\": \"SITE_27\" }, { \"label\": \"site28\", \"dbkey\": \"SITE_28\" }, { \"label\": \"site29\", \"dbkey\": \"SITE_29\" }, { \"label\": \"site30\", \"dbkey\": \"SITE_30\" }, { \"label\": \"site31\", \"dbkey\": \"SITE_31\" }, { \"label\": \"site32\", \"dbkey\": \"SITE_32\" }, { \"label\": \"site33\", \"dbkey\": \"SITE_33\" }, { \"label\": \"site34\", \"dbkey\": \"SITE_34\" }, { \"label\": \"site35\", \"dbkey\": \"SITE_35\" }, { \"label\": \"site36\", \"dbkey\": \"SITE_36\" }, { \"label\": \"site37\", \"dbkey\": \"SITE_37\" }, { \"label\": \"site38\", \"dbkey\": \"SITE_38\" }, { \"label\": \"site39\", \"dbkey\": \"SITE_39\" }, { \"label\": \"site40\", \"dbkey\": \"SITE_40\" }, { \"label\": \"site41\", \"dbkey\": \"SITE_41\" }, { \"label\": \"site42\", \"dbkey\": \"SITE_42\" }, { \"label\": \"site43\", \"dbkey\": \"SITE_43\" }, { \"label\": \"site44\", \"dbkey\": \"SITE_44\" }, { \"label\": \"site45\", \"dbkey\": \"SITE_45\" }, { \"label\": \"site46\", \"dbkey\": \"SITE_46\" }, { \"label\": \"site47\", \"dbkey\": \"SITE_47\" }, { \"label\": \"site48\", \"dbkey\": \"SITE_48\" }, { \"label\": \"site49\", \"dbkey\": \"SITE_49\" }, { \"label\": \"site50\", \"dbkey\": \"SITE_50\" }, { \"label\": \"site51\", \"dbkey\": \"SITE_51\" }, { \"label\": \"site52\", \"dbkey\": \"SITE_52\" }, { \"label\": \"site53\", \"dbkey\": \"SITE_53\" }, { \"label\": \"site54\", \"dbkey\": \"SITE_54\" }, { \"label\": \"site55\", \"dbkey\": \"SITE_55\" }, { \"label\": \"site56\", \"dbkey\": \"SITE_56\" }, { \"label\": \"site57\", \"dbkey\": \"SITE_57\" }, { \"label\": \"site58\", \"dbkey\": \"SITE_58\" }, { \"label\": \"site59\", \"dbkey\": \"SITE_59\" }, { \"label\": \"site60\", \"dbkey\": \"SITE_60\" }, { \"label\": \"site61\", \"dbkey\": \"SITE_61\" }, { \"label\": \"site62\", \"dbkey\": \"SITE_62\" }, { \"label\": \"site63\", \"dbkey\": \"SITE_63\" }, { \"label\": \"site64\", \"dbkey\": \"SITE_64\" }, { \"label\": \"site65\", \"dbkey\": \"SITE_65\" }, { \"label\": \"site66\", \"dbkey\": \"SITE_66\" }, { \"label\": \"site67\", \"dbkey\": \"SITE_67\" }, { \"label\": \"site68\", \"dbkey\": \"SITE_68\" }, { \"label\": \"site69\", \"dbkey\": \"SITE_69\" }, { \"label\": \"site70\", \"dbkey\": \"SITE_70\" },\n" +
            "                        { \"label\": \"unitOfMeasure\", \"dbkey\": \"UNIT_OF_MEASURE\" }, { \"label\": \"totalQtyHours\", \"dbkey\": \"TOTAL_QTY\" }, { \"label\": \"BudgetCurrencyAed\", \"dbkey\": \"BUDGET_CURRENCY_AED\" }, { \"label\": \"foreignCurrency\", \"dbkey\": \"FOREIGN_CURRENCY\" }, { \"label\": \"conversionRate\", \"dbkey\": \"CONVERSION_RATE\" }, { \"label\": \"lastBudgetPrice\", \"dbkey\": \"LAST_BUDGET_PRICE\" }, { \"label\": \"lastBudgetPriceCurrency\", \"dbkey\": \"LAST_BUDGET_PRICE_CURRENCY\" }, { \"label\": \"unitPriceRate\", \"dbkey\": \"UNIT_PRICE\" }, { \"label\": \"unitCostAed\", \"dbkey\": \"UNIT_COST_AED\" }, { \"label\": \"totalCostAed\", \"dbkey\": \"TOTAL_COST_AED\" }, { label: '', dbkey: '' }, { \"label\": \"lastStandardDiscount\", \"dbkey\": \"LAST_STANDARD_DISCOUNT\" }, { \"label\": \"standardDiscount\", \"dbkey\": \"STANDARD_DISCOUNT\" }, { \"label\": \"specialDiscount\", \"dbkey\": \"SPECIAL_DISCOUNT\" }, { \"label\": \"unitCostAedDiscounted\", \"dbkey\": \"UNIT_COST_AED_DISCOUNTED\" }, { \"label\": \"totalCostAedDiscounted\", \"dbkey\": \"TOTAL_COST_AED_DISCOUNTED\" }, { \"label\": \"gpEquipment\", \"dbkey\": \"GP_EQUIPMENT\" }, { \"label\": \"fixedOverhead\", \"dbkey\": \"FIXED_OVERHEAD\" }, { \"label\": \"variableOverhead\", \"dbkey\": \"VARIABLE_OVERHEAD\" }, { \"label\": \"unitCostAedDubai\", \"dbkey\": \"UNIT_COST_AED_DUBAI\" }, { \"label\": \"totalCostAed\", \"dbkey\": \"TOTAL_COST_AED\" }, { \"label\": \"summaryTotalPriceAed\", \"dbkey\": \"SUMMARY_TOTAL_PRICE_AED\" }, { \"label\": \"unitPriceFc\", \"dbkey\": \"UNIT_PRICE_FC\" }, { \"label\": \"totalPriceFc\", \"dbkey\": \"TOTAL_PRICE_FC\" }, { \"label\": \"summaryTotalPriceAed\", \"dbkey\": \"SUMMARY_TOTAL_PRICE_AED\" }]";

        JSONArray headerMappedWithBodyArr =
            new JSONArray(headerMappedWithBody);


        //create header
        CellStyle style = workbook.createCellStyle();
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        for (int x = 0; x < headerMappedWithBodyArr.length(); x++) {
            Cell cell = header.createCell(x);
            cell.setCellValue(headerMappedWithBodyArr.getJSONObject(x).get("label").toString());
            cell.setCellStyle(style);
            //            colorCells(workbook, realSheet, 7, x);
        }

        //loop over rows
        JSONArray rows = obj.getJSONArray("data");
        for (int x = 0; x < rows.length(); x++) {
            int c_row_index = x + 1;
            int c_row_ser = x + 2;
            HSSFRow row = realSheet.createRow(c_row_index);

            JSONObject rowData = rows.getJSONObject(x);
            for (int c = 0; c < headerMappedWithBodyArr.length(); c++) {
                String dbKey =
                    headerMappedWithBodyArr.getJSONObject(c).get("dbkey").toString();
                String val =
                    rowData.has(dbKey) ? rowData.get(dbKey).toString() : "";
                String formla = null;

                //insert equation instead of values (equations printed as string)
                if (dbKey.equalsIgnoreCase("TOTAL_COST_AED"))
                    formla =
                            String.format("CD%d*CV%d", c_row_ser, c_row_ser); //=CD11*CV11

                else if (dbKey.equalsIgnoreCase("UNIT_COST_AED_DISCOUNTED"))
                    formla =
                            String.format("CK%d*(1-CO%d-CP%d)", c_row_ser, c_row_ser,
                                          c_row_ser);

                else if (dbKey.equalsIgnoreCase("TOTAL_COST_AED_DISCOUNTED"))
                    formla = String.format("CD%d*CQ%d", c_row_ser, c_row_ser);


                else if (dbKey.equalsIgnoreCase("UNIT_COST_AED_DUBAI"))
                    formla =
                            String.format("CQ%d/(1-SUM(CS%d:CU%d))", c_row_ser,
                                          c_row_ser, c_row_ser);

                else if (dbKey.equalsIgnoreCase("TOTAL_PRICE_FC"))
                    formla = String.format("CY%d*CD%d", c_row_ser, c_row_ser);

                if (dbKey.equalsIgnoreCase("SPECIAL_DISCOUNT") ||
                    dbKey.equalsIgnoreCase("STANDARD_DISCOUNT") ||
                    dbKey.equalsIgnoreCase("FIXED_OVERHEAD") ||
                    dbKey.equalsIgnoreCase("GP_EQUIPMENT") ||
                    dbKey.equalsIgnoreCase("VARIABLE_OVERHEAD")) {
                    double num = Double.parseDouble(val);
                    if (num * 100 > 100) { //
                        //                        num = num / 100;
                        val = val + "%";
                    } else {
                        val = (num * 100) + "%";
                    }
                }

                Cell cel = row.createCell(c);
                if (formla != null) {
                    //                    cel.setCellType(HSSFCell.CELL_TYPE_FORMULA);
                    cel.setCellFormula(formla);
                    System.out.println(formla);
                } else {
                    //                    System.out.println(val);
                    cel.setCellValue(val);
                }
            }
        }

        for (int x = 0; x < header.getLastCellNum(); x++) {
            realSheet.autoSizeColumn(x);
        }

        realSheet.setColumnWidth(9, 5000);
        //
        //        realSheet.getRow(0).setHeight((short)2);
        //        realSheet.getRow(1).setHeight((short)2);
        //        realSheet.getRow(6).setHeight((short)2);
        //        realSheet.getRow(8).setHeight((short)2);
        //        realSheet.getRow(9).setHeight((short)2);


        String b64 = workBookToString(workbook);
        stringToFile(b64, "D:\\hussein\\tools\\upload\\book1.xls");
        return b64;
    }

    private void colorCells(Workbook workbook, Sheet sheet, int rowIndex,
                            int cellIndex) {
        //        System.out.println(rowIndex + " "+cellIndex);
        //        CellStyle redBackgroundStyle = workbook.createCellStyle();
        //        redBackgroundStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        //        redBackgroundStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        //        redBackgroundStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        //        sheet.getRow(rowIndex).getCell(cellIndex).setCellStyle(redBackgroundStyle);
    }

    private static String workBookToString(Workbook workbook) {
        try {
            ByteArrayOutputStream byteArrayOutputStream =
                new ByteArrayOutputStream();

            workbook.write(byteArrayOutputStream);
            String b64 =
                Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
            return b64;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void stringToFile(String b64, String path) {
        //base64 to file
        try {
            byte[] bytes = Base64.decodeBase64(b64);
            FileOutputStream stream = new FileOutputStream(path);
            stream.write(bytes);
            stream.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String obj =
            "{ \"data\": [{ \"UNIT_COST_AED_DISCOUNTED\": \"9437.58\", \"COST_ID\": \"01\", \"PART_NO\": \"Sheriff, Ahmed\", \"TOTAL_COST_AED\": \"536226.17\", \"SUMMARY_TOTAL_PRICE_AED\": \"0\", \"UNIT_PRICE\": \"3,027.00\", \"SUPPLIER\": \"Scneider Electric\", \"TOTAL_PRICE_FC\": \"536226.17\", \"DESCRIPTION\": \"Superb quality indoor/outdoor pendant PTZ dome camera with 1080p HD resolution; 20x optical zoom; IVA; PoE; iSCSI/SD; multiple pre-programmed user modes; H.264 quad-streaming (CPP4); clear bubble. Rated IP66\", \"UNIT_COST_AED_DUBAI\": \"10724.52\", \"ID\": 5, \"LAST_STANDARD_DISCOUNT\": \"0\", \"PROJECT_ID\": \"300000002187836\", \"GP_EQUIPMENT\": \"7\", \"CONVERSION_RATE\": \"USD\", \"VARIABLE_OVERHEAD\": \"5\", \"FIXED_OVERHEAD\": \"0\", \"START_DATE\": \"2019-11-10\", \"UNIT_OF_MEASURE\": \"Nos.\", \"TOTAL_COST_AED_DISCOUNTED\": \"471879.03\", \"SPECIAL_DISCOUNT\": \"5\", \"VERSION_ID\": \"300000002364327\", \"POS\": \"1.01\", \"UNIT_COST_AED\": \"11103.04\", \"TOTAL_QTY\": \"50\", \"UNIT_PRICE_FC\": \"10724.52\", \"STANDARD_DISCOUNT\": \"10\", \"FINISH_DATE\": \"2019-11-10\", \"RESOURCE_NAME\": \"Material\", \"FOREIGN_CURRENCY\": \"USD\" }], \"status\": \"OK\" }";

        ExcelHelper.getInstance().exportVersionData(new JSONObject(obj));
    }
}
