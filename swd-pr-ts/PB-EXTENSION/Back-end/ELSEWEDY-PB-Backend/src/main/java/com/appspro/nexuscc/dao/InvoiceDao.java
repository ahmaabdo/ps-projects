package com.appspro.nexuscc.dao;

import static biPReports.RestHelper.getSchema_Name;

import com.appspro.db.AppsproConnection;
import com.appspro.nexuscc.bean.InvoiceBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import java.util.ArrayList;

import org.json.JSONObject;

public class InvoiceDao extends AppsproConnection {
    Connection connection = null;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    private static InvoiceDao instance;

    public static InvoiceDao getInstance() {
        if (instance == null)
            instance = new InvoiceDao();

        return instance;
    }


    public ArrayList<InvoiceBean> getAllInvoice(String projectVer, String projectId) {
        ArrayList<InvoiceBean> list = new ArrayList<InvoiceBean>();
        try {
            String query = null;
            query =
                    "SELECT * FROM " + getSchema_Name() + ".XXX_INVOICES WHERE PROJECT_VER = ? AND PROJECT_ID = ? ORDER BY SR_NO ASC";
            connection = AppsproConnection.getConnection();
            ps = connection.prepareStatement(query);
            ps.setString(1, projectVer);
            ps.setString(2, projectId);
            rs = ps.executeQuery();
            while (rs.next()) {
                InvoiceBean bean = new InvoiceBean();
                bean.setId(rs.getInt("ID"));
                bean.setSrNo(rs.getString("SR_NO"));
                bean.setMilestoneDes(rs.getString("MILESTONE_DESCRIPTION"));
                bean.setInvDate(rs.getString("INV_DATE"));
                bean.setPmtTerm(rs.getString("PMT_TERM"));
                bean.setInvDateDays(rs.getString("INV_DATE_DAYS"));
                bean.setPercent(rs.getString("PERCENT"));
                bean.setAmountAED(rs.getString("AMOUNT_AED"));
                list.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public String deleteAnInvoice(String id) {
        try {
            String query = null;

            connection = AppsproConnection.getConnection();
            query =
                    "DELETE FROM " + getSchema_Name() + ".XXX_INVOICES WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, id);

            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return "";

    }

    public String createNewInvoice(String body) {
        try {
            String query = null;
            JSONObject item = new JSONObject(body);

            connection = AppsproConnection.getConnection();
            query =
                    "INSERT INTO " + getSchema_Name() + ".XXX_INVOICES (SR_NO, MILESTONE_DESCRIPTION, INV_DATE, PMT_TERM, INV_DATE_DAYS," +
                    "PERCENT, AMOUNT_AED, PROJECT_VER, PROJECT_ID) VALUES (?,?,?,?,?,?,?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, item.get("srNo").toString());
            ps.setString(2, item.get("milestoneDes").toString());
            ps.setString(3, item.get("invDate").toString());
            ps.setString(4, item.get("pmtTerm").toString());
            ps.setString(5, item.get("invDateDays").toString());
            ps.setString(6, item.get("percent").toString());
            ps.setString(7, item.get("amountAED").toString());
            ps.setString(8, item.get("projectVer").toString());
            ps.setString(9, item.get("projectId").toString());

            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return "";
    }


}
