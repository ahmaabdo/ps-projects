/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.dao;

import com.appspro.helper.Helper;

import java.io.IOException;
import java.io.InputStream;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author Anas Alghawi
 */
public class PINumberDAO {

    private static String cloudURL = Helper.paasURL;
    private static String testURL =
        "http://10.10.10.142:8060/ords/bsbc_ws/PInv/";


    private static String streamToString(InputStream inputStream) {
        String text =
            new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

    public static String jsonGetRequestNonParam(String endpoint,
                                                String bUName) {
        String json = null;
        try {
            //            URL url = new URL(cloudURL+endpoint);
            URL url =
                new URL(null, cloudURL + endpoint, new sun.net.www.protocol.https.Handler());
            HttpURLConnection connection = null;
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                Helper.trustAllHosts();
                HttpsURLConnection https =
                    (javax.net.ssl.HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                https.setHostnameVerifier(Helper.DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }


            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("BUNAME", bUName);
            connection.connect();
            InputStream inStream = connection.getInputStream();
            json = streamToString(inStream); // input stream to string
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println(json);
        return json;
    }
}
