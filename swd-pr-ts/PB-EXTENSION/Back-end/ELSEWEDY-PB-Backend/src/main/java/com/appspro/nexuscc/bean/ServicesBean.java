package com.appspro.nexuscc.bean;

public class ServicesBean {

    private int id;
    private String item;
    private String description;
    private String biddingEstimate;
    private String purchaseEstimate;
    private String supplierAED;
    private String supplierAED_1;
    private String supplierAED_2;
    private String supplierAED_3;
    private String qty;
    private String unit;
    private String totalAED;
    private String totalAED_1;
    private String gpServices;
    
    private String taskNumberRef;
    private String projectService;
    private String rates;
    private String hours;
    private String costs;
    private String dateFrom;
    private String dateTo;
    

    public void setItem(String item) {
        this.item = item;
    }

    public String getItem() {
        return item;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setBiddingEstimate(String biddingEstimate) {
        this.biddingEstimate = biddingEstimate;
    }

    public String getBiddingEstimate() {
        return biddingEstimate;
    }

    public void setPurchaseEstimate(String purchaseEstimate) {
        this.purchaseEstimate = purchaseEstimate;
    }

    public String getPurchaseEstimate() {
        return purchaseEstimate;
    }

    public void setSupplierAED(String supplierAED) {
        this.supplierAED = supplierAED;
    }

    public String getSupplierAED() {
        return supplierAED;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQty() {
        return qty;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setTotalAED(String totalAED) {
        this.totalAED = totalAED;
    }

    public String getTotalAED() {
        return totalAED;
    }

    public void setGpServices(String gpServices) {
        this.gpServices = gpServices;
    }

    public String getGpServices() {
        return gpServices;
    }

    public void setSupplierAED_1(String supplierAED_1) {
        this.supplierAED_1 = supplierAED_1;
    }

    public String getSupplierAED_1() {
        return supplierAED_1;
    }

    public void setSupplierAED_2(String supplierAED_2) {
        this.supplierAED_2 = supplierAED_2;
    }

    public String getSupplierAED_2() {
        return supplierAED_2;
    }

    public void setSupplierAED_3(String supplierAED_3) {
        this.supplierAED_3 = supplierAED_3;
    }

    public String getSupplierAED_3() {
        return supplierAED_3;
    }

    public void setTotalAED_1(String totalAED_1) {
        this.totalAED_1 = totalAED_1;
    }

    public String getTotalAED_1() {
        return totalAED_1;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTaskNumberRef(String taskNumberRef) {
        this.taskNumberRef = taskNumberRef;
    }

    public String getTaskNumberRef() {
        return taskNumberRef;
    }

    public void setProjectService(String projectService) {
        this.projectService = projectService;
    }

    public String getProjectService() {
        return projectService;
    }

    public void setRates(String rates) {
        this.rates = rates;
    }

    public String getRates() {
        return rates;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getHours() {
        return hours;
    }

    public void setCosts(String costs) {
        this.costs = costs;
    }

    public String getCosts() {
        return costs;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateTo() {
        return dateTo;
    }
}
