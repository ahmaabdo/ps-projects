package com.appspro.nexuscc.dao;

import biPReports.BIReportModel;
import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.nexuscc.rest.BIReportService;
import com.appspro.nexuscc.bean.BudgetLine;
import com.appspro.nexuscc.bean.BudgetLineDetails;
import com.appspro.nexuscc.bean.ProjectBean;

import com.appspro.nexuscc.bean.ProjectBudgetVersion;

import com.appspro.nexuscc.bean.ProjectBudgetVersionDetails;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


import java.net.URLEncoder;


import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.ResultSet;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.codehaus.jackson.map.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;


public class projectDAO extends RestHelper {

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static Connection connection;
    private static PreparedStatement ps;
    private static ResultSet rs;
    private static projectDAO instance = null;

    public static projectDAO getInstance() {
        if (instance == null)
            instance = new projectDAO();
        return instance;
    }

    public static void main(String[] args) {
//        JSONArray arr = getInstance().getDataToBeExported("300000002187836", "300000002246503");
//        System.out.println(arr.length());
        //        try {
        //        String data = getInstance().getDataToBeExported("300000001811128");
        //        System.out.println(data);


        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
    }

    public ArrayList<ProjectBean> getProDetails() {
        ArrayList<ProjectBean> projectList = new ArrayList<ProjectBean>();
        boolean hasMore = false;
        int offset = 0;
        do {
            String serverUrl =
                getInstanceUrl() + getProjectUrl() + "?limit=500&offset=" +
                offset;
            try {
                JSONObject obj =
                    RestHelper.getInstance().callRest(serverUrl, "GET", null);
                JSONObject data = obj.getJSONObject("data");

                JSONArray arr = data.getJSONArray("items");

                for (int i = 0; i < arr.length(); i++) {
                    ProjectBean bean = new ProjectBean();
                    JSONObject proObj = arr.getJSONObject(i);
                    bean.setProjectName(proObj.get("ProjectName").toString());
                    bean.setProjectId(proObj.getLong("ProjectId"));
                    bean.setProjectNumber(proObj.get("ProjectNumber").toString());
                    bean.setOwningOrganizationName(proObj.has("OwningOrganizationName") ?
                                                   proObj.get("OwningOrganizationName").toString() :
                                                   "");
                    bean.setProjectManagerName(proObj.has("ProjectManagerName") ?
                                               proObj.get("ProjectManagerName").toString() :
                                               "");
                    bean.setProjectStatus(proObj.has("ProjectStatus") ?
                                          proObj.get("ProjectStatus").toString() :
                                          "");
                    bean.setProjectEndDate(proObj.has("ProjectEndDate") ?
                                           proObj.get("ProjectEndDate").toString() :
                                           "");
                    bean.setProjectStartDate(proObj.has("ProjectStartDate") ?
                                             proObj.get("ProjectStartDate").toString() :
                                             "");
                    bean.setLinkes(proObj.get("links").toString());
                    projectList.add(bean);
                }
                hasMore = data.getBoolean("hasMore");
                offset += 500;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (hasMore);
        return projectList;
    }

    //    public ArrayList<ProjectBudgetVersion> getProjectBudgetVersions(String projectId) throws UnsupportedEncodingException,
    //                                                                                             MalformedURLException,
    //                                                                                             IOException {
    //        ArrayList<ProjectBudgetVersion> projectBudgetVersionsList =
    //            new ArrayList();
    //        String finalresponse = "";
    //
    //        HttpsURLConnection https = null;
    //        HttpURLConnection connection = null;
    //
    //        String serverUrl =
    //            getInstanceUrl() + getProjectBudgetURL() + "?q=ProjectId=" +
    //            projectId;
    //        String jsonResponse = "";
    //
    //        URL url = new URL(serverUrl);
    //        if (url.getProtocol().toLowerCase().equals("https")) {
    //            trustAllHosts();
    //            https = (HttpsURLConnection)url.openConnection();
    //            https.setHostnameVerifier(DO_NOT_VERIFY);
    //            connection = https;
    //        } else {
    //            connection = (HttpURLConnection)url.openConnection();
    //        }
    //        String SOAPAction = getInstanceUrl();
    //        connection.setDoOutput(true);
    //
    //        connection.setRequestMethod("GET");
    //        connection.setRequestProperty("Content-Type", "application/json;");
    //        connection.setRequestProperty("Accept", "application/json");
    //        connection.setConnectTimeout(6000000);
    //        connection.setRequestProperty("SOAPAction", SOAPAction);
    //        connection.setRequestProperty("charset", "utf-8");
    //        connection.setRequestProperty("Authorization", "Basic " + getAuth());
    //
    //        BufferedReader in =
    //            new BufferedReader(new InputStreamReader(connection.getInputStream()));
    //        StringBuffer response = new StringBuffer();
    //        String inputLine;
    //        while ((inputLine = in.readLine()) != null) {
    //            response.append(inputLine);
    //        }
    //        in.close();
    //
    //        JSONObject obj = new JSONObject(response.toString());
    //        JSONArray arr = obj.getJSONArray("items");
    //        System.out.println(arr.get(0));
    //        for (int i = 0; i < arr.length(); i++) {
    //            ProjectBudgetVersion bean = new ProjectBudgetVersion();
    //            JSONObject proObj = arr.getJSONObject(i);
    //            bean.setPlanVersionName(proObj.getString("PlanVersionName"));
    //            bean.setPlanVersionId(proObj.getLong("PlanVersionId"));
    //            bean.setPlanVersionStatus(proObj.getString("PlanVersionStatus"));
    //            bean.setFinancialPlanType(proObj.getString("FinancialPlanType"));
    //            bean.setProjectName(proObj.getString("ProjectName"));
    //            bean.setProjectId(proObj.getInt("ProjectId"));
    //            bean.setLinkes(proObj.get("links").toString());
    //            if (proObj.get("PCRevenueAmounts") != JSONObject.NULL)
    //                bean.setPCRevenueAmounts(proObj.getInt("PCRevenueAmounts"));
    //            if (proObj.get("PCRawCostAmounts") != JSONObject.NULL)
    //                bean.setPCRawCostAmounts(proObj.getDouble("PCRawCostAmounts"));
    //            projectBudgetVersionsList.add(bean);
    //
    //
    //        }
    //
    //        if (finalresponse.length() > 1) {
    //        } else {
    //            finalresponse = jsonResponse;
    //        }
    //
    //        return projectBudgetVersionsList;
    //    }


    //    public ArrayList<ProjectBudgetVersionDetails> getProjectBudgetVersionLines(String planVersionId) throws UnsupportedEncodingException {
    //        ArrayList<ProjectBudgetVersionDetails> projectBudgetLines =
    //            new ArrayList();
    //        String finalresponse = "";
    //
    //        HttpsURLConnection https = null;
    //        HttpURLConnection connection = null;
    //
    //        String serverUrl =
    //            getInstanceUrl() + getProjectBudgetLinesURL() + planVersionId;
    //        String jsonResponse = "";
    //        try {
    //            URL url = new URL(serverUrl);
    //            if (url.getProtocol().toLowerCase().equals("https")) {
    //                trustAllHosts();
    //                https = (HttpsURLConnection)url.openConnection();
    //                https.setHostnameVerifier(DO_NOT_VERIFY);
    //                connection = https;
    //            } else {
    //                connection = (HttpURLConnection)url.openConnection();
    //            }
    //            String SOAPAction = getInstanceUrl();
    //            connection.setDoOutput(true);
    //
    //            connection.setRequestMethod("GET");
    //            connection.setRequestProperty("Content-Type", "application/json;");
    //            connection.setRequestProperty("Accept", "application/json");
    //            connection.setConnectTimeout(6000000);
    //            connection.setRequestProperty("SOAPAction", SOAPAction);
    //            connection.setRequestProperty("charset", "utf-8");
    //            connection.setRequestProperty("Authorization",
    //                                          "Basic " + getAuth());
    //
    //            BufferedReader in =
    //                new BufferedReader(new InputStreamReader(connection.getInputStream()));
    //            StringBuffer response = new StringBuffer();
    //            String inputLine;
    //            while ((inputLine = in.readLine()) != null) {
    //                response.append(inputLine);
    //            }
    //            in.close();
    //
    //            JSONObject obj = new JSONObject(response.toString());
    //
    //            ProjectBudgetVersionDetails versionDetails =
    //                new ProjectBudgetVersionDetails();
    //            versionDetails.setTaskNumber(obj.getString("ProjectNumber"));
    //            versionDetails.setTaskName(obj.getString("ProjectName"));
    //            versionDetails.setRevenue(obj.getInt("PCRevenueAmounts"));
    //
    //            JSONArray childrenLinks = obj.getJSONArray("links");
    //
    //            for (int i = 0; i < childrenLinks.length(); i++) {
    //                JSONObject proObj = childrenLinks.getJSONObject(i);
    //                if (proObj.getString("rel").equals("child") &&
    //                    proObj.getString("name").equals("PlanningResources"))
    //                    versionDetails.setChildren(getProjectBudgetVersionAllLines(proObj.getString("href")));
    //            }
    //
    //            projectBudgetLines.add(versionDetails);
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //
    //        if (finalresponse.length() > 1) {
    //        } else {
    //            finalresponse = jsonResponse;
    //        }
    //
    //        return projectBudgetLines;
    //    }


    //    public ArrayList<BudgetLine> getProjectBudgetVersionAllLines(String childrenLink) throws UnsupportedEncodingException {
    //        ArrayList<BudgetLine> projectBudgetLines = new ArrayList();
    //        ArrayList<BudgetLineDetails> lineDetails = new ArrayList();
    //        String finalresponse = "";
    //
    //        HttpsURLConnection https = null;
    //        HttpURLConnection connection = null;
    //        String serverUrl = childrenLink;
    //        String jsonResponse = "";
    //        try {
    //            URL url = new URL(serverUrl);
    //            if (url.getProtocol().toLowerCase().equals("https")) {
    //                trustAllHosts();
    //                https = (HttpsURLConnection)url.openConnection();
    //                https.setHostnameVerifier(DO_NOT_VERIFY);
    //                connection = https;
    //            } else {
    //                connection = (HttpURLConnection)url.openConnection();
    //            }
    //            String SOAPAction = getInstanceUrl();
    //            connection.setDoOutput(true);
    //
    //            connection.setRequestMethod("GET");
    //            connection.setRequestProperty("Content-Type", "application/json;");
    //            connection.setRequestProperty("Accept", "application/json");
    //            connection.setConnectTimeout(6000000);
    //            connection.setRequestProperty("SOAPAction", SOAPAction);
    //            connection.setRequestProperty("charset", "utf-8");
    //            connection.setRequestProperty("Authorization",
    //                                          "Basic " + getAuth());
    //
    //            BufferedReader in =
    //                new BufferedReader(new InputStreamReader(connection.getInputStream()));
    //            StringBuffer response = new StringBuffer();
    //            String inputLine;
    //            while ((inputLine = in.readLine()) != null) {
    //                response.append(inputLine);
    //            }
    //            in.close();
    //
    //            JSONObject obj = new JSONObject(response.toString());
    //            JSONArray children = obj.getJSONArray("items");
    //
    //            for (int i = 0; i < children.length(); i++) {
    //                JSONObject proObj = children.getJSONObject(i);
    //                BudgetLine line = new BudgetLine();
    //                line.setTaskNumber(proObj.getString("TaskNumber"));
    //                line.setTaskName(proObj.getString("TaskName"));
    //                BudgetLineDetails budgetLineDetails = new BudgetLineDetails();
    //                budgetLineDetails.setTaskNumber(proObj.getString("ResourceName"));
    //                budgetLineDetails.setUnitOfMeasure(proObj.getString("UnitOfMeasure"));
    //                budgetLineDetails.setStartDate(proObj.getString("PlanningStartDate"));
    //                budgetLineDetails.setFinishDate(proObj.getString("PlanningEndDate"));
    //                JSONArray arrOfLinks = proObj.getJSONArray("links");
    //                for (int j = 0; j < arrOfLinks.length(); j++) {
    //                    JSONObject jsonOb = arrOfLinks.getJSONObject(j);
    //                    if (jsonOb.getString("rel").equals("child") &&
    //                        jsonOb.getString("name").equals("PlanningAmounts")) {
    //                        budgetLineDetails =
    //                                getAmountDetails(budgetLineDetails, jsonOb.getString("href"));
    //                    }
    //                }
    //
    //                lineDetails.add(budgetLineDetails);
    //            }
    //
    //            JSONObject proObj = children.getJSONObject(0);
    //            BudgetLine line = new BudgetLine();
    //            line.setTaskNumber(proObj.getString("TaskNumber"));
    //            line.setTaskName(proObj.getString("TaskName"));
    //            line.setChildren(lineDetails);
    //            projectBudgetLines.add(line);
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //
    //        if (finalresponse.length() > 1) {
    //        } else {
    //            finalresponse = jsonResponse;
    //        }
    //
    //        return projectBudgetLines;
    //    }

    //    private BudgetLineDetails getAmountDetails(BudgetLineDetails budgetLineDetails,
    //                                               String link) throws MalformedURLException,
    //                                                                   IOException {
    //
    //        String finalresponse = "";
    //
    //        HttpsURLConnection https = null;
    //        HttpURLConnection connection = null;
    //        String jsonResponse = "";
    //        try {
    //            URL url = new URL(link);
    //            if (url.getProtocol().toLowerCase().equals("https")) {
    //                trustAllHosts();
    //                https = (HttpsURLConnection)url.openConnection();
    //                https.setHostnameVerifier(DO_NOT_VERIFY);
    //                connection = https;
    //            } else {
    //                connection = (HttpURLConnection)url.openConnection();
    //            }
    //            String SOAPAction = getInstanceUrl();
    //            connection.setDoOutput(true);
    //
    //            connection.setRequestMethod("GET");
    //            connection.setRequestProperty("Content-Type", "application/json;");
    //            connection.setRequestProperty("Accept", "application/json");
    //            connection.setConnectTimeout(6000000);
    //            connection.setRequestProperty("SOAPAction", SOAPAction);
    //            connection.setRequestProperty("charset", "utf-8");
    //            connection.setRequestProperty("Authorization",
    //                                          "Basic " + getAuth());
    //
    //            BufferedReader in =
    //                new BufferedReader(new InputStreamReader(connection.getInputStream()));
    //            StringBuffer response = new StringBuffer();
    //            String inputLine;
    //            while ((inputLine = in.readLine()) != null) {
    //                response.append(inputLine);
    //            }
    //            in.close();
    //
    //            JSONObject obj = new JSONObject(response.toString());
    //            JSONArray arr = obj.getJSONArray("items");
    //            if (arr.getJSONObject(0).get("Quantity") != JSONObject.NULL)
    //                budgetLineDetails.setQuantity(arr.getJSONObject(0).getNumber("Quantity"));
    //            budgetLineDetails.setRevenue(arr.getJSONObject(0).getInt("RevenueAmounts"));
    //            budgetLineDetails.setRevenueRate(arr.getJSONObject(0).getInt("EffectiveRevenueRate"));
    //
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //
    //        if (finalresponse.length() > 1) {
    //        } else {
    //            finalresponse = jsonResponse;
    //        }
    //        return budgetLineDetails;
    //    }

    public JSONObject getDataToBeExported(String projectId, String versionId) {
        JSONObject response = new JSONObject();
        response.put("status", "OK");
        
        JSONArray res = new JSONArray();
        String query =
            "SELECT * FROM " + " " + getSchema_Name() + ".XXX_BUDGET_VERSIONS where PROJECT_ID = ? AND VERSION_ID = ?";
        try {
            connection = AppsproConnection.getConnection();
            ps = connection.prepareStatement(query);
            ps.setString(1, projectId);
            ps.setString(2, versionId);
            rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();

            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                JSONObject obj = new JSONObject();
                for (int i = 1; i <= numColumns; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name, rs.getObject(column_name));
                }
                res.put(obj);
            }
            response.put("data", res);
        } catch (SQLException e) {
            response.put("status", "ERROR");
            response.put("data", e.getMessage());
            e.printStackTrace();
        }
        return response;
    }
}
