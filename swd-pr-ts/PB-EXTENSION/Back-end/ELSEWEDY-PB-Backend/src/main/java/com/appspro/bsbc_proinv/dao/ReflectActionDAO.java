/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.dao;

import biPReports.RestHelper;

import com.appspro.helper.Helper;
//import java.time.*;
import com.fasterxml.jackson.core.util.Instantiatable;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.sql.Timestamp;


import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;

/**

/**
 *
 * @author Anas Alghawi
 */
public class ReflectActionDAO extends RestHelper {

    public static void main(String[] arg) {
        //    new ReflectActionDAO().executeReports();
        //     new ReflectActionDAO().getTimestamp();


    }
    //        public static enum REPORT_NAME {
    //
    //        GET_PROFORMA_INVOICE_SHIPMENT("GET_PROFORMA_INVOICE_SHIPMENT");
    //        private String value;
    //
    //        private REPORT_NAME(String value) {
    //            this.value = value;
    //        }
    //
    //        public String getValue() {
    //            return value;
    //        }
    //
    //    }
    //                public static enum GET_PROFORMA_INVOICE_SHIPMENT_PARAM {
    //        order_number("SALES_ORDER"),
    //        po_number("CUSTOMER_PO");
    //
    //        private String value;
    //
    //        private GET_PROFORMA_INVOICE_SHIPMENT_PARAM(String value) {
    //            this.value = value;
    //        }
    //
    //        public String getValue() {
    //            return value;
    //        }
    //    }
    //
    //        public static enum ATTRIBUTE_TEMPLATE {
    //
    //
    //        ATTRIBUTE_TEMPLATE_XML("DEFAULT"),
    //        ATTRIBUTE_TEMPLATE_PDF("PDF");
    //        private String value;
    //
    //        private ATTRIBUTE_TEMPLATE(String value) {
    //            this.value = value;
    //        }
    //
    //        public String getValue() {
    //            return value;
    //        }
    //
    //    }
    //        public static enum ATTRIBUTE_FORMAT {
    //
    //
    //        ATTRIBUTE_FORMAT_XML("xml"),
    //        ATTRIBUTE_FORMAT_PDF("pdf"); //keep in small caps
    //        private String value;
    //
    //        private ATTRIBUTE_FORMAT(String value) {
    //            this.value = value;
    //        }
    //
    //        public String getValue() {
    //            return value;
    //        }
    //
    //    }


    private Map<String, String> paramMap = new HashMap<String, String>();
    private String soapRequest = null;

    //      public void setParamMap(Map<String, String> paramMap) {
    //        this.paramMap = paramMap;
    //    }

    //    public Map<String, String> getParamMap() {
    //        return paramMap;
    //    }
    //    public void setReportAbsolutePath(String reportAbsolutePath) {
    //        this.reportAbsolutePath = reportAbsolutePath;
    //    }
    //
    //    public String getReportAbsolutePath() {
    //        return reportAbsolutePath;
    //    }
    //
    //    public java.time.instant getTimestamp() {
    //
    //        LocalTime time = instant.atZone(ZoneOffset.UTC).toLocalTime();
    //        System.out.println(time);
    //        return time;
    //    }

    public String getSoapRequest() {
        //        this.soapRequest ="<soapenv:Envelope xmlns:ship=\"http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/\">\n" +
        //"   <soapenv:Header>\n" +
        //"      <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
        //"         <wsu:Timestamp wsu:Id=\"TS-0E3EACD46F9512C941154167186204411\">\n" +
        //"            <wsu:Created>"+getTimestamp()+"</wsu:Created>\n" +
        //"            <wsu:Expires>"+getTimestamp()+"</wsu:Expires>\n" +
        //"         </wsu:Timestamp>\n" +
        //"         <wsse:UsernameToken wsu:Id=\"UsernameToken-0E3EACD46F9512C941154167185866710\">\n" +
        //"            <wsse:Username>"+ Helper.username +"</wsse:Username>\n" +
        //"            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"+ Helper.password +"</wsse:Password>\n" +
        //"            <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">am9kB+4ZlqzqPHRqrKLx8w==</wsse:Nonce>\n" +
        //"            <wsu:Created>"+getTimestamp()+"</wsu:Created>\n" +
        //"         </wsse:UsernameToken>\n" +
        //"      </wsse:Security>\n" +
        //"   </soapenv:Header>\n" +
        //"   <soapenv:Body>\n" +
        //" <typ:processCreateUpdateShipment>\n" +
        //"         <typ:apiVersionNumber>1.2</typ:apiVersionNumber>\n" +
        //"         <!--Optional:-->\n" +
        //"         <typ:InitMsgList>F</typ:InitMsgList>\n" +
        //"         <typ:ActionCode>UPDATE</typ:ActionCode>\n" +
        //"         <typ:ShipmentInformation>\n" +
        //"            <ship:Shipment>40027</ship:Shipment>\n" +
        //"            <ship:Attribute1>654321</ship:Attribute1>\n" +
        //"             <ship:Attribute3>654321</ship:Attribute3>\n" +
        //"         </typ:ShipmentInformation>\n" +
        //"      </typ:processCreateUpdateShipment>"+
        //"   </soapenv:Body>\n" +
        //"</soapenv:Envelope>";
        return soapRequest;
    }

    public String executeReports() {
        String output = "";
        try {
            System.setProperty("DUseSunHttpHandler", "true");


            byte[] buffer = new byte[getSoapRequest().length()];
            buffer = getSoapRequest().getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url =
                new URL(null, "https://ehxm-dev1.fa.us6.oraclecloud.com:443/fscmService/ShipmentService",
                        new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";
            //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            System.out.println(getSoapRequest());


            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            String responseStatus = http.getResponseMessage(); //
            System.out.println("Response Message Status = " +
                               responseStatus); //
            InputStream is = http.getInputStream();


            String responseXML = getStringFromInputStream(is);
            System.out.println(responseXML);


            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(responseXML));
            System.out.println(src);

            //
            //            Document doc = builder.parse(src);
            //            String data =
            //                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            //            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
            //                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            //            } else {
            //                output = data;
            //            }
        }

        catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }


    //
    //    public void setAttributeFormat(String attributeFormat) {
    //        this.attributeFormat = attributeFormat;
    //    }
    //    public void setAttributeTemplate(String attributeTemplate) {
    //        this.attributeTemplate = attributeTemplate;
    //    }
    //
}
