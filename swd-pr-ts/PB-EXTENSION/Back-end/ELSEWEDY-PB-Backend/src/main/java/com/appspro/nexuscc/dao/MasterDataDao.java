package com.appspro.nexuscc.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.nexuscc.bean.MasterDataBean;

import com.appspro.nexuscc.bean.OverHeadBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;

public class MasterDataDao extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public ArrayList<MasterDataBean> searchbyCostType(String costType) {

        ArrayList<MasterDataBean> list = new ArrayList<MasterDataBean>();
        try {
            
            connection = AppsproConnection.getConnection();
         
           String  query = "SELECT * FROM XXX_MASTER_DATA_TABLE where COST_TYPE = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, costType);        
            rs = ps.executeQuery();
            while (rs.next()) {
            
                MasterDataBean bean = new MasterDataBean();
                bean.setId(rs.getString("ID"));
                bean.setCostId(rs.getString("COST_ID"));
                bean.setDescription(rs.getString("DESCRIPTION"));
                bean.setSystemCode(rs.getString("SYSTEM_CODE"));
                bean.setEnable(rs.getString("ENABLE"));
                bean.setCostType(rs.getString("COST_TYPE"));
                
                list.add(bean);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    
    
    public ArrayList<MasterDataBean> searchbyCostId(String costId , String ProjectId) {
    
        ArrayList<MasterDataBean> list = new ArrayList<MasterDataBean>();
        try {
            
            connection = AppsproConnection.getConnection();
         
             
           String  query = "SELECT * FROM XXX_MASTER_DATA_TABLE where COST_ID = ? and project_id =?";
            ps = connection.prepareStatement(query);
            ps.setString(1, costId);        
            ps.setString(2, ProjectId);  
            rs = ps.executeQuery();
          
            System.out.println("test "+query);

            while (rs.next()) {
            
                MasterDataBean bean = new MasterDataBean();
                bean.setId(rs.getString("ID"));
                bean.setCostId(rs.getString("COST_ID"));
                bean.setDescription(rs.getString("DESCRIPTION"));
                bean.setSystemCode(rs.getString("SYSTEM_CODE"));
                bean.setEnable(rs.getString("ENABLE"));
                bean.setCostType(rs.getString("COST_TYPE"));
                
                list.add(bean);
               
            }
System.out.println("list"+list);
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    
        public JSONObject searchbyCostdesc(String costType) {
            
            MasterDataBean bean = new MasterDataBean();

            ArrayList<MasterDataBean> list = new ArrayList<MasterDataBean>();
            try {
                
                connection = AppsproConnection.getConnection();
             
               String  query = "SELECT * FROM XXX_MASTER_DATA_TABLE where DESCRIPTION = ?";
                ps = connection.prepareStatement(query);
                ps.setString(1, costType);        
                rs = ps.executeQuery();
                while (rs.next()) {
                
                    bean.setId(rs.getString("ID"));
                    bean.setCostId(rs.getString("COST_ID"));
                    bean.setDescription(rs.getString("DESCRIPTION"));
                    bean.setSystemCode(rs.getString("SYSTEM_CODE"));
                    bean.setEnable(rs.getString("ENABLE"));
                    bean.setCostType(rs.getString("COST_TYPE"));
                }

            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                closeResources(connection, ps, rs);
            }
            return new JSONObject( bean);
        }
    public List<MasterDataBean> getallrequest(String projectId) {
        ArrayList<MasterDataBean> list = new ArrayList<MasterDataBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_MASTER_DATA_TABLE  where PROJECT_ID = ?ORDER BY ID DESC";
            ps = connection.prepareStatement(query);
             ps.setString(1,projectId );  
            rs = ps.executeQuery();

            while (rs.next()) {
                MasterDataBean bean = new MasterDataBean();
                bean.setId(rs.getString("ID"));
                bean.setCostId(rs.getString("COST_ID"));
                bean.setDescription(rs.getString("DESCRIPTION"));
                bean.setSystemCode(rs.getString("SYSTEM_CODE"));
                bean.setEnable(rs.getString("ENABLE"));
                bean.setCostType(rs.getString("COST_TYPE"));
             
                
                list.add(bean);

            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    public MasterDataBean insertCostCategory(MasterDataBean bean) {
        String query = null;

        try {
            connection = AppsproConnection.getConnection();
            query =
                    "insert  into xxx_master_data_table (COST_ID,SYSTEM_CODE,DESCRIPTION,ENABLE,COST_TYPE,PROJECT_ID)values" +
                "(?,?,?,?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getCostId());   
            ps.setString(2, bean.getSystemCode());   
            ps.setString(3, bean.getDescription());
            ps.setString(4, bean.getEnable());       
            ps.setString(5, bean.getCostType());     
            ps.setString(6, bean.getProjectId());   
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
}


