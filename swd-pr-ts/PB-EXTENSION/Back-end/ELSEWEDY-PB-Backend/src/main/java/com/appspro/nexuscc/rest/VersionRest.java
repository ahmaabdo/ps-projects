package com.appspro.nexuscc.rest;

import com.appspro.nexuscc.bean.ProjectBudgetVersion;
import com.appspro.nexuscc.dao.BudgetVersionDAO;

import com.appspro.nexuscc.dao.ProjBudgetVerDao;
import com.appspro.nexuscc.dao.SoapApiDao;

import com.appspro.nexuscc.dao.projectDAO;

import com.appspro.nexuscc.excel.ExcelHelper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.sun.jersey.core.header.MediaTypes;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.sql.SQLException;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/versions")
public class VersionRest {


    @POST
    @Path("/newVersion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createNewVersion(String excelJSON) {
        JSONObject json = new JSONObject(excelJSON);
        String planVersionIdToUpdated = null;
        
        boolean isUpdatePlan = json.has("updatePlan") && json.get("updatePlan").toString().equalsIgnoreCase("true");
        boolean isUpdateVersion = json.has("updateSelectedVersion") && json.get("updateSelectedVersion").toString().equalsIgnoreCase("true");
        planVersionIdToUpdated =json.has("planVersionId") ? json.get("planVersionId").toString() : null;

        JSONObject res = null;
        if (isUpdateVersion && planVersionIdToUpdated != null) { //update
            res =BudgetVersionDAO.getInstance().updateProjectBudget(json.get("data").toString(),planVersionIdToUpdated);
        } else { //create
            res =BudgetVersionDAO.getInstance().createProjectBudget(json.get("data").toString());
        }


        if ("error".equalsIgnoreCase(res.get("status").toString())) {
            return Response.ok(res.get("data").toString()).status(200).build();
        } else {
            String projectId = res.getJSONObject("data").get("ProjectId").toString();
            String versionId = res.getJSONObject("data").get("PlanVersionId").toString();

            if (isUpdateVersion && planVersionIdToUpdated != null) { //update
                // remove old data
                BudgetVersionDAO.getInstance().delete(versionId);
            }

            BudgetVersionDAO.getInstance().createDataFromExcelSheet(projectId, versionId,  json.get("payload").toString());


            //check if need to update plan
            if (isUpdatePlan) {
                //update financial plan
                JSONObject savedData = res.getJSONObject("data");
                JSONObject updateFinancePlanRes = BudgetVersionDAO.getInstance().updateFinancePlan(savedData);
                System.out.println("updateFinancePlanRes:" + updateFinancePlanRes);

            }

            return Response.ok("").status(200).build();
        }

    }


    @POST
    @Path("/updateVersion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateVersion(String json) {
        String res = "error";
        try {
            res = BudgetVersionDAO.getInstance().updateProjectBudget(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok(res).status(200).build();
    }


    @DELETE
    @Path("/deleteVersion/{PlanVersionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteVersion(@PathParam("PlanVersionId")
        String planVersionId) {

        JSONObject res =
            BudgetVersionDAO.getInstance().deleteVersion(planVersionId);

        if ("error".equalsIgnoreCase(res.get("status").toString()))
            return Response.ok(res.get("data").toString()).status(200).build();
        else {
            BudgetVersionDAO.getInstance().delete(planVersionId);
            return Response.ok("").status(200).build();
        }
    }

    @GET
    @Path("{pid}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getProjectBudgetVersion(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @PathParam("pid")
        String projectId) throws JsonProcessingException,
                                 UnsupportedEncodingException {
        try {

            ArrayList<ProjectBudgetVersion> allVersions =
                ProjBudgetVerDao.getInstance().getProjectBudgetVersions(projectId);
            return new JSONArray(allVersions).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    
    @GET
    @Path("lastRecord/{pid}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getLastProjectBudgetRecord(@PathParam("pid")
        String projectId){
        try {

            ArrayList<ProjectBudgetVersion> allVersions = 
                BudgetVersionDAO.getInstance().getLastUpdatedVer(projectId);
            return new JSONArray(allVersions).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GET
    @Path("export/{projectId}/{planVersionId}")
    //    @Produces(MediaType.APPLICATION_JSON)
    @Produces("application/xls")
    public Response doDownload(@PathParam("projectId")
        String pid, @PathParam("planVersionId")
        String pvid) {
        JSONObject data;
        try {
            data = projectDAO.getInstance().getDataToBeExported(pid, pvid);
            String b64 = ExcelHelper.getInstance().exportVersionData(data);
            //            Response.ResponseBuilder response =
            //                Response.ok(b64); //fbytes, "application/xls"
            data = new JSONObject();
            data.put("status", "OK");
            data.put("data", b64);
            return Response.ok(data.toString()).build();
            //            return response.build();
        } catch (Exception e) {
            data = new JSONObject();
            data.put("status", "ERROR");
            data.put("data", e.getMessage());
            return Response.ok(data.toString()).build();
        }
    }
    @GET
    @Path("uploded/{projectId}/{planVersionId}")
    //    @Produces(MediaType.APPLICATION_JSON)
    @Produces("application/xls")
    public Response uploded(@PathParam("projectId")
        String pid, @PathParam("planVersionId")
        String pvid) {
        JSONObject data;
        try {
            data = projectDAO.getInstance().getDataToBeExported(pid, pvid);
            return Response.ok(data.toString()).build();
        } catch (Exception e) {
          data = new JSONObject();
            return Response.ok(data.toString()).build();
        }
    }


    @GET
    @Path("view/{projectId}/{planVersionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response view(@PathParam("projectId")
        String pid, @PathParam("planVersionId")
        String pvid) {
        JSONObject data;
        try {
            data = projectDAO.getInstance().getDataToBeExported(pid, pvid);
            Response.ResponseBuilder response =
                Response.ok(data.toString()); //fbytes, "application/xls"
            return response.build();
        } catch (Exception e) {
            data = new JSONObject();
            data.put("status", "ERROR");
            data.put("data", e.getMessage());
            return Response.ok(data.toString()).build();
        }
    }

}
