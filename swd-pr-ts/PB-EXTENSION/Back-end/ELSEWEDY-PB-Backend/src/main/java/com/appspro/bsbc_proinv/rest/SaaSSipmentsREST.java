/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.rest;

import com.appspro.bsbc_proinv.dao.SaaSSipmentsDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Anas Alghawi
 */
@Path("/SaaSShipmentsREST")
public class SaaSSipmentsREST {
    SaaSSipmentsDAO service = new SaaSSipmentsDAO();

    @GET
    @Path("/{ShipmentNumber}")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String getSaaSShipments(@PathParam("ShipmentNumber")
        String shipmentNumber) throws Exception {
        System.out.println("shipmentNumber=" + shipmentNumber);

        return service.jsonGetRequestNonParam(shipmentNumber);

    }

    @GET
    @Path("shippingCosts/{ShipmentNumber}")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String getSaaSshippingCosts(@PathParam("ShipmentNumber")
        String shipmentNumber) throws Exception {
        System.out.println("shipmentNumber=" + shipmentNumber);
        String shipmentTotalAmount = shipmentNumber + "/child/shippingCosts";

        return service.jsonGetRequestNonParam(shipmentTotalAmount);

    }

}
