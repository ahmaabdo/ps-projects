package com.appspro.nexuscc.bean;

public class PRBean {

    private int id;
    private String bomLineNumber;
    private String lineDescription;
    private String itemCode;
    private String previousOrdered;
    private String currentQuantity;
    private String totalQuantity;
    private String uom;
    private String itemCost;
    private String needByDate;
    private String projectNumber;
    private String taskNumber;
    private String expenditureOrganiz;
    private String expenditureType;
    private String expenditureDate;


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setBomLineNumber(String bomLineNumber) {
        this.bomLineNumber = bomLineNumber;
    }

    public String getBomLineNumber() {
        return bomLineNumber;
    }

    public void setLineDescription(String lineDescription) {
        this.lineDescription = lineDescription;
    }

    public String getLineDescription() {
        return lineDescription;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setPreviousOrdered(String previousOrdered) {
        this.previousOrdered = previousOrdered;
    }

    public String getPreviousOrdered() {
        return previousOrdered;
    }

    public void setCurrentQuantity(String currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    public String getCurrentQuantity() {
        return currentQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getTotalQuantity() {
        return totalQuantity;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setItemCost(String itemCost) {
        this.itemCost = itemCost;
    }

    public String getItemCost() {
        return itemCost;
    }

    public void setNeedByDate(String needByDate) {
        this.needByDate = needByDate;
    }

    public String getNeedByDate() {
        return needByDate;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setExpenditureOrganiz(String expenditureOrganiz) {
        this.expenditureOrganiz = expenditureOrganiz;
    }

    public String getExpenditureOrganiz() {
        return expenditureOrganiz;
    }

    public void setExpenditureType(String expenditureType) {
        this.expenditureType = expenditureType;
    }

    public String getExpenditureType() {
        return expenditureType;
    }

    public void setExpenditureDate(String expenditureDate) {
        this.expenditureDate = expenditureDate;
    }

    public String getExpenditureDate() {
        return expenditureDate;
    }
}
