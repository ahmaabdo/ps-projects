package com.appspro.bsbc_proinv.rest;

import com.appspro.helper.Helper;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/ProductRegisDetailsRest")
public class ProductRegistrationTabsDetailsRest {


    @POST
    @Path("/addProductRegisDetails")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String postProductRegisDetailse(String bean) throws Exception {

           //String returnValue1 =   serviceProductRegistrationDAO.sendPost(bean);
        //     return bean ;
        String url = Helper.paasURL + "ProdRegisTabsDetails/";
        String returnValue = Helper.callPostRest(url, bean);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;

    }
    
    
    
    @GET
    @Path("getProductRegisDetails/{id}")
    @Produces({"application/json"})
    public String getProductRegis(@PathParam("id")
        String id) throws Exception {
        
        String url = Helper.paasURL + "getProductRegistrationDetails/"+id;
        String returnValue = Helper.callGetRest(url);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;


       

    }
    
}
