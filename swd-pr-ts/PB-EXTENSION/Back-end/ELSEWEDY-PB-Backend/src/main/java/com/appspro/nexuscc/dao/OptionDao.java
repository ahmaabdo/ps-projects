package com.appspro.nexuscc.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.nexuscc.bean.MasterDataBean;
import com.appspro.nexuscc.bean.OptionBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class OptionDao extends AppsproConnection{
    Connection connection;
    PreparedStatement pre;
    CallableStatement cs;
    ResultSet rs;
    public OptionBean addOption (OptionBean bean){
            String query = "";
            try {
                connection = AppsproConnection.getConnection();
                query ="INSERT  INTO XXX_OPTION_TABLE(OPTION_DEC,AMOUT,PROJECT_VERSION) VALUES(?,?,?)";
                pre = connection.prepareStatement(query);
                pre.setString(1, bean.getOptions());
                pre.setString(2, bean.getAmount());
                pre.setString(3, bean.getProjectVersion());
                pre.executeUpdate();
            }catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(connection, pre, rs);
            }
        return bean;
        }
    public List<OptionBean> getallrequest(String PROJECT_VERSION) {
        ArrayList<OptionBean> list = new ArrayList<OptionBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_OPTION_TABLE  where PROJECT_VERSION = ?";
            pre = connection.prepareStatement(query);
             pre.setString(1,PROJECT_VERSION );  
            rs = pre.executeQuery();

            while (rs.next()) {
                OptionBean bean = new OptionBean();
                bean.setId(rs.getString("ID"));
                bean.setOptions(rs.getString("OPTION_DEC"));
                bean.setAmount(rs.getString("AMOUT"));         
                list.add(bean);

            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, pre, rs);
        }
        return list;
    }
    public String removeElementFromDb (String id ){
            connection = AppsproConnection.getConnection();
            
        try {
            String query = "DELETE FROM XXX_OPTION_TABLE WHERE ID =?";
            pre = connection.prepareStatement(query);
          
            pre.setString(1, id);
             pre.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
                closeResources(connection, pre, rs);
            }
            return "";
        }
}
