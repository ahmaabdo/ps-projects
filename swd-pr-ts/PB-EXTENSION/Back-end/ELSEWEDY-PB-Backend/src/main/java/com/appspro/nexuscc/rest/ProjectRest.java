package com.appspro.nexuscc.rest;

import biPReports.BIReportModel;

import biPReports.RestHelper;

import com.appspro.nexuscc.bean.ProjectBean;
import com.appspro.nexuscc.bean.ProjectBudgetVersion;

import com.appspro.nexuscc.dao.projectDAO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/projects")

public class ProjectRest {
   
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjects(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        ArrayList<ProjectBean> projectList = projectDAO.getInstance().getProDetails();
        String jsonInString = mapper.writeValueAsString(projectList);
        return Response.status(Status.OK).entity(jsonInString).build();
    }
    
    @GET
    @Path("callSaas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjects( @QueryParam("link") String link) throws JsonProcessingException, UnsupportedEncodingException {
       JSONObject res =  RestHelper.getInstance().callRest(link, "GET", null);
        return Response.ok(res.getJSONObject("data").toString()).build();
    }
    
    //
    @GET
    @Path("getPlanTypes")
//    @Produces("application/xls")
    public Response getTypes(){
        JSONObject reportplanjson =  BIReportModel.getInstance().runReport("ppm_plan_types_reports", new HashMap());
        JSONArray arr = reportplanjson.getJSONObject("DATA_DS").getJSONArray("G_1");
        return Response.ok(arr.toString()).build();
    }
}
