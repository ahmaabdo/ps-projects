/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.dao;

import com.appspro.helper.Helper;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;

import org.apache.http.HttpResponse;

import java.net.URL;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import static org.apache.http.HttpHeaders.USER_AGENT;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class AcknowledgeDAO {

    private static String cloudURL = Helper.paasURL;

    private static String streamToString(InputStream inputStream) {
        String text =
            new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

    public String sendPost(String bean) {

        try {


            String url = cloudURL + "PI";
            //            String url = "https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/PInv/PI";
            System.out.println(bean);

            //            JSONArray jsonArray = new JSONArray('['+bean+']');
            JSONObject jsonObj = new JSONObject(bean);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            System.out.println(jsonObj);
            // add header
            post.setHeader("User-Agent", USER_AGENT);
            //            System.out.println(jsonArray.getString("attachment"));
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

            //            for (int i = 0; i < jsonArray.length(); i++) {
            //                JSONObject jsonObj = jsonArray.getJSONObject(i);
            urlParameters.add(new BasicNameValuePair("pINumber",
                                                     jsonObj.getString("pINumber")));
            urlParameters.add(new BasicNameValuePair("pITotalAmount",
                                                     jsonObj.getString("pITotalAmount")));
            urlParameters.add(new BasicNameValuePair("Curancey",
                                                     jsonObj.getString("Curancey")));
            urlParameters.add(new BasicNameValuePair("attachment",
                                                     jsonObj.getString("attachment")));
            urlParameters.add(new BasicNameValuePair("poNumber",
                                                     jsonObj.getString("poNumber")));
            urlParameters.add(new BasicNameValuePair("saleOrder",
                                                     jsonObj.getString("saleOrder")));
            urlParameters.add(new BasicNameValuePair("tender",
                                                     jsonObj.getString("tender")));
            urlParameters.add(new BasicNameValuePair("referenceNumber",
                                                     jsonObj.getString("referenceNumber")));


            post.setEntity(new UrlEncodedFormEntity(urlParameters));

            HttpResponse response = client.execute(post);
            System.out.println("\nSending 'POST' request to URL : " +
                               urlParameters);
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + post.getEntity());
            System.out.println("Response Code : " +
                               response.getStatusLine().getStatusCode());

            BufferedReader rd =
                new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());
        } catch (Exception e) {
            System.out.println("Error: ");
            e.printStackTrace();
        }
        return "";
    }

    public String sendPut(String bean) {

        try {
            String url = cloudURL + "updatePI";
            //            String url = "https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/updatePI";
            System.out.println(bean);

            JSONObject jsonObj = new JSONObject(bean);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            System.out.println(jsonObj);
            post.setHeader("User-Agent", USER_AGENT);

            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

            urlParameters.add(new BasicNameValuePair("process",
                                                     jsonObj.getString("process")));
            urlParameters.add(new BasicNameValuePair("piNumber",
                                                     jsonObj.getString("piNumber")));
            urlParameters.add(new BasicNameValuePair("attachment",
                                                     jsonObj.getString("attachment")));
            urlParameters.add(new BasicNameValuePair("attachment",
                                                     jsonObj.getString("attachment")));
            urlParameters.add(new BasicNameValuePair("attachment",
                                                     jsonObj.getString("attachment")));

            post.setEntity(new UrlEncodedFormEntity(urlParameters));

            HttpResponse response = client.execute(post);
            System.out.println("\nSending 'Put' request to -URL : " +
                               urlParameters);
            System.out.println("\nSending 'Put' request to -URL : " + url);
            System.out.println("Put parameters : " + post.getEntity());
            System.out.println("Response Code : " +
                               response.getStatusLine().getStatusCode());

            BufferedReader rd =
                new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());
        } catch (Exception e) {
            System.out.println("Error: ");
            e.printStackTrace();
        }
        return "";
    }

    public static String jsonGetRequestNonParam() {
        String json = null;
        try {
            URL url = new URL(cloudURL + "PI");
            //            URL url = new URL(null, cloudURL+"PI", new sun.net.www.protocol.https.Handler());
            //            URL url = new URL("https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/PInv/PI");
            HttpURLConnection connection =
                (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();
            InputStream inStream = connection.getInputStream();
            json = streamToString(inStream); // input stream to string
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println(json);
        return json;
    }

    public static String jsonGetRequestParam(String pINumber) {
        String json = null;
        try {
            URL url = new URL(cloudURL + "PI/" + pINumber);
            //            URL url = new URL(null, cloudURL+"PI/"+pINumber, new sun.net.www.protocol.https.Handler());
            //            URL url = new URL("https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/PInv/PI/"+pINumber);
            HttpURLConnection connection =
                (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();
            InputStream inStream = connection.getInputStream();
            json = streamToString(inStream); // input stream to string
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println(json);
        return json;
    }

    public static String jsonGetRequestSaleOrderParam(String saleOrder) {
        String json = null;
        try {
            //            URL url = new URL(cloudURL+"saleorderack/"+saleOrder);
            URL url =
                new URL(null, cloudURL + "saleorderack/" + saleOrder, new sun.net.www.protocol.https.Handler());
            //            URL url = new URL("https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/PInv/saleorderack/"+saleOrder);
            HttpURLConnection connection =
                (HttpURLConnection)url.openConnection();


            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();
            InputStream inStream = connection.getInputStream();
            json = streamToString(inStream); // input stream to string
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println(json);
        return json;
    }

    public static String jsonGetRequestTenderParam(String tender) {
        String json = null;
        try {
            //            URL url = new URL(Helper.paasURL + "tenderack/"+tender);
            URL url =
                new URL(null, Helper.paasURL + "tenderack/" + tender, new sun.net.www.protocol.https.Handler());
            //            URL url = new URL("https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/PInv/ponumberack/"+poNumber);
            HttpURLConnection connection =
                (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();
            InputStream inStream = connection.getInputStream();
            json = streamToString(inStream); // input stream to string

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println(json);
        return json;
    }

    public static String jsonGetRequestPOParam(String poNumber) {
        String json = null;
        try {
            //            URL url = new URL(cloudURL+"ponumberack/"+poNumber);
            URL url =
                new URL(null, cloudURL + "ponumberack/" + poNumber, new sun.net.www.protocol.https.Handler());
            //            URL url = new URL("https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/PInv/ponumberack/"+poNumber);
            HttpURLConnection connection =
                (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();
            InputStream inStream = connection.getInputStream();
            json = streamToString(inStream); // input stream to string
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println(json);
        return json;
    }

}
