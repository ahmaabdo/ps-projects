package com.appspro.nexuscc.rest;

import biPReports.RestHelper;

import com.appspro.nexuscc.bean.MasterDataBean;

import com.appspro.nexuscc.bean.OverHeadBean;
import com.appspro.nexuscc.dao.MasterDataDao;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("masterData")
public class MasterDataRest extends RestHelper{
    @POST
    @Path("/getByCostType")
    @Produces(MediaType.APPLICATION_JSON)
    public String findbyid(String data) {
        MasterDataDao service = new MasterDataDao();
        ArrayList<MasterDataBean> list = new ArrayList<MasterDataBean>();
        try {
            JSONObject o = new JSONObject(data);
            String costType =
                o.has("costType") ? o.getString("costType") : null;
            list = service.searchbyCostType(costType);

        } catch (Exception e) {
            System.out.println(e);
        }

        return new JSONArray(list).toString();

    }
    
    @POST
    @Path("/getByCostId")
    @Produces(MediaType.APPLICATION_JSON)
    
    public String searchByCostID(String data) {
        System.out.println(data);
        MasterDataDao service = new MasterDataDao();
        ArrayList<MasterDataBean> list = new ArrayList<MasterDataBean>();
        try {
           
                 JSONObject o = new JSONObject(data);
                String costId =
                    o.has("costId") ? o.getString("costId") : null;
            String ProjectId =
                o.has("projectId") ? o.getString("projectId") : null;
                list = service.searchbyCostId(costId, ProjectId);
            
            

        } catch (Exception e) {
            System.out.println(e);
        }

        return new JSONArray(list).toString();

    }
    
    @POST
    @Path("/costIdDetails")
    @Produces(MediaType.APPLICATION_JSON)
    
    public String searchByCostTYPE(String data) {
        MasterDataDao service = new MasterDataDao();
        JSONArray  arr2 = new JSONArray();

        try {
            JSONArray arr = new JSONArray(data);

            for(int i = 0 ; i < arr.length() ; i++) {

                JSONObject o = arr.getJSONObject(i);
                String costId = o.has("desc") ? o.get("desc").toString() : null;
                arr2.put(service.searchbyCostdesc(costId));
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }

        return arr2.toString();
    }
    @POST
    @Path("/getAllRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllRequest(String data) {
        MasterDataDao service = new MasterDataDao();
        List<MasterDataBean> list = new ArrayList<MasterDataBean>();
        JSONObject o = new JSONObject(data);
        String projectId =
           o.has("projectId") ? o.getString("projectId") : null;
        list = service.getallrequest(projectId);
        return new JSONArray(list).toString();

    }
    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
        MasterDataBean list = new MasterDataBean();
        MasterDataDao service = new MasterDataDao();
        try {
            ObjectMapper mapper = new ObjectMapper();
            MasterDataBean bean = mapper.readValue(body, MasterDataBean.class);
            list = service.insertCostCategory(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    
}
