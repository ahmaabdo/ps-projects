package com.appspro.nexuscc.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.nexuscc.bean.OverHeadBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class OverHeadDao extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public OverHeadBean insertToOverHead(OverHeadBean bean) {
        String query = null;

        try {
            connection = AppsproConnection.getConnection();
            query =
                    "insert into xxx_overhead_cost (COST_ID,DETALIS,RULES,RATE,AMOUNT,VALUE,project_id,version_id,COST_DATE,DESCRIPTION,BOND_RATE,DURATION_OF_BOND,BOND_START,BOND_END,PLANNING_ELEMENT_ID)" +
                    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getCostId());
            ps.setString(2, bean.getDetalis());
            ps.setString(3, bean.getRules());
            ps.setString(4, bean.getRate());
            ps.setString(5, bean.getAmount());
            ps.setString(6, bean.getValue());
            ps.setString(7, bean.getProjectId());
            ps.setString(8, bean.getVersionId());
            ps.setString(9, bean.getCostDate());
            ps.setString(10, bean.getDescription());
            ps.setString(11, bean.getBondRate());
            ps.setString(12, bean.getBondDuration());
            ps.setString(13, bean.getBondStart());
            ps.setString(14, bean.getBondEnd());
            ps.setString(15, "");
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    public ArrayList<OverHeadBean> getallrequest(String projectId , String versionId) {
        ArrayList<OverHeadBean> list = new ArrayList<OverHeadBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM xxx_overhead_cost  where PROJECT_ID=? AND VERSION_ID =? ORDER BY ID DESC";
            
            ps = connection.prepareStatement(query);
            ps.setString(1, projectId);
            ps.setString(2, versionId);  
            rs = ps.executeQuery();

            while (rs.next()) {
                OverHeadBean bean = new OverHeadBean();  
                bean.setId(rs.getString("ID"));
                bean.setCostId(rs.getString("COST_ID"));
                bean.setDetalis(rs.getString("DETALIS"));
                bean.setRules(rs.getString("RULES"));
                bean.setRate(rs.getString("RATE"));
                bean.setAmount(rs.getString("AMOUNT"));
                bean.setValue(rs.getString("VALUE"));
                bean.setBondRate(rs.getString("BOND_RATE"));
                bean.setProjectId(rs.getString("PROJECT_ID"));
                bean.setVersionId(rs.getString("VERSION_ID"));
                bean.setCostDate(rs.getString("COST_DATE"));
                bean.setDescription(rs.getString("DESCRIPTION"));
                bean.setBondRate(rs.getString("BOND_RATE"));
                bean.setBondDuration(rs.getString("DURATION_OF_BOND"));
                bean.setBondStart(rs.getString("BOND_START"));
                bean.setBondEnd(rs.getString("BOND_END"));
                bean.setPlanningElementId(rs.getString("PLANNING_ELEMENT_ID"));
                bean.setStatus(rs.getString("STATUS"));

                list.add(bean);

            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    public JSONObject removePlanningElement (String Body){
        JSONObject json = new JSONObject(Body);
            JSONObject returnData = new JSONObject(Body);
        System.out.println(json);
        String planVersionId= json.getString("plannigVersionId");
        String planElemntId =json.getString("planningElementId");
        
            String serverUrl = rh.getInstanceUrl() + rh.getDeleteplanningResoursURL()+planVersionId+"/child/PlanningResources/"+planElemntId;
        System.out.println(serverUrl);
     
             returnData = RestHelper.getInstance().callRest(serverUrl, "DELETE", "");
            return returnData;
        
        }
    public String removeElementFromDb (String id ){
            connection = AppsproConnection.getConnection();
            
        try {
            String query = "DELETE FROM XXX_OVERHEAD_COST WHERE ID =?";
            ps = connection.prepareStatement(query);
          
            ps.setString(1, id);
             ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
                closeResources(connection, ps, rs);
            }
            return "";
        }
    public String updateStatus (String id ){
            connection = AppsproConnection.getConnection();
           
        try {
            String query = "update xxx_overhead_cost set status ='uploaded' where id = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
                closeResources(connection, ps, rs);
            }
            return "";
    }
}
