
package com.appspro.nexuscc.bean;

import java.util.List;

public class BudgetLineDetails {

    private String taskNumber;
    private String taskName;
    private Number quantity;
    private String resourceClass;
    private String unitOfMeasure;
    private Integer revenue;
    private String exceptions;
    private Integer revenueRate;
    private String startDate;
    private String finishDate;
    private List<BudgetLineAmountsDetails> children = null;

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Number getQuantity() {
        return quantity;
    }

    public void setQuantity(Number quantity) {
        this.quantity = quantity;
    }

    public String getResourceClass() {
        return resourceClass;
    }

    public void setResourceClass(String resourceClass) {
        this.resourceClass = resourceClass;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public Integer getRevenue() {
        return revenue;
    }

    public void setRevenue(Integer revenue) {
        this.revenue = revenue;
    }

    public String getExceptions() {
        return exceptions;
    }

    public void setExceptions(String exceptions) {
        this.exceptions = exceptions;
    }

    public Integer getRevenueRate() {
        return revenueRate;
    }

    public void setRevenueRate(Integer revenueRate) {
        this.revenueRate = revenueRate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public List<BudgetLineAmountsDetails> getChildren() {
        return children;
    }

    public void setChildren(List<BudgetLineAmountsDetails> children) {
        this.children = children;
    }

}
