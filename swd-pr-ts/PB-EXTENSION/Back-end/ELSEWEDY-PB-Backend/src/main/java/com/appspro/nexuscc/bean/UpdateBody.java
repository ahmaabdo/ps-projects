package com.appspro.nexuscc.bean;

public class UpdateBody {
    String projectName;
    String planVersionId;
    String planningElementId;
    String planLineId;
    String effectiveRawCostRate;
    String effectiveRevenueRate;
    String quantity;


    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setPlanVersionId(String planVersionId) {
        this.planVersionId = planVersionId;
    }

    public String getPlanVersionId() {
        return planVersionId;
    }

    public void setPlanningElementId(String planningElementId) {
        this.planningElementId = planningElementId;
    }

    public String getPlanningElementId() {
        return planningElementId;
    }

    public void setPlanLineId(String planLineId) {
        this.planLineId = planLineId;
    }

    public String getPlanLineId() {
        return planLineId;
    }

    public void setEffectiveRawCostRate(String effectiveRawCostRate) {
        this.effectiveRawCostRate = effectiveRawCostRate;
    }

    public String getEffectiveRawCostRate() {
        return effectiveRawCostRate;
    }

    public void setEffectiveRevenueRate(String effectiveRevenueRate) {
        this.effectiveRevenueRate = effectiveRevenueRate;
    }

    public String getEffectiveRevenueRate() {
        return effectiveRevenueRate;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }
}
