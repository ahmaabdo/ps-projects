package com.appspro.nexuscc.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.nexuscc.bean.MainProject;
import com.appspro.nexuscc.bean.ProjectBean;

import com.appspro.nexuscc.bean.ProjectBudgetVersion;
import com.appspro.nexuscc.bean.ServicesBean;
import com.appspro.nexuscc.bean.SubProject;

import com.appspro.nexuscc.bean.UpdateBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;


public class BudgetVersionDAO extends RestHelper {
    Connection dbConnection = null;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    private static BudgetVersionDAO instance;


    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public JSONObject createProjectBudget(String body) {
        String serverUrl = getInstanceUrl() + getProjectBudgetLinesURL();
        return RestHelper.getInstance().callRest(serverUrl, "POST", body);
    }

    public ArrayList<ProjectBudgetVersion> getLastUpdatedVer(String projectId) {
        ArrayList<ProjectBudgetVersion> list =
            new ArrayList<ProjectBudgetVersion>();
        try {
            String query =
                "SELECT VERSION_ID FROM xxx_budget_versions WHERE project_id = ? AND rownum = 1 ORDER BY ID DESC";
            dbConnection = AppsproConnection.getConnection();
            ps = dbConnection.prepareStatement(query);
            ps.setString(1, projectId);
            rs = ps.executeQuery();
            while (rs.next()) {
                ProjectBudgetVersion bean = new ProjectBudgetVersion();
                bean.setPlanVersionId(rs.getLong("VERSION_ID"));
                list.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(dbConnection, ps, rs);
        }
        return list;
    }
    
    public String updateProjectBudget(String body) {
        UpdateBody inputBody;
        try {
            inputBody = new ObjectMapper().readValue(body, UpdateBody.class);
            JSONObject jsonOfSaas = new JSONObject();
            jsonOfSaas.put("Quantity", inputBody.getQuantity());
            jsonOfSaas.put("EffectiveRevenueRate",
                           inputBody.getEffectiveRevenueRate());
            jsonOfSaas.put("EffectiveRawCostRate",
                           inputBody.getEffectiveRawCostRate());

            String serverUrl =
                getInstanceUrl() + getProjectBudgetLinesURL() + inputBody.getPlanVersionId() +
                getResourcesPath() + inputBody.getPlanningElementId() +
                getAmountsPath() + inputBody.getPlanLineId();

            JSONObject res =
                RestHelper.getInstance().callRest(serverUrl, "PATCH",
                                                  jsonOfSaas.toString());

            String result =
                res.get("status").toString().equalsIgnoreCase("error") ?
                res.get("data").toString() : "";
            if (result.length() == 0) {

                update(inputBody.getPlanVersionId(), inputBody.getQuantity(),
                       inputBody.getEffectiveRevenueRate(),
                       inputBody.getEffectiveRawCostRate());
            }
            return result;
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Error";
    }


    public JSONObject deleteVersion(String planVersionId) {

        String serverUrl =
            getInstanceUrl() + getProjectBudgetLinesURL() + planVersionId;
        return RestHelper.getInstance().callRest(serverUrl, "DELETE", null);

    }

    public String createDataFromExcelSheet(String projectNumber,
                                           String versionNumber, String body) {
        try {
            String query = null;
            JSONArray jsonArray = new JSONArray(body);
            dbConnection = AppsproConnection.getConnection();
            query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_BUDGET_VERSIONS (POS,REFERENCE,COST_ID, RESOURCE_NAME,PART_NO,SUPPLIER,MANUFACTURER,DESCRIPTION,UNIT_OF_MEASURE,TOTAL_QTY,FOREIGN_CURRENCY,UNIT_COST_AED_DUBAI,STANDARD_DISCOUNT,SPECIAL_DISCOUNT,GP_EQUIPMENT,FIXED_OVERHEAD,VARIABLE_OVERHEAD,UNIT_COST_AED,SUMMARY_TOTAL_PRICE_AED,TOTAL_COST_AED,BUDGET_CURRENCY_AED,CONVERSION_RATE,FINISH_DATE,LAST_BUDGET_PRICE,LAST_BUDGET_PRICE_CURRENCY,LAST_STANDARD_DISCOUNT,START_DATE,TOTAL_COST_AED_DISCOUNTED,UNIT_COST_AED_DISCOUNTED,UNIT_PRICE,PROJECT_ID,VERSION_ID,MAKE,CURRENCY,TOTAL_PRICE_AED,UNIT_PRICE_AED,TOTAL_COST_AED_DUBAI)\n" +
                    "                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = dbConnection.prepareStatement(query);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = jsonArray.getJSONObject(i);
                ps.setString(1, item.get("Pos").toString());
                ps.setString(2, item.get("refrence").toString());
                ps.setString(3, item.get("costId").toString());
                ps.setString(4, item.get("resourceName").toString());
                ps.setString(5, item.get("partNo").toString());
                ps.setString(6, item.get("supplier").toString());
                ps.setString(7, item.get("manufacturer").toString());
                ps.setString(8, item.get("desciption").toString());
                ps.setString(9, item.get("unitOfMeasure").toString());
                ps.setString(10, item.get("totalQtyHours").toString());
                ps.setString(11, item.get("foreignCurrency").toString());
//                ps.setString(12, item.get("unitPriceFc").toString());
                ps.setString(12, item.get("unitCostAedDubai").toString());
                ps.setString(13, item.get("standardDiscount").toString());
                ps.setString(14, item.get("specialDiscount").toString());
                ps.setString(15, item.get("gpEquipment").toString());
                ps.setString(16, item.get("fixedOverhead").toString());
                ps.setString(17, item.get("variableOverhead").toString());
                ps.setString(18, item.get("unitCostAed").toString());
                ps.setString(19, item.get("summaryTotalPriceAed").toString());
                ps.setString(20, item.get("totalCostAed").toString());
                ps.setString(21, item.get("BudgetCurrencyAed").toString());
                ps.setString(22, item.get("conversionRate").toString());
                ps.setString(23, item.get("finishDate").toString());
                ps.setString(24, item.get("lastBudgetPrice").toString());
                ps.setString(25,item.get("lastBudgetPriceCurrency").toString());
                ps.setString(26, item.get("lastStandardDiscount").toString());
                ps.setString(27, item.get("startDate").toString());
                ps.setString(28,item.get("totalCostAedDiscounted").toString());
                ps.setString(29, item.get("unitCostAedDiscounted").toString());
//                ps.setString(30, item.get("totalPriceFc").toString());
                ps.setString(30, item.get("unitPriceRate").toString());
                ps.setString(31, projectNumber);
                ps.setString(32, versionNumber);
                ps.setString(33, item.get("make").toString());
                ps.setString(34, item.get("currency").toString());            
                ps.setString(35, item.get("unitPriceAED").toString());
                ps.setString(36, item.get("totalPriceAED").toString());
                ps.setString(37, item.get("totalCostAedDubai").toString());
//                ps.setString(37, item.get("totalPriceAed").toString());






                int s = ps.executeUpdate();
                System.out.println(s == 1 ? "saved" : "error");
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(dbConnection, ps, rs);
        }
        return "";
    }

    public static BudgetVersionDAO getInstance() {
        if (instance == null)
            instance = new BudgetVersionDAO();
        return instance;
    }

    public void delete(String planVersionId) {
        try {
            dbConnection = AppsproConnection.getConnection();
            ps =
 dbConnection.prepareStatement("DELETE FROM " + getSchema_Name() +
                               ".XXX_BUDGET_VERSIONS WHERE VERSION_ID = ?");
            ps.setString(1, planVersionId);
            int executeUpdate = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(dbConnection, ps, rs);
        }
    }

    private void update(String versionId, String quantity, String unitPriceFc,
                        String unitCostAedDiscounted) {
        try {
            dbConnection = AppsproConnection.getConnection();
            ps =
 dbConnection.prepareStatement("UPDATE  " + getSchema_Name() + ".XXX_BUDGET_VERSIONS SET TOTAL_QTY = ?,UNIT_PRICE_FC = ?,UNIT_COST_AED_DISCOUNTED=?  WHERE VERSION_ID = ?");
            ps.setString(1, quantity);
            ps.setString(2, unitPriceFc);
            ps.setString(3, unitCostAedDiscounted);
            ps.setString(4, versionId);
            int executeUpdate = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(dbConnection, ps, rs);
        }
    }


    public JSONObject updateFinancePlan(JSONObject addNewVersionResponseJson) {
        JSONObject res = new JSONObject();
        //        String addNewVersionResponse =
        //            "{ \"ProjectName\": \"Setup and Install\", \"SourcePlanVersionNumber\": null, \"AdministrativeCode\": null, \"SourcePlanVersionStatus\": null, \"BudgetGenerationSource\": null, \"PlanVersionDescription\": null, \"CopyAdjustmentPercentage\": null, \"PFCBurdenedCostAmounts\": null, \"PlanningAmounts\": \"COST_AND_REV_TOGETHER\", \"PFCRawCostAmounts\": null, \"links\": [ { \"kind\": \"item\", \"rel\": \"self\", \"name\": \"projectBudgets\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550\", \"properties\": { \"changeIndicator\": \"ACED0005737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A65787000000001770400000001737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000178\" } }, { \"kind\": \"item\", \"rel\": \"canonical\", \"name\": \"projectBudgets\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550\" }, { \"kind\": \"collection\", \"rel\": \"lov\", \"name\": \"ProjectLOVVO\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/lov/ProjectLOVVO\" }, { \"kind\": \"collection\", \"rel\": \"lov\", \"name\": \"BudgetGenerationSourceLOVVO\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/lov/BudgetGenerationSourceLOVVO\" }, { \"kind\": \"collection\", \"rel\": \"lov\", \"name\": \"FinancialPlanTypesLOVVO\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/lov/FinancialPlanTypesLOVVO\" }, { \"kind\": \"collection\", \"rel\": \"lov\", \"name\": \"BudgetCreationMethodLOVVO\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/lov/BudgetCreationMethodLOVVO\" }, { \"kind\": \"collection\", \"rel\": \"child\", \"name\": \"VersionErrors\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/VersionErrors\" }, { \"kind\": \"collection\", \"rel\": \"child\", \"name\": \"PlanningResources\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources\" } ], \"PCBurdenedCostAmounts\": null, \"DeferFinancialPlanCreation\": \"Y\", \"FinancialPlanType\": \"Cost and Revenue Budget\", \"PCRawCostAmounts\": null, \"PlanningResources\": [ { \"TaskId\": 300000002226385, \"UnitOfMeasure\": \"HOURS\", \"PlanningAmounts\": [ { \"EffectiveRevenueRate\": 10724.52, \"RawCostAmounts\": null, \"PlanLineId\": 300000002376552, \"Quantity\": 44, \"BurdenedCostAmounts\": null, \"StandardBurdenedCostRate\": null, \"RevenueAmounts\": null, \"EffectiveRawCostRate\": 9437.58, \"PFCBurdenedCostAmounts\": null, \"StandardRawCostRate\": null, \"Currency\": \"AED\", \"EffectiveBurdenedCostRate\": null, \"StandardRevenueRate\": null, \"PFCRawCostAmounts\": null, \"links\": [ { \"kind\": \"item\", \"rel\": \"self\", \"name\": \"PlanningAmounts\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551/child/PlanningAmounts/300000002376552\", \"properties\": { \"changeIndicator\": \"ACED0005737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A65787000000001770400000001737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000178\" } }, { \"kind\": \"item\", \"rel\": \"canonical\", \"name\": \"PlanningAmounts\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551/child/PlanningAmounts/300000002376552\" }, { \"kind\": \"item\", \"rel\": \"parent\", \"name\": \"PlanningResources\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551\" }, { \"kind\": \"collection\", \"rel\": \"child\", \"name\": \"PlanLinesDFF\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551/child/PlanningAmounts/300000002376552/child/PlanLinesDFF\" }, { \"kind\": \"collection\", \"rel\": \"child\", \"name\": \"PlanningAmountDetails\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551/child/PlanningAmounts/300000002376552/child/PlanningAmountDetails\" } ], \"PCBurdenedCostAmounts\": null, \"PCRevenueAmounts\": null, \"PCRawCostAmounts\": null, \"PFCRevenueAmounts\": null } ], \"TaskName\": \"Task 02\", \"ResourceName\": \"Sheriff, Ahmed\", \"TaskNumber\": \"01\", \"PlanningElementId\": 300000002376551, \"PlanningStartDate\": \"2019-11-10T00:00:00+00:00\", \"PlanningEndDate\": \"2019-11-10T00:00:00+00:00\", \"links\": [ { \"kind\": \"item\", \"rel\": \"self\", \"name\": \"PlanningResources\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551\", \"properties\": { \"changeIndicator\": \"ACED0005737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A65787000000001770400000001737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000178\" } }, { \"kind\": \"item\", \"rel\": \"canonical\", \"name\": \"PlanningResources\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551\" }, { \"kind\": \"item\", \"rel\": \"parent\", \"name\": \"projectBudgets\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550\" }, { \"kind\": \"collection\", \"rel\": \"lov\", \"name\": \"PrimaryPlanningRBSLOVVO\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551/lov/PrimaryPlanningRBSLOVVO\" }, { \"kind\": \"collection\", \"rel\": \"lov\", \"name\": \"FinancialTaskVersionLOVVO\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551/lov/FinancialTaskVersionLOVVO\" }, { \"kind\": \"collection\", \"rel\": \"child\", \"name\": \"PlanningAmounts\", \"href\": \"https://elhn-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projectBudgets/300000002376550/child/PlanningResources/300000002376551/child/PlanningAmounts\" } ], \"RbsElementId\": 100000001510214 } ], \"PlanVersionNumber\": 33, \"ProjectNumber\": \"PRJ 0001\", \"PlanVersionId\": 300000002376550, \"ProjectId\": 300000002187836, \"PlanVersionName\": \"vvcv\", \"SourcePlanVersionId\": null, \"PlanVersionStatus\": \"Current Working\", \"SourcePlanType\": null, \"PCRevenueAmounts\": null, \"BudgetCreationMethod\": \"MANUAL\", \"PFCRevenueAmounts\": null }";
        //        JSONObject addNewVersionResponseJson =
        //            new JSONObject(addNewVersionResponse);

        String projectId =
            addNewVersionResponseJson.get("ProjectId").toString();
        String url =
            RestHelper.getInstance().getInstanceUrl() + "/fscmRestApi/resources/11.13.18.05/financialProjectPlans?q=ProjectId=" +
            projectId +
            "&expand=ResourceAssignments,ResourceAssignments.PlanningAmounts&limit=500";
        System.out.println("url:" + url);
        String planVersionId = null;
        JSONObject o = RestHelper.getInstance().callRest(url, "GET", null);
        JSONArray items = o.getJSONObject("data").getJSONArray("items");
        JSONObject item = items.getJSONObject(0);
        JSONArray resourcesTasks;
        System.out.println("Project:" + projectId);
        planVersionId = item.get("PlanVersionId").toString();
        if (planVersionId != null) { //&& resourcesLink != null
            //-get resources for each task to get PlanningElementId for each resource
            resourcesTasks = item.getJSONArray("ResourceAssignments");            
                     //   System.out.println(resourcesTasks);
            //loop over resources that has uploaded by excel
            JSONArray PlanningResources =
                addNewVersionResponseJson.getJSONArray("PlanningResources");
            for (int x = 0; x < PlanningResources.length(); x++) {
                JSONObject PlanningResourcesItem =
                    PlanningResources.getJSONObject(x);
                System.out.println(PlanningResourcesItem);
                String TaskName =
                    PlanningResourcesItem.get("TaskName").toString();
                String ResourceName =
                    PlanningResourcesItem.get("ResourceName").toString();
                String TaskNumber =
                    PlanningResourcesItem.get("TaskNumber").toString();
                String PlanningElementId =
                    PlanningResourcesItem.get("PlanningElementId").toString();
                String Quantity =
                    PlanningResourcesItem.getJSONArray("PlanningAmounts").getJSONObject(0).get("Quantity").toString();
                String EffectiveRawCostRate =
                    PlanningResourcesItem.getJSONArray("PlanningAmounts").getJSONObject(0).get("EffectiveRawCostRate").toString();
                String PlanLineId =
                    PlanningResourcesItem.getJSONArray("PlanningAmounts").getJSONObject(0).get("PlanLineId").toString();
                //                System.out.println("----------------------");
                //                System.out.println("planVersionId:" + planVersionId);
                //                System.out.println("ResourceName:" + ResourceName);
                //                System.out.println("TaskName:" + TaskName);
                //                System.out.println("TaskNumber:" + TaskNumber);
                //get valid PlanningElementId,PlanLineId
              //  System.out.println("REEEESSSSSSOO"+resourcesTasks.toString());               
                for (int xx = 0; xx < resourcesTasks.length(); xx++) {
                    JSONObject resourcesTasksItem =
                        resourcesTasks.getJSONObject(xx);
                    if (resourcesTasksItem.get("TaskNumber").toString().equalsIgnoreCase(TaskNumber) &&
                        resourcesTasksItem.get("ResourceName").toString().equalsIgnoreCase(ResourceName)) {
                        PlanningElementId =
                                resourcesTasksItem.get("PlanningElementId").toString();
                        PlanLineId =
                                resourcesTasksItem.getJSONArray("PlanningAmounts").getJSONObject(0).get("PlanLineId").toString();         
//                                System.out.println("PlanningElementId:" + PlanningElementId);
//                                System.out.println("PlanLineId:" + PlanLineId);          
//                                System.out.println("Quantity:" + Quantity);
//                                System.out.println("EffectiveRawCostRate:" +
//                                                   EffectiveRawCostRate);
//                                System.out.println("----------------------");

                String url2 =
                    RestHelper.getInstance().getInstanceUrl() + "/fscmRestApi/resources/11.13.18.05/financialProjectPlans/" +
                    planVersionId + "/child/ResourceAssignments/" +
                    PlanningElementId + "/child/PlanningAmounts/" + PlanLineId;
                JSONObject data = new JSONObject();
                data.put("PlannedQuantity", Quantity);
                data.put("EffectiveRawCostRate", EffectiveRawCostRate);
                res =  RestHelper.getInstance().callRest(url2, "PATCH",
                                                         data.toString());
                System.out.println(res);
                
                        }
                }
            }
            return res;
        }

        res.put("status", "error");
        res.put("data", "Internal server error");
        return res;
    }

    public JSONObject updateProjectBudget(String body, String planVersionId) {
        JSONObject jsonobj = new JSONObject(body);
        String serverUrl =
            getInstanceUrl() + getProjectBudgetLinesURL() + planVersionId;
        JSONArray arr = jsonobj.getJSONArray("PlanningResources");
        JSONObject res = new JSONObject();
        for (int x = 0; x < arr.length(); x++) {
            JSONObject obj = new JSONObject();
            JSONArray newArr = new JSONArray();
            newArr.put(arr.getJSONObject(x));
            obj.put("PlanningResources", newArr);
            System.out.println(obj);
            res =
              RestHelper.getInstance().callRest(serverUrl, "PATCH", obj.toString());
        }
        return res;
    }
}
