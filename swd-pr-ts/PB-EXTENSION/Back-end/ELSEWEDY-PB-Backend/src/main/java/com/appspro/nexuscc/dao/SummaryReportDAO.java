/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.nexuscc.dao;


import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import static biPReports.RestHelper.getSchema_Name;

import com.appspro.nexuscc.bean.SummaryReportBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author lenovo
 */
public class SummaryReportDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public SummaryReportBean addSummary(SummaryReportBean summaryAdd,
                                        String transactionType) {

        try {
            connection = AppsproConnection.getConnection();
            if (transactionType.equals("ADD")) {
                String query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_Report(PARAMETER1,PARAMETER2,PARAMETER3,PARAMETER4,PARAMETER5,REPORT_NAME,reportTypeVal)\n" +
                    "                     VALUES (?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query);

                ps.setString(1, summaryAdd.getParameter1());
                ps.setString(2, summaryAdd.getParameter2());
                ps.setString(3, summaryAdd.getParameter3());
                ps.setString(4, summaryAdd.getParameter4());
                ps.setString(5, summaryAdd.getParameter5());
                ps.setString(6, summaryAdd.getReport_Name());
                ps.setString(7, summaryAdd.getReportTypeVal());
                ps.executeUpdate();
            } else if (transactionType.equals("EDIT")) {
                String query =
                    "update  " + " " + getSchema_Name() + ".XXX_Report set REPORT_NAME=?, PARAMETER1=?,PARAMETER2=?,PARAMETER3=?,PARAMETER4=?,PARAMETER5=?,reportTypeVal=?\n" +
                    "where ID=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, summaryAdd.getReport_Name());
                ps.setString(2, summaryAdd.getParameter1());
                ps.setString(3, summaryAdd.getParameter2());
                ps.setString(4, summaryAdd.getParameter3());
                ps.setString(5, summaryAdd.getParameter4());
                ps.setString(6, summaryAdd.getParameter5());
                ps.setString(7, summaryAdd.getReportTypeVal());
                ps.setString(8, summaryAdd.getId());

                ps.executeUpdate();

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return summaryAdd;
    }

    public String getReportName(String reportName) {

        String result = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select REPORT_NAME FROM  " + " " + getSchema_Name() +
                ".XXX_Report WHERE REPORT_NAME=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, reportName);

            rs = ps.executeQuery();
            while (rs.next()) {
                result = rs.getString(1);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return result;
    }

    public ArrayList<SummaryReportBean> getAllSummary() {

        ArrayList<SummaryReportBean> SummaryReportList =
            new ArrayList<SummaryReportBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + "  " + " " + getSchema_Name() + ".XXX_Report";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                SummaryReportBean summaryReport = new SummaryReportBean();
                summaryReport.setId(rs.getString("ID"));
                summaryReport.setReport_Name(rs.getString("REPORT_NAME"));
                summaryReport.setParameter1(rs.getString("PARAMETER1"));
                summaryReport.setParameter2(rs.getString("PARAMETER2"));
                summaryReport.setParameter3(rs.getString("PARAMETER3"));
                summaryReport.setParameter4(rs.getString("PARAMETER4"));
                summaryReport.setParameter5(rs.getString("PARAMETER5"));
                summaryReport.setReportTypeVal(rs.getString("REPORTTYPEVAL"));
                SummaryReportList.add(summaryReport);

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return SummaryReportList;

    }

    public ArrayList<SummaryReportBean> getReportParamSummary(String reportName) {

        ArrayList<SummaryReportBean> SummaryReportList =
            new ArrayList<SummaryReportBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_Report  WHERE REPORT_NAME=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, reportName);
            rs = ps.executeQuery();
            while (rs.next()) {
                SummaryReportBean summaryReport = new SummaryReportBean();
                summaryReport.setId(rs.getString("ID"));
                summaryReport.setReport_Name(rs.getString("REPORT_NAME"));
                summaryReport.setParameter1(rs.getString("PARAMETER1"));
                summaryReport.setParameter2(rs.getString("PARAMETER2"));
                summaryReport.setParameter3(rs.getString("PARAMETER3"));
                summaryReport.setParameter4(rs.getString("PARAMETER4"));
                summaryReport.setParameter5(rs.getString("PARAMETER5"));
                summaryReport.setReportTypeVal(rs.getString("REPORTTYPEVAL"));
                SummaryReportList.add(summaryReport);
                //(SummaryReportList);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return SummaryReportList;

    }

    public String checkReportName(String reportName, String reportNamebefor) {

        String result = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from (select REPORT_NAME from  " + " " + getSchema_Name() +
                ".XXX_Report where REPORT_NAME !=?) where REPORT_NAME=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, reportName);
            ps.setString(2, reportNamebefor);

            rs = ps.executeQuery();
            while (rs.next()) {
                result = rs.getString(1);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return result;
    }

    public ArrayList<SummaryReportBean> searchReports(String reportname) {
        ArrayList<SummaryReportBean> reportList =
            new ArrayList<SummaryReportBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT * FROM  " + " " + getSchema_Name() + ".XXX_REPORT reports WHERE reports.REPORT_NAME LIKE ?";

            ps = connection.prepareStatement(query);
            //            ps.setString(1, "%" + eitCode + "%");
            ps.setString(1, reportname);
            rs = ps.executeQuery();
            while (rs.next()) {
                SummaryReportBean ReportBean = new SummaryReportBean();

                ReportBean.setId(rs.getString("ID"));
                ReportBean.setReport_Name(rs.getString("REPORT_NAME"));
                ReportBean.setParameter1(rs.getString("PARAMETER1"));
                ReportBean.setParameter2(rs.getString("PARAMETER2"));
                ReportBean.setParameter3(rs.getString("PARAMETER3"));
                ReportBean.setParameter4(rs.getString("PARAMETER4"));
                ReportBean.setParameter5(rs.getString("PARAMETER5"));
                ReportBean.setReportTypeVal(rs.getString("REPORTTYPEVAL"));
                reportList.add(ReportBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }

        return reportList;
    }

    public Map<String, SummaryReportBean> getAllSummaryMapped() {

        Map<String, SummaryReportBean> summaryReportMap =
            new HashMap<String, SummaryReportBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + "  " + " " + getSchema_Name() + ".XXX_Report";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            SummaryReportBean summaryReport = null;

            while (rs.next()) {

                summaryReport = new SummaryReportBean();
                summaryReport.setId(rs.getString("ID"));
                summaryReport.setReport_Name(rs.getString("REPORT_NAME"));
                summaryReport.setParameter1(rs.getString("PARAMETER1"));
                summaryReport.setParameter2(rs.getString("PARAMETER2"));
                summaryReport.setParameter3(rs.getString("PARAMETER3"));
                summaryReport.setParameter4(rs.getString("PARAMETER4"));
                summaryReport.setParameter5(rs.getString("PARAMETER5"));
                summaryReport.setReportTypeVal(rs.getString("REPORTTYPEVAL"));
                summaryReportMap.put(summaryReport.getReport_Name(),
                                     summaryReport);

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return summaryReportMap;

    }
    public String getDeleteReport(String id) {

         connection = AppsproConnection.getConnection();
         String query;
         int checkResult=0;

         try {
             query = "DELETE FROM XXX_REPORT WHERE ID=?";
             ps = connection.prepareStatement(query);
             ps.setInt(1, Integer.parseInt(id));
             checkResult = ps.executeUpdate();
             
         } catch (Exception e) {
             //("Error: ");
             e.printStackTrace();
         } finally {
             closeResources(connection, ps, rs);
         }
         return Integer.toString(checkResult);
     } 
    
    
    
    
    

}
