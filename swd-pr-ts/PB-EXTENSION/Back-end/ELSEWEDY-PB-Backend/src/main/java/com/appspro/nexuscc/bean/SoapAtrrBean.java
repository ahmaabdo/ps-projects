package com.appspro.nexuscc.bean;

public class SoapAtrrBean {
  
    private String pOHeaderId;
    private int OrderNumber;
    private int DocumentStyleId;
    private String DocumentStyle;
    private String ProcurementBusinessUnit;
    private String RequisitioningBusinessUnit;
    private String BuyerName;
    private String Supplier;
    private String Currency;
    private String ChangeOrderDescription;
    private String soldToLegalEntity;
    private String orchestrationOrderFlag;
    private String srnNumDFF;
    private String sectorDFF;
    private String serviceDFF;
    private String airCraftDFF;
    private String cardNumDFF;
    private String cardHolderNameDFF;
    private String cctrnNumDFF;
    private String arrICAODFF;
    private String depICAODFF;
    private String itemDescription;
    private String price;
    private String currencyCode;
    private String shipToLocationCode;
    private String supplierSite;
    


    public void setDocumentStyle(String DocumentStyle) {
        this.DocumentStyle = DocumentStyle;
    }

    public String getDocumentStyle() {
        return DocumentStyle;
    }

    public void setProcurementBusinessUnit(String ProcurementBusinessUnit) {
        this.ProcurementBusinessUnit = ProcurementBusinessUnit;
    }

    public String getProcurementBusinessUnit() {
        return ProcurementBusinessUnit;
    }

    public void setRequisitioningBusinessUnit(String RequisitioningBusinessUnit) {
        this.RequisitioningBusinessUnit = RequisitioningBusinessUnit;
    }

    public String getRequisitioningBusinessUnit() {
        return RequisitioningBusinessUnit;
    }

    public void setBuyerName(String BuyerName) {
        this.BuyerName = BuyerName;
    }

    public String getBuyerName() {
        return BuyerName;
    }

    public void setSupplier(String Supplier) {
        this.Supplier = Supplier;
    }

    public String getSupplier() {
        return Supplier;
    }

    public void setCurrency(String Currency) {
        this.Currency = Currency;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setChangeOrderDescription(String ChangeOrderDescription) {
        this.ChangeOrderDescription = ChangeOrderDescription;
    }

    public String getChangeOrderDescription() {
        return ChangeOrderDescription;
    }


    public void setSoldToLegalEntity(String soldToLegalEntity) {
        this.soldToLegalEntity = soldToLegalEntity;
    }

    public String getSoldToLegalEntity() {
        return soldToLegalEntity;
    }

    public void setOrchestrationOrderFlag(String orchestrationOrderFlag) {
        this.orchestrationOrderFlag = orchestrationOrderFlag;
    }

    public String getOrchestrationOrderFlag() {
        return orchestrationOrderFlag;
    }

    public void setOrderNumber(int OrderNumber) {
        this.OrderNumber = OrderNumber;
    }

    public int getOrderNumber   () {
        return OrderNumber;
    }


    public void setDocumentStyleId(int DocumentStyleId) {
        this.DocumentStyleId = DocumentStyleId;
    }

    public int getDocumentStyleId() {
        return DocumentStyleId;
    }


    public void setPOHeaderId(String pOHeaderId) {
        this.pOHeaderId = pOHeaderId;
    }

    public String getPOHeaderId() {
        return pOHeaderId;
    }

    public void setSrnNumDFF(String srnNumDFF) {
        this.srnNumDFF = srnNumDFF;
    }

    public String getSrnNumDFF() {
        return srnNumDFF;
    }

    public void setSectorDFF(String sectorDFF) {
        this.sectorDFF = sectorDFF;
    }

    public String getSectorDFF() {
        return sectorDFF;
    }

    public void setServiceDFF(String serviceDFF) {
        this.serviceDFF = serviceDFF;
    }

    public String getServiceDFF() {
        return serviceDFF;
    }

    public void setAirCraftDFF(String airCraftDFF) {
        this.airCraftDFF = airCraftDFF;
    }

    public String getAirCraftDFF() {
        return airCraftDFF;
    }

    public void setCardNumDFF(String cardNumDFF) {
        this.cardNumDFF = cardNumDFF;
    }

    public String getCardNumDFF() {
        return cardNumDFF;
    }

    public void setCardHolderNameDFF(String cardHolderNameDFF) {
        this.cardHolderNameDFF = cardHolderNameDFF;
    }

    public String getCardHolderNameDFF() {
        return cardHolderNameDFF;
    }

    public void setCctrnNumDFF(String cctrnNumDFF) {
        this.cctrnNumDFF = cctrnNumDFF;
    }

    public String getCctrnNumDFF() {
        return cctrnNumDFF;
    }

    public void setArrICAODFF(String arrICAODFF) {
        this.arrICAODFF = arrICAODFF;
    }

    public String getArrICAODFF() {
        return arrICAODFF;
    }

    public void setDepICAODFF(String depICAODFF) {
        this.depICAODFF = depICAODFF;
    }

    public String getDepICAODFF() {
        return depICAODFF;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setShipToLocationCode(String shipToLocationCode) {
        this.shipToLocationCode = shipToLocationCode;
    }

    public String getShipToLocationCode() {
        return shipToLocationCode;
    }

    public void setSupplierSite(String supplierSite) {
        this.supplierSite = supplierSite;
    }

    public String getSupplierSite() {
        return supplierSite;
    }
}
