package com.appspro.nexuscc.rest;

import biPReports.RestHelper;

import com.appspro.nexuscc.bean.OverHeadBean;

import com.appspro.nexuscc.dao.BudgetVersionDAO;
import com.appspro.nexuscc.dao.MasterDataDao;
import com.appspro.nexuscc.dao.OverHeadDao;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("overHead")
public class OverHeadRest extends RestHelper {
    OverHeadDao service = new OverHeadDao();
    @POST
    @Path("/getByProjectId")
    @Produces(MediaType.APPLICATION_JSON)
    public String findbyid(String data) {
        OverHeadDao service = new OverHeadDao();
        ArrayList<OverHeadBean> list = new ArrayList<OverHeadBean>();
        try {
            JSONObject o = new JSONObject(data);
            String projectId =
                o.has("projectId") ? o.getString("projectId") : null;
            String versionId =
                o.has("versionId") ? o.getString("versionId") : null;
           list = service.getallrequest(projectId, versionId);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONArray(list).toString();

    }
    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
        OverHeadBean list = new OverHeadBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            OverHeadBean bean = mapper.readValue(body, OverHeadBean.class);
            list = service.insertToOverHead(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @POST
    @Path("/deleteSaasOverHead")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeElementOverHead (String body ){
            JSONObject json = new JSONObject(body);
            JSONObject res = null;
            OverHeadDao service = new OverHeadDao();
        JSONObject returnData = service.removePlanningElement(body);
           return Response.ok(returnData.toString()).status(200).build();
        }
    @GET
    @Path("/deleteDataBaseOverHead/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getdelete(@PathParam("id")
           String id) {

        try {
            service.removeElementFromDb(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @POST
    @Path("/updateOverHeadStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateOverHead (String id ){
                
            try {
                service.updateStatus(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
   

}
