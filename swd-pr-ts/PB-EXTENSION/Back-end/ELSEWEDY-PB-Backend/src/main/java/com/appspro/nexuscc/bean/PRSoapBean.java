package com.appspro.nexuscc.bean;

import org.json.JSONArray;

public class PRSoapBean {
   
    public String interfaceSourceCode;
    public String requisitioningBUId;
    public String requisitioningBUName;
    public String approverId;
    public String documentStatusCode;
    public String interfaceHeaderKey;
    public String preparerId;
    public String externallyManagedFlag;
    public String deliverToLocationId;
    public String categoryName;
    public String currencyCode;
    public String deliverToOrganizationCode;
    public String deliverToOrganizationId;
    public String destinationTypeCode;
    public String itemDescription;
    public String itemId;
    public String itemNumber;
    public String lineType;
    public String quantity;
    public String requesterId;
    public String unitOfMeasure;
    public String percent;
    public String budgetDate;
    public String lineItems ;
    public JSONArray items = new JSONArray();
    
    public void setInterfaceSourceCode(String interfaceSourceCode) {
        this.interfaceSourceCode = interfaceSourceCode;
    }

    public String getInterfaceSourceCode() {
        return interfaceSourceCode;
    }

    public void setRequisitioningBUId(String requisitioningBUId) {
        this.requisitioningBUId = requisitioningBUId;
    }

    public String getRequisitioningBUId() {
        return requisitioningBUId;
    }

    public void setRequisitioningBUName(String requisitioningBUName) {
        this.requisitioningBUName = requisitioningBUName;
    }

    public String getRequisitioningBUName() {
        return requisitioningBUName;
    }

    public void setApproverId(String approverId) {
        this.approverId = approverId;
    }

    public String getApproverId() {
        return approverId;
    }

    public void setDocumentStatusCode(String documentStatusCode) {
        this.documentStatusCode = documentStatusCode;
    }

    public String getDocumentStatusCode() {
        return documentStatusCode;
    }

    public void setInterfaceHeaderKey(String interfaceHeaderKey) {
        this.interfaceHeaderKey = interfaceHeaderKey;
    }

    public String getInterfaceHeaderKey() {
        return interfaceHeaderKey;
    }

    public void setPreparerId(String preparerId) {
        this.preparerId = preparerId;
    }

    public String getPreparerId() {
        return preparerId;
    }

    public void setExternallyManagedFlag(String externallyManagedFlag) {
        this.externallyManagedFlag = externallyManagedFlag;
    }

    public String getExternallyManagedFlag() {
        return externallyManagedFlag;
    }

    public void setDeliverToLocationId(String deliverToLocationId) {
        this.deliverToLocationId = deliverToLocationId;
    }

    public String getDeliverToLocationId() {
        return deliverToLocationId;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setDeliverToOrganizationCode(String deliverToOrganizationCode) {
        this.deliverToOrganizationCode = deliverToOrganizationCode;
    }

    public String getDeliverToOrganizationCode() {
        return deliverToOrganizationCode;
    }

    public void setDeliverToOrganizationId(String deliverToOrganizationId) {
        this.deliverToOrganizationId = deliverToOrganizationId;
    }

    public String getDeliverToOrganizationId() {
        return deliverToOrganizationId;
    }

    public void setDestinationTypeCode(String destinationTypeCode) {
        this.destinationTypeCode = destinationTypeCode;
    }

    public String getDestinationTypeCode() {
        return destinationTypeCode;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemDescription() {
        return itemDescription;
    }

   
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getLineType() {
        return lineType;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setRequesterId(String requesterId) {
        this.requesterId = requesterId;
    }

    public String getRequesterId() {
        return requesterId;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getPercent() {
        return percent;
    }

    public void setBudgetDate(String budgetDate) {
        this.budgetDate = budgetDate;
    }

    public String getBudgetDate() {
        return budgetDate;
    }


    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setLineItems(String lineItems) {
        this.lineItems = lineItems;
    }

    public String getLineItems() {
        return lineItems;
    }

    public void setItems(JSONArray items) {
        this.items = items;
    }

    public JSONArray getItems() {
        return items;
    }
}
