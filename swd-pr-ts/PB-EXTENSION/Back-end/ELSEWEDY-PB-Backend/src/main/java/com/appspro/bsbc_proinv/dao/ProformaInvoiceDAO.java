/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.dao;


import com.appspro.helper.Helper;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;

import org.apache.http.HttpResponse;

import java.net.URL;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
//import jdk.nashorn.internal.parser.JSONParser;
import static org.apache.http.HttpHeaders.USER_AGENT;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.jersey.json.impl.JSONHelper;


/**
 *
 * @author Anas Alghawi
 */
public class ProformaInvoiceDAO {

    private static String cloudURL = Helper.paasURL;
    private static String testURL =
        "http://10.10.10.142:8060/ords/bsbc_ws/PInv/shipments";

    private static String streamToString(InputStream inputStream) {
        String text =
            new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

    public String sendPost(String bean) {

        try {
            String url = cloudURL + "shipments";
            System.out.println(bean);

            //            JSONArray jsonArray = new JSONArray('['+bean+']');
            JSONObject jsonObj = new JSONObject(bean);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            System.out.println(jsonObj);
            // add header
            post.setHeader("User-Agent", USER_AGENT);
            //            System.out.println(jsonArray.getString("attachment"));
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

            //            for (int i = 0; i < jsonArray.length(); i++) {
            //                JSONObject jsonObj = jsonArray.getJSONObject(i);
            urlParameters.add(new BasicNameValuePair("PISeqNumber",
                                                     jsonObj.getString("PISeqNumber")));
            urlParameters.add(new BasicNameValuePair("shipmentNumber",
                                                     jsonObj.getString("shipmentNumber")));
            urlParameters.add(new BasicNameValuePair("shipmentDate",
                                                     jsonObj.getString("shipmentDate")));
            urlParameters.add(new BasicNameValuePair("curancey",
                                                     jsonObj.getString("curancey")));
            urlParameters.add(new BasicNameValuePair("referenceNumber",
                                                     jsonObj.getString("referenceNumber")));
            urlParameters.add(new BasicNameValuePair("attachment",
                                                     jsonObj.getString("attachment")));
            urlParameters.add(new BasicNameValuePair("shipmentTotalAmount",
                                                     jsonObj.getString("shipmentTotalAmount")));
            urlParameters.add(new BasicNameValuePair("saleOrder",
                                                     jsonObj.getString("saleOrder")));
            urlParameters.add(new BasicNameValuePair("poNumber",
                                                     jsonObj.getString("poNumber")));
            urlParameters.add(new BasicNameValuePair("tender",
                                                     jsonObj.getString("tender")));
            urlParameters.add(new BasicNameValuePair("headerReference",
                                                     jsonObj.getString("headerReference")));
            urlParameters.add(new BasicNameValuePair("lineReference",
                                                     jsonObj.getString("lineReference")));


            //            }


            post.setEntity(new UrlEncodedFormEntity(urlParameters));

            HttpResponse response = client.execute(post);
            System.out.println("\nSending 'POST'  : " + urlParameters);
            System.out.println("\nSending 'POST'  : " + url);
            System.out.println("Post parameters : " + post.getEntity());
            System.out.println("Response Code : " +
                               response.getStatusLine().getStatusCode());


            BufferedReader rd =
                new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());
        } catch (Exception e) {
            System.out.println("Error: ");
            e.printStackTrace();
        }
        return "";
    }


    public static String jsonGetRequestNonParam() {
        String json = null;
        try {
            //            URL url = new URL(cloudURL+"shipments");
            //            URL url = new URL(null, cloudURL+"shipments", new sun.net.www.protocol.https.Handler());
            //            HttpURLConnection connection = (HttpURLConnection) url.openConnection();


            URL url =
                new URL(null, cloudURL + "shipments", new sun.net.www.protocol.https.Handler());
            //                    URL url = new URL(cloudURL+"shipments");
            //                   HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                //                Helper.trustAllHosts();
                HttpsURLConnection https =
                    (javax.net.ssl.HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                https.setHostnameVerifier(Helper.DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }


            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.connect();
            InputStream inStream = conn.getInputStream();
            json = streamToString(inStream); // input stream to string
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }

    public static String jsonGetRequestTRX(String piNumber) {
        String json = null;
        try {
            //        URL url = new URL( Helper.paasURL + "custrx/"+piNumber);
            URL url =
                new URL(null, Helper.paasURL + "custrx/" + piNumber, new sun.net.www.protocol.https.Handler());
            HttpURLConnection conn = null;
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                //                Helper.trustAllHosts();
                HttpsURLConnection https =
                    (javax.net.ssl.HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                https.setHostnameVerifier(Helper.DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }


            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.connect();
            InputStream inStream = conn.getInputStream();
            json = streamToString(inStream); // input stream to string


        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }

}
