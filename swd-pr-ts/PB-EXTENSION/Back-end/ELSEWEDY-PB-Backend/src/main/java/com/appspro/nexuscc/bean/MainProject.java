
package com.appspro.nexuscc.bean;
import java.util.List;

public class MainProject {

    private Integer projectNumber;
    private String projectName;
    private Double summary;
    private List<SubProject> resources = null;

    public Integer getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(Integer projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Double getSummary() {
        return summary;
    }

    public void setSummary(Double summary) {
        this.summary = summary;
    }

    public List<SubProject> getResources() {
        return resources;
    }

    public void setResources(List<SubProject> resources) {
        this.resources = resources;
    }

}
