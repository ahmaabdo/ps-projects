/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.rest;

import com.appspro.bsbc_proinv.dao.PINumberDAO;
import com.appspro.helper.Helper;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Anas Alghawi
 */
@Path("/PINumber")
public class PINumberRest {
    PINumberDAO service = new PINumberDAO();


    @GET
    @Path("/PINextNumber/{bUName}")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String getPINumberRest(@PathParam("bUName")
        String bUName) throws Exception {
        String endpoint = "PINumber";

        return service.jsonGetRequestNonParam(endpoint, bUName);
        //        return Helper.callGetRest(Helper.paasURL+endpoint);


    }

    @GET
    @Path("/PICurrNumber")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String getPICurentNumberRest() throws Exception {
        String endpoint = "PINCurrNumber";
        String dummayStr = "";
        return service.jsonGetRequestNonParam(endpoint, dummayStr);
        //    return Helper.callGetRest(Helper.paasURL+endpoint);
    }

}
