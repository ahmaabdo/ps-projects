/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.rest;


//import javax.ws.rs.GET;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.appspro.bsbc_proinv.dao.ProformaInvoiceDAO;
import com.appspro.bsbc_proinv.dao.AcknowledgeDAO;
import com.appspro.bsbc_proinv.dao.ReflectActionDAO;
import com.appspro.helper.Helper;

import static javax.ws.rs.HttpMethod.PUT;
import javax.ws.rs.PUT;

/**
 *
 * @author Anas Alghawi
 */
@Path("/AcknowledgeRest")
public class AcknowledgeRest {
    AcknowledgeDAO serviceAcknowledgeDAO = new AcknowledgeDAO();


    @GET
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String getPIAcknowledge() throws Exception {

        return serviceAcknowledgeDAO.jsonGetRequestNonParam();

    }

    @GET
    @Produces( { "application/json" })
    @Path("/{pINumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    public String getPIAcknowledgePara(@PathParam("pINumber")
        String pINumber) throws Exception {

        return serviceAcknowledgeDAO.jsonGetRequestParam(pINumber);

    }

    @GET
    @Path("/saleorder/{ordernumber}")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String getBySaleOrderPara(@PathParam("ordernumber")
        String orderNumber) throws Exception {

        return serviceAcknowledgeDAO.jsonGetRequestSaleOrderParam(orderNumber);

    }

    @GET
    @Path("/tender/{tender}")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String jsonGetRequestTenderParam(@PathParam("tender")
        String tender) throws Exception {

        return serviceAcknowledgeDAO.jsonGetRequestTenderParam(tender);

    }

    @GET
    @Path("/POnumber/{ponumber}")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String getByPONumberPara(@PathParam("ponumber")
        String poNumber) throws Exception {

        return serviceAcknowledgeDAO.jsonGetRequestPOParam(poNumber);

    }

    @GET
    @Path("/soap")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String getByPONumberPara() throws Exception {
        ReflectActionDAO ref = new ReflectActionDAO();
        return ref.executeReports();

    }


    @POST
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String postPIAcknowledge(String bean) throws Exception {

        //     serviceAcknowledgeDAO.sendPost(bean);
        //     return bean ;
        String url = Helper.paasURL + "PI";
        String returnValue = Helper.callPostRest(url, bean);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;

    }

    @POST
    @Path("/ack")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String putPIAcknowledge(String bean) throws Exception {

        serviceAcknowledgeDAO.sendPut(bean);

        String url = Helper.paasURL + "updatePI";
        String returnValue = Helper.callPostRest(url, bean);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        return returnValue1;
    }

    @POST
    @Path("/saasarinv/{custrx}")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String addNewToSaaSARInvoice(@PathParam("custrx")
        String custrx, String bean) throws Exception {

        String url =
            Helper.saasURL + "fscmRestApi/resources/11.13.18.05/receivablesInvoices/" +
            custrx + "/child/receivablesInvoiceDFF";

        String returnValue = Helper.callPostRestSaaS(url, bean);

        System.out.println(url);
        return returnValue;

    }

}

