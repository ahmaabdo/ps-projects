package com.appspro.nexuscc.dao;

import com.appspro.db.AppsproConnection;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.nexuscc.bean.ProcurmentPlanBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProcurmentPlanDao extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ProcurmentPlanBean insertToProcurment(ProcurmentPlanBean bean) {
        String query = null;
        try {
            connection = AppsproConnection.getConnection();
            query =
                    "insert into xxx_procurment_plan (SYSTEM,MAKE,SUPPLIER,COST_ID,CURRENCY,AFTER_DISC_TOT_COST,EXCHANGE_RATE,AFTER_DISC_TOT_COST_AED,BEFORE_DISC_COST_AED,DISCOUNT_AED,DISCOUNT_RATE,FORECOST_PR_DATE,PR_PROCESSING_TIME,FORECOST_ORDER_DATE,MANUFACTURING_LEAD_TIME,CONTINGENCY_TIME,MAT_READY_WITH_SUPPLIER,SHIPPING_TIME,ETA_DSO_DATE,CABINET_ASSEMBLY,PRE_FAT,FAT,FAT_PUNCH_CLERANCE,INTEGRATION_FAT_DATE,PACKING,DELIVERY,TARGET_DELIVERY_DATE,PROJECT_ID,VERSION_ID,PAYMENT_DATE,PAYMENT_TERM)" +
                    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = connection.prepareStatement(query);

            ps.setString(1, bean.getSystem());
            ps.setString(2, bean.getMake());
            ps.setString(3, bean.getSupplier());
            ps.setString(4, bean.getCostId());
            ps.setString(5, bean.getCurrency());
            ps.setString(6, bean.getAfterDistotalCostFc());
            ps.setString(7, bean.getExchangeRate());
            ps.setString(8, bean.getAfterDistotalCostAED());
            ps.setString(9, bean.getBeforeDistotalCostAED());
            ps.setString(10, bean.getDiscountAED());
            ps.setString(11, bean.getDiscountRate());
            ps.setString(12, bean.getChild().getJSONObject(0).optString("forecostPRDate"));
            ps.setString(13, bean.getChild().getJSONObject(0).optString("prProcessingTime"));
            ps.setString(14, bean.getChild().getJSONObject(0).optString("forecostOrderDate"));
            ps.setString(15, bean.getChild().getJSONObject(0).optString("manufacturingLeadTime"));
            ps.setString(16, bean.getChild().getJSONObject(0).optString("contingencyTime"));
            ps.setString(17, bean.getChild().getJSONObject(0).optString("matReadyWithSupplier"));
            ps.setString(18, bean.getChild().getJSONObject(0).optString("shippingTime"));
            ps.setString(19, bean.getChild().getJSONObject(0).optString("ETADSODate"));
            ps.setString(20, bean.getChild().getJSONObject(0).optString("cabinetAssembly"));
            ps.setString(21, bean.getChild().getJSONObject(0).optString("PreFAT"));
            ps.setString(22, bean.getChild().getJSONObject(0).optString("FAT"));
            ps.setString(23, bean.getChild().getJSONObject(0).optString("FATPunchClearance"));
            ps.setString(24, bean.getChild().getJSONObject(0).optString("integerationFATDate"));
            ps.setString(25, bean.getChild().getJSONObject(0).optString("packing"));
            ps.setString(26, bean.getChild().getJSONObject(0).optString("delivery"));
            ps.setString(27, bean.getChild().getJSONObject(0).optString("targetDeliveryDate"));
            ps.setString(28, bean.getProjectId());
            ps.setString(29, bean.getVersionId());
            ps.setString(30, bean.getChild().getJSONObject(0).optString("paymentDate"));
            ps.setString(31, bean.getChild().getJSONObject(0).optString("paymentTerm"));

            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public JSONObject getAllProcurment(String projectId , String versionId) {
           
        JSONObject response = new JSONObject();
        JSONArray res = new JSONArray();
        
        String query = null;
        try {
            ProcurmentPlanBean bean = new ProcurmentPlanBean();
            connection = AppsproConnection.getConnection();
            query = "SELECt * FROM  xxx_procurment_plan WHERE PROJECT_ID =? AND VERSION_ID = ?";
            
            ps = connection.prepareStatement(query);
            ps.setString(1, projectId);
            ps.setString(2, versionId);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                
                bean.setSystem(rs.getString("SYSTEM"));
                bean.setMake(rs.getString("MAKE"));
                bean.setSupplier(rs.getString("SUPPLIER"));
                bean.setCostId(rs.getString("COST_ID"));
                bean.setCurrency(rs.getString("CURRENCY"));
                bean.setAfterDistotalCostFc(rs.getString("AFTER_DISC_TOT_COST"));
                bean.setExchangeRate(rs.getString("EXCHANGE_RATE"));
                bean.setAfterDistotalCostAED(rs.getString("AFTER_DISC_TOT_COST_AED"));
                bean.setBeforeDistotalCostAED(rs.getString("BEFORE_DISC_COST_AED"));
                bean.setDiscountAED(rs.getString("DISCOUNT_AED"));
                bean.setDiscountRate(rs.getString("DISCOUNT_RATE"));
                bean.setForecastPrDate(rs.getString("FORECOST_PR_DATE"));
                bean.setPrProcessingTime(rs.getString("PR_PROCESSING_TIME"));
                bean.setForecostOrderDate(rs.getString("FORECOST_ORDER_DATE"));
                bean.setManufactingLeadTime(rs.getString("MANUFACTURING_LEAD_TIME"));
                bean.setCantingencyTime(rs.getString("CONTINGENCY_TIME"));
                bean.setMatReadyWith(rs.getString("MAT_READY_WITH_SUPPLIER"));
                bean.setShippingTime(rs.getString("SHIPPING_TIME"));
                bean.setEtaDsoDate(rs.getString("ETA_DSO_DATE"));
                bean.setCabinetAssembly(rs.getString("CABINET_ASSEMBLY"));
                bean.setPreFat(rs.getString("PRE_FAT"));
                bean.setFat(rs.getString("FAT"));
                bean.setFatPunchClearance(rs.getString("FAT_PUNCH_CLERANCE"));
                bean.setIntegerationFatDate(rs.getString("INTEGRATION_FAT_DATE"));
                bean.setPacking(rs.getString("PACKING"));
                bean.setDelivery(rs.getString("DELIVERY"));
                bean.setPaymentTerm(rs.getString("PAYMENT_TERM"));
                bean.setPaymentDate(rs.getString("PAYMENT_DATE"));
                bean.setTargetDeliveryDate(rs.getString("TARGET_DELIVERY_DATE"));
                bean.setProjectId(rs.getString("PROJECT_ID"));
                bean.setVersionId(rs.getString("VERSION_ID"));
                
                res.put(new JSONObject( bean));
                
            }
            response.put("data", res);
        }
    
        catch (Exception e) {
            response.put("status", "ERROR");
            response.put("data", e.getMessage());
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return response;
        }
    
}
