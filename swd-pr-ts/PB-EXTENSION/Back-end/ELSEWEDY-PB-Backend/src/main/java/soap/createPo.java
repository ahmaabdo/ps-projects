package soap;

import biPReports.RestHelper;

import com.appspro.nexuscc.bean.SoapAtrrBean;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONObject;

import org.json.XML;

import org.w3c.dom.Document;

import org.w3c.dom.stylesheets.DocumentStyle;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class createPo {
    public static  String status;

    public static  int poHeaderId;

    public static void main(String [] args) throws IOException,SAXException, ParserConfigurationException {

//        SoapAtrrBean bean = new SoapAtrrBean();
//                                    bean.setOrderNumber(5005);
//                                    bean.setDocumentStyle(("Purchase Order"));
//                                    bean.setDocumentStyleId(1); 
//                                    bean.setProcurementBusinessUnit(("Aviation Academy KSA B.U"));
//                                    bean.setRequisitioningBusinessUnit(("Aviation Academy KSA B.U"));
//                                    bean.setSoldToLegalEntity(("Purchase Order"));
//                                    bean.setBuyerName(("SCM, APPSPRO-"));
//                                    bean.setCurrency(("Saudi Riyal"));
//                                    bean.setSupplier(("ABN Ashlan Real Estate Office"));
                                    
                        String response =
                                    callPostRest2("https://ejdg-test.fa.em2.oraclecloud.com:443/fscmService/PurchaseOrderServiceV2",new SoapAtrrBean());

                                System.out.println(response);
                                }
                            
                            public static String callPostRest2(String destUrl,SoapAtrrBean bean) throws IOException,
                                                              SAXException,
                                                              ParserConfigurationException {
                                RestHelper rh = new RestHelper();


                                try {

                                    String postData =
                                        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/prc/po/editDocument/purchaseOrderServiceV2/types/\" xmlns:pur=\"http://xmlns.oracle.com/apps/prc/po/editDocument/purchaseOrderServiceV2/\" xmlns:draf=\"http://xmlns.oracle.com/apps/prc/po/editDocument/flex/draftPurchaseOrderDistribution/\" xmlns:pjc=\"http://xmlns.oracle.com/apps/prc/po/commonPo/flex/PJCPoDraftDistribution/\" xmlns:draf1=\"http://xmlns.oracle.com/apps/prc/po/editDocument/flex/draftPurchasingDocumentSchedule/\" xmlns:draf2=\"http://xmlns.oracle.com/apps/prc/po/editDocument/flex/draftPurchasingDocumentLine/\" xmlns:draf3=\"http://xmlns.oracle.com/apps/prc/po/editDocument/flex/draftPurchasingDocumentHeader/\">\n" +
                                        "   <soapenv:Header/>\n" +
                                        "   <soapenv:Body>\n" +
                                        "      <typ:createPurchaseOrder>\n" +
                                        "         <typ:createOrderEntry>\n" +
                                                        "<pur:DocumentStyle>"+bean.getDocumentStyle()+"</pur:DocumentStyle>\n" +
                                                        "<pur:ProcurementBusinessUnit>"+bean.getProcurementBusinessUnit()+"</pur:ProcurementBusinessUnit>\n" +
                                                        "<pur:RequisitioningBusinessUnit>"+bean.getRequisitioningBusinessUnit()+"</pur:RequisitioningBusinessUnit>\n" +
                                                        "<pur:BuyerName>"+bean.getBuyerName()+"</pur:BuyerName>\n" +
                                                        "<pur:Supplier>"+bean.getSupplier()+"</pur:Supplier>\n" +
                                                        "<pur:DefaultShipToLocationCode>Riyadh</pur:DefaultShipToLocationCode>\n" +
                                                        "<pur:Currency>"+bean.getCurrency()+"</pur:Currency>\n" +
                                            "           <pur:PurchaseOrderEntryLine>\n" +
                                            "               <pur:LineNumber>1</pur:LineNumber>\n" +
                                            "               <pur:LineType>Goods</pur:LineType>\n" +
                                            "               <pur:ItemNumber>RAM01</pur:ItemNumber>\n" +
                                            "               <pur:ItemDescription>"+bean.getItemDescription()+"</pur:ItemDescription>\n" +
                                            "               <pur:UnitOfMeasure>Each</pur:UnitOfMeasure>\n" +
                                            "               <pur:Quantity unitCode=\"Ea\">1</pur:Quantity>\n" +
                                            "               <pur:Price currencyCode=\"SAR\">"+bean.getPrice()+"</pur:Price>\n" +
                                            "               <pur:PurchaseOrderEntrySchedule>\n" +
                                            "                  <pur:ScheduleNumber>1</pur:ScheduleNumber>\n" +
                                            "                  <pur:Quantity unitCode=\"Ea\">1</pur:Quantity>\n" +
                                            "                  <pur:OrderedAmount currencyCode=\"SAR\">1000</pur:OrderedAmount>\n" +
                                            "                  <pur:ShipToLocationCode>"+bean.getShipToLocationCode()+"</pur:ShipToLocationCode>\n" +
                                            "                  <pur:ShipToOrganizationCode>AAK</pur:ShipToOrganizationCode>\n" +
                                            "                  <pur:DestinationTypeCode>EXPENSE</pur:DestinationTypeCode>\n" +
                                            "                  <pur:DestinationType>Expense</pur:DestinationType>\n" +
                                           "                   <pur:PurchaseOrderEntryDistribution>\n" +
                                            "                     <pur:DistributionNumber>1</pur:DistributionNumber>\n" +
                                            "                     <pur:Quantity unitCode=\"Ea\">1</pur:Quantity>\n" +
                                            "                     <pur:OrderedAmount currencyCode=\"SAR\">1000</pur:OrderedAmount>\n" +
                                            "                     <pur:DeliverToLocationCode>Riyadh</pur:DeliverToLocationCode>\n" +
                                            "                         <pur:DistributionFlexfield>\n" + 
                                                                        "<draf:cctrn>"+bean.getCctrnNumDFF()+"</draf:cctrn>\n" +
                                                                        "<draf:srn>"+bean.getSrnNumDFF()+"</draf:srn>\n" +
                                                                        "<draf:sectorNumber>"+bean.getSectorDFF()+"</draf:sectorNumber>\n" +
                                                                        "<draf:cardNumber>"+bean.getCardNumDFF()+"</draf:cardNumber>\n" +                                                                     
                                                                        "<draf:cardHolderName>"+bean.getCardHolderNameDFF()+"</draf:cardHolderName>\n" +                                                                     
                                                                        "<draf:depIcao>"+bean.getDepICAODFF()+"</draf:depIcao>\n" +                                                                     
                                                                        "<draf:arrIcao>"+bean.getArrICAODFF()+"</draf:arrIcao>\n" +                                                                     
                                                                        "<draf:service>"+bean.getServiceDFF()+"</draf:service>\n" +
                                                                        "<draf:airCraft>"+bean.getAirCraftDFF()+"</draf:airCraft>\n" +
                                        "                        </pur:DistributionFlexfield>\n" + 
                                        "                      </pur:PurchaseOrderEntryDistribution>\n" +
                                        "                </pur:PurchaseOrderEntrySchedule>\n" +
                                        "            </pur:PurchaseOrderEntryLine>\n" +
                                        "         </typ:createOrderEntry>\n" +
                                        "      </typ:createPurchaseOrder>\n" +
                                        "   </soapenv:Body>\n" +
                                        "</soapenv:Envelope>";
                                    
                                    System.out.println(postData);
                                        System.setProperty("DUseSunHttpHandler", "true");
                                        byte[] buffer = new byte[postData.length()];
                                        buffer = postData.getBytes();
                                        ByteArrayOutputStream bout = new ByteArrayOutputStream();
                                        bout.write(buffer);
                                        byte[] b = bout.toByteArray();
                                        java.net.URL url =
                                            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
                                        java.net.HttpURLConnection http;
                                        if (url.getProtocol().toLowerCase().equals("https")) {
                                           rh.trustAllHosts();
                                            java.net.HttpURLConnection https =
                                                (HttpsURLConnection)url.openConnection();
                                                        System.setProperty("DUseSunHttpHandler", "true");
                                         //   https.setHostnameVerifier(rh.DO_NOT_VERIFY);
                                            http = https;
                                        } else {
                                            http = (HttpURLConnection)url.openConnection();
                                        }
                                        String SOAPAction = "";
                            
                                        http.setRequestProperty("Content-Length",
                                                                String.valueOf(b.length));
                                        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                                        http.setRequestProperty("SOAPAction", SOAPAction);
                                        http.setRequestProperty("Authorization",
                                                                "Basic " + "bW9oYW1lZC5xdXJhbnk6TmV4dXNAMjAxOQ==");
                                        http.setRequestMethod("POST");
                                        http.setDoOutput(true);
                                        http.setDoInput(true);
                                        OutputStream out = http.getOutputStream();
                                        out.write(b);
                            
                                        System.out.println("connection status: " + http.getResponseCode() +
                                                           "; connection response: " +
                                                           http.getResponseMessage());
                                       
                                                            if (http.getResponseCode() == 200) {
                                                               InputStream in = http.getInputStream();
                                                               InputStreamReader iReader = new InputStreamReader(in);
                                                               BufferedReader bReader = new BufferedReader(iReader);

                                                               String line;
                                                               String response = "";
                                                               System.out.println("==================Service response: ================ ");
                                                               while ((line = bReader.readLine()) != null) {
                                                                   response += line;
                                                                   System.out.println(response);

                                                               }

                                                               if (response.indexOf("<?xml") > 0)
                                                                   response =
                                                                           response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                                                                              15);

                                                               DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
                                                               fact.setNamespaceAware(true);
                                                               DocumentBuilder builder =
                                                                   DocumentBuilderFactory.newInstance().newDocumentBuilder();

                                                               InputSource src = new InputSource();
                                                               src.setCharacterStream(new StringReader(response));

                                                               Document doc = builder.parse(src);
                                                               
                                                              // XML.toJSONObject(response).toString();
                                                               System.out.println(XML.toJSONObject(response).toString());
                                                               JSONObject o = XML.toJSONObject(response);
                                                               //x["env:Envelope"]["env:Body"]["ns0:createPurchaseOrderResponse"]["ns1:result"]["ns0:RequestStatus"]
                                                               status =  o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:createPurchaseOrderResponse").getJSONObject("ns1:result").getString("ns0:RequestStatus");
                                                               poHeaderId = o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:createPurchaseOrderResponse").getJSONObject("ns1:result").getInt("ns0:OrderNumber");
                                                              System.out.println("request status "+status);
                                                              System.out.println("request po header id  "+poHeaderId);
                                                              
                                                              
                                                           }         
                                       
                                    } catch (MalformedURLException e) {
                            
                                        e.printStackTrace();
                            
                                    } catch (IOException e) {
                            
                                        e.printStackTrace();
                            
                                    }
                return  status;
        }
}
