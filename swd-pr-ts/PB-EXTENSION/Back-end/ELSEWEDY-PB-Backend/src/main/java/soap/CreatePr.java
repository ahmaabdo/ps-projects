package soap;

import biPReports.RestHelper;

import com.appspro.nexuscc.bean.PRSoapBean;
import com.appspro.nexuscc.bean.SoapAtrrBean;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class CreatePr {
    
    public static String status;
    public static String requisitionNum;
    public static String requisitionStatus;

    public static void main(String[] args) throws IOException, SAXException,
                                                  ParserConfigurationException {
      
        String response =
            callPostRest2("https://elhn-test.fa.em2.oraclecloud.com:443/fscmService/PurchaseRequestService",
                          new PRSoapBean());

        System.out.println(response);
    }

    public static String callPostRest2(String destUrl,
                                       PRSoapBean bean) throws IOException,
                                                                 SAXException,
                                                                 ParserConfigurationException {
        RestHelper rh = new RestHelper();


        try {

            String postData1 =
           "<soapenv:Envelope xmlns:pur=\"http://xmlns.oracle.com/apps/prc/po/editDocument/purchaseRequestService/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/prc/po/editDocument/purchaseRequestService/types/\">\n" + 
           "  <soapenv:Header/>\n" + 
           "   <soapenv:Body>\n" + 
           "      <typ:createRequisition>\n" + 
           "         <typ:interfaceSourceCode>"+bean.getInterfaceSourceCode()+"</typ:interfaceSourceCode>\n" + 
           "         <typ:requisitioningBUId>"+bean.getRequisitioningBUId()+"</typ:requisitioningBUId>\n" + 
           "         <typ:requisitioningBUName>"+bean.getRequisitioningBUName()+"</typ:requisitioningBUName>\n" + 
           "         <typ:purchaseRequestPayload>\n" + 
           "            <pur:DocumentStatusCode>"+bean.getDocumentStatusCode()+"</pur:DocumentStatusCode>\n" + 
           "            <pur:InterfaceHeaderKey>"+bean.getInterfaceHeaderKey()+"</pur:InterfaceHeaderKey>\n" + 
           "            <pur:PreparerId>"+bean.getPreparerId()+"</pur:PreparerId>\n" + 
           "            <pur:RequisitioningBUId>"+bean.getRequisitioningBUId()+"</pur:RequisitioningBUId>\n" + 
           "            <pur:RequisitioningBUName>"+bean.getRequisitioningBUName()+"</pur:RequisitioningBUName>\n" + 
           "            <pur:ExternallyManagedFlag>"+bean.getExternallyManagedFlag()+"</pur:ExternallyManagedFlag>\n"+
           "            <pur:PurchaseRequestInputReqLineInterface>\n" ; 
           String postData2 =                  " <pur:PurchaseRequestInputReqDistInterface>\n" + 
           "                  <pur:Percent>"+bean.getPercent()+"</pur:Percent>\n" + 
           "                  <pur:BudgetDate>"+bean.getBudgetDate()+"</pur:BudgetDate>\n" + 
           "               </pur:PurchaseRequestInputReqDistInterface>\n" +  
                     " </pur:PurchaseRequestInputReqLineInterface>\n"+
           "         </typ:purchaseRequestPayload>\n" + 
           "      </typ:createRequisition>\n" + 
           "   </soapenv:Body>\n" + 
           "</soapenv:Envelope>" ;
            
            
            
            StringBuilder postData = new StringBuilder();
            postData.append(postData1);
            
            System.out.println("lineItems Length: \n"+bean.getItems().length());            
            System.out.println("lineItems: \n"+bean.getItems().getJSONObject(0).optString("deliverToOrganizationCode"));
            for (int i = 0; i < bean.getItems().length(); i++) {
                String postLine =
                "               <pur:DeliverToLocationId>"+bean.getItems().getJSONObject(i).optString("deliverToLocationId")+"</pur:DeliverToLocationId>\n" + 
                "               <pur:CurrencyCode>"+bean.getItems().getJSONObject(i).optString("currencyCode")+"</pur:CurrencyCode>\n" + 
                "               <pur:DeliverToOrganizationCode>"+bean.getItems().getJSONObject(i).optString("deliverToOrganizationCode")+"</pur:DeliverToOrganizationCode>\n" + 
                "               <pur:DeliverToOrganizationId>"+bean.getItems().getJSONObject(i).optString("deliverToOrganizationId")+"</pur:DeliverToOrganizationId>\n" + 
                "               <pur:DestinationTypeCode>"+bean.getItems().getJSONObject(i).optString("destinationTypeCode")+"</pur:DestinationTypeCode>\n" + 
                "               <pur:ItemDescription>"+bean.getItems().getJSONObject(i).optString("itemDescription")+"</pur:ItemDescription>\n" + 
                "               <pur:ItemId>"+bean.getItems().getJSONObject(i).optString("itemId")+"</pur:ItemId>\n" + 
                "               <pur:ItemNumber>"+bean.getItems().getJSONObject(i).optString("itemNumber")+"</pur:ItemNumber>\n" + 
                "               <pur:LineType>"+bean.getItems().getJSONObject(i).optString("lineType")+"</pur:LineType>\n" + 
                "               <pur:Quantity unitCode=\"Each\">"+bean.getItems().getJSONObject(i).optString("quantity")+"</pur:Quantity>\n" + 
                "               <pur:UnitOfMeasureCode>"+bean.getItems().getJSONObject(i).optString("unitOfMeasure")+"</pur:UnitOfMeasureCode>\n" ; 
                postData.append(postLine);
            }
            postData.append(postData2);
            
            

            System.out.println(postData);
            System.setProperty("DUseSunHttpHandler", "true");
            byte[] buffer = new byte[postData.length()];

            buffer =  postData.toString().getBytes();
            System.out.println("bufffffffffffffffffffer is \n"+buffer);

            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                rh.trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";

            http.setRequestProperty("Content-Length",String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization", "Basic " + "YWxraGF3YXJyYS53YXNlZW06OTg3NjU0MzIx");
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);

            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());

            if (http.getResponseCode() == 200) {
                InputStream in = http.getInputStream();
                InputStreamReader iReader = new InputStreamReader(in);
                BufferedReader bReader = new BufferedReader(iReader);

                String line;
                String response = "";
                System.out.println("==================Service response: ================ ");
                while ((line = bReader.readLine()) != null) {
                    response += line;
                    System.out.println(response);

                }

                if (response.indexOf("<?xml") > 0)
                    response =
                            response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                               15);

                DocumentBuilderFactory fact =
                    DocumentBuilderFactory.newInstance();
                fact.setNamespaceAware(true);
                DocumentBuilder builder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

                InputSource src = new InputSource();
                src.setCharacterStream(new StringReader(response));

                Document doc = builder.parse(src);

                 XML.toJSONObject(response).toString();
                System.out.println(XML.toJSONObject(response).toString());
                JSONObject o = XML.toJSONObject(response);
                //x["env:Envelope"]["env:Body"]["ns0:createRequisitionResponse"]["ns1:result"]["ns0:Status"]
                status = o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:createRequisitionResponse").getJSONObject("ns1:result").getString("ns0:Status");
                requisitionNum = o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:createRequisitionResponse").getJSONObject("ns1:result").getString("ns0:RequisitionNumber");
                requisitionStatus = o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:createRequisitionResponse").getJSONObject("ns1:result").getString("ns0:RequisitionStatus");
              
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return status;
    }
}
