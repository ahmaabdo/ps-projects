define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'config/services',
    'util/commonhelper', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout',
    'ojs/ojarraytabledatasource', 'ojs/ojoffcanvas', 'ojs/ojbutton', 'jquery',
    'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojselectcombobox', 
    'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojmessages'],
        function (oj, ko, moduleUtils, services, commonhelper) {
            function ControllerViewModel() {
                var self = this;
                var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);

                self.username = ko.observable();
                self.password = ko.observable();
                self.userLogin = ko.observable();
                self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
                self.empGrade = ko.observable();
                self.empDepartment = ko.observable();
                self.empNumber = ko.observable();
                self.empJob = ko.observable();
                self.empName = ko.observable();
                self.jwt = ko.observable();
                self.usersArr = ko.observableArray();
                self.locationArr = ko.observableArray();
                self.vendorSiteArr = ko.observableArray();
                self.vendorArr = ko.observableArray();
                self.customerNameArr = ko.observableArray();
                self.loginUser = ko.observable();
                self.invoiceNum = ko.observableArray();
                self.buyerNameArr = ko.observableArray();
                self.businessUnitArr = ko.observableArray();
                self.invoiceNumArr = ko.observableArray();
                self.purchaseOrdersArrPoHeader = ko.observableArray();
                self.purchaseOrdersArrPoNum = ko.observableArray();
                self.vendorSiteIndepArr = ko.observableArray();
                self.vendorNameVal = ko.observable();
                self.messagesDataProvider = ko.observableArray();
                self.test2 = ko.observableArray();
                

                self.loading = function (load) {
                    if (load) {
                        $('#loading').show();
                    } else {
                        $('#loading').hide();
                    }
                };
                // Search Function for lables 
                self.searchArray = function (nameKey, searchArray) {
                    for (var i = 0; i < searchArray.length; i++) {
                        if (searchArray[i].value == nameKey) {
                            return searchArray[i].desc;
                        }
                    }
                };
                if (sessionStorage.empName != "null")
                {
                    self.empName(sessionStorage.empName);
                } else
                {
                    self.empName('');
                }
                if (sessionStorage.empJob != "null")
                {
                    self.empJob(sessionStorage.empJob);
                } else
                {
                    self.empJob('');
                }
                if (sessionStorage.empNumber != "null")
                {
                    self.empNumber(sessionStorage.empNumber);
                } else
                {
                    self.empNumber('');

                }
                if (sessionStorage.empDepartment != "null")
                {
                    self.empDepartment(sessionStorage.empDepartment);
                } else
                {
                    self.empDepartment('');
                }
                if (sessionStorage.empGrade != "null")
                {
                    self.empGrade(sessionStorage.empGrade);
                } else
                {
                    self.empGrade('');
                }

                self.isUserLoggedIn = ko.observable(false);

                var url = new URL(window.location.href);
                var jwt;

                if (url.searchParams.get("jwt")) {
                    jwt = url.searchParams.get("jwt");
                    self.jwt(jwt);


                    jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
                    var username = jwtJSON.sub;
                    self.username(username.split('.')[0] + username.split('.')[1]);

                    self.userLogin(sessionStorage.Username);

                    if (self.isUserLoggedIn() === 'false') {
                        self.isUserLoggedIn(false);
                    } else {
                        self.isUserLoggedIn(true);
                    }

                } else if (sessionStorage.isUserLoggedIn) {
                    self.userLogin(sessionStorage.Username);


                    if (self.isUserLoggedIn() === 'false') {
                        self.isUserLoggedIn(false);
                    } else {
                        self.isUserLoggedIn(true);
                    }
                }

                $(function () {
                    $('#username').on('keydown', function (ev) {
                        var mobEvent = ev;
                        var mobPressed = mobEvent.keyCode || mobEvent.which;
                        if (mobPressed == 13) {
                            $('#username').blur();
                            $('#password').focus();
                        }
                        return true;
                    });
                });

                //close notify in when focus on login input
                $("#userValidation,#passValidation").focusout(function () {
//                    $(".notifyjs-bootstrap-base").notify().hide();
                });
                //login button call function
                self.validateUser = function () {
                    $(".apLoginBtn button").addClass("loading");
                    var username = self.username();
                    var password = self.password();
                    // Here we check null value and show error to fill
                    if (!username && !password) {
                        $(".apLoginBtn button").removeClass("loading");
                        self.messagesDataProvider.push(self.createMessage('error', 'Username and Password are required'));
                    } else if (!username) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Username and Password are required'));
                        $(".apLoginBtn button").removeClass("loading");
                    } else if (!password) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Username and Password are required'));
                        $(".apLoginBtn button").removeClass("loading");
                    } else {

                        $('#loader-overlay').show();
                        var loginCbFn = function (data) {
                            self.getEmployeeDetails();
                            var parsedData = jQuery.parseJSON(data.replace('\'', '"').replace('\'', '"'));


                            if (parsedData.result == true) {
                                self.messagesDataProvider([]);
                                self.isUserLoggedIn(true);
                                sessionStorage.setItem('username', self.isUserLoggedIn());

                                var userNamelabel = self.username();
                                sessionStorage.setItem('isUserLoggedIn', self.isUserLoggedIn());
                                sessionStorage.setItem('Username', userNamelabel);

                                $('#loader-overlay').hide();


                            } else {
                                $(".apLoginBtn button").removeClass("loading");
                                // If authentication failed
                                // alert(response.Error);
//                                $.notify("Wrong user name or passowrd", "error");
                                self.messagesDataProvider([]);
                                self.messagesDataProvider.push(self.createMessage('error', 'Invalid Username or Password'));

                                return;

                            }
                        };
                        self.loginUser(username);
                        var loginFailCbFn = function () {
                            $(".apLoginBtn button").removeClass("loading");
                            self.messagesDataProvider.push(self.createMessage('error', 'Invalid Username or Password'));
                            return;
                        };

                        var payload = {
                            "userName": username,
                            "password": password
                        };
                        services.authenticate(payload).then(loginCbFn, loginFailCbFn);
                        oj.Router.rootInstance.go('PurchaseSummry');
                        return;
                    }
                };

                $(".home-icon").click(function () {
                    oj.Router.rootInstance.go('PurchaseSummry');
                });

                self.getEmployeeDetails = function () {
                    var getEmployeeDetailsCbFn = function (data) {

                        sessionStorage.setItem('empName', data.displayName);
                        sessionStorage.setItem('empJob', data.jobName);
                        sessionStorage.setItem('empNumber', data.personNumber);
                        sessionStorage.setItem('empDepartment', data.departmentName);
                        sessionStorage.setItem('empGrade', data.grade);

                        if (sessionStorage.empName != "null")
                        {
                            self.empName(sessionStorage.empName);

                        } else
                        {
                            self.empName('');
                        }
                        if (sessionStorage.empJob != "null")
                        {
                            self.empJob(sessionStorage.empJob);
                        } else
                        {
                            self.empJob('');
                        }
                        if (sessionStorage.empNumber != "null")
                        {
                            self.empNumber(sessionStorage.empNumber);
                        } else
                        {
                            self.empNumber('');

                        }
                        if (sessionStorage.empDepartment != "null")
                        {
                            self.empDepartment(sessionStorage.empDepartment);
                        } else
                        {
                            self.empDepartment('');
                        }
                        if (sessionStorage.empGrade != "null")
                        {
                            self.empGrade(sessionStorage.empGrade);
                        } else
                        {
                            self.empGrade('');
                        }
                    };
                    var getEmployeeDetailsfailCbFn = function () {
                    };
                    var UrlPath = "getemployee/" + self.username();

                    //services.getGeneric(UrlPath).then(getEmployeeDetailsCbFn, getEmployeeDetailsfailCbFn);
                };


                self.menuItemAction = function (event) {
                    // if (event.target.value === 'out') {
                    location.reload();
                    self.isUserLoggedIn(false);
                    sessionStorage.setItem('username', '');
                    sessionStorage.setItem('isUserLoggedIn', '');


                    if (self.language() === 'english')
                    {
                        $('html').attr({'dir': 'ltr'});
                    } else if (self.language() === 'arabic')
                    {
                        $('html').attr({'dir': 'rtl'});
                    }

                };

                self.loginErrorPopup = function () {
                    var popup = document.querySelector('#popup1');
                    popup.close('#btnGo');
                };

                startAnimationListener = function (data, event) {
                    var ui = event.detail;
                    if (!$(event.target).is("#popup1"))
                        return;

                    if ("open" === ui.action)
                    {
                        event.preventDefault();
                        var options = {"direction": "top"};
                        oj.AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
                    } else if ("close" === ui.action)
                    {
                        event.preventDefault();
                        ui.endCallback();
                    }
                };

                // Router setup
                self.router = oj.Router.rootInstance;
                self.router.configure({
                    'PurchaseSummry': {label: 'Purchase Requisition', value: "PurchaseRequisition/PurchaseSummry", title: "El Sewedy - Purchas Search", isDefault: true},
                    'PurchaseOperations': {label: 'Purchase Requisition', value: "PurchaseRequisition/PurchaseOperations", title: "El Sewedy - Purchas Operations"}
                });
                oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();
                self.localeLogin = ko.observable();
                self.UsernameLabel = ko.observable();
                self.passwordLabel = ko.observable();
                self.loginword = ko.observable();
                self.forgotPasswordLabel = ko.observable();
                self.localeName = ko.observable();
                self.localeJop = ko.observable();
                self.localeempNO = ko.observable();
                self.localeDep = ko.observable();
                self.localeGrade = ko.observable();
                self.language = ko.observable();
                self.languagelable = ko.observable("عربي");
                var storage = window.localStorage;
                var preferedLanguage;

                self.langSelected = function (event) {
                    var valueObj = buildValueChange(event['detail']);
                    self.setPreferedLanguage(valueObj.value);
                };

                function buildValueChange(valueParam) {
                    var valueObj = {};
                    //valueObj.previousValue = valueParam.previousValue;
                    if (valueParam.previousValue) {
                        valueObj.value = valueParam.value;
                    } else {
                        valueObj.value = valueParam.previousValue;
                    }
                    return valueObj;
                }

                $(function () {
                    storage.setItem('setLang', 'english');
//                    storage.setItem('setLang', 'arabic');
                    preferedLanguage = storage.getItem('setLang');
                    self.language(preferedLanguage);
                    changeToArabic(preferedLanguage);
                });

                // Choose prefered language
                function selectLanguage() {
                    oj.Config.setLocale('english',
                            function () {
                                $('html').attr({'lang': 'en-us', 'dir': 'ltr'});
                                self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                self.loginword(oj.Translations.getTranslatedString('login'));
                                self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                                self.localeName(oj.Translations.getTranslatedString('Name'));
                                self.localeJop(oj.Translations.getTranslatedString('JOP'));
                                self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                                self.localeDep(oj.Translations.getTranslatedString('Dep'));
                                self.localeGrade(oj.Translations.getTranslatedString('Grade'));

                            }
                    );
                }

                self.setPreferedLanguage = function (lang) {
                    var selecedLanguage = lang;
                    storage.setItem('setLang', selecedLanguage);
                    storage.setItem('setTrue', true);
                    preferedLanguage = storage.getItem('setLang');
                    changeToArabic(preferedLanguage);
                };

                self.createMessage = function (type, summary) {
                    return {
                        severity: type,
                        summary: summary,
                        autoTimeout: 10000
                    };
                };

                function changeToArabic(preferedLanguage) {
                    if (preferedLanguage === 'arabic') {

                        oj.Config.setLocale('ar-sa',
                                function () {
                                    $('html').attr({'lang': 'ar-sa', 'dir': 'rtl'});
                                    self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                    self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                    self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                    self.loginword(oj.Translations.getTranslatedString('login'));
                                    self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                                    self.localeName(oj.Translations.getTranslatedString('Name'));
                                    self.localeJop(oj.Translations.getTranslatedString('JOP'));
                                    self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                                    self.localeDep(oj.Translations.getTranslatedString('Dep'));
                                    self.localeGrade(oj.Translations.getTranslatedString('Grade'));
                                }
                        );

                    } else if (preferedLanguage === 'english') {
                        selectLanguage();


                    }
                }
                $('.login-label').toggleClass('user-label-right');
                $('.input100').toggleClass('box-input-text');
                $('.wrap-input100').toggleClass('box-input-text');
                self.changeLanguage = function () {
                    if (self.language() === 'english')
                    {
                        self.language("arabic");
                        self.setPreferedLanguage(self.language());
                        self.languagelable("English");
                        $('.login-label').toggleClass('user-label-right');
                        $('.login-label').removeClass('user-label-left');
                        $('.input100').toggleClass('box-input-text');
                        $('.wrap-input100').toggleClass('box-input-text');
                    } else if (self.language() === 'arabic')
                    {
                        self.language("english");
                        self.setPreferedLanguage(self.language());
                        self.languagelable("عربي");
                        $('.login-label').toggleClass('user-label-left');
                        $('.login-label').removeClass('user-label-right');
                        $('.input100').removeClass('box-input-text');
                        $('.wrap-input100').removeClass('box-input-text');


                    }
                };

                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});
                self.keyword = ko.observableArray();

                self.searchWord = ko.observable();
                self.tags = [
                    {value: "Value 1", label: "Unit 1"},
                    {value: "Value 2", label: "Unit 2"},
                    {value: "Value 3", label: "Unit 3"},
                    {value: "Value 4", label: "Unit 4"},
                    {value: "Value 5", label: "Unit 5"},
                    {value: "Value 6", label: "Unit 6"},
                    {value: "Value 7", label: "Unit 7"}
                ];

                self.tagsDataProvider = new oj.ArrayDataProvider(self.tags, {keyAttributes: 'value'});

                self.search = function (event) {

                    var trigger = event.type;
                    var term;

                    if (trigger === "ojValueUpdated") {
                        term = event['detail']['value'];
                        trigger += " event";
                    } else {
                        term = document.getElementById("search").value;
                        trigger = "click on search button";
                    }
                };

                self.toolbarClassNames = ko.observableArray([]);

                self.toolbarClasses = ko.computed(function () {
                    return self.toolbarClassNames().join(" ");
                }, this);

                self.loadModule = function () {
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath})
                        ]);
                        masterPromise.then(
                                function (values) {
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                },
                                function (reason) {}
                        );
                    });
                };

                // Navigation setup
                var navData = [];
                self.navDataSource = new oj.ArrayTableDataSource(navData, {idAttribute: 'id'});

                // Drawer
                // Close offcanvas on medium and larger screens
                self.mdScreen.subscribe(function () {
                    oj.OffcanvasUtils.close(self.drawerParams);
                });
                self.drawerParams = {
                    displayMode: 'push',
                    selector: '#navDrawer',
                    content: '#pageContent'
                };
                // Called by navigation drawer toggle button and after selection of nav drawer item
                self.toggleDrawer = function () {
                    return oj.OffcanvasUtils.toggle(self.drawerParams);
                };
                // Add a close listener so we can move focus back to the toggle button when the drawer closes
                $("#navDrawer").on("ojclose", function () {
                    $('#drawerToggleButton').focus();
                });

                // Header
                // Application Name used in Branding Area
                self.appName = ko.observable("Nexus");
                // User Info used in Global Navigation area


                // Footer
                function footerLink(name, id, linkTarget) {
                    this.name = name;
                    this.linkId = id;
                    this.linkTarget = linkTarget;
                }
                self.footerLinks = ko.observableArray([
                ]);
            }

            $(window).scroll(function () {

                if ($(this).scrollTop() > 130) {
                    $('header').addClass("purple");
                    $('header').addClass("scrollnav");
                    $('header').removeClass("forwhite");
                } else {
                    $('header').removeClass("purple");
                    $('header').addClass("forwhite");
                    $('header').removeClass("scrollnav");
                }
            });

            return new ControllerViewModel();
        }
);
