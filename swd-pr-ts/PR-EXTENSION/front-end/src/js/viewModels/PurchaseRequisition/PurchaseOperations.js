define(['ojs/ojcore', 'knockout', 'appController', 'config/services',
    'util/commonhelper', 'ojs/ojvalidation-base', 'ojs/ojtimezonedata', 'ojs/ojvalidationgroup',
    'ojs/ojlabel', 'ojs/ojlabel', 'ojs/ojdatetimepicker'],
        function (oj, ko, app, services, commonhelper, ValidationBase) {
            /**
             * The view model for the main content view template
             */
            function PurchaseOperationsContentViewModel() {
                var self = this;
                self.opLablel = ko.observable();
                self.backLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.isvisable = ko.observable(true);
                self.isRequired = ko.observable(true);
                self.isDisabled = ko.observable(false);
                self.groupValid = ko.observable();
                self.BomLnum = ko.observable();
                self.id = ko.observable();
                self.bomval = ko.observable();
                self.lineval = ko.observable();
                self.itemcodeval = ko.observable();
                self.prorderval = ko.observable();
                self.currentQtyVal = ko.observable();
                self.totalqtyval = ko.observable();
                self.uomval = ko.observable();
                self.itmCostval = ko.observable();
                self.needval = ko.observable();
                self.projnumVal = ko.observable();
                self.tasknumval = ko.observable();
                self.expenorgVal = ko.observable();
                self.expentypeVal = ko.observable();
                self.expendateVal = ko.observable();
                self.LineDesc = ko.observable();
                self.Quantity = ko.observable();
                self.unitofmeasure = ko.observable();
                self.itemCode = ko.observable();
                self.PrOrder = ko.observable();

                self.totalQty = ko.observable();
                self.UOM = ko.observable();
                self.itmCost = ko.observable();
                self.NeedBy = ko.observable();
                self.ProjNum = ko.observable();
                self.TaskNum = ko.observable();
                self.ExpenOrgan = ko.observable();
                self.ExpenType = ko.observable();
                self.ExpenDate = ko.observable();
                self.pagetype = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;



                self.connected = function () {
                    var psobj = oj.Router.rootInstance.retrieve();


                    if (psobj.type === 'update') {
                      
                        self.pagetype(psobj.type);
                       
                        self.id(psobj[0].id);
                        self.lineval(psobj[0].lineDescription);
                        self.currentQtyVal(psobj[0].currentQuantity);
                        self.needval(psobj[0].needByDate);
                        self.bomval(psobj[0].bomLineNumber);

                        self.itemcodeval(psobj[0].itemCode);
                        self.prorderval(psobj[0].previousOrdered);

                        self.totalqtyval(psobj[0].totalQuantity);
                        self.uomval(psobj[0].uom);
                        self.itmCostval(psobj[0].itemCost);

                        self.projnumVal(psobj[0].projectNumber);
                        self.tasknumval(psobj[0].taskNumber);
                        self.expenorgVal(psobj[0].expenditureOrganiz);
                        self.expentypeVal(psobj[0].expenditureType);
                        self.expendateVal(psobj[0].expenditureDate);




                    } else if (psobj.type === 'View') {
                        self.isvisable(false);
                        self.isDisabled(true);
                        self.isRequired(true);
//                        self.pagetype(psobj.type);
//                        self.id(psobj.id);
//                        self.bomval(psobj.bomLineNumber);
//                        self.lineval(psobj.lineDescription);
//                        self.itemcodeval(psobj.itemCode);
//                        self.prorderval(psobj.previousOrdered);
//                        self.currentQtyVal(psobj.currentQuantity);
//                        self.totalqtyval(psobj.totalQuantity);
//                        self.uomval(psobj.uom);
//                        self.itmCostval(psobj.itemCost);
//                        self.needval(psobj.needByDate);
//                        self.projnumVal(psobj.projectNumber);
//                        self.tasknumval(psobj.taskNumber);
//                        self.expenorgVal(psobj.expenditureOrganiz);
//                        self.expentypeVal(psobj.expenditureType);
//                        self.expendateVal(psobj.expenditureDate);
                    }


                };



                self.BackStep = function () {
                    oj.Router.rootInstance.go('PurchaseSummry');

                };
                self.submitButton = function () {

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }


                    if (self.pagetype() == 'update') {
                      
                        var payload2 = {
                            "id": self.id(),
                            "expenditureDate": self.expendateVal(),
                            "totalQuantity": self.totalqtyval(),
                            "expenditureOrganiz": self.expenorgVal(),
                            "previousOrdered": self.prorderval(),
                            "needByDate": self.needval(),
                            "bomLineNumber": self.bomval(),
                            "itemCost": self.itmCostval(),
                            "uom": self.uomval(),
                            "currentQuantity": self.currentQtyVal(),
                            "taskNumber": self.tasknumval(),
                            "projectNumber": self.projnumVal(),
                            "expenditureType": self.expentypeVal(),
                            "lineDescription": self.lineval(),
                            "itemCode": self.itemcodeval()

                        };
                        var getValidRequestupdate = function (data) {

                            oj.Router.rootInstance.go('PurchaseSummry');

                        };
                        services.addGeneric(commonhelper.updaterequest, payload2).then(getValidRequestupdate, app.failCbFn);

                    } else if (self.pagetype() == 'View') {


                    }

                };



                initTranslations();

                function initTranslations() {

//            self.cancel(getTranslation("Purchase.cancel"));
                    self.opLablel(getTranslation("Purchase.opLablel"));
                    self.backLbl(getTranslation("Purchase.backLbl"));
                    self.submitLbl(getTranslation("Purchase.submitLbl"));
                    self.BomLnum(getTranslation("Purchase.BomLnum"));
                    self.LineDesc(getTranslation("Purchase.LineDesc"));
                    self.unitofmeasure(getTranslation("Purchase.unitofmeasure"));
                    self.itemCode(getTranslation("Purchase.itemCode"));
                    self.PrOrder(getTranslation("Purchase.PrOrder"));
                    self.Quantity(getTranslation("Purchase.Quantity"));
                    self.totalQty(getTranslation("Purchase.totalQty"));
                    self.UOM(getTranslation("Purchase.UOM"));
                    self.itmCost(getTranslation("Purchase.itmCost"));
                    self.NeedBy(getTranslation("Purchase.NeedBy"));
                    self.ProjNum(getTranslation("Purchase.ProjNum"));
                    self.TaskNum(getTranslation("Purchase.TaskNum"));
                    self.ExpenOrgan(getTranslation("Purchase.ExpenOrgan"));
                    self.ExpenType(getTranslation("Purchase.ExpenType"));
                    self.ExpenDate(getTranslation("Purchase.ExpenDate"));

                }
            }

            return PurchaseOperationsContentViewModel;
        });
