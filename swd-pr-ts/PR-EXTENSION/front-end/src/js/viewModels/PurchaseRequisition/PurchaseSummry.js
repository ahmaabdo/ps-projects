
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojformlayout', 'ojs/ojlabel', 'ojs/ojlabel', 'ojs/ojdatetimepicker', 'ojs/ojbutton',
    'ojs/ojtable', 'ojs/ojcheckboxset', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojoffcanvas', 'xlsx','ojs/ojpagingtabledatasource',
    'ojs/ojpagingcontrol'],
        function (oj, ko, $, app, services, commonhelper) {
            /**
             * The view model for the main content view template
             */
            function PurchaseSummryContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.HLablel = ko.observable();
                self.projectDetailsLbl = ko.observable();
                self.taskDetailsLbl = ko.observable();
                self.ExpenditureType = ko.observable();
                self.data = ko.observable();
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(false);
                self.isDisabled = ko.observable(true);
                self.isDisabledEdit = ko.observable(true);
                self.isDisabledGenerate = ko.observable(true);
                self.taskNumDisable = ko.observable(false);
                self.placeholder = ko.observable();
                self.searchDisable = ko.observable(false);
                self.allToGenDisable = ko.observable(true);
                self.subInvDisble = ko.observable(false);
          
                self.divVisable=ko.observable(false);
                self.searchLbl = ko.observable();
                self.clearPrLbl = ko.observable();
                self.EditLbl = ko.observable();
                self.AddToGenelLbl = ko.observable();
                self.ViewLbl = ko.observable();
                self.CancelLbl = ko.observable();
                self.CancelLineLbl = ko.observable();
                self.DisplayLbl = ko.observable();
                self.GenePrLbl = ko.observable();
                self.resourceClassLbl = ko.observable();
                self.columnArray = ko.observableArray();
                self.generateColumnArray = ko.observableArray();
                self.DetailsColumnArray = ko.observableArray();
                self.BomLnum = ko.observable();
                self.projectStartDate = ko.observable();
                self.projectEndDate = ko.observable();
                self.projectManager = ko.observable();
                self.projectStauts = ko.observable();
                self.projName = ko.observable();
                self.LineDesc = ko.observable();
                self.itemCode = ko.observable();
                self.PrOrder = ko.observable();
                self.CurrrentQty = ko.observable(0);
                self.totalQty = ko.observable();
                self.UOM = ko.observable();
                self.itmCost = ko.observable();
                self.NeedBy = ko.observable();
                self.ProjNum = ko.observable();
                self.TaskNum = ko.observable();
                self.TaskName = ko.observable();
                self.ExpenOrgan = ko.observable();
                self.ExpenType = ko.observable();
                self.ExpenDate = ko.observable();
                self.ExpenStartDate = ko.observable();
                self.ExpenEndDate = ko.observable();
                self.chck = ko.observable(false);
                self.objdata = ko.observableArray();
                self.detailsData = ko.observableArray();
                self.lineData = ko.observableArray([]);
                self.generateArr = ko.observableArray();
                self.generateArrTo = ko.observableArray();
                self.generateData = ko.observableArray();
                self.selectedIndex = ko.observable();
                self.id = ko.observable('ID');
                self.tasknumval = ko.observable();
                self.expenorgVal = ko.observable();
                self.projnumVal = ko.observable();
                self.exportLbl = ko.observable();
                self.arr = ko.observableArray([]);
                self.projectNumber = ko.observable();
                self.tempArr = ko.observableArray(self.objdata());
                self.tempArr2 = ko.observableArray();
                self.linDataprovider = ko.observable();
                self.linDataprovider(new oj.ArrayTableDataSource(self.lineData, {keyAttributes: 'id'}));
               // self.dataprovider = new oj.ArrayDataProvider(self.objdata, {keyAttributes: 'id'});
                self.dataprovider = ko.observable();
                self.dataprovider = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.objdata));
                self.detailsDataprovider = new oj.ArrayDataProvider(self.detailsData, {keyAttributes: 'id'});
                self.generateDataprovider = new oj.ArrayTableDataSource(self.generateData, {keyAttributes: 'id'});
                self.taskNumber = ko.observable();
                self.expOrg = ko.observableArray();
                self.taskName = ko.observableArray();
                self.ProjectId = ko.observableArray();
                self.projectIds = ko.observableArray();
                self.items = ko.observableArray();
                self.expOrganiz = ko.observable();
                self.Qty = ko.observable();
                self.Expendarr = ko.observableArray();
                self.headerTextReAssignDailogLb2 = ko.observable();
                self.selectedItems = ko.observable([]);
                self.selectedItemsArr = ko.observableArray();
                self.dataprovider2 = ko.observable();
                self.viewDisable = ko.observable(true);
                self.UOMAtrr = ko.observableArray();
                self.generatePr = ko.observable();
                self.deptObservableArray = ko.observableArray();
                self.currProjName = ko.observable();
                self.lengthh = ko.observable();
                self.linkes = ko.observable();
                self.lineColumnArray = ko.observableArray();
                self.details = ko.observable();
                self.projectManagerVal = ko.observable();
                self.projectSatuesVal = ko.observable();
                self.projectNameVal = ko.observable();
                self.tasknumVal = ko.observable();
                self.taskNameVal = ko.observable();
                self.quantityval = ko.observable();
                self.needByVal = ko.observable();
                self.organizationVal = ko.observable();
                self.ExpenTypeVal = ko.observable();
                self.tempArr3 = ko.observableArray();
                self.arrOfAllLines = ko.observableArray();
                self.countnerData = ko.observableArray();
                self.lengthOfGenerateArr = ko.observable();
                self.reqArrMatsr = ko.observableArray();
                self.reqArrDetails = ko.observableArray();
                self.headerTextLineDailogLb = ko.observable();
                self.bodyTextLineDailogLb = ko.observable();
                self.headerTextGenerateDailogLb = ko.observable();
                self.headerTextQuanDailogLb = ko.observable();
                self.currentQty = ko.observable(1);
                self.CurrentQty = ko.observable(1);
                self.addQtyLbl = ko.observable();
                self.destinationTypeVal = ko.observable();
                self.destinationSubTypeVal = ko.observable();
                self.projectNameLbl = ko.observable();
                self.projectNumberLbl = ko.observable();
                self.taskNameLbl = ko.observable();
                self.taskNumberLbl = ko.observable();
                self.projectManagerLbl = ko.observable();
                self.projectStatusLbl = ko.observable();
                self.expenDateFromLbl = ko.observable();
                self.expenDateToLbl = ko.observable();
                self.expenTypeLbl = ko.observable();
                self.expenOrganizationLbl = ko.observable();
                self.destinationTypeLbl = ko.observable();
                self.projectOrganizationLbl = ko.observable();
                self.expenditureOrganizationLbl = ko.observable();
                self.expenditureOrganization = ko.observable();
                self.needByDateLbl = ko.observable();
                self.qtyLbl = ko.observable();
                self.reMaster = ko.observableArray();
                self.reDeatils = ko.observableArray();
                self.resetLbl = ko.observable();
                self.expenDateFromVal = ko.observable();
                self.expenDateToVal = ko.observable();
                self.ProjectNumberArr = ko.observableArray();
                self.ProjectManagerNameArr = ko.observableArray();
                self.projectNameArr = ko.observableArray();
                self.invOrgNameArr = ko.observableArray();
                self.projectStatusArr = ko.observableArray();
                self.ProjectNameData = new oj.ArrayDataProvider(self.projectNameArr, {keyAttributes: 'value'});
                 self.organizatioInvDataProvider = new oj.ArrayDataProvider(self.invOrgNameArr, {keyAttributes: 'value'});
                self.ProjectNumbeData = new oj.ArrayDataProvider(self.ProjectNumberArr, {keyAttributes: 'value'});
                self.ProjectManagerData = new oj.ArrayDataProvider(self.ProjectManagerNameArr, {keyAttributes: 'value'});
                self.ProjectStatuesData = new oj.ArrayDataProvider(self.projectStatusArr, {keyAttributes: 'value'});
                self.taskNameArr = ko.observableArray();
                self.taskNumArr = ko.observableArray([]);
                self.expenTypeArr = ko.observableArray();
                self.projectOrganizArr = ko.observableArray();
                self.taskNumberArr = ko.observableArray();
                self.taskNumArr2 = ko.observableArray();
                self.resClassArr = ko.observableArray();
                self.taskNameData = new oj.ArrayDataProvider(self.taskNameArr, {keyAttributes: 'value'});
                self.taskNumberData = new oj.ArrayDataProvider(self.taskNumberArr, {keyAttributes: 'value'});
                self.projectOrganizData = new oj.ArrayDataProvider(self.projectOrganizArr, {keyAttributes: 'value'});
                self.resourceClassArr = new oj.ArrayDataProvider(self.resClassArr, {keyAttributes: 'value'});
                self.itemsData = ko.observableArray();
                self.ExpenDateTo = ko.observable();
                self.arrMaster = ko.observableArray();
                self.taskID = ko.observable();
                self.clearSelected = ko.observableArray();
                self.taskID = ko.observable();
                self.proArr = ko.observableArray();
                self.taskArr = ko.observableArray();
                self.resetDetailsArr = ko.observableArray();
                self.sele = ko.observableArray();
                self.generateArrToRe = ko.observableArray();
                self.counterData2 = ko.observableArray();
                self.taskNameListenerArr = ko.observableArray();
                self.localData = ko.observableArray();
                self.subInventoryArray = ko.observableArray();
                self.subInventoryArrayCollect = ko.observableArray();
                self.sum = ko.observableArray();
                self.projectId = ko.observable();
                self.ser = ko.observable();
                self.requisitionBu = ko.observable();
                self.requisitionBuId = ko.observable();
                self.subInvLbl = ko.observable();
                self.SubInvDestination = ko.observable('');
                self.destinationType=ko.observable();
                self.referenceBomLbl=ko.observable();
                self.referenceBOMval=ko.observable('');
                self.organizationInvArr =ko.observableArray();
                self.invOrgLbl=ko.observable();
                self.invOrganizationVal=ko.observable();
                    self.taskRowData=ko.observable();
                    self.taskAndLineDetailes=ko.observableArray();
                
                var index;
                self.connected = function () {
                    app.loading(false);
                    initTranslations();
                    self.getLocalData();
                    self.getProjectsDetails();
                    self.getResourcesClass();
                    self.getProjectOrangzations();
                    self.getSubInventoryDetils();
                     self.getOrganizationInv();
                };
                self.getProjectsDetails = function () {
                    var getSucc = function (data) {
                        $.each(data, function (index) {

                            self.projectNameArr().push(
                                    {
                                        label: data[index].projectName,
                                        value: data[index].projectName
                                    }
                            );
                            self.ProjectNumberArr().push(
                                    {
                                        label: data[index].projectNumber,
                                        value: data[index].projectNumber
                                    }
                            );
                            var projectMangerOptions = data.map(o => {
                                return {label: o.projectManagerName, value: o.projectManagerName};
                            });
                            $.each(projectMangerOptions, function (i, el) {
                                if (!self.ProjectManagerNameArr().find(item => item.label === el.label))
                                    self.ProjectManagerNameArr.push(el);
                            });
                            var projectStatusOptions = data.map(o => {
                                return {label: o.projectStatus, value: o.projectStatus};
                            });
                            $.each(projectStatusOptions, function (i, el) {
                                if (!self.projectStatusArr().find(item => item.label === el.label))
                                    self.projectStatusArr.push(el);
                            });
                        });
                        data = data.filter(o => o.projectStatus === "Approved" ||o.projectStatus === "Tender");
                        var x = 0;
                        data.forEach(e => e.seq = ++x);

                        self.objdata(data);
                        self.tempArr(data);
                        self.arrMaster(data);
                        $.each(data, function (index) {
                            self.reqArrMatsr().push({
                                ProjectName: data[index].projectName,
                                ProjectNumber: data[index].projectNumber,
                                ProjectStartDate: data[index].projectStartDate,
                                ProjectEndDate: data[index].projectEndDate,
                                ProjectManagerName: data[index].projectManagerName,
                                ProjectStatus: data[index].projectStatus
                            });
                        });
                        self.reMaster(self.reqArrMatsr());
                    };
                    services.getGeneric(commonhelper.getProjectDet).then(getSucc, app.failCbFn);
                };

                /* ---------------------------table selection listener for master table --------------------------- */

                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    if (!data)
                        return;
                    var currentRow = data.currentRow;
                    if (!currentRow)
                        return;
                    self.objdata();
                    var row = self.objdata()[currentRow['rowIndex']];
                    var arr = JSON.parse(row.links);
                    self.taskArr(arr);
                    self.currProjName(row.projectNumber);
                    self.requisitionBu(row.businessUnitName);
                    self.requisitionBuId(row.businessUnitId);
                    self.expenditureOrganization(row.owningOrganizationName);
                    var projectId = row.projectId.toString();
                    var versionPayload =
                            {
                                "projectId": projectId
                            };
                    var getPojectVer = function (data) {
                        self.projectId(data[0].planVersionId);
                    };
                    services.postGeneric(commonhelper.getProjectVersionDetUrl, versionPayload).then(getPojectVer, app.failCbFn);
                    var nameRest = "Tasks";
                    for (var i = 0; i < self.taskArr().length; i++)
                    {
                        var hreef = self.taskArr()[i].href;
                        var name = self.taskArr()[i].name;
                        if (name === nameRest)
                        {
                            var taskUrl = hreef;
                            var payload = {
                                "href": taskUrl
                            };
                            self.getSuccs = function (data2)
                            {
                                var x = data2.map(o => {
                                    return {taskname: o.taskName, taskNumber: o.taskNumber};
                                });
                                self.taskNameListenerArr(x);
                                for (var item in data2) {
                                    data2[item].seq = parseInt(item) + 1;
                                }

                                self.detailsData(data2);
                                self.itemsData(data2);
                                self.tempArr3(data2);
                                self.resetDetailsArr(data2);
                                self.taskNameArr([]);
                                var xx = [];
                                $.each(data2, function (index) {
                                    xx.push({
                                        label: data2[index].taskName,
                                        value: data2[index].taskName
                                    });
                                });
                                self.taskNameArr(xx);
                                self.taskNumArr([]);
                                $.each(data2, function (index) {
                                    self.taskNumArr.push({
                                        label: data2[index].taskNumber,
                                        value: data2[index].taskNumber
                                    });
                                });
                                self.taskNumberArr(self.taskNumArr());
                                self.expenTypeArr([]);
                                $.each(data2, function (index) {
                                    self.expenTypeArr.push({
                                        label: data2[index].ExpenditureType,
                                        value: data2[index].ExpenditureType
                                    });
                                });

                                self.reqArrDetails([]);
                                $.each(data2, function (index) {
                                    self.reqArrDetails.push({
                                        TaskName: data2[index].taskName,
                                        TaskNumber: data2[index].taskNumber,
                                        previousOrdered: data2[index].previousOrdered,
                                        NeedByDate: data2[index].needByDate,
                                        ExpenditureOrganization: data2[index].expenditureOrganization,
                                        ExpenditureType: data2[index].ExpenditureType,
                                        ExpenditureItemDate: data2[index].ExpenditureItemDate

                                    });
                                });
                                self.reDeatils(self.reqArrDetails());
                            };
                            services.postGeneric(commonhelper.getTaskDet, payload).then(self.getSuccs, app.failCbFn);
                        }
                    }
                };
                /* ---------------------------table selection listener for details table --------------------------- */

                $("body").delegate("#DetailsTable .oj-table-body-row", "click", (e) => {
                    var index = $(e.currentTarget).find(".hiddenkeyHolder").val();
                    var row = self.detailsData()[index];
                    if (!row)
                    {
                         self.taskRowData([])
                        var row = self.reqArrDetails()[index];
                        self.taskID(row.taskId);
                       
                    }
                    self.taskID(row.taskId);
                     self.taskRowData(row);
                    app.loading(true);
                    self.countnerData([]);
                    self.getLocalData();
                    self.getResourcesTask();
                    self.currentQty(0);
                    
                    document.querySelector("#ViewTaskDialog").open();
                });
                self.tableSelectionListener3 = function (event)
                {
                    var data = event.detail;
                    if (!data)
                    {
                        return;
                    }
                    var currentRow = data.currentRow;
                    if (!currentRow)
                    {
                        return;
                    }
                    index = [currentRow['rowIndex']];
                };
                self.searchBtnAction = function ()
                {

                    self.detailsData([]);
                    self.objdata([]);
                    if (!self.projnumVal() && !self.projectManagerVal() && !self.organizationVal()
                            && !self.projectNameVal()) {
                        self.objdata(self.tempArr());
                        self.reMaster();
                        self.reqArrMatsr(self.reMaster());
                        return;
                    }
                    var arrSearch = self.tempArr().filter(e =>
                        (self.projnumVal() ? e.projectNumber === self.projnumVal() : true)
                                && (self.projectManagerVal() ? e.projectManagerName === self.projectManagerVal() : true)
                                && (self.organizationVal() ? e.businessUnitName === self.organizationVal() : true)
                                &&(self.projectNameVal() ? e.projectName === self.projectNameVal() : true)
                    );
                    var x = 0;
                    arrSearch.forEach(e => e.seq = ++x);
                    var arrCoulmnFilter = [];
                    $.each(arrSearch, function (index) {
                        arrCoulmnFilter.push({
                            ProjectName: arrSearch[index].projectName,
                            ProjectNumber: arrSearch[index].projectNumber,
                            ProjectStartDate: arrSearch[index].projectStartDate,
                            ProjectEndDate: arrSearch[index].projectEndDate,
                            ProjectManagerName: arrSearch[index].projectManagerName,
                            ProjectStatus: arrSearch[index].projectStatus

                        });
                    });
                    self.objdata(arrSearch);
                    self.reqArrMatsr(arrCoulmnFilter);
                };
                /* --------------------------- button search in details fields --------------------------- */

                self.searchBtnAction2 = function () {
                    self.detailsData([]);
                    if (!self.tasknumVal() && !self.taskNameVal() && !self.ExpenTypeVal() && !self.needByVal()
                            && !self.quantityval() && !self.expenDateFromVal() && !self.expenDateToVal()) {
                        app.messagesDataProvider.push(app.createMessage('warning', 'Select project to search !!!'));
                        //   self.detailsData(self.resetDetailsArr());
                        return;
                    }

                    self.detailsData([]);
                    var arrFiltered = self.tempArr3().filter(e =>
                        (self.tasknumVal() ? e.taskNumber === self.tasknumVal() : true)
                                && (self.taskNameVal() ? e.taskName === self.taskNameVal() : true)
                                && (self.needByVal() ? e.needByDate === self.needByVal() : true)
                                && (self.expenDateFromVal() ? e.expenditureDateFrom === self.expenDateFromVal() : true)
                                && (self.expenDateToVal() ? e.expenditureDateTo === self.expenDateToVal() : true)
                                && (self.quantityval() ? e.Quantity === self.quantityval() : true)
                                && (self.ExpenTypeVal() ? e.ExpenditureType === self.ExpenTypeVal() : true)

                    );
                    var arrCoulmnFilter = [];
                    $.each(arrFiltered, function (index) {
                        arrCoulmnFilter.push({
                            taskName: arrFiltered[index].taskName,
                            taskNumber: arrFiltered[index].taskNumber,
                            previousOrdered: arrFiltered[index].previousOrdered,
                            Quantity: arrFiltered[index].Quantity,
                            NeedByDate: arrFiltered[index].NeedByDate,
                            expenditureOrganization: arrFiltered[index].expenditureOrganization,
                            expenditureType: arrFiltered[index].expenditureType,
                            expenditureStartDate: arrFiltered[index].expenditureDateFrom,
                            expenditureENDDate: arrFiltered[index].expenditureDateTo,
                            taskId: arrFiltered[index].taskId


                        });
                    });
                    self.detailsData(arrFiltered);
                    self.reqArrDetails(arrCoulmnFilter);
                    self.detailsData([]);
                };
                /* --------------------------- button for Reset MasterTabel Field  --------------------------- */
                self.resetBtnAction = function () {
                    self.reqArrMatsr(self.reMaster());
                    // self.getProjectsDetails();
                    self.detailsData([]);
                    self.projnumVal('');
                    self.projectManagerVal('');
                    self.organizationVal('');
                    self.projectNameVal('');
                    self.placeholder('Please Select');
                    self.objdata([]);
                    self.objdata(self.arrMaster());

                };
                /* --------------------------- button for Reset DetailTabel Field  --------------------------- */
                self.resetDetailBtnAction = function () {

                    self.detailsData([]);
                    self.tasknumVal('');
                    self.taskNameVal('');
                    self.ExpenTypeVal('');
                    self.needByVal('');
                    self.quantityval('');
                    self.expenDateFromVal('');
                    self.expenDateToVal('');
                    //self.taskNumArr([]);
                    self.taskNumberArr(self.taskNumArr());
                    self.taskNumDisable(false);
                    self.detailsData(self.resetDetailsArr());
                };
                /* --------------------------- button for view in operation screen --------------------------- */

                self.ViewBtnAction = function () {
                    if (self.chck() === true && self.selectedItemsArr().length >= 1) {


                        var obj = self.selectedItemsArr();
                        obj.type = "View";
                        document.querySelector("#ViewDialog").open();
                        oj.Router.rootInstance.store(obj);
                        //  oj.Router.rootInstance.go('PurchaseOperations');
                    } else {

                        app.messagesDataProvider.push(app.createMessage('error', 'Please Select CheckBox'));
                    }

                };
                /* --------------------------- function for index selected item   --------------------------- */

                self.getIndexInSelectedItems = function (id) {

                    for (var i = 0; i < self.selectedItems().length; i++)
                    {
                        var range = self.selectedItems()[i];
                        var startIndex = range.startIndex.row;
                        var endIndex = range.endIndex.row;
                        if (id >= startIndex && id <= endIndex)
                        {
                            return i;
                        }
                    }
                    return -1;
                };
                /* --------------------------- function for handle checkbox checked   --------------------------- */

                self.handleCheckbox = function (id) {
                    var isChecked = self.getIndexInSelectedItems(id) !== -1;
                    return isChecked ? ["checked"] : [];
                };
                /* --------------------------- function for select row and dispaly in dailog   --------------------------- */

                self.checkboxListener = function (event)
                {
                    if (event.detail !== null)
                    {
                        var value = event.detail.value;
                        var newSelectedItems;
                        var id = event.target.dataset.rowId;
                        // need to convert to Number to match the key type
                        //  var key = Number(event.target.dataset.rowKey);
                        if (value.length > 0) {
                            document.querySelector("#quantityDialog").open();
                            self.placeholder("Please Select")
                            newSelectedItems = self.getIndexInSelectedItems(id) === -1 ?
                                    self.selectedItems().concat([{startIndex: {row: id}, endIndex: {row: id}}]) :
                                    self.selectedItems();
                        } else {

                            newSelectedItems = self.selectedItems();
                            var idx = self.getIndexInSelectedItems(id);
                            if (idx !== -1)
                            {

                                var item = newSelectedItems.splice(idx, 1)[0];
                                // break up ranges into two separate ranges if index is in range;
                                if (item.startIndex.row !== item.endIndex.row)
                                {
                                    var newItems = [];
                                    if (id !== 0)
                                    {
                                        newItems.push({startIndex: {row: item.startIndex.row}, endIndex: {row: Number(id) - 1}});
                                    }
                                    if (id !== self.dataProvider.totalSize() - 1)
                                    {
                                        newItems.push({startIndex: {row: Number(id) + 1}, endIndex: {row: item.endIndex.row}});
                                    }
                                    newSelectedItems = newSelectedItems.concat(newItems);
                                }
                            }

                        }
                        self.selectedItems(newSelectedItems);
                    }
                    if (self.selectedItems().length > 0) {
                        self.isDisabled(false);
                        self.isDisabledEdit(false);
                        self.allToGenDisable(false);
                        // self.currentQty();

                    }
                    if (self.selectedItems().length === 0) {
                        self.isDisabled(true);
                        self.isDisabledEdit(true);
                        self.allToGenDisable(true);
                    }
                    if (self.selectedItems().length >= 2) {

                        self.isDisabledEdit(true);
                    }
                    self.printCurrentSelection();
                };
                /* --------------------------- function for print current selection   --------------------------- */

                self.printCurrentSelection = function ()
                {
                    self.selectedItemsArr([]);
                    var selectedItems = [];
                    var indexes = self.selectedItems().map(e => e.endIndex.row);
                    //var indexes = self.selectedItems()[i].endIndex.row;
                    for (var i = 0; i < indexes.length; i++) {

                        selectedIndex = indexes[i];
                        if (self.generateData().length == 0)
                        {
                            var x = 0;
                            selectedItems.push(self.lineData()[selectedIndex]);
                            selectedItems.forEach(e => {
                                e.ser = ++x;
                            });
                            self.selectedItemsArr(selectedItems);
                            self.ser(selectedItems[i].ser);
                        } else
                        {
                            x = self.ser();
                            selectedItems.push(self.lineData()[selectedIndex]);
                            selectedItems.forEach(e => {
                                e.ser = ++x;
                            });
                            self.ser(selectedItems[i].ser);
                            for (var i = 0; i < self.generateData().length; i++) {
                                var o = self.generateData().find(e => (e.name == selectedItems[i].name)
                                            && (e.TaskNumber == selectedItems[i].TaskNumber));
                                if (o)
                                {
                                    app.messagesDataProvider.push(app.createMessage('error', 'Item line already added in Purchase Requsition list'));
                                    self.selectedItems().length = 0;
                                    self.selectedItemsArr([]);
                                    if (self.selectedItems().length === 0) {
                                        self.allToGenDisable(true);
                                        self.selectedItemsArr([]);
                                    }

                                } else {
                                    self.selectedItemsArr(selectedItems);
                                }
                            }
                        }
                    }
                };
                /* --------------------------- project name change listener   --------------------------- */

                self.projectNameChangeListener = function (event)
                {
                    self.projectNumber(event['detail'].value);
                };
                /* --------------------------- button for export excel data selected from master table --------------------------- */

                self.exportBtnAction = function () {
                    /* make the worksheet */

                    var ws = XLSX.utils.json_to_sheet(self.reqArrMatsr());
                    /* add to workbook */
                    var wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, "People");
                    /* generate an XLSX file */
                    XLSX.writeFile(wb, "PRequest.xlsx");
                };
                /* --------------------------- button for export excel data selected from details table --------------------------- */

                self.exportBtnDetailsAction = function () {
                    self.lineData();
                    self.taskRowData();
                    self.taskAndLineDetailes().push({
                        TaskName: self.taskRowData().taskName,
                        TaskNumber: self.taskRowData().taskNumber,
                        ExpendetureOrganization: self.taskRowData().expenditureOrganization,
                        ExpendetureDateFrom: self.taskRowData().expenditureDateFrom,
                        ExpendetureDateTo: self.taskRowData().expenditureDateTo,

                    });
                    $.each(self.lineData(), function (index) {
                        self.taskAndLineDetailes().push({
                            itemCode: self.lineData()[index].name,
                            LineDescription: self.lineData()[index].assignmentDescription ? self.lineData()[index].assignmentDescription : null,
                            itemCost: self.lineData()[index].tc_std_raw_cost,
                            PreviousOrdered: self.lineData()[index].previousOrdered,
                            Quantity: self.lineData()[index].CurrentQty,
                            TotalQuantity: self.lineData()[index].quantity,
                            UOM: self.lineData()[index].unitOfMeasure,
                            DestinationType: self.lineData()[index].destinationType,
                        });
                    }); 
                    var ws = XLSX.utils.json_to_sheet(self.taskAndLineDetailes());
                    /* add to workbook */
                    var wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, "People");
                    /* generate an XLSX file */
                    XLSX.writeFile(wb, "PRequest.xlsx");
                };
                /* --------------------------- Task name change listener   --------------------------- */

                self.taskNameChangeListener = function (event)
                {

                    self.tasknumVal(event['detail'].value);
                    var tasks = self.taskNameListenerArr().filter(o => o.taskname === self.tasknumVal());
                    self.taskNumArr2(tasks.map(t => {
                        return {label: t.taskNumber, value: t.taskNumber};
                    }));
                    setTimeout(function () {
                        if (tasks.length > 0)
                        {
                            self.tasknumVal(self.taskNumArr2()[0].value);
                            self.taskNumDisable(true);
                        }
                    }, 50);
                };
                /* --------------------------- button for dispaly in dialog  --------------------------- */
                self.DisplayBtnAction = function () {

                };
                /* --------------------------- button for generate pr request --------------------------- */

                self.destinationValueChanged = function (event) {             
                     if (event.detail.value == "INVENTORY") {
                        self.destinationType(event.detail.value);

                         self.divVisable(true)
                    }else if(event.detail.value == "EXPENSE"){
                     self.divVisable(false)
                      self.destinationSubTypeVal('')
                        
                    }
                }
                self.subValueChanged = function(event){
                    
                    self.SubInvDestination(event.detail.value)
               //  self.subInvDisble(true);                  
                }
                self.GenePrBtnAction = function ()
                {
                    var x = [... new Set(self.generateData().map(el => el.bussinesUnit))]
                    if (x.length > 1)
                    {
                        app.messagesDataProvider.push(app.createMessage('error', 'Can\'t generate pr in different business unit'));
                        self.generateData([]);
                        self.isDisabledGenerate(true);
                        document.querySelector("#ViewGenerateDialog").close();
                        return;
                    }
                    app.loading(true);
                    var payload = {
                        "interfaceSourceCode": "ORA_FUSION_DOO",
                        "requisitioningBUId": self.requisitionBuId(),
                        "requisitioningBUName": self.requisitionBu(),
                        "documentStatusCode": "INCOMPLETE",
                        "interfaceHeaderKey": "300000001832688",
                        "preparerId": "300000001461423",
                        "RequisitioningBUId": self.requisitionBuId(),
                        "RequisitioningBUName": self.requisitionBu(),
                        "externallyManagedFlag": "false"
                    };
                    var lineItemsArr = [];
                                          
                    for (var i = 0; i < self.generateData().length; i++) {
                        if (self.requisitionBu() == "3WN_FZCO_BU" || self.requisitionBu()== "3W Networks_BU")
                        {
                            //assignmentDescription
                            lineItemsArr.push({
                                "deliverToLocationId": "300000001349828",
                                "deliverToOrganizationId": self.requisitionBu() == "3WN_FZCO_BU"?"300000002159300":"300000004109632",
                                "destinationTypeCode": self.generateData()[i].destinationType,
                                "DestinationSubinventory": self.generateData()[i].subinventory,
                                "itemId": self.generateData()[i].itemId,
                                "price": self.generateData()[i].tc_std_raw_cost,
                                "quantity": self.generateData()[i].CurrentQty,
                                "projectNumber": self.generateData()[i].destinationType =="EXPENSE"?self.currProjName():null,
                                "taskId": self.generateData()[i].destinationType =="EXPENSE"?self.generateData()[i].TaskNumber:null,
                                "unitOfMeasure": self.generateData()[i].unitOfMeasure,
                                "itemDescription": self.generateData()[i].name,
                                "itemNumber": self.generateData()[i].name,
                                "projectOrganizationName":self.generateData()[i].destinationType =="EXPENSE"? self.expenditureOrganization():null,
                                "projectExpendTypeName": self.generateData()[i].destinationType =="EXPENSE"?"Material":null,
                                "referenceBomNumber":self.generateData()[i].referenceBOM,
                                "currencyCode": "AED",
                                "CategoryName": "CCTV",
                                "lineType": "Goods",
                                "percent": "100",
                                "segment1": "FZC061",
                                "segment2": "000000",
                                "segment3": "11111000",
                                "segment4": "000000",
                                "segment5": "0000000",
                                "segment6": "000000",
                                "segment7": "000000",
                                "segment8": "000000",
                                "segment9": "000000",
                                "segment10": "000000"
                            });
                        } else
                        {
                            lineItemsArr.push({
                                "deliverToLocationId": "300000001349828",
                                "destinationTypeCode": self.generateData()[i].destinationType,
                                "deliverToOrganizationId": "300000002159300",
                                "DestinationSubinventory": self.generateData()[i].subinventory,
                                "itemId": self.generateData()[i].itemId,
                                "price": self.generateData()[i].tc_std_raw_cost,
                                "quantity": self.generateData()[i].CurrentQty,
                                "projectNumber": self.generateData()[i].destinationType =="EXPENSE"?self.currProjName():null,
                               "taskId": self.generateData()[i].destinationType =="EXPENSE"?self.generateData()[i].TaskNumber:null,
                                "unitOfMeasure": self.generateData()[i].unitOfMeasure,
                                "itemDescription": self.generateData()[i].assignmentDescription,
                                "itemNumber": self.generateData()[i].name,
                                "projectOrganizationName":self.generateData()[i].destinationType =="EXPENSE"? self.expenditureOrganization():null,
                                "projectExpendTypeName": self.generateData()[i].destinationType =="EXPENSE"?"Material":null,
                                 "referenceBomNumber":self.generateData()[i].referenceBOM,
                                "lineType": "Goods",
                                "currencyCode": "AED",
                                "CategoryName": "CCTV",
                                "percent": "100",
                                "segment1": "EGY062",
                                "segment2": "000000",
                                "segment3": "522101000000",
                                "segment4": "000000",
                                "segment5": "0000000",
                                "segment6": "000000",
                                "segment7": "000000",
                                "segment8": "000000",
                                "segment9": "000000",
                                "segment10": "000000"
                            });
                        }
                    }
                    payload.lineItems = lineItemsArr;

                    var getSucc = function (data)
                    {
                        console.log(data)
                        self.insertItemAfterGeneratedPr();
                        app.loading(false);
                        self.generateData([]);
                        self.SubInvDestination('')
                        self.isDisabledGenerate(true);
                        document.querySelector("#ViewGenerateDialog").close();
                        app.messagesDataProvider.push(app.createMessage('confirmation', `Purchase Requsition is Generated With Number ${data.prNumber}`));
                    };

                    var getFailReq = function (data)
                    {
                        app.loading(false);
                        document.querySelector("#ViewGenerateDialog").close();
                        self.generateData([]);
                        self.isDisabledGenerate(true);
                        var msg = data.responseJSON.errorMessage;
                        app.messagesDataProvider.push(app.createMessage('error', 'Purchase Requsition Not Generated - \n\n' + msg));
                    };
                    services.postSaasPrGeneric(commonhelper.createPr, payload).then(getSucc, getFailReq);
                };

                /* --------------------------- function insert item to local database after generated --------------------------- */

                self.insertItemAfterGeneratedPr = function ()
                {
                    if (self.localData().length === 0)
                    {
                        var payload3 = [];
                        for (var i = 0; i < self.generateData().length; i++) {
                            var payload2 = {
                                "itemCost": self.generateData()[i].tc_std_raw_cost,
                                "totalQuantity": self.generateData()[i].quantity,
                                "uom": self.generateData()[i].unitOfMeasure,
                                "currentQuantity": self.generateData()[i].CurrentQty,
                                "taskNumber": self.generateData()[i].TaskNumber,
                                "projectNumber": self.currProjName(),
                                "previousOrdered": " ",
                                "itemCode": self.generateData()[i].name,
                                "lineDescription": self.generateData()[i].assignmentDescription
                            };
                            payload3.push(payload2);
                            var getSuccss = function (data) {
                                app.loading(false);
                            };
                            var getFailReque = function () {
                                app.loading(false);
                            };
                        }
                        services.postSaasPrGeneric(commonhelper.insertLocal, payload3).then(getSuccss, getFailReque);
                    } else
                    {
                        var foundUpdate, foundInsert = false;
                        var payloadArrUpdate = [];
                        var payloadArrInsert = [];

                        for (var i = 0; i < self.generateData().length; i++)
                        {
                            var found = self.localData().find(e => (e.itemCode === self.generateData()[i].name) && (e.taskNumber == self.generateData()[i].TaskNumber))
                            if (found)
                            {
                                self.sum(parseInt(self.generateData()[i].CurrentQty) + parseInt(found.currentQuantity));
                                var payload21 = {
                                    "id": found.id,
                                    "currentQuantity": self.sum().toString()
                                };
                                payloadArrUpdate.push(payload21);
                                foundUpdate = true;
                            } else
                            {
                                var payload2 = {
                                    "itemCost": self.generateData()[i].tc_std_raw_cost.toString(),
                                    "totalQuantity": self.generateData()[i].quantity.toString(),
                                    "uom": self.generateData()[i].unitOfMeasure,
                                    "currentQuantity": self.generateData()[i].CurrentQty.toString(),
                                    "taskNumber": self.generateData()[i].TaskNumber.toString(),
                                    "projectNumber": self.currProjName(),
                                    "previousOrdered": " ",
                                    "itemCode": self.generateData()[i].name,
                                    "lineDescription": self.generateData()[i].assignmentDescription
                                };
                                payloadArrInsert.push(payload2);
                                foundInsert = true;
                            }
                        }
                        if (foundUpdate)
                        {
                            var getSuccss = function (data) {
                            };
                            var getFailReque = function () {
                            };
                            services.postSaasPrGeneric(commonhelper.updateLocalData, payloadArrUpdate).then(getSuccss, getFailReque);
                        }
                        if (foundInsert)
                        {
                            var getSuccss = function (data) {
                            };
                            var getFailReque = function () {
                            };
                            services.postSaasPrGeneric(commonhelper.insertLocal, payloadArrInsert).then(getSuccss, getFailReque);
                        }
                    }
                };
                /* --------------------------- function btn for clear line in dialog --------------------------- */

                self.clearPrBtnAction = function ()
                {
                    self.generateData([]);
                    document.querySelector("#ViewGenerateDialog").close();
                    self.isDisabledGenerate(true);
                };
                /* --------------------------- function btn for edit line in dialog --------------------------- */

                self.EditLineBtnAction = function ()
                {

                };
                /* --------------------------- function btn to select items to generate pr --------------------------- */

                self.AllToGeneBtnAction = function ()
                {

                    self.isDisabledGenerate(false);
                    var xxx;
                    for (var i = 0; i < self.selectedItemsArr().length; i++)
                    {

                        if (self.selectedItemsArr()[i].CurrentQty != 0)
                        {
                            self.generateData().push(...self.selectedItemsArr());
                            xxx = self.generateData();
                            self.generateData([]);
                            setTimeout(function () {
                                self.generateData(xxx);
                            }, 1000);
                        }
                    }

                    self.lengthOfGenerateArr(self.generateData().length);
                    self.selectedItems([]);
                    var empty = false;
                    for (var i = 0; i < xxx.length; i++)
                    {
                        if (xxx[i].CurrentQty === '0')
                        {
                            empty = true;
                        }
                    }
                    if (empty)
                    {
                        app.messagesDataProvider.push(app.createMessage('error', 'You must enter quantity to generate purchase requsition '));
                    } else
                    {
                        document.querySelector("#ViewTaskDialog").close();
                    }

                };
                /* --------------------------- button for open dialog for generate selected item --------------------------- */

                self.generateBtnAction = function ()
                {
                    document.querySelector("#ViewGenerateDialog").open();
                };
                /* --------------------------- function btn to cancel line dialog --------------------------- */

                self.CancelLinBtnAction = function ()
                {

                    if (self.generateData() !== null)
                    {

                    } else
                    {
                        self.isDisabledGenerate(true);
                        self.lengthOfGenerateArr([]);
                    }

                    self.selectedItems([]);

                    document.querySelector("#ViewTaskDialog").close();
                };
                /* --------------------------- button for cancel in generate dialog  --------------------------- */

                self.CancelGeneBtnAction = function ()
                {

                    document.querySelector("#ViewGenerateDialog").close();
                };
                /* --------------------------- button for add quantity in dialog  --------------------------- */

                self.AddQtyBtnAction = function () {

                    if (self.lineData() && parseInt(self.currentQty()) +
                            parseInt(self.lineData()[index[0]].previousOrdered) > self.lineData()[index[0]].quantity) {
                        app.messagesDataProvider.push(app.createMessage('error', 'More than total Quantity'));
                        self.lineData()[index[0]].destinationType = "EXPENSE";
                        self.lineData()[index[0]].CurrentQty = 0;

                        self.linDataprovider(new oj.ArrayDataProvider(self.lineData, {keyAttributes: 'id'}));
                        document.querySelector("#quantityDialog").close();
                    } else if (typeof self.currentQty() != "number") {

                        app.messagesDataProvider.push(app.createMessage('warning', 'Enter a Number '));
                    } else if (self.currentQty() == 0)
                    {
                        self.selectedItems().length = 0;
                        if (self.selectedItems().length === 0) {
                            document.querySelector("#quantityDialog").close();
                            self.allToGenDisable(true);
                        }
                    } else {
                        self.lineData()[index[0]].CurrentQty = parseInt(self.currentQty());
                        self.lineData()[index[0]].destinationType = self.destinationTypeVal();
                        self.lineData()[index[0]].TaskNumber = self.taskID();
                        self.lineData()[index[0]].subinventory = self.destinationSubTypeVal();
                        self.lineData()[index[0]].referenceBOM = self.referenceBOMval();
                        self.linDataprovider(new oj.ArrayDataProvider(self.lineData, {keyAttributes: 'id'}));
                        self.currentQty(0);
                        document.querySelector("#quantityDialog").close();
                    }
                };
                /* --------------------------- get all project details --------------------------- */
                self.invOrgValueChanged = function (event) {
                    self.subInventoryArray([])
                    var filterData = self.subInventoryArrayCollect().filter(e => e.organizationId == event.detail.value)
                    self.subInventoryArray.push(...filterData)
                }
                self.getProjectOrangzations = async function () {

                    var getProOrangzations = function (data) {

                        $.each(data, function (index) {
                            self.projectOrganizArr.push({
                                label: data[index].BU_NAME,
                                value: data[index].BU_NAME
                            });
                        });
                    };
                    await services.getGenericReport({"reportName": "BUSINESS_UNIT_REPORT"}).then(getProOrangzations, app.failCbFn);
                };
                self.getSubInventoryDetils =  function () {

                    var sucessFunc = function (data) {
                      
                        $.each(data, function (index) {
                          self.subInventoryArrayCollect.push({
                                label: data[index].SECONDARY_INVENTORY_NAME,
                                value: data[index].SECONDARY_INVENTORY_NAME,
                                organizationId:data[index].ORGANIZATION_ID
                            });
                        });
                    };
                     services.getGenericReport({"reportName": "SUB_INVENTORY_DESC"}).then(sucessFunc, app.failCbFn);
                };
                self.getOrganizationInv=function(){
                    var getOrganizationSucess = function(data){
                        $.each(data, function (index) {
                            self.invOrgNameArr.push({
                                label: data[index].organizationName,
                                value: data[index].organizationId
                            });
                        });
                    }
                   services.getGeneric(commonhelper.organizatioURL, {}).then(getOrganizationSucess, app.failCbFn);
                }
                
                
                /* --------------------------- REPORT TO GET LOV Resource Class   --------------------------- */

                self.getResourcesClass = async function () {

                    var getReportCBCF = function (data) {

                        $.each(data, function (index) {
                            self.resClassArr.push({
                                label: data[index].NAME,
                                value: data[index].NAME
                            });
                        });
                    };
                    await services.getGenericReport({"reportName": "Resource_Class_Report"}).then(getReportCBCF, app.failCbFn);
                };
                /* --------------------------- REPORT TO GET ELEMENTS LINE   --------------------------- */

                self.getResourcesTask = async function () {

                    self.lineData([]);
                    var getReportCBCF = function (data) {

                        data = typeof data === 'string' ? JSON.parse(data) : data;
                        if (data.length === 0)
                        {
                            app.loading(false);
                            document.querySelector("#ViewTaskDialog").close();
                            app.messagesDataProvider.push(app.createMessage('warning', 'No Item found !!!'));
                        } else if (!data.length)
                        {
                            data = [data];
                            data = typeof data === 'string' ? JSON.parse(data) : data;
                            app.loading(false);
                            for (var item in  data) {

                                self.lineData.push({
                                    "unitOfMeasure": data[0].G_2.UNIT_OF_MEASURE,
                                    "categoryName": data[0].G_2.CATEGORY_NAME,
                                    "tc_std_raw_cost": data[0].G_2.TC_BRDND_COST,
                                    "assignmentDescription": data[0].G_2.ASSIGNMENT_DESCRIPTION,
                                    "quantity": data[0].G_2.QUANTITY,
                                    "name": data[0].G_2.NAME,
                                    "itemId": data[0].G_2.RESOURCE_SOURCE_ID,
                                    "TaskNumber": data[item].G_2.TASK_ID,
                                    "seq": data[0].G_2.seq = parseInt(0) + 1,
                                    "previousOrdered": 0,
                                    "bussinesUnit": self.requisitionBu(),
                                    "CurrentQty": self.lineData().forEach(e => {
                                        e.CurrentQty = '0'
                                    }),
                                    "destinationType": self.destinationTypeVal(),
                                    "referenceBOM": self.referenceBOMval(),

                                });
                                self.linDataprovider(new oj.ArrayTableDataSource(self.lineData, {keyAttributes: 'id'}));
                            }
                            for (var i = 0; i < self.lineData().length; i++) {
                                for (var j = 0; j < self.localData().length; j++) {
                                    if ((self.lineData()[i].name == self.localData()[j].itemCode) &&
                                            (self.lineData()[i].TaskNumber == self.localData()[j].taskNumber))
                                    {
                                        self.lineData()[i].previousOrdered = self.localData()[j].currentQuantity;
                                    }
                                }

                                self.linDataprovider(new oj.ArrayTableDataSource(self.lineData, {keyAttributes: 'id'}));
                            }
                        } else
                        {
                            app.loading(false);
                            for (var item in  data) {

                                self.lineData.push({
                                    "unitOfMeasure": data[item].G_2.UNIT_OF_MEASURE,
                                    "tc_std_raw_cost": data[item].G_2.TC_BRDND_COST,
                                    "assignmentDescription": data[item].G_2.ASSIGNMENT_DESCRIPTION,
                                    "quantity": data[item].G_2.QUANTITY,
                                    "categoryName": data[item].G_2.CATEGORY_NAME,
                                    "itemId": data[item].G_2.RESOURCE_SOURCE_ID,
                                    "name": data[item].G_2.NAME,
                                    "TaskNumber": data[item].G_2.TASK_ID,
                                    "bussinesUnit": self.requisitionBu(),
                                    "seq": data[item].G_2.seq = parseInt(item) + 1,
                                    "previousOrdered": 0,
                                    "CurrentQty": self.lineData().forEach(e => {
                                        e.CurrentQty = '0'
                                    }),
                                    "destinationType": self.destinationTypeVal(),
                                    "referenceBOM": self.referenceBOMval(),

                                });
                                self.linDataprovider(new oj.ArrayTableDataSource(self.lineData, {keyAttributes: 'id'}));
                            }
                            for (var i = 0; i < self.lineData().length; i++) {
                                for (var j = 0; j < self.localData().length; j++) {
                                    if ((self.lineData()[i].name == self.localData()[j].itemCode) &&
                                            (self.lineData()[i].TaskNumber == self.localData()[j].taskNumber))
                                    {
                                        self.lineData()[i].previousOrdered = self.localData()[j].currentQuantity;
                                    }
                                }
                            }
                            self.linDataprovider(new oj.ArrayTableDataSource(self.lineData, {keyAttributes: 'id'}));

                        }
                    };
                    await services.getGenericReport({"reportName": "ELEMENT_LINE_REPORT", "task_id": self.taskID(), "VERSION_ID": self.projectId()}).then(getReportCBCF, app.failCbFn);
                };
                /* --------------------------- rest to get local data from db   --------------------------- */

                self.getLocalData = function () {

                    var getReportCBCF = function (data) {
                        self.localData(data);
                    };
                    services.getGeneric(commonhelper.getLocalData, {}).then(getReportCBCF, app.failCbFn);
                };
                /* --------------------------- button for cancel view dailog --------------------------- */

                self.CancelBtnAction = function () {
                    document.querySelector("#ViewDialog").close();
                };
                function initTranslations() {
                    self.HLablel(getTranslation("Purchase.HLablel"));
                    self.ExpenditureType(getTranslation("Purchase.ExpenditureType"));
                    self.searchLbl(getTranslation("Purchase.searchLbl"));
                    self.EditLbl(getTranslation("Purchase.EditLbl"));
                    self.ViewLbl(getTranslation("Purchase.ViewLbl"));
                    self.BomLnum(getTranslation("Purchase.BomLnum"));
                    self.LineDesc(getTranslation("Purchase.LineDesc"));
                    self.itemCode(getTranslation("Purchase.itemCode"));
                    self.PrOrder(getTranslation("Purchase.PrOrder"));
                    self.CurrrentQty(getTranslation("Purchase.Quantity"));
                    self.totalQty(getTranslation("Purchase.totalQty"));
                    self.UOM(getTranslation("Purchase.UOM"));
                    self.itmCost(getTranslation("Purchase.itmCost"));
                    self.NeedBy(getTranslation("Purchase.NeedBy"));
                    self.ProjNum(getTranslation("Purchase.ProjNum"));
                    self.TaskNum(getTranslation("Purchase.TaskNum"));
                    self.ExpenOrgan(getTranslation("Purchase.ExpenOrgan"));
                    self.ExpenType(getTranslation("Purchase.ExpenType"));
                    self.ExpenDate(getTranslation("Purchase.ExpenDate"));
                    self.ExpenStartDate(getTranslation("Purchase.ExpenStartDate"));
                    self.ExpenEndDate(getTranslation("Purchase.ExpenEndDate"));
                    self.exportLbl(getTranslation("Purchase.exportLbl"));
                    self.resourceClassLbl(getTranslation("Purchase.resourceClassLbl"));
                    self.headerTextReAssignDailogLb2(getTranslation("Purchase.headerTextReAssignDailogLb2"));
                    self.CancelLbl(getTranslation("Purchase.CancelLbl"));
                    self.CancelLineLbl(getTranslation("Purchase.CancelLineLbl"));
                    self.DisplayLbl(getTranslation("Purchase.DisplayLbl"));
                    self.projectStartDate(getTranslation("Purchase.projectStartDate"));
                    self.projectEndDate(getTranslation("Purchase.projectEndDate"));
                    self.generatePr(getTranslation("Purchase.generatePr"));
                    self.projName(getTranslation("Purchase.projName"));
                    self.TaskName(getTranslation("Purchase.TaskName"));
                    self.AddToGenelLbl(getTranslation("Purchase.AddToGenelLbl"));
                    self.GenePrLbl(getTranslation("Purchase.GenePrLbl"));
                    self.headerTextLineDailogLb(getTranslation("Purchase.headerTextLineDailogLb"));
                    self.bodyTextLineDailogLb(getTranslation("Purchase.bodyTextLineDailogLb"));
                    self.headerTextGenerateDailogLb(getTranslation("Purchase.headerTextGenerateDailogLb"));
                    self.projectNameLbl(getTranslation("Purchase.projectNameLbl"));
                    self.projectNumberLbl(getTranslation("Purchase.projectNumberLbl"));
                    self.projectManagerLbl(getTranslation("Purchase.projectManagerLbl"));
                    self.projectStatusLbl(getTranslation("Purchase.projectStatusLbl"));
                    self.taskNameLbl(getTranslation("Purchase.taskNameLbl"));
                    self.taskNumberLbl(getTranslation("Purchase.taskNumberLbl"));
                    self.expenDateFromLbl(getTranslation("Purchase.expenDateFromLbl"));
                    self.expenDateToLbl(getTranslation("Purchase.expenDateToLbl"));
                    self.ExpenDateTo(getTranslation("Purchase.expenDateToLbl"));
                    self.expenTypeLbl(getTranslation("Purchase.expenTypeLbl"));
                    self.projectOrganizationLbl(getTranslation("Purchase.projectOrganizationLbl"));
                    self.expenditureOrganization(getTranslation("Purchase.expenditureOrganizationLbl"));
                    self.needByDateLbl(getTranslation("Purchase.needByDateLbl"));
                    self.qtyLbl(getTranslation("Purchase.qtyLbl"));
                    self.resetLbl(getTranslation("Purchase.resetLbl"));
                    self.addQtyLbl(getTranslation("Purchase.addQtyLbl"));
                    self.headerTextQuanDailogLb(getTranslation("Purchase.headerTextQuanDailogLb"));
                    self.projectDetailsLbl(getTranslation("Purchase.projectDetailsLbl"));
                    self.taskDetailsLbl(getTranslation("Purchase.taskDetailsLbl"));
                    self.destinationTypeLbl(getTranslation("Purchase.destinationTypeLbl"));
                    self.clearPrLbl(getTranslation("Purchase.clearPrLbl"));
                    self.placeholder(getTranslation("Purchase.placeholder"));
                    self.subInvLbl(getTranslation("Purchase.subInvLbl"));
                    self.referenceBomLbl(getTranslation("Purchase.referenceBomLbl"));
                   self.invOrgLbl(getTranslation("Purchase.invOrgLbl"));
                    self.columnArray([
                        {
                            "headerText": self.BomLnum(), "field": "seq"
                        }
                        ,
                        {
                            "headerText": self.projName(), "field": "projectName"
                        }
                        ,
                        {
                            "headerText": self.ProjNum(), "field": "projectNumber"
                        }
                        ,
                        {
                            "headerText": self.projectStartDate(), "field": "projectStartDate"
                        }
                        ,
                        {
                            "headerText": self.projectEndDate(), "field": "projectEndDate"
                        }
                        ,
                        {
                            "headerText": self.projectManagerLbl(), "field": "projectManagerName"
                        }
                        ,
                        {
                            "headerText": self.projectOrganizationLbl(), "field": "businessUnitName"
                        }
                        ,
                        {
                            "headerText": self.projectStatusLbl(), "field": "projectStatus"
                        },
                        {
                            "headerText": self.expenditureOrganizationLbl(), "field": "owningOrganizationName"
                        }
                    ]);
                    self.DetailsColumnArray([

                        {
                            "headerText": self.BomLnum(), "template": "seqTemplate"
                        },
                        {
                            "headerText": self.TaskName(), "field": "taskName"
                        },
                        {
                            "headerText": self.TaskNum(), "field": "taskNumber"
                        }
                        ,
                        {
                            "headerText": self.NeedBy(), "field": "expenditureDateFrom"
                        }
                        ,
                        {
                            "headerText": self.ExpenOrgan(), "field": "expenditureOrganization"
                        }
                        ,
                        {
                            "headerText": self.resourceClassLbl(), "field": "ExpenditureType"
                        }
                        ,
                        {
                            "headerText": self.expenDateFromLbl(), "field": "expenditureDateFrom"
                        }
                        ,
                        {
                            "headerText": self.ExpenDateTo(), "field": "expenditureDateTo"
                        }

                    ]);
                    self.lineColumnArray([
                        {
                            "headerText": "Selected", "template": "checkTemplate"
                        },
                        {
                            "headerText": self.BomLnum(), "field": "seq"
                        }
                        , {
                            "headerText": self.itemCode(), "field": "name"
                        }
                        ,
                        {
                            "headerText": self.LineDesc(), "field": "assignmentDescription"
                        }
                        ,
                        {
                            "headerText": self.itmCost(), "field": "tc_std_raw_cost"
                        }
                        ,
                        {
                            "headerText": self.PrOrder(), "field": "previousOrdered"
                        }
                        ,
                        {
                            "headerText": self.CurrrentQty(), "field": "CurrentQty"
                        }
                        ,
                        {
                            "headerText": self.totalQty(), "field": "quantity"
                        }
                        ,
                        {
                            "headerText": self.UOM(), "field": "unitOfMeasure"
                        },
                        {
                            "headerText": self.destinationTypeLbl(), "field": "destinationType"
                        }

                    ]);
                    self.generateColumnArray([
                        {
                            "headerText": self.BomLnum(), "field": "ser"
                        },
                        {
                            "headerText": self.itemCode(), "field": "name"
                        }
                        ,
                        {
                            "headerText": self.LineDesc(), "field": "assignmentDescription"
                        }
                        ,
                        {
                            "headerText": self.itmCost(), "field": "tc_std_raw_cost"
                        }
                        ,
                        {
                            "headerText": self.PrOrder(), "field": "previousOrdered"
                        }
                        ,
                        {
                            "headerText": self.CurrrentQty(), "field": "CurrentQty"
                        }
                        ,
                        {
                            "headerText": self.totalQty(), "field": "quantity"
                        }
                        ,
                        {
                            "headerText": self.UOM(), "field": "unitOfMeasure"
                        }
                        ,
                        {
                            "headerText": self.destinationTypeLbl(), "field": "destinationType"
                        }

                    ]);
                }

            }
            return PurchaseSummryContentViewModel;
        });






