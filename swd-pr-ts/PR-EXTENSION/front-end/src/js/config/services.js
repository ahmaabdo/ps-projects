define(['config/serviceconfig', 'util/commonhelper'], function (serviceConfig, commonHelper) {

    function services() {

        var self = this;
        var biReportServletPath = "report/commonbireport";
        var restPath = commonHelper.getInternalRest();
        var headers = {
            // "Authorization": "Basic bmlkYWxAc3VsdGFuLmNvbS5rdzpCc2JjT3JhQDE5"
        };

        self.authenticate = function (payload) {
            var serviceURL = restPath + "login";
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.getSaasLinksGeneric = function (serviceName) {
            var serviceURL = serviceName;
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.getGeneric = function (serviceName, header) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, header, true);
        };

        self.getGenericReport = function (payload) {
            var serviceURL = restPath + biReportServletPath;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.searcheGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
                "content-type": "application/x-www-form-urlencoded"
            };
            return serviceConfig.callPostServiceUncodeed(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
        };

        self.editGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPutService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.postGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.postSaasPrGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.addGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.addGenericSaas = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };

        self.deleteGeneric = function (serviceName, headers) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callDeleteService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

    }

    return new services();
});