/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biPReports;


import com.appspro.db.CommonConfigReader;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;



/**
 *
 * @author user
 */
public class BIReportModel extends RestHelper {

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getSegmentName() {
        return segmentName;
    }


    public static enum REPORT_NAME {
        GET_POSITION_EMAIL_REPORT("GetPositionEmailsReport"),
        PERSON_ABSENCE_REPORT("PersonAbsenceReport"),
        FUSE_REPORT("FuseReport2"),
        PERSON_FUSE_REPORT("PersonFuseReport"),
        IDENTIFICATION_LETTERS_REPORT("ID_LETTER_REPORT"),
        DEPENDENT_REPORT("DependentsReport"),
        PERSON_ELEMENT_ENTRY_REPORT("PersonElementEntryReport"),
        EMPLOYEE_FUSE_REPORT("PersonFuseReport"),
        get_Query_For_List_Report("getQueryForListReport"),
        GET_ALL_POSITIONS("GetAllPositions"),
        Dynamic_Rrport("DynamicReport2"),
        EIT_REPORT("EIT Report"),
        All_EIT_REPORT("getAllEITReport"),
        ABSENCE_REPORT("AbsenceReport"),
        Dynamic_Validation_Rrport("DynamicValidationReport"),
        // EitName_Report("EitName_Report"),
        EIT_Name("EitNameReport"),
        workerData("workerDataReport"),
        EmployeesByDepartmentName("GetEmployeesByDepartment_Report"),
        MoeEmployeeLetter("MOE_Employment_Letter_1"),
        MOEnewAbsence("validationNewAbsence"),
        ValidationOVERLAB_ABS_CREATE_DECREE("OVERLAB_ABS_CREATE_DECREE"),
        ValidationOVERLAB_ABS_CREATE_DECREE_OVERTIMEPayment("OVERLAB_ABS_CREATE_DECREE_OVERTIMEPayment"),
        ValidationOVERLAB_ABS_CREATE_DECREE_MissionPayment("OVERLAB_ABS_CREATE_DECREE_MissionPayment"),
        ValidationOVERLAB_OVERTIME_REQUEST("OVERLAB_OVERTIME_REQUEST"),
        ValidationOVERLAB_OVERTIME_REQUEST_OVERTIME_PAYMENT("OVERLAB_OVERTIME_REQUEST_OVERTIME_PAYMENT"),
        ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_MISSION("OVERLAB_OVERTIME_REQUEST_BUSINESS_MISSION"),
        ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_PAYMENT("OVERLAB_OVERTIME_REQUEST_BUSINESS_PAYMENT"),
        ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_REQUEST("OVERLAB_OVERTIME_PAYMENT_OVERTIME_REQUEST"),
        ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_PAYMENT("OVERLAB_OVERTIME_PAYMENT_OVERTIME_PAYMENT"),
        ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_MISSION("OVERLAB_OVERTIME_PAYMENT_BUSINESS_MISSION"),
        ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_PAYMENT("OVERLAB_OVERTIME_PAYMENT_BUSINESS_PAYMENT"),
        ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_REQUEST("OVERLAB_BUSINESS_PAYMENT_OVERTIME_REQUEST"),
        ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_PAYMENT("OVERLAB_BUSINESS_PAYMENT_OVERTIME_PAYMENT"),
        ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_MISSION("OVERLAB_BUSINESS_PAYMENT_BUSINESS_MISSION"),
        ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_PAYMENT("OVERLAB_BUSINESS_PAYMENT_BUSINESS_PAYMENT"),
        ValidationOVERLAB_BUSINESS_MISSION_OVERTIME_REQUEST("OVERLAB_BUSINESS_MISSION_OVERTIME_REQUEST"),
        ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_MISSION("OVERLAB_BUSINESS_MISSION_BUSINESS_MISSION"),
        ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_PAYMENT("OVERLAB_BUSINESS_MISSION_BUSINESS_PAYMENT"),
        GET_LATEST_ANNUAL_LEAVE("GetLatestAnnualLeaveReport"),
        UserAuthenticationToShowSetUp("UserAuthentication"),
        MasterDataReport("MasterDataReport"),
        SupervisorHierarchyDataReport("SupervisorHierarchyDataReport"),
        GET_TERRITORIES_REPORT("GET_TERRITORIES_REPORT"),
        GET_CITY_BY_COUNTRY_REPORT("GET_CITY_BY_COUNTRY_REPORT"),
        GET_RATE_REPORT("GET_RATE_REPORT"),
        GET_JOB_RATE("GET_JOB_RATE"),
        GET_MANAGER_JOB_RATE("GET_MANAGER_JOB_RATE"),
        GET_AOR_REPORT("GET_AOR"),
        GET_UDT_REPORT("GET_UDT_REPORT"),
        absence_salary_in_advance_new("AbsenceSalaryInAdvanceJobReport");


        private String value;

        private REPORT_NAME(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum ATTRIBUTE_TEMPLATE {

        ATTRIBUTE_TEMPLATE_XML("DEFAULT"),
        ATTRIBUTE_TEMPLATE_PDF("PDF");
        private String value;

        private ATTRIBUTE_TEMPLATE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum ATTRIBUTE_FORMAT {

        ATTRIBUTE_FORMAT_XML("xml"),
        ATTRIBUTE_FORMAT_PDF("pdf"); //keep in small caps
        private String value;

        private ATTRIBUTE_FORMAT(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }
    public static enum UDT_REPORT_PARAM {
        P_TABLE_NAME("P_TABLE_NAME"),
        P_INFORMATION_TYPE("P_INFORMATION_TYPE"),
        P_ROW_NAME("P_ROW_NAME");

        private String value;

        private UDT_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum POSITION_EMAIL_REPORT_PARAM {
        POSITION_CODE("positionCode"),
        PERSON_ID("personId");
        private String value;

        private POSITION_EMAIL_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum MoeEmployeeLetter_PARAM {
        PersonId("PersonId");
        private String value;

        private MoeEmployeeLetter_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum FUSE_REPORT_PARAM {
        BIND_LOOKUP_TYPE("BINDLOOKUPTYPE"),
        P_TABLE_NAME("P_TABLE_NAME"),
        BIND_LANGUAGE("BINDLANGUAGE");

        private String value;

        private FUSE_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum IDENTIICATIONS_LETTER_REPORT_PARAM {
        CF_DIRECTTO("CF_DIRECTTO"),
        EMP_NO("EMP_NO");

        private String value;

        private IDENTIICATIONS_LETTER_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum DEPENDENT_REPORT_PARAM {
        personNumber("personNumber");

        private String value;

        private DEPENDENT_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum PERSON_ELEMENT_ENTRY_REPORT_PARAM {
        P_PERSON_ID("P_PERSON_ID"),
        P_START_DATE("P_START_DATE"),
        P_END_DATE("P_END_DATE");

        private String value;

        private PERSON_ELEMENT_ENTRY_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum UserAuthenticatio_PARAM {
        USERNAME("USERNAME");

        private String value;

        private UserAuthenticatio_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum MasterDataReport_PARAM {
        USERNAME("USERNAME"),
        LANG("LANG"),
        LANGX("LANGX");

        private String value;

        private MasterDataReport_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum EIT_REPORT_PARAM {
        Structure_Code("Structure_Code"),
        LANG("LANG");

        private String value;

        private EIT_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum All_EIT_REPORT_PARAM {
        LANG("LANG");

        private String value;

        private All_EIT_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum EIT_Name_PARAM {
        LANG("LANG");

        private String value;

        private EIT_Name_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum Dynamic_Rrport_PARAM {
        str("str");

        private String value;

        private Dynamic_Rrport_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum ABSENCE_REPORT_PARAM {
        employeeNumber("employeeNumber");

        private String value;

        private ABSENCE_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum get_Query_For_List_Report_PARAM {
        valueSet("valueSet");

        private String value;

        private get_Query_For_List_Report_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    public static enum get_UserAuthenticatio_PARAM {
        USERNAME("USERNAME");

        private String value;

        private get_UserAuthenticatio_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum WORKER_Data_PARAM {
        Person_id("Person_id");

        private String value;

        private WORKER_Data_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum validation_New_Absence_Param {

        P_START_DATE("P_START_DATE"),
        P_END_DATE("P_END_DATE"),
        personId("personId");

        private String value;

        private validation_New_Absence_Param(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum SupervisorHierarchyDataParam {

        P_MGR_NUMBER("P_MGR_NUMBER"),
        P_EFFECTIVE_DATE("P_EFFECTIVE_DATE");

        private String value;

        private SupervisorHierarchyDataParam(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum Employees_REPORT_PARAM {
        personNumber("department_id");

        private String value;

        private Employees_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    
    public static enum FND_LOOKUP_REPORT_PARAM {
        BIND_LOOKUP_TYPE("bindLookupType"),
        BIND_LANGUAGE("bindLanguage");

        private String value;

        private FND_LOOKUP_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public static enum GET_RATE_REPORT_PARAM {
        NAME("NAME"),
        GRADE_ID("GRADE_ID");

        private String value;

        private GET_RATE_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public static enum GET_JOB_RATE_REPORT_PARAM {
        P_PERSON_NUMBER("P_PERSON_NUMBER");

        private String value;

        private GET_JOB_RATE_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public static enum GET_MANAGER_JOB_RATE_REPORT_PARAM {
        P_PERSON_NUMBER("P_PERSON_NUMBER");

        private String value;

        private GET_MANAGER_JOB_RATE_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    //get Aor
    public static enum GET_AOR_REPORT_PARAM {
        P_PERSON_NUMBER("P_PERSON_NUMBER");

        private String value;

        private GET_AOR_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public static enum GET_CITY_BY_COUNTRY_REPORT_PARAM {
        COUNTRY_VALUE("COUNTRY_VALUE");

        private String value;

        private GET_CITY_BY_COUNTRY_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    private String reportAbsolutePath;
    private String attributeFormat;
    private String attributeTemplate;
    private String parameters;
    private Map<String, String> paramMap = new HashMap<String, String>();
    private String soapRequest = null;
    private String username = CommonConfigReader.getValue("USER_NAME");
    private String password = CommonConfigReader.getValue("PASSWORD");
    private String segmentName;
    public static List<BIReportModel> THREAD_REPORT =
        new ArrayList<BIReportModel>();
    public static int THREAD_REPORT_COUNTER = -1;
    public static List<String> THREAD_REPORT_RESPONSE =
        new ArrayList<String>();
    //     private String username = "mahmoud.essam@appspro-me.com";
    //     private String password = "Welcome@123";

    public void setParamMap(Map<String, String> paramMap) {
        this.paramMap = paramMap;
    }

    public Map<String, String> getParamMap() {
        return paramMap;
    }

    public void setReportAbsolutePath(String reportAbsolutePath) {
        this.reportAbsolutePath = reportAbsolutePath;
    }

    public String getReportAbsolutePath() {
        return reportAbsolutePath;
    }

    public String getParameters() {
        Iterator it = paramMap.entrySet().iterator();
        parameters = "";
        while (it.hasNext()) {
            Map.Entry parameter = (Map.Entry)it.next();
            parameters = parameters + "             <pub:item>\n" +
                    "                <pub:name>" + parameter.getKey() +
                    "</pub:name>\n" +
                    "                 <pub:values>\n" +
                    "                    <pub:item>" + parameter.getValue() +
                    "</pub:item>\n" +
                    "                  </pub:values>\n" +
                    "             </pub:item>\n";
        }
        return parameters;
    }

    public String getSoapRequest() {
        this.soapRequest =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pub=\"http://xmlns.oracle.com/oxp/service/PublicReportService\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <pub:runReport>\n" +
                "         <pub:reportRequest>\n" +
                "            <pub:attributeLocale>en-US</pub:attributeLocale>\n" +
                "            <pub:attributeTemplate>" +
                this.attributeTemplate + "</pub:attributeTemplate>\n" +
                "            <pub:attributeFormat>" + this.attributeFormat +
                "</pub:attributeFormat>\n" +
                "            <pub:reportAbsolutePath>/Custom/SAAS Extension Reports/Reports/" +
                reportAbsolutePath + ".xdo</pub:reportAbsolutePath>\n" +
                "<pub:parameterNameValues>\n" +
                " \n" +
                getParameters() + "             </pub:parameterNameValues>\n" +
                " \n" +
                "         </pub:reportRequest>\n" +
                "         <pub:userID>" + this.username + "</pub:userID>\n" +
                "         <pub:password>" + this.password +
                "</pub:password>\n" +
                "      </pub:runReport>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        return soapRequest;
    }

    public byte[] executeReportsPDF() {
        String output = "";
        try {
            System.setProperty("DUseSunHttpHandler", "true");

            byte[] buffer = new byte[getSoapRequest().length()];
            buffer = getSoapRequest().getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url =
                new URL(null, this.getBiReportUrl(), new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                //https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";
            //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            InputStream is = http.getInputStream();
            String responseXML = getStringFromInputStream(is);

            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(responseXML));

            Document doc = builder.parse(src);
            String data =
                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            } else {
                output = data;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.getBytes();
    }

    public void setAttributeFormat(String attributeFormat) {
        this.attributeFormat = attributeFormat;
    }

    public void setAttributeTemplate(String attributeTemplate) {
        this.attributeTemplate = attributeTemplate;
    }

    public static JSONObject runReport(String reportName,
                                       Map<String, String> paramMap) {
        BIReportModel biPReports = new BIReportModel();
        biPReports.setAttributeFormat(BIReportModel.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIReportModel.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
        biPReports.setReportAbsolutePath(reportName);

        biPReports.setParamMap(paramMap);

        String fresponse = biPReports.executeReports();
        //  System.out.print(fresponse);
        JSONObject xmlJSONObj = null;

        try {
            xmlJSONObj = XML.toJSONObject(fresponse.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(xmlJSONObj);

        return xmlJSONObj;
    }

    public String executeReports() {
        String output = "";
        try {
            System.out.println("Invoke service using direct HTTP call with Basic Auth");

            Document doc =
                new RestHelper().httpPost(this.getBiReportUrl(), getSoapRequest());

            doc.getDocumentElement().normalize();

            String data =
                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            } else {
                output = data;
            }

            return output;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }

    public String executeReportsThread() {
        String output = "";
        try {
            System.out.println("Invoke service using direct HTTP call with Basic Auth");

            Document doc =
                new RestHelper().httpPost(this.getBiReportUrl(), getSoapRequest());

            doc.getDocumentElement().normalize();

            String data =
                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            } else {
                output = data;
            }

            return segmentName + "SPLIT" + output;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return segmentName + "SPLIT" + output;
    }

    public List<String> runMultipleReports() throws InterruptedException,
                                                    ExecutionException {

        Map<String, String> responesMap = new HashMap<String, String>();

        System.out.println(new Date());
        int size = THREAD_REPORT.size();
        if (size < 1) {
            return new ArrayList<String>();
        }
        ExecutorService threads = Executors.newFixedThreadPool(size);
        List<Callable<String>> torun = new ArrayList(size);
        final CountDownLatch countdown = new CountDownLatch(size);
        for (int i = 0; i < size; i++) {
            torun.add(new Callable<String>() {
                    public String call() throws InterruptedException {
                        countdown.countDown();
                        countdown.await();
                        THREAD_REPORT_COUNTER++;
                        return THREAD_REPORT.get(THREAD_REPORT_COUNTER).executeReportsThread();
                    }
                });
        }

        // all tasks executed in different threads, at 'once'.
        List<Future<String>> futures = threads.invokeAll(torun);

        // no more need for the threadpool
        threads.shutdown();
        // check the results of the tasks...throwing the first exception, if any.
        for (Future<String> fut : futures) {
            THREAD_REPORT_RESPONSE.add(fut.get());
        }
        countdown.await();
        System.out.println(new Date());
        //        threads.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        //check the threadpool is now in fact complete
        if (!threads.isShutdown()) {
            // something went wrong... our accounting is off...
        }

        System.out.println(new Date());
        return THREAD_REPORT_RESPONSE;
    }


}
