package com.appspro.nexuscc.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties({"expenditureType","needByDate"})

public class ProjectTasksBean {
  
  private String taskName ; 
  private String taskNumber ;
  private String needByDate;
  private String expenditureOrganization ;
  private String expenditureType;
  private String expenditureDateTo;
  private String expenditureDateFrom;
  private Long taskId;


    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setNeedByDate(String needByDate) {
        this.needByDate = needByDate;
    }

    public String getNeedByDate() {
        return needByDate;
    }

    public void setExpenditureOrganization(String expenditureOrganization) {
        this.expenditureOrganization = expenditureOrganization;
    }

    public String getExpenditureOrganization() {
        return expenditureOrganization;
    }

    public void setExpenditureType(String expenditureType) {
        this.expenditureType = expenditureType;
    }

    public String getExpenditureType() {
        return expenditureType;
    }

    public void setExpenditureDateTo(String expenditureDateTo) {
        this.expenditureDateTo = expenditureDateTo;
    }

    public String getExpenditureDateTo() {
        return expenditureDateTo;
    }

    public void setExpenditureDateFrom(String expenditureDateFrom) {
        this.expenditureDateFrom = expenditureDateFrom;
    }

    public String getExpenditureDateFrom() {
        return expenditureDateFrom;
    }


    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTaskId() {
        return taskId;
    }
}
