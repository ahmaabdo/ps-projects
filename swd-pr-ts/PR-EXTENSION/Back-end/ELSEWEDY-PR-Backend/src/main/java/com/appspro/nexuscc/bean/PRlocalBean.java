package com.appspro.nexuscc.bean;

public class PRlocalBean {
    private int id;
        private String lineDescription;
        private String itemCode;
        private String previousOrdered;
        private String currentQuantity;
        private String totalQuantity;
        private String uom;
        private String itemCost;
        private String projectNumber;
        private String taskNumber;

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }


        public void setLineDescription(String lineDescription) {
            this.lineDescription = lineDescription;
        }

        public String getLineDescription() {
            return lineDescription;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setPreviousOrdered(String previousOrdered) {
            this.previousOrdered = previousOrdered;
        }

        public String getPreviousOrdered() {
            return previousOrdered;
        }

        public void setCurrentQuantity(String currentQuantity) {
            this.currentQuantity = currentQuantity;
        }

        public String getCurrentQuantity() {
            return currentQuantity;
        }

        public void setTotalQuantity(String totalQuantity) {
            this.totalQuantity = totalQuantity;
        }

        public String getTotalQuantity() {
            return totalQuantity;
        }

        public void setUom(String uom) {
            this.uom = uom;
        }

        public String getUom() {
            return uom;
        }

        public void setItemCost(String itemCost) {
            this.itemCost = itemCost;
        }

        public String getItemCost() {
            return itemCost;
        }

        public void setProjectNumber(String projectNumber) {
            this.projectNumber = projectNumber;
        }

        public String getProjectNumber() {
            return projectNumber;
        }

        public void setTaskNumber(String taskNumber) {
            this.taskNumber = taskNumber;
        }

        public String getTaskNumber() {
            return taskNumber;
        }
}
