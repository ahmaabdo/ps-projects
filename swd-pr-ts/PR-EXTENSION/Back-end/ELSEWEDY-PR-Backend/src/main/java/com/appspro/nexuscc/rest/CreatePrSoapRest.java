package com.appspro.nexuscc.rest;

import com.appspro.nexuscc.bean.PRSoapBean;

import com.google.gson.JsonObject;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONObject;


import org.xml.sax.SAXException;

import soap.CreatePr;

@Path("/PrSoap")


public class CreatePrSoapRest {
       
     @POST 
     @Path("/CreatPrReq")
     @Consumes(MediaType.APPLICATION_JSON)
     @Produces(MediaType.APPLICATION_JSON)
    
    public Response createPrReq (String incomingData) throws IOException,
                                                            SAXException,
                                                            ParserConfigurationException {
         PRSoapBean bean = new PRSoapBean ();
         CreatePr pr = new CreatePr ();
         JSONObject object = new JSONObject(incomingData); 
         System.out.println(object);
         bean.setInterfaceSourceCode(object.getString("interfaceSourceCode"));
         bean.setRequisitioningBUId(object.getString("requisitioningBUId"));
         bean.setInterfaceHeaderKey(object.getLong("interfaceHeaderKey"));
         bean.setRequisitioningBUName(object.getString("requisitioningBUName")); 
         bean.setDocumentStatusCode(object.getString("documentStatusCode"));
         bean.setPreparerId(object.getString("preparerId")); 
         bean.setRequisitioningBUId(object.getString("RequisitioningBUId"));
         bean.setRequisitioningBUName(object.getString("RequisitioningBUName")); 
         bean.setExternallyManagedFlag(object.getString("externallyManagedFlag"));
       
//         bean.setProjectNumber(object.getString("projectNumber"));
//         bean.setTaskId(object.getString("taskId"));
          bean.setItems(object.getJSONArray("lineItems"));
//         bean.setProjectOrganizationName(object.getString("projectOrganizationName"));
//         bean.setPercent(object.getString("percent"));
//         bean.setSegment1(object.getString("segment1"));
//         bean.setSegment2(object.getString("segment2"));
//         bean.setSegment3(object.getString("segment3"));
//         bean.setSegment4(object.getString("segment4"));
//         bean.setSegment5(object.getString("segment5"));
//         bean.setSegment6(object.getString("segment6"));
//         bean.setSegment7(object.getString("segment7"));
//         bean.setSegment8(object.getString("segment8"));
//         bean.setSegment9(object.getString("segment9"));
//         bean.setSegment10(object.getString("segment10"));

//         bean.setBudgetDate(object.getString("budgetDate"));
         
         
        String _response =
                 CreatePr.callPostRest2("https://elhn-test.fa.em2.oraclecloud.com:443/fscmService/PurchaseRequestService",bean);
        String statusMsg = "SUCCESS";
        JsonObject json = new JsonObject();
        json.addProperty("prNumber", CreatePr.requisitionNum);
        JsonObject json2 = new JsonObject();
        json2.addProperty("errorMessage", CreatePr.statusErrorMessage);

        if(statusMsg.equalsIgnoreCase(_response)) {
            return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
        }
        else {
           // return  Response.ok(json2.toString(), MediaType.APPLICATION_JSON).build();
           return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(json2.toString()).build();

        }
    }
    
    
   
    
    
}
