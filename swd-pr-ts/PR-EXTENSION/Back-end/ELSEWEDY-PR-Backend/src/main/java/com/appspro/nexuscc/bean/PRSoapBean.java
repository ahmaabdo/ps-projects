package com.appspro.nexuscc.bean;

import org.json.JSONArray;

public class PRSoapBean {
   
    public String interfaceSourceCode;
    public String requisitioningBUId;
    public String requisitioningBUName;
    public String approverId;
    public String documentStatusCode;
    public Long interfaceHeaderKey;
    public String preparerId;
    public String externallyManagedFlag;
    public String deliverToLocationId;
    public String categoryName;
    public String categoryId;
    public String currencyCode;
    public String deliverToOrganizationCode;
    public String deliverToOrganizationId;
    public String destinationTypeCode;
    public String itemDescription;
    public String itemId;
    public String itemNumber;
    public String lineType;
    public String quantity;
    public String requesterId;
    public String unitOfMeasure;
    public String percent;
    public String lineItems ;
    public String segment1;
    public String segment2;
    public String segment3;
    public String segment4;
    public String segment5;
    public String segment6;
    public String segment7;
    public String segment8;
    public String segment9;
    public String segment10;
    public String projectNumber;
    public String taskId;
    public String projectOrganizationName;
    public String DestinationSubinventory;
    public String projectExpendTypeName;
    public String referenceBomNumber;
    
    

    
    public JSONArray items = new JSONArray();
    
    public void setInterfaceSourceCode(String interfaceSourceCode) {
        this.interfaceSourceCode = interfaceSourceCode;
    }

    public String getInterfaceSourceCode() {
        return interfaceSourceCode;
    }

    public void setRequisitioningBUId(String requisitioningBUId) {
        this.requisitioningBUId = requisitioningBUId;
    }

    public String getRequisitioningBUId() {
        return requisitioningBUId;
    }

    public void setRequisitioningBUName(String requisitioningBUName) {
        this.requisitioningBUName = requisitioningBUName;
    }

    public String getRequisitioningBUName() {
        return requisitioningBUName;
    }

    public void setApproverId(String approverId) {
        this.approverId = approverId;
    }

    public String getApproverId() {
        return approverId;
    }

    public void setDocumentStatusCode(String documentStatusCode) {
        this.documentStatusCode = documentStatusCode;
    }

    public String getDocumentStatusCode() {
        return documentStatusCode;
    }

   

    public void setPreparerId(String preparerId) {
        this.preparerId = preparerId;
    }

    public String getPreparerId() {
        return preparerId;
    }

    public void setExternallyManagedFlag(String externallyManagedFlag) {
        this.externallyManagedFlag = externallyManagedFlag;
    }

    public String getExternallyManagedFlag() {
        return externallyManagedFlag;
    }

    public void setDeliverToLocationId(String deliverToLocationId) {
        this.deliverToLocationId = deliverToLocationId;
    }

    public String getDeliverToLocationId() {
        return deliverToLocationId;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
    

    public String getCategoryName() {
        return categoryName;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setDeliverToOrganizationCode(String deliverToOrganizationCode) {
        this.deliverToOrganizationCode = deliverToOrganizationCode;
    }

    public String getDeliverToOrganizationCode() {
        return deliverToOrganizationCode;
    }

    public void setDeliverToOrganizationId(String deliverToOrganizationId) {
        this.deliverToOrganizationId = deliverToOrganizationId;
    }

    public String getDeliverToOrganizationId() {
        return deliverToOrganizationId;
    }

    public void setDestinationTypeCode(String destinationTypeCode) {
        this.destinationTypeCode = destinationTypeCode;
    }

    public String getDestinationTypeCode() {
        return destinationTypeCode;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemDescription() {
        return itemDescription;
    }

   
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getLineType() {
        return lineType;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setRequesterId(String requesterId) {
        this.requesterId = requesterId;
    }

    public String getRequesterId() {
        return requesterId;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getPercent() {
        return percent;
    }

   

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setLineItems(String lineItems) {
        this.lineItems = lineItems;
    }

    public String getLineItems() {
        return lineItems;
    }

    public void setItems(JSONArray items) {
        this.items = items;
    }

    public JSONArray getItems() {
        return items;
    }


    public void setSegment1(String segment1) {
        this.segment1 = segment1;
    }

    public String getSegment1() {
        return segment1;
    }

    public void setSegment2(String segment2) {
        this.segment2 = segment2;
    }

    public String getSegment2() {
        return segment2;
    }

    public void setSegment3(String segment3) {
        this.segment3 = segment3;
    }

    public String getSegment3() {
        return segment3;
    }

    public void setSegment4(String segment4) {
        this.segment4 = segment4;
    }

    public String getSegment4() {
        return segment4;
    }

    public void setSegment5(String segment5) {
        this.segment5 = segment5;
    }

    public String getSegment5() {
        return segment5;
    }

    public void setSegment6(String segment6) {
        this.segment6 = segment6;
    }

    public String getSegment6() {
        return segment6;
    }

    public void setSegment7(String segment7) {
        this.segment7 = segment7;
    }

    public String getSegment7() {
        return segment7;
    }

    public void setSegment8(String segment8) {
        this.segment8 = segment8;
    }

    public String getSegment8() {
        return segment8;
    }

    public void setSegment9(String segment9) {
        this.segment9 = segment9;
    }

    public String getSegment9() {
        return segment9;
    }

    public void setSegment10(String segment10) {
        this.segment10 = segment10;
    }

    public String getSegment10() {
        return segment10;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setInterfaceHeaderKey(Long interfaceHeaderKey) {
        this.interfaceHeaderKey = interfaceHeaderKey;
    }

    public Long getInterfaceHeaderKey() {
        return interfaceHeaderKey;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setProjectOrganizationName(String projectOrganizationName) {
        this.projectOrganizationName = projectOrganizationName;
    }

    public String getProjectOrganizationName() {
        return projectOrganizationName;
    }

    public void setDestinationSubinventory(String subInventory) {
        this.DestinationSubinventory = subInventory;
    }

    public String getDestinationSubinventory() {
        return DestinationSubinventory;
    }

    public void setProjectExpendTypeName(String projectExpendTypeName) {
        this.projectExpendTypeName = projectExpendTypeName;
    }

    public String getProjectExpendTypeName() {
        return projectExpendTypeName;
    }

    public void setReferenceBomNumber(String referenceBomNumber) {
        this.referenceBomNumber = referenceBomNumber;
    }

    public String getReferenceBomNumber() {
        return referenceBomNumber;
    }
}
