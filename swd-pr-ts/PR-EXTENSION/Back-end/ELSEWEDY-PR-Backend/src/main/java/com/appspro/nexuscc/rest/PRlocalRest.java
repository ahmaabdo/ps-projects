package com.appspro.nexuscc.rest;


import com.appspro.nexuscc.bean.PRlocalBean;
import com.appspro.nexuscc.dao.PRlocalDao;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/PrLocal")

public class PRlocalRest {
   
    PRlocalDao service = new PRlocalDao();

      @GET
      @Path("/getAll")
      @Produces(MediaType.APPLICATION_JSON)
      public String getAllPrRequest() {
          List<PRlocalBean> list = new ArrayList<PRlocalBean>();
          list = service.getallrequest();
          return new JSONArray(list).toString();

      }

      @POST
      @Path("/insert")
      @Consumes(MediaType.APPLICATION_JSON)
      @Produces(MediaType.APPLICATION_JSON)
      public String insertOrUpdateExit(String body) {
          PRlocalBean list = new PRlocalBean();
          JSONObject obj2 =new JSONObject();
          try {
              System.out.println(body);
              JSONArray arr = new JSONArray(body);
              for(int i = 0 ; i < arr.length() ; i++) {
                  JSONObject obj = arr.getJSONObject(i);
                  System.out.println(obj);
                  obj2 = service.insertToLocal(obj);
              }
              return new JSONObject(list).toString();
          } catch (Exception e) {
              e.printStackTrace();
          }
          return new JSONObject(list).toString();
      }
      
      @POST
      @Path("/update")
      @Consumes(MediaType.APPLICATION_JSON)
      @Produces(MediaType.APPLICATION_JSON)
      public String UpdateExist(String body) {
          PRlocalBean list = new PRlocalBean();
          JSONObject obj2 =new JSONObject();
          try {
              System.out.println(body);
              JSONArray arr = new JSONArray(body);
              for(int i = 0 ; i < arr.length() ; i++) {
                  JSONObject obj = arr.getJSONObject(i);
                  obj2 = service.upDateLocal(obj);
              }

              return new JSONObject(list).toString();
          } catch (Exception e) {
              e.printStackTrace();
          }
          return new JSONObject(list).toString();
      }

      @POST
      @Path("getTask")
      @Produces(MediaType.APPLICATION_JSON)
      public String findbyid(String data) {
          PRlocalBean list = new PRlocalBean();
          try {
              JSONObject o = new JSONObject(data);
              String taskNumber =
                  o.has("taskNumber") ? o.getString("taskNumber") : null;
              list = service.searchbyTaskNum(taskNumber);

          } catch (Exception e) {
              System.out.println(e);
          }

          return new JSONObject(list).toString();

      }
}
