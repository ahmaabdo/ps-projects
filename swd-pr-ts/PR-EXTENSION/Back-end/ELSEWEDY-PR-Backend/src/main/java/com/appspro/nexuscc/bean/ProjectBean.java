package com.appspro.nexuscc.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.sql.Date;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProjectBean {

        public String owningOrganizationName;
        public String projectName;
        public String projectNumber;
        public String projectManagerName;
        public String projectStatus;
        public String projectStartDate;
        public String projectEndDate;
        public Long  projectId;
        private String businessUnitName ; 
        private String businessUnitId;

        @JsonIgnore
        public JSONArray lis  ;

        public String links;
        
     

    public void setOwningOrganizationName(String owningOrganizationName) {
        this.owningOrganizationName = owningOrganizationName;
    }

    public String getOwningOrganizationName() {
        return owningOrganizationName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectManagerName(String projectManagerName) {
        this.projectManagerName = projectManagerName;
    }

    public String getProjectManagerName() {
        return projectManagerName;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setLis(JSONArray lis) {
        this.lis = lis;
    }

    public JSONArray getLis() {
        return lis;
    }

    public void setLinks(String links) {
        this.links = links;
    }

    public String getLinks() {
        return links;
    }
    
    public void setProjectStartDate(String projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public String getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectEndDate(String projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public String getProjectEndDate() {
        return projectEndDate;
    }


    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectId() {
        return projectId;
    }


    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitId(String businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public String getBusinessUnitId() {
        return businessUnitId;
    }
}
