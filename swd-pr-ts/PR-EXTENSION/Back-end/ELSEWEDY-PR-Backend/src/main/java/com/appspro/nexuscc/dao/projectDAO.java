package com.appspro.nexuscc.dao;

import biPReports.RestHelper;

import com.appspro.nexuscc.bean.ProjectBean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;

public class projectDAO extends RestHelper {
    
    public static void main(String[] args) {
      
        projectDAO dao = new projectDAO();
    }
    
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public ArrayList<ProjectBean> getProDetails() {
        ArrayList<ProjectBean> projectList = new ArrayList<ProjectBean>();
        
        int offset = 0;
        Boolean hasMore = false;
        do{

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getAllProjectUrl()+ "?limit=50&offset="+offset;
        
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");

            
            for (int i = 0; i < arr.length(); i++) {
                ProjectBean bean = new ProjectBean();
                JSONObject proObj = arr.getJSONObject(i);
                bean.setProjectName(proObj.getString("ProjectName"));
                bean.setProjectNumber(proObj.getString("ProjectNumber"));
                bean.setOwningOrganizationName(proObj.has("OwningOrganizationName")?proObj.optString("OwningOrganizationName") :null);
                bean.setProjectManagerName(proObj.has("ProjectManagerName") ?proObj.optString("ProjectManagerName") :null);
                bean.setProjectStatus(proObj.getString("ProjectStatus"));
                bean.setProjectStartDate(proObj.getString("ProjectStartDate"));
                bean.setProjectId(proObj.getLong("ProjectId"));
                bean.setBusinessUnitName(proObj.has("BusinessUnitName") ?proObj.optString("BusinessUnitName") :null);
                bean.setBusinessUnitId(proObj.has("BusinessUnitId")?proObj.optString("BusinessUnitId") :null);
                bean.setProjectEndDate(proObj.has("ProjectEndDate") ? proObj.optString("ProjectEndDate") : null);
                
               JSONArray linkes = proObj.getJSONArray("links");
               bean.setLinks(linkes.toString());
   
                projectList.add(bean);
            }
            
            hasMore = obj.getBoolean("hasMore");
            offset +=50;

            System.out.println(hasMore);


        } catch (Exception e) {
            e.printStackTrace();
        }

        }
        
        while(hasMore);
        return projectList;
    }
   
}
