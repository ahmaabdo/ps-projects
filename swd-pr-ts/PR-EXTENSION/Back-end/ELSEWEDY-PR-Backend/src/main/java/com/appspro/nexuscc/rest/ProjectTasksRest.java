package com.appspro.nexuscc.rest;

import com.appspro.nexuscc.bean.ProjectBean;
import com.appspro.nexuscc.bean.ProjectTasksBean;
import com.appspro.nexuscc.dao.ProjectTaskDAO;
import com.appspro.nexuscc.dao.projectDAO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;


@Path("/tasks")


public class ProjectTasksRest {
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGradeDetails( String url) throws JsonProcessingException, UnsupportedEncodingException, IOException {
    
        ObjectMapper mapper = new ObjectMapper();
        ProjectTasksBean obj = new ProjectTasksBean();
        ProjectTaskDAO pro = new ProjectTaskDAO();
        
        JSONObject o = new JSONObject(url);
        
        String taskUrl = o.has("href") ? o.getString("href") : null;
        
        System.out.println(taskUrl);

        
        ArrayList<ProjectTasksBean> projectList = pro.getProTasksDetails(taskUrl);
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(projectList);
    //        new String(jsonInString.getBytes(), "UTF-8")
        return Response.ok(jsonInString, MediaType.APPLICATION_JSON).build();
    }
}
