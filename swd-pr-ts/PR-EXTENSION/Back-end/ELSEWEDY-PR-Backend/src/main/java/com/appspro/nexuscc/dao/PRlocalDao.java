package com.appspro.nexuscc.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.db.AppsproConnection;
import com.appspro.nexuscc.bean.PRlocalBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import org.json.JSONObject;

public class PRlocalDao extends AppsproConnection {
    
        Connection connection;
        PreparedStatement pre;
        CallableStatement cs;
        ResultSet rs;

        public JSONObject upDateLocal(JSONObject bean) {
            String query = "";
            try {
                connection = AppsproConnection.getConnection();
                query = "update xx_sewedy_pr set CURRENT_QUANTITY=? where id = ?";

                pre = connection.prepareStatement(query);
                pre.setString(1, bean.getString("currentQuantity"));
                pre.setInt(2, bean.getInt("id"));
                pre.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(connection, pre, rs);
            }

            return bean;
        }

        public JSONObject insertToLocal(JSONObject bean) {
            String query = "";
            try {
                connection = AppsproConnection.getConnection();
                query =
                        "INSERT INTO xx_sewedy_pr(LINE_DESCRIPTION,ITEMCODE,PREVIOUS_ORDERED,CURRENT_QUANTITY,TOTAL_QUANTITY,UOM," +
                        "ITEMCOST,PROJECTNUMBER,TASKNUMBER)VALUES(?,?,?,?,?,?,?,?,?)";

                pre = connection.prepareStatement(query);
                pre.setString(1, bean.getString("itemCode"));
                pre.setString(2, bean.getString("itemCode"));
                pre.setString(3, bean.getString("previousOrdered"));
                pre.setString(4, bean.getString("currentQuantity"));
                pre.setString(5, bean.getString("totalQuantity"));
                pre.setString(6, bean.getString("uom"));
                pre.setString(7, bean.getString("itemCost"));
                pre.setString(8, bean.getString("projectNumber"));
                pre.setString(9, bean.getString("taskNumber"));
                pre.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(connection, pre, rs);
            }

            return bean;
        }

        public List<PRlocalBean> getallrequest() {
            ArrayList<PRlocalBean> list = new ArrayList<PRlocalBean>();
            try {
                connection = AppsproConnection.getConnection();
                String query = "SELECT * FROM xx_sewedy_pr";
                pre = connection.prepareStatement(query);
                rs = pre.executeQuery();

                while (rs.next()) {
                    PRlocalBean bean = new PRlocalBean();
                    bean.setId(rs.getInt("ID"));
                    bean.setLineDescription(rs.getString("LINE_DESCRIPTION"));
                    bean.setItemCode(rs.getString("ITEMCODE"));
                    bean.setPreviousOrdered(rs.getString("PREVIOUS_ORDERED"));
                    bean.setCurrentQuantity(rs.getString("CURRENT_QUANTITY"));
                    bean.setTotalQuantity(rs.getString("TOTAL_QUANTITY"));
                    bean.setUom(rs.getString("UOM"));
                    bean.setItemCost(rs.getString("ITEMCOST"));
                    bean.setProjectNumber(rs.getString("PROJECTNUMBER"));
                    bean.setTaskNumber(rs.getString("TASKNUMBER"));
                    list.add(bean);
                }

            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                closeResources(connection, pre, rs);
            }
            return list;
        }

        public PRlocalBean searchbyTaskNum(String TASKNUMBER) {


            connection = AppsproConnection.getConnection();
            String sql = "select * from xx_sewedy_pr where TASKNUMBER =? ";
            PreparedStatement pre;
            PRlocalBean bean = new PRlocalBean();
            try {
                pre = connection.prepareStatement(sql);
                pre.setString(1, TASKNUMBER);
                ResultSet rs = pre.executeQuery();

                while (rs.next()) {
                    bean.setLineDescription(rs.getString("LINE_DESCRIPTION"));
                    bean.setItemCode(rs.getString("ITEMCODE"));
                    bean.setPreviousOrdered(rs.getString("PREVIOUS_ORDERED"));
                    bean.setCurrentQuantity(rs.getString("CURRENT_QUANTITY"));
                    bean.setTotalQuantity(rs.getString("TOTAL_QUANTITY"));
                    bean.setUom(rs.getString("UOM"));
                    bean.setItemCost(rs.getString("ITEMCOST"));
                    bean.setProjectNumber(rs.getString("PROJECTNUMBER"));
                    bean.setTaskNumber(rs.getString("TASKNUMBER"));


                    ObjectMapper mapper = new ObjectMapper();
                    String s;
                    s = mapper.writeValueAsString(bean);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bean;
        }
}
