package com.appspro.nexuscc.dao;

import biPReports.RestHelper;

import com.appspro.nexuscc.bean.ProjectBean;

import com.appspro.nexuscc.bean.ProjectVersionBean;

import biPReports.RestHelper;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import com.appspro.nexuscc.bean.ProjectTasksBean;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProjectVersionDAO extends RestHelper {
    
    public static void main(String[] args) {
      
    }
    
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public ArrayList<ProjectVersionBean> getProVersionDeatils(String projectId) {
        ArrayList<ProjectVersionBean> projectList = new ArrayList<ProjectVersionBean>();
        String finalresponse = "";
        //        String jwttoken = jwt.trim();
        ProjectVersionBean pro = new ProjectVersionBean();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getProjectVersionUrl() + "?q=ProjectId=" + projectId;
        System.out.println("response"+serverUrl);

        //getInstanceUrl()+

        String jsonResponse = "";
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            System.out.println("response"+obj);
            JSONArray arr = obj.getJSONArray("items");

            
            for (int i = 0; i < arr.length(); i++) {
                ProjectVersionBean bean = new ProjectVersionBean();
                JSONObject proObj = arr.getJSONObject(i);
                bean.setPlanVersionId(proObj.getLong("PlanVersionId"));
                
                projectList.add(bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return projectList;
    }
    
}
