package com.appspro.db;

import java.util.ResourceBundle;

/**
 *
 * @author user
 */
public class CommonConfigReader {

    private static ResourceBundle bundle = null;

    public static String getValue(String key) {
        if (bundle == null) {
            bundle = BundelReader.loadResourceBundle("connNew", "en");
        }
        return bundle.getString(key);
    }
}
