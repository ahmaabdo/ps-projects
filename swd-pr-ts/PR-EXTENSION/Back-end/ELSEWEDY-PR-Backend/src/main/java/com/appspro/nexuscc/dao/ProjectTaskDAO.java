package com.appspro.nexuscc.dao;


import biPReports.RestHelper;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import com.appspro.nexuscc.bean.ProjectTasksBean;


import org.json.JSONArray;
import org.json.JSONObject;

public class ProjectTaskDAO extends RestHelper {
   
    public ArrayList<ProjectTasksBean> getProTasksDetails(String TaskUrl) {
        ArrayList<ProjectTasksBean> projectList = new ArrayList<ProjectTasksBean>();
        String finalresponse = "";
        //        String jwttoken = jwt.trim();
        ProjectTasksBean pro = new ProjectTasksBean();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = TaskUrl;
        //getInstanceUrl()+
        
        System.out.println(serverUrl);             

        String jsonResponse = "";
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
             //   trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
          //  System.out.println("response"+obj);
            JSONArray arr = obj.getJSONArray("items");
                      
            for (int i = 0; i < arr.length(); i++) {
                ProjectTasksBean bean = new ProjectTasksBean();
                JSONObject proObj = arr.getJSONObject(i);
                bean.setTaskName(proObj.getString("TaskName"));
                bean.setTaskNumber(proObj.getString("TaskNumber"));
                bean.setTaskId(proObj.getLong("TaskId"));
              //  bean.setNeedByDate(proObj.getString("TaskOrganizationName"));
                bean.setExpenditureOrganization(proObj.has("TaskOrganizationName") ?proObj.optString("TaskOrganizationName") :null);
               // bean.setExpenditureType(proObj.getString("ProjectStatus"));
                bean.setExpenditureDateTo(proObj.has("TaskFinishDate") ?proObj.optString("TaskFinishDate") :null);
                bean.setExpenditureDateFrom(proObj.has("TaskStartDate") ?proObj.optString("TaskStartDate") :null);

                projectList.add(bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return projectList;
    }
}
