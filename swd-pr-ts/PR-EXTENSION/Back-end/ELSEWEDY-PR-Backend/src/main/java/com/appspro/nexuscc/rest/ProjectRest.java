package com.appspro.nexuscc.rest;

import com.appspro.nexuscc.bean.ProjectBean;

import com.appspro.nexuscc.dao.projectDAO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;


@Path("/projects")

public class ProjectRest {
   
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectDetails(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        ProjectBean obj = new ProjectBean();
        projectDAO pro = new projectDAO();

        ArrayList<ProjectBean> projectList = pro.getProDetails();
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(projectList);
//        new String(jsonInString.getBytes(), "UTF-8")
        return Response.ok(jsonInString, MediaType.APPLICATION_JSON).build();

    }

}
