package com.appspro.nexuscc.rest;


import biPReports.BIPReports;

import biPReports.BIReportModel;

import com.appspro.nexuscc.bean.SummaryReportBean;

import com.appspro.nexuscc.dao.SummaryReportDAO;

import java.io.BufferedReader;
import java.io.InputStream;

import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.XML;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


@Path("/report")
public class BIReportService {

    @POST
    @Path("/commonbireport")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getBiReport(String incomingData, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        HttpSession session = request.getSession();
        String authorization =
            session.getAttribute("authorization") != null ? session.getAttribute("authorization").toString() :
            null;

        JSONObject jObject = null; // json
        JSONArray reportListJSON = null;
        String fresponse = null;
        String reportName = null;
        try {
            if (incomingData != null && !incomingData.isEmpty()) {

                Object jsonTokner = new JSONTokener(incomingData).nextValue();
                if (jsonTokner instanceof JSONObject) {
                    jObject = new JSONObject(incomingData);
                    reportName = jObject.getString("reportName");
                } else {
                    reportListJSON = new JSONArray(incomingData);
                }


                BIPReports biPReports = new BIPReports();
                biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
                biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

                if (reportName != null) {

                    if (reportName.equals(BIPReports.REPORT_NAME.FUSE_REPORT.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.FUSE_REPORT.getValue());

                        if (resposne != null) {
                            if (!resposne.toString().isEmpty()) {
                                return Response.ok(resposne,
                                                   MediaType.APPLICATION_JSON).build();
                            }
                        }
                        //                    for (int i = 0;
                        //                         i < BIPReports.FUSE_REPORT_PARAM.values().length;
                        //                         i++) {
                        //                        paramName =
                        //                                BIPReports.FUSE_REPORT_PARAM.values()[i].getValue();
                        //                        biPReports.getParamMap().put(paramName,
                        //                                                     jObject.getString(paramName));
                        //                    }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.FUSE_REPORT.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();

                        session.setAttribute(BIPReports.REPORT_NAME.FUSE_REPORT.getValue(),
                                             jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.GET_ALL_POSITIONS.getValue())) {

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.GET_ALL_POSITIONS.getValue());
                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.EIT_REPORT.getValue())) {
                        String paramName = null;
                        for (int i = 0;
                             i < BIPReports.EIT_REPORT_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.EIT_REPORT_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.EIT_REPORT.getValue());
                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.All_EIT_REPORT.getValue())) {
                        String paramName = null;
                        for (int i = 0;
                             i < BIPReports.All_EIT_REPORT_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.All_EIT_REPORT_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.All_EIT_REPORT.getValue());
                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.MasterDataReport.getValue())) {
                        String paramName = null;
                        for (int i = 0;
                             i < BIPReports.MasterDataReport_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.MasterDataReport_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.MasterDataReport.getValue());
                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //  //("Response");
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("USERROLE").toString().replaceAll("VALUE",
                                                                                                      "value").replaceAll("LABEL",
                                                                                                                          "label");
                        JSONObject myString = new JSONObject();
                        myString.put("USERROLE", jsonString);
                        myString.put("FUSEREPORT",
                                     xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("FUSEREPORT").toString());
                        myString.put("ALLEITSEGMENT",
                                     xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("ALLEITSEGMENT").toString().replaceAll("VALUE",
                                                                                                                             "value").replaceAll("LABEL",
                                                                                                                                                 "label"));
                        myString.put("EITNAME",
                                     xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("EITNAME").toString());
                       // System.out.println(xmlJSONObj.getJSONObject("DATA_DS").toString());
                        myString.put("personalDetails",
                                     xmlJSONObj.getJSONObject("DATA_DS").get("PERSONALDETAILS").toString());

                        //                    jsonString =
                        //                    xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("FUSEREPORT").toString();

                        return Response.ok(myString.toString(),
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue())) {
                        String paramName = null;

                        for (int i = 0;
                             i < BIPReports.Dynamic_Rrport_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.Dynamic_Rrport_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName).replaceAll(">",
                                                                                                 "GT").replaceAll("<",
                                                                                                                  "LT"));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());
                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());

                        String jsonString =
                            xmlJSONObj.getJSONObject("ROWSET").get("ROW").toString().replaceAll("VALUE",
                                                                                                "value").replaceAll("LABEL",
                                                                                                                    "label");

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.Dynamic_Validation_Rrport.getValue())) {
                        String paramName = null;

                        for (int i = 0;
                             i < BIPReports.Dynamic_Rrport_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.Dynamic_Rrport_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName).replaceAll(">",
                                                                                                 "GT").replaceAll("<",
                                                                                                                  "LT"));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());
                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());

                        String jsonString =
                            xmlJSONObj.getJSONObject("ROWSET").get("ROW").toString().replaceAll("VALUE",
                                                                                                "value");

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.contains(BIPReports.REPORT_NAME.MoeEmployeeLetter.getValue())) {

                        String paramName = null;

                        for (int i = 0;
                             i < BIPReports.MoeEmployeeLetter_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.MoeEmployeeLetter_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.MoeEmployeeLetter.getValue());
                        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                        byte[] reportBytes = biPReports.executeReportsPDF();

                        fresponse =
                                javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                        return Response.ok("{" + "\"REPORT\":" + "\"" +
                                           fresponse + "\"" + "}",
                                           MediaType.APPLICATION_JSON).build();
                    } else if (reportName.equals(BIPReports.REPORT_NAME.workerData.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.workerData.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.WORKER_Data_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.WORKER_Data_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.workerData.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.workerData.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.MOEnewAbsence.getValue())) { // start  validation

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.MOEnewAbsence.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.MOEnewAbsence.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.MOEnewAbsence.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE_OVERTIMEPayment.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE_OVERTIMEPayment.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE_OVERTIMEPayment.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE_OVERTIMEPayment.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE_MissionPayment.getValue())) {
                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE_MissionPayment.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE_MissionPayment.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_ABS_CREATE_DECREE_MissionPayment.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_OVERTIME_PAYMENT.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_OVERTIME_PAYMENT.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_OVERTIME_PAYMENT.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_OVERTIME_PAYMENT.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_MISSION.getValue())) {
                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_MISSION.getValue());
                        //
                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_MISSION.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_MISSION.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_PAYMENT.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_PAYMENT.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_PAYMENT.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_REQUEST_BUSINESS_PAYMENT.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_REQUEST.getValue())) {
                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_REQUEST.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_REQUEST.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_REQUEST.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_PAYMENT.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_PAYMENT.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_PAYMENT.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_OVERTIME_PAYMENT.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_MISSION.getValue())) {
                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_MISSION.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_MISSION.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_MISSION.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_PAYMENT.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_PAYMENT.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_PAYMENT.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_OVERTIME_PAYMENT_BUSINESS_PAYMENT.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_REQUEST.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_REQUEST.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_REQUEST.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_REQUEST.getValue(),
                                             jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_PAYMENT.getValue())) {
                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_PAYMENT.getValue());
                        //
                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_PAYMENT.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_OVERTIME_PAYMENT.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_MISSION.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_MISSION.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_MISSION.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_MISSION.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_PAYMENT.getValue())) {
                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_PAYMENT.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_PAYMENT.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_PAYMENT_BUSINESS_PAYMENT.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_OVERTIME_REQUEST.getValue())) {
                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_OVERTIME_REQUEST.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_OVERTIME_REQUEST.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_OVERTIME_REQUEST.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_MISSION.getValue())) {
                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_MISSION.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_MISSION.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_MISSION.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_PAYMENT.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_PAYMENT.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.validation_New_Absence_Param.values().length;
                             i++) {
                            paramName =
                                    BIPReports.validation_New_Absence_Param.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_PAYMENT.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        //                    session.setAttribute(BIPReports.REPORT_NAME.ValidationOVERLAB_BUSINESS_MISSION_BUSINESS_PAYMENT.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.UserAuthenticationToShowSetUp.getValue())) { //User Authentication

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.UserAuthenticationToShowSetUp.getValue());

                        if (resposne != null) {
                            if (!resposne.toString().isEmpty()) {
                                return Response.ok(resposne.toString(),
                                                   MediaType.APPLICATION_JSON).build();
                            }
                        }
                        for (int i = 0;
                             i < BIPReports.UserAuthenticatio_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.UserAuthenticatio_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.UserAuthenticationToShowSetUp.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        //                   String jsonString
                        //                           = xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        session.setAttribute(BIPReports.REPORT_NAME.UserAuthenticationToShowSetUp.getValue(),
                                             jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();
                    }
                    
                    
                    //dur
                                                        else if (reportName.equals(BIReportModel.REPORT_NAME.GET_TERRITORIES_REPORT.getValue())) {
                                                            biPReports.setReportAbsolutePath(BIReportModel.REPORT_NAME.GET_TERRITORIES_REPORT.getValue());
                                                            fresponse = biPReports.executeReports();
                                                            JSONObject xmlJSONObj =
                                                                XML.toJSONObject(fresponse.toString());
                                        
                                                            if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                                                fresponse =
                                                                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
                                                            }
                                        
                                                        } else if (reportName.contains(BIReportModel.REPORT_NAME.GET_CITY_BY_COUNTRY_REPORT.getValue())) {
                                                            String paramName = null;
                                                            //                    System.out.println(subReportName);
                                                            for (int i = 0;
                                                                 i < BIReportModel.GET_CITY_BY_COUNTRY_REPORT_PARAM.values().length;
                                                                 i++) {
                                                                paramName =
                                                                        BIReportModel.GET_CITY_BY_COUNTRY_REPORT_PARAM.values()[i].getValue();
                                                                biPReports.getParamMap().put(paramName,
                                                                                             jObject.getString(paramName));
                                                            }
                                        
                                                            biPReports.setReportAbsolutePath(BIReportModel.REPORT_NAME.GET_CITY_BY_COUNTRY_REPORT.getValue());
                                                            fresponse = biPReports.executeReports();
                                        
                                                            JSONObject xmlJSONObj =
                                                                XML.toJSONObject(fresponse.toString());
                                        
                                                            if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                                                fresponse =
                                                                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
                                                            } else {
                                                                fresponse = "";
                                                            }
                                        
                                                        } else if (reportName.contains(BIReportModel.REPORT_NAME.GET_RATE_REPORT.getValue())) {
                                                            String paramName = null;
                                                            //                    System.out.println(subReportName);
                                                            for (int i = 0;
                                                                 i < BIReportModel.GET_RATE_REPORT_PARAM.values().length;
                                                                 i++) {
                                                                paramName =
                                                                        BIReportModel.GET_RATE_REPORT_PARAM.values()[i].getValue();
                                                                biPReports.getParamMap().put(paramName,
                                                                                             jObject.getString(paramName));
                                                            }
                                        
                                                            biPReports.setReportAbsolutePath(BIReportModel.REPORT_NAME.GET_RATE_REPORT.getValue());
                                                            fresponse = biPReports.executeReports();
                                        
                                                            JSONObject xmlJSONObj =
                                                                XML.toJSONObject(fresponse.toString());
                                        
                                                            if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                                                fresponse =
                                                                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
                                                            } else {
                                                                fresponse = "";
                                                            }
                                        
                                                        } else if (reportName.equals(BIReportModel.REPORT_NAME.GET_JOB_RATE.getValue())) {
                                                            String paramName = null;
                                                            Object resposne =
                                                                session.getAttribute(BIReportModel.REPORT_NAME.GET_JOB_RATE.getValue());
                                        
                                        
                                                            for (int i = 0;
                                                                 i < BIReportModel.GET_JOB_RATE_REPORT_PARAM.values().length;
                                                                 i++) {
                                                                paramName =
                                                                        BIReportModel.GET_JOB_RATE_REPORT_PARAM.values()[i].getValue();
                                                                biPReports.getParamMap().put(paramName,
                                                                                             jObject.getString(paramName));
                                                            }
                                        
                                                            biPReports.setReportAbsolutePath(BIReportModel.REPORT_NAME.GET_JOB_RATE.getValue());
                                                            fresponse = biPReports.executeReports();
                                                            JSONObject xmlJSONObj =
                                                                XML.toJSONObject(fresponse.toString());
                                                            String jsonString = "";
                                                            if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                                                jsonString =
                                                                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
                                                            }
                                        
                                                            return Response.ok(jsonString.toString(),
                                                                               MediaType.APPLICATION_JSON).build();
                                        
                                        
                                                        } else if (reportName.equals(BIReportModel.REPORT_NAME.GET_MANAGER_JOB_RATE.getValue())) {
                                                            String paramName = null;
                                                            Object resposne =
                                                                session.getAttribute(BIReportModel.REPORT_NAME.GET_MANAGER_JOB_RATE.getValue());
                                        
                                                            for (int i = 0;
                                                                 i < BIReportModel.GET_MANAGER_JOB_RATE_REPORT_PARAM.values().length;
                                                                 i++) {
                                                                paramName =
                                                                        BIReportModel.GET_MANAGER_JOB_RATE_REPORT_PARAM.values()[i].getValue();
                                                                biPReports.getParamMap().put(paramName,
                                                                                             jObject.getString(paramName));
                                                            }
                                        
                                                            biPReports.setReportAbsolutePath(BIReportModel.REPORT_NAME.GET_MANAGER_JOB_RATE.getValue());
                                                            fresponse = biPReports.executeReports();
                                                            JSONObject xmlJSONObj =
                                                                XML.toJSONObject(fresponse.toString());
                                                            String jsonString = "";
                                                            if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                                                jsonString =
                                                                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
                                                            }
                                                            return Response.ok(jsonString.toString(),
                                                                               MediaType.APPLICATION_JSON).build();
                                        
                                        
                                                        }
                                                        //                get aor
                                                        else if (reportName.equals(BIReportModel.REPORT_NAME.GET_AOR_REPORT.getValue())) {
                                                            String paramName = null;
                                                            Object resposne =
                                                                session.getAttribute(BIReportModel.REPORT_NAME.GET_AOR_REPORT.getValue());
                                                            System.out.print(resposne);
                                                            if (resposne != null) {
                                                                return Response.ok(resposne.toString(),
                                                                                   MediaType.APPLICATION_JSON).build();
                                        
                                        
                                                            }
                                                            for (int i = 0;
                                                                 i < BIReportModel.GET_AOR_REPORT_PARAM.values().length;
                                                                 i++) {
                                                                paramName =
                                                                        BIReportModel.GET_AOR_REPORT_PARAM.values()[i].getValue();
                                                                biPReports.getParamMap().put(paramName,
                                                                                             jObject.getString(paramName));
                                                            }
                                                            biPReports.setReportAbsolutePath(BIReportModel.REPORT_NAME.GET_AOR_REPORT.getValue());
                                                            fresponse = biPReports.executeReports();
                                                            JSONObject xmlJSONObj =
                                                                XML.toJSONObject(fresponse.toString());
                                                            String jsonString = "";
                                                            if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                                                jsonString =
                                                                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
                                                            }
                                        
                                                            return Response.ok(jsonString.toString(),
                                                                               MediaType.APPLICATION_JSON).build();
                                        
                                                        } else if (reportName.equals(BIReportModel.REPORT_NAME.GET_UDT_REPORT.getValue())) {
                                                            String paramName = null;
                                        
                                                            for (int i = 0;
                                                                 i < BIReportModel.UDT_REPORT_PARAM.values().length;
                                                                 i++) {
                                                                paramName =
                                                                        BIReportModel.UDT_REPORT_PARAM.values()[i].getValue();
                                                                biPReports.getParamMap().put(paramName,
                                                                                             request.getParameter(paramName));
                                                            }
                                        
                                                            biPReports.setReportAbsolutePath(BIReportModel.REPORT_NAME.GET_UDT_REPORT.getValue());
                                                            fresponse = biPReports.executeReports();
                                                            JSONObject xmlJSONObj =
                                                                XML.toJSONObject(fresponse.toString());
                                                            String jsonString = "";
                                                            if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                                                jsonString =
                                                                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
                                                            }
                                        
                                                            return Response.ok(jsonString.toString(),
                                                                               MediaType.APPLICATION_JSON).build();
                                        
                                                        }
                                                        //dur
                                                        //Dur Job
                                                        else if (reportName.equals(BIPReports.REPORT_NAME.absence_salary_in_advance_new.getValue())) {
                                                            biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.absence_salary_in_advance_new.getValue());
                                                            fresponse = biPReports.executeReports();
                                                            JSONObject xmlJSONObj =
                                                                XML.toJSONObject(fresponse.toString());
                                                            if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                                                fresponse =
                                                                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
                                                            } else {
                                                                fresponse = "";
                                                            }
                                                        }
                                                        //End of dur Job


                    else if (reportName.equals(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue())) {
                        String paramName = null;

                        for (int i = 0;
                             i < BIPReports.get_Query_For_List_Report_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.get_Query_For_List_Report_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));

                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue());
                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                 "value").replaceAll("LABEL",
                                                                                                                     "label");

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.EIT_Name.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.EIT_Name.getValue());

                        //                    if (resposne != null) {
                        //                        if (!resposne.toString().isEmpty()) {
                        //                            return Response.ok(resposne.toString(), MediaType.APPLICATION_JSON).build();
                        //                        }
                        //                    }
                        for (int i = 0;
                             i < BIPReports.EIT_Name_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.EIT_Name_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.EIT_Name.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();

                        //                    session.setAttribute(BIPReports.REPORT_NAME.EIT_Name.getValue(),
                        //                            jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else if (reportName.equals(BIPReports.REPORT_NAME.EmployeesByDepartmentName.getValue())) {

                        String paramName = null;
                        Object resposne =
                            session.getAttribute(BIPReports.REPORT_NAME.EmployeesByDepartmentName.getValue());

                        if (resposne != null) {
                            if (!resposne.toString().isEmpty()) {
                                return Response.ok(resposne.toString(),
                                                   MediaType.APPLICATION_JSON).build();
                            }
                        }
                        for (int i = 0;
                             i < BIPReports.Employees_REPORT_PARAM.values().length;
                             i++) {
                            paramName =
                                    BIPReports.Employees_REPORT_PARAM.values()[i].getValue();
                            biPReports.getParamMap().put(paramName,
                                                         jObject.getString(paramName));
                        }

                        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.EmployeesByDepartmentName.getValue());
                        fresponse = biPReports.executeReports();

                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());
                        String jsonString =
                            xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();

                        session.setAttribute(BIPReports.REPORT_NAME.EmployeesByDepartmentName.getValue(),
                                             jsonString);

                        return Response.ok(jsonString,
                                           MediaType.APPLICATION_JSON).build();

                    } else {
//                        List<SummaryReportBean> list =(List<SummaryReportBean>)session.getAttribute("Reports");
                        //   //("Report cahing List "+list.toString());

                        String paramName;
                        //                    List<String> paramValues = new ArrayList<String>();
                        //                    List<String> paramNameList =
                        //                        Arrays.asList("id", "report_Name", "parameter1",
                        //                                      "parameter2", "parameter3", "parameter4",
                        //                                      "parameter5","reportTypeVal");

                        SummaryReportDAO summary = new SummaryReportDAO();
                        SummaryReportBean reportBeans =
                            new SummaryReportBean();
//                        if (list != null) {
//                            for (int x = 0; x < list.size(); x++) {
//                                System.out.println("List: " + list.get(x).getReport_Name().toString());
//                                System.out.println("reportName: " + reportName);
//                                if ((list.get(x).getReport_Name().toString()) != null && reportName != null) {
//                                    if (list.get(x).getReport_Name().toString().equals(reportName)) {
//                                        reportBeans = list.get(x);
//                                        break;
//                                    }
//                                }
//                            }
//                        } else {
//                            SummaryReportDAO service = new SummaryReportDAO();
//                            List<SummaryReportBean> listCashing = service.getAllSummary();
//                            session.setAttribute("Reports", listCashing);

                            ArrayList arr =
                                summary.getReportParamSummary(reportName);
                            if (!arr.isEmpty()) {
                                reportBeans = (SummaryReportBean)arr.get(0);
                            }

//                        }


                        if (reportBeans.getParameter1() != null) {
                            paramName = reportBeans.getParameter1();
                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }


                        }
                        if (reportBeans.getParameter2() != null) {
                            paramName = reportBeans.getParameter2();

                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }


                        }
                        if (reportBeans.getParameter3() != null) {
                            paramName = reportBeans.getParameter3();
                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }

                        }
                        if (reportBeans.getParameter4() != null) {
                            paramName = reportBeans.getParameter4();
                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }

                        }
                        if (reportBeans.getParameter5() != null) {
                            paramName = reportBeans.getParameter5();
                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }

                        }

                        if (reportBeans.getReportTypeVal() != null) {
                            if (reportBeans.getReportTypeVal().equals("xml")) {

                                biPReports.setReportAbsolutePath(reportBeans.getReport_Name());

                                //                    for (SummaryReportBean reportBeans : arr) {
                                //
                                ////                        paramValues.add(reportBeans.getId());
                                ////                        paramValues.add(reportBeans.getReport_Name());
                                ////                        paramValues.add(reportBeans.getParameter1());
                                ////                        paramValues.add(reportBeans.getParameter2());
                                ////                        paramValues.add(reportBeans.getParameter3());
                                ////                        paramValues.add(reportBeans.getParameter4());
                                ////                        paramValues.add(reportBeans.getParameter5());
                                //
                                ////                        for (int i = 0; i < paramValues.size(); i++) {
                                ////                            paramName = paramNameList.get(i);
                                ////                            biPReports.getParamMap().put(paramName, jObject.getString(paramName));
                                ////                            biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue());
                                ////                        }
                                //
                                //                     }
                                biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                                System.out.println(reportBeans.getReport_Name());

                                System.out.println("Loggggggggggggggggggggggg");
                                System.out.println(biPReports.executeReports());
                                fresponse = biPReports.executeReports();
                                JSONObject xmlJSONObj =
                                    XML.toJSONObject(fresponse.toString());

                                if (!xmlJSONObj.isNull("DATA_DS") &&
                                    !xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                    if (xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) {

                                        return Response.ok("[]",
                                                           MediaType.TEXT_PLAIN).build();
                                    } else {
                                        return Response.ok(xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                                                "value").replaceAll("LABEL",
                                                                                                                                                    "label"),
                                                           MediaType.APPLICATION_JSON).build();
                                    }
                                } else {
                                    return Response.ok("[]",
                                                       MediaType.TEXT_PLAIN).build();
                                }
                            } else if (reportBeans.getReportTypeVal().equals("pdf")) {

                                biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                                biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                                biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                                byte[] reportBytes =
                                    biPReports.executeReportsPDF();

                                fresponse =
                                        javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                                return Response.ok("{" + "\"REPORT\":" + "\"" +
                                                   fresponse + "\"" + "}",
                                                   MediaType.APPLICATION_JSON).build();

                            }
                        }

                    }
                } else {
                    List<BIReportModel> threadReport =
                        new ArrayList<BIReportModel>();


                    JSONArray reportResponeArray = new JSONArray();
                    SummaryReportBean reportBeans = new SummaryReportBean();

                    SummaryReportDAO service = new SummaryReportDAO();
                    Map<String, SummaryReportBean> mapReportParam =
                        new HashMap<String, SummaryReportBean>();
                    mapReportParam = service.getAllSummaryMapped();
                    for (int in = 0; in < reportListJSON.length(); in++) {
                        BIPReports biPReportsThread = new BIPReports();
                        biPReportsThread.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
                        biPReportsThread.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

                        JSONObject reportDetails =
                            reportListJSON.getJSONObject(in).getJSONObject("nameSeg");
                        reportName = reportDetails.getString("reportName");
                        //   //("Report cahing List "+list.toString());

                        String paramName;
                        //                    List<String> paramValues = new ArrayList<String>();
                        //                    List<String> paramNameList =
                        //                        Arrays.asList("id", "report_Name", "parameter1",
                        //                                      "parameter2", "parameter3", "parameter4",
                        //                                      "parameter5","reportTypeVal");


                        reportBeans = mapReportParam.get(reportName);
                        if (reportBeans == null) {
                            continue;
                        }

                        if (reportBeans.getParameter1() != null) {
                            paramName = reportBeans.getParameter1();
                            if (reportDetails.has(paramName)) {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   reportDetails.get(paramName).toString());
                            } else {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   "");
                            }


                        }
                        if (reportBeans.getParameter2() != null) {
                            paramName = reportBeans.getParameter2();

                            if (reportDetails.has(paramName)) {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   reportDetails.get(paramName).toString());
                            } else {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   "");
                            }


                        }
                        if (reportBeans.getParameter3() != null) {
                            paramName = reportBeans.getParameter3();
                            if (reportDetails.has(paramName)) {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   reportDetails.get(paramName).toString());
                            } else {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   "");
                            }

                        }
                        if (reportBeans.getParameter4() != null) {
                            paramName = reportBeans.getParameter4();
                            if (reportDetails.has(paramName)) {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   reportDetails.get(paramName).toString());
                            } else {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   "");
                            }

                        }
                        if (reportBeans.getParameter5() != null) {
                            paramName = reportBeans.getParameter5();
                            if (reportDetails.has(paramName)) {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   reportDetails.get(paramName).toString());
                            } else {
                                biPReportsThread.getParamMap().put(paramName,
                                                                   "");
                            }

                        }

                        if (reportBeans.getReportTypeVal() != null) {
                            if (reportBeans.getReportTypeVal().equals("xml")) {

                                biPReportsThread.setReportAbsolutePath(reportBeans.getReport_Name());

                                biPReportsThread.setSegmentName(reportDetails.getString("segmentName"));
                                threadReport.add(biPReportsThread);

                            } else if (reportBeans.getReportTypeVal().equals("pdf")) {

                                biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                                biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                                biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                                byte[] reportBytes =
                                    biPReports.executeReportsPDF();

                                fresponse =
                                        javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                                return Response.ok("{" + "\"REPORT\":" + "\"" +
                                                   fresponse + "\"" + "}",
                                                   MediaType.APPLICATION_JSON).build();

                            }
                        }
                    }

                    try {

                        BIReportModel.THREAD_REPORT = threadReport;
                        List<String> responseList =
                            new BIReportModel().runMultipleReports();

                        for (int t = 0; t < responseList.size(); t++) {
                            String[] responeArray =
                                responseList.get(t).split("SPLIT");
                            fresponse = responeArray[1];
                            JSONObject xmlJSONObj =
                                XML.toJSONObject(fresponse.toString());


                            if (xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1") ||
                                xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) {
                                reportResponeArray.put(new JSONObject().put("[]",
                                                                            new JSONObject()));

                            } else {
                                JSONObject jsonRespone =
                                    new JSONObject(xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                                        "value").replaceAll("LABEL",
                                                                                                                                            "label"));

                                reportResponeArray.put(new JSONObject().put(responeArray[0],
                                                                            jsonRespone));


                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();

                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } finally {
                        BIReportModel.THREAD_REPORT =
                                new ArrayList<BIReportModel>();
                        BIReportModel.THREAD_REPORT_COUNTER = -1;
                        BIReportModel.THREAD_REPORT_RESPONSE =
                                new ArrayList<String>();
                    }
                    return Response.ok(reportResponeArray.toString(),
                                       MediaType.APPLICATION_JSON).build();

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            //            return Response.status(500).entity(e.getMessage()).build();
        }

        // return HTTP response 200 in case of success
        return Response.ok(fresponse, MediaType.APPLICATION_JSON).build();
    }

    public static void doSomething(Node node) {
        // do something with the current node instead of System.out
        if (node.getNodeName().equals("G_1")) {
            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                Node currentNode = node.getChildNodes().item(i);

            }
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                doSomething(currentNode);
            }
        }
    }
    
    @POST
               @Path("/commonbireportt")
               @Consumes(MediaType.APPLICATION_JSON)
               public String getBiReportAbcense(InputStream incomingData /*, @Context HttpServletRequest request*/) {
                   StringBuilder crunchifyBuilder = new StringBuilder();
                   // HttpSession session = request.getSession();
                   try {
                       BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
                       String line = null;
                       while ((line = in.readLine()) != null) {
                           crunchifyBuilder.append(line);
                       }
                   } catch (Exception e) {
                       System.out.println("Error Parsing: - ");
                   }
                   JSONObject jObject;
                   String fresponse = null;
                   try {
                       jObject = new JSONObject(crunchifyBuilder.toString());
                       String reportName = jObject.getString("reportName");
                       BIReportModel biPReports = new BIReportModel();
                       biPReports.setAttributeFormat(BIReportModel.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML
                               .getValue());
                       biPReports.setAttributeTemplate(BIReportModel.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML
                               .getValue());
                       if (reportName != null) {
                           if (reportName.equals(BIReportModel.REPORT_NAME.absence_salary_in_advance_new
                                   .getValue())) {
                               biPReports.setReportAbsolutePath(BIReportModel.REPORT_NAME.absence_salary_in_advance_new
                                       .getValue());
                               fresponse = biPReports.executeReports();
                               JSONObject xmlJSONObj = XML.toJSONObject(fresponse.toString());
                                   if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                   fresponse = xmlJSONObj.getJSONObject("DATA_DS")
                                           .get("G_1")
                                           .toString();
                               } else {
                                   fresponse = "";
                               }
                           }
                       }
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
                   // return HTTP response 200 in case of success
                   return fresponse;
               }

}
