package com.appspro.sewedyts.dao;
import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.sewedyts.bean.SWD_EmployeeBean;




import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import static biPReports.RestHelper.getSchema_Name;
public class SWD_EmployeeDAO extends AppsproConnection {
    
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public SWD_EmployeeBean insertOrUpdateEmployee(SWD_EmployeeBean bean,
                                                      String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            if (transactionType.equals("ADD")) {

                String query =
                    "INSERT INTO " + getSchema_Name() + ".xx_sewedy_emp " +
                    "(EMPLOYEE_NAME,EMPLOYEE_NUMBER,ORGANIZATION,TIME_SHEET_DATE,TOTAL_STRAIGHT_TIME,TOTAL_OVER_TIME,UOM)" +
                    " VALUES (?,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getEmployeeName());
                ps.setString(2, bean.getEmployeeNumber());
                ps.setString(3, bean.getOrganization());
                ps.setString(4, bean.getTimeSheetDate());
                ps.setString(5, bean.getTotalStraightTime());
                ps.setString(6, bean.getTotalOverTime());
                ps.setString(7, bean.getUom());
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {
                String query =
                    "UPDATE " + getSchema_Name() + ".xx_sewedy_emp set EMPLOYEE_NAME=?,EMPLOYEE_NUMBER=?,ORGANIZATION=?,TIME_SHEET_DATE=?,TOTAL_STRAIGHT_TIME=?,TOTAL_OVER_TIME=?,UOM=? where ID=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getEmployeeName());
                ps.setString(2, bean.getEmployeeNumber());
                ps.setString(3, bean.getOrganization());
                ps.setString(4, bean.getTimeSheetDate());
                ps.setString(5, bean.getTotalStraightTime());
                ps.setString(6, bean.getTotalOverTime());
                ps.setString(7, bean.getUom());
                ps.setString(8, bean.getId());

               
                
                ps.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    public ArrayList<SWD_EmployeeBean> getAllEmp() {

        ArrayList<SWD_EmployeeBean> employeeList =
            new ArrayList<SWD_EmployeeBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from " + getSchema_Name() + ".xx_sewedy_emp";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                SWD_EmployeeBean employee = new SWD_EmployeeBean();
                
                employee.setId(rs.getString("ID"));
                employee.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                employee.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));
                employee.setOrganization(rs.getString("ORGANIZATION"));
                employee.setTimeSheetDate(rs.getString("TIME_SHEET_DATE"));
                employee.setTotalStraightTime(rs.getString("TOTAL_STRAIGHT_TIME"));
                employee.setTotalOverTime(rs.getString("TOTAL_OVER_TIME"));
                employee.setUom(rs.getString("UOM"));


                employeeList.add(employee);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return employeeList;

    }
    
    
    public String getDelete(String id) {

            connection = AppsproConnection.getConnection();
            String query;
            int checkResult=0;

            try {
                query = "DELETE FROM xx_sewedy_emp WHERE ID=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, id);
                checkResult = ps.executeUpdate();

            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
            return Integer.toString(checkResult);
        }
    
    
}
