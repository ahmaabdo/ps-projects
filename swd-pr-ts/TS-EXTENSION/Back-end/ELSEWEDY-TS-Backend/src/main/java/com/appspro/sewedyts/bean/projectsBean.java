package com.appspro.sewedyts.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.json.JSONArray;

public class projectsBean {
    private int id;
    private String projectName;
    private String projectNumber;
    private String transactionDate;
    private String taskNumber;
    private String taskName;
    private String expenditureType;
    private String employeeNumber;
    private String employeeName;
    private String timeSheetType;
    private String personName;
    private String personNumber;
    private String createBy;
    private String createDate;
    private String approvalStatus;
    private int quantity;
    private int cost;
    private String organization;
    private String uom;
    private String timeSheetDate;
    private String batchName;
    private String personId;
    private String mangerId;
    private String mangerOfManger;
    private String costControllerId;
    private String financeMangerId;
    private Long projectId;
    private String businessUnitName;
    private String rejectReason;
    private String importStatus;
    private String billable;
    private String projectOrgId;

    @JsonIgnore
    public JSONArray lis  ;

    public String links;
    public String TeamMembers;
    public String ProjectOrganization;
    
    
    public void setLinks(String links) {
        this.links = links;
    }

    public String getLinks() {
        return links;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setTransactionDate(String transactionData) {
        this.transactionDate = transactionData;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setExpenditureType(String Expendituretype) {
        this.expenditureType = Expendituretype;
    }

    public String getExpenditureType() {
        return expenditureType;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setTimeSheetType(String timeSheetType) {
        this.timeSheetType = timeSheetType;
    }

    public String getTimeSheetType() {
        return timeSheetType;
    }

    public void setQuantity(int Quantity) {
        this.quantity = Quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public void setLis(JSONArray lis) {
        this.lis = lis;
    }

    public JSONArray getLis() {
        return lis;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setOrganization(String ExpenditureOrganization) {
        this.organization = ExpenditureOrganization;
    }

    public String getOrganization() {
        return organization;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setMangerId(String mangerId) {
        this.mangerId = mangerId;
    }

    public String getMangerId() {
        return mangerId;
    }

    public void setMangerOfManger(String mangerOfManger) {
        this.mangerOfManger = mangerOfManger;
    }

    public String getMangerOfManger() {
        return mangerOfManger;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setTimeSheetDate(String timeSheetDate) {
        this.timeSheetDate = timeSheetDate;
    }

    public String getTimeSheetDate() {
        return timeSheetDate;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setCostControllerId(String costControllerId) {
        this.costControllerId = costControllerId;
    }

    public String getCostControllerId() {
        return costControllerId;
    }

    public void setFinanceMangerId(String financeMangerId) {
        this.financeMangerId = financeMangerId;
    }

    public String getFinanceMangerId() {
        return financeMangerId;
    }


    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setTeamMembers(String TeamMembers) {
        this.TeamMembers = TeamMembers;
    }

    public String getTeamMembers() {
        return TeamMembers;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setImportStatus(String importStatus) {
        this.importStatus = importStatus;
    }

    public String getImportStatus() {
        return importStatus;
    }

    public void setBillable(String billableFlag) {
        this.billable = billableFlag;
    }

    public String getBillable() {
        return billable;
    }

    public void setProjectOrganization(String ProjectOrganization) {
        this.ProjectOrganization = ProjectOrganization;
    }

    public String getProjectOrganization() {
        return ProjectOrganization;
    }

    public void setProjectOrgId(String projectOrgId) {
        this.projectOrgId = projectOrgId;
    }

    public String getProjectOrgId() {
        return projectOrgId;
    }
}
