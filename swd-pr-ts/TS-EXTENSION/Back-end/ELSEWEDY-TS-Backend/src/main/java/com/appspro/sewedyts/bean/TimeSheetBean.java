package com.appspro.sewedyts.bean;

public class TimeSheetBean {
    private String transactionData;
    private String projectNumber;
    private String projectName;
    private String taskNumber;
    private String taskName;
    private String expenditureType;
    private String timeSheetType;
    private String quantity;
    private String uom;
    private String id;
    private String empTsId;


    public void setTransactionData(String transactionData) {
        this.transactionData = transactionData;
    }

    public String getTransactionData() {
        return transactionData;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setExpenditureType(String expenditureType) {
        this.expenditureType = expenditureType;
    }

    public String getExpenditureType() {
        return expenditureType;
    }

    public void setTimeSheetType(String timeSheetType) {
        this.timeSheetType = timeSheetType;
    }

    public String getTimeSheetType() {
        return timeSheetType;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }



    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setEmpTsId(String empTsId) {
        this.empTsId = empTsId;
    }

    public String getEmpTsId() {
        return empTsId;
    }
}
