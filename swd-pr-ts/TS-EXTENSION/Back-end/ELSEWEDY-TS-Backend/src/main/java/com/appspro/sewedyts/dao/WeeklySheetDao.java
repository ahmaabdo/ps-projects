package com.appspro.sewedyts.dao;

import com.appspro.db.AppsproConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.JSONArray;
import org.json.JSONObject;

public class WeeklySheetDao extends AppsproConnection {
    Connection connection = null;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    public String createDataFromExcelSheet(String body) {
        try {
            String query = null;
            JSONArray jsonArray = new JSONArray(body);

            connection = AppsproConnection.getConnection();
            query ="insert into WEEKLY_TIME_SHEET (TRANSACTION_DATE,PROJECT_NAME,PROJECT_NUMBER,TASK_NAME,TASK_NUMBER,EXPENDITURE_TYPE,TIME_SHEET_TYPE,TOTAL_STRAIGHT_TIME\n" + 
            ",TOTAL_OVER_TIME,UOM)VALUES (?,?,?,?,?,?,?,?,?,?)";
            
            ps = connection.prepareStatement(query);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = jsonArray.getJSONObject(i);
                ps.setString(1, item.get("Transaction Date").toString());
                ps.setString(2, item.get("Project Name").toString());
                ps.setString(3, item.get("Project Number").toString());
                ps.setString(4, item.get("Task Name").toString());
                ps.setString(5, item.get("Task Number").toString());
                ps.setString(6, item.get("Expenditure Type").toString());
                ps.setString(7, item.get("Time Sheet Type").toString());
                ps.setString(8, item.get("Total Straight Time").toString());
                ps.setString(9, item.get("Total over Time").toString());
                ps.setString(10, item.get("uom").toString());

                ps.executeUpdate();
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return "";
    }
}
