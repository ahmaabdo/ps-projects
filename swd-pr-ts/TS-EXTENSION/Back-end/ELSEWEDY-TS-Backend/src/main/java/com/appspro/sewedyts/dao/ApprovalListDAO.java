package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.sewedyts.bean.ApprovalListBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

public class ApprovalListDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public void updateReqeustDate(String ID) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = " UPDATE  XX_APPROVAL_LIST SET REQUEST_DATE = SYSDATE WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, ID);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    public String getMinStepLeval(String transactionId, String serviceType) {
        String minStepLeval = "";
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT MIN(STEP_LEVEL) AS MIN_ID  FROM XX_APPROVAL_LIST "
                    + "WHERE TRANSACTION_ID = ? AND RESPONSE_CODE IS NULL AND SERVICE_TYPE = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionId);
            ps.setString(2, serviceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                minStepLeval = rs.getString("MIN_ID");

            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return minStepLeval;
    }
    public ApprovalListBean getNextApproval(String transactionId, String minStepLeval, String serviceType) {
        ApprovalListBean bean = new ApprovalListBean();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT * FROM XX_APPROVAL_LIST "
                    + "WHERE TRANSACTION_ID = ? AND RESPONSE_CODE IS NULL "
                    + "AND STEP_LEVEL = ? AND SERVICE_TYPE = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionId);
            ps.setString(2, minStepLeval);
            ps.setString(3, serviceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDate(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));

            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }
    public void updateApprovalList(String RESPONSE_CODE, String transactionId, String stepLeval, String serviceType) {
        try {
            //("ApprovalList ====> " + RESPONSE_CODE);
            //("ApprovalList ====> " + transactionId);
            //("ApprovalList ====> " + stepLeval);
            //("ApprovalList ====> " + serviceType);

            connection = AppsproConnection.getConnection();
            String query = null;
    //             query ="UPDATE  "+ " " + getSchema_Name() + ".XX_APPROVAL_LIST \n" +
    //"    SET \n" +
    //"        RESPONSE_CODE = ? ,\n" +
    //"        RESPONSE_DATE = SYSDATE \n" +
    //"     WHERE\n" +
    //"        TRANSACTION_ID = ?  \n" +
    //"        AND STEP_LEVEL = ?  \n" +
    //"        AND SERVICE_TYPE = ?";
            query = "UPDATE XX_APPROVAL_LIST SET  RESPONSE_DATE =SYSDATE , RESPONSE_CODE = ?    "
                    + " WHERE TRANSACTION_ID = ? "
                    + "AND STEP_LEVEL = ?"
                    + " AND SERVICE_TYPE = ?";

            //("ApprovalList ====> " + query);
            ps = connection.prepareStatement(query);
            ps.setString(1, RESPONSE_CODE);
            // ps.setDate(2, getCurrentDate());
            ps.setString(2, transactionId);
            ps.setString(3, stepLeval);
            ps.setString(4, serviceType);
            //("ApprovalList ====> " + query);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    public ArrayList<ApprovalListBean> getLastStepForApproval(String transActionId) {
           ArrayList<ApprovalListBean> approvalSetupList = new ArrayList<ApprovalListBean>();
           ApprovalListBean bean = null;
           connection = AppsproConnection.getConnection();
           
           
        String query = null;
              query = "select * from xx_approval_list where TRANSACTION_ID =?";
        try {
                   ps = connection.prepareStatement(query);
                   ps.setString(1, transActionId);
                   rs = ps.executeQuery();

                   while (rs.next()) {
                       bean = new ApprovalListBean();
                       bean.setId(rs.getString("ID"));
                       bean.setStepLeval(rs.getString("STEP_LEVEL"));
                       bean.setServiceType(rs.getString("SERVICE_TYPE"));
                       bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                       bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                       bean.setRolrType(rs.getString("ROLE_TYPE"));
                       bean.setRoleId(rs.getString("ROLE_ID"));
                       bean.setRequestDate(rs.getString("REQUEST_DATE"));
                       bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                       bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                       bean.setNote(rs.getString("NOTE"));
                       bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                       bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                       approvalSetupList.add(bean);
                   }

               } catch (Exception e) {
                   //("Error: ");
                   e.printStackTrace();
               } finally {
                   closeResources(connection, ps, rs);
               }
               return approvalSetupList;
    }
    public ApprovalListBean insertIntoApprovalList(ApprovalListBean bean) {
           try {
               connection = AppsproConnection.getConnection();
               String query = null;
               query = "insert into XX_APPROVAL_LIST (STEP_LEVEL,SERVICE_TYPE,TRANSACTION_ID,WORKFLOW_ID,ROLE_TYPE,ROLE_ID,REQUEST_DATE,RESPONSE_CODE," +
                   "NOTIFICATION_TYPE,NOTE,LINE_MANAGER_NAME,EMPLOYEE_NAME,PERSON_NUMBER,CREATE_BY,CREATE_DATE,RESPONSE_DATE)"
                       + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysDate)";
               ps = connection.prepareStatement(query);
               ps.setString(1, bean.getStepLeval());
               ps.setString(2, bean.getServiceType());
               ps.setString(3, bean.getTransActionId());
               ps.setString(4, bean.getWorkflowId());
               ps.setString(5, bean.getRolrType());
               ps.setString(6, bean.getRoleId());
               ps.setString(7, bean.getRequestDate());
               ps.setString(8, bean.getResponseCode());
               ps.setString(9, bean.getNotificationType());
               ps.setString(10, bean.getNote());            
               ps.setString(11, bean.getLineManagerName());
               ps.setString(12, bean.getPersonName());
               ps.setString(13, bean.getPersonNumber());
               ps.setString(14, bean.getCreateBy());
               ps.setString(15, bean.getCreateDate());
               // ps.setString(11, bean.getResponseDate());
               ps.executeUpdate();
               
               


           } catch (Exception e) {
               //("Error: ");
               e.printStackTrace();
           } finally {
               closeResources(connection, ps, rs);
           }

           return bean;
       }
    public ArrayList<ApprovalListBean> getApprovalType(String serviceType, String T_id) {
        ArrayList<ApprovalListBean> approvalSetupList = new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = " SELECT * FROM XX_APPROVAL_LIST WHERE SERVICE_TYPE = ? AND TRANSACTION_ID = ? order by step_level";
            ps = connection.prepareStatement(query);
            ps.setString(1, serviceType);
            ps.setString(2, T_id);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDate(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }
}
