package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.TaskDetailsBean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class TaskDetailsDao extends RestHelper  {
    
    public ArrayList<TaskDetailsBean> getProTasksDetails(String TaskUrl) {
        ArrayList<TaskDetailsBean> projectList = new ArrayList<TaskDetailsBean>();
        String finalresponse = "";
        TaskDetailsBean pro = new TaskDetailsBean();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = TaskUrl;
        
        String jsonResponse = "";
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
          
            JSONArray arr = obj.getJSONArray("items");
                      
            for (int i = 0; i < arr.length(); i++) {
                TaskDetailsBean bean = new TaskDetailsBean();
                JSONObject proObj = arr.getJSONObject(i);
                bean.setTaskName(proObj.getString("TaskName"));
                bean.setTaskNumber(proObj.getString("TaskNumber"));
                bean.setChargeableFlag(proObj.has("ChargeableFlag") ?proObj.optBoolean("ChargeableFlag"): null);
              // bean.setExpenditureOrganization(proObj.has("TaskOrganizationName") ? proObj.optString("TaskOrganizationName") : null);
             
                projectList.add(bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return projectList;
    }
}
