package com.appspro.sewedyts.bean;

public class UnprocessedCostBean {
    private Long unprocessedTransactionReferenceId;
    private String businessUnit;
    private Long transactionId ; 
    private String quantity ;

    public void setUnprocessedTransactionReferenceId(Long UnprocessedTransactionReferenceId) {
        this.unprocessedTransactionReferenceId = UnprocessedTransactionReferenceId;
    }

    public Long getUnprocessedTransactionReferenceId() {
        return unprocessedTransactionReferenceId;
    }

    public void setBusinessUnit(String BusinessUnit) {
        this.businessUnit = BusinessUnit;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setTransactionId(Long TransactionId) {
        this.transactionId = TransactionId;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setQuantity(String Quantity) {
        this.quantity = Quantity;
    }

    public String getQuantity() {
        return quantity;
    }
}
