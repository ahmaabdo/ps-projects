package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.sewedyts.bean.ApprovalListBean;
import com.appspro.sewedyts.bean.WorkFlowNotificationBean;
import com.appspro.sewedyts.bean.projectsBean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProjectCostDao extends RestHelper {
    Connection connection;
    PreparedStatement pre;
    ResultSet rs;
    AppsproConnection con = new AppsproConnection();
    ApprovalListDAO ald = new ApprovalListDAO();
    ApprovalListBean al = new ApprovalListBean();
    ApprovalListBean al2 = new ApprovalListBean();
    ApprovalListBean al3 = new ApprovalListBean();
    ApprovalListBean al4 = new ApprovalListBean();
    ApprovalListBean al5 = new ApprovalListBean();
    ApprovalListBean al6 = new ApprovalListBean();
    WorkFlowNotificationBean wf = new WorkFlowNotificationBean();
    WorkFlowNotificationBean wfMaster = new WorkFlowNotificationBean();
    WorkFlowNotificationDao wfd = new WorkFlowNotificationDao();
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public ArrayList<projectsBean> getProjectCostDetails() {
        ArrayList<projectsBean> projectList = new ArrayList<projectsBean>();
      
      
        boolean hasMore = false;
        int offset = 0;
        do {
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;


        String serverUrl = getInstanceUrl() + getProjectsCostUrl()+"?limit=500&offset="+offset;
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;

            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());
            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
            for (int i = 0; i < arr.length(); i++) {
                projectsBean bean = new projectsBean();
                JSONObject projectObj = arr.getJSONObject(i);
                bean.setProjectNumber(projectObj.getString("ProjectNumber"));
                bean.setProjectName(projectObj.getString("ProjectName"));
                bean.setTaskName(projectObj.getString("TaskName"));
                bean.setTaskNumber(projectObj.getString("TaskNumber"));
                bean.setExpenditureType(projectObj.getString("ExpenditureType"));
                bean.setCost(projectObj.getInt("BurdenedCostInProjectCurrency"));
                bean.setEmployeeNumber(projectObj.has("PersonNumber") ?
                                       projectObj.optString("PersonNumber") :
                                       null);
                bean.setBatchName(projectObj.has("UserExpenditureBatch") ?
                                       projectObj.optString("UserExpenditureBatch") :
                                       null);
                bean.setEmployeeName(projectObj.has("PersonName") ?
                                     projectObj.optString("PersonName") :
                                     null);
                bean.setTimeSheetType(projectObj.getString("DocumentEntry"));
                bean.setQuantity(projectObj.getInt("Quantity"));
                bean.setTransactionDate(projectObj.getString("ExpenditureItemDate"));
                bean.setOrganization(projectObj.has("ExpenditureBusinessUnit") ?
                                                projectObj.optString("ExpenditureBusinessUnit") :
                                                null);

                JSONArray linkes = projectObj.getJSONArray("links");
                bean.setLinks(linkes.toString());
                projectList.add(bean);
            }
            hasMore = obj.getBoolean("hasMore");
            offset += 500;
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
        }while(hasMore);
        return projectList;
    }

    public projectsBean insertToLocal(projectsBean bean) {
       
        try {
            String query = "";
            connection = con.getConnection();
            int request_id = 0;


            String sql =
                "Select NVL(MAX(ID),0)+1 as request_id from XX_PROJECT_COST";
            pre = connection.prepareStatement(sql);
            rs = pre.executeQuery();
            while (rs.next()) {
                request_id = rs.getInt("request_id");
            }
            System.out.println(request_id);
            query =
                    "insert into XX_PROJECT_COST (TRANSACTION_DATE,PROJECT_NAME,PROJECT_NUMBER,TASK_NAME,TASK_NUMBER,EXPENDITURE_TYPE,EMPLOYEE_NUMBER,EMPLOYEE_NAME,\n" +
                    "TIME_SHEET_TYPE,QUANTITY,COST,PERSON_NAME,PERSON_NUMBER,CREATE_BY,CREATE_DATE,APPROVAL_STATUS,BATCH_NAME,ORGANIZATION,TIME_SHEET_DATE,UOM,ID,IMPORT_STATUS,BILLABLE_FLAG,EXPENDITURE_ORG_ID)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            pre = connection.prepareStatement(query);
            pre.setString(1, bean.getTransactionDate());
            pre.setString(2, bean.getProjectName());
            pre.setString(3, bean.getProjectNumber());
            pre.setString(4, bean.getTaskName());
            pre.setString(5, bean.getTaskNumber());
            pre.setString(6, bean.getExpenditureType());
            pre.setString(7, bean.getEmployeeNumber());
            pre.setString(8, bean.getEmployeeName());
            pre.setString(9, bean.getTimeSheetType());
            pre.setInt(10, bean.getQuantity());
            pre.setInt(11, bean.getCost());
            pre.setString(12, bean.getPersonName());
            pre.setString(13, bean.getPersonNumber());
            pre.setString(14, bean.getCreateBy());
            pre.setString(15, bean.getCreateDate());
            pre.setString(16, bean.getApprovalStatus());
            
            pre.setString(17, bean.getBatchName());
            pre.setString(18, bean.getOrganization());
            pre.setString(19, bean.getTimeSheetDate());
            pre.setString(20, bean.getUom());
            pre.setInt(21, request_id);
            pre.setString(22, bean.getImportStatus());
            pre.setString(23, bean.getBillable());
            pre.setString(24, bean.getProjectOrgId());
            
            
            pre.executeUpdate();
            
            al.setStepLeval("0");
            al.setServiceType("XX_PROJECT_COST");
            al.setTransActionId(String.valueOf(request_id));
            al.setRolrType("EMP");
            al.setRoleId(bean.getPersonId());
            al.setResponseCode("SUBMIT");
            al.setNotificationType("FYI");
            al.setResponseDate(bean.getCreateDate());
            al.setPersonName(bean.getPersonName());
            ald.insertIntoApprovalList(al);
            
            al2.setStepLeval("1");
            al2.setServiceType("XX_PROJECT_COST");
            al2.setTransActionId(String.valueOf(request_id));
            al2.setRolrType("LINE_MANAGER");
            al2.setRoleId(bean.getMangerId());
            al2.setNotificationType("FYA");
            al2.setPersonName(bean.getPersonName());
            ald.insertIntoApprovalList(al2);
            
            al3.setStepLeval("2");
            al3.setServiceType("XX_PROJECT_COST");
            al3.setTransActionId(String.valueOf(request_id));
            al3.setRolrType("PROJECT_MANAGER");
            al3.setRoleId(bean.getMangerOfManger());
            al3.setNotificationType("FYA");
            al3.setPersonName(bean.getPersonName());
            ald.insertIntoApprovalList(al3);
            
            al4.setStepLeval("3");
            al4.setServiceType("XX_PROJECT_COST");
            al4.setTransActionId(String.valueOf(request_id));
            al4.setRolrType("COST_CONTROLLER");
            al4.setRoleId(bean.getCostControllerId());      
            al4.setNotificationType("FYI");                 
            al4.setPersonName(bean.getPersonName());
            ald.insertIntoApprovalList(al4);
            
            al5.setStepLeval("4");
            al5.setServiceType("XX_PROJECT_COST");
            al5.setTransActionId(String.valueOf(request_id));
            al5.setRolrType("FINANCE_MANAGER");
            al5.setRoleId(bean.getFinanceMangerId());
            al5.setNotificationType("FYI");                 
            al5.setPersonName(bean.getPersonName());
            ald.insertIntoApprovalList(al5);
            
            
            wf.setMsgTitle("Time Sheet Request");
            wf.setMsgBody("Time Sheet Request / " + bean.getPersonName());
            wf.setReceiverType(al2.getRolrType());
            wf.setReceiverId(al2.getRoleId());
            wf.setType("FYA");
            wf.setRequestId(String.valueOf(request_id));
            wf.setStatus("OPEN");
            wf.setSelfType("XX_PROJECT_COST");
            wf.setPersonName(bean.getPersonName());
            wfd.insertIntoWorkflow(wf);
            
            wfMaster.setMsgTitle("Time Sheet Request");
            wfMaster.setMsgBody("Time Sheet Request / " +
                                bean.getPersonName());
            wfMaster.setReceiverType("LINE_MANAGER");
            wfMaster.setReceiverId("300000002159490");
            wfMaster.setType("FYA");
            wfMaster.setRequestId(String.valueOf(request_id));
            wfMaster.setStatus("OPEN");
            wfMaster.setSelfType("XX_PROJECT_COST");
            wfMaster.setPersonName(bean.getPersonName());
            wfd.insertIntoWorkflow(wfMaster);
         
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.closeResources(connection, pre, rs);
        }

        return bean;
    }
    public List<projectsBean> getallrequest() {
        ArrayList<projectsBean> list = new ArrayList<projectsBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XX_PROJECT_COST ORDER BY ID DESC";
            pre = connection.prepareStatement(query);
            rs = pre.executeQuery();

            while (rs.next()) {
                projectsBean bean = new projectsBean();
                bean.setId(rs.getInt("ID"));
                bean.setProjectName(rs.getString("PROJECT_NAME"));
                bean.setProjectNumber(rs.getString("PROJECT_NUMBER"));
                bean.setTaskName(rs.getString("TASK_NAME"));
                bean.setTaskNumber(rs.getString("TASK_NUMBER"));
                bean.setTransactionDate(rs.getString("TRANSACTION_DATE"));
                bean.setExpenditureType(rs.getString("EXPENDITURE_TYPE"));
                bean.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                bean.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));
                bean.setTimeSheetType(rs.getString("TIME_SHEET_TYPE"));
                bean.setQuantity(rs.getInt("QUANTITY"));
                bean.setCost(rs.getInt("COST"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                bean.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
                
                bean.setUom(rs.getString("UOM"));
                bean.setBatchName(rs.getString("BATCH_NAME"));
                bean.setOrganization(rs.getString("ORGANIZATION"));
                bean.setTimeSheetDate(rs.getString("TIME_SHEET_DATE"));
                bean.setRejectReason(rs.getString("REJECT_REASON"));
                bean.setImportStatus(rs.getString("IMPORT_STATUS"));
                bean.setBillable(rs.getString("BILLABLE_FLAG"));
                bean.setProjectOrgId(rs.getString("EXPENDITURE_ORG_ID"));
                
                
                list.add(bean);

            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            con.closeResources(connection, pre, rs);
        }
        return list;
    }

    public projectsBean upDateStatus(projectsBean bean) {
        String query = "";

        try {
            connection = AppsproConnection.getConnection();
            query =
                    "UPDATE XX_PROJECT_COST set APPROVAL_STATUS =?, REJECT_REASON=? where id = ?";

            pre = connection.prepareStatement(query);
            pre.setString(1, bean.getApprovalStatus());
            pre.setString(2, bean.getRejectReason());
            pre.setInt(3, bean.getId());
            pre.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.closeResources(connection, pre, rs);
        }

        return bean;
    }
    
    public String updateImportStatus(String status , int id) {
        String query = "";

        try {
            connection = AppsproConnection.getConnection();
            query =
                    "UPDATE XX_PROJECT_COST set IMPORT_STATUS =? where id = ?";

            pre = connection.prepareStatement(query);
            pre.setString(1, status);
            pre.setInt(2, id);
            pre.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.closeResources(connection, pre, rs);
        }

        return "updated";
    }
    public ArrayList<projectsBean> searchbyid(String body) {
        ArrayList<projectsBean> beanArrList = new ArrayList<projectsBean>();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(body);
        JSONObject jsonObjInput;
        for (int i = 0; i < arr.length(); i++) {
            jsonObjInput = arr.getJSONObject(i);
            beanList.add(jsonObjInput);
        }
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XX_PROJECT_COST where id = ?";
            pre = connection.prepareStatement(query);
            for (JSONObject bean1 : beanList) {
                    pre.setInt(1,Integer.parseInt(bean1.get("id").toString()));
                    rs = pre.executeQuery();
                while (rs.next()) {
                    projectsBean bean = new projectsBean();
                    bean.setId(rs.getInt("ID"));
                    bean.setProjectName(rs.getString("PROJECT_NAME"));
                    bean.setProjectNumber(rs.getString("PROJECT_NUMBER"));
                    bean.setTaskName(rs.getString("TASK_NAME"));
                    bean.setTaskNumber(rs.getString("TASK_NUMBER"));
                    bean.setTransactionDate(rs.getString("TRANSACTION_DATE"));
                    bean.setExpenditureType(rs.getString("EXPENDITURE_TYPE"));
                    bean.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                    bean.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));
                    bean.setTimeSheetType(rs.getString("TIME_SHEET_TYPE"));
                    bean.setQuantity(rs.getInt("QUANTITY"));
                    bean.setCost(rs.getInt("COST"));
                    bean.setPersonName(rs.getString("PERSON_NAME"));
                    bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                    bean.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
                    
                    bean.setUom(rs.getString("UOM"));
                    bean.setBatchName(rs.getString("BATCH_NAME"));
                    bean.setOrganization(rs.getString("ORGANIZATION"));
                    bean.setTimeSheetDate(rs.getString("TIME_SHEET_DATE"));
                    
                
                    beanArrList.add(bean);
                }
                
            }
               

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            con.closeResources(connection, pre, rs);
        }
        return beanArrList;
    }
    public String getDelete(String id) {

            connection = AppsproConnection.getConnection();
            String query;
            int checkResult=0;

            try {
                query = "DELETE FROM XX_PROJECT_COST WHERE ID=?";
                pre = connection.prepareStatement(query);
                pre.setString(1, id);
                checkResult = pre.executeUpdate();

            } catch (Exception e) {
          
                e.printStackTrace();
            } finally {
                con.closeResources(connection, pre, rs);
            }
            return Integer.toString(checkResult);
        }
    public String getDeleteApprovalRequest (String TransactionId) {

            connection = AppsproConnection.getConnection();
            String query;
            int checkResult=0;

            try {
                query = "delete from XX_APPROVAL_LIST where TRANSACTION_ID = ?";
                pre = connection.prepareStatement(query);
                pre.setString(1, TransactionId);
                checkResult = pre.executeUpdate();

            } catch (Exception e) {
          
                e.printStackTrace();
            } finally {
                con.closeResources(connection, pre, rs);
            }
            return Integer.toString(checkResult);
        }
    
}
