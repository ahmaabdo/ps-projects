package com.appspro.sewedyts.rest;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.ApprovalListBean;

import com.appspro.sewedyts.dao.ApprovalListDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/approval")
public class ApprovalListRest extends RestHelper {
    ApprovalListDAO service = new ApprovalListDAO();

    @POST
    @Path("/getApprovalList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getbyId(String data) {
        ArrayList<ApprovalListBean> list = new ArrayList<ApprovalListBean>();
        try {
            JSONObject o = new JSONObject(data);
            String transActionId =
                o.has("transActionId") ? o.getString("transActionId") : null;
            list = service.getLastStepForApproval(transActionId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray(list).toString();
    }
    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
        ApprovalListBean list = new ApprovalListBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ApprovalListBean bean = mapper.readValue(body, ApprovalListBean.class);
            list = service.insertIntoApprovalList(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @GET
    @Path("approvaltype/{s_type}/{t_type}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getapprovaltype(@PathParam("s_type") String s_type, @PathParam("t_type") String t_type) {
        ArrayList<ApprovalListBean> list = new ArrayList<ApprovalListBean>();

        list = service.getApprovalType(s_type, t_type);

        return  new  JSONArray(list).toString();
    }
}
