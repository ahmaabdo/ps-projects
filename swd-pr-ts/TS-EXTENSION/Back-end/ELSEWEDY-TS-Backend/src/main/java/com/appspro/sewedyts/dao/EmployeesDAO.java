package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.EmployeeBean;
import com.appspro.sewedyts.bean.projectsBean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;

public class EmployeesDAO extends RestHelper {
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public ArrayList<EmployeeBean> getAllEmployees() {
        ArrayList<EmployeeBean> employeesList = new ArrayList<EmployeeBean>();

        boolean hasMore = false;
        int offset = 0;
        do{
            
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getAllEmployeesUrl()+"&limit=500&offset="+offset;
            System.out.println(serverUrl);
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
                
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization", "Basic " + "YWhtZWRzaGVyaWZmOjEyMzQ1Njc4");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            
            JSONObject obj = new JSONObject(response.toString());
           
            JSONArray arr = obj.getJSONArray("items");
              System.out.println(arr.length());
            for (int i = 0; i < arr.length(); i++) {
                EmployeeBean bean = new EmployeeBean();
                JSONObject projectObj = arr.getJSONObject(i);
                bean.setDisplayName(projectObj.has("DisplayName")?projectObj.getString("DisplayName"):null);
                bean.setPersonNumber(projectObj.has("PersonNumber")?projectObj.getString("PersonNumber"):null);
                bean.setPersonID(projectObj.has("PersonId")?projectObj.getLong("PersonId"):null);
                bean.setUserName(projectObj.has("UserName")?projectObj.optString("UserName"):null);
                
                JSONArray assignments = projectObj.getJSONArray("assignments");
                bean.setAssingments(assignments.toString());
                employeesList.add(bean);
            }
            
              hasMore = obj.getBoolean("hasMore");
             offset += 500;
            

          } catch (Exception e) {
            e.printStackTrace();
          }
        }while (hasMore);
        return employeesList;
    }
}
