package com.appspro.sewedyts.rest;

import com.appspro.sewedyts.bean.ProjectsCostJobBean;


import com.google.gson.JsonObject;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONObject;

import org.xml.sax.SAXException;

import soap.createPC;

@Path("/projectCost")


public class ProjectCostRest {
    @POST 
    @Path("/updateProjectCost")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response updateProjectCost (String incomingData) throws IOException,
                                                           SAXException,
                                                           ParserConfigurationException {
        ProjectsCostJobBean bean = new ProjectsCostJobBean ();
        createPC pc = new createPC ();
        JSONObject object = new JSONObject(incomingData); 
        bean.setBusinessUnit(object.getString("businessUnitName"));
        bean.setExpenditureBatch(object.getString("batchName"));
        bean.setFromProjectNumber(object.getString("fromProjectNumber")); 
        bean.setToProjectNumber(object.getString("toProjectNumber"));
        bean.setExpenditureItemDate(object.getString("expenditureItemDate")); 
        
       String _response =
                pc.callPostRest("https://elhn-test.fa.em2.oraclecloud.com:443/fscmService/ErpIntegrationService",bean);
       
       JsonObject json = new JsonObject();
       json.addProperty("status", pc.status);
       
       return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();

       
      
    }
}
