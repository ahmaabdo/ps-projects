package com.appspro.sewedyts.rest;

import com.appspro.sewedyts.bean.ProjectTeamMember;
import com.appspro.sewedyts.bean.TaskDetailsBean;
import com.appspro.sewedyts.dao.ProjectTeamMemberDao;
import com.appspro.sewedyts.dao.TaskDetailsDao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/projectTeamMember")

public class ProTeamMemberRest {
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProTeamMember( String urls){
            ObjectMapper mapper = new ObjectMapper();
            TaskDetailsBean obj = new TaskDetailsBean ();
            ProjectTeamMemberDao pro = new ProjectTeamMemberDao();
            JSONArray res = new JSONArray();
            JSONObject responeObject = new JSONObject();

            try {
            JSONArray arr = new JSONArray(urls);
            responeObject.put("message", "OK");
            
            for (int i = 0 ; i <arr.length() ; i++ ) {
                String taskUrl = arr.get(i).toString();
//                String taskUrl = o.has("href") ? o.getString("href") : null;
                ArrayList<ProjectTeamMember> projectList;
                String jsonInString = null;
            
                projectList = pro.getProTeamMember(taskUrl);
                jsonInString = mapper.writeValueAsString(projectList);
                res.put(jsonInString);
            }
            responeObject.put("data", res);

            } catch (MalformedURLException e) {
                responeObject.put("message", "ERROR");
                responeObject.put("data",e.getMessage());
            } catch (IOException e) {
                responeObject.put("message", "ERROR");
                responeObject.put("data",e.getMessage());
            }
          
            return Response.ok(responeObject.toString(), MediaType.APPLICATION_JSON).build();
                
        
        }
}
