package com.appspro.sewedyts.dao;
import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.sewedyts.bean.TimeSheetBean;




import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import static biPReports.RestHelper.getSchema_Name;
public class TimeSheetDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public TimeSheetBean insertOrUpdateTimeSheet(TimeSheetBean bean,
                                                      String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            if (transactionType.equals("ADD")) {

                String query =
                    "INSERT INTO " + getSchema_Name() + ".xx_sewedy_ts " +
                    "(TRANSACTION_DATA,PROJECT_NUMBER,PROJECT_NAME,TASK_NAME,TASK_NUMBER,EXPENDITURE_TYPE,TIME_SHEET_TYPE,QUANTITY,UOM,EMP_TS_ID)" +
                    " VALUES (?,?,?,?,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getTransactionData());
                ps.setString(2, bean.getProjectNumber());
                ps.setString(3, bean.getProjectName());
                ps.setString(4, bean.getTaskName());
                ps.setString(5, bean.getTaskNumber());
                ps.setString(6, bean.getExpenditureType());
                ps.setString(7, bean.getTimeSheetType());
                ps.setString(8, bean.getQuantity());
                ps.setString(9, bean.getUom());
                ps.setString(10,bean.getEmpTsId());
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {
                String query =
                    "UPDATE " + getSchema_Name() + ".xx_sewedy_ts set TRANSACTION_DATA=?,PROJECT_NUMBER=?,PROJECT_NAME=?,TASK_NAME=?,TASK_NUMBER=?,EXPENDITURE_TYPE=?,TIME_SHEET_TYPE=?,QUANTITY=?,UOM=? where ID=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getTransactionData());
                ps.setString(2, bean.getProjectNumber());
                ps.setString(3, bean.getProjectName());
                ps.setString(4, bean.getTaskName());
                ps.setString(5, bean.getTaskNumber());
                ps.setString(6, bean.getExpenditureType());
                ps.setString(7, bean.getTimeSheetType());
                ps.setString(8, bean.getQuantity());
                ps.setString(9, bean.getUom());
                ps.setString(10, bean.getId());
               
                
                ps.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    public ArrayList<TimeSheetBean> getAllInfo() {

        ArrayList<TimeSheetBean> TimeSheetList =
            new ArrayList<TimeSheetBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from " + getSchema_Name() + ".xx_sewedy_ts";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                TimeSheetBean TimeSheet = new TimeSheetBean();
                
                TimeSheet.setId(rs.getString("ID"));
                TimeSheet.setEmpTsId(rs.getString("EMP_TS_ID"));
                TimeSheet.setTransactionData(rs.getString("TRANSACTION_DATA"));
                TimeSheet.setProjectNumber(rs.getString("PROJECT_NUMBER"));
                TimeSheet.setProjectName(rs.getString("PROJECT_NAME"));
                TimeSheet.setTaskNumber(rs.getString("TASK_NUMBER"));
                TimeSheet.setTaskName(rs.getString("TASK_NAME"));
                TimeSheet.setExpenditureType(rs.getString("EXPENDITURE_TYPE"));
                TimeSheet.setTimeSheetType(rs.getString("TIME_SHEET_TYPE"));
                TimeSheet.setQuantity(rs.getString("QUANTITY"));
                TimeSheet.setUom(rs.getString("UOM"));

                TimeSheetList.add(TimeSheet);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return TimeSheetList;

    }
    
    
    public String getDelete(String id) {

            connection = AppsproConnection.getConnection();
            String query;
            int checkResult=0;

            try {
                query = "DELETE FROM xx_sewedy_ts WHERE ID=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, id);
                checkResult = ps.executeUpdate();

            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
            return Integer.toString(checkResult);
        }
    public ArrayList<TimeSheetBean> getAllInfoEmp(String id) {

        ArrayList<TimeSheetBean> TimeSheetList =
            new ArrayList<TimeSheetBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from " + getSchema_Name() + ".xx_sewedy_ts t ,"+ getSchema_Name()
                +" .xx_sewedy_emp e "+"where t.emp_ts_id=e.id and e.id= ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                TimeSheetBean TimeSheet = new TimeSheetBean();
                
                TimeSheet.setId(rs.getString("ID"));
                TimeSheet.setEmpTsId(rs.getString("EMP_TS_ID"));
                TimeSheet.setTransactionData(rs.getString("TRANSACTION_DATA"));
                TimeSheet.setProjectNumber(rs.getString("PROJECT_NUMBER"));
                TimeSheet.setProjectName(rs.getString("PROJECT_NAME"));
                TimeSheet.setTaskNumber(rs.getString("TASK_NUMBER"));
                TimeSheet.setTaskName(rs.getString("TASK_NAME"));
                TimeSheet.setExpenditureType(rs.getString("EXPENDITURE_TYPE"));
                TimeSheet.setTimeSheetType(rs.getString("TIME_SHEET_TYPE"));
                TimeSheet.setQuantity(rs.getString("QUANTITY"));
                TimeSheet.setUom(rs.getString("UOM"));

                TimeSheetList.add(TimeSheet);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return TimeSheetList;

    }
    
    
}
