package com.appspro.mail;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;



public class CredentialStoreClassMail {
    public static String SEND_MAIL(String emailTo, String subject,
                                   String body) {
        SSLContext ctx = null;
        System.setProperty("DUseSunHttpHandler", "true");
        TrustManager[] trustAllCerts =
            new X509TrustManager[] { new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs,
                                               String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs,
                                               String authType) {
                }
            } };
        try {
            ctx = SSLContext.getInstance("TLSv1.2");
            ctx.init(null, trustAllCerts, null);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Error loading ssl context {}" +
                               e.getMessage());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        SSLContext.setDefault(ctx);

        ClientConfig def = new DefaultClientConfig();

        def.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
                                new HTTPSProperties(null, ctx));
        Client client = Client.create(def);
     
        String input = "{  \n" +
            "   \"Messages\":[  \n" +
            "      {  \n" +
            "         \"From\":{  \n" +
            "            \"Email\":\"HR@hha.com.sa\",\n" +
            "            \"Name\":\"PaaS Self Service Admin\"\n" +
            "         },\n" +
            "         \"To\":[  \n" +
            "            {  \n" +
            "               \"Email\":\"" + emailTo + "\",\n" +
            "               \"Name\":\"Employee\"\n" +
            "            }\n" +
            "         ],\n" +
            "         \"Subject\":\"" + subject + "\",\n" +
            "         \"TextPart\":\"" + body + "\",\n" +
            "         \"HTMLPart\":\"\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";

        client.addFilter(new HTTPBasicAuthFilter("0c466d45ad6dfa75b0d02b2c0fa1c069",
                                                 "75f2279fdd22bc3a5ac563d2da7a5dd6"));

        //        serverURL = serverURL.replaceAll("\\s", "%20");
        WebResource resource =
            client.resource("https://api.mailjet.com/v3.1/send");
        //Security.addProvider(new BouncyCastleProvider());



        return resource.type("application/json").post(String.class, input);
    }

}
