package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.projectsBean;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;

import java.net.HttpURLConnection;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;

public class projectsDao extends RestHelper {

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public ArrayList<projectsBean> getOrgDetails() {
        ArrayList<projectsBean> projectList = new ArrayList<projectsBean>();
        boolean hasMore = false;
        int offset = 0;
        do {
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        String serverUrl = getInstanceUrl() + getProjectsUrl() + "&limit=500&offset=" + offset;
        String jsonResponse = "";
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
                
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization", "Basic " + getAuth());
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
//            JSONObject data = obj.getJSONObject("data");
          //  System.out.println("dataaaaa"+Tes);
            
            for (int i = 0; i < arr.length(); i++) {
                projectsBean bean = new projectsBean();
                JSONObject projectObj = arr.getJSONObject(i);           
                bean.setProjectNumber(projectObj.getString("ProjectNumber"));
                bean.setProjectName(projectObj.getString("ProjectName"));
                bean.setApprovalStatus(projectObj.getString("ProjectStatus"));
                bean.setProjectId(projectObj.getLong("ProjectId"));
                bean.setBusinessUnitName(projectObj.has("BusinessUnitName")? projectObj.get("BusinessUnitName").toString():null);
                JSONArray TeamMembers = projectObj.getJSONArray("ProjectTeamMembers");
                bean.setTeamMembers(TeamMembers.toString());
                JSONArray linkes = projectObj.getJSONArray("links");
                bean.setLinks(linkes.toString());
                JSONArray projectOrgnization = projectObj.getJSONArray("ProjectOrganizationBuPuPVO1");
                bean.setProjectOrganization(projectOrgnization.toString());

                projectList.add(bean);
            }
            hasMore = obj.getBoolean("hasMore");
            offset += 500;
          } catch (Exception e) {
            e.printStackTrace();
         }
        }
        while(hasMore);
        return projectList;
    }
}
