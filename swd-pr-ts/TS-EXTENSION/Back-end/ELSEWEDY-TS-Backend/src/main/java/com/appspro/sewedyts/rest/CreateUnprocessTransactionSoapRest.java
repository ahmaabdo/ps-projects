package com.appspro.sewedyts.rest;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.TransactionProcessBean;

import com.google.gson.JsonObject;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONObject;

import org.xml.sax.SAXException;
import soap.createTS;


@Path("/TsSoap")

public class CreateUnprocessTransactionSoapRest extends RestHelper{
   
   
      
    @POST 
    @Path("/CreatTsReq")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response createTsReq (String incomingData)  {
        TransactionProcessBean bean = new TransactionProcessBean ();
        createTS ts = new createTS ();
        JSONObject object = new JSONObject(incomingData); 
        bean.setBusinessUnitName(object.getString("businessUnitName"));
        bean.setBatchName(object.getString("batchName"));
        bean.setDocumentEntryName(object.getString("documentEntryName")); 
        bean.setExpenditureItemDate(object.getString("expenditureItemDate"));
        bean.setPersonName(object.getString("personName")); 
        bean.setPersonId(object.getString("personId"));
        bean.setProjectName(object.getString("projectName")); 
        bean.setProjectNumber(object.getString("projectNumber"));
//        bean.setTaskName(object.getString("taskName"));
        bean.setTaskNumber(object.getString("taskNumber"));
        bean.setQuantity(object.getString("quantity"));
        bean.setBillableFlag(object.getString("billableFlag"));
        bean.setExpenditureOrganizationId(object.getString("expenditureOrganizationId"));
       
        
        
       
        String _response=null;
        String statusMsg = "PJC_TXN_XFACE_IS_PENDING";
        JsonObject json = new JsonObject();
        json.addProperty("status", ts.status);
        try {
            System.out.println(getInstanceUrl());
            _response =
                    ts.callPostRest(getInstanceUrl()+"/fscmService/ProjectTimecardServiceV1",bean);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(statusMsg.equalsIgnoreCase(_response)) {
            return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("the number is exists").build(); 
        }
    }
    
}
