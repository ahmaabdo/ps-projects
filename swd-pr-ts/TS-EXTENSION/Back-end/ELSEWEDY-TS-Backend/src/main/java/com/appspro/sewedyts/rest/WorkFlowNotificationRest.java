package com.appspro.sewedyts.rest;

import com.appspro.sewedyts.bean.ApprovalListBean;

import com.appspro.sewedyts.bean.WorkFlowNotificationBean;
import com.appspro.sewedyts.dao.ApprovalListDAO;

import com.appspro.sewedyts.dao.WorkFlowNotificationDao;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/workFlowNotification")
public class WorkFlowNotificationRest {
    WorkFlowNotificationDao service = new WorkFlowNotificationDao();

    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
        WorkFlowNotificationBean list = new WorkFlowNotificationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            WorkFlowNotificationBean bean =
                mapper.readValue(body, WorkFlowNotificationBean.class);
            list = service.insertIntoWorkflow(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @POST
    @Path("/allNotifications")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllNotificationsDetails(String bean, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        JSONObject jsonObj = new JSONObject(bean);
        String MANAGER_ID = jsonObj.getString("MANAGER_ID");
        String emp = jsonObj.getString("emp");    
        String managerOfManager = jsonObj.getString("MANAGER_OF_MANAGER");
        String costController = jsonObj.getString("COST_CONTROLLER");
        String financeManger = jsonObj.getString("FINANCE_MANAGER");
        WorkFlowNotificationDao det = new WorkFlowNotificationDao();
        ArrayList<WorkFlowNotificationBean> list = new ArrayList<WorkFlowNotificationBean>();

        list = det.getAllNotification(MANAGER_ID, emp, managerOfManager, costController, financeManger);

        return  new  JSONArray(list).toString();
    }
    @POST
    @Path("/updateNotifi")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdateExist(String body) {
        WorkFlowNotificationBean list = new WorkFlowNotificationBean();
        try {
            JSONObject obj = new JSONObject(body);
            String TRS_ID = obj.has("requestId")?obj.getString("requestId"):null;
             service.updatelocal(TRS_ID);
         
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @POST
    @Path("/updateAdminAL")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String ADminUPdateAL(String body) {
        WorkFlowNotificationBean list = new WorkFlowNotificationBean();
        try {
            JSONObject obj = new JSONObject(body);
            String NOTI_TYP = obj.has("notificationType")?obj.getString("notificationType"):null;
            String RCODE = obj.has("responseCode")?obj.getString("responseCode"):null;
            String TRS_ID = obj.has("transactionId")?obj.getString("transactionId"):null;
             service.updateAdminAL(NOTI_TYP, RCODE, TRS_ID);
         
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String worwflowApproval(String bean, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        JSONObject jsonObj = new JSONObject(bean);

        WorkFlowNotificationDao det = new WorkFlowNotificationDao();

        det.workflowAction(bean);
        jsonObj.put("STATUS", "Success");
        return jsonObj.toString();

    }
}
