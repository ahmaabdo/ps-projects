package com.appspro.sewedyts.bean;

public class TransactionProcessBean {
  
    public String businessUnitName;
    public String batchName;
    public String sourceId;
    public String documentName;
    public String documentId;
    public String documentEntryName;
    public String documentEntryId;
    public String expenditureItemDate;
    public String personName;
    public String personId;
    public String personNumber;
    public String projectName;
    public String projectNumber;
    public String taskName;
    public String taskNumber;
    public String expenditureTypeName;
    public String expenditureOrganizationId;
    public String quantity;
    public String workTypeName;
    public String billableFlag;
    public String capitalizableFlag;
    public String origTransactionReference;
    public String providerLedgerCurrencyConversionRateType;
    public String providerLedgerCurrencyConversionRate;
    public String accountingDate;


    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentEntryName(String documentEntryName) {
        this.documentEntryName = documentEntryName;
    }

    public String getDocumentEntryName() {
        return documentEntryName;
    }

    public void setDocumentEntryId(String documentEntryId) {
        this.documentEntryId = documentEntryId;
    }

    public String getDocumentEntryId() {
        return documentEntryId;
    }

    public void setExpenditureItemDate(String expenditureItemDate) {
        this.expenditureItemDate = expenditureItemDate;
    }

    public String getExpenditureItemDate() {
        return expenditureItemDate;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }
    
    

    public void setExpenditureTypeName(String expenditureTypeName) {
        this.expenditureTypeName = expenditureTypeName;
    }

    public String getExpenditureTypeName() {
        return expenditureTypeName;
    }

    public void setExpenditureOrganizationId(String expenditureOrganizationId) {
        this.expenditureOrganizationId = expenditureOrganizationId;
    }

    public String getExpenditureOrganizationId() {
        return expenditureOrganizationId;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setBillableFlag(String billableFlag) {
        this.billableFlag = billableFlag;
    }

    public String getBillableFlag() {
        return billableFlag;
    }

    public void setCapitalizableFlag(String capitalizableFlag) {
        this.capitalizableFlag = capitalizableFlag;
    }

    public String getCapitalizableFlag() {
        return capitalizableFlag;
    }

    public void setOrigTransactionReference(String origTransactionReference) {
        this.origTransactionReference = origTransactionReference;
    }

    public String getOrigTransactionReference() {
        return origTransactionReference;
    }

    public void setProviderLedgerCurrencyConversionRateType(String providerLedgerCurrencyConversionRateType) {
        this.providerLedgerCurrencyConversionRateType = providerLedgerCurrencyConversionRateType;
    }

    public String getProviderLedgerCurrencyConversionRateType() {
        return providerLedgerCurrencyConversionRateType;
    }

    public void setProviderLedgerCurrencyConversionRate(String providerLedgerCurrencyConversionRate) {
        this.providerLedgerCurrencyConversionRate = providerLedgerCurrencyConversionRate;
    }

    public String getProviderLedgerCurrencyConversionRate() {
        return providerLedgerCurrencyConversionRate;
    }

    public void setAccountingDate(String accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getAccountingDate() {
        return accountingDate;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }
}
