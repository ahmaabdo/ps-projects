package com.appspro.sewedyts.bean;

public class ProjectsCostJobBean {
    public String businessUnit;
    public String processMode;
    public String transactionStatus;
    public String transactionSource;
    public String document;
    public String expenditureBatch;
    public String fromProjectNumber;
    public String toProjectNumber;
    public String expenditureItemDate;
    public String generateReport;


    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setProcessMode(String processMode) {
        this.processMode = processMode;
    }

    public String getProcessMode() {
        return processMode;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionSource(String transactionSource) {
        this.transactionSource = transactionSource;
    }

    public String getTransactionSource() {
        return transactionSource;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getDocument() {
        return document;
    }

    public void setExpenditureBatch(String expenditureBatch) {
        this.expenditureBatch = expenditureBatch;
    }

    public String getExpenditureBatch() {
        return expenditureBatch;
    }

    public void setFromProjectNumber(String fromProjectNumber) {
        this.fromProjectNumber = fromProjectNumber;
    }

    public String getFromProjectNumber() {
        return fromProjectNumber;
    }

    public void setToProjectNumber(String toProjectNumber) {
        this.toProjectNumber = toProjectNumber;
    }

    public String getToProjectNumber() {
        return toProjectNumber;
    }

    public void setExpenditureItemDate(String expenditureItemDate) {
        this.expenditureItemDate = expenditureItemDate;
    }

    public String getExpenditureItemDate() {
        return expenditureItemDate;
    }

    public void setGenerateReport(String generateReport) {
        this.generateReport = generateReport;
    }

    public String getGenerateReport() {
        return generateReport;
    }
}
