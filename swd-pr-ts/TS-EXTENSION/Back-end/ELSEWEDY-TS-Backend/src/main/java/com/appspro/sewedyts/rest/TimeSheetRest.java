package com.appspro.sewedyts.rest;

import com.appspro.sewedyts.bean.TimeSheetBean;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.appspro.sewedyts.dao.TimeSheetDAO;



import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;

import org.json.JSONArray;
import org.json.JSONObject;
@Path("/timeSheet")
public class TimeSheetRest {
    TimeSheetDAO service = new TimeSheetDAO();
    
    
    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateTimeSheetfun(String body, @PathParam("transactionType")
        String transactionType) {
        TimeSheetBean list = new TimeSheetBean();
        try {

            ObjectMapper mapper = new ObjectMapper();
           
                TimeSheetBean bean = mapper.readValue(body, TimeSheetBean.class);
                list = service.insertOrUpdateTimeSheet(bean, transactionType);
            
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject(list).toString();
        }
    }
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllData() {
        List<TimeSheetBean> list = new ArrayList<TimeSheetBean>();
        list = service.getAllInfo();
        return new JSONArray(list).toString();


    }
    @GET
       @Path("delete/{id}")
       @Produces(MediaType.APPLICATION_JSON)
       public String getdelete(@PathParam("id")
           String id) {
           String B = null;
           try {
               B = service.getDelete(id);
               return B;
           } catch (Exception e) {
               e.printStackTrace();
           }
               return B;

           }
    @GET
    @Path("/getAll/{empId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllData(@PathParam("empId") String id) {
        List<TimeSheetBean> list = new ArrayList<TimeSheetBean>();
        list = service.getAllInfoEmp(id);
        return new JSONArray(list).toString();


    }
       
}
