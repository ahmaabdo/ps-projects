package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.sewedyts.bean.TimeSheetBean;

import com.appspro.sewedyts.bean.TimeSheetOperationBean;

import com.appspro.sewedyts.bean.projectsBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

public class TimeSheetOperationDao extends AppsproConnection {    
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public TimeSheetOperationBean  insertDraftTimeSheetOperation(TimeSheetOperationBean bean) {
        try {
            connection = AppsproConnection.getConnection();
    
               String query ="INSERT INTO XXX_TIMESHEET_OPERATION_REQ" +
      "(TRANSACTION_DATE,PROJECT_NAME,PROJECT_NUMBER,TASK_NAME,TASK_NUMBER,EXPENDITURE_TYPE,QUANTITY,UOM,PERSON_ID,TIMESHEET_TYPE,BUSINESS_UNIT_NAME,EMPLOYEE_NAME,EMPLOYEE_NUMBER,TIME_SHEET_DATE,BATCH_NAME,BILLABLE_FLAG,PROJECT_ORG_ID)\n" + 
               "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, bean.getTransactionDate());
            ps.setString(2, bean.getProjectName());
            ps.setString(3, bean.getProjectNumber());
            ps.setString(4, bean.getTaskName());
            ps.setString(5, bean.getTaskNumber());
            ps.setString(6, bean.getExpenditureType());
            ps.setString(7, bean.getQuantity());
            ps.setString(8, bean.getUom());
            ps.setString(9, bean.getPersoId());
            ps.setString(10, bean.getTimeSheetType());
            ps.setString(11, bean.getBusinessUnitName());
            ps.setString(12, bean.getEmployeeName());
            ps.setString(13, bean.getEmployeeNumber());
            ps.setString(14, bean.getTimeSheetDate());
            ps.setString(15, bean.getBatchName());
            ps.setString(16, bean.getBillableFlag());
            ps.setString(17, bean.getExpenditureOrganizationId());


            ps.executeUpdate();

            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    public List<TimeSheetOperationBean> searchById(String personId) {
    ArrayList<TimeSheetOperationBean> list = new ArrayList<TimeSheetOperationBean>();
        
        try {
            
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_TIMESHEET_OPERATION_REQ where PERSON_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, personId);        
            rs = ps.executeQuery();

            while (rs.next()) {
                TimeSheetOperationBean bean = new TimeSheetOperationBean();
                bean.setId(rs.getInt("ID"));
                bean.setProjectName(rs.getString("PROJECT_NAME"));
                bean.setProjectNumber(rs.getString("PROJECT_NUMBER"));
                bean.setTaskName(rs.getString("TASK_NAME"));
                bean.setTaskNumber(rs.getString("TASK_NUMBER"));
                bean.setTransactionDate(rs.getString("TRANSACTION_DATE"));
                bean.setExpenditureType(rs.getString("EXPENDITURE_TYPE"));                  
                bean.setUom(rs.getString("UOM"));
                bean.setQuantity(rs.getString("QUANTITY"));
                bean.setTimeSheetType(rs.getString("TIMESHEET_TYPE"));
                bean.setBusinessUnitName(rs.getString("BUSINESS_UNIT_NAME"));
                bean.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                bean.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));
                bean.setTimeSheetDate(rs.getString("TIME_SHEET_DATE"));
                bean.setBatchName(rs.getString("BATCH_NAME"));
                bean.setBillableFlag(rs.getString("BILLABLE_FLAG"));
                bean.setExpenditureOrganizationId(rs.getString("PROJECT_ORG_ID"));
                
                list.add(bean);
            }        
             
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
    public String deleteDraftRequest(String personId) {

            connection = AppsproConnection.getConnection();
            String query;
            int checkResult=0;

            try {
                query = "DELETE FROM XXX_TIMESHEET_OPERATION_REQ WHERE id=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, personId);
                checkResult = ps.executeUpdate();

            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
            return Integer.toString(checkResult);
        }
    public List<TimeSheetOperationBean> getMax() {
    ArrayList<TimeSheetOperationBean> list = new ArrayList<TimeSheetOperationBean>();
        String sql="";
        try {
            connection = AppsproConnection.getConnection();
                 sql =  "Select NVL(MAX(ID),0) as request_id from XXX_TIMESHEET_OPERATION_REQ";
                ps = connection.prepareStatement(sql);             
                rs = ps.executeQuery();
                while (rs.next()) {
                    TimeSheetOperationBean bean = new TimeSheetOperationBean();
                    bean.setMaxRequestID(rs.getInt("request_id"));
                    list.add(bean);
                }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
   
}
