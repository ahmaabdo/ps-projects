package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.EmployeeBean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class DepartmentsDao extends RestHelper {
    
    public ArrayList<EmployeeBean> getAllDepartments() {
        ArrayList<EmployeeBean> departmentsList = new ArrayList<EmployeeBean>();

       
            
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getAllDepartmentsUrl();
            System.out.println(serverUrl);
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
                
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization", "Basic " + "YWhtZWRzaGVyaWZmOjEyMzQ1Njc4");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            
            JSONObject obj = new JSONObject(response.toString());
           
            JSONArray arr = obj.getJSONArray("items");
          
            for (int i = 0; i < arr.length(); i++) {
                EmployeeBean bean = new EmployeeBean();
                JSONObject projectObj = arr.getJSONObject(i);
                bean.setDepartmentId(projectObj.has("OrganizationId")?projectObj.getLong("OrganizationId"):null);
                bean.setDepartmentName(projectObj.has("Name")?projectObj.getString("Name"):null);               
                
                departmentsList.add(bean);
            }
            
           
          } catch (Exception e) {
            e.printStackTrace();
          }
       
        return departmentsList;
    }
}
