package com.appspro.sewedyts.bean;

public class TaskDetailsBean {
    private String taskName ; 
    private String taskNumber ;
    private Boolean chargeableFlag;
    private String expenditureOrganization ;

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setExpenditureOrganization(String expenditureOrganization) {
        this.expenditureOrganization = expenditureOrganization;
    }

    public String getExpenditureOrganization() {
        return expenditureOrganization;
    }

    public void setChargeableFlag(Boolean chargeableFlag) {
        this.chargeableFlag = chargeableFlag;
    }

    public Boolean getChargeableFlag() {
        return chargeableFlag;
    }
}
