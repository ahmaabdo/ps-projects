package com.appspro.sewedyts.rest;
import com.appspro.sewedyts.bean.SWD_EmployeeBean;

import com.appspro.sewedyts.bean.TimeSheetBean;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.appspro.sewedyts.dao.SWD_EmployeeDAO;



import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;

import org.json.JSONArray;
import org.json.JSONObject;
@Path("/employees")
public class SWD_EmployeeRest {
    
    SWD_EmployeeDAO service = new SWD_EmployeeDAO();
    
    
    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateEmp(String body, @PathParam("transactionType")
        String transactionType) {
        SWD_EmployeeBean list = new SWD_EmployeeBean();
        try {

            ObjectMapper mapper = new ObjectMapper();
           
                SWD_EmployeeBean bean = mapper.readValue(body, SWD_EmployeeBean.class);
                list = service.insertOrUpdateEmployee(bean, transactionType);
            
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject(list).toString();
        }
    }
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllData() {
        List<SWD_EmployeeBean> list = new ArrayList<SWD_EmployeeBean>();
        list = service.getAllEmp();
        return new JSONArray(list).toString();


    }
    @GET
       @Path("delete/{id}")
       @Produces(MediaType.APPLICATION_JSON)
       public String getdelete(@PathParam("id")
           String id) {
           String B = null;
           try {
               B = service.getDelete(id);
               return B;
           } catch (Exception e) {
               e.printStackTrace();
           }
               return B;

           }
       
    
}
