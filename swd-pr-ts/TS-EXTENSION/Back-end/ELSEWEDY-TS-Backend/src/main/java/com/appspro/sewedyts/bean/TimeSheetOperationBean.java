package com.appspro.sewedyts.bean;

public class TimeSheetOperationBean {
    private int id;
    private String projectNumber;
    private String projectName;
    private String taskName;
    private String taskNumber;
    private String transactionDate;
    private String expenditureType;
    private String quantity;
    private String uom;
    private String persoId;
    private String timeSheetType;
    private String businessUnitName;
    private String employeeName;
    private String employeeNumber;
    private String timeSheetDate;
    private String batchName;
    private int maxRequestID;
    private String billableFlag;
    private String expenditureOrganizationId;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setExpenditureType(String expendetureType) {
        this.expenditureType = expendetureType;
    }

    public String getExpenditureType() {
        return expenditureType;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setPersoId(String persoId) {
        this.persoId = persoId;
    }

    public String getPersoId() {
        return persoId;
    }

    public void setTimeSheetType(String timeSheetType) {
        this.timeSheetType = timeSheetType;
    }

    public String getTimeSheetType() {
        return timeSheetType;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setTimeSheetDate(String timeSheetDate) {
        this.timeSheetDate = timeSheetDate;
    }

    public String getTimeSheetDate() {
        return timeSheetDate;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setMaxRequestID(int maxRequestID) {
        this.maxRequestID = maxRequestID;
    }

    public int getMaxRequestID() {
        return maxRequestID;
    }

    public void setBillableFlag(String billableFlag) {
        this.billableFlag = billableFlag;
    }

    public String getBillableFlag() {
        return billableFlag;
    }

    public void setExpenditureOrganizationId(String expenditureOrganizationId) {
        this.expenditureOrganizationId = expenditureOrganizationId;
    }

    public String getExpenditureOrganizationId() {
        return expenditureOrganizationId;
    }
}
