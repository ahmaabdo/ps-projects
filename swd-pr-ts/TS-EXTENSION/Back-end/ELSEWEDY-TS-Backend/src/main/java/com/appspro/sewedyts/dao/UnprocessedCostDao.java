package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.TaskDetailsBean;

import com.appspro.sewedyts.bean.UnprocessedCostBean;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class UnprocessedCostDao extends RestHelper {
    
    public ArrayList<UnprocessedCostBean> getUnprocessedsDetails() {
        ArrayList<UnprocessedCostBean> projectList = new ArrayList<UnprocessedCostBean>();
        String finalresponse = "";
        //        String jwttoken = jwt.trim();
        UnprocessedCostBean pro = new UnprocessedCostBean();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getUnprocessedCostUrl();
        //getInstanceUrl()+
        
        System.out.println(serverUrl);             

        String jsonResponse = "";
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
             //   trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
          //  System.out.println("response"+obj);
            JSONArray arr = obj.getJSONArray("items");
                      
            for (int i = 0; i < arr.length(); i++) {
                UnprocessedCostBean bean = new UnprocessedCostBean();
                JSONObject proObj = arr.getJSONObject(i);
                bean.setUnprocessedTransactionReferenceId(proObj.getLong("UnprocessedTransactionReferenceId"));     
                bean.setBusinessUnit(proObj.getString("BusinessUnit"));
                projectList.add(bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return projectList;
    }
    public ArrayList<UnprocessedCostBean> updateUnprocessedCost() {
        ArrayList<UnprocessedCostBean> projectList = new ArrayList<UnprocessedCostBean>();
        UnprocessedCostBean co = new UnprocessedCostBean();
        String finalresponse = "";

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        String serverUrl =
            getInstanceUrl() + getUnprocessedCostUrl() + co.getTransactionId();
        String jsonResponse = "";

        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                //   trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("PATCH");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            int responseCode = connection.getResponseCode();
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());
            connection.setDoOutput(true);
    //            ObjectWriter ow =
    //                new ObjectMapper().writer().withDefaultPrettyPrinter();
    //            String json =ow.writeValueAsString()
    //            String urlParameters = json;
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
           
            
            
    //            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
    //            wr.writeByte(TransactionId);
    //            wr.flush();
    //            wr.close();
            
            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
                        for (int i = 0; i < arr.length(); i++) {
                UnprocessedCostBean bean = new UnprocessedCostBean();
                JSONObject proObj = arr.getJSONObject(i);
                bean.setTransactionId(proObj.getLong("UnprocessedTransactionReferenceId"));     
                bean.setQuantity(proObj.getString("Quantity"));
                projectList.add(bean);
            }
        } catch (Exception e) {
        }
        return projectList;
    }
}
