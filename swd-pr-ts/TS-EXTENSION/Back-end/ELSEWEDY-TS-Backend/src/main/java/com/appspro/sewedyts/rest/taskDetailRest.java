package com.appspro.sewedyts.rest;


import com.appspro.sewedyts.dao.TaskDetailsDao;
import com.appspro.sewedyts.bean.TaskDetailsBean;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

@Path("/tasks")
public class taskDetailRest {
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGradeDetails( String url) throws JsonProcessingException, UnsupportedEncodingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            TaskDetailsBean obj = new TaskDetailsBean ();
            TaskDetailsDao pro = new TaskDetailsDao();
            JSONObject o = new JSONObject(url);
            
            String taskUrl = o.has("href") ? o.getString("href") : null;
            ArrayList<TaskDetailsBean> projectList = pro.getProTasksDetails(taskUrl);
            String jsonInString = null;
            jsonInString = mapper.writeValueAsString(projectList);
            return Response.ok(jsonInString, MediaType.APPLICATION_JSON).build();
                
        
        }
}
