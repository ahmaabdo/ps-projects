package com.appspro.sewedyts.rest;

import com.appspro.sewedyts.bean.WorkFlowNotificationBean;
import com.appspro.sewedyts.bean.projectsBean;

import com.appspro.sewedyts.dao.ProjectCostDao;
import com.appspro.sewedyts.dao.projectsDao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/projecDetails")
public class projectsRest {
    ProjectCostDao service = new ProjectCostDao();
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGradeDetails(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        projectsBean obj = new projectsBean();
        projectsDao det = new projectsDao();

        ArrayList<projectsBean> gradeList = det.getOrgDetails();
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(gradeList);
        return Response.ok(new String(jsonInString.getBytes(), "UTF-8"), MediaType.APPLICATION_JSON).build();

    }
    @Path("/projectCost")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectCost(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        projectsBean obj = new projectsBean();
        ProjectCostDao det = new ProjectCostDao();

        ArrayList<projectsBean> projectList = det.getProjectCostDetails();
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(projectList);
        return Response.ok(new String(jsonInString.getBytes(), "UTF-8"), MediaType.APPLICATION_JSON).build();

    }
    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
        projectsBean list = new projectsBean();
        try {
            JSONArray arr = new JSONArray(body);
                        JSONArray res = new JSONArray();
                        
                        for (int i = 0 ; i <arr.length() ; i++ ) {
                            String beans = arr.get(i).toString();
                            ObjectMapper mapper = new ObjectMapper();
                            projectsBean bean = mapper.readValue(beans, projectsBean.class);
                            list = service.insertToLocal(bean);
                        }
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @GET
    @Path("/getAllRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllRequest() {
        List<projectsBean> list = new ArrayList<projectsBean>();
        list = service.getallrequest();
        return new JSONArray(list).toString();

    }
    @POST
    @Path("/updateStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdateExist(String body) {
        projectsBean list = new projectsBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            projectsBean bean = mapper.readValue(body, projectsBean.class);
            list = service.upDateStatus(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    
    @POST
    @Path("/updateImportStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdateImport(String body) {
        String response  = "";
        try {
            JSONObject obj = new JSONObject(body);
            System.out.println(obj);
            String status  = obj.has("importStatus")?obj.getString("importStatus"):null;
            int id = obj.has("id")?obj.getInt("id"):null;
            response = service.updateImportStatus(status,id);
            return new String(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(response);
    }
    
    
    @POST
    @Path("getById")
    @Produces(MediaType.APPLICATION_JSON)
    public String findbyid(String data) {
        ArrayList<projectsBean> list = new ArrayList<projectsBean>();
        try {
            list = service.searchbyid(data);
        } catch (Exception e) {
            System.out.println(e);
        }
        return new JSONArray(list).toString();

    }

    @GET
    @Path("delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getdelete(@PathParam("id")
        String id) {
        String body = null;
        try {
            body = service.getDelete(id);
            return body;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return body;

    }
    @GET
    @Path("deleteApprovedList/{transactionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getdeleteApprovedList(@PathParam("transactionId")
        String transactionId) {
        String body = null;
        try {
            body = service.getDeleteApprovalRequest(transactionId);
            return body;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return body;

    }
}
