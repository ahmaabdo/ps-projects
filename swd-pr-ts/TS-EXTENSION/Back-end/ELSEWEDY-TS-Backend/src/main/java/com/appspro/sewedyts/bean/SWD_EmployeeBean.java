package com.appspro.sewedyts.bean;

public class SWD_EmployeeBean {
    private String employeeName;
    private String employeeNumber;
    private String organization;
    private String timeSheetDate;
    private String totalStraightTime;
    private String totalOverTime;
    private String uom;
    private String id;

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getOrganization() {
        return organization;
    }

    public void setTimeSheetDate(String timeSheetDate) {
        this.timeSheetDate = timeSheetDate;
    }

    public String getTimeSheetDate() {
        return timeSheetDate;
    }

    public void setTotalStraightTime(String totalStraightTime) {
        this.totalStraightTime = totalStraightTime;
    }

    public String getTotalStraightTime() {
        return totalStraightTime;
    }

    public void setTotalOverTime(String totalOverTime) {
        this.totalOverTime = totalOverTime;
    }

    public String getTotalOverTime() {
        return totalOverTime;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
