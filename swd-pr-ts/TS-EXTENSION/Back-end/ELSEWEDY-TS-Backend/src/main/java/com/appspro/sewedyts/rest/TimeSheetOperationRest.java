package com.appspro.sewedyts.rest;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.ApprovalListBean;

import com.appspro.sewedyts.bean.TimeSheetOperationBean;

import com.appspro.sewedyts.bean.projectsBean;
import com.appspro.sewedyts.dao.ApprovalListDAO;
import com.appspro.sewedyts.dao.TimeSheetOperationDao;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/timeSheetOperation")


public class TimeSheetOperationRest extends RestHelper{
    TimeSheetOperationDao service = new TimeSheetOperationDao();
    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
       
        TimeSheetOperationBean list = new TimeSheetOperationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            TimeSheetOperationBean bean = mapper.readValue(body, TimeSheetOperationBean.class);
            list = service.insertDraftTimeSheetOperation(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @POST
    @Path("getById")
    @Produces(MediaType.APPLICATION_JSON)
    public String findbyid(String data) {
        List<TimeSheetOperationBean> list = new  ArrayList<TimeSheetOperationBean>();
        try {
            JSONObject o = new JSONObject(data);
            String personId =
                o.has("id") ? o.getString("id") : null;
            list = service.searchById(personId);

        } catch (Exception e) {
          e.printStackTrace();
        }

        return new JSONArray(list).toString();

    }

    @GET
    @Path("delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getdelete(@PathParam("id")
        String id) {
        String remark = null;
        try {
           remark = service.deleteDraftRequest(id);
            return remark;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return remark;

    }
    @GET
    @Path("maxId")
    @Produces(MediaType.APPLICATION_JSON)
    public String maxId(String data) {
        List<TimeSheetOperationBean> list = new  ArrayList<TimeSheetOperationBean>();
        try {
           
            list = service.getMax();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONArray(list).toString();

    }
}
