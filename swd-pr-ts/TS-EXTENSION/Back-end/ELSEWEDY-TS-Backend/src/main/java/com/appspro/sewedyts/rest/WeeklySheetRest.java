package com.appspro.sewedyts.rest;

import com.appspro.sewedyts.dao.WeeklySheetDao;

import com.appspro.sewedyts.excelHelper.ExcelHelper;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

@Path("/weeklySheet")
public class WeeklySheetRest {
    
    @POST
    @Path("/newVersion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createNewVersion(String excelJSON) {
        WeeklySheetDao obj = new WeeklySheetDao();
            obj.createDataFromExcelSheet(excelJSON);
            return Response.ok("").status(200).build();
   
    }
}
