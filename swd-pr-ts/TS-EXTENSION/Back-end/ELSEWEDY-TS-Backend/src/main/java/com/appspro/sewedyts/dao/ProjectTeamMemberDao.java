package com.appspro.sewedyts.dao;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.ProjectTeamMember;
import com.appspro.sewedyts.bean.TaskDetailsBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProjectTeamMemberDao extends RestHelper {
   
    public ArrayList<ProjectTeamMember> getProTeamMember(String TaskUrl) throws MalformedURLException,
                                                                                IOException {
        ArrayList<ProjectTeamMember> projectList = new ArrayList<ProjectTeamMember>();
        String finalresponse = "";
        ProjectTeamMember pro = new ProjectTeamMember();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = TaskUrl;
        //getInstanceUrl()+
        
        String jsonResponse = "";
        
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
             //   trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
          
            JSONArray arr = obj.getJSONArray("items");
                      
            for (int i = 0; i < arr.length(); i++) {
                ProjectTeamMember bean = new ProjectTeamMember();
                JSONObject proObj = arr.getJSONObject(i);
                bean.setPersonName(proObj.getString("PersonName"));
                bean.setPersonId(proObj.getLong("PersonId"));
                bean.setProjectRole(proObj.getString("ProjectRole"));
                bean.setTeamMemberId(proObj.getLong("TeamMemberId"));
                bean.setProjectId(proObj.getLong("ProjectId"));
             

                projectList.add(bean);
            }

       

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return projectList;
    }
}
