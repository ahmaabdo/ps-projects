
package biPReports.rest;

public class departmentBean {
   private int organizationId;
   private String organizationName;

    public departmentBean(int organizationId, String organizationName) {
        super();
        this.organizationId = organizationId;
        this.organizationName = organizationName;
    }

    public departmentBean() {
        
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationName() {
        return organizationName;
    }
}
