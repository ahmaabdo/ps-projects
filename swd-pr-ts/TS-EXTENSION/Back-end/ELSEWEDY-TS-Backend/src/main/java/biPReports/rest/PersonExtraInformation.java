
package biPReports.rest;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder( { "PersonId", "CategoryCode", "ObjectVersionNumber",
                      "links" })
public class PersonExtraInformation {

    @JsonProperty("PersonId")
    private Long personId;
    @JsonProperty("CategoryCode")
    private String categoryCode;
    @JsonProperty("ObjectVersionNumber")
    private Long objectVersionNumber;
    @JsonProperty("links")
    private List<Link> links = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties =
        new HashMap<String, Object>();

    @JsonProperty("PersonId")
    public Long getPersonId() {
        return personId;
    }

    @JsonProperty("PersonId")
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @JsonProperty("CategoryCode")
    public String getCategoryCode() {
        return categoryCode;
    }

    @JsonProperty("CategoryCode")
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    @JsonProperty("ObjectVersionNumber")
    public Long getObjectVersionNumber() {
        return objectVersionNumber;
    }

    @JsonProperty("ObjectVersionNumber")
    public void setObjectVersionNumber(Long objectVersionNumber) {
        this.objectVersionNumber = objectVersionNumber;
    }

    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
