package soap;

import biPReports.RestHelper;

import com.appspro.sewedyts.bean.TransactionProcessBean;

import com.appspro.sewedyts.bean.ProjectsCostJobBean;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class createPC {
    
    public   String status;
    public   String statusCode;
   
   
    
    public  String callPostRest(String destUrl,
                                       ProjectsCostJobBean bean) throws IOException,
                                                                 SAXException,
                                                                 ParserConfigurationException {
        RestHelper rh = new RestHelper();

        try {

        String postData =
          "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\">/n"+  
          "<soapenv:Header/>/n"+     
          "<soapenv:Body>/n"+     
          "<typ:submitESSJobRequest>/n"+       
            "<typ:jobPackageName>/oracle/apps/ess/projects/costing/transactions/onestop/</typ:jobPackageName>/n"+            
            "<typ:jobDefinitionName>ImportProcessParallelEssJob</typ:jobDefinitionName>/n"+           
              "<typ:paramList>"+bean.getBusinessUnit()+"</typ:paramList>/n"+          
              "<typ:paramList>300000002122898</typ:paramList>/n"+          
              "<typ:paramList>IMPORT_AND_PROCESS</typ:paramList>/n"+          
              "<typ:paramList>ALL</typ:paramList>/n"+          
              "<typ:paramList>#NULL</typ:paramList>/n"+          
              "<typ:paramList>300000001818812</typ:paramList>/n"+         
              "<typ:paramList>300000001818815</typ:paramList>/n"+         
              "<typ:paramList>"+bean.getExpenditureBatch()+"</typ:paramList>/n"+         
              "<typ:paramList>"+bean.getFromProjectNumber()+"</typ:paramList>/n"+         
              "<typ:paramList>"+bean.getToProjectNumber()+"</typ:paramList>/n"+        
              "<typ:paramList>"+bean.getExpenditureItemDate()+"</typ:paramList>/n"+         
              "<typ:paramList>#NULL</typ:paramList>/n"+        
              "<typ:paramList>ORA_PJC_SUMMARY</typ:paramList>/n"+       
              "</typ:submitESSJobRequest>/n"+    
             "</soapenv:Body>/n"+  
           "</soapenv:Envelope>";
            
            System.out.println(postData);
            System.setProperty("DUseSunHttpHandler", "true");
            byte[] buffer = new byte[postData.length()];

            buffer =  postData.toString().getBytes();
            System.out.println("bufffffffffffffffffffer is \n"+buffer);

            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                rh.trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";

            http.setRequestProperty("Content-Length",String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization", "Basic " + "YWhtZWRzaGVyaWZmOjEyMzQ1Njc4");
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);

            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());

            if (http.getResponseCode() == 200) {
                InputStream in = http.getInputStream();
                InputStreamReader iReader = new InputStreamReader(in);
                BufferedReader bReader = new BufferedReader(iReader);

                String line;
                String response = "";
                System.out.println("==================Service response: ================ ");
                while ((line = bReader.readLine()) != null) {
                    response += line;
                    System.out.println(response);

                }

                if (response.indexOf("<?xml") > 0)
                    response =
                            response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                               15);

                DocumentBuilderFactory fact =
                    DocumentBuilderFactory.newInstance();
                fact.setNamespaceAware(true);
                DocumentBuilder builder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

                InputSource src = new InputSource();
                src.setCharacterStream(new StringReader(response));

                Document doc = builder.parse(src);

                 XML.toJSONObject(response).toString();
                System.out.println(XML.toJSONObject(response).toString());
                JSONObject o = XML.toJSONObject(response);
                //x["env:Envelope"]["env:Body"]["ns0:createRequisitionResponse"]["ns1:result"]["ns0:Status"]
                status = o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:submitESSJobRequestResponse").getJSONObject("result").toString();
     //           statusCode = o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:receiveTimecardTransactionResponse").getJSONObject("ns2:result").getJSONObject("ns1:Value").getString("ns1:Status");

    //                requisitionNum = o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:createRequisitionResponse").getJSONObject("ns1:result").getString("ns0:RequisitionNumber");
    //                requisitionStatus = o.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("ns0:createRequisitionResponse").getJSONObject("ns1:result").getString("ns0:RequisitionStatus");
              
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return status ;
    }
}
