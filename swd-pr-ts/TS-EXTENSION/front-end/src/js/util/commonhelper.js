define([], function () {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function () {
            var host = "http://192.168.1.118:7101/EBSMobile/rest";
            return host;
        };

        self.getPaaSHost = function () {
            var host = "https://apex-hcuk.db.em2.oraclecloudapps.com/apex/xx_selfService/";
            return host;
        };

        self.getAppHost = function () {
            var host = "/HHA-SSHR-BackEnd/";
            return host;
        };

        self.getAllEmployessInfo = "Empolyees/AllEmployees";
        self.generatePoNumber = "soap/soapRest";
        self.getAllEmployees = "11.13.18.05/emps";
        self.getAll = "employee/getAll";
        self.getAllTasks="11.13.18.05/projectFinancialTasks";
        self.projectDet="projecDetails";
        self.getTaskDet = 'tasks';
        self.getProjectTeamMember = 'projectTeamMember';
        self.createTs = "TsSoap/CreatTsReq";
        self.projectCost = "projectCost/updateProjectCost";
        self.getProjectCost = "projecDetails/projectCost";
        self.getAllNotifi = "workFlowNotification/allNotifications";
        self.approveAction = "workFlowNotification/updateNotifi";
        self.approveAction = "workFlowNotification/updateNotifi";
        self.getAllProjectCost = "projecDetails/getAllRequest";
        self.updateStatus = "projecDetails/updateStatus";
        self.insertTimeSheetRequest = "projecDetails/insert";
        self.getApprovalList='approval/approvaltype/';
        self.getRequestById='projecDetails/getById';
        self.deleteApprovedRequest='projecDetails/delete/';
        self.deleteApprovedListRequest='projecDetails/deleteApprovedList/';
        self.updateNotif='workFlowNotification/updateNotifi';
        self.updateAL='workFlowNotification/updateAdminAL';
        self.projectUpdateImport ='projecDetails/updateImportStatus';
        self.insertOperationRequest ='timeSheetOperation/insert';
        self.getDraftRequest ='timeSheetOperation/getById';
        self.deleteDraftReq ='timeSheetOperation/delete';
        self.getMaxId ='timeSheetOperation/maxId';
        self.getAllDepartments ='Empolyees/AllDepartments';
         self.addWeeklySheet = "weeklySheet/newVersion";
        
        self.REPORT_DATA_TYPE = {
            UDT: 'UDT', LOOKUP: 'LOOKUP', TERRITORIES: 'TERRITORIES'
        };

        self.FUSE_SERVICE_PARAM = {
            BINDLOOKUPTYPE: self.BTRIP_TYPE + ',' + self.YES_NO + ',' + self.ADV_MONTHS + ',' + self.SA_BANKS + ',' + self.BTRIPDRIVER_TYPE + ',' + self.BTRIPDRIVER_AREA + ',' + self.HRC_YES_NO + ',' + self.NADEC_HR_ID_MAIL_TYPE + ',' + self.NADEC_HR_IDENTIFICATION_LANG + ',' + self.HR_TICKETS_ROUTES + ',' + self.HR_TICKETS_REASONS + ',' + self.HR_TICKETS_CLASS + ',' + self.HR_GRIEVANCE_TYPE + ',' + self.HR_GRIEVANCE_STATUS + ',' + self.HR_MGR_GRIEVANCE_STATUS + ',' + self.HR_HEAD_GRIEVANCE_STATUS + ',' + self.EDU_YEARS + ',' + self.EDU_SEMESTER + ',' + self.HR_ALLOWANCES + ',' + self.NADEC_BTR_ROUTE_TYPE + ',' + self.HR_HEAD_HR_GRIEVANCE_STATUS + ',' + self.NADEC_CAR_INSIDE_LOV + ',' + self.HR_CAR_LOCATION + ',' + self.NADEC_TICKET_RAUTES_NEW_US + ',' + self.HR_NADEC_HOUSING_PERIOD + ',' + self.NADEC_TICKET_REFUND + ',' + self.NADEC_TRIP_DIRECTION_1 + ',' + self.NADEC_TICKET_RAUTES_NEW_US,
            P_TABLE_NAME: 'XXX_HR_REG_BTRIP_DAYS_B,XXX_HR_TRAIN_BTRIP_DAYS_B,XXX_HR_REG_BTRIP_DAYS_A,XXX_HR_TRAIN_BTRIP_DAYS_A,XXX_HR_REG_BTRIP_PERDIEM,XXX_HR_TRAIN_BTRIP_PERDIEM,XXX_HR_REG_BTRIP_TICKET,XXX_HR_TRAIN_BTRIP_TICKET,XXX_HR_PART_OF_EOS_AMT,XXX_HR_GLOBAL_VALUES,XXX_HR_ALLOWANCES_DETAILS,' + self.NADEC_BTR_ROUTE_TYPE, BINDLANGUAGE: 'US'
        };

        self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };

        self.getInternalRest = function () {
         
//          var host = "http://127.0.0.1:7101/ELSEWEDY-TS-context-root/webapi/";
         var host = "https://144.21.71.46:7002/ELSEWEDY-TS-context-root/webapi/";

            return host;
        };

    }

    return new commonHelper();
});