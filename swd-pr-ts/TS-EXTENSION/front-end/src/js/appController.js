/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmessages', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
    'ojs/ojoffcanvas', 'ojs/ojbutton', 'jquery', 'ojs/ojarraytabledatasource', 'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojdialog'],
        function (oj, ko, moduleUtils, app, services, commonhelper) {
            function ControllerViewModel() {
                var self = this;
                var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);

                self.username = ko.observable();
                self.password = ko.observable();
                self.userLogin = ko.observable();
                self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
                self.empGrade = ko.observable();
                self.empDepartment = ko.observable();
                self.empNumber = ko.observable();
                self.empJob = ko.observable();
                self.empName = ko.observable();
                self.jwt = ko.observable();
                self.usersArr = ko.observableArray();
                self.items = ko.observableArray();
                self.empItems = ko.observableArray();
                self.orgData = ko.observableArray();
                self.taskData = ko.observableArray();
                self.projArr = ko.observableArray();
                self.locationArr = ko.observableArray();
                self.vendorSiteArr = ko.observableArray();
                self.vendorArr = ko.observableArray();
                self.customerNameArr = ko.observableArray();
                self.loginUser = ko.observable();
                self.invoiceNum = ko.observableArray();
                self.buyerNameArr = ko.observableArray();
                self.businessUnitArr = ko.observableArray();
                self.invoiceNumArr = ko.observableArray();
                self.purchaseOrdersArrPoHeader = ko.observableArray();
                self.purchaseOrdersArrPoNum = ko.observableArray();
                self.vendorSiteIndepArr = ko.observableArray();
                self.vendorNameVal = ko.observable();
                self.messagesDataProvider = ko.observableArray([]);
                self.test2 = ko.observableArray([]);
                self.test3 = ko.observableArray([]);
                self.projectNameArr = ko.observableArray();
                self.projectNameArr2 = ko.observableArray();
                self.projectNameAr = ko.observableArray();
                self.proData = ko.observableArray([]);
                self.proData2 = ko.observableArray();
                self.projectOrganizArr = ko.observableArray();
                self.loginBtnLbl = ko.observable("LOGIN");
                self.personDetails = ko.observableArray([]);
                self.personNumber = ko.observable();
                self.notiCount = ko.observable(0);
                self.teamMemberArr = ko.observableArray();
                self.urlsArr = ko.observableArray();
                self.approvalProArr = ko.observableArray();
                self.teamMemberProArr = ko.observableArray();
                self.check = ko.observable();
                self.maxId=ko.observableArray();
                self.projectCurrentOrg=ko.observable();
                
                sessionStorage.clear();

//     ----------------------------------------  loading function ------------------------ 

                self.loading = function (load) {
                    if (load) {
                        $('#loading').show();
                    } else {
                        $('#loading').hide();
                    }
                };

//     ---------------------------------------- message function ------------------------ 

                self.createMessage = function (type, summary, time) {
                    return {
                        severity: type,
                        summary: summary,
                        autoTimeout: 10000
                    };
                };
              
              
                // Search Function for lables 
                self.searchArray = function (nameKey, searchArray) {
                    for (var i = 0; i < searchArray.length; i++) {
                        if (searchArray[i].value == nameKey) {
                            return searchArray[i].desc;
                        }
                    }
                };

                if (sessionStorage.empName != "null")
                {
                    self.empName(sessionStorage.empName);
                } else
                {
                    self.empName('');
                }
                if (sessionStorage.empJob != "null")
                {
                    self.empJob(sessionStorage.empJob);
                } else
                {
                    self.empJob('');
                }
                if (sessionStorage.empNumber != "null")
                {
                    self.empNumber(sessionStorage.empNumber);
                } else
                {
                    self.empNumber('');

                }
                if (sessionStorage.empDepartment != "null")
                {
                    self.empDepartment(sessionStorage.empDepartment);
                } else
                {
                    self.empDepartment('');
                }
                if (sessionStorage.empGrade != "null")
                {
                    self.empGrade(sessionStorage.empGrade);
                } else
                {
                    self.empGrade('');
                }

                self.isUserLoggedIn = ko.observable(false);

                var url = new URL(window.location.href);
                var jwt;

                if (url.searchParams.get("jwt")) {
                    jwt = url.searchParams.get("jwt");
                    self.jwt(jwt);


                    jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
                    var username = jwtJSON.sub;
                    self.username(username.split('.')[0] + username.split('.')[1]);

                    self.userLogin(sessionStorage.Username);

                    if (self.isUserLoggedIn() === 'false') {
                        self.isUserLoggedIn(false);
                    } else {
                        self.isUserLoggedIn(true);
                    }

                } else if (sessionStorage.isUserLoggedIn) {
                    self.userLogin(sessionStorage.Username);


                    if (self.isUserLoggedIn() === 'false') {
                        self.isUserLoggedIn(false);
                    } else {
                        self.isUserLoggedIn(true);
                    }

                }


                $(function () {
                    $('#username').on('keydown', function (ev) {
                        var mobEvent = ev;
                        var mobPressed = mobEvent.keyCode || mobEvent.which;
                        if (mobPressed == 13) {
                            $('#username').blur();
                            $('#password').focus();
                        }
                        return true;
                    });
                });


//     ---------------------------------------- login button function------------------------ 

                self.validateUser = function () {
                    $(".apLoginBtn button").addClass("loading");
                    var username = self.username();
                    var password = self.password();
                    // Here we check null value and show error to fill
                    if (!username && !password) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter user name'));

                    } else if (!username) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter user name'));

                    } else if (!password) {

                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter user name'));

                    } else {
                        $('#loader-overlay').show();
//                        req.validateUser(username, password, function (response) {
                        var loginCbFn = function (data) {
                            self.test2([]);
                            self.getEmployeeDetails();
                            self.getprojectsDetails();
                            oj.Router.rootInstance.go('timeSheetSummary');

                            $(".apLoginBtn button").removeClass("loading");
                            self.messagesDataProvider([]);

                            var parsedData = jQuery.parseJSON(data.replace('\'', '"').replace('\'', '"'));

                            if (parsedData.result == true) {
                                self.isUserLoggedIn(true);
                                self.messagesDataProvider([]);
                                sessionStorage.setItem('username', self.isUserLoggedIn());
//                          self.messagesDataProvider.push(self.createMessage('error', 'Wrong user name or passowrd'));

                                var userNamelabel = self.username();
                                sessionStorage.setItem('isUserLoggedIn', self.isUserLoggedIn());
                                sessionStorage.setItem('Username', userNamelabel);

                                $('#loader-overlay').hide();

                            } else {

                                $(".apLoginBtn button").removeClass("loading");
                                // If authentication failed
                                // alert(response.Error);
                                self.messagesDataProvider([]);
                                self.messagesDataProvider.push(self.createMessage('error', 'Wrong user name or passowrd'));

                                return;
                                $('#loader-overlay').hide();
                            }
                        };
                        self.loginUser(username);

                        var loginFailCbFn = function () {
                            self.messagesDataProvider.push(self.createMessage('error', 'Cannot validate UserName'));

                            $(".apLoginBtn button").removeClass("loading");
                            return;
                            $('#loader-overlay').hide();
                        };

                        var payload = {
                            "userName": username,
                            "password": password
                        };
                        services.authenticate(payload).then(loginCbFn, loginFailCbFn);
                        return;
                    }
                };

//     ---------------------------------------- home icon function------------------------ 

                $(document).on('click', '.home-icon', function () {
                    oj.Router.rootInstance.go('timeSheetSummary');
                });

//     ---------------------------------------- notification icon function ------------------------ 

                $(document).on('click', '.notifi-icon', function () {
                    oj.Router.rootInstance.go('notificationScreen');
                });

//     ---------------------------------------- get all employess function ------------------------ 

                self.getAllEmpolyessInfo = async function ()
                {
                    var getEmployessCBCF = function (data) {
                        self.loading(false);
                        self.empItems(data);
                        for (var item in data) {
                            var x = JSON.parse(data[item].assingments)                          
                            self.usersArr.push({
                                label: data[item].displayName,
                                value: data[item].personNumber,
                                displayName:data[item].userName,
                                assingments:x
                            });
                        }
                    };

                    var failCbFn = function (err) {

                    };
                    var empUrl = commonhelper.getAllEmployessInfo;
                    await services.getAllEmployees(empUrl).then(getEmployessCBCF, failCbFn);
                };

//     ---------------------------------------- get all projects function------------------------ 

                self.getprojectsDetails = function ()
                {
                    var projectDetailsCbFn = function (data) {
                        var data3 = data.filter(o => o.approvalStatus === "Approved" || o.approvalStatus === "Tender" 
                                           || o.approvalStatus === "bedding" || o.approvalStatus === "Project" || o.approvalStatus === "Warranty");
                        self.proData2(data3);
                         for (var i = 0; i < data3.length; i++) {
                                var arr = JSON.parse(data3[i].TeamMembers);
                                self.teamMemberProArr(arr);
                            }
                        $.each(data3, function (index) {
                            self.projectNameArr2().push(
                                    {
                                        label: data3[index].projectName,
                                        value: data3[index].projectNumber,
                                        ProjectStatus:data3[index].approvalStatus
                                    }
                            );
                        });
                    };
                    var failTSCbFn = function () {
                    };
                    services.getGeneric(commonhelper.projectDet, {}).then(projectDetailsCbFn, failTSCbFn);
                };
                //     ---------------------------------------- report to get all bussines unit function------------------------ 

                self.getProjectOrangzations = async function () {
                    var getProOrangzations = function (data) {
                        $.each(data, function (index) {
                            self.projectOrganizArr().push({
                                label: data[index].BU_NAME,
                                value: data[index].BU_NAME
                            });
                        });
                    };
                    var failCbFn = function ()
                    {

                    };
                    await services.getGenericReport({"reportName": "BUSINESS_UNIT_REPORT"}).then(getProOrangzations, failCbFn);
                };

  
                self.getAllEmpolyessInfo();
                self.getProjectOrangzations();
            //     ---------------------------------------- report to get all project Organizations-----------
                var getProjectCurrentExpendOrg = function () {
                    var getPro = function (data) {
                        self.projectCurrentOrg(data)
                    };
                    var failCbFn = function ()
                    {
                    };
                    services.getGenericReport({"reportName": "PROJECT_ORG_ID_INFO"}).then(getPro, failCbFn);
                };
                getProjectCurrentExpendOrg();
//     ---------------------------------------- employee details for login employee function------------------------ 

                self.getEmployeeDetails = function () {
                    var getEmployeeDetailsCbFn = function (data) {

                        self.personDetails(data);
                        self.loading(false);
                        self.getALlNotification();
                        sessionStorage.setItem('empName', data.displayName);
                        sessionStorage.setItem('empJob', data.jobName);
                        sessionStorage.setItem('empNumber', data.personNumber);
                        sessionStorage.setItem('empDepartment', data.departmentName);
                        sessionStorage.setItem('empGrade', data.grade);
                        localStorage.setItem('personID', data.personId);
                        

                        if (sessionStorage.empName != "null")
                        {
                            self.empName(sessionStorage.empName);

                        } else
                        {
                            self.empName('');
                        }
                        if (sessionStorage.empJob != "null")
                        {
                            self.empJob(sessionStorage.empJob);
                        } else
                        {
                            self.empJob('');
                        }
                        if (sessionStorage.empNumber != "null")
                        {
                            self.empNumber(sessionStorage.empNumber);
                        } else
                        {
                            self.empNumber('');

                        }
                        if (sessionStorage.empDepartment != "null")
                        {
                            self.empDepartment(sessionStorage.empDepartment);
                        } else
                        {
                            self.empDepartment('');
                        }
                        if (sessionStorage.empGrade != "null")
                        {
                            self.empGrade(sessionStorage.empGrade);
                        } else
                        {
                            self.empGrade('');
                        }
                    };
                    var getEmployeeDetailsfailCbFn = function () {
                    };
                    var UrlPath;
                    if (self.loginUser()) {
                        UrlPath = "getemployee/" + self.loginUser();
                    } else {
                        UrlPath = "getemployee/" + sessionStorage.Username;
                    }

                    services.getGeneric(UrlPath).then(getEmployeeDetailsCbFn, getEmployeeDetailsfailCbFn);
                };

//     ---------------------------------------- get all notification function------------------------ 

                  self.getALlNotification = async function () {
                    var getSuccs = function (data) {
                        self.notiCount(data.length);
                    };
                    self.failFn = function () {
                    };
                    var payload = {
                        "MANAGER_ID": self.personDetails().personId,
                        "emp": self.personDetails().personId,
                        "MANAGER_OF_MANAGER": self.personDetails().personId,
                        "COST_CONTROLLER": self.personDetails().personId,
                        "FINANCE_MANAGER": self.personDetails().personId
                    };
                    await services.postGeneric(commonhelper.getAllNotifi, payload).then(getSuccs, self.failFn);
                };

//     ---------------------------------------- log out button function------------------------ 

                self.menuItemAction = function (event) {
                    // if (event.target.value === 'out') {
                    location.reload();
                    sessionStorage.clear();
                    self.isUserLoggedIn(false);
                    self.test2([]);
                    self.test3([]);
                    sessionStorage.setItem('username', '');
                    sessionStorage.setItem('isUserLoggedIn', '');


                    if (self.language() === 'english')
                    {
                        $('html').attr({'dir': 'ltr'});
                    } else if (self.language() === 'arabic')
                    {
                        $('html').attr({'dir': 'rtl'});
                    }

                };

                self.loginErrorPopup = function () {
                    var popup = document.querySelector('#popup1');
                    popup.close('#btnGo');
                };

                startAnimationListener = function (data, event) {
                    var ui = event.detail;
                    if (!$(event.target).is("#popup1"))
                        return;

                    if ("open" === ui.action)
                    {
                        event.preventDefault();
                        var options = {"direction": "top"};
                        oj.AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
                    } else if ("close" === ui.action)
                    {
                        event.preventDefault();
                        ui.endCallback();
                    }
                };


                // Router setup
                self.router = oj.Router.rootInstance;
                self.router.configure({
                    'timeSheetSummary': {label: 'TimeSheetSummary', value: "timeSheet/timeSheetSummary", title: "El Sewedy-TS", isDefault: true},
                    'timeSheetOperation': {label: 'timeSheetOperation', value: "timeSheet/timeSheetOperation", title: "Time Sheet Operation"},
                    'employeeOperation': {label: 'employeeOperation', value: "timeSheet/employeeOperation", title: "Employee Operation"},
                    'costScreen': {label: 'CostScreen', value: "timeSheet/costScreen", title: "El Sewedy - Cost Screen"},
                    'notificationScreen': {label: 'notification', value: "notification/notificationScreen", title: "El Sewedy - notification Screen"}
                });
                oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();


                self.localeLogin = ko.observable();
                self.UsernameLabel = ko.observable();
                self.passwordLabel = ko.observable();
                self.loginword = ko.observable();
                self.forgotPasswordLabel = ko.observable();
                self.localeName = ko.observable();
                self.localeJop = ko.observable();
                self.localeempNO = ko.observable();
                self.localeDep = ko.observable();
                self.localeGrade = ko.observable();


                self.language = ko.observable();
                self.languagelable = ko.observable("English");
                var storage = window.localStorage;
                var isTrue, preferedLanguage;

                self.langSelected = function (event) {
                    var valueObj = buildValueChange(event['detail']);

                    self.setPreferedLanguage(valueObj.value);
                };

                function buildValueChange(valueParam) {
                    var valueObj = {};
                    //valueObj.previousValue = valueParam.previousValue;
                    if (valueParam.previousValue) {
                        valueObj.value = valueParam.value;
                    } else {
                        valueObj.value = valueParam.previousValue;
                    }
                    //
                    return valueObj;
                }

                $(function () {
                    storage.setItem('setLang', 'english');
//                    storage.setItem('setLang', 'arabic');
                    preferedLanguage = storage.getItem('setLang');
                    self.language(preferedLanguage);
                    changeToArabic(preferedLanguage);
                    //document.addEventListener("deviceready", selectLanguage, false);
                });

                // Choose prefered language
                function selectLanguage() {
                    oj.Config.setLocale('english',
                            function () {
                                $('html').attr({'lang': 'en-us', 'dir': 'ltr'});
                                self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                self.loginword(oj.Translations.getTranslatedString('login'));
                                self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                                self.localeName(oj.Translations.getTranslatedString('Name'));
                                self.localeJop(oj.Translations.getTranslatedString('JOP'));
                                self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                                self.localeDep(oj.Translations.getTranslatedString('Dep'));
                                self.localeGrade(oj.Translations.getTranslatedString('Grade'));

                            }
                    );
                }

                // Set Prefered Language
                self.setPreferedLanguage = function (lang) {
                    var selecedLanguage = lang;
                    storage.setItem('setLang', selecedLanguage);
                    storage.setItem('setTrue', true);
                    preferedLanguage = storage.getItem('setLang');

                    // Call function to convert arabic
                    changeToArabic(preferedLanguage);
                };

                function changeToArabic(preferedLanguage) {
                    if (preferedLanguage == 'arabic') {

                        oj.Config.setLocale('ar-sa',
                                function () {
                                    $('html').attr({'lang': 'ar-sa', 'dir': 'rtl'});
                                    self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                    self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                    self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                    self.loginword(oj.Translations.getTranslatedString('login'));
                                    self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                                    self.localeName(oj.Translations.getTranslatedString('Name'));
                                    self.localeJop(oj.Translations.getTranslatedString('JOP'));
                                    self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                                    self.localeDep(oj.Translations.getTranslatedString('Dep'));
                                    self.localeGrade(oj.Translations.getTranslatedString('Grade'));
                                }
                        );

                    } else if (preferedLanguage == 'english') {
                        selectLanguage();
                    }
                }
                $('.login-label').toggleClass('user-label-right');
                $('.input100').toggleClass('box-input-text');
                $('.wrap-input100').toggleClass('box-input-text');
                self.changeLanguage = function () {
                    //storage.setItem('setLang', 'arabic');
                    if (self.language() === 'english')
                    {
                        self.language("arabic");
                        self.loginBtnLbl("تسجيل دخول")
                        self.setPreferedLanguage(self.language());
                        self.languagelable("English");
                        $('.login-label').toggleClass('user-label-right');
                        $('.login-label').removeClass('user-label-left');
                        $('.input100').toggleClass('box-input-text');
                        $('.wrap-input100').toggleClass('box-input-text');
                    } else if (self.language() === 'arabic')
                    {
                        self.language("english");
                        self.loginBtnLbl("LOGIN")
                        self.setPreferedLanguage(self.language());
                        self.languagelable("عربي");
                        $('.login-label').toggleClass('user-label-left');
                        $('.login-label').removeClass('user-label-right');
                        $('.input100').removeClass('box-input-text');
                        $('.wrap-input100').removeClass('box-input-text');


                    }
                    //  self.setPreferedLanguage('arabic');
                    //  self.languagelable("Arabic");
                };


                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});

                // Search Start
                self.keyword = ko.observableArray();
                //Business Unit Input search Start 

                self.searchWord = ko.observable();
                self.tags = [
                    {value: "Value 1", label: "Unit 1"},
                    {value: "Value 2", label: "Unit 2"},
                    {value: "Value 3", label: "Unit 3"},
                    {value: "Value 4", label: "Unit 4"},
                    {value: "Value 5", label: "Unit 5"},
                    {value: "Value 6", label: "Unit 6"},
                    {value: "Value 7", label: "Unit 7"}
                ];

                self.tagsDataProvider = new oj.ArrayDataProvider(self.tags, {keyAttributes: 'value'});

                self.search = function (event) {
                    var trigger = event.type;
                    var term;

                    if (trigger === "ojValueUpdated") {
                        // search triggered from input field
                        // getting the search term from the ojValueUpdated event
                        term = event['detail']['value'];
                        trigger += " event";
                    } else {
                        // search triggered from end slot
                        // getting the value from the element to use as the search term.
                        term = document.getElementById("search").value;
                        trigger = "click on search button";
                    }

                };

                // toolbar Start

                self.toolbarClassNames = ko.observableArray([]);

                self.toolbarClasses = ko.computed(function () {
                    return self.toolbarClassNames().join(" ");
                }, this);
                // toolbar End

                // Related to New Design Add BY Mohamed Yasser  //End

                self.loadModule = async function () {
//                    oj.Router.rootInstance.go('timeSheetSummary');
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath})
                        ]);
                        masterPromise.then(
                                function (values) {
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                },
                                function (reason) {}
                        );
                    });
                };

                // Navigation setup
                var navData = [
                    /* {name: 'Search', id: 'searchProformaInvoice',
                     iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'},
                     {name: 'Acknowledge', id: 'proFormaInvAckn',
                     iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'},
                     {name: "Search Invoices", id: 'search',
                     iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'}*/
//	   {name:  "Product Registration", id: 'productRegistration',
//       iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'},
                ];
                self.navDataSource = new oj.ArrayTableDataSource(navData, {idAttribute: 'id'});

                // Drawer
                // Close offcanvas on medium and larger screens
                self.mdScreen.subscribe(function () {
                    oj.OffcanvasUtils.close(self.drawerParams);
                });
                self.drawerParams = {
                    displayMode: 'push',
                    selector: '#navDrawer',
                    content: '#pageContent'
                };
                // Called by navigation drawer toggle button and after selection of nav drawer item
                self.toggleDrawer = function () {
                    return oj.OffcanvasUtils.toggle(self.drawerParams);
                };
                // Add a close listener so we can move focus back to the toggle button when the drawer closes
                $("#navDrawer").on("ojclose", function () {
                    $('#drawerToggleButton').focus();
                });

                // Header
                // Application Name used in Branding Area
                self.appName = ko.observable("Nexus");
                // User Info used in Global Navigation area


                // Footer
                function footerLink(name, id, linkTarget) {
                    this.name = name;
                    this.linkId = id;
                    this.linkTarget = linkTarget;
                }
                self.footerLinks = ko.observableArray([
                    new footerLink('About Oracle', 'aboutOracle', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
                    new footerLink('Contact Us', 'contactUs', 'http://www.oracle.com/us/corporate/contact/index.html'),
                    new footerLink('Legal Notices', 'legalNotices', 'http://www.oracle.com/us/legal/index.html'),
                    new footerLink('Terms Of Use', 'termsOfUse', 'http://www.oracle.com/us/legal/terms/index.html'),
                    new footerLink('Your Privacy Rights', 'yourPrivacyRights', 'http://www.oracle.com/us/legal/privacy/index.html')
                ]);
            }

            $(window).scroll(function () {
                var windowHeight = $(window).scrollTop();
                if (windowHeight > 100) {
                    $("header").addClass("swNavbarBackground ");
                    $("header").removeClass("swNavbar");
                } else {
                    $("header").addClass("swNavbar");
                    $("header").removeClass("swNavbarBackground ");
                }
            });

            return new ControllerViewModel();
        }
);
