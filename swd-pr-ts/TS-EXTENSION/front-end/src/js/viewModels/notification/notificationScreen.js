/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * notificationScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojarraydataprovider'
            , 'ojs/ojnavigationlist', 'ojs/ojtable', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojdialog'
            , 'ojs/ojswitcher','ojs/ojpagingcontrol','ojs/ojcheckboxset'],
        function (oj, ko, $, app, commonhelper, services) {
            /**
             * The view model for the main content view template
             */
            function notificationScreenContentViewModel() {
                var self = this;

                self.Disable2 = ko.observable(true);
                self.viewDisable = ko.observable(true);
                self.approveDisable = ko.observable();
                self.rejectDisable = ko.observable();
                self.messages = ko.observableArray();
                self.deptObservableArray = ko.observableArray([]);
                self.id = ko.observableArray([]);
                self.viewDataArr = ko.observableArray();
                self.dataprovider = new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'});
                self.dataprovider2 = new oj.ArrayTableDataSource(self.viewDataArr, {idAttribute: 'id'});
                self.dataSource = ko.observable();
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'}));
                self.columnArray = ko.observableArray();
                self.columnArrayView = ko.observableArray();
                self.approvalStatus = ko.observable();

                self.viewLbl = ko.observable();
                self.notificationType = ko.observable();
                self.approveLbl = ko.observable();
                self.rejectLbl = ko.observable();
                self.msgTitle = ko.observable();
                self.msgBody = ko.observable();
                self.requestId = ko.observable();
                self.requesterNumber = ko.observable();
                self.type = ko.observable();
                self.creationDate = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.selectedRowId = ko.observable();
                self.selectedrequestID = ko.observable();
                self.rwindex = ko.observable();
                self.notificationTypeValue = ko.observable();
                self.requestNumbreValue = ko.observable();
                self.notificationTypeArr = ko.observableArray();
                self.PRAM = ko.observableArray();

                self.notificationTypeArr([{label: "FYA", value: "FYA"}, {label: "FYI", value: "FYI"}]);
                self.PRAM([{NOTI: "FYA", RCODE: "APPROVED"}, {NOTI: "FYI", RCODE: "Delivered"}]);
                self.placeholder = ko.observable('Please Select');
                self.tempArr = ko.observableArray();
                self.allDataArr = ko.observableArray();
                self.isLastApproved = ko.observable("300000001621679");
                self.employeeName = ko.observable();
                self.employeeNumber = ko.observable();
                self.projectNumber = ko.observable();
                self.projectName = ko.observable();
                self.taskNumber = ko.observable();
                self.taskName = ko.observable();
                self.expenditureOrganization = ko.observable();
                self.timeSheetType = ko.observable();
                self.transactionDate = ko.observable();
                self.Expendituretype = ko.observable();
                self.Quantity = ko.observable();
                self.employeeNameValue = ko.observable();
                self.employeeNumberValue = ko.observable();
                self.projectNumberValue = ko.observable();
                self.projectNameValue = ko.observable();
                self.taskNumberValue = ko.observable();
                self.taskNameValue = ko.observable();
                self.timeSheetDateFromValue = ko.observable();
                self.expenditureOrganizationValue = ko.observable();
                self.timeSheetDateToValue = ko.observable();
                self.timeSheetTypeValue = ko.observable();
                self.viewDialogTiltle = ko.observable()
                self.batchName = ko.observable();
                self.rejectReson = ko.observable();
                self.rejectReasonVal = ko.observable();
                 self.selectedItems=ko.observableArray();
                 self.selectedItemsArr=ko.observableArray();
                
                
                
            /* --------------------------- function for handle checkbox checked   --------------------------- */

                self.handleCheckbox = function (id) {
                    var isChecked = self.getIndexInSelectedItems(id) !== -1;
                    return isChecked ? ["checked"] : [];
                }; 
          /* --------------------------- checkbox Listener   --------------------------- */
                 self.checkboxListener = function (event)
                {
                    if (event.detail !== null)
                    {
                        
                        var value = event.detail.value;
                        var newSelectedItems;
                        var id = event.target.dataset.rowId;
                        if (value.length > 0) {
                            newSelectedItems = self.getIndexInSelectedItems(id) === -1 ?
                                    self.selectedItems().concat([{startIndex: {row: id}, endIndex: {row: id}}]) :
                                    self.selectedItems();
                        } else {
                            newSelectedItems = self.selectedItems();
                            var idx = self.getIndexInSelectedItems(id);
                            if (idx !== -1)
                            {
                                var item = newSelectedItems.splice(idx, 1)[0];
                                // break up ranges into two separate ranges if index is in range;
                                if (item.startIndex.row !== item.endIndex.row)
                                {
                                    var newItems = [];
                                    if (id !== 0)
                                    {
                                        newItems.push({startIndex: {row: item.startIndex.row}, endIndex: {row: Number(id) - 1}});
                                    }
                                    if (id !== self.dataProvider.totalSize() - 1)
                                    {
                                        newItems.push({startIndex: {row: Number(id) + 1}, endIndex: {row: item.endIndex.row}});
                                    }
                                    newSelectedItems = newSelectedItems.concat(newItems);
                                }
                            }
                        }
                        self.selectedItems(newSelectedItems);  
                      
                    }            
                    self.printCurrentSelection(); 
                    if(self.selectedItems().length > 0){
                        self.Disable2(false);
                    }else{
                         self.Disable2(true);  
                    }
                }; 
         /* --------------------------- function for print Current Selection   --------------------------- */
                
                    self.printCurrentSelection = function () {
                    var selectedId;
                    self.selectedItemsArr([]);
                    for (var i = 0; i < self.selectedItems().length; i++) {
                        selectedId = self.selectedItems()[i].startIndex.row;
                        for (var x = 0; x < self.deptObservableArray().length; x++) {
                            if (self.deptObservableArray()[x].seq -1 == selectedId)
                                self.selectedItemsArr.push(self.deptObservableArray()[x]);
                        }
                    }
                };
       /* --------------------------- function for get Current Item  --------------------------- */
                self.getIndexInSelectedItems = function (id) {

                    for (var i = 0; i < self.selectedItems().length; i++)
                    {
                        var range = self.selectedItems()[i];
                        var startIndex = range.startIndex.row;
                        var endIndex = range.endIndex.row;
                        if (id >= startIndex && id <= endIndex)
                        {
                            return i;
                        }
                    }
                    return -1;
                };

                //  ---------------------- Dom String -------------------------

                var DOMsStrings = {

                    approvalDialog: '#ApproveDialog',
                    rejectDialog: '#rejectDialog',
                    viewDialog: '#ViewDialog'

                };

//  ---------------------- connected function -------------------------


                self.connected = function () {
                    self.getALlNotification();
                };

//  ---------------------- table selection for notification request -------------------------

                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow) {
                    //     self.getALlNotification();
//                        if (self.deptObservableArray()[[currentRow['rowIndex']]].type == "FYI") {
//                            self.Disable2(true);
//                        } else {
//                            self.Disable2(false);
//                        }
                        self.viewDisable(false);
                        self.rwindex(currentRow['rowIndex']);
                        self.selectedrequestID(self.deptObservableArray()[[currentRow['rowIndex']]].id);
//                        self.selectedrequestID(self.deptObservableArray()[[currentRow['rowIndex']]].requestId);
                    }
                };

//  ---------------------- table Selection Listener View -------------------------

                self.tableSelectionListenerView = function (event) {
                };

//  ---------------------- position  -------------------------

                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };

//  ---------------------- message handler -------------------------

                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };
//  ---------------------- fun to request details -------------------------
                var getData = function () {
                    var sucessCbFn = function (data) {
                        for (var item in data) {
                            data[item].seq = parseInt(item) + 1;
                        }
                        self.deptObservableArray(data);
                    };

                    var payload = [{
                            "id": self.id()
                        }];
                    services.postGeneric(commonhelper.getRequestById, payload[0].id).then(sucessCbFn, app.failCbFn);
                };
//  ---------------------- get all notifications  -------------------------

                self.getALlNotification = function () {
                    self.allDataArr([]);
                    self.deptObservableArray([]);
                    self.id([]);
                    var getSuccs = function (data) {
                        self.id([]);
                        for (var item in data) {
                            data[item].seq = parseInt(item) + 1;
                            self.id.push({id:data[item].requestId});
                        }
                        self.allDataArr(data);
                            getData();
                        app.notiCount(self.allDataArr().length);
                        self.tempArr(data);
                    };
                  var payload = {
                        "MANAGER_ID": app.personDetails().personId?app.personDetails().personId:localStorage.personID,
                        "emp": app.personDetails().personId?app.personDetails().personId:localStorage.personID,
                        "MANAGER_OF_MANAGER": app.personDetails().personId?app.personDetails().personId:localStorage.personID,
                        "COST_CONTROLLER": app.personDetails().personId?app.personDetails().personId:localStorage.personID,
                        "FINANCE_MANAGER": app.personDetails().personId?app.personDetails().personId:localStorage.personID 
                    };
                    services.postGeneric(commonhelper.getAllNotifi, payload).then(getSuccs, app.failCbFn);
                };

//  ---------------------- approval request button -------------------------

                self.approveRequst = function () {
                    if (app.personDetails().displayName == 'Ahmed Sheriff') {
                        self.adminUserUpdateStatus();
                        self.adminUserUpdateNoti();
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'Request Approved'));
                        self.updateAdminAL();
                    } else {
                        app.loading(true);
                        var idArr=[];
                        for (var i = 0; i < self.selectedItemsArr().length; i++) {
                            idArr.push(self.selectedItemsArr()[i].id.toString());
                            for (var j in self.allDataArr()) {
                                if (self.selectedItemsArr()[i].id == self.allDataArr()[j].requestId) {
                                    var headers = {
                                        "MSG_TITLE": self.allDataArr()[j].msgTitle,
                                        "MSG_BODY": self.allDataArr()[j].msgBody,
                                        "TRS_ID": self.allDataArr()[j].requestId,
                                        "PERSON_ID": app.personDetails().personId,
                                        "RESPONSE_CODE": "APPROVED",
                                        "ssType": self.allDataArr()[j].selfType,
                                        "PERSON_NAME": self.allDataArr()[j].personName
                                    };
                              
                            

                            var approvalActionCBFN = function () {
                                app.loading(false);
                                if (self.isLastApproved() == app.personDetails().personId) {
                                    for(var v in idArr)
                                    {
                                    self.adminUserUpdateNoti();
                                    var updateStatusCbFn = function (data) {
                                    };
                                    var payload2 = {
                                        "approvalStatus": "APPROVED",
                                        "id": idArr[v]
                                    };
                                    services.postGeneric(commonhelper.updateStatus, payload2).then(updateStatusCbFn, app.failCbFn);
                                }
                                }
                                self.getALlNotification();
                                app.messagesDataProvider.push(app.createMessage('confirmation', 'Request Approved'));
//                        oj.Router.rootInstance.go('costScreen');

                            };
                            services.postGeneric("workFlowNotification", headers).then(approvalActionCBFN, app.failCbFn);
                        }
                    
                            }
                              }
                            }
                };

//  ---------------------- reject request button -------------------------

                self.RejectRequst = function () {
                    app.loading(true);
                    self.adminUserUpdateNoti();
                    for (var i = 0; i < self.selectedItemsArr().length; i++) {
                        for (var j in self.allDataArr()) {
                            if (self.selectedItemsArr()[i].id == self.allDataArr()[j].requestId) {
                                var idArr=self.allDataArr()[j].requestId.toString();
                                var headers = {
                                    "MSG_TITLE": self.allDataArr()[j].msgTitle,
                                    "MSG_BODY": self.allDataArr()[j].msgBody,
                                    "TRS_ID": self.allDataArr()[j].requestId,
                                    "PERSON_ID": app.personDetails().personId,
                                    "RESPONSE_CODE": "REJECTED",
                                    "ssType": self.allDataArr()[j].selfType,
                                    "PERSON_NAME": self.allDataArr()[j].personName
                                };
                            }
                            var payload2 = {
                                "approvalStatus": "REJECTED",
                                "rejectReason": self.rejectReasonVal() ? self.rejectReasonVal() : "No Reject Reason",
                                "id": idArr
                            };
                            var updateStatusCbFn = function () {
//                            oj.Router.rootInstance.go('costScreen');
                                self.getALlNotification();
                            };
                            services.postGeneric(commonhelper.updateStatus, payload2).then(updateStatusCbFn, app.failCbFn);

                        }
                        var approvalActionCBFN = function () {
//                            self.getALlNotification();
                            app.loading(false);
                            app.messagesDataProvider.push(app.createMessage('warning', 'REQUEST REJECTED'));
                        };
                        services.postGeneric("workFlowNotification", headers).then(approvalActionCBFN, app.failCbFn);
                        }
                   
                };

//  ---------------------- admin user notification -------------------------

                self.adminUserUpdateNoti = function () {
                    for (var i = 0; i < self.selectedItemsArr().length; i++) {
                        var payload2 = {
                            "requestId": self.selectedItemsArr()[i].id.toString()
                        };
                        var updateStatusCbFn = function () {
                            self.getALlNotification();
                        };
                        services.postGeneric(commonhelper.updateNotif, payload2).then(updateStatusCbFn, app.failCbFn);
                    }
           
                };
                self.adminUserUpdateStatus = function () {
                    for (var i=0; i< self.selectedItemsArr().length;i++){
                        
                        var updateStatusCbFn = function () {
                            self.getALlNotification();
                        };
                        var payload2 = {
                            "approvalStatus": "APPROVED",
                            "id": self.selectedItemsArr()[i].id.toString()
                        };

                    services.postGeneric(commonhelper.updateStatus, payload2).then(updateStatusCbFn, app.failCbFn);
                    }
                    
                };
                self.updateAdminAL = function () {
                    for(var j = 0 ; j<self.selectedItemsArr().length;j++){
                            
                   
                    var updateSucsessCbFn = function () {
                    };
                    for (var i = 0; i < self.PRAM().length; i++) {
                        var payload2 = {
                            "notificationType": self.PRAM()[i].NOTI,
                            "responseCode": self.PRAM()[i].RCODE,
                            "transactionId": self.selectedItemsArr()[j].id.toString()
                        };
                        services.postGeneric(commonhelper.updateAL, payload2).then(updateSucsessCbFn, app.failCbFn);
                    }
 }
                };

//  ---------------------- open and close notification screen dialog -------------------------

                self.commitRecord = function () {
                    self.approveRequst();
                    document.getElementById('ApproveDialog').close();
                };
                self.cancelButton = function () {
                    document.getElementById('ApproveDialog').close();
                };
                self.cancelRejectButton = function () {
                    document.getElementById('rejectDialog').close();
                };
                self.cancelViewButton = function () {
                    document.querySelector('#ViewDialog').close();
                };
                self.approveBtnAction = function () {

                    document.querySelector('#ApproveDialog').open();
                };
                self.rejectBtnAction = function () {
                    document.querySelector('#rejectDialog').open();
                };
                self.commitRejectRecord = function () {
                    self.RejectRequst();
                    document.querySelector('#rejectDialog').close();
                };



                var getTranslation = oj.Translations.getTranslatedString;

                function initTranslations() {

                    self.requestId(getTranslation("notification.requestId"));
                    self.msgTitle(getTranslation("notification.msgTitle"));
                    self.msgBody(getTranslation("notification.msgBody"));
                    self.requesterNumber(getTranslation("notification.requesterNumber"));
                    self.type(getTranslation("notification.type"));
                    self.creationDate(getTranslation("notification.creationDate"));
                    self.viewLbl(getTranslation("buttoms.viewBtn"));
                    self.approveLbl(getTranslation("buttoms.approveLbl"));
                    self.rejectLbl(getTranslation("buttoms.rejectLbl"));
                    self.yes(getTranslation("timeSheetSummary.yes"));
                    self.no(getTranslation("timeSheetSummary.no"));
                    self.confirmMessage(getTranslation("timeSheetSummary.confirmMessage"));
                    self.oprationMessage(getTranslation("timeSheetSummary.oprationMessage"));
                    self.notificationType(getTranslation("notification.notificationType"));
                    self.employeeName(getTranslation("timeSheetSummary.employeeName"));
                    self.employeeNumber(getTranslation("timeSheetSummary.employeeNumber"));
                    self.projectNumber(getTranslation("timeSheetSummary.projectNumber"));
                    self.projectName(getTranslation("timeSheetSummary.projectName"));
                    self.taskNumber(getTranslation("timeSheetSummary.taskNumber"));
                    self.taskName(getTranslation("timeSheetSummary.taskName"));
                    self.expenditureOrganization(getTranslation("timeSheetSummary.expenditureOrganization"));
                    self.timeSheetType(getTranslation("timeSheetSummary.timeSheetType"));
                    self.transactionDate(getTranslation("timeSheetSummary.transactionDate"));
                    self.Expendituretype(getTranslation("timeSheetSummary.Expendituretype"));
                    self.Quantity(getTranslation("timeSheetSummary.Quantity"));
                    self.batchName(getTranslation("timeSheetSummary.batchName"));
                    self.viewDialogTiltle(getTranslation("notification.viewDialogTiltle"));
                    self.rejectReson(getTranslation("notification.rejectReson"));

                    self.columnArray([
                        {
                            "headerText": "Selection", "template": "checkTemplate"
                        },
                        {
                            "headerText": self.transactionDate(), "field": "transactionDate"
                        },
                        {
                            "headerText": self.projectName(), "field": "projectName"
                        },
                        {
                            "headerText": self.projectNumber(), "field": "projectNumber"
                        },
                        {
                            "headerText": self.taskName(), "field": "taskName"
                        },
                        {
                            "headerText": self.taskNumber(), "field": "taskNumber"
                        },
                        {
                            "headerText": self.employeeName(), "field": "employeeName"
                        },
                        {
                            "headerText": self.Expendituretype(), "field": "expenditureType"
                        },
                        {
                            "headerText": self.expenditureOrganization(), "field": "organization"
                        },
                        {
                            "headerText": self.timeSheetType(), "field": "timeSheetType"
                        },
                        {
                            "headerText": self.Quantity(), "field": "quantity"
                        },
                        {
                            "headerText": self.batchName(), "field": "batchName"
                        }
                    ]);
                }
                initTranslations();

            }

            return notificationScreenContentViewModel;
        });
