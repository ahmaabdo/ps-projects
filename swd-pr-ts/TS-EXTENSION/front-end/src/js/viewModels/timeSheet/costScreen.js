/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * costScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'xlsx', 'util/commonhelper', 'ojs/ojarraydataprovider',
    'ojs/ojdatetimepicker', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojvalidationgroup', 'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojdialog', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol'
], function (oj, ko, $, app, services, x, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function costScreenContentViewModel() {
        var self = this;
        self.isDisabled = ko.observable(false);
        self.isDisabledEmpVal = ko.observable(true);
        self.isDisabledProNumVal = ko.observable(true);
        self.isDisabledTaskNumVal = ko.observable(true);
        self.searchDisable = ko.observable(false);
        self.resetDisable = ko.observable(false);
        self.approvalDisable = ko.observable(true);
        self.disabledLbl = ko.observable(true);
        self.DisabledTotal = ko.observable(true);
        self.exportProCostDis = ko.observable(true);
        self.importAndProDisable = ko.observable(true);
        self.groupValid = ko.observable();
        self.placeholder = ko.observable('Please Select');
        self.deptObservableArray = ko.observableArray([]);
        self.columnArray = ko.observableArray();
        self.columnArraySec = ko.observableArray();
        self.tableProjectCostArr = ko.observableArray([]);
        self.test = ko.observableArray([]);
        self.employeeNameobj = ko.observable();
        self.exportLbl2 = ko.observable();
        self.cancelLbl = ko.observable();
        self.exportAllDataLbl = ko.observable();
        self.costSummaryLbl = ko.observable();
        self.ProjectNumberArr = ko.observableArray();
        self.taskNumArr = ko.observableArray();
        self.taskNameArr = ko.observableArray();
        self.taskNameArr1 = ko.observableArray();
        self.timeSheetTypesArr = ko.observableArray();
        self.projectArr = ko.observableArray();
        self.dataSource = ko.observable();
        self.dataSourceN = ko.observable();
        self.timeSheetTypesArr([{label: "Over time", value: "Overtime"}, {label: "Straight Time", value: "Straight Time"}]);
        self.taskNameData = new oj.ArrayDataProvider(self.taskNameArr, {keyAttributes: 'value'});
        self.taskNameData1 = new oj.ArrayDataProvider(self.taskNameArr1, {keyAttributes: 'value'});
        self.taskNumberData = new oj.ArrayDataProvider(self.taskNumArr, {keyAttributes: 'value'});
        self.ProjectNameData = new oj.ArrayDataProvider(app.projectNameArr2, {keyAttributes: 'value'});
        self.ProjectNumData = new oj.ArrayDataProvider(self.ProjectNumberArr, {keyAttributes: 'value'});
        self.expenditureOrgData = new oj.ArrayDataProvider(app.projectOrganizArr, {keyAttributes: 'value'});
        self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tableProjectCostArr));
        self.dataSourceN = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.projectArr, {idAttribute: 'sequance'}));
        self.employeeName = ko.observable();
        self.employeeNumber = ko.observable();
        self.projectNumber = ko.observable();
        self.projectName = ko.observable();
        self.taskNumber = ko.observable();
        self.taskName = ko.observable();
        self.expenditureOrganization = ko.observable();
        self.timeSheetType = ko.observable();
        self.searchLbl = ko.observable();
        self.resetLbl = ko.observable();
        self.transactionFromLbl = ko.observable();
        self.transactionToLbl = ko.observable();
        self.viewApprovalLabel = ko.observable();
        self.recordNumber = ko.observable();
        self.transactionDate = ko.observable();
        self.Expendituretype = ko.observable();
        self.Quantity = ko.observable();
        self.approvStatus = ko.observable();
        self.cost = ko.observable();
        self.labelSectionTwo = ko.observable();
        self.labelSectionOne = ko.observable();
        //values Section Pending Approval//
        self.employeeNameValue = ko.observable();
        self.employeeNumberValue = ko.observable();
        self.projectNumberValue = ko.observable();
        self.projectNameValue = ko.observable();
        self.taskNumberValue = ko.observable();
        self.taskNameValue = ko.observable();
        self.timeSheetDateFromValue = ko.observable();
        self.expenditureOrganizationValue = ko.observable();
        self.timeSheetDateToValue = ko.observable();
        self.timeSheetTypeValue = ko.observable();
        self.employeeNameAttr = ko.observableArray();
        self.projectData = ko.observableArray();
        //values Section Project Cost//
        self.employeeNameValueSec = ko.observable();
        self.employeeNumberValueSec = ko.observable();
        self.projectNumberValueSec = ko.observable();
        self.projectNameValueSec = ko.observable();
        self.taskNumberValueSec = ko.observable();
        self.taskNameValueSec = ko.observable();
        self.expenditureOrganizationValueSec = ko.observable();
        self.timeSheetTypeValueSec = ko.observable();
        self.employeeNameAttrSec = ko.observableArray();
        self.projectDataSec = ko.observableArray();
        self.approvalList = ko.observable("Approval List");
        self.name = ko.observable("name");
        self.status = ko.observable();
        self.approvalDate = ko.observable();
        self.okLbl = ko.observable();
        self.dataTB2 = ko.observableArray([]);
        self.selectedRowKey = ko.observable();
        self.dataTB2 = ko.observableArray([]);
        self.dataSourceTB2 = ko.observable();
        self.taskData = ko.observableArray();
        self.orgData = ko.observableArray();
        self.tempArr = ko.observableArray();
        self.tempArr2 = ko.observableArray();
        self.total = ko.observable();
        self.totalVal = ko.observable('00.00');
        self.tasklinks = ko.observableArray();
        self.importAndProcessLbl1 = ko.observable();
        self.currentRowData = ko.observable();
        self.batchName = ko.observable();
        self.comments = ko.observable();
        self.employeeNameobj2 = ko.observable();
        self.allPending = ko.observableArray();
        self.transactionFromValue = ko.observable();
        self.transactionToValue = ko.observable();
        self.filterExportData = ko.observableArray();

        self.connected = function () {
            app.loading(true);
            self.getprojectCost();
            self.getProjectCostFromDB();
        };

//     --------------------------------------- table selection for approval table ------------------------ 

        self.tableSelectionListener = function (event) {


            var data = event.detail;
            if (!data)
            {
                return;
            }
            var currentRow = data.currentRow;
            if (!currentRow)
            {
                return;
            }
            self.approvalDisable(false);
            self.selectedRowKey(self.projectArr()[[currentRow['rowIndex']]].id)
            self.currentRowData(self.projectArr()[[currentRow['rowIndex']]]);
            if (self.projectArr()[[currentRow['rowIndex']]].approvalStatus === "APPROVED") {
                self.importAndProDisable(false);
            } else {
                self.importAndProDisable(true);
            }
        };

//     --------------------------------------- employee name value changed for approval table ------------------------ 

        self.empNameValueChanged = function (event) {

            if (event['detail'].value == '')
            {
                self.employeeNameValueSec('');
                self.employeeNumberValueSec('');
                self.employeeNameobj('');
            }

            if (!event['detail'] || !event['detail'].value)
                return;
            self.employeeNumberValue(event['detail'].value);
        };

//     --------------------------------------- project name value changed approval table ------------------------ 

        self.projNameValueChanged = function (event) {
            $(".customSelect img").show();

            for (var i = 0; i < app.proData2().length; i++) {
                if (event['detail'].value == app.proData2()[i].projectNumber) {
                    var arr = JSON.parse(app.proData2()[i].links);
                    for (var j = 0; j < arr.length; j++)
                    {
                        var hreef = arr[j].href;
                        var name = arr[j].name;
                        if (name === "Tasks")
                        {
                            var taskUrl = hreef;
                            var payload = {
                                "href": taskUrl
                            };
                        }
                    }
                    self.getSuccs = function (data) {
                        $(".customSelect img").hide();
                        self.taskNumberValue([]);
                        self.taskNameArr1([]);
                        $.each(data, function (index) {
                            self.taskNameArr1.push({
                                label: data[index].taskName,
                                value: data[index].taskNumber
                            });
                        });
                    };
                    services.postGeneric(commonhelper.getTaskDet, payload).then(self.getSuccs, app.failCbFn);
                } else {
                }
            }
            if (event['detail'].value == '')
            {
                self.projectNumberValue('');
                self.projectNameValue('');
                self.taskNumberValue('');
                self.taskNameValue('');
                $(".customSelect img").hide();
            }
            if (!event['detail'] || !event['detail'].value)
                return;
            self.projectNumberValue(event['detail'].value);

        };

//     --------------------------------------- task name value changed for approval table ------------------------ 

        self.taskNameValueChanged = function (event) {

            if (event['detail'].value == '')
            {
                self.taskNumberValue('');
                self.taskNameValue('');
            }

            if (!event['detail'] || !event['detail'].value)
                return;
            self.taskNumberValue(event['detail'].value);

        };

//     --------------------------------------- search for approval table ------------------------ 

        self.searchBtnAction = function () {

            self.projectArr([]);

            if (self.employeeNumberValue())
            {
                var employeeValueName = app.empItems().find(o => o.personNumber == self.employeeNumberValue()).displayName;
                self.employeeNameobj2(employeeValueName);
            }

            if (self.projectNumberValue())
            {
                var projectsName = app.proData2().find(e => e.projectNumber == self.projectNumberValue()).projectName;
                self.projectNameValue(projectsName);
            }

            cond = function (e) {
                var res = (self.projectNameValue() ? e.projectName === self.projectNameValue() : true)
                        && (self.employeeNameobj2() ? e.employeeName === self.employeeNameobj2() : true)
                        && (self.taskNameValue() ? e.taskNumber === self.taskNameValue() : true)
                        && (self.timeSheetTypeValue() ? e.timeSheetType === self.timeSheetTypeValue() : true)
                        && (self.expenditureOrganizationValue() ? e.organization === self.expenditureOrganizationValue() : true)
                return res;
            };

            var arrSearch = self.tempArr2().filter(e => cond(e));

            var seq = 0;
            arrSearch.forEach(e => {
                e.sequance = ++seq;
            });

            self.projectArr(arrSearch);
        };

//     --------------------------------------- rest button for approval table ------------------------ 

        self.resetBtnAction = function () {
            self.projectArr(self.tempArr2());
            self.employeeNameValue('');
            self.employeeNumberValue('');
            self.projectNumberValue('');
            self.projectNameValue('');
            self.taskNumberValue('');
            self.taskNameValue('');
            self.expenditureOrganizationValue('');
            self.timeSheetTypeValue('');
            self.placeholder('Please Select');
            $(".customSelect img").hide();
            self.taskNameArr([]);
            self.taskNameArr1([]);
        };

//     --------------------------------------- view approval function button ------------------------ 

        self.viewApproval = function () {
            var getApprovalList = function (data) {
                self.dataTB2([]);
                // var data = data ;
                // data.pop();
                var checkStatusPending = false;
                $.each(data, function (i, item) {
                    var notificationStatus, bodyCardStatus, cardStatus;

                    if (data[i].notificationType == "FYA") {

                        if (data[i].responseCode == "REJECTED") {
                            bodyCardStatus = 'app-crd-bdy-border-fail';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-rj';

                        } else if (data[i].responseCode == null) {

                            bodyCardStatus = 'app-crd-bdy-border-pen';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-pen';

                        } else if (data[i].responseCode == "APPROVED") {
                            bodyCardStatus = 'app-crd-bdy-border-suc';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-app';
                        }
                    } else if (data[i].notificationType == "FYI") {

                        bodyCardStatus = 'app-crd-bdy-border-ntre';
                        notificationStatus = 'app-type-i';
                        cardStatus = 'app-card-prog-type-ntr';
                    }
                    if (!item.responseCode && !checkStatusPending) {
                        checkStatusPending = true;
                    }
                    self.dataTB2.push({

                        name: item.rolrType == 'EMP' ? "ahmedsheriff"
                                : item.rolrType == 'LINE_MANAGER' ? "alkhawarra.waseem"
                                : item.rolrType == 'PROJECT_MANAGER' ? "Appspro.projects"
                                : item.rolrType == 'COST_CONTROLLER' ? "COST_CONTROLLER"
                                : item.rolrType == 'FINANCE_MANAGER' ? "FINANCE_MANAGER" : item.roleId,

                        type: item.notificationType,
                        status: !item.responseCode && checkStatusPending && item.notificationType == 'FYA' ? 'PENDING' : item.responseCode,
                        bodyStatus: bodyCardStatus,
                        appCardStatus: cardStatus,
                        aistatus: notificationStatus,
                        approvalDate: item.responseDate

                    });
                });
                self.dataSourceTB2(new oj.ArrayTableDataSource(self.dataTB2));
                document.querySelector("#approvalDialog").open();
            };
            if (self.selectedRowKey()) {
                var headers = {

                };
                services.getGeneric(commonhelper.getApprovalList + 'XX_PROJECT_COST/' + self.selectedRowKey(), headers).then(getApprovalList, app.failCbFn);
            }
        };

//     --------------------------------------- employee name value changed ------------------------ 
        self.empNameValueChangedSec = function (event) {

            if (event['detail'].value == '')
            {
                self.employeeNameValueSec('');
                self.employeeNumberValueSec('');
                self.employeeNameobj('');
            }

            if (!event['detail'] || !event['detail'].value)
                return;
            self.employeeNumberValueSec(event['detail'].value);
        };

//     --------------------------------------- task name value changed ------------------------ 

        self.taskNameValueChangedSec = function (event) {

            if (event['detail'].value == '')
            {
                self.taskNumberValueSec('');
                self.taskNameValueSec('');
            }

            if (!event['detail'] || !event['detail'].value)
                return;
            self.taskNumberValueSec(event['detail'].value);
        };

//     --------------------------------------- import and process button  ------------------------ 

        self.importAndProcessBtnAction = function ()
        {
            app.loading(true);
            var payload = {
                "businessUnitName": self.currentRowData().organization,
                "batchName": "Time Sheet Extension",//self.currentRowData().batchName,
                "expenditureItemDate": self.currentRowData().transactionDate,
                "personName": self.currentRowData().employeeName,
                "personId": self.currentRowData().employeeNumber,
                "documentEntryName": self.currentRowData().timeSheetType,
                "projectName": self.currentRowData().projectName,
                "projectNumber": self.currentRowData().projectNumber,
//                "taskName": self.currentRowData().taskName,
                "taskNumber": self.currentRowData().taskNumber,
                "quantity": self.currentRowData().quantity.toString(),
                "importStatus": "Not Imported",
                "billableFlag":self.currentRowData().billable,
                "expenditureOrganizationId":self.currentRowData().projectOrgId

            };
            var getSuccss = function () {

                var payloadPC = {
                    "businessUnitName": self.currentRowData().organization,
                    "batchName": "Time Sheet Extension",//self.currentRowData().batchName,
                    "fromProjectNumber": self.currentRowData().projectNumber,
                    "toProjectNumber": self.currentRowData().projectNumber,
                    "expenditureItemDate": self.currentRowData().transactionDate
                };
                var projectCostSuccss = function ()
                {
                    var payloadUpdate = {
                        "importStatus": "Imported",
                        "id": self.currentRowData().id
                    };
                    self.importUpdateSuccss = function ()
                    {
                        self.getProjectCostFromDB();
                        app.loading(false);
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'Time sheet created successfully and added in project cost '));
                        app.test2([]);
                        app.test3([]);
                        self.projectArr([]);
                        self.getProjectCostFromDB();
                    };

                    services.postSaasTsGeneric(commonhelper.projectUpdateImport, payloadUpdate).then(self.importUpdateSuccss(), app.failTSCbFn);
                };
                var projectCostFail = function () {

                };
                services.postSaasTsGeneric(commonhelper.projectCost, payloadPC).then(projectCostSuccss, projectCostFail);
            };
            var getFailReque = function () {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'request failed'));
            };
            services.postSaasTsGeneric(commonhelper.createTs, payload).then(getSuccss, getFailReque);
        };

//     --------------------------------------- proj Name Value Changed for cost table  ------------------------ 

        self.projNameValueChangedSec = function (event) {
            if (event['detail'].value == "") {
                $(".customSelect img").hide();
            } else {
                $(".customSelect img").show();
            }

            self.hreef = ko.observable();
            self.name = ko.observable();

            for (var i = 0; i < app.proData2().length; i++) {
                if (event['detail'].value == app.proData2()[i].projectNumber) {
                    var arr = JSON.parse(app.proData2()[i].links);
                    for (var j = 0; j < arr.length; j++)
                    {
                        var hreef = arr[j].href;
                        var name = arr[j].name;
                        if (name === "Tasks")
                        {
                            var taskUrl = hreef;
                            var payload = {
                                "href": taskUrl
                            };
                        }
                    }
                    self.getSuccs = function (data) {
                        $(".customSelect img").hide();
                        self.taskNumArr([]);
                        self.taskNameArr([]);
                        $.each(data, function (index) {
                            self.taskNumArr.push({
                                label: data[index].taskNumber,
                                value: data[index].taskNumber
                            });
                            self.taskNameArr.push({
                                label: data[index].taskName,
                                value: data[index].taskNumber
                            });

                        });
                    };
                    services.postGeneric(commonhelper.getTaskDet, payload).then(self.getSuccs, app.failCbFn);
                } else {
                }
            }
            if (event['detail'].value == '')
            {
                self.projectNumberValueSec('');
                self.projectNameValueSec('');
                self.taskNumberValueSec('');
                self.taskNameValueSec('');
            }
            if (!event['detail'] || !event['detail'].value)
                return;
            self.projectNumberValueSec(event['detail'].value);

        };

//     --------------------------------------- search button for cost table ------------------------ 

        self.searchBtnActionSec = function () {
            self.tableProjectCostArr([]);
            if (self.employeeNumberValueSec())
            {
                var employeeValueName = app.empItems().find(o => o.personNumber == self.employeeNumberValueSec()).displayName;
                self.employeeNameobj(employeeValueName);
            }

            if (self.projectNumberValueSec())
            {
                var projectsName = app.proData2().find(e => e.projectNumber == self.projectNumberValueSec()).projectName;
                self.projectNameValueSec(projectsName);
            }

            cond = function (e) {
                var res = (self.projectNameValueSec() ? e.projectName === self.projectNameValueSec() : true)
                        && (self.employeeNameobj() ? e.employeeName === self.employeeNameobj() : true)
                        && (self.taskNameValueSec() ? e.taskNumber === self.taskNameValueSec() : true)
                        && (self.timeSheetTypeValueSec() ? e.timeSheetType === self.timeSheetTypeValueSec() : true)
                        && (self.expenditureOrganizationValueSec() ? e.Expenditureorganization === self.expenditureOrganizationValueSec() : true);
                return res;
            };
            var arrSearch = self.tempArr().filter(e => cond(e));

            var seq = 0;
            arrSearch.forEach(e => {
                e.sequance = ++seq;
            });

            self.totalVal('');
            totalCost(arrSearch);
            self.tableProjectCostArr(arrSearch);
            self.exportProCostDis(false);

        };
//      ---------------------------total cost function -----------------------------

        var totalCost = function (arrSearch)
        {
            var countCost = 0;
            for (var i = 0; i < arrSearch.length; i++) {

                countCost = parseInt(arrSearch[i].cost) + countCost;
            }
            self.totalVal(numberWithCommas(countCost));
            return countCost;
        };

//     --------------------------------------- reset button for cost table ------------------------ 

        self.resetBtnActionSec = function () {
            self.tableProjectCostArr(self.tempArr());
            self.employeeNameValueSec('');
            self.employeeNumberValueSec('');
            self.employeeNameobj('');
            self.projectNumberValueSec('');
            self.projectNameValueSec('');
            self.taskNumberValueSec('');
            self.taskNameValueSec('');
            self.expenditureOrganizationValueSec('');
            self.timeSheetTypeValueSec('');
            self.totalVal('00.00');
            self.tableProjectCostArr([]);
            self.taskNameArr([]);
            self.placeholder('Please Select');
            $(".customSelect img").hide();
            self.taskNameArr([]);
            self.totalVal([0]);
            self.exportProCostDis(true);
        };

//     --------------------------------------- fill arrays from app controller ------------------------ 

        self.fillGlobalArr = ko.computed(function () {
            if (app.projArr().length > 0) {
                self.projectData(app.projArr());
            }
            if (app.usersArr().length > 0) {
                self.employeeNameAttr(app.usersArr());
            }
            if (app.orgData().length > 0) {
                self.orgData(app.orgData());
            }
            if (app.taskData().length > 0) {
                self.taskData(app.taskData());
            }
        });

//     --------------------------------------- function for comma in total cost ------------------------ 

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

//     --------------------------------------- export project cost rest ------------------------ 

        self.exportTransSheet = function ()
        {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }

            var listDate = [];
            var arr = [];
            var startDate = self.transactionFromValue();
            var endDate = self.transactionToValue();
            var dateMove = new Date(startDate);
            var strDate = startDate;

            while (strDate < endDate) {
                var strDate = dateMove.toISOString().slice(0, 10);
                listDate.push(strDate);
                new Date(dateMove.setDate(dateMove.getDate() + 1));
            }
            ;
            for (var x in listDate)
            {
                var y = self.tableProjectCostArr().filter(el => el.transactionDate == listDate[x]);
                arr.push(...y);
            }
            self.filterExportData(arr);
            self.exportCostSheet(self.filterExportData());
        };

//     --------------------------------------- export all data button ------------------------ 

        self.exportAllData = function ()
        {
            self.exportCostSheet(self.tableProjectCostArr());
        };

//     --------------------------------------- function to export project cost excel sheet ------------------------ 

        self.exportCostSheet = function (arr)
        {
            if (arr.length == 0)
            {
                app.messagesDataProvider.push(app.createMessage('error', `No project cost from ${self.transactionFromValue()} to ${self.transactionToValue()}`));
                document.querySelector('#exportTransactionDate').close();
                restExportFun();
                return;
            }
            /* make the worksheet */
            var ws = XLSX.utils.json_to_sheet(arr);
//            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "Employee");
//            /* generate an XLSX file */
            XLSX.writeFile(wb, "ProCost.xlsx");
//            document.querySelector(DOMsStrings.yesNoDialogExport).close();
            app.messagesDataProvider.push(app.createMessage('confirmation', 'Data Exported '));
            restExportFun();
            document.querySelector('#exportTransactionDate').close();
        };

//     --------------------------------------- project cost rest ------------------------ 

        self.getprojectCost = function () {
            var projectCostDetailsCbFn = function (data) {
                app.loading(false);
                $.each(data, function (index) {
                    self.test().push(
                            {
                                transactionDate: data[index].transactionDate,
                                projectNumber: data[index].projectNumber,
                                projectName: data[index].projectName,
                                taskNumber: data[index].taskNumber,
                                taskName: data[index].taskName,
                                Expendituretype: data[index].expenditureType,
                                employeeName: data[index].employeeName,
                                timeSheetType: data[index].timeSheetType,
                                Quantity: data[index].quantity,
                                cost: data[index].cost,
                                batchName: data[index].batchName,
                                sequance: '',
                                Expenditureorganization: data[index].organization
                            }
                    );
                });
                var countCost = 0;
                for (var i = 0; i < self.test().length; i++) {
                    countCost = parseInt(self.test()[i].cost) + countCost;
                }
                self.tempArr(self.test());
            };

            services.getGeneric(commonhelper.getProjectCost, {}).then(projectCostDetailsCbFn, app.failTSCbFn);
        };

//     --------------------------------------- project cost rest ------------------------ 

        self.getProjectCostFromDB = function () {

            var projectCostCbFn = function (data) {
                app.getALlNotification();

                var data2 = data.filter(el => el.importStatus === "Not Imported");

                var seq = 0;
                data2.forEach(e => {
                    e.sequance = ++seq;
                });
                self.projectArr(data2);
                self.tempArr2(data2);
            };
            services.getGeneric(commonhelper.getAllProjectCost, {}).then(projectCostCbFn, app.failTSCbFn);
        };
        
//     --------------------------------------- reset function for export popup value  ------------------------ 

        restExportFun = () => {
            self.transactionFromValue('');
            self.transactionToValue('');
        };

//     --------------------------------------- button to open export dialog ------------------------ 

        self.exportProCostBtn = function ()
        {
            document.querySelector('#exportTransactionDate').open();
        };

//     --------------------------------------- button to close export dialog  ------------------------ 

        self.cancelTransDialog = function ()
        {
            restExportFun();
            document.querySelector('#exportTransactionDate').close();
        };

//     --------------------------------------- open approval dialog ------------------------ 

        self.closeDialog = function () {
            document.querySelector("#approvalDialog").close();
        };


        var getTranslation = oj.Translations.getTranslatedString;
        function initTranslations() {

            self.employeeName(getTranslation("timeSheetSummary.employeeName"));
            self.employeeNumber(getTranslation("timeSheetSummary.employeeNumber"));
            self.projectNumber(getTranslation("timeSheetSummary.projectNumber"));
            self.projectName(getTranslation("timeSheetSummary.projectName"));
            self.taskNumber(getTranslation("timeSheetSummary.taskNumber"));
            self.taskName(getTranslation("timeSheetSummary.taskName"));
            self.expenditureOrganization(getTranslation("timeSheetSummary.expenditureOrganization"));
            self.timeSheetType(getTranslation("timeSheetSummary.timeSheetType"));
            self.searchLbl(getTranslation("timeSheetSummary.searchLbl"));
            self.resetLbl(getTranslation("timeSheetSummary.resetLbl"));
            self.viewApprovalLabel(getTranslation("timeSheetSummary.viewApprovalLabel"));
            self.recordNumber(getTranslation("timeSheetSummary.recordNumber"));
            self.transactionDate(getTranslation("timeSheetSummary.transactionDate"));
            self.Expendituretype(getTranslation("timeSheetSummary.Expendituretype"));
            self.Quantity(getTranslation("timeSheetSummary.Quantity"));
            self.importAndProcessLbl1(getTranslation("timeSheetSummary.importAndProcessLbl1"));
            self.approvStatus(getTranslation("timeSheetSummary.approvStatus"));
            self.cost(getTranslation("timeSheetSummary.cost"));
            self.labelSectionTwo(getTranslation("timeSheetSummary.labelSectionTwo"));
            self.labelSectionOne(getTranslation("timeSheetSummary.labelSectionOne"));
            self.total(getTranslation("timeSheetSummary.total"));
            self.placeholder(getTranslation("timeSheetSummary.placeholder"));
            self.batchName(getTranslation("timeSheetSummary.batchName"));
            self.exportLbl2(getTranslation("timeSheetSummary.exportLbl2"));
            self.comments(getTranslation("notification.comments"));
            self.cancelLbl(getTranslation("timeSheetSummary.cancelLbl"));
            self.transactionFromLbl(getTranslation("timeSheetSummary.transactionFromLbl"));
            self.transactionToLbl(getTranslation("timeSheetSummary.transactionToLbl"));
            self.exportAllDataLbl(getTranslation("timeSheetSummary.exportAllDataLbl"));
            self.costSummaryLbl(getTranslation("timeSheetSummary.costSummaryLbl"));
            self.okLbl(getTranslation("timeSheetSummary.okLbl"));

            //            --------------------Table 1--------------------------------
            self.columnArray([
                {
                    "headerText": self.recordNumber(), "field": "sequance"
                },
                {
                    "headerText": self.approvStatus(), "field": "approvalStatus"
                },
                {
                    "headerText": self.transactionDate(), "field": "transactionDate"
                },
                {
                    "headerText": self.projectName(), "field": "projectName"
                },
                {
                    "headerText": self.projectNumber(), "field": "projectNumber"
                },
                {
                    "headerText": self.taskName(), "field": "taskName"
                },
                {
                    "headerText": self.taskNumber(), "field": "taskNumber"
                },
                {
                    "headerText": self.employeeName(), "field": "employeeName"
                },
                {
                    "headerText": self.Expendituretype(), "field": "expenditureType"
                },
                {
                    "headerText": self.expenditureOrganization(), "field": "organization"
                },
                {
                    "headerText": self.timeSheetType(), "field": "timeSheetType"
                },
                {
                    "headerText": self.Quantity(), "field": "quantity"
                },
                {
                    "headerText": self.batchName(), "field": "batchName"
                },
                {
                    "headerText": self.comments(), "field": "rejectReason"
                },
            ]);
            //       --------------------Table 2--------------------------------
            self.columnArraySec([
                {
                    "headerText": self.recordNumber(), "field": "sequance"
                },
                {
                    "headerText": self.transactionDate(), "field": "transactionDate"
                },
                {
                    "headerText": self.projectName(), "field": "projectName"
                },
                {
                    "headerText": self.projectNumber(), "field": "projectNumber"
                },
                {
                    "headerText": self.taskName(), "field": "taskName"
                },
                {
                    "headerText": self.taskNumber(), "field": "taskNumber"
                },
                {
                    "headerText": self.employeeName(), "field": "employeeName"
                },
                {
                    "headerText": self.Expendituretype(), "field": "Expendituretype"
                },
                {
                    "headerText": self.expenditureOrganization(), "field": "Expenditureorganization"
                },
                {
                    "headerText": self.timeSheetType(), "field": "timeSheetType"
                },
                {
                    "headerText": self.Quantity(), "field": "Quantity"
                },
                {
                    "headerText": self.batchName(), "field": "batchName"
                },
                {
                    "headerText": self.cost(), "field": "cost"
                }
            ]);
        }
        initTranslations();
    }

    return costScreenContentViewModel;
});
