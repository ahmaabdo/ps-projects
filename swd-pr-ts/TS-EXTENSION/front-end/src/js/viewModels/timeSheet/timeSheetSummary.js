/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * timeSheetSummary module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'xlsx', 'util/commonhelper','ojs/ojarraydataprovider',
    'ojs/ojdatetimepicker', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojdialog','ojs/ojcheckboxset','ojs/ojfilepicker'
], function (oj, ko, $, app, services, x, commonhelper,ArrayDataProvider) {
    /**
     * The view model for the main content view template
     */
    function timeSheetSummaryContentViewModel() {
        var self = this;
//        ---------------------search Variables--------------------------
        self.employeeName = ko.observable();
        self.employeeNumber = ko.observable();
        self.projectNumber = ko.observable();
        self.projectName = ko.observable();
        self.taskNumber = ko.observable();
        self.taskName = ko.observable();
        self.timeSheetDateFrom = ko.observable();
        self.expenditureOrganization = ko.observable();
        self.timeSheetDateTo = ko.observable();
        self.timeSheetType = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();

//       ----------buttons observables----------------------------
        self.isDisabled = ko.observable(false);
        self.draftDisable = ko.observable(false);
        self.searchDisable = ko.observable(false);
        self.resetDisable = ko.observable(false);
        self.exportDisable = ko.observable(true);
        self.exportDisable2 = ko.observable(true);
        self.importAndProDisable = ko.observable(true);
        self.projectCostDisable = ko.observable(false);
        self.deleteDisable = ko.observable(true);
        self.addDisable2 = ko.observable(true);
        self.addDisable = ko.observable(false);
        self.editDisable2 = ko.observable(true);
        self.uploadDisabled = ko.observable(true);
        self.editeOpDisable2 = ko.observable(true);
        self.viewDisable2 = ko.observable(true);
        self.deleteDisable2 = ko.observable(true);
        self.searchLbl = ko.observable('search');
        self.resetLbl = ko.observable('reset');
        self.exportLbl = ko.observable('export');
        self.exportLbl2 = ko.observable('export');
        self.addLbl = ko.observable('add');
        self.editLbl = ko.observable();
        self.viewLbl = ko.observable('view');
        self.deleteLbl = ko.observable('view');
        self.addLbl2 = ko.observable('add');
        self.editLbl2 = ko.observable();
        self.createTimeSheetDraftLbl = ko.observable();
        self.importAndProcessLbl1 = ko.observable('import and process');
        self.projectCostLbl = ko.observable('Project Cost');
        self.createTSLbl = ko.observable();
        self.viewLbl2 = ko.observable('view');
        self.deleteLbl2 = ko.observable('view');
        self.timesheetSummaryLbl = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();

        self.employeeNameValue = ko.observable();
        self.employeeNumberValue = ko.observable();
        self.projectNumberValue = ko.observable();
        self.projectNameValue = ko.observable();
        self.taskNumberValue = ko.observable();
        self.taskNameValue = ko.observable();
        self.timeSheetDateFromValue = ko.observable();
        self.expenditureOrganizationValue = ko.observable();
        self.timeSheetDateToValue = ko.observable();
        self.timeSheetTypeValue = ko.observable();
        self.tempArr = ko.observableArray([]);
        self.arrayEmp = ko.observableArray([]);
        self.arrayTS = ko.observableArray([]);
//        ----------------------Table 1 Coloums -------------------------
        self.deptObservableArray = ko.observableArray([]);
        self.deptObservableArray2 = ko.observableArray([]);
        self.oprationdisabled = ko.observable(true);
        self.selectedRow = ko.observable();
        self.selectedIndex = ko.observable();
        self.selectedRow2 = ko.observable();
        self.selectedIndex2 = ko.observable();
        self.organization = ko.observable();
        self.timeSheetDate = ko.observable();
        self.totalStraightTime = ko.observable();
        self.totalOverTime = ko.observable();
        self.bussinesUnitLbl = ko.observable();
        self.uom = ko.observable();
        self.batchName = ko.observable();
        self.columnArray = ko.observableArray();
        self.dataSource = ko.observable();
        self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'id'});
        self.idrowMaster = ko.observable();
        self.idrowDetail = ko.observable();
        self.employeeNameAttr = ko.observableArray();
        self.projectData = ko.observableArray();
        self.taskData = ko.observableArray();
        self.orgData = ko.observableArray();
        self.ProjectNumberArr = ko.observableArray();
        self.projectOrganizArr = ko.observableArray();
        self.taskNumArr = ko.observableArray();
        self.taskNameArr = ko.observableArray();
        self.taskNameData = new oj.ArrayDataProvider(self.taskNameArr, {keyAttributes: 'value'});
        self.taskNumberData = new oj.ArrayDataProvider(self.taskNumArr, {keyAttributes: 'value'});
        self.ProjectNameData = new oj.ArrayDataProvider(app.projectNameArr, {keyAttributes: 'value'});
        self.ProjectNumData = new oj.ArrayDataProvider(self.ProjectNumberArr, {keyAttributes: 'value'});
        self.expenditureOrgData = new oj.ArrayDataProvider(app.projectOrganizArr, {keyAttributes: 'value'});

        self.deptObservableArray2 = ko.observableArray([]);
        self.deptObservableArray3 = ko.observableArray([]);
        self.expenditureType = ko.observable();
        self.transactionDate = ko.observable();
        self.quantity = ko.observable();
        self.columnArray2 = ko.observableArray();
        self.columnArray3 = ko.observableArray();
        self.dataSource2 = ko.observable();
        self.dataprovider3 = ko.observable();
        self.dataprovider2 = new oj.ArrayDataProvider(self.deptObservableArray2, {idAttribute: 'id'});
        self.dataprovider3 (new oj.ArrayDataProvider(self.deptObservableArray3, {idAttribute: 'id'})) ;
        self.placeholder = ko.observable('Please Select');
        self.isDisabledS = ko.observable(true);
        self.timeSheetTypesArr = ko.observableArray();
        self.timeSheetTypesArr([{label: "Over time", value: "Over time"}, {label: "Straight Time", value: "Straight Time"}]);
        self.masterPayload = ko.observableArray();
        self.deleteRow = ko.observableArray();
        self.deleteDetailsRow = ko.observableArray();
        self.currentdate = ko.observable(formatDate(new Date()));
        self.allTranscation = ko.observableArray();
        self.draftDataArray=ko.observableArray();
        self.selectedItems=ko.observableArray();
        self.selectedItemsArr=ko.observableArray();
        self.billInfo=ko.observable();
        self.payload=ko.observable();
        self.acceptArr = ko.observableArray([]);
        self.selected_files = ko.observableArray([]);
        // ---------------------------------------- function for formate date ------------------------ 

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1), day = '' + date.getDate(), year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };
        self.connected = function () {      
            init();            
        };
        self.addNewBtnAction = function ()
        {            
            //----------------------------Header Validation--------------------------//
            const StaticHeaders =  ["Transaction_Date", "Project_Name", "Project_Number", "Task_Name", "Task_Number", "Expenditure_Type", "TimeSheet_Type","Total_Straight","Total_over", "uom", "billable"];
            var headers =    Object.keys(self.payload()[0])  
            for (var item in StaticHeaders) {
                if (StaticHeaders[item] != headers[item]) {
                    app.messagesDataProvider.push(app.createMessage('error', 'Invalid Sheet Type'));
                    return;
                } 
            }     
            for (var item in self.payload()) {
                            self.payload()[item].seq = parseInt(item) + 1;
                        }
            //---------------------------record exist validation -------------------//
             
               var  projectCostCbFn =  function(data){
                   if(data.length!=0){
                   var employeeCreatedRecords = data.filter(e=>e.employeeName == app.test2()[0].employeeName);
                   var EmployeevalidRecord = employeeCreatedRecords.filter(e=>e.approvalStatus !="REJECTED");
                 if(employeeCreatedRecords.length ==0){
                      addWeeklySheetRules();
                 }else{
                   for(var i =0; i<self.payload().length; i++ ){                
                     var recordData =  EmployeevalidRecord.filter(x=> (x.transactionDate == formatDate(new Date(self.payload()[i].Transaction_Date)).toString()) ) //== formatDate(new Date(self.payload()[i].Transaction_Date)).toString() )
 
                     if(recordData.length !=0){
                          var totalStoredOverTime =0;
                          var totalStoredStraight =0;
                          for(var j = 0 ;j<recordData.length ;j++){
                              if(recordData[j].timeSheetType == "Overtime"){
                                  totalStoredOverTime =  parseInt(recordData[j].quantity) + parseInt(totalStoredOverTime)
                                  if(totalStoredOverTime >= 15 ){
                                  app.messagesDataProvider.push(app.createMessage('error', `Transaction date with ${self.payload()[i].Transaction_Date} is already added`));
                                   return;
                                 }
                              } if (recordData[j].timeSheetType == "Straight Time"){
                                  totalStoredStraight = parseInt(recordData[j].quantity) + parseInt(totalStoredStraight);
                                  if(totalStoredStraight >= 9 ){
                                  app.messagesDataProvider.push(app.createMessage('error', `Transaction date with ${self.payload()[i].Transaction_Date} is already added`));
                                   return;
                                 }
                              }
                          
                           }
                        
                     }
                  } 
                   addWeeklySheetRules();
                   }
               }else{
                   addWeeklySheetRules();
               }
           }
                
           services.getGeneric(commonhelper.getAllProjectCost, {}).then(projectCostCbFn, app.failTSCbFn);
                
             //---------------------------projectName Validation-------------------//
             var addWeeklySheetRules = function (){
            var allProjectsInfo = [...new Set(app.proData2().map(e => ({projectName: e.projectName, projectNumber: e.projectNumber})))]
            var SheetProjects = [...new Set(self.payload().map(e => ({projectName: e.Project_Name, projectNumber: e.Project_Number})))]
            var validationPro = [];
            var validationProNum = [];
            for (var j in allProjectsInfo) {
                for (var x in SheetProjects) {
                    if (SheetProjects[x].projectName != allProjectsInfo[j].projectName  || SheetProjects[x].projectNumber != allProjectsInfo[j].projectNumber) {
                        continue;
                    } else {
                        validationPro.push(SheetProjects[x].projectName)
                        validationProNum.push(SheetProjects[x].projectNumber)
                    }
                }
            }
            if (validationPro.length != SheetProjects.length ||validationProNum.length != SheetProjects.length) {
                app.messagesDataProvider.push(app.createMessage('error', 'Invalid Project Details'));
                return;
            }
       
                    //---------------------------Fill Sheet Data-------------------//           
            var sheetData =[];
             $.each(self.payload(), function (index) { 
                var projectExpenOrgID = app.projectCurrentOrg().find(el => el.PROJECT_NUMBER == self.payload()[index].Project_Number).PROJECT_ORGANIZATION_ID            
                        sheetData.push({                          
                    "billableFlag": self.payload()[index].billable,
                    "businessUnitName": app.proData2().find(el => el.projectNumber == self.payload()[index].Project_Number).businessUnitName,
                    "expenditureType": self.payload()[index].Expenditure_Type,
                    "projectName": self.payload()[index].Project_Name,
                    "projectNumber": self.payload()[index].Project_Number,
                    "quantity": self.payload()[index].TimeSheet_Type =="Overtime"?self.payload()[index].Total_over:self.payload()[index].Total_Straight,
                    "taskName": self.payload()[index].Task_Name,
                    "taskNumber": self.payload()[index].Task_Number,
                    "timeSheetType": self.payload()[index].TimeSheet_Type,
                    "transactionDate":formatDate(new Date(self.payload()[index].Transaction_Date)),// new Date(self.payload()[index].Transaction_Date).toISOString().substring(0,10) ,
                    "uom": self.payload()[index].uom,
                    "empTsId": self.payload()[index].seq,
                    "expenditureOrganizationId":projectExpenOrgID
                    

                        });                             
                     });
                   //---------------------------date Validation-----------------------//
            var SheetDates = [...new Set(sheetData.map(e => e.transactionDate))]
            if (SheetDates.length > 5) {
                app.messagesDataProvider.push(app.createMessage('warning', 'Maximum 5 Days to Upload Weekly Sheet'));
                return;
            }if(SheetDates.length < 5){                
                    app.messagesDataProvider.push(app.createMessage('warning', 'Sheet Must Contains 5 Days'));
                return;
            }       
            //-----------------Overtime and straightTime Validation------------//
            var totalStrightTime = 0;
            var totalOverTime = 0
            for (var i in sheetData) {
                if (sheetData[i].timeSheetType == "Straight Time") {
                    totalStrightTime = parseInt(sheetData[i].quantity) + totalStrightTime
                    if (totalStrightTime > 45) {
                        app.messagesDataProvider.push(app.createMessage('warning', 'Stright Time Maximum 45 Hours Per Week'));
                        return;
                    }
                }
                if (sheetData[i].timeSheetType == "Overtime") {
                    totalOverTime = parseInt(sheetData[i].quantity) + totalOverTime
                    if (totalOverTime > 75) {
                        app.messagesDataProvider.push(app.createMessage('warning', 'OverTime Maximum 75 Hours Per Week'));
                        return;
                    }
                }
            }
            app.test3().push(...sheetData)
            init();
            self.importAndProDisable(false);
            self.uploadDisabled(true);
            self.addDisable2(true);
               app.messagesDataProvider.push(app.createMessage('confirmation', 'Weekly Timesheet Uploaded Successfully'));
              self.removePickerBtnAction()     
        }
        }
        
        self.selectListener = function (event)
                {                    
                      var files = event.detail.files;
                        self.selected_files(files);
                       var reader = new FileReader();
                       reader.onload = function (e) {
                           var data = e.target.result;
                        var workbook = XLSX.read(data, {type: 'binary'});
                        var sheet = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], {defval: ""});
                        app.loading(false);
                        
                          self.payload(sheet);
                            self.payload().forEach(o => {
                            Object.keys(o).forEach(key => {
                                var val = o[key];
                                delete o[key];
                                key = (key.replace(" ", "_").replace("(", "").replace(")", ""));
                                val = (val.replace("%", "").replace("(", "").replace(")", "").replace("$", "").trim());
                                o[key] = val;
                            });
                        });
                     }
                       reader.onerror = function (ex) {
                    };

                    reader.readAsBinaryString(files[0]);
                    
                }
              /* --------------------------- function for Formate Date   --------------------------- */
                   function formatDate(date) {
                var d = new Date(date),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;

                return [year, month, day].join('-');
            }
         /* --------------------------- function for handle checkbox checked   --------------------------- */

                self.handleCheckbox = function (id) {
                    var isChecked = self.getIndexInSelectedItems(id) !== -1;
                    return isChecked ? ["checked"] : [];
                }; 
                /* --------------------------- check Box listener   --------------------------- */
                   self.checkboxListener = function (event)
                {
                    if (event.detail !== null)
                    {
                        var value = event.detail.value;
                        var newSelectedItems;
                        var id = event.target.dataset.rowId;
                        if (value.length > 0) {
                            newSelectedItems = self.getIndexInSelectedItems(id) === -1 ?
                                    self.selectedItems().concat([{startIndex: {row: id}, endIndex: {row: id}}]) :
                                    self.selectedItems();
                        } else {
                            newSelectedItems = self.selectedItems();
                            var idx = self.getIndexInSelectedItems(id);
                            if (idx !== -1)
                            {
                                var item = newSelectedItems.splice(idx, 1)[0];
                                // break up ranges into two separate ranges if index is in range;
                                if (item.startIndex.row !== item.endIndex.row)
                                {
                                    var newItems = [];
                                    if (id !== 0)
                                    {
                                        newItems.push({startIndex: {row: item.startIndex.row}, endIndex: {row: Number(id) - 1}});
                                    }
                                    if (id !== self.dataProvider.totalSize() - 1)
                                    {
                                        newItems.push({startIndex: {row: Number(id) + 1}, endIndex: {row: item.endIndex.row}});
                                    }
                                    newSelectedItems = newSelectedItems.concat(newItems);
                                }
                            }
                        }
                        self.selectedItems(newSelectedItems);                     
                    }            
                    self.printCurrentSelection();   
                };
                 self.printCurrentSelection = function () {
                    var selectedId;
                    self.selectedItemsArr([]);
                    for (var i = 0; i < self.selectedItems().length; i++) {
                        selectedId = self.selectedItems()[i].startIndex.row;
                        for (var x = 0; x < self.deptObservableArray3().length; x++) {
                            if (self.deptObservableArray3()[x].id == selectedId)                               
                                self.selectedItemsArr.push(self.deptObservableArray3()[x]);
                        }
                    }
                };
                self.getIndexInSelectedItems = function (id) {

                    for (var i = 0; i < self.selectedItems().length; i++)
                    {
                        var range = self.selectedItems()[i];
                        var startIndex = range.startIndex.row;
                        var endIndex = range.endIndex.row;
                        if (id >= startIndex && id <= endIndex)
                        {
                            return i;
                        }
                    }
                    return -1;
                };
             self.removePickerBtnAction = function () {
            self.selected_files([]);
            document.querySelector("input[type='file']").value = '';
        };
        //  ---------------------- function Draft -------------------------      
        self.getDraftRequests = async function () {
            var sucessCbFn = function (data) { 
                self.draftDataArray([])
                var seq = 0 ;
                 $.each(data, function (index) {              
                        self.draftDataArray.push({
                            "empTsId": "",
                            "transactionDate": data[index].transactionDate,
                            "projectNumber": data[index].projectNumber,
                            "projectName": data[index].projectName,
                            "taskNumber": data[index].taskNumber,
                            "taskName": data[index].taskName,
                            "expenditureType": data[index].expenditureType,
                            "timeSheetType": data[index].timeSheetType,
                            "quantity": parseInt(data[index].quantity),
                            "businessUnitName": data[index].businessUnitName,
                            "uom": data[index].uom,
                            "employeeName": data[index].employeeName,
                            "employeeNumber": data[index].employeeNumber,
                            "timeSheetDate": data[index].timeSheetDate,
                            "batchName": data[index].batchName,
                            "id":seq++,
                            "requistId":data[index].id,
                            "billableFlag":data[index].billableFlag,
                            "expenditureOrganizationId":data[index].expenditureOrganizationId

                        });                             
                     });   
                self.deptObservableArray3(self.draftDataArray()) ; 
                self.dataprovider3 (new oj.ArrayDataProvider(self.deptObservableArray3, {idAttribute: 'id'})) 
            };
            var payload = {
                "id":app.personDetails().personId
            };
          await services.postGeneric(commonhelper.getDraftRequest,payload).then(sucessCbFn, app.failCbFn);
        };

//  ---------------------- init function  -------------------------

        var init = function ()
        {
            var id = 0;
            app.test3().forEach(e => {
                e.empTsId = id++
            });
            app.test2().forEach(e => {
                e.empTsId = ++id
            });
            self.deptObservableArray(app.test2());
            if (app.test2().length === 0) {
                self.addDisable(false);
            }
            if (app.test2().length > 0)
            {
                self.addDisable(true);
                self.exportDisable(false);
            }

            if (app.test3().length > 0)
            {
                self.exportDisable2(false);
                var sumStraight = 0;
                var sumOver = 0;

                for (var i = 0; i < app.test3().length; i++)
                {
                    if (app.test3()[i].timeSheetType == "Straight Time" ||app.test3()[i].timeSheetType == "StraightTime")
                    {
                        sumStraight += parseInt(app.test3()[i].quantity);
                    } else
                    {
                        sumOver += parseInt(app.test3()[i].quantity);
                    }
                    app.test2()[0].totalOverTime = sumOver;
                    app.test2()[0].totalStraightTime = sumStraight;
                }
                self.getDraftRequests();
               
                  self.getMaxId();
           
                self.deptObservableArray2(app.test3())             
            }
        };

//  ---------------------- Dom String -------------------------

        var DOMsStrings = {

            yesNoDailog: "#yesNoDialog",
            yesNoDialogExport: "#yesNoDialogExport",
            yesNoDialogDetail: "#yesNoDialogDetail ",
            yesNoDialogDetailExport: "#yesNoDialogDetailExport",
            DraftDialog: "#DraftDialog"

        };

//  ---------------------- Table listener for empolye details record -------------------------

        self.tableSelectionListener = function (event)
        {
            if (app.test3().length > 0)
            {
                self.importAndProDisable(false);
            }
            if (app.test2().length > 0)
            {
                self.deleteDisable(false);
                self.addDisable2(false);
                self.uploadDisabled(false);
            }
            var data = event.detail;
            var currentRow = data.currentRow;
            self.oprationdisabled(false);
            if (currentRow) {
                self.selectedIndex(currentRow['rowIndex']);
                self.selectedRow(self.deptObservableArray()[[currentRow['rowIndex']]]);
                self.idrowMaster([self.deptObservableArray()[[currentRow['rowIndex']]].id]);
                self.deleteDetailsRow([self.deptObservableArray()[[currentRow['rowIndex']]]])
                self.arrayEmp().push(self.deptObservableArray());
            }
        };
//       ----------------------- get employee name ------------------------ 

        self.fillGlobalArr = ko.computed(function () {
            if (app.projArr().length > 0) {
                self.projectData(app.projArr());
            }
            if (app.usersArr().length > 0) {
                self.employeeNameAttr(app.usersArr());
            }
            if (app.orgData().length > 0) {
                self.orgData(app.orgData());
            }
            if (app.taskData().length > 0) {
                self.taskData(app.taskData());
            }
        });

//    ----------------------- reflect employee number based on employee name ------------------------ 

        self.empNameValueChanged = function (event) {
            if (!event['detail'] || !event['detail'].value)
                return;
            self.employeeNumberValue(event['detail'].value);

        };

//    ----------------------- reflect project number based on project name ------------------------ 

        self.projNameValueChanged = function (event) {

            for (var i = 0; i < app.proData().length; i++) {
                if (event['detail'].value == app.proData()[i].projectNumber) {
                    var arr = JSON.parse(app.proData()[i].links);
                    for (var j = 0; j < arr.length; j++)
                    {
                        var hreef = arr[j].href;
                        var name = arr[j].name;
                        if (name === "Tasks")
                        {
                            var taskUrl = hreef;
                            var payload = {
                                "href": taskUrl
                            };
                        }
                    }
                    self.getSuccs = function (data) {
                        $.each(data, function (index) {
                            self.taskNumArr.push({
                                label: data[index].taskNumber,
                                value: data[index].taskNumber
                            });
                            self.taskNameArr.push({
                                label: data[index].taskName,
                                value: data[index].taskNumber
                            });

                        });
                    };
                    services.postGeneric(commonhelper.getTaskDet, payload).then(self.getSuccs, app.failCbFn);
                } else {
                }
            }
            if (!event['detail'] || !event['detail'].value)
                return;
            self.projectNumberValue(event['detail'].value);

            var tasks = app.items().filter(o => o.ProjectNumber == self.projectNumberValue());

            tasks = tasks.map(t => {
                return {label: t.TaskName, value: t.TaskNumber}
            });
            self.taskData(tasks);
        };

//    ----------------------- reflect task number based on task name ------------------------ 

        self.taskNameValueChanged = function (event) {

            if (!event['detail'] || !event['detail'].value)
                return;
            self.taskNumberValue(event['detail'].value);

        };

//    ----------------------- table selection listener for employee details record ------------------------ 

        self.tableSelectionListener2 = function (event)
        {
            self.editDisable2(false);
            self.viewDisable2(false);
            if (app.test3().length > 0)
            {
                self.deleteDisable2(false);
                self.exportDisable2(false);
            }
            var data = event.detail;
            var currentRow = data.currentRow;
            self.oprationdisabled(false);
            if (currentRow) {
                self.selectedIndex2(currentRow['rowIndex']);
                self.idrowDetail([self.deptObservableArray2()[[currentRow['rowIndex']]].id]);
                self.deleteRow([self.deptObservableArray2()[currentRow['rowIndex']]])
                
                self.arrayTS().push(self.deptObservableArray2()[[currentRow['rowIndex']]]);
                self.editeOpDisable2(false);
            }else{
                 self.editeOpDisable2(true);
            }
        };
        
        self.tableSelectionListener3=function(event){
            
        }

//     --------------------------------------- add employee record buttons ------------------------ 

        self.addBtnAction = function ()
        {
            app.loading(false);
            var data = {};
            data.row = {};//get detail object to be viewed or updated
            data.type = "ADD";

            oj.Router.rootInstance.store(data);
            oj.Router.rootInstance.go('employeeOperation');

        };

//     ------------------------------------- project cost button -----------------------------------------------

        self.projectCostBtnAction = function ()
        {
            oj.Router.rootInstance.go('costScreen');
        };
        //     ------------------------------------- Second Edit button -------------------------------
        self.editBtnAction2 = function ()
        {
            oj.Router.rootInstance.go('timeSheetOperation');
            var obj = self.deleteRow()       
            obj.type="EDIT"
            oj.Router.rootInstance.store(obj);
            
        };


        //     --------------------------------------- delete employee record in first table buttons ------------------------ 
        self.deleteBtnAction = function ()
        {

            app.test2().splice(0, self.deleteDetailsRow()[0].empTsId);
            app.test2([]);
            self.deptObservableArray([]);
            app.test3([]);
            self.deptObservableArray2([]);
            self.deleteDetailsRow([]);
            self.exportDisable(true);
            self.exportDisable2(true);
            self.addDisable(false);
            self.addDisable2(true);
            self.deleteDisable(true);
            self.importAndProDisable(true);

        };

//     --------------------------------------- commit record buttons ------------------------ 

        self.commitRecord = function (data, event) {

        };

//     --------------------------------------- cancel buttons ------------------------ 

        self.cancelButton = function () {
            document.querySelector(DOMsStrings.yesNoDailog).close();
        };

//     --------------------------------------- export buttons ------------------------ 
        self.exportBtnAction = function () {

            document.querySelector(DOMsStrings.yesNoDialogExport).open();
        };

//     --------------------------------------- commit export buttons ------------------------ 

        self.commitExportRecord = function () {
            /* make the worksheet */
            var SheetData=[];
               $.each(self.deptObservableArray(), function (i) {
                            SheetData.push({
                               EmployeeName:self.deptObservableArray()[i].employeeName,
                               EmployeeNumber:self.deptObservableArray()[i].employeeNumberShow,
                               BusinessUnite:self.deptObservableArray()[i].organization,
                               TimeSheetDate:self.deptObservableArray()[i].timeSheetDate,
                               TotalStraightTime:self.deptObservableArray()[i].totalStraightTime?self.deptObservableArray()[i].totalStraightTime:null,
                               TotalOverTTime:self.deptObservableArray()[i].totalOverTime?self.deptObservableArray()[i].totalOverTime:null,
                               BatchName:self.deptObservableArray()[i].batchName,
                               UOM:self.deptObservableArray()[i].uom,
                        
                            });
                        });           
            var ws = XLSX.utils.json_to_sheet(SheetData);
//            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "Employee");
//            /* generate an XLSX file */
            XLSX.writeFile(wb, "EmpReq.xlsx");
            document.querySelector(DOMsStrings.yesNoDialogExport).close();
            app.messagesDataProvider.push(app.createMessage('confirmation', 'Data Exported '));
        };

//     ---------------------------------------- cancel export sheet buttons ------------------------ 


        self.cancelExportButton = function () {
            document.querySelector(DOMsStrings.yesNoDialogExport).close();
        };

//     ---------------------------------------- add emplyee time sheet details buttons ------------------------ 
        self.addBtnAction2 = function ()
        {
            app.loading(false);
            var data = {};

            data.row = {};//get detail object to be viewed or updated
            data.type = "ADD";

            oj.Router.rootInstance.store(data);
            oj.Router.rootInstance.go('timeSheetOperation');

        };

//  ---------------------------------------- delete emplyee time sheet details buttons ------------------------ 

        self.deleteBtnAction2 = function ()
        {
            app.test3(app.test3().filter(e => e.empTsId != self.selectedIndex2()));
            var id = 0;
            app.test3().forEach(e => {
                e.empTsId = id++
            });

            self.deptObservableArray2(app.test3());
            if (app.test3().length === 0)
            {
                self.exportDisable2(true);
                self.deleteDisable2(true);
            }
        };

//     ---------------------------------------- commit details emplyee time sheet details buttons ------------------------ 

        self.commitDetailRecord = function () {
            self.deptObservableArray2.remove(function (item) {
                return item.id == self.idrowDetail()[0];
            });
            var getDataSucc = function (data) {
                document.querySelector(DOMsStrings.yesNoDialogDetail).close();
            };
            services.getGeneric("timeSheet/delete/" + self.idrowDetail(), {}).then(getDataSucc, app.failCbFn);

            app.messagesDataProvider.push(app.createMessage('confirmation', 'item deleted '));
        };

//     ---------------------------------------- cancel details emplyee time sheet details buttons ------------------------ 

        self.cancelDetailButton = function () {
            document.querySelector(DOMsStrings.yesNoDialogDetail).close();
        };

//     ---------------------------------------- open dailog for export employee time sheet button ------------------------ 

        self.exportBtnAction2 = function () {
            document.querySelector(DOMsStrings.yesNoDialogDetailExport).open();
        };

//     ---------------------------------------- export to excel emplyee time sheet details buttons ------------------------ 

        self.commitExportDetailRecord = function () {

            /* make the worksheet */
            var ws = XLSX.utils.json_to_sheet(self.deptObservableArray2());
//            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "TimeSheet");
//            /* generate an XLSX file */
            XLSX.writeFile(wb, "TSReq.xlsx");
            document.querySelector(DOMsStrings.yesNoDialogDetailExport).close();
            app.messagesDataProvider.push(app.createMessage('confirmation', 'Data is exported'));
        };

//     ---------------------------------------- cancel employee time sheet details buttons ------------------------ 

        self.cancelExportDetailButton = function () {
            document.querySelector(DOMsStrings.yesNoDialogDetailExport).close();
        };
        
        self.editBtnAction = function () {
            oj.Router.rootInstance.go('employeeOperation')
            var obj={};
            obj = self.selectedRow()
            obj.type ="EmpInfo"
            oj.Router.rootInstance.store(obj)
               
        };        
        self.draftBtnAction2 = function () {           
               self.getDraftRequests(); 
         document.querySelector(DOMsStrings.DraftDialog).open();                                    
        };

//     ---------------------------------------- create time sheet button------------------------ 
        self.createTimeSheetAction = function () {
            for (var i = 0; i < app.test3().length; i++)
            {
                var payload = {
                    "projectName": app.test3()[i].projectName,
                    "projectNumber": app.test3()[i].projectNumber,
                    "transactionDate": app.test3()[i].transactionDate,
                    "taskNumber": app.test3()[i].taskNumber,
                    "taskName": app.test3()[i].taskName,
                    "employeeNumber":  app.test2()[0].employeeNumber,
                    "employeeName": app.test2()[0].employeeName,
                    "timeSheetType": app.test3()[i].timeSheetType,
                    "personName": app.personDetails().displayName,
                    "personNumber": app.personDetails().personNumber,
                    "createBy": app.personDetails().displayName,
                    "createDate": self.currentdate(),
                    "approvalStatus": "PENDING APPROVAL",
                    "expenditureType": app.test3()[i].expenditureType,
                    "quantity": app.test3()[i].quantity,
                    "cost": "",
                    "personId": app.personDetails().personId,
                    "mangerId": "300000001461423",
                    "mangerOfManger": "300000001621679",
                    "costControllerId": "300000001456299",
                    "financeMangerId": "300000002115256",
                    "organization": app.test3()[0].businessUnitName,
                    "uom": app.test2()[0].uom,
                    "timeSheetDate": app.test2()[0].timeSheetDate,
                    "batchName": app.test2()[0].batchName,
                    "importStatus":"Not Imported",
                    "billable":app.test3()[i].billableFlag,
                    "projectOrgId":app.test3()[i].expenditureOrganizationId

                };
                self.allTranscation().push(payload);
            }
               app.loading(true);
            var successEMPCbFn = function () {
                self.deleteDraftRequest();
                app.test3([]);
                app.test2([]);
                self.addDisable(false);
                self.addDisable2(true);
                self.importAndProDisable(true);
                self.deleteDisable(true);
                self.deleteDisable2(true);
                self.exportDisable(true);
                self.exportDisable2(true);
                self.deptObservableArray([]);
                self.deptObservableArray2([]);
                app.loading(false);
                app.getALlNotification();
                                
                app.messagesDataProvider.push(app.createMessage('confirmation', 'Requested Need to Approved'));
                
            };
            var failEMPCbFn = function () {
            };
            services.postGeneric(commonhelper.insertTimeSheetRequest, self.allTranscation()).then(successEMPCbFn, failEMPCbFn);
        };
        //     --------------------- create time sheet Draft button------------------------
        self.createTimeSheetDraft= function(){
            for (var i = 0; i < self.selectedItemsArr().length; i++)
            {
                var payload = {
                    "projectName": self.selectedItemsArr()[i].projectName,
                    "projectNumber": self.selectedItemsArr()[i].projectNumber,
                    "transactionDate": self.selectedItemsArr()[i].transactionDate,
                    "taskNumber": self.selectedItemsArr()[i].taskNumber,
                    "taskName": self.selectedItemsArr()[i].taskName,
                    "employeeNumber": self.selectedItemsArr()[0].employeeNumber,
                    "employeeName": self.selectedItemsArr()[0].employeeName,
                    "timeSheetType": self.selectedItemsArr()[i].timeSheetType,
                    "personName": app.personDetails().displayName,
                    "personNumber": app.personDetails().personNumber,
                    "createBy": app.personDetails().displayName,
                    "createDate": self.currentdate(),
                    "approvalStatus": "PENDING APPROVAL",
                    "expenditureType": self.selectedItemsArr()[i].expenditureType,
                    "quantity": self.selectedItemsArr()[i].quantity,
                    "cost": "",
                    "personId": app.personDetails().personId,
                    "mangerId": "300000001461423",
                    "mangerOfManger": "300000001621679",
                    "costControllerId": "300000001456299",
                    "financeMangerId": "300000002115256",
                    "organization": self.selectedItemsArr()[i].businessUnitName,
                    "uom": self.selectedItemsArr()[i].uom,
                    "timeSheetDate": self.selectedItemsArr()[i].timeSheetDate,
                    "batchName": self.selectedItemsArr()[i].batchName,
                    "importStatus": "Not Imported",
                     "billable":self.selectedItemsArr()[i].billableFlag,
                    "projectOrgId":self.selectedItemsArr()[i].expenditureOrganizationId

                };
                self.allTranscation().push(payload);
            }
            document.querySelector(DOMsStrings.DraftDialog).close();
            app.loading(true);
            var successEMPCbFn = function (data) {
                app.getALlNotification();
               // self.deptObservableArray3([])
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('confirmation', 'Requested Need to Approved'));
                self.deleteDrafRequestSelection();
            };
            var failEMPCbFn = function () {

            };
            services.postGeneric(commonhelper.insertTimeSheetRequest, self.allTranscation()).then(successEMPCbFn, failEMPCbFn);

        }
//     ---------------------------------------- delete Draft Selection after Create -----------------------
        self.deleteDrafRequestSelection = function () {
         
            for (var i =0;i<self.selectedItemsArr().length;i++){
                  var deleteSucess = function () {
            };
                 services.getGeneric(commonhelper.deleteDraftReq + "/" +self.selectedItemsArr()[i].requistId).then(deleteSucess, app.failCbFn)
            }          
        }
        //     ---------------------------------------- delete button  Selection ------------------------
        self.deleteDraftBtnAction = function () {
        for (var i =0;i<self.selectedItemsArr().length;i++){
                  var deleteSucess = function () {
                      self.getDraftRequests()
            };
                 services.getGeneric(commonhelper.deleteDraftReq + "/" +self.selectedItemsArr()[i].requistId).then(deleteSucess, app.failCbFn)
            }         
        }
           //     ---------------------------------------- delete button  Selection ------------------------
        self.deleteDraftRequest =  function () { 
            for (var i =0;i<app.maxId().length;i++){
                var deleteSucess = function () {
                    
                };
            services.getGeneric(commonhelper.deleteDraftReq + "/" +app.maxId()[i] ).then(deleteSucess, app.failCbFn);                 
        }
        }
           //     ---------------------------------------- Get Max ID of Drfat Request  ------------------------
           self.getMaxId =  function () {
            var sucessData = function (data) {
                app.maxId.push(data[0].maxRequestID)
            };
            services.getGeneric(commonhelper.getMaxId).then(sucessData, app.failCbFn)
        };      
        //     ----------------------- Translation------------------------
        var getTranslation = oj.Translations.getTranslatedString;

        function initTranslations() {

            self.employeeName(getTranslation("timeSheetSummary.employeeName"));
            self.employeeNumber(getTranslation("timeSheetSummary.employeeNumber"));
            self.projectNumber(getTranslation("timeSheetSummary.projectNumber"));
            self.projectName(getTranslation("timeSheetSummary.projectName"));
            self.taskNumber(getTranslation("timeSheetSummary.taskNumber"));
            self.taskName(getTranslation("timeSheetSummary.taskName"));
            self.timeSheetDateFrom(getTranslation("timeSheetSummary.timeSheetDateFrom"));
            self.expenditureOrganization(getTranslation("timeSheetSummary.expenditureOrganization"));
            self.timeSheetDateTo(getTranslation("timeSheetSummary.timeSheetDateTo"));
            self.timeSheetType(getTranslation("timeSheetSummary.timeSheetType"));
            self.searchLbl(getTranslation("timeSheetSummary.searchLbl"));
            self.exportLbl(getTranslation("timeSheetSummary.exportLbl"));
            self.exportLbl2(getTranslation("timeSheetSummary.exportLbl2"));
            self.resetLbl(getTranslation("timeSheetSummary.resetLbl"));
            self.addLbl(getTranslation("timeSheetSummary.addLbl"));
            self.editLbl(getTranslation("timeSheetSummary.editLbl"));
            self.importAndProcessLbl1(getTranslation("timeSheetSummary.importAndProcessLbl1"));
            self.projectCostLbl(getTranslation("timeSheetSummary.projectCostLbl"));
            self.createTSLbl(getTranslation("timeSheetSummary.createTSLbl"));
            self.viewLbl(getTranslation("timeSheetSummary.viewLbl"));
            self.deleteLbl(getTranslation("timeSheetSummary.deleteLbl"));
            self.addLbl2(getTranslation("timeSheetSummary.addLbl"));
            self.editLbl2(getTranslation("timeSheetSummary.editLbl"));
            self.viewLbl2(getTranslation("timeSheetSummary.viewLbl"));
            self.deleteLbl2(getTranslation("timeSheetSummary.deleteLbl"));
            self.organization(getTranslation("timeSheetSummary.organization"));
            self.timeSheetDate(getTranslation("timeSheetSummary.timeSheetDate"));
            self.totalStraightTime(getTranslation("timeSheetSummary.totalStraightTime"));
            self.totalOverTime(getTranslation("timeSheetSummary.totalOverTime"));
            self.uom(getTranslation("timeSheetSummary.uom"));
            self.batchName(getTranslation("timeSheetSummary.batchName"));
            self.expenditureType(getTranslation("timeSheetSummary.expenditureType"));
            self.quantity(getTranslation("timeSheetSummary.quantity"));
            self.transactionDate(getTranslation("timeSheetSummary.transactionDate"));
            self.yes(getTranslation("timeSheetSummary.yes"));
            self.no(getTranslation("timeSheetSummary.no"));
            self.confirmMessage(getTranslation("timeSheetSummary.confirmMessage"));
            self.oprationMessage(getTranslation("timeSheetSummary.oprationMessage"));
            self.placeholder(getTranslation("timeSheetSummary.placeholder"));
            self.bussinesUnitLbl(getTranslation("timeSheetSummary.bussinesUnitLbl"));
            self.timesheetSummaryLbl(getTranslation("timeSheetSummary.timesheetSummaryLbl"));
            self.createTimeSheetDraftLbl(getTranslation("timeSheetSummary.createTimeSheetDraftLbl"));
            self.billInfo(getTranslation("timeSheetSummary.billInfo"));

//            --------------------Table 1--------------------------------
            self.columnArray([
                {
                    "headerText": self.employeeName(), "field": "employeeName"
                },
                {
                    "headerText": self.employeeNumber(), "field": "employeeNumberShow"
                },
                {
                    "headerText": self.bussinesUnitLbl(), "field": "organization"
                },
                {
                    "headerText": self.timeSheetDate(), "field": "timeSheetDate"
                },
                {
                    "headerText": self.totalStraightTime(), "field": "totalStraightTime"
                },
                {
                    "headerText": self.totalOverTime(), "field": "totalOverTime"
                },
                {
                    "headerText": self.batchName(), "field": "batchName"
                },
                {
                    "headerText": self.uom(), "field": "uom"
                }
            ]);
//            --------------- Table 2 -------------
            self.columnArray2([
                {
                    "headerText": self.transactionDate(), "field": "transactionDate"
                },
                {
                    "headerText": self.projectName(), "field": "projectName"
                },
                {
                    "headerText": self.projectNumber(), "field": "projectNumber"
                },
                {
                    "headerText": self.taskName(), "field": "taskName"
                },
                {
                    "headerText": self.taskNumber(), "field": "taskNumber"
                },
                {
                    "headerText": self.expenditureType(), "field": "expenditureType"
                },
                {
                    "headerText": self.timeSheetType(), "field": "timeSheetType"
                },
                {
                    "headerText": self.quantity(), "field": "quantity"
                },
                {
                    "headerText": self.uom(), "field": "uom"
                },
                  {
                    "headerText": self.billInfo(), "field": "billableFlag"
                },
                
            ]);
            //            --------------- Draft Table  -------------
            self.columnArray3([
                {
                    "headerText": "Selection", "template": "checkTemplate"
                },
                {
                    "headerText": self.transactionDate(), "field": "transactionDate"
                },
                {
                    "headerText": self.projectName(), "field": "projectName"
                },
                {
                    "headerText": self.projectNumber(), "field": "projectNumber"
                },
                {
                    "headerText": self.taskName(), "field": "taskName"
                },
                {
                    "headerText": self.taskNumber(), "field": "taskNumber"
                },
                {
                    "headerText": self.expenditureType(), "field": "expenditureType"
                },
                {
                    "headerText": self.timeSheetType(), "field": "timeSheetType"
                },
                {
                    "headerText": self.quantity(), "field": "quantity"
                },
                {
                    "headerText": self.uom(), "field": "uom"
                },{
                    "headerText": self.employeeName(), "field": "employeeName"
                },
                {
                    "headerText": self.employeeNumber(), "field": "employeeNumber"
                },             
                {
                    "headerText": self.timeSheetDate(), "field": "timeSheetDate"
                }
                ,
//                {
//                    "headerText": self.totalStraightTime(), "field": "totalStraightTime"
//                },
//                {
//                    "headerText": self.totalOverTime(), "field": "totalOverTime"
//                },
                {
                    "headerText": self.batchName(), "field": "batchName"
                },
                 {
                    "headerText": self.billInfo(), "field": "billableFlag"
                },
                
            ]);

        }
        initTranslations();
    }

    return timeSheetSummaryContentViewModel;
});
