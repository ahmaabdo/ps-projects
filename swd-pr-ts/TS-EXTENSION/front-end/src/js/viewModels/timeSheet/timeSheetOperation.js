/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * timeSheetOperation module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmessages', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function timeSheetOperationContentViewModel() {
        var self = this;
        self.transactionDate = ko.observable();
        self.projectNumber = ko.observable();
        self.projectName = ko.observable();
        self.taskNumber = ko.observable();
        self.taskName = ko.observable();
        self.expenditureType = ko.observable();
        self.timeSheetType = ko.observable();
        self.uom = ko.observable();
        self.quantity = ko.observable();
        self.idValue = ko.observable();
        self.submitLbl = ko.observable();
        self.backLbl = ko.observable();

        self.groupValid = ko.observable();
        self.isRequired = ko.observable(true);

        self.placeholder = ko.observable('Please Select');
        self.sumbitVisable = ko.observable(true);
        self.submitDisable = ko.observable();
        self.backDisable = ko.observable();
        self.isDisabled = ko.observable(false);
        self.isDisabledProVal = ko.observable(true);
        self.isDisabledTaskVal = ko.observable(true);
        self.quantityDisabled = ko.observable(true);
        self.projectNumberValue = ko.observable();
        self.projectNameValue = ko.observable();
        self.taskNumberValue = ko.observable();
        self.taskNameValue = ko.observable();
        self.timeSheetTypeValue = ko.observable();
        self.transactionDateValue = ko.observable();
        self.expenditureTypeValue = ko.observable("Manpower");
        self.uomValue = ko.observable("HOURS");
        self.quantityValue = ko.observable();
        self.empTsId = ko.observable();
        self.pagetype = ko.observable();
        self.mastId = ko.observable();
        self.data = ko.observable();
        self.max = ko.observable();
        self.orgValue = ko.observable();
        self.organizationLbl = ko.observable();
        self.projectData = ko.observableArray();
        self.taskData = ko.observableArray();
        self.timeSheetTypesArr = ko.observableArray();
        self.timeSheetTypesArr([{label: "Overtime", value: "Overtime"}, {label: "Straight Time", value: "Straight Time"}]);
        self.ProjectNumberArr = ko.observableArray();
        self.taskNumArr = ko.observableArray();
        self.taskNameArr = ko.observableArray();
        self.orgData = new oj.ArrayDataProvider(app.projectOrganizArr, {keyAttributes: 'value'});
        self.taskNameData = new oj.ArrayDataProvider(self.taskNameArr, {keyAttributes: 'value'});
        self.taskNumberData = new oj.ArrayDataProvider(self.taskNumArr, {keyAttributes: 'value'});
        self.ProjectNameData = new oj.ArrayDataProvider(self.projectData, {keyAttributes: 'value'});
        self.ProjectNumData = new oj.ArrayDataProvider(self.ProjectNumberArr, {keyAttributes: 'value'});
        self.allTasks = ko.observableArray();
        self.projOrg = ko.observable();
        self.approvalProArr = ko.observableArray();
        self.projectNameArr = ko.observableArray();
        self.quantitsArr = ko.observableArray([]);
        self.filterArr = ko.observableArray();
        self.currentDate=ko.observable()
        self.currentDate(new Date().toISOString())
        self.timesheetOperationLbl = ko.observable();
        self.billInfo = ko.observable();
        self.billableVal = ko.observable();
        self.projectStatues = ko.observable();
        self.projectStatusVal = ko.observable();
        self.isDisabledProjectStatus = ko.observable(true);
        self.billOptionArray = ko.observableArray();
        self.projectExpenOrg=ko.observable();
        self.billOptionArray ([
            {label:"Yes" ,value:"Y"},{label:"No" ,value:"N"}          
        ]);
        
        
        ko.computed(function () {
            if (app.teamMemberProArr().length > 0) {
                app.loading(false);
                if (app.test2().length == 0)
                    return;

                for (var i = 0; i < app.proData2().length; i++)
                {
                    var arr = JSON.parse(app.proData2()[i].TeamMembers);
                    var x = arr.find(e => e.PersonName === app.test2()[0].employeeName);
                    if (x && x != "undefined") {
                        if (x.ProjectId == app.proData2()[i].projectId)
                        {
                            self.approvalProArr().push(app.proData2()[i]);
                        }
                    }
                }
                $.each(self.approvalProArr(), function (index) {
                    self.projectNameArr().push(
                            {
                                label: self.approvalProArr()[index].projectName,
                                value: self.approvalProArr()[index].projectNumber
                            }
                    );
                });
            }
        });
        self.connected = function () {
            app.loading(false);
            var receivedData = oj.Router.rootInstance.retrieve();
            self.pagetype(receivedData.type);
            self.data(receivedData[0]);
            self.mastId(receivedData.masterId);
            if (self.pagetype() == 'ADD') {
            }
            if (self.pagetype() == 'EDIT') {
                self.transactionDateValue(new Date(receivedData[0].transactionDate ).toISOString());
                self.projectNameValue(receivedData[0].projectNumber);
                self.projectNumberValue(receivedData[0].projectNumber);
                self.taskNumberValue(receivedData[0].taskNumber);
                self.taskNameValue(receivedData[0].taskName);
                self.quantityValue(parseInt(receivedData[0].quantity));
                self.timeSheetTypeValue(receivedData[0].timeSheetType);
                self.billableVal(receivedData[0].billableFlag)
                self.quantityDisabled(false);
       }
            
        };

//     ---------------------------------------- fill clibal array function ------------------------ 

        self.fillGlobalArr = ko.computed(function () {
            if (self.approvalProArr().length > 0) {
                self.projectData(self.projectNameArr());
            } else {
                if (app.test2().length > 0)
                {
                    app.messagesDataProvider.push(app.createMessage('error', `${app.test2()[0].employeeName} Not assign to any project`));
                } else
                {
                    return;
                }
            }
            if (app.orgData().length > 0) {
                self.orgData(app.orgData());
            }
            if (app.taskData().length > 0) {
                self.taskData(app.taskData());
            }
        });

        self.orgValueChanged = function (event) {};

//     ---------------------------------------- project name value changed function ------------------------ 

        self.projNameValueChanged = function (event) {          
            self.projectNumberValue(event['detail'].value.value);
            var projectOrg = app.proData2().find(el => el.projectNumber == self.projectNumberValue()).businessUnitName;
            var projectExpenOrgID = app.projectCurrentOrg().find(el => el.PROJECT_NUMBER == self.projectNumberValue()).PROJECT_ORGANIZATION_ID
            self.projectExpenOrg(projectExpenOrgID)
            self.projOrg(projectOrg);           
            var projectStatus = app.proData2().find(e=>e.projectNumber ==  self.projectNumberValue().toString() ).approvalStatus;
           self.projectStatusVal(projectStatus)
          
            $(".customSelect img").show();
            for (var i = 0; i < app.proData2().length; i++) {
                if (event['detail'].value.value == app.proData2()[i].projectNumber) {
                    var arr = JSON.parse(app.proData2()[i].links);
                    for (var j = 0; j < arr.length; j++)
                    {
                        var hreef = arr[j].href;
                        var name = arr[j].name;
                        if (name === "Tasks")
                        {
                            var taskUrl = hreef;
                            var payload = {
                                "href": taskUrl
                            };
                        }
                    }
                    self.getSuccs = function (data) {
                        $(".customSelect img").hide();
                        self.allTasks(data);
                        self.taskNumArr([]);
                        self.taskNameArr([]);
                        $.each(data, function (index) {
                            if (data[index].chargeableFlag) {
                                self.taskNumArr.push({
                                    label: data[index].taskNumber,
                                    value: data[index].taskNumber
                                });
                                self.taskNameArr.push({
                                    label: data[index].taskName,
                                    value: data[index].taskNumber
                                });
                            }
                        });
                    };
                    services.postGeneric(commonhelper.getTaskDet, payload).then(self.getSuccs, app.failCbFn);
                } else {
                }
            }
            if (!event['detail'] || !event['detail'].value.value)
                return;
            self.projectNumberValue(event['detail'].value.value);
        };

//     ---------------------------------------- task name value changed function ------------------------ 

        self.taskNameValueChanged = function (event) {
            if (!event['detail'] || !event['detail'].value)
                return;
            self.taskNumberValue(event['detail'].value);
        };

//     ---------------------------------------- time sheet type value changed function ------------------------ 

        self.timesheetTypeValChanged = function (event)
        {           
            if (self.pagetype() == 'EDIT') {
                self.quantityDisabled(false);
                self.timeSheetTypeValue(event['detail'].value.value);
                if (self.timeSheetTypeValue() === 'Straight Time')
                {
                    self.max(9);
                    self.transDateValChanged();
                    self.quantityValue(0);
                } else {
                    self.max(15);
                    self.quantityValue(0);
                    // self.transDateValChanged();
                }
            } else {           
            self.quantityValue(0);
            self.quantityDisabled(false);
            self.timeSheetTypeValue(event['detail'].value.value);
            if (self.timeSheetTypeValue() === 'Straight Time')
            {
                self.max(9);
                self.transDateValChanged();
            } else {
                self.max(15);
              // self.transDateValChanged();
            }
            }
        };

//     ---------------------------------------- transaction date value changed function ------------------------ 

        self.transDateValChanged = function (event)
        {
            if (self.pagetype() == 'EDIT') {
                
            }else{         
            self.quantityValue(0);
            self.timeSheetTypeValue('')
            if (event)
                self.transactionDateValue(event['detail'].value);

            var sum = 0;
            var sumOver=0;
            for (var i = 0; i < app.test3().length; i++)
            {
                if (app.test3()[i].transactionDate == self.transactionDateValue()
                        && app.test3()[i].timeSheetType == 'Straight Time')
                {
                    sum += app.test3()[i].quantity;
                    var reminder = 9 - sum;
                    self.max(reminder);
                } else if (app.test3()[i].transactionDate == self.transactionDateValue()
                        && app.test3()[i].timeSheetType == 'Over time') {

                    sumOver += app.test3()[i].quantity;
                    var reminder = 15 - sumOver;
                    self.max(reminder);

                }
            }
            }
        };

//     ---------------------------------------- submit button ------------------------ 

        self.submitBtnAction = function () {

            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            if (self.quantityValue() == 0) {
                app.messagesDataProvider.push(app.createMessage('error', 'Quantity Must be More Than Zero  '));
                return;
            }

            var obj = app.proData2().find(e => e.projectNumber == self.projectNumberValue()).projectName;
            var taskName = self.allTasks().find(e => e.taskNumber == self.taskNumberValue()).taskName;

            if (self.pagetype() == 'ADD') {
                var payload = {
                    "empTsId": self.mastId(),
                    "transactionDate": self.transactionDateValue(),
                    "projectNumber": self.projectNumberValue(),
                    "projectName": obj,
                    "taskNumber": self.taskNumberValue(),
                    "taskName": taskName,
                    "expenditureType": self.expenditureTypeValue(),
                    "timeSheetType": self.timeSheetTypeValue(),
                    "quantity": parseInt(self.quantityValue()),
                    "businessUnitName": self.projOrg(),
                    "uom": self.uomValue(),
                    "billableFlag":self.billableVal(),
                    "expenditureOrganizationId":self.projectExpenOrg().toString()
                };             
                 var payload2 = {
               //     "empTsId": self.mastId(),
                    "transactionDate": self.transactionDateValue(),
                    "projectNumber": self.projectNumberValue(),
                    "projectName": obj,
                    "taskNumber": self.taskNumberValue(),
                    "taskName": taskName,
                    "expenditureType": self.expenditureTypeValue(),
                    "timeSheetType": self.timeSheetTypeValue(),
                    "quantity": parseInt(self.quantityValue()),
                    "businessUnitName": self.projOrg(),
                    "uom": self.uomValue(),
                    "persoId":app.personDetails().personId,                    
                    "employeeName": app.test2()[0].employeeName, 
                    "employeeNumber": app.test2()[0].employeeNumber,
                    "timeSheetDate": app.test2()[0].timeSheetDate,
                    "batchName": app.test2()[0].batchName,    
                    "billableFlag":self.billableVal(),
                    "expenditureOrganizationId":self.projectExpenOrg().toString()
                };
            app.loading(true);
         
            }
            if (self.pagetype() == 'EDIT') {
           var editedRecord = app.test3().find(e=>e.empTsId == self.data().empTsId);
                 app.test3().splice(editedRecord.empTsId,1)

                var payload = {
                    "empTsId": self.mastId(),
                    "transactionDate": self.transactionDateValue(),
                    "projectNumber": self.projectNumberValue(),
                    "projectName": obj,
                    "taskNumber": self.taskNumberValue(),
                    "taskName": taskName,
                    "expenditureType": self.expenditureTypeValue(),
                    "timeSheetType": self.timeSheetTypeValue(),
                    "quantity": parseInt(self.quantityValue()),
                    "businessUnitName": self.projOrg(),
                    "uom": self.uomValue(),
                    "billableFlag":self.billableVal()
                };

            }
            getProjectCostFromDB().then(found => {
                app.loading(false)
                
                var xx;
                if (app.test3()&& app.test3().length > 0) {
                    var x = app.test3().filter(el => el.transactionDate == self.transactionDateValue());
                    if (x.length > 0) 
                    {
                        var obj = {
                            "transactionDate": x.transactionDate,
                            "quantity": x.map(el => el.quantity).reduce((p, c) => (p || 0) + c)
                        };
                        xx = obj.quantity;

                    }
               
                }

                if (self.timeSheetTypeValue() == 'Straight Time') {
                if (self.filterArr().length == 0)
                {
                    app.test3().push(payload);
                    oj.Router.rootInstance.go('timeSheetSummary');
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation success'));
                } else if (self.quantitsArr()[0].quantity > 9 && self.quantitsArr()[0].timeSheetType =="Straight Time")
                {
                    app.messagesDataProvider.push(app.createMessage('error', `Transaction date with ${self.quantitsArr()[0].transactionDate} is already added`));
                    return;
                } else { 
                    if(self.quantitsArr()[0].quantity < 9 && self.quantitsArr()[0].timeSheetType =="Straight Time"){
                          var x = 9 - (self.quantitsArr()[0].quantity + (xx?xx:0)); 
                          if (parseInt(self.quantityValue()) > x)
                    {
                        app.messagesDataProvider.push(app.createMessage('error', `Vaild hours for this date ${self.quantitsArr()[0].transactionDate} is ${x} Hours`));
                        self.quantityValue(null);
                        self.max(x);
                        return;
                    } else
                    {
                        app.test3().push(payload);
                        oj.Router.rootInstance.go('timeSheetSummary');
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation success'));
                    }
                    }else{
                          app.test3().push(payload);
                        oj.Router.rootInstance.go('timeSheetSummary');
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation success'));
                    }
                 
                                       
                }
                }
                if (self.timeSheetTypeValue() == 'Overtime') {
                    if (self.filterArr().length == 0)
                    {
                        app.test3().push(payload);
                        oj.Router.rootInstance.go('timeSheetSummary');
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation success'));
                    } else if (self.quantitsArr()[0].quantity > 15 && self.quantitsArr()[0].timeSheetType == 'Overtime')
                    {
                        app.messagesDataProvider.push(app.createMessage('error', `Transaction date with ${self.quantitsArr()[0].transactionDate} is already added`));
                        return;
                    } else  {
                        if (self.quantitsArr()[0].quantity < 15 && self.quantitsArr()[0].timeSheetType == 'Overtime'){
                             var x = 15 - (self.quantitsArr()[0].quantity + (xx ? xx : 0));
                     
                        if (parseInt(self.quantityValue()) > parseInt(x))
                        {
                            app.messagesDataProvider.push(app.createMessage('error', `Vaild hours for this date ${self.quantitsArr()[0].transactionDate} is ${x} Hours`));
                            self.quantityValue(null);
                            self.max(x);
                            return;
                        } else
                        {
                            app.test3().push(payload);
                            oj.Router.rootInstance.go('timeSheetSummary');
                            app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation success'));
                        }
                        }else{
                           app.test3().push(payload);
                            oj.Router.rootInstance.go('timeSheetSummary');
                            app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation success')); 
                        }

                    }
                }
            }, error => {

            });  
                if (self.pagetype() == 'ADD'){
                var insertSucess = function (data) {
            }          
            services.postGeneric(commonhelper.insertOperationRequest, payload2).then(insertSucess, app.failCbFn)
            }
             
        };

//     ---------------------------------------- create time sheet button------------------------ 

        getProjectCostFromDB = () => {
            return new Promise((succ, fail) => {
                var projectCostCbFn = function (data) {
                    self.quantitsArr([]);
                    var filterData = data.filter(el => el.employeeName == app.test2()[0].employeeName && el.transactionDate == self.transactionDateValue())
                    self.filterArr(filterData);
                    if (filterData.length > 0)
                    {
                        var obj = {
                            "transactionDate": filterData[0].transactionDate,
                            "quantity": filterData.map(el => el.quantity).reduce((p, c) => (p || 0) + c),
                            "timeSheetType":data[0].timeSheetType
                        };
                        self.quantitsArr().push(obj);
                    }

                    if (self.filterArr().length > 0)
                    {
                        succ(self.quantitsArr());
                    }

                    if (self.filterArr().length == 0)
                    {
                        succ(self.filterArr([]));
                    }
                };
                services.getGeneric(commonhelper.getAllProjectCost, {}).then(projectCostCbFn, app.failTSCbFn);
            });
        };

//     ---------------------------------------- back button ------------------------ 

        self.backBtnAction = function () {

            oj.Router.rootInstance.go('timeSheetSummary');
        };


        var getTranslation = oj.Translations.getTranslatedString;

        function initTranslations() {

            self.projectNumber(getTranslation("timeSheetSummary.projectNumber"));
            self.projectName(getTranslation("timeSheetSummary.projectName"));
            self.taskNumber(getTranslation("timeSheetSummary.taskNumber"));
            self.taskName(getTranslation("timeSheetSummary.taskName"));
            self.timeSheetType(getTranslation("timeSheetSummary.timeSheetType"));
            self.uom(getTranslation("timeSheetSummary.uom"));
            self.expenditureType(getTranslation("timeSheetSummary.expenditureType"));
            self.quantity(getTranslation("timeSheetSummary.quantity"));
            self.transactionDate(getTranslation("timeSheetSummary.transactionDate"));
            self.submitLbl(getTranslation("timeSheetOperation.submitLbl"));
            self.backLbl(getTranslation("timeSheetOperation.backLbl"));
            self.organizationLbl(getTranslation("timeSheetSummary.organizationLbl"));
            self.timesheetOperationLbl(getTranslation("timeSheetSummary.timesheetOperationLbl"));
            self.billInfo(getTranslation("timeSheetSummary.billInfo"));
            self.projectStatues(getTranslation("timeSheetSummary.projectStatues"));
        }

        initTranslations();
    }

    return timeSheetOperationContentViewModel;
});
