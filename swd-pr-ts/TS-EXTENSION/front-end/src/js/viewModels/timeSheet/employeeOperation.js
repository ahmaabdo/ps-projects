/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * employeeOperation module
 */
define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox', 'ojs/ojvalidationgroup', 'ojs/ojbutton'
], function (oj, ko, app, services, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function employeeOperationContentViewModel() {
        var self = this;

        self.employeeOperationLbl = ko.observable();
        self.employeeName = ko.observable();
        self.employeeNumber = ko.observable();
        self.organization = ko.observable();
        self.timeSheetDate = ko.observable();
        self.totalStraightTime = ko.observable();
        self.totalOverTime = ko.observable();
        self.uom = ko.observable();
        self.id = ko.observable();
        self.batchName = ko.observable();

        self.groupValid = ko.observable();
        self.isRequired = ko.observable(true);

        self.submitDisable = ko.observable();
        self.backDisable = ko.observable();
        self.submitLbl = ko.observable();
        self.backLbl = ko.observable();
        self.bussinesUnitLbl = ko.observable();
        self.isDisabled = ko.observable(false);
        self.organizationDis = ko.observable(true);
        self.isDisabledEmpVal = ko.observable(true);
        self.isDisabledUom = ko.observable(true);
        self.empValueDisable = ko.observable(false);
        self.employeeNameValue = ko.observable();
        self.employeeNumberValue = ko.observable();
        self.organizationValue = ko.observable();
        self.timeSheetDateValue = ko.observable();
        self.totalStraightTimeValue = ko.observable();
        self.totalOverTimeValue = ko.observable();
        self.uomValue = ko.observable("HOURS");
        self.batchNameValue = ko.observable();
        self.selectName = ko.observable();
        self.selectNumber = ko.observable();
        self.employeeNameAttr = ko.observableArray();
        self.orgData = ko.observableArray();
        self.emptyArr = ko.observableArray();
        self.sumbitVisable = ko.observable(true);
        self.pagetype = ko.observable();
        self.data = ko.observable();
        self.empsArr = ko.observableArray();
        self.employeeNumberArr = ko.observable();
        self.selectedUserDisplayName = ko.observable();
        self.organizationArr = ko.observableArray();
        self.retrivedEmpData = ko.observableArray();
        self.employeeNumberArr(new oj.ArrayDataProvider(app.usersArr, {keyAttributes: 'value'}));
        self.expenditureOrgData = new oj.ArrayDataProvider(self.organizationArr, {keyAttributes: 'value'});
        self.currentDate = ko.observable();
         self.currentDate(new Date().toISOString())
         self.departmentName= ko.observable();
         self.departmentValue= ko.observable();
         self.isDisabledDepartment= ko.observable(true);

        self.connected = function () {
            app.loading(true);
            init();

            ko.computed(function () {
                if (app.empItems().length > 0) {
                    app.loading(false);
                    self.empsArr(app.empItems());
                }
            });
        };

//     ---------------------------------------- fill clibal array function ------------------------ 

        var init = function ()
        {
            var receivedData = oj.Router.rootInstance.retrieve();
            self.pagetype(receivedData.type);
            self.data(receivedData.row);
            self.retrivedEmpData(receivedData)

            if (receivedData.type == 'EmpInfo') {
                if (sessionStorage.Username === 'ahmedsheriff')
                {
                var obj1 = app.empItems().find(e => e.personNumber == receivedData.employeeNumberShow).personNumber;
                    self.employeeNameAttr(app.usersArr())
                    self.employeeNameValue(obj1)
                    self.employeeNumberValue(obj1);
                    self.organizationValue(receivedData.organization)
                    self.batchNameValue(receivedData.batchName)
                    self.timeSheetDateValue(receivedData.timeSheetDate)

                } else {
                    self.empValueDisable(true);
                    self.employeeNameAttr().push(receivedData.employeeName);
                    self.employeeNumberValue(receivedData.employeeNumberShow);
                    self.organizationValue(receivedData.organization)
                    self.batchNameValue(receivedData.batchName)
                    self.timeSheetDateValue(receivedData.timeSheetDate)
                }                  
            }
         if (self.pagetype() === 'ADD') {
            if (sessionStorage.Username === 'ahmedsheriff')
            {
                self.empValueDisable(false);
               // self.organizationDis(false);
                if (app.usersArr().length > 0) {
                    self.employeeNameAttr(app.usersArr());
                }
                if (app.projectOrganizArr().length > 0) {
                    self.organizationArr(app.projectOrganizArr());
                }
                
            } else {
                self.empValueDisable(true);
                self.employeeNameAttr().push(app.personDetails().displayName);
                self.employeeNumberValue(app.personDetails().personNumber);
                self.organizationValue(app.personDetails().businessUnitName)
               // self.organizationArr([]);
               // self.organizationArr().push(app.personDetails().businessUnitName)
                self.organizationDis(true);
                self.departmentValue(app.personDetails().departmentName)
            }
            }
        };

//     ---------------------------------------- fill clibal array function ------------------------ 

        self.fillGlobalArr = ko.computed(function () {

//            if (app.usersArr().length > 0) {
//                self.employeeNameAttr(app.usersArr());
//            }
//            if (app.orgData().length > 0) {
//                self.orgData(app.orgData());
//            }

        });

//     ---------------------------------------- employee name value changed function ------------------------ 

        self.empNameValueChanged = function (event) {
             $(".customSelect img").show();
             self.organizationValue('')
            if (!event['detail'] || !event['detail'].value)
                return;
            self.employeeNumberValue(event['detail'].value);           
            for (var i = 0; i < app.usersArr().length; i++) {
                if (event.detail.value == app.usersArr()[i].value) {
                    var EmpDepartmentId =app.usersArr()[i].assingments[0]?app.usersArr()[i].assingments[0].DepartmentId:"Not Assigned";
                    if (EmpDepartmentId) {
                        app.loading(true);
                        var sucessCbFn = function (data) {
                             app.loading(false);
                            for (var index in data) {
                                if (data[index].departmentId == EmpDepartmentId) {
                                     self.departmentValue(data[index].departmentName)
                                }
                            }
                        }
                    services.getGeneric(commonhelper.getAllDepartments).then(sucessCbFn, app.failCbFn)
                    }
                   
                    self.selectedUserDisplayName(app.usersArr()[i].displayName)
                    var getEmployeeDetailsCbFn = function (data) {
                        var MyData = data;
                        self.organizationValue(MyData.businessUnitName)
                        $(".customSelect img").hide();
                    };
                    var getEmployeeDetailsfailCbFn = function () {
                    };
                    var UrlPath = "getemployee/" + self.selectedUserDisplayName()
                    services.getGeneric(UrlPath).then(getEmployeeDetailsCbFn, getEmployeeDetailsfailCbFn);
                }
            }
            
        };

//     ---------------------------------------- submit button function ------------------------ 

        self.submitBtnAction = function () {

            self.submitDisable(false);

            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }

            if (sessionStorage.Username === 'ahmedsheriff')
            {
                var obj = app.empItems().find(e => e.personNumber == self.employeeNumberValue()).displayName;
                var obj1 = app.empItems().find(e => e.personNumber == self.employeeNumberValue()).personID;
                var org = self.organizationValue();
            } else
            {
                var obj = app.personDetails().displayName;
                var obj1 = app.personDetails().personId;
                var org = app.personDetails().businessUnitName;
            }

            if (self.pagetype() === 'ADD') {
                var payload = {
                    "employeeName": obj, //todo get name by number
                    "employeeNumber":obj1,
                    "employeeNumberShow":self.employeeNumberValue(),
                    "organization": org,
                    "timeSheetDate": self.timeSheetDateValue(),
                    "totalOverTime": self.totalOverTimeValue(),
                    "batchName": "Time Sheet Extension",//self.batchNameValue(),
                    "uom": "Hours",
                    "id": 1
                };

                app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation success'));
                app.test2().push(payload);
                oj.Router.rootInstance.go('timeSheetSummary');
            }
            if(self.pagetype() === 'EmpInfo'){
                app.test2([])
             var payload = {
                    "employeeName": obj, //todo get name by number
                    "employeeNumber": obj1,
                    "employeeNumberShow": self.employeeNumberValue(),
                    "organization": org,
                    "timeSheetDate": self.timeSheetDateValue(),
                    "totalOverTime": self.totalOverTimeValue(),
                    "batchName":  "Time Sheet Extension",//self.batchNameValue(),
                    "uom": "Hours",
                    "id": 1
                };
                 app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation success'));
                 app.test2().push(payload);
                 oj.Router.rootInstance.go('timeSheetSummary');
            }
        };

//     ---------------------------------------- back button function ------------------------ 

        self.backBtnAction = function () {

            oj.Router.rootInstance.go('timeSheetSummary');
        };


        var getTranslation = oj.Translations.getTranslatedString;

        function initTranslations() {

            self.employeeName(getTranslation("timeSheetSummary.employeeName"));
            self.employeeNumber(getTranslation("timeSheetSummary.employeeNumber"));
            self.submitLbl(getTranslation("employeeOperation.submitLbl"));
            self.organization(getTranslation("timeSheetSummary.organization"));
            self.timeSheetDate(getTranslation("timeSheetSummary.timeSheetDate"));
            self.totalStraightTime(getTranslation("timeSheetSummary.totalStraightTime"));
            self.totalOverTime(getTranslation("timeSheetSummary.totalOverTime"));
            self.uom(getTranslation("timeSheetSummary.uom"));
            self.batchName(getTranslation("timeSheetSummary.batchName"));
            self.backLbl(getTranslation("timeSheetOperation.backLbl"));
            self.bussinesUnitLbl(getTranslation("timeSheetSummary.bussinesUnitLbl"));
            self.employeeOperationLbl(getTranslation("timeSheetSummary.employeeOperationLbl"));
            self.departmentName(getTranslation("timeSheetSummary.departmentName"));
        }
        initTranslations();
    }
    return employeeOperationContentViewModel;

});
