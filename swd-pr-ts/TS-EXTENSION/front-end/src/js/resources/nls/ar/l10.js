define({
    "Delete": "مسح",
    "Save Draft": "حفظ/مسودة",
    "Submit": "تقديم",
    "Competition Number": "رقم المنافسة",
    "Competition Subject": "موضوع المنافسة",
    "Operating Unit": "وحدة التشغيل",
    "Security Number": "رقم الضمان",
    "Supplier Name": "اسم المورد",
    "Guarantee Value": "قيمة الضمان",
    "Banck": "البنك",
    "Bank Branch": "فرع البنك",
    "Guarantee Type": "نوع الضمان",
    "Guarantee Status": "حالة الضمان",
    "Date Start Guarantee": "تاريخ بداية الضمان",
    "Envelopes Opening Date": "تاريخ فتح المظاريف",
    "Date End Guarantee": "تاريخ  نهاية الضمان",
    "Purchase Order Number": "رقم التعميد",
    "Currency": "العملة",
    "Supplier Representative": "ممثل المورد",
    "Phone Number": "رقم الهاتف",
    "Mail Box": "صندوق البريد",
    "The required release": "الافراج المطلوب",
    "Subject of Guarantee": "موضوع المنافسة",
    "Number of Guarantee": "عدد الضمانات ",
    "Guarantee Number": "رقم الضمان ",
    "Entry": "المدخل",
    "Approval": "اعتماد",
    "Select All": "اختيار الكل ",
    "Cancel": "الغاء",
    "Save": "حفظ",
    "confirmation message": "رسالة تاكيد",
    "Send the guarantee or guarantees to the credit you are sure": "ارسال الضمان او الضمانات الى الاعتماد هل انت متاكد",
    "Description of the competition": "وصف المنافسة",
    "Provision of accreditation": "تقديم الاعتماد",
    "Request Additional Information": "طلب معلومات إضافية",
    "Extension Date": "تمديد الضمان",
    "Forward": "توجيه",
    "Request extended warranty": "طلب تمديد ضمان",
    "Reject": "رفض",
    "Sender": "المرسل",
    "Supplier": "المورد",
    "Ok": "موافق",
    "Search": "بحث",
    "message success": "رسالة نجاح العملية",
    "Guarantees that are near completion": "الضمانات التى قاربت على الانتهاء",
    "The request was successfully approved": "تم اعتماد الطلب بنجاح",
    "Add": "اضافة ",
    "Editing data": "تعديل البيانات",
    "Previous": "السابق",
    "sure Guarantee": "تاكيد ضمان",
    "Next": "التالى",
    "show Extensions": "استعراض التمديدات",
    "Yes": "نعم",
    "Undo release": "التراجع عن الافراج",
    "No": "لأ",
    "Sure Message": "هل أنت متأكد ؟",
//    "Search":"بحث",
    "Login": "تسجيل الدخول",
    "Username": "اسم المستخدم",
    "Password": "الرقم السري",
    "login": "دخول",
    "Forgot Password": "هل نسيت الرقم السرى ؟",
    "Undo Confiscation": "التراجع عن المصادرة",
    "Sure Message": "هل أنت متأكد ؟",
    "Extension": "تمديد",
    "Release": "افراج",
    "confiscation": "مصادرة",
    "Reducing Guarantee": "تخفيض ضمان",
    "Rest": "اعادة تعين ",
    "Guidance": "توجية",
    "Refusal": "رفض",
    "Notification of request for extension guarantee": "تبليغ اعتماد طلب تمديد ضمان ",
    "warrantyNumber": "رقم الضمان",
    "CompetitionNumber": "رقم المنافسة ",
    "value of the Guarantee provided": "قيمة الضمان المقدمة ",
    "Number of baptism": "رقم التعميد",
    "From Date": "تاريخ بداية الضمان",
    "To Date": "تاريخ نهاية الضمان",
    "Guarantee status": "حالة الضمان",
    "Type of Guarantee": "نوع الضمان",
    "Bank": "البنك",
    "Department": "القسم",
    "Number of guarantee letter": "رقم خطاب الضمان",
    "Date of guarantee letter": "تاريخ خطاب الضمان",
    "beginning date of warranty": "تاريخ بداية الضمان",
    "End date of warranty": "تاريخ نهاية الضمان",
    "value of warranty": "قيمة الضمان",
    "Date Of Extension": "تاريخ التمديد",
    "Are you sure to undo the release": "هل انت متاكد من التراجع عن الافراج",
    "Are you sure to undo the confiscation": "هل انت متاكد من التراجع عن المصادرة ",
    "Number": "رقم",
    "Selected": "اختيار",
    "Report undoing the release": "تبليغ التراجع عن الافراج",
    "Competition Number": "رقم المنافسة",
    "Type of letter": "نوع الخطاب",
    "Sure": "تأكيد",
    "Request for withdrawal from the confiscation of the guarantee": "طلب التراجع عن مصادرة الضمان",
    "Notification Confirm Warranty": "تبليغ تأكيد ضمان",
    "Confirm warranty": "تأكيد ضمان",
    "Approve Confiscation Guarantee": "تبليغ إعتماد مصادرة ضمان",
    "Notes": "ملاحظات",
    "Approve Release Guarantee": "تبليغ إعتماد إفراج عن ضمان",
    "Warning": "تحذير",
    "Release Guarantee": "إفراج عن ضمان",
    "Reason": "السبب",
    "Please Select Value": "الرجاء اختيار قيمة",
    "Send the guarantee or guarantees to the release you are sure": "ارسال الضمان او الضمانات الى الافراج  هل انت متاكد",
    "Confirm Guarantee": "طلب تاكيد ضمان",
    "The guarantee approval already": "رقم الضمان معتمد من قبل",
    "Add attachment": "إضافة مرفق",
    "you should select row": "يجب اختيار صف",
    "Please Select Row From Table !!": "الرجاء اختيار رقم الضمان",
    "Warranty status must be released": "يجب ان تكون حالة الضمان مفرج عنة  ",
    "The guarantee Released already": "رقم الضمان مفرج عنه من قبل",
    "Warranty status must be Reserved": "يجب ان تكون حالة الضمان مصادر",
    "The guarantee Confiscated already": "رقم الضمان مصادر من قبل",
    "Warrning": "تحذير",
    "you should Enter Date": "يجب ادخال تاريخ التمديد",
    "extension date must be greater than the end of the warranty": "يجب ان يكون تاريخ التمديد اكبر من تاريخ نهاية الضمان",
    "value of the current warranty": "قيمة الضمان الحالية ",
    "Reducing value": "قيمة التخفيض",
    "Editing Approval": "إعتماد التعديلات",
    "Request reduction guarantee": "طلب تخفيض ضمان",
    "Send a request for undo the confiscation of the guarantee to the approved, are you sure?": "ارسال طلب التراجع عن مصادرة ضمان الى الاعتماد هل انت متأكد؟",
    "You should Enter Reducing Value": "يجب ادخال قيمة التخفيض ",
    "Send an undo request to release the warranty to the approved, are you sure?": "إرسال طلب التراجع عن الافراج عن الضمان الى الاعتماد هل أنت متأكد؟",
    " Reducing Value should be less than current value": "يجب ان تكون قيمة التخفيض اقل من القيمة الحالية ",
    "Review Extensions": "استعراض التمديدات",
    "Sending a request to reduce the warranty Are you sure": "ارسال طلب تخفيض الضمان الى الاعتماد هل انت متاكد؟ ",
    "Exit": "خروج",
    "Are you sure Delete Record ?": "هل انت متأكد من حذف الضمان",
    "Confiscation Guarantee": "مصادرة الضمان",
    "Warranty value after reduction": "قيمة الضمان بعد التخفيض ",
    "Are you sure Delete Record ?": "هل انت متأكد من حذف الضمان",
    "Notification of Credit Reduction Guarantee": "تبليغ اعتماد تخفيض الضمان ",
    "There is no extension on this warranty": "لا يوجد تمديد على هذا الضمان",
    "Name": "الأسم",
    "JOP": "الوظيفة",
    "EmpNO": "رقم الموظف",
    "Dep": "القسم",
    "Grade": "الدرجة",
    "Notification of Credit Reduction Guarantee": "تبليغ اعتماد تخفيض الضمان ",
    "Send modification to modifications": "ارسال التعديل الى التعديلات ",

    //ProForma Invoice  Search Filelds Labels
    "Sales Order Number": "Sales Order Number:",
    "Cutomer PO": "Cutomer PO:",
    "Tender": "Tender:",
    //ProForma Invoice  Search Filelds Labels
    "Shipment Number": "Shipment Number",
    "Shipment Date": "Shipment Date",
    "Shipment Total Amount": "Shipment Total Amount",
    "Curancey": "Curancey",
    "Reference Number": "Reference Number",
    //ProForma Invoice  Create Button Label
    "Create": "إضافة",
    "Acknowledge": "تأكيد",
    //popup Table Headers
    "PI Number": "PI Number",
    "PI Total amount": "PI Total amount",
    "PI Date": "PI Date",
    "PI Status": "PI Status",
    //popup buttons
    "Create PI": "إنشاء الفاتورة الأولية",
    "Cancel PI": "إلغاء الفاتورة الأولية",

    "timeSheetSummary": {
        "employeeName": "اسم الموظف",
        "employeeNumber": "رقم الموظف",
        "projectNumber": "رقم المشروع",
        "projectName": "اسم المشروع",
        "taskNumber": "رقم المهمة",
        "taskName": "اسم المهمة",
        "timeSheetDateFrom": "التاريخ من",
        "expenditureOrganization": "منظمة الإنفاق",
        "timeSheetDateTo": "الى",
        "timeSheetType": "نوع الملف",
        "searchLbl": "بحث",
        "resetLbl": "اعادة",
        "exportLbl": "تحميل",
        "exportLbl2": "تحميل",
        "addLbl": "اضافة",
        "editLbl": "تعديل",
        "viewLbl": "عرض",
        "deleteLbl": "مسح",
        "addLbl2": "اضافة",
        "editLbl2": "تعديل",
        "viewLbl2": "عرض",
        "deleteLbl2": "مسح",
        "organization": "منظمة",
        "timeSheetDate": "تاريخ الملف",
        "totalStraightTime": "مجموع الوقت",
        "totalOverTime": "مجموع الوقت الاضافى",
        "uom": "وحدة القياس",
        "quantity": "الكمية",
        "expenditureType": "نوع الإنفاق",
        "transactionData": "بيانات المعاملات",
        "yes": "نعم",
        "no": "لا",
        "confirmMessage": "Confirm Message",
        "oprationMessage": "هل انت متاكد؟",
        "viewApprovalLabel": "قائمه الموافقة",
        "recordNumber": "رقم الطلب",
        "transactionDate": "تاريخ العمليه",
        "Expendituretype": "نوع الانفاق ",
        "Quantity": "الكميه ",
        "approvStatus": "حاله الموافقة",
        "cost": "السعر",
        "labelSectionTwo": "تكلفة المشروع",
        "labelSectionOne": " انتظار الموافقة",
        "total": "إجمالي الكمية",
        "placeholder": "يرجي الاختيار",
        "batchName": "اسم الدفعة",
        "projectCostLbl": "تكلفة المشروع",
        "createTSLbl": "إنشاء الجدول الزمني",
        "importAndProcessLbl1": "إستيراد و تفعيل",
        "cancelLbl": "إلغاء",
        "transactionToLbl": "المده إلى",
        "transactionFromLbl": "المده من",
        "exportAllDataLbl" : "تنزيل المده كامله",
        "costSummaryLbl" : "تكاليف المشروع",
        "timesheetSummaryLbl" : "ملخص الجدول الزمني",
        "bussinesUnitLbl" : "وحده العمل",
        "timesheetOperationLbl" : "بيانات  الجدول الزمنى",
        "employeeOperationLbl" : "بيانات الموظف",
        "okLbl" : "موافق",
        "createTimeSheetDraftLbl" : "إنشاء الجدول الزمني",
        "billInfo" : "فوترة",
        "departmentName" : "اسم الادارة",
        "projectStatues" : "حاله المشروع",






    }, "timeSheetOperation": {

        "submitLbl": "تأكيد",
        "backLbl": "الرجوع"
    }, "employeeOperation": {

        "submitLbl": "تأكيد",
        "backLbl": "الرجوع"
    },
    "buttoms": {
        "addBtn": "إضافة",
        "editBtn": "تعديل",
        "viewBtn": "عرض",
        "deleteBtn": "مشاهدة",
        "submitBtn": "إرسال",
        "cancleBtn": "إلغاء",
        "searchBtn": "بحث",
        "exportExcel": "استخراج ملف اكسيل",
        "exportPdf": "pdf استخراج ملف ",
        "approveLbl": "موافقة",
        "rejectLbl": "رفض",
    },
    "common": {
        "inputPlaceholder": "ادخل فيمة",
        "selectPlaceholder": "اختر قيمة"
    }, "notification": {
        "requestId": "رقم الطلب",
        "msgTitle": "عنوان الرساله",
        "msgBody": "نص الرساله",
        "requesterNumber": "رقم الطلب",
        "type": "النوع",
        "creationDate": "تاريخ الانشاء",
        "viewDialogTiltle": "بيانات الطلب",
        "rejectReson": "سبب الرفض",
        "comments": "تعليقات "

    }

});
