
function  getAllTranslation(oj,app, services) {
    var getAlltranslationdata = function (data) {
        app.translationArr([]);
        for (var i = 0; i < data.length; i++) {
            app.translationArr.push({
                "englishValue": data[i].englishValue, "arabicValue": data[i].arabicValue, "wordName": data[i].wordName

            });
        }
        oj.Router.rootInstance.go('stclogin');
    };
    services.getGeneric("Translation/GetAll/").then(getAlltranslationdata, app.failCbFn);

}
 
 function  getAllPoNumber(oj,app, services,orgId) {
    var getAllPoNumberdata = function (data) {
        console.log(data);
        console.log(data.poNumber);
        for (var i = 0; i < data.poNumber.length; i++) {
            app.PoNumberArr.push({
                "value": data.poNumber[i].PO_HEADER_ID, "label": data.poNumber[i].PO_NUMBER

            });
        }
    };
    services.getGeneric("MainServices/getPoNumber/"+"99").then(getAllPoNumberdata, app.failCbFn);
}
 function  getAllLPN(oj,app, services,orgId) {
    var getAllLPNdata = function (data) {
        console.log(data.poNumber);
        for (var i = 0; i < data.LPN.length; i++) {
            app.LPNArr.push({
                "value": data.LPN[i].LPN, "label": data.LPN[i].LPN

            });
        }
    };
    services.getGeneric("MainServices/LPN?orgId="+"162").then(getAllLPNdata, app.failCbFn);
}