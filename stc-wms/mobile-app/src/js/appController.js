define(['ojs/ojcore', 'knockout','jquery', 'util/commonhelper', 'ojs/ojrouter', 'ojs/ojthemeutils', 'ojs/ojmodule-element-utils',
    'ojs/ojmoduleanimations', 'ojs/ojknockouttemplateutils', 'ojs/ojarraydataprovider', 'ojs/ojoffcanvas', 'config/services',
    'ojs/ojknockout', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojselectcombobox', 'ojs/ojoffcanvas',
    'ojs/ojbutton', 'ojs/ojmodule-element', 'ojs/ojmessages','hammerjs', 'ojs/ojjquery-hammer','ojs/ojarraytabledatasource','ojs/ojprogress','ojs/ojdialog'],
        function (oj, ko,$ ,commonUtil, Router, ThemeUtils, moduleUtils, ModuleAnimations, KnockoutTemplateUtils,
                ArrayDataProvider, OffcanvasUtils , services , Hammer) {
            function ControllerViewModel() {
                var self = this;
                
                self.qrcode_ali=ko.observable("1702162 | 7700 | 2 | 3768810 | E0111 | 141 | EACH");
                 self.subInventory=ko.observable();
                 self.Locator=ko.observable();
                self.KnockoutTemplateUtils = KnockoutTemplateUtils;
                var getTranslation = oj.Translations.getTranslatedString;
                var selectFORcoy;
                var selectFORpaste;
                self.languageSwitchLbl = ko.observable();
                self.refreshViewForLanguage = ko.observable(false);
                self.personDetails = ko.observable("");
                self.messages = ko.observableArray("");
                self.userData = ko.observable();
                self.messagesDataprovider = new ArrayDataProvider(self.messages);
                self.globalMasterLookup = ko.observableArray();
                self.PoNumberArr = ko.observableArray();
                self.LPNArr = ko.observableArray();
                self.loadingText = ko.observable();
                //***** Define Storage Variable*****//
                self.UserName=ko.observable();
                self.UserId=ko.observable();
                self.Password=ko.observable();
                self.PageValue=ko.observable();
                self.userremember=ko.observable();
                self.passwordremember=ko.observable();
                self.NotifyNO=ko.observable();
                self.responsibilityId=ko.observable();
                self.selectedLanguage=ko.observable();
                self.OrganizationId=ko.observable();
                self.userremember=ko.observable();
                self.passwordremember=ko.observable();
                self.ponumidqr=ko.observable();
                self.vendoridqr=ko.observable();
                self.qtyqr=ko.observable();
                self.linenumidqr=ko.observable();
                self.lpnqr=ko.observable();
                self.locationidqr=ko.observable();
                self.uomqr=ko.observable();
                self.ponumqr=ko.observable();
                self.vendorqr=ko.observable();
                self.linenumqr=ko.observable();
                self.locationqr=ko.observable();
                self.itemqr=ko.observable();
                self.itemidqr=ko.observable();
                //**share**//deliver inf field
                self.ponum=ko.observable();//x2
                self.vendor=ko.observable();
                self.item=ko.observable();
                self.qty=ko.observable();
                self.location=ko.observable();
                self.sub_inv=ko.observable();
                self.locator=ko.observable();
                //**share**//normal deliver
                self.supplier=ko.observable();
                self.linenum=ko.observable();
                self.item=ko.observable();
                self.desc=ko.observable();
                self.lpn=ko.observable();
                self.location=ko.observable();
                self.uom=ko.observable();
                self.qty=ko.observable();
                self.from=ko.observable();
                self.acceptqty=ko.observable();
                self.rejectqty=ko.observable();
                self.acclpn=ko.observable();
                self.RequestId=ko.observable();
                self.translationArr1=ko.observableArray();
//                self.database = window.sqlitePlugin.openDatabase({name: 'STC_WMS_DB.db', location: 'default'});
                self.translationArr = ko.observableArray([
    {
        "WORD_NAME": "ReassignNTFLBL2",
        "CREATION_DATE": "3-MAR-2020",
        "ID": "120",
        "ENGLISH_NAME": "Re Assign",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اعادة توجيه"
    },
    {
        "WORD_NAME": "ReassignHeader",
        "CREATION_DATE": "03-FEB-20",
        "ID": "1",
        "ENGLISH_NAME": "Reassign",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "إعادة تعيين"
    },
    {
        "WORD_NAME": "ReAssignLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "2",
        "ENGLISH_NAME": "Reassign",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اعادة تعيين"
    },
    {
        "WORD_NAME": "PoNumLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "3",
        "ENGLISH_NAME": "Po Num",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رقم po"
    },
    {
        "WORD_NAME": "SupplierLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "4",
        "ENGLISH_NAME": "Supplier",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المورد"
    },
    {
        "WORD_NAME": "LineNumLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "5",
        "ENGLISH_NAME": "Line Num",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رقم الخط"
    },
    {
        "WORD_NAME": "ItemLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "6",
        "ENGLISH_NAME": "Item",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الصنف"
    },
    {
        "WORD_NAME": "DescLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "7",
        "ENGLISH_NAME": "Description",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الوصف"
    },
    {
        "WORD_NAME": "ShipmentNumLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "8",
        "ENGLISH_NAME": "Shipment Number",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رقم الشحنة"
    },
    {
        "WORD_NAME": "LPNLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "9",
        "ENGLISH_NAME": "LPN",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "LPN"
    },
    {
        "WORD_NAME": "LocationLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "10",
        "ENGLISH_NAME": "Location",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الموقع"
    },
    {
        "WORD_NAME": "UOMLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "11",
        "ENGLISH_NAME": "UOM",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "وحدة القياس"
    },
    {
        "WORD_NAME": "QtyLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "12",
        "ENGLISH_NAME": "quantity",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الكمية"
    },
    {
        "WORD_NAME": "vendorLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "13",
        "ENGLISH_NAME": "vendor",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "البائع"
    },
    {
        "WORD_NAME": "EnterLocationLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "14",
        "ENGLISH_NAME": "Enter Location",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "ادخل الموقع"
    },
    {
        "WORD_NAME": "SubInventoryLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "15",
        "ENGLISH_NAME": "Sub-Inventory",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المخزون الفرعى"
    },
    {
        "WORD_NAME": "LocatorLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "16",
        "ENGLISH_NAME": "Locator",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تحديد الموقع"
    },
    {
        "WORD_NAME": "SubmitForApproveLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "17",
        "ENGLISH_NAME": "Submit",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تقديم"
    },
    {
        "WORD_NAME": "ScanQRCodeLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "18",
        "ENGLISH_NAME": "Scan QR Code",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "فحص ال QR"
    },
    {
        "WORD_NAME": "ReturnToMenuLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "19",
        "ENGLISH_NAME": "Return To Menu",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "العودة للقائمة"
    },
    {
        "WORD_NAME": "ShareLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "20",
        "ENGLISH_NAME": "Share",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "مشاركة"
    },
    {
        "WORD_NAME": "ExpressLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "21",
        "ENGLISH_NAME": "Express",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "سريع"
    },
    {
        "WORD_NAME": "NormalLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "22",
        "ENGLISH_NAME": "Normal",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "عادى"
    },
    {
        "WORD_NAME": "ReceiptsLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "23",
        "ENGLISH_NAME": "Receipts",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الاستلام"
    },
    {
        "WORD_NAME": "DeliveryLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "24",
        "ENGLISH_NAME": "Delivery",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "التوصيل"
    },
    {
        "WORD_NAME": "InspectionLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "25",
        "ENGLISH_NAME": "Inspection",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الفحص"
    },
    {
        "WORD_NAME": "ReturnLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "26",
        "ENGLISH_NAME": "Return",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الارجاع"
    },
    {
        "WORD_NAME": "confirmationMessageHeader",
        "CREATION_DATE": "03-FEB-20",
        "ID": "27",
        "ENGLISH_NAME": "Confirmation Message",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رسالة تاكيد"
    },
    {
        "WORD_NAME": "ConfirmationMessageBody",
        "CREATION_DATE": "03-FEB-20",
        "ID": "28",
        "ENGLISH_NAME": "Are you sure?",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "هل انت متاكد؟"
    },
    {
        "WORD_NAME": "yesLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "29",
        "ENGLISH_NAME": "yes",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "نعم"
    },
    {
        "WORD_NAME": "NOLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "30",
        "ENGLISH_NAME": "No",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "لا"
    },
    {
        "WORD_NAME": "FavoriteLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "31",
        "ENGLISH_NAME": "Favorite pages",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المفضلة"
    },
    {
        "WORD_NAME": "InfoNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "32",
        "ENGLISH_NAME": "Info",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "معلومات"
    },
    {
        "WORD_NAME": "rejreasonLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "33",
        "ENGLISH_NAME": "Rejection Reason",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "سبب الرفض"
    },
    {
        "WORD_NAME": "rejnotesLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "34",
        "ENGLISH_NAME": "Notes",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "ملاحظات"
    },
    {
        "WORD_NAME": "subinvLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "35",
        "ENGLISH_NAME": "Sub-Inventory",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المخزون الفرعي"
    },
    {
        "WORD_NAME": "locatorLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "36",
        "ENGLISH_NAME": "locator",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تحديد الموقع"
    },
    {
        "WORD_NAME": "ReAssignLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "37",
        "ENGLISH_NAME": "Reassign",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اعادة تعيين"
    },
    {
        "WORD_NAME": "FavoriteLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "38",
        "ENGLISH_NAME": "Favorite pages",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المفضلة"
    },
    {
        "WORD_NAME": "InfoNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "39",
        "ENGLISH_NAME": "Info",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "معلومات"
    },
    {
        "WORD_NAME": "rejreasonLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "40",
        "ENGLISH_NAME": "Rejection Reason",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "سبب الرفض"
    },
    {
        "WORD_NAME": "rejnotesLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "41",
        "ENGLISH_NAME": "Notes",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "ملاحظات"
    },
    {
        "WORD_NAME": "subinvLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "42",
        "ENGLISH_NAME": "Sub-Inventory",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المخزون الفرعي"
    },
    {
        "WORD_NAME": "locatorLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "43",
        "ENGLISH_NAME": "locator",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تحديد الموقع"
    },
    {
        "WORD_NAME": "common",
        "CREATION_DATE": "03-FEB-20",
        "ID": "44",
        "ENGLISH_NAME": "Name",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الاسم"
    },
    {
        "WORD_NAME": "common",
        "CREATION_DATE": "03-FEB-20",
        "ID": "45",
        "ENGLISH_NAME": "View Profile",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الملف الشخصي"
    },
    {
        "WORD_NAME": "login",
        "CREATION_DATE": "03-FEB-20",
        "ID": "46",
        "ENGLISH_NAME": "User Name:",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اسم المستخدم"
    },
    {
        "WORD_NAME": "login",
        "CREATION_DATE": "03-FEB-20",
        "ID": "47",
        "ENGLISH_NAME": "Password:",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "كلمة المرور"
    },
    {
        "WORD_NAME": "others",
        "CREATION_DATE": "03-FEB-20",
        "ID": "48",
        "ENGLISH_NAME": "search",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "بحث"
    },
    {
        "WORD_NAME": "report",
        "CREATION_DATE": "03-FEB-20",
        "ID": "49",
        "ENGLISH_NAME": "PARAMETER 1",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المتغير الاول"
    },
    {
        "WORD_NAME": "report",
        "CREATION_DATE": "03-FEB-20",
        "ID": "50",
        "ENGLISH_NAME": "PARAMETER 2 test",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المتغير الاول  اختبار"
    },
    {
        "WORD_NAME": "report",
        "CREATION_DATE": "03-FEB-20",
        "ID": "51",
        "ENGLISH_NAME": "PARAMETER 3",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المتغير الثالث"
    },
    {
        "WORD_NAME": "report",
        "CREATION_DATE": "03-FEB-20",
        "ID": "52",
        "ENGLISH_NAME": "PARAMETER 4",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المتغيرالرابع"
    },
    {
        "WORD_NAME": "report",
        "CREATION_DATE": "03-FEB-20",
        "ID": "53",
        "ENGLISH_NAME": "PARAMETER 5",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المتغير الخامس"
    },
    {
        "WORD_NAME": "verifiy",
        "CREATION_DATE": "03-FEB-20",
        "ID": "54",
        "ENGLISH_NAME": "verifiy",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تاكيد"
    },
    {
        "WORD_NAME": "placeholder",
        "CREATION_DATE": "03-FEB-20",
        "ID": "55",
        "ENGLISH_NAME": "Please select",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "من فضلك اختار"
    },
    {
        "WORD_NAME": "loginErrorMessage",
        "CREATION_DATE": "03-FEB-20",
        "ID": "56",
        "ENGLISH_NAME": "incorrect username or password",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اسم المستخدم أو كلمة المرور غير صحيحة"
    },
    {
        "WORD_NAME": "errorMessageVerificationCode",
        "CREATION_DATE": "03-FEB-20",
        "ID": "57",
        "ENGLISH_NAME": "Please enter correct verification code",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الرجاء إدخال رمز التحقق الصحيح"
    },
    {
        "WORD_NAME": "errorMessageCodeEmpty",
        "CREATION_DATE": "03-FEB-20",
        "ID": "58",
        "ENGLISH_NAME": "Please enter complete code verification",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الرجاء إدخال رمز التحقق الكامل"
    },
    {
        "WORD_NAME": "SubmitReceit",
        "CREATION_DATE": "03-FEB-20",
        "ID": "59",
        "ENGLISH_NAME": "Submit",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تقديم"
    },
    {
        "WORD_NAME": "DoneLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "60",
        "ENGLISH_NAME": "Done",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تم"
    },
    {
        "WORD_NAME": "EnterSerialLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "61",
        "ENGLISH_NAME": "Enter Serial",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "أدخل الرقم التسلسلي"
    },
    {
        "WORD_NAME": "FromLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "62",
        "ENGLISH_NAME": "Enter SN",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "ادخل SN"
    },
    {
        "WORD_NAME": "TOLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "63",
        "ENGLISH_NAME": "To",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الى"
    },
    {
        "WORD_NAME": "InspectAcceptLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "64",
        "ENGLISH_NAME": "Accept",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "قبول"
    },
    {
        "WORD_NAME": "InspectRejectLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "65",
        "ENGLISH_NAME": "Reject",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رفض"
    },
    {
        "WORD_NAME": "receiveCode",
        "CREATION_DATE": "03-FEB-20",
        "ID": "66",
        "ENGLISH_NAME": "Didn't receive a code ?",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "لم تستلم رسالة"
    },
    {
        "WORD_NAME": "resend",
        "CREATION_DATE": "03-FEB-20",
        "ID": "67",
        "ENGLISH_NAME": "Resend",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "إعادة إرسال"
    },
    {
        "WORD_NAME": "remember",
        "CREATION_DATE": "03-FEB-20",
        "ID": "68",
        "ENGLISH_NAME": "Remember Me",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تذكرنى"
    },
    {
        "WORD_NAME": "Password",
        "CREATION_DATE": "03-FEB-20",
        "ID": "69",
        "ENGLISH_NAME": "Password",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الرقم السرى"
    },
    {
        "WORD_NAME": "username",
        "CREATION_DATE": "03-FEB-20",
        "ID": "70",
        "ENGLISH_NAME": "User Name",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اسم المستخدم"
    },
    {
        "WORD_NAME": "login",
        "CREATION_DATE": "03-FEB-20",
        "ID": "71",
        "ENGLISH_NAME": "login",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تسجيل الدخول"
    },
    {
        "WORD_NAME": "smsMessageVerification",
        "CREATION_DATE": "03-FEB-20",
        "ID": "72",
        "ENGLISH_NAME": "We have sent you SMS with verification cond on your number",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "لقد أرسلنا لك رسالة نصية قصيرة مع كود التحقق الخاص على رقمك"
    },
    {
        "WORD_NAME": "verifiyYourNumber",
        "CREATION_DATE": "03-FEB-20",
        "ID": "73",
        "ENGLISH_NAME": "Verify your number !",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تحقق من الرقم !"
    },
    {
        "WORD_NAME": "serialNumber",
        "CREATION_DATE": "03-FEB-20",
        "ID": "74",
        "ENGLISH_NAME": "Serial Number",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رقم التسلسل"
    },
    {
        "WORD_NAME": "AddserialNumber",
        "CREATION_DATE": "03-FEB-20",
        "ID": "75",
        "ENGLISH_NAME": "Add",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اضافة"
    },
    {
        "WORD_NAME": "AccLPNLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "76",
        "ENGLISH_NAME": "Acc LPN",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "لجنة التنسيق الإدارية LPN"
    },
    {
        "WORD_NAME": "AccQtyLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "77",
        "ENGLISH_NAME": "Acc Quantity",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "كمية  Acc"
    },
    {
        "WORD_NAME": "RejQtyLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "78",
        "ENGLISH_NAME": "Rej Quantity",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "كمية Rej"
    },
    {
        "WORD_NAME": "placeholderlbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "79",
        "ENGLISH_NAME": "Please select one",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اختر واحد"
    },
    {
        "WORD_NAME": "copyLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "80",
        "ENGLISH_NAME": "Copy",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "نسخ"
    },
    {
        "WORD_NAME": "PasteLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "81",
        "ENGLISH_NAME": "Paste",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "لصق"
    },
    {
        "WORD_NAME": "LovLbll",
        "CREATION_DATE": "03-FEB-20",
        "ID": "82",
        "ENGLISH_NAME": "Lov",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "Lov"
    },
    {
        "WORD_NAME": "GenerateLpnLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "83",
        "ENGLISH_NAME": "Generate LPN",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "إنشاء LPN"
    },
    {
        "WORD_NAME": "ShareLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "84",
        "ENGLISH_NAME": "Share",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "مشاركة"
    },
    {
        "WORD_NAME": "AddFavLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "85",
        "ENGLISH_NAME": "Add To Favorite",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "إضافة إلى المفضلة"
    },
    {
        "WORD_NAME": "RemainingLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "86",
        "ENGLISH_NAME": "Remaining :",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المتبقي :"
    },
    {
        "WORD_NAME": "DeleteLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "87",
        "ENGLISH_NAME": "Delete",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "حذف"
    },
    {
        "WORD_NAME": "SaveLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "88",
        "ENGLISH_NAME": "Save",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "حفظ"
    },
    {
        "WORD_NAME": "MSCALbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "89",
        "ENGLISH_NAME": "MSCA Modules",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "وحدات التطبيق"
    },
    {
        "WORD_NAME": "ChooseRepnsLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "90",
        "ENGLISH_NAME": "Choose Responsibility",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "أختر المسؤولية"
    },
    {
        "WORD_NAME": "LogoutLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "91",
        "ENGLISH_NAME": "Logout",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تسجيل خروج"
    },
    {
        "WORD_NAME": "FromNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "92",
        "ENGLISH_NAME": "From",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "من"
    },
    {
        "WORD_NAME": "TypeNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "93",
        "ENGLISH_NAME": "Type",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "النوع"
    },
    {
        "WORD_NAME": "MSGNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "94",
        "ENGLISH_NAME": "Message",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الرسالة"
    },
    {
        "WORD_NAME": "SubjectNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "95",
        "ENGLISH_NAME": "Subject",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الموضوع"
    },
    {
        "WORD_NAME": "SentNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "96",
        "ENGLISH_NAME": "Sent",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "ارسلت"
    },
    {
        "WORD_NAME": "FromNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "97",
        "ENGLISH_NAME": "From",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "من"
    },
    {
        "WORD_NAME": "NTFIdLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "98",
        "ENGLISH_NAME": "Notification ID",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رقم الاشعار"
    },
    {
        "WORD_NAME": "acceptNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "99",
        "ENGLISH_NAME": "Approve",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "قبول"
    },
    {
        "WORD_NAME": "RejectNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "100",
        "ENGLISH_NAME": "Reject",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "ررفض"
    },
    {
        "WORD_NAME": "CancleNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "101",
        "ENGLISH_NAME": "Cancel",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "غلق"
    },
    {
        "WORD_NAME": "ReAssignLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "102",
        "ENGLISH_NAME": "Reassign",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اعادة تعيين"
    },
    {
        "WORD_NAME": "FavoriteLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "103",
        "ENGLISH_NAME": "Favorite pages",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المفضلة"
    },
    {
        "WORD_NAME": "InfoNTFLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "104",
        "ENGLISH_NAME": "Info",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "معلومات"
    },
    {
        "WORD_NAME": "rejreasonLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "105",
        "ENGLISH_NAME": "Rejection Reason",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "سبب الرفض"
    },
    {
        "WORD_NAME": "rejnotesLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "106",
        "ENGLISH_NAME": "Notes",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "ملاحظات"
    },
    {
        "WORD_NAME": "subinvLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "107",
        "ENGLISH_NAME": "Sub-Inventory",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "المخزون الفرعي"
    },
    {
        "WORD_NAME": "locatorLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "108",
        "ENGLISH_NAME": "locator",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تحديد الموقع"
    },
    {
        "WORD_NAME": "fingerPrinauthLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "109",
        "ENGLISH_NAME": "FingerPrint Authentication",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "مصادقة بصمة اليد"
    },
    {
        "WORD_NAME": "nofavoriteLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "110",
        "ENGLISH_NAME": "There Is No Favorite Pages",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "لا يوجد صفحات بالمفضلة"
    },
    {
        "WORD_NAME": "noNTFLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "111",
        "ENGLISH_NAME": "There Is No Notification",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "لا يوجد اشعارات"
    },
    {
        "WORD_NAME": "turnonLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "112",
        "ENGLISH_NAME": "Turn On",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "تشغيل"
    },
    {
        "WORD_NAME": "turnoffLbl",
        "CREATION_DATE": "03-FEB-20",
        "ID": "113",
        "ENGLISH_NAME": "Turn Off",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "ايقاف"
    },
    {
        "WORD_NAME": "FaceIdLBL",
        "CREATION_DATE": "03-FEB-20",
        "ID": "114",
        "ENGLISH_NAME": "Face ID Recognition",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "مصادقة بصمة الوجه"
    },
    {
        "WORD_NAME": "PO_NUMLBL",
        "CREATION_DATE": "3-MAR-2020",
        "ID": "115",
        "ENGLISH_NAME": "PO Number",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رقم امر الشراء"
    },
    {
        "WORD_NAME": "QUANTITYLBL",
        "CREATION_DATE": "3-MAR-2020",
        "ID": "116",
        "ENGLISH_NAME": "Quantity",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "الكمية"
    },
    {
        "WORD_NAME": "ORG_IDLBL",
        "CREATION_DATE": "3-MAR-2020",
        "ID": "117",
        "ENGLISH_NAME": "ORG ID",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رقم الادارة"
    },
    {
        "WORD_NAME": "VENDOR_NAMELBL",
        "CREATION_DATE": "3-MAR-2020",
        "ID": "118",
        "ENGLISH_NAME": "Vendor Name",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اسم المورد"
    },
    {
        "WORD_NAME": "PONumTFLbl",
        "CREATION_DATE": "3-MAR-2020",
        "ID": "119",
        "ENGLISH_NAME": "PO Number",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "رقم امر الشراء"
    },
    {
        "WORD_NAME": "VendorNameNTFLbl",
        "CREATION_DATE": "3-MAR-2020",
        "ID": "120",
        "ENGLISH_NAME": "Vendor Name",
        "APPLICATION_NAME": "XXWMS",
        "ARABIC_NAME": "اسم المورد"
    }
]);

               // ************define Array***************//
             
                //--------------------------------------------//
                self.showHeader = ko.observable("off");
                self.headerLable = ko.observable("");
                
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable();
                //initTranslations();
                // Header Config
                self.headerConfig = ko.observable({'view': [], 'viewModel': null});
                moduleUtils.createView({'viewPath': 'views/header.html'}).then(function (view) {
                    self.headerConfig({'view': view, 'viewModel': new self.getHeaderModel()});
                });
                self.selectedMenuItem = ko.observable("(None selected yet)");
                self.pageLbel = ko.observable();
                self.menuItemAction = function (event)
                {
                    self.selectedMenuItem(event.target.value);
                }.bind(self);

                self.headerLable = ko.pureComputed(function () {
                    var label = self.router.currentState().label;
                    localStorage.setItem("PageHeader",label);
                    console.log(label);
                     
                    return label;
                     
                }.bind(this));
                
                

//                

//                self.backbtn=function(data){
//                     oj.Router.rootInstance.go(data);
//                }
self.selectItemTOClipboardCombo=function(){
                                                                    
     $(document).ready(function () {
            $("oj-combobox-one").click(function () {
         
               var obj = this.options.find(e=>e.value == this.value);
                if(obj){
                    selectFORcoy=obj.label;
                    console.log(selectFORcoy);
                selectFORpaste = this;
                }
                
                
            });

        });
};

self.selectItemTOClipboardInputText=function(){
                                                                    
     $(document).ready(function () {
            $("oj-input-text").click(function () {
         
               
                    selectFORcoy=this.value;
                    console.log(selectFORcoy);
                selectFORpaste = this;
                });
               
            });

};

                self.count=ko.observable();
                self.GetNTFCount=function(){
                    var username=  localStorage.getItem("UserName");
                    var NTFCount=function(data){
                        
                       console.log("app",username);
                        if(data.notificationCount.length == 0)
                        {
                        
                       self.count(0);
                        }
                           
                        else {
                      
                        self.count(data.notificationCount[0].NOTIFICATION_COUNT);
                        }
                        console.log("ccc",self.count());
                        console.log(data);
                        
                    };
                    console.log("user notif",username);
                    services.getGeneric("notification/GetNTFCount?userName="+username).then(NTFCount,self.failCbFn);
                    
                };
               // self.GetNTFCount();
                //***Post TO Favorite***//
                var countId=0;
                self.AddFavoritePage=function(){
                 
                    
                    var FavoriteCBFN=function(data){
                         localStorage.setItem("countId",countId+1);
                        console.log("data",data);
                       // console.log("app pagec",localStorage.getItem("page"));
                    };
                      console.log("loc return",localStorage.getItem("PageHeader"));
                     var paylod={
                      "arabicMeaning":localStorage.getItem("PageHeader"),
                      "englishMeaning":localStorage.getItem("PageHeader"),
                      "userId":localStorage.getItem("User_Id"),
                      "pageKey":localStorage.getItem("PageValue")
                              
                      //localStorage.getItem("User_Id")
                  };  
                    
                    services.addGeneric("Favourite/AddFavourite",JSON.stringify(paylod)).then(FavoriteCBFN,self.failCbFn);
                   
                };
                          
         
               
                self.copy = function () {
                     cordova.plugins.clipboard.copy(selectFORcoy);
                     console.log("copied",selectFORcoy);
                };
                self.paste = function () {
                       cordova.plugins.clipboard.paste(function (coppiedText) {

                selectFORpaste.value = coppiedText;
                 console.log(coppiedText);
            });
                };

//                self.backbtn=function(){
//                    
//                    if(localStorage.getItem("PageValue")=='Receipts' || localStorage.getItem("PageValue")=='Deliver'
//                            || localStorage.getItem("PageValue")=='Inspect'||localStorage.getItem("PageValue")=='Return')
//                    {
//                        console.log("pageval",localStorage.getItem("PageValue"));
//                        oj.Router.rootInstance.go('PO_INQUIRER');
//                    }else
//                     window.history.back();
//                };
                
                var fingerprint=localStorage.getItem('switchvalue') == 'true'
                if(fingerprint){
                    Fingerprint();
                }
                function Fingerprint() {
                   
                    FingerprintAuth.isAvailable(function (result) {

                        console.log("FingerprintAuth available: " + JSON.stringify(result));

                        // If has fingerprint device and has fingerprints registered
                        if (result.isAvailable == true && result.hasEnrolledFingerprints == true) {
                            checkAgain = false;
                            // Check the docs to know more about the encryptConfig object :)
                            var encryptConfig = {
                                clientId: "myAppName",
                                username: "taqi",
                                password: "1230",
                                maxAttempts: 3,
                                locale: "en_US",
                                dialogTitle: "Please put your finger on Device",
                                dialogMessage: "For Authentication Security",
                                dialogHint: "No one will steal your identity, promised"
                            }; // See config object for required parameters

                            // Set config and success callback
                            FingerprintAuth.encrypt(encryptConfig, function (_fingerResult) {
                                console.log("successCallback(): " + JSON.stringify(_fingerResult));

                                if (_fingerResult.withFingerprint) {

                                    console.log("Successfully encrypted credentials.");
                                    console.log("Encrypted credentials: " + result.token);
                                } else if (_fingerResult.withBackup) {
                                    console.log("Authenticated with backup password");
                                }
                                // Error callback
                            }, function (err) {
                                if (err === "Cancelled") {
                                    console.log("FingerprintAuth Dialog Cancelled!");

                                } else {
                                    // console.log("FingerprintAuth Error: " + err);
                                    navigator.app.exitApp();
                                }
                            });
                        }

                    }, function (message) {
                        console.log("isAvailableError(): " + message);
                    });
                }
                self.LOV = function () {};
                self.generateLPN = function () {};
                self.delete1 = function () {};
                self.share = function () {};
                self.submitForApprove = function () {};
                self.addFav = function () {
//                    console.log("mainMenu");
//                    console.log(window.location.href);
//                    var path = window.location.href;
//                    console.log(path.indexOf("="));
//                    var pageRoot = path.substring(path.indexOf("=") + 1);
////                    console.log(last);
//                    console.log(window.location.pathname);
//                    console.log(window.location.pathname + window.location.search);
//                    console.log(Router.rootInstance);
//                    var x = self.router.currentState().label;
//                    var value = Router.rootInstance.states.find(r => r.id === "changeLang").value;
//                    var _label = Router.rootInstance.states.find(r => r.id === "changeLang")._label;
////                      console.log(last,label);
//                    console.log(value);
//                    console.log(_label);
//                    console.log(pageRoot);

                     self.AddFavoritePage();

                };
                self.BackBtn=function(){
                    
                   if(localStorage.getItem("PageValue")=='Receipts' || localStorage.getItem("PageValue")=='Deliver'
                            || localStorage.getItem("PageValue")=='Inspect'||localStorage.getItem("PageValue")=='Return')
                    {
                       
                        oj.Router.rootInstance.go('PO_INQUIRER');
                    }else
                     window.history.back();
                };
//--------------------------------------------//
                self.setLocale = function (lang) {
                    oj.Config.setLocale(lang,
                            function () {
                                self.refreshViewForLanguage(false);
                                $("html").attr("lang", lang);
                                if (lang === 'ar') {
                                    $("html").attr("dir", "rtl");
                                } else {
                                    $("html").attr("dir", "ltr");
                                }
                              //  initTranslations();
                                self.refreshViewForLanguage(true);
                            }
                    );
                };
                self.getLocale = function () {
                    return oj.Config.getLocale();
                };
                /************ action to switch language*****************/
                self.switchLanguage = function () {
                    if (self.getLocale() === "ar") {
                        self.setLocale("en-US");
                        localStorage.setItem("selectedLanguage", "en-US");
                    } else if (self.getLocale() === "en-US") {
                        self.setLocale("ar");
                        localStorage.setItem("selectedLanguage", "ar");
                    }
                };
              //******************** Get Translation Service****************//
                self.getTranslationDB = function () {

                    var translationCBFN = function (data) {
                        for (var i = 0; i < data["get reassign users"].length; i++) {
                            self.translationArr1.push({
                                "WordName": data["get reassign users"][i].WORD_NAME,
                                "EngWord": data["get reassign users"][i].ENGLISH_NAME,
                                "ArrWord": data["get reassign users"][i].ARABIC_NAME
                            });
                        }
                        self.TranslationSqLite();
                    };
////                  
                    services.getGeneric("MainServices/getTranslation").then(translationCBFN, self.failCbFn);

                };
//                ///*********Create And Insert Into SQlite*********///
                self.getTranslationDB();
// self.TranslationSqLite=function(){
//                    deletedb();
//                     console.log("heeeeey");
//                  // self.translationArr()[t].WordName,self.translationArr()[t].ArrWord,self.translationArr()[t].EngWord
//                  if(!self.database)
//                        self.database = window.sqlitePlugin.openDatabase({name: 'STC_WMS_DB.db',location:'default'});
//                    
//                            self.database.transaction(function (trans) {
//                             
//                       trans.executeSql('CREATE TABLE IF NOT EXISTS StcWMSTranslation (code,arabicName,englishName)');
//                       
//                         for(var t=0;t<self.translationArr().length;t++){
//                        trans.executeSql("INSERT INTO StcWMSTranslation(code,arabicName,englishName) VALUES (?,?,?)",[self.translationArr()[t].WordName,self.translationArr()[t].ArrWord,self.translationArr()[t].EngWord],function(trans,res){
//                          
//                },function(e){
//                            
//                            console.log("error",e.message);
//                            
//                        });
//                    }
//                    
//            });
//          
//            };
//            function deletedb(){
//          
//          var  database = window.sqlitePlugin.openDatabase({name: 'STC_WMS_DB.db',location:'default'});
//          database.executeSql('delete from StcWMSTranslation');
//          
//      }
//                self.getTranslationMethod = function (code,cbFun) {
                self.getTranslationMethod = function (code) {
                           var tempvalue;
                           ///when we use sqlite must activate this code////
//                    ///////

//                 self.database.transaction(function(trans){
//                                
//                                trans.executeSql("SELECT * FROM StcWMSTranslation;",[],function(trans,res){
//                                    for(var i=0;i<res.rows.length;i++){
//                                       
//                                        if(res.rows.item(i).code==code){
//                                            
//                                            if(localStorage.getItem("selectedLanguage")==="ar"){
//                                                if(cbFun)
//                                             cbFun(res.rows.item(i).arabicName);                                              
//                                            }else{
//                                                if(cbFun)
//                                                cbFun(res.rows.item(i).englishName); 
//                                            }
//                                   
//                                        }
//                          
//                                    }
//                                   
//                          
//                                });
//                                
//                            });
//                    
//                    //////////
//                                            
                    for (var i = 0; i < self.translationArr1().length; i++) {
                        if (self.translationArr1()[i].WordName == code) {
                            if (localStorage.getItem("selectedLanguage") === "ar") {
                                tempvalue = self.translationArr1()[i].ArrWord;
                            } else {   
                                tempvalue = self.translationArr1()[i].EngWord ;   
                            }
                        }  
                    }
                return tempvalue;
                
                };
            //  self.CallTranslationAfterChangeLang=function(){
                 //   getAllTranslation(oj,self, services);
                   // getAllPoNumber(oj,self, services);
            //  };
                self.getAllPoNumberByOrganizatiOnId=function(orgId){
                     //getAllPoNumber(oj,self, services,orgId);
                    // getAllLPN(oj,self, services,orgId);
                };


                self.customRestPayload = function (restMethod, url, payload) {
                    return{
                        "restMethod": restMethod,
                        "url": url,
                        "payload": payload
                    };
                };

                var platform = ThemeUtils.getThemeTargetPlatform();

                self.createMessage = function (severity, summary) {
                    self.messages([]);
                    self.messages.push({
                        severity: severity,
                        summary: summary,
                        autoTimeout: 0
                    });
                };

                self.failCbFn = function (event) {
                   $("#loader").hide();
                    self.createMessage("error", event.responseText.substring(event.responseText.indexOf('<title>') + 7, event.responseText.indexOf('</title>')));
                };


                // *************************get person login Details ***************************//
                self.getUserLoginData = function () {
                    var UserLoginDataCbFn = function (data) {

                    };
                    oj.Router.rootInstance.go('loginVerify');
                    var parameter = "?userName=" + "hosni" + "&userId=" + "231";
                    services.getGeneric(commonUtil.getPersonLoginData + parameter).then(UserLoginDataCbFn, self.failCbFn);
                };

                // ***************************************************************************//
                // ***************************** Get All Organization *************************//   
                self.getAllOrganization = function () {
                    var GetAllOrganizationCbFn = function () {


                    };
                    var parameter = "?userName=" + "hosni" + "&userId=" + "231";
                    services.getGeneric(commonUtil.getOrganizationData + parameter).then(GetAllOrganizationCbFn, self.failCbFn);
                };

                //***************************************************************************************// 
                // ************************************ get User Responsibility *************************//
                self.getUserResponsibility = function () {
                    var GetAllUserResponsibilityCbFn = function () {


                    };
                    var parameter = "?userName=" + "test" + "&userId=" + "231";
                    services.getGeneric(commonUtil.getUserResponsibility + parameter).then(GetAllUserResponsibilityCbFn, self.failCbFn);
                };

                //***************************************************************************************//
 function GetUserDetails(){
              var username=localStorage.getItem("UserName");
             
               var GetUDCBFN=function(data){
                    
            localStorage.setItem("UserId",data.LoginDetails[0].USER_ID);
            self.GetNTFCount();
            console.log("uid login");
             oj.Router.rootInstance.go('mainMenu');
               };
               
               services.getGeneric("MainServices/UserLoginDetails?userName="+username).then(GetUDCBFN,self.failCbFn);
           };
                self.login = function (userName, password) {

                    var loginSuccessCbFn = function (P_SESSION_ID) {
                        oj.Router.rootInstance.store(P_SESSION_ID);
                       GetUserDetails();
//                        oj.Router.rootInstance.go('mainMenu');
                       
                    };
                    var loginFailCbFn = function () {
                        self.createMessage("error", self.loginErrorMessage());
                         $("#loader").hide();
                        oj.Router.rootInstance.go('stclogin');
                      
                    };

                    function authinticate(data) {
                    
                        var P_SESSION_ID = data["env:Envelope"]["env:Body"]["OutputParameters"]["P_SESSION_ID"];
                        var result = data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"];
                        var check = result;
                        
                        if (check == "S") {
                            loginSuccessCbFn(P_SESSION_ID);
                            
                            
                        } else {
                            loginFailCbFn();
                        }
                    }

                    services.authenticate(userName, password).then(authinticate, self.failCbFn)
                };

                // Router setup
                self.router = Router.rootInstance;

                var lang = self.getLocale();
                self.router.configure({
                    'changeLang': {label: 'Change Language ', title: lang === 'ar' ? 'تغير اللغة' : 'Change Language ', value: "generalScreen/changeLang"},
                    'pendingApprovalRequest': {label: 'Pending Approval Request ', title: lang === 'ar' ? 'في انتظار طلب الموافقة' : 'Pending Approval Request', value: "generalScreen/pendingApprovalRequest"},
                    'notifications': {label: 'Notifications', value: "generalScreen/notifications"},
                    'organization': {label: 'Organization', title: lang === 'ar' ? 'منظمة' : 'Organization', value: "generalScreen/organization"},
                    'stclogin': {label: 'Login', title: lang === 'ar' ? 'تسجيل' : 'Login', value: "generalScreen/stclogin",isDefault: true},
                    'testNav': {label: 'testNav', value: "generalScreen/testNav"},
                    'loginVerify': {label: 'Login Verify ', title: lang === 'ar' ? 'تاكيد الدخول' : 'Login Verify', value: "generalScreen/loginVerify"},
                    'mainMenu': {label: 'Main Menu', title: lang === 'ar' ? 'الصفحة الرئسية' : 'Main Menu', value: "generalScreen/mainMenu"},
                    'PO_INQUIRER': {label: 'purchasing wizard', title: lang === 'ar' ? 'الصفحة الفرعية' : 'Sub Menu ', value: "generalScreen/PO_INQUIRER"},
                    'signup': {label: 'Signup Page', value: "main/signup"},
                    'faqs': {label: 'FAQs Page', value: "main/faqs"},
                    'notification': {label: 'Notification', title: lang === 'ar' ? 'الصفحة الفرعية' : 'Sub Menu ', value: "generalScreen/notification"},
                    'dashboard': {label: 'Dashboard Page', value: "main/dashboard"},
                    'ourLocation': {label: 'Our location', value: "main/ourLocation", title: "Our Location"},
                    'offers': {label: 'Offers', value: "main/offers", title: "Offers"},
                    'maintenance': {label: 'Maintenance', value: "maintenance/maintenance", title: "Maintenance"},
                    'bookMaintenance': {label: 'Book maintenance', value: "maintenance/bookMaintenance", title: "Book maintenance"},
                    'complain': {label: 'Complain', value: "complain/complain", title: "Complain"},
                    'testDrive': {label: 'Test drive', value: "testDrive/testDrive", title: "Test drive"},
                    'reservationPayment': {label: 'Reservation payment', value: "reservationPayment/reservationPayment", title: "Reservation payment"},
                    'vehicleHistory': {label: 'Vehicle history', value: "vehicleHistory/vehicleHistory", title: "Vehicle history"},
                    'cars': {label: 'Cars', value: "vehicleHistory/cars", title: "Cars"},
                    'inquiry': {label: 'Inquiry', value: "inquiry/inquiry", title: "Inquiry"},
                    'service': {label: 'Service', value: "service/service", title: "Service"},
                    'profile': {label: 'Profile', value: "profile/profile", title: "Profile"},
                    'resetPassword': {label: 'Reset Password', value: "resetPassword/resetPassword", title: "Reset Password"},
                    'carHistory': {label: 'Car history', value: "vehicleHistory/carHistory", title: "Car history"},
                    'promotions': {label: 'Promotions', value: "promotions/promotions", title: "Promotions"},
                    'Receipts': {label: 'Receipt', value: "Receipt/Receipts", title: "Receipts"},
                    'ReceiptSerial': {label: 'Serial Receipt', value: "Receipt/ReceiptSerial", title: "Serial Receipt"},
                    'ExpressReceipt': {label: 'Express Receipt', value: "Receipt/ExpressReceipt", title: "Express Receipt"},
                    'ReceiptInformation': {label: 'Receipt Information', value: "Receipt/ReceiptInformation", title: "Receipt Information"},
                    'NormalReceipt': {label: 'Normal Receipt', value: "Receipt/NormalReceipt", title: "Normal Receipt"},
                    'ReceiptInformationField': {label: 'Receipt Information', value: "Receipt/ReceiptInformationField", title: "Receipt Information"},
                    'Deliver': {label: 'Deliver', value: "Deliver/Deliver", title: "Deliver"},
                    'serialDeliver': {label: 'Serial Deliver', value: "Deliver/serialDeliver", title: "Serial Deliver"},
                    'expressDeliver': {label: 'Express Deliver', value: "Deliver/expressDeliver", title: "Express Deliver"},
                    'normalDeliver': {label: 'Normal Deliver', value: "Deliver/normalDeliver", title: "Normal Deliver"},
                    'deliverInformation': {label: 'Deliver Information', value: "Deliver/deliverInformation", title: "Deliver Information"},
                    'deliverInformationField': {label: 'Deliver Information', value: "Deliver/deliverInformationField", title: "Deliver Information"},
                    'Inspect': {label: 'Inspect', value: "Inspect/Inspect", title: "Inspect"},
                    'serialInspect': {label: 'Serial Inspect', value: "Inspect/serialInspect", title: "Serial Inspect"},
                    'expressInspect': {label: 'Express Inspect', value: "Inspect/expressInspect", title: "Express Inspect"},
                    'normalInspect': {label: 'Normal Inspect', value: "Inspect/normalInspect", title: "Normal Inspect"},
                    'inspectInformation': {label: 'Inspect Information', value: "Inspect/inspectInformation", title: "Inspect Information"},
                    'inspectInformationField': {label: 'Inspect Information', value: "Inspect/inspectInformationField", title: "Inspect Information"},
                    ////
                    'Return': {label: 'Return', value: "Return/Return", title: "Return"},
                    'serialReturn': {label: 'Serial Return', value: "Return/serialReturn", title: "Serial Return"},
                    'expressReturn': {label: 'Express Return', value: "Return/expressReturn", title: "Express Return"},
                    'normalReturn': {label: 'Normal Return', value: "Return/normalReturn", title: "Normal Return"},
                    'returnInformation': {label: 'Return Information', value: "Return/returnInformation", title: "Return Information"},
                    'returnInformationField':{label:'Return Information',value:"Return/returnInformationField",title:"Return Information"},
                    'NotificationDetails':{label:'Notification Details',value:"generalScreen/NotificationDetails",title:"Notification Details"},
                    'FovritePage':{label:'Favorite Pages',value:"generalScreen/FovritePage",title:"Favorite Pages"}
                    
                });
                
                Router.defaults['urlAdapter'] = new Router.urlParamAdapter();
                // Callback function that can return different animations based on application logic.
                function switcherCallback(context) {
                    if (platform === 'android')
                        return 'fade';
                    return null;
                }



                //refresh view when language change
                self.refreshData = ko.computed(function () {

//                    self.personDetails(JSON.parse(localStorage.getItem("personDetails")));
//                    self.personDetails() ? window.history.pushState({}, document.title, "/" + "?root=dashboard") : window.history.pushState({}, document.title, "/" + "");
                    if (self.refreshViewForLanguage()) {
                        self.setLocale(localStorage.getItem("selectedLanguage"));
                    }
//                    if (!self.userData())
//                        self.getUserData();
                });

                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});

                //------------------------------------------------//
                self.progressValue = ko.observable(0);
                self.notfClickAction = function () {
                    
                    
                    oj.Router.rootInstance.go('notifications');

                };
                self.goToSetting =function(){
                    self.closeInner();
                   // var value = self.router.currentState().label;
                    //self.showHeader("on");
                    oj.Router.rootInstance.go('changeLang');
                };
                self.goToMainMenu = function(){
                     //self.closeInner();
                     //oj.Router.rootInstance.go('mainMenu');
                };
                self.goToResponsibility = function(){
                    self.closeInner();
                     oj.Router.rootInstance.go('mainMenu');
                };
                
//                self.Logout = function(){
////                    self.closeInner();
////                    localStorage.clear();
////                    
////                   // console.log(localStorage.getItem("username"))
////                     oj.Router.rootInstance.go('stclogin');
//
//                };

                self.Logout = function () {
                    self.closeInner();
                    window.history.length=1;
//                    localStorage.clear();
                    localStorage.setItem("UserName","");
                    localStorage.setItem("Password","");
                    oj.Router.rootInstance.go('stclogin');
                };
                
                self.GoToFav=function(){
                 
                  self.closeInner();
                    oj.Router.rootInstance.go('FovritePage');
                    
                };
//                self.goToFav = function () {
//                     
//                    
//                };
                
                this.innerDrawer =
                        {
                            "displayMode": "overlay",
                            "selector": "#iDrawer",
                            "content": "#iMain"
                        };

                // toggle show/hide offcanvas
                this.toggleInner = function ()
                {
                    self.transmain();
                    return oj.OffcanvasUtils.toggle(self.innerDrawer);
                   
                    
                };

                this.closeInner = function ()
                {
                    return oj.OffcanvasUtils.close(self.innerDrawer);
                };

                this.toggleOuter = function ()
                {
                    return oj.OffcanvasUtils.toggle(self.outerDrawer);
                };


                //----------------------------------------------//

                self.loadModule = function () {
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath})
                        ]);
                        masterPromise.then(
                                function (values) {
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                }
                        );
                    });
                };
                self.moduleAnimation = ModuleAnimations.switcher(switcherCallback);

                // Header Setup
                self.getHeaderModel = function () {
                    this.pageTitle = self.router.currentState().label;
//                    this.transitionCompleted = function () {
//                        // Adjust content padding after header bindings have been applied
//                        self.adjustContentPadding();
//                    };
                };


                self.adjustContentPadding = function () {
                    var topElem = document.getElementsByClassName('oj-applayout-fixed-top')[0];
                    var contentElem = document.getElementsByClassName('oj-applayout-content')[0];
                    var bottomElem = document.getElementsByClassName('oj-applayout-fixed-bottom')[0];

                    if (topElem) {
                        contentElem.style.paddingTop = topElem.offsetHeight + 'px';
                    }
                    if (bottomElem) {
                        contentElem.style.paddingBottom = bottomElem.offsetHeight + 'px';
                    }

                    contentElem.classList.add('oj-complete');
                };
                //*********define Label***************//
                self.loginErrorMessage = ko.observable();
                self.loginErrorMessage(self.getTranslationMethod("loginErrorMessage"))
                //**************************************************//
//            
               
            
                // Method for adjusting the content area top/bottom paddings to avoid overlap with any fixed regions.
                // This method should be called whenever your fixed region height may change.  The application
                // can also adjust content paddings with css classes if the fixed region height is not changing between
                // views.
                self.adjustContentPadding = function () {
                    var topElem = document.getElementsByClassName('oj-applayout-fixed-top')[0];
                    var contentElem = document.getElementsByClassName('oj-applayout-content')[0];
                    var bottomElem = document.getElementsByClassName('oj-applayout-fixed-bottom')[0];

                    if (topElem) {
                        contentElem.style.paddingTop = topElem.offsetHeight + 'px';
                    }
                    if (bottomElem) {
                        contentElem.style.paddingBottom = bottomElem.offsetHeight + 'px';
                    }
                    // Add oj-complete marker class to signal that the content area can be unhidden.
                    // See the override.css file to see when the content area is hidden.
                    contentElem.classList.add('oj-complete');
                }

                //*************************************************//
                
           this.innerDrawer =
        {
          displayMode: 'overlay',
          selector: '#iDrawer',
          content: '#iMain'
        };
  
        // toggle show/hide offcanvas
        this.toggleInner = function () {
            self.transmain();
          return oj.OffcanvasUtils.toggle(self.innerDrawer);
           
        };
  
        this.closeInner = function () {
          return oj.OffcanvasUtils.close(self.innerDrawer);
        };
         self.navData = ko.observableArray();
      self.navDataSource = ko.observableArray();
        self.navDataArray = [
        {
          name: 'STC EAM MODULES',
          id: 'mainMenu',
          iconClass: 'oj-navigationlist-item-icon navSettignsIcon'
        }
//        {
//          name: self.nav_choose_responsibility(),
//          id: 'responsibilities',
//          iconClass: 'oj-navigationlist-item-icon dashboardIcon'
//        },
//        {
//          name: self.dashboards(),
//          id: 'dashboards',
//          iconClass: 'oj-navigationlist-item-icon navSettignsIcon'
//        },
//        {
//          name: self.nav_signout(),
//          id: 'signout',
//          iconClass: 'oj-navigationlist-item-icon signoutIcon'
//        }
      ];
        self.navData(self.navDataArray);
        self.navDataSource = new oj.ArrayTableDataSource(self.navData(), {
          idAttribute: 'id'
        });

self.menuSelectedChange = function (event) {
          
          console.log(event.detail.value);
          oj.Router.rootInstance.go(event.detail.value);
        };
      
  
                
//                  this.drawer =
//        {
//          displayMode: 'push',
//          selector: '#drawer',
//          content: '#main'
//        };
//  
//        this.toggleDrawer = function () {
//          return oj.OffcanvasUtils.toggle(self.drawer);
//        };
//  
//        this.openDrawer = function () {
//          return oj.OffcanvasUtils.open(self.drawer);
//        };
//  
//        this.isRTL = function () {
//          var dir = document.documentElement.getAttribute('dir');
//          if (dir) { dir = dir.toLowerCase(); }
//          return (dir === 'rtl');
//        };
//  
//        // use hammer for swipe
//        var mOptions = {
//          recognizers: [
//            [Hammer.Swipe, { direction: Hammer.DIRECTION_HORIZONTAL }]
//          ] };
//  
//        $('#main')
//          .ojHammer(mOptions)
//          .on('swipeleft', function (event) {
//            event.preventDefault();
//            if (self.isRTL()) { self.openDrawer(); }
//          })
//          .on('swiperight', function (event) {
//            event.preventDefault();
//            if (!self.isRTL()) { self.openDrawer(); }
//          });
         

                //************************************************//
  
//             self.refreshView = ko.computed(function () {
//            if (self.refreshViewForLanguage()) {
//               
//            }
//        });

//


           
//            setInterval(self.TranslationSqLite,3000);
    
    var x=5;
    console.log(x);

self.user11=ko.observable(localStorage.getItem("UserName"));
       // initTranslations();
                self.copyLbl=ko.observable();
                self.PasteLbl=ko.observable();
                self.LovLbl=ko.observable();
                self.GenerateLpnLbl=ko.observable();
                self.ShareLbl=ko.observable();
                self.AddFavLbl=ko.observable();
                self.DeleteLbl=ko.observable();
                self.SubmitForApproveLBL=ko.observable();
                self.MSCALbl=ko.observable();
                self.ChooseRespLbl=ko.observable();
                self.LogoutLbl=ko.observable();
                self.FavoriteLbl=ko.observable(); 
                
                
                
                self.transmain=function(){
                self.languageSwitchLbl(getTranslation("main.language"));
                self.copyLbl(self.getTranslationMethod("copyLbl"));
                self.PasteLbl(self.getTranslationMethod("PasteLbl"));
                self.LovLbl(self.getTranslationMethod("LovLbll"));
                self.GenerateLpnLbl(self.getTranslationMethod("GenerateLpnLbl"));
                self.ShareLbl(self.getTranslationMethod("ShareLbl"));
                self.AddFavLbl(self.getTranslationMethod("AddFavLbl"));
                self.DeleteLbl(self.getTranslationMethod("DeleteLbl"));
                self.SubmitForApproveLBL(self.getTranslationMethod("SubmitForApproveLBL"));
                self.MSCALbl(self.getTranslationMethod("MSCALbl"));
                self.ChooseRespLbl(self.getTranslationMethod("ChooseRepnsLbl"));
                self.LogoutLbl(self.getTranslationMethod("LogoutLbl"));
                self.FavoriteLbl(self.getTranslationMethod("FavoriteLbl"));
                }
                
                
                
                
//                self.getTranslationMethod("copyLbl", label => {
//                        self.copyLbl(label);
//                    });
//                    self.getTranslationMethod("PasteLbl", label => {
//                        self.PasteLbl(label);
//                    });
//                    self.getTranslationMethod("LovLbl", label => {
//                        self.LovLbl(label);
//                    });
//                    self.getTranslationMethod("GenerateLpnLbl", label => {
//                        self.GenerateLpnLbl(label);
//                    });
//                    self.getTranslationMethod("ShareLbl", label => {
//                        self.ShareLbl(label);
//                    });
//                    self.getTranslationMethod("AddFavLbl", label => {
//                        self.AddFavLbl(label);
//                    });
//                    self.getTranslationMethod("DeleteLbl", label => {
//                        self.DeleteLbl(label);
//                    });
//                    self.getTranslationMethod("SubmitForApproveLBL", label => {
//                        self.SubmitForApproveLBL(label);
//                    });
//                    
//                    
//                     self.getTranslationMethod("MSCALbl", label => {
//                        self.MSCALbl(label);
//                    });
//                    self.getTranslationMethod("ChooseRespLbl", label => {
//                        self.ChooseRespLbl(label);
//                    });
//                    self.getTranslationMethod("LogoutLbl", label => {
//                        self.LogoutLbl(label);
//                    });
//                   
//                    self.getTranslationMethod("FavoriteLbl", label => {
//                        self.FavoriteLbl(label);
//                    });
                
            }
               
            return new ControllerViewModel();
        }
);
