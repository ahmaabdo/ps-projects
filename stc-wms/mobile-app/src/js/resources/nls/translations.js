define({
    "root": {
        "main": {
            "appname": "Ghabbour",
            "signIn": "Sign in",
            "signUp": "Sign up",
            "faq": "FAQ",
            "ourLocation": "Our locations",
            "offers": "Offers",
            "userName": "User name",
            "password": "Passowrd",
            "confirmPassword": "Confirm Password",
            "email": "Email Adrress",
            "forgetPassword": "Forget password?",
            "haveAccount": "Don't have account?",
            "createNewAccount": "Create new account",
            "firstName": "First name",
            "lastName": "Last name",
            "mobileNumber": "Mobile number",
            "chassisNumber": "SV30-0169266",
            "createAccount": "Create account",
            "cars":"Cars",
            "app":"App",
            "complain":"Complain",
            "inquiry":"Inquiry",
            "service":"Service",
            "maintenance":"Maintenance",
            "vehicle": "Vehicle",
            "search": "Search",
            "add":"Add",
            "history":"History",
            "ghabbourlocation": "Ghabbour auto location",
            "city": "City",
            "location": "Location",
            "branch": "Branch",
            "placeHolder":"Please select value",
            "testDrive":"Test drive",
            "reservationPayment":"Reservation payment",
            "vehicleHistory":"Vehicle history",
            "promotions":"Promotions",
            "submit":"Submit",
            "language":"العربية",
            "logout":"Logout"
        },
        "maintenance":{
            "upcoming":"Upcoming",
            "past":"Past",
            "owner":"Amr mohammed",
            "serviceCenter":"October",
            "date":"14/11/2019",
            "carModel":"Car model",
            "carType":"Car type",
            "maintenanceType":"Maintenance type",
            "maintenanceInfo":"Maintenance info",
            "carInfo":"Car info",
            "customerInfo":"Customer info",
            "bookMaintenance":"Book maintenance",
            "book":"Book",
            "nearestLocation":"Reserve maintenance to the nearest location of yours.",
            "add":"Add"
        },
        "complain":{
            "complainType":"Complaint Type",
            "complaintCategory":"Complaint Category",
            "send":"Send",
            "complaintLetter":"Complain Letter"
        },"inquiry":{
            "inquiryType": "Inquiry Type",
            "pcOrBajaj": "Inquiry Category",
            "inquiryDetail": "Inquiry Detail"
        },"service":{
            "serviceType": "Service Type",
            "rsaLocation": "Enter RSA Location",
            "serviceCategory": "Services Category",
            "serviceDetail": "Service Detail"
        },
        "testDrive":{
            "title":"The request will be sent to call center to reserve the test drive and we contact you soon.",
            "location":"Location",
            "preferredDate":"Preferred date",
            "test":"Test",
            "requetInfo":"Request info"
        },
        "cars":{
           "itemsAsOperation":"Items as operation:",
           "dateOfVisit":"Date of visit:",
           "comments":"Comments:"
        },
        "resetPassword":{
            "confirmEmail":"Confirm Email Address",
            "codeLbl":"Code",
            "code":"Enter Code",
            "newPass":"New Password",
            "confirmPass":"Confirm Password",
            "reset":"Reset Password"
        },
        "profile":{
            "title":"Account Settings",
            "name":"Name : ",
            "email":"Email : ",
            "chassisNumber":"Chassis Number : ",
            "phone":"Mobile Number : ",
            "edit":"Edit",
            "changePass":"Change Password",
            "back":"Back",
            "firstName":"First Name",
            "lastName":"Last Name",
            "newPass":"New Password",
            "confirmPass":"Confirm Password",
            "currentPass":"Current Password",
            "image":"Choose a profile Picture"
        }

    },
    "ar": true
});
