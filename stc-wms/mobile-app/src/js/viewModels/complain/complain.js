define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 
    'util/commonhelper', 'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojpagingdataproviderview', 'ojs/ojvalidationgroup', 
    'ojs/ojpagingcontrol', 'ojs/ojlabel', 
    'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox'],
        function (oj, ko, app, services, commonhelper, moduleUtils, ArrayDataProvider, PagingDataProviderView) {

            function ComplainViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.label = ko.observableArray([]);
                self.dataprovider = ko.observableArray([]);
                self.pagingDataProvider = new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.dataprovider));
                self.drink = ko.observable(["Add"]);
                self.drinkValues = [];
                self.groupValid = ko.observable();
                self.fieldsOption = {
                    secondValueDisplay: ko.observable(false),
                    secondValueRequired: ko.observable(false)
                };
                self.fieldsValue = {
                    firstValue: ko.observable(''),
                    firstValueArr: ko.observableArray([]),
                    secondValue: ko.observable(''),
                    secValueArr: ko.observableArray([]),
                    complaintLetter: ko.observable("")
                };


                self.connected = function () {
                    initTranslations();

                    //Get complain LOVs data
                    services.getGeneric("complainService/complainsType").then(data => {
                        self.fieldsValue.firstValueArr([]);
                        for (var i in data)
                            self.fieldsValue.firstValueArr.push({id: i, value: data[i].lookupName, label: data[i].lookupName, dependsObj: data[i].dependsObj});
                    }, app.failCbFn);

                };

                ko.computed(p => {
                    if (app.userData()) {
                        self.dataprovider([]);
                        setTimeout(p => {
                            self.dataprovider(app.userData().complaintData);
                            self.pagingDataProvider = new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.dataprovider));
                        }, 0);
                    }
                });

                // Header Config
                self.headerConfig = ko.observable({'view': [], 'viewModel': null});
                moduleUtils.createView({'viewPath': 'views/header.html'}).then(function (view) {
                    self.headerConfig({'view': view, 'viewModel': new app.getHeaderModel()});
                });

                /************* button functions ************/
                self.submitAction = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var payload = {
                        "personId": 14,
                        "firstValue": self.fieldsValue.firstValue(),
                        "secValue": self.fieldsValue.secondValue(),
                        "detailsValue": self.fieldsValue.complaintLetter()
                    };

                    services.postGeneric("complainService", JSON.stringify(payload)).then(data => {
                        if (data.status == 'done') {
                            app.createMessage("confirmation", "Complain submitted successfully!");
                            //Refresh/Get all user complains here...
                            app.getUserData();
                        } else {
                            app.failCbFn();
                        }
                    }, app.failCbFn);
                };

                self.selectComplainHandler = function (event) {
                    var selectedVal = event['detail'].value;
                    var firstValSelected = self.fieldsValue.firstValueArr().find(p => p.value == selectedVal);
                    if (firstValSelected) {
                        var dependsObj = firstValSelected.dependsObj;
                        self.fieldsValue.secValueArr([]);
                        self.fieldsOption.secondValueRequired("");
                        if (dependsObj) {
                            self.fieldsOption.secondValueRequired(true);
                            self.fieldsOption.secondValueDisplay(true);
                            for (var i in dependsObj)
                                self.fieldsValue.secValueArr.push({value: dependsObj[i].lookupName, label: dependsObj[i].lookupName});
                        } else {
                            self.fieldsOption.secondValueRequired(false);
                            self.fieldsOption.secondValueDisplay(false);
                        }
                    }
                };
                //self.pagingDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.dataprovider));

                /***************** refresh for language ******************/
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                /**********************************************************/

                function initTranslations() {
                    self.label = {
                        complainType: ko.observable(getTranslation('complain.complainType')),
                        secValueLbl: ko.observable(getTranslation('complain.complaintCategory')),
                        complaintLetter: ko.observable(getTranslation('complain.complaintLetter')),
                        submit: ko.observable(getTranslation("main.submit")),
                        addLbl: ko.observable(getTranslation("main.add")),
                        historyLbl: ko.observable(getTranslation("main.history"))
                    };
                    self.drinkValues = [
                        {id: ['Add'], label: self.label.addLbl},
                        {id: 'History', label: self.label.historyLbl}
                    ];
                }
            }

            return new ComplainViewModel();
        });
