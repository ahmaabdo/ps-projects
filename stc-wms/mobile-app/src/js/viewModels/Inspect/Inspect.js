define(['ojs/ojcore', 'knockout', 'ojs/ojbootstrap', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton','ojs/ojprogress'],
        function (oj, ko, Bootstrap, app, moduleUtils) {

            function IncpectContentViewModel() {
                var self = this;


                /**********************************************************/
self.progressValue = ko.observable(0);
                self.express_receipt = function () {
 
                    oj.Router.rootInstance.go('expressInspect');
                };
                self.normal_receipt = function () {
                    
                    oj.Router.rootInstance.go('normalInspect');

                };

//                
//                self.BackBtn=function(){
//                     oj.Router.rootInstance.go('PO_INQUIRER');
//                };
                
                  self.connected = function () {
                      initTranslations();
                  app.showHeader("on");
                  var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                    console.log("locsl",localStorage.getItem("PageValue"));
                     
                 
                };

              self.disconnected = function () {
                    // Implement if needed
                 
                };
                //***************define translation label******************//
                
                self.ExpressLBL=ko.observable();
                self.NormalLBL=ko.observable();
                
//                self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        initTranslations();
//                    }
//                });
                
                function initTranslations() {
//                    app.getTranslationMethod("ExpressLBL", label => {
//                        self.ExpressLBL(label);
//                    });
//                    app.getTranslationMethod("NormalLBL", label => {
//                        self.NormalLBL(label);
//                    });
                   
                    self.ExpressLBL(app.getTranslationMethod("ExpressLBL")); 
                   self.NormalLBL(app.getTranslationMethod("NormalLBL")); 
                }   
                

 
                
                
                
                //***************define translation label******************//
                
               
//                
                
            }

            return new IncpectContentViewModel();
        }
);