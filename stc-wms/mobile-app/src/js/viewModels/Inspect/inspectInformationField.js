/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * inspect_information_field module
 */
define(['ojs/ojcore', 'knockout', 'util/commonhelper', 'config/services','jquery' ,'ojs/ojbootstrap', 'appController',
    'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojbutton','ojs/ojprogress'
], function (oj, ko, commonUtil, services, $,Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function inspect_information_fieldContentViewModel() {
        var self = this;
      var arrfrom=[];

        self.isDisabled = ko.observable(true);

self.rej_reason_arr=ko.observableArray();
//
app.selectItemTOClipboardCombo();

        self.disconnected = function () {
            // Implement if needed

        };
        self.connected = function () {
            initTranslations();
            app.showHeader("on");
            var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                   GetRejReason();
           
        };

        //************************Add Insepct Normal**********************************//
        self.informationInspectModel = {
            poNumber: ko.observable(),
            vendor: ko.observable(),
            item: ko.observable(),
            location: ko.observable(),
            qty: ko.observable(),
            rejreason: ko.observable(),
            rejnotes:ko.observable(),
            
        };

        
             function formatDateToday() {
                    var d = new Date(),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [day, month, year].join('-');
                }; 
        self.informationInspectModel.poNumber(localStorage.getItem('ponumqr'));
        self.informationInspectModel.vendor(localStorage.getItem('vendorqr'));
        self.informationInspectModel.location(localStorage.getItem('locationqr'));
        self.informationInspectModel.item(localStorage.getItem('itemqr'));
        self.informationInspectModel.qty(localStorage.getItem('qtyqr'));
        
        //************************Add Insepct Normal**********************************//
        self.AddInsepctExpress = function () {
            var userId=localStorage.getItem("UserId");
            
              var payload = {
                "supplier":localStorage.getItem("vendoridqr"), 
                "poNumber": self.informationInspectModel.poNumber(),
                "lineNumber":localStorage.getItem("linenumqr"),
                "description": localStorage.getItem("itemidqr"),   
                "lpn":localStorage.getItem('lpnqr'),
                "accLpn":localStorage.getItem('lpnqr'),
                "location":localStorage.getItem('locationidqr'),  
                "qty": self.informationInspectModel.qty(),
                "uom":  localStorage.getItem('uomqr'),
                "accQty":self.informationInspectModel.qty(),
                "rejQty":"0",
                "transactionType": "INSPECT",
                "createdBy":userId,
                "creatationDate": formatDateToday(),
                "lastUpdatedDate": formatDateToday(),
                "lastUpdatedBy":userId,
               "transactionDate":formatDateToday(),
                "orgId":localStorage.getItem("OrganizationId"),
                "qrText":localStorage.getItem("qrtext"),
                "serialArr":arrfrom
               
            };
            var InsepctNormalCbFn = function (data) {
                        var x=data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"];
                        console.log("x",x);
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                           
                             var InspectexpressCbFn = function (data) {
                        var x2=data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"];
                        console.log("x2",x2);
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                            app.createMessage("sucess",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            $("#loader").hide();
                            self.DataToShare();
                            
                        }
                        else
                         app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                        $("#loader").hide();
                    };
                    console.log("newpaylod",payload);
                    services.addGeneric(commonUtil.AddPurchasing,JSON.stringify(payload)).then(InspectexpressCbFn, app.failCbFn);
                   

                }else{
                    app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    console.log("false");
                    $("#loader").hide();
                }
                
                
            };
           
                 console.log("payload",payload);
            services.addGeneric("purchasing/ValidateCreate", JSON.stringify(payload)).then(InsepctNormalCbFn, app.failCbFn);
        };
        //************************************************************************//  

//        self.BackBtn = function () {
//            oj.Router.rootInstance.go('expressInspect');
//        };

self.progressValue = ko.observable(0);

        self.rejectBtn = function () {

        };
        self.AcceptBtn = function () {
             $("#loader").show();
               
            self.AddInsepctExpress();
            
        };
        
        self.DataToShare=function(){
            
            oj.Router.rootInstance.store({action: "express_share"});
            localStorage.setItem("ponum", self.informationInspectModel.poNumber());
            localStorage.setItem("vendor", self.informationInspectModel.vendor());
            localStorage.setItem("location", self.informationInspectModel.item());
            localStorage.setItem("item", self.informationInspectModel.qty());
            localStorage.setItem("qty", self.informationInspectModel.location());
            oj.Router.rootInstance.go('inspectInformation');
            
            
        };

 function GetRejReason(){
            
            var RejReasonCBFN=function(data){
                for(var i=0;i<data["get reassign users"].length;i++){
                    
                    self.rej_reason_arr.push({
                        
                          "value":data["get reassign users"][i].REASON_ID,"label":data["get reassign users"][i].REASON_NAME
                       
                    });
                  
                };
              
            };
         
            services.getGeneric("MainServices/getRejReason").then(RejReasonCBFN,app.failCbFn);
        };

//        $(document).ready(function () {
//            $("#rejqty_id").keypress(function () {
//
//                document.getElementById("reason_id").style.display = "block";
//                document.getElementById("note_id").style.display = "block";
//            });
//          
//        });
        //   DEFINE TRANSLATION
        self.PoNumLbl = ko.observable();
        self.vendorLBL = ko.observable();
        self.ItemLBL = ko.observable();
        self.LocationLBL = ko.observable();
        self.SubmitForApproveLBL = ko.observable();
        self.QtyLBL = ko.observable();
        self.InspectAcceptLBL = ko.observable();
        self.InspectRejectLBL = ko.observable();
        self.rejreasonLbl = ko.observable();
        self.rejnotesLbl=ko.observable();

//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });
        function initTranslations() {
//            app.getTranslationMethod("PoNumLbl", label => {
//                self.PoNumLbl(label);
//            });
//            app.getTranslationMethod("vendorLBL", label => {
//                self.vendorLBL(label);
//            });
//            app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//            app.getTranslationMethod("QtyLBL", label => {
//                self.QtyLBL(label);
//            });
//            app.getTranslationMethod("LocationLBL", label => {
//                self.LocationLBL(label);
//            });
//            app.getTranslationMethod("SubmitForApproveLBL", label => {
//                self.SubmitForApproveLBL(label);
//            });
//            app.getTranslationMethod("InspectAcceptLBL", label => {
//                self.InspectAcceptLBL(label);
//            });
//            app.getTranslationMethod("InspectRejectLBL", label => {
//                self.InspectRejectLBL(label);
//            });
//            app.getTranslationMethod("rejreasonLbl", label => {
//                self.rejreasonLbl(label);
//            });
//            app.getTranslationMethod("rejnotesLbl", label => {
//                self.rejnotesLbl(label);
//            });
           
            self.PoNumLbl(app.getTranslationMethod("PoNumLbl"));
            self.vendorLBL(app.getTranslationMethod("vendorLBL"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.QtyLBL(app.getTranslationMethod("QtyLBL"));
            self.LocationLBL(app.getTranslationMethod("LocationLBL"));
            self.SubmitForApproveLBL(app.getTranslationMethod("SubmitForApproveLBL"));
            self.InspectAcceptLBL(app.getTranslationMethod("InspectAcceptLBL"));
            self.InspectRejectLBL(app.getTranslationMethod("InspectRejectLBL"));
            self.rejreasonLbl(app.getTranslationMethod("rejreasonLbl"));
            self.rejnotesLbl(app.getTranslationMethod("rejnotesLbl"));
        }

        //************************************************************************//  
    }

    return inspect_information_fieldContentViewModel;
});
