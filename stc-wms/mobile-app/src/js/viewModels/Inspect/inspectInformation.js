define(['ojs/ojcore', 'knockout', 'ojs/ojbootstrap', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton','ojs/ojprogress'
], function (oj, ko, Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function deliver_infoContentViewModel() {
        var self = this;
        var ponum;
        var vendor;
        var location;
        var lpn;
        var item;
        var desc;
        var uom;
        var qty;
        var acclpn;
        var accqty;
        var rejqty;
        var from;
        var to;
        var text;
        var data;
                



        self.share = function () {
            navigator.share(text);
        };
self.progressValue = ko.observable(0);
        self.return_to_menu = function () {
            
            oj.Router.rootInstance.go('Inspect');
        };
app.selectItemTOClipboardCombo();
//
//        self.BackBtn = function () {
//            window.history.back();
//        };
        self.connected = function () {
             initTranslations();
                  app.showHeader("on");
                  var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                    
                 
                
            data = oj.Router.rootInstance.retrieve();
            if (data.action === 'express_share')
            {

                ponum = localStorage.getItem("ponum");
                vendor = localStorage.getItem("vendor");
                location = localStorage.getItem("location");
                item = localStorage.getItem("item");
                qty = localStorage.getItem("qty");

                text = "PO Number: " + ponum + "\nVendor: " + vendor + "\nItem: " + item + "\nQuantity: " + qty +
                        "\nLocation: " + location;
            } else if (data.action === 'manual_share')
            {
                lpn = localStorage.getItem('lpn');
                item = localStorage.getItem('item');
                desc = localStorage.getItem('desc');
                //shipment = localStorage.getItem('shipment');
                uom = localStorage.getItem('uom');
                qty = localStorage.getItem('qty');
                acclpn = localStorage.getItem('acclpn');
                accqty = localStorage.getItem('acceptqty');
                rejqty = localStorage.getItem('rejectqty');
                from = localStorage.getItem('From');
                to = localStorage.getItem('To');
                text = "LPN: " + lpn + "\nItem: " + item + "\nDescription: " + desc + "\nUOM: " + uom +
                        "\nQuantity: " + qty + "\nAcc Lpn: " + acclpn + "\nAcc Qty:" + accqty + "\nRej Qty:" + rejqty +
                        "\nSerial Information" + "\nFrom: " + from + "\nTo: " + to;

            }
        };
     self.disconnected = function () {
                    // Implement if needed
                  
                };


        //  define translation  
        self.ShareLBL = ko.observable();
        self.ReturnToMenuLBL = ko.observable();

//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });

        function initTranslations() {
//            app.getTranslationMethod("ShareLBL", label => {
//                self.ShareLBL(label);
//            });
//            app.getTranslationMethod("ReturnToMenuLBL", label => {
//                self.ReturnToMenuLBL(label);
//            });
            self.ShareLBL(app.getTranslationMethod("ShareLBL"));
            self.ReturnToMenuLBL(app.getTranslationMethod("ReturnToMenuLBL"));
        }

    }

    return deliver_infoContentViewModel;
});
