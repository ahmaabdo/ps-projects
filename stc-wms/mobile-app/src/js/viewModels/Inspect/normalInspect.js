/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * normal_inspect module
 */
define(['ojs/ojcore', 'knockout','util/commonhelper','config/services','jquery' ,'ojs/ojbootstrap', 'appController',
    'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojbutton','ojs/ojdialog','ojs/ojselectcombobox','ojs/ojvalidationgroup', 'ojs/ojprogress'
], function (oj, ko,commonUtil,services,$,Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function normal_inspectContentViewModel() {
        var self = this;
        var selectFORcoy;
        var selectFORpaste;
        var selectFORcoy;
        var selectFORpaste;
        var result;
        var quantityNo;
        var NoOFEnter=0;
        var remain;
        var arrfrom = [];
        self.isvisBtnSerial = ko.observable(true);
        self.isvisBtnDone = ko.observable(false);
        self.checkSerialized=ko.observable();
        //****************define array******************//
          self.LPN_Arr=ko.observableArray();
          self.AccLpn_Arr=ko.observableArray();
          self.rej_reason_arr=ko.observableArray();
app.selectItemTOClipboardCombo();
       app.selectItemTOClipboardInputText();

        self.enter_to_serial = function () {
          self.normalInsepctModel.serialFrom("");
            var valid = self._checkValidationGroup();
            if (valid) {
                // submit the form would go here
                
                document.querySelector("#yesNoDialog").open();
            }
            };
            
            self.BackBtn=function(){
                     oj.Router.rootInstance.go('Inspect');
            };
            
             self.closeBtn=function(){
                 
                 document.querySelector("#yesNoDialog").close();
             };
          
            //************ Changed  Handler
            self.LPNChangedHandler = function (event) {
                self.normalInsepctModel.item("");
                self.normalInsepctModel.description("");
                self.normalInsepctModel.uom("");
                self.normalInsepctModel.qty("");
            var poLineId = event.detail.value;
            if(poLineId !=undefined && poLineId !=""){
             getDescription(poLineId);
             GetUom(poLineId);
             GetQuantity(self.LPN_Arr().find(e => e.value == poLineId).label);
         }
             
             };
            self.AccLpnChangedHandler=function(){
                
            };
     
             self.normalInsepctModel = {
                        rejQty: ko.observable(),
                        accQty: ko.observable(),
                        accLpn: ko.observable(),
                        item: ko.observable(),
                        description: ko.observable(),
                        itemAndDescId: ko.observable(),
                        lpn: ko.observable(),
                        lpnval:ko.observable(),
                        uom: ko.observable(),
                        qty: ko.observable(),
                        serialFrom: ko.observable(),
                       
                        rejreason:ko.observable(),
                        rejnotes:ko.observable(),
                        supplierValue:ko.observable()
                        
                       
                    };
                                 
        function formatDateToday() {
            var d = new Date(),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('-');
        }
        ;     
                    
        
        //**************ADD INSPECT EXPRESS **********************************//
         self.AddInsepctNormal=function(){
             var userId=localStorage.getItem("UserId");
            var payload = {
                //"lpn":self.LPN_Arr().find(e => e.value == self.normalInsepctModel.lpn()).LPN,
                //"supplier":"18614",
                "lpn":self.LPN_Arr().find(e => e.value == self.normalInsepctModel.lpn()).label,
                "item": self.normalInsepctModel.itemAndDescId(),
                "description": self.normalInsepctModel.itemAndDescId(),
                "uom":self.normalInsepctModel.uom(),
                "qty":self.normalInsepctModel.qty(),
                "accLpn":self.normalInsepctModel.accLpn(),
                "poNumber": self.LPN_Arr().find(e => e.value == self.normalInsepctModel.lpn()).PO_NUM,
                "lineNumber": self.LPN_Arr().find(e => e.value == self.normalInsepctModel.lpn()).PO_LINE, 
                "accQty":self.normalInsepctModel.accQty(),
                "rejQty":self.normalInsepctModel.rejQty(),
                "transactionType": "INSPECT",
                "createdBy": userId,
                "creatationDate": formatDateToday(),
                "lastUpdatedDate": formatDateToday(),
                "transactionDate":formatDateToday(),
                "lastUpdatedBy": userId,
                "serialArr":arrfrom,
                "rejReason":self.normalInsepctModel.rejreason(),
                "rejNotes":self.normalInsepctModel.rejnotes(),
                "orgId":localStorage.getItem("OrganizationId"),
                "supplier":localStorage.getItem("supplier")
            };
            console.log(payload);
            var InspectValidateCbFn = function (data) {
                   if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S") {
                    var InspectNormalCbFn = function (data) {                        
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                            app.createMessage("sucess",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            document.querySelector("#yesNoSubmitReceiptDialog").close();
                            self.DataToShare(); 
                        }
                    };
                    services.addGeneric(commonUtil.AddPurchasing ,JSON.stringify(payload)).then(InspectNormalCbFn, app.failCbFn);

                }else{
                    app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    document.querySelector("#yesNoSubmitReceiptDialog").close();
                }              
            };
            services.addGeneric("purchasing/ValidateCreate", JSON.stringify(payload)).then(InspectValidateCbFn, app.failCbFn);
         };   
      //************************************************************************//  
      $(document).ready(function () {
                    $("oj-input-text").click(function () {
                        selectFORcoy = this.value;
                        selectFORpaste = this;
                    });
                    });

        
        self.DataToShare=function(){
            
            oj.Router.rootInstance.store({action: "manual_share"});
            localStorage.setItem("lpn", self.normalInsepctModel.lpn());
            localStorage.setItem("item", self.normalInsepctModel.item());
            localStorage.setItem("desc", self.normalInsepctModel.description());
            localStorage.setItem("uom", self.normalInsepctModel.uom());
            localStorage.setItem("qty", self.normalInsepctModel.qty());
            localStorage.setItem("acclpn", self.normalInsepctModel.accLpn());
            localStorage.setItem("acceptqty", self.normalInsepctModel.accQty());
            localStorage.setItem("rejectqty", self.normalInsepctModel.rejQty());
            localStorage.setItem("From", self.normalInsepctModel.serialFrom());
            
            document.querySelector("#yesNoDialog").close();
            oj.Router.rootInstance.go('inspectInformation');  
        };
       
        
        
        
         self.groupvalid = ko.observable();
         self.groupvalid1 = ko.observable();
        self._checkValidationGroup = function () {
            var tracker = document.getElementById("tracker");
            //var tracker1 = document.getElementById("tracker1");
            if (tracker.valid === "valid")
            {
                
                if(parseInt(self.normalInsepctModel.accQty())+parseInt(self.normalInsepctModel.rejQty())<=parseInt(self.normalInsepctModel.qty())){
                   return true; 
                }else{
                    app.createMessage("error","ACC Qty and Rej Qty must be less than Qty");
                }
                
                
            } else {

                tracker.showMessages();
                return false;
            }
        };
        
          self.closedialog = function () {
            result = 0;
            NoOFEnter=0;
            self.remain("");
            document.querySelector("#yesNoDialog").close();


        };
        
         self.remain = ko.observable();
        self.AddSerialNumber = function () {
            var valid = self._checkValidationGroup();
            if (valid) {
                quantityNo = Number(self.normalInsepctModel.qty());
                arrfrom[NoOFEnter] = self.normalInsepctModel.serialFrom();
                console.log(arrfrom[NoOFEnter]);
                NoOFEnter += 1;
                result = quantityNo - NoOFEnter;
                self.remain(result);
                if (result < quantityNo && result != 0) {
                    document.querySelector("#yesNoDialog").close();
                    self.normalInsepctModel.serialFrom("");
                    self.enter_to_serial();

                } else if (result == 0){
                    document.querySelector("#yesNoDialog").close();
                self.isvisBtnSerial(false);
                self.isvisBtnDone(true);
            }
                //self.AddRecieiptNormal();
            }
            console.log($('#po_num option:combobox').text());
        };
        
        
            //**************submit receipt ****************************//
        self.SubmitInspect=function(){
             document.querySelector("#yesNoSubmitReceiptDialog").open();
        };
        self.yesSubmitInspect=function(){
            self.AddInsepctNormal();
        };
        self.NOSubmitInspect=function(){
            document.querySelector("#yesNoSubmitReceiptDialog").close();
        };
        
        
        
        
        $(document).ready(function(){
            $("#rejqty_id").keypress(function(){
                
                    document.getElementById("reason_id").style.display="block";
                    document.getElementById("note_id").style.display="block";
            });
          
        });
        
        
        
        
        //***************get Lpn inspect *******************//
        self.getLpnInspect=function() {
            self.LPN_Arr([]);
            var getAllLpnInspectdata = function (data) {
                console.log(data);
               for (var i = 0; i < data.LPNInspect.length; i++) {
                    self.LPN_Arr.push({
                        "value": data.LPNInspect[i].PO_LINE_ID,
                        "label": data.LPNInspect[i].LPN,
                        "PO_NUM":data.LPNInspect[i].PO_NUM ,
                        "PO_HEADER_ID":data.LPNInspect[i].PO_HEADER_ID,
                        "PO_LINE_ID":data.LPNInspect[i].PO_LINE_ID,
                        "PO_LINE":data.LPNInspect[i].PO_LINE,
                    });
                    self.AccLpn_Arr.push({
                        "value": data.LPNInspect[i].PO_LINE_ID,
                        "label": data.LPNInspect[i].LPN
                    });
                }
            };
            services.getGeneric("MainServices/LPNInspect?orgId="+localStorage.getItem("OrganizationId")).then(getAllLpnInspectdata, app.failCbFn);
        };
        
           //*********************get itemDescription*****************//
        function  getDescription(po_header_id) {
            self.normalInsepctModel.item("");
            self.normalInsepctModel.description("");
            var ItemDescriptiondata = function (data) {


                for (var i = 0; i < data.ItemDescription.length; i++) {

                    self.normalInsepctModel.item(data.ItemDescription[i].ITEM_CODE);
                    self.normalInsepctModel.description(data.ItemDescription[i].ITEM_DESCRIPTION);
                    self.normalInsepctModel.itemAndDescId(data.ItemDescription[i].ITEM_ID);

                }
                
                GetSerializedFlag(self.normalInsepctModel.itemAndDescId());
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/ItemDescription?po_header_id=" + po_header_id).then(ItemDescriptiondata, app.failCbFn);
            
        }
        
       //*********************get GetUom*****************//
        function  GetUom(po_header_id) {
            self.normalInsepctModel.uom("");
            var UomCbFn = function (data) {


                for (var i = 0; i < data.Uom.length; i++) {
                   self.normalInsepctModel.uom(data.Uom[i].UOM);
                }
               
               
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/GetUom?po_line_id=" + po_header_id).then(UomCbFn, app.failCbFn);
        } 
        
         //*********************get GetQuantity*****************//
        function  GetQuantity(quantityByLpn) {
            var QuantityCbFn = function (data) {

                   console.log(data);
                for (var i = 0; i < data.QuantityByLpn.length; i++) {

                    self.normalInsepctModel.qty(data.QuantityByLpn[i].ITEM_QTY);

                }
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/QuantityByLpn?QuantityByLpn="+quantityByLpn).then(QuantityCbFn, app.failCbFn);
        }
         //****************************get flag to check serialized or not serialized**********************// 
        function  GetSerializedFlag(itemId) {
            
            var checkSerializedFlagCbFn = function (data) {
                for (var i = 0; i < data.checkSerializedFlag.length; i++) {
                    self.checkSerialized(data.checkSerializedFlag[i].SERIALIZED_FLAG);
                }
                 if(self.checkSerialized()!= "Y"){
                     self.isvisBtnSerial(false);
                     self.isvisBtnDone(true);
                }
                $("#loader").hide();          
            };
            services.getGeneric("MainServices/checkSerializedFlag?orgId="+localStorage.getItem("OrganizationId")+"&itemId="+itemId).then(checkSerializedFlagCbFn, app.failCbFn);
        }
        
        //*** reject reason**///
         function GetRejReason(){
            
            var RejReasonCBFN=function(data){
                console.log("dd",data);
                for(var i=0;i<data["get reassign users"].length;i++){
                    
                    self.rej_reason_arr.push({
                        
                        "value":data["get reassign users"][i].REASON_ID,"label":data["get reassign users"][i].REASON_NAME
                       
                    });
                  
                };
              
            };
         
            services.getGeneric("MainServices/getRejReason").then(RejReasonCBFN,app.failCbFn);
        };
        
        
          self.disconnected = function () {
            // Implement if needed
           };
           self.connected=function(){
               initTranslations();
               app.showHeader("on");
               self.getLpnInspect();
               var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                     GetRejReason();
             

           };
           
           
        // define translation label
             self.LPNLBL=ko.observable();
             self.ItemLBL=ko.observable();
             self.DescLBL=ko.observable();
             self.UOMLBL=ko.observable();
             self.QtyLBL=ko.observable();
             self.AddSerialLBL=ko.observable();
             self.AccLPNLBL=ko.observable();
             self.AccQtyLBL=ko.observable();
             self.RejQtyLBL=ko.observable();
             self.EnterSerialHeader=ko.observable();
             self.DoneLBL=ko.observable();
             self.EnterSerialLBL=ko.observable();
            self.FromLBL=ko.observable();
            self.TOLBL=ko.observable();
             self.RemainingLbl=ko.observable();
            self.SubmitForApproveLBL = ko.observable(); 
            self.SubmitReceitLBL = ko.observable();
            self.confirmationMessageHeader = ko.observable();
            self.ConfirmationMessageBody = ko.observable();
            self.yesLbl = ko.observable();
            self.NOLbl = ko.observable(); 
            self.rejreasonLbl=ko.observable();    
                self.rejnotesLbl=ko.observable();
//             self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        initTranslations();
//                    }
//                });
                
           function initTranslations() {
//                app.getTranslationMethod("LPNLBL", label => {
//                self.LPNLBL(label);
//            });
//            app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//            app.getTranslationMethod("DescLBL", label => {
//                self.DescLBL(label);
//            });
//            app.getTranslationMethod("UOMLBL", label => {
//                self.UOMLBL(label);
//            });
//            app.getTranslationMethod("QtyLBL", label => {
//                self.QtyLBL(label);
//            });
//            app.getTranslationMethod("AddserialNumber", label => {
//                self.AddSerialLBL(label);
//            });
//            app.getTranslationMethod("AccLPNLBL", label => {
//                self.AccLPNLBL(label);
//            });
//            app.getTranslationMethod("AccQtyLBL", label => {
//                self.AccQtyLBL(label);
//            });
//            app.getTranslationMethod("RejQtyLBL", label => {
//                self.RejQtyLBL(label);
//            });
//            app.getTranslationMethod("serialNumber", label => {
//                self.EnterSerialHeader(label);
//            });
//
//         app.getTranslationMethod("DoneLBL", label => {
//                self.DoneLBL(label);
//            });
//            app.getTranslationMethod("EnterSerialLBL", label => {
//                self.EnterSerialLBL(label);
//            });
//            app.getTranslationMethod("FromLBL", label => {
//                self.FromLBL(label);
//            });
//            app.getTranslationMethod("TOLBL", label => {
//                self.TOLBL(label);
//            });
//            app.getTranslationMethod("RemainingLbl", label => {
//                self.RemainingLbl(label);
//            });
//            app.getTranslationMethod("SubmitForApproveLBL", label => {
//                self.SubmitForApproveLBL(label);
//            });
//            app.getTranslationMethod("SubmitReceit", label => {
//                self.SubmitReceitLBL(label);
//            });
//            app.getTranslationMethod("confirmationMessageHeader", label => {
//                self.confirmationMessageHeader(label);
//            });
//            app.getTranslationMethod("ConfirmationMessageBody", label => {
//                self.ConfirmationMessageBody(label);
//            });
//            app.getTranslationMethod("yesLbl", label => {
//                self.yesLbl(label);
//            });        
//            app.getTranslationMethod("NOLbl", label => {
//                self.NOLbl(label);
//            });
//            app.getTranslationMethod("rejreasonLbl", label => {
//                self.rejreasonLbl(label);
//            });
//            app.getTranslationMethod("rejnotesLbl", label => {
//                self.rejnotesLbl(label);
//            });        
            
            
            self.LPNLBL(app.getTranslationMethod("LPNLBL"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.DescLBL(app.getTranslationMethod("DescLBL"));
            self.UOMLBL(app.getTranslationMethod("UOMLBL"));
            self.QtyLBL(app.getTranslationMethod("QtyLBL"));
            self.AddSerialLBL(app.getTranslationMethod("AddserialNumber"));
            self.AccLPNLBL(app.getTranslationMethod("AccLPNLBL"));
            self.AccQtyLBL(app.getTranslationMethod("AccQtyLBL"));
            self.RejQtyLBL(app.getTranslationMethod("RejQtyLBL"));
            self.EnterSerialHeader(app.getTranslationMethod("serialNumber"));
            self.DoneLBL(app.getTranslationMethod("DoneLBL"));
            self.EnterSerialLBL(app.getTranslationMethod("EnterSerialLBL"));
            self.FromLBL(app.getTranslationMethod("FromLBL"));
            self.TOLBL(app.getTranslationMethod("TOLBL"));
             self.RemainingLbl(app.getTranslationMethod("RemainingLbl"));
            self.SubmitForApproveLBL(app.getTranslationMethod("SubmitForApproveLBL"));
            self.SubmitReceitLBL(app.getTranslationMethod("SubmitReceit"));
            self.confirmationMessageHeader(app.getTranslationMethod("confirmationMessageHeader"));
            self.ConfirmationMessageBody(app.getTranslationMethod("ConfirmationMessageBody"));
            self.yesLbl(app.getTranslationMethod("yesLbl"));
            self.NOLbl(app.getTranslationMethod("NOLbl"));
             self.rejreasonLbl(app.getTranslationMethod("rejreasonLbl"));
                self.rejnotesLbl(app.getTranslationMethod("rejnotesLbl"));
            
           }       
                
        
    }
    


    return normal_inspectContentViewModel;
});
