/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Receipt_serial module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojbootstrap', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojbutton'],
        function (oj, ko, Bootstrap, app, moduleUtils) {
            /**
             * The view model for the main content view template
             */
            function Receipt_serialContentViewModel() {
                var self = this;
                self.headerConfig = ko.observable({'view': [], 'viewModel': null});
                moduleUtils.createView({'viewPath': 'views/header.html'}).then(function (view) {
                    self.headerConfig({'view': view, 'viewModel': new app.getHeaderModel()});
                });
                self.from = ko.observable();
                self.to = ko.observable();


                self.serial_done = function () {
                    oj.Router.rootInstance.go('ReceiptInformation');
                };
//                self.BackBtn = function () {
//                    oj.Router.rootInstance.go('NormalReceipt');
//                };
//                self.notification = function () {};
//                self.cc = function () {};
                self.disconnected = function () {
                    // Implement if needed
//                   
                };
                self.connected = function () {
                    app.showHeader("on");
                };

            }

            return Receipt_serialContentViewModel;
        });
