/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Receipt_Information_Field module
 */
define(['ojs/ojcore', 'knockout','jquery','util/commonhelper', 'config/services', 'ojs/ojbootstrap', 'appController',
    'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojbutton','ojs/ojprogress'
], function (oj, ko, $,commonUtil, services, Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function Receipt_Information_FieldContentViewModel() {
        var self = this;

         var arrfrom=[];
        self.isDisabled = ko.observable(true);
   self.sub_inv_arr=ko.observableArray();
   self.locator_arr=ko.observableArray();


app.selectItemTOClipboardCombo();

self.subinvchangehandler=function(event){
    
  var subinvval=event.detail.value;
            GetLocator(subinvval);
    
};

        self.recieiptInformationModel = {
            poNumber: ko.observable(),
            vendor: ko.observable(),
            item: ko.observable(),
            qty: ko.observable(),
            location: ko.observable(),
            subInventory: ko.observable(),
            locator: ko.observable(),
            lpn: ko.observable(),
            poID: ko.observable(),
            locationID: ko.observable(),
            uom: ko.observable()
           
        };
        
        //*** variables ***//
        var ponumbervalqr;
        var vendorvalqr;
        var itemvalqr;
        var qtyvalqr;
        var locationvalqr;
        var subinvvalqr;
        var locator;
        
        self.disconnected = function () {
            // Implement if needed
//                   
        };
        self.connected = function () {
            initTranslations();
            //getPoNumber();
            app.showHeader("on");
            
            self.recieiptInformationModel.poNumber(localStorage.getItem('ponumqr'));
            self.recieiptInformationModel.vendor(localStorage.getItem('vendorqr'));
            self.recieiptInformationModel.location(localStorage.getItem('locationqr'));
            self.recieiptInformationModel.item(localStorage.getItem('itemqr'));
            self.recieiptInformationModel.qty(localStorage.getItem('qtyqr'));
             
            
            GetSubInv(localStorage.getItem("OrganizationId"));
            var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                     
            
        };
    
        
        function formatDateToday() {
                    var d = new Date(),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [day, month, year].join('-');
                };        
           
       
        
//        self.recieiptInformationModel.poID(localStorage.getItem('poidqr'));
//        self.recieiptInformationModel.lpn(localStorage.getItem('lpnqr'));
//        self.recieiptInformationModel.locationID(localStorage.getItem('locationidqr'));
//        self.recieiptInformationModel.uom(localStorage.getItem('uomqr'));
       
        
       
        //
        //  *****************Add Recieipt Express ************************//
        
      
        self.AddRecieiptExpress = function () {
             var userId=localStorage.getItem("UserId");
             
            var payload = {
                "supplier":localStorage.getItem("vendoridqr"), 
                "poNumber": self.recieiptInformationModel.poNumber(),
                "lineNumber":localStorage.getItem("linenumqr"), 
                "description": localStorage.getItem("itemidqr"),   
                "lpn":localStorage.getItem('lpnqr'),  
                "locator": self.recieiptInformationModel.locator(),
                "subInventory": self.recieiptInformationModel.subInventory(),    
                "location":localStorage.getItem('locationidqr'),  
                 "qty": self.recieiptInformationModel.qty(),
                "uom":  localStorage.getItem('uomqr'),
                 "transactionType": "RECEIVE",
                "createdBy":userId,
                "creatationDate": formatDateToday(),
                "transactionDate":formatDateToday(),
                "lastUpdatedDate": formatDateToday(),
                "lastUpdatedBy":userId,
                "orgId":localStorage.getItem("OrganizationId"),
                "qrText":localStorage.getItem("qrtext"),
              "serialArr":arrfrom
                
            };
            
           var RecieiptExpressCbFn = function (data) {
               
                
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                         var RecieiptNormalCbFn = function (data) {
                        
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                            app.createMessage("sucess",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            $("#loader").hide();
                            self.DataToShare();
                            
                        }
                        else{
                            app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            $("#loader").hide();
                            
                        }
                    };
                    console.log("newpaylod",payload);
                    services.addGeneric(commonUtil.AddPurchasing,JSON.stringify(payload)).then(RecieiptNormalCbFn, app.failCbFn);
                   

                }else{
                    app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    $("#loader").hide();
                }
                
                
            };
           
            console.log(payload);
           
            services.addGeneric("purchasing/ValidateCreate", JSON.stringify(payload)).then(RecieiptExpressCbFn, app.failCbFn);
          
        };
        //************************************************************************//      

       

self.progressValue = ko.observable(0);
        self.done = function () {
         
            $("#loader").show();                    
                  
            self.AddRecieiptExpress();
            
        };
        
        
         function  GetSubInv(orgId) {
            self.sub_inv_arr([]);
            var GetSubInvCBFN = function (data) {
                for (var i = 0; i < data.getSubInventory.length; i++) {
                    
                    self.sub_inv_arr.push({
                        
                        "value":data.getSubInventory[i].SUB_INVENTORY_NAME,"label":data.getSubInventory[i].SUB_INVENTORY_NAME
                        
                    });
                    
                   
                };
                       
            };
            services.getGeneric("MainServices/getSubInventory?orgId="+orgId).then(GetSubInvCBFN, app.failCbFn);
        }
        
        
        function GetLocator(sub_inv_code){
            self.locator_arr([]);
            var GetLocatorCBFN=function(data){
                
                for(var i=0;i<data.getLocator.length;i++){
                    
                    self.locator_arr.push({
                        
                        "value":data.getLocator[i].LOCATOR_ID,"label":data.getLocator[i].LOCATOR_DESC
                        
                    });
                    
                };
                
            };
            
            services.getGeneric("MainServices/getLocator?sub_inv_code="+sub_inv_code+"&orgId="+localStorage.getItem("OrganizationId")).then(GetLocatorCBFN,app.failCbFn)
        }
        
        self.DataToShare=function(){
            
            oj.Router.rootInstance.store({action: "express_share"});
            localStorage.setItem("ponum", self.recieiptInformationModel.poNumber());
            localStorage.setItem("vendor", self.recieiptInformationModel.vendor());
            localStorage.setItem("item", self.recieiptInformationModel.item());
            localStorage.setItem("qty", self.recieiptInformationModel.qty());
            localStorage.setItem("location", self.recieiptInformationModel.location());
            localStorage.setItem("sub_inv", self.recieiptInformationModel.subInventory());
            localStorage.setItem("locator", self.recieiptInformationModel.locator());
             oj.Router.rootInstance.go('ReceiptInformation');
            
            
            
        }

        //   DEFINE TRANSLATION
        self.PoNumLbl = ko.observable();
        self.vendorLBL = ko.observable();
        self.ItemLBL = ko.observable();
        self.QtyLBL = ko.observable();
        self.LocationLBL = ko.observable();
        self.EnterLocationLBL = ko.observable();
        self.SubInventoryLBL = ko.observable();
        self.LocatorLBL = ko.observable();
        self.SubmitForApproveLBL = ko.observable();
         self.subinvLbl=ko.observable();
        self.locatorLbl=ko.observable();

//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });

        function initTranslations() {
//            app.getTranslationMethod("PoNumLbl", label => {
//                self.PoNumLbl(label);
//            });
//            app.getTranslationMethod("vendorLBL", label => {
//                self.vendorLBL(label);
//            });
//            app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//            app.getTranslationMethod("QtyLBL", label => {
//                self.QtyLBL(label);
//            });
//            app.getTranslationMethod("LocationLBL", label => {
//                self.LocationLBL(label);
//            });
//            //
//             app.getTranslationMethod("EnterLocationLBL", label => {
//                self.EnterLocationLBL(label);
//            });
//            app.getTranslationMethod("SubInventoryLBL", label => {
//                self.SubInventoryLBL(label);
//            });
//            app.getTranslationMethod("LocatorLBL", label => {
//                self.LocatorLBL(label);
//            });
//            app.getTranslationMethod("SubmitForApproveLBL", label => {
//                self.SubmitForApproveLBL(label);
//            });
//            app.getTranslationMethod("subinvLbl", label => {
//                self.subinvLbl(label);
//            });
//            app.getTranslationMethod("locatorLbl", label => {
//                self.locatorLbl(label);
//            });

            self.PoNumLbl(app.getTranslationMethod("PoNumLbl"));
            self.vendorLBL(app.getTranslationMethod("vendorLBL"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.QtyLBL(app.getTranslationMethod("QtyLBL"));
            self.LocationLBL(app.getTranslationMethod("LocationLBL"));
            self.EnterLocationLBL(app.getTranslationMethod("EnterLocationLBL"));
            self.SubInventoryLBL(app.getTranslationMethod("SubInventoryLBL"));
            self.LocatorLBL(app.getTranslationMethod("LocatorLBL"));
            self.SubmitForApproveLBL(app.getTranslationMethod("SubmitForApproveLBL"));
             self.subinvLbl(app.getTranslationMethod("subinvLbl"));
             self.locatorLbl(app.getTranslationMethod("locatorLbl"));

        }
        ;
    }

    return Receipt_Information_FieldContentViewModel;
});
