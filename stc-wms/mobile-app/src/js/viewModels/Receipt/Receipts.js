/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['ojs/ojcore', 'knockout', 'ojs/ojbootstrap', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton','ojs/ojdialog','ojs/ojprogress'],
        function (oj, ko, Bootstrap, app, moduleUtils) {

            function LandingViewModel() {
                var self = this;

                /**********************************************************/
                self.BackBtn=function(){
                    oj.Router.rootInstance.go('PO_INQUIRER');
                };
     self.progressValue = ko.observable(0);
                self.express_receipt = function () {
            
                    oj.Router.rootInstance.go('ExpressReceipt');
                };
                
                self.normal_receipt = function () {
              
                    oj.Router.rootInstance.go('NormalReceipt');
   
                };
                 
               
              
                self.disconnected = function () {
                    // Implement if needed
//                   
                };
                self.connected = function () {
                    initTranslations();
                    app.showHeader("on");
                    var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                    console.log("locsl",localStorage.getItem("PageValue"));
                    
                };

                //***************define translation label******************//

                self.ExpressLBL = ko.observable();
                self.NormalLBL = ko.observable();

//                self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        initTranslations();
//                    }
//                });

                function initTranslations() {
//                    app.getTranslationMethod("ExpressLBL", label => {
//                        self.ExpressLBL(label);
//                    });
//                    app.getTranslationMethod("NormalLBL", label => {
//                        self.NormalLBL(label);
//                    });
                    self.ExpressLBL(app.getTranslationMethod("ExpressLBL"));
                    self.NormalLBL(app.getTranslationMethod("NormalLBL"));
                }



            }

            return new LandingViewModel();
        }
);

