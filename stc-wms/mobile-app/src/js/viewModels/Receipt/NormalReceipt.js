/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Normal_Receipt module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'util/commonhelper', 'config/services', 'ojs/ojbootstrap', 'appController',
    'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojbutton', 'ojs/ojdialog', 'ojs/ojvalidationgroup', 'ojs/ojprogress',
], function (oj, ko, $, commonUtil, services, Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function Normal_ReceiptContentViewModel() {
        var self = this;
        var selectFORcoy;
        var selectFORpaste;
        var result;
        var quantityNo;
        var NoOFEnter=0;
        var remain;
        var remainingQuantity;
        var arrfrom = [];
        self.isvisBtnSerial = ko.observable(true);
        self.isvisBtnDone = ko.observable(false);
        self.checkSerialized=ko.observable();
        //  add new 
                 self.po_num_Arr=ko.observableArray();
                 self.linenum_Arr=ko.observableArray();
                 self.LPN_Arr=ko.observableArray();
                 self.QtyArr=ko.observableArray();
                 self.LocationArr=ko.observableArray();
                 self.sub_inv_arr=ko.observableArray();
                 self.locator_arr=ko.observableArray();
                 
                 app.showHeader("on");
      
     
        self.PoNumChangedHandler = function (event) {
           console.log(event);
           self.normalRecieiptModel.supplier("");
           
           self.normalRecieiptModel.lineNumber("");
            var poNumberValue = event.detail.value;
            $("#loader").show();
            
            getSupplier(poNumberValue);
            getLineNumber(poNumberValue);

        };
   
        self.linenumChangedHandler = function (event) {
            
           self.normalRecieiptModel.item("");
           self.normalRecieiptModel.description("");
           self.normalRecieiptModel.lpn("");
           self.normalRecieiptModel.location("");
           self.normalRecieiptModel.uom("");
           
           
            console.log(event.detail);
            var lineNumberValue = event.detail.value;
            $("#loader").show();
            
            if (lineNumberValue != undefined && lineNumberValue!="") {
                getLocation(lineNumberValue);
                getDescription(lineNumberValue);
                getLPN(self.normalRecieiptModel.poNumber(),lineNumberValue);
                GetQuantityRemaning(self.normalRecieiptModel.poNumber(),lineNumberValue);//--------Quantity Remaning 
                GetUom(lineNumberValue);


            }

        };
         self.LPNChangedHandler = function (event) {
             
         //  self.normalRecieiptModel.qty("");
             console.log("normalRecieiptModel.lpn == "+self.normalRecieiptModel.lpn())
             console.log("qtyby lpn",event.detail.value);
            var lpnvalue=event.detail.value;
            if(lpnvalue!=undefined && lpnvalue!=""){
              //  GetQuantity(lpnvalue);
            }
            // $("#loader").show();
            
          
           
    };
        self.QtyChangedHandler = function () {};
        self.LocationChangedHandler = function () {};
        self.subinvchangehandler=function(event){
            var subinvval=event.detail.value;
            console.log(subinvval);
            GetLocator(subinvval);
            
            
        };
        

        //---------------- add serial number

        self.DataToShare = function () {
            oj.Router.rootInstance.store({action: "manual_share"});
            localStorage.setItem("ponum",self.po_num_Arr().find(e => e.value == self.normalRecieiptModel.poNumber()).label);
            localStorage.setItem("supplier", self.normalRecieiptModel.supplier());
            localStorage.setItem("linenum", self.linenum_Arr().find(e => e.value == self.normalRecieiptModel.lineNumber()).labelValue);
            localStorage.setItem("item", self.normalRecieiptModel.item());
            localStorage.setItem("desc", self.normalRecieiptModel.description());
            //localStorage.setItem("shipment", self.normalRecieiptModel.shipment_v());
            localStorage.setItem("lpn", self.LPN_Arr().find(e => e.value == self.normalRecieiptModel.lpn()).label);
            localStorage.setItem("location",self.LocationArr().find(e=>e.value==self.normalRecieiptModel.location()).label);
           
            localStorage.setItem("uom", self.normalRecieiptModel.uom());
            localStorage.setItem("qty", self.normalRecieiptModel.qty());

            localStorage.setItem("From", self.normalRecieiptModel.serialFrom());
            document.querySelector("#yesNoDialog").close();
//            app.subInventory(self.normalRecieiptModel.sub_inv());
//            var locatorValue=self.locator_arr().find(val=>val.value==self.normalRecieiptModel.locator()).label;
//            app.Locator(locatorValue);
            oj.Router.rootInstance.go('ReceiptInformation');
        };

        self.remain = ko.observable();
        self.AddSerialNumber = function () {

 var valid = self._checkValidationGroup();
 
            if (valid) {
                // submit the form would go here
                
            quantityNo = Number(self.normalRecieiptModel.qty());
            arrfrom[NoOFEnter] = self.normalRecieiptModel.serialFrom();
            console.log(arrfrom[NoOFEnter]);
            NoOFEnter += 1;
            result = quantityNo - NoOFEnter;
            self.remain(result);
            if (result < quantityNo && result != 0) {
                document.querySelector("#yesNoDialog").close();
                self.normalRecieiptModel.serialFrom("");
                self.enter_to_serial();

            } else if (result == 0){
                document.querySelector("#yesNoDialog").close();
                self.isvisBtnSerial(false);
                self.isvisBtnDone(true);
            }
            }
           console.log($('#po_num option:combobox').text());
        };
        self.enter_to_serial = function () {
            if(self.normalRecieiptModel.qty()<=remainingQuantity){
            self.normalRecieiptModel.serialFrom("");
            var valid = self._checkValidationGroup();
            if (valid) {
                // submit the form would go here
                
                document.querySelector("#yesNoDialog").open();
            }}else{
             app.createMessage("error", "The Quantity you enter is greater than remaining.");
            }  
        }; 
        
        //**************submit receipt ****************************//
        self.SubmitReceipt=function(){
             document.querySelector("#yesNoSubmitReceiptDialog").open();
        };
        self.yesSubmitReceipt=function(){
            self.AddRecieiptNormal();
        };
        self.NOSubmitReceipt=function(){
            document.querySelector("#yesNoSubmitReceiptDialog").close();
            oj.Router.rootInstance.go('Receipts');
        };
        
        
        
        
        
                //  *****************Add Recieipt Normal ************************//
                
        function formatDateToday() {
                    var d = new Date(),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [day, month, year].join('-');
                };        
        self.AddRecieiptNormal = function () {
            var userId=localStorage.getItem("UserId");
            var payload = {
                "poNumber": self.po_num_Arr().find(e => e.value == self.normalRecieiptModel.poNumber()).label,
                "supplier": self.normalRecieiptModel.supplierValue(),
                "lineNumber": self.linenum_Arr().find(e => e.value == self.normalRecieiptModel.lineNumber()).labelValue,
                "item": self.normalRecieiptModel.itemAndDescId(),
                "description": self.normalRecieiptModel.itemAndDescId(),
                "lpn": self.normalRecieiptModel.lpn(),
                "location": self.normalRecieiptModel.location(),
                "uom": self.normalRecieiptModel.uomValue(),
                "qty": self.normalRecieiptModel.qty(),
                "transactionType": "RECEIVE",
                "createdBy":userId,
                "creatationDate": formatDateToday(),
                "lastUpdatedDate": formatDateToday(),
                "lastUpdatedBy":userId,
                "serialArr":arrfrom,
                "locator":self.normalRecieiptModel.locator(),
                "subInventory":self.normalRecieiptModel.sub_inv(),
                "orgId":localStorage.getItem("OrganizationId")
            };
           localStorage.setItem("supplier",self.normalRecieiptModel.supplierValue());
            var RecieiptValidateCbFn = function (data) {
              
                if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"]=="S" ) {
                    var RecieiptNormalCbFn = function (data) {
                        
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                            app.createMessage("sucess",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            document.querySelector("#yesNoSubmitReceiptDialog").close();
                            self.DataToShare();
                            
                        }
                    };
                   console.log("newpaylod",payload);
                    services.addGeneric(commonUtil.AddPurchasing,JSON.stringify(payload)).then(RecieiptNormalCbFn, app.failCbFn);

                }else{
                    app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    console.log(data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    document.querySelector("#yesNoSubmitReceiptDialog").close();
                }
                 

            };
            console.log(payload);
            services.addGeneric("purchasing/ValidateCreate", JSON.stringify(payload)).then(RecieiptValidateCbFn, app.failCbFn);
        };
//        
      //************************************************************************//  
     

        
        self.BackBtn = function () {
            oj.Router.rootInstance.go('Receipts');
        };
        self.normalRecieiptModel = {
            poNumber: ko.observable(),
            supplier: ko.observable(),
            supplierValue: ko.observable(),
            lineNumber: ko.observable(),
            item: ko.observable(),
            description: ko.observable(),
            itemAndDescId: ko.observable(),
            //shipment_v: ko.observable('shipment_v'),
            lpn: ko.observable(),
            location: ko.observable(),
            uom: ko.observable(),
            uomValue: ko.observable(),
            qty: ko.observable(),
            serialFrom: ko.observable(),
            sub_inv:ko.observable(),
            locator:ko.observable()

        };
                
               
      
       app.selectItemTOClipboardCombo();
       app.selectItemTOClipboardInputText();
       
        //************************************************************************//  


     
     
 
        self.closedialog = function () {
            result = 0;
            NoOFEnter=0;
            self.remain("");
            document.querySelector("#yesNoDialog").close();


        };
        self.progressValue = ko.observable(0);
        //*********************get supplier*****************//
        function  getSupplier(po_header_id) {
            self.normalRecieiptModel.supplier("");
            var Supplierdata = function (data) {
                for (var i = 0; i < data.supplier.length; i++) {
                   self.normalRecieiptModel.supplier(data.supplier[i].SUPPLIER_NAME);
                   self.normalRecieiptModel.supplierValue(data.supplier[i].SUPPLIER_ID);
                        
                }
                
                $("#loader").hide();
            
            };
            services.getGeneric("MainServices/Supplier?po_header_id=" + po_header_id).then(Supplierdata, app.failCbFn);
        }

     function  getPoNumber() {
         $("#loader").show();
     var getAllPoNumberdata = function (data) {
        for (var i = 0; i < data.poNumber.length; i++) {
            self.po_num_Arr.push({
                "value": data.poNumber[i].PO_HEADER_ID, "label": data.poNumber[i].PO_NUMBER

            });
        }
        $("#loader").hide();
    };
    
    services.getGeneric("MainServices/getPoNumber?OrganizationId="+localStorage.getItem("OrganizationId")).then(getAllPoNumberdata, app.failCbFn);
    
        }


        //*********************get Line Number*****************//
        function  getLineNumber(po_header_id) {

            self.linenum_Arr([]);
            var LineNumberdata = function (data) {

                for (var i = 0; i < data.LineNumber.length; i++) {
                    self.linenum_Arr.push({  
                     "value": data.LineNumber[i].PO_LINE_ID, "labelValue": data.LineNumber[i].PO_LINE_NUMBER  ,"label":data.LineNumber[i].PO_LINE_NUMBER+"-"+data.LineNumber[i].DESCRIPTION
                     });
                       
                }
                
                $("#loader").hide();
            
            };
            services.getGeneric("MainServices/LineNumber?po_header_id=" + po_header_id).then(LineNumberdata, app.failCbFn);
        }

        //*********************get Location*****************//
        function  getLocation(po_header_id) {
            self.normalRecieiptModel.location("");
            self.LocationArr([]);
            var Locationdata = function (data) {


                for (var i = 0; i < data.Location.length; i++) {
                    self.LocationArr.push({  
                     "value": data.Location[i].LOCATION_ID, "label": data.Location[i].LOCATION_DESCRIPTION
                     }); 
                }
                console.log("data loc",data);
                $("#loader").hide();
               
            };
            services.getGeneric("MainServices/Location?po_header_id="+po_header_id+"&Lang="+"AR" ).then(Locationdata, app.failCbFn);
        }

        //*********************get itemDescription*****************//
        function  getDescription(po_header_id) {
            self.normalRecieiptModel.item("");
            self.normalRecieiptModel.description("");
            var ItemDescriptiondata = function (data) {
                
                for (var i = 0; i < data.ItemDescription.length; i++) {

                    self.normalRecieiptModel.item(data.ItemDescription[i].ITEM_CODE);
                    self.normalRecieiptModel.description(data.ItemDescription[i].ITEM_DESCRIPTION);
                    self.normalRecieiptModel.itemAndDescId(data.ItemDescription[i].ITEM_ID);
                   // self.checkSerialized(data.ItemDescription[i].SERIALIZED_FLAG);
                }
//                if(self.checkSerialized()!= "YES"){
//                     self.isvisBtnSerial(false);
//                     self.isvisBtnDone(true);
//                }
                GetSerializedFlag(self.normalRecieiptModel.itemAndDescId(),self.normalRecieiptModel.lineNumber());
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/ItemDescription?po_header_id=" + po_header_id).then(ItemDescriptiondata, app.failCbFn);
        }
     //********************get Lpn****************************//
        function  getLPN(po_header_id, po_line_Id) {
            self.LPN_Arr([]);
//           self.normalRecieiptModel.lpn("");
            var LPNdata = function (data) {
                console.log(data);
                if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] == "GENERATED LPN") {
                    self.LPN_Arr.push({
                        "value": data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"], "label": data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"]

                    });
                }

                $("#loader").hide();
            };
            services.getGeneric("MainServices/getLpnGeneration?po_HEADER_ID=" + po_header_id + "&po_LINE_ID=" + po_line_Id + "&lang=ar").then(LPNdata, app.failCbFn);
        }
        
        
   //*********************get GetUom*****************//
        function  GetUom(po_header_id) {
            self.normalRecieiptModel.uom("");
            var UomCbFn = function (data) {


                for (var i = 0; i < data.Uom.length; i++) {
                   self.normalRecieiptModel.uom(data.Uom[i].UOM);
                   self.normalRecieiptModel.uomValue(data.Uom[i].UOM);
                }
               
               
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/GetUom?po_line_id=" + po_header_id).then(UomCbFn, app.failCbFn);
        }
        //*********************get GetQuantity*****************//
        function  GetQuantity(po_header_id) {
            
            var QuantityCbFn = function (data) {
           for(var i=0;i<data.QuantityByLpn.length;i++){
                         self.normalRecieiptModel.qty(data.QuantityByLpn[i].ITEM_QTY);
                     }
                $("#loader").hide();
            
            
              };
            services.getGeneric("MainServices/QuantityByLpn?QuantityByLpn="+po_header_id).then(QuantityCbFn, app.failCbFn);
        }
      //****************************get flag to check serialized or not serialized**********************// 
        function  GetSerializedFlag(itemId,po_line_Id) { 
            var checkSerializedFlagCbFn = function (data) {
                for (var i = 0; i < data.checkSerializedFlag.length; i++) {
                    self.checkSerialized(data.checkSerializedFlag[i].SERIALIZED_FLAG);
                }
                if(self.checkSerialized()!= "Y"){
                     self.isvisBtnSerial(false);
                     self.isvisBtnDone(true);
                }
                $("#loader").hide();          
            };
            services.getGeneric("MainServices/checkSerializedFlag?orgId="+localStorage.getItem("OrganizationId")+"&itemId="+itemId+"&po_line_Id="+po_line_Id).then(checkSerializedFlagCbFn, app.failCbFn);
        }
        
         //****************************get flag to check serialized or not serialized**********************// 
        //OrganizationId
        function  GetSubInv(orgId) {
            
            var GetSubInvCBFN = function (data) {
                for (var i = 0; i < data.getSubInventory.length; i++) {
                    
                    self.sub_inv_arr.push({
                        
                        "value":data.getSubInventory[i].SUB_INVENTORY_NAME,"label":data.getSubInventory[i].SUB_INVENTORY_NAME
                        
                    });
                    
                   
                };
                       
            };
            services.getGeneric("MainServices/getSubInventory?orgId="+orgId).then(GetSubInvCBFN, app.failCbFn);
        }
        
        
        function GetLocator(sub_inv_code){
             self.locator_arr([]);
            var GetLocatorCBFN=function(data){
                
                for(var i=0;i<data.getLocator.length;i++){
                    
                    self.locator_arr.push({
                        
                        "value":data.getLocator[i].LOCATOR_ID,"label":data.getLocator[i].LOCATOR_DESC
                        
                    });
                    
                };
                
            };
            
            services.getGeneric("MainServices/getLocator?sub_inv_code="+sub_inv_code+"&orgId="+localStorage.getItem("OrganizationId")).then(GetLocatorCBFN,app.failCbFn)
        }
        
        //-------------   get remaing quantitiy
           function  GetQuantityRemaning(po_header_id,po_line_id) {

            var QuantityRemaningCbFn = function (data) {
                remainingQuantity=data.QuantityByLpn[0].REMAINING;
                self.normalRecieiptModel.qty(data.QuantityByLpn[0].REMAINING);
                $("#loader").hide();
            };
            services.getGeneric("MainServices/QuantityRemaining?po_Header_id=" + po_header_id+"&po_line_id="+po_line_id+"&org_Id="+localStorage.getItem("OrganizationId") ).then(QuantityRemaningCbFn, app.failCbFn);
        }
        
      
        self.disconnected = function () {
            // Implement if needed
        };
         self.connected=function(){
             console.log("normalreceipt");
             initTranslations();
             app.showHeader("on");
              var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
             getPoNumber();
             GetSubInv(localStorage.getItem("OrganizationId")); 
         };
      
        self.AddSerialLBL = ko.observable();
        self.EnterSerialHeader = ko.observable();
        self.PoNumLbl = ko.observable();
        self.SupplierLBL = ko.observable();
        self.LineNumLBL = ko.observable();
        self.ItemLBL = ko.observable();
        self.DescLBL = ko.observable();
        self.ShipmentNumLBL = ko.observable();
        self.LPNLBL = ko.observable();
        self.LocationLBL = ko.observable();
        self.UOMLBL = ko.observable();
        self.QtyLBL = ko.observable();
        self.SubmitForApproveLBL = ko.observable();
        self.EnterSerialLBL = ko.observable();
        self.FromLBL = ko.observable();
        self.TOLBL = ko.observable();
        self.placeholder = ko.observable();
        self.SubmitReceitLBL = ko.observable();
        self.confirmationMessageHeader = ko.observable();
        self.ConfirmationMessageBody = ko.observable();
        self.yesLbl = ko.observable();
        self.NOLbl = ko.observable();
        self.RemainingLbl=ko.observable();
        self.subinvLbl=ko.observable();
        self.locatorLbl=ko.observable();

//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });
        function initTranslations() {
//            app.getTranslationMethod("AddserialNumber", label => {
//                self.AddSerialLBL(label);
//            });
//            app.getTranslationMethod("serialNumber", label => {
//                self.EnterSerialHeader(label);
//            });
//            app.getTranslationMethod("PoNumLbl", label => {
//                self.PoNumLbl(label);
//            });
//            app.getTranslationMethod("SupplierLBL", label => {
//                self.SupplierLBL(label);
//            });
//            app.getTranslationMethod("LineNumLBL", label => {
//                self.LineNumLBL(label);
//            });
//            app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//            app.getTranslationMethod("DescLBL", label => {
//                self.DescLBL(label);
//            });
//            app.getTranslationMethod("ShipmentNumLBL", label => {
//                self.ShipmentNumLBL(label);
//            });
//            app.getTranslationMethod("LPNLBL", label => {
//                self.LPNLBL(label);
//            });
//            app.getTranslationMethod("LocationLBL", label => {
//                self.LocationLBL(label);
//            });
//            app.getTranslationMethod("UOMLBL", label => {
//                self.UOMLBL(label);
//            });
//            app.getTranslationMethod("QtyLBL", label => {
//                self.QtyLBL(label);
//            });
//            app.getTranslationMethod("placeholder", label => {
//                self.placeholder(label);
//            });
//            app.getTranslationMethod("SubmitForApproveLBL", label => {
//                self.SubmitForApproveLBL(label);
//            });
//            app.getTranslationMethod("EnterSerialLBL", label => {
//                self.EnterSerialLBL(label);
//            });
//             app.getTranslationMethod("FromLBL", label => {
//                self.FromLBL(label);
//            });
//             app.getTranslationMethod("TOLBL", label => {
//                self.TOLBL(label);
//            });
//             app.getTranslationMethod("SubmitReceit", label => {
//                self.SubmitReceitLBL(label);
//            });
//             app.getTranslationMethod("confirmationMessageHeader", label => {
//                self.confirmationMessageHeader(label);
//            });
//             app.getTranslationMethod("ConfirmationMessageBody", label => {
//                self.ConfirmationMessageBody(label);
//            });
//             app.getTranslationMethod("yesLbl", label => {
//                self.yesLbl(label);
//            });
//             app.getTranslationMethod("NOLbl", label => {
//                self.NOLbl(label);
//            });
//             app.getTranslationMethod("RemainingLbl", label => {
//                self.RemainingLbl(label);
//            });
//             app.getTranslationMethod("subinvLbl", label => {
//                self.subinvLbl(label);
//            });
//             app.getTranslationMethod("locatorLbl", label => {
//                self.locatorLbl(label);
//            });
            
            self.AddSerialLBL(app.getTranslationMethod("AddserialNumber"));
            self.EnterSerialHeader(app.getTranslationMethod("serialNumber"));
            self.PoNumLbl(app.getTranslationMethod("PoNumLbl"));
            self.SupplierLBL(app.getTranslationMethod("SupplierLBL"));
            self.LineNumLBL(app.getTranslationMethod("LineNumLBL"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.DescLBL(app.getTranslationMethod("DescLBL"));
            self.ShipmentNumLBL(app.getTranslationMethod("ShipmentNumLBL"));
            self.LPNLBL(app.getTranslationMethod("LPNLBL"));
            self.LocationLBL(app.getTranslationMethod("LocationLBL"));
            self.UOMLBL(app.getTranslationMethod("UOMLBL"));
            self.QtyLBL(app.getTranslationMethod("QtyLBL"));
            self.placeholder(app.getTranslationMethod("placeholder"));
            self.SubmitForApproveLBL(app.getTranslationMethod("SubmitForApproveLBL"));
            self.EnterSerialLBL(app.getTranslationMethod("EnterSerialLBL"));
            self.FromLBL(app.getTranslationMethod("FromLBL"));
            self.TOLBL(app.getTranslationMethod("TOLBL"));
            self.SubmitReceitLBL(app.getTranslationMethod("SubmitReceit"));
            self.confirmationMessageHeader(app.getTranslationMethod("confirmationMessageHeader"));
            self.ConfirmationMessageBody(app.getTranslationMethod("ConfirmationMessageBody"));
            self.yesLbl(app.getTranslationMethod("yesLbl"));
            self.NOLbl(app.getTranslationMethod("NOLbl"));
             self.RemainingLbl(app.getTranslationMethod("RemainingLbl"));
             self.subinvLbl(app.getTranslationMethod("subinvLbl"));
             self.locatorLbl(app.getTranslationMethod("locatorLbl"));
        }


        self.groupvalid = ko.observable();
         self.groupvalid1 = ko.observable();
        self._checkValidationGroup = function () {
            var tracker = document.getElementById("tracker");
            var tracker1 = document.getElementById("tracker1");
            if (tracker.valid === "valid")
            {
                return true;
            } else {

                tracker.showMessages();
               

                return false;
            }
            

        };


    }
    return Normal_ReceiptContentViewModel;
});
