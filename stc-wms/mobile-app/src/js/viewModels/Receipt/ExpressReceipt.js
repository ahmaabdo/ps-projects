/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Express_Receipt module
 */
define(['ojs/ojcore', 'knockout','config/services' ,'ojs/ojbootstrap', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton'
], function (oj, ko,services,Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function Express_ReceiptContentViewModel() {
        var self = this;
        var checkValidateQR; 
        self.scan = function () {
//
            cordova.plugins.barcodeScanner.scan(
                    function (result) {
                        if (result.format == "QR_CODE") {
                            alert(result.text);
                            localStorage.setItem("qrtext", result.text);
                            var spl=result.text.split(" | ");
                          
            localStorage.setItem('ponumidqr', spl[0]);
            localStorage.setItem('vendoridqr', spl[1]);
            localStorage.setItem('qtyqr', spl[2]);
            localStorage.setItem('linenumidqr', spl[3]);
            localStorage.setItem('lpnqr', spl[4]);
            localStorage.setItem('locationidqr', spl[5]);
            localStorage.setItem('uomqr', spl[6]);

             // ---------start validate QR  ------------------//
           var CheckValidateQR = function (data) {
                 if(data["env:Envelope"]["env:Body"]["OutputParameters"]["GET_REQUEST_STATUS"]=="REQUEST REJECTED"){
                       app.createMessage("error", "REQUEST REJECTED");
                       oj.Router.rootInstance.go('ExpressReceipt');
                 }else{
                     getPoNumber(localStorage.getItem("OrganizationId"));
                 }
                
            };
            var lang;
            if (localStorage.getItem("selectedLanguage") === "ar") {
                lang="AR";
            }else{
                lang="US";
            }
            
            var path = "purchasing/ValidateQR?lpn=" +localStorage.getItem("lpnqr")+ "&transaction_Type=RECEIVE&lang="+ lang;
            services.getGeneric(path).then(CheckValidateQR, app.failCbFn);  
            
            //---------------End Validate QR ----------------// 
            
                        } else
                            alert("Sorry This Code IS Not QR!!");
                    },
                    function (error) {
                        alert("Scanning failed: " + error);
                    }
            );


//     var res=app.qrcode_ali();
//     
//     localStorage.setItem("qrtext",res);
//     var spl=res.split(" | ");
//     localStorage.setItem('ponumidqr', spl[0]);
//     localStorage.setItem('vendoridqr', spl[1]);
//     localStorage.setItem('qtyqr', spl[2]);
//     localStorage.setItem('linenumidqr', spl[3]);
//     localStorage.setItem('lpnqr', spl[4]);
//     localStorage.setItem('locationidqr', spl[5]);
//     localStorage.setItem('uomqr', spl[6]);
     
             // ---------start validate QR  ------------------//
//           var CheckValidateQR = function (data) {
//                 if(data["env:Envelope"]["env:Body"]["OutputParameters"]["GET_REQUEST_STATUS"]=="REQUEST REJECTED"){
//                       app.createMessage("error", "REQUEST REJECTED");
//                       oj.Router.rootInstance.go('ExpressReceipt');
//                 }else{
//                     getPoNumber(localStorage.getItem("OrganizationId"));
//                 }
//                
//            };
//            var lang;
//            if (localStorage.getItem("selectedLanguage") === "ar") {
//                lang="AR";
//            }else{
//                lang="US";
//            }
//            
//            var path = "purchasing/ValidateQR?lpn=" +"E012071"+ "&transaction_Type=RECEIVE&lang="+ lang;
//            services.getGeneric(path).then(CheckValidateQR, app.failCbFn);  
//            
            //---------------End Validate QR ----------------//
             
            
           
             //getPoNumber(localStorage.getItem("OrganizationId"));
      
             
              
      };
       
    
       
       
       function  getPoNumber(orgId) {
     var getAllPoNumberdata = function (data) {
        
            if(data.poNumber.length == 0){
                app.createMessage("error", "No PO Found");
                return;
                
            };
            localStorage.setItem("ponumqr",data.poNumber[0].PO_NUMBER);
       
    
      getLineNumber(localStorage.getItem("ponumidqr"));
    };
     var path="MainServices/getPoNumberExpress?OrganizationId="+localStorage.getItem("OrganizationId")+"&poID="+localStorage.getItem('ponumidqr');
    services.getGeneric(path).then(getAllPoNumberdata, app.failCbFn);
        };
       
       
       function  getSupplier(po_header_id) {
           
            var Supplierdata = function (data) {
                for (var i = 0; i < data.supplier.length; i++) {
                    if(data.supplier[i].SUPPLIER_ID==localStorage.getItem("vendoridqr"))
                  localStorage.setItem("vendorqr",data.supplier[i].SUPPLIER_NAME);
                        
                }
                
                getLocation(localStorage.getItem("linenumidqr"));
            
            };
            services.getGeneric("MainServices/Supplier?po_header_id=" + po_header_id).then(Supplierdata, app.failCbFn);
        }
       function  getLineNumber(po_header_id) {

            
            var LineNumberdata = function (data) {

                for (var i = 0; i < data.LineNumber.length; i++) {
                  if(data.LineNumber[i].PO_LINE_ID==localStorage.getItem("linenumidqr")){
                    localStorage.setItem("linenumqr",data.LineNumber[i].PO_LINE_NUMBER);
                }
                }
                
                
                getSupplier(localStorage.getItem("ponumidqr"));
            };
            services.getGeneric("MainServices/LineNumber?po_header_id=" + po_header_id).then(LineNumberdata, app.failCbFn);
        }
    
    
    function  getLocation(po_header_id) {
            
            
            var Locationdata = function (data) {


                for (var i = 0; i < data.Location.length; i++) {
                    if(data.Location[i].LOCATION_ID==localStorage.getItem("locationidqr"))
                    {
                        localStorage.setItem("locationqr",data.Location[i].LOCATION_DESCRIPTION);
                    }
                    
                }
                console.log("data loc",data);
               
               getDescription(localStorage.getItem("linenumidqr"));
            };
            services.getGeneric("MainServices/Location?po_header_id="+po_header_id+"&Lang="+"AR" ).then(Locationdata, app.failCbFn);
        }
   
             function  getDescription(po_header_id) {
           
            var ItemDescriptiondata = function (data) {

                    
                for (var i = 0; i < data.ItemDescription.length; i++) {
                            
                            localStorage.setItem("itemqr",data.ItemDescription[i].ITEM_DESCRIPTION);
                           localStorage.setItem("itemidqr",data.ItemDescription[i].ITEM_ID);
                           console.log("anaa",localStorage.getItem("itemidqr"));                          

                }
        oj.Router.rootInstance.go('ReceiptInformationField');
            };
            services.getGeneric("MainServices/ItemDescription?po_header_id=" + po_header_id).then(ItemDescriptiondata, app.failCbFn);
        }
 

          //*** get values ***//
//          var ponumqr=localStorage.getItem("ponumqr");
//          var vendorqr=localStorage.getItem("vendorqr");
//          var itemqr=localStorage.getItem("itemqr");
//          var qtyqr=localStorage.getItem("qtyqr");
//          var locationqr=localStorage.getItem("locationqr");
//          var subinvqr=localStorage.getItem("sub_invqr");
//          var locatorqr=localStorage.getItem("locatorqr");

app.selectItemTOClipboardCombo();


        self.ScanQRCodeLBL = ko.observable();
//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });
        function initTranslations() {
//             app.getTranslationMethod("ScanQRCodeLBL", label => {
//                self.ScanQRCodeLBL(label);
//            });
            
            self.ScanQRCodeLBL(app.getTranslationMethod("ScanQRCodeLBL"));
        }
        self.disconnected = function () {
            // Implement if needed
//                    
        };
        self.connected = function () {
            initTranslations();
            app.showHeader("on");
            var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                    
                    
           
        };
    }
    return Express_ReceiptContentViewModel;
});
