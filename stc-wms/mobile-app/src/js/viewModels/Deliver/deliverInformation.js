/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * deliver_info module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojbootstrap', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton','ojs/ojprogress'
], function (oj, ko, Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function deliver_infoContentViewModel() {
        var self = this;
        var ponum;
        var vendor;
        var item;
        var qty;
        var location;
        var sub_inv;
        var locator;
        var supplier;
        var linenum;
        var desc;

        var lpn;
        var uom;
        
        var text;
        var data;
                

        

        self.share = function () {
            navigator.share(text);
        };
self.progressValue = ko.observable(0);
        self.return_to_menu = function () {
            
           
            
            oj.Router.rootInstance.go('Deliver');
        };
app.selectItemTOClipboardCombo();
        self.connected = function () {
            initTranslations();
            app.showHeader("on");
            var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                     
            
            data = oj.Router.rootInstance.retrieve();
            if (data.action === 'express_share')
            {

                ponum = localStorage.getItem('ponum');
                vendor = localStorage.getItem('vendor');
                item = localStorage.getItem('item');
                qty = localStorage.getItem('qty');
                location = localStorage.getItem('location');
                sub_inv = localStorage.getItem('sub_inv');
                locator = localStorage.getItem('locator');
                text = "PO Number: " + ponum + "\nVendor: " + vendor + "\nItem: " + item + "\nQuantity: " + qty +
                        "\nLocation: " + location + "\nSub-Inv: " + sub_inv + "\nLocator: " + locator;
            } else if (data.action === 'manual_share')
            {
                ponum = localStorage.getItem('ponum');
                supplier = localStorage.getItem('supplier');
                linenum = localStorage.getItem('linenum');
                item = localStorage.getItem('item');
                desc = localStorage.getItem('desc');
                //shipment = localStorage.getItem('shipment');
                lpn = localStorage.getItem('lpn');
                location = localStorage.getItem('location');
                uom = localStorage.getItem('uom');
                qty = localStorage.getItem('qty');
               
               
                text = "Po Number: " + ponum + "\nSupplier: " + supplier + "\nlinenum: " + linenum +
                        "\nitem: " + item + "\nDescription: " + desc + "\nLPN: " + lpn +
                        "\nLocation: " + location + "\nUOM: " + uom + "\nQuantity: " + qty;
                        
            }
        };
       self.disconnected = function () {
                    // Implement if needed
                 
                };


        //  define translation  
        self.ShareLBL = ko.observable();
        self.ReturnToMenuLBL = ko.observable();

//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });

        function initTranslations() {
//            app.getTranslationMethod("ShareLBL", label => {
//                self.ShareLBL(label);
//            });
//            app.getTranslationMethod("ReturnToMenuLBL", label => {
//                self.ReturnToMenuLBL(label);
//            });
            self.ShareLBL(app.getTranslationMethod("ShareLBL"));
            self.ReturnToMenuLBL(app.getTranslationMethod("ReturnToMenuLBL"));
        }

    }

    return deliver_infoContentViewModel;
});
