/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * deliver_information_field module
 */
define(['ojs/ojcore', 'knockout','jquery','util/commonhelper','config/services', 'ojs/ojbootstrap', 'appController',
    'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojbutton','ojs/ojprogress'
], function (oj, ko,$,commonUtil,services,Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function deliver_information_fieldContentViewModel() {
        var self = this;
                
   var arrfrom=[];
       
        // define global 
         self.isDisabled=ko.observable(true);
         self.sub_inv_arr=ko.observableArray();
         self.locator_arr=ko.observableArray();
       

 self.subinvchangehandler=function(event){
     var subinvval=event.detail.value;
     GetLocator(subinvval);
     
 };
 
 function formatDateToday() {
                    var d = new Date(),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [day, month, year].join('-');
                };        
 
 
app.selectItemTOClipboardCombo();
        self.informationDelivertModel = {
                        poNumber: ko.observable(),
                        vendor: ko.observable(),
                        item: ko.observable(),
                        location: ko.observable(),
                        qty: ko.observable(),
                        sub_inv: ko.observable(),
                        locator: ko.observable()
                    };
                    
                    
                    
        self.informationDelivertModel.poNumber(localStorage.getItem('ponumqr'));
        self.informationDelivertModel.vendor(localStorage.getItem('vendorqr'));
        self.informationDelivertModel.location(localStorage.getItem('locationqr'));
        self.informationDelivertModel.item(localStorage.getItem('itemqr'));
        self.informationDelivertModel.qty(localStorage.getItem('qtyqr'));
                  //************************Add Deliver Normal**********************************//
          self.AddDeliverExpress=function(){
               var userId=localStorage.getItem("UserId");
               var payload ={
               "supplier":localStorage.getItem("vendoridqr"), 
                "poNumber": self.informationDelivertModel.poNumber(),
                "lineNumber":localStorage.getItem("linenumqr"), 
                "description": localStorage.getItem("itemidqr"),   
                "lpn":localStorage.getItem('lpnqr'),  
                "locator": self.informationDelivertModel.locator(),
                "subInventory": self.informationDelivertModel.sub_inv(),
                "location":localStorage.getItem('locationidqr'),  
                 "qty": self.informationDelivertModel.qty(),
                "uom":  localStorage.getItem('uomqr'),
                 "transactionType": "DELIVER",
                "createdBy":userId,
                "creatationDate": formatDateToday(),
                "lastUpdatedDate": formatDateToday(),
                "lastUpdatedBy":userId,
                "orgId":localStorage.getItem("OrganizationId"),
                "serialArr":arrfrom,
                "qrText":localStorage.getItem("qrtext")              
             };  
              
             var DeliverExpressCbFn=function(data){
                  
                 var x=data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"];
                        console.log("x",x);
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                         var DeliverNormalCbFn = function (data) {
                         var x2=data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"];
                        console.log("x2",x2);
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                            app.createMessage("sucess",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                           $("#loader").hide();
                            self.DataToShare();
                            
                        }
                        else{
                            app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                           $("#loader").hide();
                            
                        } 
                            
                    };
                    console.log("newpaylod",payload);
                    services.addGeneric(commonUtil.AddPurchasing,JSON.stringify(payload)).then(DeliverNormalCbFn, app.failCbFn);
                   

                }else{
                    app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    $("#loader").hide();
                }
                
                
            
             };
            
             
             services.addGeneric("purchasing/ValidateCreate", JSON.stringify(payload)).then(DeliverExpressCbFn, app.failCbFn);
         };   
      //************************************************************************//  
       
    self.progressValue = ko.observable(0);
        self.done = function () {
           // oj.Router.rootInstance.go('deliverInformation');
          $("#loader").show();                    
                  
            self.AddDeliverExpress();
           
        };
//        
//        self.BackBtn=function(){
//             oj.Router.rootInstance.go('expressDeliver');
//       };
        
        self.DataToShare=function(){
            
            oj.Router.rootInstance.store({action: "express_share"});
            localStorage.setItem("ponum", self.informationDelivertModel.poNumber());
            localStorage.setItem("vendor", self.informationDelivertModel.vendor());
            localStorage.setItem("item", self.informationDelivertModel.item());
            localStorage.setItem("qty", self.informationDelivertModel.qty());
            localStorage.setItem("location", self.informationDelivertModel.location());
            localStorage.setItem("sub_inv", self.informationDelivertModel.sub_inv());
            localStorage.setItem("locator", self.informationDelivertModel.locator());
             oj.Router.rootInstance.go('deliverInformation');
            
            
        };
        
        function  GetSubInv(orgId) {
              self.sub_inv_arr([]);
            var GetSubInvCBFN = function (data) {
                for (var i = 0; i < data.getSubInventory.length; i++) {
                    
                    self.sub_inv_arr.push({
                        
                        "value":data.getSubInventory[i].SUB_INVENTORY_NAME,"label":data.getSubInventory[i].SUB_INVENTORY_NAME
                        
                    });
                    
                   
                };
                       
            };
            services.getGeneric("MainServices/getSubInventory?orgId="+orgId).then(GetSubInvCBFN, app.failCbFn);
        }
        
        
        function GetLocator(sub_inv_code){
             self.locator_arr([]);
            var GetLocatorCBFN=function(data){
                
                for(var i=0;i<data.getLocator.length;i++){
                    
                    self.locator_arr.push({
                        
                        "value":data.getLocator[i].LOCATOR_ID,"label":data.getLocator[i].LOCATOR_DESC
                        
                    });
                    
                };
                
            };
            
            services.getGeneric("MainServices/getLocator?sub_inv_code="+sub_inv_code+"&orgId="+localStorage.getItem("OrganizationId")).then(GetLocatorCBFN,app.failCbFn)
        }
         //   DEFINE TRANSLATION
         self.PoNumLbl=ko.observable();
         self.vendorLBL=ko.observable();
         self.ItemLBL=ko.observable();
         self.QtyLBL=ko.observable();
         self.LocationLBL=ko.observable();
         self.EnterLocationLBL=ko.observable();
         self.SubInventoryLBL=ko.observable();
         self.LocatorLBL=ko.observable();
         self.SubmitForApproveLBL=ko.observable();
         self.DoneLBL=ko.observable();
         self.subinvLbl=ko.observable();
        self.locatorLbl=ko.observable();
         
//          self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        initTranslations();
//                    }
//                });
//        
        function initTranslations() {
            
//             app.getTranslationMethod("PoNumLbl", label => {
//                self.PoNumLbl(label);
//            });
//            app.getTranslationMethod("vendorLBL", label => {
//                self.vendorLBL(label);
//            });
//             app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//            app.getTranslationMethod("QtyLBL", label => {
//                self.QtyLBL(label);
//            });
//             app.getTranslationMethod("LocationLBL", label => {
//                self.LocationLBL(label);
//            });
//            app.getTranslationMethod("EnterLocationLBL", label => {
//                self.EnterLocationLBL(label);
//            });
//             app.getTranslationMethod("SubInventoryLBL", label => {
//                self.SubInventoryLBL(label);
//            });
//            app.getTranslationMethod("LocatorLBL", label => {
//                self.LocatorLBL(label);
//            });
//             app.getTranslationMethod("SubmitForApproveLBL", label => {
//                self.SubmitForApproveLBL(label);
//            });
//            app.getTranslationMethod("DoneLBL", label => {
//                self.DoneLBL(label);
//            });
//            app.getTranslationMethod("subinvLbl", label => {
//                self.subinvLbl(label);
//            });
//            app.getTranslationMethod("locatorLbl", label => {
//                self.locatorLbl(label);
//            });
            
            
            self.PoNumLbl(app.getTranslationMethod("PoNumLbl"));
            self.vendorLBL(app.getTranslationMethod("vendorLBL"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.QtyLBL(app.getTranslationMethod("QtyLBL"));
            self.LocationLBL(app.getTranslationMethod("LocationLBL"));
            self.EnterLocationLBL(app.getTranslationMethod("EnterLocationLBL"));
            self.SubInventoryLBL(app.getTranslationMethod("SubInventoryLBL"));
            self.LocatorLBL(app.getTranslationMethod("LocatorLBL"));
            self.SubmitForApproveLBL(app.getTranslationMethod("SubmitForApproveLBL"));
            self.DoneLBL(app.getTranslationMethod("DoneLBL"));
            self.subinvLbl(app.getTranslationMethod("subinvLbl"));
            self.locatorLbl(app.getTranslationMethod("locatorLbl"));
                }; 
  
         //   DEFINE TRANSLATION
         
        
         self.disconnected = function () {
                  
                };
         self.connected=function(){
             initTranslations();
             app.showHeader("on");
                  var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);  
             
             GetSubInv(localStorage.getItem("OrganizationId"));
         };
          self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
        
       
                
                
    }

    return deliver_information_fieldContentViewModel;
});
