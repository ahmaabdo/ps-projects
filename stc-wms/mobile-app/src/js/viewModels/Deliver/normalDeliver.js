/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * normal_deliver module
 */
define(['ojs/ojcore', 'knockout','jquery' ,'ojs/ojrouter', 'util/commonhelper', 'config/services', 'ojs/ojbootstrap', 'appController',
    'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojlabel', 'jquery',
    'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojknockout', 'ojs/ojlistview', 'ojs/ojoffcanvas',
    'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojdialog', 'ojs/ojvalidationgroup', , 'ojs/ojprogress'
], function (oj, ko,$ ,Router, commonUtil, services, Bootstrap, app, moduleUtils, ArrayDataProvider) {
    
    
    /**
     * The view model for the main content view template
     */
    function normal_deliverContentViewModel() {
        var self = this;
        var selectFORcoy;
        var selectFORpaste;
        var result;
        var quantityNo;
        var remain;
        var NoOFEnter=0;
        var arrfrom = [];
        self.selectedMenuItem = ko.observable("(None selected yet)");
        self.pageLbel = ko.observable();
        
        self.isvisBtnSerial = ko.observable(true);
        self.isvisBtnDone = ko.observable(false);
        self.checkSerialized=ko.observable();
        
        self.menuItemAction = function (event)
        {
            self.selectedMenuItem(event.target.value);
        }.bind(self);
        self.copy = function () {};
        self.paste = function () {};
        self.LOV = function () {};
        self.generateLPN = function () {};
        self.delete1 = function () {};
        self.share = function () {};
        self.submitForApprove = function () {};
        self.addFav = function () {
            var path = window.location.href;
            var pageRoot = path.substring(path.indexOf("=") + 1);
//                    console.log(last);
            var value = Router.rootInstance.states.find(r => r.id === "changeLang").value;
            var _label = Router.rootInstance.states.find(r => r.id === "changeLang")._label;
//                      console.log(last,label);
        };
        self.po_num_Arr = ko.observableArray();
        
        
        self.progressValue = ko.observable(0);
        //***********************Add New*********************************// 
        self.linenum_Arr = ko.observableArray([]);
        self.LPN_Arr = ko.observableArray();
        self.QtyArr = ko.observableArray();
        self.LocationArr = ko.observableArray();
        self.sub_inv_arr=ko.observableArray();
        self.locator_arr=ko.observableArray();
        app.selectItemTOClipboardCombo();
       app.selectItemTOClipboardInputText();
        //******************on change Handler*************************//
       
        self.PoNumChangedHandler = function (event) {
            self.normalDeliverModel.supplier("");
           
           self.normalDeliverModel.lineNumber("");
            var poNumberValue = event.detail.value;
            $("#loader").show();
            
            getSupplier(poNumberValue);
            getLineNumber(poNumberValue);
        };
        
  
        
        self.linenumChangedHandler = function (event) {
            
           self.normalDeliverModel.item("");
           self.normalDeliverModel.description("");
           self.normalDeliverModel.lpn("");
           self.normalDeliverModel.location("");
           self.normalDeliverModel.uom("");
            
            var lineNumberValue = event.detail.value;
            $("#loader").show();
           
               if (lineNumberValue != undefined && lineNumberValue!="") {
                getLocation(lineNumberValue);
                getDescription(lineNumberValue);
                GetUom(lineNumberValue);
               getLPN(localStorage.getItem("OrganizationId"));
            }
        };
        
        
         self.LPNChangedHandler = function () {
             var lpnvalue=event.detail.value;
             self.normalDeliverModel.qty("");
            
             
            console.log("lpnvalueee",self.normalDeliverModel.lpn());
             
             $("#loader").show();
            
            if(lpnvalue !=undefined && lpnvalue!=""){
            
             GetQuantity(lpnvalue);
        };
             
         };
        self.QtyChangedHandler = function () {};
        self.LocationChangedHandler = function () {};
        self.subinvchangehandler=function(event){
            var subinvval=event.detail.value;
            GetLocator(subinvval);
        };
        self.DataToShare = function () {
            oj.Router.rootInstance.store({action: "manual_share"});
            localStorage.setItem("ponum", self.po_num_Arr().find(e => e.value == self.normalDeliverModel.poNumber()).label);
            localStorage.setItem("supplier", self.normalDeliverModel.supplier());
            localStorage.setItem("linenum", self.linenum_Arr().find(e => e.value == self.normalDeliverModel.lineNumber()).label);
            localStorage.setItem("item", self.normalDeliverModel.item());
            localStorage.setItem("desc", self.normalDeliverModel.description());
            //localStorage.setItem("shipment", self.normalDeliverModel.shipmentNumber());
            localStorage.setItem("lpn", self.LPN_Arr().find(e => e.value == self.normalDeliverModel.lpn()).label);
            localStorage.setItem("location", self.LocationArr().find(e=>e.value==self.normalDeliverModel.location()).label);
            localStorage.setItem("uom", self.normalDeliverModel.uom());
            localStorage.setItem("qty", self.normalDeliverModel.qty);
            localStorage.setItem("From", self.normalDeliverModel.serialFrom());
            document.querySelector("#yesNoDialog").close();
            oj.Router.rootInstance.go('deliverInformation');
        };
        self.remain = ko.observable();
        self.AddSerialNumber = function () {


 var valid = self._checkValidationGroup();
 //var valid1 = self._checkValidationGroup();
            if (valid) {
                // submit the form would go here
                
                
           
            quantityNo = Number(self.normalDeliverModel.qty());
            arrfrom[NoOFEnter] = self.normalDeliverModel.serialFrom();
            console.log(arrfrom[NoOFEnter]);
            NoOFEnter += 1;
            result = quantityNo - NoOFEnter;
            self.remain(result);
            if (result < quantityNo && result != 0) {
                document.querySelector("#yesNoDialog").close();
                self.normalDeliverModel.serialFrom("");
                self.enter_to_serial();

            } else if (result == 0)
            {
                document.querySelector("#yesNoDialog").close();
                self.isvisBtnSerial(false);
                self.isvisBtnDone(true);
                //self.AddRecieiptNormal();
            }
            }
           console.log($('#po_num option:combobox').text());
        };
        self.enter_to_serial = function () {
            self.normalDeliverModel.serialFrom("");
            var valid = self._checkValidationGroup();
            if (valid) {
                // submit the form would go here
                document.querySelector("#yesNoDialog").open();
            }
        }; 
        
        self.normalDeliverModel = {
            poNumber: ko.observable(),
            supplier: ko.observable(),
            supplierValue: ko.observable(),
            lineNumber: ko.observable(),
            item: ko.observable(),
            description: ko.observable(),
            itemAndDescId: ko.observable(),
            //shipment_v: ko.observable('shipment_v'),
            lpn: ko.observable(),
            location: ko.observable(),
            uom: ko.observable(),
            uomValue: ko.observable(),
            qty: ko.observable(),
            serialFrom: ko.observable(),
            sub_inv:ko.observable(),
            locator:ko.observable()
        };
        
        function formatDateToday() {
            var d = new Date(),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('-');
        }
        ;   
        
        //******************* Add Deliver Normal********************************//
        var payload;
        self.AddDeliverNormal = function () {
            var userId=localStorage.getItem("UserId");
             payload = {
                "poNumber": self.po_num_Arr().find(e => e.value == self.normalDeliverModel.poNumber()).label,
                "supplier": self.normalDeliverModel.supplierValue(),
                "lineNumber": self.linenum_Arr().find(e => e.value == self.normalDeliverModel.lineNumber()).label,
                "item": self.normalDeliverModel.itemAndDescId(),
                "description": self.normalDeliverModel.itemAndDescId(),
                "lpn": self.normalDeliverModel.lpn(),
                "location": self.normalDeliverModel.location(),
                "uom": self.normalDeliverModel.uomValue(),
                "qty": self.normalDeliverModel.qty(),
                "transactionType": "DELIVER",
                "createdBy":userId,
                "creatationDate": formatDateToday(),
                "lastUpdatedDate": formatDateToday(),
                "lastUpdatedBy": userId,
                "serialArr":arrfrom,
                 "locator":self.normalDeliverModel.locator(),
                "subInventory":self.normalDeliverModel.sub_inv(),
                 "orgId":localStorage.getItem("OrganizationId")
            };
            var DeliverValidateCbFn = function (data) {
                if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S") {
                    var DeliverNormalCbFn = function (data) {
                        
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"]== "S")
                        {
                            app.createMessage("sucess",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            document.querySelector("#yesNoSubmitReceiptDialog").close();
                            self.DataToShare();
                            
                        }
                    };
                    services.addGeneric(commonUtil.AddPurchasing,JSON.stringify(payload)).then(DeliverNormalCbFn, app.failCbFn);

                }else{
                    app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    document.querySelector("#yesNoSubmitReceiptDialog").close();
                }

            };
            services.addGeneric("purchasing/ValidateCreate", JSON.stringify(payload)).then(DeliverValidateCbFn, app.failCbFn);
        };
        console.log("deliver paylod",payload);
        //****************************************************************************************//
        self.SubmitReceipt = function () {
            document.querySelector("#yesNoSubmitReceiptDialog").open();
        };
         self.yesSubmitDeliver=function(){
            self.AddDeliverNormal();
        };
        self.NOSubmitDeliver=function(){
            document.querySelector("#yesNoSubmitReceiptDialog").close();
        };
        
  
  
  
        self.closedialog = function () {
            
            result = 0;
            NoOFEnter=0;
            self.remain("");
            document.querySelector("#yesNoDialog").close();
        };
        self.notification = function () {
            cordova.plugins.clipboard.copy(selectFORcoy);
        };
        self.cc = function () {
            cordova.plugins.clipboard.paste(function (coppiedText) {
                selectFORpaste.value = coppiedText;
            });
        };
        
        function  getPoNumber() {
            var getAllPoNumberdata = function (data) {
                console.log(data);
                for (var i = 0; i < data.DeliverPoNumber.length; i++) {
                    self.po_num_Arr.push({
                        "value": data.DeliverPoNumber[i].PO_HEADER_ID, "label": data.DeliverPoNumber[i].PO_NUM

                    });
                }
            };
            services.getGeneric("MainServices/deliverPoNumber?orgId="+localStorage.getItem("OrganizationId")).then(getAllPoNumberdata, app.failCbFn);
        }
        
        //*********************get supplier*****************//
        function  getSupplier(po_header_id) {
            self.normalDeliverModel.supplier("");
            var Supplierdata = function (data) {
                for (var i = 0; i < data.supplier.length; i++) {
                    self.normalDeliverModel.supplier(data.supplier[i].SUPPLIER_NAME);
                    self.normalDeliverModel.supplierValue(data.supplier[i].SUPPLIER_ID);
                }
                $("#loader").hide();
            };
            services.getGeneric("MainServices/Supplier?po_header_id=" + po_header_id).then(Supplierdata, app.failCbFn);
        }
        //*********************get Line Number*****************//
         function  getLineNumber(po_header_id) {
            self.linenum_Arr([]);
            var LineNumberdata = function (data) {
                for (var i = 0; i < data.LineNumber.length; i++) {
                    self.linenum_Arr.push({  
                     "value": data.LineNumber[i].PO_LINE_ID, "label": data.LineNumber[i].PO_LINE_NUMBER
                     });
                       
                }
                
                $("#loader").hide();
            
            };
            services.getGeneric("MainServices/LineNumber?po_header_id=" + po_header_id).then(LineNumberdata, app.failCbFn);
        }
               //*********************get Location*****************//
        function  getLocation(po_header_id) {
            self.normalDeliverModel.location("");
            self.LocationArr([]);
            var Locationdata = function (data) {


                for (var i = 0; i < data.Location.length; i++) {
                    self.LocationArr.push({  
                     "value": data.Location[i].LOCATION_ID, "label": data.Location[i].LOCATION_DESCRIPTION
                     }); 
                }
                console.log("data loc",data);
                $("#loader").hide();
               
            };
            services.getGeneric("MainServices/Location?po_header_id="+po_header_id+"&Lang="+"AR" ).then(Locationdata, app.failCbFn);
        }

        //*********************get itemDescription*****************//
        function  getDescription(po_header_id) {
            self.normalDeliverModel.item("");
            self.normalDeliverModel.description("");
            var ItemDescriptiondata = function (data) {
                
                for (var i = 0; i < data.ItemDescription.length; i++) {

                    self.normalDeliverModel.item(data.ItemDescription[i].ITEM_CODE);
                    self.normalDeliverModel.description(data.ItemDescription[i].ITEM_DESCRIPTION);
                    self.normalDeliverModel.itemAndDescId(data.ItemDescription[i].ITEM_ID);

                }
                GetSerializedFlag(self.normalDeliverModel.itemAndDescId());
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/ItemDescription?po_header_id=" + po_header_id).then(ItemDescriptiondata, app.failCbFn);
        }
     //********************get Lpn****************************//
       function  getLPN(orgid) {
           self.LPN_Arr([]);
           self.normalDeliverModel.lpn("");
            var LPNdata = function (data) {
                console.log(data);

                for (var i = 0; i < data.LPNInspect.length; i++) {
                      self.LPN_Arr.push({
                       "value": data.LPNInspect[i].LPN,"label": data.LPNInspect[i].LPN
                        
                    });
                    
                }
            };
            services.getGeneric("MainServices/LPNDeliver?orgId="+orgid).then(LPNdata, app.failCbFn);
        }
        
        
   //*********************get GetUom*****************//
        function  GetUom(po_header_id) {
            self.normalDeliverModel.uom("");
            var UomCbFn = function (data) {


                for (var i = 0; i < data.Uom.length; i++) {
                   self.normalDeliverModel.uom(data.Uom[i].UOM);
                   self.normalDeliverModel.uomValue(data.Uom[i].UOM);
                }
               
               
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/GetUom?po_line_id=" + po_header_id).then(UomCbFn, app.failCbFn);
        }
        //*********************get GetQuantity*****************//
        function  GetQuantity(lpn) {
            self.QtyArr([]);
            var QuantityCbFn = function (data) {


                for (var i = 0; i < data.LPNInspect.length; i++) {

                    self.normalDeliverModel.qty(data.LPNInspect[i].QUANTITY);

                }
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/DeliverQuantity?lpn="+lpn).then(QuantityCbFn, app.failCbFn);
        }
           //****************************get flag to check serialized or not serialized**********************// 
        function  GetSerializedFlag(itemId) {

            var checkSerializedFlagCbFn = function (data) {
                for (var i = 0; i < data.checkSerializedFlag.length; i++) {
                    self.checkSerialized(data.checkSerializedFlag[i].SERIALIZED_FLAG);
                }
                 if(self.checkSerialized()!= "Y"){
                     self.isvisBtnSerial(false);
                     self.isvisBtnDone(true);
                }
                $("#loader").hide();
            };
            services.getGeneric("MainServices/checkSerializedFlag?orgId=" + localStorage.getItem("OrganizationId") + "&itemId=" + itemId).then(checkSerializedFlagCbFn, app.failCbFn);
        }
        
        
        function  GetSubInv(orgId) {
            
            var GetSubInvCBFN = function (data) {
                for (var i = 0; i < data.getSubInventory.length; i++) {
                    
                    self.sub_inv_arr.push({
                        
                        "value":data.getSubInventory[i].SUB_INVENTORY_NAME,"label":data.getSubInventory[i].SUB_INVENTORY_NAME
                        
                    });
                    
                   
                };
                       
            };
            services.getGeneric("MainServices/getSubInventory?orgId="+orgId).then(GetSubInvCBFN, app.failCbFn);
        }
        
        
        function GetLocator(sub_inv_code){
             self.locator_arr([]);
            var GetLocatorCBFN=function(data){
                
                for(var i=0;i<data.getLocator.length;i++){
                    
                    self.locator_arr.push({
                        
                        "value":data.getLocator[i].LOCATOR_ID,"label":data.getLocator[i].LOCATOR_DESC
                        
                    });
                    
                };
                
            };
            
            services.getGeneric("MainServices/getLocator?sub_inv_code="+sub_inv_code+"&orgId="+localStorage.getItem("OrganizationId")).then(GetLocatorCBFN,app.failCbFn)
        }
      
       
         
        self.AddSerialLBL = ko.observable();
        self.EnterSerialHeader = ko.observable();
        self.PoNumLbl = ko.observable();
        self.SupplierLBL = ko.observable();
        self.LineNumLBL = ko.observable();
        self.ItemLBL = ko.observable();
        self.DescLBL = ko.observable();
        self.ShipmentNumLBL = ko.observable();
        self.LPNLBL = ko.observable();
        self.LocationLBL = ko.observable();
        self.UOMLBL = ko.observable();
        self.QtyLBL = ko.observable();
        self.SubmitForApproveLBL = ko.observable();
        self.EnterSerialLBL = ko.observable();
        self.FromLBL = ko.observable();
        self.RemainingLbl=ko.observable();
        self.confirmationMessageHeader = ko.observable();
        self.ConfirmationMessageBody = ko.observable();
        self.placeholder = ko.observable();
        self.yesLbl = ko.observable();
        self.NOLbl = ko.observable();
        self.SubmitReceitLBL = ko.observable();
        self.subinvLbl=ko.observable();
        self.locatorLbl=ko.observable();
        self.BackBtn = function () {
            oj.Router.rootInstance.go('Deliver');
        };
        //**********************define translation***********************// 
//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });
        function initTranslations() {
//             app.getTranslationMethod("AddserialNumber", label => {
//                self.AddSerialLBL(label);
//            });
//            app.getTranslationMethod("serialNumber", label => {
//                self.EnterSerialHeader(label);
//            });
//             app.getTranslationMethod("PoNumLbl", label => {
//                self.PoNumLbl(label);
//            });
//            app.getTranslationMethod("SupplierLBL", label => {
//                self.SupplierLBL(label);
//            });
//             app.getTranslationMethod("LineNumLBL", label => {
//                self.LineNumLBL(label);
//            });
//            app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//             app.getTranslationMethod("DescLBL", label => {
//                self.DescLBL(label);
//            });
//            app.getTranslationMethod("ShipmentNumLBL", label => {
//                self.ShipmentNumLBL(label);
//            });
//             app.getTranslationMethod("LPNLBL", label => {
//                self.LPNLBL(label);
//            });
//            app.getTranslationMethod("LocationLBL", label => {
//                self.LocationLBL(label);
//            });
//            app.getTranslationMethod("UOMLBL", label => {
//                self.UOMLBL(label);
//            });
//            app.getTranslationMethod("QtyLBL", label => {
//                self.QtyLBL(label);
//            });
//             app.getTranslationMethod("SubmitForApproveLBL", label => {
//                self.SubmitForApproveLBL(label);
//            });
//            app.getTranslationMethod("EnterSerialLBL", label => {
//                self.EnterSerialLBL(label);
//            });
//             app.getTranslationMethod("FromLBL", label => {
//                self.FromLBL(label);
//            });
//            app.getTranslationMethod("RemainingLbl", label => {
//                self.RemainingLbl(label);
//            });
//             app.getTranslationMethod("confirmationMessageHeader", label => {
//                self.confirmationMessageHeader(label);
//            });
//            app.getTranslationMethod("ConfirmationMessageBody", label => {
//                self.ConfirmationMessageBody(label);
//            });
//             app.getTranslationMethod("SubInventoryLBL", label => {
//                self.SubInventoryLBL(label);
//            });
//            app.getTranslationMethod("yesLbl", label => {
//                self.yesLbl(label);
//            });
//             app.getTranslationMethod("NOLbl", label => {
//                self.NOLbl(label);
//            });
//            app.getTranslationMethod("SubmitReceit", label => {
//                self.SubmitReceitLBL(label);
//            });
//            app.getTranslationMethod("placeholder", label => {
//                self.placeholder(label);
//            });
//            app.getTranslationMethod("subinvLbl", label => {
//                self.subinvLbl(label);
//            });
//              app.getTranslationMethod("locatorLbl", label => {
//                self.locatorLbl(label);
//            });
            self.AddSerialLBL(app.getTranslationMethod("AddserialNumber"));
            self.EnterSerialHeader(app.getTranslationMethod("serialNumber"));
            self.PoNumLbl(app.getTranslationMethod("PoNumLbl"));
            self.SupplierLBL(app.getTranslationMethod("SupplierLBL"));
            self.LineNumLBL(app.getTranslationMethod("LineNumLBL"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.DescLBL(app.getTranslationMethod("DescLBL"));
            self.ShipmentNumLBL(app.getTranslationMethod("ShipmentNumLBL"));
            self.LPNLBL(app.getTranslationMethod("LPNLBL"));
            self.LocationLBL(app.getTranslationMethod("LocationLBL"));
            self.UOMLBL(app.getTranslationMethod("UOMLBL"));
            self.QtyLBL(app.getTranslationMethod("QtyLBL"));
            self.SubmitForApproveLBL(app.getTranslationMethod("SubmitForApproveLBL"));
            self.EnterSerialLBL(app.getTranslationMethod("EnterSerialLBL"));
            self.FromLBL(app.getTranslationMethod("FromLBL"));
            self.RemainingLbl(app.getTranslationMethod("RemainingLbl"));
            self.confirmationMessageHeader(app.getTranslationMethod("confirmationMessageHeader"));
            self.ConfirmationMessageBody(app.getTranslationMethod("ConfirmationMessageBody"));
            self.yesLbl(app.getTranslationMethod("yesLbl"));
            self.NOLbl(app.getTranslationMethod("NOLbl"));
            self.SubmitReceitLBL(app.getTranslationMethod("SubmitReceit"));
            self.placeholder(app.getTranslationMethod("placeholder"));
            self.subinvLbl(app.getTranslationMethod("subinvLbl"));
            self.locatorLbl(app.getTranslationMethod("locatorLbl"));
            
        }
        self.groupvalid = ko.observable();
        self.groupvalid1 = ko.observable();
        self._checkValidationGroup = function () {
            var tracker = document.getElementById("tracker");
            if (tracker.valid === "valid")
            {
                return true;
            } else {
                tracker.showMessages();
                
                return false;
            }
        };
         self.disconnected = function () {
            // Implement if needed
        };
       
        self.connected=function(){
            initTranslations();
            app.showHeader("on");
            getPoNumber();
            
            GetSubInv(localStorage.getItem("OrganizationId"));
            var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                     
                     
            
        };
    }
    return normal_deliverContentViewModel;
});