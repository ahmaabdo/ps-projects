define(['ojs/ojcore', 'knockout','ojs/ojbootstrap', 'appController','config/services',
    'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojlistview', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojdialog', 'ojs/ojvalidationgroup','ojs/ojprogress'],
        function (oj, ko,Bootstrap, app,services ,moduleUtils, ArrayDataProvider) {


            function notificationContentViewModel() {
                var self = this;
                self.commentval=ko.observable();
               // self.reassigntoVal=ko.observable();
               app.showHeader("on");
               self.ReAssignUsers_Arr=ko.observableArray();
               self.reassign_user_val=ko.observable();
               self.usertoassign=ko.observable();
              
            self.dataa=ko.observableArray([
        {
            "UOM": "EACH",
            "TO_USER": "SYSADMIN",
            "NOTIFICATION_ID": "5536275",
            "SUBJECT": "Your Approval is Required on Journal",
            "FROM_USER": "BAHAA",
            "ITEM_CODE": "1906011400001",
            "ITEM_DESCRIPTION": "Fixed Wireless, GSM, Voice, Fax, PBX, Atheer Plus Shamel ( Recovered /enterprise )",
            "SENT": "2019-12-18T17:31:04.000+03:00"
        }
    ])
            var ReqId=localStorage.getItem("RequestId");
            self.NTFData=ko.observableArray();
             self.NTFDetailsListData=new ArrayDataProvider(self.NTFData,{keyAttributes:'name'});
            var username= localStorage.getItem("UserName");
            self.GetNotificationDetails=function(){
               
               var GetNTFCBFN=function(data){
                   
                 self.NTFData(data.Notification);
             
               };
//              
                
               services.getGeneric("notification/GetNTFDetails/"+ReqId).then(GetNTFCBFN, app.failCbFn);
           };
           self.backBtn=function(){
               oj.Router.rootInstance.go('notifications');
               
           };
           
                self.ApproveFunc=function(){
                    
                self.ApproveRequest();
                    
                };
                 self.RejectFunc=function(){
                     
                    self.RejectRequest();
                     
                    
                };
                 
                self.ReassignFunc=function(){
                    
                    self.ReAssignUsers();
                    document.querySelector("#reassigndialog").open();
                };
                
                    self.ApproveRequest=function(){
                        
                        var ApproveCBFN=function(data){
                            console.log(data);
                          
                            app.GetNTFCount();
                            
                            if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S") {
                                 oj.Router.rootInstance.go('notifications');
                                app.createMessage("sucess","approved sucessfully");
                            }else{
                                app.createMessage("error ","approve Failed");    
                            }
                        };
                        services.getGeneric("notification/Approve?requestId="+ReqId+"&comment="+self.commentval()).then(ApproveCBFN,app.failCbFn);
                    };
                    
                     self.RejectRequest=function(){
                         
                         var RejectCBFN=function(data){
                            
                             app.GetNTFCount();
                              if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"]["xsi:nil"] == true) {
                                  
                                 oj.Router.rootInstance.go('notifications');
                            app.createMessage("sucess","Rejected");
                            }else{
                                app.createMessage("error ","Reject Failed");    
                            }
                               
                         };
                         
                         services.getGeneric("notification/Reject?requestId="+ReqId+"&comment="+self.commentval()).then(RejectCBFN ,app.failCbFn);
                     };
                     self.CancleRequest=function(){
                         
                         var CancleCBFN=function(data){
                                if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S") {
                                 oj.Router.rootInstance.go('notifications');
                            app.createMessage("sucess","Canceled");
                            }else{
                                app.createMessage("error ","Cancel Failed");    
                            }
                         };
                       services.getGeneric("notification/Cancle?requestId="+ReqId).then(CancleCBFN,app.failCbFn);   
                     };
                     
                       self.ReassignReuest=function(){
                         
                         var ReassignCBFN=function(data){
                              if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S") {
                                 oj.Router.rootInstance.go('notifications');
                            }
                         };
                         
                         
                       services.getGeneric("notification/Reassign?requestid="+ReqId+"&comment="+self.commentval()+"&username="+self.reassign_user_val()).then(ReassignCBFN,app.failCbFn);   
                     };
                
                
                 self.ReAssignUsers=function(){
                         
                         var ReassignUsersCBFN=function(data){
                             for(var i=0;i<data.poNumber.length;i++){
                                 self.ReAssignUsers_Arr.push({"value":data.poNumber[i].USER_NAME,"label":data.poNumber[i].USER_NAME});
                                
                             }
                                
                            
                         };
                       services.getGeneric("notification/ReassignUsers").then(ReassignUsersCBFN,app.failCbFn);   
                     };
                     
                     
                     self.closedialog=function(){
                         
                         
                         document.querySelector("#reassigndialog").close();
                         
                         
                     }
                     self.OkReassign=function(){
                         
                         self.ReassignReuest();
                         
                         
                     }
                
                self.UOMLBL = ko.observable();
                self.TOLBL = ko.observable();
                self.NTFidLbl = ko.observable();
                self.SubjectNTFLbl = ko.observable();
                self.FromNTFLbl = ko.observable();
                self.ItemLBL = ko.observable();
                self.DescLBL = ko.observable();
                self.SentNTFLbl = ko.observable();
                self.acceptNTFLbl = ko.observable();
                self.RejectNTFLbl = ko.observable();
                self.CancleNTFLbl = ko.observable();
                self.ReassignNTFLBL2 = ko.observable();
                self.InfoNTFLbl=ko.observable();
                // added new 
                self.PO_NUMLBL=ko.observable();
                self.QUANTITYLBL=ko.observable();
                self.ORG_IDLBL=ko.observable();
                self.VENDOR_NAMELBL=ko.observable();
                self.commentOrReason=ko.observable();
                
      
        function initTranslations() {
            
            
//             app.getTranslationMethod("UOMLBL", label => {
//                self.UOMLBL(label);
//            });
//            app.getTranslationMethod("TOLBL", label => {
//                self.TOLBL(label);
//            });
//            app.getTranslationMethod("NTFidLbl", label => {
//                self.NTFidLbl(label);
//            });
//            app.getTranslationMethod("SubjectNTFLbl", label => {
//                self.SubjectNTFLbl(label);
//            });
//            app.getTranslationMethod("FromNTFLbl", label => {
//                self.FromNTFLbl(label);
//            });
//            app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//            app.getTranslationMethod("DescLBL", label => {
//                self.DescLBL(label);
//            });
//            app.getTranslationMethod("SentNTFLbl", label => {
//                self.SentNTFLbl(label);
//            });
//            app.getTranslationMethod("acceptNTFLbl", label => {
//                self.acceptNTFLbl(label);
//            });
//            app.getTranslationMethod("RejectNTFLbl", label => {
//                self.RejectNTFLbl(label);
//            });
//            app.getTranslationMethod("CancleNTFLbl", label => {
//                self.CancleNTFLbl(label);
//            });
//            
//             app.getTranslationMethod("ReAssignLbl", label => {
//                self.ReAssignLbl(label);
//            });
//            app.getTranslationMethod("InfoNTFLbl", label => {
//                self.InfoNTFLbl(label);
//            });
            
            self.UOMLBL(app.getTranslationMethod("UOMLBL"));
            self.TOLBL(app.getTranslationMethod("TOLBL"));
            self.NTFidLbl(app.getTranslationMethod("NTFidLbl"));
            self.SubjectNTFLbl(app.getTranslationMethod("SubjectNTFLbl"));
            self.FromNTFLbl(app.getTranslationMethod("FromNTFLbl"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.DescLBL(app.getTranslationMethod("DescLBL"));
            self.SentNTFLbl(app.getTranslationMethod("SentNTFLbl"));
            self.acceptNTFLbl(app.getTranslationMethod("acceptNTFLbl"));
            self.RejectNTFLbl(app.getTranslationMethod("RejectNTFLbl"));
            self.CancleNTFLbl(app.getTranslationMethod("CancleNTFLbl"));
            self.ReassignNTFLBL2(app.getTranslationMethod("ReassignNTFLBL2"));
            self.InfoNTFLbl(app.getTranslationMethod("InfoNTFLbl"));
            // added new 
            self.PO_NUMLBL(app.getTranslationMethod("PO_NUMLBL"));
            self.QUANTITYLBL(app.getTranslationMethod("QUANTITYLBL"));
            self.ORG_IDLBL(app.getTranslationMethod("ORG_IDLBL"));
            self.VENDOR_NAMELBL(app.getTranslationMethod("VENDOR_NAMELBL"));
            self.commentOrReason(app.getTranslationMethod("commentOrReason"));
        }
           
                
            self.connected=function(){
                initTranslations();
                app.showHeader("on");
                self.GetNotificationDetails();
               var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                   
            }
            }
            return notificationContentViewModel;
        });