define(['ojs/ojcore', 'knockout','jquery' ,'config/services','appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton', 'ojs/ojlabel',
    'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojswitch', 'ojs/ojdialog', 'ojs/ojprogress'],
        function (oj, ko, $,services,app, moduleUtils) {



            function ChangeLangViewModel() {
                var self = this;
                self.LanguageValue = ko.observable();
             self.translationArr=ko.observableArray();
                var ss;
                self.switchBtn=ko.observable();
                
                 
                    self.switch_handler=function(){
                         localStorage.setItem('switchvalue',self.switchBtn());
                         

                        
                    };
              
//                self.active_fingerprint(){
//                    
//                    if(localStorage.getItem('switchres')=="true"){
//                        Fingerprint();
//                        
//                    }
//                    
//                }
//                self.active_fingerprint = function () {
//
//                    if (localStorage.getItem('Enabled') == "false" || localStorage.getItem('Enabled') == null) {
//                        localStorage.setItem('Enabled', true);
////                        localStorage.setItem('storeEnable',true);
//                    } else if (localStorage.getItem('Enabled') == "true")
//                        localStorage.setItem('Enabled', false);
//
//                };
//               
               self.backBtn=function(){
    window.history.back();
};

                self.progressValue = ko.observable(0);
                var myData 
                console.log(window.history);
                function changeLang(){
                   
                    $('#basicSelect').val(localStorage.getItem("selectedLanguage"));

                    $('#basicSelect').change(function () {
                        var dropVal = $(this).val();
                        localStorage.setItem("selectedLanguage", dropVal);
});
                };
                self.SaveLang = function () {
                    changeLang();
                };

                self.changeLanguageChangedHandler = function () {
                    app.switchLanguage();
                     
//                     app.getTranslationMethod("SaveLbl", label => {
//                        self.SaveLbl(label);
//                    
//                });
self.SaveLbl(app.getTranslationMethod("SaveLbl"));
            };
            
                 self.SaveLbl = ko.observable();
//                 self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });
        function initTranslations() {
//             app.getTranslationMethod("SaveLbl", label => {
//                        self.SaveLbl(label);
//                    });
            
            self.SaveLbl(app.getTranslationMethod("SaveLbl"));
            
            console.log("saveeeee");
            }
                

                self.connected = function () {
                     changeLang();
                   self.switchBtn( localStorage.getItem('switchvalue') == 'true');
                    console.log("get trans");
                    app.showHeader("off");  
                initTranslations();
                    var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
//                       ss = localStorage.getItem('Enabled');
                    //&&checkAgain
                    // console.log(Enabled);
                   

                };
                self.disconnected = function () {

                };
            }          
            return new ChangeLangViewModel();
        }
);

        