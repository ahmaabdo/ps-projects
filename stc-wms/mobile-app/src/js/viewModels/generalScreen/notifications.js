define(['ojs/ojcore', 'knockout','jquery','ojs/ojbootstrap', 'appController','config/services',
    'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojlistview', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojprogress'],
        function (oj, ko,$,Bootstrap,app,services ,moduleUtils, ArrayDataProvider) {


            function notificationContentViewModel() {
                var self = this;

                self.allitem=ko.observableArray([
        {
            "MESSAGE": "MESSAGE DESCRIPTION",
            "SUBJECT": "Journal has been approved",
            "FROM_USER": "BAHAA",
            "SENT": "2019-11-25T18:49:14.000+03:00"
        }
                ]
       
);

               
             var username;
            self.selectedItems = ko.observable();
             self.NTFData=ko.observableArray();
             self.NTFListData = new ArrayDataProvider(self.NTFData, {keyAttributes: 'name'});
          self.GetNotificationSummary=function(){
               //.Notification
               var GetNTFCBFN=function(data){
                     self.NTFData(data.Notification);
             
               };

               services.getGeneric("notification/getAllNotification?userName="+username).then(GetNTFCBFN, app.failCbFn);
           };
            
           
           self.ckickFunc=function(event, current, bindingContext){
                $("#loader").show();
               localStorage.setItem("RequestId",current.data.REQUEST_ID);
               console.log("reqreqid",current.data.REQUEST_ID);
                                 
                 $("#loader").hide();
               oj.Router.rootInstance.go('NotificationDetails');
               
           };
           
          
               
                self.backBtn = function () {
                    window.history.back();

                };
                self.notfClickAction = function () {
                    oj.Router.rootInstance.go('notifications');

                };
                
                self.MSGNTFLbl=ko.observable();
                self.SubjectNTFLbl=ko.observable();
                self.SentNTFLbl=ko.observable();
                self.TypeNTFLbl=ko.observable();
                self.FromNTFLbl=ko.observable();
                self.PONumTFLbl=ko.observable();
                self.VendorNameNTFLbl=ko.observable();
                
//                self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });
        function initTranslations() {
            
//                    app.getTranslationMethod("MSGNTFLbl", label => {
//                        self.MSGNTFLbl(label);
//                    });
//                    app.getTranslationMethod("SubjectNTFLbl", label => {
//                        self.SubjectNTFLbl(label);
//                    });
//                    app.getTranslationMethod("SentNTFLbl", label => {
//                        self.SentNTFLbl(label);
//                    });
//                    app.getTranslationMethod("TypeNTFLbl", label => {
//                        self.TypeNTFLbl(label);
//                    });
//                    app.getTranslationMethod("FromNTFLbl", label => {
//                        self.FromNTFLbl(label);
//                    });
            
            self.MSGNTFLbl(app.getTranslationMethod("MSGNTFLbl"));
            self.SubjectNTFLbl(app.getTranslationMethod("SubjectNTFLbl"));
            self.SentNTFLbl(app.getTranslationMethod("SentNTFLbl"));
            self.TypeNTFLbl(app.getTranslationMethod("TypeNTFLbl"));
            self.FromNTFLbl(app.getTranslationMethod("FromNTFLbl"));
            self.PONumTFLbl(app.getTranslationMethod("PONumTFLbl"));
            self.VendorNameNTFLbl(app.getTranslationMethod("VendorNameNTFLbl"));
        }
        
//              
         self.connected=function(){
             initTranslations();
             app.showHeader("on");
              username=  localStorage.getItem("UserName");
              console.log("ntf un",username);
             self.GetNotificationSummary();
               var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                   

           };
            }
            return notificationContentViewModel;
        });


