define(['ojs/ojcore', 'knockout', 'appController','config/services','ojs/ojmodule-element-utils','ojs/ojarraydataprovider',
    'ojs/ojknockout','ojs/ojbutton','ojs/ojdialog','ojs/ojprogress'],
        function (oj, ko, app,services,moduleUtils,ArrayDataProvider) {

            function LandingViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable();
               
                initTranslations();
              
//                 self.selectedMenuItem = ko.observable("(None selected yet)");
//                self.pageLbel = ko.observable();
//                self.menuItemAction = function (event)
//                {
//                    self.selectedMenuItem(event.target.value);
//                }.bind(self);
//                
//                self.copy = function () {};
//                self.paste = function () {};
//                self.LOV = function () {};
//                self.generateLPN = function () {};
//                self.delete1 = function () {};
//                self.share = function () {};
//                self.submitForApprove = function () {};

////                self.addFav = function () {
//                    console.log("mainMenu");
//                    console.log(window.location.href);
//                    var path = window.location.href;
//                    console.log(path.indexOf("="));
//                    var pageRoot = path.substring(path.indexOf("=") + 1);
////                    console.log(last);
//                    console.log(window.location.pathname);
//                    console.log(window.location.pathname + window.location.search);
//                    console.log(Router.rootInstance);
//                    var value = Router.rootInstance.states.find(r => r.id === "changeLang").value;
//                    var _label = Router.rootInstance.states.find(r => r.id === "changeLang")._label;
////                      console.log(last,label);
//                    console.log(value);
//                    console.log(_label);
//                    console.log(pageRoot);
//                };
//                self.backBtn=function(){
//                      oj.Router.rootInstance.go('loginVerify');
//                 };
//                 
//                 app.backbtn('loginVerify');
                // Header Config
                
                 self.orgData = ko.observableArray();
                 self.approvalListData = new ArrayDataProvider(self.orgData, {keyAttributes: 'name'});
//                self.BackBtn=function(){ 
//                     oj.Router.rootInstance.go('PO_INQUIRER');
//                };
                self.FeatureList=function(){};
//                self.notification=function(){};
                self.progressValue = ko.observable(0);
                self.clickfunc = function (event, current, bindingContext) {
                    localStorage.setItem("OrganizationId",current.data.ORG_ID);
                    console.log(localStorage.getItem("OrganizationId"));
                    app.getAllPoNumberByOrganizatiOnId(current.data.ORG_ID);
                    var responsibilitySelected=oj.Router.rootInstance.retrieve();
                    oj.Router.rootInstance.go(responsibilitySelected);
//                       document.getElementById('loader').style.display.hide();;                    
//                  self.progressValue.subscribe(function (newValue) {
//                    if (newValue === 100) {
//
//                       self.progressValue(0);
//                       self.progressValue(self.progressValue() + 1);
//                    }
//                  });
//                  window.setInterval(function () {
//                    if (self.progressValue() !== -1) { self.progressValue(self.progressValue() + 1); }
//                  }, 30);
                    //localStorage.setItem("OrganizationId",current.data.INV_ORGANIZATION_ID);
                    //app.getAllPoNumberByOrganizatiOnId(current.data.INV_ORGANIZATION_ID);
                    //oj.Router.rootInstance.go('mainMenu');
                };
                
               
//                 self.notfClickAction=function() {
//                    oj.Router.rootInstance.go('notifications');
//                    
//                };



           self.getAllOrganization=function(){
               
               var OrganizationCbFn=function(data){
                   
                
                   self.orgData(data.Organization);
//                   self.approvalListData = new ArrayDataProvider(self.orgData(), {keyAttributes: 'name'});
                   
               };
               var lang;
                if(localStorage.getItem("selectedLanguage")=="ar"){
                    lang="AR";
                }else{
                    lang="US" 
                }
                
               services.getGeneric("MainServices/Organization?lang="+lang+"&responsibilityId="+localStorage.getItem("responsibilityId")).then(OrganizationCbFn, app.failCbFn);
           };

                self.connected = function () {
                    initTranslations();
                    app.showHeader("on"); 
                    
                    // Implement if needed
                    
                   var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                    self.getAllOrganization();
                     
                };


                self.disconnected = function () {
                    // Implement if needed
                     console.log(app.showHeader());
                   
                };


                self.transitionCompleted = function () {
                    // Implement if needed
                };
                
                /***************** refresh for language ******************/
//                self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        initTranslations();
//                    }
//                });
                /**********************************************************/
                
                function initTranslations() {
                    self.labels({
                        signInlbl: ko.observable(getTranslation("main.signIn")),
                        faqLbl: ko.observable(getTranslation("main.faq")),
                        ourLocationsLbl:ko.observable (getTranslation("main.ourLocation")),
                        offersLbl: ko.observable(getTranslation("main.offers")),
                        languageSwitchLbl: ko.observable(getTranslation("main.language"))});
                      
                }
            }

            return new LandingViewModel();
        }
);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


