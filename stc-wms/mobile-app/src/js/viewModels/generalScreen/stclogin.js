define(['ojs/ojcore', 'knockout', 'appController', 'jquery', 'ojs/ojarraydataprovider', 'config/services', 'ojs/ojknockout', 'ojs/ojselectcombobox', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojtable', 'ojs/ojlabel'
            , 'ojs/ojbutton', 'ojs/ojcheckboxset', 'ojs/ojbootstrap', 'ojs/ojresponsiveutils', 'ojs/ojinputnumber',
    'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojformlayout', 'ojs/ojformlayout', 'appController', 'ojs/ojdialog', 'ojs/ojprogress'],
        function (oj, ko, app, $, ArrayDataProvider, services) {
            function loginContentViewModel() {
                var self = this;
                var a;
                self.userNameValue = ko.observable();
                self.passwordValue = ko.observable();
                 self.remeberMe=ko.observable();
                 self.translationArr=ko.observableArray();
                 self.WordName=ko.observable();
                 self.ArrWord=ko.observable();
                 self.EngWord=ko.observable();
                 
                 var userName;
                    var password;
                self.backBtn = function () {
                    oj.Router.rootInstance.go('changeLang');

                };
                //*** Get User Details ***//
                 
               
                self.GetUserDetails=function(){
              var username=localStorage.getItem("UserName");
             
               var GetUDCBFN=function(data){
                    
            localStorage.setItem("UserId",data.LoginDetails[0].USER_ID);
            app.GetNTFCount();
             oj.Router.rootInstance.go('mainMenu');
               };
               
               services.getGeneric("MainServices/UserLoginDetails?userName="+username).then(GetUDCBFN,app.failCbFn);
           };
           
//               
           
             
                
                self.progressValue = ko.observable(0);
                self.loginMethod = function () {
                    localStorage.setItem("UserName",self.userNameValue());
                    localStorage.setItem("Password",self.passwordValue());
                   
                     self.checkboxListener();
//                    generateOTP(); /////todo
                    
                    app.login(self.userNameValue(), self.passwordValue()); 
                   self.GetUserDetails();
                       $("#loader").show();
                    
                      
                   

                };
                self.disconnected = function () {
                    $('#loader').hide();
                };
                

                self.connected = function () {
                    
                    initTranslations();
                    app.showHeader("off");
                   RetrieveRememberUser();
                    var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                   
                  if(localStorage.getItem("selectedLanguage") === "ar"){
                      self.loginLBL("تسجيل الدخول ");
                      self.rememberLbl("تذكرنى");
                  }else {
                      self.loginLBL("Login");
                      self.rememberLbl("remember me");
                  }
//                    var checkRemember=localStorage.getItem("remeberMe");
//                   
//                    if(userName !=null && password !=null && document.getEle == "true"){
//                        console.log("test .............");
//                        self.userNameValue(userName);
//                        self.passwordValue(password);
//                    }else{
//                        self.userNameValue("");
//                        self.passwordValue("");
//                        
//                    }
                    
                };
                // ----------------define-------------------//
                self.rememberLbl = ko.observable();
                self.usernameLable = ko.observable();
                self.PasswordLable = ko.observable();
                self.loginLBL = ko.observable();
                   
                  

//                //------------------translation---------------//
//                self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        console.log("lang changed");
//                        initTranslations();
//                       
//                    }
//                });
//              

                 function initTranslations()  {
                     
//                    app.getTranslationMethod("remember me", label => {
//                        self.rememberLbl(label);
//                    });
//                    app.getTranslationMethod("username", label => {
//                        self.usernameLable(label);
//                    });
//                    app.getTranslationMethod("Password", label => {
//                        self.PasswordLable(label);
//                    });
//                    app.getTranslationMethod("login", label => {
//                        self.loginLBL(label);
//                    });
                     
                    self.rememberLbl(app.getTranslationMethod("remember"));
                    self.usernameLable(app.getTranslationMethod("username"));
                    self.PasswordLable(app.getTranslationMethod("Password"));
                    self.loginLBL(app.getTranslationMethod("login"));
                   
////                    
                }
             ///************* Translation DB***********///
        
            
      function generateOTP(){
          
          var generateOTPCBFN=function(data){
           console.log("data generate="+JSON.stringify(data));
           if(data.Status=="Success"){
               app.createMessage("success", data.Message);
               console.log("generate success");
          
             oj.Router.rootInstance.go("loginVerify");
           }
           else if(data.Status=="Failed")
           {
               app.createMessage("error", data.Message);
             oj.Router.rootInstance.go("stclogin");
         }
          }
          console.log("geneate");
      services.generateOTP("aosalman.c@stc.com.sa","Oracle@123").then(generateOTPCBFN,app.failCbFn);
//      services.generateOTP(self.userNameValue(),self.passwordValue()).then(generateOTPCBFN,app.failCbFn);
//                    $.ajax({
//
//                        url: 'https://fsso-dev.stc.com.sa/sso/ropcauth',
//                        type: 'POST',
//                        headers: {
//                            'Content-Type':'application/x-www-form-urlencoded'
//                        },
//                        data: {
//                            'username': 'aosalman.c@stc.com.sa',
//                            'password': 'Oracle@123',
//                            'client_id' : 'N93P7UyQEyW4sIT1RZHS'
//                        },
////                        dataType: 'json',
//                        success: function (data) {
//                            alert('Data: ' + JSON.stringify(data));
//                        },
//                        error: function (request, error)
//                        {
//                            alert("Request: " + JSON.stringify(request));
//                        }
//                    });

      }
            
             
//           if($("#remember_me").attr('checked')){
//               
//               var usernamecookie=$('#username').attr("value");
//               var passwordcookie=$('#username').attr("value");
//               $.cookie('username',usernamecookie,{expires:14});
//               $.cookie('password',passwordcookie,{expires:14});
//               $.cookie('remember',true,{expires:14});
//               
//           } else{
//               $.cookie('username',null);
//               $.cookie('password',null);
//               $.cookie('remember',null);
//           }
//           
//           var remember=$.cookie('remember');
//           if(remember==true){
//               
//                usernamecookie=$.cookie('username');
//                passwordcookie=$.cookie('password');
//               
//               $('#username').attr("value",usernamecookie);
//               $('#password').attr("value",passwordcookie)
//               
//           }

self.checkboxListener=function(){
    console.log("value",self.remeberMe());
   if(self.remeberMe()){
       console.log("rememberedd");
    localStorage.setItem('userremember',self.userNameValue());
    localStorage.setItem('passwordremember',self.passwordValue());
       
   }
   else
       console.log("nooo");
   
}



function RetrieveRememberUser(){
   
     self.userNameValue(localStorage.getItem('userremember'));
    self.passwordValue(localStorage.getItem('passwordremember'));
    console.log("retrieved");
    console.log(self.userNameValue());
}
    }
            return loginContentViewModel;
        });
