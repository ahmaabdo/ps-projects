define(['ojs/ojcore', 'knockout', 'jquery','appController','config/services','ojs/ojmodule-element-utils', 'ojs/ojbutton', 'ojs/ojlabel',
    'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata','ojs/ojdialog','ojs/ojprogress'],
        function (oj, ko,$ ,app,services,moduleUtils) {

            function loginVerifyContentViewModel() {
                var self = this;
                self.code1=ko.observable(); 
                self.code2=ko.observable(); 
                self.code3=ko.observable(); 
                self.code4=ko.observable(); 
                var token;
self.backBtn=function(){
    oj.Router.rootInstance.go('stclogin');
    
};
self.progressValue = ko.observable(0);
                self.Verify = function () {

                    if (self.code1() == undefined || self.code2() == undefined || self.code3() == undefined || self.code4() == undefined) {
                        app.createMessage("error", self.errorMessageCodeEmpty());
                    } else {
                        var sessionId = oj.Router.rootInstance.retrieve();
//                        self.checkVerificationLogin(sessionId);
                    }
                    validateOTP();
//                    oj.Router.rootInstance.go('mainMenu');
                    var sessionId=oj.Router.rootInstance.retrieve();
                        $("#loader").show();                    
                  self.progressValue.subscribe(function (newValue) {
                    if (newValue === 100) {

                       self.progressValue(0);
                       self.progressValue(self.progressValue() + 1);
                    }
                  });
                  window.setInterval(function () {
                    if (self.progressValue() !== -1) { self.progressValue(self.progressValue() + 1); }
                  }, 30);
//                    self.checkVerificationLogin(sessionId); 
                    
                };
                self.Resend = function () {

                };
                self.disconnected = function () {
                    // Implement if needed
                    
                };


                //******************check verfication login***************//
                 self.checkVerificationLogin=function(sessionId){
                     var verifiyCbFn=function(data){
                         
                         if(data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"]=="S" ){
                             
                             self.getUserLoginDetails();
                         }else{
                             app.createMessage("error",self.errorMessageVerificationCode());
                             oj.Router.rootInstance.go('loginVerify');
                         }
                         
                         
                     };
                    
                   var verifiyCode=self.code1()+self.code2()+self.code3()+self.code4();
                   
                  services.verification(sessionId+"",verifiyCode).then(verifiyCbFn, self.failCbFn)
                 };


                //*******************get user Login Details*****************//
                
                self.getUserLoginDetails=function(){
                    
                   var UserLoginDetailsdata=function(data){
                      
                       localStorage.setItem("userId",data.LoginDetails[0].USER_ID);
                       oj.Router.rootInstance.go('mainMenu');
                   }; 
                    var userName=localStorage.getItem("UserName");
                  services.getGeneric("MainServices/UserLoginDetails?userName="+userName).then(UserLoginDetailsdata, app.failCbFn);  
                };
                
                $(function(){
                    
                     $('#code1').keypress(function(){
                         //var self1=$(this);
                         $('#code2').focus();
                         
                         
                     });
                     $('#code2').keypress(function(){
                         //var self1=$(this);
                         $('#code3').focus();
                         
                         
                     });
                     $('#code3').keypress(function(){
                         //var self1=$(this);
                         $('#code4').focus();
                         
                         
                     });
                    
                    
                });

                self.connected = function () {
                   
                    // Implement if needed
                    initTranslations();
                    self.getUserLoginDetails();
                    app.showHeader("off");
                    var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                   
                };
         //**************************Define Lablel*******************************//
                 self.verifyLbl=ko.observable();
                 self.receiveCodeLbl=ko.observable();
                 self.resendLbl=ko.observable();
                 self.verifiyYourNumberLbl=ko.observable();
                 self.smsMessageVerificationLBL=ko.observable();
                 self.errorMessageVerificationCode=ko.observable();
                 self.errorMessageCodeEmpty=ko.observable();

//            self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        initTranslations();
//                    }
//                });
              function initTranslations() {
                  
//                   app.getTranslationMethod("verifiy", label => {
//                        self.verifyLbl(label);
//                    });
//                     app.getTranslationMethod("receiveCode", label => {
//                        self.receiveCodeLbl(label);
//                    });
//                    app.getTranslationMethod("resend", label => {
//                        self.resendLbl(label);
//                    });
//                    app.getTranslationMethod("verifiyYourNumber", label => {
//                        self.verifiyYourNumberLbl(label);
//                    });
//                     app.getTranslationMethod("smsMessageVerification", label => {
//                        self.smsMessageVerificationLBL(label);
//                    });
//                     app.getTranslationMethod("errorMessageVerificationCode", label => {
//                        self.errorMessageVerificationCode(label);
//                    });
//                    app.getTranslationMethod("errorMessageCodeEmpty", label => {
//                        self.errorMessageCodeEmpty(label);
//                    });
                   
                 self.verifyLbl(app.getTranslationMethod("verifiy"));
                 self.receiveCodeLbl(app.getTranslationMethod("receiveCode"));
                 self.resendLbl(app.getTranslationMethod("resend"));
                 self.verifiyYourNumberLbl(app.getTranslationMethod("verifiyYourNumber"));
                 self.smsMessageVerificationLBL(app.getTranslationMethod("smsMessageVerification"));
                 self.errorMessageVerificationCode(app.getTranslationMethod("errorMessageVerificationCode"));
                 self.errorMessageCodeEmpty(app.getTranslationMethod("errorMessageCodeEmpty"));
                }    
                
                
                function validateOTP() {

                    var validateOTPCBFN = function (data) {

                        console.log("Success validateOTP");
                        console.log(data);
                        token = data.access_token;
                        validateToken();
                    };
                    var validateOTPFailCBFN = function (data) {
                        app.createMessage("error",data.responseJSON.error_description);
                        $("#loader").hide();
                        console.log(data.responseJSON.error_description);

                    };
                    services.validateOTP(self.code1() + "" + self.code2() + "" + self.code3() + "" + self.code4(), "aosalman.c@stc.com.sa").then(validateOTPCBFN, validateOTPFailCBFN);
//      services.generateOTP("0092",self.userNameValue()).then(generateOTPCBFN,app.failCbFn);
                }
      
                
                
                 function validateToken(){
          
          var validateTokenCBFN=function(data){
              console.log(JSON.stringify(data))
              if(data.status=="error"){
                  app.createMessage("error",data.message)
              oj.Router.rootInstance.go("loginVerify");
          }
          else
              oj.Router.rootInstance.go("mainMenu");
              
              
          }
          
          
          services.validateToken(token).then(validateTokenCBFN,app.failCbFn);
      }
                
            }
            return loginVerifyContentViewModel;
        }
);