define(['ojs/ojcore', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton','ojs/ojdialog','ojs/ojprogress'],
        function (oj, ko, app, moduleUtils) {

            function LandingViewModel() {
                var self = this;
                

                // Header Config
               
                self.notfClickAction = function () {
                    oj.Router.rootInstance.go('notifications');

                };
//                self.backBtn = function () {
//                    oj.Router.rootInstance.go('mainMenu');
//
//                };

                self.Go_Receipt = function () {
                    oj.Router.rootInstance.go('Receipts');
                };
                self.progressValue = ko.observable(0);
                self.Go_Receipt=function(){
                 
                    oj.Router.rootInstance.go('Receipts');};
                self.Go_Return=function(){                    
                 
                  
                  oj.Router.rootInstance.go('Return');};
                self.Go_Deliver=function(){
                  oj.Router.rootInstance.go('Deliver');};
                self.Go_Inspect=function(){
                  oj.Router.rootInstance.go('Inspect');};
                
                
                self.connected = function () {
                    initTranslations();
                     app.showHeader("on"); 
                    
                    // Implement if needed
                   
                    var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                };


                self.disconnected = function () {
                    // Implement if needed
                   
                };

                // define translation 
                self.ReceiptsLBL = ko.observable();
                self.InspectionLBL = ko.observable();
                self.DeliveryLBL = ko.observable();
                self.ReturnLBL = ko.observable();

                /***************** refresh for language ******************/
//                self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        initTranslations();
//                    }
//                });
                /**********************************************************/
                self.numberOfNotification=ko.observable();
                self.numberOfNotification=localStorage.getItem("NotifyNO");
                console.log(self.number);
                function initTranslations() {
                    console.log("submenue");
//                    app.getTranslationMethod("ReceiptsLBL", label => {
//                        self.ReceiptsLBL(label);
//                    });
//                     app.getTranslationMethod("InspectionLBL", label => {
//                        self.InspectionLBL(label);
//                    });
//                    app.getTranslationMethod("DeliveryLBL", label => {
//                        self.DeliveryLBL(label);
//                    });
//                    app.getTranslationMethod("ReturnLBL", label => {
//                        self.ReturnLBL(label);
//                    });
                    self.ReceiptsLBL(app.getTranslationMethod("ReceiptsLBL"));
                    self.InspectionLBL(app.getTranslationMethod("InspectionLBL"));
                    self.DeliveryLBL(app.getTranslationMethod("DeliveryLBL"));
                    self.ReturnLBL(app.getTranslationMethod("ReturnLBL"));

                }
            }

            return new LandingViewModel();
        }
);
