define(['ojs/ojcore', 'knockout','jquery' ,'ojs/ojbootstrap', 'ojs/ojarraydataprovider','appController','config/services',
    'ojs/ojmodule-element-utils', 'ojs/ojknockout', 'ojs/ojlistview', 'ojs/ojbutton', 'ojs/ojlistview','ojs/ojrouter','ojs/ojprogress'],
        function (oj, ko,$ ,Bootstrap, ArrayDataProvider,app,services, moduleUtils,Router) {


            function favoriteContentViewModel() {
                var self=this;
                
               
                self.GetFavData = ko.observableArray();
                self.favoritelistData = new ArrayDataProvider(self.GetFavData, {keyAttributes: 'name'});
                self.selectedItems = ko.observable();
                
             var userId=localStorage.getItem("User_Id");
             var buttons;
              app.showHeader("on");
                  //***Get FAv***//
                
                self.GetFavorite = function () {

                    var GetFAVCBFN = function (data) {

                        self.GetFavData(data.Favourite);
                       // localStorage.setItem("length",data.Favourite.length);
                        var length=data.Favourite.length;
                        if(length==0){
                        
                        document.getElementById("favBtn2").style.display="none";
                        document.getElementById("text").style.display="block";
                    }
                     
                    };

                    services.getGeneric("Favourite/GetFavouriteByUserId?userId="+userId).then(GetFAVCBFN, app.failCbFn);
                };
               
                var pagekey;
                var DelFav;
                
                //self.pagindex=ko.observable();
             
                self.clickFunc = function (event, current, bindingContext) {

                    pagekey = current.data.PAGE_KEY;
                    DelFav = current.data.SEQ;
                    
                   // toggle( self.pagindex());
                 
           };
           
           self.GO=function(){
                oj.Router.rootInstance.go(pagekey);
           };
           
           self.Delete=function(){
           self.DeleteFavoriteData();
           
              
           };
          
         self.progressValue = ko.observable(0);
                self.DeleteFavoriteData = function () {

                    var DeleteCBFN = function () {
                     
                        location.reload();
                        

                        console.log("delete");
                        $("#loader").show();
                    self.progressValue.subscribe(function (newValue) {
                        if (newValue === 100) {
                            self.progressValue(0);
                            self.progressValue(self.progressValue() + 1);
                        }
                    });
                    window.setInterval(function () {
                        if (self.progressValue() !== -1) {
                            self.progressValue(self.progressValue() + 1);
                        }
                    }, 30);
                    };

                    services.getGeneric("Favourite/deleteFavourite?favouriteId="+DelFav).then(DeleteCBFN, app.failCbFn);
                    console.log(DelFav);
                };

                self.connected = function () {
                   
//                    initTranslations();
                    app.showHeader("on"); 
                    
                var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);

                    self.GetFavorite();
                  
                };

            }
            return favoriteContentViewModel;
        });
            
           