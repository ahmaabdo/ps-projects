define(['ojs/ojcore', 'knockout', 'ojs/ojrouter', 'ojs/ojbootstrap', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'config/services', 'ojs/ojlabel', 'jquery', 'ojs/ojknockout', 'ojs/ojlistview', 'ojs/ojoffcanvas', 'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption'],
        function (oj, ko, Router, Bootstrap, app, moduleUtils, ArrayDataProvider, services) {

            function MainMenuViewModel() {
                var self = this;
                //-----------------------------------------------------------------//                
                self.selectedMenuItem = ko.observable("(None selected yet)");
                self.pageLbel = ko.observable();
                self.menuItemAction = function (event)
                {
                    self.selectedMenuItem(event.target.value);
                }.bind(self);
               
                self.copy = function () {};
                self.paste = function () {};
                self.LOV = function () {};
                self.generateLPN = function () {};
                self.delete1 = function () {};
                self.share = function () {};
                self.submitForApprove = function () {};
                
                self.addFav = function () {
                   
                    var path = window.location.href;
                   
                    var pageRoot = path.substring(path.indexOf("=") + 1);

                   
                    var value = Router.rootInstance.states.find(r => r.id === "changeLang").value;
                    var _label = Router.rootInstance.states.find(r => r.id === "changeLang")._label;

                  
                };
                //--------------------------------------------------------//
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable();
                initTranslations();
                // Header Config
                

                self.orgData = ko.observableArray();
                self.approvalListData = new ArrayDataProvider(self.orgData, {keyAttributes: 'name'});

                self.notfClickAction = function () {
                    oj.Router.rootInstance.go('notifications');
                };
//                self.backBtn=function(){
//                      oj.Router.rootInstance.go('loginVerify');
//                 };
//                 
                 self.numberOfNotification=ko.observable();
                self.numberOfNotification=localStorage.getItem("NotifyNO");
                self.progressValue = ko.observable(0);
                 self.clickfunc = function (event, current, bindingContext) {
                 
                    var ResponsibilitySelected;
                    localStorage.setItem("responsibilityId",current.data.RESPONSIBILITY_ID);
                   
                
                         ResponsibilitySelected="PO_INQUIRER";
                        
                        oj.Router.rootInstance.store(ResponsibilitySelected);
                        oj.Router.rootInstance.go("organization");
                    
                    
                   
                     
                  
                };
             
                self.getUserLoginResponsibility = function () {
                      var userId=localStorage.getItem("UserId");
                    var Responsibilitydata = function (data) {
                       
                        self.orgData(data.Responsibility);

                    };
                    var lang;
                    if (localStorage.getItem("selectedLanguage") == "ar") {
                        lang = "AR";
                       
                    } else {
                        lang = "US";
                    }
                    
                    
                    services.getGeneric("MainServices/Responsibility?userId=" + userId+ "&lang=" + lang).then(Responsibilitydata, app.failCbFn);

                };

                self.faqsClickAction = function () {
                         
                 
                    oj.Router.rootInstance.go('subMenu');
                };
                self.orgClickAction = function () {
                   
                    oj.Router.rootInstance.go('organization');
                };


                self.connected = function () {
                    initTranslations();
//                    userId=localStorage.getItem("UserId");
//                    console.log("uid conn",userId);
                     self.getUserLoginResponsibility();
                    // Implement if needed
                    app.showHeader("on");
                    var value = oj.Router.rootInstance.currentValue();
                   
                    var mypage = value.substr(value.indexOf("/") + 1);
                     console.log(mypage);
                    localStorage.setItem("PageValue",mypage);
                   
                   
                  
                };
                self.disconnected = function () {
                    // Implement if needed
                 

                };
                /***************** refresh for language ******************/
//                self.refreshView = ko.computed(function () {
//                    if (app.refreshViewForLanguage()) {
//                        initTranslations();
//                    }
//                });




                /**********************************************************/
                //**Define Translation**//
//                self.PurchasingLBL=ko.observable();
//                self.LPNTransactionLBL=ko.observable();
//                self.LOTTransactionLBL=ko.observable();
//                self.InventoryLBL=ko.observable();
//                self.InQuiryLBL=ko.observable();
//                self.CountLBL=ko.observable();
//                self.OrderManagementLBL=ko.observable();
//                self.WarehouseManagementLBL=ko.observable();
//                self.OrganizationLBL=ko.observable();


                function initTranslations() {
//                    self.PurchasingLBL(app.getTranslationMethod("PurchasingLBL"));
//                    self.LPNTransactionLBL(app.getTranslationMethod("LPNTransactionLBL"));
//                    self.LOTTransactionLBL(app.getTranslationMethod("LOTTransactionLBL"));
//                    self.InventoryLBL(app.getTranslationMethod("InventoryLBL"));
//                    self.InQuiryLBL(app.getTranslationMethod("InQuiryLBL"));
//                    self.CountLBL(app.getTranslationMethod("CountLBL"));
//                    self.OrderManagementLBL(app.getTranslationMethod("OrderManagementLBL"));
//                    self.WarehouseManagementLBL(app.getTranslationMethod("WarehouseManagementLBL"));
//                    self.OrganizationLBL(app.getTranslationMethod("OrganizationLBL"));

                }
            }

            return new MainMenuViewModel();
        }
);
