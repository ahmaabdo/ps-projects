/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * serial_return module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojbootstrap', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojbutton'
], function (oj, ko, Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function serial_returnContentViewModel() {
        var self = this;
        

//        self.headerConfig = ko.observable({'view': [], 'viewModel': null});
//        moduleUtils.createView({'viewPath': 'views/header.html'}).then(function (view) {
//            self.headerConfig({'view': view, 'viewModel': new app.getHeaderModel()});
//        });
        self.from = ko.observable();
        self.to = ko.observable();

        self.serial_done = function () {
            oj.Router.rootInstance.go('returnInformation');
        };
//       
//        
        self.disconnected = function () {
            // Implement if needed
//                    app.showHeader("off");
        };
         self.connected = function () {
                   app.showHeader("on");
                };
    }

    return serial_returnContentViewModel;
});
