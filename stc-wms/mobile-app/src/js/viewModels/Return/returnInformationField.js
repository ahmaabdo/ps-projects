/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * return_information_field module
 */
define(['ojs/ojcore', 'knockout','jquery','util/commonhelper','config/services', 'ojs/ojbootstrap', 'appController',
    'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojbutton','ojs/ojprogress','ojs/ojvalidationgroup'
], function (oj, ko,$,commonUtil,services,Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function return_information_fieldContentViewModel() {
        var self = this;
        var arrfrom=[];
                app.showHeader("on");
self.rej_reason_arr=ko.observableArray();

app.selectItemTOClipboardCombo();
        self.disconnected = function () {
                    // Implement if needed

                };
                
                 self.connected = function () {
                    initTranslations();
                    app.showHeader("on");
                    var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
                   GetRejReason();
                };
                

        
            self.informationReturntModel = {
            poNumber: ko.observable(),
            vendor: ko.observable(),
            item: ko.observable(),
            location: ko.observable(),
            qty: ko.observable(),
            rejreason: ko.observable(),
            rejnotes: ko.observable()
            
                    };
        
         self.informationReturntModel.poNumber(localStorage.getItem('ponumqr'));
        self.informationReturntModel.vendor(localStorage.getItem('vendorqr'));
        self.informationReturntModel.location(localStorage.getItem('locationqr'));
        self.informationReturntModel.item(localStorage.getItem('itemqr'));
        self.informationReturntModel.qty(localStorage.getItem('qtyqr'));
        
        
          function formatDateToday() {
                    var d = new Date(),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [day, month, year].join('-');
                };        
           //  *****************Add RETURN Express ************************//
         self.AddReturnExpress=function(){
             var userId=localStorage.getItem("UserId");
             var payload ={
                "supplier":localStorage.getItem("vendoridqr"), 
                "poNumber": self.informationReturntModel.poNumber(),
                "lineNumber":localStorage.getItem("linenumqr"), 
                "description": localStorage.getItem("itemidqr"),   
                "lpn":localStorage.getItem('lpnqr'),  
                
                "location":localStorage.getItem('locationidqr'),  
                 "qty": self.informationReturntModel.qty(),
                "uom":  localStorage.getItem('uomqr'),
             
                 "transactionType": "RETURN",
                "createdBy":userId,
                "creatationDate": formatDateToday(),
                "lastUpdatedDate": formatDateToday(),
                "lastUpdatedBy":userId,
                "orgId":localStorage.getItem("OrganizationId"),
                "serialArr":arrfrom,
                "qrText":localStorage.getItem("qrtext"),
//                "rejReason":self.informationReturntModel.rejreason(),
//                "rejNotes":self.informationReturntModel.rejnotes()
             };  
             var ReturnExpressCbFn=function(data){
                 
                  if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                         var ReturnNormalCbFn = function (data) {
                        
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                            app.createMessage("sucess",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            $("#loader").hide();
                            self.DataToShare();
                            
                        }else{
                             app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            $("#loader").hide();
                            
                        }
                    };
                    console.log("newpaylod",payload);
                    services.addGeneric(commonUtil.AddPurchasing,JSON.stringify(payload)).then(ReturnNormalCbFn, app.failCbFn);
                   

                }else{
                    app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    $("#loader").hide();
                }
             
             };
             
             services.addGeneric("purchasing/ValidateCreate", JSON.stringify(payload)).then(ReturnExpressCbFn, app.failCbFn);
         };   
      //************************************************************************//  
       
        
        
        self.done = function () {
            var valid = self._checkValidationGroup();
            if(valid){
             $("#loader").show();                    
                  
            self.AddReturnExpress();
            }
            };
            self.DataToShare=function(){
                
            oj.Router.rootInstance.store({action: "express_share"});
            localStorage.setItem("ponum", self.informationReturntModel.poNumber());
            localStorage.setItem("vendor", self.informationReturntModel.vendor());
            localStorage.setItem("location", self.informationReturntModel.item());
            localStorage.setItem("item", self.informationReturntModel.qty());
            localStorage.setItem("qty", self.informationReturntModel.location());
            oj.Router.rootInstance.go('returnInformation');
                
            }
        self.PoNumLbl = ko.observable();
        self.vendorLBL = ko.observable();
        self.ItemLBL = ko.observable();
        self.QtyLBL = ko.observable();
        self.LocationLBL = ko.observable();
        self.EnterLocationLBL = ko.observable();
        
        self.SubmitForApproveLBL = ko.observable();
        self.DoneLBL=ko.observable();
        self.rejreasonLbl=ko.observable();    
                self.rejnotesLbl=ko.observable();

//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });
      
      function GetRejReason(){
            
            var RejReasonCBFN=function(data){
                for(var i=0;i<data["get reassign users"].length;i++){
                    
                    self.rej_reason_arr.push({
                        
                          "value":data["get reassign users"][i].REASON_ID,"label":data["get reassign users"][i].REASON_NAME
                       
                    });
                };
            };
            services.getGeneric("MainServices/getRejReason").then(RejReasonCBFN,app.failCbFn);
        }
        
        self.groupvalid = ko.observable();
        self._checkValidationGroup = function () {
            var tracker = document.getElementById("tracker");

            if (tracker.valid === "valid")
            {
                return true;
            } else {

                tracker.showMessages();
                

                return false;
            }
        };
        
        function initTranslations() {


//            app.getTranslationMethod("PoNumLbl", label => {
//                self.PoNumLbl(label);
//            });
//            app.getTranslationMethod("vendorLBL", label => {
//                self.vendorLBL(label);
//            });
//            app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//            app.getTranslationMethod("QtyLBL", label => {
//                self.QtyLBL(label);
//            });
//            app.getTranslationMethod("LocationLBL", label => {
//                self.LocationLBL(label);
//            });
//            //
//             app.getTranslationMethod("EnterLocationLBL", label => {
//                self.EnterLocationLBL(label);
//            });
////            app.getTranslationMethod("SubInventoryLBL", label => {
////                self.SubInventoryLBL(label);
////            });
//            app.getTranslationMethod("DoneLBL", label => {
//                self.DoneLBL(label);
//            });
//            app.getTranslationMethod("SubmitForApproveLBL", label => {
//                self.SubmitForApproveLBL(label);
//            });
//            app.getTranslationMethod("rejreasonLbl", label => {
//                self.rejreasonLbl(label);
//            });
//            app.getTranslationMethod("rejnotesLbl", label => {
//                self.rejnotesLbl(label);
//            });
                   
                   
            self.PoNumLbl(app.getTranslationMethod("PoNumLbl"));
            self.vendorLBL(app.getTranslationMethod("vendorLBL"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.QtyLBL(app.getTranslationMethod("QtyLBL"));
            self.LocationLBL(app.getTranslationMethod("LocationLBL"));
            self.EnterLocationLBL(app.getTranslationMethod("EnterLocationLBL"));
            
            self.SubmitForApproveLBL(app.getTranslationMethod("SubmitForApproveLBL"));
            self.DoneLBL(app.getTranslationMethod("DoneLBL"));
            self.rejreasonLbl(app.getTranslationMethod("rejreasonLbl"));
                self.rejnotesLbl(app.getTranslationMethod("rejnotesLbl"));
        }


    }

    return return_information_fieldContentViewModel;
});
