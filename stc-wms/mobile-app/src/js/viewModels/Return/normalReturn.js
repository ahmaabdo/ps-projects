/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * normal_return module
 */
define(['ojs/ojcore', 'knockout', 'jquery','util/commonhelper', 'config/services', 'ojs/ojbootstrap', 'appController',
    'ojs/ojmodule-element-utils', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojbutton', 'ojs/ojdialog', 'ojs/ojvalidationgroup', 'ojs/ojprogress'
], function (oj, ko,$ ,commonUtil, services, Bootstrap, app, moduleUtils) {
    /**
     * The view model for the main content view template
     */
    function normal_returnContentViewModel() {
        var self = this;
        var selectFORcoy;
        var selectFORpaste;
        var result;
        var NoOFEnter=0;
        var quantityNo;
        var remain;
        var arrfrom = [];
        self.po_num_Arr = ko.observableArray();
        self.isvisBtnSerial = ko.observable(true);
        self.isvisBtnDone = ko.observable(false);
        self.checkSerialized=ko.observable();
        self.linenum_Arr = ko.observableArray();
        self.LPN_Arr = ko.observableArray();
        self.QtyArr = ko.observableArray();
        self.LocationArr = ko.observableArray();
        self.progressValue = ko.observable(0);
        self.EnterSerialLBL = ko.observable();
        self.SubmitReceitLBL = ko.observable();
        self.rej_reason_arr=ko.observableArray();
       app.selectItemTOClipboardCombo();
       app.selectItemTOClipboardInputText();
        self.PoNumChangedHandler = function (event) {
            self.normalReturnModel.supplier("");
           
           self.normalReturnModel.lineNumber("");
            var poNumberValue = event.detail.value;
            $("#loader").show();
           
            getSupplier(poNumberValue);
            getLineNumber(poNumberValue);

        };
        self.linenumChangedHandler = function (event) {
           self.normalReturnModel.item("");
           self.normalReturnModel.description("");
           self.normalReturnModel.lpn("");
           self.normalReturnModel.location("");
           self.normalReturnModel.uom("");
            var lineNumberValue = event.detail.value;
            $("#loader").show();
           
            if (lineNumberValue != undefined && lineNumberValue!="") {
                getLocation(lineNumberValue);
                getDescription(lineNumberValue);
                getLPN(lineNumberValue);
                GetUom(lineNumberValue);
                
                GetRejReason();
            }

        };
        
         self.LPNChangedHandler = function () {
             self.normalReturnModel.qty("");
              var lpnvalue=event.detail.value;
             $("#loader").show();
            
            if(lpnvalue !=undefined && lpnvalue!=""){
             
             GetQuantity(lpnvalue);
        };
             
             
         };
        self.QtyChangedHandler = function () {};
        self.LocationChangedHandler = function () {};

        self.closedialog = function () {
           result = 0;
            NoOFEnter=0;
            self.remain("");
            document.querySelector("#yesNoDialog").close();

        };
        self.DataToShare = function () {
    
            console.log("data to share return");
            oj.Router.rootInstance.store({action: "manual_share"});
            localStorage.setItem("ponum", self.po_num_Arr().find(e => e.value == self.normalReturnModel.poNumber()).label);
            localStorage.setItem("supplier", self.normalReturnModel.supplier());
            localStorage.setItem("linenum", self.linenum_Arr().find(e => e.value == self.normalReturnModel.lineNumber()).label);
            localStorage.setItem("item", self.normalReturnModel.item());
            localStorage.setItem("desc", self.normalReturnModel.description());
            localStorage.setItem("lpn", self.LPN_Arr().find(e => e.value == self.normalReturnModel.lpn()).label);
            localStorage.setItem("location", self.LocationArr().find(e=>e.value==self.normalReturnModel.location()).label);
            localStorage.setItem("uom", self.normalReturnModel.uom());
            localStorage.setItem("qty", self.normalReturnModel.qty);
            localStorage.setItem("From", self.normalReturnModel.serialFrom());
            document.querySelector("#yesNoDialog").close();
            oj.Router.rootInstance.go('returnInformation');
           

        };
        self.remain = ko.observable();
        self.AddSerialNumber = function () {


 

 var valid = self._checkValidationGroup();
 //var valid1 = self._checkValidationGroup();
            if (valid) {
                // submit the form would go here
                
                
           
            quantityNo = Number(self.normalReturnModel.qty());
            arrfrom[NoOFEnter] = self.normalReturnModel.serialFrom();
            console.log(arrfrom[NoOFEnter]);
            NoOFEnter += 1;
            result = quantityNo - NoOFEnter;
            self.remain(result);
            if (result < quantityNo && result != 0) {
                document.querySelector("#yesNoDialog").close();
                self.normalReturnModel.serialFrom("");
                self.enter_to_serial();

            } else if (result == 0){
                document.querySelector("#yesNoDialog").close();
                self.isvisBtnSerial(false);
                self.isvisBtnDone(true);
                //self.AddRecieiptNormal();
            }
            }
           console.log($('#po_num option:combobox').text());
        };
        self.enter_to_serial = function () {
            self.normalReturnModel.serialFrom("");
            var valid = self._checkValidationGroup();
            if (valid) {
                // submit the form would go here
                
                document.querySelector("#yesNoDialog").open();
            }
        
        }; 

        self.normalReturnModel = {
            poNumber: ko.observable(),
            supplier: ko.observable(),
            supplierValue: ko.observable(),
            lineNumber: ko.observable(),
            item: ko.observable(),
            description: ko.observable(),
            itemAndDescId: ko.observable(),
            //shipment_v: ko.observable('shipment_v'),
            lpn: ko.observable(),
            location: ko.observable(),
            uom: ko.observable(),
            uomValue: ko.observable(),
            qty: ko.observable(),
            serialFrom: ko.observable(),
             rejreason:ko.observable(),
             rejnotes:ko.observable()
           
           
        };
        //******************* Add Return Normal********************************//
//        self.BackBtn = function () {
//            oj.Router.rootInstance.go('Return');
//        };

         function formatDateToday() {
            var d = new Date(),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('-');
        };   
        self.AddReturnNormal = function () {
            var userId=localStorage.getItem("UserId");
            var payload = {
                "poNumber": self.po_num_Arr().find(e => e.value == self.normalReturnModel.poNumber()).label,
                "supplier": self.normalReturnModel.supplierValue(),
                "lineNumber": self.linenum_Arr().find(e => e.value == self.normalReturnModel.lineNumber()).label,
                "item": self.normalReturnModel.itemAndDescId(),
                "description": self.normalReturnModel.itemAndDescId(),
                "lpn": self.normalReturnModel.lpn(),
                "location": self.normalReturnModel.location(),
                "uom": self.normalReturnModel.uomValue(),
                "qty": self.normalReturnModel.qty(),
                "serialFrom": self.normalReturnModel.serialFrom(),
                "transactionType": "RETURN",
                "createdBy":userId,
                "creatationDate": formatDateToday(),
                "lastUpdatedDate": formatDateToday(),
                "lastUpdatedBy": userId,
                "serialArr":arrfrom,
                "rejReason":self.normalReturnModel.rejreason(),
                "rejNotes":self.normalReturnModel.rejnotes(),
                "orgId":localStorage.getItem("OrganizationId")
            };
            var ReturnValidateCbFn = function (data) {
                     if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S") {
                    var RecieiptNormalCbFn = function (data) {
                        
                        if (data["env:Envelope"]["env:Body"]["OutputParameters"]["X_RESULT"] == "S")
                        {
                            app.createMessage("sucess",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"] );
                            document.querySelector("#yesNoSubmitReturnDialog").close();
                            self.DataToShare();
                            
                        }
                    };
                    services.addGeneric(commonUtil.AddPurchasing,JSON.stringify(payload)).then(RecieiptNormalCbFn, app.failCbFn);

                }else{
                    app.createMessage("error",data["env:Envelope"]["env:Body"]["OutputParameters"]["X_MESSAGE"]);
                    document.querySelector("#yesNoSubmitReturnDialog").close();
                    console.log("normal return error");
                }

            };
            services.addGeneric("purchasing/ValidateCreate", JSON.stringify(payload)).then(ReturnValidateCbFn, app.failCbFn);
        };


        $(document).ready(function () {
            $("oj-select-one").click(function () {
                selectFORcoy = this.value;
                selectFORpaste = this;
            });

        });
        
        self.SubmitReceipt = function () {
            document.querySelector("#yesNoSubmitReturnDialog").open();
        };

        self.yesSubmitReturn = function () {
            self.AddReturnNormal();
        };
        self.NOSubmitReturn = function () {
            document.querySelector("#yesNoSubmitReturnDialog").close();
        };
        

        
        self.connected = function () {
            initTranslations();
             app.showHeader("on"); 
            
           
             var value = oj.Router.rootInstance.currentValue();
                    var mypage = value.substr(value.indexOf("/") + 1);
                    localStorage.setItem("PageValue",mypage);
            getPoNumber();
        };


        //****************************************************************************************//
       app.selectItemTOClipboardCombo();
       //*******************************************
        function  getPoNumber() {
            var getAllPoNumberdata = function (data) {
                for (var i = 0; i < data.ReturnPoNumber.length; i++) {
                    self.po_num_Arr.push({
                        "value": data.ReturnPoNumber[i].PO_HEADER_ID, "label": data.ReturnPoNumber[i].PO_NUM

                    });
                }
                $("#loader").hide();
            };
    services.getGeneric("MainServices/returnPoNumber?orgId="+localStorage.getItem("OrganizationId")).then(getAllPoNumberdata, app.failCbFn);
        }

    //*********************get Line Number*****************//
        function  getLineNumber(po_header_id) {

            self.linenum_Arr([]);
            var LineNumberdata = function (data) {

                for (var i = 0; i < data.LineNumber.length; i++) {
                    self.linenum_Arr.push({  
                     "value": data.LineNumber[i].PO_LINE_ID, "label": data.LineNumber[i].PO_LINE_NUMBER
                     });
                       
                }
                
                $("#loader").hide();
            
            };
            services.getGeneric("MainServices/LineNumber?po_header_id=" + po_header_id).then(LineNumberdata, app.failCbFn);
        }

        //*********************get Location*****************//
        function  getLocation(po_header_id) {
            self.normalReturnModel.location("");
            self.LocationArr([]);
            var Locationdata = function (data) {


                for (var i = 0; i < data.Location.length; i++) {
                    self.LocationArr.push({  
                     "value": data.Location[i].LOCATION_ID, "label": data.Location[i].LOCATION_DESCRIPTION
                     }); 
                }
                console.log("data loc",data);
                $("#loader").hide();
               
            };
            services.getGeneric("MainServices/Location?po_header_id="+po_header_id+"&Lang="+"AR" ).then(Locationdata, app.failCbFn);
        }

        //*********************get itemDescription*****************//
        function  getDescription(po_header_id) {
            self.normalReturnModel.item("");
            self.normalReturnModel.description("");
            var ItemDescriptiondata = function (data) {
                for (var i = 0; i < data.ItemDescription.length; i++) {
                    
                    self.normalReturnModel.item(data.ItemDescription[i].ITEM_CODE);
                    self.normalReturnModel.description(data.ItemDescription[i].ITEM_DESCRIPTION);
                    self.normalReturnModel.itemAndDescId(data.ItemDescription[i].ITEM_ID);

                }
                GetSerializedFlag(self.normalReturnModel.itemAndDescId());
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/ItemDescription?po_header_id=" + po_header_id).then(ItemDescriptiondata, app.failCbFn);
        }
     //********************get Lpn****************************//
       function  getLPN(po_header_id) {
           self.LPN_Arr([]);
//           self.normalReturnModel.lpn("");
            var LPNdata = function (data) {
                console.log(data);

                for (var i = 0; i < data.LPN.length; i++) {
                      self.LPN_Arr.push({
                       "value": data.LPN[i].LPN,"label": data.LPN[i].LPN
                        
                    });
                    
                }
                $("#loader").hide();
            };
            services.getGeneric("MainServices/LPN?po_header_id="+po_header_id).then(LPNdata, app.failCbFn);
        }
        
        
   //*********************get GetUom*****************//
        function  GetUom(po_header_id) {
            self.normalReturnModel.uom("");
            var UomCbFn = function (data) {


                for (var i = 0; i < data.Uom.length; i++) {
                   self.normalReturnModel.uom(data.Uom[i].UOM);
                   self.normalReturnModel.uomValue(data.Uom[i].UOM);
                }
               
               
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/GetUom?po_line_id=" + po_header_id).then(UomCbFn, app.failCbFn);
        }
        //*********************get GetQuantity*****************//
        function  GetQuantity(po_header_id) {
            self.QtyArr([]);
            var QuantityCbFn = function (data) {


                for (var i = 0; i < data.QuantityByLpn.length; i++) {

                    self.normalReturnModel.qty(data.QuantityByLpn[i].ITEM_QTY);

                }
                $("#loader").hide();
            
            
            };
            services.getGeneric("MainServices/Quantity?po_line_id="+po_header_id).then(QuantityCbFn, app.failCbFn);
        }
        function  getSupplier(po_header_id) {
            self.normalReturnModel.supplier("");
            var Supplierdata = function (data) {
                for (var i = 0; i < data.supplier.length; i++) {
                    self.normalReturnModel.supplier(data.supplier[i].SUPPLIER_NAME);
                    self.normalReturnModel.supplierValue(data.supplier[i].SUPPLIER_ID);

                }

                $("#loader").hide();

            };
            services.getGeneric("MainServices/Supplier?po_header_id=" + po_header_id).then(Supplierdata, app.failCbFn);
        }
        //****************************get flag to check serialized or not serialized**********************// 
        function  GetSerializedFlag(itemId) {

            var checkSerializedFlagCbFn = function (data) {
                for (var i = 0; i < data.checkSerializedFlag.length; i++) {
                    self.checkSerialized(data.checkSerializedFlag[i].SERIALIZED_FLAG);
                }
                 if(self.checkSerialized()!= "Y"){
                     self.isvisBtnSerial(false);
                     self.isvisBtnDone(true);
                }
                $("#loader").hide();
            };
            services.getGeneric("MainServices/checkSerializedFlag?orgId=" + localStorage.getItem("OrganizationId") + "&itemId=" + itemId).then(checkSerializedFlagCbFn, app.failCbFn);
        }
        
        
        function GetRejReason(){
            
            var RejReasonCBFN=function(data){
                for(var i=0;i<data["get reassign users"].length;i++){
                    
                    self.rej_reason_arr.push({
                        
                           "value":data["get reassign users"][i].REASON_ID,"label":data["get reassign users"][i].REASON_NAME
                        
                        
                    });
                    
                    
                };
                
                
                
            };
            
            
            
            services.getGeneric("MainServices/getRejReason").then(RejReasonCBFN,app.failCbFn);
        }
        
        
        self.disconnected = function () {
            // Implement if needed
//                    app.showHeader("off");
        };
       

        // define 
        self.AddSerialLBL = ko.observable();
        self.EnterSerialHeader = ko.observable();
        self.PoNumLbl = ko.observable();
        self.SupplierLBL = ko.observable();
        self.LineNumLBL = ko.observable();
        self.ItemLBL = ko.observable();
        self.DescLBL = ko.observable();
        self.ShipmentNumLBL = ko.observable();
        self.LPNLBL = ko.observable();
        self.LocationLBL = ko.observable();
        self.UOMLBL = ko.observable();
        self.QtyLBL = ko.observable();
        self.DoneLBL = ko.observable();
        self.EnterSerialLBL = ko.observable();
        self.FromLBL = ko.observable();
        self.TOLBL = ko.observable();
        self.placeholder = ko.observable();
         self.RemainingLbl=ko.observable();
        self.confirmationMessageHeader = ko.observable(); 
        self.ConfirmationMessageBody = ko.observable(); 
        self.yesLbl = ko.observable();
        self.NOLbl = ko.observable();
         self.rejreasonLbl=ko.observable();    
                self.rejnotesLbl=ko.observable();
//        self.refreshView = ko.computed(function () {
//            if (app.refreshViewForLanguage()) {
//                initTranslations();
//            }
//        });
        function initTranslations() {
            
//              app.getTranslationMethod("AddserialNumber", label => {
//                self.AddSerialLBL(label);
//            });
//            app.getTranslationMethod("serialNumber", label => {
//                self.EnterSerialHeader(label);
//            });
//            app.getTranslationMethod("PoNumLbl", label => {
//                self.PoNumLbl(label);
//            });
//            app.getTranslationMethod("SupplierLBL", label => {
//                self.SupplierLBL(label);
//            });
//            app.getTranslationMethod("LineNumLBL", label => {
//                self.LineNumLBL(label);
//            });
//            app.getTranslationMethod("ItemLBL", label => {
//                self.ItemLBL(label);
//            });
//            app.getTranslationMethod("DescLBL", label => {
//                self.DescLBL(label);
//            });
//            app.getTranslationMethod("ShipmentNumLBL", label => {
//                self.ShipmentNumLBL(label);
//            });
//            app.getTranslationMethod("LPNLBL", label => {
//                self.LPNLBL(label);
//            });
//            app.getTranslationMethod("LocationLBL", label => {
//                self.LocationLBL(label);
//            });
//            app.getTranslationMethod("UOMLBL", label => {
//                self.UOMLBL(label);
//            });
//            app.getTranslationMethod("QtyLBL", label => {
//                self.QtyLBL(label);
//            });
//            app.getTranslationMethod("placeholder", label => {
//                self.placeholder(label);
//            });
//            app.getTranslationMethod("DoneLBL", label => {
//                self.DoneLBL(label);
//            });
//            app.getTranslationMethod("EnterSerialLBL", label => {
//                self.EnterSerialLBL(label);
//            });
//             app.getTranslationMethod("FromLBL", label => {
//                self.FromLBL(label);
//            });
//             app.getTranslationMethod("TOLBL", label => {
//                self.TOLBL(label);
//            });
//             app.getTranslationMethod("SubmitReceit", label => {
//                self.SubmitReceitLBL(label);
//            });
//             app.getTranslationMethod("confirmationMessageHeader", label => {
//                self.confirmationMessageHeader(label);
//            });
//             app.getTranslationMethod("ConfirmationMessageBody", label => {
//                self.ConfirmationMessageBody(label);
//            });
//             app.getTranslationMethod("yesLbl", label => {
//                self.yesLbl(label);
//            });
//             app.getTranslationMethod("NOLbl", label => {
//                self.NOLbl(label);
//            });
//             app.getTranslationMethod("RemainingLbl", label => {
//                self.RemainingLbl(label);
//            });
//             app.getTranslationMethod("rejreasonLbl", label => {
//                self.rejreasonLbl(label);
//            });
//             app.getTranslationMethod("rejnotesLbl", label => {
//                self.rejnotesLbl(label);
//            });
            
            self.AddSerialLBL(app.getTranslationMethod("AddserialNumber"));
            self.EnterSerialHeader(app.getTranslationMethod("serialNumber"));
            self.PoNumLbl(app.getTranslationMethod("PoNumLbl"));
            self.SupplierLBL(app.getTranslationMethod("SupplierLBL"));
            self.LineNumLBL(app.getTranslationMethod("LineNumLBL"));
            self.ItemLBL(app.getTranslationMethod("ItemLBL"));
            self.DescLBL(app.getTranslationMethod("DescLBL"));
            self.ShipmentNumLBL(app.getTranslationMethod("ShipmentNumLBL"));
            self.LPNLBL(app.getTranslationMethod("LPNLBL"));
            self.LocationLBL(app.getTranslationMethod("LocationLBL"));
            self.UOMLBL(app.getTranslationMethod("UOMLBL"));
            self.QtyLBL(app.getTranslationMethod("QtyLBL"));
            self.DoneLBL(app.getTranslationMethod("DoneLBL"));
            self.EnterSerialLBL(app.getTranslationMethod("EnterSerialLBL"));
            self.FromLBL(app.getTranslationMethod("FromLBL"));
            self.TOLBL(app.getTranslationMethod("TOLBL"));
            self.placeholder(app.getTranslationMethod("placeholder"));
              self.RemainingLbl(app.getTranslationMethod("RemainingLbl"));
            self.SubmitReceitLBL(app.getTranslationMethod("SubmitReceit"));
            self.confirmationMessageHeader(app.getTranslationMethod("confirmationMessageHeader"));
            self.ConfirmationMessageBody(app.getTranslationMethod("ConfirmationMessageBody"));
            self.yesLbl(app.getTranslationMethod("yesLbl"));
            self.NOLbl(app.getTranslationMethod("NOLbl"));
            self.rejreasonLbl(app.getTranslationMethod("rejreasonLbl"));
                self.rejnotesLbl(app.getTranslationMethod("rejnotesLbl"));
            
        }
        self.groupvalid = ko.observable();
        self.groupvalid1 = ko.observable();
        self._checkValidationGroup = function () {
            var tracker = document.getElementById("tracker");

            if (tracker.valid === "valid")
            {
                return true;
            } else {

                tracker.showMessages();
                

                return false;
            }
        };

    }

    return normal_returnContentViewModel;
});
