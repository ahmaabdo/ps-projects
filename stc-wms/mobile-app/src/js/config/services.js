define(['config/serviceconfig', 'util/commonhelper'], function (serviceConfig, commonHelper) {

    function services() {
        var self = this;
        var biReportServletPath = "report/commonbireport";
        var restPath = commonHelper.getInternalRest();
        var headers = {
//             Authorization: "QURNSU46QURNSU4="
        };
        self.getGeneric = function (serviceName) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.postGeneric = function (serviceName, payload) {
            return serviceConfig.callPostService(restPath + serviceName, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.addGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
         self.deleteGeneric = function (serviceName,headers) {
            var serviceURL = restPath + serviceName;
             return serviceConfig.callDeleteService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers,true);
        };
        
        self.authenticate = function (userName, password) {
            var serviceURL = restPath + "MainServices/validateLogin";
            var payload = {
                "userName": userName, "password": password
            };
            var headers = {
            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
         self.verification = function (sessionId, verifiyCode) {
            var serviceURL = restPath + "MainServices/VerifiyLogin";
            var payload = {
                "sessionId": sessionId, "verifiyCode": verifiyCode
            };
            var headers = {
            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        
        self.generateOTP = function(email, password){
            var serviceURL = "https://fsso-dev.stc.com.sa/sso/ropcauth";  //For pre prod
//            var serviceURL = "https://fsso.stc.com.sa/eai/ropcauth";     // For Production
            var headers = {
                "Content-Type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache"
            };
            var payload = {
                "username": email,
                "password": password,
                "client_id": "N93P7UyQEyW4sIT1RZHS"
            };
            return serviceConfig.callOTPPostService(serviceURL, payload, 'application/x-www-form-urlencoded', true, headers);
        };
        self.validateOTP = function(otpCode, email){
            var serviceURL = "https://fsso-dev.stc.com.sa/mga/sps/oauth/oauth20/token";  //For pre prod
//            var serviceURL = "https://fsso.stc.com.sa/mga/sps/oauth/oauth20/token";     // For Production
            var headers = {
                "Content-Type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache"
            };
            var payload = {
            "response_type":"token",
            "grant_type":"password",
            "client_id":"N93P7UyQEyW4sIT1RZHS", //for pre prod
            "client_secret":"Nwlwk9EtFLET5fmVY1N9", //for pre prod
            "username":email,
            "password":otpCode
            };
            return serviceConfig.callOTPPostService(serviceURL, payload, "application/x-www-form-urlencoded", true, headers);
        };
        self.validateToken = function(token){
            var serviceURL = restPath+"otp/getIdentity?token="+token;
            var headers = {
            };

            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        
    }
    
    
    
    
    
    
    

    return new services();
});