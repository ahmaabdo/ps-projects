define([], function () {

    function commonHelper() {
        var self = this;
        self.getUsers = "users/getUsers";
        self.addUser="users/ADD";    
        self.incidents="incidents";
        self.contact="contact";
        self.customRest="customRest";
        self.get="GET";
        self.post="POST";
        self.getPayload="null";
        self.AddRecieiptExpress="purchasing/Receipt";
        self.AddPurchasing="purchasing/createPurchasing";
        
        self.AddReturnExpress="purchasing/Return";
        self.AddReturnNormal="purchasing/Return";
        
        self.AddInspectExpress="purchasing/Inspect";
        self.AddInspectNormal="purchasing/Inspect";
        
        self.AddDeliverExpress="purchasing/Deliver";
        self.AddDeliverNormal="purchasing/Deliver";
        
       
        self.getInternalRest = function () {
         
         //var host = "http://localhost:8081/stcwms-rest/";
         var host = "https://sitehubdev.stc.com.sa/stcwms-rest/";

            return host;
        };

    }

    return new commonHelper();
});