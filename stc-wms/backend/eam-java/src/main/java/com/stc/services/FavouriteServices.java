/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stc.controller.FavouriteDAO;
import com.stc.objects.FavouriteBean;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author lenovo
 */
@Path("Favourite")
public class FavouriteServices {
        FavouriteDAO  favouritecontroller = new FavouriteDAO();
   //********************************* Add Favourite*************************************************//   
        
    @Path("AddFavourite")
    @POST 
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response AddFavourite(String favouriteBean){
         String result = null;
    
        try {
            ObjectMapper mapper = new ObjectMapper();

            FavouriteBean bean
                    = mapper.readValue(favouriteBean, FavouriteBean.class);

            result = favouritecontroller.AddFavorite(bean);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    //****************************** Delete Favourite ******************************************//
    @GET
    @Path("deleteFavourite")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response DeleteFavourite(@QueryParam("favouriteId") String favouriteId ,@QueryParam("lang") String lang) {
        String result = null;
        try {
          result = favouritecontroller.DeleteFavorite(favouriteId ,lang);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }

    }
   //************************************ Get Favourite by user Id ***********************************************//     
    @GET
    @Path("GetFavouriteByUserId")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response GetFavouriteByUserId(@QueryParam("userId") String userId) {
          String result=null;
        try {
           result = favouritecontroller.getFavorite(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    
  }
