/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import com.db.StcWmsConnection;
import static com.stc.controller.StcWmsMainController.logger;
import static com.stc.controller.WorkOrderController.logger;
import static com.stc.controller.WorkOrderController.pool;
import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.FavouriteBean;
import com.stc.objects.PurchasingBean;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author lenovo
 */
public class FavouriteDAO {

    final static Logger logger = Logger.getLogger(NotificationController.class);
    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public NamedParameterStatement namedParameterStatement;
    public PreparedStatement preparedStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;

    //-------------------------AddFavorite-----------------------------------------------------------------------//
    public String AddFavorite(FavouriteBean favourite) {

        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/add_favorite/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/add_favorite/");
            Map<String, Object> parent = new LinkedHashMap<String, Object>();
            Map<String, String> param = new LinkedHashMap<String, String>();
            param.put("USER_ID", favourite.getUserId());
            param.put("PAGE_KEY", favourite.getPageKey());
            param.put("CREATION_DATE", favourite.getCreationDate());
            param.put("ARABIC_MEANING", favourite.getArabicMeaning());
            param.put("ENGLISH_MEANING", favourite.getEnglishMeaning());
           

            parent.put("P_REC_FAV", param);
            seo.setParameter(parent);
            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Favourite Error", e);
        }
        return result;
    }

    //-------------------------DeleteFavorite-----------------------------------------------------------------------//
    public String DeleteFavorite(String p_seq, String lang) {

        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/delete_favorite/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/delete_favorite/");
            Map<String, Object> param = new LinkedHashMap<String, Object>();
            param.put("P_SEQ", p_seq);
            param.put("P_LANG", lang);

            seo.setParameter(param);
            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Delete Favourite Error", e);
        }
        return result;
    }

    // *****************************GET Favourite******************************//
    public String getFavorite(String userId) {

        String result = null;

        String statement = "SELECT SEQ,\n"
                + "       USER_ID,\n"
                + "       RESP_ID,\n"
                + "       PAGE_KEY,\n"
                + "       CREATION_DATE,\n"
                + "       ARABIC_MEANING,\n"
                + "       ENGLISH_MEANING\n"
                + "  FROM XXWMS_FAVORITE_PAGES_TBL\n"
                + " WHERE USER_ID = '" + userId + "'";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Favourite");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Favourite Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet, callableStatement);
        }
        return result;

    }

    void closeAllConnection(Connection connection,
            PreparedStatement preparedStatement,
            ResultSet resultSet,
            CallableStatement callableStatement) {

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

    }

}
