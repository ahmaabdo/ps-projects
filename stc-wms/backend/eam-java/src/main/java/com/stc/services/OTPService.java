/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.OTPController;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Anas Alghawi
 */
@Path("/otp")
public class OTPService {
    
    @GET
    @Path("getIdentity")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces("application/json")
    public Response getIdentity(@QueryParam("token") String token) throws IOException, NoSuchAlgorithmException, KeyManagementException {

        if (token == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            OTPController stcWebService = new OTPController();
            result = stcWebService.getIdentity(token);
             System.out.println(result);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
                
            }
           
        }
    }
    
    public static void main(String args[]) throws IOException, NoSuchAlgorithmException, KeyManagementException{
        
        OTPService a= new OTPService();
        a.getIdentity("vQKBv2e2rYq4z9fSgc8D");
    }
    
}
