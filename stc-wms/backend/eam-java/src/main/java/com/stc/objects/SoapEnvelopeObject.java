package com.stc.objects;

import java.util.HashMap;
import java.util.Map;

public class SoapEnvelopeObject {
    String header;
    String username;
    String password;
    String soapurl;
    Map<String,Object> parameter = new HashMap<String,Object>();
    
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSoapurl() {
        return soapurl;
    }

    public void setSoapurl(String soapurl) {
        this.soapurl = soapurl;
    }

    public Map<String, Object> getParameter() {
        return parameter;
    }

    public void setParameter(Map<String, Object> parameter) {
        this.parameter = parameter;
    }
}




