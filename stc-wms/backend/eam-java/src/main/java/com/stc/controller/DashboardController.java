/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import static com.stc.controller.WorkOrderController.logger;
import static com.stc.controller.WorkOrderController.pool;
import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class DashboardController {

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public PreparedStatement preparedStatement;
    public NamedParameterStatement namedParameterStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;
    public ResultSetMetaData resultSetMetaData;

    final static Logger logger = Logger.getLogger(DashboardController.class);

    public String wrCount(String wrType,
            String userID,
            String respId,
            String respAppId,
            String orgId
            ) {

        String result = null;
        int columnCount;
        String columnName;
        String statement = "SELECT COUNT (*) wr_count\n"
                + "  FROM wip_eam_work_requests wr, fnd_lookup_values lv\n"
                + " WHERE     lv.lookup_type = 'WIP_EAM_WORK_REQ_STATUS'\n"
                + "       AND language = 'US'\n"
                + "       AND wr.WORK_REQUEST_STATUS_ID = lv.lookup_code\n"
                + "  AND organization_id = '"+orgId+"' \n"
                + "       AND (   ('"+wrType+"' = 'WR')\n"
                + "            OR (wr.attribute15 IS NULL AND '"+wrType+"' = 'INT')\n"
                + "            OR (    wr.attribute15 IS NULL\n"
                + "                AND wr.ATTRIBUTE2 = 'DRAFT'\n"
                + "                AND lv.meaning NOT IN ('Rejected', 'Cancelled', 'Complete')\n"
                + "                AND '"+wrType+"' = 'INT-DRAFT')\n"
                + "            OR (    wr.attribute15 IS NULL\n"
                + "                AND wr.ATTRIBUTE2 LIKE 'INP%'\n"
                + "                AND lv.meaning NOT IN ('Rejected', 'Cancelled', 'Complete')\n"
                + "                AND '"+wrType+"' = 'INT-INPROCESS')\n"
                + "            OR (    wr.attribute15 IS NULL\n"
                + "                AND (lv.meaning = 'Rejected' or wr.ATTRIBUTE2 like 'R%')\n"
                + "                AND '"+wrType+"' = 'INT-REJECTED')\n"
                + "            OR (    wr.attribute15 IS NULL\n"
                + "                AND lv.meaning = 'Cancelled'\n"
                + "                AND '"+wrType+"' = 'INT-CANCELED')\n"
                + "            OR (    wr.attribute15 IS NULL\n"
                + "                AND lv.meaning = 'Awaiting Work Order'\n"
                + "                AND '"+wrType+"' = 'INT-AWAITING')\n"
                + "            OR (    wr.attribute15 IS NULL\n"
                + "                AND lv.meaning = 'On Work Order'\n"
                + "                AND '"+wrType+"' = 'INT-ON-WO')\n"
                + "            OR (    wr.attribute15 IS NULL\n"
                + "                AND lv.meaning = 'Complete'\n"
                + "                AND '"+wrType+"' = 'INT-COMPLETE')\n"
                + "            OR (    wr.attribute15 IS NOT NULL\n"
                + "               AND '"+wrType+"' = 'IGATE')\n"
                + "            OR (    wr.attribute15 IS NOT NULL\n"
                + "                AND lv.meaning = 'Cancelled'\n"
                + "                AND '"+wrType+"' = 'IGATE-CANCELED')\n"
                + "            OR (    wr.attribute15 IS NOT NULL\n"
                + "                AND lv.meaning = 'Awaiting Work Order'\n"
                + "                AND '"+wrType+"' = 'IGATE-AWAITING')\n"
                + "            OR (    wr.attribute15 IS NOT NULL\n"
                + "                AND lv.meaning = 'Complete'\n"
                + "                AND '"+wrType+"' = 'IGATE-COMPLETE')\n"
                + "            OR (    wr.attribute15 IS NOT NULL\n"
                + "                AND lv.meaning = 'On Work Order'\n"
                + "                AND '"+wrType+"' = 'IGATE-ON-WO'))\n"
                + "       AND (   'Y' = 'N'\n"
                + "            OR (    'Y' = 'Y'\n"
                + "                AND (   (       (SELECT NVL (\n"
                + "                                           fnd_profile.VALUE (\n"
                + "                                              'XXX_SCM_EAM_SUPERVISOR'),\n"
                + "                                           'N')\n"
                + "                                   FROM DUAL) = 'Y'\n"
                + "                            AND (SELECT NVL (\n"
                + "                                           fnd_profile.VALUE (\n"
                + "                                              'XXX_EAM_IGATE_SUPERUSER'),\n"
                + "                                           'N')\n"
                + "                                   FROM DUAL) = 'N'\n"
                + "                         OR (    (SELECT NVL (\n"
                + "                                            fnd_profile.VALUE (\n"
                + "                                               'XXX_SCM_EAM_SUPERVISOR'),\n"
                + "                                            'N')\n"
                + "                                    FROM DUAL) <> 'Y'\n"
                + "                             AND (SELECT NVL (\n"
                + "                                            fnd_profile.VALUE (\n"
                + "                                               'XXX_EAM_IGATE_SUPERUSER'),\n"
                + "                                            'N')\n"
                + "                                    FROM DUAL) = 'N'\n"
                + "                             AND wr.work_request_number IN\n"
                + "                                    (SELECT DISTINCT work_request_number\n"
                + "                                       FROM xx_mtl_eam_asset_numbers_all_v cii,\n"
                + "                                            stc.xxx_stc_eam_approvers app,\n"
                + "                                            xxx_stc_eam_approvers_dept_v dept,\n"
                + "                                            fnd_flex_values_vl fv,\n"
                + "                                            fnd_flex_value_sets fvs,\n"
                + "                                            fnd_flex_values_vl fv1,\n"
                + "                                            fnd_flex_value_sets fvs1,\n"
                + "                                            bom_departments bd,\n"
                + "                                            org_organization_definitions org,\n"
                + "                                            wip_eam_work_requests wdj\n"
                + "                                      WHERE     fv.flex_value_set_id =\n"
                + "                                                   fvs.flex_value_set_id\n"
                + "                                            AND fvs.flex_value_set_name =\n"
                + "                                                   'XXSCM_Zone'\n"
                + "                                            AND fv.flex_value =\n"
                + "                                                   cii.attribute1\n"
                + "                                            AND dept.zone_id =\n"
                + "                                                   fv.flex_value_id\n"
                + "                                            AND dept.approver_dept_id =\n"
                + "                                                   app.approver_dept_id\n"
                + "                                            AND org.organization_id =\n"
                + "                                                   dept.organization_id\n"
                + "                                            AND org.organization_code = 'E00'\n"
                + "                                            AND wdj.work_request_owning_dept =\n"
                + "                                                   bd.department_id\n"
                + "                                            AND bd.attribute1 =\n"
                + "                                                   fv1.flex_value\n"
                + "                                            AND fv1.flex_value_id =\n"
                + "                                                   dept.department_id\n"
                + "                                            AND fv1.flex_value_set_id =\n"
                + "                                                   fvs1.flex_value_set_id\n"
                + "                                            AND fvs1.flex_value_set_name =\n"
                + "                                                   'XXSCM_Departments'\n"
                + "                                            AND dept.position_name IN\n"
                + "                                                   ('ABFM', 'BFM')\n"
                + "                                            AND wdj.attribute15 IS NULL\n"
                + "                                            AND wdj.work_request_number =\n"
                + "                                                   wr.work_request_number\n"
                + "                                            AND cii.instance_number =\n"
                + "                                                   (SELECT CASE\n"
                + "                                                              WHEN INSTR (\n"
                + "                                                                      SUBSTR (\n"
                + "                                                                         c.instance_number,\n"
                + "                                                                         8,\n"
                + "                                                                         5),\n"
                + "                                                                      '-') BETWEEN 1\n"
                + "                                                                               AND 4\n"
                + "                                                              THEN\n"
                + "                                                                 RTRIM (\n"
                + "                                                                    SUBSTR (\n"
                + "                                                                       c.instance_number,\n"
                + "                                                                       1,\n"
                + "                                                                       10),\n"
                + "                                                                    '-')\n"
                + "                                                              WHEN INSTR (\n"
                + "                                                                      SUBSTR (\n"
                + "                                                                         c.instance_number,\n"
                + "                                                                         8,\n"
                + "                                                                         5),\n"
                + "                                                                      '-') >\n"
                + "                                                                      4\n"
                + "                                                              THEN\n"
                + "                                                                 RTRIM (\n"
                + "                                                                    SUBSTR (\n"
                + "                                                                       c.instance_number,\n"
                + "                                                                       1,\n"
                + "                                                                       11),\n"
                + "                                                                    '-')\n"
                + "                                                              ELSE\n"
                + "                                                                 c.instance_number\n"
                + "                                                           END\n"
                + "                                                      FROM xx_mtl_eam_asset_numbers_all_v c\n"
                + "                                                     WHERE     current_organization_id =\n"
                + "                                                                  wr.organization_id\n"
                + "                                                           AND c.serial_number =\n"
                + "                                                                  (SELECT asset_number\n"
                + "                                                                     FROM wip_eam_work_requests wdj1\n"
                + "                                                                    WHERE     wdj1.work_request_number =\n"
                + "                                                                                 wr.work_request_number\n"
                + "                                                                          AND wdj1.organization_id =\n"
                + "                                                                                 wr.organization_id))\n"
                + "                                            AND app.user_id =\n"
                + "                                                   (SELECT TO_CHAR (\n"
                + "                                                              fnd_profile.VALUE (\n"
                + "                                                                 'USER_ID'))\n"
                + "                                                      FROM DUAL))))\n"
                + "                     OR (    (SELECT NVL (\n"
                + "                                        fnd_profile.VALUE (\n"
                + "                                           'XXX_EAM_IGATE_SUPERUSER'),\n"
                + "                                        'N')\n"
                + "                                FROM DUAL) = 'Y'\n"
                + "                         AND wr.work_request_number IN\n"
                + "                                (SELECT DISTINCT work_request_number\n"
                + "                                   FROM wip_eam_work_requests wdj\n"
                + "                                  WHERE     wdj.attribute15 IS NOT NULL\n"
                + "                                        AND wdj.work_request_number =\n"
                + "                                               wr.work_request_number))\n"
                + "                     OR (   (SELECT NVL (\n"
                + "                                       fnd_profile.VALUE (\n"
                + "                                          'XXX_EAM_CONT_SUPERUSER'),\n"
                + "                                       'N')\n"
                + "                               FROM DUAL) = 'Y'\n"
                + "                         OR (    (SELECT NVL (\n"
                + "                                            fnd_profile.VALUE (\n"
                + "                                               'XXX_EAM_IGATE_SUPERUSER'),\n"
                + "                                            'N')\n"
                + "                                    FROM DUAL) = 'N'\n"
                + "                             AND (SELECT NVL (\n"
                + "                                            fnd_profile.VALUE (\n"
                + "                                               'XXX_EAM_CONT_SUPERUSER'),\n"
                + "                                            'N')\n"
                + "                                    FROM DUAL) <> 'Y'\n"
                + "                             AND wr.work_request_number IN\n"
                + "                                    (SELECT DISTINCT work_request_number\n"
                + "                                       FROM stc.xxx_stc_eam_approvers app,\n"
                + "                                            xxx_stc_eam_approvers_dept_v dept,\n"
                + "                                            org_organization_definitions org,\n"
                + "                                            wip_eam_work_requests wdj\n"
                + "                                      WHERE     dept.approver_dept_id =\n"
                + "                                                   app.approver_dept_id\n"
                + "                                            AND org.organization_id =\n"
                + "                                                   dept.organization_id\n"
                + "                                            AND org.organization_code = 'E00'\n"
                + "                                            AND dept.position_name =\n"
                + "                                                   'CONT_SPEC_ENGINEER'\n"
                + "                                            AND dept.sub_department_id =\n"
                + "                                                   wdj.work_request_owning_dept\n"
                + "                                            AND wdj.work_request_number =\n"
                + "                                                   wr.work_request_number\n"
                + "                                            AND app.user_id =\n"
                + "                                                   (SELECT TO_CHAR (\n"
                + "                                                              fnd_profile.VALUE (\n"
                + "                                                                 'USER_ID'))\n"
                + "                                                      FROM DUAL)))))))";
        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_USER_ID", userID);
            param.put("P_RESP_ID", respId);
            param.put("P_RESP_APPL_ID", respAppId);
            param.put("P_ORG_ID", orgId);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "WORK_REQUEST");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("WORK_REQUEST");
            System.out.println("/////////**///////////qqqq" + jsonArray.length());
            
            JSONObject jsonObject1 = new JSONObject();
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject1.put("WR_COUNT", jsonArray.getJSONObject(i).get("WR_COUNT"));
 
            }

             System.out.println("/////////**///////////" + result);
            result = jsonObject1.toString();
            System.out.println(result);
            
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("SOA Error", e);
            logger.error("QUERY : " + statement);
        }

        return result;

    }
//        try {
//            connection = pool.getConnection();
//            preparedStatement = connection.prepareStatement(statement);
//            preparedStatement.setString(1, pWrType);
//            preparedStatement.setString(2, pWrType);
//            preparedStatement.setString(3, pWrType);
//            preparedStatement.setString(4, pWrType);
//            preparedStatement.setString(5, pWrType);
//            preparedStatement.setString(6, pWrType);
//            preparedStatement.setString(7, pWrType);
//            preparedStatement.setString(8, pWrType);
//            preparedStatement.setString(9, pWrType);
//            preparedStatement.setString(10, pWrType);
//            preparedStatement.setString(11, pWrType);
//            preparedStatement.setString(12, pWrType);
//            preparedStatement.setString(13, pWrType);
//
//            resultSet = preparedStatement.executeQuery();
//            resultSetMetaData = resultSet.getMetaData();
//            columnCount = resultSetMetaData.getColumnCount();
//
//            JsonArray jsonArray = new JsonArray();
//            while (resultSet.next()) {
//                JsonObject jsonObject = new JsonObject();
//                for (int i = 1; i <= columnCount; i++) {
//                    columnName = resultSetMetaData.getColumnName(i);
//                    jsonObject.addProperty(columnName, resultSet.getString(columnName));
//                }
//                jsonArray.add(jsonObject);
//            }
//
//            result = jsonArray.toString();
//            System.out.println("WR_COUNT= \n" + result);
//
//            connection.close();
//            preparedStatement.close();
//            resultSet.close();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error("Get WR_COUNT Error", ex);
//        } finally {
//            closeAllConnection(connection, preparedStatement, resultSet, callableStatement, namedParameterStatement);
//        }
//        return result;
//    }

    public String woCount(String woType, String userID,
            String respId,
            String respAppId,
            String orgId
            ) {

        String result = null;
        int columnCount;
        String columnName;
        String statement = "SELECT COUNT (*) wo_count\n"
                + "  FROM wip_discrete_jobs wdj, fnd_lookup_values lv,wip_entities wip_ent\n"
                + " WHERE     lv.lookup_code = wdj.status_type\n"
                + "       AND wdj.organization_id = '"+orgId+"'  \n"
                + "       AND wdj.wip_entity_id = wip_ent.wip_entity_id\n"
                + "       AND lv.lookup_type = 'WIP_JOB_STATUS'\n"
                + "       AND lv.language = 'US'\n"
                + "       AND (   ('"+woType+"' = 'WO')\n"
                + "            OR (nvl(wdj.PLAN_MAINTENANCE,'N') = 'Y' AND '"+woType+"' = 'WO-PMR')\n"
                + "            OR (nvl(wdj.PLAN_MAINTENANCE,'N') = 'N' AND '"+woType+"' = 'WO-CMR')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('A1', 'INP1', 'R1')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-CREATION')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('A2', 'INP2', 'R2')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-SPR')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('A3', 'INP3', 'R3')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-COMPLETE')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND (wdj.ATTRIBUTE15 = 'DRAFT' OR lv.meaning = 'Cancelled')\n"
                + "                AND '"+woType+"' = 'WO-CMR-OTHER')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('A1')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-CREATION-A')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('INP1')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-CREATION-INP')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('R1')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-CREATION-R')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('A2')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-SPR-A')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('INP2')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-SPR-INP')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('R2')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-SPR-R')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('A3')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-COMPLETE-A')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('INP3')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-COMPLETE-INP')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('R3')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-COMPLETE-R')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND wdj.ATTRIBUTE15 NOT IN ('DRAFT')\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-OTHER')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND (wdj.ATTRIBUTE15 = 'DRAFT' AND lv.meaning <> 'Cancelled')\n"
                + "                AND '"+woType+"' = 'WO-CMR-OTHER-DRAFT')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'N'\n"
                + "                AND lv.meaning = 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-CMR-OTHER-CANCELED')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'Y'\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND wdj.ATTRIBUTE15 = 'A1'\n"
                + "                AND '"+woType+"' = 'WO-PMR-CREATION')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'Y'\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('A3', 'INP3', 'R3')\n"
                + "                AND '"+woType+"' = 'WO-PMR-COMPLETION')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'Y'\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('A3')\n"
                + "                AND '"+woType+"' = 'WO-PMR-COMPLETION-A')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'Y'\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('INP3')\n"
                + "                AND '"+woType+"' = 'WO-PMR-COMPLETION-INP3')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'Y'\n"
                + "                AND lv.meaning <> 'Cancelled'\n"
                + "                AND wdj.ATTRIBUTE15 IN ('R3')\n"
                + "                AND '"+woType+"' = 'WO-PMR-COMPLETION-R3')\n"
                + "            OR (    nvl(wdj.PLAN_MAINTENANCE,'N') = 'Y'\n"
                + "                AND lv.meaning = 'Cancelled'\n"
                + "                AND '"+woType+"' = 'WO-PMR-OTHER'))\n"
                + "       AND (   (    'Y' = 'Y'\n"
                + "                AND (   (       (SELECT NVL (\n"
                + "                                           fnd_profile.VALUE (\n"
                + "                                              'XXX_SCM_EAM_SUPERVISOR'),\n"
                + "                                           'N')\n"
                + "                                   FROM DUAL) = 'Y'\n"
                + "                            AND (SELECT NVL (\n"
                + "                                           fnd_profile.VALUE (\n"
                + "                                              'XXX_EAM_IGATE_SUPERUSER'),\n"
                + "                                           'N')\n"
                + "                                   FROM DUAL) = 'N'\n"
                + "                         OR (    (SELECT NVL (\n"
                + "                                            fnd_profile.VALUE (\n"
                + "                                               'XXX_SCM_EAM_SUPERVISOR'),\n"
                + "                                            'N')\n"
                + "                                    FROM DUAL) <> 'Y'\n"
                + "                             AND (SELECT NVL (\n"
                + "                                            fnd_profile.VALUE (\n"
                + "                                               'XXX_EAM_IGATE_SUPERUSER'),\n"
                + "                                            'N')\n"
                + "                                    FROM DUAL) = 'N'\n"
                + "                             AND wip_ent.wip_entity_name IN\n"
                + "                                    (SELECT DISTINCT wip_entity_name\n"
                + "                                       FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V cii,\n"
                + "                                            STC.XXX_STC_EAM_APPROVERS app,\n"
                + "                                            XXX_STC_EAM_APPROVERS_DEPT_V dept,\n"
                + "                                            FND_FLEX_VALUES_VL FV,\n"
                + "                                            FND_FLEX_VALUE_SETS FVS,\n"
                + "                                            FND_FLEX_VALUES_VL FV1,\n"
                + "                                            FND_FLEX_VALUE_SETS FVS1,\n"
                + "                                            BOM_DEPARTMENTS BD,\n"
                + "                                            org_organization_definitions org,\n"
                + "                                            wip_discrete_jobs wdj,\n"
                + "                                            wip_entities we\n"
                + "                                      WHERE     FV.FLEX_VALUE_SET_ID =\n"
                + "                                                   FVS.FLEX_VALUE_SET_ID\n"
                + "                                            AND FVS.FLEX_VALUE_SET_NAME =\n"
                + "                                                   'XXSCM_Zone'\n"
                + "                                            AND FV.FLEX_VALUE =\n"
                + "                                                   cii.attribute1\n"
                + "                                            AND dept.zone_id =\n"
                + "                                                   FV.FLEX_VALUE_ID\n"
                + "                                            AND dept.APPROVER_DEPT_ID =\n"
                + "                                                   app.APPROVER_DEPT_ID\n"
                + "                                            AND org.organization_id =\n"
                + "                                                   dept.organization_id\n"
                + "                                            AND org.organization_code = 'E00'\n"
                + "                                            AND wdj.OWNING_DEPARTMENT =\n"
                + "                                                   bd.department_id\n"
                + "                                            AND bd.attribute1 =\n"
                + "                                                   fv1.flex_value\n"
                + "                                            AND fv1.flex_value_id =\n"
                + "                                                   dept.department_id\n"
                + "                                            AND fv1.flex_value_set_id =\n"
                + "                                                   fvs1.flex_value_set_id\n"
                + "                                            AND fvs1.flex_value_set_name =\n"
                + "                                                   'XXSCM_Departments'\n"
                + "                                            AND dept.position_name IN\n"
                + "                                                   ('ABFM', 'BFM')\n"
                + "                                            AND wdj.wip_entity_id =\n"
                + "                                                   we.wip_entity_id\n"
                + "                                            AND we.wip_entity_name =\n"
                + "                                                   wip_ent.wip_entity_name\n"
                + "                                            AND wdj.wip_entity_id IN\n"
                + "                                                   (SELECT wip_entity_id\n"
                + "                                                      FROM wip_eam_work_requests\n"
                + "                                                     WHERE attribute15\n"
                + "                                                              IS NULL)\n"
                + "                                            AND cii.instance_number =\n"
                + "                                                   (SELECT CASE\n"
                + "                                                              WHEN INSTR (\n"
                + "                                                                      SUBSTR (\n"
                + "                                                                         C.INSTANCE_NUMBER,\n"
                + "                                                                         8,\n"
                + "                                                                         5),\n"
                + "                                                                      '-') BETWEEN 1\n"
                + "                                                                               AND 4\n"
                + "                                                              THEN\n"
                + "                                                                 RTRIM (\n"
                + "                                                                    SUBSTR (\n"
                + "                                                                       C.INSTANCE_NUMBER,\n"
                + "                                                                       1,\n"
                + "                                                                       10),\n"
                + "                                                                    '-')\n"
                + "                                                              WHEN INSTR (\n"
                + "                                                                      SUBSTR (\n"
                + "                                                                         C.INSTANCE_NUMBER,\n"
                + "                                                                         8,\n"
                + "                                                                         5),\n"
                + "                                                                      '-') >\n"
                + "                                                                      4\n"
                + "                                                              THEN\n"
                + "                                                                 RTRIM (\n"
                + "                                                                    SUBSTR (\n"
                + "                                                                       C.INSTANCE_NUMBER,\n"
                + "                                                                       1,\n"
                + "                                                                       11),\n"
                + "                                                                    '-')\n"
                + "                                                              ELSE\n"
                + "                                                                 C.INSTANCE_NUMBER\n"
                + "                                                           END\n"
                + "                                                      FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V c\n"
                + "                                                     WHERE     CURRENT_ORGANIZATION_ID =\n"
                + "                                                                  wip_ent.organization_id\n"
                + "                                                           AND c.serial_number =\n"
                + "                                                                  (SELECT asset_number\n"
                + "                                                                     FROM wip_discrete_jobs wdj1,\n"
                + "                                                                          wip_entities we1\n"
                + "                                                                    WHERE     wdj1.wip_entity_id =\n"
                + "                                                                                 we1.wip_entity_id\n"
                + "                                                                          AND we1.wip_entity_name =\n"
                + "                                                                                 wip_ent.wip_entity_name\n"
                + "                                                                          AND we1.ORGANIZATION_ID =\n"
                + "                                                                                 wip_ent.organization_id))\n"
                + "                                            AND app.user_id =\n"
                + "                                                   (SELECT TO_CHAR (\n"
                + "                                                              fnd_profile.VALUE (\n"
                + "                                                                 'USER_ID'))\n"
                + "                                                      FROM DUAL))))\n"
                + "                     OR (    (SELECT NVL (\n"
                + "                                        fnd_profile.VALUE (\n"
                + "                                           'XXX_EAM_IGATE_SUPERUSER'),\n"
                + "                                        'N')\n"
                + "                                FROM DUAL) = 'Y'\n"
                + "                         AND wip_ent.wip_entity_name IN\n"
                + "                                (SELECT DISTINCT wip_entity_name\n"
                + "                                   FROM wip_discrete_jobs wdj,\n"
                + "                                        wip_entities we\n"
                + "                                  WHERE     wdj.wip_entity_id =\n"
                + "                                               we.wip_entity_id\n"
                + "                                        AND we.wip_entity_name =\n"
                + "                                               wip_ent.wip_entity_name\n"
                + "                                        AND wdj.wip_entity_id IN\n"
                + "                                               (SELECT wip_entity_id\n"
                + "                                                  FROM wip_eam_work_requests\n"
                + "                                                 WHERE attribute15\n"
                + "                                                          IS NOT NULL)))\n"
                + "                     OR (   (SELECT NVL (\n"
                + "                                       fnd_profile.VALUE (\n"
                + "                                          'XXX_EAM_CONT_SUPERUSER'),\n"
                + "                                       'N')\n"
                + "                               FROM DUAL) = 'Y'\n"
                + "                         OR (    (SELECT NVL (\n"
                + "                                            fnd_profile.VALUE (\n"
                + "                                               'XXX_EAM_IGATE_SUPERUSER'),\n"
                + "                                            'N')\n"
                + "                                    FROM DUAL) = 'N'\n"
                + "                             AND (SELECT NVL (\n"
                + "                                            fnd_profile.VALUE (\n"
                + "                                               'XXX_EAM_CONT_SUPERUSER'),\n"
                + "                                            'N')\n"
                + "                                    FROM DUAL) <> 'Y'\n"
                + "                             AND wip_ent.wip_entity_name IN\n"
                + "                                    (SELECT DISTINCT wip_entity_name\n"
                + "                                       FROM STC.XXX_STC_EAM_APPROVERS app,\n"
                + "                                            XXX_STC_EAM_APPROVERS_DEPT_V dept,\n"
                + "                                            org_organization_definitions org,\n"
                + "                                            wip_discrete_jobs wdj,\n"
                + "                                            wip_entities we\n"
                + "                                      WHERE     dept.APPROVER_DEPT_ID =\n"
                + "                                                   app.APPROVER_DEPT_ID\n"
                + "                                            AND org.organization_id =\n"
                + "                                                   dept.organization_id\n"
                + "                                            AND org.organization_code = 'E00'\n"
                + "                                            AND dept.position_name =\n"
                + "                                                   'CONT_SPEC_ENGINEER'\n"
                + "                                            AND DEPT.SUB_DEPARTMENT_ID =\n"
                + "                                                   wdj.OWNING_DEPARTMENT\n"
                + "                                            AND wdj.wip_entity_id =\n"
                + "                                                   we.wip_entity_id\n"
                + "                                            AND we.wip_entity_name =\n"
                + "                                                   wip_ent.wip_entity_name\n"
                + "                                            AND app.user_id =\n"
                + "                                                   (SELECT TO_CHAR (\n"
                + "                                                              fnd_profile.VALUE (\n"
                + "                                                                 'USER_ID'))\n"
                + "                                                      FROM DUAL))))))\n"
                + "            OR (    'Y' = 'N'\n"
                + "                AND (   (SELECT NVL (\n"
                + "                                   fnd_profile.VALUE (\n"
                + "                                      'XXX_EAM_DIST_CONT_SUPERUSER'),\n"
                + "                                   'N')\n"
                + "                           FROM DUAL) = 'Y'\n"
                + "                     OR (    (SELECT NVL (\n"
                + "                                        fnd_profile.VALUE (\n"
                + "                                           'XXX_EAM_DIST_CONT_SUPERUSER'),\n"
                + "                                        'N')\n"
                + "                                FROM DUAL) <> 'Y'\n"
                + "                         AND wip_ent.wip_entity_name IN\n"
                + "                                (SELECT DISTINCT wip_entity_name\n"
                + "                                   FROM FND_FLEX_VALUES_VL FV,\n"
                + "                                        FND_FLEX_VALUE_SETS FVS,\n"
                + "                                        wip_discrete_jobs wdj,\n"
                + "                                        wip_entities we\n"
                + "                                  WHERE     FV.FLEX_VALUE_SET_ID =\n"
                + "                                               FVS.FLEX_VALUE_SET_ID\n"
                + "                                        AND FVS.FLEX_VALUE_SET_NAME =\n"
                + "                                               'XX_EAM_DIST_CONT_WO_STATUS'\n"
                + "                                        AND FV.FLEX_VALUE = wdj.attribute15\n"
                + "                                        AND wdj.wip_entity_id =\n"
                + "                                               we.wip_entity_id)))))";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_USER_ID", userID);
            param.put("P_RESP_ID", respId);
            param.put("P_RESP_APPL_ID", respAppId);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "WORK_ORDER");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("WORK_ORDER");

            JSONObject jsonObject1 = new JSONObject();
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject1.put("WO_COUNT", jsonArray.getJSONObject(i).get("WO_COUNT"));
            }

            result = jsonObject1.toString();
            System.out.println(result);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("SOA Error", e);
            logger.error("QUERY : " + statement);
        }

        return result;

    }

    void closeAllConnection(Connection connection,
            PreparedStatement preparedStatement,
            ResultSet resultSet,
            CallableStatement callableStatement,
            NamedParameterStatement namedParameterStatement) {

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }
        if (namedParameterStatement != null) {
            try {
                namedParameterStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing namedParameterStatement Error", e);
            }
        }
    }
    
//    public static void main(String[] args){
//        DashboardController a= new DashboardController();
//        a.woCount("WO", "58998", "23119", "426");
//        
//    }

}
