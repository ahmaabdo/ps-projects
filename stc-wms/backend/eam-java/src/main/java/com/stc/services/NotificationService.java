package com.stc.services;

import com.stc.controller.NotificationController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 02/01/18
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */

@Path("/notification")
public class NotificationService {

    //processNotification

    @GET
    @Path("getNotification")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getNotification(@QueryParam("username")String username) {

        if (username == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getNotification(username);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }


    @GET
    @Path("getEngineerName")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getEngineerName(@QueryParam("orgid")int orgid) {

        if (orgid == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getEngineerName(orgid);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getNotificationDetail")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getNotificationDetail(@QueryParam("notificationId")int notificationId, @QueryParam("item_type")String item_type
                                          ) {

        if (notificationId == 0 || item_type==null || item_type.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getNotificationDetail(notificationId,item_type);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    @GET
    @Path("getNotificationAttachementForWorkOrder")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getNotificationAttachementForWorkOrder(@QueryParam("item_key")String item_key, @QueryParam("item_type")String item_type
                                          ) {

        if (item_key.isEmpty() || item_type.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getNotificationAttachementForWorkOrder(item_type,item_key);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    @GET
    @Path("getNotificationAttachementForClaim")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getNotificationAttachementForClaim(@QueryParam("item_key")String item_key, @QueryParam("item_type")String item_type
                                          ) {

        if (item_key.isEmpty() || item_type.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getNotificationAttachementForClaim(item_type,item_key);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getSPRLinesForWorkOrder")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getSPRLinesForWorkOrder(@QueryParam("workOrder")String workOrder) {

        if (workOrder.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getSPRLinesForWorkOrder(workOrder);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    @PUT
    @Path("workRequestApprove")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response workRequestApprove(@FormParam("workRequestId") int workRequestId) {

        String result;
        if (workRequestId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workRequestApprove(workRequestId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("workRequestReject")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response workRequestReject(@FormParam("workRequestId") int workRequestId) {

        String result;
        if (workRequestId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workRequestReject(workRequestId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("workRequestCancel")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response workRequestCancel(@FormParam("workRequestId") int workRequestId) {

        String result;
        if (workRequestId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workRequestCancel(workRequestId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("workRequestAddInfo")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response workRequestAddInfo(@FormParam("notificationId") int notificationId,
                                       @FormParam("responder") String responder) {

        String result;
        if (notificationId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workRequestAddInfo(notificationId,responder);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("workOrderApprove")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response workOrderApprove(@FormParam("isHqc") String isHqc,
                                     @FormParam("entityId")int entityId) {

        String result;
        if (entityId == 0 || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workOrderApprove(isHqcFlag(isHqc),entityId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    private boolean isHqcFlag(String isHqc){
        boolean isHqcflag = false;
        if(isHqc.equalsIgnoreCase("Y")){
            isHqcflag = true;
        } else if (isHqc.equalsIgnoreCase("N")){
            isHqcflag = false;
        }
        return isHqcflag;
    }

    @PUT
    @Path("workOrderReject")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response workOrderReject(@FormParam("isHqc") String  isHqc,
                                    @FormParam("entityId")int entityId) {

        String result;
        if (entityId == 0 || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workOrderReject(isHqcFlag(isHqc) ,entityId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("workOrderRejectSparePart")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response workOrderRejectSparePart(@FormParam("isHqc") String isHqc,
                                             @FormParam("entityId")int entityId) {

        String result;
        if (entityId == 0 || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workOrderRejectSparePart(isHqcFlag(isHqc), entityId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("workOrderCancel")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response workOrderCancel(@FormParam("isHqc") String isHqc,
                                    @FormParam("entityId")int entityId) {

        String result;
        if (entityId == 0 || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workOrderCancel(isHqcFlag(isHqc), entityId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("transferNotification")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response transferNotification(@FormParam("notificationId") int notificationId,
                                         @FormParam("newRole")String newRole,
                                         @FormParam("userComment")String userComment) {

        String result;
        if (notificationId == 0 || newRole == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.transferNotification(notificationId,newRole,userComment);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("forwardNotification")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response forwardNotification(@FormParam("notificationId") int notificationId,
                                         @FormParam("newRole")String newRole,
                                         @FormParam("userComment")String userComment) {

        String result;
        if (notificationId == 0 || newRole == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.forwardNotification(notificationId,newRole,userComment);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    /**
     *
     * QUESTION -> INSERT ALL PARAM
     * ANSWER -> LEAVE USERNAME NULL
     */

    @PUT
    @Path("requestInformation")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response requestInformation(@FormParam("notificationId") int notificationId,
                                       @FormParam("username")String username,
                                       @FormParam("comment")String comment,
                                       @FormParam("responder")String responder) {

        String result;
        if (notificationId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.requestInformation(notificationId,username,comment,responder);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("closeNotification")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response closeNotification(@FormParam("notificationId") int notificationId,
                                      @FormParam("responder")String responder) {

        String result;
        if (notificationId == 0 || responder == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.closeNotification(notificationId,responder);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getActionHistory")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getActionHistory(@QueryParam("notificationId")int notificationId) {

        if (notificationId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getActionHistory(notificationId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getUserRoles")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getUserRoles() {

        String result;
        NotificationController notificationController = new NotificationController();
        result = notificationController.getUserRoles();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getUserListFromRoles")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getUserListFromRoles(@QueryParam("origSystem")String origSystem, @QueryParam("userName")String userName) {

        if (origSystem == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getUserListFromRoles(origSystem, userName);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    //Added on 10-01-2019 By Ajay Singh
    @GET
    @Path("getPenaltyDetails")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getPenaltyDetails(@QueryParam("itemKey")String itemKey) {

        if (itemKey == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getPenaltyDetails(itemKey);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkflowParticipant")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkflowParticipant(@QueryParam("notificationId")int notificationId) {

        if (notificationId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            NotificationController notificationController = new NotificationController();
            result = notificationController.getWorkflowParticipant(notificationId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("processNotification")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response processNotification(@FormParam("P_ITEMTYPE") String P_ITEMTYPE,
                                        @FormParam("P_ITEMKEY") String P_ITEMKEY,
                                        @FormParam("P_ACTIVITY") String P_ACTIVITY,
                                        @FormParam("P_RESULT") String P_RESULT) {

        String result;
        if (P_ITEMTYPE == null || P_ITEMKEY==null || P_ACTIVITY==null || P_RESULT==null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.processNotification(P_ITEMTYPE,P_ITEMKEY,P_ACTIVITY,P_RESULT);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }


    @PUT
    @Path("addCommentOrEngg")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response addCommentOrEngg(@FormParam("P_ITEMTYPE") String P_ITEMTYPE,
                                        @FormParam("P_ITEMKEY") String P_ITEMKEY,
                                        @FormParam("P_ATTRIBUTE_NAME") String P_ATTRIBUTE_NAME,
                                        @FormParam("P_ATTRIBUTE_VALUE") String P_ATTRIBUTE_VALUE) {
        // P_ATTRIBUTE_NAME == REASON_OF_REJECT (For Reason)
        // P_ATTRIBUTE_NAME == XX_ENG (For Engg Name)
        String result;
        if (P_ITEMTYPE == null || P_ITEMKEY==null || P_ATTRIBUTE_NAME==null || P_ATTRIBUTE_VALUE==null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.addCommentOrEngg(P_ITEMTYPE,P_ITEMKEY,P_ATTRIBUTE_NAME,P_ATTRIBUTE_VALUE);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    
    @PUT
    @Path("receiptApprove")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response receiptApprove(@FormParam("receiptId") int receiptId) {

        String result;
        if (receiptId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workRequestApprove(receiptId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    @PUT
    @Path("receiptReject")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response receiptReject(@FormParam("receiptId") int receiptId) {

        String result;
        if (receiptId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workRequestReject(receiptId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    
    @PUT
    @Path("receiptCancel")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response receiptCancel(@FormParam("receiptId") int receiptId) {

        String result;
        if (receiptId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            NotificationController notificationController = new NotificationController();
            result = notificationController.workRequestCancel(receiptId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

}
