/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import static com.stc.controller.WorkOrderController.logger;
import static com.stc.controller.WorkOrderController.pool;
import com.stc.objects.PurchasingBean;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 *
 * @author lenovo
 */
public class PurchasingController {

    //-------------------------AddReceipt-----------------------------------------------------------------------//
    public String AddReceiptMethod(PurchasingBean bean) {

        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/create_rcv_request/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/create_rcv_request/");

            Map<String, Object> parent = new LinkedHashMap<String, Object>();            
            Map<String, String> param = new LinkedHashMap<String, String>();           
            param.put("REQUEST_ID", String.valueOf(bean.getRequestId()));
            param.put("VENDOR_ID", String.valueOf(bean.getSupplier()));  
            param.put("PO_NUM", String.valueOf(bean.getPoNumber()));  
            param.put("PO_LINE", String.valueOf(bean.getLineNumber())); 
            param.put("ITEM_ID", String.valueOf(bean.getDescription())); 
            param.put("SHIPMENT_NUM", String.valueOf(bean.getShipmentNumber()));
            param.put("LPN", bean.getLpn()); 
            param.put("LOCATOR", String.valueOf(bean.getLocator()));
            param.put("INVENTORY_ID", String.valueOf(bean.getSubInventory()));
            param.put("LOCATION_ID", String.valueOf(bean.getLocation())); 
            param.put("QUANTITY", String.valueOf(bean.getQty()));  
            param.put("ACCEPTED_QTY", String.valueOf(bean.getAccQty()));
            param.put("REJECTED_QTY", String.valueOf(bean.getRejQty()));
            param.put("UOM", String.valueOf(bean.getUom()));  
            param.put("FROM_SERIAL", String.valueOf(bean.getSerialFrom())); 
            param.put("TO_SERIAL", String.valueOf(bean.getSerialto()));
            param.put("TRANSACTION_TYPE", String.valueOf(bean.getTransactionType())); 
            param.put("TRANSACTION_DATE", String.valueOf(bean.getTransactionDate())); 
            param.put("CREATED_BY", String.valueOf(bean.getCreatedBy()));
            param.put("CREATION_DATE", String.valueOf(bean.getCreatationDate())); 
            param.put("LAST_UPDATED_DATE", String.valueOf(bean.getLastUpdatedDate())); 
            param.put("LAST_UPDATED_BY", String.valueOf(bean.getLastUpdatedBy()));
            param.put("QR_TEXT", String.valueOf(bean.getQrText()));
            param.put("ITEM_TYPE", String.valueOf(bean.getItemType()));
            param.put("ITEM_KEY", String.valueOf(bean.getItemKey()));
            param.put("STATUS", String.valueOf(bean.getStatus()));
            param.put("IMPORT_FLAG", String.valueOf(bean.getImportFlag()));
            param.put("IMPORT_DATE", String.valueOf(bean.getImportDate()));
            param.put("ERROR_MSG", String.valueOf(bean.getErrorMessage()));
            param.put("REFERENCE_ID1", String.valueOf(bean.getPeference1()));
            param.put("REFERENCE_ID2", String.valueOf(bean.getPeference2()));
            param.put("REFERENCE_ID3", String.valueOf(bean.getPeference3()));
            param.put("REFERENCE_ID4", String.valueOf(bean.getPeference4()));
            param.put("REFERENCE_ID5", String.valueOf(bean.getPeference5())); 
            param.put("ORG_ID",String.valueOf(bean.getOrgId()));
            param.put("REJECT_REASON_ID",String.valueOf(bean.getRejReason()));
            param.put("NOTE",String.valueOf(bean.getRejNotes()));
      
            parent.put("P_RCV_REQUEST", param);       
            Map<String, String> param1 = new LinkedHashMap<String, String>();
            ArrayList<String> serialArr=bean.getSerialArr();
            for(int i=0;i<serialArr.size();i++){
             param1.put("P_SERIALS_ITEM"+i, serialArr.get(i)); 
            }            
            parent.put("P_SERIALS", param1);            
            seo.setParameter(parent);
            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Receipt Error", e);
        }
        return result;
    }
    
   

    //------------------------------------------------------------------------------------------------------------------// 
    //----------------------------------------------------AddDeliverMethod-----------------------------------------------------------//
    public String AddDeliverMethod(PurchasingBean bean, String transactionType) {

        String result = null;

        try {

           String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/create_rcv_request/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/create_rcv_request/");

            Map<String, Object> parent = new LinkedHashMap<String, Object>();
            
            Map<String, String> param = new LinkedHashMap<String, String>();  
            
            param.put("REQUEST_ID", String.valueOf(bean.getRequestId()));
            param.put("VENDOR_ID", String.valueOf(bean.getSupplier()));  
            param.put("PO_NUM", String.valueOf(bean.getPoNumber()));  
            param.put("PO_LINE", String.valueOf(bean.getLineNumber())); 
            param.put("ITEM_ID", String.valueOf(bean.getDescription())); 
            param.put("SHIPMENT_NUM", String.valueOf(bean.getShipmentNumber()));
            param.put("LPN", bean.getLpn()); 
            param.put("LOCATOR", String.valueOf(bean.getLocator()));
            param.put("INVENTORY_ID", String.valueOf(bean.getSubInventory()));
            param.put("LOCATION_ID", String.valueOf(bean.getLocation())); 
            param.put("QUANTITY", String.valueOf(bean.getQty()));  
            param.put("ACCEPTED_QTY", String.valueOf(bean.getAccQty()));
            param.put("REJECTED_QTY", String.valueOf(bean.getRejQty()));
            param.put("UOM", String.valueOf(bean.getUom()));  
            param.put("FROM_SERIAL", String.valueOf(bean.getSerialFrom())); 
            param.put("TO_SERIAL", String.valueOf(bean.getSerialto()));
            param.put("TRANSACTION_TYPE", String.valueOf(bean.getTransactionType())); 
            param.put("TRANSACTION_DATE", String.valueOf(bean.getTransactionDate())); 
            param.put("CREATED_BY", String.valueOf(bean.getCreatedBy()));
            param.put("CREATION_DATE", String.valueOf(bean.getCreatationDate())); 
            param.put("LAST_UPDATED_DATE", String.valueOf(bean.getLastUpdatedDate())); 
            param.put("LAST_UPDATED_BY", String.valueOf(bean.getLastUpdatedBy()));
            param.put("QR_TEXT", String.valueOf(bean.getQrText()));
            param.put("ITEM_TYPE", String.valueOf(bean.getItemType()));
            param.put("ITEM_KEY", String.valueOf(bean.getItemKey()));
            param.put("STATUS", String.valueOf(bean.getStatus()));
            param.put("IMPORT_FLAG", String.valueOf(bean.getImportFlag()));
            param.put("IMPORT_DATE", String.valueOf(bean.getImportDate()));
            param.put("ERROR_MSG", String.valueOf(bean.getErrorMessage()));
            param.put("REFERENCE_ID1", String.valueOf(bean.getPeference1()));
            param.put("REFERENCE_ID2", String.valueOf(bean.getPeference2()));
            param.put("REFERENCE_ID3", String.valueOf(bean.getPeference3()));
            param.put("REFERENCE_ID4", String.valueOf(bean.getPeference4()));
            param.put("REFERENCE_ID5", String.valueOf(bean.getPeference5())); 
           
             parent.put("P_RCV_REQUEST", param);
            seo.setParameter(parent);
            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Deliver Error", e);
        }
        return result;
    }
    //---------------------------------------------------------------------------------------------------------------//

    //----------------------------------------------AddReturnMethod-----------------------------------------------------------------//
    public String AddReturnMethodNormal(PurchasingBean bean, String transactionType) {

        String result = null;

        try {

         String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/create_rcv_request/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/create_rcv_request/");

            Map<String, Object> parent = new LinkedHashMap<String, Object>();
            
            Map<String, String> param = new LinkedHashMap<String, String>();  
            
            param.put("REQUEST_ID", String.valueOf(bean.getRequestId()));
            param.put("VENDOR_ID", String.valueOf(bean.getSupplier()));  
            param.put("PO_NUM", String.valueOf(bean.getPoNumber()));  
            param.put("PO_LINE", String.valueOf(bean.getLineNumber())); 
            param.put("ITEM_ID", String.valueOf(bean.getDescription())); 
            param.put("SHIPMENT_NUM", String.valueOf(bean.getShipmentNumber()));
            param.put("LPN", bean.getLpn()); 
            param.put("LOCATOR", String.valueOf(bean.getLocator()));
            param.put("INVENTORY_ID", String.valueOf(bean.getSubInventory()));
            param.put("LOCATION_ID", String.valueOf(bean.getLocation())); 
            param.put("QUANTITY", String.valueOf(bean.getQty()));  
            param.put("ACCEPTED_QTY", String.valueOf(bean.getAccQty()));
            param.put("REJECTED_QTY", String.valueOf(bean.getRejQty()));
            param.put("UOM", String.valueOf(bean.getUom()));  
            param.put("FROM_SERIAL", String.valueOf(bean.getSerialFrom())); 
            param.put("TO_SERIAL", String.valueOf(bean.getSerialto()));
            param.put("TRANSACTION_TYPE", String.valueOf(bean.getTransactionType())); 
            param.put("TRANSACTION_DATE", String.valueOf(bean.getTransactionDate())); 
            param.put("CREATED_BY", String.valueOf(bean.getCreatedBy()));
            param.put("CREATION_DATE", String.valueOf(bean.getCreatationDate())); 
            param.put("LAST_UPDATED_DATE", String.valueOf(bean.getLastUpdatedDate())); 
            param.put("LAST_UPDATED_BY", String.valueOf(bean.getLastUpdatedBy()));
            param.put("QR_TEXT", String.valueOf(bean.getQrText()));
            param.put("ITEM_TYPE", String.valueOf(bean.getItemType()));
            param.put("ITEM_KEY", String.valueOf(bean.getItemKey()));
            param.put("STATUS", String.valueOf(bean.getStatus()));
            param.put("IMPORT_FLAG", String.valueOf(bean.getImportFlag()));
            param.put("IMPORT_DATE", String.valueOf(bean.getImportDate()));
            param.put("ERROR_MSG", String.valueOf(bean.getErrorMessage()));
            param.put("REFERENCE_ID1", String.valueOf(bean.getPeference1()));
            param.put("REFERENCE_ID2", String.valueOf(bean.getPeference2()));
            param.put("REFERENCE_ID3", String.valueOf(bean.getPeference3()));
            param.put("REFERENCE_ID4", String.valueOf(bean.getPeference4()));
            param.put("REFERENCE_ID5", String.valueOf(bean.getPeference5())); 
           
             parent.put("P_RCV_REQUEST", param);
            seo.setParameter(parent);
            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Return Error", e);
        }
        return result;
    }

    //-------------------------AddInspectMethod------------------------//  
    public String AddInspectMethodNormal(PurchasingBean bean, String transactionType) {

        String result = null;

        try {

           String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/create_rcv_request/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/create_rcv_request/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("item", String.valueOf(bean.getItem()));
            param.put("Qty", String.valueOf(bean.getQty()));
            if (transactionType.equals("Expense")) {
                param.put("po_Num ", String.valueOf(bean.getPoNumber()));
                param.put("vendor", String.valueOf(bean.getVendor()));
                param.put("Location", String.valueOf(bean.getLocation()));
                param.put("Sub_Inventory", String.valueOf(bean.getSubInventory()));
                param.put("Locator", String.valueOf(bean.getLocator()));
            }
            if (transactionType.equals("Normal")) {
                param.put("LPN", bean.getLpn());
                param.put("Desc", String.valueOf(bean.getDescription()));
                param.put("UOM", String.valueOf(bean.getUom()));
                param.put("AccLpn", String.valueOf(bean.getAccLpn()));
                param.put("AccQty", String.valueOf(bean.getAccQty()));
                param.put("RejQty", String.valueOf(bean.getRejQty()));
         param.put("",String.valueOf(bean.getRejNotes()));
            }
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Inspect Error", e);
        }
        return result;
    }
    //*********************** Validate QR****************************************//
    public String QrValidation(String lpn,String transaction_Type , String lang) {

        String result = null;

        try {

           String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/get_request_status/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/get_request_status/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("P_LPN",lpn);
            param.put("P_TRANSACTION_TYPE",transaction_Type);
            param.put("P_LANG",lang);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Validate QR Error", e);
        }
        return result;
    }
    
     //*********************** Validate Create request****************************************//
     public String  ValidateRequest(PurchasingBean bean) {

        String result = null;

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            //String soapEndpointUrl = "http://bss05.stc.com.sa:8020/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/validate_rcv_request/";
            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
           // seo.setUsername("stcwebservice");
           // seo.setPassword("stcoracle");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/validate_rcv_request/");
            Map<String, Object> parent = new LinkedHashMap<String, Object>();
            
            Map<String, String> param = new LinkedHashMap<String, String>();
            
        
            
            param.put("REQUEST_ID", String.valueOf(bean.getRequestId()));
            param.put("VENDOR_ID", String.valueOf(bean.getSupplier()));  
            param.put("PO_NUM", String.valueOf(bean.getPoNumber()));  
            param.put("PO_LINE", String.valueOf(bean.getLineNumber())); 
            param.put("ITEM_ID", String.valueOf(bean.getDescription())); 
            param.put("SHIPMENT_NUM", String.valueOf(bean.getShipmentNumber()));
            param.put("LPN", bean.getLpn()); 
            param.put("LOCATOR", String.valueOf(bean.getLocator()));
            param.put("INVENTORY_ID", String.valueOf(bean.getSubInventory()));
            param.put("LOCATION_ID", String.valueOf(bean.getLocation())); 
            param.put("QUANTITY", String.valueOf(bean.getQty()));  
            param.put("ACCEPTED_QTY", String.valueOf(bean.getAccQty()));
            param.put("REJECTED_QTY", String.valueOf(bean.getRejQty()));
            param.put("UOM", String.valueOf(bean.getUom()));  
            param.put("FROM_SERIAL", String.valueOf(bean.getSerialFrom())); 
            param.put("TO_SERIAL", String.valueOf(bean.getSerialto()));
            param.put("TRANSACTION_TYPE", String.valueOf(bean.getTransactionType())); 
            param.put("TRANSACTION_DATE", String.valueOf(bean.getTransactionDate())); 
            param.put("CREATED_BY", String.valueOf(bean.getCreatedBy()));
            param.put("CREATION_DATE", String.valueOf(bean.getCreatationDate())); 
            param.put("LAST_UPDATED_DATE", String.valueOf(bean.getLastUpdatedDate())); 
            param.put("LAST_UPDATED_BY", String.valueOf(bean.getLastUpdatedBy()));
            param.put("QR_TEXT", String.valueOf(bean.getQrText()));
            param.put("ITEM_TYPE", String.valueOf(bean.getItemType()));
            param.put("ITEM_KEY", String.valueOf(bean.getItemKey()));
            param.put("STATUS", String.valueOf(bean.getStatus()));
            param.put("IMPORT_FLAG", String.valueOf(bean.getImportFlag()));
            param.put("IMPORT_DATE", String.valueOf(bean.getImportDate()));
            param.put("ERROR_MSG", String.valueOf(bean.getErrorMessage()));
            param.put("REFERENCE_ID1", String.valueOf(bean.getPeference1()));
            param.put("REFERENCE_ID2", String.valueOf(bean.getPeference2()));
            param.put("REFERENCE_ID3", String.valueOf(bean.getPeference3()));
            param.put("REFERENCE_ID4", String.valueOf(bean.getPeference4()));
            param.put("REFERENCE_ID5", String.valueOf(bean.getPeference5()));  
            param.put("REJECT_REASON_ID",String.valueOf(bean.getRejReason()));
            param.put("NOTE",String.valueOf(bean.getRejNotes()));
            param.put("ORG_ID",String.valueOf(bean.getOrgId()));
            parent.put("P_RCV_REQUEST", param);
            
          
            Map<String, String> param1 = new LinkedHashMap<String, String>();
            ArrayList<String> serialArr=bean.getSerialArr();
            for(int i=0;i<serialArr.size();i++){
             param1.put("P_SERIALS_ITEM"+i, serialArr.get(i)); 
            }
            
            parent.put("P_SERIALS", param1);
            
            seo.setParameter(parent);

            SoapHandler handler = new SoapHandler();
            System.out.println("com.stc.controller.PurchasingController.ValidateRequest()");
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
            System.out.println("soap result:"+result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Validate Request Error", e);
        }
        return result;
    }
    
    
    
    
    
}
