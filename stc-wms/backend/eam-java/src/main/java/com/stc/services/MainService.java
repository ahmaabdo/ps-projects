package com.stc.services;

import com.stc.controller.MainController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 27/12/17
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */

@Path("/service")
public class MainService {

    @POST
    @Path("validateLogin")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public void validateLogin(String data) {
        JSONObject jsonObject = new JSONObject(data);
        String username=jsonObject.getString("userName");
        String password=jsonObject.getString("password");
//        if (username == null || password == null) {
//            return Response.status(Response.Status.BAD_REQUEST).build();
//        }
//        else {
//            String result;
//            MainController mainController = new MainController();
//            result = mainController.validateLogin(username,password);
//
//            if (result.isEmpty()) {
//                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//            }
//            else {
//                return Response
//                        .status(200)
//                        .entity(result)
//                        .type(MediaType.APPLICATION_JSON)
//                        .build();
//            }
//        }
    }
    
    @GET

    @Path("getSecurityProfiles")

    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})

    @Produces({"application/xml", "application/json", "text/plain", "text/html"})

    public Response getSecurityProfiles(@QueryParam("responsibilityId")int responsibilityId) {

        if (responsibilityId ==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            MainController mainController = new MainController();
            result = mainController.getSecurityProfiles(responsibilityId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getLoginDetails")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLoginDetails(@QueryParam("userName")String userName,
                                    @QueryParam("userId")String userId) {
        
        System.out.println(userName);        System.out.println(userId);
            
        
        if (userName.isEmpty() && userId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result=null;
            MainController mainController = new MainController();
            result = mainController.getLoginDetails(userName, userId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    @GET
    @Path("validateUserOnEBS")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response validateUserOnEBS(@QueryParam("emailId")String emailId) {
        
        System.out.println(emailId);
            
        
        if (emailId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            MainController mainController = new MainController();
            result = mainController.validateUserOnEBS(emailId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getResponsibility")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getResponsibility(@QueryParam("userId")String userId, @QueryParam("language")String language) {

        if (userId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            MainController mainController = new MainController();
            result = mainController.getResponsibility(userId, language);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getOrganization")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getOrganization(@QueryParam("respId")String respId,
                                    @QueryParam("respAppId")String respAppId) {

        if (respId == null || respAppId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            MainController mainController = new MainController();
            result = mainController.getOrganization(respId,respAppId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getUserInformation")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getUserInformation(@QueryParam("username")String username,
                                       @QueryParam("fullname")String fullname) {

        String result;
        MainController mainController = new MainController();
        result = mainController.getUserInformation(username,fullname);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("test")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response test() {

        String result;
        MainController mainController = new MainController();
        result = mainController.test();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
}
