package com.stc.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 09/01/18
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */
public class PlsqlQuery {

    public static final String createWorkOrder = "declare\n" +
            "\n" +
            "        i_eam_wo_rec           EAM_PROCESS_WO_PUB.eam_wo_rec_type       ;--    := EAM_PROCESS_WO_PUB.G_MISS_EAM_WO_REC;\n" +
            "        i_eam_op_tbl           EAM_PROCESS_WO_PUB.eam_op_tbl_type        ;--   := EAM_PROCESS_WO_PUB.G_MISS_EAM_OP_REC;\n" +
            "        i_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type ;--  := EAM_PROCESS_WO_PUB.G_MISS_EAM_OP_NETWORK_TBL;\n" +
            "        i_eam_res_tbl          EAM_PROCESS_WO_PUB.eam_res_tbl_type        ;--  := EAM_PROCESS_WO_PUB.G_MISS_EAM_RES_TBL ;\n" +
            "        i_eam_res_inst_tbl     EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type   ;--  := EAM_PROCESS_WO_PUB.G_MISS_EAM_RES_INST_TBL;\n" +
            "        i_eam_sub_res_tbl      EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type    ;--  := EAM_PROCESS_WO_PUB.G_MISS_EAM_SUB_RES_TBL;\n" +
            "        i_eam_res_usage_tbl    EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type ;--   := EAM_PROCESS_WO_PUB.G_MISS_EAM_RES_USAGE_TBL;\n" +
            "        i_eam_mat_req_tbl      EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type   ;--   := EAM_PROCESS_WO_PUB.G_MISS_EAM_MAT_REQ_TBL;\n" +
            "        i_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type; --:=  EAM_PROCESS_WO_PUB.G_MISS_EAM_DIRECT_ITEMS_TBL;\n" +
            "        i_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type   ;--  := EAM_PROCESS_WO_PUB.G_MISS_EAM_WO_COMP_REC ;\n" +
            "        i_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type     ;--:= EAM_PROCESS_WO_PUB.G_MISS_EAM_WO_QUALITY_TBL;\n" +
            "        i_eam_meter_reading_tbl   EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type;-- := EAM_PROCESS_WO_PUB.G_MISS_EAM_METER_READING_TBL;\n" +
            "        i_eam_counter_prop_tbl   EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type  ;-- := EAM_PROCESS_WO_PUB.G_MISS_EAM_COUNTER_PROP_TBL;\n" +
            "        i_eam_wo_comp_rebuild_tbl    EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type;-- := EAM_PROCESS_WO_PUB.G_MISS_EAM_WO_COMP_REBUILD_TBL;\n" +
            "        i_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type;-- := EAM_PROCESS_WO_PUB.G_MISS_EAM_WO_COMP_MR_READ_TBL;\n" +
            "        i_eam_op_comp_tbl      EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type;--:= EAM_PROCESS_WO_PUB.G_MISS_EAM_OP_COMP_TBL;\n" +
            "        i_eam_request_tbl          EAM_PROCESS_WO_PUB.eam_request_tbl_type  ;--:= EAM_PROCESS_WO_PUB.G_MISS_EAM_REQUEST_TBL;\n" +
            "        i_eam_permit_tbl           EAM_PROCESS_PERMIT_PUB.eam_wp_tbl_type; -- new param for safety permit\n" +
            "        i_eam_permit_wo_assoc_tbl  EAM_PROCESS_PERMIT_PUB.eam_wp_association_tbl_type;\n" +
            "        O_eam_wo_rec               EAM_PROCESS_WO_PUB.eam_wo_rec_type;\n" +
            "        O_eam_op_tbl               EAM_PROCESS_WO_PUB.eam_op_tbl_type;\n" +
            "        O_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type;\n" +
            "        O_eam_res_tbl    EAM_PROCESS_WO_PUB.eam_res_tbl_type;\n" +
            "        O_eam_res_inst_tbl  EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type;\n" +
            "        O_eam_sub_res_tbl   EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type;\n" +
            "        O_eam_res_usage_tbl  EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type;\n" +
            "        O_eam_mat_req_tbl EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type;\n" +
            "        O_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type;\n" +
            "        O_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type;\n" +
            "        O_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type;\n" +
            "        O_eam_meter_reading_tbl EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type;\n" +
            "        O_eam_counter_prop_tblOUT  EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type;\n" +
            "        O_eam_wo_comp_rebuild_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type;\n" +
            "        O_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type;\n" +
            "        O_eam_op_comp_tbl    EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type;\n" +
            "        O_eam_request_tbl     EAM_PROCESS_WO_PUB.eam_request_tbl_type;\n" +
            "        O_return_status    VARCHAR2(50);\n" +
            "        O_msg_count        NUMBER;\n" +
            "        l_msg_count         NUMBER;\n" +
            "        msg_index NUMBER;\n" +
            "        temp_err_mesg VARCHAR2(2000);\n" +
            "        num integer := 1000;\n" +
            "\n" +
            "begin\n" +
            "    \n" +
            "   fnd_global.apps_initialize (user_id => ?,resp_id => ?,resp_appl_id => ?);\n" +
            "    \n" +
            "    i_eam_wo_rec.HEADER_ID := 1;\n" +
            "    \n" +
            "    i_eam_wo_rec.BATCH_ID := 1;\n" +
            "    \n" +
            "    i_eam_wo_rec.transaction_type := EAM_PROCESS_WO_PVT.G_OPR_CREATE;\n" +
            "    \n" +
            "    i_eam_wo_rec.wip_entity_name := ?; \n" +
            "    \n" +
            "    i_eam_wo_rec.organization_id := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.asset_number    := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.asset_group_id  := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.maintenance_object_id  := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.maintenance_object_type  := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.maintenance_object_source  := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.class_code := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.scheduled_start_date := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.scheduled_completion_date := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.description := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.owning_department := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.asset_activity_id := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.firm_planned_flag := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.status_type := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.work_order_type := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.shutdown_type := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.priority := ?;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).HEADER_ID := 1;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).BATCH_ID := 1;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).REQUEST_NUMBER   := ?;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).REQUEST_ID := ?;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).REQUEST_TYPE := 1;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).TRANSACTION_TYPE := EAM_PROCESS_WO_PVT.G_OPR_CREATE;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).ORGANIZATION_ID := ?;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).WIP_ENTITY_NAME := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.ACTIVITY_TYPE := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.ACTIVITY_CAUSE := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.ACTIVITY_SOURCE := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.MATERIAL_ISSUE_BY_MO := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.PLAN_MAINTENANCE := ?;\n" +
            "    \n" +
            "    i_eam_wo_rec.WARRANTY_CLAIM_STATUS :=?;\n" +
            "    \n" +
            "    i_eam_wo_rec.WARRANTY_ACTIVE :=?;\n" +
            "    \n" +
            "    i_eam_wo_rec.TAGOUT_REQUIRED :=?;\n" +
            "    \n" +
            "    i_eam_wo_rec.NOTIFICATION_REQUIRED :=?;\n" +
            "\n" +
            "EAM_PROCESS_WO_PUB.PROCESS_WO\n" +
            "       (  p_bo_identifier           => 'EAM'\n" +
            "        , p_api_version_number      =>  1.0\n" +
            "        , p_init_msg_list           => TRUE\n" +
            "        , p_commit                  => 'Y'\n" +
            "        , p_eam_wo_rec              => i_eam_wo_rec\n" +
            "        , p_eam_op_tbl              => i_eam_op_tbl\n" +
            "        , p_eam_op_network_tbl      => i_eam_op_network_tbl\n" +
            "        , p_eam_res_tbl             => i_eam_res_tbl\n" +
            "        , p_eam_res_inst_tbl        => i_eam_res_inst_tbl\n" +
            "        , p_eam_sub_res_tbl         => i_eam_sub_res_tbl\n" +
            "        , p_eam_res_usage_tbl       => i_eam_res_usage_tbl\n" +
            "        , p_eam_mat_req_tbl         => i_eam_mat_req_tbl\n" +
            "        , p_eam_direct_items_tbl    => i_eam_direct_items_tbl\n" +
            "        , p_eam_wo_comp_rec         => i_eam_wo_comp_rec\n" +
            "        , p_eam_wo_quality_tbl      => i_eam_wo_quality_tbl\n" +
            "        , p_eam_meter_reading_tbl   => i_eam_meter_reading_tbl\n" +
            "        , p_eam_counter_prop_tbl    => i_eam_counter_prop_tbl\n" +
            "        , p_eam_wo_comp_rebuild_tbl => i_eam_wo_comp_rebuild_tbl\n" +
            "        , p_eam_wo_comp_mr_read_tbl => i_eam_wo_comp_mr_read_tbl\n" +
            "        , p_eam_op_comp_tbl         => i_eam_op_comp_tbl\n" +
            "        , p_eam_request_tbl         => i_eam_request_tbl\n" +
            "        , p_eam_permit_tbl          => i_eam_permit_tbl\n" +
            "        , p_eam_permit_wo_assoc_tbl => i_eam_permit_wo_assoc_tbl\n" +
            "        , x_eam_wo_rec              => O_eam_wo_rec\n" +
            "        , x_eam_op_tbl              => O_eam_op_tbl\n" +
            "        , x_eam_op_network_tbl      => O_eam_op_network_tbl\n" +
            "        , x_eam_res_tbl             => O_eam_res_tbl\n" +
            "        , x_eam_res_inst_tbl        => O_eam_res_inst_tbl\n" +
            "        , x_eam_sub_res_tbl         => O_eam_sub_res_tbl\n" +
            "        , x_eam_res_usage_tbl       => O_eam_res_usage_tbl\n" +
            "        , x_eam_mat_req_tbl         => O_eam_mat_req_tbl\n" +
            "        , x_eam_direct_items_tbl    => O_eam_direct_items_tbl\n" +
            "        , x_eam_wo_comp_rec         => O_eam_wo_comp_rec\n" +
            "        , x_eam_wo_quality_tbl      => O_eam_wo_quality_tbl\n" +
            "        , x_eam_meter_reading_tbl   => O_eam_meter_reading_tbl\n" +
            "        , x_eam_counter_prop_tbl    => O_eam_counter_prop_tblOUT\n" +
            "        , x_eam_wo_comp_rebuild_tbl => O_eam_wo_comp_rebuild_tbl\n" +
            "        , x_eam_wo_comp_mr_read_tbl => O_eam_wo_comp_mr_read_tbl\n" +
            "        , x_eam_op_comp_tbl         => O_eam_op_comp_tbl\n" +
            "        , x_eam_request_tbl         => O_eam_request_tbl\n" +
            "        , x_return_status           => O_return_status\n" +
            "        , x_msg_count               => O_msg_count\n" +
            "        --, p_debug                   => 'Y'\n" +
            "        --, p_output_dir              => '/usr/tmp'\n" +
            "        --, p_debug_filename          => 'EAM_WO_DEBUGa.log'        , p_debug_file_mode         => 'w'\n" +
            "        );\n" +
            "        \n" +

            "dbms_output.put_line(O_return_status);\n" +
            "DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_ID);\n" +
            "DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_NAME);\n" +

            "\n" +
            "l_msg_count := fnd_msg_pub.count_msg;\n" +
            "\n" +
            "IF(l_msg_count>0) THEN\n" +
            "msg_index := l_msg_count;\n" +
            "FOR i IN 1..l_msg_count LOOP\n" +
            "fnd_msg_pub.get\n" +
            "            (p_msg_index => FND_MSG_PUB.G_NEXT,\n" +
            "   p_encoded   => 'F',\n" +
            "   p_data      => temp_err_mesg,\n" +
            "   p_msg_index_out => msg_index);\n" +
            "dbms_output.put_line('Error:' || msg_index||':'||temp_err_mesg);\n" +
            "\n" +
            "END LOOP;\n" +
            "END IF;\n" +
            "dbms_output.get_lines(?, num);\n" +
            "\n" +
            "commit;\n" +
            "\n" +
            "\n" +
            "\n" +
            "END;\n" +
            "\n";

    public static final String addOperations = "declare \n" +
            "             \n" +
            "        i_eam_wo_rec           EAM_PROCESS_WO_PUB.eam_wo_rec_type;\n" +
            "        i_eam_op_tbl           EAM_PROCESS_WO_PUB.eam_op_tbl_type;\n" +
            "        i_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type;\n" +
            "        i_eam_res_tbl          EAM_PROCESS_WO_PUB.eam_res_tbl_type;\n" +
            "        i_eam_res_inst_tbl     EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type;\n" +
            "        i_eam_sub_res_tbl      EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type;\n" +
            "        i_eam_res_usage_tbl    EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type;\n" +
            "        i_eam_mat_req_tbl      EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type;\n" +
            "        i_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type;\n" +
            "        i_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type;\n" +
            "        i_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type;\n" +
            "        i_eam_meter_reading_tbl   EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type;\n" +
            "        i_eam_counter_prop_tbl   EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type;\n" +
            "        i_eam_wo_comp_rebuild_tbl    EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type;\n" +
            "        i_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type;\n" +
            "        i_eam_op_comp_tbl      EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type;\n" +
            "        i_eam_request_tbl          EAM_PROCESS_WO_PUB.eam_request_tbl_type;\n" +
            "        i_eam_permit_tbl           EAM_PROCESS_PERMIT_PUB.eam_wp_tbl_type; \n" +
            "        i_eam_permit_wo_assoc_tbl  EAM_PROCESS_PERMIT_PUB.eam_wp_association_tbl_type; \n" +
            "        O_eam_wo_rec               EAM_PROCESS_WO_PUB.eam_wo_rec_type; \n" +
            "        O_eam_op_tbl               EAM_PROCESS_WO_PUB.eam_op_tbl_type; \n" +
            "        O_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type; \n" +
            "        O_eam_res_tbl    EAM_PROCESS_WO_PUB.eam_res_tbl_type; \n" +
            "        O_eam_res_inst_tbl  EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type; \n" +
            "        O_eam_sub_res_tbl   EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type; \n" +
            "        O_eam_res_usage_tbl  EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type; \n" +
            "        O_eam_mat_req_tbl EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type; \n" +
            "        O_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type; \n" +
            "        O_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type; \n" +
            "        O_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "        O_eam_meter_reading_tbl EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "        O_eam_counter_prop_tblOUT  EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type; \n" +
            "        O_eam_wo_comp_rebuild_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "        O_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "        O_eam_op_comp_tbl    EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "        O_eam_request_tbl     EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "        O_return_status    VARCHAR2(50); \n" +
            "        O_msg_count        NUMBER; \n" +
            "        l_msg_count         NUMBER; \n" +
            "        msg_index NUMBER; \n" +
            "        temp_err_mesg VARCHAR2(2000); \n" +
            "        entity_id NUMBER;\n" +
            "        organization_id NUMBER;\n" +
            "        department_id NUMBER;\n" +
            "        start_date DATE;\n" +
            "        completion_date DATE;\n" +
            "        operation_seq_num NUMBER;\n" +
            "        resource_seq_num NUMBER;\n" +
            "        num integer := 1000; \n" +
            " \n" +
            "begin \n" +
            " \n" +
            "   fnd_global.apps_initialize (user_id => ?,resp_id => ?,resp_appl_id => ?); \n" +
            "   \n" +
            "    entity_id := ?;\n" +
            "    organization_id := ?;\n" +
            "    department_id := ?;\n" +
            "    start_date := ?;\n" +
            "    completion_date := ?;\n" +
            "    operation_seq_num := ?;\n" +
            "    resource_seq_num := ?;\n" +
            " \n" +
            "    i_eam_wo_rec.transaction_type := EAM_PROCESS_WO_PVT.G_OPR_UPDATE;      \n" +
            "    i_eam_wo_rec.WIP_ENTITY_ID := entity_id; \n" +
            "    i_eam_wo_rec.ORGANIZATION_ID := organization_id;          \n" +
            "     \n" +
            "    i_eam_op_tbl(1).TRANSACTION_TYPE := EAM_PROCESS_WO_PVT.G_OPR_CREATE;    \n" +
            "    i_eam_op_tbl(1).WIP_ENTITY_ID := entity_id;          \n" +
            "    i_eam_op_tbl(1).ORGANIZATION_ID := organization_id;    \n" +
            "    i_eam_op_tbl(1).OPERATION_SEQ_NUM := operation_seq_num;      \n" +
            "    i_eam_op_tbl(1).DESCRIPTION := ?; \n" +
            "    i_eam_op_tbl(1).DEPARTMENT_ID := department_id; \n" +
            "    i_eam_op_tbl(1).START_DATE := start_date;\n" +
            "    i_eam_op_tbl(1).COMPLETION_DATE := completion_date;\n" +
            "     \n" +
            "    i_eam_res_tbl(1).TRANSACTION_TYPE := EAM_PROCESS_WO_PVT.G_OPR_CREATE;\n" +
            "    i_eam_res_tbl(1).WIP_ENTITY_ID := entity_id;\n" +
            "    i_eam_res_tbl(1).ORGANIZATION_ID := organization_id;\n" +
            "    i_eam_res_tbl(1).OPERATION_SEQ_NUM := operation_seq_num;\n" +
            "    i_eam_res_tbl(1).RESOURCE_SEQ_NUM := resource_seq_num;\n" +
            "    i_eam_res_tbl(1).RESOURCE_ID := ?;\n" +
            "    i_eam_res_tbl(1).UOM_CODE := ?;\n" +
            "    i_eam_res_tbl(1).BASIS_TYPE := ?;\n" +
            "    i_eam_res_tbl(1).USAGE_RATE_OR_AMOUNT := ?;\n" +
            "    i_eam_res_tbl(1).ASSIGNED_UNITS := ?;\n" +
            "    i_eam_res_tbl(1).AUTOCHARGE_TYPE := ?;\n" +
            "    i_eam_res_tbl(1).STANDARD_RATE_FLAG := ?;\n" +
            "    i_eam_res_tbl(1).START_DATE := start_date;\n" +
            "    i_eam_res_tbl(1).COMPLETION_DATE := completion_date;\n" +
            "    i_eam_res_tbl(1).DEPARTMENT_ID := department_id;\n" +
            "    \n" +
            "    i_eam_res_inst_tbl(1).TRANSACTION_TYPE := EAM_PROCESS_WO_PVT.G_OPR_CREATE;\n" +
            "    i_eam_res_inst_tbl(1).WIP_ENTITY_ID := entity_id;\n" +
            "    i_eam_res_inst_tbl(1).ORGANIZATION_ID := organization_id;\n" +
            "    i_eam_res_inst_tbl(1).OPERATION_SEQ_NUM := operation_seq_num;\n" +
            "    i_eam_res_inst_tbl(1).RESOURCE_SEQ_NUM := resource_seq_num;\n" +
            "    i_eam_res_inst_tbl(1).INSTANCE_ID := ?;\n" +
            "    i_eam_res_inst_tbl(1).START_DATE := start_date;\n" +
            "    i_eam_res_inst_tbl(1).COMPLETION_DATE := completion_date;\n" +
            " \n" +
            "EAM_PROCESS_WO_PUB.PROCESS_WO \n" +
            "       (  p_bo_identifier           => 'EAM' \n" +
            "        , p_api_version_number      =>  1.0 \n" +
            "        , p_init_msg_list           => TRUE \n" +
            "        , p_commit                  => 'Y' \n" +
            "        , p_eam_wo_rec              => i_eam_wo_rec \n" +
            "        , p_eam_op_tbl              => i_eam_op_tbl \n" +
            "        , p_eam_op_network_tbl      => i_eam_op_network_tbl \n" +
            "        , p_eam_res_tbl             => i_eam_res_tbl \n" +
            "        , p_eam_res_inst_tbl        => i_eam_res_inst_tbl \n" +
            "        , p_eam_sub_res_tbl         => i_eam_sub_res_tbl \n" +
            "        , p_eam_res_usage_tbl       => i_eam_res_usage_tbl \n" +
            "        , p_eam_mat_req_tbl         => i_eam_mat_req_tbl \n" +
            "        , p_eam_direct_items_tbl    => i_eam_direct_items_tbl \n" +
            "        , p_eam_wo_comp_rec         => i_eam_wo_comp_rec \n" +
            "        , p_eam_wo_quality_tbl      => i_eam_wo_quality_tbl \n" +
            "        , p_eam_meter_reading_tbl   => i_eam_meter_reading_tbl \n" +
            "        , p_eam_counter_prop_tbl    => i_eam_counter_prop_tbl \n" +
            "        , p_eam_wo_comp_rebuild_tbl => i_eam_wo_comp_rebuild_tbl \n" +
            "        , p_eam_wo_comp_mr_read_tbl => i_eam_wo_comp_mr_read_tbl \n" +
            "        , p_eam_op_comp_tbl         => i_eam_op_comp_tbl \n" +
            "        , p_eam_request_tbl         => i_eam_request_tbl \n" +
            "        , p_eam_permit_tbl          => i_eam_permit_tbl \n" +
            "        , p_eam_permit_wo_assoc_tbl => i_eam_permit_wo_assoc_tbl \n" +
            "        , x_eam_wo_rec              => O_eam_wo_rec \n" +
            "        , x_eam_op_tbl              => O_eam_op_tbl \n" +
            "        , x_eam_op_network_tbl      => O_eam_op_network_tbl \n" +
            "        , x_eam_res_tbl             => O_eam_res_tbl \n" +
            "        , x_eam_res_inst_tbl        => O_eam_res_inst_tbl \n" +
            "        , x_eam_sub_res_tbl         => O_eam_sub_res_tbl \n" +
            "        , x_eam_res_usage_tbl       => O_eam_res_usage_tbl \n" +
            "        , x_eam_mat_req_tbl         => O_eam_mat_req_tbl \n" +
            "        , x_eam_direct_items_tbl    => O_eam_direct_items_tbl \n" +
            "        , x_eam_wo_comp_rec         => O_eam_wo_comp_rec \n" +
            "        , x_eam_wo_quality_tbl      => O_eam_wo_quality_tbl \n" +
            "        , x_eam_meter_reading_tbl   => O_eam_meter_reading_tbl \n" +
            "        , x_eam_counter_prop_tbl    => O_eam_counter_prop_tblOUT \n" +
            "        , x_eam_wo_comp_rebuild_tbl => O_eam_wo_comp_rebuild_tbl \n" +
            "        , x_eam_wo_comp_mr_read_tbl => O_eam_wo_comp_mr_read_tbl \n" +
            "        , x_eam_op_comp_tbl         => O_eam_op_comp_tbl \n" +
            "        , x_eam_request_tbl         => O_eam_request_tbl \n" +
            "        , x_return_status           => O_return_status \n" +
            "        , x_msg_count               => O_msg_count \n" +
            "        --, p_debug                   => 'Y' \n" +
            "        --, p_output_dir              => '/usr/tmp' \n" +
            "        --, p_debug_filename          => 'EAM_WO_DEBUGa.log'        , p_debug_file_mode         => 'w' \n" +
            "        ); \n" +
            "                \n" +
            "dbms_output.put_line(O_return_status); \n" +
            "DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_ID); \n" +
            "DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_NAME); \n" +
            "\n" +
            " \n" +
            "l_msg_count := fnd_msg_pub.count_msg; \n" +
            " \n" +
            "IF(l_msg_count>0) THEN \n" +
            "msg_index := l_msg_count; \n" +
            "FOR i IN 1..l_msg_count LOOP \n" +
            "fnd_msg_pub.get \n" +
            "            (p_msg_index => FND_MSG_PUB.G_NEXT, \n" +
            "   p_encoded   => 'F', \n" +
            "   p_data      => temp_err_mesg, \n" +
            "   p_msg_index_out => msg_index); \n" +
            "dbms_output.put_line('Error:' || msg_index||':'||temp_err_mesg); \n" +
            " \n" +
            "END LOOP; \n" +
            "END IF; \n" +
            "dbms_output.get_lines(?, num); \n" +
            " \n" +
            "commit; \n" +
            "END;             ";


    public static final String completeWorkOrder = "declare \n" +
            "             \n" +
            "              O_msg_index     NUMBER; \n" +
            "              O_return_status VARCHAR2(50); \n" +
            "              O_msg_count     NUMBER; \n" +
            "              O_err_mesg      VARCHAR2(2000); \n" +
            "             \n" +
            "              -- Input parameters as per API \n" +
            "              i_eam_wo_rec              EAM_PROCESS_WO_PUB.eam_wo_rec_type; \n" +
            "              i_eam_op_tbl              EAM_PROCESS_WO_PUB.eam_op_tbl_type; \n" +
            "              i_eam_op_network_tbl      EAM_PROCESS_WO_PUB.eam_op_network_tbl_type; \n" +
            "              i_eam_res_tbl             EAM_PROCESS_WO_PUB.eam_res_tbl_type; \n" +
            "              i_eam_res_inst_tbl        EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type; \n" +
            "              i_eam_sub_res_tbl         EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type; \n" +
            "              i_eam_res_usage_tbl       EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type; \n" +
            "              i_eam_mat_req_tbl         EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type; \n" +
            "              i_eam_direct_items_tbl    EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type; \n" +
            "              i_eam_wo_comp_rec         EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type; \n" +
            "              i_eam_wo_quality_tbl      EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "              i_eam_meter_reading_tbl   EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "              i_eam_counter_prop_tbl    EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type; \n" +
            "              i_eam_wo_comp_rebuild_tbl EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "              i_eam_wo_comp_mr_read_tbl EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "              i_eam_op_comp_tbl         EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "              i_eam_request_tbl         EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "              i_eam_permit_tbl          EAM_PROCESS_PERMIT_PUB.eam_wp_tbl_type; \n" +
            "              i_eam_permit_wo_assoc_tbl EAM_PROCESS_PERMIT_PUB.eam_wp_association_tbl_type; \n" +
            "             \n" +
            "              -- Output parameters as per API \n" +
            "              O_eam_wo_rec              EAM_PROCESS_WO_PUB.eam_wo_rec_type; \n" +
            "              O_eam_op_tbl              EAM_PROCESS_WO_PUB.eam_op_tbl_type; \n" +
            "              O_eam_op_network_tbl      EAM_PROCESS_WO_PUB.eam_op_network_tbl_type; \n" +
            "              O_eam_res_tbl             EAM_PROCESS_WO_PUB.eam_res_tbl_type; \n" +
            "              O_eam_res_inst_tbl        EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type; \n" +
            "              O_eam_sub_res_tbl         EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type; \n" +
            "              O_eam_res_usage_tbl       EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type; \n" +
            "              O_eam_mat_req_tbl         EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type; \n" +
            "              O_eam_direct_items_tbl    EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type; \n" +
            "              O_eam_wo_comp_rec         EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type;                                                                                        \n" +
            "              O_eam_wo_quality_tbl      EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "              O_eam_meter_reading_tbl   EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "              O_eam_counter_prop_tblOUT EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type; \n" +
            "              O_eam_wo_comp_rebuild_tbl EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "              O_eam_wo_comp_mr_read_tbl EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "              O_eam_op_comp_tbl         EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "              O_eam_request_tbl         EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "             \n" +
            "              num integer := 1000; \n" +
            "\n" +
            "            begin \n" +
            "             \n" +
            "              fnd_global.apps_initialize(user_id      => ?, \n" +
            "                                         resp_id      => ?, \n" +
            "                                         resp_appl_id => ?);  \n" +
            "             -- [ Mandatory ] Parameters \n" +
            "                i_eam_wo_comp_rec.Batch_Id            := 1; \n" +
            "                i_eam_wo_comp_rec.wip_entity_id       := ?; \n" +
            "                i_eam_wo_comp_rec.header_id           := 1; \n" +
            "                i_eam_wo_comp_rec.Organization_Id     := ?; \n" +
            "                i_eam_wo_comp_rec.transaction_type    := EAM_PROCESS_WO_PVT.G_OPR_COMPLETE; \n" +
            "                i_eam_wo_comp_rec.User_Status_Id      := ?;\n" +
            "                i_eam_wo_comp_rec.actual_start_date   := ?;\n" +
            "                i_eam_wo_comp_rec.actual_end_date     := ?;\n" +
            "                i_eam_wo_comp_rec.actual_duration     := ?; \n" +
            "                i_eam_wo_comp_rec.Transaction_Date    := sysdate; \n" +
            "                i_eam_wo_comp_rec.Rebuild_Job         := 'N'; \n" +
            "                i_eam_wo_comp_rec.Shutdown_Start_Date := ?;\n" +
            "                i_eam_wo_comp_rec.Shutdown_End_Date   := ?;\n" +
            "                i_eam_wo_comp_rec.Reconciliation_Code := ?;\n" +
            "                i_eam_wo_comp_rec.COMPLETION_SUBINVENTORY := ?;  \n" +
            "                i_eam_wo_comp_rec.COMPLETION_LOCATOR_ID := ?;  \n" +
            "                i_eam_wo_comp_rec.LOT_NUMBER := ?;  \n" +
            "             \n" +
            "              EAM_PROCESS_WO_PUB.PROCESS_WO(p_bo_identifier           => 'EAM', \n" +
            "                                            p_api_version_number      => 1.0, \n" +
            "                                            p_init_msg_list           => TRUE, \n" +
            "                                            p_commit                  => 'Y', \n" +
            "                                            p_eam_wo_rec              => i_eam_wo_rec, \n" +
            "                                            p_eam_op_tbl              => i_eam_op_tbl, \n" +
            "                                            p_eam_op_network_tbl      => i_eam_op_network_tbl, \n" +
            "                                            p_eam_res_tbl             => i_eam_res_tbl, \n" +
            "                                            p_eam_res_inst_tbl        => i_eam_res_inst_tbl, \n" +
            "                                            p_eam_sub_res_tbl         => i_eam_sub_res_tbl, \n" +
            "                                            p_eam_res_usage_tbl       => i_eam_res_usage_tbl, \n" +
            "                                            p_eam_mat_req_tbl         => i_eam_mat_req_tbl, \n" +
            "                                            p_eam_direct_items_tbl    => i_eam_direct_items_tbl, \n" +
            "                                            p_eam_wo_comp_rec         => i_eam_wo_comp_rec, \n" +
            "                                            p_eam_wo_quality_tbl      => i_eam_wo_quality_tbl, \n" +
            "                                            p_eam_meter_reading_tbl   => i_eam_meter_reading_tbl, \n" +
            "                                            p_eam_counter_prop_tbl    => i_eam_counter_prop_tbl, \n" +
            "                                            p_eam_wo_comp_rebuild_tbl => i_eam_wo_comp_rebuild_tbl, \n" +
            "                                            p_eam_wo_comp_mr_read_tbl => i_eam_wo_comp_mr_read_tbl, \n" +
            "                                            p_eam_op_comp_tbl         => i_eam_op_comp_tbl, \n" +
            "                                            p_eam_request_tbl         => i_eam_request_tbl, \n" +
            "                                            p_eam_permit_tbl          => i_eam_permit_tbl, \n" +
            "                                            p_eam_permit_wo_assoc_tbl => i_eam_permit_wo_assoc_tbl, \n" +
            "                                            x_eam_wo_rec              => O_eam_wo_rec, \n" +
            "                                            x_eam_op_tbl              => O_eam_op_tbl, \n" +
            "                                            x_eam_op_network_tbl      => O_eam_op_network_tbl, \n" +
            "                                            x_eam_res_tbl             => O_eam_res_tbl, \n" +
            "                                            x_eam_res_inst_tbl        => O_eam_res_inst_tbl, \n" +
            "                                            x_eam_sub_res_tbl         => O_eam_sub_res_tbl, \n" +
            "                                            x_eam_res_usage_tbl       => O_eam_res_usage_tbl, \n" +
            "                                            x_eam_mat_req_tbl         => O_eam_mat_req_tbl, \n" +
            "                                            x_eam_direct_items_tbl    => O_eam_direct_items_tbl, \n" +
            "                                            x_eam_wo_comp_rec         => O_eam_wo_comp_rec, \n" +
            "                                            x_eam_wo_quality_tbl      => O_eam_wo_quality_tbl, \n" +
            "                                            x_eam_meter_reading_tbl   => O_eam_meter_reading_tbl, \n" +
            "                                            x_eam_counter_prop_tbl    => O_eam_counter_prop_tblOUT, \n" +
            "                                            x_eam_wo_comp_rebuild_tbl => O_eam_wo_comp_rebuild_tbl, \n" +
            "                                            x_eam_wo_comp_mr_read_tbl => O_eam_wo_comp_mr_read_tbl, \n" +
            "                                            x_eam_op_comp_tbl         => O_eam_op_comp_tbl, \n" +
            "                                            x_eam_request_tbl         => O_eam_request_tbl, \n" +
            "                                            x_return_status           => O_return_status, \n" +
            "                                            x_msg_count               => O_msg_count \n" +
            "                                            --,p_debug                   => 'Y', \n" +
            "                                            --p_output_dir              => '/usr/tmp', \n" +
            "                                            --p_debug_filename          => 'MRM_WOCOMPLETE_debug.log', \n" +
            "                                            --p_debug_file_mode         => 'w' \n" +
            "                                            ); \n" +
            "             \n" +
            "            dbms_output.put_line(O_return_status);  \n" +
            "            --DBMS_OUTPUT.PUT_LINE(O_eam_wo_comp_rec.WIP_ENTITY_ID);  \n" +
            "            --DBMS_OUTPUT.PUT_LINE(O_eam_wo_comp_rec.WIP_ENTITY_NAME);  \n" +
            "             \n" +
            "              O_msg_count := fnd_msg_pub.count_msg; \n" +
            "             \n" +
            "              IF (O_msg_count > 0) THEN \n" +
            "                O_msg_index := O_msg_count; \n" +
            "                FOR i IN 1 .. O_msg_count LOOP \n" +
            "                  fnd_msg_pub.get(p_msg_index     => FND_MSG_PUB.G_NEXT, \n" +
            "                                  p_encoded       => 'F', \n" +
            "                                  p_data          => O_err_mesg, \n" +
            "                                  p_msg_index_out => O_msg_index); \n" +
            "                  dbms_output.put_line('Error ' || O_msg_index || ' : ' || O_err_mesg); \n" +
            "                 \n" +
            "                END LOOP; \n" +
            "              END IF; \n" +
            "            dbms_output.get_lines(?, num); \n" +
            "            commit; \n" +
            "            END;";

    public static final String updateWorkOrderRequestNumber = "declare              \n" +
            "        i_eam_wo_rec           EAM_PROCESS_WO_PUB.eam_wo_rec_type;        \n" +
            "        i_eam_op_tbl           EAM_PROCESS_WO_PUB.eam_op_tbl_type;         \n" +
            "        i_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type;  \n" +
            "        i_eam_res_tbl          EAM_PROCESS_WO_PUB.eam_res_tbl_type;         \n" +
            "        i_eam_res_inst_tbl     EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type;    \n" +
            "        i_eam_sub_res_tbl      EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type;     \n" +
            "        i_eam_res_usage_tbl    EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type;  \n" +
            "        i_eam_mat_req_tbl      EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type;    \n" +
            "        i_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type;  \n" +
            "        i_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type; \n" +
            "        i_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "        i_eam_meter_reading_tbl   EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "        i_eam_counter_prop_tbl   EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type  ; \n" +
            "        i_eam_wo_comp_rebuild_tbl    EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "        i_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "        i_eam_op_comp_tbl      EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "        i_eam_request_tbl          EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "        i_eam_permit_tbl           EAM_PROCESS_PERMIT_PUB.eam_wp_tbl_type; \n" +
            "        i_eam_permit_wo_assoc_tbl  EAM_PROCESS_PERMIT_PUB.eam_wp_association_tbl_type; \n" +
            "        O_eam_wo_rec               EAM_PROCESS_WO_PUB.eam_wo_rec_type; \n" +
            "        O_eam_op_tbl               EAM_PROCESS_WO_PUB.eam_op_tbl_type; \n" +
            "        O_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type; \n" +
            "        O_eam_res_tbl    EAM_PROCESS_WO_PUB.eam_res_tbl_type; \n" +
            "        O_eam_res_inst_tbl  EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type; \n" +
            "        O_eam_sub_res_tbl   EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type; \n" +
            "        O_eam_res_usage_tbl  EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type; \n" +
            "        O_eam_mat_req_tbl EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type; \n" +
            "        O_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type; \n" +
            "        O_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type; \n" +
            "        O_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "        O_eam_meter_reading_tbl EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "        O_eam_counter_prop_tblOUT  EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type; \n" +
            "        O_eam_wo_comp_rebuild_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "        O_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "        O_eam_op_comp_tbl    EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "        O_eam_request_tbl     EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "        O_return_status    VARCHAR2(50); \n" +
            "        O_msg_count        NUMBER; \n" +
            "        l_msg_count         NUMBER; \n" +
            "        msg_index NUMBER; \n" +
            "        temp_err_mesg VARCHAR2(2000); \n" +
            "        num integer := 1000; \n" +
            " \n" +
            "begin \n" +
            " \n" +
            "    fnd_global.apps_initialize (user_id => ?,resp_id => ?,resp_appl_id => ?); \n" +
            " \n" +
            "    i_eam_wo_rec.HEADER_ID := 1; \n" +
            "     \n" +
            "    i_eam_wo_rec.BATCH_ID := 1; \n" +
            "     \n" +
            "    i_eam_wo_rec.wip_entity_id := ?; \n" +
            " \n" +
            "    i_eam_wo_rec.organization_id := ?;     \n" +
            " \n" +
            "    i_eam_wo_rec.transaction_type := EAM_PROCESS_WO_PVT.G_OPR_UPDATE; \n" +
            " \n" +
            "    i_eam_request_tbl(1).HEADER_ID := 1;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).BATCH_ID := 1;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).REQUEST_NUMBER   := ?;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).REQUEST_ID := ?;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).REQUEST_TYPE := 1;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).TRANSACTION_TYPE := EAM_PROCESS_WO_PVT.G_OPR_CREATE;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).ORGANIZATION_ID := ?;\n" +
            "    \n" +
            "    i_eam_request_tbl(1).WIP_ENTITY_NAME := ?;\n" +
            "    \n" +
            " \n" +
            "EAM_PROCESS_WO_PUB.PROCESS_WO \n" +
            "       (  p_bo_identifier           => 'EAM' \n" +
            "        , p_api_version_number      =>  1.0 \n" +
            "        , p_init_msg_list           => TRUE \n" +
            "        , p_commit                  => 'Y' \n" +
            "        , p_eam_wo_rec              => i_eam_wo_rec \n" +
            "        , p_eam_op_tbl              => i_eam_op_tbl \n" +
            "        , p_eam_op_network_tbl      => i_eam_op_network_tbl \n" +
            "        , p_eam_res_tbl             => i_eam_res_tbl \n" +
            "        , p_eam_res_inst_tbl        => i_eam_res_inst_tbl \n" +
            "        , p_eam_sub_res_tbl         => i_eam_sub_res_tbl \n" +
            "        , p_eam_res_usage_tbl       => i_eam_res_usage_tbl \n" +
            "        , p_eam_mat_req_tbl         => i_eam_mat_req_tbl \n" +
            "        , p_eam_direct_items_tbl    => i_eam_direct_items_tbl \n" +
            "        , p_eam_wo_comp_rec         => i_eam_wo_comp_rec \n" +
            "        , p_eam_wo_quality_tbl      => i_eam_wo_quality_tbl \n" +
            "        , p_eam_meter_reading_tbl   => i_eam_meter_reading_tbl \n" +
            "        , p_eam_counter_prop_tbl    => i_eam_counter_prop_tbl \n" +
            "        , p_eam_wo_comp_rebuild_tbl => i_eam_wo_comp_rebuild_tbl \n" +
            "        , p_eam_wo_comp_mr_read_tbl => i_eam_wo_comp_mr_read_tbl \n" +
            "        , p_eam_op_comp_tbl         => i_eam_op_comp_tbl \n" +
            "        , p_eam_request_tbl         => i_eam_request_tbl \n" +
            "        , p_eam_permit_tbl          => i_eam_permit_tbl \n" +
            "        , p_eam_permit_wo_assoc_tbl => i_eam_permit_wo_assoc_tbl \n" +
            "        , x_eam_wo_rec              => O_eam_wo_rec \n" +
            "        , x_eam_op_tbl              => O_eam_op_tbl \n" +
            "        , x_eam_op_network_tbl      => O_eam_op_network_tbl \n" +
            "        , x_eam_res_tbl             => O_eam_res_tbl \n" +
            "        , x_eam_res_inst_tbl        => O_eam_res_inst_tbl \n" +
            "        , x_eam_sub_res_tbl         => O_eam_sub_res_tbl \n" +
            "        , x_eam_res_usage_tbl       => O_eam_res_usage_tbl \n" +
            "        , x_eam_mat_req_tbl         => O_eam_mat_req_tbl \n" +
            "        , x_eam_direct_items_tbl    => O_eam_direct_items_tbl \n" +
            "        , x_eam_wo_comp_rec         => O_eam_wo_comp_rec \n" +
            "        , x_eam_wo_quality_tbl      => O_eam_wo_quality_tbl \n" +
            "        , x_eam_meter_reading_tbl   => O_eam_meter_reading_tbl \n" +
            "        , x_eam_counter_prop_tbl    => O_eam_counter_prop_tblOUT \n" +
            "        , x_eam_wo_comp_rebuild_tbl => O_eam_wo_comp_rebuild_tbl \n" +
            "        , x_eam_wo_comp_mr_read_tbl => O_eam_wo_comp_mr_read_tbl \n" +
            "        , x_eam_op_comp_tbl         => O_eam_op_comp_tbl \n" +
            "        , x_eam_request_tbl         => O_eam_request_tbl \n" +
            "        , x_return_status           => O_return_status \n" +
            "        , x_msg_count               => O_msg_count \n" +
            "        --, p_debug                   => 'Y' \n" +
            "        --, p_output_dir              => '/usr/tmp' \n" +
            "        --, p_debug_filename          => 'EAM_WO_DEBUGa.log'        , p_debug_file_mode         => 'w' \n" +
            "        ); \n" +
            "         \n" +
            "dbms_output.put_line(O_return_status); \n" +
            "DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_ID); \n" +
            "DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_NAME); \n" +
            "\n" +
            "l_msg_count := fnd_msg_pub.count_msg; \n" +
            " \n" +
            "IF(l_msg_count>0) THEN \n" +
            "msg_index := l_msg_count; \n" +
            "FOR i IN 1..l_msg_count LOOP \n" +
            "fnd_msg_pub.get \n" +
            "            (p_msg_index => FND_MSG_PUB.G_NEXT, \n" +
            "   p_encoded   => 'F', \n" +
            "   p_data      => temp_err_mesg, \n" +
            "   p_msg_index_out => msg_index); \n" +
            "dbms_output.put_line('Error:' || msg_index||':'||temp_err_mesg); \n" +
            " \n" +
            "END LOOP; \n" +
            "END IF; \n" +
            "dbms_output.get_lines(?, num); \n" +
            " \n" +
            "commit; \n" +
            " \n" +
            " \n" +
            " \n" +
            "END; \n";


    public static final String sendWorkOrderForApproval = "declare              \n" +
            "        i_eam_wo_rec           EAM_PROCESS_WO_PUB.eam_wo_rec_type;        \n" +
            "        i_eam_op_tbl           EAM_PROCESS_WO_PUB.eam_op_tbl_type;         \n" +
            "        i_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type;  \n" +
            "        i_eam_res_tbl          EAM_PROCESS_WO_PUB.eam_res_tbl_type;         \n" +
            "        i_eam_res_inst_tbl     EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type;    \n" +
            "        i_eam_sub_res_tbl      EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type;     \n" +
            "        i_eam_res_usage_tbl    EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type;  \n" +
            "        i_eam_mat_req_tbl      EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type;    \n" +
            "        i_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type;  \n" +
            "        i_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type; \n" +
            "        i_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "        i_eam_meter_reading_tbl   EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "        i_eam_counter_prop_tbl   EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type  ; \n" +
            "        i_eam_wo_comp_rebuild_tbl    EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "        i_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "        i_eam_op_comp_tbl      EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "        i_eam_request_tbl          EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "        i_eam_permit_tbl           EAM_PROCESS_PERMIT_PUB.eam_wp_tbl_type; \n" +
            "        i_eam_permit_wo_assoc_tbl  EAM_PROCESS_PERMIT_PUB.eam_wp_association_tbl_type; \n" +
            "        O_eam_wo_rec               EAM_PROCESS_WO_PUB.eam_wo_rec_type; \n" +
            "        O_eam_op_tbl               EAM_PROCESS_WO_PUB.eam_op_tbl_type; \n" +
            "        O_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type; \n" +
            "        O_eam_res_tbl    EAM_PROCESS_WO_PUB.eam_res_tbl_type; \n" +
            "        O_eam_res_inst_tbl  EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type; \n" +
            "        O_eam_sub_res_tbl   EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type; \n" +
            "        O_eam_res_usage_tbl  EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type; \n" +
            "        O_eam_mat_req_tbl EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type; \n" +
            "        O_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type; \n" +
            "        O_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type; \n" +
            "        O_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "        O_eam_meter_reading_tbl EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "        O_eam_counter_prop_tblOUT  EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type; \n" +
            "        O_eam_wo_comp_rebuild_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "        O_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "        O_eam_op_comp_tbl    EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "        O_eam_request_tbl     EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "        O_return_status    VARCHAR2(50); \n" +
            "        O_msg_count        NUMBER; \n" +
            "        l_msg_count         NUMBER; \n" +
            "        msg_index NUMBER; \n" +
            "        temp_err_mesg VARCHAR2(2000); \n" +
            "        num integer := 1000; \n" +
            " \n" +
            "begin \n" +
            " \n" +
            "    fnd_global.apps_initialize (user_id => ?,resp_id => ?,resp_appl_id => ?); \n" +
            " \n" +
            "    i_eam_wo_rec.HEADER_ID := 1; \n" +
            "     \n" +
            "    i_eam_wo_rec.BATCH_ID := 1; \n" +
            "     \n" +
            "    i_eam_wo_rec.wip_entity_id := ?; \n" +
            " \n" +
            "    i_eam_wo_rec.organization_id := ?;     \n" +
            " \n" +
            "    i_eam_wo_rec.transaction_type := EAM_PROCESS_WO_PVT.G_OPR_UPDATE; \n" +
            "     \n" +
            "    i_eam_wo_rec.status_type := 3; \n" +
            "     \n" +
            "EAM_PROCESS_WO_PUB.PROCESS_WO \n" +
            "       (  p_bo_identifier           => 'EAM' \n" +
            "        , p_api_version_number      =>  1.0 \n" +
            "        , p_init_msg_list           => TRUE \n" +
            "        , p_commit                  => 'Y' \n" +
            "        , p_eam_wo_rec              => i_eam_wo_rec \n" +
            "        , p_eam_op_tbl              => i_eam_op_tbl \n" +
            "        , p_eam_op_network_tbl      => i_eam_op_network_tbl \n" +
            "        , p_eam_res_tbl             => i_eam_res_tbl \n" +
            "        , p_eam_res_inst_tbl        => i_eam_res_inst_tbl \n" +
            "        , p_eam_sub_res_tbl         => i_eam_sub_res_tbl \n" +
            "        , p_eam_res_usage_tbl       => i_eam_res_usage_tbl \n" +
            "        , p_eam_mat_req_tbl         => i_eam_mat_req_tbl \n" +
            "        , p_eam_direct_items_tbl    => i_eam_direct_items_tbl \n" +
            "        , p_eam_wo_comp_rec         => i_eam_wo_comp_rec \n" +
            "        , p_eam_wo_quality_tbl      => i_eam_wo_quality_tbl \n" +
            "        , p_eam_meter_reading_tbl   => i_eam_meter_reading_tbl \n" +
            "        , p_eam_counter_prop_tbl    => i_eam_counter_prop_tbl \n" +
            "        , p_eam_wo_comp_rebuild_tbl => i_eam_wo_comp_rebuild_tbl \n" +
            "        , p_eam_wo_comp_mr_read_tbl => i_eam_wo_comp_mr_read_tbl \n" +
            "        , p_eam_op_comp_tbl         => i_eam_op_comp_tbl \n" +
            "        , p_eam_request_tbl         => i_eam_request_tbl \n" +
            "        , p_eam_permit_tbl          => i_eam_permit_tbl \n" +
            "        , p_eam_permit_wo_assoc_tbl => i_eam_permit_wo_assoc_tbl \n" +
            "        , x_eam_wo_rec              => O_eam_wo_rec \n" +
            "        , x_eam_op_tbl              => O_eam_op_tbl \n" +
            "        , x_eam_op_network_tbl      => O_eam_op_network_tbl \n" +
            "        , x_eam_res_tbl             => O_eam_res_tbl \n" +
            "        , x_eam_res_inst_tbl        => O_eam_res_inst_tbl \n" +
            "        , x_eam_sub_res_tbl         => O_eam_sub_res_tbl \n" +
            "        , x_eam_res_usage_tbl       => O_eam_res_usage_tbl \n" +
            "        , x_eam_mat_req_tbl         => O_eam_mat_req_tbl \n" +
            "        , x_eam_direct_items_tbl    => O_eam_direct_items_tbl \n" +
            "        , x_eam_wo_comp_rec         => O_eam_wo_comp_rec \n" +
            "        , x_eam_wo_quality_tbl      => O_eam_wo_quality_tbl \n" +
            "        , x_eam_meter_reading_tbl   => O_eam_meter_reading_tbl \n" +
            "        , x_eam_counter_prop_tbl    => O_eam_counter_prop_tblOUT \n" +
            "        , x_eam_wo_comp_rebuild_tbl => O_eam_wo_comp_rebuild_tbl \n" +
            "        , x_eam_wo_comp_mr_read_tbl => O_eam_wo_comp_mr_read_tbl \n" +
            "        , x_eam_op_comp_tbl         => O_eam_op_comp_tbl \n" +
            "        , x_eam_request_tbl         => O_eam_request_tbl \n" +
            "        , x_return_status           => O_return_status \n" +
            "        , x_msg_count               => O_msg_count \n" +
            "        --, p_debug                   => 'Y' \n" +
            "        --, p_output_dir              => '/usr/tmp' \n" +
            "        --, p_debug_filename          => 'EAM_WO_DEBUGa.log'        , p_debug_file_mode         => 'w' \n" +
            "        ); \n" +
            "         \n" +
            "dbms_output.put_line(O_return_status); \n" +
            "DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_ID); \n" +
            "DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_NAME); \n" +
            "\n" +
            "l_msg_count := fnd_msg_pub.count_msg; \n" +
            " \n" +
            "IF(l_msg_count>0) THEN \n" +
            "msg_index := l_msg_count; \n" +
            "FOR i IN 1..l_msg_count LOOP \n" +
            "fnd_msg_pub.get \n" +
            "            (p_msg_index => FND_MSG_PUB.G_NEXT, \n" +
            "   p_encoded   => 'F', \n" +
            "   p_data      => temp_err_mesg, \n" +
            "   p_msg_index_out => msg_index); \n" +
            "dbms_output.put_line('Error:' || msg_index||':'||temp_err_mesg); \n" +
            " \n" +
            "END LOOP; \n" +
            "END IF; \n" +
            "dbms_output.get_lines(?, num); \n" +
            " \n" +
            "commit; \n" +
            " \n" +
            " \n" +
            " \n" +
            "END; \n";

    public static final String updateWorkOrder = "declare         \n" +
            "            \n" +
            "            i_eam_wo_rec           EAM_PROCESS_WO_PUB.eam_wo_rec_type;        \n" +
            "            i_eam_op_tbl           EAM_PROCESS_WO_PUB.eam_op_tbl_type;         \n" +
            "            i_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type;  \n" +
            "            i_eam_res_tbl          EAM_PROCESS_WO_PUB.eam_res_tbl_type;         \n" +
            "            i_eam_res_inst_tbl     EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type;    \n" +
            "            i_eam_sub_res_tbl      EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type;     \n" +
            "            i_eam_res_usage_tbl    EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type;  \n" +
            "            i_eam_mat_req_tbl      EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type;    \n" +
            "            i_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type;  \n" +
            "            i_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type; \n" +
            "            i_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "            i_eam_meter_reading_tbl   EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "            i_eam_counter_prop_tbl   EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type  ; \n" +
            "            i_eam_wo_comp_rebuild_tbl    EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "            i_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "            i_eam_op_comp_tbl      EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "            i_eam_request_tbl          EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "            i_eam_permit_tbl           EAM_PROCESS_PERMIT_PUB.eam_wp_tbl_type; \n" +
            "            i_eam_permit_wo_assoc_tbl  EAM_PROCESS_PERMIT_PUB.eam_wp_association_tbl_type; \n" +
            "            O_eam_wo_rec               EAM_PROCESS_WO_PUB.eam_wo_rec_type; \n" +
            "            O_eam_op_tbl               EAM_PROCESS_WO_PUB.eam_op_tbl_type; \n" +
            "            O_eam_op_network_tbl   EAM_PROCESS_WO_PUB.eam_op_network_tbl_type; \n" +
            "            O_eam_res_tbl    EAM_PROCESS_WO_PUB.eam_res_tbl_type; \n" +
            "            O_eam_res_inst_tbl  EAM_PROCESS_WO_PUB.eam_res_inst_tbl_type; \n" +
            "            O_eam_sub_res_tbl   EAM_PROCESS_WO_PUB.eam_sub_res_tbl_type; \n" +
            "            O_eam_res_usage_tbl  EAM_PROCESS_WO_PUB.eam_res_usage_tbl_type; \n" +
            "            O_eam_mat_req_tbl EAM_PROCESS_WO_PUB.eam_mat_req_tbl_type; \n" +
            "            O_eam_direct_items_tbl EAM_PROCESS_WO_PUB.eam_direct_items_tbl_type; \n" +
            "            O_eam_wo_comp_rec      EAM_PROCESS_WO_PUB.eam_wo_comp_rec_type; \n" +
            "            O_eam_wo_quality_tbl   EAM_PROCESS_WO_PUB.eam_wo_quality_tbl_type; \n" +
            "            O_eam_meter_reading_tbl EAM_PROCESS_WO_PUB.eam_meter_reading_tbl_type; \n" +
            "            O_eam_counter_prop_tblOUT  EAM_PROCESS_WO_PUB.eam_counter_prop_tbl_type; \n" +
            "            O_eam_wo_comp_rebuild_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_rebuild_tbl_type; \n" +
            "            O_eam_wo_comp_mr_read_tbl  EAM_PROCESS_WO_PUB.eam_wo_comp_mr_read_tbl_type; \n" +
            "            O_eam_op_comp_tbl    EAM_PROCESS_WO_PUB.eam_op_comp_tbl_type; \n" +
            "            O_eam_request_tbl     EAM_PROCESS_WO_PUB.eam_request_tbl_type; \n" +
            "            O_return_status    VARCHAR2(50); \n" +
            "            O_msg_count        NUMBER; \n" +
            "            l_msg_count         NUMBER; \n" +
            "            msg_index NUMBER; \n" +
            "            temp_err_mesg VARCHAR2(2000); \n" +
            "            num integer := 1000;\n" +
            "             \n" +
            "            begin \n" +
            "             \n" +
            "            fnd_global.apps_initialize (user_id => ?,resp_id => ?,resp_appl_id => ?); \n" +
            "            \n" +
            "            i_eam_wo_rec.wip_entity_id := ?; \n" +
            "            \n" +
            "            i_eam_wo_rec.organization_id := ?; \n" +
            "            \n" +
            "            i_eam_wo_rec.transaction_type := EAM_PROCESS_WO_PVT.G_OPR_UPDATE; \n" +
            "            \n" +
            "            i_eam_wo_rec.scheduled_start_date := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.scheduled_completion_date := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.asset_number    := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.maintenance_object_id  := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.asset_group_id  := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.owning_department := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.class_code := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.asset_activity_id := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.description := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.firm_planned_flag := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.status_type := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.work_order_type := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.shutdown_type := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.priority := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.ACTIVITY_TYPE := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.ACTIVITY_CAUSE := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.ACTIVITY_SOURCE := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.MATERIAL_ISSUE_BY_MO := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.PLAN_MAINTENANCE := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.WARRANTY_CLAIM_STATUS := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.WARRANTY_ACTIVE := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.TAGOUT_REQUIRED := ?;\n" +
            "            \n" +
            "            i_eam_wo_rec.NOTIFICATION_REQUIRED := ?;\n" +
            "             \n" +
            "            EAM_PROCESS_WO_PUB.PROCESS_WO \n" +
            "                   (  p_bo_identifier           => 'EAM' \n" +
            "                    , p_api_version_number      =>  1.0 \n" +
            "                    , p_init_msg_list           => TRUE \n" +
            "                    , p_commit                  => 'Y' \n" +
            "                    , p_eam_wo_rec              => i_eam_wo_rec \n" +
            "                    , p_eam_op_tbl              => i_eam_op_tbl \n" +
            "                    , p_eam_op_network_tbl      => i_eam_op_network_tbl \n" +
            "                    , p_eam_res_tbl             => i_eam_res_tbl \n" +
            "                    , p_eam_res_inst_tbl        => i_eam_res_inst_tbl \n" +
            "                    , p_eam_sub_res_tbl         => i_eam_sub_res_tbl \n" +
            "                    , p_eam_res_usage_tbl       => i_eam_res_usage_tbl \n" +
            "                    , p_eam_mat_req_tbl         => i_eam_mat_req_tbl \n" +
            "                    , p_eam_direct_items_tbl    => i_eam_direct_items_tbl \n" +
            "                    , p_eam_wo_comp_rec         => i_eam_wo_comp_rec \n" +
            "                    , p_eam_wo_quality_tbl      => i_eam_wo_quality_tbl \n" +
            "                    , p_eam_meter_reading_tbl   => i_eam_meter_reading_tbl \n" +
            "                    , p_eam_counter_prop_tbl    => i_eam_counter_prop_tbl \n" +
            "                    , p_eam_wo_comp_rebuild_tbl => i_eam_wo_comp_rebuild_tbl \n" +
            "                    , p_eam_wo_comp_mr_read_tbl => i_eam_wo_comp_mr_read_tbl \n" +
            "                    , p_eam_op_comp_tbl         => i_eam_op_comp_tbl \n" +
            "                    , p_eam_request_tbl         => i_eam_request_tbl \n" +
            "                    , p_eam_permit_tbl          => i_eam_permit_tbl \n" +
            "                    , p_eam_permit_wo_assoc_tbl => i_eam_permit_wo_assoc_tbl \n" +
            "                    , x_eam_wo_rec              => O_eam_wo_rec \n" +
            "                    , x_eam_op_tbl              => O_eam_op_tbl \n" +
            "                    , x_eam_op_network_tbl      => O_eam_op_network_tbl \n" +
            "                    , x_eam_res_tbl             => O_eam_res_tbl \n" +
            "                    , x_eam_res_inst_tbl        => O_eam_res_inst_tbl \n" +
            "                    , x_eam_sub_res_tbl         => O_eam_sub_res_tbl \n" +
            "                    , x_eam_res_usage_tbl       => O_eam_res_usage_tbl \n" +
            "                    , x_eam_mat_req_tbl         => O_eam_mat_req_tbl \n" +
            "                    , x_eam_direct_items_tbl    => O_eam_direct_items_tbl \n" +
            "                    , x_eam_wo_comp_rec         => O_eam_wo_comp_rec \n" +
            "                    , x_eam_wo_quality_tbl      => O_eam_wo_quality_tbl \n" +
            "                    , x_eam_meter_reading_tbl   => O_eam_meter_reading_tbl \n" +
            "                    , x_eam_counter_prop_tbl    => O_eam_counter_prop_tblOUT \n" +
            "                    , x_eam_wo_comp_rebuild_tbl => O_eam_wo_comp_rebuild_tbl \n" +
            "                    , x_eam_wo_comp_mr_read_tbl => O_eam_wo_comp_mr_read_tbl \n" +
            "                    , x_eam_op_comp_tbl         => O_eam_op_comp_tbl \n" +
            "                    , x_eam_request_tbl         => O_eam_request_tbl \n" +
            "                    , x_return_status           => O_return_status \n" +
            "                    , x_msg_count               => O_msg_count \n" +
            "                    --, p_debug                   => 'Y' \n" +
            "                    --, p_output_dir              => '/usr/tmp' \n" +
            "                    --, p_debug_filename          => 'EAM_WO_DEBUGa.log'        , p_debug_file_mode         => 'w' \n" +
            "                    ); \n" +
            "                     \n" +
            "            dbms_output.put_line(O_return_status); \n" +
            "            DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_ID); \n" +
            "            DBMS_OUTPUT.PUT_LINE(O_eam_wo_rec.WIP_ENTITY_NAME); \n" +
            "            \n" +
            "            l_msg_count := fnd_msg_pub.count_msg; \n" +
            "             \n" +
            "            IF(l_msg_count>0) THEN \n" +
            "            msg_index := l_msg_count; \n" +
            "            FOR i IN 1..l_msg_count LOOP \n" +
            "            fnd_msg_pub.get \n" +
            "                        (p_msg_index => FND_MSG_PUB.G_NEXT, \n" +
            "               p_encoded   => 'F', \n" +
            "               p_data      => temp_err_mesg, \n" +
            "               p_msg_index_out => msg_index); \n" +
            "            dbms_output.put_line('Error:' || msg_index||':'||temp_err_mesg); \n" +
            "             \n" +
            "            END LOOP; \n" +
            "            END IF; \n" +
            "            dbms_output.get_lines(?, num); \n" +
            "             \n" +
            "            commit; \n" +
            "             \n" +
            "             \n" +
            "             \n" +
            "            END;";

}
