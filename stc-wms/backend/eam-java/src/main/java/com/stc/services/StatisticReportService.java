package com.stc.services;

import com.stc.controller.StatisticReportController;
import com.stc.controller.WorkRequestController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Date;

/**
 * Created with IntelliJ IDEA. User: Ardi sugiarto Date: 02/08/18 Time: 9:31 To
 * change this template use File | Settings | File Templates.
 */
@Path("/statistic")
public class StatisticReportService {

    @GET
    @Path("woStatusReportCM_EM")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response woStatusReportCM_EM(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") String orgId) {

        if (dateFrom == null || dateTo == null || orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.woStatusReportCM_EM(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("woStatusReportCM_Civil")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response woStatusReportCM_Civil(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") String orgId) {

        if (dateFrom == null || dateTo == null || orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.woStatusReportCM_Civil(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("woStatusReportPMR_EM")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response woStatusReportPMR_EM(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") String orgId) {

        if (dateFrom == null || dateTo == null || orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.woStatusReportPMR_EM(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("woStatusReportPMR_Civil")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response woStatusReportPMR_Civil(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") String orgId) {

        if (dateFrom == null || dateTo == null || orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.woStatusReportPMR_Civil(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("woStatusReportIgate")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response woStatusReportIgate(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") String orgId) {

        if (dateFrom == null || dateTo == null || orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.woStatusReportIgate(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("satisfactionLevel")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response satisfactionLevel(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") String orgId) {

        if (dateFrom == null || dateTo == null || orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.satisfactionLevel(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("CMworkOrderSummary")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response CMworkOrderSummary(@QueryParam("dateFrom") Date dateFrom,
            @QueryParam("dateTo") Date dateTo) {

        if (dateFrom == null || dateTo == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.CMworkOrderSummary(dateFrom, dateTo);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("PMRworkOrderSummary")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response PMRworkOrderSummary(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo) {

        if (dateFrom == null || dateTo == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.PMRworkOrderSummary(dateFrom, dateTo);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("summaryMaintenanceType")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response summaryMaintenanceType(@QueryParam("month") String month,
            @QueryParam("year") String year) {

        if (month == null || year == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.summaryMaintenanceType(month, year);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("createdClosedWeeklyReport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response createdClosedWeeklyReport(@QueryParam("year") String year,
            @QueryParam("month") String month) {

        if (year == null || month == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.createdClosedWeeklyReport(year, month);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("cmemCcpReport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response cmemCcpReport(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") int orgId) {

        if (dateFrom == null || dateTo == null || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.CM_EM_ccpReport(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("cmcivilccpreport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response CMCivilccpReport(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") int orgId) {

        if (dateFrom == null || dateTo == null || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.CM_Civil_ccpReport(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }

    }

    @GET
    @Path("igateccpreport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response Igate_ccpReport(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") int orgId) {

        if (dateFrom == null || dateTo == null || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.Igate_ccpReport(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("pmremccpReport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response pmremccpReport(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") int orgId) {

        if (dateFrom == null || dateTo == null || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.PMR_EM_ccpReport(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("pmrcivilccpReport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response pmrcivilccpReport(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("orgId") int orgId) {

        if (dateFrom == null || dateTo == null || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.PMR_CIVIL_ccpReport(dateFrom, dateTo, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("CMWorkOrderSumm")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response CMWorkOrderSumm(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo) {

        if (dateFrom == null || dateTo == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.CMWorkOrderSumm(dateFrom, dateTo);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("PMRWOccpreport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response PMRWOccpreport(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo) {

        if (dateFrom == null || dateTo == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.PMR_WO_ccpreport(dateFrom, dateTo);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("PMRWorkOrderSumm")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response PMRWorkOrderSumm(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo) {

        if (dateFrom == null || dateTo == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.PMRWorkOrderSumm(dateFrom, dateTo);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("summaryMaintinaceHQC")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response summaryMaintinaceHQC(@QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo,
            @QueryParam("lan") String lan,
            @QueryParam("orgId") int orgId) {

        if (dateFrom == null || dateTo == null || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.summaryMaintinaceHQC(dateFrom, dateTo, orgId, lan);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWeeklyReport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWeeklyReport(@QueryParam("orgId") String orgId,
            @QueryParam("fromDate") String fromDate,
            @QueryParam("toDate") String toDate,
            @QueryParam("reportName") String reportName
    ) {

        if (orgId == null || fromDate == null || toDate == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.getWeeklyReport(orgId, fromDate, toDate, reportName);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("claimEntryStatusReport")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response claimEntryStatus(@QueryParam("period") String period,
            @QueryParam("area") String area
    ) {

        if (period == null || area == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            StatisticReportController statisticReportController = new StatisticReportController();
            result = statisticReportController.claimEntryStatus(period, area);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("claimEntryStatusAreaLOV")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response claimEntryStatusAreaLOV() {

        String result;
        StatisticReportController statisticReportController = new StatisticReportController();
        result = statisticReportController.claimEntryStatusAreaLOV();
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    
    @GET
    @Path("claimEntryStatusPeriodLOV")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response claimEntryStatusPeriodLOV() {

        String result;
        StatisticReportController statisticReportController = new StatisticReportController();
        result = statisticReportController.claimEntryStatusPeriodLOV();
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
}
