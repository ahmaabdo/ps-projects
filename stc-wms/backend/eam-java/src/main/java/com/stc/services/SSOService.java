/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.SOOController;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Anas Alghawi
 */
@Path("/SSO")
public class SSOService {

    @GET
    @Path("getIdentity")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getIdentity(@QueryParam("token") String token) throws IOException, NoSuchAlgorithmException, KeyManagementException {

        if (token == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            SOOController stcWebService = new SOOController();
            result = stcWebService.getIdentity(token);
             System.out.println(result);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
                
            }
           
        }
    }
    
//    public static void main(String args[]) throws IOException, NoSuchAlgorithmException, KeyManagementException{
//        
//        SSOService a= new SSOService();
//        a.getIdentity("vQKBv2e2rYq4z9fSgc8D");
//    }

}
