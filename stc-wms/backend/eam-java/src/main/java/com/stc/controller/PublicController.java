package com.stc.controller;

import com.stc.db.ConnectionPool;
import com.stc.objects.ItemDetails;
import org.apache.log4j.Logger;

import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 09/01/18
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
public class PublicController {
    final static Logger logger = Logger.getLogger(PublicController.class);

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public CallableStatement callableStatement;

    public String initializeUser(int userId, int respId, int respAppId) {
        String result = "";

        try{
            connection  = pool.getConnection();

            callableStatement = connection.prepareCall("{call FND_GLOBAL.APPS_INITIALIZE (?,?,?)}");
            callableStatement.setInt(1, userId);
            callableStatement.setInt(2, respId);
            callableStatement.setInt(3, respAppId);
            callableStatement.executeUpdate();

            connection.close();
            callableStatement.close();

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Initialize User Error",e);
        }
        finally {
            closeAllConnection(connection,callableStatement);
        }
        return result;
    }

    public ItemDetails getItemDetails(int notificationId) {

        ItemDetails itemDetails = null;

        try{
            connection  = pool.getConnection();

            callableStatement = connection.prepareCall("{call WF_NOTIFICATION.getNtfActInfo(?,?,?,?)}");
            callableStatement.setInt(1, notificationId);
            callableStatement.registerOutParameter(2, Types.VARCHAR);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, Types.NUMERIC);
            callableStatement.execute();

            String itemType = callableStatement.getString(2);
            String itemKey = callableStatement.getString(3);
            int actId = callableStatement.getInt(4);
            itemDetails = new ItemDetails(itemType,itemKey,actId);

            connection.close();
            callableStatement.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Item Details Error",e);
        }
        finally {
            closeAllConnection(connection,callableStatement);
        }
        return itemDetails;
    }

    void closeAllConnection(Connection connection,
                            CallableStatement callableStatement) {

        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

    }
}
