/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.MainController;
import com.stc.controller.StcWmsMainController;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;

/**
 *
 * @author lenovo
 */
@Path("MainServices")
public class StcWmsMainServices {
    
    //***************************************validate User Login**********************//
    @POST
    @Path("validateLogin")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response validateLogin(String data) {
        JSONObject jsonObject = new JSONObject(data);
        String username=jsonObject.getString("userName");
        String password=jsonObject.getString("password");
        if (username == null || password == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.validateLogin(username,password);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    //***************************************Verifiy User Login**********************//
    @POST
    @Path("VerifiyLogin")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response VerifiyLogin(String data) {
        JSONObject jsonObject = new JSONObject(data);
        String sessionId=jsonObject.getString("sessionId");
        String verifiyCode=jsonObject.getString("verifiyCode");
        if (sessionId == null || verifiyCode == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
                StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.validateVerifiy(sessionId,verifiyCode);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
     //******************************* get LoginDetails****************************//
    @GET
    @Path("UserLoginDetails")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getUserLoginDetails(@QueryParam("userName") String userName) {
    
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getUserLoginDetails(userName);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
    
    }
    
    
    //******************************* get PoNumber****************************//
    @GET
    @Path("getPoNumber")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getPoNumber(@QueryParam("OrganizationId") String OrganizationId) {
         
        
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getPoNumber(OrganizationId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
    
    }
    @GET
    @Path("getPoNumberExpress")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getPoNumberExpress(@QueryParam("OrganizationId") String OrganizationId,@QueryParam("poID") String poID) {
         
        
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getPoNumberExpress(OrganizationId,poID);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
    
    }
       //******************************* get ORG_ID****************************//
    @GET
    @Path("ORG_ID")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getORG_ID(@QueryParam("po_header_id") int po_header_id) {
         
        if (po_header_id ==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getORG_ID(po_header_id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
      //******************************* get Supplier****************************//
    @GET
    @Path("Supplier")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getSupplier(@QueryParam("po_header_id") int po_header_id) {
         
        if (po_header_id ==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getSupplier(po_header_id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
    
     //******************************* get LineNumber****************************//
    @GET
    @Path("LineNumber")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLineNumber(@QueryParam("po_header_id") int po_header_id) {
         
        if (po_header_id ==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getLineNumber(po_header_id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
    //******************************* get ItemDescription****************************//
    @GET
    @Path("ItemDescription")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getItemDescription(@QueryParam("po_header_id") int po_header_id) {
         
        if (po_header_id ==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getItemDescription(po_header_id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }  
    
    //******************************* get Location****************************//
    @GET
    @Path("Location")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLocation(@QueryParam("po_header_id") int po_header_id, @QueryParam("Lang") String lang) {
         
        if (po_header_id ==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getLocation(po_header_id ,lang);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }   
     //******************************* get Quantity****************************//
    @GET
    @Path("Quantity")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getQuantity(@QueryParam("po_line_id") String po_line_id) {
         
        if (po_line_id.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getQuantityByLpn(po_line_id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }   
    //******************************* get Uom****************************//
    @GET
    @Path("GetUom")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response GetUom(@QueryParam("po_line_id") int po_line_id) {
        if (po_line_id ==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getUom(po_line_id);
            System.out.println(result);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }   
    
    
    
    
     //******************************* get Organization****************************//
    @GET
    @Path("Organization")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getOrganization(@QueryParam("lang") String lang ,@QueryParam("responsibilityId") String responsibilityId) {
         
        if (lang.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getOrganization(lang,responsibilityId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }   
      //******************************* get Responsibility****************************//
    @GET
    @Path("Responsibility")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getResponsibility(@QueryParam("userId") int userId,@QueryParam("lang") String lang) {
         
        if (lang.isEmpty() ||userId==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getResponsibility(userId,lang);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }   
    
          //******************************* get LPN****************************//
    @GET
    @Path("LPN")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLPN(@QueryParam("po_header_id") int po_header_id) {
         
        if (po_header_id == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getLPn(po_header_id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
    
    //******************************* get LPN Inspect****************************//
    @GET
    @Path("LPNInspect")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLPNInspect(@QueryParam("orgId") int orgId) {
         
        if (orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getLPnInspect(orgId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    //******************************* get LPN Inspect****************************//
    @GET
    @Path("LPNDeliver")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLPNDeliver(@QueryParam("orgId") String orgId) {
         
        if (orgId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getLPnDeliver(orgId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    
     //******************************* get LPN Inspect****************************//
    @GET
    @Path("DeliverQuantity")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response DeliverQuantity(@QueryParam("lpn") String lpn) {
         
        if (lpn.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.DeliverQuantity(lpn);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    
    
       //******************************* get Quantity****************************//
    @GET
    @Path("QuantityByLpn")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getQuantityByLpn(@QueryParam("QuantityByLpn") String QuantityByLpn) {
         
        if (QuantityByLpn.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getQuantityByLpn(QuantityByLpn);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
           //******************************* get deliver Ponumber****************************//
    @GET
    @Path("deliverPoNumber")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getDeliverPoNumber(@QueryParam("orgId") String orgId) {
         
        if (orgId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getDeliverPoNumber(orgId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
           //******************************* get Return Ponumber****************************//
    @GET
    @Path("returnPoNumber")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getReturnPoNumber(@QueryParam("orgId") String orgId) {
         
        if (orgId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getReturnPoNumber(orgId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
           //******************************* get Return Ponumber****************************//
    @GET
    @Path("checkSerializedFlag")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response checkSerializedFlag(@QueryParam("orgId") String orgId ,@QueryParam("itemId") String itemId ,@QueryParam("po_line_Id") String po_line_Id) {
         
        if (orgId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.checkSerializedFlag(orgId,itemId,po_line_Id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
    //******************************* get sub inventory****************************//
    @GET
    @Path("getSubInventory")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response Sub_Inventory(@QueryParam("orgId") String orgId) {
        if (orgId.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getSubInventory(orgId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();

            }

        }

    }
//******************************* get locator****************************//

    @GET
    @Path("getLocator")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response Locator(@QueryParam("sub_inv_code") String sub_inv_code,@QueryParam("orgId") String orgId) {
        if (sub_inv_code.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getLocator(sub_inv_code,orgId);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();

            }

        }

    }


@GET
@Path("getRejReason")
@Consumes({"application/xml", "application/json", "text/plain", "text/html"})
@Produces({"application/xml", "application/json", "text/plain", "text/html"})
public Response getRej_Reason(){
String result;
 StcWmsMainController mainController = new StcWmsMainController();
 result=mainController.getRejReason();
 if(result.isEmpty()){
 return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
 }else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        }

}
    
@GET
@Path("getTranslation")
@Consumes({"application/xml", "application/json", "text/plain", "text/html"})
@Produces({"application/xml", "application/json", "text/plain", "text/html"})
public Response getTranslation(){
String result;
 StcWmsMainController mainController = new StcWmsMainController();
 result=mainController.getTranslationLbl();
 if(result.isEmpty()){
 return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
 }else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        }

}
@GET
@Path("getLpnGeneration")
@Consumes({"application/xml", "application/json", "text/plain", "text/html"})
@Produces({"application/xml", "application/json", "text/plain", "text/html"})
public Response getLPNGeneration(@QueryParam("po_HEADER_ID") String po_HEADER_ID, @QueryParam("po_LINE_ID") String po_LINE_ID,@QueryParam("lang") String lang){
String result;
 StcWmsMainController mainController = new StcWmsMainController();
 result=mainController.getLPNGeneration(po_HEADER_ID,po_LINE_ID,lang);
 if(result.isEmpty()){
 return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
 }else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        }

}

    @GET
    @Path("QuantityRemaining")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getQuantityRemaining(@QueryParam("po_Header_id") String po_Header_id,@QueryParam("po_line_id") String po_line_id, @QueryParam("org_Id") String org_Id) {
         
        if (po_line_id.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            StcWmsMainController mainController = new StcWmsMainController();
            result = mainController.getQuantityRemaing(po_Header_id,po_line_id,org_Id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 



}
