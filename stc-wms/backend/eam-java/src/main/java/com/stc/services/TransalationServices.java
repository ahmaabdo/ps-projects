/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.TranslationDAO;
import com.stc.objects.TranslationBean;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;

/**
 *
 * @author lenovo
 */
@Path("Translation")
public class TransalationServices {
    @GET
    @Path("/GetAll")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllJobs() {
        ArrayList<TranslationBean> list = new ArrayList<TranslationBean>();
        TranslationDAO translationObj = new TranslationDAO();
        try {
            list = translationObj.getAllTranslation();
            return new JSONArray(list).toString();
        } catch (Exception e) {
            return new JSONArray(list).toString();
        }
    }
}
