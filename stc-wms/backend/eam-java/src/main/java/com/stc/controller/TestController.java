package com.stc.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.*;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;



/**
 * Created with IntelliJ IDEA.
 * User: Intellinum-PC11
 * Date: 16/12/18
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestController {

    public String getIdentity() throws IOException, KeyManagementException, NoSuchAlgorithmException {

        JsonObject jsonObject = new JsonObject();

        final TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };

        // Install the all-trusting trust manager
        final SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        // Create an ssl socket factory with our all-trusting manager
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        OkHttpClient client = new OkHttpClient();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);
        //builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
        /*builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        });  */
        client = builder.build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{P_USER:ahibrahim,P_PWD:scm@123}");
        Request request = new Request.Builder()
                .url("http://bss05.stc.com.sa:8020/webservices/rest/validate/validate_login/")
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("authorization", "Basic c3Rjd2Vic2VydmljZTpzdGNvcmFjbGU=")
                .build();

        Response response = client.newCall(request).execute();
        ResponseBody responseBody = response.body();
        if (body != null) {
            String result = responseBody.string();
            char firstChar = result.charAt(0);

            if (!String.valueOf(firstChar).equals("{")) {
                jsonObject.addProperty("status","error");
                jsonObject.addProperty("message", "Unauthorized Token");
            }
            else {
                JsonParser parser = new JsonParser();
                jsonObject = parser.parse(result).getAsJsonObject();

            }
        }
        return  jsonObject.toString();
    }
}
