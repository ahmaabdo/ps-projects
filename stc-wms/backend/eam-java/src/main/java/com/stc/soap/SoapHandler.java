package com.stc.soap;

import com.google.gson.JsonObject;
import com.stc.objects.SoapEnvelopeObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import oracle.net.aso.m;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 13/07/18
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
public class SoapHandler {

    private void createSoapEnvelope(SOAPMessage soapMessage, SoapEnvelopeObject seo) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "wsu";
        String myNamespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        // SOAP Header
        SOAPHeader soapHeader = envelope.getHeader();
        soapHeader.addNamespaceDeclaration("ns1", seo.getHeader());

        // SOAP Security
        SOAPElement soapSecurity = soapHeader.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
        SOAPElement userToken = soapSecurity.addChildElement("UsernameToken", "wsse");

        userToken.addChildElement("Username", "wsse").addTextNode(seo.getUsername());
        userToken.addChildElement("Password", "wsse").addTextNode(seo.getPassword());

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        soapBody.addNamespaceDeclaration("ns2", seo.getSoapurl());
        SOAPElement inputParameters = soapBody.addChildElement("inputParameters","ns2");
             
             
        for(Map.Entry m:seo.getParameter().entrySet()){
            if (m.getValue() != null &&  m.getValue() != "null") {
                System.out.println("m.getValue()= cc"+m.getValue());
                if(m.getValue() instanceof Map){
                   SOAPElement inputParametersChild= inputParameters.addChildElement((String) m.getKey(),"ns2");
                   if(m.getKey().equals("P_SERIALS")){
                    Map<String, String> child = (Map) m.getValue();
                       for (Map.Entry h : child.entrySet()) {
                           if (h.getValue() != null && h.getValue() != "null") {
                               System.out.println("m.getValue()=" + h.getValue());
                               inputParametersChild.addChildElement("P_SERIALS_ITEM", "ns2").addTextNode(h.getValue().toString());

                           }
                       }
                   
                   }else{
                   
                    Map<String, String> child = (Map)m.getValue(); 
                    for (Map.Entry h : child.entrySet()) {
                        if (h.getValue() != null && h.getValue() != "null") {
                            System.out.println("m.getValue()=" + h.getValue());
                               inputParametersChild.addChildElement(h.getKey().toString(), "ns2").addTextNode(h.getValue().toString());
                          
                        }
                    }
                   }
                  
                    // inputParameters.addChildElement(m.getKey().toString(),"ns2").addTextNode(m.getValue().toString());
                }else{
                    inputParameters.addChildElement(m.getKey().toString(),"ns2").addTextNode(m.getValue().toString());
                }
              
            }
        }

        if (seo.getSoapurl().equals("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/eam_completion/complete_work_order_generic/")) {
            SOAPElement invItemInfo = inputParameters.addChildElement("X_INVENTORY_ITEM_INFO","ns2");
            SOAPElement invItemInfoItem = invItemInfo.addChildElement("X_INVENTORY_ITEM_INFO_ITEM","ns2");
            invItemInfoItem.addChildElement("SUBINVENTORY","ns2");
            invItemInfoItem.addChildElement("LOCATOR","ns2");
            invItemInfoItem.addChildElement("LOT_NUMBER","ns2");
            invItemInfoItem.addChildElement("SERIAL_NUMBER","ns2");
            invItemInfoItem.addChildElement("QUANTITY","ns2");
        }
    }

    public String callSoapWebServiceJSON(String soapEndpointUrl, String soapAction, SoapEnvelopeObject seo, String objectName) {

        String result = "";

        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, seo), soapEndpointUrl);


            if (soapResponse != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);

                String finalString = new String(baos.toByteArray(), "UTF-8");
                JSONObject xmlJSONObj = XML.toJSONObject(finalString);

                System.out.println(xmlJSONObj);
                

//                JSONObject outputParam = xmlJSONObj.getJSONObject("env:Envelope")
//                        .getJSONObject("env:Body");
//                  System.out.println("hosni start");
//                  System.out.println(outputParam.toString());
//                  System.out.println("hosni end");
                
                JSONObject outputParam = xmlJSONObj.getJSONObject("env:Envelope")
                        .getJSONObject("env:Body").getJSONObject("OutputParameters");

                JSONArray jsonArray = new JSONArray();

                if (outputParam.toString().contains("Row")) {
                    Object rowObj = outputParam.getJSONObject("EXECUTE_QUERY").get("Row");
                    if (rowObj instanceof JSONArray) {
                        JSONArray row = outputParam.getJSONObject("EXECUTE_QUERY").getJSONArray("Row");
                        for (int i=0; i<row.length(); i++) {
                            
                            JSONObject jsonObject = new JSONObject();
                            Object colojb = row.getJSONObject(i).get("Column");
                        if (colojb instanceof JSONObject) {                           
                            JSONObject column = row.getJSONObject(i).getJSONObject("Column");
                            String name = column.getString("name");
                            String content = null;
                            if (column.has("content")) {
                                content = column.get("content").toString();
                                jsonObject.put(name, content);
                            } else {
                                jsonObject.put(name, JSONObject.NULL);
                            }

                            jsonArray.put(jsonObject);

                            JSONObject jsonResult = new JSONObject();
                            jsonResult.put(objectName, jsonArray);

                            String jsonPrettyPrintString = jsonResult.toString(4);
                            result = jsonPrettyPrintString;

                        } else {

                            JSONArray column = row.getJSONObject(i).getJSONArray("Column");
                            for (int j = 0; j < column.length(); j++) {
                                String name = column.getJSONObject(j).getString("name");
                                String content = null;
                                if (column.getJSONObject(j).has("content")) {
                                    content = column.getJSONObject(j).get("content").toString();
                                    jsonObject.put(name, content);
                                } else {
                                    jsonObject.put(name, JSONObject.NULL);
                                }
                            }
                            jsonArray.put(jsonObject);

                            JSONObject jsonResult = new JSONObject();
                            jsonResult.put(objectName, jsonArray);

                            String jsonPrettyPrintString = jsonResult.toString(4);
                            result = jsonPrettyPrintString;
                        }
                            
//                            JSONObject jsonObject = new JSONObject();
//                            JSONArray column = row.getJSONObject(i).getJSONArray("Column");
//                            for (int j=0; j<column.length(); j++) {
//                                String name = column.getJSONObject(j).getString("name");
//                                String content = null;
//                                if (column.getJSONObject(j).has("content")) {
//                                    content = column.getJSONObject(j).get("content").toString();
//                                    jsonObject.put(name, content);
//                                }
//                                else
//                                    jsonObject.put(name, JSONObject.NULL);
//                            }
//                            jsonArray.put(jsonObject);
                            
                        }
//                        JSONObject jsonObject = new JSONObject();
//                        jsonObject.put(objectName, jsonArray);
//
//                        String jsonPrettyPrintString = jsonObject.toString(4);
//                        result = jsonPrettyPrintString;
                    }
                    
                    else if (rowObj instanceof JSONObject) {
                        
                        JSONObject row = outputParam.getJSONObject("EXECUTE_QUERY").getJSONObject("Row");

                        JSONObject jsonObject = new JSONObject();

//                        
                        Object colojb = row.get("Column");
                        if (colojb instanceof JSONObject) {                           
                            JSONObject column = row.getJSONObject("Column");
                            String name = column.getString("name");
                            String content = null;
                            if (column.has("content")) {
                                content = column.get("content").toString();
                                jsonObject.put(name, content);
                            } else {
                                jsonObject.put(name, JSONObject.NULL);
                            }

                            jsonArray.put(jsonObject);

                            JSONObject jsonResult = new JSONObject();
                            jsonResult.put(objectName, jsonArray);

                            String jsonPrettyPrintString = jsonResult.toString(4);
                            result = jsonPrettyPrintString;

                        } else {

                            JSONArray column = row.getJSONArray("Column");
                            for (int j = 0; j < column.length(); j++) {
                                String name = column.getJSONObject(j).getString("name");
                                String content = null;
                                if (column.getJSONObject(j).has("content")) {
                                    content = column.getJSONObject(j).get("content").toString();
                                    jsonObject.put(name, content);
                                } else {
                                    jsonObject.put(name, JSONObject.NULL);
                                }
                            }
                            jsonArray.put(jsonObject);

                            JSONObject jsonResult = new JSONObject();
                            jsonResult.put(objectName, jsonArray);

                            String jsonPrettyPrintString = jsonResult.toString(4);
                            result = jsonPrettyPrintString;
                        }

                    }

                }
                //for remove material service
                else if (outputParam.toString().contains("P_STATUS")) {
//                    JSONObject status = outputParam.getJSONObject("P_STATUS");
                    JSONObject jsonObject = new JSONObject();
                    String content = null;

                    if (outputParam.getString("P_STATUS").isEmpty()) {
                        jsonObject.put("P_STATUS", JSONObject.NULL);
                    } else {
                        jsonObject.put("P_STATUS", outputParam.getString("P_STATUS"));
                    }

                    jsonArray.put(jsonObject);

                    JSONObject jsonResult = new JSONObject();
                    jsonResult.put(objectName, jsonArray);

                    String jsonPrettyPrintString = jsonResult.toString(4);
                    result = jsonPrettyPrintString;

                }
                else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(objectName, jsonArray);
                    result = jsonObject.toString();
                }
                baos.close();
            }
            else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "complete");
                result = jsonObject.toString();
            }

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return result;
    }
    
    
    
     public String callSoapWebServiceJSONResponsibility(String soapEndpointUrl, String soapAction, SoapEnvelopeObject seo, String objectName) {

        String result = "";

        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, seo), soapEndpointUrl);


            if (soapResponse != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);

                String finalString = new String(baos.toByteArray(), "UTF-8");
                JSONObject xmlJSONObj = XML.toJSONObject(finalString);

                System.out.println(xmlJSONObj);

                JSONObject outputParam = xmlJSONObj.getJSONObject("env:Envelope")
                        .getJSONObject("env:Body").getJSONObject("OutputParameters");

                JSONArray jsonArray = new JSONArray();

                if (outputParam.toString().contains("Row")) {
                    Object rowObj = outputParam.getJSONObject("EXECUTE_QUERY").get("Row");
                      System.out.println(rowObj);
                    if (rowObj instanceof JSONArray) {
                        JSONArray row = outputParam.getJSONObject("EXECUTE_QUERY").getJSONArray("Row");
                        for (int i=0; i<row.length(); i++) {
                            
                            
                            JSONObject jsonObject = new JSONObject();
                            String name = row.getJSONObject(i).getJSONObject("Column").getString("name").toString();
                            String content = row.getJSONObject(i).getJSONObject("Column").getString("content").toString();
                            jsonObject.put(name, content);
                            jsonArray.put(jsonObject);
                            
                            
                        }
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(objectName, jsonArray);

                        String jsonPrettyPrintString = jsonObject.toString(4);
                        result = jsonPrettyPrintString;
                    }
                }
              
                baos.close();
            }
            else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "complete");
                result = jsonObject.toString();
            }

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return result;
    }
    
     
     //  ****************************get po number *******************************//
      public String callSoapWebServiceJSONPoNumber(String soapEndpointUrl, String soapAction, SoapEnvelopeObject seo, String objectName) {

        String result = "";

        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, seo), soapEndpointUrl);


            if (soapResponse != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);

                String finalString = new String(baos.toByteArray(), "UTF-8");
                JSONObject xmlJSONObj = XML.toJSONObject(finalString);

                System.out.println(xmlJSONObj);

                JSONObject outputParam = xmlJSONObj.getJSONObject("env:Envelope")
                        .getJSONObject("env:Body").getJSONObject("OutputParameters");

                JSONArray jsonArray = new JSONArray();

                if (outputParam.toString().contains("Row")) {
                    Object rowObj = outputParam.getJSONObject("EXECUTE_QUERY").get("Row");
                      System.out.println(rowObj);
                    if (rowObj instanceof JSONArray) {
                        JSONArray row = outputParam.getJSONObject("EXECUTE_QUERY").getJSONArray("Row");
                        for (int i=0; i<row.length(); i++) {
                            
                            
                            JSONObject jsonObject = new JSONObject();
                            String name = row.getJSONObject(i).getJSONObject("Column").getString("name").toString();
                            int content =Integer.parseInt(row.getJSONObject(i).getJSONObject("Column").get("content").toString());
                            jsonObject.put(name, content);
                            jsonArray.put(jsonObject);
                            
                            
                        }
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(objectName, jsonArray);

                        String jsonPrettyPrintString = jsonObject.toString(4);
                        result = jsonPrettyPrintString;
                    }
                }
              
                baos.close();
            }
            else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "complete");
                result = jsonObject.toString();
            }

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return result;
    }
     

    public String callSoapWebService(String soapEndpointUrl, String soapAction, SoapEnvelopeObject seo) {
        String result = "";
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, seo), soapEndpointUrl);
            if (soapResponse != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);
                String finalString = new String(baos.toByteArray());

                JSONObject xmlJSONObj = XML.toJSONObject(finalString);

                String jsonPrettyPrintString = xmlJSONObj.toString(4);
                result = jsonPrettyPrintString;
                baos.close();
            }
            else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status","complete");
                result = jsonObject.toString();
            }
            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return result;
    }

    private SOAPMessage createSOAPRequest(String soapAction, SoapEnvelopeObject seo) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, seo);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }
}
