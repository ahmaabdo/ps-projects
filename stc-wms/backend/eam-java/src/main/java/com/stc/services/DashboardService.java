/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.DashboardController;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Anas Alghawi
 */
@Path("/dashboards")
public class DashboardService {

    @GET
    @Path("wrCount")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response wrCount(@QueryParam("wr_type") String wr_type,
                            @QueryParam("userID") String userID,
                            @QueryParam("respId") String respId,
                            @QueryParam("respAppId") String respAppId,
                            @QueryParam("orgId") String orgId
                            ) {

        if (wr_type == null || userID == null || respId == null || respAppId == null || orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            DashboardController dashboardController = new DashboardController();
            result = dashboardController.wrCount(wr_type,userID, respId, respAppId, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("woCount")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response woCount(@QueryParam("wo_type") String wo_type,
                            @QueryParam("userID") String userID,
                            @QueryParam("respId") String respId,
                            @QueryParam("respAppId") String respAppId,
                            @QueryParam("orgId") String orgId
                            ) {

        if (wo_type == null || userID == null || respId == null || respAppId == null || orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            DashboardController dashboardController = new DashboardController();
            result = dashboardController.woCount(wo_type,userID, respId, respAppId, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

}
