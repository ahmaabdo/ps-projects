/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import com.db.StcWmsConnection;
import com.stc.objects.TranslationBean;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author lenovo
 */
public class TranslationDAO {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;

    public ArrayList<TranslationBean> getAllTranslation() {

        ArrayList<TranslationBean> translationListArr = new ArrayList<TranslationBean>();
        try {
            connection = StcWmsConnection.getConnection();
            String query
                    = "select * from DUSSER_DB.TRANSLATION_DB WHERE  APPLICATIONNAME='STC-WMS'";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                TranslationBean translationBean = new TranslationBean();
                translationBean.setId(rs.getString("ID"));
                translationBean.setArabicValue(rs.getString("ARABICNAME"));
                translationBean.setEnglishValue(rs.getString("ENGLISHNAME"));
                translationBean.setWordName(rs.getString("WORDNAME"));
                translationListArr.add(translationBean);

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
//            closeResources(connection, ps, rs);
        }
        return translationListArr;

    }

}
