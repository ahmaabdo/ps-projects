/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import static com.stc.controller.MainController.logger;
import static com.stc.controller.MainController.pool;
import static com.stc.controller.NotificationController.logger;
import static com.stc.controller.NotificationController.pool;
import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author lenovo
 */


public class StcWmsMainController {
    
    final static Logger logger = Logger.getLogger(NotificationController.class);

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public NamedParameterStatement namedParameterStatement;
    public PreparedStatement preparedStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;
    
    
    
    //  ********************validate login *************************//
      public String validateLogin(String username,
                                String password){
        JSONObject jsonObj;
        String response="";
        String result = "";

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/validate_login/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/validate_login/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_USER_NAME",username);
            param.put("P_PASSWORD", password);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            System.out.println(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Validate Login Error",e);
        }
        return result;
    }
    //  ********************validate Verifiy *************************//
      public String validateVerifiy(String sessionId,
                                String verifiyCode){
        JSONObject jsonObj;
        String response="";
        String result = "";

        try{
             String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/verify_user/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/verify_user/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_SESSION_ID",sessionId);
            param.put("P_VERIFY_CODE", verifiyCode);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Validate Login Error",e);
        }
        return result;
    }
     
    //************************* get Login Details******************************************//
    
      public String getUserLoginDetails(String userName) {

        String result = null;

          String statement = "SELECT user_id,\n"
                  + "                         emp_name,\n"
                  + "                         phone_number,\n"
                  + "                         email_address,\n"
                  + "                         dept_name\n"
                  + "                    FROM xxwms_user_info_v\n"
                  + "                   WHERE UPPER (user_name) =upper('" + userName + "') ";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
          
                    
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "LoginDetails");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get LoginDetails Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
         
           
      
      
    
    //************************* get PoNumber******************************************//
    
      public String getPoNumber(String organizationId) {

        String result = null;

          String statement = "SELECT po_header_id, po_number\n"
                  + "  FROM xxwms_po_lov\n"
                  + " WHERE ORG_ID='" + organizationId + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "poNumber");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get poNumber Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
      public String getPoNumberExpress(String organizationId,String poID) {

        String result = null;

          String statement = "SELECT po_header_id, po_number\n"
                  + "  FROM xxwms_po_lov\n"
                  + " WHERE ORG_ID='" + organizationId + "' and PO_HEADER_ID= '"+poID+"'" ;
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "poNumber");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get poNumber Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
         
         
     // *****************************GET  responsabilityGET  responsability******************************//
         
      public String getResponsibility(int userId, String language) {

        String result = null;

          String statement = "    SELECT responsibility_id, responsibility_name\n"
                  + "     FROM xxwms_responsibility_info_v\n"
                  + "      WHERE language ='"+language+"'      \n"
                  + "      AND user_id = '"+userId+"'";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_LANGUAGE", language);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Responsibility");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Responsibility Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet, callableStatement);
        }
        return result;

    }
   
             
     // *****************************GET  Organization ******************************//
         
      public String getOrganization(String language, String responsabilityId) {

        String result = null;

        String statement = "SELECT org_id,\n"
                  + "       org_code,\n"
                  + "       org_name\n"
                  + "  FROM xxwms_org_info_v\n"
                  + "  where resp_id = '"+responsabilityId+"'";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_LANGUAGE", language);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
//            result = handler.callSoapWebServiceJSONResponsibility(soapEndpointUrl, soapAction, seo, "Responsibility");
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Organization");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Organization Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet, callableStatement);
        }
        return result;

    }
      

         
    //************************* get ORG_ID******************************************//
    
      public String getORG_ID(int po_header_id) {

        String result = null;

             String statement = "SELECT DISTINCT ORG_ID, LOCATION_CODE\n"
                  + "  FROM XXWMS_DETAILS_PO_V\n"
                  + " WHERE po_header_id ='" + po_header_id + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ORG_ID");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get ORG_ID Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }     
         
     //************************* get supplier******************************************//
  
      public String getSupplier(int po_header_id) {

        String result = null;

             String statement = "select supplier_id,supplier_name\n"
                  + "from xxwms_supplier_lov\n"
                  + "where po_header_id  ='" + po_header_id + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "supplier");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get supplier Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }        
         
       //************************* get LINE_NUM******************************************//
  
      public String getLineNumber(int po_header_id) {

        String result = null;

          String statement = " select po_line_id, po_line_number ,DESCRIPTION \n"
                  + " from xxwms_po_lines_lov\n"
                  + " where po_header_id = '" + po_header_id + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "LineNumber");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get LineNumber Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }             
      //************************* get ItemDescription******************************************//
  
      public String getItemDescription(int PO_LINE_ID) {

        String result = null;

          String statement = " select item_id, item_code, item_description\n"
                  + " from xxwms_po_item_lov\n"
                  + " where po_line_id = '" + PO_LINE_ID + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ItemDescription");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get ItemDescription Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }                 
         
   //************************* get LOCATION******************************************//
  
      public String getLocation(int PO_LINE_ID , String Lang) {

        String result = null;

          String statement = "  select location_id , location_code, location_description\n"
                  + " from xxwms_po_line_location_lov\n"
                  + "  where po_line_id='" + PO_LINE_ID + "' and LANGUAGE = '"+ Lang +"'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_LANGUAGE", Lang);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Location");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Location Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }                  
         
     //************************* get QUANTITY******************************************//
  
      public String getQuantity(int po_line_id) {

        String result = null;

          String statement = " select quantity\n"
                  + " from xxwms_po_line_qty_lov\n"
                  + " where po_line_id='" + po_line_id + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Quantity");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Quantity Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }       
       //************************* get Uom******************************************//
  
      public String getUom(int PO_LINE_ID) {

        String result = null;

            String statement = " select uom\n"
                  + " from xxwms_po_line_uom_lov\n"
                  + " where po_line_id= '" + PO_LINE_ID + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Uom");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Uom Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }       
      
      //************************* get LPN******************************************//
  
      public String getLPn(int po_line_id) {

        String result = null;

          String statement = " select lpn\n"
                  + " from xxwms_po_line_lpn_lov\n"
                  + " where po_line_id = '" + po_line_id + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
//            result = handler.callSoapWebServiceJSONResponsibility(soapEndpointUrl, soapAction, seo, "LPN");
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "LPN");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get LPN Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }     
      
   //********************************************get LPN Inspect*************//
        
      public String getLPnInspect(int org_Id) {

        String result = null;

          String statement = "SELECT PO_NUM,\n"
                  + "       PO_LINE,\n"
                  + "       LPN,\n"
                  + "       ORG_ID,\n"
                  + "       PO_HEADER_ID,\n"
                  + "       PO_LINE_ID\n"
                  + "  FROM XXWMS_LPN_LOV\n"
                  + " WHERE ORG_ID = '"+org_Id+"'";
          try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "LPNInspect");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get LPNInspect Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    } 
      //************************ Get LPn Deliver ***************************************//
    
       public String getLPnDeliver(String org_Id) {

        String result = null;

          String statement = "select lpn from XXWMS_LPN_DELIVER_LOV where org_id ='"+org_Id+"'";
          try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "LPNInspect");

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get LPNInspect Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    } 
      
      
      
      
      //************************ Get Deliver Quantity********************************************//
      
       public String DeliverQuantity(String lpn) {

        String result = null;

           String statement = "select quantity\n"
                   + "from xxwms_deliver_qty_lov\n"
                   + "where lpn ='"+lpn+"'";
          try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "LPNInspect");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get LPNInspect Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    } 
      
      
         
     //************************* get QUANTITY By Lpn******************************************//
  
      public String getQuantityByLpn(String  quantityByLpn) {

        String result = null;

          String statement = "select item_qty\n"
                  + "from xxwms_lpn_qny_lov\n"
                  + "where lpn='"+quantityByLpn+"'";
          
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "QuantityByLpn");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Quantity By Lpn Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }         
             
     //************************* get Deliver Po Number******************************************//
  
      public String getDeliverPoNumber(String  orgId) {

        String result = null;

          String statement = "SELECT po_header_id, po_num\n"
                  + "  FROM xxwms_po_deliver_lov\n"
                  + " WHERE org_id = '" + orgId + "' ";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "DeliverPoNumber");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Deliver Po Number Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }        
   
          //************************* get Return Po Number******************************************//
  
      public String getReturnPoNumber(String  orgId) {

        String result = null;

          String statement = "   SELECT po_header_id, po_num\n"
                  + "  FROM xxwms_po_return_lov\n"
                  + " WHERE org_id ='" + orgId + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ReturnPoNumber");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Return Po Number Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    } 
          //************************* Check serialized flag Po Number******************************************//
  
      public String checkSerializedFlag(String  orgId , String itemId , String po_line_Id) {

        String result = null;

          String statement = "SELECT serialized_flag\n"
                  + "FROM xxwms_item_type\n"
                  + "WHERE org_id ='" + orgId + "'\n"
                  + "      AND item_id ='" + itemId + "' and po_line_id='"+po_line_Id+"'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "checkSerializedFlag");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get check Serialized Flag Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    } 
      
    
      
      
      ///*************************** Get Sub Inventory *****************************///
      
        public String getSubInventory(String  orgId) {

        String result = null;

          String statement = "SELECT sub_inventory_name\n"
                  +"FROM xxwms_sub_inventories_lov\n"
                  +" WHERE org_id ='"+ orgId+"'";
              
                     
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
//            result = handler.callSoapWebServiceJSONResponsibility(soapEndpointUrl, soapAction, seo, "getSubInventory");
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "getSubInventory");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get sub inventory Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    } 
      ///////***************get locator *****************/////////
      
       public String getLocator(String  sub_inv_code,String orgId) {

        String result = null;

          String statement = "SELECT locator_id, locator_desc\n"
                  + "FROM xxwms_locators_lov\n"
                  + "WHERE subinventory_code ='"+sub_inv_code+"' AND org_Id='"+orgId+"'";
    
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "getLocator");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get sub inventory Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    } 
      
      
      
      
      //********* get reject Reason*********//
      
       public String getRejReason() {

        String result = null;

        String statement = "SELECT REASON_NAME, REASON_ID\n" +
                            "FROM XXWMS_REJECT_REASON_LOV";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "get reassign users");
            JSONObject jsonObject = new JSONObject(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Get users Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet, callableStatement);
        }
        return result;
    }
      
      //************* get translation *************//
      public String getTranslationLbl() {

        String result = null;

          String statement = "  SELECT ID,\n"
                  + "WORD_NAME,\n"
                  + "ENGLISH_NAME,\n"
                  + "ARABIC_NAME,\n"
                  + "APPLICATION_NAME,\n"
                  + "CREATION_DATE\n"
                  + "FROM XXWMS_APPLICATION_LABEL_TL_V ";
          try {

              String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "get reassign users");
            JSONObject jsonObject = new JSONObject(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Get users Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet, callableStatement);
        }
        return result;
    }
      
       
      //************* get LPN Generation *************//
      public String getLPNGeneration(String po_Header , String po_LineNumber ,String lang
                                ){
        
        String result = "";

        try{
             String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/xxwms_lpn_normal/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/xxwms_lpn_normal/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_PO_HEADER_ID",po_Header);
            param.put("P_PO_LINE_ID", po_LineNumber);
            param.put("P_LANG", lang);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Lpn Generation Error",e);
        }
        return result;
    }
    //---------------------------- get remaining quantity
      
    public String getQuantityRemaing(String po_Header_id, String po_line_id ,String org_Id) {

        String result = null;

        String statement = "SELECT REMAINING,\n"
                + "       PO_HEADER_ID,\n"
                + "       PO_LINE_ID,\n"
                + "       ORG_ID\n"
                + "  FROM QUANTITY_REMAINING\n"
                + " WHERE     1 = 1\n"
                + "       AND  PO_HEADER_ID = '"+po_Header_id+"'\n"
                + "       AND  PO_LINE_ID = '"+po_line_id+"'\n"
                + "       AND org_id = '"+org_Id+"'";
          
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query//";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "QuantityByLpn");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Quantity Remaining Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }         
      
      

    void closeAllConnection(Connection connection,
            PreparedStatement preparedStatement,
            ResultSet resultSet,
            CallableStatement callableStatement) {

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

    }
    
       public static void main(String[] args){
      StcWmsMainController obj=new StcWmsMainController();
        System.out.println(obj.checkSerializedFlag("94","4","699721")); 
      }     
}
