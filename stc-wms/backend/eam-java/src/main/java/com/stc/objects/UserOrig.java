package com.stc.objects;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 27/12/17
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */
public class UserOrig {
    public String userOrigSystem;
    public String userOrigSystemId;

    public UserOrig(String userOrigSystem, String userOrigSystemId) {
        this.userOrigSystem = userOrigSystem;
        this.userOrigSystemId = userOrigSystemId;
    }

    public String getUserOrigSystem() {
        return userOrigSystem;
    }

    public void setUserOrigSystem(String userOrigSystem) {
        this.userOrigSystem = userOrigSystem;
    }

    public String getUserOrigSystemId() {
        return userOrigSystemId;
    }

    public void setUserOrigSystemId(String userOrigSystemId) {
        this.userOrigSystemId = userOrigSystemId;
    }
}
