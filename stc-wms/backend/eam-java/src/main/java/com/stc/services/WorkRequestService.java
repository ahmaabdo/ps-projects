package com.stc.services;

import com.stc.controller.WorkRequestController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 02/01/18
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */

@Path("/workRequest")
public class WorkRequestService {

    @GET
    @Path("getWorkRequestDashboard")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkRequestDashboard(@QueryParam("orgId")int orgId,@QueryParam("userId")int userId) {

        if (orgId == 0 || userId==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getWorkRequestDashboard(orgId,userId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkRequest")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkRequest(@QueryParam("orgId")String orgId,
                                   @QueryParam("requestNumber")String requestNumber,
                                   @QueryParam("assetDepartment")String assetDepartment,
                                   @QueryParam("status")String status,
                                   @QueryParam("assetNumber")String assetNumber,
                                   @QueryParam("creationDateFrom")Date creationDateFrom,
                                   @QueryParam("creationDateTo")Date creationDateTo,
                                   @QueryParam("creatorName")String creatorName,
                                   @QueryParam("customStatus")String customStatus,
                                   @QueryParam("isHqc")String isHqc,
                                   @QueryParam("userID")String userID,
                                   @QueryParam("respId")String respId,
                                   @QueryParam("respAppId")String respAppId,
                                   @QueryParam("dasboardSearch")String dasboardSearch) {

        if (orgId == null || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getWorkRequest(orgId,
                    requestNumber,
                    assetDepartment,
                    status,
                    assetNumber,
                    creationDateFrom,
                    creationDateTo,
                    creatorName,
                    customStatus,
                    isHqc,
                    userID,
                    respId,
                    respAppId,
                    dasboardSearch
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkRequestDetails")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkRequestDetails(@QueryParam("workRequestId")String workRequestNumber) {

        if (workRequestNumber == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getWorkRequestDetails(workRequestNumber);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

/*    @GET
    @Path("getWorkRequestV1")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkRequestV1(@QueryParam("orgId")String orgId,
                                   @QueryParam("requestNumber")String requestNumber,
                                   @QueryParam("assetDepartment")String assetDepartment,
                                   @QueryParam("status")String status,
                                   @QueryParam("assetNumber")String assetNumber,
                                   @QueryParam("creationDateFrom")Date creationDateFrom,
                                   @QueryParam("creationDateTo")Date creationDateTo,
                                   @QueryParam("creatorName")String creatorName) {

        if (orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getWorkRequestV1(orgId,
                    requestNumber,
                    assetDepartment,
                    status,
                    assetNumber,
                    creationDateFrom,
                    creationDateTo,
                    creatorName);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }*/

    @GET
    @Path("getAssetNumber")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAssetNumber(@QueryParam("orgId")String orgId,
                                   @QueryParam("assetNumber")String assetNumber,
                                   @QueryParam("assetGroup")String assetGroup,
                                   @QueryParam("department")String department) {

        if (orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getAssetNumber(orgId, assetNumber, assetGroup, department);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getDepartment")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getDepartment(@QueryParam("orgId")String orgId) {

        if (orgId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getDepartment(orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    @GET
    @Path("getBuildingType")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getBuildingType() {

       
        
            String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getBuildingType();

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        
    }

    @GET
    @Path("getPriority")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getPriority() {

        String result;
        WorkRequestController workRequestController = new WorkRequestController();
        result = workRequestController.getPriority();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getRequestType")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getRequestType() {

        String result;
        WorkRequestController workRequestController = new WorkRequestController();
        result = workRequestController.getRequestType();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getRequestedFor")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getRequestedFor(@QueryParam("username")String username,@QueryParam("fullname")String fullname) {

        String result;
        WorkRequestController workRequestController = new WorkRequestController();
        result = workRequestController.getRequestFor(username,fullname);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getWorkRequestStatus")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkRequestStatus() {

        String result;
        WorkRequestController workRequestController = new WorkRequestController();
        result = workRequestController.getWorkRequestStatus();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getCreatorName")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getCreatorName(@QueryParam("orgId")int orgId) {

        String result;
        if (orgId<=0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
        WorkRequestController workRequestController = new WorkRequestController();
        result = workRequestController.getCreatorName(orgId);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
        }

    }

    @GET
    @Path("getWfStatus")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWfStatus(@QueryParam("language")String language) {

        String result;
        WorkRequestController workRequestController = new WorkRequestController();
        result = workRequestController.getWfStatus(language);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getWrSources")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWrSources() {

        String result;
        WorkRequestController workRequestController = new WorkRequestController();
        result = workRequestController.getWrSources();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @POST
    @Path("createWorkRequest")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response createWorkRequest(@FormParam("orgId") int orgId,
                                      @FormParam("assetGroupId") int assetGroupId,
                                      @FormParam("assetNumber") String assetNumber,
                                      @FormParam("priorityId") int priorityId,
                                      @FormParam("requestByDate") String requestByDate,  //need to remove     make by default current date
                                      @FormParam("requestLog") String requestLog,
                                      @FormParam("owningDeptId") int owningDeptId,
                                      @FormParam("userId") int userId,
                                      @FormParam("workRequestTypeId") int workRequestTypeId,      //  need to remove  //make by default null
                                      @FormParam("maintenanceObjectId") int maintenanceObjectId,    //need to remove  //make by default null
                                      @FormParam("createdBy") int createdBy,
                                      @FormParam("createdFor") int createdFor,
                                      @FormParam("phoneNumber") String phoneNumber,
                                      @FormParam("email") String email,
                                      @FormParam("contactPreference") int contactPreference,
                                      @FormParam("wfStatus") String wfStatus,
                                      @FormParam("wrSource") String wrSource,
                                      @FormParam("notifyOriginator") int notifyOriginator) {

        String result;
        if (orgId == 0 || assetGroupId == 0 || assetNumber == null || priorityId == 0
                || requestByDate == null||  owningDeptId == 0 || userId == 0 ||  createdBy == 0 || createdFor == 0 || maintenanceObjectId ==0 || workRequestTypeId ==0 ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.createWorkRequest(orgId,assetGroupId,assetNumber,priorityId,
                    requestByDate,requestLog,owningDeptId,userId,workRequestTypeId,maintenanceObjectId,createdBy,createdFor,
                    phoneNumber,email,contactPreference,wfStatus,wrSource,notifyOriginator);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }


    @POST
    @Path("updateWorkRequest")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response updateWorkRequest(@FormParam("P_WORK_REQUEST_ID") String P_WORK_REQUEST_ID,
                                      @FormParam("P_LAST_UPDATE_DATE") String P_LAST_UPDATE_DATE,
                                      @FormParam("P_EXPECTED_RESOLUTION_DATE") String P_EXPECTED_RESOLUTION_DATE,
                                      @FormParam("P_LAST_UPDATED_BY") String P_LAST_UPDATED_BY,
                                      @FormParam("P_ASSET_NUMBER") String P_ASSET_NUMBER,
                                      @FormParam("P_ASSET_GROUP") String P_ASSET_GROUP,
                                      @FormParam("P_WORK_REQUEST_STATUS_ID") String P_WORK_REQUEST_STATUS_ID,
                                      @FormParam("P_WORK_REQUEST_PRIORITY_ID") String P_WORK_REQUEST_PRIORITY_ID,
                                      @FormParam("P_WORK_REQUEST_OWNING_DEPT") String P_WORK_REQUEST_OWNING_DEPT,
                                      @FormParam("P_ATTRIBUTE1") String P_ATTRIBUTE1,
                                      @FormParam("P_ATTRIBUTE2") String P_ATTRIBUTE2, // WF Status
                                      @FormParam("P_ATTRIBUTE3") String P_ATTRIBUTE3,
                                      @FormParam("P_ATTRIBUTE4") String P_ATTRIBUTE4, // WR Status
                                      @FormParam("P_ATTRIBUTE5") String P_ATTRIBUTE5,
                                      @FormParam("P_ATTRIBUTE6") String P_ATTRIBUTE6,
                                      @FormParam("P_ATTRIBUTE7") String P_ATTRIBUTE7,
                                      @FormParam("P_ATTRIBUTE8") String P_ATTRIBUTE8,
                                      @FormParam("P_ATTRIBUTE9") String P_ATTRIBUTE9,
                                      @FormParam("P_ATTRIBUTE10") String P_ATTRIBUTE10,
                                      @FormParam("P_ATTRIBUTE11") String P_ATTRIBUTE11,
                                      @FormParam("P_ATTRIBUTE12") String P_ATTRIBUTE12,
                                      @FormParam("P_ATTRIBUTE13") String P_ATTRIBUTE13,
                                      @FormParam("P_ATTRIBUTE14") String P_ATTRIBUTE14,
                                      @FormParam("P_ATTRIBUTE15") String P_ATTRIBUTE15,
                                      @FormParam("P_DESCRIPTION") String P_DESCRIPTION,
                                      @FormParam("P_WORK_REQUEST_TYPE_ID") String P_WORK_REQUEST_TYPE_ID,
                                      @FormParam("P_WORK_REQUEST_CREATED_BY") String P_WORK_REQUEST_CREATED_BY,
                                      @FormParam("P_CREATED_FOR") String P_CREATED_FOR,
                                      @FormParam("P_PHONE_NUMBER") String P_PHONE_NUMBER,
                                      @FormParam("P_E_MAIL") String P_E_MAIL,
                                      @FormParam("P_CONTACT_PREFERENCE") String P_CONTACT_PREFERENCE,
                                      @FormParam("P_MAINTENANCE_OBJECT_ID") String P_MAINTENANCE_OBJECT_ID) {

        String result="";
        if (P_WORK_REQUEST_ID.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.updateWorkRequest(P_WORK_REQUEST_ID,
                    P_LAST_UPDATE_DATE,
                    P_EXPECTED_RESOLUTION_DATE,
                    P_LAST_UPDATED_BY,
                    P_ASSET_NUMBER,
                    P_ASSET_GROUP,
                    P_WORK_REQUEST_STATUS_ID,
                    P_WORK_REQUEST_PRIORITY_ID,
                    P_WORK_REQUEST_OWNING_DEPT,
                    P_ATTRIBUTE1,
                    P_ATTRIBUTE2,
                    P_ATTRIBUTE3,
                    P_ATTRIBUTE4,
                    P_ATTRIBUTE5,
                    P_ATTRIBUTE6,
                    P_ATTRIBUTE7,
                    P_ATTRIBUTE8,
                    P_ATTRIBUTE9,
                    P_ATTRIBUTE10,
                    P_ATTRIBUTE11,
                    P_ATTRIBUTE12,
                    P_ATTRIBUTE13,
                    P_ATTRIBUTE14,
                    P_ATTRIBUTE15,
                    P_DESCRIPTION,
                    P_WORK_REQUEST_TYPE_ID,
                    P_WORK_REQUEST_CREATED_BY,
                    P_CREATED_FOR,
                    P_PHONE_NUMBER,
                    P_E_MAIL,
                    P_CONTACT_PREFERENCE,
                    P_MAINTENANCE_OBJECT_ID);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("sendWorkRequestForApproval")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response sendWorkRequestForApproval(@FormParam("workRequestId") int workRequestId,
                                               @FormParam("userId") int userId,
                                               @FormParam("respId") int respId,
                                               @FormParam("respAppId") int respAppId) {

        String result;
        if (workRequestId == 0 || userId == 0 || respId == 0 || respAppId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.sendWorkRequestForApproval(workRequestId,userId,respId,respAppId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkFlowStatus")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkFlowStatus(@QueryParam("workRequestId") int workRequestId) {

        String result;
        if (workRequestId == 0 ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getWorkFlowStatus(workRequestId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getAllWorkFlowStatus")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAllWorkFlowStatus() {

        String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.getAllWorkFlowStatus();

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
    }

    @GET
    @Path("isHqcOrgWorkRequest")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response isHqcOrgWorkRequest(@QueryParam("organizationID")String organizationID) {

        if (organizationID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            WorkRequestController workRequestController = new WorkRequestController();
            result = workRequestController.isHqcOrgWorkRequest(organizationID);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

}
