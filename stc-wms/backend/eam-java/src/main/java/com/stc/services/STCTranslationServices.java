/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stc.controller.PurchasingController;
import com.stc.controller.STCTranslationDAO;
import com.stc.objects.PurchasingBean;
import com.stc.objects.TranslationBean;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author lenovo
 */
@Path("Translation")
public class STCTranslationServices {
    @POST
    @Path("AddTranslation")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})

    public Response AddTranslation(String translationBean) {
        String result = null;
        STCTranslationDAO translationController = new STCTranslationDAO();
        try {
            ObjectMapper mapper = new ObjectMapper();

            TranslationBean bean
                    = mapper.readValue(translationBean, TranslationBean.class);

            result = translationController.AddTranslation(bean);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
   
}
