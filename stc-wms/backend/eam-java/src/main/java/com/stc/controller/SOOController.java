/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.stc.db.ConnectionPool;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.apache.log4j.Logger;
import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Anas Alghawi
 */
public class SOOController {

    final static Logger logger = Logger.getLogger(SOOController.class);

    public ResultSet resultSet;

    public String getIdentity(String token) throws IOException, KeyManagementException, NoSuchAlgorithmException {

        JsonObject jsonObject = new JsonObject();

        final TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }
            }
        };

        // Install the all-trusting trust manager
        final SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        // Create an ssl socket factory with our all-trusting manager
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        OkHttpClient client = new OkHttpClient();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);
        builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
        builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        });
        client = builder.build();

        Properties properties = getProperties();

        Request request = new Request.Builder()
                .url(properties.getProperty("sso_url"))
                .header("Authorization", "Bearer " + token)
                .build();

        Response response = client.newCall(request).execute();
        ResponseBody body = response.body();
        if (body != null) {
            String result = body.string();
            char firstChar = result.charAt(0);
            if (!String.valueOf(firstChar).equals("{")) {
                jsonObject.addProperty("status", "error");
                jsonObject.addProperty("message", "Unauthorized Token");
            } else {
                JsonParser parser = new JsonParser();
                jsonObject = parser.parse(result).getAsJsonObject();
            }
        }
        return jsonObject.toString();
    }

    public Properties getProperties() {
        Properties properties = new Properties();

        InputStream in = getClass().getResourceAsStream("/config.properties");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        try {
            properties.load(reader);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

   

}
