package com.stc.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import static com.stc.controller.MainController.logger;
import static com.stc.controller.MainController.pool;
import static com.stc.controller.PublicController.pool;
import static com.stc.controller.WorkRequestController.logger;
import com.stc.db.ConnectionPool;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import java.io.UnsupportedEncodingException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA. User: Ardi sugiarto Date: 01/08/18 Time: 18:02 To
 * change this template use File | Settings | File Templates.
 */
public class StatisticReportController {

    final static Logger logger = Logger.getLogger(StatisticReportController.class);

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public PreparedStatement preparedStatement;
    //public NamedParameterStatement namedParameterStatement;
    //public CallableStatement callableStatement;
    public ResultSet resultSet;
    public ResultSetMetaData resultSetMetaData;
    static List<String> monthInString = new ArrayList<>(Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"));

    public static boolean isLeapYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
    }

    /*
    * Report CM_EM using query provided by Maher
    * */
    public String CM_EM_ccpReport(String dateFrom, String dateTo, int orgId) {
        String result = null;
        ResultSet resultSet1;
        String statement = "SELECT  COUNT (*) cm_em_created,TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')   cm_em_Period,   \n"
                + " TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') ORDR\n"
                + "    FROM wip.wip_discrete_jobs wdj,\n"
                + "         apps.eam_work_order_details_v wod,\n"
                + "         org_organization_definitions org,\n"
                + "         bom_departments bd\n"
                + "   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "         AND wod.user_defined_status_id <> 7                --NOT IN (7, 4, 5)\n"
                + "         AND TRUNC (wdj.scheduled_start_date) BETWEEN TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                                                  AND TO_DATE(?, 'YYYY/MM/DD')\n"
                + "         AND wdj.plan_maintenance <> 'Y'\n"
                + "         AND org.organization_id = wdj.organization_id\n"
                + "         AND wdj.owning_department = bd.department_id\n"
                + "        AND  wdj.organization_id=?\n"
                + "         AND bd.attribute1 = 1\n"
                + "GROUP BY TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english') ,   "
                + "TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') \n"
                + "ORDER BY    TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY')";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);
            System.out.println(statement);

            while (resultSet1.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("MONTH", resultSet1.getString("cm_em_Period"));
                int created = resultSet1.getInt("cm_em_created");
                jsonObject.addProperty("CREATED", created);
                int completed = CM_EM_Completed(dateFrom, dateTo, orgId, resultSet1.getString("cm_em_Period"));
                jsonObject.addProperty("CLOSED_WO", completed);
                int pending = 0;
                float completionPercentage = 0;
                if (created > 0) {
                    pending = created - completed;
                    completionPercentage = (100 * completed / created);
                }
                jsonObject.addProperty("PENDING_WO", pending);
                jsonObject.addProperty("CompletionPercent", completionPercentage);
                jsonArray.add(jsonObject);
            }
            jsonResult.add("WO_STATUS_REPORT_CM_EM", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("CM EM Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    // Get CM_EM Completed
    public int CM_EM_Completed(String dateFrom, String dateTo, int orgId, String period) {
        int result = 0;
        String statement = "SELECT COUNT (*) as completed\n"
                + "  FROM wip.wip_discrete_jobs wdj,\n"
                + "       apps.eam_work_order_details_v wod,\n"
                + "       org_organization_definitions org,\n"
                + "       bom_departments bd\n"
                + " WHERE     wod.wip_entity_id = wdj.wip_entity_id\n"
                + "       AND wod.user_defined_status_id <> 7                  --NOT IN (7, 4, 5)\n"
                + "       AND STATUS_TYPE = 4                                        -- completed\n"
                + "       AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                                                  AND TO_DATE(?, 'YYYY/MM/DD')\n"
                + "       AND wdj.plan_maintenance <> 'Y'\n"
                + "       AND org.organization_id = wdj.organization_id\n"
                + "       AND  wdj.organization_id=?\n"
                + "       AND wdj.owning_department = bd.department_id\n"
                + "       AND bd.attribute1 = '1'\n"
                + "       and TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')=\n"
                + "       ?";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            preparedStatement.setString(4, period);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            if (resultSet.next()) {
                result = resultSet.getInt("completed");
            }
            preparedStatement.close();
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("CM EM Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }


    /*
    * Report CM_Civil using query provided by Maher
    * */
    public String CM_Civil_ccpReport(String dateFrom, String dateTo, int orgId) {
        String result = null;
        ResultSet resultSet1;
        String statement = "SELECT COUNT (*) cm_civil_created,TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')  cm_civil_Period ,      \n"
                + "TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') ORDR\n"
                + "    FROM wip.wip_discrete_jobs wdj,\n"
                + "         apps.eam_work_order_details_v wod,\n"
                + "         org_organization_definitions org,\n"
                + "         bom_departments bd\n"
                + "   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "         AND wod.user_defined_status_id <> 7                --NOT IN (7, 4, 5)\n"
                + "         AND TRUNC (wdj.scheduled_start_date) BETWEEN TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                                         AND TO_DATE(?, 'YYYY/MM/DD') \n"
                + "         AND wdj.plan_maintenance <> 'Y'\n"
                + "         AND org.organization_id = wdj.organization_id\n"
                + "         AND wdj.owning_department = bd.department_id\n"
                + "         AND bd.attribute1 = 2\n"
                + "         AND  wdj.organization_id=? \n"
                + "GROUP BY TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english') ,   "
                + "TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') \n"
                + "ORDER BY    TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY')";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);
            System.out.println(statement);

            while (resultSet1.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("MONTH", resultSet1.getString("cm_civil_Period"));
                int created = resultSet1.getInt("cm_civil_created");
                jsonObject.addProperty("CREATED", created);
                int completed = CM_Civil_Completed(dateFrom, dateTo, orgId, resultSet1.getString("cm_civil_Period"));
                jsonObject.addProperty("CLOSED_WO", completed);
                int pending = 0;
                float completionPercentage = 0;
                if (created > 0) {
                    pending = created - completed;
                    completionPercentage = (100 * completed / created);
                }
                jsonObject.addProperty("PENDING_WO", pending);
                jsonObject.addProperty("CompletionPercent", completionPercentage);
                jsonArray.add(jsonObject);
            }
            jsonResult.add("WO_STATUS_REPORT_CM_Civil", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("WO_STATUS_REPORT_CM_Civil", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    // Get CM_Civil_Completed
    public int CM_Civil_Completed(String dateFrom, String dateTo, int orgId, String period) {
        int result = 0;
        String statement = "SELECT COUNT (*)\n"
                + "  as completed\n"
                + "  FROM wip.wip_discrete_jobs wdj,\n"
                + "       apps.eam_work_order_details_v wod,\n"
                + "       org_organization_definitions org,\n"
                + "       bom_departments bd\n"
                + " WHERE     wod.wip_entity_id = wdj.wip_entity_id\n"
                + "       AND wod.user_defined_status_id <> 7                  --NOT IN (7, 4, 5)\n"
                + "       AND STATUS_TYPE = 4                                        -- completed\n"
                + "       AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                                                AND TO_DATE(?, 'YYYY/MM/DD') \n"
                + "       AND wdj.plan_maintenance <> 'Y'\n"
                + "       AND org.organization_id = wdj.organization_id\n"
                + "       AND wdj.owning_department = bd.department_id\n"
                + "       AND  wdj.organization_id=?\n"
                + "       AND bd.attribute1 = '2'\n"
                + "       and TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')=?";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            preparedStatement.setString(4, period);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            if (resultSet.next()) {
                result = resultSet.getInt("completed");
            }
            preparedStatement.close();
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("CM_Civil_Completed", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /*
   * Report Igate_ccpReport using query provided by Maher
   * */
    public String Igate_ccpReport(String dateFrom, String dateTo, int orgId) {
        String result = null;
        ResultSet resultSet1;
        String statement = "SELECT  COUNT (*) igate_created,TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')  igate_Period,\n"
                + " TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') ORDR\n"
                + "    FROM wip.wip_discrete_jobs wdj,\n"
                + "         wip_eam_work_requests wr,\n"
                + "         apps.eam_work_order_details_v wod,\n"
                + "         org_organization_definitions org\n"
                + "   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "         AND wod.user_defined_status_id <> 7                --NOT IN (7, 4, 5)\n"
                + "         AND TRUNC (wdj.scheduled_start_date) BETWEEN TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                                                  AND TO_DATE(?, 'YYYY/MM/DD')\n"
                + "         --  AND wdj.plan_maintenance <> 'Y'\n"
                + "         AND org.organization_id = wdj.organization_id\n"
                + "         AND wdj.wip_entity_id = wr.wip_entity_id\n"
                + "         AND wr.attribute15 IS NOT NULL\n"
                + "         AND  wdj.organization_id=?\n"
                + "GROUP BY TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english') ,   "
                + "TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY')\n"
                + "ORDER BY    TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY')";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);
            System.out.println(statement);

            while (resultSet1.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("MONTH", resultSet1.getString("igate_Period"));
                int created = resultSet1.getInt("igate_created");
                jsonObject.addProperty("CREATED", created);
                int completed = Igate_Completed(dateFrom, dateTo, orgId, resultSet1.getString("igate_Period"));
                jsonObject.addProperty("CLOSED_WO", completed);
                int pending = 0;
                float completionPercentage = 0;
                if (created > 0) {
                    pending = created - completed;
                    completionPercentage = (100 * completed / created);
                }
                jsonObject.addProperty("PENDING_WO", pending);
                jsonObject.addProperty("CompletionPercent", completionPercentage);
                jsonArray.add(jsonObject);
            }
            jsonResult.add("Igate_ccpReport", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Igate_ccpReport", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    // Get Igate_Completed
    public int Igate_Completed(String dateFrom, String dateTo, int orgId, String period) {
        int result = 0;
        String statement = "SELECT COUNT (*)\n"
                + "  as completed\n"
                + "  FROM wip.wip_discrete_jobs wdj,\n"
                + "         wip_eam_work_requests wr,\n"
                + "         apps.eam_work_order_details_v wod,\n"
                + "         org_organization_definitions org\n"
                + " WHERE     wod.wip_entity_id = wdj.wip_entity_id\n"
                + "       AND wod.user_defined_status_id <> 7                  --NOT IN (7, 4, 5)\n"
                + "       AND STATUS_TYPE = 4                                        -- completed\n"
                + "       AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                                                  AND TO_DATE(?, 'YYYY/MM/DD')\n"
                + "     --  AND wdj.plan_maintenance = 'Y'\n"
                + "       AND org.organization_id = wdj.organization_id\n"
                + "       AND  wdj.organization_id=?\n"
                + "       AND wdj.wip_entity_id = wr.wip_entity_id\n"
                + "       AND wr.attribute15 IS NOT NULL\n"
                + "      and TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')=?";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            preparedStatement.setString(4, period);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            if (resultSet.next()) {
                result = resultSet.getInt("completed");
            }
            preparedStatement.close();
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Igate_Completed", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }


    /*
    * Report PMR_EM using query provided by Maher
    * */
    public String PMR_EM_ccpReport(String dateFrom, String dateTo, int orgId) {
        String result = null;
        ResultSet resultSet1;
        String statement = "select  count (*) pmr_em_created ,TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')   pmr_em_Period  , \n"
                + "  TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') ORDR\n"
                + "    from wip.wip_discrete_jobs wdj,\n"
                + "         apps.eam_work_order_details_v wod,\n"
                + "         org_organization_definitions org,bom_departments bd \n"
                + "   where     wod.wip_entity_id = wdj.wip_entity_id \n"
                + "         and wod.user_defined_status_id <> 7                --NOT IN (7, 4, 5)\n"
                + "         and trunc(wdj.scheduled_start_date ) BETWEEN TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                                                  AND TO_DATE(?, 'YYYY/MM/DD') \n"
                + "         and wdj.plan_maintenance = 'Y' \n"
                + "         and org.organization_id = wdj.organization_id \n"
                + "         and wdj.owning_department = bd.department_id \n"
                + "         and bd.attribute1  =1 -- PMR  EM \n"
                + "       AND  wdj.organization_id=? \n"
                + " group by TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english') ,"
                + " TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') \n"
                + " ORDER BY TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY')";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);
            System.out.println(statement);

            while (resultSet1.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("MONTH", resultSet1.getString("pmr_em_Period"));
                int created = resultSet1.getInt("pmr_em_created");
                jsonObject.addProperty("CREATED", created);
                int completed = PMR_EM_COMPLETED(dateFrom, dateTo, orgId, resultSet1.getString("pmr_em_Period"));
                jsonObject.addProperty("CLOSED_WO", completed);
                int pending = 0;
                float completionPercentage = 0;
                if (created > 0) {
                    pending = created - completed;
                    completionPercentage = (100 * completed / created);
                }
                jsonObject.addProperty("PENDING_WO", pending);
                jsonObject.addProperty("CompletionPercent", completionPercentage);
                jsonArray.add(jsonObject);
            }
            jsonResult.add("PMR_EM_ccpReport", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("PMR_EM_ccpReport", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    // Get PMR_EM_COMPLETED
    public int PMR_EM_COMPLETED(String dateFrom, String dateTo, int orgId, String period) {
        int result = 0;
        String statement = "SELECT COUNT (*)\n"
                + "  as completed\n"
                + "  FROM wip.wip_discrete_jobs wdj,\n"
                + "       apps.eam_work_order_details_v wod,\n"
                + "       org_organization_definitions org,\n"
                + "       bom_departments bd\n"
                + " WHERE     wod.wip_entity_id = wdj.wip_entity_id \n"
                + "       AND wod.user_defined_status_id <> 7                  --NOT IN (7, 4, 5)\n"
                + "       AND STATUS_TYPE = 4                                        -- completed\n"
                + "       AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                                                  AND TO_DATE(?, 'YYYY/MM/DD') \n"
                + "       AND wdj.plan_maintenance = 'Y'\n"
                + "       AND org.organization_id = wdj.organization_id \n"
                + "       AND  wdj.organization_id=? \n"
                + "       AND wdj.owning_department = bd.department_id \n"
                + "       AND bd.attribute1 = '1'                                        \n"
                + "        -- and to_char( wdj.SCHEDULED_START_DATE,'MON-RR')=:PMR_EM_PERIOD \n"
                + "        and TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english') =?";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            preparedStatement.setString(4, period);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            if (resultSet.next()) {
                result = resultSet.getInt("completed");
            }
            preparedStatement.close();
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("PMR_EM_COMPLETED", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /*
    * Report PMR_CIVIL_ccpReport using query provided by Maher
    * */
    public String PMR_CIVIL_ccpReport(String dateFrom, String dateTo, int orgId) {
        String result = null;
        ResultSet resultSet1;
        String statement = "select count (*) pmr_civil_created,TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')  pmr_civil_Period ,   TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') ORDR\n"
                + "    from wip.wip_discrete_jobs wdj,\n"
                + "         apps.eam_work_order_details_v wod,\n"
                + "         org_organization_definitions org,bom_departments bd\n"
                + "   where     wod.wip_entity_id = wdj.wip_entity_id\n"
                + "         and wod.user_defined_status_id <> 7                --NOT IN (7, 4, 5)\n"
                + "         and trunc(wdj.scheduled_start_date ) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD') \n"
                + "         and wdj.plan_maintenance = 'Y'\n"
                + "         and org.organization_id = wdj.organization_id\n"
                + "         and wdj.owning_department = bd.department_id\n"
                + "         and bd.attribute1  =2 -- PMR  CIVIL\n"
                + "          AND  wdj.organization_id=?\n"
                + " group by TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english') ,  TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY') \n"
                + " ORDER BY   TO_DATE(TO_CHAR(wdj.scheduled_start_date, 'Mon-RR'),'MON-YY')";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);
            System.out.println(statement);

            while (resultSet1.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("MONTH", resultSet1.getString("pmr_civil_Period"));
                int created = resultSet1.getInt("pmr_civil_created");
                jsonObject.addProperty("CREATED", created);
                int completed = PMR_CIVIL_COMPLETED(dateFrom, dateTo, orgId, resultSet1.getString("pmr_civil_Period"));
                jsonObject.addProperty("CLOSED_WO", completed);
                int pending = 0;
                float completionPercentage = 0;
                if (created > 0) {
                    pending = created - completed;
                    completionPercentage = (100 * completed / created);
                }
                jsonObject.addProperty("PENDING_WO", pending);
                jsonObject.addProperty("CompletionPercent", completionPercentage);
                jsonArray.add(jsonObject);
            }
            jsonResult.add("PMR_CIVIL_ccpReport", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Igate_ccpReport", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    // Get PMR_CIVIL_COMPLETED
    public int PMR_CIVIL_COMPLETED(String dateFrom, String dateTo, int orgId, String period) {
        int result = 0;
        String statement = "SELECT COUNT (*)\n"
                + "  as completed\n"
                + "  FROM wip.wip_discrete_jobs wdj,\n"
                + "       apps.eam_work_order_details_v wod,\n"
                + "       org_organization_definitions org,\n"
                + "       bom_departments bd\n"
                + " WHERE     wod.wip_entity_id = wdj.wip_entity_id\n"
                + "       AND wod.user_defined_status_id <> 7                  --NOT IN (7, 4, 5)\n"
                + "       AND STATUS_TYPE = 4                                        -- completed\n"
                + "       AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                                                  AND TO_DATE(?, 'YYYY/MM/DD')\n"
                + "       AND wdj.plan_maintenance = 'Y'\n"
                + "       AND org.organization_id = wdj.organization_id\n"
                + "       AND wdj.owning_department = bd.department_id\n"
                + "       AND  wdj.organization_id=?\n"
                + "       AND bd.attribute1 = '2'                                        -- PMR  EM\n"
                + "       and TO_CHAR (wdj.scheduled_start_date, 'Mon-RR' ,'NLS_DATE_LANGUAGE = english')=?";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);
            preparedStatement.setString(4, period);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            if (resultSet.next()) {
                result = resultSet.getInt("completed");
            }
            preparedStatement.close();
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("PMR_CIVIL_COMPLETED", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }


    /*
    * Report CMWorkOrderSumm using query provided by Maher
    * */
    public String CMWorkOrderSumm(String dateFrom, String dateTo) {
        String result = null;
        ResultSet resultSet1;
        String statement = "SELECT organization_id,\n"
                + "       organization_name,\n"
                + "       orders ,\n"
                + "       Status,\n"
                + "       VALUE\n"
                + "  FROM (SELECT organization_id,\n"
                + "               organization_name,\n"
                + "               '1' orders,\n"
                + "               'Created' Status,\n"
                + "               total_created VALUE\n"
                + "          FROM (  SELECT org.organization_id,\n"
                + "                         vl.DESCRIPTION organization_name ,\n"
                + "                         COUNT (*) total_created\n"
                + "                    FROM wip.wip_discrete_jobs wdj,\n"
                + "                         apps.eam_work_order_details_v wod,\n"
                + "                         org_organization_definitions org,\n"
                + "                         FND_LOOKUP_VALUES_VL vl\n"
                + "                   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "                         AND wod.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                         AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                         AND wdj.plan_maintenance <> 'Y'\n"
                + "                         AND org.organization_id = wdj.organization_id\n"
                + "                         and org.organization_id=to_number(vl.meaning)\n"
                + "                         and lookup_type = 'STC_EAM_ORG_NAME'\n"
                + "                GROUP BY org.organization_id, vl.DESCRIPTION )\n"
                + "        UNION ALL\n"
                + "        SELECT organization_id,\n"
                + "               organization_name,\n"
                + "               '2' orders,\n"
                + "               'Closed' Status,\n"
                + "               Closed VALUE\n"
                + "          FROM (  SELECT org.organization_id,\n"
                + "                         vl.DESCRIPTION organization_name,\n"
                + "                         COUNT (*) total_created,\n"
                + "                         nvl ((  SELECT COUNT (*) Closed\n"
                + "                              FROM wip.wip_discrete_jobs wdj1,\n"
                + "                                   apps.eam_work_order_details_v wod1,\n"
                + "                                   org_organization_definitions org1\n"
                + "                             WHERE     wod1.wip_entity_id = wdj1.wip_entity_id\n"
                + "                                   AND wod1.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                                   AND STATUS_TYPE = 4            -- completed\n"
                + "                                   AND TRUNC (wdj1.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                                   AND wdj1.plan_maintenance <>'Y'\n"
                + "                                   AND org1.organization_id =\n"
                + "                                          wdj1.organization_id\n"
                + "                                   AND org1.organization_id = org.organization_id\n"
                + "                          GROUP BY org.organization_id, organization_name),0)\n"
                + "                            Closed\n"
                + "                    FROM wip.wip_discrete_jobs wdj,\n"
                + "                         apps.eam_work_order_details_v wod,\n"
                + "                         org_organization_definitions org,\n"
                + "                         FND_LOOKUP_VALUES_VL vl\n"
                + "                   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "                         AND wod.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                         AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                         AND wdj.plan_maintenance <> 'Y'\n"
                + "                          and org.organization_id=to_number(vl.meaning)\n"
                + "                         and lookup_type = 'STC_EAM_ORG_NAME'\n"
                + "                         AND org.organization_id = wdj.organization_id\n"
                + "                GROUP BY org.organization_id, vl.DESCRIPTION )\n"
                + "        UNION ALL\n"
                + "        SELECT organization_id,\n"
                + "                   organization_name ,\n"
                + "               '3' orders,\n"
                + "               'Pending' Status,\n"
                + "               (total_created - Closed) VALUE\n"
                + "          FROM (  SELECT org.organization_id,\n"
                + "                         vl.DESCRIPTION organization_name ,\n"
                + "                         COUNT (*) total_created,\n"
                + "                         nvl ((  SELECT COUNT (*) Closed\n"
                + "                              FROM wip.wip_discrete_jobs wdj1,\n"
                + "                                   apps.eam_work_order_details_v wod1,\n"
                + "                                   org_organization_definitions org1\n"
                + "                             WHERE     wod1.wip_entity_id = wdj1.wip_entity_id\n"
                + "                                   AND wod1.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                                   AND STATUS_TYPE = 4            -- completed\n"
                + "                                   AND TRUNC (wdj1.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                                   AND wdj1.plan_maintenance <> 'Y'\n"
                + "                                   AND org1.organization_id =\n"
                + "                                          wdj1.organization_id\n"
                + "                                   AND org1.organization_id = org.organization_id\n"
                + "                          GROUP BY org.organization_id, organization_name),0)\n"
                + "                            Closed\n"
                + "                    FROM wip.wip_discrete_jobs wdj,\n"
                + "                         apps.eam_work_order_details_v wod,\n"
                + "                         org_organization_definitions org,\n"
                + "                         FND_LOOKUP_VALUES_VL vl\n"
                + "                   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "                         AND wod.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                         AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD') \n"
                + "                         AND wdj.plan_maintenance <> 'Y'\n"
                + "                         AND org.organization_id = wdj.organization_id\n"
                + "                          and org.organization_id=to_number(vl.meaning)\n"
                + "                         and lookup_type = 'STC_EAM_ORG_NAME'\n"
                + "                GROUP BY org.organization_id, vl.DESCRIPTION ))\n"
                + "ORDER BY organization_id ,orders ";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);

            preparedStatement.setString(3, dateFrom);
            preparedStatement.setString(4, dateTo);

            preparedStatement.setString(5, dateFrom);
            preparedStatement.setString(6, dateTo);

            preparedStatement.setString(7, dateFrom);
            preparedStatement.setString(8, dateTo);

            preparedStatement.setString(9, dateFrom);
            preparedStatement.setString(10, dateTo);

            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);

            ResultSetMetaData rsmd = resultSet1.getMetaData();
            int numberOfColumns = rsmd.getColumnCount();

            while (resultSet1.next()) {
                JsonObject jsonObject2 = new JsonObject();
                for (int i = 1; i <= numberOfColumns; i++) {
                    jsonObject2.addProperty(rsmd.getColumnName(i), resultSet1.getString(rsmd.getColumnName(i)));
                }
                jsonArray.add(jsonObject2);
            }
            jsonResult.add("CMWorkOrderSumm", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("CMWorkOrderSumm", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }


    /*
    * Report PMR_WO_ccpreport using query provided by Maher
    * */
    public String PMR_WO_ccpreport(String dateFrom, String dateTo) {
        String result = null;
        ResultSet resultSet1;
        String statement = "SELECT org.organization_id,vl.DESCRIPTION organization_name, COUNT (*) total_created\n"
                + "    FROM wip.wip_discrete_jobs wdj,\n"
                + "         apps.eam_work_order_details_v wod,\n"
                + "         org_organization_definitions org,\n"
                + "         FND_LOOKUP_VALUES_VL vl\n"
                + "   WHERE     wod.wip_entity_id = wdj.wip_entity_id\n"
                + "         AND wod.user_defined_status_id <> 7       \n"
                + "         AND trunc(wdj.SCHEDULED_START_DATE ) between TO_DATE(?, 'DD/MM/YYYY') and TO_DATE(?, 'DD/MM/YYYY')\n"
                + "         AND wdj.plan_maintenance = 'Y'\n"
                + "         AND org.organization_id = wdj.organization_id\n"
                + "         and org.organization_id=to_number(vl.meaning)\n"
                + "         and lookup_type = 'STC_EAM_ORG_NAME'\n"
                + "GROUP BY org.organization_id,vl.DESCRIPTION\n"
                + "ORDER BY org.organization_id";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            //preparedStatement.setInt(3,orgId);
            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);
            //System.out.println(statement);

            while (resultSet1.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("OrgId", resultSet1.getString("organization_id"));
                jsonObject.addProperty("OrgName", resultSet1.getString("organization_name"));
                int created = resultSet1.getInt("total_created");
                jsonObject.addProperty("CREATED", created);
                int completed = PMR_WO_COMPLETED(dateFrom, dateTo, resultSet1.getInt("organization_id"));
                jsonObject.addProperty("CLOSED", completed);
                int pending = 0;
                float completionPercentage = 0;
                if (created > 0) {
                    pending = created - completed;
                    completionPercentage = (100 * completed / created);
                }
                jsonObject.addProperty("PENDING", pending);
                jsonObject.addProperty("CompletionPercent", completionPercentage);
                jsonArray.add(jsonObject);
            }
            jsonResult.add("PMR_WO_ccpreport", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("PMR_WO_ccpreport", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    // Get PMR_WO_COMPLETED
    public int PMR_WO_COMPLETED(String dateFrom, String dateTo, int orgId) {
        int result = 0;
        String statement = "SELECT COUNT (*)\n"
                + "    as completed\n"
                + "    FROM wip.wip_discrete_jobs wdj,\n"
                + "         apps.eam_work_order_details_v wod,\n"
                + "         org_organization_definitions org\n"
                + "   WHERE     wod.wip_entity_id = wdj.wip_entity_id\n"
                + "         AND wod.user_defined_status_id <> 7        \n"
                + "           and STATUS_TYPE=4     \n"
                + "         AND trunc (wdj.SCHEDULED_START_DATE) between TO_DATE(?, 'DD/MM/YYYY') and TO_DATE(?, 'DD/MM/YYYY')\n"
                + "         AND wdj.plan_maintenance = 'Y'\n"
                + "         AND org.organization_id = wdj.organization_id\n"
                + "         AND org.organization_id = :organization_id";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setInt(3, orgId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            if (resultSet.next()) {
                result = resultSet.getInt("completed");
            }
            preparedStatement.close();
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("PMR_WO_COMPLETED", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }


    /*
    * Report PMRWorkOrderSumm using query provided by Maher
    * */
    public String PMRWorkOrderSumm(String dateFrom, String dateTo) {
        String result = null;
        ResultSet resultSet1;
        String statement = "SELECT organization_id,\n"
                + "       organization_name,\n"
                + "       orders ,\n"
                + "       Status,\n"
                + "       VALUE\n"
                + "  FROM (SELECT organization_id,\n"
                + "               organization_name,\n"
                + "               '1' orders,\n"
                + "               'Created' Status,\n"
                + "               total_created VALUE\n"
                + "          FROM (  SELECT org.organization_id,\n"
                + "                         vl.DESCRIPTION organization_name ,\n"
                + "                         COUNT (*) total_created\n"
                + "                    FROM wip.wip_discrete_jobs wdj,\n"
                + "                         apps.eam_work_order_details_v wod,\n"
                + "                         org_organization_definitions org,\n"
                + "                         FND_LOOKUP_VALUES_VL vl\n"
                + "                   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "                         AND wod.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                         AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                         AND wdj.plan_maintenance = 'Y'\n"
                + "                         AND org.organization_id = wdj.organization_id\n"
                + "                         and org.organization_id=to_number(vl.meaning)\n"
                + "                         and lookup_type = 'STC_EAM_ORG_NAME'\n"
                + "                GROUP BY org.organization_id, vl.DESCRIPTION )\n"
                + "        UNION ALL\n"
                + "        SELECT organization_id,\n"
                + "               organization_name,\n"
                + "               '2' orders,\n"
                + "               'Closed' Status,\n"
                + "               Closed VALUE\n"
                + "          FROM (  SELECT org.organization_id,\n"
                + "                         vl.DESCRIPTION organization_name,\n"
                + "                         COUNT (*) total_created,\n"
                + "                        nvl ((  SELECT COUNT (*) Closed\n"
                + "                              FROM wip.wip_discrete_jobs wdj1,\n"
                + "                                   apps.eam_work_order_details_v wod1,\n"
                + "                                   org_organization_definitions org1\n"
                + "                             WHERE     wod1.wip_entity_id = wdj1.wip_entity_id\n"
                + "                                   AND wod1.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                                   AND STATUS_TYPE = 4            -- completed\n"
                + "                                   AND TRUNC (wdj1.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                                   AND wdj1.plan_maintenance = 'Y'\n"
                + "                                   AND org1.organization_id =\n"
                + "                                          wdj1.organization_id\n"
                + "                                   AND org1.organization_id = org.organization_id\n"
                + "                          GROUP BY org.organization_id, organization_name),0)\n"
                + "                            Closed\n"
                + "                    FROM wip.wip_discrete_jobs wdj,\n"
                + "                         apps.eam_work_order_details_v wod,\n"
                + "                         org_organization_definitions org,\n"
                + "                         FND_LOOKUP_VALUES_VL vl\n"
                + "                   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "                         AND wod.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                         AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                         AND wdj.plan_maintenance = 'Y'\n"
                + "                          and org.organization_id=to_number(vl.meaning)\n"
                + "                         and lookup_type = 'STC_EAM_ORG_NAME'\n"
                + "                         AND org.organization_id = wdj.organization_id\n"
                + "                GROUP BY org.organization_id, vl.DESCRIPTION )\n"
                + "        UNION ALL\n"
                + "        SELECT organization_id,\n"
                + "                   organization_name ,\n"
                + "               '3' orders,\n"
                + "               'Pending' Status,\n"
                + "               (total_created - Closed) VALUE\n"
                + "          FROM (  SELECT org.organization_id,\n"
                + "                         vl.DESCRIPTION organization_name ,\n"
                + "                         COUNT (*) total_created,\n"
                + "                        nvl( (  SELECT COUNT (*) Closed\n"
                + "                              FROM wip.wip_discrete_jobs wdj1,\n"
                + "                                   apps.eam_work_order_details_v wod1,\n"
                + "                                   org_organization_definitions org1\n"
                + "                             WHERE     wod1.wip_entity_id = wdj1.wip_entity_id\n"
                + "                                   AND wod1.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                                   AND STATUS_TYPE = 4            -- completed\n"
                + "                                   AND TRUNC (wdj1.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                                   AND wdj1.plan_maintenance = 'Y'\n"
                + "                                   AND org1.organization_id =\n"
                + "                                          wdj1.organization_id\n"
                + "                                   AND org1.organization_id = org.organization_id\n"
                + "                          GROUP BY org.organization_id, organization_name),0)\n"
                + "                            Closed\n"
                + "                    FROM wip.wip_discrete_jobs wdj,\n"
                + "                         apps.eam_work_order_details_v wod,\n"
                + "                         org_organization_definitions org,\n"
                + "                         FND_LOOKUP_VALUES_VL vl\n"
                + "                   WHERE wod.wip_entity_id = wdj.wip_entity_id\n"
                + "                         AND wod.user_defined_status_id <> 7 --NOT IN (7, 4, 5)\n"
                + "                         AND TRUNC (wdj.SCHEDULED_START_DATE) BETWEEN TO_DATE(?, 'YYYY/MM/DD') and TO_DATE(?, 'YYYY/MM/DD')\n"
                + "                         AND wdj.plan_maintenance = 'Y'\n"
                + "                         AND org.organization_id = wdj.organization_id\n"
                + "                          and org.organization_id=to_number(vl.meaning)\n"
                + "                         and lookup_type = 'STC_EAM_ORG_NAME'\n"
                + "                GROUP BY org.organization_id, vl.DESCRIPTION ))\n"
                + "ORDER BY organization_id ,orders ";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);

            preparedStatement.setString(3, dateFrom);
            preparedStatement.setString(4, dateTo);

            preparedStatement.setString(5, dateFrom);
            preparedStatement.setString(6, dateTo);

            preparedStatement.setString(7, dateFrom);
            preparedStatement.setString(8, dateTo);

            preparedStatement.setString(9, dateFrom);
            preparedStatement.setString(10, dateTo);

            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);

            ResultSetMetaData rsmd = resultSet1.getMetaData();
            int numberOfColumns = rsmd.getColumnCount();

            while (resultSet1.next()) {
                JsonObject jsonObject2 = new JsonObject();
                for (int i = 1; i <= numberOfColumns; i++) {
                    jsonObject2.addProperty(rsmd.getColumnName(i), resultSet1.getString(rsmd.getColumnName(i)));
                }
                jsonArray.add(jsonObject2);
            }
            jsonResult.add("PMRWorkOrderSumm", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("PMRWorkOrderSumm", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    // Required Format for From Date and to date is MM-YYYY
    public String summaryMaintinaceHQC(String dateFrom, String dateTo, int orgId, String lan) {
        String result = null;
        ResultSet resultSet1;
        System.out.println("lan===========================================" + lan);
        String statement = "";
        if (lan.equals("US")) {
            System.out.println("lan is US");
            statement = "SELECT TO_CHAR(TO_DATE('01-' || lookup_code,'dd/mm/rrrr'),'Mon-rr') lookup_code\n"
                    + "FROM fnd_lookup_values WHERE lookup_type = 'EAM_CALENDAR' AND TO_DATE('01-' || lookup_code,'dd/mm/rrrr') BETWEEN \n"
                    + "add_months(TO_DATE('01-' ||?,'dd/mm/rrrr'),-2) AND TO_DATE ('01-' ||?,'dd/mm/rrrr') AND language = 'US'";
        } else if (lan.equals("AR")) {
            System.out.println("lan is AR");
            statement = "SELECT TO_CHAR(TO_DATE('01-' || lookup_code,'dd/mm/rrrr'),'Mon-rr') lookup_code,MEANING\n"
                    + "FROM fnd_lookup_values WHERE lookup_type = 'EAM_CALENDAR' AND TO_DATE('01-' || lookup_code,'dd/mm/rrrr') BETWEEN \n"
                    + "add_months(TO_DATE('01-' ||?,'dd/mm/rrrr'),-2) AND TO_DATE ('01-' ||?,'dd/mm/rrrr') AND language = 'AR'";
        }

        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            //preparedStatement.setInt(3,orgId);
            resultSet1 = preparedStatement.executeQuery();
            resultSet1.setFetchSize(1000);

            while (resultSet1.next()) {
//                if(lan.equals("US")){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("lookupCode", resultSet1.getString("lookup_code"));
                jsonObject.add("data", summaryMaintinaceHQCP2(resultSet1.getString("lookup_code"), orgId));
                jsonArray.add(jsonObject);
//                }else if(lan.equals("AR")){
//                    JsonObject jsonObject = new JsonObject();
//                jsonObject.addProperty("lookupCode",resultSet1.getString("MEANING"));
//                jsonObject.add("data",summaryMaintinaceHQCP2(resultSet1.getString("MEANING"),orgId));
//                jsonArray.add(jsonObject);
//                }

            }
            jsonResult.add("summaryMaintinaceHQC", jsonArray);
            result = jsonResult.toString();
            preparedStatement.close();
            resultSet1.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("summaryMaintinaceHQC", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        System.out.println(result);
        return result;
    }

    public JsonArray summaryMaintinaceHQCP2(String lookupCode, int orgId) {
        JsonObject jsonResult = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        String statement = "SELECT * FROM (SELECT COUNT (*)\n"
                + "     as value, 'CREATED' as name\n"
                + "     FROM wip_discrete_jobs jo,\n"
                + "          wip_entities en,\n"
                + "          bom_departments bd,\n"
                + "          eam_work_order_details st\n"
                + "    WHERE     jo.wip_entity_id = en.wip_entity_id\n"
                + "          AND jo.owning_department = bd.department_id\n"
                + "          AND jo.wip_entity_id = st.wip_entity_id\n"
                + "          AND jo.organization_id = st.organization_id\n"
                + "          AND jo.organization_id = ?\n"
                + "          AND jo.plan_maintenance <> 'Y'\n"
                + "          AND st.user_defined_status_id <> 7\n"
                + "          AND TO_CHAR (jo.creation_date, 'Mon-rr') = ?\n"
                + "    UNION ALL\n"
                + "    SELECT COUNT (*)\n"
                + "     as CP_HVAC, 'HVAC' as name\n"
                + "     FROM wip_discrete_jobs jo,\n"
                + "          wip_entities en,\n"
                + "          bom_departments bd,\n"
                + "          eam_work_order_details st\n"
                + "    WHERE     jo.wip_entity_id = en.wip_entity_id\n"
                + "          AND jo.owning_department = bd.department_id\n"
                + "          AND jo.wip_entity_id = st.wip_entity_id\n"
                + "          AND jo.organization_id = st.organization_id\n"
                + "          AND jo.organization_id = ?\n"
                + "          AND jo.plan_maintenance <> 'Y'\n"
                + "          AND st.user_defined_status_id <> 7\n"
                + "          AND TO_CHAR (jo.creation_date, 'Mon-rr') = ?\n"
                + "          AND BD.DEPARTMENT_CODE IN ('060')\n"
                + "    UNION ALL\n"
                + "    SELECT COUNT (*)\n"
                + "     as CP_ELEC,'ELEC' as name\n"
                + "     FROM wip_discrete_jobs jo,\n"
                + "          wip_entities en,\n"
                + "          bom_departments bd,\n"
                + "          eam_work_order_details st\n"
                + "    WHERE     jo.wip_entity_id = en.wip_entity_id\n"
                + "          AND jo.owning_department = bd.department_id\n"
                + "          AND jo.wip_entity_id = st.wip_entity_id\n"
                + "          AND jo.organization_id = st.organization_id\n"
                + "          AND jo.organization_id = ?\n"
                + "          AND jo.plan_maintenance <> 'Y'\n"
                + "          AND st.user_defined_status_id <> 7\n"
                + "          AND TO_CHAR (jo.creation_date, 'Mon-rr') = ?\n"
                + "          AND BD.DEPARTMENT_CODE IN ('050', '070')\n"
                + "    UNION ALL\n"
                + "    SELECT COUNT (*)\n"
                + "     as CP_CIVIL,'CIVIL' as name\n"
                + "     FROM wip_discrete_jobs jo,\n"
                + "          wip_entities en,\n"
                + "          bom_departments bd,\n"
                + "          eam_work_order_details st\n"
                + "    WHERE     jo.wip_entity_id = en.wip_entity_id\n"
                + "          AND jo.owning_department = bd.department_id\n"
                + "          AND jo.wip_entity_id = st.wip_entity_id\n"
                + "          AND jo.organization_id = st.organization_id\n"
                + "          AND jo.organization_id = ?\n"
                + "          AND jo.plan_maintenance <> 'Y'\n"
                + "          AND st.user_defined_status_id <> 7\n"
                + "          AND TO_CHAR (jo.creation_date, 'Mon-rr') = ?\n"
                + "          AND BD.DEPARTMENT_CODE IN ('020', '030', '040', '100')\n"
                + "    UNION ALL\n"
                + "    SELECT COUNT (*)\n"
                + "     as CP_AGRI,'AGRI' as name\n"
                + "     FROM wip_discrete_jobs jo,\n"
                + "          wip_entities en,\n"
                + "          bom_departments bd,\n"
                + "          eam_work_order_details st\n"
                + "    WHERE     jo.wip_entity_id = en.wip_entity_id\n"
                + "          AND jo.owning_department = bd.department_id\n"
                + "          AND jo.wip_entity_id = st.wip_entity_id\n"
                + "          AND jo.organization_id = st.organization_id\n"
                + "          AND jo.organization_id = ?\n"
                + "          AND jo.plan_maintenance <> 'Y'\n"
                + "          AND st.user_defined_status_id <> 7\n"
                + "          AND TO_CHAR (jo.creation_date, 'Mon-rr') = ?\n"
                + "          AND BD.DEPARTMENT_CODE IN ('010', '090')\n"
                + "    UNION ALL\n"
                + "    SELECT COUNT (*)\n"
                + "     as CP_HK ,'H K (CLEAN)' as name\n"
                + "     FROM wip_discrete_jobs jo,\n"
                + "          wip_entities en,\n"
                + "          bom_departments bd,\n"
                + "          eam_work_order_details st\n"
                + "    WHERE     jo.wip_entity_id = en.wip_entity_id\n"
                + "          AND jo.owning_department = bd.department_id\n"
                + "          AND jo.wip_entity_id = st.wip_entity_id\n"
                + "          AND jo.organization_id = st.organization_id\n"
                + "          AND jo.organization_id = ?\n"
                + "          AND jo.plan_maintenance <> 'Y'\n"
                + "          AND st.user_defined_status_id <> 7\n"
                + "          AND TO_CHAR (jo.creation_date, 'Mon-rr') = ?\n"
                + "          AND BD.DEPARTMENT_CODE IN ('080', '120')\n"
                + "    UNION ALL\n"
                + "    SELECT COUNT (*)\n"
                + "     as CP_MECH,'MECH' as name\n"
                + "     FROM wip_discrete_jobs jo,\n"
                + "          wip_entities en,\n"
                + "          bom_departments bd,\n"
                + "          eam_work_order_details st\n"
                + "    WHERE     jo.wip_entity_id = en.wip_entity_id\n"
                + "          AND jo.owning_department = bd.department_id\n"
                + "          AND jo.wip_entity_id = st.wip_entity_id\n"
                + "          AND jo.organization_id = st.organization_id\n"
                + "          AND jo.organization_id = ?\n"
                + "          AND jo.plan_maintenance <> 'Y'\n"
                + "          AND st.user_defined_status_id <> 7\n"
                + "          AND TO_CHAR (jo.creation_date, 'Mon-rr') = ?\n"
                + "          AND BD.DEPARTMENT_CODE IN ('130'))";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);
            preparedStatement.setInt(1, orgId);
            preparedStatement.setString(2, lookupCode);

            preparedStatement.setInt(3, orgId);
            preparedStatement.setString(4, lookupCode);

            preparedStatement.setInt(5, orgId);
            preparedStatement.setString(6, lookupCode);

            preparedStatement.setInt(7, orgId);
            preparedStatement.setString(8, lookupCode);

            preparedStatement.setInt(9, orgId);
            preparedStatement.setString(10, lookupCode);

            preparedStatement.setInt(11, orgId);
            preparedStatement.setString(12, lookupCode);

            preparedStatement.setInt(13, orgId);
            preparedStatement.setString(14, lookupCode);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                //jsonObject.addProperty("Name",resultSet.getString(2));
                jsonObject.addProperty(resultSet.getString(2), resultSet.getInt(1));
                jsonArray.add(jsonObject);
            }

            preparedStatement.close();
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("summary Maintinace HQC P2", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return jsonArray;
    }

    //----------------------------------------------------------------------------------------------------------------//
    public String woStatusReportCM_EM(String dateFrom, String dateTo, String orgId) {
        String result = null;

        String statement = "select (select count(wip_entity_id) as count from APPS.eam_work_orders_v where plan_maintenance = 'N' \n"
                + "and owning_department_code in ('050','070') and organization_id = ? and work_order_status in ('Closed','Complete') \n"
                + "and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) as CLOSED_WO,\n"
                + "(select count(wip_entity_id) as count from APPS.eam_work_orders_v where plan_maintenance = 'N' \n"
                + "and owning_department_code in ('050','070') and organization_id = ? and work_order_status not in ('Closed','Complete')\n"
                + "and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS PENDING_WO\n"
                + "FROM DUAL";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date startDate = formatter.parse(dateFrom);
            java.util.Date endDate = formatter.parse(dateTo);

            Calendar cStart = Calendar.getInstance();
            cStart.setTime(startDate);
            Calendar cEnd = Calendar.getInstance();
            cEnd.setTime(endDate);

            cEnd.add(Calendar.MONTH, 1);

            while (cStart.before(cEnd)) {

                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.setFetchSize(1000);

                setDateRange(orgId, startDate, endDate, cStart, cEnd, preparedStatement);

                resultSet = preparedStatement.executeQuery();
                resultSet.setFetchSize(1000);

                while (resultSet.next()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("MONTH", monthInString.get(cStart.get(Calendar.MONTH)) + "-" + cStart.get(Calendar.YEAR));
                    jsonObject.addProperty("CLOSED_WO", resultSet.getInt("CLOSED_WO"));
                    jsonObject.addProperty("PENDING_WO", resultSet.getInt("PENDING_WO"));
                    jsonArray.add(jsonObject);
                }

                preparedStatement.close();
                resultSet.close();

                cStart.add(Calendar.MONTH, 1);
            }

            jsonResult.add("WO_STATUS_REPORT_CM_EM", jsonArray);
            result = jsonResult.toString();

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("CM EM Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String woStatusReportCM_Civil(String dateFrom, String dateTo, String orgId) {
        String result = null;

        String statement = "select (select count(wip_entity_id) as count from APPS.eam_work_orders_v where plan_maintenance = 'N' \n"
                + "and owning_department_code in ('020','030','040','100') and organization_id = ? and work_order_status in ('Closed','Complete') \n"
                + "and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) as CLOSED_WO,\n"
                + "(select count(wip_entity_id) as count from APPS.eam_work_orders_v where plan_maintenance = 'N' \n"
                + "and owning_department_code in ('020','030','040','100') and organization_id = ? and work_order_status not in ('Closed','Complete')\n"
                + "and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS PENDING_WO\n"
                + "FROM DUAL";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date startDate = formatter.parse(dateFrom);
            java.util.Date endDate = formatter.parse(dateTo);

            Calendar cStart = Calendar.getInstance();
            cStart.setTime(startDate);
            Calendar cEnd = Calendar.getInstance();
            cEnd.setTime(endDate);

            cEnd.add(Calendar.MONTH, 1);

            while (cStart.before(cEnd)) {

                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.setFetchSize(1000);

                setDateRange(orgId, startDate, endDate, cStart, cEnd, preparedStatement);

                resultSet = preparedStatement.executeQuery();
                resultSet.setFetchSize(1000);

                while (resultSet.next()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("MONTH", monthInString.get(cStart.get(Calendar.MONTH)) + "-" + cStart.get(Calendar.YEAR));
                    jsonObject.addProperty("CLOSED_WO", resultSet.getInt("CLOSED_WO"));
                    jsonObject.addProperty("PENDING_WO", resultSet.getInt("PENDING_WO"));
                    jsonArray.add(jsonObject);
                }

                preparedStatement.close();
                resultSet.close();

                cStart.add(Calendar.MONTH, 1);
            }

            jsonResult.add("WO_STATUS_REPORT_CM_Civil", jsonArray);
            result = jsonResult.toString();

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("CM Civil Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String woStatusReportPMR_EM(String dateFrom, String dateTo, String orgId) {
        String result = null;

        String statement = "select (select count(wip_entity_id) as count from APPS.eam_work_orders_v where plan_maintenance = 'Y' \n"
                + "and owning_department_code in ('050','070') and organization_id = ? and work_order_status in ('Closed','Complete') \n"
                + "and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) as CLOSED_WO,\n"
                + "(select count(wip_entity_id) as count from APPS.eam_work_orders_v where plan_maintenance = 'Y' \n"
                + "and owning_department_code in ('050','070') and organization_id = ? and work_order_status not in ('Closed','Complete')\n"
                + "and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS PENDING_WO\n"
                + "FROM DUAL";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date startDate = formatter.parse(dateFrom);
            java.util.Date endDate = formatter.parse(dateTo);

            Calendar cStart = Calendar.getInstance();
            cStart.setTime(startDate);
            Calendar cEnd = Calendar.getInstance();
            cEnd.setTime(endDate);

            cEnd.add(Calendar.MONTH, 1);

            while (cStart.before(cEnd)) {

                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.setFetchSize(1000);

                setDateRange(orgId, startDate, endDate, cStart, cEnd, preparedStatement);

                resultSet = preparedStatement.executeQuery();
                resultSet.setFetchSize(1000);

                while (resultSet.next()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("MONTH", monthInString.get(cStart.get(Calendar.MONTH)) + "-" + cStart.get(Calendar.YEAR));
                    jsonObject.addProperty("CLOSED_WO", resultSet.getInt("CLOSED_WO"));
                    jsonObject.addProperty("PENDING_WO", resultSet.getInt("PENDING_WO"));
                    jsonArray.add(jsonObject);
                }

                preparedStatement.close();
                resultSet.close();

                cStart.add(Calendar.MONTH, 1);
            }

            jsonResult.add("WO_STATUS_REPORT_PMR_EM", jsonArray);
            result = jsonResult.toString();

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("PMR EM Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String woStatusReportPMR_Civil(String dateFrom, String dateTo, String orgId) {
        String result = null;

        String statement = "select (select count(wip_entity_id) as count from APPS.eam_work_orders_v where plan_maintenance = 'Y' \n"
                + "and owning_department_code in ('020','030','040','100') and organization_id = ? and work_order_status in ('Closed','Complete') \n"
                + "and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) as CLOSED_WO,\n"
                + "(select count(wip_entity_id) as count from APPS.eam_work_orders_v where plan_maintenance = 'Y' \n"
                + "and owning_department_code in ('020','030','040','100') and organization_id = ? and work_order_status not in ('Closed','Complete')\n"
                + "and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS PENDING_WO\n"
                + "FROM DUAL";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date startDate = formatter.parse(dateFrom);
            java.util.Date endDate = formatter.parse(dateTo);

            Calendar cStart = Calendar.getInstance();
            cStart.setTime(startDate);
            Calendar cEnd = Calendar.getInstance();
            cEnd.setTime(endDate);

            cEnd.add(Calendar.MONTH, 1);

            while (cStart.before(cEnd)) {

                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.setFetchSize(1000);

                setDateRange(orgId, startDate, endDate, cStart, cEnd, preparedStatement);

                resultSet = preparedStatement.executeQuery();
                resultSet.setFetchSize(1000);

                while (resultSet.next()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("MONTH", monthInString.get(cStart.get(Calendar.MONTH)) + "-" + cStart.get(Calendar.YEAR));
                    jsonObject.addProperty("CLOSED_WO", resultSet.getInt("CLOSED_WO"));
                    jsonObject.addProperty("PENDING_WO", resultSet.getInt("PENDING_WO"));
                    jsonArray.add(jsonObject);
                }

                preparedStatement.close();
                resultSet.close();

                cStart.add(Calendar.MONTH, 1);
            }

            jsonResult.add("WO_STATUS_REPORT_PMR_Civil", jsonArray);
            result = jsonResult.toString();

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("PMR Civil Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String woStatusReportIgate(String dateFrom, String dateTo, String orgId) {
        String result = null;

        String statement = "select (select count (ewov.wip_entity_id) from APPS.wip_eam_work_requests wewr, APPS.eam_work_orders_v ewov \n"
                + "where wewr.wip_entity_id = ewov.wip_entity_id and wewr.attribute15 is not null and ewov.organization_id = ? and ewov.scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY')\n"
                + "and ewov.scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY') and work_order_status in ('Closed','Complete')) as CLOSED_WO,\n"
                + "(select count (ewov.wip_entity_id) from APPS.wip_eam_work_requests wewr, APPS.eam_work_orders_v ewov \n"
                + "where wewr.wip_entity_id = ewov.wip_entity_id and wewr.attribute15 is not null and ewov.organization_id = ? and ewov.scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY')\n"
                + "and ewov.scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY') and work_order_status not in ('Closed','Complete')) as PENDING_WO\n"
                + "FROM DUAL";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date startDate = formatter.parse(dateFrom);
            java.util.Date endDate = formatter.parse(dateTo);

            Calendar cStart = Calendar.getInstance();
            cStart.setTime(startDate);
            Calendar cEnd = Calendar.getInstance();
            cEnd.setTime(endDate);

            cEnd.add(Calendar.MONTH, 1);

            while (cStart.before(cEnd)) {

                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.setFetchSize(1000);

                setDateRange(orgId, startDate, endDate, cStart, cEnd, preparedStatement);

                resultSet = preparedStatement.executeQuery();
                resultSet.setFetchSize(1000);

                while (resultSet.next()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("MONTH", monthInString.get(cStart.get(Calendar.MONTH)) + "-" + cStart.get(Calendar.YEAR));
                    jsonObject.addProperty("CLOSED_WO", resultSet.getInt("CLOSED_WO"));
                    jsonObject.addProperty("PENDING_WO", resultSet.getInt("PENDING_WO"));
                    jsonArray.add(jsonObject);
                }

                preparedStatement.close();
                resultSet.close();

                cStart.add(Calendar.MONTH, 1);
            }

            jsonResult.add("WO_STATUS_REPORT_IGATE", jsonArray);
            result = jsonResult.toString();

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("IGATE REPORT Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String satisfactionLevel(String dateFrom, String dateTo, String orgId) {

        String result = null;

        String statement = "SELECT TO_CHAR(ROUND (SUM (qa.CHARACTER1) / SUM (qa.CHARACTER1 + qa.CHARACTER2), 2)\n"
                + "       * 100)\n"
                + "          Satisfied,\n"
                + "       TO_CHAR(ROUND (SUM (qa.CHARACTER2) / SUM (qa.CHARACTER1 + qa.CHARACTER2), 2)\n"
                + "       * 100)\n"
                + "          Not_Satisfied,\n"
                + "     SUM (qa.CHARACTER1 + qa.CHARACTER2) total_wo1,\n"
                + "     '  '  total_wo   \n"
                + " \n"
                + "  FROM APPS.qa_results qa, APPS.qa_plans qp\n"
                + "WHERE 1 = 1 AND qp.plan_id = qa.plan_id AND qp.attribute1 = '32'\n"
                + "       AND TO_DATE (character100, 'MM-YYYY') BETWEEN (to_date(?,'YYYY-MM-DD'))\n"
                + "                                                  AND (to_date(?,'YYYY-MM-DD'))\n"
                + "    AND qa.organization_id= ?";
        try {

            connection = pool.getConnection();

            System.out.println(statement);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setString(3, orgId);
            System.out.println(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                if (resultSet.getString("SATISFIED") != null) {
                    jsonObject.addProperty("SATISFIED", resultSet.getString("SATISFIED"));
                } else {
                    jsonObject.addProperty("SATISFIED", "0");
                }

                if (resultSet.getString("NOT_SATISFIED") != null) {
                    jsonObject.addProperty("NOT_SATISFIED", resultSet.getString("NOT_SATISFIED"));
                } else {
                    jsonObject.addProperty("NOT_SATISFIED", "0");
                }

                if (resultSet.getString("TOTAL_WO1") != null) {
                    jsonObject.addProperty("TOTAL_WO", resultSet.getString("TOTAL_WO1"));
                } else {
                    jsonObject.addProperty("TOTAL_WO", "0");
                }
                jsonArray.add(jsonObject);
            }

            jsonResult.add("SATISFACTION_LEVEL", jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Satisfaction Level Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String CMworkOrderSummary(Date dateFrom, Date dateTo) {

        String result = "";

        String statement1 = "select oav.organization_name, count(ewov.wip_entity_id) as count \n"
                + "    from APPS.org_access_view oav left join APPS.eam_work_orders_v ewov\n"
                + "    on ewov.organization_id = oav.organization_id and ewov.plan_maintenance = 'N' \n"
                + "    and ewov.scheduled_start_date >= ?\n"
                + "    and ewov.scheduled_start_date <= ?\n"
                + "    left join APPS.mtl_parameters mp\n"
                + "    on mp.organization_id = oav.organization_id\n"
                + "    where mp.eam_enabled_flag = 'Y'\n"
                + "    group by oav.organization_name";

        String statement2 = "select oav.organization_name, count(ewov.wip_entity_id) as count \n"
                + "    from APPS.org_access_view oav left join APPS.eam_work_orders_v ewov\n"
                + "    on ewov.organization_id = oav.organization_id and ewov.plan_maintenance = 'N' and ewov.work_order_status in ('Closed','Complete')\n"
                + "    and ewov.scheduled_start_date >= ?\n"
                + "    and ewov.scheduled_start_date <= ?\n"
                + "    left join APPS.mtl_parameters mp\n"
                + "    on mp.organization_id = oav.organization_id\n"
                + "    where mp.eam_enabled_flag = 'Y'\n"
                + "    group by oav.organization_name";

        String statement3 = "select oav.organization_name, count(ewov.wip_entity_id) as count \n"
                + "    from APPS.org_access_view oav left join APPS.eam_work_orders_v ewov\n"
                + "    on ewov.organization_id = oav.organization_id and ewov.plan_maintenance = 'N' and ewov.work_order_status not in ('Closed','Complete')\n"
                + "    and ewov.scheduled_start_date >= ?\n"
                + "    and ewov.scheduled_start_date <= ?\n"
                + "    left join APPS.mtl_parameters mp\n"
                + "    on mp.organization_id = oav.organization_id\n"
                + "    where mp.eam_enabled_flag = 'Y'\n"
                + "    group by oav.organization_name";

        try {

            connection = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement1);
            preparedStatement.setDate(1, dateFrom);
            preparedStatement.setDate(2, dateTo);
            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonArray jsonArray1 = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORGANIZATION_NAME", resultSet.getString("ORGANIZATION_NAME"));
                jsonObject.addProperty("COUNT", resultSet.getInt("COUNT"));
                jsonArray1.add(jsonObject);
            }

            preparedStatement.close();
            resultSet.close();

            preparedStatement = connection.prepareStatement(statement2);
            preparedStatement.setDate(1, dateFrom);
            preparedStatement.setDate(2, dateTo);
            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonArray jsonArray2 = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORGANIZATION_NAME", resultSet.getString("ORGANIZATION_NAME"));
                jsonObject.addProperty("COUNT", resultSet.getInt("COUNT"));
                jsonArray2.add(jsonObject);
            }
            preparedStatement.close();
            resultSet.close();

            preparedStatement = connection.prepareStatement(statement3);
            preparedStatement.setDate(1, dateFrom);
            preparedStatement.setDate(2, dateTo);
            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonArray jsonArray3 = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORGANIZATION_NAME", resultSet.getString("ORGANIZATION_NAME"));
                jsonObject.addProperty("COUNT", resultSet.getInt("COUNT"));
                jsonArray3.add(jsonObject);
            }
            preparedStatement.close();
            resultSet.close();

            JsonObject jsonResult = new JsonObject();
            jsonResult.add("CREATED_WO", jsonArray1);
            jsonResult.add("CLOSED_WO", jsonArray2);
            jsonResult.add("PENDING_WO", jsonArray3);

            connection.close();
            result = jsonResult.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Initialize User Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String PMRworkOrderSummary(String dateFrom, String dateTo) {
        String result = "";

        String statement1 = "select oav.organization_name, count(ewov.wip_entity_id) as count \n"
                + "    from APPS.org_access_view oav left join APPS.eam_work_orders_v ewov\n"
                + "    on ewov.organization_id = oav.organization_id and ewov.plan_maintenance = 'Y' \n"
                + "    and ewov.scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY')\n"
                + "    and ewov.scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')\n"
                + "    left join APPS.mtl_parameters mp\n"
                + "    on mp.organization_id = oav.organization_id\n"
                + "    where mp.eam_enabled_flag = 'Y'\n"
                + "    group by oav.organization_name";

        String statement2 = "select oav.organization_name, count(ewov.wip_entity_id) as count \n"
                + "    from APPS.org_access_view oav left join APPS.eam_work_orders_v ewov\n"
                + "    on ewov.organization_id = oav.organization_id and ewov.plan_maintenance = 'Y' and ewov.work_order_status in ('Closed','Complete')\n"
                + "    and ewov.scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY')\n"
                + "    and ewov.scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')\n"
                + "    left join APPS.mtl_parameters mp\n"
                + "    on mp.organization_id = oav.organization_id\n"
                + "    where mp.eam_enabled_flag = 'Y'\n"
                + "    group by oav.organization_name";

        String statement3 = "select oav.organization_name, count(ewov.wip_entity_id) as count \n"
                + "    from APPS.org_access_view oav left join APPS.eam_work_orders_v ewov\n"
                + "    on ewov.organization_id = oav.organization_id and ewov.plan_maintenance = 'Y' and ewov.work_order_status not in ('Closed','Complete')\n"
                + "    and ewov.scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY')\n"
                + "    and ewov.scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')\n"
                + "    left join APPS.mtl_parameters mp\n"
                + "    on mp.organization_id = oav.organization_id\n"
                + "    where mp.eam_enabled_flag = 'Y'\n"
                + "    group by oav.organization_name";

        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement1);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonArray jsonArray1 = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORGANIZATION_NAME", resultSet.getString("ORGANIZATION_NAME"));
                jsonObject.addProperty("COUNT", resultSet.getInt("COUNT"));
                jsonArray1.add(jsonObject);
            }

            preparedStatement.close();
            resultSet.close();

            preparedStatement = connection.prepareStatement(statement2);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonArray jsonArray2 = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORGANIZATION_NAME", resultSet.getString("ORGANIZATION_NAME"));
                jsonObject.addProperty("COUNT", resultSet.getInt("COUNT"));
                jsonArray2.add(jsonObject);
            }
            preparedStatement.close();
            resultSet.close();

            preparedStatement = connection.prepareStatement(statement3);
            preparedStatement.setString(1, dateFrom);
            preparedStatement.setString(2, dateTo);
            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonArray jsonArray3 = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORGANIZATION_NAME", resultSet.getString("ORGANIZATION_NAME"));
                jsonObject.addProperty("COUNT", resultSet.getInt("COUNT"));
                jsonArray3.add(jsonObject);
            }
            preparedStatement.close();
            resultSet.close();

            JsonObject jsonResult = new JsonObject();
            jsonResult.add("CREATED_WO", jsonArray1);
            jsonResult.add("CLOSED_WO", jsonArray2);
            jsonResult.add("PENDING_WO", jsonArray3);

            connection.close();
            result = jsonResult.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Initialize User Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String summaryMaintenanceType(String month, String year) {
        String result = null;

        String statement = "select (select count(wip_entity_id) from APPS.eam_work_orders_v\n"
                + "where owning_department_code in ('060') and scheduled_start_date >= to_date(?,'DD/MM/YYYY')\n"
                + "and scheduled_start_date <= to_date(?,'DD/MM/YYYY')) as hvac_count,\n"
                + "(select count(wip_entity_id) from APPS.eam_work_orders_v\n"
                + "where owning_department_code in ('050','070') and scheduled_start_date >= to_date(?,'DD/MM/YYYY')\n"
                + "and scheduled_start_date <= to_date(?,'DD/MM/YYYY')) as elec_count,\n"
                + "(select count(wip_entity_id) from APPS.eam_work_orders_v\n"
                + "where owning_department_code in ('020','030','040','100') and scheduled_start_date >= to_date(?,'DD/MM/YYYY')\n"
                + "and scheduled_start_date <= to_date(?,'DD/MM/YYYY')) as civil_count,\n"
                + "(select count(wip_entity_id) from APPS.eam_work_orders_v\n"
                + "where owning_department_code in ('010','090') and scheduled_start_date >= to_date(?,'DD/MM/YYYY')\n"
                + "and scheduled_start_date <= to_date(?,'DD/MM/YYYY')) as agri_count,\n"
                + "(select count(wip_entity_id) from APPS.eam_work_orders_v\n"
                + "where owning_department_code in ('080','120') and scheduled_start_date >= to_date(?,'DD/MM/YYYY')\n"
                + "and scheduled_start_date <= to_date(?,'DD/MM/YYYY')) as clean_count,\n"
                + "(select count(wip_entity_id) from APPS.eam_work_orders_v\n"
                + "where owning_department_code in ('130') and scheduled_start_date >= to_date(?,'DD/MM/YYYY')\n"
                + "and scheduled_start_date <= to_date(?,'DD/MM/YYYY')) as mech_count\n"
                + "from dual";
        try {

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            connection = pool.getConnection();
            connection.setAutoCommit(false);

            int deptTotal1 = 0;
            int deptTotal2 = 0;
            int deptTotal3 = 0;
            int deptTotal4 = 0;
            int deptTotal5 = 0;
            int deptTotal6 = 0;

            JsonArray monthlyJsonArray = new JsonArray();

            String date = "";
            int i = Integer.parseInt(month);

            if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12) {
                date = "31/";
            } else if (i == 4 || i == 6 || i == 9 || i == 11) {
                date = "30/";
            } else {
                if (isLeapYear(Integer.parseInt(year))) {
                    date = "29/";
                } else {
                    date = "28/";
                }
            }
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, "01" + "/" + i + "/" + year);
            preparedStatement.setString(3, "01" + "/" + i + "/" + year);
            preparedStatement.setString(5, "01" + "/" + i + "/" + year);
            preparedStatement.setString(7, "01" + "/" + i + "/" + year);
            preparedStatement.setString(9, "01" + "/" + i + "/" + year);
            preparedStatement.setString(11, "01" + "/" + i + "/" + year);

            preparedStatement.setString(2, date + i + "/" + year);
            preparedStatement.setString(4, date + i + "/" + year);
            preparedStatement.setString(6, date + i + "/" + year);
            preparedStatement.setString(8, date + i + "/" + year);
            preparedStatement.setString(10, date + i + "/" + year);
            preparedStatement.setString(12, date + i + "/" + year);

            preparedStatement.setFetchSize(1000);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            int total = 0;

            //Random rand = new Random();
            while (resultSet.next()) {

                int count1 = resultSet.getInt("HVAC_COUNT");
                int count2 = resultSet.getInt("ELEC_COUNT");
                int count3 = resultSet.getInt("CIVIL_COUNT");
                int count4 = resultSet.getInt("AGRI_COUNT");
                int count5 = resultSet.getInt("CLEAN_COUNT");
                int count6 = resultSet.getInt("MECH_COUNT");

                /*int count1 = rand.nextInt(100) + 1;
                int count2 = rand.nextInt(100) + 1;
                int count3 = rand.nextInt(100) + 1;
                int count4 = rand.nextInt(100) + 1;
                int count5 = rand.nextInt(100) + 1;
                int count6 = rand.nextInt(100) + 1;*/
                JsonObject jsonObject1 = new JsonObject();
                jsonObject1.addProperty("DEPARTMENT", "HVAC");
                jsonObject1.addProperty("COUNT", count1);
                JsonObject jsonObject2 = new JsonObject();
                jsonObject2.addProperty("DEPARTMENT", "ELEC");
                jsonObject2.addProperty("COUNT", count2);
                JsonObject jsonObject3 = new JsonObject();
                jsonObject3.addProperty("DEPARTMENT", "CIVIL");
                jsonObject3.addProperty("COUNT", count3);
                JsonObject jsonObject4 = new JsonObject();
                jsonObject4.addProperty("DEPARTMENT", "AGRI");
                jsonObject4.addProperty("COUNT", count4);
                JsonObject jsonObject5 = new JsonObject();
                jsonObject5.addProperty("DEPARTMENT", "CLEAN");
                jsonObject5.addProperty("COUNT", count5);
                JsonObject jsonObject6 = new JsonObject();
                jsonObject6.addProperty("DEPARTMENT", "MECH");
                jsonObject6.addProperty("COUNT", count6);

                monthlyJsonArray.add(jsonObject1);
                monthlyJsonArray.add(jsonObject2);
                monthlyJsonArray.add(jsonObject3);
                monthlyJsonArray.add(jsonObject4);
                monthlyJsonArray.add(jsonObject5);
                monthlyJsonArray.add(jsonObject6);

                total = count1 + count2 + count3 + count4 + count5 + count6;

                deptTotal1 = deptTotal1 + count1;
                deptTotal2 = deptTotal2 + count2;
                deptTotal3 = deptTotal3 + count3;
                deptTotal4 = deptTotal4 + count4;
                deptTotal5 = deptTotal5 + count5;
                deptTotal6 = deptTotal6 + count6;
            }

            preparedStatement.close();
            resultSet.close();

            JsonObject summaryObject = new JsonObject();
            summaryObject.addProperty("MONTH", monthInString.get(i - 1) + "-" + year);
            summaryObject.add("RESULT", monthlyJsonArray);
            summaryObject.addProperty("TOTAL_MONTH", total);
            jsonArray.add(summaryObject);

            JsonArray totalJsonArray = new JsonArray();

            JsonObject jsonObject1 = new JsonObject();
            jsonObject1.addProperty("DEPARTMENT", "HVAC");
            jsonObject1.addProperty("COUNT", deptTotal1);
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.addProperty("DEPARTMENT", "ELEC");
            jsonObject2.addProperty("COUNT", deptTotal2);
            JsonObject jsonObject3 = new JsonObject();
            jsonObject3.addProperty("DEPARTMENT", "CIVIL");
            jsonObject3.addProperty("COUNT", deptTotal3);
            JsonObject jsonObject4 = new JsonObject();
            jsonObject4.addProperty("DEPARTMENT", "AGRI");
            jsonObject4.addProperty("COUNT", deptTotal4);
            JsonObject jsonObject5 = new JsonObject();
            jsonObject5.addProperty("DEPARTMENT", "CLEAN");
            jsonObject5.addProperty("COUNT", deptTotal5);
            JsonObject jsonObject6 = new JsonObject();
            jsonObject6.addProperty("DEPARTMENT", "MECH");
            jsonObject6.addProperty("COUNT", deptTotal6);

            totalJsonArray.add(jsonObject1);
            totalJsonArray.add(jsonObject2);
            totalJsonArray.add(jsonObject3);
            totalJsonArray.add(jsonObject4);
            totalJsonArray.add(jsonObject5);
            totalJsonArray.add(jsonObject6);

            //totalJsonObject.add("TOTAL_DEPT", totalJsonArray);
            //jsonArray.add(totalJsonObject);
            jsonResult.add("SUMMARY_MAINTENANCE_TYPE", jsonArray);
            jsonResult.add("TOTAL_DEPT", totalJsonArray);
            result = jsonResult.toString();

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Summary Maintenance Type Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String createdClosedWeeklyReport(String year, String month) {
        String result = null;

        // from date = 2018-01-18
        // to date = 2018-01-18
        String statement = "SELECT (select count(wip_entity_id) as COUNT from APPS.eam_work_orders_v \n"
                + "where scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') \n"
                + "and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS WEEK_1_CREATED,\n"
                + "(select count(wip_entity_id) as COUNT from APPS.eam_work_orders_v \n"
                + "where scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') \n"
                + "and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS WEEK_2_CREATED,\n"
                + "(select count(wip_entity_id) as COUNT from APPS.eam_work_orders_v \n"
                + "where scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') \n"
                + "and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS WEEK_3_CREATED,\n"
                + "(select count(wip_entity_id) as COUNT from APPS.eam_work_orders_v \n"
                + "where scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') \n"
                + "and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS WEEK_4_CREATED,\n"
                + "(select count(*) as COUNT from APPS.eam_work_orders_v \n"
                + "where work_order_status in('Closed','Complete') and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') \n"
                + "and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS WEEK_1_CLOSED,\n"
                + "(select count(*) as COUNT from APPS.eam_work_orders_v \n"
                + "where work_order_status in('Closed','Complete') and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') \n"
                + "and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS WEEK_2_CLOSED,\n"
                + "(select count(*) as COUNT from APPS.eam_work_orders_v \n"
                + "where work_order_status in('Closed','Complete') and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') \n"
                + "and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS WEEK_3_CLOSED,\n"
                + "(select count(*) as COUNT from APPS.eam_work_orders_v \n"
                + "where work_order_status in('Closed','Complete') and scheduled_start_date >= TO_DATE(?, 'DD/MM/YYYY') \n"
                + "and scheduled_start_date <= TO_DATE(?, 'DD/MM/YYYY')) AS WEEK_4_CLOSED\n"
                + "FROM DUAL";
        try {

            connection = pool.getConnection();
            connection.setAutoCommit(false);
            System.out.println(statement);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, "01/" + month + "/" + year);
            preparedStatement.setString(2, "08/" + month + "/" + year);
            preparedStatement.setString(3, "08/" + month + "/" + year);
            preparedStatement.setString(4, "15/" + month + "/" + year);
            preparedStatement.setString(5, "15/" + month + "/" + year);
            preparedStatement.setString(6, "22/" + month + "/" + year);
            preparedStatement.setString(7, "22/" + month + "/" + year);
            if (month.matches("01|03|05|07|08|10|12")) {
                preparedStatement.setString(8, "31/" + month + "/" + year);
            } else if (month.matches("04|06|09|11")) {
                preparedStatement.setString(8, "30/" + month + "/" + year);
            } else {
                if (isLeapYear(Integer.parseInt(year))) {
                    preparedStatement.setString(8, "29/" + month + "/" + year);
                } else {
                    preparedStatement.setString(8, "28/" + month + "/" + year);
                }
            }

            preparedStatement.setString(9, "01/" + month + "/" + year);
            preparedStatement.setString(10, "08/" + month + "/" + year);
            preparedStatement.setString(11, "08/" + month + "/" + year);
            preparedStatement.setString(12, "15/" + month + "/" + year);
            preparedStatement.setString(13, "15/" + month + "/" + year);
            preparedStatement.setString(14, "22/" + month + "/" + year);
            preparedStatement.setString(15, "22/" + month + "/" + year);
            if (month.matches("01|03|05|07|08|10|12")) {
                preparedStatement.setString(16, "31/" + month + "/" + year);
            } else if (month.matches("04|06|09|11")) {
                preparedStatement.setString(16, "30/" + month + "/" + year);
            } else {
                if (isLeapYear(Integer.parseInt(year))) {
                    preparedStatement.setString(16, "29/" + month + "/" + year);
                } else {
                    preparedStatement.setString(16, "28/" + month + "/" + year);
                }
            }

            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            if (resultSet.next()) {

                double week1Created = resultSet.getInt("WEEK_1_CREATED");
                double week2Created = resultSet.getInt("WEEK_2_CREATED");
                double week3Created = resultSet.getInt("WEEK_3_CREATED");
                double week4Created = resultSet.getInt("WEEK_4_CREATED");

                double week1Closed = resultSet.getInt("WEEK_1_CLOSED");
                double week2Closed = resultSet.getInt("WEEK_2_CLOSED");
                double week3Closed = resultSet.getInt("WEEK_3_CLOSED");
                double week4Closed = resultSet.getInt("WEEK_4_CLOSED");

                double createdTotal = 0;
                double closedTotal = 0;

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("WEEK_OF_THE_MONTH_1", "01-" + monthInString.get(Integer.parseInt(month) - 1) + "-" + year);
                jsonObject.addProperty("WEEK_OF_THE_MONTH_2", "02-" + monthInString.get(Integer.parseInt(month) - 1) + "-" + year);
                jsonObject.addProperty("WEEK_OF_THE_MONTH_3", "03-" + monthInString.get(Integer.parseInt(month) - 1) + "-" + year);
                jsonObject.addProperty("WEEK_OF_THE_MONTH_4", "04-" + monthInString.get(Integer.parseInt(month) - 1) + "-" + year);
                jsonArray.add(jsonObject);

                JsonObject jsonObject1 = new JsonObject();
                jsonObject1.addProperty("WEEK_1_CREATED", week1Created);
                jsonObject1.addProperty("WEEK_2_CREATED", week2Created);
                jsonObject1.addProperty("WEEK_3_CREATED", week3Created);
                jsonObject1.addProperty("WEEK_4_CREATED", week4Created);
                createdTotal = week1Created + week2Created + week3Created + week4Created;
                jsonObject1.addProperty("CREATED_TOTAL", createdTotal);
                jsonArray.add(jsonObject1);
                JsonObject jsonObject2 = new JsonObject();
                jsonObject2.addProperty("WEEK_1_CLOSED", week1Closed);
                jsonObject2.addProperty("WEEK_2_CLOSED", week2Closed);
                jsonObject2.addProperty("WEEK_3_CLOSED", week3Closed);
                jsonObject2.addProperty("WEEK_4_CLOSED", week4Closed);
                closedTotal = week1Closed + week2Closed + week3Closed + week4Closed;
                jsonObject2.addProperty("CLOSED_TOTAL", closedTotal);
                jsonArray.add(jsonObject2);
                JsonObject jsonObject3 = new JsonObject();
                if (week1Created != 0) {
                    jsonObject3.addProperty("WEEK_1_COMP", Math.round((week1Closed / week1Created) * 100) + "%");
                } else {
                    jsonObject3.addProperty("WEEK_1_COMP", "0%");
                }
                if (week2Created != 0) {
                    jsonObject3.addProperty("WEEK_2_COMP", Math.round((week2Closed / week2Created) * 100) + "%");
                } else {
                    jsonObject3.addProperty("WEEK_2_COMP", "0%");
                }
                if (week3Created != 0) {
                    jsonObject3.addProperty("WEEK_3_COMP", Math.round((week3Closed / week3Created) * 100) + "%");
                } else {
                    jsonObject3.addProperty("WEEK_3_COMP", "0%");
                }
                if (week4Created != 0) {
                    jsonObject3.addProperty("WEEK_4_COMP", Math.round((week4Closed / week4Created) * 100) + "%");
                } else {
                    jsonObject3.addProperty("WEEK_4_COMP", "0%");
                }
                jsonObject3.addProperty("COMP_TOTAL", Math.round((closedTotal / createdTotal) * 100) + "%");
                jsonArray.add(jsonObject3);
            }

            jsonResult.add("CREATED_CLOSED_WEEKLY_REPORT", jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Get User Details Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public void setDateRange(String orgId,
            java.util.Date startDate,
            java.util.Date endDate,
            Calendar cStart,
            Calendar cEnd,
            PreparedStatement preparedStatement) throws SQLException {

        if (startDate.getMonth() == endDate.getMonth()) {
            preparedStatement.setString(1, orgId);
            preparedStatement.setString(2, startDate.getDate() + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
            preparedStatement.setString(3, endDate.getDate() + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));

            preparedStatement.setString(4, orgId);
            preparedStatement.setString(5, startDate.getDate() + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
            preparedStatement.setString(6, endDate.getDate() + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
        } else if ((cStart.get(Calendar.MONTH) + 1) == (startDate.getMonth() + 1)) {
            int i = cStart.get(Calendar.MONTH) + 1;
            String date = "";
            if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12) {
                date = "31";
            } else if (i == 4 || i == 6 || i == 9 || i == 11) {
                date = "30";
            } else {
                if (isLeapYear(cStart.get(Calendar.YEAR))) {
                    date = "29";
                } else {
                    date = "28";
                }
            }

            preparedStatement.setString(1, orgId);
            preparedStatement.setString(2, startDate.getDate() + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
            preparedStatement.setString(3, date + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));

            preparedStatement.setString(4, orgId);
            preparedStatement.setString(5, startDate.getDate() + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
            preparedStatement.setString(6, date + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
        } else if ((cStart.get(Calendar.MONTH) + 1) == (endDate.getMonth() + 1)) {
            int i = cStart.get(Calendar.MONTH) + 1;
            String date = "";
            if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12) {
                date = "31";
            } else if (i == 4 || i == 6 || i == 9 || i == 11) {
                date = "30";
            } else {
                if (isLeapYear(cStart.get(Calendar.YEAR))) {
                    date = "29";
                } else {
                    date = "28";
                }
            }

            preparedStatement.setString(1, orgId);
            preparedStatement.setString(2, 1 + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
            preparedStatement.setString(3, endDate.getDate() + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));

            preparedStatement.setString(4, orgId);
            preparedStatement.setString(5, 1 + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
            preparedStatement.setString(6, endDate.getDate() + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
        } else {
            int i = cStart.get(Calendar.MONTH) + 1;
            String date = "";
            if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12) {
                date = "31";
            } else if (i == 4 || i == 6 || i == 9 || i == 11) {
                date = "30";
            } else {
                if (isLeapYear(cStart.get(Calendar.YEAR))) {
                    date = "29";
                } else {
                    date = "28";
                }
            }

            preparedStatement.setString(1, orgId);
            preparedStatement.setString(2, 1 + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
            preparedStatement.setString(3, date + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));

            preparedStatement.setString(4, orgId);
            preparedStatement.setString(5, 1 + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
            preparedStatement.setString(6, date + "/" + (cStart.get(Calendar.MONTH) + 1) + "/" + cStart.get(Calendar.YEAR));
        }
    }

    public String getWeeklyReport(String orgId, String fromDate, String toDate, String reportName) //                String orgId,
    //            String fromDate,
    //            String toDate,
    //            String userID,
    //            String respId,
    //            String respAppId
    //         
    //    ) 
    {

        String result = null;
        String statement = "SELECT cp_week_of_month week_of_month,\n"
                + "\n"
                + "         wo_created,\n"
                + "\n"
                + "         NVL (cp_wo_completed, 0) wo_completed,\n"
                + "\n"
                + "         DECODE (wo_created,\n"
                + "\n"
                + "                 0, 0,\n"
                + "\n"
                + "                 ROUND ( (nvl(cp_wo_completed,0) / wo_created) * 100))\n"
                + "\n"
                + "            comp\n"
                + "\n"
                + "    FROM (SELECT TO_CHAR (t1.week, 'DD-Mon-YYYY') cp_week_of_month,\n"
                + "\n"
                + "                 wo_created,\n"
                + "\n"
                + "                 (SELECT count_for_week\n"
                + "\n"
                + "                    FROM (  SELECT week, COUNT (*) count_for_week\n"
                + "\n"
                + "                              FROM (SELECT jo.wip_entity_id,\n"
                + "\n"
                + "                                           TO_DATE (\n"
                + "\n"
                + "                                                   TRUNC (\n"
                + "\n"
                + "                                                        (  jo.creation_date\n"
                + "\n"
                + "                                                         - NEXT_DAY (\n"
                + "\n"
                + "                                                                TRUNC (\n"
                + "\n"
                + "                                                                   jo.creation_date,\n"
                + "\n"
                + "                                                                   'mm')\n"
                + "\n"
                + "                                                              - 8,\n"
                + "\n"
                + "                                                              'sun'))\n"
                + "\n"
                + "                                                      / 7)\n"
                + "\n"
                + "                                                 + 1\n"
                + "\n"
                + "                                              || '-'\n"
                + "\n"
                + "                                              || TO_CHAR (jo.creation_date,\n"
                + "\n"
                + "                                                          'Mon-rr'),\n"
                + "\n"
                + "                                              'DD/MM/rrrr')\n"
                + "\n"
                + "                                              week\n"
                + "\n"
                + "                                      FROM wip_discrete_jobs jo,\n"
                + "\n"
                + "                                           wip_entities en,\n"
                + "\n"
                + "                                           bom_departments bd\n"
                + "\n"
                + "                                     WHERE     jo.wip_entity_id =\n"
                + "\n"
                + "                                                  en.wip_entity_id\n"
                + "\n"
                + "                                           AND jo.owning_department =\n"
                + "\n"
                + "                                                  bd.department_id\n"
                + "\n"
                + "                                           AND jo.organization_id =\n"
                + "\n"
                + orgId + " \n"
                + "\n"
                + "                                           AND jo.plan_maintenance  not in('Y')\n"
                + "\n"
                + "                                           AND jo.wip_entity_id IN\n"
                + "\n"
                + "                                                  (SELECT wip_entity_id\n"
                + "\n"
                + "                                                     FROM XXX_MAX_WO_APPROVER_TEMP_T v\n"
                + "\n"
                + "                                                    WHERE     wip_entity_id =\n"
                + "\n"
                + "                                                                 jo.wip_entity_id\n"
                + "\n"
                + "                                                          AND rows_id =\n"
                + "\n"
                + "                                                                 (SELECT MAX (\n"
                + "\n"
                + "                                                                            rows_id)\n"
                + "\n"
                + "                                                                    FROM XXX_MAX_WO_APPROVER_TEMP_T\n"
                + "\n"
                + "                                                                   WHERE wip_entity_id =\n"
                + "\n"
                + "                                                                            v.wip_entity_id)\n"
                + "\n"
                + "                                                          AND seq_num = '3.3'\n"
                + "\n"
                + "                                                          AND approval_status IN\n"
                + "\n"
                + "                                                                 ('APPROVED'))\n"
                + "\n"
                + "                                    UNION\n"
                + "\n"
                + "                                    SELECT jo.wip_entity_id,\n"
                + "\n"
                + "                                           TO_DATE (\n"
                + "\n"
                + "                                                   TRUNC (\n"
                + "\n"
                + "                                                        (  jo.creation_date\n"
                + "\n"
                + "                                                         - NEXT_DAY (\n"
                + "\n"
                + "                                                                TRUNC (\n"
                + "\n"
                + "                                                                   jo.creation_date,\n"
                + "\n"
                + "                                                                   'mm')\n"
                + "\n"
                + "                                                              - 8,\n"
                + "\n"
                + "                                                              'sun'))\n"
                + "\n"
                + "                                                      / 7)\n"
                + "\n"
                + "                                                 + 1\n"
                + "\n"
                + "                                              || '-'\n"
                + "\n"
                + "                                              || TO_CHAR (jo.creation_date,\n"
                + "\n"
                + "                                                          'Mon-rr'),\n"
                + "\n"
                + "                                              'DD/MM/rrrr')\n"
                + "\n"
                + "                                              week\n"
                + "\n"
                + "                                      FROM wip_discrete_jobs jo,\n"
                + "\n"
                + "                                           wip_entities en,\n"
                + "\n"
                + "                                           bom_departments bd\n"
                + "\n"
                + "                                     WHERE     jo.wip_entity_id =\n"
                + "\n"
                + "                                                  en.wip_entity_id\n"
                + "\n"
                + "                                           AND jo.owning_department =\n"
                + "\n"
                + "                                                  bd.department_id\n"
                + "\n"
                + "                                           AND jo.plan_maintenance not in('Y')\n"
                + "\n"
                + "                                           AND jo.organization_id =\n"
                + "\n"
                + orgId + " \n"
                + "\n"
                + "                                           AND jo.wip_entity_id IN\n"
                + "\n"
                + "                                                  (SELECT wip_entity_id\n"
                + "\n"
                + "                                                     FROM XXX_MAX_WO_APPROVER_TEMP_T v\n"
                + "\n"
                + "                                                    WHERE     wip_entity_id =\n"
                + "\n"
                + "                                                                 jo.wip_entity_id\n"
                + "\n"
                + "                                                          AND rows_id =\n"
                + "\n"
                + "                                                                 (SELECT MAX (\n"
                + "\n"
                + "                                                                            rows_id)\n"
                + "\n"
                + "                                                                    FROM XXX_MAX_WO_APPROVER_TEMP_T\n"
                + "\n"
                + "                                                                   WHERE wip_entity_id =\n"
                + "\n"
                + "                                                                            v.wip_entity_id)\n"
                + "\n"
                + "                                                          AND seq_num = '3.1'\n"
                + "\n"
                + "                                                          AND approval_status IN\n"
                + "\n"
                + "                                                                 ('APPROVED'))) t2\n"
                + "\n"
                + "                          GROUP BY t2.week) cnt\n"
                + "\n"
                + "                   WHERE cnt.week = t1.week)\n"
                + "\n"
                + "                    cp_wo_completed\n"
                + "\n"
                + "            FROM (  SELECT TO_DATE (\n"
                + "\n"
                + "                                   TRUNC (\n"
                + "\n"
                + "                                        (  jo.creation_date\n"
                + "\n"
                + "                                         - NEXT_DAY (\n"
                + "\n"
                + "                                              TRUNC (jo.creation_date, 'mm') - 8,\n"
                + "\n"
                + "                                              'sun'))\n"
                + "\n"
                + "                                      / 7)\n"
                + "\n"
                + "                                 + 1\n"
                + "\n"
                + "                              || '-'\n"
                + "\n"
                + "                              || TO_CHAR (jo.creation_date, 'Mon-rr'),\n"
                + "\n"
                + "                              'DD/MM/rrrr')\n"
                + "\n"
                + "                              week,\n"
                + "\n"
                + "                           COUNT (*) wo_created\n"
                + "\n"
                + "                      FROM wip_discrete_jobs jo,\n"
                + "\n"
                + "                           wip_entities en,\n"
                + "\n"
                + "                           bom_departments bd\n"
                + "\n"
                + "                     WHERE     jo.wip_entity_id = en.wip_entity_id\n"
                + "\n"
                + "                           AND jo.owning_department = bd.department_id\n"
                + "\n"
                + "                           AND jo.organization_id =" + orgId + " \n"
                + "\n"
                + "                           AND TRUNC (jo.creation_date) BETWEEN TO_DATE (\n'"
                + "\n"
                + fromDate + "',\n"
                + "\n"
                + "                                                                   'DD/MM/RRRR')\n"
                + "\n"
                + "                                                            AND TO_DATE (\n'"
                + "\n"
                + toDate + "',\n"
                + "\n"
                + "                                                                   'DD/MM/RRRR')\n"
                + "\n"
                + "                           AND jo.plan_maintenance not in ('Y')\n"
                + "\n"
                + "                           AND jo.wip_entity_id NOT IN\n"
                + "\n"
                + "                                  (SELECT wip_entity_id\n"
                + "\n"
                + "                                     FROM xxx_stc_work_order_approver_v\n"
                + "\n"
                + "                                    WHERE     wip_entity_id = jo.wip_entity_id\n"
                + "\n"
                + "                                          AND approval_status = 'CANCELED')\n"
                + "\n"
                + "                  GROUP BY TO_DATE (\n"
                + "\n"
                + "                                   TRUNC (\n"
                + "\n"
                + "                                        (  jo.creation_date\n"
                + "\n"
                + "                                         - NEXT_DAY (\n"
                + "\n"
                + "                                                TRUNC (jo.creation_date, 'mm')\n"
                + "\n"
                + "                                              - 8,\n"
                + "\n"
                + "                                              'sun'))\n"
                + "\n"
                + "                                      / 7)\n"
                + "\n"
                + "                                 + 1\n"
                + "\n"
                + "                              || '-'\n"
                + "\n"
                + "                              || TO_CHAR (jo.creation_date, 'Mon-rr'),\n"
                + "\n"
                + "                              'DD/MM/rrrr')) t1) order by 1";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            System.out.println("/////////////////////////////////");
            System.out.println(reportName);
            System.out.println("/////////////////////////////////");
            param.put("P_REPORT_NAME", reportName);
//            param.put("P_USER_ID", userID);
//            param.put("P_RESP_ID", respId);
//            param.put("P_RESP_APPL_ID", respAppId);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "WEEKLY_REPORT");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("WEEKLY_REPORT");

            JSONObject jsonResult = new JSONObject();
            JSONArray arrayResult = new JSONArray();

            jsonResult.put("WEEKLY_REPORT", jsonArray);

            result = jsonResult.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("SOA Error", e);
            logger.error("QUERY : " + statement);
        }
        System.out.println("//getWorkRequest Return Result");
        System.out.println(result);

        return result;
    }

    void closeAllConnection(Connection connection,
            PreparedStatement preparedStatement,
            ResultSet resultSet) {

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }
    }

    public String claimEntryStatus(String period, String area) {
        String result = null;
        String statement = "SELECT org.name,\n"
                + "        org.organization_id,\n"
                + "        (SELECT MEANING\n"
                + "           FROM FND_LOOKUP_VALUES_VL\n"
                + "          WHERE     lookup_type = 'STC_CLAIMS_STATUS'\n"
                + "                AND (LOOKUP_TYPE LIKE 'STC_CLAIMS_STATUS')\n"
                + "                AND LOOKUP_CODE =\n"
                + "                       NVL (\n"
                + "                          (SELECT DECODE (entry_status_d,\n"
                + "                                          'Claim Entered', '01',\n"
                + "                                          '06', '01',\n"
                + "                                          '02')\n"
                + "                             FROM xxx_stc_period_plans_v d, qa_plans qp\n"
                + "                            WHERE     d.period_name = :p_period\n"
                + "                                  AND d.organization_id = org.organization_id\n"
                + "                                  AND d.plan_id = qp.plan_id\n"
                + "                                  AND qp.attribute1 = '1'),\n"
                + "                          '02'))\n"
                + "           cp_elmech_clm_status,\n"
                + "       (SELECT MEANING\n"
                + "           FROM FND_LOOKUP_VALUES_VL\n"
                + "          WHERE     lookup_type = 'STC_CLAIMS_STATUS'\n"
                + "                AND (LOOKUP_TYPE LIKE 'STC_CLAIMS_STATUS')\n"
                + "                AND LOOKUP_CODE = NVL (\n"
                + "           (SELECT DECODE (entry_status_d,\n"
                + "                           'Claim Entered', '01',\n"
                + "                           '06', '01',\n"
                + "                           '02')\n"
                + "              FROM xxx_stc_period_plans_v d, qa_plans qp\n"
                + "             WHERE     d.period_name = :p_period\n"
                + "                   AND d.organization_id = org.organization_id\n"
                + "                   AND d.plan_id = qp.plan_id\n"
                + "                   AND qp.attribute1 = '2'),\n"
                + "           '02'))\n"
                + "           cp_agrclean_clm_status,\n"
                + "        (SELECT MEANING\n"
                + "           FROM FND_LOOKUP_VALUES_VL\n"
                + "          WHERE     lookup_type = 'STC_CLAIMS_STATUS'\n"
                + "                AND (LOOKUP_TYPE LIKE 'STC_CLAIMS_STATUS')\n"
                + "                AND LOOKUP_CODE = NVL (\n"
                + "           (SELECT DECODE (entry_status_d,\n"
                + "                           'Claim Entered', '01',\n"
                + "                           '06', '01',\n"
                + "                           '02')\n"
                + "              FROM xxx_stc_period_plans_v d, qa_plans qp\n"
                + "             WHERE     d.period_name = :p_period\n"
                + "                   AND d.organization_id = org.organization_id\n"
                + "                   AND d.plan_id = qp.plan_id\n"
                + "                   AND qp.attribute1 = '4'),\n"
                + "           '02'))\n"
                + "           cp_soudi_clm_status,\n"
                + "        (SELECT MEANING\n"
                + "           FROM FND_LOOKUP_VALUES_VL\n"
                + "          WHERE     lookup_type = 'STC_CLAIMS_STATUS'\n"
                + "                AND (LOOKUP_TYPE LIKE 'STC_CLAIMS_STATUS')\n"
                + "                AND LOOKUP_CODE = NVL (\n"
                + "           (SELECT DECODE (entry_status_d,\n"
                + "                           'Claim Entered', '01',\n"
                + "                           '06', '01',\n"
                + "                           '02')\n"
                + "              FROM xxx_stc_period_plans_v d, qa_plans qp\n"
                + "             WHERE     d.period_name = :p_period\n"
                + "                   AND d.organization_id = org.organization_id\n"
                + "                   AND d.plan_id = qp.plan_id\n"
                + "                   AND qp.attribute1 = '3'),\n"
                + "           '02') )\n"
                + "           cp_water_clm_status,\n"
                + "       (SELECT MEANING\n"
                + "           FROM FND_LOOKUP_VALUES_VL\n"
                + "          WHERE     lookup_type = 'STC_CLAIMS_STATUS'\n"
                + "                AND (LOOKUP_TYPE LIKE 'STC_CLAIMS_STATUS')\n"
                + "                AND LOOKUP_CODE = NVL (\n"
                + "           (SELECT DECODE (entry_status_d,\n"
                + "                           'Claim Entered', '01',\n"
                + "                           '06', '01',\n"
                + "                           '02')\n"
                + "              FROM xxx_stc_period_plans_v d, qa_plans qp\n"
                + "             WHERE     d.period_name = :p_period\n"
                + "                   AND d.organization_id = org.organization_id\n"
                + "                   AND d.plan_id = qp.plan_id\n"
                + "                   AND qp.attribute1 = '5'),\n"
                + "           '02'))\n"
                + "           cp_performance_clm_status,\n"
                + "        (SELECT MEANING\n"
                + "           FROM FND_LOOKUP_VALUES_VL\n"
                + "          WHERE     lookup_type = 'STC_CLAIMS_STATUS'\n"
                + "                AND (LOOKUP_TYPE LIKE 'STC_CLAIMS_STATUS')\n"
                + "                AND LOOKUP_CODE = NVL (\n"
                + "           (SELECT DECODE (approval_status,\n"
                + "                           'APPROVED', '05',\n"
                + "                           'IN_PROCESS', '03',\n"
                + "                           '04')\n"
                + "              FROM xxx_stc_claims_approval_hdr h\n"
                + "             WHERE     h.period_name = :p_period\n"
                + "                   AND h.organization_id = org.organization_id),\n"
                + "           '04'))\n"
                + "           CP_APPROVAL_STATUS\n"
                + "   FROM hr_all_organization_units org\n"
                + "  WHERE org.attribute16 = :p_area";

        String a = "select *  FROM XXX_STC_PERIOD_PLANS_V_test  where rownum = 1";

        int columnCount;
        String columnName;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, period);
            preparedStatement.setString(2, period);
            preparedStatement.setString(3, period);
            preparedStatement.setString(4, period);
            preparedStatement.setString(5, period);
            preparedStatement.setString(6, period);
            preparedStatement.setString(7, area);
            System.out.println(statement);
            resultSet = preparedStatement.executeQuery();
            resultSetMetaData = resultSet.getMetaData();
            columnCount = resultSetMetaData.getColumnCount();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                for (int i = 1; i <= columnCount; i++) {
                    columnName = resultSetMetaData.getColumnName(i);
                    jsonObject.addProperty(columnName, resultSet.getString(columnName));

                }
                jsonArray.add(jsonObject);
            }

            jsonResult.add("CLAIM_ENTRY_STATUS", jsonArray);
            result = jsonResult.toString();
            System.out.println(result);

            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("claimEntryStatus Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String claimEntryStatusAreaLOV() {
        String result = null;
        String statement = "SELECT distinct  attribute16 ARIA FROM hr_all_organization_units WHERE attribute16 IS NOT NULL AND attribute16 <> 'Y'";
//      }

        int columnCount;
        String columnName;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();
            resultSetMetaData = resultSet.getMetaData();
            columnCount = resultSetMetaData.getColumnCount();
            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                for (int i = 1; i <= columnCount; i++) {
                    columnName = resultSetMetaData.getColumnName(i);
                    jsonObject.addProperty(columnName, resultSet.getString(columnName));
                    System.out.println(resultSet.getString(columnName));

                }
                jsonArray.add(jsonObject);
            }

            jsonResult.add("CLAIM_ENTRY_STATUS_AREA_LOV", jsonArray);
            result = jsonResult.toString();
            System.out.println(result);

            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException e) {
            logger.error("claimEntryStatus Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public String claimEntryStatusPeriodLOV() {
        String result = null;
        String statement = "select LOOKUP_CODE,MEANING from FND_LOOKUP_VALUES where LOOKUP_TYPE = 'EAM_CALENDAR' and language ='US' order by to_date(lookup_code,'dd-yyyy')";

        int columnCount;
        String columnName;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();
            resultSetMetaData = resultSet.getMetaData();
            columnCount = resultSetMetaData.getColumnCount();
            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                for (int i = 1; i <= columnCount; i++) {
                    columnName = resultSetMetaData.getColumnName(i);
                    jsonObject.addProperty(columnName, resultSet.getString(columnName));
                    System.out.println(resultSet.getString(columnName));

                }
                jsonArray.add(jsonObject);
            }

            jsonResult.add("CLAIM_ENTRY_STATUS_PERIOD_LOV", jsonArray);
            result = jsonResult.toString();
            System.out.println(result);

            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException e) {
            logger.error("claimEntryStatus Error", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet);
        }
        return result;
    }

    public static void main(String args[]) throws UnsupportedEncodingException {
        StatisticReportController a = new StatisticReportController();

        a.claimEntryStatus("81", "مشروع المنطقه الغربية");
//        a.claimEntryStatusAreaLOV();

//        String d= "??????";
//       byte[] utf8bytes = d.getBytes();
//       String statement = new String(utf8bytes, "UTF-8");
//       String arabicString = new String("??? ????".getBytes(), "UTF-8");
//       System.out.println(arabicString);
//        a.getWeeklyReport("9401","02/01/2019","31/01/2019", "XXWOCOMRP");
//        
////a.summaryMaintinaceHQC("4-2019",  "4-2019", 9401, "2");
//    }
//a.summaryMaintinaceHQC("4-2019",  "4-2019", 9401, "2");
    }

}
