/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.NotificationController;
import com.stc.controller.WmsNotificationController;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
/**
 *
 * @author lenovo
 */
@Path("notification")
public class WmsNotificationService {
    //**********************************approve request*******************************//
    @GET
    @Path("Approve")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response ApproveRquest(@QueryParam("requestId")String requestId ,@QueryParam("comment") String comment) {

        if (requestId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WmsNotificationController notificationController = new WmsNotificationController();
            result = notificationController.ApproveAction(requestId ,comment);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
   //**********************************Reject request*******************************//
    @GET
    @Path("Reject")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response RejectRquest(@QueryParam("requestId")String requestId ,@QueryParam("comment")String comment) {

        if (requestId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WmsNotificationController notificationController = new WmsNotificationController();
            result = notificationController.RejectAction(requestId,comment);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
    //**********************************Cancle Request*****************************************//
    @GET
    @Path("Cancle")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response CancleRquest(@QueryParam("requestId")String requestId) {

        if (requestId.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WmsNotificationController notificationController = new WmsNotificationController();
            result = notificationController.CancelRequest(requestId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    } 
    
     //**********************************Reassign Request*****************************************//
     @GET
    @Path("Reassign")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response ReassignRquest(@QueryParam("requestid") String requestid,@QueryParam("comment")String comment,@QueryParam("username") String username) {

        if (requestid.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WmsNotificationController notificationController = new WmsNotificationController();
            result = notificationController.ReAssignAction(requestid ,comment,username);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    //**********************************Reassign Request*****************************************//
     @GET
    @Path("ReassignUsers")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response ReassignUsers() {

       
       
            String result;
            WmsNotificationController notificationController = new WmsNotificationController();
            result = notificationController.getUsersReassign();

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            
        }
    }
    
    //**********************************get Notification request*******************************//
    @GET
    @Path("getAllNotification")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response GetNotification(@QueryParam("userName")String userName) {     
            String result;
            WmsNotificationController notificationController = new WmsNotificationController();
            result = notificationController.getAllNotificationForUserLogin(userName);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
     //**********************************get more infromation About Notification request*******************************//
    @GET
    @Path("GetNTFDetails/{requestId}")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response GetMoreInformationAboutNotification(@PathParam("requestId") String requestId) {     
            String result;
            WmsNotificationController notificationController = new WmsNotificationController();
            result = notificationController.getMoreDetailsOfNotification(requestId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    @GET
    @Path("GetNTFCount")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response GetNotificationCount(@QueryParam("userName") String userName) {     
            String result;
            WmsNotificationController notificationController = new WmsNotificationController();
            result = notificationController.NotificationCount(userName);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    
    
}
