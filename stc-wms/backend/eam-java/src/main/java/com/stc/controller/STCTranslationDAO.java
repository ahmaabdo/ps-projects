/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import static com.stc.controller.WorkOrderController.logger;
import static com.stc.controller.WorkOrderController.pool;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.objects.TranslationBean;
import com.stc.soap.SoapHandler;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author lenovo
 */
public class STCTranslationDAO {
    

    public String AddTranslation(TranslationBean translationBean){
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_material_requirements/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_material_requirements/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("WORDNAME", String.valueOf(translationBean.getWordName()));
            param.put("APPLICATIONNAME", String.valueOf(translationBean.getApplicationName()));
            param.put("englishValue", String.valueOf(translationBean.getEnglishValue()));
            param.put("arabicValue", String.valueOf(translationBean.getArabicValue()));
            
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Material Error", e);
        }
        return result;
    }
    
    
}
