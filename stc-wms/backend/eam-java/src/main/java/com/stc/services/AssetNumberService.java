/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.AssetNumberController;
import com.stc.controller.MainController;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Anas Alghawi
 */
@Path("/assetNumber")
public class AssetNumberService {

    @GET
    @Path("getBuildingsList")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getBuildingsList(@QueryParam("organizationID") String organizationID) {

        if (organizationID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            AssetNumberController assetNumberController = new AssetNumberController();
            result = assetNumberController.getBuildingsList(organizationID);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    @GET
    @Path("getFloorsList")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getFloorsList(@QueryParam("buildingID") String buildingID) {

        if (buildingID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            AssetNumberController assetNumberController = new AssetNumberController();
            result = assetNumberController.getFloorsList(buildingID);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    
    @GET
    @Path("getRoomsList")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getRoomsList(@QueryParam("floorID") String floorID) {

        if (floorID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            AssetNumberController assetNumberController = new AssetNumberController();
            result = assetNumberController.getRoomsList(floorID);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    
    @GET
    @Path("getAssetsList")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAssetsList(@QueryParam("roomID") String roomID) {

        if (roomID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            AssetNumberController assetNumberController = new AssetNumberController();
            result = assetNumberController.getAssetsList(roomID);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

//    public static void main(String args[]) {
//        AssetNumberService a = new AssetNumberService();
////        a.getBuildingsList("9401");
//        a.getAssetsList("23079");
//    }
}
