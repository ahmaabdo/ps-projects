package com.stc.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import static com.stc.controller.WorkOrderController.logger;
import static com.stc.controller.WorkOrderController.pool;
import com.stc.db.ConnectionPool;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 04/01/18
 * Time: 15:23
 * To change this template use File | Settings | File Templates.
 */
public class AttachmentController {
    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public PreparedStatement preparedStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;

    final static Logger logger = Logger.getLogger(AttachmentController.class);

    public String getTextAttachment(String dataType, String mediaId) {

        String result = null;
        String statement = null;

        if (dataType.equalsIgnoreCase("shortText")) {
            statement = "SELECT * FROM APPS.FND_DOCUMENTS_SHORT_TEXT WHERE MEDIA_ID = ?";
        }
        else if (dataType.equalsIgnoreCase("longText")) {
            statement = "SELECT * FROM APPS.FND_DOCUMENTS_LONG_TEXT WHERE MEDIA_ID = ?";
        }
        else {
            return "";
        }

        try {
            connection  = pool.getConnection();


            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, mediaId);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonObject = new JsonObject();

            if (resultSet.next()){


                if (dataType.equalsIgnoreCase("shortText")) {
                    String text = resultSet.getString(2);
                    jsonObject.addProperty("TEXT", text);
                }
                else if (dataType.equalsIgnoreCase("longText")) {
                    String longText = resultSet.getString(3);
                    jsonObject.addProperty("TEXT", longText);
                }
            }

            result = jsonObject.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Text Attachment Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public Blob getFileAttachment(String fileId) {

        Blob blob = null;

        String statement = "select FILE_NAME, FILE_DATA \n" +
                "FROM APPS.FND_LOBS \n" +
                "WHERE FILE_ID = ?";
        try {
            connection  = pool.getConnection();


            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, fileId);

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                blob = resultSet.getBlob(2);
            }
            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get File Attachment Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return blob;
    }

    public String getAttachmentList(String requestType, String pkValue1, String pkValue2, String lang) {
        String result = null;

        String statement = "SELECT FDV.TITLE, FDV.DATATYPE_ID, FDV.DATATYPE_NAME, FDV.FILE_NAME, FDV.DESCRIPTION, (SELECT DISTINCT FDCT.USER_NAME FROM APPS.FND_DOCUMENT_CATEGORIES_TL FDCT \n" +
                "WHERE FDCT.CATEGORY_ID = FAD.CATEGORY_ID AND FDCT.language = ? AND ROWNUM<=1) AS CATEGORY,FAD.CATEGORY_ID, FAD.ENTITY_NAME, FAD.ATTACHED_DOCUMENT_ID, FAD.PK1_VALUE,FAD.PK2_VALUE,FAD.PK3_VALUE,FAD.PK4_VALUE,FAD.PK5_VALUE, " +
                "FAD.LAST_UPDATED_BY, TO_CHAR(FAD.LAST_UPDATE_DATE,'DD-MON-YYYY hh:mmAM') FORMATED_LAST_UPDATE_DATE, FAD.LAST_UPDATE_DATE, FDV.USAGE_TYPE_DESCR, FDV.MEDIA_ID, FDV.URL, FDV.DOCUMENT_ID, "
                + "(SELECT (CASE WHEN file_data IS NULL THEN NULL ELSE file_data END) FROM fnd_lobs WHERE file_id = fdv.media_id) AS ATTACHMENTS, (SELECT file_content_type FROM fnd_lobs WHERE file_id = fdv.media_id) AS fileType, (SELECT short_text FROM applsys.fnd_documents_short_text WHERE media_id = fdv.media_id) AS short_text \n" +
                "FROM APPS.FND_ATTACHED_DOCUMENTS FAD, APPS.FND_DOCUMENTS_VL FDV\n" +
                "WHERE FAD.DOCUMENT_ID = FDV.DOCUMENT_ID ";

        if (requestType.equalsIgnoreCase("WR")) {
            statement += " AND PK1_VALUE =? AND entity_name='EAM_WORK_REQUESTS' ";
        }
        else if (requestType.equalsIgnoreCase("WO")) {
            statement += " AND PK2_VALUE =? AND entity_name='EAM_WORK_ORDERS' ";
        }
        else if (requestType.equalsIgnoreCase("OP")) {
            statement += " AND PK1_VALUE =? AND PK2_VALUE =? ";
        }
        else {
            return "";
        }
        
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
             System.out.println(statement);
            if (requestType.equalsIgnoreCase("WR")) {
                preparedStatement.setString(2, pkValue1);
            }
            else if (requestType.equalsIgnoreCase("WO")) {
                preparedStatement.setString(2, pkValue2);
            }
            else if (requestType.equalsIgnoreCase("OP")) {
                preparedStatement.setString(2, pkValue1);
                preparedStatement.setString(3, pkValue2);
            }

            preparedStatement.setString(1, lang);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ATTACHED_DOCUMENT_ID", resultSet.getString("ATTACHED_DOCUMENT_ID"));
                jsonObject.addProperty("DOCUMENT_ID", resultSet.getString("DOCUMENT_ID"));
                jsonObject.addProperty("MEDIA_ID", resultSet.getString("MEDIA_ID"));
                if(resultSet.getString("DATATYPE_NAME").equals("Web Page")) {
                    jsonObject.addProperty("VALUE", resultSet.getString("URL"));
                } else if (resultSet.getString("DATATYPE_NAME").equals("File")) {
                    jsonObject.addProperty("VALUE",resultSet.getString("FILE_NAME"));
                } else if(resultSet.getString("DATATYPE_NAME").equals("Short Text")) {
                    jsonObject.addProperty("VALUE",resultSet.getString("SHORT_TEXT"));
                }
                jsonObject.addProperty("TITLE",resultSet.getString("TITLE"));
                jsonObject.addProperty("DATATYPE_ID",resultSet.getString("DATATYPE_ID"));
                jsonObject.addProperty("DATATYPE_NAME",resultSet.getString("DATATYPE_NAME"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                jsonObject.addProperty("ENTITY_NAME",resultSet.getString("ENTITY_NAME"));
                jsonObject.addProperty("CATEGORY",resultSet.getString("CATEGORY"));
                jsonObject.addProperty("CATEGORY_ID",resultSet.getString("CATEGORY_ID"));
                jsonObject.addProperty("LAST_UPDATED_BY", resultSet.getString("LAST_UPDATED_BY"));
                jsonObject.addProperty("LAST_UPDATE_DATE", resultSet.getString("LAST_UPDATE_DATE"));
                jsonObject.addProperty("USAGE_TYPE_DESCR", resultSet.getString("USAGE_TYPE_DESCR"));
                jsonObject.addProperty("URL", resultSet.getString("URL"));
                jsonObject.addProperty("PK1_VALUE", resultSet.getString("PK1_VALUE"));
                jsonObject.addProperty("PK2_VALUE", resultSet.getString("PK2_VALUE"));
                jsonObject.addProperty("PK3_VALUE", resultSet.getString("PK3_VALUE"));
                jsonObject.addProperty("PK4_VALUE", resultSet.getString("PK4_VALUE"));
                jsonObject.addProperty("PK5_VALUE", resultSet.getString("PK5_VALUE"));
                jsonObject.addProperty("ATTACHMENTS", resultSet.getString("ATTACHMENTS"));
                jsonObject.addProperty("FileType", resultSet.getString("fileType"));
                jsonObject.addProperty("FORMATED_LAST_UPDATE_DATE", resultSet.getString("FORMATED_LAST_UPDATE_DATE"));

                jsonArray.add(jsonObject);
            }

            jsonResult.add("ATTACHMENTS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
            
            System.out.println("getAttachmentList result: \n"+result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Attachment List Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getAttachmentDetails(int documentId, String language) {
        String result = null;

        String statement = "SELECT FDV.TITLE, FDV.DATATYPE_ID, FDV.DATATYPE_NAME, FDV.DESCRIPTION, (SELECT DISTINCT FDCT.USER_NAME FROM APPS.FND_DOCUMENT_CATEGORIES_TL FDCT \n" +
                "WHERE FDCT.CATEGORY_ID = FAD.CATEGORY_ID AND FDCT.Language =? ANd RowNum <=1) AS CATEGORY,FAD.CATEGORY_ID, FAD.ENTITY_NAME, FAD.ATTACHED_DOCUMENT_ID, FAD.PK1_VALUE,FAD.PK2_VALUE,FAD.PK3_VALUE,FAD.PK4_VALUE,FAD.PK5_VALUE, " +
                "FAD.LAST_UPDATED_BY, FAD.LAST_UPDATE_DATE, FDV.USAGE_TYPE_DESCR, FDV.MEDIA_ID, FDV.URL, FDV.DOCUMENT_ID, (SELECT FL.FILE_NAME FROM APPS.FND_LOBS FL WHERE FL.FILE_ID = FDV.MEDIA_ID) AS FILE_NAME\n" +
                "FROM APPS.FND_ATTACHED_DOCUMENTS FAD, APPS.FND_DOCUMENTS_VL FDV\n" +
                "WHERE FAD.DOCUMENT_ID = FDV.DOCUMENT_ID AND FDV.DOCUMENT_ID = ?";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setString(1, language);
            preparedStatement.setInt(2, documentId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ATTACHED_DOCUMENT_ID", resultSet.getString("ATTACHED_DOCUMENT_ID"));
                jsonObject.addProperty("DOCUMENT_ID", resultSet.getString("DOCUMENT_ID"));
                jsonObject.addProperty("MEDIA_ID", resultSet.getString("MEDIA_ID"));
                jsonObject.addProperty("FILE_NAME", resultSet.getString("FILE_NAME"));
                jsonObject.addProperty("TITLE",resultSet.getString("TITLE"));
                jsonObject.addProperty("DATATYPE_ID",resultSet.getString("DATATYPE_ID"));
                jsonObject.addProperty("DATATYPE_NAME",resultSet.getString("DATATYPE_NAME"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                jsonObject.addProperty("ENTITY_NAME",resultSet.getString("ENTITY_NAME"));
                jsonObject.addProperty("CATEGORY",resultSet.getString("CATEGORY"));
                jsonObject.addProperty("CATEGORY_ID",resultSet.getString("CATEGORY_ID"));
                jsonObject.addProperty("LAST_UPDATED_BY", resultSet.getString("LAST_UPDATED_BY"));
                jsonObject.addProperty("LAST_UPDATE_DATE", resultSet.getString("LAST_UPDATE_DATE"));
                jsonObject.addProperty("USAGE_TYPE_DESCR", resultSet.getString("USAGE_TYPE_DESCR"));
                jsonObject.addProperty("URL", resultSet.getString("URL"));
                jsonObject.addProperty("PK1_VALUE", resultSet.getString("PK1_VALUE"));
                jsonObject.addProperty("PK2_VALUE", resultSet.getString("PK2_VALUE"));
                jsonObject.addProperty("PK3_VALUE", resultSet.getString("PK3_VALUE"));
                jsonObject.addProperty("PK4_VALUE", resultSet.getString("PK4_VALUE"));
                jsonObject.addProperty("PK5_VALUE", resultSet.getString("PK5_VALUE"));

                jsonArray.add(jsonObject);
            }

            jsonResult.add("ATTACHMENT_DETAILS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Attachment Details Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getCatalogAttachment(String title) {
        String result = null;

        String statement = "SELECT DISTINCT FDV.TITLE, FDV.DATATYPE_NAME, FDV.DESCRIPTION, (SELECT DISTINCT FDCT.USER_NAME FROM APPS.FND_DOCUMENT_CATEGORIES_TL FDCT \n" +
                "WHERE FDCT.CATEGORY_ID = FAD.CATEGORY_ID) AS CATEGORY, FAD.LAST_UPDATED_BY, FAD.LAST_UPDATE_DATE, FDV.USAGE_TYPE_DESCR, FDV.MEDIA_ID, FDV.URL, FDV.DOCUMENT_ID\n" +
                "FROM APPS.FND_ATTACHED_DOCUMENTS FAD, APPS.FND_DOCUMENTS_VL FDV\n" +
                "WHERE FAD.DOCUMENT_ID = FDV.DOCUMENT_ID AND FDV.USAGE_TYPE = 'S' AND FDV.SECURITY_TYPE IN (2,4) AND FDV.PUBLISH_FLAG = 'Y' ";

        if (title != null && !title.isEmpty()) {
            statement += "AND FDV.TITLE LIKE ?";
        }

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);

            if (title != null && !title.isEmpty()){
                preparedStatement.setString(1, "%" + title + "%");
            }
            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("TITLE",resultSet.getString("TITLE"));
                jsonObject.addProperty("DATATYPE_NAME",resultSet.getString("DATATYPE_NAME"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                jsonObject.addProperty("CATEGORY",resultSet.getString("CATEGORY"));
                jsonObject.addProperty("LAST_UPDATED_BY", resultSet.getString("LAST_UPDATED_BY"));
                jsonObject.addProperty("LAST_UPDATE_DATE", resultSet.getString("LAST_UPDATE_DATE"));
                jsonObject.addProperty("USAGE_TYPE_DESCR", resultSet.getString("USAGE_TYPE_DESCR"));
                jsonObject.addProperty("MEDIA_ID", resultSet.getString("MEDIA_ID"));
                jsonObject.addProperty("URL", resultSet.getString("URL"));
                jsonObject.addProperty("DOCUMENT_ID", resultSet.getString("DOCUMENT_ID"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("CATALOG_ATTACHMENTS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Catalog Attachment Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    /**
     * USING SOA GATEWAY
     * @param file
     * @param filename
     * @param contentType
     * @param title
     * @param description
     * @param categoryId
     * @param dataType
     * @param text
     * @param url
     * @param entityName
     * @param pkValue1
     * @param pkValue2
     * @param pkValue3
     * @param userId
     * @return
     */
    public String addAttachment(InputStream file,
                                String filename,
                                String contentType,
                                String title,
                                String description,
                                String categoryId,
                                String dataType,
                                String text,
                                String url,
                                String entityName,
                                String pkValue1,
                                String pkValue2,
                                String pkValue3,
                                String userId) {

        String result = "";
        String base64 = null;
        if (file != null) {
            try {
                byte[] imageBytes = IOUtils.toByteArray(file);
                file.read(imageBytes, 0, imageBytes.length);
                file.close();
                base64 = Base64.encodeBase64String(imageBytes);
                dataType="6";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/add_attachment/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/xxstc_eam_custom_api/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/add_attachment/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("INPUT_FILE", base64);
            param.put("CONTENT_TYPE", contentType);
            param.put("SEQ_NUM", "1");
            param.put("CATEGORY_ID", categoryId);
            param.put("DOCUMENT_DESCRIPTION", description);
            param.put("DATATYPE_ID", dataType);
            param.put("TEXT", text);
            param.put("FILE_NAME", filename);
            param.put("URL", url);
            param.put("ENTITY_NAME", entityName);
            param.put("PK1_VALUE", pkValue1);
            param.put("PK2_VALUE", pkValue2);
            param.put("PK3_VALUE", pkValue3);
            param.put("USER_ID", userId);
            param.put("TITLE", title);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Add Attachment Error",e);
        }
        return result;
    }

    /**
     *
     * @param category_id
     * @param document_description
     * @param text
     * @param p_title
     * @param p_file_name
     * @param p_url
     * @param attached_document_id
     * @param dataType
     * @param entity_name
     * @param pk1_value
     * @param pk2_value
     * @param pk3_value
     * @param media_id
     * @param user_id
     * @param p_content_type
     * @param p_file_data
     * @return
     */
    public String updateAttachment(
            String category_id,
            String document_description,
            String text,
            String p_title,
            String p_file_name,
            String p_url,
            String attached_document_id,
            String dataType,
            String entity_name,
            String pk1_value,
            String pk2_value,
            String pk3_value,
            String media_id,
            String user_id,
            String p_content_type,
            InputStream p_file_data
    ) {

        String result = "";
        String base64 = null;
        if (p_file_data != null) {
            try {
                byte[] imageBytes = IOUtils.toByteArray(p_file_data);
                p_file_data.read(imageBytes, 0, imageBytes.length);
                p_file_data.close();
                base64 = Base64.encodeBase64String(imageBytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/update_attachment/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/xxstc_eam_custom_api/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/update_attachment/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("SEQ_NUM", "1");
            param.put("CATEGORY_ID", category_id);
            param.put("DOCUMENT_DESCRIPTION", document_description);
            param.put("TEXT", text);
            param.put("P_TITLE", p_title);
            param.put("P_FILE_NAME", p_file_name);
            param.put("P_URL", p_url);
            param.put("P_ATTACHED_DOCUMENT_ID", attached_document_id);
            param.put("DATATYPE_ID", dataType);
            param.put("ENTITY_NAME", entity_name);
            param.put("PK1_VALUE", pk1_value);
            param.put("PK2_VALUE", pk2_value);
            param.put("PK3_VALUE", pk3_value);
            param.put("MEDIA_ID", media_id);
            param.put("USER_ID", user_id);
            param.put("P_CONTENT_TYPE", p_content_type);
            param.put("P_FILE_DATA", base64);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Update Attachment Error",e);
        }
        return result;
    }

    public String deleteAttachment(String attached_document_id,
                                   String datatype_id,
                                   String media_id) {

        String result = "";

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/delete_attachment/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/xxstc_eam_custom_api/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/delete_attachment/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("L_ATTACHED_DOCUMENT_ID", attached_document_id);
            param.put("L_DATATYPE_ID", datatype_id);
            param.put("L_MEDIA_ID", media_id);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Delete Attachment Error",e);
        }
        return result;
    }

    /**
     * USING SOA GATEWAY
     * @param documentId
     * @return
     */
    public String publishToCatalog(String documentId) {

        String result = "";
        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/publish_to_catalog/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/xxstc_eam_custom_api/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/publish_to_catalog/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_DOCUMENT_ID", documentId);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Publish to Catalog Error",e);
        }
        return result;
    }

    /**
     * USING SOA GATEWAY
     * @param documentId
     * @param pkValue1
     * @param pkValue2
     * @param pkValue3
     * @return
     */
    public String addAttachmentFromCatalog(String documentId, String pkValue1,String pkValue2,String pkValue3) {

        String result = "";

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/add_attachment_from_catalog/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/xxstc_eam_custom_api/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/add_attachment_from_catalog/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_DOCUMENT_ID", documentId);
            param.put("P_PK1_VALUE", pkValue1);
            param.put("P_PK2_VALUE", pkValue2);
            param.put("P_PK3_VALUE", pkValue3);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Add Attachment From Catalog Error",e);
        }
        return result;
    }
    
        public String getAttachmentSize(
            String userID,
            String respId,
            String respAppId
    ) {

        String result = null;
        String statement = "SELECT NVL (B.PROFILE_OPTION_VALUE, 0)/1024 As size_limit, SYSDATE\n"
                + "FROM FND_PROFILE_OPTIONS A, FND_PROFILE_OPTION_VALUES B\n"
                + "WHERE     A.APPLICATION_ID = B.APPLICATION_ID\n"
                + "AND A.PROFILE_OPTION_ID = B.PROFILE_OPTION_ID\n"
                + "AND B.LEVEL_VALUE = 0\n"
                + "AND A.PROFILE_OPTION_NAME = 'UPLOAD_FILE_SIZE_LIMIT'";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_USER_ID", userID);
            param.put("P_RESP_ID", respId);
            param.put("P_RESP_APPL_ID", respAppId);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ATTACHMENT_SIZE");

            JSONObject jsonObject = new JSONObject(result);
            result = jsonObject.toString();
            
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("SOA Error", e);
            logger.error("QUERY : " + statement);
        }
        System.out.println(result);
        return result;
    }

    void closeAllConnection(Connection connection,
                            PreparedStatement preparedStatement,
                            ResultSet resultSet,
                            CallableStatement callableStatement) {

        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }
        
        

    }
    
        public String blobTobase64(InputStream finput){
        String result= "";
      
        try {
            byte[] bytes = IOUtils.toByteArray(finput);
            String encoded = java.util.Base64.getEncoder().encodeToString(bytes);
            result = encoded;
           
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    
//    public static void main(String args[]){
//       AttachmentController a= new AttachmentController();
////        a.getAttachmentSize( "58998",  "23119",  "426");
//          a.getAttachmentList("WR", "62046", "", "");
//    }
}
