/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.objects;

/**
 *
 * @author lenovo
 */
public class FavouriteBean {
 
    private String userId;
    private String pageKey;
    private String arabicMeaning;
    private String englishMeaning;
    private String creationDate;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPageKey() {
        return pageKey;
    }

    public void setPageKey(String pageKey) {
        this.pageKey = pageKey;
    }

    public String getArabicMeaning() {
        return arabicMeaning;
    }

    public void setArabicMeaning(String arabicMeaning) {
        this.arabicMeaning = arabicMeaning;
    }

    public String getEnglishMeaning() {
        return englishMeaning;
    }

    public void setEnglishMeaning(String englishMeaning) {
        this.englishMeaning = englishMeaning;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
    
    
    
    
    
}
