/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import static com.stc.controller.MainController.logger;
import static com.stc.controller.MainController.pool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Anas Alghawi
 */
public class AssetNumberController {

    public PreparedStatement preparedStatement;
    public NamedParameterStatement namedParameterStatement;
    public ResultSet resultSet;

    public String getBuildingsList(String organizatioID) {
        
        String result = "";

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxx_stc_eam_lov_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/buildings_xml_lov/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/buildings_xml_lov/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("P_ORGANIZATION_ID", organizatioID);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Buildings List Error", e);
        }
        System.out.println(result);
        return result;
    }
    
    
    public String getFloorsList(String buildingID) {
        
        String result = "";

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxx_stc_eam_lov_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/floors_xml_lov/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/floors_xml_lov/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("P_BUILDING_ID", buildingID);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Floors List Error", e);
        }
        System.out.println(result);
        return result;
    }
    
    
    public String getRoomsList(String floorID) {
        
        String result = "";

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxx_stc_eam_lov_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/rooms_xml_lov/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/rooms_xml_lov/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("P_FLOOR_ID", floorID);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Rooms List Error", e);
        }
        System.out.println(result);
        return result;
    }
    
    public String getAssetsList(String roomID) {
        
        String result = "";

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxx_stc_eam_lov_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/assets_xml_lov/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/xxx_stc_eam_lov_ws/assets_xml_lov/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("P_ROOM_ID", roomID);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Assets List Error", e);
        }
        System.out.println(result);
        return result;
    }
    


}
