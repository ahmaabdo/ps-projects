/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.objects;

import java.util.ArrayList;

/**
 *
 * @author lenovo
 */
public class PurchasingBean {

    private String poNumber;
    private String supplier;
    private String lineNumber;
    private String item;
    private String description;
    private String shipmentNumber;
    private String lpn;
    private String location;
    private String uom;
    private String qty;
    private String vendor;
    private String subInventory;
    private String locator;
    private String serialFrom;
    private String serialto;
    private String accLpn;
    private String accQty;
    private String rejQty;
    private String transactionType;
    private String transactionDate;
    private String createdBy;
    private String creatationDate;
    private String lastUpdatedDate;
    private String lastUpdatedBy;
    private String qrText;
    private String itemType;
    private String itemKey;
    private String status;
    private String importFlag;
    private String importDate;
    private String errorMessage;
    private String peference1; 
    private String peference2;
    private String peference3;
    private String peference4;
    private String peference5;
    private String requestId;
    private String rejReason;
    private String rejNotes;
    private String orgId;
//    private String poLID;
//    private String QR;
   private  ArrayList<String>  serialArr = new ArrayList<String>();
   
    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

//    public String getQR() {
//        return QR;
//    }
//
//    public void setQR(String QR) {
//        this.QR = QR;
//    }
//
//    public String getPoLID() {
//        return poLID;
//    }
//
//    public void setPoLID(String poLID) {
//        this.poLID = poLID;
//    }

    public String getRejReason() {
        return rejReason;
    }

    public void setRejReason(String rejReason) {
        this.rejReason = rejReason;
    }

    public String getRejNotes() {
        return rejNotes;
    }

    public void setRejNotes(String rejNotes) {
        this.rejNotes = rejNotes;
    }
 

    public ArrayList<String> getSerialArr() {
        return serialArr;
    }

    public void setSerialArr(ArrayList<String> serialArr) {
        this.serialArr = serialArr;
    }
    
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatationDate() {
        return creatationDate;
    }

    public void setCreatationDate(String creatationDate) {
        this.creatationDate = creatationDate;
    }

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getQrText() {
        return qrText;
    }

    public void setQrText(String qrText) {
        this.qrText = qrText;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemKey() {
        return itemKey;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getImportDate() {
        return importDate;
    }

    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getLpn() {
        return lpn;
    }

    public void setLpn(String lpn) {
        this.lpn = lpn;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getSubInventory() {
        return subInventory;
    }

    public void setSubInventory(String subInventory) {
        this.subInventory = subInventory;
    }

    public String getLocator() {
        return locator;
    }

    public void setLocator(String locator) {
        this.locator = locator;
    }

    public String getSerialFrom() {
        return serialFrom;
    }

    public void setSerialFrom(String serialFrom) {
        this.serialFrom = serialFrom;
    }

    public String getSerialto() {
        return serialto;
    }

    public void setSerialto(String serialto) {
        this.serialto = serialto;
    }

    public String getAccLpn() {
        return accLpn;
    }

    public void setAccLpn(String accLpn) {
        this.accLpn = accLpn;
    }

    public String getAccQty() {
        return accQty;
    }

    public void setAccQty(String accQty) {
        this.accQty = accQty;
    }

    public String getRejQty() {
        return rejQty;
    }

    public void setRejQty(String rejQty) {
        this.rejQty = rejQty;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getPeference1() {
        return peference1;
    }

    public void setPeference1(String peference1) {
        this.peference1 = peference1;
    }

    public String getPeference2() {
        return peference2;
    }

    public void setPeference2(String peference2) {
        this.peference2 = peference2;
    }

    public String getPeference3() {
        return peference3;
    }

    public void setPeference3(String peference3) {
        this.peference3 = peference3;
    }

    public String getPeference4() {
        return peference4;
    }

    public void setPeference4(String peference4) {
        this.peference4 = peference4;
    }

    public String getPeference5() {
        return peference5;
    }

    public void setPeference5(String peference5) {
        this.peference5 = peference5;
    }

   
}
