package com.stc.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import static com.stc.controller.NotificationController.logger;
import static com.stc.controller.NotificationController.pool;
import static com.stc.controller.PublicController.pool;
import static com.stc.controller.WorkRequestController.logger;
import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 09/01/18
 * Time: 15:26
 * To change this template use File | Settings | File Templates.
 */
public class WorkOrderController extends PlsqlQuery{

    final static Logger logger = Logger.getLogger(WorkOrderController.class);

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public PreparedStatement preparedStatement;
    public NamedParameterStatement namedParameterStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;

    public String getWorkOrderDashboard(int orgId,int userId) {
        String result = null;

        String statement = "select flv.meaning as WORK_ORDER_STATUS, count(ewov.ROW_ID) as COUNT \n" +
                "from APPS.eam_work_orders_v ewov, fnd_lookup_values flv\n" +
                "where ewov.organization_id =? AND ewov.CREATED_BY=? \n" +
                "AND flv.lookup_type = 'XXX_STC_WO_WF_STATUS' AND flv.lookup_code =ewov.attribute15\n" +
                "group by flv.meaning order by flv.meaning";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(2000);
            preparedStatement.setInt(1, orgId);
            preparedStatement.setInt(2, userId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(2000);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("WORK_ORDER_STATUS",resultSet.getString(1));
                jsonObject.addProperty("COUNT",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_ORDER_STATUS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Dashboard Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWorkOrderRequest(int orgId,
                                      String requestNumber,
                                      String assetDepartment,
                                      String assetNumber,
                                      String status,
                                      Date creationDateFrom,
                                      Date creationDateTo,
                                      String userID,
                                      String respId,
                                      String respAppId,
                                      String isHqc
    ) {


        String result = null;

        String statement = "SELECT\n" +
"    *\n" +
"FROM ( SELECT\n" +
"    wr.request_number   AS requestnumber, ( SELECT\n" +
"    DECODE( wr.request_type_code,\n" +
"  1,\n" +
"  DECODE(wr.wip_entity_id,NULL,'N','Y'),\n" +
"  2, \n" +
"                decode(count(ewos.wip_entity_id) , 0 , 'N' , 'Y' ) ) from APPS.eam_wo_service_association ewos where ewos.service_request_id = wr.request_id  \n" +
"                and ewos.maintenance_organization_id = wr.organization_id and (ewos.enable_flag IS NULL OR ewos.enable_flag = 'Y')) as AssignStatus ,  \n" +
"                wr.request_type as RequestType, wr.status as Status, wr.asset_number as AssetNumber,wr.department_description as departmentDescription,wr.instance_number as AssetCode, wr.asset_group as AssetGroup, wr.department_code as DepartmentCode,  \n" +
"                wr.exp_resolution_date as ExpectedResolutionDate , wr.description as Description, wr.originator as Originator , wr.reported_date as ReportedDate,  \n" +
"                wr.asset_type as AssetType , decode(wr.request_type_code, 1, decode(wr.wip_entity_id,null,'CreateEnabled' ,'CreateDisabled') , 2,  \n" +
"                decode(wr.maintenance_flag,'N','CreateDisabled', 'CreateEnabled')) as CreateSwitcher, 'N' as Select_Flag1, wr.wip_entity_id as Wip_Entity_Id ,  \n" +
"                wr.wr_created_by as WRCreatedBy, wr.wr_status_id as WRStatusId, wr.wr_priority_id as    WRPriorityId, wr.wr_type_id as  WRTypeId,  \n" +
"                wr.request_id as    RequestId, wr.organization_id as    OrganizationId, wr.department_id as DepartmentId, wr.sr_customer_type as    SRCustomerType, \n" +
"                wr.sr_customer_id as    SRCustomerId, wr.sr_customer_number as  SRCustomerNumber, wr.sr_account_id as   SRAccountId, wr.sr_contact_type as SRContactType,  \n" +
"                wr.sr_contact_party_id as   SRContactPartyId, wr.sr_contact_number as   SRContactNumber, wr.sr_created_by   as  SRCreatedBy, wr.sr_status_id    as  SRStatusId,  \n" +
"                wr.sr_priority_id   as SRPriorityId, wr.sr_type_id  as  SRTypeId , wr.exp_resolution_date as ExpectedResolutionDate2, wr.request_type_code as RequestTypeCode ,  \n" +
"                ( select decode( wr.request_type_code , 1, decode(wr.wip_entity_id , null, 0 , 1) , 2, count(ewos.wip_entity_id) ) from APPS.eam_wo_service_association ewos  \n" +
"                where ewos.service_request_id = wr.request_id and (ewos.enable_flag IS NULL OR ewos.enable_flag = 'Y')) as OrderCount , decode(wr.request_type_code, 1,  \n" +
"                decode(wr.wip_entity_id,null,'AssignEnabled' ,'AssignDisabled') , 2, decode(wr.maintenance_flag,'N','AssignDisabled', 'AssignEnabled') ) as AssignSwitcher ,  \n" +
"                wr.sr_close_flag    as SRCloseFlag , nvl(wr.asset_type_code,0)  as AssetTypeCode, 'N' as detail_flag, wr.reported_date as ReportedDate2, to_char(wr.reported_date, 'dd-MON-yyyy hh:mmAM') AS formated_reported_date, org_id as OrgId,  \n" +
"                decode(wr.request_type_code, 1,'WR' , 2, wr.maintenance_flag) as maintenance_flag , wr.maintenance_object_type as maint_obj_type,  \n" +
"                wr.maintenance_object_id as maint_obj_id, wr.instance_number AS INSTANCE_NUMBER, decode(wr.request_type,'Work Request','WorkRequest', 'Service Request','ServiceRequest') as RequestSwitcher \n" +
"                \n" +
"FROM\n" +
"    apps.xxeam_manage_reqs_v wr\n" +
") qrslt\n" +
"WHERE\n" +
"    ( ( organizationid ="+orgId+")\n" +
"      AND ( wrstatusid IN (\n" +
"        '3',\n" +
"        '4'\n" +
"    )\n" +
"            OR nvl(srcloseflag,'N') != 'Y' ) )\n" +
"AND (   '"+isHqc+"' = 'N'\n" +
"            OR (    '"+isHqc+"' = 'Y'\n" +
"                AND (   (       (SELECT NVL (\n" +
"                                           fnd_profile.VALUE (\n" +
"                                              'XXX_SCM_EAM_SUPERVISOR'),\n" +
"                                           'N')\n" +
"                                   FROM DUAL) = 'Y'\n" +
"                            AND (SELECT NVL (\n" +
"                                           fnd_profile.VALUE (\n" +
"                                              'XXX_EAM_IGATE_SUPERUSER'),\n" +
"                                           'N')\n" +
"                                   FROM DUAL) = 'N'\n" +
"                         OR (    (SELECT NVL (\n" +
"                                            fnd_profile.VALUE (\n" +
"                                               'XXX_SCM_EAM_SUPERVISOR'),\n" +
"                                            'N')\n" +
"                                    FROM DUAL) <> 'Y'\n" +
"                             AND (SELECT NVL (\n" +
"                                            fnd_profile.VALUE (\n" +
"                                               'XXX_EAM_IGATE_SUPERUSER'),\n" +
"                                            'N')\n" +
"                                    FROM DUAL) = 'N'\n" +
"                             AND qrslt.requestnumber IN\n" +
"                                    (SELECT DISTINCT work_request_number\n" +
"                                       FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V cii,\n" +
"                                            STC.XXX_STC_EAM_APPROVERS app,\n" +
"                                            XXX_STC_EAM_APPROVERS_DEPT_V dept,\n" +
"                                            FND_FLEX_VALUES_VL FV,\n" +
"                                            FND_FLEX_VALUE_SETS FVS,\n" +
"                                            FND_FLEX_VALUES_VL FV1,\n" +
"                                            FND_FLEX_VALUE_SETS FVS1,\n" +
"                                            BOM_DEPARTMENTS BD,\n" +
"                                            org_organization_definitions org,\n" +
"                                            wip_eam_work_requests wdj\n" +
"                                      WHERE     FV.FLEX_VALUE_SET_ID =\n" +
"                                                   FVS.FLEX_VALUE_SET_ID\n" +
"                                            AND FVS.FLEX_VALUE_SET_NAME =\n" +
"                                                   'XXSCM_Zone'\n" +
"                                            AND FV.FLEX_VALUE =\n" +
"                                                   cii.attribute1\n" +
"                                            AND dept.zone_id =\n" +
"                                                   FV.FLEX_VALUE_ID\n" +
"                                            AND dept.APPROVER_DEPT_ID =\n" +
"                                                   app.APPROVER_DEPT_ID\n" +
"                                            AND org.organization_id =\n" +
"                                                   dept.organization_id\n" +
"                                            AND org.organization_code = 'E00'\n" +
"                                            AND wdj.WORK_REQUEST_OWNING_DEPT =\n" +
"                                                   bd.department_id\n" +
"                                            AND bd.attribute1 =\n" +
"                                                   fv1.flex_value\n" +
"                                            AND fv1.flex_value_id =\n" +
"                                                   dept.department_id\n" +
"                                            AND fv1.flex_value_set_id =\n" +
"                                                   fvs1.flex_value_set_id\n" +
"                                            AND fvs1.flex_value_set_name =\n" +
"                                                   'XXSCM_Departments'\n" +
"                                            AND dept.position_name IN\n" +
"                                                   ('ABFM', 'BFM')\n" +
"                                            AND wdj.attribute15 IS NULL\n" +
"                                            AND wdj.work_request_number =\n" +
"                                                   qrslt.requestnumber\n" +
"                                            AND cii.instance_number =\n" +
"                                                   (SELECT CASE\n" +
"                                                              WHEN INSTR (\n" +
"                                                                      SUBSTR (\n" +
"                                                                         C.INSTANCE_NUMBER,\n" +
"                                                                         8,\n" +
"                                                                         5),\n" +
"                                                                      '-') BETWEEN 1\n" +
"                                                                               AND 4\n" +
"                                                              THEN\n" +
"                                                                 RTRIM (\n" +
"                                                                    SUBSTR (\n" +
"                                                                       C.INSTANCE_NUMBER,\n" +
"                                                                       1,\n" +
"                                                                       10),\n" +
"                                                                    '-')\n" +
"                                                              WHEN INSTR (\n" +
"                                                                      SUBSTR (\n" +
"                                                                         C.INSTANCE_NUMBER,\n" +
"                                                                         8,\n" +
"                                                                         5),\n" +
"                                                                      '-') >\n" +
"                                                                      4\n" +
"                                                              THEN\n" +
"                                                                 RTRIM (\n" +
"                                                                    SUBSTR (\n" +
"                                                                       C.INSTANCE_NUMBER,\n" +
"                                                                       1,\n" +
"                                                                       11),\n" +
"                                                                    '-')\n" +
"                                                              ELSE\n" +
"                                                                 C.INSTANCE_NUMBER\n" +
"                                                           END\n" +
"                                                      FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V c\n" +
"                                                     WHERE     CURRENT_ORGANIZATION_ID =\n" +
"                                                                  QRSLT.ORGANIZATIONID\n" +
"                                                           AND c.serial_number =\n" +
"                                                                  (SELECT asset_number\n" +
"                                                                     FROM wip_eam_work_requests wdj1\n" +
"                                                                    WHERE     wdj1.work_request_number =\n" +
"                                                                                 qrslt.requestnumber\n" +
"                                                                          AND wdj1.ORGANIZATION_ID =\n" +
"                                                                                 QRSLT.ORGANIZATIONID))\n" +
"                                            AND app.user_id =\n" +
"                                                   (SELECT TO_CHAR (\n" +
"                                                              fnd_profile.VALUE (\n" +
"                                                                 'USER_ID'))\n" +
"                                                      FROM DUAL))))\n" +
"                     OR (    (SELECT NVL (\n" +
"                                        fnd_profile.VALUE (\n" +
"                                           'XXX_EAM_IGATE_SUPERUSER'),\n" +
"                                        'N')\n" +
"                                FROM DUAL) = 'Y'\n" +
"                         AND qrslt.requestnumber IN\n" +
"                                (SELECT DISTINCT work_request_number\n" +
"                                   FROM wip_eam_work_requests wdj\n" +
"                                  WHERE     wdj.attribute15 IS NOT NULL\n" +
"                                        AND wdj.work_request_number =\n" +
"                                               qrslt.requestnumber))\n" +
"                     OR (   (SELECT NVL (\n" +
"                                       fnd_profile.VALUE (\n" +
"                                          'XXX_EAM_CONT_SUPERUSER'),\n" +
"                                       'N')\n" +
"                               FROM DUAL) = 'Y'\n" +
"                         OR (    (SELECT NVL (\n" +
"                                            fnd_profile.VALUE (\n" +
"                                               'XXX_EAM_IGATE_SUPERUSER'),\n" +
"                                            'N')\n" +
"                                    FROM DUAL) = 'N'\n" +
"                             AND (SELECT NVL (\n" +
"                                            fnd_profile.VALUE (\n" +
"                                               'XXX_EAM_CONT_SUPERUSER'),\n" +
"                                            'N')\n" +
"                                    FROM DUAL) <> 'Y'\n" +
"                             AND qrslt.requestnumber IN\n" +
"                                    (SELECT DISTINCT work_request_number\n" +
"                                       FROM STC.XXX_STC_EAM_APPROVERS app,\n" +
"                                            XXX_STC_EAM_APPROVERS_DEPT_V dept,\n" +
"                                            org_organization_definitions org,\n" +
"                                            wip_eam_work_requests wdj\n" +
"                                      WHERE     dept.APPROVER_DEPT_ID =\n" +
"                                                   app.APPROVER_DEPT_ID\n" +
"                                            AND org.organization_id =\n" +
"                                                   dept.organization_id\n" +
"                                            AND org.organization_code = 'E00'\n" +
"                                            AND dept.position_name =\n" +
"                                                   'CONT_SPEC_ENGINEER'\n" +
"                                            AND DEPT.SUB_DEPARTMENT_ID =\n" +
"                                                   wdj.WORK_REQUEST_OWNING_DEPT\n" +
"                                            AND wdj.work_request_number =\n" +
"                                                   qrslt.requestnumber\n" +
"                                            AND app.user_id =\n" +
"                                                   (SELECT TO_CHAR (\n" +
"                                                              fnd_profile.VALUE (\n" +
"                                                                 'USER_ID'))\n" +
"                                                      FROM DUAL)))))))     			";

        if (requestNumber != null && !requestNumber.isEmpty()) {
            statement += " AND (REQUESTNUMBER LIKE UPPER('%"+requestNumber.toUpperCase()+"%') OR REQUESTNUMBER IS NULL)";
        }
        if (assetDepartment != null && !assetDepartment.isEmpty()) {
            statement += " AND (DepartmentId LIKE '"+assetDepartment+"' OR DepartmentId IS NULL)";
        }
        if (assetNumber != null && !assetNumber.isEmpty()) {
            statement += " AND (maint_obj_id LIKE '"+assetNumber+"' OR maint_obj_id IS NULL)";
        }
        if (status != null && !status.isEmpty()) {
            statement += " AND (WRStatusId LIKE '"+status+"' OR WRStatusId IS NULL)";
        }
        if (creationDateFrom != null) {
            statement += " AND REPORTEDDATE2 >= to_date('"+creationDateFrom+"', 'YYYY-MM-DD')";
        }
        if (creationDateTo != null) {
            statement += " AND REPORTEDDATE2 <= TO_DATE('"+creationDateTo+"', 'YYYY-MM-DD')+1";
        }
        
//        System.out.println(statement);

//        try {
//            connection  = pool.getConnection();
//            connection.setAutoCommit(false);
//
//            namedParameterStatement = new NamedParameterStatement(connection, statement);
//            namedParameterStatement.setFetchSize(1000);
//            namedParameterStatement.setInt("orgId", orgId);
//
//            if (requestNumber != null && !requestNumber.isEmpty()) {
//                namedParameterStatement.setString("requestNumber","%" + requestNumber + "%");
//            }
//            if (assetDepartment != null && !assetDepartment.isEmpty()) {
//                namedParameterStatement.setString("assetDepartment","%" + assetDepartment + "%");
//            }
//            if (assetNumber != null && !assetNumber.isEmpty()) {
//                namedParameterStatement.setString("assetNumber","%" + assetNumber + "%");
//            }
//            if (status != null && !status.isEmpty()) {
//                namedParameterStatement.setString("status","%" + status + "%");
//            }
//            if (creationDateFrom != null) {
//                namedParameterStatement.setDate("creationDateFrom", creationDateFrom);
//            }
//            if (creationDateTo != null) {
//                namedParameterStatement.setDate("creationDateTo", creationDateTo);
//            }
//
//            System.out.println(statement);
//            resultSet = namedParameterStatement.executeQuery();
//            resultSet.setFetchSize(1000);
//
//            JsonObject jsonResult = new JsonObject();
//            JsonArray jsonArray = new JsonArray();
//
//            while (resultSet.next()){
//                JsonObject jsonObject = new JsonObject();
//                jsonObject.addProperty("REQUEST_NUMBER",resultSet.getString("REQUESTNUMBER"));
//                jsonObject.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
//                jsonObject.addProperty("ASSIGNED_WORK_ORDER",resultSet.getString(2));
//                jsonObject.addProperty("STATUS",resultSet.getString("STATUS"));
//                jsonObject.addProperty("ASSET_NUMBER",resultSet.getString("ASSETNUMBER"));
//                jsonObject.addProperty("DEPARTMENT_CODE",resultSet.getString("DEPARTMENTCODE"));
//                jsonObject.addProperty("CREATION_DATE",resultSet.getString("REPORTEDDATE2"));
//                jsonObject.addProperty("ASSET_CODE",resultSet.getString("ASSETCODE"));
//                jsonObject.addProperty("DEPARTMENT_DESCRIPTION",resultSet.getString("DEPARTMENTDESCRIPTION"));
//                jsonArray.add(jsonObject);
//            }
//
//            jsonResult.add("WORK_ORDER_REQUEST",jsonArray);
//            result = jsonResult.toString();
//
//            connection.close();
//            namedParameterStatement.close();
//            resultSet.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Get Work Order Request Error",e);
//        }
//        finally {
//            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
//        }
try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_USER_ID", userID);
            param.put("P_RESP_ID", respId);
            param.put("P_RESP_APPL_ID", respAppId);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "WORK_ORDER_REQUEST");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("WORK_ORDER_REQUEST");

            JSONObject jsonResult = new JSONObject();
            JSONArray arrayResult = new JSONArray();

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject1 = new JSONObject();
                
            	jsonObject1.put("REQUEST_NUMBER",jsonArray.getJSONObject(i).get("REQUESTNUMBER"));
                jsonObject1.put("DESCRIPTION",jsonArray.getJSONObject(i).get("DESCRIPTION"));
                jsonObject1.put("ASSIGNED_WORK_ORDER",jsonArray.getJSONObject(i).get("ASSIGNSTATUS"));
                jsonObject1.put("STATUS",jsonArray.getJSONObject(i).get("STATUS"));
                jsonObject1.put("ASSET_NUMBER",jsonArray.getJSONObject(i).get("ASSETNUMBER"));
                jsonObject1.put("DEPARTMENT_CODE",jsonArray.getJSONObject(i).get("DEPARTMENTCODE"));
                jsonObject1.put("CREATION_DATE",jsonArray.getJSONObject(i).get("FORMATED_REPORTED_DATE"));
                jsonObject1.put("ASSET_CODE",jsonArray.getJSONObject(i).get("ASSETCODE"));
                jsonObject1.put("DEPARTMENT_DESCRIPTION",jsonArray.getJSONObject(i).get("DEPARTMENTDESCRIPTION"));
                
                arrayResult.put(jsonObject1);
            }

            jsonResult.put("WORK_ORDER_REQUEST", arrayResult);

            result = jsonResult.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("SOA Error", e);
            logger.error("QUERY : " + statement);
        }
        System.out.println("//WORK_ORDER_REQUEST Return Result");
        System.out.println(result);

        return result;

    }

    public String getAssignedWorkOrder(String workRequestId, int orgId) {
        String result = null;

        String statement = "SELECT * FROM (SELECT distinct we.wip_entity_name work_order_name, wdj.description work_description, wdj.wip_entity_id , wdj.organization_id , \n" +
                "bd.department_code, wdj.owning_department department_id, msibkfv1.concatenated_segments asset_group, nvl(wdj.rebuild_item_id,wdj.asset_group_id) asset_group_id,  \n" +
                "mlu1.meaning priority, wdj.priority priority_id, ewodv.work_order_status_pending AS status, wdj.status_type status_id, msibkfv2.concatenated_segments activity,  \n" +
                "msibkfv2.inventory_item_id activity_id, cii.instance_number asset_number, wdj.scheduled_start_date, wdj.scheduled_completion_date, wdj.date_completed,  \n" +
                "wdj.requested_start_date, wdj.shutdown_type shutdown_type_id, mlu3.meaning shutdown_type, DECODE(wdj.plan_maintenance, 'Y','Yes','No') planned,  \n" +
                "DECODE(wdj.tagout_required,'Y','Yes','No') tagout_required, ppv.project_name, ppv.project_number, ppv.project_description, wdj.project_id, mtv.task_number,  \n" +
                "mtv.task_name, mtv.task_id , to_char(null) AS delete_icon, 'N' AS Details, msibkfv1.eam_item_type asset_type_id, mlu4.meaning asset_type , mp.organization_code  \n" +
                "FROM APPS.pjm_projects_v ppv, APPS.mtl_task_v mtv, APPS.wip_discrete_jobs wdj, APPS.wip_entities we, APPS.bom_departments bd, APPS.mtl_system_items_b_kfv msibkfv1, APPS.mfg_lookups mlu1,  \n" +
                "APPS.mfg_lookups mlu3, APPS.mfg_lookups mlu4, APPS.mtl_system_items_b_kfv msibkfv2, APPS.csi_item_instances cii, APPS.eam_work_order_details_v ewodv, APPS.mtl_parameters mp  \n" +
                "WHERE we.organization_id = mp.organization_id and we.wip_entity_id=wdj.wip_entity_id AND we.organization_id=wdj.organization_id  \n" +
                "AND nvl(wdj.rebuild_item_id,wdj.asset_group_id)=msibkfv1.inventory_item_id AND wdj.owning_department = bd.department_id(+)  \n" +
                "AND wdj.priority = mlu1.lookup_code(+) AND mlu1.lookup_type(+)='WIP_EAM_ACTIVITY_PRIORITY' AND wdj.wip_entity_id = ewodv.wip_entity_id  \n" +
                "AND wdj.organization_id = ewodv.organization_id AND msibkfv2.inventory_item_id(+)=wdj.primary_item_id AND msibkfv2.organization_id(+)=wdj.organization_id  \n" +
                "AND cii.instance_id (+) = DECODE(wdj.maintenance_object_type,3,wdj.maintenance_object_id,NULL) AND mlu3.lookup_type(+)='BOM_EAM_SHUTDOWN_TYPE'  \n" +
                "AND mlu3.lookup_code(+)=wdj.shutdown_type AND mlu4.lookup_type ='MTL_EAM_ASSET_TYPE' AND mlu4.lookup_code =msibkfv1.eam_item_type  \n" +
                "AND wdj.project_id=ppv.project_id(+) AND wdj.task_id=mtv.task_id(+) and ( EXISTS (SELECT 1 FROM APPS.wip_eam_work_requests wewr  \n" +
                "WHERE wip_entity_id is not null AND wewr.wip_entity_id = wdj.wip_entity_id AND wewr.work_request_id = ? )  \n" +
                "OR EXISTS ( SELECT 1 FROM APPS.eam_wo_service_association ewsa WHERE ewsa.wip_entity_id = wdj.wip_entity_id and (ewsa.enable_flag is null or ewsa.enable_flag = 'Y')  \n" +
                "AND ewsa.service_request_id = ? ) )) QRSLT WHERE (organization_id = ?) ORDER BY WORK_ORDER_NAME ASC";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);

            preparedStatement.setString(1, workRequestId);
            preparedStatement.setString(2, workRequestId);
            preparedStatement.setInt(3, orgId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("WORK_ORDER_NAME",resultSet.getString("WORK_ORDER_NAME"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("WORK_DESCRIPTION"));
                jsonObject.addProperty("WIP_ENTITY_ID",resultSet.getString("WIP_ENTITY_ID"));
                jsonObject.addProperty("STATUS",resultSet.getString("STATUS"));
                jsonObject.addProperty("ASSET_GROUP",resultSet.getString("ASSET_GROUP"));
                jsonObject.addProperty("ASSET_NUMBER",resultSet.getString("ASSET_NUMBER"));
                jsonObject.addProperty("ASSET_TYPE",resultSet.getString("ASSET_TYPE"));
                jsonObject.addProperty("START_DATE",resultSet.getString("SCHEDULED_START_DATE"));
                jsonObject.addProperty("DEPARTMENT_CODE",resultSet.getString("DEPARTMENT_CODE"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ASSIGNED_WORK_ORDER",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Assigned Work Order Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String generateWorkOrderName() {
        JsonObject jsonObject = new JsonObject();
        String result = null;


        //String statement = "SELECT MAX(CAST(SUBSTR(WIP_ENTITY_NAME,3,LENGTH(WIP_ENTITY_NAME))AS INT)) + 1 FROM EAM_WORK_ORDERS_V WHERE WIP_ENTITY_NAME LIKE 'WO%'";

        String statement = "select MAX(CAST(regexp_replace(wip_entity_name, '[^0-9]', '') AS INT)) + 1 \n" +
                "from apps.EAM_WORK_ORDERS_V\n" +
                "where wip_entity_name like 'WO%' AND CREATION_DATE>(TRUNC(SYSDATE-30))";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(1000);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);
            if (resultSet.next()) {
                result = "WO" + resultSet.getString(1);
            }

            jsonObject.addProperty("WORK_ORDER_NAME", result);

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Generate Work Order Name Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return jsonObject.toString();
    }

    public String getClassCode(int orgId) {
        JsonObject jsonObject = new JsonObject();
        String result = null;

        String statement = "SELECT CLASS_CODE FROM APPS.WIP_ACCOUNTING_CLASSES WHERE CLASS_TYPE = 6 AND ORGANIZATION_ID = ?";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1,orgId);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = resultSet.getString(1);
            }

            jsonObject.addProperty("CLASS_CODE", result);

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Class Code Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return jsonObject.toString();
    }

    public String getAssetActivity(int orgId, int maintenanceObjectId) {
        String result = null;

        String statement = "SELECT eaa.asset_activity_id, msi.organization_id, msi.concatenated_segments as asset_activity, msi.description as description, \n" +
                "eomd.activity_cause_code as activity_cause_lkcode, eomd.activity_type_code as activity_type_lkcode, eaa.priority_code as priority_lkcode, \n" +
                "bd.department_code, eomd.shutdown_type_code, eomd.activity_source_code, eomd.owning_department_id, eaa.maintenance_object_type, \n" +
                "eaa.maintenance_object_id, bd.description as dept_description, DECODE(eomd.tagging_required_flag,'Y',1,2) as tagout_required, \n" +
                "DECODE(msi.eam_act_notification_flag,'Y',1,2) as notification_required FROM APPS.mtl_system_items_vl msi, APPS.mtl_eam_asset_activities eaa, \n" +
                "APPS.bom_departments bd, ( select * from APPS.eam_org_maint_defaults where organization_id = ? ) eomd WHERE msi.inventory_item_id = eaa.asset_activity_id\n" +
                "AND eaa.activity_association_id = eomd.object_id (+) AND NVL(eaa.START_DATE_ACTIVE, SYSDATE-2) < SYSDATE AND NVL(eaa.END_DATE_ACTIVE, SYSDATE+2) > SYSDATE \n" +
                "AND ( eomd.object_type is null OR eomd.object_type in ( 40, 60 ) ) AND nvl(eaa.tmpl_flag,'N') <> 'Y' AND eomd.owning_department_id = bd.department_id (+) \n" +
                "AND msi.organization_id = ? and eaa.maintenance_object_id = ?";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1,orgId);
            preparedStatement.setInt(2,orgId);
            preparedStatement.setInt(3,maintenanceObjectId);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ASSET_ACTIVITY_ID", resultSet.getString("ASSET_ACTIVITY_ID"));
                jsonObject.addProperty("ASSET_ACTIVITY", resultSet.getString("ASSET_ACTIVITY"));
                jsonObject.addProperty("DESCRIPTION", resultSet.getString("DESCRIPTION"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ASSET_ACTIVITIES", jsonArray);

            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Asset Activity Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWorkOrderStatus() {
        String result = null;

        String statement = "SELECT * FROM (SELECT status_id, system_status, work_order_status, enabled_flag, seeded_flag FROM APPS.EAM_WO_STATUSES_V) QRSLT \n" +
                "--WHERE status_id IN(1,3,6,17,1000) and enabled_flag='Y' ORDER BY system_status,work_order_status";
        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("STATUS_ID",resultSet.getString(1));
                jsonObject.addProperty("STATUS_NAME",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_ORDER_STATUS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Status Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWorkOrderTypes() {

        String result = null;

        String statement = "select LOOKUP_CODE, MEANING from APPS.mfg_lookups WHERE LOOKUP_TYPE = 'WIP_EAM_WORK_ORDER_TYPE'";
        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORDER_TYPE_ID",resultSet.getString(1));
                jsonObject.addProperty("ORDER_TYPE_NAME",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_ORDER_TYPES",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Types Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWoStatus() {

        String result = null;

        String statement = "SELECT *\n" +
                "  FROM APPS.fnd_lookup_values\n" +
                " WHERE    (    lookup_type = 'XXX_STC_WO_STATUS'\n" +
                "           AND language = USERENV ('lang'))\n" +
                "       OR (    lookup_type = 'XXX_STC_HQ_WO_STATUS'\n" +
                "           AND language = USERENV ('lang'))";
        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("LOOKUP_CODE",resultSet.getString("DESCRIPTION"));
                jsonObject.addProperty("MEANING",resultSet.getString("DESCRIPTION"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                jsonArray.add(jsonObject);
            }

            for (int i=1; i<5; i++) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("LOOKUP_CODE", "LOOKUP_CODE " + i);
                jsonObject.addProperty("MEANING", "MEANING " + i);
                jsonObject.addProperty("DESCRIPTION", "DESCRIPTION " + i);
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_ORDER_TYPES",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Types Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWorkOrders(int orgId,
                                String workOrderName,
                                String assetNumber,
                                String assetDescription,
                                String owningDepartment,
                                Date startDateFrom,
                                Date startDateTo,
                                String status,
                                String planned,
                                String requestNumber,
                                String isHqc,
                                String approvalType,
                                String CREATED_BY,
                                String buildingType,
                                String userID,
                                String respId,
                                String respAppId,
                                String lookupCode,
                                String dasboardSearch
    ) {
        
    
        String result = null;
        System.out.println("Search WO");
        String statement
                = "SELECT * FROM (SELECT wewodv.wip_entity_id, wewodv.wip_entity_name, wewodv.asset_number, wewodv.asset_description, wewodv.owning_department,"
                + "(SELECT count(ATTACHED_DOCUMENT_ID) FROM apps.fnd_attached_documents fad where fad.PK2_VALUE = TO_CHAR(wewodv.wip_entity_id)  AND fad.entity_name='EAM_WORK_ORDERS')  attachments_count,"
                + " wewodv.status, wewodv.description, wewodv.owning_department_id, wewodv.plan_maintenance, wewodv.organization_id, wewodv.asset_serial_number,"
                + " wdj.attribute3 AS invoice_status, wdj.attribute4 AS invoice_date, to_char(to_date(wdj.attribute4, 'YYYY/MM/DD HH24:MI:SS'), 'dd-MON-yyyy hh:mmAM') As f_invoice_date, wdj.attribute5 AS invoice_month, wdj.attribute9 AS grace_period,"
                + " wdj.attribute8 AS reason, wdj.attribute10 AS sptotal,"
                + " (SELECT meaning FROM apps.mfg_lookups WHERE lookup_type = 'WIP_EAM_ACTIVITY_PRIORITY' AND lookup_code = wewodv.priority) AS priority,"
                + " wdj.date_released, to_char(wdj.date_released, 'dd-MON-yyyy hh:mmAM') AS f_date_released, wdj.date_completed, to_char(wdj.date_completed, 'dd-MON-yyyy hh:mmAM') AS f_date_completed, wewodv.status_type, ROUND ( (wdj.scheduled_completion_date - wdj.scheduled_start_date) * 24, 3) AS duration,"
                + " wewr.work_request_number"
                + " AS request_number, wdj.scheduled_start_date, to_char(wdj.scheduled_start_date, 'dd-MON-yyyy hh:mmAM') AS f_scheduled_start_date, wdj.created_by, wdj.attribute15 AS wfstatus, (SELECT meaning FROM fnd_lookup_values WHERE lookup_type"
                + " = 'XXX_STC_WO_WF_STATUS' AND language = 'US' AND lookup_code IN (wdj.attribute15)) AS wfstatusmeaning, wdj.attribute11 AS wostatus, "
                + "(SELECT meaning FROM fnd_lookup_values WHERE lookup_type = 'XXX_STC_HQ_WO_STATUS' AND language = 'US' AND lookup_code IN (wdj.attribute11)) "
                + "AS wostatusmeaning, wdj.description AS descritpion_wdj, wdj.scheduled_completion_date, to_char(wdj.scheduled_completion_date, 'dd-MON-yyyy hh:mmAM') AS f_scheduled_completion_date, wdj.creation_date AS "
                + "creation_date, xxstc_wip_wo_custom_soa_pkg.check_lines_and_attachments ( wdj.wip_entity_id) AS claa, bd.description department_desc,"
                + " msik1.concatenated_segments AS asset_activity, (SELECT COUNT (*) FROM wip_eam_direct_items WHERE wip_entity_id IN (wewodv.wip_entity_id)) "
                + "AS sparepartlines, cii.context building_type FROM apps.wip_eam_work_order_dtls_v wewodv, apps.wip_discrete_jobs wdj, bom_departments bd,  "
                + "mtl_system_items_b_kfv msik1, csi_item_instances cii, apps.wip_eam_work_requests wewr WHERE wewodv.wip_entity_id = wdj.wip_entity_id AND xxstc_wip_hq_wo_wf_soa_pkg.is_hqc_org "
                + "("+orgId+") = '"+isHqc+"' AND wdj.attribute15 IS NOT NULL AND wdj.owning_department = bd.department_id(+) AND msik1.inventory_item_id(+) = "
                + "wdj.primary_item_id AND msik1.organization_id(+) = wdj.organization_id AND cii.serial_number = wdj.asset_number AND wewr.wip_entity_id(+) = wewodv.wip_entity_id AND cii.last_vld_organization_id "
                + "= wdj.organization_id";
        
        System.out.println("///////////////////");
        System.out.println(statement);
        System.out.println("////////////////////");

        /*if(isHqc.equals("Y")){
            statement += " AND v2.lookup_type =  'XXX_STC_HQ_WO_STATUS' and v2.language='US' and v2.lookup_code IN (WDJ.ATTRIBUTE11) ";
        } else if (isHqc.equals("N")) {
            statement += " AND v2.lookup_type =  'XXX_STC_WO_STATUS' and v2.language='US' and v2.lookup_code IN (WDJ.ATTRIBUTE11) ";
        } */
        if(approvalType.equalsIgnoreCase("SPR")){
            statement +=  " And (wdj.attribute15 IN ('A1', 'R2')) ";
        }
        else if(approvalType.equalsIgnoreCase("COMPLETE")){
            statement +=   " AND (wdj.attribute15 IN ('A1','A2', 'R3')) ";
        }
        
                if(lookupCode!=null && !lookupCode.isEmpty()){
            statement += " AND wdj.attribute15 = '"+lookupCode+"'";
        }
        
        statement += ") QRSLT WHERE ORGANIZATION_ID = "+orgId+" AND (( '"+isHqc+"' = 'Y' AND "
                + "( ( (SELECT NVL ( fnd_profile.VALUE ( 'XXX_SCM_EAM_SUPERVISOR'), 'N') FROM DUAL) "
                + "= 'Y' AND (SELECT NVL ( fnd_profile.VALUE ( 'XXX_EAM_IGATE_SUPERUSER'), 'N') FROM DUAL)"
                + " = 'N' OR ( (SELECT NVL ( fnd_profile.VALUE ( 'XXX_SCM_EAM_SUPERVISOR'), 'N') FROM DUAL) <> 'Y' "
                + "AND (SELECT NVL ( fnd_profile.VALUE ( 'XXX_EAM_IGATE_SUPERUSER'), 'N') FROM DUAL) = 'N' "
                + "AND qrslt.wip_entity_name IN (SELECT DISTINCT wip_entity_name FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V cii, "
                + "STC.XXX_STC_EAM_APPROVERS app, XXX_STC_EAM_APPROVERS_DEPT_V dept, FND_FLEX_VALUES_VL FV, "
                + "FND_FLEX_VALUE_SETS FVS, FND_FLEX_VALUES_VL FV1, FND_FLEX_VALUE_SETS FVS1, BOM_DEPARTMENTS BD, "
                + "org_organization_definitions org, wip_discrete_jobs wdj, wip_entities we WHERE FV.FLEX_VALUE_SET_ID"
                + " = FVS.FLEX_VALUE_SET_ID AND FVS.FLEX_VALUE_SET_NAME = 'XXSCM_Zone' AND FV.FLEX_VALUE = "
                + "cii.attribute1 AND dept.zone_id = FV.FLEX_VALUE_ID AND dept.APPROVER_DEPT_ID = "
                + "app.APPROVER_DEPT_ID AND org.organization_id = dept.organization_id AND org.organization_code "
                + "= 'E00' AND wdj.OWNING_DEPARTMENT = bd.department_id AND bd.attribute1 = "
                + "fv1.flex_value AND fv1.flex_value_id = dept.department_id AND fv1.flex_value_set_id = "
                + "fvs1.flex_value_set_id AND fvs1.flex_value_set_name = 'XXSCM_Departments' AND dept.position_name IN "
                + "('ABFM', 'BFM') AND wdj.wip_entity_id = we.wip_entity_id AND we.wip_entity_name = "
                + "qrslt.wip_entity_name AND wdj.wip_entity_id IN (SELECT wip_entity_id FROM wip_eam_work_requests WHERE attribute15 IS NULL) "
                + "AND cii.instance_number = (SELECT CASE WHEN INSTR ( SUBSTR ( C.INSTANCE_NUMBER, 8, 5), '-') BETWEEN 1 AND 4 THEN RTRIM "
                + "( SUBSTR ( C.INSTANCE_NUMBER, 1, 10), '-') WHEN INSTR ( SUBSTR ( C.INSTANCE_NUMBER, 8, 5), '-') > 4 THEN RTRIM ( "
                + "SUBSTR ( C.INSTANCE_NUMBER, 1, 11), '-') ELSE C.INSTANCE_NUMBER END FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V c WHERE "
                + "CURRENT_ORGANIZATION_ID = QRSLT.organization_id AND c.serial_number = (SELECT asset_number FROM wip_discrete_jobs wdj1, "
                + "wip_entities we1 WHERE wdj1.wip_entity_id = we1.wip_entity_id AND we1.wip_entity_name = qrslt.wip_entity_name "
                + "AND we1.ORGANIZATION_ID = QRSLT.organization_id)) AND app.user_id = (SELECT TO_CHAR ( fnd_profile.VALUE ( 'USER_ID')) FROM DUAL)))) "
                + "OR ( (SELECT NVL ( fnd_profile.VALUE ( 'XXX_EAM_IGATE_SUPERUSER'), 'N') FROM DUAL) = 'Y' AND qrslt.wip_entity_name IN "
                + "(SELECT DISTINCT wip_entity_name FROM wip_discrete_jobs wdj, wip_entities we WHERE wdj.wip_entity_id = we.wip_entity_id AND we.wip_entity_name = "
                + "qrslt.wip_entity_name AND wdj.wip_entity_id IN (SELECT wip_entity_id FROM wip_eam_work_requests WHERE attribute15 IS NOT NULL))) "
                + "OR ( (SELECT NVL ( fnd_profile.VALUE ( 'XXX_EAM_CONT_SUPERUSER'), 'N') FROM DUAL) = 'Y' OR "
                + "( (SELECT NVL ( fnd_profile.VALUE ( 'XXX_EAM_IGATE_SUPERUSER'), 'N') FROM DUAL) = 'N' "
                + "AND (SELECT NVL ( fnd_profile.VALUE ( 'XXX_EAM_CONT_SUPERUSER'), 'N') FROM DUAL) <> 'Y' "
                + "AND qrslt.wip_entity_name IN (SELECT DISTINCT wip_entity_name FROM STC.XXX_STC_EAM_APPROVERS app,"
                + " XXX_STC_EAM_APPROVERS_DEPT_V dept, org_organization_definitions org, wip_discrete_jobs wdj,"
                + " wip_entities we WHERE dept.APPROVER_DEPT_ID = app.APPROVER_DEPT_ID AND org.organization_id = dept.organization_id "
                + "AND org.organization_code = 'E00' AND dept.position_name = 'CONT_SPEC_ENGINEER' AND DEPT.SUB_DEPARTMENT_ID = "
                + "wdj.OWNING_DEPARTMENT AND wdj.wip_entity_id = we.wip_entity_id AND we.wip_entity_name = qrslt.wip_entity_name "
                + "AND app.user_id = (SELECT TO_CHAR ( fnd_profile.VALUE ( 'USER_ID')) FROM DUAL)))))) OR ( '"+isHqc+"' = 'N' "
                + "AND ( (SELECT NVL ( fnd_profile.VALUE ( 'XXX_EAM_DIST_CONT_SUPERUSER'), 'N') FROM DUAL) = 'Y' "
                + "OR ( (SELECT NVL ( fnd_profile.VALUE ( 'XXX_EAM_DIST_CONT_SUPERUSER'), 'N') FROM DUAL) <> 'Y' "
                + "AND qrslt.wip_entity_name IN (SELECT DISTINCT wip_entity_name FROM FND_FLEX_VALUES_VL FV, "
                + "FND_FLEX_VALUE_SETS FVS, wip_discrete_jobs wdj, wip_entities we WHERE FV.FLEX_VALUE_SET_ID = FVS.FLEX_VALUE_SET_ID "
                + "AND FVS.FLEX_VALUE_SET_NAME = 'XX_EAM_DIST_CONT_WO_STATUS' AND FV.FLEX_VALUE = wdj.attribute15 AND wdj.wip_entity_id = we.wip_entity_id))))) AND ROWNUM <= 200";

        if (workOrderName != null && !workOrderName.isEmpty()) {
            statement += " AND (WIP_ENTITY_NAME LIKE UPPER('%"+workOrderName.toUpperCase() +"%') OR WIP_ENTITY_NAME IS NULL)";
        }
        if (assetNumber != null && !assetNumber.isEmpty()) {
            statement += " AND (ASSET_NUMBER = '"+ assetNumber +"' OR ASSET_NUMBER IS NULL)";
        }
        if (assetDescription != null && !assetDescription.isEmpty()) {
            statement += " AND (ASSET_DESCRIPTION LIKE '"+assetDescription+"' OR ASSET_DESCRIPTION IS NULL)";
        }
        if (owningDepartment != null && !owningDepartment.isEmpty()) {
            statement += " AND (OWNING_DEPARTMENT_ID LIKE '"+owningDepartment+"' OR OWNING_DEPARTMENT_ID IS NULL)";
        }
        if (startDateFrom != null) {
//            statement += " AND (CREATION_DATE >= :startDate )";
            statement += " AND (SCHEDULED_START_DATE >= TO_DATE('"+startDateFrom+"','YYYY-MM-DD'))";
        }
        if (startDateTo != null) {
            statement += " AND (SCHEDULED_START_DATE <= TO_DATE('"+startDateTo+"','YYYY-MM-DD')+1 )";
        }
        if (status != null && !status.isEmpty()) {
            statement += " AND (STATUS_TYPE LIKE '"+status+"' OR STATUS_TYPE IS NULL)";
        }
        if (planned != null && !planned.isEmpty()) {
            statement += " AND (PLAN_MAINTENANCE LIKE '"+planned+"') ";
        }
        if (requestNumber != null && !requestNumber.isEmpty()) {
            statement += " AND  (REQUEST_NUMBER LIKE UPPER('%"+requestNumber+"%') AND REQUEST_NUMBER IS NOT NULL) ";
        }

        if(CREATED_BY!=null && !CREATED_BY.isEmpty()){
            statement += " AND (CREATED_BY = '"+CREATED_BY+"' OR CREATED_BY IS NULL) ";
        }
        if(buildingType!=null && !buildingType.isEmpty()){
            statement += "and building_type = '"+buildingType+"'";
        }
        if(dasboardSearch!=null && !dasboardSearch.isEmpty()){
            statement += "       AND (   ('"+dasboardSearch+"' = 'WO')\n" +
"            OR (nvl(PLAN_MAINTENANCE,'N') = 'Y' AND '"+dasboardSearch+"' = 'WO-PMR')\n" +
"            OR (nvl(PLAN_MAINTENANCE,'N') = 'N' AND '"+dasboardSearch+"' = 'WO-CMR')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('A1', 'INP1', 'R1')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-CREATION')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('A2', 'INP2', 'R2')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-SPR')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('A3', 'INP3', 'R3')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-COMPLETE')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND (wfstatus = 'DRAFT' OR status = 'Cancelled')\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-OTHER')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('A1')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-CREATION-A')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('INP1')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-CREATION-INP')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('R1')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-CREATION-R')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('A2')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-SPR-A')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('INP2')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-SPR-INP')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('R2')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-SPR-R')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('A3')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-COMPLETE-A')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('INP3')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-COMPLETE-INP')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus IN ('R3')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-COMPLETE-R')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND wfstatus NOT IN ('DRAFT')\n" +
"                AND status <> 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-OTHER')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND (wfstatus = 'DRAFT' AND status <> 'Cancelled')\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-OTHER-DRAFT')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'N'\n" +
"                AND status = 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-CMR-OTHER-CANCELED')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'Y'\n" +
"                AND status <> 'Cancelled'\n" +
"                AND wfstatus = 'A1'\n" +
"                AND '"+dasboardSearch+"' = 'WO-PMR-CREATION')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'Y'\n" +
"                AND status <> 'Cancelled'\n" +
"                AND wfstatus IN ('A3', 'INP3', 'R3')\n" +
"                AND '"+dasboardSearch+"' = 'WO-PMR-COMPLETION')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'Y'\n" +
"                AND status <> 'Cancelled'\n" +
"                AND wfstatus IN ('A3')\n" +
"                AND '"+dasboardSearch+"' = 'WO-PMR-COMPLETION-A')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'Y'\n" +
"                AND status <> 'Cancelled'\n" +
"                AND wfstatus IN ('INP3')\n" +
"                AND '"+dasboardSearch+"' = 'WO-PMR-COMPLETION-INP3')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'Y'\n" +
"                AND status <> 'Cancelled'\n" +
"                AND wfstatus IN ('R3')\n" +
"                AND '"+dasboardSearch+"' = 'WO-PMR-COMPLETION-R3')\n" +
"            OR (    nvl(PLAN_MAINTENANCE,'N') = 'Y'\n" +
"                AND status = 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'WO-PMR-OTHER'))";
        }

       
        
        

//        try {
//            System.out.println("Search WO Try ");
//            connection  = pool.getConnection();
//            connection.setAutoCommit(false);
//
//            namedParameterStatement = new NamedParameterStatement(connection,statement);
//            namedParameterStatement.setFetchSize(1000);
//            namedParameterStatement.setInt("orgId",orgId);
//            namedParameterStatement.setInt("orgId1",orgId);
//            namedParameterStatement.setString("isHqc", isHqc);
//
//            System.out.println(statement);
//            if (workOrderName != null && !workOrderName.isEmpty()) {
//                namedParameterStatement.setString("workOrderName", "%" + workOrderName + "%");
//            }
//            if (assetNumber != null && !assetNumber.isEmpty()) {
//                namedParameterStatement.setString("assetNumber", assetNumber );
//            }
//            if (assetDescription != null && !assetDescription.isEmpty()) {
//                namedParameterStatement.setString("assetDescription", "%" + assetDescription + "%");
//            }
//            if (owningDepartment != null && !owningDepartment.isEmpty()) {
//                namedParameterStatement.setString("owningDepartment", "%" + owningDepartment + "%");
//            }
//            if (startDateFrom!=null) {
//                namedParameterStatement.setDate("startDate", startDateFrom);
//            }
//            if (startDateTo!=null) {
//                namedParameterStatement.setDate("endDate", startDateTo);
//            }
//            if (status != null && !status.isEmpty()) {
//                namedParameterStatement.setString("status", "%" + status + "%");
//            }
//            if (planned != null && !planned.isEmpty()) {
//                if (planned.equalsIgnoreCase("yes")) {
//                    planned = "Y";
//                }
//                if (planned.equalsIgnoreCase("no")) {
//                    planned = "N";
//                }
//                if (planned.equalsIgnoreCase("all")) {
//                    planned = "";
//                }
//                namedParameterStatement.setString("planned", "%" + planned + "%");
//            }
//            if (requestNumber != null && !requestNumber.isEmpty()) {
//                namedParameterStatement.setString("requestNumber", "%" + requestNumber + "%");
//            }
//
//            if(CREATED_BY!=null && !CREATED_BY.isEmpty()){
//                namedParameterStatement.setString("CREATED_BY", CREATED_BY);
//            }
//            if(buildingType!=null && !buildingType.isEmpty()){
//                namedParameterStatement.setString("building_type", buildingType);
//            }
//            
//
//            
//
//            //System.out.println(statement);
//            resultSet = namedParameterStatement.executeQuery();
//            //System.out.println(statement);
//            resultSet.setFetchSize(500);
//            JsonObject jsonResult = new JsonObject();
//            JsonArray jsonArray = new JsonArray();
//
//            while (resultSet.next()){
//
//                if(approvalType.equalsIgnoreCase("SPR") && resultSet.getString("CLAA").equalsIgnoreCase("Y"))  {
//                //System.out.println("if SPR && CLAA");
//                JsonObject jsonObject = new JsonObject();
//                jsonObject.addProperty("WIP_ENTITY_ID", resultSet.getString("WIP_ENTITY_ID"));
//                jsonObject.addProperty("WIP_ENTITY_NAME", resultSet.getString("WIP_ENTITY_NAME"));
//                jsonObject.addProperty("ASSET_NUMBER", resultSet.getString("ASSET_NUMBER"));
//                jsonObject.addProperty("ASSET_DESCRIPTION", resultSet.getString("ASSET_DESCRIPTION"));
//                jsonObject.addProperty("OWNING_DEPARTMENT", resultSet.getString("OWNING_DEPARTMENT"));
//                jsonObject.addProperty("CLAA", resultSet.getString("CLAA"));
//                jsonObject.addProperty("SCHEDULED_START_DATE", resultSet.getString("SCHEDULED_START_DATE"));
//                jsonObject.addProperty("SCHEDULED_COMPLETION_DATE", resultSet.getString("SCHEDULED_COMPLETION_DATE"));
//                jsonObject.addProperty("STATUS", resultSet.getString("STATUS"));
//                jsonObject.addProperty("DESCRIPTION", resultSet.getString("DESCRIPTION"));
//                jsonObject.addProperty("WFSTATUS", resultSet.getString("WFSTATUS"));
//                jsonObject.addProperty("WFSTATUSMEANING", resultSet.getString("WFSTATUSMEANING"));
//                jsonObject.addProperty("WOSTATUS", resultSet.getString("WOSTATUS"));
//                jsonObject.addProperty("WOSTATUSMEANING", resultSet.getString("WOSTATUSMEANING"));
//                jsonObject.addProperty("OWNING_DEPARTMENT_ID", resultSet.getString("OWNING_DEPARTMENT_ID"));
//
//                    jsonObject.addProperty("department_desc", resultSet.getString("department_desc"));
//                    jsonObject.addProperty("Invoice_Status", resultSet.getString("Invoice_Status"));
//                    jsonObject.addProperty("Invoice_date",resultSet.getString("Invoice_date"));
//                    jsonObject.addProperty("Invoice_Month", resultSet.getString("Invoice_Month"));
//                    jsonObject.addProperty("Grace_Period", resultSet.getString("Grace_Period"));
//                    jsonObject.addProperty("Reason", resultSet.getString("Reason"));
//                    jsonObject.addProperty("asset_activity", resultSet.getString("asset_activity"));
//                    jsonObject.addProperty("sparepartlines",resultSet.getString("sparepartlines"));
//                    jsonObject.addProperty("Priority",resultSet.getString("Priority"));
//                    jsonObject.addProperty("date_released",resultSet.getString("date_released"));
//                    jsonObject.addProperty("date_completed",resultSet.getString("date_completed"));
//                    jsonObject.addProperty("spTotal",resultSet.getString("spTotal"));
//                    // 
//                    
//                String planMaintenance = resultSet.getString("PLAN_MAINTENANCE");
//                if (planMaintenance == null) {
//                    planMaintenance = "null";
//                }
//                else {
//                    if (planMaintenance.equals("Y")) {
//                        planMaintenance = "Yes";
//                    }
//                    if (planMaintenance.equals("N")) {
//                        planMaintenance = "No";
//                    }
//                }
//                jsonObject.addProperty("PLANNED", planMaintenance);
//                jsonObject.addProperty("REQUEST_NUMBER", resultSet.getString("REQUEST_NUMBER"));
//                jsonArray.add(jsonObject);
//                }
//                else if(approvalType.equalsIgnoreCase("COMPLETE") && resultSet.getString("CLAA").equalsIgnoreCase("N"))  {
//                    JsonObject jsonObject = new JsonObject();
//                    jsonObject.addProperty("WIP_ENTITY_ID", resultSet.getString("WIP_ENTITY_ID"));
//                    jsonObject.addProperty("WIP_ENTITY_NAME", resultSet.getString("WIP_ENTITY_NAME"));
//                    jsonObject.addProperty("ASSET_NUMBER", resultSet.getString("ASSET_NUMBER"));
//                    jsonObject.addProperty("ASSET_DESCRIPTION", resultSet.getString("ASSET_DESCRIPTION"));
//                    jsonObject.addProperty("OWNING_DEPARTMENT", resultSet.getString("OWNING_DEPARTMENT"));
//                    jsonObject.addProperty("CLAA", resultSet.getString("CLAA"));
//                    jsonObject.addProperty("SCHEDULED_START_DATE", resultSet.getString("SCHEDULED_START_DATE"));
//                    jsonObject.addProperty("SCHEDULED_COMPLETION_DATE", resultSet.getString("SCHEDULED_COMPLETION_DATE"));
//                    jsonObject.addProperty("STATUS", resultSet.getString("STATUS"));
//                    jsonObject.addProperty("DESCRIPTION", resultSet.getString("DESCRIPTION"));
//                    jsonObject.addProperty("WFSTATUS", resultSet.getString("WFSTATUS"));
//                    jsonObject.addProperty("WFSTATUSMEANING", resultSet.getString("WFSTATUSMEANING"));
//                    jsonObject.addProperty("WOSTATUS", resultSet.getString("WOSTATUS"));
//                    jsonObject.addProperty("WOSTATUSMEANING", resultSet.getString("WOSTATUSMEANING"));
//                    jsonObject.addProperty("OWNING_DEPARTMENT_ID", resultSet.getString("OWNING_DEPARTMENT_ID"));
//
//                    jsonObject.addProperty("department_desc", resultSet.getString("department_desc"));
//                    jsonObject.addProperty("Invoice_Status", resultSet.getString("Invoice_Status"));
//                    jsonObject.addProperty("Invoice_date",resultSet.getString("Invoice_date"));
//                    jsonObject.addProperty("Invoice_Month", resultSet.getString("Invoice_Month"));
//                    jsonObject.addProperty("Grace_Period", resultSet.getString("Grace_Period"));
//                    jsonObject.addProperty("Reason", resultSet.getString("Reason"));
//                    jsonObject.addProperty("asset_activity", resultSet.getString("asset_activity"));
//                    jsonObject.addProperty("sparepartlines",resultSet.getString("sparepartlines"));
//                    jsonObject.addProperty("Priority",resultSet.getString("Priority"));
//                    jsonObject.addProperty("date_released",resultSet.getString("date_released"));
//                    jsonObject.addProperty("date_completed",resultSet.getString("date_completed"));
//                    jsonObject.addProperty("spTotal",resultSet.getString("spTotal"));
//
//                    String planMaintenance = resultSet.getString("PLAN_MAINTENANCE");
//                    if (planMaintenance == null) {
//                        planMaintenance = "null";
//                    }
//                    else {
//                        if (planMaintenance.equals("Y")) {
//                            planMaintenance = "Yes";
//                        }
//                        if (planMaintenance.equals("N")) {
//                            planMaintenance = "No";
//                        }
//                    }
//                    jsonObject.addProperty("PLANNED", planMaintenance);
//                    jsonObject.addProperty("REQUEST_NUMBER", resultSet.getString("REQUEST_NUMBER"));
//                    jsonArray.add(jsonObject);
//                }  else   {
//                    JsonObject jsonObject = new JsonObject();
//                    jsonObject.addProperty("WIP_ENTITY_ID", resultSet.getString("WIP_ENTITY_ID"));
//                    jsonObject.addProperty("WIP_ENTITY_NAME", resultSet.getString("WIP_ENTITY_NAME"));
//                    jsonObject.addProperty("ASSET_NUMBER", resultSet.getString("ASSET_NUMBER"));
//                    jsonObject.addProperty("ASSET_DESCRIPTION", resultSet.getString("ASSET_DESCRIPTION"));
//                    jsonObject.addProperty("OWNING_DEPARTMENT", resultSet.getString("OWNING_DEPARTMENT"));
//                    jsonObject.addProperty("CLAA", resultSet.getString("CLAA"));
//                    jsonObject.addProperty("SCHEDULED_START_DATE", resultSet.getString("SCHEDULED_START_DATE"));
//                    jsonObject.addProperty("SCHEDULED_COMPLETION_DATE", resultSet.getString("SCHEDULED_COMPLETION_DATE"));
//                    jsonObject.addProperty("STATUS", resultSet.getString("STATUS"));
//                    jsonObject.addProperty("DESCRIPTION", resultSet.getString("DESCRIPTION"));
//                    jsonObject.addProperty("WFSTATUS", resultSet.getString("WFSTATUS"));
//                    jsonObject.addProperty("WFSTATUSMEANING", resultSet.getString("WFSTATUSMEANING"));
//                    jsonObject.addProperty("WOSTATUS", resultSet.getString("WOSTATUS"));
//                    jsonObject.addProperty("WOSTATUSMEANING", resultSet.getString("WOSTATUSMEANING"));
//                    jsonObject.addProperty("OWNING_DEPARTMENT_ID", resultSet.getString("OWNING_DEPARTMENT_ID"));
//
//                    jsonObject.addProperty("department_desc", resultSet.getString("department_desc"));
//                    jsonObject.addProperty("Invoice_Status", resultSet.getString("Invoice_Status"));
//                    jsonObject.addProperty("Invoice_date",resultSet.getString("Invoice_date"));
//                    jsonObject.addProperty("Invoice_Month", resultSet.getString("Invoice_Month"));
//                    jsonObject.addProperty("Grace_Period", resultSet.getString("Grace_Period"));
//                    jsonObject.addProperty("Reason", resultSet.getString("Reason"));
//                    jsonObject.addProperty("asset_activity", resultSet.getString("asset_activity"));
//                    jsonObject.addProperty("sparepartlines",resultSet.getString("sparepartlines"));
//                    jsonObject.addProperty("Priority",resultSet.getString("Priority"));
//                    jsonObject.addProperty("date_released",resultSet.getString("date_released"));
//                    jsonObject.addProperty("date_completed",resultSet.getString("date_completed"));
//                    jsonObject.addProperty("spTotal",resultSet.getString("spTotal"));
//
//                    String planMaintenance = resultSet.getString("PLAN_MAINTENANCE");
//                    if (planMaintenance == null) {
//                        planMaintenance = "null";
//                    }
//                    else {
//                        if (planMaintenance.equals("Y")) {
//                            planMaintenance = "Yes";
//                        }
//                        if (planMaintenance.equals("N")) {
//                            planMaintenance = "No";
//                        }
//                    }
//                    jsonObject.addProperty("PLANNED", planMaintenance);
//                    jsonObject.addProperty("REQUEST_NUMBER", resultSet.getString("REQUEST_NUMBER"));
//                    jsonArray.add(jsonObject);
//                }
//            }
//            jsonResult.add("WORK_ORDERS",jsonArray);
//            result = jsonResult.toString();
//            System.out.println(result);
//            connection.close();
//            namedParameterStatement.close();
//            resultSet.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Get Work Orders Error",e);
//        }
//        finally {
//            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
//        }
//        return result;
        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_USER_ID", userID);
            param.put("P_RESP_ID", respId);
            param.put("P_RESP_APPL_ID", respAppId);
            seo.setParameter(param);

            

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "WORK_ORDERS");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("WORK_ORDERS");

            JSONObject jsonResult = new JSONObject();
            JSONArray arrayResult = new JSONArray();

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject1 = new JSONObject();

                     jsonObject1.put("WIP_ENTITY_ID", jsonArray.getJSONObject(i).get("WIP_ENTITY_ID"));
                jsonObject1.put("WIP_ENTITY_NAME", jsonArray.getJSONObject(i).get("WIP_ENTITY_NAME"));
                jsonObject1.put("ASSET_NUMBER", jsonArray.getJSONObject(i).get("ASSET_NUMBER"));
                jsonObject1.put("ASSET_DESCRIPTION", jsonArray.getJSONObject(i).get("ASSET_DESCRIPTION"));
                jsonObject1.put("OWNING_DEPARTMENT", jsonArray.getJSONObject(i).get("OWNING_DEPARTMENT"));
                jsonObject1.put("CLAA", jsonArray.getJSONObject(i).get("CLAA"));
                jsonObject1.put("SCHEDULED_START_DATE", jsonArray.getJSONObject(i).get("F_SCHEDULED_START_DATE"));
                jsonObject1.put("SCHEDULED_COMPLETION_DATE", jsonArray.getJSONObject(i).get("F_SCHEDULED_COMPLETION_DATE"));
                jsonObject1.put("STATUS", jsonArray.getJSONObject(i).get("STATUS"));
                jsonObject1.put("DESCRIPTION", jsonArray.getJSONObject(i).get("DESCRIPTION"));
                jsonObject1.put("WFSTATUS", jsonArray.getJSONObject(i).get("WFSTATUS"));
                jsonObject1.put("WFSTATUSMEANING", jsonArray.getJSONObject(i).get("WFSTATUSMEANING"));
                jsonObject1.put("WOSTATUS", jsonArray.getJSONObject(i).get("WOSTATUS"));
                jsonObject1.put("WOSTATUSMEANING", jsonArray.getJSONObject(i).get("WOSTATUSMEANING"));
                jsonObject1.put("OWNING_DEPARTMENT_ID", jsonArray.getJSONObject(i).get("OWNING_DEPARTMENT_ID"));

                    jsonObject1.put("DEPARTMENT_DESC", jsonArray.getJSONObject(i).get("DEPARTMENT_DESC"));
                    
                    jsonObject1.put("INVOICE_STATUS", jsonArray.getJSONObject(i).get("INVOICE_STATUS"));
                    jsonObject1.put("INVOICE_DATE",jsonArray.getJSONObject(i).get("F_INVOICE_DATE"));
                    jsonObject1.put("INVOICE_MONTH", jsonArray.getJSONObject(i).get("INVOICE_MONTH"));
                    jsonObject1.put("GRACE_PERIOD", jsonArray.getJSONObject(i).get("GRACE_PERIOD"));
                    jsonObject1.put("REASON", jsonArray.getJSONObject(i).get("REASON"));
                    jsonObject1.put("ASSET_ACTIVITY", jsonArray.getJSONObject(i).get("ASSET_ACTIVITY"));
                    jsonObject1.put("SPAREPARTLINES",jsonArray.getJSONObject(i).get("SPAREPARTLINES"));
                    jsonObject1.put("PRIORITY",jsonArray.getJSONObject(i).get("PRIORITY"));
                    jsonObject1.put("DATE_RELEASED",jsonArray.getJSONObject(i).get("F_DATE_RELEASED"));
                    jsonObject1.put("DATE_COMPLETED",jsonArray.getJSONObject(i).get("F_DATE_COMPLETED"));
                    jsonObject1.put("SPTOTAL",jsonArray.getJSONObject(i).get("SPTOTAL"));
                    jsonObject1.put("ATTACHMENTS_COUNT",jsonArray.getJSONObject(i).get("ATTACHMENTS_COUNT"));
                    // 
                    

                Object planMaintenance = jsonArray.getJSONObject(i).get("PLAN_MAINTENANCE");
                if (planMaintenance == null) {
                    planMaintenance = "null";
                }
                else {
                    if (planMaintenance.equals("Y")) {
                        planMaintenance = "Yes";
                    }
                    if (planMaintenance.equals("N")) {
                        planMaintenance = "No";
                    }
                }
                jsonObject1.put("PLAN_MAINTENANCE", planMaintenance);
                jsonObject1.put("REQUEST_NUMBER", jsonArray.getJSONObject(i).get("REQUEST_NUMBER"));

//                jsonObject1.put("WORK_REQUEST_ID", jsonArray.getJSONObject(i).get("WORK_REQUEST_ID"));
                    
                

                arrayResult.put(jsonObject1);
            }

            jsonResult.put("WORK_ORDERS", arrayResult);

            result = jsonResult.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("SOA Error", e);
            logger.error("QUERY : " + statement);
        }
        System.out.println("//getWorkeOrder Return Result");
        System.out.println(result);
        System.out.println(statement);
        return result;
    }

    public String getAssetLocation(String assetNumber) {

        String result = null;

        String statement = "select decode(cii.location_type_code, 'HZ_PARTY_SITES',(SELECT hz.address1||','||hz.address2||','||hz.address3||','||hz.address4||','||hz.city||','||hz.state||','||hz.postal_code||','||hz.country \n" +
                "FROM APPS.hz_locations hz,APPS.hz_party_sites hps WHERE hz.location_id = hps.location_id and hps.party_site_id = cii.location_id), 'HZ_LOCATIONS',(SELECT hz.address1||','||hz.address2||','||hz.address3||','||hz.address4||','||hz.city||','||hz.state||','||hz.postal_code||','||hz.country \n" +
                "FROM APPS.hz_locations hz WHERE hz.location_id = cii.location_id), 'VENDOR_SITE',(SELECT pvs.address_line1||','||pvs.address_line2||','||pvs.address_line3||','||pvs.city||','||pvs.state||','||pvs.zip||','||pvs.country FROM APPS.po_vendor_sites_all pvs WHERE pvs.vendor_site_id = cii.location_id), 'INVENTORY', \n" +
                "(SELECT hrl.address_line_1||','||hrl.address_line_2||','||hrl.address_line_3||','||hrl.region_1||','||hrl.postal_code||','||hrl.country FROM APPS.hr_locations_all hrl WHERE hrl.location_id = cii.location_id), 'INTERNAL_SITE',\n" +
                "(SELECT hrl.address_line_1||','||hrl.address_line_2||','||hrl.address_line_3||','||hrl.region_1||','||hrl.postal_code||','||hrl.country FROM APPS.hr_locations_all hrl WHERE hrl.location_id = cii.location_id), 'WIP',\n" +
                "(SELECT hz.address1||','||hz.address2||','||hz.address3||','||hz.address4||','||hz.city||','||hz.state||','||hz.postal_code||','||hz.country FROM APPS.hz_locations hz WHERE hz.location_id = cii.location_id), 'PROJECT',\n" +
                "(SELECT hz.address1||','||hz.address2||','||hz.address3||','||hz.address4||','||hz.city||','||hz.state||','||hz.postal_code||','||hz.country FROM APPS.hz_locations hz WHERE hz.location_id = cii.location_id), 'IN_TRANSIT',\n" +
                "(SELECT hz.address1||','||hz.address2||','||hz.address3||','||hz.address4||','||hz.city||','||hz.state||','||hz.postal_code||','||hz.country FROM APPS.hz_locations hz WHERE hz.location_id = cii.location_id), 'PO',\n" +
                "(SELECT hz.address1||','||hz.address2||','||hz.address3||','||hz.address4||','||hz.city||','||hz.state||','||hz.postal_code||','||hz.country from APPS.hz_locations hz where hz.location_id = cii.location_id), null) \n" +
                "current_location, cii.instance_id , (select inst_latitude from apps.csi_ii_geolocations cig where cig.instance_id = cii.instance_id) as latitude, \n" +
                "(select inst_longitude from apps.csi_ii_geolocations cig where cig.instance_id = cii.instance_id) as longitude FROM APPS.csi_item_instances cii \n" +
                "where cii.instance_id = (select instance_id from APPS.csi_item_instances where instance_number = ?)";
        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, assetNumber);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("CURRENT_LOCATION",resultSet.getString("CURRENT_LOCATION"));
                jsonObject.addProperty("LATITUDE",resultSet.getDouble("LATITUDE"));
                jsonObject.addProperty("LONGITUDE",resultSet.getDouble("LONGITUDE"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ASSET_LOCATION",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Asset Location Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWorkOrderDetails(int entityId) {

        String result = null;

        String statement = "SELECT WEWODV.*,EWODV.*,WDJ.*, (SELECT count(ATTACHED_DOCUMENT_ID) FROM apps.fnd_attached_documents fad where fad.PK2_VALUE = TO_CHAR(EWODV.WIP_ENTITY_ID)  AND fad.entity_name='EAM_WORK_ORDERS')  attachments_count,\n" +
"                (select ML.MEANING from APPS.mfg_lookups ML WHERE ML.LOOKUP_TYPE = 'WIP_EAM_WORK_ORDER_TYPE' AND ML.LOOKUP_CODE = WEWODV.WORK_ORDER_TYPE) NEW_WORK_ORDER_TYPE,\n" +
"                (SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'WIP_EAM_ACTIVITY_PRIORITY' AND ML.LOOKUP_CODE = WEWODV.PRIORITY) NEW_PRIORITY,\n" +
"                (SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_TYPE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_TYPE) ACTIVITY_TYPE_NAME,\n" +
"                (SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_CAUSE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_CAUSE) ACTIVITY_CAUSE_NAME,\n" +
"                (SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_SOURCE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_SOURCE) ACTIVITY_SOURCE_NAME,\n" +
"                round((WDJ.SCHEDULED_COMPLETION_DATE - WDJ.SCHEDULED_START_DATE) * 24, 3) AS duration, WDJ.SCHEDULED_COMPLETION_DATE, WDJ.DATE_RELEASED\n" +
"                FROM APPS.wip_eam_work_order_dtls_v WEWODV, APPS.WIP_DISCRETE_JOBS WDJ, APPS.eam_work_order_details_v EWODV WHERE\n" +
"                WEWODV.WIP_ENTITY_ID = WDJ.WIP_ENTITY_ID AND WDJ.WIP_ENTITY_ID = EWODV.WIP_ENTITY_ID AND EWODV.WIP_ENTITY_ID = ?";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1,entityId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){

                JsonObject woDetails = new JsonObject();
                woDetails.addProperty("ENTITY_ID",resultSet.getString("WIP_ENTITY_ID"));
                woDetails.addProperty("ENTITY_NAME",resultSet.getString("WIP_ENTITY_NAME"));
                woDetails.addProperty("ASSET_NUMBER",resultSet.getString("ASSET_NUMBER"));
                woDetails.addProperty("ASSET_GROUP",resultSet.getString("ASSET_GROUP"));
                woDetails.addProperty("ASSET_GROUP_ID",resultSet.getString("ASSET_GROUP_ID"));
                woDetails.addProperty("OWNING_DEPARTMENT_ID",resultSet.getString("OWNING_DEPARTMENT_ID"));
                woDetails.addProperty("OWNING_DEPARTMENT",resultSet.getString("OWNING_DEPARTMENT"));
                woDetails.addProperty("ASSET_CATEGORY",resultSet.getString("ASSET_CATEGORY"));
                woDetails.addProperty("ASSET_LOCATION",resultSet.getString("ASSET_LOCATION"));
                woDetails.addProperty("ASSET_ACTIVITY_ID",resultSet.getString("ASSET_ACTIVITY_ID"));
                woDetails.addProperty("ASSET_ACTIVITY",resultSet.getString("ASSET_ACTIVITY"));
                woDetails.addProperty("ASSET_OWNING_DEPARTMENT",resultSet.getString("ASSET_OWNING_DEPARTMENT"));
                woDetails.addProperty("ASSET_OWNING_DEPARTMENT_ID",resultSet.getString("OWNING_DEPARTMENT_ID"));
                woDetails.addProperty("MAINTENANCE_OBJECT_ID",resultSet.getString("MAINTENANCE_OBJECT_ID"));
                woDetails.addProperty("STATUS",resultSet.getString("WORK_ORDER_STATUS_PENDING"));
                woDetails.addProperty("SYSTEM_STATUS",resultSet.getString("STATUS"));
                woDetails.addProperty("STATUS_ID",resultSet.getString("STATUS_TYPE"));
                woDetails.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                woDetails.addProperty("START_DATE",resultSet.getString("START_DATE"));
                woDetails.addProperty("COMPLETION_DATE",resultSet.getString("COMPLETION_DATE"));
                woDetails.addProperty("WORK_ORDER_TYPE",resultSet.getString("NEW_WORK_ORDER_TYPE"));
                woDetails.addProperty("WORK_ORDER_TYPE_ID",resultSet.getString("WORK_ORDER_TYPE"));
                woDetails.addProperty("PRIORITY",resultSet.getString("NEW_PRIORITY"));
                woDetails.addProperty("PRIORITY_ID",resultSet.getString("PRIORITY"));
                woDetails.addProperty("SCHEDULED_COMPLETION_DATE",resultSet.getString("SCHEDULED_COMPLETION_DATE"));
                woDetails.addProperty("DURATION",resultSet.getString("DURATION"));
                woDetails.addProperty("ATTACHMENTS_COUNT",resultSet.getString("ATTACHMENTS_COUNT"));
                int firmPlannedFlag = resultSet.getInt("FIRM_PLANNED_FLAG");
                if (firmPlannedFlag != 0) {
                    if (firmPlannedFlag == 1) {
                        woDetails.addProperty("FIRM_PLANNED_FLAG", "Yes");
                    }
                    else if (firmPlannedFlag == 2) {
                        woDetails.addProperty("FIRM_PLANNED_FLAG", "No");
                    }
                }
                else {
                    woDetails.addProperty("FIRM_PLANNED_FLAG", "null");
                }
                woDetails.addProperty("ACTIVITY_TYPE_ID",resultSet.getString("ACTIVITY_TYPE"));
                woDetails.addProperty("ACTIVITY_TYPE",resultSet.getString("ACTIVITY_TYPE_NAME"));
                woDetails.addProperty("ACTIVITY_CAUSE_ID",resultSet.getString("ACTIVITY_CAUSE"));
                woDetails.addProperty("ACTIVITY_CAUSE",resultSet.getString("ACTIVITY_CAUSE_NAME"));
                woDetails.addProperty("ACTIVITY_SOURCE_ID",resultSet.getString("ACTIVITY_SOURCE"));
                woDetails.addProperty("ACTIVITY_SOURCE",resultSet.getString("ACTIVITY_SOURCE_NAME"));
                String materialIssueByMO = resultSet.getString("MATERIAL_ISSUE_BY_MO");
                if (materialIssueByMO != null) {
                    if (materialIssueByMO.equals("Y")) {
                        woDetails.addProperty("MATERIAL_ISSUE_BY_MO", "Yes");
                    }
                    else if (materialIssueByMO.equals("N")) {
                        woDetails.addProperty("MATERIAL_ISSUE_BY_MO", "No");
                    }
                }
                else {
                    woDetails.addProperty("MATERIAL_ISSUE_BY_MO", "null");
                }
                woDetails.addProperty("MATERIAL_SHORTAGE",resultSet.getString("MATERIAL_SHORTAGE_DISP") + " As Of " +
                        resultSet.getString("MATERIAL_SHORTAGE_CHECK_DATE"));
                int statusType =  resultSet.getInt("STATUS_TYPE");
                String dateReleased = resultSet.getString("DATE_RELEASED");
                if ((statusType == 1 || statusType == 17 || statusType == 7) || (statusType == 6 && dateReleased == null)) {
                    woDetails.addProperty("IS_APPROVAL_STILL_REQUIRED","YES");
                }
                else{
                    woDetails.addProperty("IS_APPROVAL_STILL_REQUIRED","NO");
                }

                jsonArray.add(woDetails);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();

            JsonArray additionalDetails = getAdditionalDetails(entityId);
            JsonArray requests = getRequests(entityId);
            JsonArray operationRequirements = getWorkOrderOperations(entityId);
            JsonArray materialRequirements = getWorkOrderMaterialDirect(entityId);
            JsonArray workOrderApprovalHistory = getWorkOrderApprovalHistory(entityId);

            jsonResult.add("WORK_ORDER_DETAILS",jsonArray);
            jsonResult.add("ADDITIONAL_DETAILS",additionalDetails);
            jsonResult.add("REQUESTS", requests);
            jsonResult.add("OPERATION_REQUIREMENTS",operationRequirements);
            jsonResult.add("MATERIAL_REQUIREMENTS",materialRequirements);
            jsonResult.add("APPROVAL_HISTORY",workOrderApprovalHistory);

            result = jsonResult.toString();

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Details Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    /*//need to connect with fauji for this
    public String getWorkOrderSearchDetails(int entityId, boolean isHqc) {

        String result = null;
        String statement = null;
        if(isHqc)  {
        statement = "SELECT WEWODV.*,EWODV.*,WDJ.*, (select ML.MEANING from APPS.mfg_lookups ML WHERE ML.LOOKUP_TYPE = 'WIP_EAM_WORK_ORDER_TYPE' AND ML.LOOKUP_CODE = WEWODV.WORK_ORDER_TYPE) NEW_WORK_ORDER_TYPE,\n" +
                "(SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'WIP_EAM_ACTIVITY_PRIORITY' AND ML.LOOKUP_CODE = WEWODV.PRIORITY) NEW_PRIORITY,\n" +
                "(SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_TYPE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_TYPE) ACTIVITY_TYPE_NAME,\n" +
                "(SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_CAUSE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_CAUSE) ACTIVITY_CAUSE_NAME,\n" +
                "(SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_SOURCE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_SOURCE) ACTIVITY_SOURCE_NAME,\n" +
                "round((WDJ.SCHEDULED_COMPLETION_DATE - WDJ.SCHEDULED_START_DATE) * 24, 3) AS duration, WDJ.SCHEDULED_COMPLETION_DATE, WDJ.DATE_RELEASED\n" +
                "FROM APPS.wip_eam_work_order_dtls_v WEWODV, APPS.WIP_DISCRETE_JOBS WDJ, APPS.eam_work_order_details_v EWODV WHERE\n" +
                "WEWODV.WIP_ENTITY_ID = WDJ.WIP_ENTITY_ID AND WDJ.WIP_ENTITY_ID = EWODV.WIP_ENTITY_ID AND EWODV.WIP_ENTITY_ID = ?\n" +

                "AND ( ( (SELECT NVL ( fnd_profile.VALUE ( 'XXX_SCM_EAM_SUPERVISOR'), 'N') FROM DUAL) = 'Y' AND (SELECT NVL ( fnd_profile.VALUE (\n" +
               "'XXX_EAM_IGATE_SUPERUSER'), 'N') FROM DUAL) = 'N' OR ( (SELECT NVL (  fnd_profile.VALUE ( 'XXX_SCM_EAM_SUPERVISOR'),  'N')  FROM DUAL) <> 'Y' AND (SELECT NVL ( \n" +
                "fnd_profile.VALUE ( 'XXX_EAM_IGATE_SUPERUSER'),  'N')  FROM DUAL) = 'N' AND qrslt.wip_entity_name IN  (SELECT DISTINCT wip_entity_name  FROM \n" +
                "XX_MTL_EAM_ASSET_NUMBERS_ALL_V cii,  STC.XXX_STC_EAM_APPROVERS app,  XXX_STC_EAM_APPROVERS_DEPT_V dept,  FND_FLEX_VALUES_VL FV,  FND_FLEX_VALUE_SETS FVS, \n" +
                "FND_FLEX_VALUES_VL FV1,  FND_FLEX_VALUE_SETS FVS1,  BOM_DEPARTMENTS BD,  org_organization_definitions org,  wip_discrete_jobs wdj,  wip_entities we WHERE \n" +
                "FV.FLEX_VALUE_SET_ID = FVS.FLEX_VALUE_SET_ID  AND FVS.FLEX_VALUE_SET_NAME = 'XXSCM_Zone'  AND FV.FLEX_VALUE = cii.attribute1  AND dept.zone_id = FV.FLEX_VALUE_ID \n" +
                "AND dept.APPROVER_DEPT_ID = app.APPROVER_DEPT_ID  AND org.organization_id = dept.organization_id  AND org.organization_code = 'E00'  AND wdj.OWNING_DEPARTMENT = \n" +
                "bd.department_id  AND bd.attribute1 = fv1.flex_value  AND fv1.flex_value_id = dept.department_id  AND fv1.flex_value_set_id = fvs1.flex_value_set_id  AND \n" +
                "fvs1.flex_value_set_name = 'XXSCM_Departments'  AND dept.position_name IN ('ABFM', 'BFM')  AND wdj.wip_entity_id = we.wip_entity_id  AND we.wip_entity_name = \n" +
                "qrslt.wip_entity_name  AND wdj.wip_entity_id IN (SELECT wip_entity_id  FROM wip_eam_work_requests WHERE attribute15 IS NULL)  AND cii.instance_number = (SELECT CASE \n" +
                "WHEN INSTR ( SUBSTR ( C.INSTANCE_NUMBER, 8, 5), '-') BETWEEN 1 AND 4 THEN RTRIM ( SUBSTR ( C.INSTANCE_NUMBER, 1, 10), '-') WHEN INSTR ( SUBSTR ( C.INSTANCE_NUMBER, \n" +
                "8, 5), '-') > 4 THEN RTRIM ( SUBSTR ( C.INSTANCE_NUMBER, 1, 11), '-') ELSE C.INSTANCE_NUMBER END  FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V c WHERE \n" +
                "CURRENT_ORGANIZATION_ID=qrslt.wdj_organization_id AND c.serial_number = (SELECT asset_number FROM wip_discrete_jobs wdj1, wip_entities we1 WHERE wdj1.wip_entity_id \n" +
                "=  we1.wip_entity_id AND we1.wip_entity_name =  qrslt.wip_entity_name and we1.ORGANIZATION_ID=qrslt.wdj_organization_id))  AND app.user_id = (SELECT TO_CHAR ( \n" +
                "fnd_profile.VALUE ( 'USER_ID'))  FROM DUAL))))  OR ( (SELECT NVL ( fnd_profile.VALUE ('XXX_EAM_IGATE_SUPERUSER'), 'N') FROM DUAL) = 'Y' AND qrslt.wip_entity_name IN \n"  +
                "(SELECT DISTINCT wip_entity_name FROM  wip_discrete_jobs wdj, wip_entities we WHERE wdj.wip_entity_id = we.wip_entity_id AND we.wip_entity_name = \n" +
                "qrslt.wip_entity_name AND wdj.wip_entity_id IN (SELECT wip_entity_id  FROM wip_eam_work_requests WHERE attribute15 IS NOT NULL) ))  OR ( (SELECT NVL \n" +
                "(fnd_profile.VALUE ('XXX_EAM_CONT_SUPERUSER'), 'N') FROM DUAL) = 'Y' OR ( (SELECT NVL(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')  FROM DUAL) = 'N' and \n" +
                "(SELECT NVL (  fnd_profile.VALUE ( 'XXX_EAM_CONT_SUPERUSER'),  'N')  FROM DUAL) <> 'Y' AND qrslt.wip_entity_name IN  (SELECT DISTINCT wip_entity_name  FROM \n" +
                "STC.XXX_STC_EAM_APPROVERS app,  XXX_STC_EAM_APPROVERS_DEPT_V dept,  org_organization_definitions org,  wip_discrete_jobs wdj,  wip_entities we WHERE \n" +
                "dept.APPROVER_DEPT_ID = app.APPROVER_DEPT_ID  AND org.organization_id = dept.organization_id  AND org.organization_code = 'E00'  AND dept.position_name = \n" +
                "'CONT_SPEC_ENGINEER'  AND DEPT.SUB_DEPARTMENT_ID = wdj.OWNING_DEPARTMENT  AND wdj.wip_entity_id = we.wip_entity_id  AND we.wip_entity_name = qrslt.wip_entity_name \n" +
                "AND app.user_id = (SELECT TO_CHAR ( fnd_profile.VALUE ( 'USER_ID'))  FROM DUAL)))))";
        } else {
            statement = "SELECT WEWODV.*,EWODV.*,WDJ.*, (select ML.MEANING from APPS.mfg_lookups ML WHERE ML.LOOKUP_TYPE = 'WIP_EAM_WORK_ORDER_TYPE' AND ML.LOOKUP_CODE = WEWODV.WORK_ORDER_TYPE) NEW_WORK_ORDER_TYPE,\n" +
                    "(SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'WIP_EAM_ACTIVITY_PRIORITY' AND ML.LOOKUP_CODE = WEWODV.PRIORITY) NEW_PRIORITY,\n" +
                    "(SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_TYPE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_TYPE) ACTIVITY_TYPE_NAME,\n" +
                    "(SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_CAUSE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_CAUSE) ACTIVITY_CAUSE_NAME,\n" +
                    "(SELECT ML.MEANING FROM APPS.MFG_LOOKUPS ML WHERE ML.LOOKUP_TYPE = 'MTL_EAM_ACTIVITY_SOURCE' AND ML.LOOKUP_CODE = WEWODV.ACTIVITY_SOURCE) ACTIVITY_SOURCE_NAME,\n" +
                    "round((WDJ.SCHEDULED_COMPLETION_DATE - WDJ.SCHEDULED_START_DATE) * 24, 3) AS duration, WDJ.SCHEDULED_COMPLETION_DATE, WDJ.DATE_RELEASED\n" +
                    "FROM APPS.wip_eam_work_order_dtls_v WEWODV, APPS.WIP_DISCRETE_JOBS WDJ, APPS.eam_work_order_details_v EWODV WHERE\n" +
                    "WEWODV.WIP_ENTITY_ID = WDJ.WIP_ENTITY_ID AND WDJ.WIP_ENTITY_ID = EWODV.WIP_ENTITY_ID AND EWODV.WIP_ENTITY_ID = ?\n" +

                    "AND ( (SELECT NVL (fnd_profile.VALUE ('XXX_EAM_DIST_CONT_SUPERUSER'),'N') FROM DUAL) = 'Y' \n" +
                    "OR ( (SELECT NVL (fnd_profile.VALUE ('XXX_EAM_DIST_CONT_SUPERUSER'),'N') FROM DUAL) <> 'Y' \n" +
                    "AND qrslt.wip_entity_name IN (SELECT DISTINCT wip_entity_name \n" +
                    "FROM FND_FLEX_VALUES_VL FV, FND_FLEX_VALUE_SETS FVS, wip_discrete_jobs wdj,wip_entities we \n" +
                    "WHERE FV.FLEX_VALUE_SET_ID = FVS.FLEX_VALUE_SET_ID AND FVS.FLEX_VALUE_SET_NAME ='XX_EAM_DIST_CONT_WO_STATUS' AND FV.FLEX_VALUE = wdj.attribute15 \n" +
                    "AND wdj.wip_entity_id = we.wip_entity_id)))";

        }
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1,entityId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){

                JsonObject woDetails = new JsonObject();
                woDetails.addProperty("ENTITY_ID",resultSet.getString("WIP_ENTITY_ID"));
                woDetails.addProperty("ENTITY_NAME",resultSet.getString("WIP_ENTITY_NAME"));
                woDetails.addProperty("ASSET_NUMBER",resultSet.getString("ASSET_NUMBER"));
                woDetails.addProperty("ASSET_GROUP",resultSet.getString("ASSET_GROUP"));
                woDetails.addProperty("ASSET_GROUP_ID",resultSet.getString("ASSET_GROUP_ID"));
                woDetails.addProperty("ASSET_CATEGORY",resultSet.getString("ASSET_CATEGORY"));
                woDetails.addProperty("ASSET_LOCATION",resultSet.getString("ASSET_LOCATION"));
                woDetails.addProperty("ASSET_ACTIVITY_ID",resultSet.getString("ASSET_ACTIVITY_ID"));
                woDetails.addProperty("ASSET_ACTIVITY",resultSet.getString("ASSET_ACTIVITY"));
                woDetails.addProperty("ASSET_OWNING_DEPARTMENT",resultSet.getString("ASSET_OWNING_DEPARTMENT"));
                woDetails.addProperty("ASSET_OWNING_DEPARTMENT_ID",resultSet.getString("ASSET_OWNING_DEPARTMENT_ID"));
                woDetails.addProperty("MAINTENANCE_OBJECT_ID",resultSet.getString("MAINTENANCE_OBJECT_ID"));
                woDetails.addProperty("STATUS",resultSet.getString("WORK_ORDER_STATUS_PENDING"));
                woDetails.addProperty("SYSTEM_STATUS",resultSet.getString("STATUS"));
                woDetails.addProperty("STATUS_ID",resultSet.getString("STATUS_TYPE"));
                woDetails.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                woDetails.addProperty("START_DATE",resultSet.getString("START_DATE"));
                woDetails.addProperty("COMPLETION_DATE",resultSet.getString("COMPLETION_DATE"));
                woDetails.addProperty("WORK_ORDER_TYPE",resultSet.getString("NEW_WORK_ORDER_TYPE"));
                woDetails.addProperty("WORK_ORDER_TYPE_ID",resultSet.getString("WORK_ORDER_TYPE"));
                woDetails.addProperty("PRIORITY",resultSet.getString("NEW_PRIORITY"));
                woDetails.addProperty("PRIORITY_ID",resultSet.getString("PRIORITY"));
                woDetails.addProperty("SCHEDULED_COMPLETION_DATE",resultSet.getString("SCHEDULED_COMPLETION_DATE"));
                woDetails.addProperty("DURATION",resultSet.getString("DURATION"));
                int firmPlannedFlag = resultSet.getInt("FIRM_PLANNED_FLAG");
                if (firmPlannedFlag != 0) {
                    if (firmPlannedFlag == 1) {
                        woDetails.addProperty("FIRM_PLANNED_FLAG", "Yes");
                    }
                    else if (firmPlannedFlag == 2) {
                        woDetails.addProperty("FIRM_PLANNED_FLAG", "No");
                    }
                }
                else {
                    woDetails.addProperty("FIRM_PLANNED_FLAG", "null");
                }
                woDetails.addProperty("ACTIVITY_TYPE_ID",resultSet.getString("ACTIVITY_TYPE"));
                woDetails.addProperty("ACTIVITY_TYPE",resultSet.getString("ACTIVITY_TYPE_NAME"));
                woDetails.addProperty("ACTIVITY_CAUSE_ID",resultSet.getString("ACTIVITY_CAUSE"));
                woDetails.addProperty("ACTIVITY_CAUSE",resultSet.getString("ACTIVITY_CAUSE_NAME"));
                woDetails.addProperty("ACTIVITY_SOURCE_ID",resultSet.getString("ACTIVITY_SOURCE"));
                woDetails.addProperty("ACTIVITY_SOURCE",resultSet.getString("ACTIVITY_SOURCE_NAME"));
                String materialIssueByMO = resultSet.getString("MATERIAL_ISSUE_BY_MO");
                if (materialIssueByMO != null) {
                    if (materialIssueByMO.equals("Y")) {
                        woDetails.addProperty("MATERIAL_ISSUE_BY_MO", "Yes");
                    }
                    else if (materialIssueByMO.equals("N")) {
                        woDetails.addProperty("MATERIAL_ISSUE_BY_MO", "No");
                    }
                }
                else {
                    woDetails.addProperty("MATERIAL_ISSUE_BY_MO", "null");
                }
                woDetails.addProperty("MATERIAL_SHORTAGE",resultSet.getString("MATERIAL_SHORTAGE_DISP") + " As Of " +
                        resultSet.getString("MATERIAL_SHORTAGE_CHECK_DATE"));
                int statusType =  resultSet.getInt("STATUS_TYPE");
                String dateReleased = resultSet.getString("DATE_RELEASED");
                if ((statusType == 1 || statusType == 17 || statusType == 7) || (statusType == 6 && dateReleased == null)) {
                    woDetails.addProperty("IS_APPROVAL_STILL_REQUIRED","YES");
                }
                else{
                    woDetails.addProperty("IS_APPROVAL_STILL_REQUIRED","NO");
                }

                jsonArray.add(woDetails);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();

            JsonArray additionalDetails = getAdditionalDetails(entityId);
            JsonArray requests = getRequests(entityId);
            JsonArray operationRequirements = getWorkOrderOperations(entityId);
            JsonArray materialRequirements = getWorkOrderMaterialDirect(entityId);
            JsonArray workOrderApprovalHistory = getWorkOrderApprovalHistory(entityId);

            jsonResult.add("WORK_ORDER_DETAILS",jsonArray);
            jsonResult.add("ADDITIONAL_DETAILS",additionalDetails);
            jsonResult.add("REQUESTS", requests);
            jsonResult.add("OPERATION_REQUIREMENTS",operationRequirements);
            jsonResult.add("MATERIAL_REQUIREMENTS",materialRequirements);
            jsonResult.add("APPROVAL_HISTORY",workOrderApprovalHistory);

            result = jsonResult.toString();

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Details Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
     }*/

    public JsonArray getAdditionalDetails(int entityId) {

        JsonArray result = new JsonArray();

        PreparedStatement preparedStatement2 = null;
        ResultSet resultSet2 = null;

        String statement1 = "select * from(SELECT ewodv.wip_entity_id,\n" +
                "ewodv.warranty_claim_status as warranty_claim_status_id,\n" +
                "(select ml.meaning from APPS.mfg_lookups ml where ml.lookup_code = ewodv.warranty_claim_status and ml.lookup_type = 'EAM_WARRANTY_STATUS') AS WARRANTY_CLAIM_STATUS, \n" +
                "(select ml.meaning from APPS.mfg_lookups ml where ml.lookup_code = ewodv.WARRANTY_ACTIVE and ml.lookup_type = 'SYS_YES_NO') AS WARRANTY_ACTIVE from APPS.eam_work_order_details_v  ewodv) \n" +
                "QRSLT where wip_entity_id = ?";

        String statement2 = "select * from (SELECT wewodv.WIP_ENTITY_ID, wewodv.shutdown_type AS SHUTDOWN_TYPE_ID,\n" +
                "(select flv.meaning from APPS.fnd_lookup_values flv where flv.lookup_code = wewodv.tagout_required and flv.lookup_type = 'YES_NO' AND LANGUAGE = 'US' AND VIEW_APPLICATION_ID = 0) AS TAGOUT_REQUIRED,\n" +
                "(select flv.meaning from APPS.fnd_lookup_values flv where flv.lookup_code = wewodv.notification_required and flv.lookup_type = 'YES_NO' AND LANGUAGE = 'US' AND VIEW_APPLICATION_ID = 0) AS NOTIFICATION_REQUIRED, \n" +
                "warranty_expn_date, (select flv.meaning from APPS.fnd_lookup_values flv where flv.lookup_code = wewodv.shutdown_type and flv.lookup_type = 'BOM_EAM_SHUTDOWN_TYPE' AND FLV.LANGUAGE = 'US') AS SHUTDOWN_TYPE, \n" +
                "(select flv.meaning from APPS.fnd_lookup_values flv where flv.lookup_code = wewodv.PLAN_MAINTENANCE and flv.lookup_type = 'YES_NO' AND LANGUAGE = 'US' AND VIEW_APPLICATION_ID = 0) AS PLAN_MAINTENANCE\n" +
                "FROM APPS.wip_eam_work_order_dtls_v wewodv) QRSLT where wip_entity_id = ?";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement1);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, entityId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonArray jsonArray = new JsonArray();

            JsonObject jsonObject = new JsonObject();

            while (resultSet.next()){
                jsonObject.addProperty("WARRANTY_CLAIM_STATUS_ID",resultSet.getString("WARRANTY_CLAIM_STATUS_ID"));
                jsonObject.addProperty("WARRANTY_CLAIM_STATUS",resultSet.getString("WARRANTY_CLAIM_STATUS"));
                jsonObject.addProperty("WARRANTY_ACTIVE",resultSet.getString("WARRANTY_ACTIVE"));
            }

            preparedStatement.close();
            resultSet.close();

            preparedStatement2 = connection.prepareStatement(statement2);
            preparedStatement2.setFetchSize(100);
            preparedStatement2.setInt(1, entityId);

            resultSet2 = preparedStatement2.executeQuery();
            resultSet2.setFetchSize(100);

            while (resultSet2.next()){
                jsonObject.addProperty("TAGOUT_REQUIRED",resultSet2.getString("TAGOUT_REQUIRED"));
                jsonObject.addProperty("NOTIFICATION_REQUIRED",resultSet2.getString("NOTIFICATION_REQUIRED"));
                jsonObject.addProperty("WARRANTY_EXPN_DATE",resultSet2.getString("WARRANTY_EXPN_DATE"));
                jsonObject.addProperty("SHUTDOWN_TYPE_ID",resultSet2.getString("SHUTDOWN_TYPE_ID"));
                jsonObject.addProperty("SHUTDOWN_TYPE",resultSet2.getString("SHUTDOWN_TYPE"));
                jsonObject.addProperty("PLAN_MAINTENANCE",resultSet2.getString("PLAN_MAINTENANCE"));
            }

            jsonArray.add(jsonObject);
            result = jsonArray;

            preparedStatement2.close();
            resultSet2.close();
            connection.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Additional Details Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
            if (preparedStatement2 != null) {
                try {
                    preparedStatement2.close();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (resultSet2 != null) {
                try {
                    resultSet2.close();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public JsonArray getRequests(int entityId) {
        JsonArray result = new JsonArray();

        String statement = "select work_request_id, description, (select ml.meaning from APPS.mfg_lookups ml where ml.lookup_type='WIP_EAM_WORK_REQ_STATUS' \n" +
                "and ml.enabled_flag='Y' and ml.lookup_code = work_request_status_id) as work_request_status, asset_number, ( select bd.department_code from APPS.bom_departments bd\n" +
                "where bd.department_id = work_request_owning_dept) as department from APPS.wip_eam_work_requests where wip_entity_id = ?\n";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1,entityId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("WORK_REQUEST_ID",resultSet.getString("WORK_REQUEST_ID"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                jsonObject.addProperty("WORK_REQUEST_STATUS",resultSet.getString("WORK_REQUEST_STATUS"));
                jsonObject.addProperty("ASSET_NUMBER",resultSet.getString("ASSET_NUMBER"));
                jsonObject.addProperty("DEPARTMENT",resultSet.getString("DEPARTMENT"));
                jsonArray.add(jsonObject);
            }

            result = jsonArray;

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get WR Assignment Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public JsonArray getWorkOrderMaterialDirect(int entityId) {

        JsonArray result = new JsonArray();

        String statement = "select operation_seq_num, direct_item_sequence_id,required_quantity,uom,attribute1 as cost, attribute7 as agent, \n" +
                "attribute12 as import_period, attribute2 as consumable, attribute4 as imported, attribute5 as supplier_name,\n" +
                "attribute6 as invoice_number, attribute8 as warranty_period, attribute9 as returned, attribute3 as note from APPS.wip_eam_direct_items\n" +
                "where wip_entity_id = ?";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1,entityId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("OPERATION_SEQ_NUM",resultSet.getString("OPERATION_SEQ_NUM"));
                jsonObject.addProperty("DIRECT_ITEM_SEQUENCE_ID",resultSet.getString("DIRECT_ITEM_SEQUENCE_ID"));
                jsonObject.addProperty("REQUIRED_QUANTITY",resultSet.getString("REQUIRED_QUANTITY"));
                jsonObject.addProperty("UOM",resultSet.getString("UOM"));
                jsonObject.addProperty("COST",resultSet.getString("COST"));
                jsonObject.addProperty("AGENT",resultSet.getString("AGENT"));
                jsonObject.addProperty("IMPORT_PERIOD",resultSet.getString("IMPORT_PERIOD"));
                jsonObject.addProperty("CONSUMABLE",resultSet.getString("CONSUMABLE"));
                jsonObject.addProperty("IMPORTED",resultSet.getString("IMPORTED"));
                jsonObject.addProperty("SUPPLIER_NAME",resultSet.getString("SUPPLIER_NAME"));
                jsonObject.addProperty("INVOICE_NUMBER",resultSet.getString("INVOICE_NUMBER"));
                jsonObject.addProperty("WARRANTY_PERIOD",resultSet.getString("WARRANTY_PERIOD"));
                jsonObject.addProperty("RETURNED",resultSet.getString("RETURNED"));
                jsonObject.addProperty("NOTE",resultSet.getString("NOTE"));
                jsonArray.add(jsonObject);
            }

            result = jsonArray;

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Materials Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public JsonArray getWorkOrderOperations(int entityId) {

        JsonArray result = new JsonArray();

        String statement = "SELECT WIP_OPERATIONS.*, wip_operation_resources.*, WIP_OP_RESOURCE_INSTANCES.*, \n" +
                "(SELECT BD.DEPARTMENT_CODE FROM APPS.BOM_DEPARTMENTS BD WHERE WIP_OPERATIONS.DEPARTMENT_ID = BD.DEPARTMENT_ID) AS DEPARTMENT_CODE, \n" +
                "(SELECT BR.RESOURCE_CODE FROM APPS.BOM_RESOURCES BR WHERE wip_operation_resources.RESOURCE_ID = BR.RESOURCE_ID) AS RESOURCE_CODE,\n" +
                "(SELECT PPF.FULL_NAME FROM APPS.PER_PEOPLE_F PPF, APPS.BOM_RESOURCE_EMPLOYEES BRE WHERE ppf.person_id = bre.person_id AND BRE.INSTANCE_ID = WIP_OP_RESOURCE_INSTANCES.INSTANCE_ID) AS EMPLOYEE_NAME,\n" +
                "(round((WIP_OPERATIONS.FIRST_UNIT_COMPLETION_DATE - WIP_OPERATIONS.FIRST_UNIT_START_DATE) * 24, 3))AS DURATION\n" +
                "FROM APPS.WIP_OPERATIONS LEFT OUTER JOIN \n" +
                "APPS.wip_operation_resources ON WIP_OPERATIONS.WIP_ENTITY_ID = wip_operation_resources.WIP_ENTITY_ID AND WIP_OPERATIONS.OPERATION_SEQ_NUM = wip_operation_resources.OPERATION_SEQ_NUM\n" +
                "LEFT OUTER JOIN APPS.WIP_OP_RESOURCE_INSTANCES ON WIP_OPERATIONS.WIP_ENTITY_ID = WIP_OP_RESOURCE_INSTANCES.WIP_ENTITY_ID AND \n" +
                "wip_operation_resources.RESOURCE_SEQ_NUM = WIP_OP_RESOURCE_INSTANCES.RESOURCE_SEQ_NUM WHERE WIP_OPERATIONS.WIP_ENTITY_ID = ? ORDER BY WIP_OPERATIONS.OPERATION_SEQ_NUM";

        try {

            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1,entityId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("OPERATION_SEQ_NUM",resultSet.getString("OPERATION_SEQ_NUM"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                jsonObject.addProperty("DEPARTMENT_ID",resultSet.getString("DEPARTMENT_ID"));
                jsonObject.addProperty("DEPARTMENT_CODE",resultSet.getString("DEPARTMENT_CODE"));
                jsonObject.addProperty("RESOURCE_ID",resultSet.getString("RESOURCE_ID"));
                jsonObject.addProperty("RESOURCE_CODE",resultSet.getString("RESOURCE_CODE"));
                jsonObject.addProperty("INSTANCE_ID",resultSet.getString("INSTANCE_ID"));
                jsonObject.addProperty("EMPLOYEE_OR_EQUIPMENT",resultSet.getString("EMPLOYEE_NAME"));
                jsonObject.addProperty("FIRST_UNIT_START_DATE",resultSet.getString("FIRST_UNIT_START_DATE"));
                jsonObject.addProperty("FIRST_UNIT_COMPLETION_DATE",resultSet.getString("FIRST_UNIT_COMPLETION_DATE"));
                jsonObject.addProperty("DURATION",resultSet.getString("DURATION"));

                jsonArray.add(jsonObject);
            }

            result = jsonArray;

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Operations Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public JsonArray getWorkOrderApprovalHistory(int entityId) {

        JsonArray result = new JsonArray();

        int itemkey = getWorkflowItemkey(entityId);

        String statement = "select atoal.transaction_id, atoal.name, atoal.order_number, atoal.approval_status, atah.row_timestamp, atah.user_comments, fu.email_address, fu.user_id,\n" +
                "(select phone_number from APPS.per_phones where parent_id = fu.employee_id) as phone_number\n" +
                "from APPS.ame_temp_old_approver_lists atoal, APPS.ame_trans_approval_history atah, APPS.fnd_user fu\n" +
                "where atoal.transaction_id = atah.transaction_id and atoal.approval_status = atah.status \n" +
                "and atoal.order_number = atah.order_number and fu.user_name = atoal.name and \n" +
                "atoal.transaction_id = ? order by atoal.order_number";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
//            preparedStatement.setInt(1,itemkey);
            preparedStatement.setString(1,String.valueOf(itemkey));

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("TRANSACTION_ID",resultSet.getString("TRANSACTION_ID"));
                jsonObject.addProperty("NAME",resultSet.getString("NAME"));
                jsonObject.addProperty("ORDER_NUMBER",resultSet.getString("ORDER_NUMBER"));
                jsonObject.addProperty("APPROVAL_STATUS",resultSet.getString("APPROVAL_STATUS"));
                jsonObject.addProperty("APPROVAL_DATE",resultSet.getString("ROW_TIMESTAMP"));
                jsonObject.addProperty("USER_COMMENTS",resultSet.getString("USER_COMMENTS"));
                jsonObject.addProperty("EMAIL_ADDRESS",resultSet.getString("EMAIL_ADDRESS"));
                jsonObject.addProperty("PHONE_NUMBER", resultSet.getString("PHONE_NUMBER"));

                jsonArray.add(jsonObject);
            }
            result = jsonArray;

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Approval History",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public int getWorkflowItemType(int entityId) {

        int result = 0;

        String statement = "SELECT wf_item_type FROM APPS.eam_wo_workflows WHERE wip_entity_id = ?";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, entityId);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                result = resultSet.getInt(1);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Workflow Item Type Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public int getWorkflowItemkey(int entityId) {

        int result = 0;

        String statement = "SELECT wf_item_key FROM APPS.eam_wo_workflows WHERE wip_entity_id = ?";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, entityId);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                result = resultSet.getInt(1);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Workflow Item Key Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getUom(String uomCode) {

        String result = null;

        String statement = "select distinct muc.uom_code uom, muom.description,muc.inventory_item_id,muc.uom_class,muc.uom_code,muc.disable_date from APPS.mtl_uom_conversions muc, \n" +
                "APPS.mtl_units_of_measure muom where muom.uom_code = muc.uom_code and muc.inventory_item_id = 0 and nvl(muc.disable_date,sysdate+1) >sysdate";

        if (uomCode != null && !uomCode.isEmpty()) {
            statement += " and upper(muc.uom_code) like :uom";
        }

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            namedParameterStatement = new NamedParameterStatement(connection, statement);
            namedParameterStatement.setFetchSize(100);

            if (uomCode != null && !uomCode.isEmpty()) {
                namedParameterStatement.setString("uom", "%" + uomCode + "%");
            }

            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("UOM",resultSet.getString(1));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(2));
                jsonObject.addProperty("UOM_CLASS",resultSet.getString(4));
                jsonObject.addProperty("UOM_CODE",resultSet.getString(5));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_ORDER_TYPES",jsonArray);
            result = jsonResult.toString();

            connection.close();
            namedParameterStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get UOM Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getResources(int departmentId) {
        String result = null;

        String statement = "SELECT * FROM (SELECT res.resource_code, res.unit_of_measure, res.description, \n" +
                "decode(res.autocharge_type,1,2,2,2,3) autocharge_type, nvl(res.default_basis_type,1) basis_type, nvl(res.standard_rate_flag,1) standard_rate, \n" +
                "res.resource_id, bdr.department_id, res.organization_id, res.resource_type, ml.meaning as autocharge_type_code FROM APPS.BOM_RESOURCES res, \n" +
                "APPS.BOM_DEPARTMENT_RESOURCES bdr,APPS.MFG_LOOKUPS ml WHERE NVL(res.disable_date,sysdate+2) > sysdate \n" +
                "AND res.resource_id = bdr.resource_id AND ml.lookup_type = 'BOM_AUTOCHARGE_TYPE' AND ml.lookup_code = DECODE(res.autocharge_type,1,2,2,2,3)) QRSLT WHERE \n" +
                "DEPARTMENT_ID = ?";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, departmentId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("RESOURCE_CODE",resultSet.getString(1));
                jsonObject.addProperty("UNIT_OF_MEASURE",resultSet.getString(2));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(3));
                jsonObject.addProperty("AUTOCHARGE_TYPE",resultSet.getString(4));
                jsonObject.addProperty("BASIS_TYPE",resultSet.getString(5));
                jsonObject.addProperty("STANDARD_RATE",resultSet.getString(6));
                jsonObject.addProperty("RESOURCE_ID",resultSet.getString(7));
                jsonObject.addProperty("DEPARTMENT_ID",resultSet.getString(8));
                jsonObject.addProperty("ORGANIZATION_ID",resultSet.getString(9));
                jsonObject.addProperty("RESOURCE_TYPE",resultSet.getString(10));
                jsonObject.addProperty("AUTOCHARGE_TYPE_CODE",resultSet.getString(11));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("RESOURCES",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Reconciliation Code Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getInstances(int resourceId) {
        String result = null;

        String statement = "SELECT * FROM (SELECT ppf.full_name instance_name , NVL(ppf.employee_number, ppf.npw_number) instance_number, bdr.resource_id, bdr.department_id, \n" +
                "bre.instance_id instance_id, CASE WHEN ppf.employee_number IS NOT NULL THEN 'Employee' WHEN ppf.npw_number IS NOT NULL THEN 'Contingent' END AS TYPE,  \n" +
                "to_char(null) as serial_number, to_char(null) as item_name, 2 as resource_type FROM APPS.PER_PEOPLE_F ppf, APPS.BOM_RESOURCE_EMPLOYEES bre, APPS.BOM_DEPT_RES_INSTANCES bdri,  \n" +
                "APPS.BOM_DEPARTMENT_RESOURCES bdr WHERE bdr.resource_id = bdri.resource_id AND NVL(bdr.share_from_dept_id,bdr.department_id) = bdri.department_id  \n" +
                "AND ppf.person_id = bre.person_id AND ( (ppf.employee_number IS NOT NULL) OR (ppf.npw_number IS NOT NULL) ) AND trunc(SYSDATE) BETWEEN ppf.effective_start_date  \n" +
                "AND ppf.effective_end_date AND bdri.instance_id = bre.instance_id AND ( (NVL(ppf.current_employee_flag,'N')='Y') OR (NVL(ppf.CURRENT_NPW_FLAG,'N')='Y') )  \n" +
                "UNION SELECT decode(serial_number,NULL,msik.concatenated_segments,serial_number) instance_name, to_char(NULL) AS instance_number, bdr.resource_id,  \n" +
                "bdr.department_id, bre.instance_id, 'Equipment' AS Type, bdri.serial_number, msik.concatenated_segments as item_name, 1 as resource_type  \n" +
                "FROM APPS.BOM_DEPT_RES_INSTANCES bdri, APPS.BOM_RESOURCE_EQUIPMENTS bre, APPS.MTL_SYSTEM_ITEMS_KFV msik, APPS.BOM_DEPARTMENT_RESOURCES bdr WHERE bdr.resource_id = bdri.resource_id  \n" +
                "AND NVL(bdr.share_from_dept_id,bdr.department_id) = bdri.department_id AND bdri.instance_id=bre.instance_id AND bre.inventory_item_id = msik.inventory_item_id  \n" +
                "AND bre.organization_id = msik.organization_id) QRSLT WHERE (resource_id = ?) ORDER BY instance_name";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, resourceId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("INSTANCE_NAME",resultSet.getString(1));
                jsonObject.addProperty("INSTANCE_NUMBER",resultSet.getString(2));
                jsonObject.addProperty("RESOURCE_ID",resultSet.getString(3));
                jsonObject.addProperty("DEPARTMENT_ID",resultSet.getString(4));
                jsonObject.addProperty("INSTANCE_ID",resultSet.getString(5));
                jsonObject.addProperty("TYPE",resultSet.getString(6));
                jsonObject.addProperty("SERIAL_NUMBER",resultSet.getString(7));
                jsonObject.addProperty("ITEM_NAME",resultSet.getString(8));
                jsonObject.addProperty("RESOURCE_TYPE",resultSet.getString(9));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("INSTANCES",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Reconciliation Code Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getReconciliationCode() {

        String result = null;

        String statement = "select lookup_code, meaning, description from APPS.mfg_lookups where lookup_type = 'WIP_EAM_RECONCILIATION_CODE'";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("CODE",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("RECONCILIATION_CODE",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Reconciliation Code Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getSubinventory(int orgId) {

        String result = null;

        String statement = "SELECT * FROM (select secondary_inventory_name as SubInventoryName, description as Description, locator_type, organization_id, asset_inventory, \n" +
                "disable_date, 0 as InventoryItemID, to_char(NULL) as InventoryItemName, to_char(NULL) as RestrictSubInvCode, SUBINVENTORY_TYPE from APPS.mtl_secondary_inventories) \n" +
                "QRSLT WHERE (NVL(DISABLE_DATE,SYSDATE+1) > SYSDATE AND (SUBINVENTORY_TYPE = 1 OR SUBINVENTORY_TYPE IS NULL) and organization_id = ? and asset_inventory = 2) ORDER BY 1";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, orgId);
            preparedStatement.setFetchSize(100);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("SUBINVENROTY_NAME",resultSet.getString(1));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("SUBINVENTORY",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Subinventory Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getLocator(int orgId, String subInventory) {

        String result = null;

        String statement = "SELECT * FROM (select msikfv.Inventory_Location_ID as LocatorID, msikfv.concatenated_segments as Locator, msikfv.description as Description, \n" +
                "msikfv.Subinventory_code as Subinventory, msikfv.organization_id as OrganizationId, to_char(NULL) as RestrictLocatorCode, \n" +
                "to_char(NULL) as InventoryItemID , msikfv.disable_date, ppv.project_number, mtv.task_number from APPS.mtl_item_locations_kfv msikfv, \n" +
                "APPS.pjm_projects_mtll_pv ppv, APPS.pjm_tasks_mtll_v mtv where ppv.project_id(+)=msikfv.project_id and mtv.project_id(+)=msikfv.project_id \n" +
                "and mtv.task_id(+)=msikfv.task_id) QRSLT WHERE (OrganizationId = ? and Subinventory = ? and nvl(Disable_Date,trunc(sysdate)+1)>trunc(sysdate)) ORDER BY 2";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, orgId);
            preparedStatement.setString(2, subInventory);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("LOCATOR_ID",resultSet.getString(1));
                jsonObject.addProperty("LOCATOR",resultSet.getString(2));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("LOCATOR",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Locator",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getLot(int orgId) {

        String result = null;

        String statement = "SELECT * FROM (select mmt.inventory_item_id as InventoryItemID, sum(-mtln.primary_quantity) as quantity_issued, mtln.lot_number, msibkfv.concatenated_segments as Material \n" +
                "from APPS.mtl_material_transactions mmt, APPS.mtl_transaction_lot_numbers mtln, APPS.mtl_system_items_b_kfv msibkfv, APPS.wip_entities we where mmt.transaction_id = mtln.transaction_id \n" +
                "and mmt.inventory_item_id = msibkfv.inventory_item_id and mmt.organization_id = msibkfv.organization_id and mmt.organization_id = mtln.organization_id \n" +
                "and mmt.organization_id = we.organization_id and mmt.organization_id = ? and msibkfv.inventory_item_id = mtln.inventory_item_id\n" +
                "and (msibkfv.lot_control_code > 1) and mmt.transaction_type_id in (35,43) and mmt.transaction_source_id = we.wip_entity_id and we.entity_type in (6,7) \n" +
                "group by mmt.inventory_item_id, msibkfv.concatenated_segments, mtln.lot_number,msibkfv.serial_number_control_code having sum(-mtln.primary_quantity) > 0) QRSLT ORDER BY 3";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, orgId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("INVENTORY_ITEM_ID",resultSet.getString(1));
                jsonObject.addProperty("QUANTITY_ISSUED",resultSet.getString(2));
                jsonObject.addProperty("LOT_NUMBER",resultSet.getString(3));
                jsonObject.addProperty("MATERIAL",resultSet.getString(4));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("LOT",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Lot Number Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getShutdownType() {

        String result = null;

        String statement = "select LOOKUP_CODE,MEANING,DESCRIPTION from APPS.mfg_lookups where lookup_type = 'BOM_EAM_SHUTDOWN_TYPE' AND ENABLED_FLAG = 'Y'";

        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("CODE",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("SHUTDOWN_TYPE",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Shutdown Type",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getActivityType() {

        String result = null;

        String statement = "SELECT lookup_code,meaning,description FROM APPS.MFG_LOOKUPS WHERE lookup_type = 'MTL_EAM_ACTIVITY_TYPE'";

        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("CODE",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ACTIVITY_TYPE",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Activity Type",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getActivityCause() {

        String result = null;

        String statement = "SELECT lookup_code,meaning,description FROM APPS.MFG_LOOKUPS WHERE lookup_type = 'MTL_EAM_ACTIVITY_CAUSE'";

        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("CODE",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ACTIVITY_TYPE",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Activity Cause Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getActivitySource() {

        String result = null;

        String statement = "SELECT lookup_code,meaning,description FROM APPS.MFG_LOOKUPS WHERE lookup_type = 'MTL_EAM_ACTIVITY_SOURCE'";

        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("CODE",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ACTIVITY_TYPE",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Activity Source Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWarrantyStatus() {

        String result = null;

        String statement = "SELECT lookup_code,meaning,description FROM APPS.MFG_LOOKUPS WHERE lookup_type = 'EAM_WARRANTY_STATUS'";

        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("CODE",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ACTIVITY_TYPE",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Warranty Status");
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String createWorkOrder(   int userId,
                                     int respId,
                                     int respAppId,
                                     int orgId,
                                     String entityName,
                                     String assetNumber,
                                     int assetGroupId,
                                     int maintenanceObjectId,
                                     String classCode,
                                     String description,
                                     int departmentId,
                                     String scheduledStartDate,
                                     String scheduledCompletionDate,
                                     int requestNumber,
                                     int statusType,
                                     int priority,
                                     String wfStatus
    ) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/" +
                    "" +
                    "/create_work_order/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_work_order/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("USER_ID", String.valueOf(userId));
            param.put("RESP_ID", String.valueOf(respId));
            param.put("ATTRIBUTE15", wfStatus);
            param.put("RESP_APP_ID", String.valueOf(respAppId));
            param.put("WIP_ENTITY_NAME", entityName);
            param.put("ORG_ID", String.valueOf(orgId));
            param.put("ASSET_NUMBER", assetNumber);
            param.put("ASSET_GROUP_ID", String.valueOf(assetGroupId));
            param.put("MAINTENANCE_OBJECT_ID", String.valueOf(maintenanceObjectId));
            param.put("CLASS_CODE", classCode);
            param.put("SCHEDULED_START_DATE", scheduledStartDate);
            param.put("SCHEDULED_COMPLETION_DATE", scheduledCompletionDate);
            param.put("REQUEST_NUMBER", String.valueOf(requestNumber));
            param.put("DESCRIPTION", description);
            param.put("DEPARTMENT_ID", String.valueOf(departmentId));
            param.put("STATUS_TYPE", String.valueOf(statusType));
            param.put("PRIORITY_ID", String.valueOf(priority));

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            System.out.println(seo);
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Work Order Error",e);
        }
        return result;
    }

    public String updateWorkOrder(int userId,
                                  int respId,
                                  int respAppId,
                                  int entityId,
                                  int orgId,
                                  String scheduledStartDate,
                                  String scheduledCompletionDate,
                                  String assetNumber,
                                  int maintenanceObjectId,
                                  int assetGroupId,
                                  int departmentId,
                                  String classCode,
                                  String description,
                                  int statusType,
                                  int priority,
                                  int requestNumber
                                  ) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/update_work_order/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/update_work_order/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("USER_ID", String.valueOf(userId));
            param.put("RESP_ID", String.valueOf(respId));
            param.put("RESP_APP_ID", String.valueOf(respAppId));
            param.put("I_WIP_ENTITY_ID", String.valueOf(entityId));
            param.put("ORG_ID", String.valueOf(orgId));
            param.put("SCHEDULED_START_DATE", scheduledStartDate);
            param.put("SCHEDULED_COMPLETION_DATE", scheduledCompletionDate);
            //param.put("ASSET_NUMBER", assetNumber);
            param.put("MAINTENANCE_OBJECT_ID", String.valueOf(maintenanceObjectId));
            //param.put("ASSET_GROUP_ID", String.valueOf(assetGroupId));
            param.put("DEPARTMENT_ID", String.valueOf(departmentId));
            //param.put("CLASS_CODE", classCode);
            param.put("DESCRIPTION", description);
            param.put("STATUS_TYPE", String.valueOf(statusType));
            param.put("PRIORITY_ID", String.valueOf(priority));
            //param.put("REQUEST_NUMBER", String.valueOf(requestNumber));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Work Order Error",e);
        }
        return result;
    }

    public String deleteWorkRequestAssign(int userId,
                                          int respId,
                                          int respAppId,
                                          int entityId,
                                          int orgId,
                                          int requestNumber) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/delete_work_request_assign/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/delete_work_request_assign/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_USER_ID", String.valueOf(userId));
            param.put("P_RESP_ID", String.valueOf(respId));
            param.put("P_RESP_APP_ID", String.valueOf(respAppId));
            param.put("I_WIP_ENTITY_ID", String.valueOf(entityId));
            param.put("ORG_ID", String.valueOf(orgId));
            param.put("REQUEST_NUMBER", String.valueOf(requestNumber));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Work Order Error",e);
        }
        return result;
    }

    public String createOperationRequirements(int userId,
                                              int respId,
                                              int respAppId,
                                              int entityId,
                                              int orgId,
                                              int departmentId,
                                              String operation,
                                              String description,
                                              String resourceSequence,
                                              String startTime,
                                              String endTime) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_operation_requirements/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_operation_requirements/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_USER_ID", String.valueOf(userId));
            param.put("P_RESP_ID", String.valueOf(respId));
            param.put("P_RESP_APP_ID", String.valueOf(respAppId));
            param.put("ENTITY_ID", String.valueOf(entityId));
            param.put("ORG_ID", String.valueOf(orgId));
            param.put("DEPARTMENT_ID", String.valueOf(departmentId));
            param.put("START_DATE", startTime);
            param.put("COMPLETION_DATE", endTime);
            param.put("OPERATION_SEQ_NUM", operation);
            param.put("RESOURCE_SEQ_NUM", resourceSequence);
            param.put("DESCRIPTION", description);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Operation Error",e);
        }
        return result;
    }

    public String deleteOperationRequirements(int userId,
                                             int respId,
                                             int respAppId,
                                             int departmentId,
                                             int operationSeqNum,
                                             int entityId,
                                             int orgId) {
        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/delete_operation_requirements/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/delete_operation_requirements/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_USER_ID", String.valueOf(userId));
            param.put("P_RESP_ID", String.valueOf(respId));
            param.put("P_RESP_APP_ID", String.valueOf(respAppId));
            param.put("ENTITY_ID", String.valueOf(entityId));
            param.put("ORG_ID", String.valueOf(orgId));
            param.put("DEPARTMENT_ID", String.valueOf(departmentId));
            param.put("OPERATION_SEQ_NUM", String.valueOf(operationSeqNum));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Delete Operation Error",e);
        }
        return result;
    }

    public String createMaterialRequirements(int userId,
                                              int respId,
                                              int respAppId,
                                              int entityId,
                                              int orgId,
                                              int departmentId,
                                              String operation,
                                              String itemType,
                                              String resourceSequence,
                                              String description,
                                              String quantity,
                                              String uom,
                                              String cost,
                                              String agent,
                                              String importPeriod,
                                              String consumable,
                                              String imported,
                                              String supplierName,
                                              String invoiceNumber,
                                              String warrantyPeriod,
                                              String returned,
                                              String note) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_material_requirements/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_material_requirements/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("USER_ID", String.valueOf(userId));
            param.put("RESP_ID", String.valueOf(respId));
            param.put("RESP_APP_ID", String.valueOf(respAppId));
            param.put("P_ENTITY_ID", String.valueOf(entityId));
            param.put("P_ORG_ID", String.valueOf(orgId));
            param.put("P_DEPARTMENT_ID", String.valueOf(departmentId));
            param.put("P_OPERATION_SEQ_NUM", operation);
            param.put("P_PURCHASING_CATEGORY_ID", "25054");
            param.put("P_RESOURCE_SEQ_NUM", resourceSequence);
            param.put("P_DESCRIPTION", description);
            param.put("P_REQUIRED_QUANTITY", quantity);
            param.put("P_UOM_CODE", uom);
            param.put("P_COST", cost);
            param.put("P_AGENT", agent);
            param.put("P_IMPORT_PERIOD", importPeriod);
            param.put("P_CONSUMABLE", consumable);
            param.put("P_IMPORTED", imported);
            param.put("P_SUPPLIER_NAME", supplierName);
            param.put("P_INVOICE_NUMBER", invoiceNumber);
            param.put("P_WARRANTY_PERIOD", warrantyPeriod);
            param.put("P_RETURNED", returned);
            param.put("P_NOTE", note);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Material Error",e);
        }
        return result;
    }

    public String deleteMaterialRequirements(int userId,
                                             int respId,
                                             int respAppId,
                                             int directItemSequenceId,
                                             int departmentId,
                                             int operationSeqNum,
                                             int entityId,
                                             int orgId) {
        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/delete_material_requirements/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/delete_material_requirements/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_USER_ID", String.valueOf(userId));
            param.put("P_RESP_ID", String.valueOf(respId));
            param.put("P_RESP_APP_ID", String.valueOf(respAppId));
            param.put("P_DIRECT_ITEM_SEQUENCE_ID", String.valueOf(directItemSequenceId));
            param.put("P_DEPARTMENT_ID", String.valueOf(departmentId));
            param.put("P_OPERATION_SEQ_NUM", String.valueOf(operationSeqNum));
            param.put("P_ENTITY_ID", String.valueOf(entityId));
            param.put("P_ORG_ID", String.valueOf(orgId));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Delete Materials Error",e);
        }
        return result;
    }

    public String completeWorkOrder(int entityId,
                                    int transactionType,
                                    String transactionDate,
                                    int userId,
                                    String reconcilCode,
                                    String actualStartDate,
                                    String actualEndDate,
                                    float duration) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/eam_completion/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/eam_completion/complete_work_order_generic/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/eam_completion/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/eam_completion/complete_work_order_generic/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("X_WIP_ENTITY_ID",String.valueOf(entityId));
            param.put("X_REBUILD_JOBS", "N");
            param.put("X_TRANSACTION_TYPE", String.valueOf(transactionType)); //4
            param.put("X_TRANSACTION_DATE", transactionDate);                     //today date
            param.put("X_USER_ID", String.valueOf(userId));           //no needed
            param.put("X_RECONCIL_CODE", String.valueOf(reconcilCode));    //no needed
            param.put("X_ACTUAL_START_DATE", actualStartDate);
            param.put("X_ACTUAL_END_DATE", actualEndDate);
            param.put("X_ACTUAL_DURATION", String.valueOf(duration));

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Complete Work Order Error",e);
        }
        return result;
    }

    public String sendWorkOrderForApproval(boolean isHqc,
                                           int entityId,
                                           int userId,
                                           int respId,
                                           int respAppId) {

        if (isHqc) {
            String result = "";

            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/?wsdl";
                String soapAction = "http://xmlns.oracle.ccom/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/oaf_wf_start/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/oaf_wf_start/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("P_PROCESS", "XXX_PHASE1_PRC");
                param.put("P_TRANSACTION_ID", String.valueOf(entityId));
                param.put("P_REQUESTOR_ID", String.valueOf(userId));
                param.put("P_RESP_ID", String.valueOf(respId));
                param.put("P_RESP_APPL_ID", String.valueOf(respAppId));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Send Work order for Approval for Hqc Error",e);
            }

            return result;
        }
        else {
            String result = "";

            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/oaf_wf_start/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/oaf_wf_start/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("P_PROCESS", "XXX_PHASE1_PRC");
                param.put("P_TRANSACTION_ID", String.valueOf(entityId));
                param.put("P_REQUESTOR_ID", String.valueOf(userId));
                param.put("P_RESP_ID", String.valueOf(respId));
                param.put("P_RESP_APPL_ID", String.valueOf(respAppId));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Send Work order for Approval Error",e);
            }

            return result;
        }
    }

    public String validateSparePartsApproval(int WipEntityId,int orgId){
        String result="";
//        String statement="SELECT (SELECT COUNT(*) FROM wip_eam_direct_items WHERE wip_entity_id =?) AS splines, (Select XXSTC_WIP_HQ_WO_WF_SOA_PKG.IS_HQC_ORG(?) as hqc FROM dual) AS ISHQC,\n" +
//                        "((SELECT  COUNT(*) FROM fnd_attached_documents fad,fnd_documents_vl fdv WHERE FAD.DOCUMENT_ID = FDV.DOCUMENT_ID AND entity_name='EAM_WORK_ORDERS' AND pk2_value = TO_CHAR(?))-\n" +
//                        "(SELECT COUNT(DISTINCT SEQ_NUM) as WR_ATTACHEMENTS FROM FND_ATTACHED_DOCS_FORM_VL WHERE entity_name like 'EAM_WORK_REQUESTS' and \n" +
//                        "pk1_value=(SELECT DISTINCT wewr.work_request_id FROM apps.wip_eam_work_requests wewr WHERE wewr.wip_entity_id =?))) AS attachments_count\n" +
//                        "FROM dual";
         String statement = "SELECT\n" +
"\n" +
"XXSTC_WIP_WO_CUSTOM.CHECK_LINES_SPR("+WipEntityId+") SPR,\n" +
"\n" +
"XXSTC_WIP_HQ_WO_WF_SOA_PKG.IS_HQC_ORG("+orgId+") as hqc,\n" +
"\n" +
"XXSTC_WIP_WO_CUSTOM.CHECK_LINES_AND_ATTACHMENTS("+WipEntityId+") Attachements from dual";
//        try{
//            connection  = pool.getConnection();
//            connection.setAutoCommit(false);
//            preparedStatement = connection.prepareStatement(statement);
//            preparedStatement.setFetchSize(100);
//            preparedStatement.setInt(1, WipEntityId);
//            preparedStatement.setInt(2, orgId);
//            preparedStatement.setInt(3, WipEntityId);            
////            preparedStatement.setInt(4, WipEntityId);
//
//            resultSet = preparedStatement.executeQuery();
//            resultSet.setFetchSize(100);
//
//            JsonObject jsonResult = new JsonObject();
//            JsonArray jsonArray = new JsonArray();
//
//            while (resultSet.next()){
//                JsonObject jsonObject = new JsonObject();
//                jsonResult.addProperty("isHqc",resultSet.getString(2));
//                jsonObject.addProperty("SpLines",resultSet.getString(1));
//                jsonObject.addProperty("AttachedLines",resultSet.getString(3));
//                jsonArray.add(jsonObject);
//            }
//
//            jsonResult.add("validateSparePartsApproval",jsonArray);
//            result = jsonResult.toString();
//            connection.close();
//            preparedStatement.close();
//            resultSet.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            logger.error("validate Spare Parts Approval Error", e);
//        }
//        finally {
//            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
//        }
     try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);

            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "validateSparePartsApproval");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("validateSparePartsApproval");

            JSONObject jsonResult = new JSONObject();
            JSONArray arrayResult = new JSONArray();
            
            
            for (int i = 0; i < jsonArray.length(); i++) {
             JSONObject jsonObject1 = new JSONObject();
             jsonObject1.put("isHqc", jsonArray.getJSONObject(i).get("HQC"));
             jsonObject1.put("SpLines", jsonArray.getJSONObject(i).get("SPR"));
             jsonObject1.put("AttachedLines", jsonArray.getJSONObject(i).get("ATTACHEMENTS"));
             arrayResult.put(jsonObject1);
         }

            jsonResult.put("validateSparePartsApproval", arrayResult);

            result = jsonResult.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("validateSparePartsApproval Error", e);
//            logger.error("QUERY : " + statement);
        }
        System.out.println("//validateSparePartsApproval Return Result \n");
        System.out.println(result);

        return result;
    }

    public String sendSparePartsForApproval(boolean isHqc,
                                            int entityId,
                                            int userId,
                                            int respId,
                                            int respAppId) {
        String result = "";
        if (isHqc) {
            // 4 Attachments and 1 Line of Spare Part
            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/oaf_wf_start/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/oaf_wf_start/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("P_PROCESS", "XXX_PHASE2_PRC");
                param.put("P_TRANSACTION_ID", String.valueOf(entityId));
                param.put("P_REQUESTOR_ID", String.valueOf(userId));
                param.put("P_RESP_ID", String.valueOf(respId));
                param.put("P_RESP_APPL_ID", String.valueOf(respAppId));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Send Spare Parts For Approval for Hqc Error",e);
            }
        }
        else {
            // 1 Attachments and 1 Line of Spare Part
            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/oaf_wf_start/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/oaf_wf_start/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("P_PROCESS", "XXX_PHASE2_PRC");
                param.put("P_TRANSACTION_ID", String.valueOf(entityId));
                param.put("P_REQUESTOR_ID", String.valueOf(userId));
                param.put("P_RESP_ID", String.valueOf(respId));
                param.put("P_RESP_APPL_ID", String.valueOf(respAppId));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Send Spare Parts For Approval Error",e);
            }
        }
        return result;
    }


    public String sendforApprovalSPnHqc(boolean isHqc,
                                        int entityId,
                                        int userId,
                                        int respId,
                                        int respAppId){
        String result = "";
        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_wf_soa_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_soa_pkg/oaf_wf_start/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_soa_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_soa_pkg/oaf_wf_start/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_PROCESS", "XXX_PHASE2_PRC");
            param.put("P_TRANSACTION_ID", String.valueOf(entityId));
            param.put("P_REQUESTOR_ID", String.valueOf(userId));
            param.put("P_RESP_ID", String.valueOf(respId));
            param.put("P_RESP_APPL_ID", String.valueOf(respAppId));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Send Spare Parts For Approval Error",e);
        }
        return result;
    }

    public String sendCompleteWorkOrderForApproval(String isHqc,
                                                   int entityId,
                                                   int userId,
                                                   int respId,
                                                   int respAppId) {
        if ("Y".equals(isHqc)) {
            String result = "";
            try{
                
                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/oaf_wf_start/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/oaf_wf_start/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("P_PROCESS", "XXX_PHASE3_PRC");
                param.put("P_TRANSACTION_ID", String.valueOf(entityId));
                param.put("P_REQUESTOR_ID", String.valueOf(userId));
                param.put("P_RESP_ID", String.valueOf(respId));
                param.put("P_RESP_APPL_ID", String.valueOf(respAppId));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Update Work Request Error",e);
            }

            return result;
        }
        else {
            String result = "";
            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/oaf_wf_start/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/oaf_wf_start/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("P_PROCESS", "XXX_PHASE3_PRC");
                param.put("P_TRANSACTION_ID", String.valueOf(entityId));
                param.put("P_REQUESTOR_ID", String.valueOf(userId));
                param.put("P_RESP_ID", String.valueOf(respId));
                param.put("P_RESP_APPL_ID", String.valueOf(respAppId));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Update Work Request Error",e);
            }

            return result;
        }
    }

    public String cancelWorkRequestWs(int entityId) {

        String result = "";

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/cancel_wo/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/cancel_wo/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_TRANSACTION_ID", String.valueOf(entityId));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("cancel Work Request Ws Error",e);
        }
        return result;
    }


    public String isWoNotGetFromIGate(String transactionID) {

        String result = null;

        String statement = "select count(*) as COUNT from APPS.wip_discrete_jobs wdj, APPS.hr_all_organization_units hrorg, wip_eam_work_requests wr where hrorg.organization_id = wdj.organization_id \n"  +
        "and wdj.wip_entity_id = wr.wip_entity_id and wr.attribute15 is not null and wdj.wip_entity_id = ?";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setString(1, transactionID);
            resultSet = preparedStatement.executeQuery();
            //resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            resultSet.next();
            jsonResult.addProperty("count",resultSet.getInt(1));

/*            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("count",resultSet.getInt(1));
                jsonArray.add(jsonObject);
            }*/
/*
            jsonResult.add("counts",jsonArray);*/
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("is Wo Not Get From IGate Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String isContractor(String respId){
        String result = "";

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_custom_soa_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_custom_soa_pkg/is_contractor_responsibility/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_custom_soa_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_custom_soa_pkg/is_contractor_responsibility/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_RESP_ID",respId);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("contractor check Error",e);
        }
        return result;
    }

    public String checkLinesAndAttachments(String transactionID){
        String result = "";

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_custom_soa_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_custom_soa_pkg/check_lines_and_attachments/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_custom_soa_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_custom_soa_pkg/check_lines_and_attachments/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_TRANSACTION_ID",transactionID);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("check lines and attachments Error",e);
        }
        return result;
    }

    public String isHqcOrg(String organizationID){
        String result = "";

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/is_hqc_org/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_soa_pkg/is_hqc_org/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_ORG_ID",organizationID);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("hq organization check Error",e);
        }
        return result;
    }
     //------------------------
     public String getWorkOrderWorkFlowStatus(int wipId) {

         String result = null;

         String statement ="select distinct(lookup_code) as lookup_code, meaning from fnd_lookup_values vl \n" +
                 "where lookup_type =  'XXX_STC_WO_WF_STATUS' \n" +
                 "and lookup_code IN (select attribute15 from wip_discrete_jobs where wip_entity_id \n" +
                 /*"IN (select wewr.wip_entity_id from APPS.wip_eam_work_requests wewr  where wewr.work_request_id = ?) \n" +*/
                 "= ?)";

         try {
             connection  = pool.getConnection();
             connection.setAutoCommit(false);
             preparedStatement = connection.prepareStatement(statement);
             preparedStatement.setFetchSize(100);
             preparedStatement.setInt(1, wipId);
             resultSet = preparedStatement.executeQuery();
             resultSet.setFetchSize(100);

             JsonObject jsonResult = new JsonObject();
             JsonArray jsonArray = new JsonArray();

             while (resultSet.next()){
                 JsonObject jsonObject = new JsonObject();
                 jsonObject.addProperty("STATUS",resultSet.getString(1));
                 jsonObject.addProperty("MEANING",resultSet.getString(2));
                 jsonArray.add(jsonObject);
             }

             jsonResult.add("WORK_ORDER_WORK_FLOW_STATUS",jsonArray);
             result = jsonResult.toString();

             connection.close();
             preparedStatement.close();
             resultSet.close();
         }
         catch (Exception e) {
             e.printStackTrace();
             logger.error("Get Work order work flow status Error", e);
         }
         finally {
             closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
         }
         return result;
     }



     //USED to SHOW THE WO STATUS FOR HQC
    public String getWorkOrderStatusForHQC(int wipId) {

        String result = null;

        String statement = "select distinct(lookup_code) as lookup_code, meaning from fnd_lookup_values vl \n" +
                 "where lookup_type =  'XXX_STC_HQ_WO_STATUS' AND language = 'US' \n" +
                 "                 and lookup_code IN (select ATTRIBUTE11 from wip_discrete_jobs where wip_entity_id\n" +
                         /*"IN (select wewr.wip_entity_id from APPS.wip_eam_work_requests wewr  where wewr.work_request_id = ?) \n" +*/
                "= ?)";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, wipId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("STATUS",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_FLOW_STATUS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Status For HQC error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    //USED to SHOW THE WO STATUS FOR District
    public String getWorkOrderStatusForDistrict(int wipId) {

        String result = null;

        String statement = "select distinct(lookup_code) as lookup_code, meaning from fnd_lookup_values vl \n" +
                 "where lookup_type =  'XXX_STC_WO_STATUS' AND language = 'US' \n" +
                 "                 and lookup_code IN (select ATTRIBUTE11 from wip_discrete_jobs where wip_entity_id=?)";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, wipId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("STATUS",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_ORDER_STATUS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work order status for District Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getSparePartsList(int WipEntityId){
        String result = null;
        JsonObject jsonResult = new JsonObject();

        JsonArray jsonArray = new JsonArray();
        String statement = "select A.* , TO_CHAR(A.NEED_BY_DATE, 'DD-MON-YYYY hh:mmAM') FORMATED_NEED_BY_DATE from wip_eam_direct_items A  WHERE wip_entity_id=?";
        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, WipEntityId);
            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData rsmd = resultSet.getMetaData();
            int numberOfColumns = rsmd.getColumnCount();
            while(resultSet.next()){
                JsonObject jsonObject2 = new JsonObject();
                for (int i=1; i<=numberOfColumns; i++ ) {
                    jsonObject2.addProperty(rsmd.getColumnName(i), resultSet.getString(rsmd.getColumnName(i)));
                }
                jsonArray.add(jsonObject2);
            }
            jsonResult.add("result", jsonArray);
            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            jsonResult.addProperty("message", e.getMessage());

            e.printStackTrace();
            logger.error("Get Spare Parts List Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        result = jsonResult.toString();
        return result;
    }


    void closeAllConnection(Connection connection,
                            PreparedStatement preparedStatement,
                            ResultSet resultSet,
                            CallableStatement callableStatement,
                            NamedParameterStatement namedParameterStatement) {

        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

        if (namedParameterStatement != null) {
            try {
                namedParameterStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing namedParameterStatement Error", e);
            }
        }
    }
        //need to connect with fauji for this
/*    public String getWorkOrdersV1(int orgId,
                                String workOrderName,
                                String assetNumber,
                                String assetDescription,
                                String owningDepartment,
                                Date startDateFrom,
                                Date startDateTo,
                                String status,
                                String planned,
                                String requestNumber) {
        String result = null;

        String statement = "select * from (SELECT WEWODV.*, round((WDJ.SCHEDULED_COMPLETION_DATE - WDJ.SCHEDULED_START_DATE) * 24, 3) AS duration, \n" +
                "(SELECT DISTINCT WEWR.WORK_REQUEST_ID FROM APPS.WIP_EAM_WORK_REQUESTS WEWR WHERE WEWR.WIP_ENTITY_ID = WEWODV.WIP_ENTITY_ID AND ROWNUM = 1) AS REQUEST_NUMBER,\n" +
                "WDJ.SCHEDULED_START_DATE, WDJ.SCHEDULED_COMPLETION_DATE FROM APPS.wip_eam_work_order_dtls_v WEWODV, APPS.WIP_DISCRETE_JOBS WDJ WHERE\n" +
                "WEWODV.WIP_ENTITY_ID = WDJ.WIP_ENTITY_ID) QRLST WHERE ORGANIZATION_ID = :orgId ";

        if (workOrderName != null && !workOrderName.isEmpty()) {
            statement += " AND (WIP_ENTITY_NAME LIKE :workOrderName OR WIP_ENTITY_NAME IS NULL)";
        }
        if (assetNumber != null && !assetNumber.isEmpty()) {
            statement += " AND (MAINTENANCE_OBJECT_ID LIKE :assetNumber OR MAINTENANCE_OBJECT_ID IS NULL)";
        }
        if (assetDescription != null && !assetDescription.isEmpty()) {
            statement += " AND (ASSET_DESCRIPTION LIKE :assetDescription OR ASSET_DESCRIPTION IS NULL)";
        }
        if (owningDepartment != null && !owningDepartment.isEmpty()) {
            statement += " AND (ASSET_OWNING_DEPARTMENT_ID LIKE :owningDepartment OR ASSET_OWNING_DEPARTMENT_ID IS NULL)";
        }
        if (startDateFrom != null) {
            statement += " AND (SCHEDULED_START_DATE >= :startDate )";
        }
        if (startDateTo != null) {
            statement += " AND (SCHEDULED_START_DATE <= :endDate )";
        }
        if (status != null && !status.isEmpty()) {
            statement += " AND (STATUS_TYPE LIKE :status OR STATUS_TYPE IS NULL)";
        }
        if (planned != null && !planned.isEmpty()) {
            statement += " AND (PLAN_MAINTENANCE LIKE :planned)";
        }
        if (requestNumber != null && !requestNumber.isEmpty()) {
            statement += " AND REQUEST_NUMBER LIKE :requestNumber";
        }

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            namedParameterStatement = new NamedParameterStatement(connection,statement);
            namedParameterStatement.setFetchSize(1000);
            namedParameterStatement.setInt("orgId",orgId);

            if (workOrderName != null && !workOrderName.isEmpty()) {
                namedParameterStatement.setString("workOrderName", "%" + workOrderName + "%");
            }
            if (assetNumber != null && !assetNumber.isEmpty()) {
                namedParameterStatement.setString("assetNumber", "%" + assetNumber + "%");
            }
            if (assetDescription != null && !assetDescription.isEmpty()) {
                namedParameterStatement.setString("assetDescription", "%" + assetDescription + "%");
            }
            if (owningDepartment != null && !owningDepartment.isEmpty()) {
                namedParameterStatement.setString("owningDepartment", "%" + owningDepartment + "%");
            }
            if (startDateFrom!=null) {
                namedParameterStatement.setDate("startDate", startDateFrom);
            }
            if (startDateTo!=null) {
                namedParameterStatement.setDate("endDate", startDateTo);
            }
            if (status != null && !status.isEmpty()) {
                namedParameterStatement.setString("status", "%" + status + "%");
            }
            if (planned != null && !planned.isEmpty()) {
                if (planned.equalsIgnoreCase("yes")) {
                    planned = "Y";
                }
                if (planned.equalsIgnoreCase("no")) {
                    planned = "N";
                }
                if (planned.equalsIgnoreCase("all")) {
                    planned = "";
                }
                namedParameterStatement.setString("planned", "%" + planned + "%");
            }
            if (requestNumber != null && !requestNumber.isEmpty()) {
                namedParameterStatement.setString("requestNumber", "%" + requestNumber + "%");
            }

            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(1000);
            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("WIP_ENTITY_ID", resultSet.getString("WIP_ENTITY_ID"));
                jsonObject.addProperty("WIP_ENTITY_NAME", resultSet.getString("WIP_ENTITY_NAME"));
                jsonObject.addProperty("ASSET_NUMBER", resultSet.getString("ASSET_NUMBER"));
                jsonObject.addProperty("ASSET_DESCRIPTION", resultSet.getString("ASSET_DESCRIPTION"));
                jsonObject.addProperty("OWNING_DEPARTMENT", resultSet.getString("ASSET_OWNING_DEPARTMENT"));
                jsonObject.addProperty("SCHEDULED_START_DATE", resultSet.getString("SCHEDULED_START_DATE"));
                jsonObject.addProperty("SCHEDULED_COMPLETION_DATE", resultSet.getString("SCHEDULED_COMPLETION_DATE"));
                jsonObject.addProperty("STATUS", resultSet.getString("STATUS"));
                String planMaintenance = resultSet.getString("PLAN_MAINTENANCE");
                if (planMaintenance == null) {
                    planMaintenance = "null";
                }
                else {
                    if (planMaintenance.equals("Y")) {
                        planMaintenance = "Yes";
                    }
                    if (planMaintenance.equals("N")) {
                        planMaintenance = "No";
                    }
                }
                jsonObject.addProperty("PLANNED", planMaintenance);
                jsonObject.addProperty("REQUEST_NUMBER", resultSet.getString("REQUEST_NUMBER"));

                //jsonObject.addProperty("ASSET_GROUP", resultSet.getString("ASSET_GROUP"));
                //jsonObject.addProperty("DESCRIPTION", resultSet.getString("DESCRIPTION"));
                //jsonObject.addProperty("DURATION", resultSet.getString("DURATION"));




                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_ORDERS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            namedParameterStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Orders Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }*/


    public String getWorkOrderMaterialUOMS() {

        String result = null;

        String statement = "select distinct(UOM) as uom from wip_eam_direct_items";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);


            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("WORK_ORDER_MATERIALS_UOM",resultSet.getString(1));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_ORDER_MATERIALS_UOMS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Order Materials UOM Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String removeMaterialDetails(String DIRECT_ITEM_SEQUENCE_ID, String WIP_ENTITY_ID, String ORGANIZATION_ID) {
        String result = "";
//        String statement = "DELETE FROM wip_eam_direct_items WHERE DIRECT_ITEM_SEQUENCE_ID= "+DIRECT_ITEM_SEQUENCE_ID;
//        try {
//            connection  = pool.getConnection();
//            connection.setAutoCommit(true);
//
//            preparedStatement = connection.prepareStatement(statement);
//            preparedStatement.setInt(1, DIRECT_ITEM_SEQUENCE_ID);

//            int a = preparedStatement.executeUpdate();
//            System.out.println("Result: "+a);
//            JsonObject jsonResult = new JsonObject();
//            if (a>0){
//                jsonResult.addProperty("Material_Status","Deleted");
//            }else{
//                jsonResult.addProperty("Material_Status","Error");
//            }
//            result = jsonResult.toString();
//            connection.close();
//            preparedStatement.close();
//
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Delete Material Error",e);
//        }
//        finally {
//            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
//        }
        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxxstc_eam_update_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/delete_wo_dir_item/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/delete_wo_dir_item/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("P_ORGANIZATION_ID", ORGANIZATION_ID);
            param.put("P_WIP_ENTITY_ID", WIP_ENTITY_ID);
            param.put("P_DIRECT_ITEM_SEQUENCE_ID", DIRECT_ITEM_SEQUENCE_ID);           
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "DELETE");
            JSONObject jsonObject = new JSONObject(result);
          
            result = jsonObject.toString();
            System.out.println("removeMaterialDetails result: \n"+result);
            
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("removeMaterialDetails Error", e);
        }
        return result;
    }

    public String updateMaterialDetails(
            String P_WIP_ENTITY_ID, //mandatory
            String P_OPERATION_SEQ_NUM,
            String P_DIRECT_ITEM_SEQUENCE_ID, //mandatory
            String P_DESCRIPTION,
            String P_ORGANIZATION_ID,//mandatory
            String P_REQUIRED_QUANTITY,
            String P_UOM,
            String P_NEED_BY_DATE,
            String P_ORDER_TYPE_LOOKUP_CODE,
            String P_COST_ATT1,
            String P_CONSUMABLE_ATT2,
            String P_NOTES_ATT3,
            String P_IMPORTED_ATT4,
            String P_VENDOR_ATT5,
            String P_INVOICE_NO_ATT6,
            String P_AGENT_ATT7,
            String P_WARANTY_PERIOD_ATT8,
            String P_RETURNED_ATT9,
            String P_VENDOR_PHONE_ATT10,
            String P_IMPORTPERIOD_ATT12,
            String P_ATT13,
            String P_ATT14,
            String P_ATT15
    ){
        System.out.println("In-Request Method Send");
        String result = "";
        String totalCost="";

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxxstc_eam_update_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/update_wo_dir_item/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/update_wo_dir_item/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_WIP_ENTITY_ID",P_WIP_ENTITY_ID);
            param.put("P_OPERATION_SEQ_NUM",P_OPERATION_SEQ_NUM);
            param.put("P_DIRECT_ITEM_SEQUENCE_ID",P_DIRECT_ITEM_SEQUENCE_ID);
            param.put("P_DESCRIPTION",P_DESCRIPTION);
            param.put("P_ORGANIZATION_ID",P_ORGANIZATION_ID);
            param.put("P_REQUIRED_QUANTITY",P_REQUIRED_QUANTITY);
            param.put("P_UOM",P_UOM);
            param.put("P_NEED_BY_DATE",P_NEED_BY_DATE);
            param.put("P_ORDER_TYPE_LOOKUP_CODE",P_ORDER_TYPE_LOOKUP_CODE);
            param.put("P_COST_ATT1",P_COST_ATT1);
            param.put("P_CONSUMABLE_ATT2",P_CONSUMABLE_ATT2);
            param.put("P_NOTES_ATT3",P_NOTES_ATT3);
            param.put("P_IMPORTED_ATT4",P_IMPORTED_ATT4);
            param.put("P_VENDOR_ATT5",P_VENDOR_ATT5);
            param.put("P_INVOICE_NO_ATT6",P_INVOICE_NO_ATT6);
            param.put("P_AGENT_ATT7",P_AGENT_ATT7);
            param.put("P_WARANTY_PERIOD_ATT8",P_WARANTY_PERIOD_ATT8);
            param.put("P_RETURNED_ATT9",P_RETURNED_ATT9);
            param.put("P_VENDOR_PHONE_ATT10",P_VENDOR_PHONE_ATT10);
            param.put("P_TOTAL_ATT11","");
            param.put("P_IMPORTPERIOD_ATT12",P_IMPORTPERIOD_ATT12);
            param.put("P_ATT13",P_ATT13);
            param.put("P_ATT14",P_ATT14);
            param.put("P_ATT15",P_ATT15);

            seo.setParameter(param);
            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("update Material Details Error",e);
        }
        return result;
    }

    public String getCreatorName(int orgId){
        String result = null;

        String statement = "SELECT * FROM (SELECT user_name, user_id, (CASE WHEN full_name IS NULL THEN user_name ELSE full_name END) AS user_full_name\n" +
                "          FROM apps.fnd_user fu, (SELECT * FROM apps.per_all_people_f WHERE employee_number IS NOT NULL\n" +
                "          AND TRUNC (SYSDATE) BETWEEN effective_start_date AND effective_end_date) pe\n" +
                "          WHERE (fu.end_date IS NULL OR fu.end_date > SYSDATE) AND fu.employee_id = pe.person_id(+)\n" +
                "          AND (    UPPER (user_name) NOT LIKE '%SYSADMIN%'\n" +
                "          AND UPPER (user_name) NOT LIKE '%ORACLE%'\n" +
                "          AND UPPER (user_name) NOT LIKE '%@%')) qrslt\n" +
                "          WHERE user_id IN (SELECT DISTINCT (created_by) FROM apps.wip_eam_work_requests WHERE organization_id = ?)";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, orgId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("USER_NAME",resultSet.getString(1));
                jsonObject.addProperty("USER_ID",resultSet.getString(2));
                jsonObject.addProperty("FULL_NAME",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("CREATORNAME",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Creator Name Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }
    
    
    public String getApprovalHistory(int wipEntityId) {

        String result = null;

        String statement = "SELECT SEQ_NUM,WIP_ENTITY_ID, APPROVER_NAME, APPROVAL_STATUS, to_char(CREATION_DATE, 'dd-MON-yyyy hh:miAM') AS CREATION_DATE, EMAIL, PHONE_NUMBER, REJECT_REASON FROM APPS.XXX_STC_WORK_ORDER_APPROVER_V \n"
                + "WHERE WIP_ENTITY_ID=?";

        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, wipEntityId);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("SEQ_NUM",resultSet.getString(1));
                //jsonObject.addProperty("WIP_ENTITY_ID",resultSet.getString(2));
                jsonObject.addProperty("APPROVER_NAME",resultSet.getString(3));
                jsonObject.addProperty("APPROVAL_STATUS",resultSet.getString(4));
                jsonObject.addProperty("CREATION_DATE",resultSet.getString(5));
                jsonObject.addProperty("EMAIL",resultSet.getString(6));
                jsonObject.addProperty("PHONE_NUMBER",resultSet.getString(7));
                jsonObject.addProperty("REJECT_REASON",resultSet.getString(8));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("APPROVAL_HISTORY",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Approval History",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }
        public String wfStatuslov() {

        String result = null;

        String statement = "SELECT lookup_code, meaning\n"
                + " FROM fnd_lookup_values f2\n"
                + " WHERE f2.lookup_type(+) = 'XXX_STC_WO_WF_STATUS' AND language = 'US'";

        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("lookup_code", resultSet.getString(1));
                jsonObject.addProperty("meaning", resultSet.getString(2));

                jsonArray.add(jsonObject);
            }

            jsonResult.add("wo_wfStatuslov", jsonArray);
            result = jsonResult.toString();
            System.out.println(result);

            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("error wfStatuslov : ", e);
        } finally {
            closeAllConnection(connection, preparedStatement, resultSet, callableStatement, namedParameterStatement);
        }
        return result;
    }
    
    
//    
//    public static void main(String args[]){
//    WorkOrderController a = new WorkOrderController();
////    a.validateSparePartsApproval(388866,9401);
////    a.removeMaterialDetails("3822", , 9401);
//
//                a.getWorkOrders(9401,
//                                "",
//                                "",
//                                "",
//                                "",
//                                null,
//                                null,
//                                "",
//                                "",
//                                "",
//                                "Y",
//                                "",
//                                "",
//                                "",
//                                "58998",
//                                "23119",
//                                "426",
//                                "A1"
//                                        );
//    }
////   

}
