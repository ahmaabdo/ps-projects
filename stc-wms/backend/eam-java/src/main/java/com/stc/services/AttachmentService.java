package com.stc.services;

import com.stc.controller.AttachmentController;
import com.stc.controller.WorkOrderController;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 04/01/18
 * Time: 15:33
 * To change this template use File | Settings | File Templates.
 */

@Path("/attachment")
public class AttachmentService {

    @GET
    @Path("getTextAttachment")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getTextAttachment(@QueryParam("dataType")String dataType,
                                      @QueryParam("mediaId")String mediaId) {

        if (dataType == null || mediaId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
                String result;
                AttachmentController attachmentController = new AttachmentController();
                result = attachmentController.getTextAttachment(dataType,mediaId);

                if (result.isEmpty()) {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                }
                else {
                    return Response
                            .status(200)
                            .entity(result)
                            .type(MediaType.APPLICATION_JSON)
                            .build();
                }
            }
    }

    @GET
    @Path("getFileAttachment")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces(MediaType.TEXT_PLAIN)
    public Response getFileAttachment(@QueryParam("mediaId") String mediaId) throws SQLException {
        if (mediaId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            AttachmentController attachmentController = new AttachmentController();
            Blob blob = attachmentController.getFileAttachment(mediaId);
            if (blob == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(blob.getBinaryStream())
                        .type(MediaType.TEXT_PLAIN)
                        .build();
            }
        }
    }

    @GET
    @Path("getAttachmentList")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAttachmentList(@QueryParam("requestType") String requestType,
                                      @QueryParam("pkValue1") String pkValue1,
                                      @QueryParam("pkValue2") String pkValue2,
                                      @QueryParam("language") String language
    ) {
        if (requestType == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            AttachmentController attachmentController = new AttachmentController();
            String result = attachmentController.getAttachmentList(requestType,pkValue1,pkValue2, language);
            if (result == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getAttachmentDetails")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAttachmentDetails(@QueryParam("documentId") int documentId,
                                         @QueryParam("language") String language) {
        if (documentId == 0 || language == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            AttachmentController attachmentController = new AttachmentController();
            String result = attachmentController.getAttachmentDetails(documentId, language);

            if (result == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getCatalogAttachment")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getCatalogAttachment(@QueryParam("title") String title) {
        AttachmentController attachmentController = new AttachmentController();
        String result = attachmentController.getCatalogAttachment(title);
        if (result == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    /**
     *
     * categoryId list
     * 1 -> Miscellaneous
     * 1001262 -> Work Request Attachments
     * 1000540 -> Asset Attachments
     * 1000541 -> Maintenance Job Attachments
     * 1000640 -> Routing Attachments
     * 5 -> Operation Attachments
     *
     * entityName list
     * Work Request -> EAM_WORK_REQUESTS
     * Work Order -> EAM_WORK_ORDERS
     * Operations -> EAM_DISCRETE_OPERATIONS
     *
     * pk1Value -> request number for WR, orgId for WO, entityId for operation
     * pk2Value -> null for WR, entityId for WO, sequence number for operation
     * pk3Value -> organization_id for operation
     *
     * dataType
     * 1 -> short text
     * 2 -> long text
     * 5 -> url
     * 6 & 7 -> file
     *
     */

    @POST
    @Path("addAttachment")
    @Consumes("multipart/form-data")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response addAttachment(@FormDataParam("file") InputStream uploadedInputStream,
                                  @FormDataParam("file") FormDataContentDisposition fileDetail,
                                  @FormDataParam("file") FormDataBodyPart body,
                                  @FormDataParam("title") String title,
                                  @FormDataParam("description") String description,
                                  @FormDataParam("categoryId") String categoryId,
                                  @FormDataParam("text") String text,
                                  @FormDataParam("url") String url,
                                  @FormDataParam("entityName") String entityName,
                                  @FormDataParam("pkValue1") String pkValue1,
                                  @FormDataParam("pkValue2") String pkValue2,
                                  @FormDataParam("pkValue3") String pkValue3,
                                  @FormDataParam("userId") String userId) {

        if ((pkValue1 == null || userId == null || categoryId == null) || (uploadedInputStream != null && text!= null && url != null)) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            String dataType = null;

            if (categoryId.equals("2")) {
                categoryId = "1001262";
            }

            if (text != null) {
                if (text.length() < 2000) {
                    dataType = "1";
                }
                else {
                    dataType = "2";
                }
            }
            if (url != null) {
                dataType = "5";
            }

            String filename= null;
            String contentType = null;

            if (fileDetail!= null && body != null) {
                dataType = "6";
                filename = fileDetail.getFileName();
                contentType = body.getMediaType().toString();
            }

            AttachmentController attachmentController = new AttachmentController();
            System.out.println(uploadedInputStream+" , "+filename+" , "+contentType+" , "+title+" , "+description+" , "+categoryId+" , "+dataType+" , "+text+" , "+url+" , "+entityName+" , "+pkValue1+" , "+pkValue2+" , "+pkValue3+" , "+userId);
            if (dataType != null) {
                result = attachmentController.addAttachment(uploadedInputStream,filename,contentType,title,description,categoryId,dataType,text,url,entityName,pkValue1,pkValue2,pkValue3,userId);
                if (result.isEmpty()) {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                }
                else {
                    return Response
                            .status(200)
                            .entity(result)
                            .type(MediaType.TEXT_PLAIN)
                            .build();
                }
            }
            else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }
    }

    @POST
    @Path("updateAttachment")
    @Consumes("multipart/form-data")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response updateAttachment(
                                     @FormDataParam("categoryId")String category_id,
                                     @FormDataParam("documentDescription")String document_description,
                                     @FormDataParam("text")String text,
                                     @FormDataParam("title")String p_title,
                                     @FormDataParam("url")String url,
                                     @FormDataParam("attachedDocumentId")String attached_document_id,
                                     @FormDataParam("dataTypeId")String dataType,
                                     @FormDataParam("entityName")String entity_name,
                                     @FormDataParam("pkValue1")String pk1_value,
                                     @FormDataParam("pkValue2")String pk2_value,
                                     @FormDataParam("pkValue3")String pk3_value,
                                     @FormDataParam("mediaId")String media_id,
                                     @FormDataParam("userId")String user_id,
                                     @FormDataParam("file") InputStream uploadedInputStream,
                                     @FormDataParam("file") FormDataContentDisposition fileDetail,
                                     @FormDataParam("file") FormDataBodyPart body) {

        if (pk1_value == null || user_id == null || attached_document_id == null || (uploadedInputStream != null && text!= null && url != null)) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;

            String p_file_name= null;
            String p_content_type = null;

            if (fileDetail!= null && body != null) {
                dataType = "6";
                p_file_name = fileDetail.getFileName();
                p_content_type = body.getMediaType().toString();
            }

            AttachmentController attachmentController = new AttachmentController();
            if (dataType != null) {
                result = attachmentController.updateAttachment(
                        category_id,
                        document_description,
                        text,
                        p_title,
                        p_file_name,
                        url,
                        attached_document_id,
                        dataType,
                        entity_name,
                        pk1_value,
                        pk2_value,
                        pk3_value,
                        media_id,
                        user_id,
                        p_content_type,
                        uploadedInputStream
                );
                if (result.isEmpty()) {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                }
                else {
                    return Response
                            .status(200)
                            .entity(result)
                            .type(MediaType.TEXT_PLAIN)
                            .build();
                }
            }
            else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }
    }

    @POST
    @Path("deleteAttachment")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response deleteAttachment(@FormParam("attachedDocumentId") String attached_document_id,
                                     @FormParam("dataTypeId") String datatype_id,
                                     @FormParam("mediaId") String media_id) {

        if (attached_document_id == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            AttachmentController attachmentController = new AttachmentController();

            result = attachmentController.deleteAttachment(attached_document_id,datatype_id,media_id);
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.TEXT_PLAIN)
                        .build();
            }
        }
    }

    @PUT
    @Path("publishToCatalog")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response publishToCatalog(@FormParam("documentId") String documentId) {

        String result;
        if (documentId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            AttachmentController attachmentController = new AttachmentController();
            result = attachmentController.publishToCatalog(documentId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("addAttachmentFromCatalog")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response addAttachmentFromCatalog(@FormParam("documentId")String documentId,
                                             @FormParam("pkValue1")String pkValue1,
                                             @FormParam("pkValue2")String pkValue2,
                                             @FormParam("pkValue3")String pkValue3) {

        if (documentId == null || pkValue1 == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            AttachmentController attachmentController = new AttachmentController();
            String result = attachmentController.addAttachmentFromCatalog(documentId, pkValue1, pkValue2, pkValue3);

            if (result == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
    
    @GET
    @Path("getAttachmentSize")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAttachmentSize(@QueryParam("userID")String userID,
                                  @QueryParam("respId")String respId,
                                  @QueryParam("respAppId")String respAppId
    ) {

        if ( userID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            AttachmentController AttachmentController = new AttachmentController();
            result = AttachmentController.getAttachmentSize(
                    userID,
                    respId,
                    respAppId
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
}