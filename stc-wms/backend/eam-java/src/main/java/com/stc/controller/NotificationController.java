package com.stc.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.stc.controller.StatisticReportController.pool;
import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

import com.stc.controller.AttachmentController;
import java.io.InputStream;

import oracle.jdbc.driver.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 02/01/18
 * Time: 16:08
 * To change this template use File | Settings | File Templates.
 */
public class NotificationController extends PublicController {

    final static Logger logger = Logger.getLogger(NotificationController.class);

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public NamedParameterStatement namedParameterStatement;
    public PreparedStatement preparedStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;
    
    private AttachmentController attachmentController = new AttachmentController();

    public String getWIPEntity(String ItemKey) {

        String result = null;
        PreparedStatement ps;
        ResultSet rs;
        Connection conn;

        String statement = "Select WDJ.wip_entity_id as  wipID FROM APPS.WIP_DISCRETE_JOBS WDJ \n" +
                " WHERE  (WDJ.ATTRIBUTE14 =? or WDJ.ATTRIBUTE12 = ? or WDJ.ATTRIBUTE13  = ?)";
        try {
            conn  = pool.getConnection();

            ps = conn.prepareStatement(statement);

            ps.setString(1, ItemKey);
            ps.setString(2, ItemKey);
            ps.setString(3, ItemKey);
            System.out.println(statement);
            rs = ps.executeQuery();

            while (rs.next()){
                result =  String.valueOf(rs.getInt(1));
            }
            conn.close();
            ps.close();
            rs.close();

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get WIP entity Error", e.getCause());

        }
        finally {
            //closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        System.out.println(result);
        return result;
    }

    public String getRequestId(String ItemKey) {

        String result = null;
        PreparedStatement ps;
        ResultSet rs;
        Connection conn;

        String statement = "Select WEWR.work_request_id FROM APPS.WIP_EAM_WORK_REQUESTS WEWR \n" +
                " WHERE  WEWR.ATTRIBUTE3 =? ";
        try {
            conn  = pool.getConnection();

            ps = conn.prepareStatement(statement);

            ps.setString(1, ItemKey);
            ps.setString(2, ItemKey);
            ps.setString(3, ItemKey);
            System.out.println(statement);
            rs = ps.executeQuery();

            while (resultSet.next()){
                result =  String.valueOf(resultSet.getInt(1));
            }

            conn.close();
            ps.close();
            rs.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get work request id Error", e);
        }
        finally {
            //closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getNotification(String username) {

        String result = null;

        String statement = "SELECT * FROM (select /*+ ORDERED PUSH_SUBQ USE_NL (WN WL WIT) index(WN WF_NOTIFICATIONS_N1)*/ WN.NOTIFICATION_ID,WN.ITEM_KEY, WN.FROM_USER, \n" +
                "WN.TO_USER, WN.SUBJECT, WN.LANGUAGE, WN.BEGIN_DATE, to_char(WN.BEGIN_DATE, 'dd-MON-yyyy hh:mmAM') AS FORMATED_BEGIN_DATE,WN.DUE_DATE, WN.STATUS, WN.PRIORITY, 'P' AS PRIORITY_F, \n" +
                "WN.RECIPIENT_ROLE, WN.END_DATE, WIT.DISPLAY_NAME AS TYPE, WN.MORE_INFO_ROLE, WN.FROM_ROLE, WN.MESSAGE_TYPE, WN.MESSAGE_NAME, WN.MAIL_STATUS, WN.ORIGINAL_RECIPIENT \n" +
                "from APPS.WF_NOTIFICATIONS WN, APPS.WF_ITEM_TYPES_TL WIT, APPS.WF_LOOKUPS_TL WL where WN.STATUS = 'OPEN' and WN.message_type = WIT.name and WIT.language = userenv('LANG') \n" +
                "and WL.lookup_type = 'WF_NOTIFICATION_STATUS' and WN.status = WL.lookup_code and WL.language = userenv('LANG') and WN.recipient_role in (select WUR.role_name \n" +
                "from APPS.WF_USER_ROLES WUR where WUR.user_name = '"+username.toUpperCase()+"') and more_info_role is null AND WIT.name IN ('XXALR', 'XXALRHQC', 'XXXSTCWO', 'XXXSTCWR', 'XXXSTCPR', 'XXXSTCHQ', 'XXXSTCCL', 'XXXSTCCO', 'XXXSTCPT', 'XXSTCFYI') union all select \n" +
                "/*+ ORDERED PUSH_SUBQ USE_NL (WN WL WIT) index(WN WF_NOTIFICATIONS_N6)*/ WN.NOTIFICATION_ID, WN.ITEM_KEY, WN.FROM_USER, DECODE(WN.MORE_INFO_ROLE, NULL, WN.TO_USER, \n" +
                "wf_directory.GetRoleDisplayName(WN.MORE_INFO_ROLE)) AS TO_USER, WN.SUBJECT, WN.LANGUAGE, WN.BEGIN_DATE, TO_CHAR(wn.begin_date,'dd-MON-yyyy hh:mmAM') AS formated_begin_date, WN.DUE_DATE, WN.STATUS, WN.PRIORITY, 'P' AS PRIORITY_F, WN.RECIPIENT_ROLE, WN.END_DATE, WIT.DISPLAY_NAME AS TYPE, \n" +
                "WN.MORE_INFO_ROLE, WN.FROM_ROLE, WN.MESSAGE_TYPE, WN.MESSAGE_NAME, WN.MAIL_STATUS, WN.ORIGINAL_RECIPIENT from APPS.WF_NOTIFICATIONS WN, APPS.WF_ITEM_TYPES_TL WIT, \n" +
                "APPS.WF_LOOKUPS_TL WL where WN.STATUS = 'OPEN' and WN.message_type = WIT.name and WIT.language = userenv('LANG') and WL.lookup_type = 'WF_NOTIFICATION_STATUS' \n" +
                "and WN.status = WL.lookup_code and WL.language = userenv('LANG') and WN.more_info_role in (select WUR.role_name from APPS.WF_USER_ROLES WUR where WUR.user_name = '"+username.toUpperCase()+"') AND WIT.name IN ('XXALR', 'XXALRHQC', 'XXXSTCWO', 'XXXSTCWR', 'XXXSTCPR', 'XXXSTCHQ', 'XXXSTCCL', 'XXXSTCCO', 'XXXSTCPT', 'XXSTCFYI')) \n" +
                "QRSLT ORDER BY BEGIN_DATE desc";
try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
//            param.put("P_USER_ID", userID);
//            param.put("P_RESP_ID", respId);
//            param.put("P_RESP_APPL_ID", respAppId);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "NOTIFICATIONS");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("NOTIFICATIONS");

            JSONObject jsonResult = new JSONObject();
            JSONArray arrayResult = new JSONArray();
            String msgType;
            
            
                        for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("NOTIFICATION_ID", jsonArray.getJSONObject(i).get("NOTIFICATION_ID"));
                jsonObject1.put("FROM_USER", jsonArray.getJSONObject(i).get("FROM_USER"));
                jsonObject1.put("TO_USER", jsonArray.getJSONObject(i).get("TO_USER"));
                jsonObject1.put("SUBJECT", jsonArray.getJSONObject(i).get("SUBJECT"));
                jsonObject1.put("BEGIN_DATE", jsonArray.getJSONObject(i).get("BEGIN_DATE"));
                jsonObject1.put("FORMATED_BEGIN_DATE", jsonArray.getJSONObject(i).get("FORMATED_BEGIN_DATE"));
                jsonObject1.put("DUE_DATE", jsonArray.getJSONObject(i).get("DUE_DATE"));
                jsonObject1.put("STATUS", jsonArray.getJSONObject(i).get("STATUS"));
                jsonObject1.put("PRIORITY", jsonArray.getJSONObject(i).get("PRIORITY"));
             
                 if(jsonArray.getJSONObject(i).getString("MESSAGE_TYPE").contains("WO")|| jsonArray.getJSONObject(i).getString("MESSAGE_TYPE").contains("XXXSTCHQ")){
                   jsonObject1.put("MESSAGETYPE","Work Order");
                   
                } else if(jsonArray.getJSONObject(i).getString("MESSAGE_TYPE").contains("WR")){
                    jsonObject1.put("MESSAGETYPE","Work Request");
                    
                }else if(jsonArray.getJSONObject(i).getString("MESSAGE_TYPE").equalsIgnoreCase("XXXSTCPT")){
                    jsonObject1.put("MESSAGETYPE","Penalty");
                    
                }else if(jsonArray.getJSONObject(i).getString("MESSAGE_TYPE").equalsIgnoreCase("XXXSTCCL")){
                    jsonObject1.put("MESSAGETYPE","Claim");
                    
                }else{
                    jsonObject1.put("MESSAGETYPE","");
                }
                 
                 jsonObject1.put("MESSAGE_TYPE", jsonArray.getJSONObject(i).get("MESSAGE_TYPE"));
                 jsonObject1.put("MESSAGE_NAME", jsonArray.getJSONObject(i).get("MESSAGE_NAME"));
                 jsonObject1.put("MAIL_STATUS", jsonArray.getJSONObject(i).get("MAIL_STATUS"));
                 jsonObject1.put("ITEM_KEY", jsonArray.getJSONObject(i).get("ITEM_KEY"));
       

                arrayResult.put(jsonObject1);

            }



            jsonResult.put("NOTIFICATIONS", arrayResult);

            result = jsonResult.toString();
            System.out.println("getNotification result dat length : "+arrayResult.length());
            System.out.println("getNotification result data : \n"+result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Notification Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
    //** JDBC getNotification **
//    public String getNotification(String username) {
//
//        String result = null;
//
//        String statement = "SELECT * FROM (select /*+ ORDERED PUSH_SUBQ USE_NL (WN WL WIT) index(WN WF_NOTIFICATIONS_N1)*/ WN.NOTIFICATION_ID,WN.ITEM_KEY, WN.FROM_USER, \n" +
//                "WN.TO_USER, WN.SUBJECT, WN.LANGUAGE, WN.BEGIN_DATE, to_char(WN.BEGIN_DATE, 'dd-MON-yyyy hh:mmAM') AS FORMATED_BEGIN_DATE,WN.DUE_DATE, WN.STATUS, WN.PRIORITY, 'P' AS PRIORITY_F, \n" +
//                "WN.RECIPIENT_ROLE, WN.END_DATE, WIT.DISPLAY_NAME AS TYPE, WN.MORE_INFO_ROLE, WN.FROM_ROLE, WN.MESSAGE_TYPE, WN.MESSAGE_NAME, WN.MAIL_STATUS, WN.ORIGINAL_RECIPIENT \n" +
//                "from APPS.WF_NOTIFICATIONS WN, APPS.WF_ITEM_TYPES_TL WIT, APPS.WF_LOOKUPS_TL WL where WN.STATUS = 'OPEN' and WN.message_type = WIT.name and WIT.language = userenv('LANG') \n" +
//                "and WL.lookup_type = 'WF_NOTIFICATION_STATUS' and WN.status = WL.lookup_code and WL.language = userenv('LANG') and WN.recipient_role in (select WUR.role_name \n" +
//                "from APPS.WF_USER_ROLES WUR where WUR.user_name = ?) and more_info_role is null union all select \n" +
//                "/*+ ORDERED PUSH_SUBQ USE_NL (WN WL WIT) index(WN WF_NOTIFICATIONS_N6)*/ WN.NOTIFICATION_ID, WN.ITEM_KEY, WN.FROM_USER, DECODE(WN.MORE_INFO_ROLE, NULL, WN.TO_USER, \n" +
//                "wf_directory.GetRoleDisplayName(WN.MORE_INFO_ROLE)) AS TO_USER, WN.SUBJECT, WN.LANGUAGE, WN.BEGIN_DATE, TO_CHAR(wn.begin_date,'dd-MON-yyyy hh:mmAM') AS formated_begin_date, WN.DUE_DATE, WN.STATUS, WN.PRIORITY, 'P' AS PRIORITY_F, WN.RECIPIENT_ROLE, WN.END_DATE, WIT.DISPLAY_NAME AS TYPE, \n" +
//                "WN.MORE_INFO_ROLE, WN.FROM_ROLE, WN.MESSAGE_TYPE, WN.MESSAGE_NAME, WN.MAIL_STATUS, WN.ORIGINAL_RECIPIENT from APPS.WF_NOTIFICATIONS WN, APPS.WF_ITEM_TYPES_TL WIT, \n" +
//                "APPS.WF_LOOKUPS_TL WL where WN.STATUS = 'OPEN' and WN.message_type = WIT.name and WIT.language = userenv('LANG') and WL.lookup_type = 'WF_NOTIFICATION_STATUS' \n" +
//                "and WN.status = WL.lookup_code and WL.language = userenv('LANG') and WN.more_info_role in (select WUR.role_name from APPS.WF_USER_ROLES WUR where WUR.user_name = ?)) \n" +
//                "QRSLT ORDER BY BEGIN_DATE desc";
//        try {
//
//            connection  = pool.getConnection();
//            connection.setAutoCommit(false);
//            preparedStatement = connection.prepareStatement(statement);
//            preparedStatement.setFetchSize(100);
//            preparedStatement.setString(1, username.toUpperCase());
//            preparedStatement.setString(2, username.toUpperCase());
//            resultSet = preparedStatement.executeQuery();
//            resultSet.setFetchSize(100);
//
//            JsonObject jsonResult = new JsonObject();
//            JsonArray jsonArray = new JsonArray();
//
//            while (resultSet.next()){
//                JsonObject jsonObject = new JsonObject();
//                jsonObject.addProperty("NOTIFICATION_ID",resultSet.getString("NOTIFICATION_ID"));
//                jsonObject.addProperty("FROM_USER",resultSet.getString("FROM_USER"));
//                jsonObject.addProperty("TO_USER",resultSet.getString("TO_USER"));
//                jsonObject.addProperty("SUBJECT",resultSet.getString("SUBJECT"));
//                jsonObject.addProperty("BEGIN_DATE",resultSet.getString("BEGIN_DATE"));
//                jsonObject.addProperty("FORMATED_BEGIN_DATE",resultSet.getString("FORMATED_BEGIN_DATE"));
//                jsonObject.addProperty("DUE_DATE",resultSet.getString("DUE_DATE"));
//                jsonObject.addProperty("STATUS",resultSet.getString("STATUS"));
//                jsonObject.addProperty("PRIORITY",resultSet.getString("PRIORITY"));
//                if(resultSet.getString("MESSAGE_TYPE").contains("WO") || resultSet.getString("MESSAGE_TYPE").contains("XXXSTCHQ")){
//                    jsonObject.addProperty("MESSAGETYPE","Work Order");
//                } else if(resultSet.getString("MESSAGE_TYPE").contains("WR")){
//                    jsonObject.addProperty("MESSAGETYPE","Work Request");
//                }else if(resultSet.getString("MESSAGE_TYPE").equalsIgnoreCase("XXXSTCPT")){
//                    jsonObject.addProperty("MESSAGETYPE","Penalty");
//                }else if(resultSet.getString("MESSAGE_TYPE").equalsIgnoreCase("XXXSTCCL")){
//                    jsonObject.addProperty("MESSAGETYPE","Claim");
//                }else{
//                    jsonObject.addProperty("MESSAGETYPE","");
//                }
//                jsonObject.addProperty("MESSAGE_TYPE",resultSet.getString("MESSAGE_TYPE"));
//                jsonObject.addProperty("MESSAGE_NAME",resultSet.getString("MESSAGE_NAME"));
//                jsonObject.addProperty("MAIL_STATUS",resultSet.getString("MAIL_STATUS"));
//                jsonObject.addProperty("ITEM_KEY",resultSet.getString("ITEM_KEY"));
//                jsonArray.add(jsonObject);
//            }
//
//            jsonResult.add("NOTIFICATIONS",jsonArray);
//            result = jsonResult.toString();
//
//            connection.close();
//            preparedStatement.close();
//            resultSet.close();
//            System.out.println(jsonArray.size());
//            System.out.println("getNotification result data1: \n"+result);
//        }
//
//        catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Get Notification Error",e);
//        }
//        finally {
//            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
//        }
//        return result;
//    }

    // To get Penalty Details Added on : 10-01-2019 By: Ajay Singh
    public String getPenaltyDetails(String itemKey){
        String result="";
        int penaltyHeaderId=0;
        String data=itemKey.split("_")[0].toString();
        System.out.println(data);
        penaltyHeaderId=Integer.parseInt(data);
        System.out.println(penaltyHeaderId);
        JsonObject jsonResult = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        String statement = "SELECT WO_NUM \"Work_Order\",\n" +
                "       DESCRIPTION \"Description\",\n" +
                "       DEPT_NAME \"Maint_Type\",\n" +
                "       DELAY_DAYS \"Delay_Days\",\n" +
                "       TO_CHAR (PENALTY_AMT, 'fm999,999,999,999,999.00') \"Penalty_Amt\",\n" +
                "       OLD_GRACE_PERIOD \"Old_Grace_Period\",\n" +
                "       NEW_GRACE_PERIOD \"New_Grace_Period\",\n" +
                "       (SELECT DECODE (WDJ.PRIORITY, '10', 100, 50) * NEW_GRACE_PERIOD\n" +
                "          FROM WIP_DISCRETE_JOBS WDJ\n" +
                "         WHERE WIP_ENTITY_ID = V.WIP_ENTITY_ID)\n" +
                "          \"Exception_Penalty_Amt\",\n" +
                "       REASON \"Reason\"\n" +
                "  FROM XX_PENALTY_LINES_V V\n" +
                "WHERE PENALTY_HEADER_ID =:penaltyHeaderId";


        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            namedParameterStatement = new NamedParameterStatement(connection,statement);
            //preparedStatement = connection.prepareStatement(statement);
            namedParameterStatement.setFetchSize(100);
            namedParameterStatement.setInt("penaltyHeaderId", penaltyHeaderId);

            //System.out.println(statement);
            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(100);
            ResultSetMetaData rsmd = resultSet.getMetaData();
            int numberOfColumns = rsmd.getColumnCount();
            while (resultSet.next()){
                JsonObject jsonData = new JsonObject();
                for (int i=1; i<=numberOfColumns; i++ ) {
                    jsonData.addProperty(rsmd.getColumnName(i), resultSet.getString(rsmd.getColumnName(i)));
                }
                jsonArray.add(jsonData);
            }

            jsonResult.add("PENALTY_DETAILS",jsonArray);
            result=jsonResult.toString();
            connection.close();
            //preparedStatement.close();
            resultSet.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Get Notification getPenaltyDetails Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getNotificationDetail(int notificationId, String item_type) {

        String result = null;
        boolean isFYI = isFYI(notificationId);
        String statement = "  SELECT NtfEO.FROM_USER,\n" +
                "         NtfEO.TO_USER,\n" +
                "         to_char( NVL (NtfEO.SENT_DATE, NtfEO.BEGIN_DATE), 'dd-MON-yyyy hh:mmAM') AS SENT,\n" +
                "         NtfEO.NOTIFICATION_ID,\n" +
                "         iav.item_type,\n" +
                "         iav.item_key,\n" +
                "         MAX (CASE WHEN iav.name = 'TRANSFER_NUMBER' THEN iav.text_value END)\n" +
                "            AS WO_WR_NUMBER,\n" +
                "         MAX (CASE WHEN iav.name = 'ASSET_NUMBER' THEN iav.text_value END)\n" +
                "            AS ASSET_NUM,\n" +
                "         MAX (CASE WHEN iav.name = 'ASSET_DESCRIPTION' THEN iav.text_value END)\n" +
                "            AS ASSET_DESC,\n" +
                "         MAX (\n" +
                "            CASE WHEN iav.name = 'PROBLEM_DESCRIPTION' THEN iav.text_value END)\n" +
                "            AS WO_WR_DESCRIPTION,\n" +
                "         MAX (CASE WHEN iav.name = 'XXX_DEP_CODE' THEN iav.text_value END)\n" +
                "            AS DEPARTMENT_CODE,\n" +
                "         MAX (CASE WHEN iav.name = 'XXX_DEPT_DESC' THEN iav.text_value END)\n" +
                "            AS DEPARTMENT_DESC,\n" +
                "         MAX (CASE WHEN iav.name = 'WO_START_DATE' THEN iav.text_value END)\n" +
                "            AS WO_START_DATE,\n" +
                "         MAX (CASE WHEN iav.name = 'WO_END_DATE' THEN iav.text_value END)\n" +
                "            AS WO_END_DATE,\n" +
                "         MAX (CASE WHEN iav.name = 'REASON_OF_REJECT' THEN iav.text_value END)\n" +
                "            AS REASON_OF_REJECT,\n" +
                "         MAX (CASE WHEN iav.name = 'REASON_OF_REJECT2' THEN iav.text_value END)\n" +
                "            AS REASON_OF_REJECT2,\n" +
                "         MAX (\n" +
                "            CASE WHEN iav.name = 'XXX_REJECTED_REASON' THEN iav.text_value END)\n" +
                "            AS XXX_REJECTED_REASON,\n" +
                "         MAX (CASE WHEN iav.name = 'APPROVAL_PHASE' THEN iav.text_value END)\n" +
                "            AS APPROVAL_PHASE,\n" +
                "         MAX (CASE WHEN iav.name = 'ASSET_NUMBER' THEN (SELECT last_vld_organization_id FROM csi_item_instances WHERE instance_number = iav.text_value) WHEN iav.name = 'XXX_ORGANIZATION_ID' then to_number(iav.text_value) END)\n" +
                "         AS ORGANIZATION_ID, \n" +
                "         (SELECT ACTIVITY_DISPLAY\n" +
                "            FROM (SELECT wi.item_key,\n" +
                "                         wi.item_type,\n" +
                "                         wias.notification_id,\n" +
                "                         wa.name AS ACTIVITY_DISPLAY\n" +
                "                    FROM       applsys.WF_ITEM_ACTIVITY_STATUSES wias\n" +
                "                            LEFT JOIN\n" +
                "                               applsys.wf_notifications wn\n" +
                "                            ON wias.notification_id = wn.notification_id\n" +
                "                         LEFT JOIN\n" +
                "                            apps.wf_lookups wnl\n" +
                "                         ON     wnl.lookup_code = wn.STATUS\n" +
                "                            AND wnl.lookup_type = 'WF_NOTIFICATION_STATUS',\n" +
                "                         apps.wf_lookups wl,\n" +
                "                         applsys.wf_items wi,\n" +
                "                         apps.wf_activities_vl wa,\n" +
                "                         applsys.wf_process_activities wpa,\n" +
                "                         apps.wf_activities_vl wa2\n" +
                "                   WHERE     wl.lookup_code = wias.ACTIVITY_STATUS\n" +
                "                         AND wl.lookup_type = 'WFENG_STATUS'\n" +
                "                         AND wias.ITEM_TYPE = wi.item_type\n" +
                "                         AND wias.ITEM_KEY = wi.item_key\n" +
                "                         AND wias.PROCESS_ACTIVITY = wpa.instance_id\n" +
                "                         AND wpa.activity_name = wa.name\n" +
                "                         AND wpa.activity_item_type = wa.item_type\n" +
                "                         AND wi.begin_date BETWEEN wa.begin_date\n" +
                "                                               AND NVL (wa.end_date,\n" +
                "                                                        wi.begin_date)\n" +
                "                         AND wpa.process_name = wa2.name\n" +
                "                         AND wpa.process_item_type = wa2.item_type\n" +
                "                         AND wpa.process_version = wa2.version\n" +
                "                  UNION ALL\n" +
                "                  SELECT wi.item_key,\n" +
                "                         wi.item_type,\n" +
                "                         wiash.notification_id,\n" +
                "                         wa.display_name AS ACTIVITY_DISPLAY\n" +
                "                    FROM       applsys.WF_ITEM_ACTIVITY_STATUSES_H wiash\n" +
                "                            LEFT JOIN\n" +
                "                               applsys.wf_notifications wn\n" +
                "                            ON wiash.notification_id = wn.notification_id\n" +
                "                         LEFT JOIN\n" +
                "                            apps.wf_lookups wnl\n" +
                "                         ON     wnl.lookup_code = wn.STATUS\n" +
                "                            AND wnl.lookup_type = 'WF_NOTIFICATION_STATUS',\n" +
                "                         apps.wf_lookups wl,\n" +
                "                         applsys.wf_items wi,\n" +
                "                         apps.wf_activities_vl wa,\n" +
                "                         applsys.wf_process_activities wpa,\n" +
                "                         apps.wf_activities_vl wa2\n" +
                "                   WHERE     wl.lookup_code = wiash.ACTIVITY_STATUS\n" +
                "                         AND wl.lookup_type = 'WFENG_STATUS'\n" +
                "                         AND wiash.ITEM_TYPE = wi.item_type\n" +
                "                         AND wiash.ITEM_KEY = wi.item_key\n" +
                "                         AND wiash.PROCESS_ACTIVITY = wpa.instance_id\n" +
                "                         AND wpa.activity_name = wa.name\n" +
                "                         AND wpa.activity_item_type = wa.item_type\n" +
                "                         AND wi.begin_date BETWEEN wa.begin_date\n" +
                "                                               AND NVL (wa.end_date,\n" +
                "                                                        wi.begin_date)\n" +
                "                         AND wpa.process_name = wa2.name\n" +
                "                         AND wpa.process_item_type = wa2.item_type\n" +
                "                         AND wpa.process_version = wa2.version \n" +
                "                                                              )\n" +
                "           WHERE     ITEM_TYPE = iav.item_type\n" +
                "                 AND notification_id = NtfEO.NOTIFICATION_ID) ACTIVITY\n" +
                "    FROM wf_item_attributes_vl ia,\n" +
                "         wf_item_attribute_values iav,\n" +
                "         WF_NOTIFICATIONS NtfEO,\n" +
                "         WF_MESSAGES_TL MsgEO,\n" +
                "         applsys.WF_ITEM_ACTIVITY_STATUSES st\n" +
                "   WHERE     ia.item_type = iav.item_type\n" +
                "         AND ia.name = iav.name\n" +
                "         AND SUBSTR (ia.name, 1, 1) <> '.'\n" +
                "         AND NtfEO.MESSAGE_TYPE = MsgEO.TYPE\n" +
                "         AND NtfEO.MESSAGE_NAME = MsgEO.NAME\n" +
                "         AND NVL (NtfEO.LANGUAGE, USERENV ('LANG')) = MsgEO.LANGUAGE\n" +
                "         AND NtfEO.item_key = iav.item_key\n" +
                "         AND NtfEO.MESSAGE_TYPE = iav.item_type\n" +
                "         AND st.notification_id = NtfEO.NOTIFICATION_ID\n" +
                "         AND iav.item_type = ?\n" +
                "         AND NtfEO.NOTIFICATION_ID = ?\n" +
                "GROUP BY iav.item_type,\n" +
                "         iav.item_key,\n" +
                "         NtfEO.FROM_USER,\n" +
                "         NtfEO.TO_USER,\n" +
                "         NVL (NtfEO.SENT_DATE, NtfEO.BEGIN_DATE),\n" +
                "         NtfEO.NOTIFICATION_ID\n";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setString(1, item_type);
            preparedStatement.setInt(2, notificationId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            if (isFYI) {
                jsonResult.addProperty("RESPONSE_REQUIRED", "FALSE");
            }
            else {
                jsonResult.addProperty("RESPONSE_REQUIRED", "TRUE");
            }

            String item_key="";
            if(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("FROM_USER",resultSet.getString(1));
                jsonObject.addProperty("TO_USER",resultSet.getString(2));
                jsonObject.addProperty("SENT",resultSet.getString(3));
                jsonObject.addProperty("NOTIFICATION_ID",resultSet.getString(4));


                jsonObject.addProperty("item_type",resultSet.getString(5));
                if(resultSet.getString(5).equalsIgnoreCase("XXXSTCPT")){
                  jsonObject.addProperty("WO_WR_NUMBER",resultSet.getString(4));
                  jsonObject.addProperty("WO_WR_DESCRIPTION",resultSet.getString(7));
                }else{
                  jsonObject.addProperty("WO_WR_NUMBER",resultSet.getString(7));
                  jsonObject.addProperty("WO_WR_DESCRIPTION",resultSet.getString(10));
                }
                jsonObject.addProperty("item_key",resultSet.getString(6));
                item_key= resultSet.getString(6);
                
                jsonObject.addProperty("ASSET_NUM",resultSet.getString(8));
                jsonObject.addProperty("ASSET_DESC",resultSet.getString(9));
                
                jsonObject.addProperty("DEPARTMENT_CODE",resultSet.getString(11));

                jsonObject.addProperty("DEPARTMENT_DESC",resultSet.getString(12));
                jsonObject.addProperty("WO_START_DATE",resultSet.getString(13));
                jsonObject.addProperty("WO_END_DATE",resultSet.getString(14));
                jsonObject.addProperty("REASON_OF_REJECT",resultSet.getString(15));
                jsonObject.addProperty("REASON_OF_REJECT2",resultSet.getString(16));
                jsonObject.addProperty("XXX_REJECTED_REASON",resultSet.getString(17));
                jsonObject.addProperty("APPROVAL_PHASE",resultSet.getString(18));
                jsonObject.addProperty("ORG_ID",resultSet.getString(19));
                jsonObject.addProperty("ACTIVITY",resultSet.getString(20));

                String  workType = resultSet.getString(6);
                //System.out.println("workType is: " + workType);

                if(workType.contains("WO")){
                    jsonResult.addProperty("WORK_TYPE_DETAILS","WorkOrder");
                } else if(workType.contains("WR")){
                    jsonResult.addProperty("WORK_TYPE_DETAILS","WorkRequest");
                }else if(resultSet.getString(5).equalsIgnoreCase("XXXSTCPT")){
                   jsonResult.addProperty("WORK_TYPE_DETAILS","Penalty"); 
                } else if (resultSet.getString(5).equalsIgnoreCase("XXXSTCCL")){
                    jsonResult.addProperty("WORK_TYPE_DETAILS","Claim");
                }
                jsonArray.add(jsonObject);
            }

            jsonResult.add("NOTIFICATION_DETAIL",jsonArray);
//            jsonResult.add("ATTACHEMENT", getNotificationAttachement(item_type, item_key));
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Notification Details Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getNotificationAttachementForWorkOrder(String P_ITEMTYPE,String P_ITEMKEY){
        String result="";
        JsonObject jsonResult = new JsonObject();
        JsonObject jsonData = new JsonObject();
        JsonArray jsonArray = new JsonArray();

            String statement = "SELECT *\n" +
                        "\n" +
                        "    FROM (SELECT ad.seq_num,\n" +
                        "\n" +
                        "                 d.TITLE,\n" +
                        "\n" +
                        "                 d.DESCRIPTION,\n" +
                        "\n" +
                        "                 d.DATATYPE_NAME,\n" +
                        "\n" +
                        "                 l.file_name AS l_file_name,\n" +
                        "\n" +
                        "                 l.file_content_type AS l_file_type,\n" +
                        "\n" +
                        "                 l.file_data AS l_attachment,\n" +
                        "\n" +
                        "                 d.URL,\n" +
                        "\n" +
                        "                 NULL SHORT_TEXT,\n" +
                        "\n" +
                        "                 ad.pk2_value,\n" +
                        "\n" +
                        "                 d.media_id\n" +
                        "\n" +
                        "            FROM FND_DOCUMENTS_VL d,\n" +
                        "\n" +
                        "                 FND_ATTACHED_DOCUMENTS ad,\n" +
                        "\n" +
                        "                 FND_DOCUMENT_ENTITIES e,\n" +
                        "\n" +
                        "                 FND_USER u,\n" +
                        "\n" +
                        "                 FND_DOCUMENT_CATEGORIES_TL cl,\n" +
                        "\n" +
                        "                 FND_DM_NODES node,\n" +
                        "\n" +
                        "                 fnd_lobs l\n" +
                        "\n" +
                        "           WHERE     ad.DOCUMENT_ID = d.DOCUMENT_ID\n" +
                        "\n" +
                        "                 AND ad.ENTITY_NAME = e.DATA_OBJECT_CODE(+)\n" +
                        "\n" +
                        "                 AND ad.LAST_UPDATED_BY = u.USER_ID(+)\n" +
                        "\n" +
                        "                 AND cl.language = USERENV ('LANG')\n" +
                        "\n" +
                        "                 AND cl.category_id = NVL (ad.category_id, d.category_id)\n" +
                        "\n" +
                        "                 AND d.dm_node = node.node_id(+)\n" +
                        "\n" +
                        "                 AND ad.entity_name = 'EAM_WORK_ORDERS'\n" +
                        "\n" +
                        "                 AND l.file_id = d.media_id\n" +
                        "\n" +
                        "          UNION ALL\n" +
                        "\n" +
                        "          SELECT ad.seq_num,\n" +
                        "\n" +
                        "                 d.TITLE,\n" +
                        "\n" +
                        "                 d.DESCRIPTION,\n" +
                        "\n" +
                        "                 d.DATATYPE_NAME,\n" +
                        "\n" +
                        "                 d.TITLE,\n" +
                        "\n" +
                        "                 NULL,\n" +
                        "\n" +
                        "                 NULL,\n" +
                        "\n" +
                        "                 d.URL,\n" +
                        "\n" +
                        "                 L.SHORT_TEXT,\n" +
                        "\n" +
                        "                 ad.pk2_value,\n" +
                        "\n" +
                        "                 d.media_id\n" +
                        "\n" +
                        "            FROM FND_DOCUMENTS_VL d,\n" +
                        "\n" +
                        "                 FND_ATTACHED_DOCUMENTS ad,\n" +
                        "\n" +
                        "                 FND_DOCUMENT_ENTITIES e,\n" +
                        "\n" +
                        "                 FND_USER u,\n" +
                        "\n" +
                        "                 FND_DOCUMENT_CATEGORIES_TL cl,\n" +
                        "\n" +
                        "                 FND_DM_NODES node,\n" +
                        "\n" +
                        "                 APPLSYS.FND_DOCUMENTS_SHORT_TEXT l\n" +
                        "\n" +
                        "           WHERE     ad.DOCUMENT_ID = d.DOCUMENT_ID\n" +
                        "\n" +
                        "                 AND ad.ENTITY_NAME = e.DATA_OBJECT_CODE(+)\n" +
                        "\n" +
                        "                 AND ad.LAST_UPDATED_BY = u.USER_ID(+)\n" +
                        "\n" +
                        "                 AND cl.language = USERENV ('LANG')\n" +
                        "\n" +
                        "                 AND cl.category_id = NVL (ad.category_id, d.category_id)\n" +
                        "\n" +
                        "                 AND d.dm_node = node.node_id(+)\n" +
                        "\n" +
                        "                 AND ad.entity_name = 'EAM_WORK_ORDERS'\n" +
                        "\n" +
                        "                 AND DATATYPE_NAME = 'Short Text'\n" +
                        "\n" +
                        "                 AND l.media_id = d.media_id\n" +
                        "\n" +
                        "          UNION ALL\n" +
                        "\n" +
                        "          SELECT ad.seq_num,\n" +
                        "\n" +
                        "                 d.TITLE,\n" +
                        "\n" +
                        "                 d.DESCRIPTION,\n" +
                        "\n" +
                        "                 d.DATATYPE_NAME,\n" +
                        "\n" +
                        "                 d.TITLE,\n" +
                        "\n" +
                        "                 NULL,\n" +
                        "\n" +
                        "                 NULL,\n" +
                        "\n" +
                        "                 d.URL,\n" +
                        "\n" +
                        "                 NULL,\n" +
                        "\n" +
                        "                 ad.pk2_value,\n" +
                        "\n" +
                        "                 d.media_id\n" +
                        "\n" +
                        "            FROM FND_DOCUMENTS_VL d,\n" +
                        "\n" +
                        "                 FND_ATTACHED_DOCUMENTS ad,\n" +
                        "\n" +
                        "                 FND_DOCUMENT_ENTITIES e,\n" +
                        "\n" +
                        "                 FND_USER u,\n" +
                        "\n" +
                        "                 FND_DOCUMENT_CATEGORIES_TL cl,\n" +
                        "\n" +
                        "                 FND_DM_NODES node\n" +
                        "\n" +
                        "           WHERE     ad.DOCUMENT_ID = d.DOCUMENT_ID\n" +
                        "\n" +
                        "                 AND ad.ENTITY_NAME = e.DATA_OBJECT_CODE(+)\n" +
                        "\n" +
                        "                 AND ad.LAST_UPDATED_BY = u.USER_ID(+)\n" +
                        "\n" +
                        "                 AND cl.language = USERENV ('LANG')\n" +
                        "\n" +
                        "                 AND cl.category_id = NVL (ad.category_id, d.category_id)\n" +
                        "\n" +
                        "                 AND d.dm_node = node.node_id(+)\n" +
                        "\n" +
                        "                 AND ad.entity_name = 'EAM_WORK_ORDERS'\n" +
                        "\n" +
                        "                 AND DATATYPE_NAME = 'Web Page')\n" +
                        "\n" +
                        "   WHERE pk2_value in\n" +
                        "\n" +
                        "            (SELECT to_char(iav.number_value)\n" +
                        "\n" +
                        "               FROM wf_item_attributes_vl ia, wf_item_attribute_values iav\n" +
                        "\n" +
                        "              WHERE     ia.item_type = iav.item_type\n" +
                        "\n" +
                        "                    AND ia.name = iav.name\n" +
                        "\n" +
                        "                    AND SUBSTR (ia.name, 1, 1) <> '.'\n" +
                        "\n" +
                        "                    AND iav.item_type = :ITEMTYPE \n" +
                        "\n" +
                        "                    AND iav.item_key = :ITEMKEY \n" +
                        "\n" +
                        "                    AND ia.name = 'XXX_TRANSACTION_ID')\n" +
                        "\n" +
                        "ORDER BY seq_num";
        
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            namedParameterStatement = new NamedParameterStatement(connection,statement);
            //preparedStatement = connection.prepareStatement(statement);
            namedParameterStatement.setFetchSize(100);
            namedParameterStatement.setString("ITEMTYPE", P_ITEMTYPE);
            namedParameterStatement.setString("ITEMKEY", P_ITEMKEY);
            
            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(100);
            
            
            
            while(resultSet.next()){
                JsonObject jsonResult1 = new JsonObject();
//                InputStream a = new InputStream();
               
                jsonResult1.addProperty("FileName",resultSet.getString(5));
                jsonResult1.addProperty("FileType",resultSet.getString(6));
                jsonResult1.addProperty("MediaId",resultSet.getString(11));
//                
                if(resultSet.getBlob("L_ATTACHMENT") == null && resultSet.getString("URL") == null ){
                    jsonResult1.addProperty("FileData",resultSet.getString(9));
                    jsonResult1.addProperty("AttachmentType", "Text");
                } else if (resultSet.getString("URL") == null && resultSet.getString("SHORT_TEXT") == null) {
//                    String convertedBlob = "";
//                    InputStream fileData = resultSet.getBinaryStream(7);
//                    convertedBlob = attachmentController.blobTobase64(fileData);


                    jsonResult1.addProperty("FileData","");
                    jsonResult1.addProperty("AttachmentType", "File");
                } else if(resultSet.getString("SHORT_TEXT") == null && resultSet.getBlob("L_ATTACHMENT") == null ){
                    jsonResult1.addProperty("FileData",resultSet.getString(8));
                    jsonResult1.addProperty("AttachmentType", "URL");
                } 
                jsonArray.add(jsonResult1);
            }
            
            jsonData.add("ATTACHMENTS", jsonArray);
            connection.close();
            //preparedStatement.close();
            namedParameterStatement.close();
            resultSet.close();
            result = jsonData.toString();
            
            System.out.println("Get Notification ATTACHMENT result: \n"+result);
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Get Notification ATTACHMENT Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
    
    
    public String getNotificationAttachementForClaim(String P_ITEMTYPE,String P_ITEMKEY){
        String result="";
        JsonObject jsonResult = new JsonObject();
        JsonObject jsonData = new JsonObject();
        JsonArray jsonArray = new JsonArray();

            String statement = "SELECT *\n" +
"\n" +
"    FROM (SELECT ad.seq_num,\n" +
"\n" +
"                 d.TITLE,\n" +
"\n" +
"                 d.DESCRIPTION,\n" +
"\n" +
"                 d.DATATYPE_NAME,\n" +
"\n" +
"                 l.file_name AS l_file_name,\n" +
"\n" +
"                 l.file_content_type AS l_file_type,\n" +
"\n" +
"                 l.file_data AS l_attachment,\n" +
"\n" +
"                 d.URL,\n" +
"\n" +
"                 NULL SHORT_TEXT,\n" +
"\n" +
"                 ad.pk1_value,\n" +
"\n" +
"                 d.media_id\n" +
"\n" +
"            FROM FND_DOCUMENTS_VL d,\n" +
"\n" +
"                 FND_ATTACHED_DOCUMENTS ad,\n" +
"\n" +
"                 FND_DOCUMENT_ENTITIES e,\n" +
"\n" +
"                 FND_USER u,\n" +
"\n" +
"                 FND_DOCUMENT_CATEGORIES_TL cl,\n" +
"\n" +
"                 FND_DM_NODES node,\n" +
"\n" +
"                 fnd_lobs l\n" +
"\n" +
"           WHERE     ad.DOCUMENT_ID = d.DOCUMENT_ID\n" +
"\n" +
"                 AND ad.ENTITY_NAME = e.DATA_OBJECT_CODE(+)\n" +
"\n" +
"                 AND ad.LAST_UPDATED_BY = u.USER_ID(+)\n" +
"\n" +
"                 AND cl.language = USERENV ('LANG')\n" +
"\n" +
"                 AND cl.category_id = NVL (ad.category_id, d.category_id)\n" +
"\n" +
"                 AND d.dm_node = node.node_id(+)\n" +
"\n" +
"                 AND ad.entity_name = 'XXX_STC_CLAIMS_APPROVAL_HDR'\n" +
"\n" +
"                 AND l.file_id = d.media_id\n" +
"\n" +
"          UNION ALL\n" +
"\n" +
"          SELECT ad.seq_num,\n" +
"\n" +
"                 d.TITLE,\n" +
"\n" +
"                 d.DESCRIPTION,\n" +
"\n" +
"                 d.DATATYPE_NAME,\n" +
"\n" +
"                 d.TITLE,\n" +
"\n" +
"                 NULL,\n" +
"\n" +
"                 NULL,\n" +
"\n" +
"                 d.URL,\n" +
"\n" +
"                 L.SHORT_TEXT,\n" +
"\n" +
"                 ad.pk1_value,\n" +
"\n" +
"                 d.media_id\n" +
"\n" +
"            FROM FND_DOCUMENTS_VL d,\n" +
"\n" +
"                 FND_ATTACHED_DOCUMENTS ad,\n" +
"\n" +
"                 FND_DOCUMENT_ENTITIES e,\n" +
"\n" +
"                 FND_USER u,\n" +
"\n" +
"                 FND_DOCUMENT_CATEGORIES_TL cl,\n" +
"\n" +
"                 FND_DM_NODES node,\n" +
"\n" +
"                 APPLSYS.FND_DOCUMENTS_SHORT_TEXT l\n" +
"\n" +
"           WHERE     ad.DOCUMENT_ID = d.DOCUMENT_ID\n" +
"\n" +
"                 AND ad.ENTITY_NAME = e.DATA_OBJECT_CODE(+)\n" +
"\n" +
"                 AND ad.LAST_UPDATED_BY = u.USER_ID(+)\n" +
"\n" +
"                 AND cl.language = USERENV ('LANG')\n" +
"\n" +
"                 AND cl.category_id = NVL (ad.category_id, d.category_id)\n" +
"\n" +
"                 AND d.dm_node = node.node_id(+)\n" +
"\n" +
"                 AND ad.entity_name = 'XXX_STC_CLAIMS_APPROVAL_HDR'\n" +
"\n" +
"                 AND DATATYPE_NAME = 'Short Text'\n" +
"\n" +
"                 AND l.media_id = d.media_id\n" +
"\n" +
"          UNION ALL\n" +
"\n" +
"          SELECT ad.seq_num,\n" +
"\n" +
"                 d.TITLE,\n" +
"\n" +
"                 d.DESCRIPTION,\n" +
"\n" +
"                 d.DATATYPE_NAME,\n" +
"\n" +
"                 d.TITLE,\n" +
"\n" +
"                 NULL,\n" +
"\n" +
"                 NULL,\n" +
"\n" +
"                 d.URL,\n" +
"\n" +
"                 NULL,\n" +
"\n" +
"                 ad.pk1_value,\n" +
"\n" +
"                 d.media_id\n" +
"\n" +
"            FROM FND_DOCUMENTS_VL d,\n" +
"\n" +
"                 FND_ATTACHED_DOCUMENTS ad,\n" +
"\n" +
"                 FND_DOCUMENT_ENTITIES e,\n" +
"\n" +
"                 FND_USER u,\n" +
"\n" +
"                 FND_DOCUMENT_CATEGORIES_TL cl,\n" +
"\n" +
"                 FND_DM_NODES node\n" +
"\n" +
"           WHERE     ad.DOCUMENT_ID = d.DOCUMENT_ID\n" +
"\n" +
"                 AND ad.ENTITY_NAME = e.DATA_OBJECT_CODE(+)\n" +
"\n" +
"                 AND ad.LAST_UPDATED_BY = u.USER_ID(+)\n" +
"\n" +
"                 AND cl.language = USERENV ('LANG')\n" +
"\n" +
"                 AND cl.category_id = NVL (ad.category_id, d.category_id)\n" +
"\n" +
"                 AND d.dm_node = node.node_id(+)\n" +
"\n" +
"                 AND ad.entity_name = 'XXX_STC_CLAIMS_APPROVAL_HDR'\n" +
"\n" +
"                 AND DATATYPE_NAME = 'Web Page')\n" +
"\n" +
"   WHERE pk1_value IN\n" +
"\n" +
"            (SELECT TO_CHAR (iav.number_value)\n" +
"\n" +
"               FROM wf_item_attributes_vl ia, wf_item_attribute_values iav\n" +
"\n" +
"              WHERE     ia.item_type = iav.item_type\n" +
"\n" +
"                    AND ia.name = iav.name\n" +
"\n" +
"                    AND SUBSTR (ia.name, 1, 1) <> '.'\n" +
"\n" +
"                    AND iav.item_type = :ITEMTYPE\n" +
"\n" +
"                    AND iav.item_key = :ITEMKEY\n" +
"\n" +
"                    AND ia.name = 'XXX_TRANSACTION_ID')\n" +
"\n" +
"ORDER BY seq_num";
        
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            namedParameterStatement = new NamedParameterStatement(connection,statement);
            //preparedStatement = connection.prepareStatement(statement);
            namedParameterStatement.setFetchSize(100);
            namedParameterStatement.setString("ITEMTYPE", P_ITEMTYPE);
            namedParameterStatement.setString("ITEMKEY", P_ITEMKEY);
            //System.out.println(statement);
            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(100);

            while(resultSet.next()){
                JsonObject jsonResult1 = new JsonObject();
                jsonResult1.addProperty("FileName",resultSet.getString(5));
                jsonResult1.addProperty("FileType",resultSet.getString(6));
                jsonResult1.addProperty("MediaId",resultSet.getString(11));
                
                if(resultSet.getBlob("L_ATTACHMENT") == null && resultSet.getString("URL") == null ){
                    jsonResult1.addProperty("FileData",resultSet.getString(9));
                    jsonResult1.addProperty("AttachmentType", "Text");
                } else if(resultSet.getString("SHORT_TEXT") == null && resultSet.getString("URL") == null ){
                    jsonResult1.addProperty("FileData","");
                    jsonResult1.addProperty("AttachmentType", "File");
                } else if(resultSet.getString("SHORT_TEXT") == null && resultSet.getBlob("L_ATTACHMENT") == null ){
                    jsonResult1.addProperty("FileData",resultSet.getString(8));
                    jsonResult1.addProperty("AttachmentType", "URL");
                }
                
                jsonArray.add(jsonResult1);
            }
            
            jsonData.add("attachments", jsonArray);
            connection.close();
            //preparedStatement.close();
            namedParameterStatement.close();
            resultSet.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Get Notification ATTACHMENT Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return jsonData.toString();
    }
    
    public String getSPRLinesForWorkOrder(String workOrder){
        String result="";
        String statement = "SELECT\n" +
                    "    description           \"Item\",\n" +
                    "    description    \"Description\",\n" +
                    "    attribute7          \"Agent Warranty\",\n" +
                    "    required_quantity   \"Qty\",\n" +
                    "    uom            \"UOM\",\n" +
                    "    attribute1          \"Cost\",\n" +
                    "    TO_CHAR(required_quantity * attribute1, 'fm999,999,999,999,999.00') \"Total\"\n" +
                    "FROM\n" +
                    "    wip.WIP_EAM_DIRECT_ITEMS \n" +
                    "WHERE \n" +
                    "wip_entity_id=(SELECT WIP_ENTITY_ID FROM wip_entities WHERE WIP_ENTITY_NAME=?)";
        try {

            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setString(1, workOrder);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            float totalCount = 0;
            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ITEM",resultSet.getString("Item"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("Description"));                
                jsonObject.addProperty("AGENT",resultSet.getString("Agent Warranty"));
                jsonObject.addProperty("QTY",resultSet.getString("Qty"));
                jsonObject.addProperty("UOM",resultSet.getString("UOM"));
                jsonObject.addProperty("COST",resultSet.getString("Cost"));
                jsonObject.addProperty("TOTAL",resultSet.getString("Total"));
                
                totalCount = totalCount + (Float.parseFloat(resultSet.getString("Cost"))*resultSet.getInt("Qty"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("SPR_LINES",jsonArray);
            jsonResult.addProperty("TOTAL_COUNT",totalCount);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get SPR Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
    
    public String getEngineerName(int orgId){
        String result="";
        String statement = "SELECT\n" +
                "     tag user_name,\n" +
                "     ( per.employee_number\n" +
                "       || '-'\n" +
                "       || per.full_name ) AS fullname\n" +
                " FROM\n" +
                "     fnd_lookup_values_vl v,\n" +
                "     per_all_people_f per,\n" +
                "     fnd_user u\n" +
                " WHERE\n" +
                "     v.lookup_type = 'XX_TECHNICAL_ENGINEER_DISTRICT'\n" +
                "     AND SYSDATE BETWEEN effective_start_date AND effective_end_date\n" +
                "     AND per.person_id = u.employee_id\n" +
                "     AND u.user_name = v.tag AND v.ENABLED_FLAG = 'Y' \n" +
                "     AND v.attribute1 =?";
        try {

            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, orgId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("user_name",resultSet.getString("user_name"));
                jsonObject.addProperty("fullname",resultSet.getString("fullname"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("TECHNICAL_ENGINEER",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get TECHNICAL_ENGINEER Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

     public String addCommentOrEngg(String P_ITEMTYPE,
                                    String P_ITEMKEY,
                                    String P_ATTRIBUTE_NAME,
                                    String P_ATTRIBUTE_VALUE) {
         // P_ATTRIBUTE_NAME == REASON_OF_REJECT (For Reason)
         // P_ATTRIBUTE_NAME == XX_ENG (For Engg Name)
         String result="";
         try{

             String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxxeam_wf_notifications_pkg_ws/?wsdl";
             String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxeam_wf_notifications_pkg_ws/set_attr_value/";

             SoapEnvelopeObject seo = new SoapEnvelopeObject();
             seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxeam_wf_notifications_pkg_ws/");
             seo.setUsername(pool.getProperties().getProperty("grantUserId"));
             seo.setPassword(pool.getProperties().getProperty("grantPassword"));
             seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxeam_wf_notifications_pkg_ws/set_attr_value/");

             Map<String,Object> param = new HashMap<String,Object>();
             param.put("P_ITEMTYPE", P_ITEMTYPE);
             param.put("P_ITEMKEY", P_ITEMKEY);
             param.put("P_ATTRIBUTE_NAME", P_ATTRIBUTE_NAME);
             param.put("P_ATTRIBUTE_VALUE", P_ATTRIBUTE_VALUE);
             seo.setParameter(param);

             SoapHandler handler = new SoapHandler();
             result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);

         } catch(Exception e){
             e.printStackTrace();
             logger.error("Set Attribute Value Error",e);
         }
         return result;
     }

    public String processNotification(String P_ITEMTYPE,String P_ITEMKEY,String P_ACTIVITY,String P_RESULT)
    {
        String result="";
         try{

             String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxxeam_wf_notifications_pkg_ws/?wsdl";
             String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxeam_wf_notifications_pkg_ws/complete_activity/";

             SoapEnvelopeObject seo = new SoapEnvelopeObject();
             seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxeam_wf_notifications_pkg_ws/");
             seo.setUsername(pool.getProperties().getProperty("grantUserId"));
             seo.setPassword(pool.getProperties().getProperty("grantPassword"));
             seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxeam_wf_notifications_pkg_ws/complete_activity/");

             Map<String,Object> param = new HashMap<String,Object>();
             param.put("P_ITEMTYPE", P_ITEMTYPE);
             param.put("P_ITEMKEY", P_ITEMKEY);
             param.put("P_ACTIVITY", P_ACTIVITY);
             param.put("P_RESULT", P_RESULT);
             seo.setParameter(param);

             SoapHandler handler = new SoapHandler();
             String jdata=handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
             JsonObject jsonObject = new JsonParser().parse(jdata).getAsJsonObject();
             JsonObject pageName = jsonObject.getAsJsonObject("env:Envelope").get("env:Body").getAsJsonObject();
             String data=pageName.getAsJsonObject("OutputParameters").get("P_OUT").getAsString();
             System.out.println(data);
             result = jdata;

         } catch(Exception e){
             e.printStackTrace();
             logger.error("Work Request Approve Error",e);
         }
        return  result;
    }
    public String workRequestApprove(int workRequestId) {

        String result = "";

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/wf_set_action_approved/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/wf_set_action_approved/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("ITEMTYPE", "XXXSTCWR");
            param.put("ITEMKEY", getWrItemKey(workRequestId));
            param.put("FUNCMODE", String.valueOf("RUN"));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Work Request Approve Error",e);
        }
        return result;
    }

    public String workRequestReject(int workRequestId) {

        String result = "";

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/wf_set_action_rejected/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/wf_set_action_rejected/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("ITEMTYPE", "XXXSTCWR");
            param.put("ITEMKEY", getWrItemKey(workRequestId));
            param.put("FUNCMODE", String.valueOf("RUN"));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Work Request Reject Error",e);
        }
        return result;
    }

    public String workRequestCancel(int workRequestId) {
        String result = "";
        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/wf_set_action_canceled/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_pkg_soa/wf_set_action_canceled/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("ITEMTYPE", "XXXSTCWR");
            param.put("ITEMKEY", getWrItemKey(workRequestId));
            param.put("FUNCMODE", String.valueOf("RUN"));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Work Request Cancel Error",e);
        }
        return result;
    }

    public String workRequestAddInfo(int notificationId, String responder) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/wf_notification/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/setattrtext/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/setattrtext/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("NID", String.valueOf(notificationId));
            param.put("ANAME", "RESULT");
            param.put("AVALUE", "ADD_INFORMATION");

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);

            String soapEndpointUrl2 = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/wf_notification/?wsdl";
            String soapAction2 = "http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/respond/";

            SoapEnvelopeObject seo2 = new SoapEnvelopeObject();
            seo2.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/");
            seo2.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo2.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo2.setSoapurl("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/respond/");

            Map<String,Object> param2 = new HashMap<String,Object>();
            param2.put("NID", String.valueOf(notificationId));
            param2.put("RESPONDER", responder.toUpperCase());

            seo2.setParameter(param2);

            result = handler.callSoapWebService(soapEndpointUrl2,soapAction2,seo2);

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Work Request Additional Information Error");
        }
        return result;
    }

    public String workOrderApprove(boolean isHqc, int entityId) {

        if (isHqc) {
            String result = "";

            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_approved/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_approved/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("ITEMTYPE", "XXXSTCHQ");
                param.put("ITEMKEY", getWoItemKey(entityId));
                param.put("FUNCMODE", String.valueOf("RUN"));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Work Order approve for HQC Error",e);
            }

            return result;
        }
        else {
            String result = "";

            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/wf_set_action_approved/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/wf_set_action_approved/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("ITEMTYPE", "XXXSTCWO");
                param.put("ITEMKEY", getWoItemKey(entityId));
                param.put("FUNCMODE", String.valueOf("RUN"));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Work Order approve for District Error",e);
            }

            return result;
        }
    }

    public String workOrderReject(boolean isHqc, int entityId) {
        if (isHqc) {
            String result = "";
            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_rejected/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_rejected/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("ITEMTYPE", "XXXSTCHQ");
                param.put("ITEMKEY", getWoItemKey(entityId));
                param.put("FUNCMODE", String.valueOf("RUN"));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Work Order Reject HQC Error",e);
            }

            return result;
        }
        else {
            String result = "";
            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/wf_set_action_rejected/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/wf_set_action_rejected/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("ITEMTYPE", "XXXSTCWO");
                param.put("ITEMKEY", getWoItemKey(entityId));
                param.put("FUNCMODE", String.valueOf("RUN"));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Work Order Reject District Error",e);
            }

            return result;
        }
    }

    public String workOrderRejectSparePart(boolean isHqc, int entityId) {

        if (isHqc) {
            String result = "";

            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_rejected_sp/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_rejected_sp/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("ITEMTYPE", "XXXSTCHQ");
                param.put("ITEMKEY", getWoItemKey(entityId));
                param.put("FUNCMODE", String.valueOf("RUN"));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Work Order Reject Spare Part HQC Error",e);
            }

            return result;
        }
        else {
            String result = "";

            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/wf_set_action_rejected_sp/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/wf_set_action_rejected_sp/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("ITEMTYPE", "XXXSTCWO");
                param.put("ITEMKEY", getWoItemKey(entityId));
                param.put("FUNCMODE", String.valueOf("RUN"));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("Work Order Reject Spare Part District Error",e);
            }

            return result;
        }
    }

    public String workOrderCancel(boolean isHqc, int entityId) {

        if (isHqc) {
            String result = "";

            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_canceled/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_canceled/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("ITEMTYPE", "XXXSTCHQ");
                param.put("ITEMKEY", getWoItemKey(entityId));
                param.put("FUNCMODE", String.valueOf("RUN"));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error(" Work order  cancel for hqc Error",e);
            }

            return result;
        }
        else {
            String result = "";

            try{

                String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_wo_wf_pkg_soa/?wsdl";
                String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/wf_set_action_canceled/";

                SoapEnvelopeObject seo = new SoapEnvelopeObject();
                seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/");
                seo.setUsername(pool.getProperties().getProperty("grantUserId"));
                seo.setPassword(pool.getProperties().getProperty("grantPassword"));
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_wo_wf_pkg_soa/wf_set_action_canceled/");

                Map<String,Object> param = new HashMap<String,Object>();
                param.put("ITEMTYPE", "XXXSTCWO");
                param.put("ITEMKEY", getWoItemKey(entityId));
                param.put("FUNCMODE", String.valueOf("RUN"));
                seo.setParameter(param);

                SoapHandler handler = new SoapHandler();
                result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error(" Work order  cancel for district Error",e);
            }

            return result;
        }
    }

    public String transferNotification(int notificationId, String newRole, String userComment) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/wf_notification/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/transfer/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/transfer/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("NID", String.valueOf(notificationId));
            param.put("NEW_ROLE", newRole.toUpperCase());
            param.put("FORWARD_COMMENT", userComment);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Transfer Notification Error",e);
        }
        return result;
    }

    public String forwardNotification(int notificationId, String newRole, String userComment) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/wf_notification/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/forward/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/forward/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("NID", String.valueOf(notificationId));
            param.put("NEW_ROLE", newRole.toUpperCase());
            param.put("FORWARD_COMMENT", userComment);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Forward Notification Error",e);
        }
        return result;
    }

    public String requestInformation(int notificationId, String username, String comment, String responder) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/notif_req_info/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/xxstc_eam_custom_api/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api/notif_req_info/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("NID", String.valueOf(notificationId));
            param.put("USERNAME", username);
            param.put("FROM_EMAIL", responder);
            param.put("COMMENT", comment);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Request Information Error",e);
        }
        return result;
    }

    public String closeNotification(int notificationId, String responder) {

        String result = null;
        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/wf_notification/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/close/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/owf/soaprovider/plsql/wf_notification/close/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("NID", String.valueOf(notificationId));
            param.put("RESPONDER", responder);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Close Notification Error",e);
        }
        return result;
    }

    public String getActionHistory(int notificationId) {

        String result = null;

        String statement = "SELECT * FROM (SELECT FROM_ROLE, FROM_USER, TO_USER, COMMENT_DATE, ACTION, USER_COMMENT,\n" +
                "LANGUAGE, SEQUENCE FROM APPS.WF_COMMENTS WHERE NOTIFICATION_ID = ?) QRSLT ORDER BY COMMENT_DATE ASC, SEQUENCE ASC";
        try {

            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1,notificationId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("FROM_ROLE",resultSet.getString("FROM_ROLE"));
                jsonObject.addProperty("FROM_USER",resultSet.getString("FROM_USER"));
                jsonObject.addProperty("TO_USER",resultSet.getString("TO_USER"));
                jsonObject.addProperty("COMMENT_DATE",resultSet.getString("COMMENT_DATE"));
                if (resultSet.getString("ACTION").equals("SEND_FIRST")) {
                    jsonObject.addProperty("ACTION" , "Submit");
                }
                else if (resultSet.getString("ACTION").equals("ANSWER")) {
                    jsonObject.addProperty("ACTION" , "Provide Information");
                }
                else if (resultSet.getString("ACTION").equals("QUESTION")) {
                    jsonObject.addProperty("ACTION" , "Request Information");
                }
                else if (resultSet.getString("ACTION").equals("RESPOND")) {
                    jsonObject.addProperty("ACTION" , "Additional Information");
                }
                else
                    jsonObject.addProperty("ACTION" , resultSet.getString("ACTION"));
                jsonObject.addProperty("USER_COMMENT",resultSet.getString("USER_COMMENT"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ACTION_HISTORY",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Action History Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getUserRoles() {

        String result = null;

        String statement = "SELECT ORIG_SYSTEM, DISPLAY_NAME FROM APPS.WF_DIRECTORY_PARTITIONS_TL WDPTL \n" +
                "WHERE userenv('LANG') = WDPTL.LANGUAGE and ORIG_SYSTEM not in ('PER_ROLE') order by \n" +
                "decode(ORIG_SYSTEM, 'VIRTUAL_ORIG_SYS','A', 'PER','E', 'FND_USR','E', 'PQH_ROLE','E', 'HZ_PARTY','E', 'WF_LOCAL_ROLES','Z','R'), DISPLAY_NAME";
        try {

            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORIG_SYSTEM",resultSet.getString("ORIG_SYSTEM"));
                jsonObject.addProperty("DISPLAY_NAME",resultSet.getString("DISPLAY_NAME"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("USER_ROLES",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get User Roles Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getUserListFromRoles(String origSystem, String userName) {

        String result = null;

        String statement = "SELECT * FROM (SELECT WU.NAME AS USER_NAME, WU.DISPLAY_NAME, WDP.ORIG_SYSTEM, WU.EMAIL_ADDRESS, WU.ORIG_SYSTEM_ID, WU.PARTITION_ID \n" +
                "FROM APPS.WF_ROLE_LOV_VL WU, APPS.WF_DIRECTORY_PARTITIONS WDP WHERE WU.PARTITION_ID = WDP.PARTITION_ID\n" +
                "UNION ALL SELECT WU.NAME AS USER_NAME, WU.DISPLAY_NAME, WU.ORIG_SYSTEM, WU.EMAIL_ADDRESS, WU.ORIG_SYSTEM_ID, WU.PARTITION_ID \n" +
                "FROM WF_ROLE_LOV_VL WU WHERE WU.ORIG_SYSTEM = 'PER' AND WU.PARTITION_ID = 1) QRSLT WHERE ORIG_SYSTEM LIKE ? AND USER_NAME LIKE ? ORDER BY DISPLAY_NAME";
        try {

            connection = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            if (origSystem.equals("VIRTUAL_ORIG_SYS")) {
                origSystem = "%%";
            }
            preparedStatement.setString(1, origSystem);

            if (userName != null) {
                preparedStatement.setString(2, "%" + userName + "%");
            }
            else {
                preparedStatement.setString(2, "%%");
            }

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("USER_NAME",resultSet.getString("USER_NAME"));
                jsonObject.addProperty("DISPLAY_NAME",resultSet.getString("DISPLAY_NAME"));
                jsonObject.addProperty("EMAIL_ADDRESS",resultSet.getString("EMAIL_ADDRESS"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("USER_LIST",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get User List From Roles",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getWorkflowParticipant(int notificationId) {

        String result = null;

        String statement = "SELECT USER_NAME USERNAME, USER_DISPLAY USERDISPLAY FROM (SELECT DISTINCT role user_name, wf_directory.GetRoleDisplayName(role) user_display \n" +
                "FROM ( SELECT wis.item_type, wis.item_key, wis.owner_role role FROM APPS.wf_items wis WHERE owner_role IS NOT NULL AND (wis.item_type, wis.item_key) \n" +
                "IN( SELECT item_type, item_key FROM APPS.wf_items wi WHERE item_type = (SELECT ITEM_TYPE FROM APPS.WF_ITEM_ACTIVITY_STATUSES WHERE notification_id = ?) " +
                "AND item_key = (SELECT ITEM_KEY FROM APPS.WF_ITEM_ACTIVITY_STATUSES WHERE notification_id = ?) )))";
        try {

            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1,notificationId);
            preparedStatement.setInt(2,notificationId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("USER_NAME",resultSet.getString("USERNAME"));
                jsonObject.addProperty("USER_DISPLAY",resultSet.getString("USERDISPLAY"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORKFLOW_PARTICIPANT",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Workflow Participant Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public boolean isFYI(int notificationId) {

        boolean result = false;
        String statement = "SELECT count(1) FROM   APPS.wf_message_attributes wma, APPS.wf_notifications wn WHERE  wn.notification_id = ? AND wma.message_type = wn.message_type \n" +
                "AND wma.message_name = wn.message_name AND wma.subtype = 'RESPOND' AND rownum = 1";

        try{
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, notificationId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            if (resultSet.next()) {
                int response = resultSet.getInt(1);
                if (response == 0) {
                    result = true;
                }
                else {
                    result = false;
                }
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();

        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Is FYI Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }

        return result;
    }

    public String getWrItemKey(int workRequestId) {
        String result = "";

        String statement = "SELECT ATTRIBUTE3 FROM APPS.wip_eam_work_requests WHERE WORK_REQUEST_ID = ?";
        try {

            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, workRequestId);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                result = resultSet.getString(1);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Item Key Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    public String getWoItemKey(int entityId) {
        String result = "";

        String statement = "SELECT ATTRIBUTE14 FROM WIP_DISCRETE_JOBS WHERE WIP_ENTITY_ID = ?";
        try {

            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, entityId);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                result = resultSet.getString(1);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Item Key Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }

    void closeAllConnection(Connection connection,
                            PreparedStatement preparedStatement,
                            ResultSet resultSet,
                            CallableStatement callableStatement) {

        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

    }
    
//        public static void main(String args[]){
//        NotificationController a =new NotificationController();
////        a.getWeeklyReport("9401","11/11/2018","11/12/2018");
////        a.getNotification("MATTA");
//          a.getNotificationAttachementForWorkOrder("XXXSTCHQ", "P3WO395763_95673");
//     
//    }



    
    
    public String receiptApprove(int entityId) {

        String result = "";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_approved/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_approved/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("ITEMTYPE", "XXXSTCHQ");
            param.put("ITEMKEY", getReceiptItemKey(entityId));
            param.put("FUNCMODE", String.valueOf("RUN"));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Work Order approve for HQC Error", e);
        }

        return result;

    }
    
    public String receiptReject(int entityId) {

        String result = "";
        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_rejected/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_rejected/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("ITEMTYPE", "XXXSTCHQ");
            param.put("ITEMKEY", getReceiptItemKey(entityId));
            param.put("FUNCMODE", String.valueOf("RUN"));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Work Order Reject HQC Error", e);
        }

        return result;

    }

    public String receiptCancel(int entityId) {

        String result = "";

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_canceled/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wo_wf_pkg_soa/wf_set_action_canceled/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("ITEMTYPE", "XXXSTCHQ");
            param.put("ITEMKEY", getReceiptItemKey(entityId));
            param.put("FUNCMODE", String.valueOf("RUN"));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" Work order  cancel for hqc Error", e);
        }

        return result;

    }
    
    public String getReceiptItemKey(int entityId) {
        String result = "";

        String statement = "SELECT ATTRIBUTE14 FROM WIP_DISCRETE_JOBS WHERE WIP_ENTITY_ID = ?";
        try {

            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, entityId);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                result = resultSet.getString(1);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Item Key Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
}
