package com.stc.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 02/01/18
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */
public class WorkRequestController extends PublicController {

    final static Logger logger = Logger.getLogger(WorkRequestController.class);

    public PreparedStatement preparedStatement;
    public NamedParameterStatement namedParameterStatement;
    public ResultSet resultSet;

    public String getWorkRequestDashboard(int orgId,int userId) {

        String result = null;

        String statement = "select ml1.meaning as STATUS, count(*) as COUNT from APPS.wip_eam_work_requests wewr, APPS.fnd_lookup_values ml1 " +
                "where wewr.organization_id = ? and wewr.CREATED_BY=? and ml1.lookup_type = 'XXX_STC_WO_WF_STATUS' \n" +
                "AND wewr.attribute2 = ml1.lookup_code group by ml1.meaning order by status";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, orgId);
            preparedStatement.setInt(2, userId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("STATUS",resultSet.getString(1));
                jsonObject.addProperty("COUNT",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_REQUEST_DASHBOARD",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Request Dashboard Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getAssetNumber(String orgId, String assetNumber, String assetGroup, String department) {

        String result = null;

            String statement = "SELECT * FROM (SELECT cii.instance_id, cii.inventory_item_id, cii.serial_number, cii.instance_description AS descriptive_text, mp.maint_organization_id current_organization_id, \n" +
                    "msi.concatenated_segments, msi.eam_item_type, ml1.meaning AS Asset_Type, mck.concatenated_segments category_name, nvl(cii.maintainable_flag, 'Y') maintainable_flag,  \n" +
                    "el.location_codes eam_location, nvl(eomd.owning_department_id, wep.default_department_id) owning_department_id, nvl(bd.department_code, bd1.department_code) owning_department,  \n" +
                    "cii.instance_number, cii.last_vld_organization_id inv_organization_id, cii.instance_id maintenance_object_id FROM APPS.mtl_parameters mp, APPS.csi_item_instances cii,  \n" +
                    "(select * from APPS.eam_org_maint_defaults where organization_id = :orgId1 ) eomd, APPS.mfg_lookups ml1, APPS.bom_departments bd, APPS.bom_departments bd1, APPS.mtl_eam_locations el,  \n" +
                    "APPS.mtl_categories_kfv mck, APPS.mtl_system_items_b_kfv msi, APPS.wip_eam_parameters wep WHERE mp.organization_id = cii.last_vld_organization_id  \n" +
                    "AND cii.last_vld_organization_id = msi.organization_id AND msi.inventory_item_id = cii.inventory_item_id AND msi.eam_item_type in (1,3)  \n" +
                    "AND msi.serial_number_control_code <> 1 AND nvl(cii.active_start_date, sysdate-1) <= sysdate AND nvl(cii.active_end_date, sysdate+1) >= sysdate  \n" +
                    "AND (cii.maintainable_flag = 'Y' or cii.maintainable_flag is null) AND cii.instance_id = eomd.object_id (+) AND eomd.object_type (+) = 50  \n" +
                    "AND bd.department_id(+) = eomd.owning_department_id AND eomd.area_id = el.location_id(+) AND cii.category_id = mck.category_id(+)  \n" +
                    "AND ml1.lookup_type = 'MTL_EAM_ASSET_TYPE' AND ml1.lookup_code = msi.eam_item_type AND wep.organization_id = mp.maint_organization_id  \n" +
                    "AND wep.default_department_id = bd1.department_id(+)) QRSLT WHERE (current_organization_id = :orgId2 )";

            if (assetNumber != null && !assetNumber.isEmpty()) {
            statement += " AND (instance_id LIKE :assetNumber or instance_id is null)";
        }
        if (assetGroup != null && !assetGroup.isEmpty()) {
            statement += " AND (inventory_item_id like :assetGroup or inventory_item_id is null)";
        }
        if (department != null && !department.isEmpty()) {
            statement += " and (owning_department_id like :department or owning_department_id is null)";
        }

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            namedParameterStatement = new NamedParameterStatement(connection,statement);
            namedParameterStatement.setFetchSize(1000);
            namedParameterStatement.setString("orgId1", orgId);
            namedParameterStatement.setString("orgId2", orgId);

            if (assetNumber != null && !assetNumber.isEmpty()) {
                namedParameterStatement.setString("assetNumber", "%" + assetNumber + "%");
            }
            if (assetGroup != null && !assetGroup.isEmpty()) {
                namedParameterStatement.setString("assetGroup","%" + assetGroup + "%");
            }
            if (department != null && !department.isEmpty()) {
                namedParameterStatement.setString("department","%" + department + "%");
            }

            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ASSET_ID",resultSet.getString(1));
                jsonObject.addProperty("ASSET_NUMBER",resultSet.getString(14));
                jsonObject.addProperty("ASSET_SERIAL_NUMBER",resultSet.getString(3));
                jsonObject.addProperty("ASSET_DESCRIPTION",resultSet.getString(4));
                jsonObject.addProperty("ASSET_TYPE",resultSet.getString(8));
                jsonObject.addProperty("ASSET_GROUP_ID",resultSet.getString(2));
                jsonObject.addProperty("ASSET_GROUP",resultSet.getString(6));
                jsonObject.addProperty("ASSET_CATEGORY", resultSet.getString(9));
                jsonObject.addProperty("AREA", resultSet.getString(11));
                jsonObject.addProperty("OWNING_DEPARTMENT_ID", resultSet.getString(12));
                jsonObject.addProperty("OWNING_DEPARTMENT", resultSet.getString(13));
                jsonObject.addProperty("MAINTENANCE_OBJECT_ID", resultSet.getString(16));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ASSET_NUMBER",jsonArray);
            result = jsonResult.toString();

            connection.close();
            namedParameterStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Asset Number Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getCreatorName(int orgId){
        String result = null;

        String statement = "SELECT * FROM (SELECT user_name, user_id, (CASE WHEN full_name IS NULL THEN user_name ELSE full_name END) AS user_full_name\n" +
                "          FROM apps.fnd_user fu, (SELECT * FROM apps.per_all_people_f WHERE employee_number IS NOT NULL\n" +
                "          AND TRUNC (SYSDATE) BETWEEN effective_start_date AND effective_end_date) pe\n" +
                "          WHERE (fu.end_date IS NULL OR fu.end_date > SYSDATE) AND fu.employee_id = pe.person_id(+)\n" +
                "          AND (    UPPER (user_name) NOT LIKE '%SYSADMIN%'\n" +
                "          AND UPPER (user_name) NOT LIKE '%ORACLE%'\n" +
                "          AND UPPER (user_name) NOT LIKE '%@%')) qrslt\n" +
                "          WHERE user_id IN (SELECT DISTINCT (created_by) FROM apps.wip_eam_work_requests WHERE organization_id = ?)";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, orgId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("USER_NAME",resultSet.getString(1));
                jsonObject.addProperty("USER_ID",resultSet.getString(2));
                jsonObject.addProperty("FULL_NAME",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("CREATORNAME",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Creator Name Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getDepartment(String orgId) {

        String result = null;

        String statement = "SELECT * FROM (select bd.department_code, bd.description, bd.department_id, bd.organization_id, " +
                "beda.dept_id as check_id from APPS.bom_departments bd, APPS.bom_eam_dept_approvers beda where bd.department_id = beda.dept_id(+) " +
                "and nvl(bd.disable_date, sysdate+1) > sysdate) QRSLT WHERE (organization_id = ?)";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setString(1, orgId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DEPARTMENT_CODE",resultSet.getString(1));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString(2));
                jsonObject.addProperty("DEPARTMENT_ID",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("DEPARTMENTS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Department Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }
    
    public String getBuildingType() {

        String result = null;

        String statement = "SELECT\n"
                + "     descriptive_flex_context_code,\n"
                + "     descriptive_flex_context_name\n"
                + " FROM\n"
                + "     fnd_descr_flex_contexts_vl\n"
                + " WHERE\n"
                + "     ( application_id = 542 )\n"
                + "     AND ( descriptive_flexfield_name = 'Item Instance Flex Field' )\n"
                + "     AND global_flag <> 'Y'";
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
//            preparedStatement.setString(1, orgId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("BUILDING_CODE",resultSet.getString("descriptive_flex_context_code"));
                jsonObject.addProperty("BUILDING_NAME",resultSet.getString("descriptive_flex_context_name"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("BUILDING_TYPES",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Building Type Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getPriority() {

        String result = null;

        String statement = "SELECT LOOKUP_CODE, MEANING, DESCRIPTION FROM APPS.MFG_LOOKUPS WHERE LOOKUP_TYPE = 'WIP_EAM_ACTIVITY_PRIORITY'";
        try {
            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("PRIORITY_CODE",resultSet.getString(1));
                jsonObject.addProperty("PRIORITY_NAME",resultSet.getString(2));
                jsonObject.addProperty("PRIORITY_DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("PRIORITY",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Priority Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getRequestType() {
        String result = null;

        String statement = "select LOOKUP_CODE, MEANING, DESCRIPTION from APPS.mfg_lookups where lookup_type = 'WIP_EAM_WORK_REQ_TYPE'";
        try {
            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("REQUEST_TYPE_CODE",resultSet.getString(1));
                jsonObject.addProperty("REQUEST_TYPE_NAME",resultSet.getString(2));
                jsonObject.addProperty("REQUEST_TYPE_DESCRIPTION",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("REQUEST_TYPE",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Request Type Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getRequestFor(String username, String fullname) {

        String result = null;

        String statement = "SELECT * FROM (SELECT user_name, user_id, full_name, end_date FROM APPS.fnd_user fu, (select * FROM APPS.per_all_people_f WHERE employee_number is not null \n" +
                "AND trunc ( sysdate ) between effective_start_date AND effective_end_date ) pe WHERE (fu.end_date IS NULL OR fu.end_date > sysdate) AND fu.employee_id = pe.person_id(+)) QRSLT\n" +
                "where 1=1 AND ( UPPER (user_name) NOT LIKE '%SYSADMIN%' AND UPPER (user_name) NOT LIKE '%ORACLE%' AND UPPER (user_name) NOT LIKE '%@%' ) ";

        
        
        if (username != null && !username.isEmpty()) {
            statement += " and (upper(user_name) like :userName)";
        }
        if (fullname != null && !fullname.isEmpty()) {
            statement += " and (upper(full_name) like :fullName)";
        }

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            namedParameterStatement = new NamedParameterStatement(connection,statement);
            namedParameterStatement.setFetchSize(1000);

            if (username != null && !username.isEmpty()) {
                namedParameterStatement.setString("userName", username.toUpperCase());
            }
            if (fullname != null && !fullname.isEmpty()) {
                namedParameterStatement.setString("fullName", fullname.toUpperCase());
            }

            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("USER_NAME",resultSet.getString(1));
                jsonObject.addProperty("USER_ID",resultSet.getString(2));
                jsonObject.addProperty("FULL_NAME",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("REQUESTED_FOR",jsonArray);
            result = jsonResult.toString();

            connection.close();
            namedParameterStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Request For Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWorkRequestStatus() {

        String result = null;

        String statement = "SELECT * FROM (select lookup_code,meaning from APPS.mfg_lookups where lookup_type='WIP_EAM_WORK_REQ_STATUS' and enabled_flag='Y' \n" +
                "and sysdate between nvl(start_date_active,sysdate) and nvl(end_date_active,sysdate)) QRSLT ORDER BY MEANING";
        try {
            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("CODE",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("STATUS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Request Status Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWfStatus(String language) {
        String result = null;
        String statement = "Select LOOKUP_CODE,MEANING from APPS.FND_LOOKUP_VALUES WHERE LOOKUP_TYPE='XXX_STC_WO_WF_STATUS' AND LANGUAGE=?";
        try {
            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1,language);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("LOOKUP_CODE",resultSet.getString("LOOKUP_CODE"));
                jsonObject.addProperty("MEANING",resultSet.getString("MEANING"));
                jsonArray.add(jsonObject);
            }

            /*for (int i=1; i<5; i++) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("LOOKUP_CODE", "LOOKUP_CODE " + i);
                jsonObject.addProperty("MEANING", "MEANING " + i);
                jsonArray.add(jsonObject);
            }*/

            jsonResult.add("STATUS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Wf Status Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWrSources() {

        String result = null;

        String statement = "SELECT v.FLEX_VALUE,\n" +
                "         v.FLEX_VALUE_MEANING,\n" +
                "         v.DESCRIPTION\n" +
                "    FROM APPS.FND_FLEX_VALUES_VL v, APPS.FND_FLEX_VALUE_SETS VS\n" +
                "   WHERE  v.FLEX_VALUE_SET_ID = vs.FLEX_VALUE_SET_ID\n" +
                "   AND FLEX_VALUE_SET_NAME = 'STC: Work Request Source'";
        try {
            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("FLEX_VALUE",resultSet.getString("FLEX_VALUE"));
                jsonObject.addProperty("FLEX_VALUE_MEANING",resultSet.getString("FLEX_VALUE_MEANING"));
                jsonObject.addProperty("DESCRIPTION",resultSet.getString("DESCRIPTION"));
                jsonArray.add(jsonObject);
            }

            /*for (int i=0; i<5; i++) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("FLEX_VALUE", "FLEX_VALUE " + i);
                jsonObject.addProperty("FLEX_VALUE_MEANING", "FLEX_VALUE_MEANING " + i);
                jsonObject.addProperty("DESCRIPTION", "DESCRIPTION " + i);
                jsonArray.add(jsonObject);
            }*/

            jsonResult.add("STATUS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Request Status Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getWorkRequest(String orgId,
            String requestNumber,
            String assetDepartment,
            String status,
            String assetNumber,
            Date creationDateFrom,
            Date creationDateTo,
            String creatorName, // send userid
            String customStatus,
            String isHqc,
            String userID,
            String respId,
            String respAppId,
            String dasboardSearch
         
    ) {

        String result = null;
        System.out.println("isHqc : " + isHqc);
        System.out.println("customStatus : " + customStatus);
        System.out.println("orgId : " + orgId);

        String statement = "SELECT\n"
                + "     *\n"
                + " FROM\n"
                + "     (\n"
                + "         SELECT\n"
                + "             wewr.work_request_number,\n"
                + "             wewr.attribute15,\n"
                + "             wewr.organization_id,\n"
                + "             wewr.work_request_id,\n"
                + "             (SELECT count(ATTACHED_DOCUMENT_ID) FROM apps.fnd_attached_documents fad where fad.PK1_VALUE = TO_CHAR(wewr.work_request_id)  AND fad.entity_name='EAM_WORK_REQUESTS')  attachments_count,"
                + "             (\n"
                + "                        SELECT NOTES \n"
                + "                            FROM wip_eam_work_req_notes rn \n"
                + "                            WHERE     rn.work_request_id = wewr.work_request_id \n"
                + "                                   AND last_update_date = \n"
                + "                                          (SELECT MAX (last_update_date) \n"
                + "                                             FROM wip_eam_work_req_notes rn1 \n"
                + "                                            WHERE rn1.work_request_id = rn.work_request_id \n"
                + "                                                  AND rn1.notes <> rn1.work_request_id  || TO_CHAR (rn1.creation_date, 'ddmmyyyy') || '.txt' \n"
                + "                                                  AND rn1.notes NOT LIKE '**%' \n"
                + "                                                  AND rn1.notes NOT LIKE '@@%') \n"
                + "                                   AND notes NOT LIKE '**%' \n"
                + "                                   AND notes NOT LIKE '@@%' \n"
                + "                                   AND notes <>   work_request_id || TO_CHAR (creation_date, 'ddmmyyyy') || '.txt' \n"
                + "                                   AND ROWNUM = 1 \n"
                + "             ) AS workdesc,\n"
                + "             (\n"
                + "                 SELECT\n"
                + "                     notes\n"
                + "                 FROM\n"
                + "                     wip_eam_work_req_notes rn\n"
                + "                 WHERE\n"
                + "                     rn.work_request_id = wewr.work_request_id\n"
                + "                     AND last_update_date = (\n"
                + "                         SELECT\n"
                + "                             MAX(last_update_date)\n"
                + "                         FROM\n"
                + "                             wip_eam_work_req_notes rn1\n"
                + "                         WHERE\n"
                + "                             rn1.work_request_id = rn.work_request_id\n"
                + "                     )\n"
                + "                     AND notes NOT LIKE '**%'\n"
                + "                     AND notes NOT LIKE '@@%'\n"
                + "                     AND ROWNUM = 1\n"
                + "             ) AS expworkdesc,\n"
                + "             + (\n"
                + "                 SELECT DISTINCT\n"
                + "                     ( meaning )\n"
                + "                 FROM\n"
                + "                     fnd_lookup_values\n"
                + "                 WHERE\n"
                + "                     lookup_type = 'XXX_STC_WO_WF_STATUS'\n"
                + "                     AND lookup_code = wewr.attribute2\n"
                + "             ) AS wf_status,\n"
                + "             wewr.attribute2,\n"
                + "             wewr.attribute4,\n"
                + "             cii.instance_id,\n"
                + "             cii.instance_number         asset_number,\n"
                + "             wewr.phone_number,\n"
                + "             cii.inventory_item_id,\n"
                + "             msi.concatenated_segments,\n"
                + "             wewr.work_request_owning_dept,\n"
                + "             bd.department_code,\n"
                + "             bd.description              AS dept_desc_wr,\n"
                + "             mck.concatenated_segments   AS category_name,\n"
                + "             (\n"
                + "                 SELECT\n"
                + "                     ewov.wip_entity_name\n"
                + "                 FROM\n"
                + "                     apps.eam_work_orders_v ewov\n"
                + "                 WHERE\n"
                + "                     ewov.wip_entity_id = wewr.wip_entity_id\n"
                + "             ) AS work_order,\n"
                + "             el.location_codes           AS eam_location,\n"
                + "             wewr.work_request_status_id,\n"
                + "             ml1.meaning                 AS statusvalue,\n"
                + "             ml1.LOOKUP_CODE                 AS statuscode,\n"
                + "             wewr.work_request_priority_id,\n"
                + "             ml2.meaning                 AS priorityvalue,\n"
                + "             wewr.work_request_type_id,\n"
                + "             ml3.meaning                 AS requesttypevalue,\n"
                + "             wewr.expected_resolution_date,\n"
                + "             wewr.created_by,\n"
                + "             fu1.user_name,\n"
                + "             fu1.user_id,\n"
                + "             (\n"
                + "                 SELECT\n"
                + "                     papf.full_name\n"
                + "                 FROM\n"
                + "                     per_all_people_f papf\n"
                + "                 WHERE\n"
                + "                     papf.person_id = fu1.employee_id\n"
                + "                     AND ROWNUM = 1\n"
                + "             ) AS full_name,\n"
                + "             wewr.created_for,\n"
                + "             fu2.user_name               AS created_for_name,\n"
                + "             (\n"
                + "                 SELECT\n"
                + "                     papf.full_name\n"
                + "                 FROM\n"
                + "                     per_all_people_f papf\n"
                + "                 WHERE\n"
                + "                     papf.person_id = fu2.employee_id\n"
                + "                     AND ROWNUM = 1\n"
                + "             ) AS created_for_full_name,\n"
                + "             wewr.wip_entity_id,\n"
                + "             msi.eam_item_type,\n"
                + "             to_char(wewr.creation_date, 'dd-MON-yyyy hh:mmAM') AS formated_creation_date,\n"
                + "             wewr.creation_date,\n"
                + "             ml4.meaning                 AS asset_type,\n"
                + "             we.wip_entity_name,\n"
                + "             wewr.maintenance_object_id,\n"
                + "             cii.instance_number,\n"
                + "             cii.instance_description    AS assetdesc,\n"
                + "             mp.maint_organization_id,\n"
                + "             +wewr.e_mail,\n"
                + "             wewr.contact_preference,\n"
                + "             wewr.notify_originator,\n"
                + "             +wewr.asset_number AS anum,\n"
                + "             wewr.asset_group            AS agroup\n"
                + "         FROM\n"
                + "             apps.wip_eam_work_requests wewr,\n"
                + "             apps.mtl_system_items_b_kfv msi,\n"
                + "             apps.csi_item_instances cii,\n"
                + "             apps.eam_org_maint_defaults eomd,\n"
                + "             apps.bom_departments bd,\n"
                + "             apps.mtl_eam_locations el,\n"
                + "             apps.mtl_categories_kfv mck,\n"
                + "             apps.mfg_lookups ml1,\n"
                + "             apps.mfg_lookups ml2,\n"
                + "             apps.mfg_lookups ml3,\n"
                + "             apps.mfg_lookups ml4,\n"
                + "             apps.fnd_user fu1,\n"
                + "             apps.fnd_user fu2,\n"
                + "             apps.wip_entities we,\n"
                + "             apps.mtl_parameters mp\n"
                + "         WHERE\n"
                + "             wewr.maintenance_object_id = cii.instance_id (+)\n"
                + "             AND cii.last_vld_organization_id = msi.organization_id (+)\n"
                + "             AND cii.inventory_item_id = msi.inventory_item_id (+)\n"
                + "             AND cii.instance_id = eomd.object_id (+)\n"
                + "             AND eomd.object_type (+) = 50\n"
                + "             AND eomd.organization_id (+) = " + orgId
                + "             AND wewr.work_request_owning_dept = bd.department_id (+)\n"
                + "             AND eomd.area_id = el.location_id (+)\n"
                + "             AND cii.category_id = mck.category_id (+)\n"
                + "             AND ml1.lookup_type = 'WIP_EAM_WORK_REQ_STATUS'\n"
                + "             AND wewr.work_request_status_id = ml1.lookup_code\n"
                + "             AND ml2.lookup_type (+) = 'WIP_EAM_ACTIVITY_PRIORITY'\n"
                + "             AND wewr.work_request_priority_id = ml2.lookup_code (+)\n"
                + "             AND ml3.lookup_type (+) = 'WIP_EAM_WORK_REQ_TYPE'\n"
                + "             AND wewr.work_request_type_id = ml3.lookup_code (+)\n"
                + "             AND ml4.lookup_type (+) = 'MTL_EAM_ASSET_TYPE'\n"
                + "             AND msi.eam_item_type = ml4.lookup_code (+)\n"
                + "             AND fu1.user_id = wewr.created_by\n"
                + "             AND fu2.user_id (+) = wewr.created_for\n"
                + "             AND wewr.wip_entity_id = we.wip_entity_id (+)\n"
                + "             AND cii.last_vld_organization_id = mp.organization_id\n"
                + "         ORDER BY\n"
                + "             wewr.work_request_number\n"
                + "     ) qrslt\n"
                + " WHERE\n"
                + "     1 = 1\n"
                + "     AND organization_id =" + orgId
                + "     AND xxstc_wip_hq_wr_wf_soa_pkg.is_hqc_org(" + orgId + ") = '" + isHqc + "'\n"
                + "     AND ( '" + isHqc + "' = 'N'\n"
                + "           OR ( '" + isHqc + "'= 'Y'\n"
                + "                AND ( ( (\n"
                + "         SELECT\n"
                + "             nvl(fnd_profile.value('XXX_SCM_EAM_SUPERVISOR'),'N')\n"
                + "         FROM\n"
                + "             dual\n"
                + "     ) = 'Y'\n"
                + "                        AND (\n"
                + "         SELECT\n"
                + "             nvl(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')\n"
                + "         FROM\n"
                + "             dual\n"
                + "     ) = 'N'\n"
                + "                        OR ( (\n"
                + "         SELECT\n"
                + "             nvl(fnd_profile.value('XXX_SCM_EAM_SUPERVISOR'),'N')\n"
                + "         FROM\n"
                + "             dual\n"
                + "     ) <> 'Y'\n"
                + "                             AND (\n"
                + "         SELECT\n"
                + "             nvl(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')\n"
                + "         FROM\n"
                + "             dual\n"
                + "     ) = 'N'\n"
                + "                             AND qrslt.work_request_number IN (\n"
                + "         SELECT DISTINCT\n"
                + "             work_request_number\n"
                + "         FROM\n"
                + "             xx_mtl_eam_asset_numbers_all_v cii,\n"
                + "             stc.xxx_stc_eam_approvers app,\n"
                + "             xxx_stc_eam_approvers_dept_v dept,\n"
                + "             fnd_flex_values_vl fv,\n"
                + "             fnd_flex_value_sets fvs,\n"
                + "             fnd_flex_values_vl fv1,\n"
                + "             fnd_flex_value_sets fvs1,\n"
                + "             bom_departments bd,\n"
                + "             org_organization_definitions org,\n"
                + "             wip_eam_work_requests wdj\n"
                + "         WHERE\n"
                + "             fv.flex_value_set_id = fvs.flex_value_set_id\n"
                + "             AND fvs.flex_value_set_name = 'XXSCM_Zone'\n"
                + "             AND fv.flex_value = cii.attribute1\n"
                + "             AND dept.zone_id = fv.flex_value_id\n"
                + "             AND dept.approver_dept_id = app.approver_dept_id\n"
                + "             AND org.organization_id = dept.organization_id\n"
                + "             AND org.organization_code = 'E00'\n"
                + "             AND wdj.work_request_owning_dept = bd.department_id\n"
                + "             AND bd.attribute1 = fv1.flex_value\n"
                + "             AND fv1.flex_value_id = dept.department_id\n"
                + "             AND fv1.flex_value_set_id = fvs1.flex_value_set_id\n"
                + "             AND fvs1.flex_value_set_name = 'XXSCM_Departments'\n"
                + "             AND dept.position_name IN (\n"
                + "                 'ABFM',\n"
                + "                 'BFM'\n"
                + "             )\n"
                + "             AND wdj.attribute15 IS NULL\n"
                + "             AND wdj.work_request_number = qrslt.work_request_number\n"
                + "             AND cii.instance_number = (\n"
                + "                 SELECT\n"
                + "                     CASE\n"
                + "                         WHEN instr(substr(c.instance_number,8,5),'-') BETWEEN 1 AND 4 THEN rtrim(substr(c.instance_number,1,10),'-'\n"
                + "                         )\n"
                + "                         WHEN instr(substr(c.instance_number,8,5),'-') > 4           THEN rtrim(substr(c.instance_number,1,11),'-')\n"
                + "                         ELSE c.instance_number\n"
                + "                     END\n"
                + "                 FROM\n"
                + "                     xx_mtl_eam_asset_numbers_all_v c\n"
                + "                 WHERE\n"
                + "                     current_organization_id = qrslt.organization_id\n"
                + "                     AND c.serial_number = (\n"
                + "                         SELECT\n"
                + "                             asset_number\n"
                + "                         FROM\n"
                + "                             wip_eam_work_requests wdj1\n"
                + "                         WHERE\n"
                + "                             wdj1.work_request_number = qrslt.work_request_number\n"
                + "                             AND wdj1.organization_id = qrslt.organization_id\n"
                + "                     )\n"
                + "             )\n"
                + "             AND app.user_id = (\n"
                + "                 SELECT\n"
                + "                     TO_CHAR(fnd_profile.value('USER_ID') )\n"
                + "                 FROM\n"
                + "                     dual\n"
                + "             )\n"
                + "     ) ) )\n"
                + "                      OR ( (\n"
                + "         SELECT\n"
                + "             nvl(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')\n"
                + "         FROM\n"
                + "             dual\n"
                + "     ) = 'Y'\n"
                + "                           AND qrslt.work_request_number IN (\n"
                + "         SELECT DISTINCT\n"
                + "             work_request_number\n"
                + "         FROM\n"
                + "             wip_eam_work_requests wdj\n"
                + "         WHERE\n"
                + "             wdj.attribute15 IS NOT NULL\n"
                + "             AND wdj.work_request_number = qrslt.work_request_number\n"
                + "     ) )\n"
                + "                      OR ( (\n"
                + "         SELECT\n"
                + "             nvl(fnd_profile.value('XXX_EAM_CONT_SUPERUSER'),'N')\n"
                + "         FROM\n"
                + "             dual\n"
                + "     ) = 'Y'\n"
                + "                           OR ( (\n"
                + "         SELECT\n"
                + "             nvl(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')\n"
                + "         FROM\n"
                + "             dual\n"
                + "     ) = 'N'\n"
                + "                                AND (\n"
                + "         SELECT\n"
                + "             nvl(fnd_profile.value('XXX_EAM_CONT_SUPERUSER'),'N')\n"
                + "         FROM\n"
                + "             dual\n"
                + "     ) <> 'Y'\n"
                + "                                AND qrslt.work_request_number IN (\n"
                + "         SELECT DISTINCT\n"
                + "             work_request_number\n"
                + "         FROM\n"
                + "             stc.xxx_stc_eam_approvers app,\n"
                + "             xxx_stc_eam_approvers_dept_v dept,\n"
                + "             org_organization_definitions org,\n"
                + "             wip_eam_work_requests wdj\n"
                + "         WHERE\n"
                + "             dept.approver_dept_id = app.approver_dept_id\n"
                + "             AND org.organization_id = dept.organization_id\n"
                + "             AND org.organization_code = 'E00'\n"
                + "             AND dept.position_name = 'CONT_SPEC_ENGINEER'\n"
                + "             AND dept.sub_department_id = wdj.work_request_owning_dept\n"
                + "             AND wdj.work_request_number = qrslt.work_request_number\n"
                + "             AND app.user_id = (\n"
                + "                 SELECT\n"
                + "                     TO_CHAR(fnd_profile.value('USER_ID') )\n"
                + "                 FROM\n"
                + "                     dual\n"
                + "             )\n"
                + "     ) ) ) ) ) ) AND ROWNUM <= 200 ";

        if (requestNumber != null && !requestNumber.isEmpty()) {
            statement += " AND (WORK_REQUEST_NUMBER LIKE UPPER('%" + requestNumber.toUpperCase() + "%') OR WORK_REQUEST_NUMBER IS NULL)";
        }
        if (assetDepartment != null && !assetDepartment.isEmpty()) {
            statement += " AND (work_request_owning_dept LIKE '" + assetDepartment + "' OR work_request_owning_dept IS NULL)";
        }
        if (status != null && !status.isEmpty()) {
            statement += " AND (work_request_status_id LIKE '" + status + "' OR work_request_status_id IS NULL)";
        }
        if (assetNumber != null && !assetNumber.isEmpty()) {
            statement += " AND (instance_id LIKE '" + assetNumber + "' OR instance_id IS NULL)";
        }
        if (customStatus != null && !customStatus.isEmpty()) {
            statement += " AND (attribute2 = '" + customStatus + "' OR attribute2 IS NULL)";
        }

        if (creationDateFrom != null) {
            statement += " AND (CREATION_DATE >= TO_DATE('" + creationDateFrom + "','YYYY-MM-DD') )";
        }
        if (creationDateTo != null) {
            statement += " AND (CREATION_DATE <= TO_DATE('" + creationDateTo + "','YYYY-MM-DD')+1 )";
        }
        if (creatorName != null && !creatorName.isEmpty()) {
            statement += " AND USER_ID ='" + creatorName + "'";
        }
        if (dasboardSearch != null && !dasboardSearch.isEmpty()) {
            statement +="       AND (   ('"+dasboardSearch+"' = 'WR')\n" +
"            OR (attribute15 IS NULL AND '"+dasboardSearch+"' = 'INT')\n" +
"            OR (    attribute15 IS NULL\n" +
"                AND ATTRIBUTE2 = 'DRAFT'\n" +
"                AND statusvalue NOT IN ('Rejected', 'Cancelled', 'Complete')\n" +
"                AND '"+dasboardSearch+"' = 'INT-DRAFT')\n" +
"            OR (    attribute15 IS NULL\n" +
"                AND ATTRIBUTE2 LIKE 'INP%'\n" +
"                AND statusvalue NOT IN ('Rejected', 'Cancelled', 'Complete')\n" +
"                AND '"+dasboardSearch+"' = 'INT-INPROCESS')\n" +
"            OR (    attribute15 IS NULL\n" +
"                AND (statusvalue = 'Rejected' or ATTRIBUTE2 like 'R%')\n" +
"                AND '"+dasboardSearch+"' = 'INT-REJECTED')\n" +
"            OR (    attribute15 IS NULL\n" +
"                AND statusvalue = 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'INT-CANCELED')\n" +
"            OR (    attribute15 IS NULL\n" +
"                AND statusvalue = 'Awaiting Work Order'\n" +
"                AND '"+dasboardSearch+"' = 'INT-AWAITING')\n" +
"            OR (    attribute15 IS NULL\n" +
"                AND statusvalue = 'On Work Order'\n" +
"                AND '"+dasboardSearch+"' = 'INT-ON-WO')\n" +
"            OR (    attribute15 IS NULL\n" +
"                AND statusvalue = 'Complete'\n" +
"                AND '"+dasboardSearch+"' = 'INT-COMPLETE')\n" +
"            OR (    attribute15 IS NOT NULL\n" +
"                AND '"+dasboardSearch+"' = 'IGATE')\n" +
"            OR (    attribute15 IS NOT NULL\n" +
"                AND statusvalue = 'Cancelled'\n" +
"                AND '"+dasboardSearch+"' = 'IGATE-CANCELED')\n" +
"            OR (    attribute15 IS NOT NULL\n" +
"                AND statusvalue = 'Awaiting Work Order'\n" +
"                AND '"+dasboardSearch+"' = 'IGATE-AWAITING')\n" +
"            OR (    attribute15 IS NOT NULL\n" +
"                AND statusvalue = 'Complete'\n" +
"                AND '"+dasboardSearch+"' = 'IGATE-COMPLETE')\n" +
"            OR (    attribute15 IS NOT NULL\n" +
"                AND statusvalue = 'On Work Order'\n" +
"                AND '"+dasboardSearch+"' = 'IGATE-ON-WO'))";
        }
        
        System.out.println("/////////**//////////");
        System.out.println(statement);
        System.out.println("/////////**///////////");

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_sql_queries_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_sql_queries_ws/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            param.put("P_USER_ID", userID);
            param.put("P_RESP_ID", respId);
            param.put("P_RESP_APPL_ID", respAppId);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "WORK_REQUEST");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("WORK_REQUEST");

            JSONObject jsonResult = new JSONObject();
            JSONArray arrayResult = new JSONArray();

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("WORK_REQUEST_ID", jsonArray.getJSONObject(i).get("WORK_REQUEST_ID"));
                jsonObject1.put("WORK_REQUEST_NUMBER", jsonArray.getJSONObject(i).get("WORK_REQUEST_NUMBER"));
                jsonObject1.put("WORK_DESCRIPTION", jsonArray.getJSONObject(i).get("WORKDESC"));
                jsonObject1.put("ASSET_NUMBER", jsonArray.getJSONObject(i).get("ASSET_NUMBER"));
                jsonObject1.put("ASSET_NUMBER_ID", jsonArray.getJSONObject(i).get("ANUM"));
                jsonObject1.put("ASSET_GROUP_ID", jsonArray.getJSONObject(i).get("AGROUP"));
                jsonObject1.put("ASSET_DESCRIPTION", jsonArray.getJSONObject(i).get("ASSETDESC"));
                jsonObject1.put("WORK_REQUEST_OWNING_DEPT", jsonArray.getJSONObject(i).get("WORK_REQUEST_OWNING_DEPT"));
                jsonObject1.put("WORK_REQUEST_DEPT_CODE", jsonArray.getJSONObject(i).get("DEPARTMENT_CODE"));
                jsonObject1.put("WORK_REQUEST_DEPT_DESC", jsonArray.getJSONObject(i).get("DEPT_DESC_WR"));
                jsonObject1.put("WORK_ORDER_NUMBER", jsonArray.getJSONObject(i).get("WORK_ORDER"));
                jsonObject1.put("ASSET_GROUP", jsonArray.getJSONObject(i).get("CONCATENATED_SEGMENTS"));
                jsonObject1.put("STATUS", jsonArray.getJSONObject(i).get("STATUSVALUE"));
                jsonObject1.put("CREATED_BY", jsonArray.getJSONObject(i).get("USER_NAME"));
                jsonObject1.put("CREATED_BY_FULL_NAME", jsonArray.getJSONObject(i).get("FULL_NAME"));
                jsonObject1.put("CREATED_BY_ID", jsonArray.getJSONObject(i).get("CREATED_BY"));
                jsonObject1.put("MOBILE_NO", jsonArray.getJSONObject(i).get("PHONE_NUMBER"));
                jsonObject1.put("CREATION_DATE", jsonArray.getJSONObject(i).get("CREATION_DATE"));
                jsonObject1.put("FORMATED_CREATION_DATE", jsonArray.getJSONObject(i).get("FORMATED_CREATION_DATE"));
                jsonObject1.put("WF_STATUS", jsonArray.getJSONObject(i).get("WF_STATUS"));
                jsonObject1.put("WF_STATUS_CODE", jsonArray.getJSONObject(i).get("ATTRIBUTE2"));
                jsonObject1.put("WR_SOURCE", jsonArray.getJSONObject(i).get("ATTRIBUTE4"));
                jsonObject1.put("CREATED_FOR", jsonArray.getJSONObject(i).get("CREATED_FOR_NAME"));
                jsonObject1.put("CREATED_FOR_FULL_NAME", jsonArray.getJSONObject(i).get("CREATED_FOR_FULL_NAME"));
                jsonObject1.put("CREATED_FOR_ID", jsonArray.getJSONObject(i).get("CREATED_FOR"));
                jsonObject1.put("PRIORITY_ID", jsonArray.getJSONObject(i).get("WORK_REQUEST_PRIORITY_ID"));
                jsonObject1.put("PRIORITY", jsonArray.getJSONObject(i).get("PRIORITYVALUE"));
                jsonObject1.put("EMAIL", jsonArray.getJSONObject(i).get("E_MAIL"));
                jsonObject1.put("CONTACT_PREF", jsonArray.getJSONObject(i).get("CONTACT_PREFERENCE"));
                jsonObject1.put("NOTIFY_ORIGINATOR", jsonArray.getJSONObject(i).get("NOTIFY_ORIGINATOR"));
                jsonObject1.put("ATTACHMENTS_COUNT", jsonArray.getJSONObject(i).get("ATTACHMENTS_COUNT"));
                jsonObject1.put("STATUSCODE", jsonArray.getJSONObject(i).get("STATUSCODE"));

                arrayResult.put(jsonObject1);
            }
             System.out.print("Get Work request result: \n"+jsonArray);
            jsonResult.put("WORK_REQUEST", arrayResult);

            result = jsonResult.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("SOA Error", e);
            logger.error("QUERY : " + statement);
        }
        System.out.println("//getWorkRequest Return Result");
        System.out.println(result);

        return result;
    }

    public String getWorkRequestDetails(String workRequestNumber) {

        String result = null;

        String statement = "SELECT * FROM (SELECT wewr.work_request_id, wewr.work_request_number, fl1.meaning as priority, fl2.meaning as status, wewr.expected_resolution_date, \n" +
                "wewr.creation_date, wewr.organization_id, wewr.asset_number, wewr.asset_group, wewr.work_request_priority_id, wewr.work_request_status_id, fu.user_name,  \n" +
                "bd.department_code, bd.department_id, wewr.work_request_type_id, wewr.work_request_auto_approve, we.wip_entity_name as work_order_number, we.wip_entity_id,  \n" +
                "wewr.created_by, wewr.created_for, wewr.phone_number, wewr.e_mail, wewr.contact_preference, wewr.notify_originator, fu2.user_name as created_for_name,  \n" +
                "wewr.attribute_category, wewr.attribute1, wewr.attribute2, wewr.attribute3, wewr.attribute4, wewr.attribute5, wewr.attribute6, wewr.attribute7,  \n" +
                "wewr.attribute8, wewr.attribute9, wewr.attribute10, wewr.attribute11, wewr.attribute12, wewr.attribute13, wewr.attribute14, wewr.attribute15,  \n" +
                "fl3.meaning work_request_type, cii.instance_number, wewr.maintenance_object_id, fu.user_name as created_by_name FROM APPS.wip_eam_work_requests wewr, APPS.mfg_lookups fl1, APPS.mfg_lookups fl2,  \n" +
                "APPS.fnd_user fu, APPS.wip_entities we, APPS.bom_departments bd, APPS.mfg_lookups fl3, APPS.fnd_user fu2, APPS.csi_item_instances cii WHERE wewr.work_request_priority_id = fl1.lookup_code (+)  \n" +
                "AND fl1.lookup_type(+) ='WIP_EAM_ACTIVITY_PRIORITY' AND wewr.work_request_status_id = fl2.lookup_code AND fl2.lookup_type = 'WIP_EAM_WORK_REQ_STATUS'  \n" +
                "AND wewr.created_by = fu.user_id AND wewr.work_request_owning_dept = bd.department_id(+) AND wewr.wip_entity_id = we.wip_entity_id(+)  \n" +
                "AND wewr.created_for = fu2.user_id(+) AND wewr.work_request_type_id = fl3.lookup_code(+) AND fl3.lookup_type(+) = 'WIP_EAM_WORK_REQ_TYPE'  \n" +
                "AND wewr.maintenance_object_id = cii.instance_id(+)) QRSLT WHERE \n" +
                /*"(work_request_id = ? )";*/
                "(work_request_number = ? )";


        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setString(1, workRequestNumber);
            //System.out.println(statement);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();

            JsonArray requestDetails = new JsonArray();
            JsonArray creationInformation = new JsonArray();

            String orgId = null;
            String maintenanceObjectId = null;
            int work_request_id = 0 ;

            while (resultSet.next()){

                orgId = resultSet.getString(7);
                maintenanceObjectId = resultSet.getString(44);
                work_request_id = resultSet.getInt("WORK_REQUEST_ID");
                JsonObject requestDetailsObject = new JsonObject();
                requestDetailsObject.addProperty("WORK_REQUEST_ID",resultSet.getString("work_request_id"));
                requestDetailsObject.addProperty("ASSET_NUMBER",resultSet.getString("ASSET_NUMBER"));
                requestDetailsObject.addProperty("WORK_REQUEST_NUMBER",resultSet.getString("WORK_REQUEST_NUMBER"));
                requestDetailsObject.addProperty("PRIORITY", resultSet.getString("PRIORITY"));
                requestDetailsObject.addProperty("EXPECTED_RESOLUTION_DATE", resultSet.getString("EXPECTED_RESOLUTION_DATE"));
                requestDetailsObject.addProperty("DEPARTMENT_CODE", resultSet.getString("DEPARTMENT_CODE"));
                requestDetailsObject.addProperty("STATUS", resultSet.getString("STATUS"));
                requestDetailsObject.addProperty("WORK_REQUEST_TYPE", resultSet.getString("WORK_REQUEST_TYPE"));
                requestDetailsObject.addProperty("CREATED_FOR", resultSet.getString("CREATED_FOR_NAME"));
                requestDetailsObject.addProperty("ASSET_GROUP_ID", resultSet.getString("ASSET_GROUP"));
                requestDetailsObject.addProperty("MAINTENANCE_OBJECT_ID", resultSet.getString("MAINTENANCE_OBJECT_ID"));
                requestDetailsObject.addProperty("PRIORITY_ID", resultSet.getString("WORK_REQUEST_PRIORITY_ID"));
                requestDetailsObject.addProperty("DEPARTMENT_ID", resultSet.getString("DEPARTMENT_ID"));

                requestDetails.add(requestDetailsObject);

                JsonObject creationInformationObject = new JsonObject();
                creationInformationObject.addProperty("CREATED_BY", resultSet.getString("CREATED_BY_NAME"));
                creationInformationObject.addProperty("PHONE_NUMBER", resultSet.getString("PHONE_NUMBER"));
                creationInformationObject.addProperty("E-MAIL", resultSet.getString("E_MAIL"));
                creationInformationObject.addProperty("CONTACT_PREFERENCE", resultSet.getString("CONTACT_PREFERENCE"));
                creationInformationObject.addProperty("NOTIFY_USER", resultSet.getString("NOTIFY_ORIGINATOR"));
                creationInformationObject.addProperty("CREATION_DATE", resultSet.getString("CREATION_DATE"));
                creationInformation.add(creationInformationObject);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();

            JsonArray assetDetails = getAssetDetails(orgId,maintenanceObjectId);


            JsonArray requestDescription = getWorkRequestDescription(work_request_id);

            jsonResult.add("ASSET_DETAILS",assetDetails);
            jsonResult.add("REQUEST_DETAILS",requestDetails);
            jsonResult.add("REQUEST_DESCRIPTION",requestDescription);
            jsonResult.add("CREATION_INFORMATION",creationInformation);
            result = jsonResult.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Request Details Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }


    /*//need to connect with fauji for this
    public String getWorkRequestV1(String orgId,
                                 String requestNumber,
                                 String assetDepartment,
                                 String status,
                                 String assetNumber,
                                 Date creationDateFrom,
                                 Date creationDateTo,
                                 String creatorName) {

        String result = null;

        String statement = "SELECT * FROM (SELECT wewr.work_request_number, wewr.organization_id, wewr.work_request_id, wewr.description AS workdesc,\n" +
                "wewr.description As expworkdesc,(select distinct(meaning) from fnd_lookup_values  where lookup_type =  'XXX_STC_WO_WF_STATUS' and lookup_code = wewr.attribute2) as wf_status," +
                "wewr.attribute4,cii.instance_id, cii.serial_number asset_number,\n" +
                "(SELECT (select pho.phone_number from APPS.PER_PEOPLE_F hr, APPS.per_phones pho where fu.employee_id = hr.person_id and hr.person_id = pho.parent_id(+)  \n" +
                "AND pho.phone_type(+) = 'W1' AND pho.parent_table(+) = 'PER_ALL_PEOPLE_F' AND sysdate between pho.date_from(+)  \n" +
                "and nvl(pho.date_to(+), sysdate) and nvl(hr.effective_start_date, sysdate-1) < sysdate  \n" +
                "and nvl(hr.effective_END_date, sysdate+1) > sysdate) phone_number from APPS.fnd_user fu where fu.user_id = fu1.user_id) as phone_number,  \n" +
                "cii.inventory_item_id, msi.concatenated_segments, wewr.work_request_owning_dept, bd.department_code, mck.concatenated_segments AS category_name,  \n" +
                "(SELECT ewov.wip_entity_name FROM APPS.eam_work_orders_v ewov where ewov.wip_entity_id = wewr.wip_entity_id) as work_order, \n" +
                "el.location_codes AS eam_location, wewr.work_request_status_id, ml1.meaning AS statusvalue, wewr.work_request_priority_id, ml2.meaning AS priorityvalue,  \n" +
                "wewr.work_request_type_id, ml3.meaning AS requesttypevalue, wewr.expected_resolution_date, wewr.created_by, fu1.user_name, fu1.user_id,\n" +
                "(select papf.full_name from per_all_people_f papf where papf.person_id = fu1.employee_id and rownum = 1) as full_name, wewr.created_for,  \n" +
                "fu2.user_name AS created_for_name,(select papf.full_name from per_all_people_f papf where papf.person_id = fu2.employee_id and rownum = 1) as created_for_full_name, wewr.wip_entity_id, msi.eam_item_type,\n" +
                "wewr.creation_date, ml4.meaning AS Asset_Type, we.wip_entity_name, wewr.maintenance_object_id, cii.instance_number, cii.instance_description AS assetdesc, mp.maint_organization_id FROM APPS.wip_eam_work_requests wewr,  \n" +
                "APPS.mtl_system_items_b_kfv msi, APPS.csi_item_instances cii, APPS.eam_org_maint_defaults eomd, APPS.bom_departments bd, APPS.mtl_eam_locations el, APPS.mtl_categories_kfv mck, APPS.mfg_lookups ml1,  \n" +
                "APPS.mfg_lookups ml2, APPS.mfg_lookups ml3, APPS.mfg_lookups ml4, APPS.fnd_user fu1, APPS.fnd_user fu2, APPS.wip_entities we, APPS.mtl_parameters mp WHERE wewr.maintenance_object_id = cii.instance_id(+)  \n" +
                "AND cii.last_vld_organization_id = msi.organization_id(+) AND cii.inventory_item_id = msi.inventory_item_id(+) AND cii.instance_id = eomd.object_id (+) \n" +
                "AND eomd.object_type (+) = 50 AND eomd.organization_id (+) = :orgId1 AND wewr.work_request_owning_dept = bd.department_id(+) AND eomd.area_id = el.location_id (+) \n" +
                "AND cii.category_id = mck.category_id (+) AND ml1.lookup_type = 'WIP_EAM_WORK_REQ_STATUS' AND wewr.work_request_status_id = ml1.lookup_code\n" +
                "AND ml2.lookup_type (+) = 'WIP_EAM_ACTIVITY_PRIORITY' AND wewr.work_request_priority_id = ml2.lookup_code (+) AND ml3.lookup_type (+) = 'WIP_EAM_WORK_REQ_TYPE'\n" +
                "AND wewr.work_request_type_id = ml3.lookup_code(+) AND ml4.lookup_type (+) = 'MTL_EAM_ASSET_TYPE' AND msi.eam_item_type = ml4.lookup_code(+) AND fu1.user_id = wewr.created_by\n" +
                "AND fu2.user_id (+) = wewr.created_for AND wewr.wip_entity_id = we.wip_entity_id(+) AND cii.last_vld_organization_id = mp.organization_id\n" +
                "order by wewr.work_request_number) QRSLT WHERE organization_id = :orgId2 ";

        if (requestNumber != null && !requestNumber.isEmpty()) {
            statement += " AND (WORK_REQUEST_NUMBER LIKE :workRequestNumber OR WORK_REQUEST_NUMBER IS NULL)";
        }
        if (assetDepartment != null && !assetDepartment.isEmpty()) {
            statement += " AND (work_request_owning_dept LIKE :departmentCode OR work_request_owning_dept IS NULL)";
        }
        if (status != null && !status.isEmpty()) {
            statement += " AND (work_request_status_id LIKE :status OR work_request_status_id IS NULL)";
        }
        if (assetNumber != null && !assetNumber.isEmpty()) {
            statement += " AND (instance_id LIKE :assetNumber OR instance_id IS NULL)";
        }
        if (creationDateFrom != null) {
            statement += "AND (CREATION_DATE >= :creationDateFrom )";
        }
        if (creationDateTo != null) {
            statement += "AND (CREATION_DATE <= :creationDateTo )";
        }
        if (creatorName != null && !creatorName.isEmpty()) {
            statement += "AND USER_ID LIKE :creatorName";
        }
          statement += " AND ((( SELECT NVL(fnd_profile.value('XXX_SCM_EAM_SUPERVISOR'),'N')  FROM DUAL) = 'Y'  and (SELECT NVL(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')  FROM DUAL) = 'N' \n" +
                  "                        OR(  \n" +
                  "                        (SELECT NVL(fnd_profile.value('XXX_SCM_EAM_SUPERVISOR'),'N')  FROM DUAL) <> 'Y'\n" +
                  "                        and (SELECT NVL(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')  FROM DUAL) = 'N'     \n" +
                  "                        and qrslt.work_request_number in   \n" +
                  "                        (SELECT distinct work_request_number   \n" +
                  "                        FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V cii,   \n" +
                  "                        STC.XXX_STC_EAM_APPROVERS app,   \n" +
                  "                        XXX_STC_EAM_APPROVERS_DEPT_V dept,   \n" +
                  "                        FND_FLEX_VALUES_VL FV,   \n" +
                  "                        FND_FLEX_VALUE_SETS FVS,   \n" +
                  "                        FND_FLEX_VALUES_VL FV1,   \n" +
                  "                        FND_FLEX_VALUE_SETS FVS1,   BOM_DEPARTMENTS BD,   \n" +
                  "                        org_organization_definitions org,   \n" +
                  "                        wip_eam_work_requests wdj   \n" +
                  "                        WHERE FV.FLEX_VALUE_SET_ID = FVS.FLEX_VALUE_SET_ID   \n" +
                  "                        AND FVS.FLEX_VALUE_SET_NAME = 'XXSCM_Zone'    \n" +
                  "                        AND FV.FLEX_VALUE = cii.attribute1   \n" +
                  "                        AND dept.zone_id = FV.FLEX_VALUE_ID   \n" +
                  "                        AND dept.APPROVER_DEPT_ID = app.APPROVER_DEPT_ID   \n" +
                  "                        AND org.organization_id = dept.organization_id   \n" +
                  "                        AND org.organization_code = 'E00'   \n" +
                  "                        AND wdj.WORK_REQUEST_OWNING_DEPT = bd.department_id   \n" +
                  "                        AND bd.attribute1 = fv1.flex_value   \n" +
                  "                        AND fv1.flex_value_id = dept.department_id   \n" +
                  "                        AND fv1.flex_value_set_id = fvs1.flex_value_set_id   \n" +
                  "                        AND fvs1.flex_value_set_name = 'XXSCM_Departments'   \n" +
                  "                        AND dept.position_name in ('ABFM','BFM')  \n" +
                  "                        AND wdj.attribute15 is null \n" +
                  "                        AND wdj.work_request_number = qrslt.work_request_number   \n" +
                  "                        AND cii.instance_number   \n" +
                  "                        = (SELECT CASE WHEN INSTR (SUBSTR (C.INSTANCE_NUMBER, 8, 5),'-') BETWEEN 1 AND 4 THEN RTRIM (SUBSTR (C.INSTANCE_NUMBER, 1, 10),'-') WHEN INSTR (SUBSTR (C.INSTANCE_NUMBER, 8, 5),'-') > 4 THEN RTRIM (SUBSTR (C.INSTANCE_NUMBER, 1, 11), '-') ELSE C.INSTANCE_NUMBER END   \n" +
                  "                        FROM XX_MTL_EAM_ASSET_NUMBERS_ALL_V c   \n" +
                  "                        WHERE CURRENT_ORGANIZATION_ID=QRSLT.ORGANIZATION_ID AND c.serial_number = (SELECT asset_number   \n" +
                  "                        FROM wip_eam_work_requests wdj1   \n" +
                  "                        WHERE wdj1.work_request_number = qrslt.work_request_number and wdj1.ORGANIZATION_ID=QRSLT.ORGANIZATION_ID))   \n" +
                  "                        AND app.user_id = (SELECT to_char(fnd_profile.VALUE ('USER_ID')) FROM DUAL)  \n" +
                  "                        ))) \n" +
                  "                        OR ( \n" +
                  "                        (SELECT NVL(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')  FROM DUAL) = 'Y'\n" +
                  "                        and qrslt.work_request_number in   \n" +
                  "                        (SELECT distinct work_request_number   \n" +
                  "                        FROM wip_eam_work_requests wdj   \n" +
                  "                        where wdj.attribute15 is not null \n" +
                  "                        AND wdj.work_request_number = qrslt.work_request_number   \n" +
                  "                        ))\n" +
                  "OR ((SELECT NVL (fnd_profile.VALUE ('XXX_EAM_CONT_SUPERUSER'), 'N') FROM DUAL) = 'Y' OR \n" +
                  "(  (SELECT NVL(fnd_profile.value('XXX_EAM_IGATE_SUPERUSER'),'N')  FROM DUAL) = 'N' AND (SELECT NVL (fnd_profile.VALUE ('XXX_EAM_CONT_SUPERUSER'),'N') FROM DUAL) <> 'Y' \n" +
                  "AND qrslt.work_request_number IN (SELECT DISTINCT work_request_number FROM STC.XXX_STC_EAM_APPROVERS app, XXX_STC_EAM_APPROVERS_DEPT_V dept, \n" +
                  "org_organization_definitions org, wip_eam_work_requests wdj WHERE dept.APPROVER_DEPT_ID = app.APPROVER_DEPT_ID AND \n" +
                  "org.organization_id = dept.organization_id AND org.organization_code = 'E00' AND dept.position_name = 'CONT_SPEC_ENGINEER' \n" +
                  "AND DEPT.SUB_DEPARTMENT_ID = wdj.WORK_REQUEST_OWNING_DEPT AND wdj.work_request_number = qrslt.work_request_number  \n" +
                  "AND app.user_id = (SELECT to_char(fnd_profile.VALUE ('USER_ID')) FROM DUAL)))))";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            namedParameterStatement = new NamedParameterStatement(connection,statement);
            namedParameterStatement.setFetchSize(1000);
            namedParameterStatement.setString("orgId1", orgId);
            namedParameterStatement.setString("orgId2", orgId);

            if (requestNumber != null && !requestNumber.isEmpty()) {
                namedParameterStatement.setString("workRequestNumber", requestNumber);
            }
            if (assetDepartment != null && !assetDepartment.isEmpty()) {
                namedParameterStatement.setString("departmentCode", assetDepartment);
            }
            if (status != null && !status.isEmpty()) {
                namedParameterStatement.setString("status", status);
            }
            if (assetNumber != null && !assetNumber.isEmpty()) {
                namedParameterStatement.setString("assetNumber", assetNumber);
            }
            if (creationDateFrom != null) {
                namedParameterStatement.setDate("creationDateFrom", creationDateFrom);
            }
            if (creationDateTo != null) {
                namedParameterStatement.setDate("creationDateTo", creationDateTo);
            }
            if (creatorName != null && !creatorName.isEmpty()) {
                namedParameterStatement.setString("creatorName", creatorName);
            }

            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("WORK_REQUEST_ID",resultSet.getString("WORK_REQUEST_ID"));
                jsonObject.addProperty("WORK_REQUEST_NUMBER",resultSet.getString("WORK_REQUEST_NUMBER"));
                jsonObject.addProperty("WORK_DESCRIPTION",resultSet.getString("WORKDESC"));
                jsonObject.addProperty("ASSET_NUMBER",resultSet.getString("ASSET_NUMBER"));
                jsonObject.addProperty("ASSET_DESCRIPTION",resultSet.getString("ASSETDESC"));
                jsonObject.addProperty("WORK_REQUEST_OWNING_DEPT",resultSet.getString("WORK_REQUEST_OWNING_DEPT"));
                jsonObject.addProperty("WORK_ORDER_NUMBER", resultSet.getString("WORK_ORDER"));
                jsonObject.addProperty("ASSET_GROUP", resultSet.getString("CONCATENATED_SEGMENTS"));
                jsonObject.addProperty("STATUS", resultSet.getString("STATUSVALUE"));
                jsonObject.addProperty("CREATED_BY", resultSet.getString("USER_NAME"));
                jsonObject.addProperty("CREATED_BY_FULL_NAME", resultSet.getString("FULL_NAME"));
                jsonObject.addProperty("CREATED_BY_ID", resultSet.getString("CREATED_BY"));
                jsonObject.addProperty("MOBILE_NO", resultSet.getString("PHONE_NUMBER"));
                jsonObject.addProperty("CREATION_DATE", resultSet.getString("CREATION_DATE"));
                jsonObject.addProperty("WF_STATUS", resultSet.getString("WF_STATUS"));
                jsonObject.addProperty("WR_SOURCE", resultSet.getString("ATTRIBUTE4"));
                jsonObject.addProperty("CREATED_FOR", resultSet.getString("CREATED_FOR_NAME"));
                jsonObject.addProperty("CREATED_FOR_FULL_NAME", resultSet.getString("CREATED_FOR_FULL_NAME"));
                jsonObject.addProperty("CREATED_FOR_ID", resultSet.getString("CREATED_FOR"));
                jsonObject.addProperty("PRIORITY_ID", resultSet.getString("WORK_REQUEST_PRIORITY_ID"));
                jsonObject.addProperty("PRIORITY",resultSet.getString("PRIORITYVALUE"));

                //jsonObject.addProperty("ASSET_TYPE",resultSet.getString("ASSET_TYPE"));
                //jsonObject.addProperty("REQUEST_BY_DATE", resultSet.getString("EXPECTED_RESOLUTION_DATE"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_REQUEST",jsonArray);
            result = jsonResult.toString();

            connection.close();
            namedParameterStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Request Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }*/

    public JsonArray getAssetDetails(String orgId,
                                     String maintenanceObjectId) {

        JsonArray jsonArray = new JsonArray();

        String statement = "SELECT * FROM (SELECT cii.serial_number, cii.instance_description descriptive_text,cii.INSTANCE_NUMBER, mp.maint_organization_id current_organization_id, msn.gen_object_id, \n" +
                "msi.concatenated_segments, msi.description as asset_group_description, cii.network_asset_flag, ml2.meaning as asset_criticality, eomd.owning_department_id,  \n" +
                "bd.department_code owning_department, el.location_codes area, eomd.area_id, cii.inventory_item_id, cii.category_id, mck.concatenated_segments category_name,  \n" +
                "nvl(cii.maintainable_flag,'Y'), msi.eam_item_type, ml1.lookup_type asset_type_code, ml1.meaning asset_type, cii.instance_id maintenance_object_id,  \n" +
                "cii.instance_number Asset_Number FROM APPS.mtl_serial_numbers msn, APPS.mtl_system_items_kfv msi, APPS.mtl_eam_locations el, APPS.bom_departments bd, APPS.mtl_categories_kfv mck,  \n" +
                "APPS.mfg_lookups ml1, APPS.mfg_lookups ml2, APPS.csi_item_instances cii, (SELECT * FROM APPS.eam_org_maint_defaults WHERE organization_id = ?) eomd,  \n" +
                "APPS.mtl_parameters mp WHERE cii.last_vld_organization_id = msi.organization_id AND cii.inventory_item_id = msi.inventory_item_id AND cii.inventory_item_id = msn.inventory_item_id  \n" +
                "AND cii.serial_number = msn.serial_number AND cii.last_vld_organization_id = mp.organization_id AND eomd.object_type (+) = 50 AND eomd.object_id (+) = cii.instance_id  \n" +
                "AND eomd.owning_department_id = bd.department_id(+) AND cii.asset_criticality_code = ml2.lookup_code(+) AND 'MTL_EAM_ASSET_CRITICALITY' = ml2.lookup_type(+)  \n" +
                "AND ml1.lookup_type = 'MTL_EAM_ASSET_TYPE' AND ml1.lookup_code = msi.eam_item_type AND msi.serial_number_control_code<>1 AND eomd.area_ID = el.location_id(+)  \n" +
                "AND cii.category_id = mck.category_id(+) AND ml2.enabled_flag(+)='Y' AND sysdate between nvl(ml2.start_date_active, sysdate - 1)  \n" +
                "AND nvl(ml2.end_date_active,sysdate + 1)) QRSLT WHERE (maintenance_object_id = ?) ORDER BY serial_number";


        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setString(1, orgId);
            preparedStatement.setString(2, maintenanceObjectId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ASSET_NUMBER",resultSet.getString(1));
                jsonObject.addProperty("DESCRIPTION", resultSet.getString(2));
                jsonObject.addProperty("ASSET_INSTANCE", resultSet.getString(3));
                jsonObject.addProperty("ASSET_GROUP", resultSet.getString(6));
                jsonArray.add(jsonObject);
            }

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Asset Details Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return jsonArray;
    }

    public JsonArray getWorkRequestDescription(int workRequestId) {

        JsonArray jsonArray = new JsonArray();

        String statement = "SELECT * FROM (select work_request_id, notes from APPS.wip_eam_work_req_notes order by work_request_note_id) QRSLT WHERE (work_request_id= ?) ";

        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, workRequestId);

            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            String descriptionHistory = "";
            String description = "";

            int i = 0;

            while (resultSet.next()){
                if (i == 1) {
                    description = resultSet.getString(2);
                }
                descriptionHistory = descriptionHistory + " " + resultSet.getString(2) /*+ "\n"*/ ;
                i++;
            }

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DESCRIPTION", description);
            jsonObject.addProperty("DESCRIPTION_HISTORY", descriptionHistory);

            jsonArray.add(jsonObject);

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work Request Description Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return jsonArray;
    }

    public String createWorkRequest(int orgId,
                                    int assetGroupId,
                                    String assetNumber,
                                    int priorityId,
                                    String requestByDate,
                                    String requestLog,
                                    int owningDeptId,
                                    int userId,
                                    int workRequestTypeId,
                                    int maintenanceObjectId,
                                    int createdBy,
                                    int createdFor,
                                    String phoneNumber,
                                    String email,
                                    int contactPreference,
                                    String wfStatus,
                                    String wrSource,
                                    int notifyOriginator) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_work_request/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/create_work_request/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_API_VERSION", "1");
            param.put("P_ORG_ID", String.valueOf(orgId));
            param.put("P_ASSET_GROUP_ID", String.valueOf(assetGroupId));
            param.put("P_ASSET_NUMBER", assetNumber);
            param.put("P_PRIORITY_ID", String.valueOf(priorityId));
            param.put("P_OWNING_DEPT_ID", String.valueOf(owningDeptId));
            param.put("P_REQUEST_BY_DATE", requestByDate);
            param.put("P_WORK_REQUEST_TYPE_ID", String.valueOf(workRequestTypeId));
            param.put("P_WORK_REQUEST_CREATED_BY", String.valueOf(createdBy));
            param.put("P_MAINTENANCE_OBJECT_TYPE", "3");
            param.put("P_MAINTENANCE_OBJECT_ID", String.valueOf(maintenanceObjectId));
            param.put("P_CREATED_FOR", String.valueOf(createdFor));
            param.put("P_PHONE_NUMBER", phoneNumber);
            param.put("P_EMAIL", email);
            param.put("P_CONTACT_PREFERENCE", String.valueOf(contactPreference));
            param.put("P_NOTIFY_ORIGINATOR", String.valueOf(notifyOriginator));
            param.put("P_REQUEST_LOG", requestLog);
            param.put("P_ATTRIBUTE2", wfStatus);
            param.put("P_ATTRIBUTE4", wrSource);
            param.put("P_USER_ID", String.valueOf(userId));

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            String jsonData=  handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            JSONObject obj = new JSONObject(jsonData).getJSONObject("env:Envelope");
            System.out.println(obj.toString());
            result =  jsonData;
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Work Request Error",e);
        }
        return result;
    }

    public String updateWorkRequest_Old(int orgId,
                                    int assetGroupId,
                                    String assetNumber,
                                    int priorityId,
                                    String requestByDate,
                                    String requestLog,
                                    int owningDeptId,
                                    int userId,
                                    int workRequestTypeId,
                                    int requestId,
                                    int statusId,
                                    int createdFor,
                                    String phoneNumber,
                                    String email,
                                    int contactPreference,
                                    String wfStatus,
                                    String wrSource,
                                    int notifyOriginator) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/update_work_request/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_eam_custom_api2/update_work_request/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_API_VERSION", "1");
            param.put("P_ORG_ID", String.valueOf(orgId));
            param.put("P_ASSET_GROUP_ID", String.valueOf(assetGroupId));
            param.put("P_ASSET_NUMBER", assetNumber);
            param.put("P_REQUEST_ID", String.valueOf(requestId));
            param.put("P_STATUS_ID", String.valueOf(statusId));
            param.put("P_PRIORITY_ID", String.valueOf(priorityId));
            param.put("P_REQUEST_BY_DATE", requestByDate);
            param.put("P_REQUEST_LOG", requestLog);
            param.put("P_WORK_REQUEST_TYPE_ID", String.valueOf(workRequestTypeId));
            param.put("P_OWNING_DEPT_ID", String.valueOf(owningDeptId));
            param.put("P_USER_ID", String.valueOf(userId));
            param.put("P_CREATED_FOR", String.valueOf(createdFor));
            param.put("P_PHONE_NUMBER", phoneNumber);
            param.put("P_EMAIL", email);
            param.put("P_CONTACT_PREFERENCE", String.valueOf(contactPreference));
            param.put("P_ATTRIBUTE2", wfStatus);
            param.put("P_ATTRIBUTE4", wrSource);
            param.put("P_NOTIFY_ORIGINATOR", String.valueOf(notifyOriginator));

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Update Work Request Error",e);
        }
        return result;
    }

    public String sendWorkRequestForApproval(int workRequestId,
                                             int userId,
                                             int respId,
                                             int respAppId) {
        String result = "";
        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wr_wf_soa_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_soa_pkg/oaf_wf_start/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_soa_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_soa_pkg/oaf_wf_start/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_PROCESS", "XXX_PHASE1_PRC");
            param.put("P_TRANSACTION_ID", String.valueOf(workRequestId));
            param.put("P_REQUESTOR_ID", String.valueOf(userId));
            param.put("P_RESP_ID", String.valueOf(respId));
            param.put("P_RESP_APPL_ID", String.valueOf(respAppId));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("send Work Request for approval for Error",e);
        }
        return result;
    }

    void closeAllConnection(Connection connection,
                            PreparedStatement preparedStatement,
                            ResultSet resultSet,
                            CallableStatement callableStatement,
                            NamedParameterStatement namedParameterStatement) {

        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

        if (namedParameterStatement != null) {
            try {
                namedParameterStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing namedParameterStatement Error", e);
            }
        }
    }

    public String getWorkFlowStatus(int workRequestId) {

        String result = null;

        String statement = "select distinct(lookup_code) as lookup_code, meaning\n" +
                "from fnd_lookup_values vl\n" +
                "                where lookup_type =  'XXX_STC_WO_WF_STATUS' and lookup_code\n" +
                "                IN (select wewr.attribute2 from APPS.wip_eam_work_requests wewr  where wewr.work_request_id = ?)";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            preparedStatement.setInt(1, workRequestId);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("STATUS",resultSet.getString(1));
                jsonObject.addProperty("MEANING",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("WORK_REQUEST_WORK_FLOW_STATUS",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Work flow status Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    //used commonly for workorder and work request work flow status
    public String getAllWorkFlowStatus() {

        String result = null;

        String statement = "select distinct(lookup_code) as lookup_code, MEANING as wrokflowdesc\n" +
                "from fnd_lookup_values vl\n" +
                "where lookup_type =  'XXX_STC_WO_WF_STATUS'";
        try {
            connection  = pool.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setFetchSize(100);
            resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(100);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("statusCode",resultSet.getString(1));
                jsonObject.addProperty("statusDescription",resultSet.getString(2));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("workFlowStatuses",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get All Work flow status Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String isHqcOrgWorkRequest(String organizationID){
        String result = "";

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_wip_hq_wr_wf_soa_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_soa_pkg/is_hqc_org/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_soa_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_wip_hq_wr_wf_soa_pkg/is_hqc_org/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_ORG_ID",organizationID);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("hq organization check Error",e);
        }
        return result;
    }

    public String updateWorkRequest(String 	P_WORK_REQUEST_ID   // Enabled
            ,String 	P_LAST_UPDATE_DATE
            ,String 	P_EXPECTED_RESOLUTION_DATE
            ,String 	P_LAST_UPDATED_BY
            ,String 	P_ASSET_NUMBER          // Enabled
            ,String 	P_ASSET_GROUP           // Enabled
            ,String 	P_WORK_REQUEST_STATUS_ID
            ,String 	P_WORK_REQUEST_PRIORITY_ID    //Enabled
            ,String 	P_WORK_REQUEST_OWNING_DEPT   //Enabled
            ,String 	P_ATTRIBUTE1
            ,String 	P_ATTRIBUTE2
            ,String 	P_ATTRIBUTE3
            ,String 	P_ATTRIBUTE4
            ,String 	P_ATTRIBUTE5
            ,String 	P_ATTRIBUTE6
            ,String 	P_ATTRIBUTE7
            ,String 	P_ATTRIBUTE8
            ,String 	P_ATTRIBUTE9
            ,String 	P_ATTRIBUTE10
            ,String 	P_ATTRIBUTE11
            ,String 	P_ATTRIBUTE12
            ,String 	P_ATTRIBUTE13
            ,String 	P_ATTRIBUTE14
            ,String 	P_ATTRIBUTE15
            ,String 	P_DESCRIPTION                 // Enabled
            ,String 	P_WORK_REQUEST_TYPE_ID
            ,String 	P_WORK_REQUEST_CREATED_BY
            ,String 	P_CREATED_FOR                  // Enabled
            ,String 	P_PHONE_NUMBER                 // Enabled
            ,String 	P_E_MAIL
            ,String 	P_CONTACT_PREFERENCE           // Enabled
            ,String 	P_MAINTENANCE_OBJECT_ID) {

        String result = null;

        try{

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxxstc_eam_update_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/xx_eam_update_wr/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxxstc_eam_update_ws/xx_eam_update_wr/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_WORK_REQUEST_ID",String.valueOf(P_WORK_REQUEST_ID));
            //param.put("P_LAST_UPDATE_DATE",String.valueOf(P_LAST_UPDATE_DATE));
            param.put("P_EXPECTED_RESOLUTION_DATE",String.valueOf(P_EXPECTED_RESOLUTION_DATE));
            //param.put("P_LAST_UPDATED_BY",String.valueOf(P_LAST_UPDATED_BY));
            param.put("P_ASSET_NUMBER",String.valueOf(P_ASSET_NUMBER));
            param.put("P_ASSET_GROUP",String.valueOf(P_ASSET_GROUP));
            //param.put("P_WORK_REQUEST_STATUS_ID",String.valueOf(P_WORK_REQUEST_STATUS_ID));
            param.put("P_WORK_REQUEST_PRIORITY_ID",String.valueOf(P_WORK_REQUEST_PRIORITY_ID));
            param.put("P_WORK_REQUEST_OWNING_DEPT",String.valueOf(P_WORK_REQUEST_OWNING_DEPT));
            /*param.put("P_ATTRIBUTE1",String.valueOf(P_ATTRIBUTE1));
            param.put("P_ATTRIBUTE2",String.valueOf(P_ATTRIBUTE2));
            param.put("P_ATTRIBUTE3",String.valueOf(P_ATTRIBUTE3));
            param.put("P_ATTRIBUTE4",String.valueOf(P_ATTRIBUTE4));
            param.put("P_ATTRIBUTE5",String.valueOf(P_ATTRIBUTE5));
            param.put("P_ATTRIBUTE6",String.valueOf(P_ATTRIBUTE6));
            param.put("P_ATTRIBUTE7",String.valueOf(P_ATTRIBUTE7));
            param.put("P_ATTRIBUTE8",String.valueOf(P_ATTRIBUTE8));
            param.put("P_ATTRIBUTE9",String.valueOf(P_ATTRIBUTE9));
            param.put("P_ATTRIBUTE10",String.valueOf(P_ATTRIBUTE10));
            param.put("P_ATTRIBUTE11",String.valueOf(P_ATTRIBUTE11));
            param.put("P_ATTRIBUTE12",String.valueOf(P_ATTRIBUTE12));
            param.put("P_ATTRIBUTE13",String.valueOf(P_ATTRIBUTE13));
            param.put("P_ATTRIBUTE14",String.valueOf(P_ATTRIBUTE14));
            param.put("P_ATTRIBUTE15",String.valueOf(P_ATTRIBUTE15)); */
            param.put("P_DESCRIPTION",String.valueOf(P_DESCRIPTION));
            //param.put("P_WORK_REQUEST_TYPE_ID",String.valueOf(P_WORK_REQUEST_TYPE_ID));
            //param.put("P_WORK_REQUEST_CREATED_BY",String.valueOf(P_WORK_REQUEST_CREATED_BY));
            param.put("P_CREATED_FOR",String.valueOf(P_CREATED_FOR));
            param.put("P_PHONE_NUMBER",String.valueOf(P_PHONE_NUMBER));
            //param.put("P_E_MAIL",String.valueOf(P_E_MAIL));
            param.put("P_CONTACT_PREFERENCE",String.valueOf(P_CONTACT_PREFERENCE));
            //param.put("P_MAINTENANCE_OBJECT_ID",String.valueOf(P_MAINTENANCE_OBJECT_ID));

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Update Work Request Error",e);
        }
        return result;
    }

}
