/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;
import static com.stc.controller.MainController.logger;
import static com.stc.controller.MainController.pool;
import static com.stc.controller.NotificationController.logger;
import static com.stc.controller.NotificationController.pool;
import static com.stc.controller.StcWmsMainController.logger;
import static com.stc.controller.StcWmsMainController.pool;
import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author lenovo
 */
public class WmsNotificationController {
      
    final static Logger logger = Logger.getLogger(NotificationController.class);

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public NamedParameterStatement namedParameterStatement;
    public PreparedStatement preparedStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;
    
    
    
    //********************************NOtification Approve request********************************//
    
    public String ApproveAction(String requestId ,String comment){
        JSONObject jsonObj;
       String result=null;

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/approve_action/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/approve_action/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_REQUEST_ID",requestId);
            param.put("P_NOTE",comment);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Validate Login Error",e);
        }
        return result;
    }
    
    
    
    //********************************NOtification Reject request********************************//
    
    public String RejectAction(String requestId,String comment){
        JSONObject jsonObj;
       String result=null;

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/reject_action/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/reject_action/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_REQUEST_ID",requestId);
            param.put("P_NOTE",comment);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Validate Login Error",e);
        }
        return result;
    }
    
      //********************************NOtification ReAssign request********************************//
    
    public String ReAssignAction(String requestid,String comment,String username){
    String result=null;
    try{
    String soapEndpointUrl=pool.getProperties().getProperty("soagatewayurl")+"/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
    String soapAction="http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/reassign_action/";
    SoapEnvelopeObject seo=new SoapEnvelopeObject();
    seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
    seo.setUsername(pool.getProperties().getProperty("grantUserId"));
    seo.setPassword(pool.getProperties().getProperty("grantPassword"));
    seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/reassign_action/");
    Map<String,Object> param=new HashMap<String,Object>();
    param.put("P_REQUEST_ID",requestid);
    param.put("P_NOTE",comment);
    param.put("P_USER_NAME",username);
    
   seo.setParameter(param);
 SoapHandler handler=new SoapHandler();
 result=handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
 
    }
    catch(Exception e){
    e.printStackTrace();
    logger.error("Validate Login Error",e);
    
    }
    return result;
    }
    //********************************NOtification users reassign********************************//
    public String getUsersReassign() {

        String result = null;

          String statement = "select user_name from xxwms_users_lov";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "poNumber");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get users erroe",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
    
    
    
    
    //********************************NOtification Cancel request********************************//
    
    public String CancelRequest(String requestId){
        JSONObject jsonObj;
       String result=null;

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/cancel_request/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/cancel_request/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_REQUEST_ID",requestId);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Cancel Request Error",e);
        }
        return result;
    }
    
    
   //************************* get notification******************************************//
    
      public String getMoreDetailsOfNotification(String requestId) {

        String result = null;

          String statement = " SELECT FROM_USER,\n"
                  + "       TO_USER,\n"
                  + "       SENT,\n"
                  + "       NOTIFICATION_ID,\n"
                  + "       SUBJECT,\n"
                  + "       ITEM_CODE,\n"
                  + "       ITEM_DESCRIPTION,\n"
                  + "       QUANTITY,\n"
                  + "       UOM,\n"
                  + "       REQUEST_ID,\n"
                  + "       PO_NUM,\n"
                  + "       ORG_ID,\n"
                  + "       VENDOR_NAME\n"
                  + "  FROM xxwms_ntf_details_v\n"
                  + " WHERE request_id = '" + requestId + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Notification");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Notification Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
     
      
       public String getAllNotificationForUserLogin(String userName) {

           String result = null;

           String statement = "select REQUEST_ID,\n"
                   + "       FROM_USER,\n"
                   + "       TYPE,\n"
                   + "       MESSAGE,\n"
                   + "       SUBJECT,\n"
                   + "       SENT,\n"
                   + "       USER_NAME,\n"
                   + "       PO_NUM,\n" 
                   + "       VENDOR_NAME\n"
                   + "  from xxwms_ntf_summary_v\n"
                   + "  where user_name = '" + userName + "'";
           try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Notification");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Notification Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
      //*************************get Notification Count************************************//
           public String NotificationCount(String userName) {

        String result = null;

               String statement = "select notification_count, user_name\n"
                       + "  from xxwms_pending_ntf_count\n"
                       + " where user_name = '" + userName + "'";
             try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxwms_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/inv/soaprovider/plsql/xxwms_pkg/execute_query/");

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("SQL_QUERY", statement);
            seo.setParameter(param);


            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "notificationCount");
            JSONObject jsonObject = new JSONObject(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get notificationCount Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement);
        }
        return result;
    }
         
       
       
     
      public static void main(String[] args){
      WmsNotificationController obj=new WmsNotificationController();
        System.out.println(obj.NotificationCount("SYSADMIN")); 
      } 
       void closeAllConnection(Connection connection,
                            PreparedStatement preparedStatement,
                            ResultSet resultSet,
                            CallableStatement callableStatement) {

        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

    }      
    
}
