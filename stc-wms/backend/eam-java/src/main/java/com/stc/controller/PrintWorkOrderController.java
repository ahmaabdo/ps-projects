package com.stc.controller;

import com.google.gson.JsonObject;
import static com.stc.controller.PublicController.pool;
import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: Intellinum-PC11
 * Date: 13/1/19
 * Time: 1:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class PrintWorkOrderController extends PublicController {

    final static Logger logger = Logger.getLogger(WorkOrderController.class);

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public PreparedStatement preparedStatement;
    public NamedParameterStatement namedParameterStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;

//    public String getReport(String orgId,String trxId, String isHQC){
//        String result=null;
//        JsonObject jsonResult = new JsonObject();
//        //WorkOrderController workOrderController=new WorkOrderController();
//        try{
//        connection=pool.getConnection();
//        //String isHQC=workOrderController.isHqcOrg(orgId);
//        System.out.println("isHQCisHQCisHQCisHQCisHQC "+isHQC);
//        if ("Y".equals(isHQC)) {
//            callableStatement=connection.prepareCall("Begin XXX_EAM_RUN_REQUEST_PKG.run_report1(:1,:2,:3,:4,:5,:6); --:7:= XXX_EAM_RUN_REQUEST_PKG.return_g_url; END;");
//            String application = "EAM";
//            String templateNmae = "XXEAMHQWORKORDER";
//            String language = "AR";
//            String outputType = "PDF";
//            String url = null;
//             
//            callableStatement.setString(1, application);
//            callableStatement.setString(2, templateNmae);
//            callableStatement.setString(3, language);
//            callableStatement.setString(4, outputType);
//            callableStatement.setString(5, trxId);
//            callableStatement.setString(6, orgId);
////            callableStatement.registerOutParameter(7, Types.VARCHAR);
//            System.out.println("callableStatement : \n"+callableStatement);
//            callableStatement.execute();
////            url = callableStatement.getString(7);
////            jsonResult.addProperty("FileUrl",url);
////            result=jsonResult.toString();
//        }else{
//            callableStatement=connection.prepareCall("Begin  XXX_EAM_RUN_REQUEST_PKG.run_report1(:1,:2,:3,:4,:5,:6); :7:= XXX_EAM_RUN_REQUEST_PKG.return_g_url; End;");
//            String application = "EAM";
//            String templateNmae = "XXEAMWORKORDER";
//            String language = "AR";
//            String outputType = "PDF";
//            String url = null;
//
//            callableStatement.setString(1, application);
//            callableStatement.setString(2, templateNmae);
//            callableStatement.setString(3, language);
//            callableStatement.setString(4, outputType);
//            callableStatement.setString(5, trxId);
//            callableStatement.setString(6, orgId);
//            callableStatement.registerOutParameter(7, Types.VARCHAR);
//            callableStatement.execute();
//            url = callableStatement.getString(7);
//            jsonResult.addProperty("FileUrl",url);
//            result=jsonResult.toString();
//        }
//        }catch (Exception e){
//             System.out.println(e.getMessage());
//        }finally {
//            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
//        }
//        return result;
//    }
    public String getReport(String orgId,String trxId, String isHQC){
        String result=null;
        try{

            System.out.println("isHQCisHQCisHQCisHQCisHQC "+isHQC);
            String application = "EAM";
            String templateNmae = "XXEAMHQWORKORDER";
            String language = "AR";
            String outputType = "PDF";
           
            
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxx_eam_run_request_pkg_ws/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxx_eam_run_request_pkg_ws/RUN_REPORT1/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxx_eam_run_request_pkg_ws/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxx_eam_run_request_pkg_ws/RUN_REPORT1/");
            
            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_TEMPLATE_APPL_NAME", application);
            param.put("P_TEMPLATE_CODE", templateNmae);
            param.put("P_TEMPLATE_LANGUAGE", language);
            param.put("P_OUTPUT_FORMAT", outputType);
            param.put("P_ARGUMENT1", trxId);
            param.put("P_ARGUMENT2", orgId);

            seo.setParameter(param);

           


            SoapHandler handler = new SoapHandler();
            String jsonData=  handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
            JSONObject obj = new JSONObject(jsonData).getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters");
            
            System.out.println(obj.toString());
            result =  obj.toString();

        }catch (Exception e){
             System.out.println(e.getMessage());
        }
        return result;
    }

    void closeAllConnection(Connection connection,
                            PreparedStatement preparedStatement,
                            ResultSet resultSet,
                            CallableStatement callableStatement,
                            NamedParameterStatement namedParameterStatement) {

        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

        if (namedParameterStatement != null) {
            try {
                namedParameterStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing namedParameterStatement Error", e);
            }
        }
    }
    
//    public static void main(String args[]){
//        PrintWorkOrderController a = new PrintWorkOrderController();
//        a.getReport("9401", "399928", "Y");
//    }
}
