/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stc.controller.PurchasingController;
import com.stc.objects.PurchasingBean;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author lenovo
 */
@Path("/purchasing")
public class PurchasingServices {

    @POST
    @Path("createPurchasing")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})

    public Response AddReceipt(String receiptbean) {
  
        System.out.println(receiptbean);
        String result = null;
        PurchasingController puchasecontroller = new PurchasingController();
        try {
            ObjectMapper mapper = new ObjectMapper();

            PurchasingBean bean
                    = mapper.readValue(receiptbean, PurchasingBean.class);

            result = puchasecontroller.AddReceiptMethod(bean);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }


    
    //******************************** validate QR ***********************************//
    @GET
    @Path("ValidateQR")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})

    public Response ValidateQR(@QueryParam("lpn") String lpn ,@QueryParam("transaction_Type") String transaction_Type,@QueryParam("lang") String lang ) {
        String result = null;
        PurchasingController puchasecontroller = new PurchasingController();
        try {
          

            result = puchasecontroller.QrValidation(lpn,transaction_Type,lang);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    //*******************************validate create request *********************************************//
    @POST
    @Path("ValidateCreate")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})

    public Response ValidateCreateRequest(String beanObject) {
  
        String result = null;
        PurchasingController puchasecontroller = new PurchasingController();
        try {
            ObjectMapper mapper = new ObjectMapper();

            PurchasingBean bean
                    = mapper.readValue(beanObject, PurchasingBean.class);

            result = puchasecontroller.ValidateRequest(bean);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    
}
