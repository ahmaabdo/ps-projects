package com.stc.objects;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 02/01/18
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
public class ItemDetails {
    String itemType;
    String itemKey;
    int actId;

    public ItemDetails(String itemType, String itemKey, int actId) {
        this.itemType = itemType;
        this.itemKey = itemKey;
        this.actId = actId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemKey() {
        return itemKey;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public int getActId() {
        return actId;
    }

    public void setActId(int actId) {
        this.actId = actId;
    }
}
