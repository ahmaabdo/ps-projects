package com.stc.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.stc.db.ConnectionPool;
import com.stc.db.NamedParameterStatement;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.soap.SoapHandler;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 27/12/17
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */
public class MainController {

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public Connection connection;
    public PreparedStatement preparedStatement;
    public NamedParameterStatement namedParameterStatement;
    public CallableStatement callableStatement;
    public ResultSet resultSet;

    final static Logger logger = Logger.getLogger(MainController.class);

    public String validateLogin(String username,
                                String password){
        String result = "";

        try{
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/fnd_web_sec/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/fnd_web_sec/validate_login/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/fnd_web_sec/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/fnd/soaprovider/plsql/fnd_web_sec/validate_login/");

            Map<String,Object> param = new HashMap<String,Object>();
            param.put("P_USER",username);
            param.put("P_PWD", password);

            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebService(soapEndpointUrl,soapAction,seo);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Validate Login Error",e);
        }
        return result;
    }
    
     //Get Security Profile Details

    public  String getSecurityProfiles(int respId){

        String result=null;

        String statement="SELECT LV1.LOOKUP_CODE FUNCTION_NUM,\n" +

                "       LV1.ATTRIBUTE1 FUNCTION_NAME\n" +

                "  FROM FND_LOOKUP_VALUES LV,FND_LOOKUP_VALUES LV1,fnd_responsibility_tl RTL\n" +

                "WHERE     LV.LOOKUP_TYPE ='RESPONSIBILITY_FUNCTION_ASSIGN'\n" +

                "       AND LV.LANGUAGE = 'US'\n" +

                "       AND LV1.LOOKUP_TYPE = LV.ATTRIBUTE1\n" +

                "       AND LV1.LANGUAGE = 'US'\n" +

                "       AND LV.MEANING=RTL.responsibility_name\n" +

                "       AND RTL.responsibility_id=?";

        try{

            connection =pool.getConnection();

            preparedStatement=connection.prepareStatement(statement);

            preparedStatement.setInt(1,respId);

 

            resultSet=preparedStatement.executeQuery();

 

            JsonObject jsonResult=new JsonObject();

            JsonArray jsonArray=new JsonArray();

 

            while (resultSet.next()){

                JsonObject jsonObject = new JsonObject();

                jsonObject.addProperty("FUNCTION_ID",resultSet.getString(1));

                jsonObject.addProperty("FUNCTION_NAME",resultSet.getString(2));

                jsonArray.add(jsonObject);

            }

 

            jsonResult.add("PROFILES",jsonArray);

            result = jsonResult.toString();

 

            connection.close();

            preparedStatement.close();

            resultSet.close();

        }catch(Exception ex){

            ex.printStackTrace();

            logger.error("Get Profiles Error",ex);

        }finally

        {

            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);

        }

                return result;

 

        }

    //to do with language
    public String getLoginDetails(String userName, String userId) {
        System.out.println(userName);
        System.out.println(userId);
        String result = null;
        
        String qUserName;
        String quserId;

        if (userId.isEmpty()) {
            quserId = "";
            qUserName = userName;
        } else {
            quserId = userId;
            qUserName = "";
        }
        
        String statement =  "SELECT fu.user_id, fu.user_name, (SELECT nvl(hr.full_name,fu.user_name) FROM apps.per_people_f hr WHERE fu.employee_id = hr.person_id \n" +
                "AND NVL (hr.effective_start_date, SYSDATE - 1) < SYSDATE AND NVL (hr.effective_end_date, SYSDATE + 1) > SYSDATE) full_name, \n" +
                "(SELECT nvl(hr.email_address,FU.EMAIL_ADDRESS) FROM apps.per_people_f hr WHERE fu.employee_id = hr.person_id AND NVL (hr.effective_start_date, SYSDATE - 1) < SYSDATE \n" +
                "AND NVL (hr.effective_end_date, SYSDATE + 1) > SYSDATE) email_address, (SELECT pho.phone_number FROM apps.per_people_f hr, apps.per_phones pho \n" +
                "WHERE fu.employee_id = hr.person_id AND hr.person_id = pho.parent_id(+) AND pho.phone_type(+) = 'W1' AND pho.parent_table(+) = 'PER_ALL_PEOPLE_F' \n" +
                "AND SYSDATE BETWEEN pho.date_from(+) AND NVL (pho.date_to(+), SYSDATE) AND NVL (hr.effective_start_date, SYSDATE - 1) < SYSDATE AND NVL (hr.effective_end_date, SYSDATE + 1) > SYSDATE \n" +
                "AND ROWNUM=1) phone_number FROM apps.fnd_user fu WHERE (UPPER (fu.user_name) = ? OR fu.user_id = ?)";
        
//        
        try {

            connection  = pool.getConnection();
            
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, qUserName.toUpperCase());            
            preparedStatement.setString(2, quserId);
            
            System.out.println("///////////////"+statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            if (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("USER_ID", resultSet.getString("USER_ID"));
                jsonObject.addProperty("USER_NAME",resultSet.getString("USER_NAME"));
                jsonObject.addProperty("FULL_NAME",resultSet.getString("FULL_NAME"));
                jsonObject.addProperty("EMAIL_ADDRESS",resultSet.getString("EMAIL_ADDRESS"));
                jsonObject.addProperty("PHONE_NUMBER",resultSet.getString("PHONE_NUMBER"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("LOGIN_DETAILS", jsonArray);
            result = jsonResult.toString();


            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get User Details Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }
    
    //to do with language
    public String validateUserOnEBS(String emailId) {
        System.out.println(emailId);
        String result = null;
        
        String statement =  "SELECT USER_NAME, USER_ID FROM apps.FND_USER WHERE UPPER(EMAIL_ADDRESS) =UPPER(:email) AND (SYSDATE BETWEEN start_date AND end_date OR end_date is null)";
                
        try {

            connection  = pool.getConnection();
            
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, emailId.toUpperCase());
            
            System.out.println("///////////////"+statement);
            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            if (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("USER_NAME", resultSet.getString("USER_NAME"));
                jsonObject.addProperty("USER_ID", resultSet.getString("USER_ID"));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("EBS_USER", jsonArray);
            result = jsonResult.toString();


            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get EBS User Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    //language done
    public String getResponsibility(String userId, String language) {

        String result = null;

        String statement = "SELECT DISTINCT FRT.RESPONSIBILITY_NAME, FURG.RESPONSIBILITY_ID, FURG.RESPONSIBILITY_APPLICATION_ID FROM APPS.FND_USER FU, APPS.FND_USER_RESP_GROUPS FURG, APPS.FND_RESPONSIBILITY_TL FRT \n" +
                "WHERE FU.USER_ID = FURG.USER_ID AND FURG.RESPONSIBILITY_ID = FRT.RESPONSIBILITY_ID AND FRT.APPLICATION_ID = 426 AND FRT.LANGUAGE = ? AND FU.USER_ID = ? ORDER BY FRT.RESPONSIBILITY_NAME";
        try {
            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, language);
            preparedStatement.setString(2, userId);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("RESPONSIBILITY_NAME",resultSet.getString(1));
                jsonObject.addProperty("RESPONSIBILITY_ID",resultSet.getString(2));
                jsonObject.addProperty("RESPONSIBILITY_APPLICATION_ID",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("RESPONSIBILITIES", jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Responsibility Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getOrganization(String respId,
                                  String respAppId) {

        String result = null;

        String statement = "SELECT * FROM (select oav.organization_id org_id, oav.organization_code org_code, oav.organization_name org_name, oav.responsibility_id resp_id, \n" +
                "oav.resp_application_id resp_app_id from APPS.org_access_view oav, APPS.mtl_parameters mp, APPS.wip_eam_parameters wep where oav.organization_id = mp.organization_id \n" +
                "and NVL(mp.eam_enabled_flag,'N') = 'Y' and oav.organization_id = wep.organization_id order by org_code) QRSLT WHERE (resp_id = ? and resp_app_id = ?)";
        try {
            connection  = pool.getConnection();

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, respId);
            preparedStatement.setString(2, respAppId);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("ORGANIZATION_ID",resultSet.getString(1));
                jsonObject.addProperty("ORGANIZATION_CODE",resultSet.getString(2));
                jsonObject.addProperty("ORGANIZATION_NAME",resultSet.getString(3));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("ORGANIZATION",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Organization Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String getUserInformation(String username, String fullname) {

        String result = null;

        String statement = "SELECT * FROM (SELECT fu.user_id, fu.user_name, (select hr.email_address from APPS.PER_PEOPLE_F hr where fu.employee_id = hr.person_id \n" +
                "and nvl(hr.effective_start_date, sysdate-1) < sysdate and nvl(hr.effective_END_date, sysdate+1) > sysdate) email_address,\n" +
                "(select hr.full_name from APPS.PER_PEOPLE_F hr where fu.employee_id = hr.person_id \n" +
                "and nvl(hr.effective_start_date, sysdate-1) < sysdate and nvl(hr.effective_END_date, sysdate+1) > sysdate) full_name,\n" +
                "(select pho.phone_number from APPS.PER_PEOPLE_F hr, APPS.per_phones pho where fu.employee_id = hr.person_id and hr.person_id = pho.parent_id(+)\n" +
                "AND pho.phone_type(+) = 'W1' AND pho.parent_table(+) = 'PER_ALL_PEOPLE_F' AND sysdate between pho.date_from(+) \n" +
                "and nvl(pho.date_to(+), sysdate) and nvl(hr.effective_start_date, sysdate-1) < sysdate \n" +
                "and nvl(hr.effective_END_date, sysdate+1) > sysdate) phone_number from APPS.fnd_user fu) QRSLT where 1=1 ";

        if (username != null && !username.isEmpty()) {
            statement += " and upper(user_name) like :userName";
        }
        if (fullname != null && !fullname.isEmpty()) {
            statement += " and (upper(full_name) like :fullName or full_name is null)";
        }

        try {
            connection  = pool.getConnection();

            namedParameterStatement = new NamedParameterStatement(connection,statement);
            namedParameterStatement.setFetchSize(1000);

            if (username != null && !username.isEmpty()) {
                namedParameterStatement.setString("userName", "%" + username.toUpperCase() + "%");
            }
            if (fullname != null && !fullname.isEmpty()) {
                namedParameterStatement.setString("fullName", "%" + fullname.toUpperCase() + "%");
            }

            resultSet = namedParameterStatement.executeQuery();
            resultSet.setFetchSize(1000);

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("USER_ID",resultSet.getString(1));
                jsonObject.addProperty("USER_NAME",resultSet.getString(2));
                jsonObject.addProperty("FULL_NAME",resultSet.getString(4));
                jsonObject.addProperty("EMAIL_ADDRESS",resultSet.getString(3));
                jsonObject.addProperty("PHONE_NUMBER",resultSet.getString(5));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("USER_INFORMATION",jsonArray);
            result = jsonResult.toString();

            connection.close();
            namedParameterStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get User Information Error", e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    public String test() {
        String result = null;

        String statement ="SELECT ml.meaning FROM EAM_WORK_ORDER_DETAILS ewod, EAM_WO_WORKFLOWS eww, mfg_lookups ml " +
                "WHERE eww.wip_entity_id = ewod.wip_entity_id AND ewod.workflow_type = ml.lookup_code (+) AND ml.lookup_type(+) = 'EAM_WORKFLOW_TYPE'";
        try {
            connection  = pool.getConnection();
            preparedStatement = connection.prepareStatement(statement);

            resultSet = preparedStatement.executeQuery();

            JsonObject jsonResult = new JsonObject();
            JsonArray jsonArray = new JsonArray();

            while (resultSet.next()){

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("meaning",resultSet.getString(1));
                jsonArray.add(jsonObject);
            }

            jsonResult.add("TEST",jsonArray);
            result = jsonResult.toString();

            connection.close();
            preparedStatement.close();
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("Get Organization Error",e);
        }
        finally {
            closeAllConnection(connection,preparedStatement,resultSet,callableStatement,namedParameterStatement);
        }
        return result;
    }

    void closeAllConnection(Connection connection,
                            PreparedStatement preparedStatement,
                            ResultSet resultSet,
                            CallableStatement callableStatement,
                            NamedParameterStatement namedParameterStatement) {

        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing connection Error", e);
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing preparedStatement Error", e);
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing resultSet Error", e);
            }
        }

        if (callableStatement != null) {
            try {
                callableStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing callableStatement Error", e);
            }
        }

        if (namedParameterStatement != null) {
            try {
                namedParameterStatement.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("Closing namedParameterStatement Error", e);
            }
        }
    }
}
