package com.stc.services;

import com.stc.controller.PrintWorkOrderController;
import com.stc.controller.WorkOrderController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Ardi sugiarto
 * Date: 09/01/18
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 */

@Path("/workOrder")
public class WorkOrderService {

    @GET
    @Path("getWorkOrderDashboard")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderDashboard(@QueryParam("orgId")int orgId,@QueryParam("userId")int userId) {

        if (orgId == 0 || userId==0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrderDashboard(orgId,userId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkOrderRequest")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderRequest(@QueryParam("orgId")int orgId,
                                        @QueryParam("requestNumber")String requestNumber,
                                        @QueryParam("assetDepartment")String assetDepartment,
                                        @QueryParam("assetNumber")String assetNumber,
                                        @QueryParam("status")String status,
                                        @QueryParam("creationDateFrom")Date creationDateFrom,
                                        @QueryParam("creationDateTo")Date creationDateTo,
                                        @QueryParam("userID")String userID,
                                        @QueryParam("respId")String respId,
                                        @QueryParam("respAppId")String respAppId,
                                        @QueryParam("isHqc")String isHqc
    ) {


        if (orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrderRequest(orgId,
                    requestNumber,
                    assetDepartment,
                    assetNumber,
                    status,
                    creationDateFrom,
                    creationDateTo,
                    userID,
                    respId,
                    respAppId,
                    isHqc
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getAssignedWorkOrder")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAssignedWorkOrder(@QueryParam("workRequestId")String workRequestId,
                                         @QueryParam("orgId")int orgId) {

        if (workRequestId == null || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getAssignedWorkOrder(workRequestId,orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("generateWorkOrderName")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response generateWorkOrderName() {
        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.generateWorkOrderName();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getClassCode")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getClassCode(@QueryParam("orgId")int orgId) {

        if (orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getClassCode(orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getAssetActivity")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAssetActivity(@QueryParam("orgId")int orgId,
                                 @QueryParam("maintenanceObjectId")int maintenanceObjectId) {

        if (orgId == 0 || maintenanceObjectId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getAssetActivity(orgId,maintenanceObjectId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkOrderStatus")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderStatus() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getWorkOrderStatus();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getWorkOrderTypes")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderTypes() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getWorkOrderTypes();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getWoStatus")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWoStatus() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getWoStatus();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getWorkOrders")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrders(@QueryParam("orgId")int orgId,
                                  @QueryParam("workOrderName")String workOrderName,
                                  @QueryParam("assetNumber")String assetNumber,
                                  @QueryParam("assetDescription")String assetDescription,
                                  @QueryParam("owningDepartment")String owningDepartment,
                                  @QueryParam("status")String status,
                                  @QueryParam("planned")String planned,
                                  @QueryParam("scheduledDateFrom")Date startDateFrom,
                                  @QueryParam("scheduledDateTo")Date startDateTo,
                                  @QueryParam("requestNumber")String requestNumber,
                                  @QueryParam("isHqc")String isHqc,            //SEND Y OR n
                                  @QueryParam("approvalType")String  approvalType,
                                  @QueryParam("CREATED_BY")String CREATED_BY,
                                  @QueryParam("buildingType")String buildingType,
                                  @QueryParam("userID")String userID,
                                  @QueryParam("respId")String respId,
                                  @QueryParam("respAppId")String respAppId,
                                  @QueryParam("lookupCode")String lookupCode,
                                  @QueryParam("dasboardSearch")String dasboardSearch
    ) {

        if (orgId == 0 || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            if(approvalType == null){
                approvalType = "";
            }
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrders(orgId,
                    workOrderName,
                    assetNumber,
                    assetDescription,
                    owningDepartment,
                    startDateFrom,
                    startDateTo,
                    status,
                    planned,
                    requestNumber,
                    isHqc,
                    approvalType,
                    CREATED_BY,
                    buildingType,
                    userID,
                    respId,
                    respAppId,
                    lookupCode,
                    dasboardSearch
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getAssetLocation")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAssetLocation(@QueryParam("assetNumber")String assetNumber) {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getAssetLocation(assetNumber);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getWorkOrderDetails")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderDetails(@QueryParam("entityId")int entityId) {

        if (entityId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrderDetails(entityId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }


    @GET
    @Path("printWorkOrder")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response printWorkOrder(@QueryParam("orgId")String orgId, @QueryParam("WipEntityId") String WipEntityId, @QueryParam("isHQC") String isHQC) {

        if (orgId ==null || WipEntityId==null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            PrintWorkOrderController printWorkOrderController=new PrintWorkOrderController();
            result = printWorkOrderController.getReport(orgId,WipEntityId,isHQC);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }



/*
    @GET
    @Path("getWorkOrderSearchDetails")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderSearchDetails(@QueryParam("entityId")int entityId, @QueryParam("isHqc") boolean isHqc) {

        if (entityId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrderSearchDetails(entityId, isHqc);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }
*/



    @GET
    @Path("getUom")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getUom(@QueryParam("uomCode")String uomCode) {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getUom(uomCode);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }


    @GET
    @Path("getResources")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getResources(@QueryParam("departmentId")int departmentId) {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getResources(departmentId);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getInstances")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getInstances(@QueryParam("resourceId")int resourceId) {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getInstances(resourceId);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getReconciliationCode")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getReconciliationCode() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getReconciliationCode();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getSubinventory")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getSubinventory(@QueryParam("orgId")int orgId) {

        if (orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getSubinventory(orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getLocator")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLocator(@QueryParam("orgId")int orgId,
                               @QueryParam("subInventory")String subInventory) {

        if (orgId == 0 || subInventory == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getLocator(orgId,subInventory);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getLot")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLot(@QueryParam("orgId")int orgId) {

        if (orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getLot(orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getShutdownType")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getShutdownType() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getShutdownType();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getActivityType")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getActivityType() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getActivityType();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getActivityCause")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getActivityCause() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getActivityCause();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getActivitySource")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getActivitySource() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getActivitySource();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("getWarrantyStatus")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWarrantyStatus() {

        String result;
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getWarrantyStatus();

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    /**
     *
     * status type
     * 1 -> unreleased
     * 1000 -> awaiting scheduling
     * 3 -> released
     * 6 -> on hold
     * 17 -> draft
     *
     */

    @POST
    @Path("createWorkOrder")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response createWorkOrder(@FormParam("userId") int userId,
                                    @FormParam("respId") int respId,
                                    @FormParam("respAppId") int respAppId,
                                    @FormParam("entityName") String entityName,
                                    @FormParam("orgId") int orgId,
                                    @FormParam("assetNumber") String assetNumber,
                                    @FormParam("assetGroupId") int assetGroupId,
                                    @FormParam("maintenanceObjectId") int maintenanceObjectId,
                                    @FormParam("classCode") String classCode,
                                    @FormParam("scheduledStartDate") String scheduledStartDate,
                                    @FormParam("scheduledCompletionDate") String scheduledCompletionDate,
                                    @FormParam("requestNumber") int requestNumber,
                                    @FormParam("description") String description,
                                    @FormParam("departmentId") int departmentId,
                                    @FormParam("statusType") int statusType,
                                    @FormParam("priority") int priority,
                                    @FormParam("wfStatus") String wfStatus
                                    ) {


        if (entityName == null || assetGroupId == 0 || classCode == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result = "TES";
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.createWorkOrder(
                 userId,
                 respId,
                 respAppId,
                 orgId,
                 entityName,
                 assetNumber,
                 assetGroupId,
                 maintenanceObjectId,
                 classCode,
                 description,
                 departmentId,
                 scheduledStartDate,
                 scheduledCompletionDate,
                 requestNumber,
                 statusType,
                 priority,
                 wfStatus
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @PUT
    @Path("updateWorkOrder")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response updateWorkOrder(@FormParam("userId") int userId,
                                    @FormParam("respId") int respId,
                                    @FormParam("respAppId") int respAppId,
                                    @FormParam("entityId") int entityId,
                                    @FormParam("orgId") int orgId,
                                    @FormParam("scheduledStartDate") String scheduledStartDate,
                                    @FormParam("scheduledCompletionDate") String scheduledCompletionDate,
                                    @FormParam("assetNumber") String assetNumber,
                                    @FormParam("maintenanceObjectId") int maintenanceObjectId,
                                    @FormParam("assetGroupId") int assetGroupId,
                                    @FormParam("departmentId") int departmentId,
                                    @FormParam("classCode") String classCode,
                                    @FormParam("description") String description,
                                    @FormParam("statusType") int statusType,
                                    @FormParam("priority") int priority,
                                    @FormParam("requestNumber") int requestNumber
                                    ) {

        System.out.println("userId "+userId);
        System.out.println("respId "+respId);
        System.out.println("respAppId "+respAppId);
        System.out.println("entityId "+entityId);
        System.out.println("orgId "+orgId);
        System.out.println("scheduledStartDate "+scheduledStartDate);
        System.out.println("scheduledCompletionDate "+scheduledCompletionDate);
        System.out.println("assetNumber "+assetNumber);
        System.out.println("maintenanceObjectId "+maintenanceObjectId);
        System.out.println("assetGroupId "+assetGroupId);
        System.out.println("departmentId "+departmentId);
        System.out.println("classCode "+classCode);
        System.out.println("description "+description);
        System.out.println("statusType "+statusType);
        System.out.println("priority "+priority);
        System.out.println("requestNumber "+requestNumber);


        if (userId == 0 || respId == 0 || respAppId == 0 || entityId == 0 || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.updateWorkOrder(
                 userId,
                 respId,
                 respAppId,
                 entityId,
                 orgId,
                 scheduledStartDate,
                 scheduledCompletionDate,
                 assetNumber,
                 maintenanceObjectId,
                 assetGroupId,
                 departmentId,
                 classCode,
                 description,
                 statusType,
                 priority,
                 requestNumber
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("deleteWorkRequestAssign")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response deleteWorkRequestAssign(@FormParam("userId") int userId,
                                    @FormParam("respId") int respId,
                                    @FormParam("respAppId") int respAppId,
                                    @FormParam("entityId") int entityId,
                                    @FormParam("orgId") int orgId,
                                    @FormParam("requestNumber") int requestNumber
    ) {


        if (userId == 0 || respId == 0 || respAppId == 0 || entityId == 0 || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.deleteWorkRequestAssign(
                    userId,
                    respId,
                    respAppId,
                    entityId,
                    orgId,
                    requestNumber
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("createOperationRequirements")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response createOperationRequirements(@FormParam("userId") int userId,
                                                @FormParam("respId") int respId,
                                                @FormParam("respAppId") int respAppId,
                                                @FormParam("entityId") int entityId,
                                                @FormParam("orgId") int orgId,
                                                @FormParam("departmentId") int departmentId,
                                                @FormParam("operation") String operation,
                                                @FormParam("description") String description,
                                                @FormParam("resourceSequence") String resourceSequence,
                                                @FormParam("startTime") String startTime,
                                                @FormParam("endTime") String endTime) {

        if (entityId == 0 || orgId == 0 || departmentId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.createOperationRequirements(userId,
                    respId,
                    respAppId,
                    entityId,
                    orgId,
                    departmentId,
                    operation,
                    description,
                    resourceSequence,
                    startTime,
                    endTime);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("deleteOperationRequirements")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response deleteOperationRequirements(@FormParam("userId") int userId,
                                               @FormParam("respId") int respId,
                                               @FormParam("respAppId") int respAppId,
                                               @FormParam("departmentId") int departmentId,
                                               @FormParam("operationSeqNum") int operationSeqNum,
                                               @FormParam("entityId") int entityId,
                                               @FormParam("orgId") int orgId) {

        if (entityId == 0 || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.deleteOperationRequirements(userId,
                    respId,
                    respAppId,
                    departmentId,
                    operationSeqNum,
                    entityId,
                    orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("createMaterialRequirements")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response createMaterialRequirements(@FormParam("userId") int userId,
                                                @FormParam("respId") int respId,
                                                @FormParam("respAppId") int respAppId,
                                                @FormParam("entityId") int entityId,
                                                @FormParam("orgId") int orgId,
                                                @FormParam("departmentId") int departmentId,
                                                @FormParam("operation") String operation,
                                                @FormParam("itemType") String itemType,
                                                @FormParam("resourceSequence") String resourceSequence,
                                                @FormParam("description") String description,
                                                @FormParam("quantity") String quantity,
                                                @FormParam("uom") String uom,
                                                @FormParam("cost") String cost,
                                                @FormParam("agent") String agent,
                                                @FormParam("importPeriod") String importPeriod,
                                                @FormParam("consumable") String consumable,
                                                @FormParam("imported") String imported,
                                                @FormParam("supplierName") String supplierName,
                                                @FormParam("invoiceNumber") String invoiceNumber,
                                                @FormParam("warrantyPeriod") String warrantyPeriod,
                                                @FormParam("returned") String returned,
                                                @FormParam("note") String note) {

        if (entityId == 0 || orgId == 0 || departmentId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.createMaterialRequirements(userId,
             respId,
             respAppId,
             entityId,
             orgId,
             departmentId,
             operation,
             itemType,
             resourceSequence,
             description,
             quantity,
             uom,
             cost,
             agent,
             importPeriod,
             consumable,
             imported,
             supplierName,
             invoiceNumber,
             warrantyPeriod,
             returned,
             note);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("deleteMaterialRequirements")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response deleteMaterialRequirements(@FormParam("userId") int userId,
                                               @FormParam("respId") int respId,
                                               @FormParam("respAppId") int respAppId,
                                               @FormParam("directItemSequenceId") int directItemSequenceId,
                                               @FormParam("departmentId") int departmentId,
                                               @FormParam("operationSeqNum") int operationSeqNum,
                                               @FormParam("entityId") int entityId,
                                               @FormParam("orgId") int orgId) {

        if (entityId == 0 || orgId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.deleteMaterialRequirements(userId,
                    respId,
                    respAppId,
                    directItemSequenceId,
                    departmentId,
                    operationSeqNum,
                    entityId,
                    orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("completeWorkOrder")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response completeWorkOrder(@FormParam("entityId")int entityId,
                                      @FormParam("transactionType")int transactionType,
                                      @FormParam("transactionDate")String transactionDate,
                                      @FormParam("userId")int userId,
                                      @FormParam("reconcilCode")String reconcilCode,
                                      @FormParam("actualStartDate")String actualStartDate,
                                      @FormParam("actualEndDate")String actualEndDate,
                                      @FormParam("duration")float duration) {

        if (entityId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.completeWorkOrder(
             entityId,
             transactionType,
             transactionDate,
             userId,
             reconcilCode,
             actualStartDate,
             actualEndDate,
             duration);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("sendWorkOrderForApproval")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response sendWorkOrderForApproval(@FormParam("isHqc") String isHqc,
                                             @FormParam("entityId") int entityId,
                                             @FormParam("userId") int userId,
                                             @FormParam("respId") int respId,
                                             @FormParam("respAppId") int respAppId) {


        if (userId == 0 || respId == 0 || respAppId == 0 || entityId == 0 || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.sendWorkOrderForApproval(
                    isHqcFlag(isHqc),
                    entityId,
                    userId,
                    respId,
                    respAppId
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    private boolean isHqcFlag(String isHqc){
        boolean isHqcflag = false;
        if(isHqc.equalsIgnoreCase("Y")){
            isHqcflag = true;
        } else if (isHqc.equalsIgnoreCase("N")){
            isHqcflag = false;
        }
           return isHqcflag;
    }

    @GET
    @Path("isWoNotGetFromIGate")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response isWoNotGetFromIGate(@QueryParam("transactionID")String transactionID) {

        if (transactionID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result="";
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.isWoNotGetFromIGate(transactionID);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("sendSparePartsForApproval")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response sendSparePartsForApproval(@FormParam("isHqc") String isHqc,
                                              @FormParam("entityId") int entityId,
                                              @FormParam("userId") int userId,
                                              @FormParam("respId") int respId,
                                              @FormParam("respAppId") int respAppId) {


        if (userId == 0 || respId == 0 || respAppId == 0 || entityId == 0 || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.sendSparePartsForApproval(
                    isHqcFlag(isHqc),
                    entityId,
                    userId,
                    respId,
                    respAppId
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("sendCompleteWorkOrderForApproval")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response sendCompleteWorkOrderForApproval(@FormParam("isHqc") String isHqc,
                                                     @FormParam("entityId") int entityId,
                                                     @FormParam("userId") int userId,
                                                     @FormParam("respId") int respId,
                                                     @FormParam("respAppId") int respAppId) {


        if (userId == 0 || respId == 0 || respAppId == 0 || entityId == 0 || isHqc == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.sendCompleteWorkOrderForApproval(
//                    isHqcFlag(isHqc),
                    isHqc,
                    entityId,
                    userId,
                    respId,
                    respAppId
            );

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @POST
    @Path("cancelWorkRequestWs")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response cancelWorkRequestWs(@FormParam("entityId") int entityId) {


        if (entityId == 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {

            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.cancelWorkRequestWs(entityId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("isContractor")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response isContractor(@QueryParam("respID")String respId) {

        if (respId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.isContractor(respId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("checkLinesAndAttachment")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response checkLinesAndAttachments(@QueryParam("transactionID")String transactionID) {

        if (transactionID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.checkLinesAndAttachments(transactionID);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("isHqcOrg")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response isHqcOrg(@QueryParam("organizationID")String organizationID) {

        if (organizationID == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.isHqcOrg(organizationID);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkOrderWorkFlowStatus")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderWorkFlowStatus(@QueryParam("wipId") int workId) {

        String result;
        if (workId == 0 ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrderWorkFlowStatus(workId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkOrderStatusForHQC")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderStatusForHQC(@QueryParam("wipId") int wipId) {

        String result;
        if (wipId == 0 ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrderStatusForHQC(wipId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }


    @GET
    @Path("getWorkOrderStatusForDistrict")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderStatusForDistrict(@QueryParam("wipId") int wipId) {

        String result;
        if (wipId == 0 ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrderStatusForDistrict(wipId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getWorkOrderMaterialUOMS")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getWorkOrderMaterialUOMS() {

        String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getWorkOrderMaterialUOMS();

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }

    }

    @GET
    @Path("getSparePartsList")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getSparePartsList(@QueryParam("wipId") int wipId) {

        String result;
        if (wipId == 0 ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getSparePartsList(wipId);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
        }
    }

    //validateSparePartsApproval

    @GET
    @Path("validateSparePartsApproval")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response validateSparePartsApproval(@QueryParam("wipId") int wipId, @QueryParam("orgId") int orgId) {

        String result;
        if (wipId == 0 ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.validateSparePartsApproval(wipId,orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }


    @POST
    @Path("updateMaterialDetails")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response updateMaterialDetails(
            @FormParam("P_WIP_ENTITY_ID") String P_WIP_ENTITY_ID, //mandatory
            @FormParam("P_OPERATION_SEQ_NUM") String P_OPERATION_SEQ_NUM,
            @FormParam("P_DIRECT_ITEM_SEQUENCE_ID") String P_DIRECT_ITEM_SEQUENCE_ID, //mandatory
            @FormParam("P_DESCRIPTION") String P_DESCRIPTION,
            @FormParam("P_ORGANIZATION_ID") String P_ORGANIZATION_ID,//mandatory
            @FormParam("P_REQUIRED_QUANTITY") String P_REQUIRED_QUANTITY,
            @FormParam("P_UOM") String P_UOM,
            @FormParam("P_NEED_BY_DATE") String P_NEED_BY_DATE,
            @FormParam("P_ORDER_TYPE_LOOKUP_CODE") String P_ORDER_TYPE_LOOKUP_CODE,
            @FormParam("P_COST_ATT1") String P_COST_ATT1,
            @FormParam("P_CONSUMABLE_ATT2") String P_CONSUMABLE_ATT2,
            @FormParam("P_NOTES_ATT3") String P_NOTES_ATT3,
            @FormParam("P_IMPORTED_ATT4") String P_IMPORTED_ATT4,
            @FormParam("P_VENDOR_ATT5") String P_VENDOR_ATT5,
            @FormParam("P_INVOICE_NO_ATT6") String P_INVOICE_NO_ATT6,
            @FormParam("P_AGENT_ATT7") String P_AGENT_ATT7,
            @FormParam("P_WARANTY_PERIOD_ATT8") String P_WARANTY_PERIOD_ATT8,
            @FormParam("P_RETURNED_ATT9") String P_RETURNED_ATT9,
            @FormParam("P_VENDOR_PHONE_ATT10") String P_VENDOR_PHONE_ATT10,
            @FormParam("P_IMPORTPERIOD_ATT12") String P_IMPORTPERIOD_ATT12,
            @FormParam("P_ATT13") String P_ATT13,
            @FormParam("P_ATT14") String P_ATT14,
            @FormParam("P_ATT15") String P_ATT15
    ) {
        System.out.println("In-Request");
        String result;
        if (P_WIP_ENTITY_ID == null || P_DIRECT_ITEM_SEQUENCE_ID==null || P_ORGANIZATION_ID==null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkOrderController workOrderController = new WorkOrderController();
            System.out.println("In-Request Send");
            result = workOrderController.updateMaterialDetails(P_WIP_ENTITY_ID,
                    P_OPERATION_SEQ_NUM,
                    P_DIRECT_ITEM_SEQUENCE_ID,
                    P_DESCRIPTION,
                    P_ORGANIZATION_ID,
                    P_REQUIRED_QUANTITY,
                    P_UOM,
                    P_NEED_BY_DATE,
                    P_ORDER_TYPE_LOOKUP_CODE,
                    P_COST_ATT1,
                    P_CONSUMABLE_ATT2,
                    P_NOTES_ATT3,
                    P_IMPORTED_ATT4,
                    P_VENDOR_ATT5,
                    P_INVOICE_NO_ATT6,
                    P_AGENT_ATT7,
                    P_WARANTY_PERIOD_ATT8,
                    P_RETURNED_ATT9,
                    P_VENDOR_PHONE_ATT10,
                    P_IMPORTPERIOD_ATT12,
                    P_ATT13,
                    P_ATT14,
                    P_ATT15);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("removeMaterialDetails")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response removeMaterialDetails(
            @QueryParam("wipId") String wipId,
            @QueryParam("orgId") String orgId,
            @QueryParam("seqId") String seqId
    ) {

        String result;
        if (wipId == "0" ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.removeMaterialDetails(seqId, wipId, orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }
    }

    @GET
    @Path("getCreatorName")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getCreatorName(@QueryParam("orgId")int orgId) {

        String result;
        if (orgId<=0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        else {
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.getCreatorName(orgId);

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        }

    }
    
    
    @GET
    @Path("getApprovalHistory")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getApprovalHistory(@QueryParam("wipEntityId")int wipEntityId) {

        String result;
        if(wipEntityId==0)
            return Response.status(Response.Status.BAD_REQUEST).build();
            
        WorkOrderController workOrderController = new WorkOrderController();
        result = workOrderController.getApprovalHistory(wipEntityId);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    
    
      @GET
    @Path("wfStatuslov")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response wfStatuslov() {

            String result;
            WorkOrderController workOrderController = new WorkOrderController();
            result = workOrderController.wfStatuslov();

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
       

    }
    

}
