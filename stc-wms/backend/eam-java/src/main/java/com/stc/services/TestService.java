package com.stc.services;

import com.stc.controller.TestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA.
 * User: Intellinum-PC11
 * Date: 16/12/18
 * Time: 4:15 PM
 * To change this template use File | Settings | File Templates.
 */

@Path("/test")
public class TestService {

    //getIdentity

    @GET
    @Path("getIdentity")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getIdentity() throws IOException, NoSuchAlgorithmException, KeyManagementException {


            String result;
        TestController testController = new TestController();
            result = testController.getIdentity();

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }

    }
}
