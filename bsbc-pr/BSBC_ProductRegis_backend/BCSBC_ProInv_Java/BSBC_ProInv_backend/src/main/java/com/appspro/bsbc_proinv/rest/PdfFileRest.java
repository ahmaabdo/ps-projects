package com.appspro.bsbc_proinv.rest;


import com.appspro.bsbc_proinv.dao.PdfFileDAO;


import java.io.FileNotFoundException;

import java.io.IOException;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;


@Path("/getFiles")
public class PdfFileRest {

    PdfFileDAO pdffiles = new PdfFileDAO();


    @POST
    @Path("/get")
    @Produces("application/pdf")
    public Response getFile(String body) {
        //System.out.println(body);
        String file_base64 = null;
        if (!body.isEmpty()) {

            file_base64 = pdffiles.createPDFWithIText(body);
            if (file_base64 == null) {
                return Response.ok("error while generating file").build();
            }
            return Response.ok(file_base64).build();
//            byte[] fbytes = Base64.decodeBase64(file_base64);
//            Response.ResponseBuilder response =
//                Response.ok(fbytes, "application/pdf");
//
//            response.header("Content-Disposition",
//                            "attachment; filename=\"table.pdf\"");
//            return response.build();

        } else {
            return Response.ok("bad request").build();
        }
    }
}
