package com.appspro.bsbc_proinv.rest;

import com.appspro.helper.Helper;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import main.java.com.appspro.bsbc_lg.bean.ProductRegistrationBean;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/SheetRest")
public class SheetRest {

    private String[] columns = {
    "Registration Number","Registeration Date", "Registration Type", "Product Name","Item Code",
    "Supplier Name","Telephone","Address","Email","Title", "Contact", "Product Registration Status",
    "Company Name","Register Location", "Country Of Origin","Shelf Life","Storage Condition","Cif Price",
    "Whole Self Price","Retail Price","Exchange Rate","Currency","Primary Packaging","Mah Name",
    "Manufacturing Name", "Indication","Pharamaceutical Form","Active Ingredient Name",
    "Relative Substance","Site Registeration Name"};
    
    @GET
    @Path("excelreport")
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheetl") 
    public Response getFile(
           @QueryParam("registerationNumber") String registerNumber, 
           @QueryParam("supplierName") String supplierName,
           @QueryParam("siteRegisterationNumber") String siteRegistrationNumber, 
           @QueryParam("companyName") String companyName,
           @QueryParam("productName") String productName, 
           @Context HttpServletResponse response)
          throws FileNotFoundException, IOException, JSONException {
        String url = Helper.paasURL + "searchProductRegis";
        String result = Helper.callGetRestWithParmsGe(url, registerNumber, supplierName, siteRegistrationNumber, companyName, productName);
        String fileName = "Excel_Output.xls";
              ProductRegistrationBean p = null;
              List<ProductRegistrationBean> list = new ArrayList<ProductRegistrationBean>();                

            JSONArray jsons =  (JSONArray)new JSONObject(result).get("items");
            for(int i=0;i< jsons.length(); i++){
                JSONObject json =  jsons.getJSONObject(i);
                System.out.println(json.toString());
                p = new ProductRegistrationBean();
                
    //                p.setId(String.valueOf(json.get("id") ));
                p.setRegisterNumber(!json.isNull("register_number") ? json.get("register_number").toString() : "");
                p.setRegisterDate(!json.isNull("register_date") ? json.get("register_date").toString() : "");
                p.setRegisterType(!json.isNull("register_type") ? json.get("register_type").toString() : "");
                p.setProductName(!json.isNull("product_name") ? json.get("product_name").toString() : "");
                p.setItemCode(!json.isNull("item_code") ? json.get("item_code").toString() : "");
                p.setSupplierName(!json.isNull("supplier_name") ? json.get("supplier_name").toString() : "");
                p.setTelephone(!json.isNull("telephone") ? json.get("telephone").toString() : "");
                p.setAddress(!json.isNull("address") ? json.get("address").toString() : "");
                p.setEmail(!json.isNull("email") ? json.get("email").toString() : "");
                p.setTitle(!json.isNull("title") ? json.get("title").toString() : "");
                p.setContact(!json.isNull("contact") ? json.get("contact").toString() : "");
                p.setProductRegisterStatus(!json.isNull("product_registration_status") ? json.get("product_registration_status").toString() : "");
                p.setCompanyName(!json.isNull("company_name") ? json.get("company_name").toString() : "");
                p.setRegisterLocation(!json.isNull("register_location") ? json.get("register_location").toString() : "");
                p.setCountryOfOrigin(!json.isNull("country_of_origin") ? json.get("country_of_origin").toString() : "");
                p.setShelfLife(!json.isNull("shelf_life") ? json.get("shelf_life").toString() : "");
                p.setStorageCodition(!json.isNull("storage_codition") ? json.get("storage_codition").toString() : "");
                p.setCifPrice(!json.isNull("cif_price") ? json.get("cif_price").toString() : "");
                p.setWholeSalePrice(!json.isNull("whole_sale_price") ? json.get("whole_sale_price").toString() : "");
                p.setRetailPrice(!json.isNull("retail_price") ? json.get("retail_price").toString() : "");
                p.setExchangeRate(!json.isNull("exchange_rate") ? json.get("exchange_rate").toString() : "");
                p.setCurrency(!json.isNull("currency") ? json.get("currency").toString() : "");
                p.setPrimaryPackaging(!json.isNull("primary_packaging") ? json.get("primary_packaging").toString() : "");
                p.setMahName(!json.isNull("mah_name") ? json.get("mah_name").toString() : "");
                p.setManufacturingName(!json.isNull("manufacturing_name") ? json.get("manufacturing_name").toString() : "");
                p.setIndication(!json.isNull("indication") ? json.get("indication").toString() : "");
                p.setPharmaceuticalForm(!json.isNull("pharamaceutical_form") ? json.get("pharamaceutical_form").toString() : "");
                p.setActiveIngredientName(!json.isNull("active_ingredient_name") ? json.get("active_ingredient_name").toString() : "");
                p.setRelativeSubstances(!json.isNull("relative_substances") ? json.get("relative_substances").toString() : "");
                p.setSiteRegistrationName(!json.isNull("site_registration_name") ? json.get("site_registration_name").toString() : "");
                list.add(p);

            }
         
        // Create a Workbook
        Workbook workbook = new HSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat, 
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Letter Of Product Registration");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBoldweight((short)1);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MMM-yyyy"));

        // Create Other rows and cells with LG data
        int rowNum = 1;
        for (ProductRegistrationBean lg : list) {
            Row row = sheet.createRow(rowNum++);

    //            row.createCell(0)
    //                    .setCellValue(lg.getId());

            row.createCell(0)
                    .setCellValue(lg.getRegisterNumber());
            
            Cell lgStartDateCell = row.createCell(1);
            lgStartDateCell.setCellValue(lg.getRegisterDate());
            lgStartDateCell.setCellStyle(dateCellStyle);
            
            row.createCell(2)
                    .setCellValue(lg.getRegisterType());
            
            row.createCell(3)
                    .setCellValue(lg.getProductName());

            row.createCell(4)
                    .setCellValue(lg.getItemCode());

            row.createCell(5)
                    .setCellValue(lg.getSupplierName());

            row.createCell(6)
                    .setCellValue(lg.getTelephone());
            
            row.createCell(7)
                    .setCellValue(lg.getAddress());

            row.createCell(8)
                    .setCellValue(lg.getEmail());
            
            row.createCell(9)
                    .setCellValue(lg.getTitle());

            row.createCell(10)
                    .setCellValue(lg.getContact());

            row.createCell(11)
                    .setCellValue(lg.getProductRegisterStatus());
            
            row.createCell(12)
                    .setCellValue(lg.getCompanyName());
            
            row.createCell(13)
                    .setCellValue(lg.getRegisterLocation());
            
            row.createCell(14)
                    .setCellValue(lg.getCountryOfOrigin());
            
            row.createCell(15)
                    .setCellValue(lg.getShelfLife());
            
            row.createCell(16)
                    .setCellValue(lg.getStorageCodition());
            
            row.createCell(17)
                    .setCellValue(lg.getCifPrice());
            
            row.createCell(18)
                    .setCellValue(lg.getWholeSalePrice());
            
            row.createCell(19)
                    .setCellValue(lg.getRetailPrice());
            
            row.createCell(20)
                    .setCellValue(lg.getExchangeRate());
            
            row.createCell(21)
                    .setCellValue(lg.getCurrency());
            
            row.createCell(22)
                    .setCellValue(lg.getPrimaryPackaging());
            
            row.createCell(23)
                    .setCellValue(lg.getMahName());
            
            row.createCell(24)
                    .setCellValue(lg.getManufacturingName());
            
            row.createCell(25)
                    .setCellValue(lg.getIndication());
            
            row.createCell(26)
                    .setCellValue(lg.getPharmaceuticalForm());
            
            row.createCell(27)
                    .setCellValue(lg.getActiveIngredientName());
            
            row.createCell(28)
                    .setCellValue(lg.getRelativeSubstances());
            
            row.createCell(29)
                    .setCellValue(lg.getSiteRegistrationName());
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
            response.setHeader("Content-disposition", "attachment; filename=BSBC Result Table.xls");
        response.setHeader("Content-type", "application/octet-stream");
            workbook.write(response.getOutputStream());
        // Closing the workbook
        //workbook.close();
        
        return Response.ok().build();

    }
}
