package main.java.com.appspro.bsbc_lg.bean;

public class ProductRegistrationBean {

    private String id;
    private String registerNumber;
    private String registerDate;
    private String registerType;
    private String productName;
    private String itemCode;
    private String supplierName;
    private String telephone;
    private String address;
    private String email;
    private String title;
    private String contact;
    private String productRegisterStatus;
    private String companyName;
    private String registerLocation;
    private String countryOfOrigin;
    private String shelfLife;
    private String storageCodition;
    private String cifPrice;
    private String wholeSalePrice;
    private String retailPrice;
    private String exchangeRate;
    private String currency;
    private String primaryPackaging;
    private String mahName;
    private String manufacturingName;
    private String indication;
    private String pharmaceuticalForm;
    private String activeIngredientName;
    private String relativeSubstances;
    private String siteRegistrationName;
    private String siteRegistrationValidity;
    private String siteRegistrationNumber;
    private String lastRenewal;
    private String nextRenewal;
    private String secondaryPackage_releaseSite;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }

    public void setProductRegisterStatus(String productRegisterStatus) {
        this.productRegisterStatus = productRegisterStatus;
    }

    public String getProductRegisterStatus() {
        return productRegisterStatus;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setRegisterLocation(String registerLocation) {
        this.registerLocation = registerLocation;
    }

    public String getRegisterLocation() {
        return registerLocation;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setShelfLife(String shelfLife) {
        this.shelfLife = shelfLife;
    }

    public String getShelfLife() {
        return shelfLife;
    }

    public void setStorageCodition(String storageCodition) {
        this.storageCodition = storageCodition;
    }

    public String getStorageCodition() {
        return storageCodition;
    }

    public void setCifPrice(String cifPrice) {
        this.cifPrice = cifPrice;
    }

    public String getCifPrice() {
        return cifPrice;
    }

    public void setWholeSalePrice(String wholeSalePrice) {
        this.wholeSalePrice = wholeSalePrice;
    }

    public String getWholeSalePrice() {
        return wholeSalePrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setPrimaryPackaging(String primaryPackaging) {
        this.primaryPackaging = primaryPackaging;
    }

    public String getPrimaryPackaging() {
        return primaryPackaging;
    }

    public void setMahName(String mahName) {
        this.mahName = mahName;
    }

    public String getMahName() {
        return mahName;
    }

    public void setManufacturingName(String manufacturingName) {
        this.manufacturingName = manufacturingName;
    }

    public String getManufacturingName() {
        return manufacturingName;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public String getIndication() {
        return indication;
    }

    public void setPharmaceuticalForm(String pharmaceuticalForm) {
        this.pharmaceuticalForm = pharmaceuticalForm;
    }

    public String getPharmaceuticalForm() {
        return pharmaceuticalForm;
    }

    public void setActiveIngredientName(String activeIngredientName) {
        this.activeIngredientName = activeIngredientName;
    }

    public String getActiveIngredientName() {
        return activeIngredientName;
    }

    public void setRelativeSubstances(String relativeSubstances) {
        this.relativeSubstances = relativeSubstances;
    }

    public String getRelativeSubstances() {
        return relativeSubstances;
    }

    public void setSiteRegistrationName(String siteRegistrationName) {
        this.siteRegistrationName = siteRegistrationName;
    }

    public String getSiteRegistrationName() {
        return siteRegistrationName;
    }

    public void setSiteRegistrationValidity(String siteRegistrationValidity) {
        this.siteRegistrationValidity = siteRegistrationValidity;
    }

    public String getSiteRegistrationValidity() {
        return siteRegistrationValidity;
    }

    public void setSiteRegistrationNumber(String siteRegistrationNumber) {
        this.siteRegistrationNumber = siteRegistrationNumber;
    }

    public String getSiteRegistrationNumber() {
        return siteRegistrationNumber;
    }

    public void setLastRenewal(String lastRenewal) {
        this.lastRenewal = lastRenewal;
    }

    public String getLastRenewal() {
        return lastRenewal;
    }

    public void setNextRenewal(String nextRenewal) {
        this.nextRenewal = nextRenewal;
    }

    public String getNextRenewal() {
        return nextRenewal;
    }

    public void setSecondaryPackage_releaseSite(String secondaryPackage_releaseSite) {
        this.secondaryPackage_releaseSite = secondaryPackage_releaseSite;
    }

    public String getSecondaryPackage_releaseSite() {
        return secondaryPackage_releaseSite;
    }

    @Override
    public String toString() {
        return "ProductRegistrationBean{" + "id=" + id + ", registerNumber=" +
            registerNumber + ", registerDate=" + registerDate +
            ", registerType=" + registerType + ", productName=" + productName +
            ", itemCode=" + itemCode + ", supplierName=" + supplierName +
            ", telephone=" + telephone + ", address=" + address + ", email=" +
            email + ", title=" + title + ", contact=" + contact +
            ", productRegisterStatus=" + productRegisterStatus +
            ", companyName=" + companyName + ", registerLocation=" +
            registerLocation + ", countryOfOrigin=" + countryOfOrigin +
            ", shelfLife=" + shelfLife + ", storageCodition=" +
            storageCodition + ", cifPrice=" + cifPrice + ", wholeSalePrice=" +
            wholeSalePrice + ", retailPrice=" + retailPrice +
            ", exchangeRate=" + exchangeRate + ", currency=" + currency +
            ", primaryPackaging=" + primaryPackaging + ", mahName=" + mahName +
            ", manufacturingName=" + manufacturingName + ", indication=" +
            indication + ", pharmaceuticalForm=" + pharmaceuticalForm +
            ", activeIngredientName=" + activeIngredientName +
            ", relativeSubstances=" + relativeSubstances +
            ", siteRegistrationName=" + siteRegistrationName +
            ", siteRegistrationValidity=" + siteRegistrationValidity +
            ", siteRegistrationNumber=" + siteRegistrationNumber +
            ", lastRenewal=" + lastRenewal + ", nextRenewal=" + nextRenewal +
            ", secondaryPackage_releaseSite=" + secondaryPackage_releaseSite +
            '}';
    }


}
