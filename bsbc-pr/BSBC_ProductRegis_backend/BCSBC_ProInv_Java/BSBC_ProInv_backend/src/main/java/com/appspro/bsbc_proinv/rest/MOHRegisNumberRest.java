package com.appspro.bsbc_proinv.rest;

import com.appspro.helper.Helper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/MOHRegisNumber")
public class MOHRegisNumberRest {
   
   
    @GET
    @Path("MOHCurrRegisNumber")
    @Produces({"application/json"})
    public String getMOHCurrRegisNumber() throws Exception {
        
        String url = Helper.paasURL + "MohCurrentRegisNumber";
        String returnValue = Helper.callGetRest(url);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;

    }
    
    
    
    @GET
    @Path("MOHNextRegisNumber")
    @Produces({"application/json"})
    public String getMOHNextRegisNumber() throws Exception {
        
        String url = Helper.paasURL + "MohRegisNumber";
        String returnValue = Helper.callGetRest(url);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;

    }
    
    
    
    
    
}
