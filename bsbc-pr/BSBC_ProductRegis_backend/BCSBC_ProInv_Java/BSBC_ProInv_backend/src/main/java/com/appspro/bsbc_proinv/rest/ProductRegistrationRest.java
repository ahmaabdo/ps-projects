package com.appspro.bsbc_proinv.rest;


import com.appspro.bsbc_proinv.dao.ProductRegistrationDAO;
import com.appspro.helper.Helper;
import main.java.com.appspro.bsbc_lg.bean.ProductRegistrationBean;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/ProductRegisRest")
public class ProductRegistrationRest {
    
    ProductRegistrationDAO serviceProductRegistrationDAO = new ProductRegistrationDAO();

    
    @POST
    @Path("/addProductRegis")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String postPIAcknowledge(String bean) throws Exception {

           //String returnValue1 =   serviceProductRegistrationDAO.sendPost(bean);
        //     return bean ;
        String url = Helper.paasURL + "ProductRegisRest/";
        String returnValue = Helper.callPostRest(url, bean);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;

    }
    
    
    
    @PUT
    @Path("/updateProductRegis")
    @Produces( { "application/json" })
    @Consumes(MediaType.APPLICATION_JSON)
    public String putProductRegistration(String bean) throws Exception {

           //String returnValue1 =   serviceProductRegistrationDAO.sendPost(bean);
        //     return bean ;
        String url = Helper.paasURL + "ProductRegisRest/";
        String returnValue = Helper.callPutRest(url, bean);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;

    }



 
    
    
    @GET
    @Path("getProductRegis/{id}")
    @Produces({"application/json"})
    public String getProductRegis(@PathParam("id")
        String id) throws Exception {
        
        String url = Helper.paasURL + "getProductRegistration/"+id;
        String returnValue = Helper.callGetRest(url);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;


       

    }


    
    @DELETE
    @Path("deleteProductRegis/{id}")
    @Produces({"application/json"})
    public String deleteProductRegisById(@PathParam("id")
        String id) throws Exception {
        
        String url = Helper.paasURL + "ProductRegisRest/";
        String returnValue = Helper.callDeleteRest(url, id);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;


       

    }
    
    
    
    @GET
    @Path("getAllProductRegis")
    @Produces({"application/json"})
    public String getAllProductRegis() throws Exception {
        
        String url = Helper.paasURL + "getAllProdutRegis/";
        String returnValue = Helper.callGetRest(url);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;


       

    }
    
    
    @POST
    @Path("searchProductRegis")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchProductRegis(@FormParam("registerNumber") String registerNumber, @FormParam("supplierName") String supplierName, 
                                     @FormParam("siteRegistrationNumber") String siteRegistrationNumber, @FormParam("companyName") String companyName, 
                                     @FormParam("productName") String productName,  @FormParam("productRegisterStatus") String productRegisterStatus, @FormParam("nextRenewalFrom") String nextRenewalFrom,  @FormParam("nextRenewalTo") String nextRenewalTo) throws Exception {
        
        String url = Helper.paasURL + "searchProductRegis";
        String returnValue = Helper.callGetRestWithParms(url, registerNumber, supplierName, siteRegistrationNumber, companyName, productName, productRegisterStatus, nextRenewalFrom, nextRenewalTo);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        System.out.println("return+" + returnValue1 + "retun");
        return returnValue1;

    }
   
}
