/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.security.cert.CertificateException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 *
 * @author Anas Alghawi
 */


public class MainSaasSrevices {


    private String InstanceUrl = "https://ehxm.fa.us6.oraclecloud.com:443";
    private String bankAccountUrl =
        "//fscmRestApi/resources/11.13.18.05/cashBankAccounts";
    private String InvoiceUrl =
        "https://ehxm.fa.us6.oraclecloud.com/fscmRestApi/resources/11.13.18.05/invoices?q=ValidationStatus=%27Validated%27&onlyData=true";
    private String biReportUrl =
        "https://ehxm.fa.us6.oraclecloud.com/xmlpserver/services/PublicReportService";
    private String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public void setBiReportUrl(String biReportUrl) {
        this.biReportUrl = biReportUrl;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }

    public String getInvoiceUrl() {
        return InvoiceUrl;
    }

    public void setInvoiceUrl(String InvoiceUrl) {
        this.InvoiceUrl = InvoiceUrl;
    }

    public void setInstanceUrl(String InstanceUrl) {
        this.InstanceUrl = InstanceUrl;
    }

    public String getBankAccountUrl() {
        return bankAccountUrl;
    }

    public void setBankAccountUrl(String bankAccountUrl) {
        this.bankAccountUrl = bankAccountUrl;
    }

    public String getEmployeeUrl() {
        return InstanceUrl + employeeServiceUrl;
    }

    public String getEmployeeRestEndPointUrl() {
        return employeeServiceUrl;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }


}

