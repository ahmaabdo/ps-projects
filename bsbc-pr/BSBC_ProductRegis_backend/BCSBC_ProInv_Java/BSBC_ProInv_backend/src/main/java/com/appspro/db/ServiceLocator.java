/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.db;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

/**
 *
 * @author user
 */
public class ServiceLocator {
    private static InitialContext context;

    static {
        try {
            context = new InitialContext();
        } catch (NamingException ne) {
            System.err.println("Unable to create JNDI Initial Context : " +
                               ne.getMessage());
        }
    }

    public static DataSource getDataSource(String dsJndiName) throws NamingException {
        return (DataSource)context.lookup(dsJndiName);
    }
}
