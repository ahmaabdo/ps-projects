package com.appspro.bsbc_proinv.rest;


import com.appspro.helper.Helper;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

@Path("/productAttachments")
public class AttachmentRest {
    @POST
    @Path("/addAttachment")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String postAttachment(String bean) throws Exception {

           //String returnValue1 =   serviceProductRegistrationDAO.sendPost(bean);
        //     return bean ;
        String url = Helper.paasURL + "attachmentsRest/";
               String returnValue = Helper.callPostRest(url, bean);
               String returnValue1 = "{\"result\":\"sucess\"}";
               if (returnValue != null && !returnValue.isEmpty()) {
                   System.out.println("return+" + returnValue + "retun");
                   return returnValue;
               }
               System.out.println("return+" + returnValue1 + "retun");
               return returnValue1;
    }
    @GET
    @Path("getAttachment/{id}")
    @Produces({"application/json"})
    public Response getData(@PathParam("id") String id) {
        String url = Helper.paasURL + "attachmentsRestGet/"+id;
        String result = Helper.callGetRest(url);
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response.status(200).entity(result).type(MediaType.APPLICATION_JSON).build();

        }
    }
    
    @DELETE
    @Path("deleteAttachment/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String deleteAttachment(@PathParam("id") String id) throws Exception {

           //String returnValue1 =   serviceProductRegistrationDAO.sendPost(bean);
        //     return bean ;
        String url = Helper.paasURL +"attachmentsRestGet/"+id;
               String returnValue =  Helper.callDeleteRest(url, id);
               String returnValue1 = "{\"result\":\"sucess\"}";
               if (returnValue != null && !returnValue.isEmpty()) {
                   System.out.println("return+" + returnValue + "retun");
                   return returnValue;
               }
               System.out.println("return+" + returnValue1 + "retun");
               return returnValue1;
    }    

}
