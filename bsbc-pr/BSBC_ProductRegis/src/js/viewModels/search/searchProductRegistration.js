define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'ojs/ojmessages', 'ojs/ojknockout', 'promise', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojarraydataprovider', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojarraytabledatasource', 'ojs/ojdatetimepicker',
    'ojs/ojselectcombobox', 'ojs/ojtimezonedata', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojformlayout', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, keySet, services) {

            function guaranteeScreenModel() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.placeholderInput = ko.observable();
                self.placeholderselect = ko.observable();
                self.registerationNumber_Val = ko.observable();
                self.supplierName_Val = ko.observable();
                self.siteRegisterationNumber_Val = ko.observable();
                self.companyName_Val = ko.observable();
                self.productName_Val = ko.observable();
                self.searchdisable = ko.observable(false);
                self.nextRenewalToDisable = ko.observable(true);
                self.supllierNameList = ko.observableArray([]);



                self.searchBtnLbl = ko.observable();
                self.id = ko.observable();
                // Translation
                var getTranslation = oj.Translations.getTranslatedString;


                this.registerationStatus = ko.observableArray([
                    {label: 'Registration valid', value: 'valid'},
                    {label: 'Registration expired', value: 'expired'},
                    {label: 'Registration cancelled', value: 'cancelled'},
                    {label: 'Registration suspended', value: 'suspended'},
                    {label: 'Terminated', value: 'terminated'}
                ]);

                self.reloadlanguage = function ()
                {
                    var newlang = localStorage.getItem('setLang');
                    if (!newlang)
                    {
                        localStorage.setItem('lang', newlang);
                    }
                    switch (newlang) {
                        case 'arabic':
                            newLang = 'ar-sa';
                            break;
                        default:
                            newLang = 'en-US';
                    }
                    oj.Config.setLocale(newLang,
                            function () {
                                $('html').attr('lang', newLang);
                                if (newLang === 'ar-sa') {
                                    $('html').attr('dir', 'rtl');


                                    self.productRegisterLblsModel.registerNumberLbl(getTranslation("productRegister.registerNumber"));
                                    self.productRegisterLblsModel.registerDateLbl(getTranslation("productRegister.registerDate"));
                                    self.productRegisterLblsModel.registerTypeLbl(getTranslation("productRegister.registerType"));
                                    self.productRegisterLblsModel.productNameLbl(getTranslation("productRegister.productName"));
                                    self.productRegisterLblsModel.itemCodeLbl(getTranslation("productRegister.itemCode"));
                                    self.productRegisterLblsModel.supplierNameLbl(getTranslation("productRegister.supplierName"));
                                    self.productRegisterLblsModel.telephoneLbl(getTranslation("productRegister.telephone"));
                                    self.productRegisterLblsModel.addressLbl(getTranslation("productRegister.address"));
                                    self.productRegisterLblsModel.emailLbl(getTranslation("productRegister.email"));
                                    self.productRegisterLblsModel.titleLbl(getTranslation("productRegister.title"));
                                    self.productRegisterLblsModel.contactLbl(getTranslation("productRegister.contact"));
                                    self.productRegisterLblsModel.productRegisterStatusLbl(getTranslation("productRegister.productRegisterStatus"));
                                    self.productRegisterLblsModel.companyNameLbl(getTranslation("productRegister.companyName"));
                                    self.productRegisterLblsModel.registerLocationLbl(getTranslation("productRegister.registerLocation"));
                                    self.productRegisterLblsModel.countryOfOriginLbl(getTranslation("productRegister.countryOfOrigin"));
                                    self.productRegisterLblsModel.shelfLifeLbl(getTranslation("productRegister.shelfLife"));
                                    self.productRegisterLblsModel.storageCoditionLbl(getTranslation("productRegister.storageCodition"));
                                    self.productRegisterLblsModel.cifPriceLbl(getTranslation("productRegister.cifPrice"));
                                    self.productRegisterLblsModel.wholeSalePriceLbl(getTranslation("productRegister.wholeSalePrice"));
                                    self.productRegisterLblsModel.retailPriceLbl(getTranslation("productRegister.retailPrice"));
                                    self.productRegisterLblsModel.exchangeRateLbl(getTranslation("productRegister.retailPrice"));
                                    self.productRegisterLblsModel.currencyLbl(getTranslation("productRegister.currency"));
                                    self.productRegisterLblsModel.primaryPackagingLbl(getTranslation("productRegister.currency"));
                                    self.productRegisterLblsModel.mahNameLbl(getTranslation("productRegister.mahName"));
                                    self.productRegisterLblsModel.manufacturingNameLbl(getTranslation("productRegister.manufacturingName"));
                                    self.productRegisterLblsModel.indicationLbl(getTranslation("productRegister.indication"));
                                    self.productRegisterLblsModel.pharmaceuticalFormLbl(getTranslation("productRegister.pharmaceuticalForm"));
                                    self.productRegisterLblsModel.activeIngredientNameLbl(getTranslation("productRegister.activeIngredientName"));
                                    self.productRegisterLblsModel.relativeSubstancesLbl(getTranslation("productRegister.relativeSubstances"));
                                    self.productRegisterLblsModel.siteRegistrationNameLbl(getTranslation("productRegister.siteRegistrationName"));
                                    self.productRegisterLblsModel.siteRegistrationValidityLbl(getTranslation("productRegister.siteRegistrationValidity"));
                                    self.productRegisterLblsModel.siteRegistrationNumberLbl(getTranslation("productRegister.siteRegistrationNumber"));
                                    self.productRegisterLblsModel.lastRenewalLbl(getTranslation("productRegister.lastRenewal"));
                                    self.productRegisterLblsModel.nextRenewalLbl(getTranslation("productRegister.nextRenewal"));
                                    self.productRegisterLblsModel.secondaryPackage_releaseSiteLbl(getTranslation("productRegister.secondaryPackage_releaseSite"));



                                    self.columnArray([
                                        {"headerText": self.productRegisterLblsModel.registerNumberLbl(), "field": "register_number"},
                                        {"headerText": self.productRegisterLblsModel.registerDateLbl(), "field": "register_date"},
                                        {"headerText": self.productRegisterLblsModel.registerTypeLbl(), "field": "register_type"},
                                        {"headerText": self.productRegisterLblsModel.productNameLbl(), "field": "product_name"},
                                        {"headerText": self.productRegisterLblsModel.itemCodeLbl(), "field": "item_code"},
                                        {"headerText": self.productRegisterLblsModel.supplierNameLbl(), "field": "supplier_name"},
                                        {"headerText": self.productRegisterLblsModel.telephoneLbl(), "field": "telephone"},
                                        {"headerText": self.productRegisterLblsModel.addressLbl(), "field": "address"},
                                        {"headerText": self.productRegisterLblsModel.emailLbl(), "field": "email"},
                                        {"headerText": self.productRegisterLblsModel.titleLbl(), "field": "title"},
                                        {"headerText": self.productRegisterLblsModel.contactLbl(), "field": "contact"},
                                        {"headerText": self.productRegisterLblsModel.productRegisterStatusLbl(), "field": "product_registration_status"},
                                        {"headerText": self.productRegisterLblsModel.companyNameLbl(), "field": "company_name"},
                                        {"headerText": self.productRegisterLblsModel.registerLocationLbl(), "field": "register_location"},
                                        {"headerText": self.productRegisterLblsModel.countryOfOriginLbl(), "field": "country_of_origin"},
                                        {"headerText": self.productRegisterLblsModel.shelfLifeLbl(), "field": "shelf_life"},
                                        {"headerText": self.productRegisterLblsModel.storageCoditionLbl(), "field": "storage_codition"},
                                        {"headerText": self.productRegisterLblsModel.cifPriceLbl(), "field": "cif_price"},
                                        {"headerText": self.productRegisterLblsModel.wholeSalePriceLbl(), "field": "whole_sale_price"},
                                        {"headerText": self.productRegisterLblsModel.retailPriceLbl(), "field": "retail_price"},
                                        {"headerText": self.productRegisterLblsModel.exchangeRateLbl(), "field": "exchange_rate"},
                                        {"headerText": self.productRegisterLblsModel.currencyLbl(), "field": "currency"},
                                        {"headerText": self.productRegisterLblsModel.primaryPackagingLbl(), "field": "primary_packaging"},
                                        {"headerText": self.productRegisterLblsModel.mahNameLbl(), "field": "mah_name"},
                                        {"headerText": self.productRegisterLblsModel.manufacturingNameLbl(), "field": "manufacturing_name"},
                                        {"headerText": self.productRegisterLblsModel.indicationLbl(), "field": "indication"},
                                        {"headerText": self.productRegisterLblsModel.pharmaceuticalFormLbl(), "field": "pharamaceutical_form"},
                                        {"headerText": self.productRegisterLblsModel.activeIngredientNameLbl(), "field": "active_ingredient_name"},
                                        {"headerText": self.productRegisterLblsModel.relativeSubstancesLbl(), "field": "relative_substances"},
                                        {"headerText": self.productRegisterLblsModel.siteRegistrationNameLbl(), "field": "site_registration_name"},
                                        {"headerText": self.productRegisterLblsModel.siteRegistrationValidityLbl(), "field": "site_registration_validity"},
                                        {"headerText": self.productRegisterLblsModel.siteRegistrationNumberLbl(), "field": "site_registration_number"},
                                        {"headerText": self.productRegisterLblsModel.lastRenewalLbl(), "field": "last_renewal"},
                                        {"headerText": self.productRegisterLblsModel.nextRenewalLbl(), "field": "next_renewal"},
                                        {"headerText": self.productRegisterLblsModel.secondaryPackage_releaseSiteLbl(), "field": "secondary_package_release_site"}



                                    ]);

                                    self.searchBtnLbl(getTranslation("buttoms.searchBtn"));
                                    self.proRegisBtnsLblsModel.addBtnLbl(getTranslation("buttoms.addBtn"));
                                    self.proRegisBtnsLblsModel.editBtnLbl(getTranslation("buttoms.editBtn"));
                                    self.proRegisBtnsLblsModel.viewBtnLbl(getTranslation("buttoms.viewBtn"));
                                    self.proRegisBtnsLblsModel.deleteBtnLbl(getTranslation("buttoms.deleteBtn"));
                                    self.proRegisBtnsLblsModel.exportToExcel(getTranslation("buttoms.exportExcel"));
                                    self.proRegisBtnsLblsModel.exportToPdf(getTranslation("buttoms.exportPdf"));

                                    self.placeholderInput(getTranslation("common.inputPlaceholder"));
                                    self.placeholderselect(getTranslation("common.selectPlaceholder"));




                                } else {
                                    $('html').attr('dir', 'ltr');
                                    $('#enableBackButton').toggleClass('backButtonRotate');
                                    $('.action-menu').toggleClass('action-right');

                                }
                                self.productRegisterLblsModel.registerNumberLbl(getTranslation("productRegister.registerNumber"));
                                self.productRegisterLblsModel.registerDateLbl(getTranslation("productRegister.registerDate"));
                                self.productRegisterLblsModel.registerTypeLbl(getTranslation("productRegister.registerType"));
                                self.productRegisterLblsModel.productNameLbl(getTranslation("productRegister.productName"));
                                self.productRegisterLblsModel.itemCodeLbl(getTranslation("productRegister.itemCode"));
                                self.productRegisterLblsModel.supplierNameLbl(getTranslation("productRegister.supplierName"));
                                self.productRegisterLblsModel.telephoneLbl(getTranslation("productRegister.telephone"));
                                self.productRegisterLblsModel.addressLbl(getTranslation("productRegister.address"));
                                self.productRegisterLblsModel.emailLbl(getTranslation("productRegister.email"));
                                self.productRegisterLblsModel.titleLbl(getTranslation("productRegister.title"));
                                self.productRegisterLblsModel.contactLbl(getTranslation("productRegister.contact"));
                                self.productRegisterLblsModel.productRegisterStatusLbl(getTranslation("productRegister.productRegisterStatus"));
                                self.productRegisterLblsModel.companyNameLbl(getTranslation("productRegister.companyName"));
                                self.productRegisterLblsModel.registerLocationLbl(getTranslation("productRegister.registerLocation"));
                                self.productRegisterLblsModel.countryOfOriginLbl(getTranslation("productRegister.countryOfOrigin"));
                                self.productRegisterLblsModel.shelfLifeLbl(getTranslation("productRegister.shelfLife"));
                                self.productRegisterLblsModel.storageCoditionLbl(getTranslation("productRegister.storageCodition"));
                                self.productRegisterLblsModel.cifPriceLbl(getTranslation("productRegister.cifPrice"));
                                self.productRegisterLblsModel.wholeSalePriceLbl(getTranslation("productRegister.wholeSalePrice"));
                                self.productRegisterLblsModel.retailPriceLbl(getTranslation("productRegister.retailPrice"));
                                self.productRegisterLblsModel.exchangeRateLbl(getTranslation("productRegister.exchangeRate"));
                                self.productRegisterLblsModel.currencyLbl(getTranslation("productRegister.currency"));
                                self.productRegisterLblsModel.primaryPackagingLbl(getTranslation("productRegister.primaryPackaging"));
                                self.productRegisterLblsModel.mahNameLbl(getTranslation("productRegister.mahName"));
                                self.productRegisterLblsModel.manufacturingNameLbl(getTranslation("productRegister.manufacturingName"));
                                self.productRegisterLblsModel.indicationLbl(getTranslation("productRegister.indication"));
                                self.productRegisterLblsModel.pharmaceuticalFormLbl(getTranslation("productRegister.pharmaceuticalForm"));
                                self.productRegisterLblsModel.activeIngredientNameLbl(getTranslation("productRegister.activeIngredientName"));
                                self.productRegisterLblsModel.relativeSubstancesLbl(getTranslation("productRegister.relativeSubstances"));
                                self.productRegisterLblsModel.siteRegistrationNameLbl(getTranslation("productRegister.siteRegistrationName"));
                                self.productRegisterLblsModel.siteRegistrationValidityLbl(getTranslation("productRegister.siteRegistrationValidity"));
                                self.productRegisterLblsModel.siteRegistrationNumberLbl(getTranslation("productRegister.siteRegistrationNumber"));
                                self.productRegisterLblsModel.lastRenewalLbl(getTranslation("productRegister.lastRenewal"));
                                self.productRegisterLblsModel.nextRenewalLbl(getTranslation("productRegister.nextRenewal"));
                                self.productRegisterLblsModel.secondaryPackage_releaseSiteLbl(getTranslation("productRegister.secondaryPackage_releaseSite"));
                                self.productRegisterLblsModel.resetBtnLbl(getTranslation("productRegister.reset"));

                                self.columnArray([
                                    {"headerText": self.productRegisterLblsModel.registerNumberLbl(), "field": "register_number"},
                                    {"headerText": self.productRegisterLblsModel.registerDateLbl(), "field": "register_date"},
                                    {"headerText": self.productRegisterLblsModel.registerTypeLbl(), "field": "register_type"},
                                    {"headerText": self.productRegisterLblsModel.productNameLbl(), "field": "product_name"},
                                    {"headerText": self.productRegisterLblsModel.itemCodeLbl(), "field": "item_code"},
                                    {"headerText": self.productRegisterLblsModel.supplierNameLbl(), "field": "supplier_name"},
                                    {"headerText": self.productRegisterLblsModel.telephoneLbl(), "field": "telephone"},
                                    {"headerText": self.productRegisterLblsModel.addressLbl(), "field": "address"},
                                    {"headerText": self.productRegisterLblsModel.emailLbl(), "field": "email"},
                                    {"headerText": self.productRegisterLblsModel.titleLbl(), "field": "title"},
                                    {"headerText": self.productRegisterLblsModel.contactLbl(), "field": "contact"},
                                    {"headerText": self.productRegisterLblsModel.productRegisterStatusLbl(), "field": "product_registration_status"},
                                    {"headerText": self.productRegisterLblsModel.companyNameLbl(), "field": "company_name"},
                                    {"headerText": self.productRegisterLblsModel.registerLocationLbl(), "field": "register_location"},
                                    {"headerText": self.productRegisterLblsModel.countryOfOriginLbl(), "field": "country_of_origin"},
                                    {"headerText": self.productRegisterLblsModel.shelfLifeLbl(), "field": "shelf_life"},
                                    {"headerText": self.productRegisterLblsModel.storageCoditionLbl(), "field": "storage_codition"},
                                    {"headerText": self.productRegisterLblsModel.cifPriceLbl(), "field": "cif_price"},
                                    {"headerText": self.productRegisterLblsModel.wholeSalePriceLbl(), "field": "whole_sale_price"},
                                    {"headerText": self.productRegisterLblsModel.retailPriceLbl(), "field": "retail_price"},
                                    {"headerText": self.productRegisterLblsModel.exchangeRateLbl(), "field": "exchange_rate"},
                                    {"headerText": self.productRegisterLblsModel.currencyLbl(), "field": "currency"},
                                    {"headerText": self.productRegisterLblsModel.primaryPackagingLbl(), "field": "primary_packaging"},
                                    {"headerText": self.productRegisterLblsModel.mahNameLbl(), "field": "mah_name"},
                                    {"headerText": self.productRegisterLblsModel.manufacturingNameLbl(), "field": "manufacturing_name"},
                                    {"headerText": self.productRegisterLblsModel.indicationLbl(), "field": "indication"},
                                    {"headerText": self.productRegisterLblsModel.pharmaceuticalFormLbl(), "field": "pharamaceutical_form"},
                                    {"headerText": self.productRegisterLblsModel.activeIngredientNameLbl(), "field": "active_ingredient_name"},
                                    {"headerText": self.productRegisterLblsModel.relativeSubstancesLbl(), "field": "relative_substances"},
                                    {"headerText": self.productRegisterLblsModel.siteRegistrationNumberLbl(), "field": "site_registration_number"},
                                    {"headerText": self.productRegisterLblsModel.siteRegistrationValidityLbl(), "field": "site_registration_validity"},
//                                    {"headerText": self.productRegisterLblsModel.siteRegistrationNumberLbl(), "field": "site_registration_validity"},
                                    {"headerText": self.productRegisterLblsModel.lastRenewalLbl(), "field": "last_renewal"},
                                    {"headerText": self.productRegisterLblsModel.nextRenewalLbl(), "field": "next_renewal"},
                                    {"headerText": self.productRegisterLblsModel.secondaryPackage_releaseSiteLbl(), "field": "secondary_package_release_site"}



                                ]);


                                self.searchBtnLbl(getTranslation("buttoms.searchBtn"));
                                self.proRegisBtnsLblsModel.addBtnLbl(getTranslation("buttoms.addBtn"));
                                self.proRegisBtnsLblsModel.editBtnLbl(getTranslation("buttoms.editBtn"));
                                self.proRegisBtnsLblsModel.viewBtnLbl(getTranslation("buttoms.viewBtn"));
                                self.proRegisBtnsLblsModel.deleteBtnLbl(getTranslation("buttoms.deleteBtn"));
                                self.proRegisBtnsLblsModel.exportToExcel(getTranslation("buttoms.exportExcel"));
                                self.proRegisBtnsLblsModel.exportToPdf(getTranslation("buttoms.exportPdf"));


                                self.placeholderInput(getTranslation("common.inputPlaceholder"));
                                self.placeholderselect(getTranslation("common.selectPlaceholder"));

                            }
                    );



                };

                self.comboBoxValue = ko.observable("dd/MM/yy");
                self.datePicker = {
                    changeMonth: 'none',
                    changeYear: 'none'
                };
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).
                        createConverter(
                                {
                                    pattern: "dd/MM/yy"
                                }));

                self.proRegisBtnsLblsModel = {
                    addBtnLbl: ko.observable(),
                    editBtnLbl: ko.observable(),
                    viewBtnLbl: ko.observable(),
                    deleteBtnLbl: ko.observable(),
                    exportToExcel: ko.observable(),
                    exportToPdf: ko.observable()
                };
                self.productRegisterLblsModel = {
                    registerNumberLbl: ko.observable(),
                    registerDateLbl: ko.observable(),
                    registerTypeLbl: ko.observable(),
                    productNameLbl: ko.observable(),
                    itemCodeLbl: ko.observable(),
                    supplierNameLbl: ko.observable(),
                    telephoneLbl: ko.observable(),
                    addressLbl: ko.observable(),
                    emailLbl: ko.observable(),
                    titleLbl: ko.observable(),
                    contactLbl: ko.observable(),
                    productRegisterStatusLbl: ko.observable(),
                    companyNameLbl: ko.observable(),
                    registerLocationLbl: ko.observable(),
                    countryOfOriginLbl: ko.observable(),
                    shelfLifeLbl: ko.observable(),
                    storageCoditionLbl: ko.observable(),
                    cifPriceLbl: ko.observable(),
                    wholeSalePriceLbl: ko.observable(),
                    retailPriceLbl: ko.observable(),
                    exchangeRateLbl: ko.observable(),
                    currencyLbl: ko.observable(),
                    primaryPackagingLbl: ko.observable(),
                    mahNameLbl: ko.observable(),
                    manufacturingNameLbl: ko.observable(),
                    indicationLbl: ko.observable(),
                    pharmaceuticalFormLbl: ko.observable(),
                    activeIngredientNameLbl: ko.observable(),
                    relativeSubstancesLbl: ko.observable(),
                    siteRegistrationNameLbl: ko.observable(),
                    siteRegistrationValidityLbl: ko.observable(),
                    siteRegistrationNumberLbl: ko.observable(),
                    lastRenewalLbl: ko.observable(),
                    nextRenewalLbl: ko.observable(),
                    secondaryPackage_releaseSiteLbl: ko.observable(),
                    resetBtnLbl: ko.observable()
                };

                self.productRegisterModel = {
                    registerNumber: ko.observable(),
                    supplierName: ko.observable(),
                    siteRegistrationNumber: ko.observable(),
                    companyName: ko.observable(),
                    productName: ko.observable(),
                    nextRenewalFrom: ko.observable(),
                    nextRenewalTo: ko.observable(),
                    productRegisterStatus: ko.observable()


                };

                self.searchtoggle = function () {

                    if (self.productRegisterModel.registerNumber() || self.productRegisterModel.supplierName()
                            || self.productRegisterModel.siteRegistrationNumber() || self.productRegisterModel.companyName()
                            || self.productRegisterModel.productName() || self.productRegisterModel.productRegisterStatus()) {
                        self.searchdisable(false);
                    } else {

                        self.searchdisable(true);
                    }

                    if (self.productRegisterModel.nextRenewalFrom()) {
                        self.nextRenewalToDisable(false);
                        document.getElementById('notificationMessages');
                    }
                    if (self.productRegisterModel.nextRenewalTo()) {
                        self.searchdisable(false);
                    }
                    if (self.productRegisterModel.nextRenewalFrom() === null) {
                        self.nextRenewalToDisable(true);
                    }

                };

                self.nextRenewalFromChangeHandler = function () {


                };


                // define and Handle table

                var tabledate;

                self.columnArray = ko.observableArray([]);
                var deptArray = [];
                self.oprationdisabled = ko.observable(true);
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.productRegisDataArr = ko.observableArray(deptArray);
                // self.dataprovider = new oj.ArrayDataProvider(self.productRegisDataArr, {idAttribute: 'id'});
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.productRegisDataArr, {idAttribute: 'id'}));
                //self.excelDisabled=ko.observable(true);
                self.fileDisabled = ko.observable(true);

                this.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    self.oprationdisabled(false);
                    self.selectedRowKey(currentRow['rowKey']);
                    self.selectedIndex(currentRow['rowIndex']);


                    if (currentRow['rowKey'] === null) {

                        console.log('RowKey is Null!!');
                    }


                };


                self.handleRowKeyOfNull = function () {


                };
                self.handleReset = function () {
                    self.productRegisterModel.productName('');
                    self.productRegisterModel.companyName('');
                    self.productRegisterModel.siteRegistrationNumber('');
                    self.productRegisterModel.supplierName('');
                    self.productRegisterModel.registerNumber('');
                    self.productRegisterModel.nextRenewalFrom('');
                    self.productRegisterModel.nextRenewalTo('');
                    self.productRegisterModel.productRegisterStatus('');
                    self.fileDisabled(true);
                    self.productRegisDataArr([]);
                    self.searchdisable(true);
                    self.nextRenewalToDisable(true);

                };


                self.handleSearch = function () {
                    //var jsonData = ko.toJSON(self.productRegisterModel);
                    var searchProductRegisCbFn = function (data) {
                        if (data.length !== 0) {
                            for (var i = 0; i < data.items.length; i++) {

                                var registerDateFormate = data.items[i].register_date;
                                var lastRenewal = data.items[i].last_renewal;
                                var nextRenewal = data.items[i].next_renewal;

                                data.items[i].register_date = formatDate(new Date(registerDateFormate));
                                data.items[i].register_date = formatDate(new Date(data.items[i].register_date));
                                //data.items[i].site_registration_validity = formatDate(new Date(data.items[i].site_registration_validity));
                                data.items[i].last_renewal = formatDate(new Date(lastRenewal));
                                data.items[i].last_renewal = formatDate(new Date(data.items[i].last_renewal));
                                data.items[i].next_renewal = formatDate(new Date(nextRenewal));
                                data.items[i].next_renewal = formatDate(new Date(data.items[i].next_renewal));

                            }
                            self.productRegisDataArr(data.items);

                            tabledate = data.items;




                            //self.excelDisabled(false);
                            //self.fileDisabled(true);

                            if (self.productRegisDataArr().length != 0)
                            {
                                self.fileDisabled(false);
                            } else {
                                self.fileDisabled(true);
                            }

                        } else {

                            self.productRegisDataArr([]);
                        }
                    };

                    var searhfailCbFn = function () {
                    };
                    var searchURL = "ProductRegisRest/searchProductRegis";
                    services.searcheGeneric(searchURL, self.productRegisterModel).then(searchProductRegisCbFn, searhfailCbFn);

                };


                self.handleAdd = function () {
                    oj.Router.rootInstance.store('add');
                    oj.Router.rootInstance.go('productRegistration');
                };
                // var storedData;
                self.handleEdit = function () {

                    self.productRegisDataArr()[self.selectedIndex()].type = "update";
                    oj.Router.rootInstance.store(self.productRegisDataArr()[self.selectedIndex()]);

                    //self.productRegisDataArr()[self.selectedIndex()].type = "update";
//                    storedData = {
//                        type: self.productRegisDataArr()[self.selectedIndex()].type,
//                        selectedID: self.productRegisDataArr()[self.selectedIndex()].id
//                    };

                    //console.log(JSON.stringify(storedData));
                    //oj.Router.rootInstance.store(storedData);
                    oj.Router.rootInstance.go('productRegistration');
                };

                self.handleview = function () {

                    self.productRegisDataArr()[self.selectedIndex()].type = "view";
                    oj.Router.rootInstance.store(self.productRegisDataArr()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('productRegistration');
                };

                self.handleDelete = function () {

                    self.id(self.productRegisDataArr()[self.selectedIndex()].id);
                    self.deleteRow(self.id());
                };
                self.handleExportExcel = function () {

                    var registerNumber = typeof self.productRegisterModel.registerNumber() === 'undefined' ? registerNumber = '' : registerNumber = self.productRegisterModel.registerNumber();
                    var supplierName = typeof self.productRegisterModel.supplierName() === 'undefined' ? supplierName = '' : supplierName = self.productRegisterModel.supplierName();
                    var siteRegistrationNumber = typeof self.productRegisterModel.siteRegistrationNumber() === 'undefined' ? siteRegistrationNumber = '' : siteRegistrationNumber = self.productRegisterModel.siteRegistrationNumber();
                    var companyName = typeof self.productRegisterModel.companyName() === 'undefined' ? companyName = '' : companyName = self.productRegisterModel.companyName();
                    var productName = typeof self.productRegisterModel.productName() === 'undefined' ? productName = '' : productName = self.productRegisterModel.productName();
                    var queryParam = "?registerationNumber=" + registerNumber +
                            "&supplierName=" + supplierName +
                            "&siteRegisterationNumber=" + siteRegistrationNumber +
                            "&companyName=" + companyName +
                            "&productName=" + productName;
                    var urlPath = "SheetRest/excelreport";
                    location.href = services.getRestPath() + urlPath + queryParam;
                };
                self.handleExportPdf = function () {

                };


                // Call Delete Web service
                self.deleteRow = function (id) {

                    var deleteRowCbFn = function (data) {

                        self.deleteRowDetails(self.id());


                    };
                    var failCbFn = function () {
                        console.log("Failed");
                    };

                    var serviceName = "ProductRegisRest/deleteProductRegis/" + id;
                    services.deleteGeneric(serviceName).then(deleteRowCbFn, failCbFn);
                };


                // Call Delete Web service
                self.deleteRowDetails = function (id) {

                    var deleteRowCbFn = function (data) {

                        self.getAllProductRegis();

                    };
                    var failCbFn = function () {
                        console.log("Failed");
                    };

                    var serviceName = "ProductRegisDetailsRest/deleteProductRegisDetails/" + id;
                    services.deleteGeneric(serviceName).then(deleteRowCbFn, failCbFn);
                };




//                self.formatDate = function (date) {
//                    //var date = new Date()
//                    var month = '' + (date.getMonth() + 1),
//                            day = '' + date.getDate(),
//                            year = date.getFullYear();
//
//                    if (month.length < 2)
//                        month = '0' + month;
//                    if (day.length < 2)
//                        day = '0' + day;
//
//                    return [year, month, day].join('/');
//                };
                function formatDate(date) {
                    var d = new Date(date),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }

//var dateFormat = require('dateformat');
//var now = new Date();
//dateFormat(now, "MM/dd/yy");


                self.getAllProductRegis = function () {

                    var productRegisCbFn = function (data) {



                        if (data.length !== 0) {
                            for (var i = 0; i < data.items.length; i++) {

                                //var registerDateFormate = data.items[i].register_date;

                                //data.items[i].register_date = self.formatDate(new Date(registerDateFormate));
//                                 data.items[i].register_date = formatDate(new Date(data.items[i].register_date));
//                                 data.items[i].site_registration_validity = formatDate(new Date(data.items[i].site_registration_validity));
//                                 data.items[i].last_renewal = formatDate(new Date(data.items[i].last_renewal));
//                                 data.items[i].next_renewal = formatDate(new Date(data.items[i].next_renewal));

                            }
                            self.productRegisDataArr(data.items);

                            //  $.each(data.items, function (index) {

//                                self.productRegisDataArr.push({
//
//                                    register_number: [data.items[index].register_number],
//                                    register_date: [data.items[index].register_date],
//                                    register_type: [data.items[index].register_type],
//                                    product_name: [data.items[index].product_name],
//                                    item_code: [data.items[index].item_code],
//                                    supplier_name: [data.items[index].supplier_name],
//                                    telephone: [data.items[index].telephone],
//                                    address: [data.items[index].address]
//                                });

                            // self.productRegisDataArr.push(data.items);

                            //  });
                        } else {

                            //self.productRegisDataArr([]);
                        }
                    };
                    var failCbFn = function () {

                    };
                    var service = "ProductRegisRest/getAllProductRegis";
                    services.getGeneric(service).then(productRegisCbFn, failCbFn);
                };




                // Handle PDF File Start 



                self.handlePDF = function () {

                    var PDFFileCbFn = function (data) {

                        var a = document.createElement('a');
                        var url = "data:application/pdf;base64," + data;
                        a.href = url;
                        a.download = 'BSBC-DATA.pdf';
                        a.click();
                        window.URL.revokeObjectURL(url);

                        document.body.removeChild(a);
                    };
                    var failCbFn = function () {

                    };
                    var service = "getFiles/get";
                    services.addGeneric(service, tabledate).then(PDFFileCbFn, failCbFn);
                };


                // Handle PDF fILE End 

                self.getSupplerName = function () {
                    var paraPayload = {
                        "reportName": "SUPPLIER_NAME"
                    };
                    var getSupplerNameCbFn = function (data) {

                        if (data.length != 0) {
                            data = JSON.parse(data);

                            $.each(data, function (index) {

                                self.supllierNameList.push({
                                    value: data[index].SUPPLIERID,
                                    label: data[index].SUPPLIERNAME,
                                    ADDRESSLINE1: data[index].ADDRESSLINE1,
                                    ADDRESSLINE2: data[index].ADDRESSLINE2,
                                    CITY: data[index].CITY,
                                    PHONE: data[index].PHONE

                                });
                            });
                        }

                    };
                    var failCbFn = function () {
                    };
                    var UrlPath = "report/commonbireport";
                    services.addGeneric(UrlPath, paraPayload).then(getSupplerNameCbFn, failCbFn);
                };



                self.connected = function () {
                    //self.getAllProductRegis ();
                    self.oprationdisabled(true);
                    self.productRegisterModel.supplierName('');
                    self.productRegisterModel.registerNumber('');
                    self.productRegisterModel.siteRegistrationNumber('');
                    self.productRegisterModel.companyName('');
                    self.productRegisterModel.productName('');
                    self.productRegisterModel.nextRenewalFrom('');
                    self.productRegisterModel.nextRenewalTo('');
                    self.productRegisterModel.productRegisterStatus('');
                    self.searchdisable(true);
                    self.getSupplerName();
                };
                self.disconnected = function () {
                    self.productRegisDataArr([]);
                    self.productRegisterModel.supplierName('');
                    self.searchdisable(false);
                    //self.supllierNameList([]);
                };
                self.transitionCompleted = function () {
                    // Implement if needed
                };



            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new guaranteeScreenModel();
        }
);