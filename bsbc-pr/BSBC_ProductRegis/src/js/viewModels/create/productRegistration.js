define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojkeyset', 'config/services', 'ojs/ojknockout', 'promise', 'ojs/ojlistview', 'ojs/ojlabel', 'ojs/ojpopup', 'ojs/ojinputtext', 'ojs/ojselectcombobox',
    'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojcheckboxset', 'ojs/ojbutton', 'ojs/ojarraydataprovider',
    'ojs/ojnavigationlist', 'ojs/ojpagingtabledatasource', 'ojs/ojarraytabledatasource',
    'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojtrain', 'ojs/ojdialog', 'ojs/ojtable',
    'ojs/ojpopup', 'ojs/ojfilepicker', 'ojs/ojswitcher', 'ojs/ojvalidationgroup', 'ojs/ojinputnumber', , 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojfilepicker'],
        function (oj, ko, $, keySet, services) {



            function guaranteeScreenModel() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.attachmentData = ko.observableArray();
                self.dummayData = ko.observableArray();
                self.supplierNameValue = ko.observableArray();
                //****************************************************************************
                self.columnArrayNewRegistrationHistory = ko.observableArray();
                self.columnArrayVairationHistory = ko.observableArray();
                self.columnArrayRenewalHistory = ko.observableArray();
                self.columnArrayProductClassificationHistory = ko.observableArray();
                self.columnArrayReferenceStandardAvailabilityHistory = ko.observableArray();
                self.columnArrayGeneralDocumentsHistory = ko.observableArray();

                //****************************************************************************
                self.placeholderInput = ko.observable();
                self.placeholderselect = ko.observable();

                self.serviceType = ko.observable();
                self.submitBtnLbl = ko.observable();
                self.cancleBtnLbl = ko.observable();
                self.uploadBtnVisible = ko.observable(false);
                self.isDisabled = ko.observable();
                self.isDisabledContacts = ko.observable();
                self.isDisabledSite = ko.observable();
                self.isDisabledHandleSubmit = ko.observable();

                self.addBtnVisible = ko.observable();
                self.saveBtnVisible = ko.observable(true);
                self.prevBtnVisible = ko.observable(false);
                self.cancleBtnVisible = ko.observable();
                self.varName = ko.observable();
                self.suppliernamedisabled = ko.observable();
                var contactfieldscount = 0;
                var sitefieldscount = 0;
                self.resumeAttachmentLbl = ko.observable();
                self.attachmentTypeLbl = ko.observable();
//                self.viewAttachmentBtnVisible = ko.observable(false);
//                self.viewAttachmentBtnVisible2 = ko.observable(false);
//                self.viewAttachmentBtnVisible3 = ko.observable(false);
//                self.viewAttachmentBtnVisible4 = ko.observable(false);
//                self.viewAttachmentBtnVisible5 = ko.observable(false);
//                self.viewAttachmentBtnVisible6 = ko.observable(false);
                self.attachmentBtnVisible = ko.observable(false);
                self.downloadLbl = ko.observable();
                var dataFiles = {};
                self.registrationArray = ko.observableArray([]);
                self.renwalArray = ko.observableArray([]);
                self.siteArray = ko.observableArray([]);
                //////////
                self.vairationRegistrationArray = ko.observableArray([]);
                self.vairationRenwalArray = ko.observableArray([]);
                self.vairationSiteArray = ko.observableArray([]);
                /////////////
                self.renewalRegistrationArray = ko.observableArray([]);
                self.renewalRenwalArray = ko.observableArray([]);
                self.renewalSiteArray = ko.observableArray([]);
                ////////////////////////
                self.productClassificationRegistrationArray = ko.observableArray([]);
                self.productClassificationRenwalArray = ko.observableArray([]);
                self.productClassificationSiteArray = ko.observableArray([]);
                //////////////////////////
                self.referenceStandardAvailabilityRegistrationArray = ko.observableArray([]);
                self.referenceStandardAvailabilityRenwalArray = ko.observableArray([]);
                self.referenceStandardAvailabilitySiteArray = ko.observableArray([]);
                //////////////////////
                self.generalDocumentsRegistrationArray = ko.observableArray([]);
                self.generalDocumentsRenwalArray = ko.observableArray([]);
                self.generalDocumentsSiteArray = ko.observableArray([]);
                /////////////////////////
                self.attachmentID = ko.observable();
                //create file upload data provider
                self.koArrayRemoved = ko.observableArray([]);
                self.koArray = ko.observableArray([]);
                self.variationKoArray = ko.observableArray([]);
                self.renewalKoArray = ko.observableArray([]);
                self.productClassificationKoArray = ko.observableArray([]);
                self.refereceStandardKoArray = ko.observableArray([]);
                self.generalDocumentKoArray = ko.observableArray([]);
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.variationDataProviderAttachment = new oj.ArrayDataProvider(self.variationKoArray, {keyAttributes: 'id'});
                self.renewalDataProviderAttachment = new oj.ArrayDataProvider(self.renewalKoArray, {keyAttributes: 'id'});
                self.productClassificationDataProviderAttachment = new oj.ArrayDataProvider(self.productClassificationKoArray, {keyAttributes: 'id'});
                self.refereceStandardDataProviderAttachment = new oj.ArrayDataProvider(self.refereceStandardKoArray, {keyAttributes: 'id'});
                self.generalDocumentDataProviderAttachment = new oj.ArrayDataProvider(self.generalDocumentKoArray, {keyAttributes: 'id'});
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.vairationSelectedItemsAttachment = ko.observableArray([]);
                self.renewalSelectedItemsAttachment = ko.observableArray([]);
                self.productClassificationSelectedItemsAttachment = ko.observableArray([]);
                self.referenceStandardAvailabilitySelectedItemsAttachment = ko.observableArray([]);
                self.generalDocumentsSelectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.AttachmentsBtnVisible = ko.observable(true);
                self.UploadFileLbl = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.fileUploadStatusLbl = ko.observable();
                self.attachmentLbl = ko.observable();
                self.cancel = ko.observable();
                self.registrationCertificate = ko.observable();
                self.renewalCertificate = ko.observable();
                self.siteCertificate = ko.observable();
                self.typeNameArray = ko.observableArray([]);
                self.saveBtnLbl = ko.observable();
                self.prevBtnLbl = ko.observable();

                this.currency = ko.observableArray([
                    {"label": "AED", "value": "UAE dirham"},
                    {"label": "AFN", "value": "Afghan afghani"},
                    {"label": "ALL", "value": "Albanian lek"},
                    {"label": "AMD", "value": "Armenian dram"},
                    {"label": "ANG", "value": "Netherlands Antillean gulden"},
                    {"label": "AOA", "value": "Angolan kwanza"},
                    {"label": "ARS", "value": "Argentine peso"},
                    {"label": "AUD", "value": "Australian dollar"},
                    {"label": "AWG", "value": "Aruban florin"},
                    {"label": "AZN", "value": "Azerbaijani manat"},
                    {"label": "BAM", "value": "Bosnia and Herzegovina konvertibilna marka"},
                    {"label": "BBD", "value": "Barbadian dollar"},
                    {"label": "BDT", "value": "Bangladeshi taka"},
                    {"label": "BGN", "value": "Bulgarian lev"},
                    {"label": "BHD", "value": "Bahraini dinar"},
                    {"label": "BIF", "value": "Burundi franc"},
                    {"label": "BMD", "value": "Bermudian dollar"},
                    {"label": "BND", "value": "Brunei dollar"},
                    {"label": "BOB", "value": "Bolivian boliviano"},
                    {"label": "BRL", "value": "Brazilian real"},
                    {"label": "BSD", "value": "Bahamian dollar"},
                    {"label": "BTN", "value": "Bhutanese ngultrum"},
                    {"label": "BWP", "value": "Botswana pula"},
                    {"label": "BYR", "value": "Belarusian ruble"},
                    {"label": "BZD", "value": "Belize dollar"},
                    {"label": "CAD", "value": "Canadian dollar"},
                    {"label": "CDF", "value": "Congolese franc"},
                    {"label": "CHF", "value": "Swiss franc"},
                    {"label": "CLP", "value": "Chilean peso"},
                    {"label": "CNY", "value": "Chinese/Yuan renminbi"},
                    {"label": "COP", "value": "Colombian peso"},
                    {"label": "CRC", "value": "Costa Rican colon"},
                    {"label": "CUC", "value": "Cuban peso"},
                    {"label": "CVE", "value": "Cape Verdean escudo"},
                    {"label": "CZK", "value": "Czech koruna"},
                    {"label": "DJF", "value": "Djiboutian franc"},
                    {"label": "DKK", "value": "Danish krone"},
                    {"label": "DOP", "value": "Dominican peso"},
                    {"label": "DZD", "value": "Algerian dinar"},
                    {"label": "EEK", "value": "Estonian kroon"},
                    {"label": "EGP", "value": "Egyptian pound"},
                    {"label": "ERN", "value": "Eritrean nakfa"},
                    {"label": "ETB", "value": "Ethiopian birr"},
                    {"label": "EUR", "value": "European Euro"},
                    {"label": "FJD", "value": "Fijian dollar"},
                    {"label": "FKP", "value": "Falkland Islands pound"},
                    {"label": "GBP", "value": "British pound"},
                    {"label": "GEL", "value": "Georgian lari"},
                    {"label": "GHS", "value": "Ghanaian cedi"},
                    {"label": "GIP", "value": "Gibraltar pound"},
                    {"label": "GMD", "value": "Gambian dalasi"},
                    {"label": "GNF", "value": "Guinean franc"},
                    {"label": "GQE", "value": "Central African CFA franc"},
                    {"label": "GTQ", "value": "Guatemalan quetzal"},
                    {"label": "GYD", "value": "Guyanese dollar"},
                    {"label": "HKD", "value": "Hong Kong dollar"},
                    {"label": "HNL", "value": "Honduran lempira"},
                    {"label": "HRK", "value": "Croatian kuna"},
                    {"label": "HTG", "value": "Haitian gourde"},
                    {"label": "HUF", "value": "Hungarian forint"},
                    {"label": "IDR", "value": "Indonesian rupiah"},
                    {"label": "ILS", "value": "Israeli new sheqel"},
                    {"label": "INR", "value": "Indian rupee"},
                    {"label": "IQD", "value": "Iraqi dinar"},
                    {"label": "IRR", "value": "Iranian rial"},
                    {"label": "ISK", "value": "Icelandic kr\u00f3na"},
                    {"label": "JMD", "value": "Jamaican dollar"},
                    {"label": "JOD", "value": "Jordanian dinar"},
                    {"label": "JPY", "value": "Japanese yen"},
                    {"label": "KES", "value": "Kenyan shilling"},
                    {"label": "KGS", "value": "Kyrgyzstani som"},
                    {"label": "KHR", "value": "Cambodian riel"},
                    {"label": "KMF", "value": "Comorian franc"},
                    {"label": "KPW", "value": "North Korean won"},
                    {"label": "KRW", "value": "South Korean won"},
                    {"label": "KWD", "value": "Kuwaiti dinar"},
                    {"label": "KYD", "value": "Cayman Islands dollar"},
                    {"label": "KZT", "value": "Kazakhstani tenge"},
                    {"label": "LAK", "value": "Lao kip"},
                    {"label": "LBP", "value": "Lebanese lira"},
                    {"label": "LKR", "value": "Sri Lankan rupee"},
                    {"label": "LRD", "value": "Liberian dollar"},
                    {"label": "LSL", "value": "Lesotho loti"},
                    {"label": "LTL", "value": "Lithuanian litas"},
                    {"label": "LVL", "value": "Latvian lats"},
                    {"label": "LYD", "value": "Libyan dinar"},
                    {"label": "MAD", "value": "Morolabelan dirham"},
                    {"label": "MDL", "value": "Moldovan leu"},
                    {"label": "MGA", "value": "Malagasy ariary"},
                    {"label": "MKD", "value": "Macedonian denar"},
                    {"label": "MMK", "value": "Myanma kyat"},
                    {"label": "MNT", "value": "Mongolian tugrik"},
                    {"label": "MOP", "value": "Macanese pataca"},
                    {"label": "MRO", "value": "Mauritanian ouguiya"},
                    {"label": "MUR", "value": "Mauritian rupee"},
                    {"label": "MVR", "value": "Maldivian rufiyaa"},
                    {"label": "MWK", "value": "Malawian kwacha"},
                    {"label": "MXN", "value": "Mexican peso"},
                    {"label": "MYR", "value": "Malaysian ringgit"},
                    {"label": "MZM", "value": "Mozambican metical"},
                    {"label": "NAD", "value": "Namibian dollar"},
                    {"label": "NGN", "value": "Nigerian naira"},
                    {"label": "NIO", "value": "Nicaraguan c\u00f3rdoba"},
                    {"label": "NOK", "value": "Norwegian krone"},
                    {"label": "NPR", "value": "Nepalese rupee"},
                    {"label": "NZD", "value": "New Zealand dollar"},
                    {"label": "OMR", "value": "Omani rial"},
                    {"label": "PAB", "value": "Panamanian balboa"},
                    {"label": "PEN", "value": "Peruvian nuevo sol"},
                    {"label": "PGK", "value": "Papua New Guinean kina"},
                    {"label": "PHP", "value": "Philippine peso"},
                    {"label": "PKR", "value": "Pakistani rupee"},
                    {"label": "PLN", "value": "Polish zloty"},
                    {"label": "PYG", "value": "Paraguayan guarani"},
                    {"label": "QAR", "value": "Qatari riyal"},
                    {"label": "RON", "value": "Romanian leu"},
                    {"label": "RSD", "value": "Serbian dinar"},
                    {"label": "RUB", "value": "Russian ruble"},
                    {"label": "SAR", "value": "Saudi riyal"},
                    {"label": "SBD", "value": "Solomon Islands dollar"},
                    {"label": "SCR", "value": "Seychellois rupee"},
                    {"label": "SDG", "value": "Sudanese pound"},
                    {"label": "SEK", "value": "Swedish krona"},
                    {"label": "SGD", "value": "Singapore dollar"},
                    {"label": "SHP", "value": "Saint Helena pound"},
                    {"label": "SLL", "value": "Sierra Leonean leone"},
                    {"label": "SOS", "value": "Somali shilling"},
                    {"label": "SRD", "value": "Surivaluese dollar"},
                    {"label": "SYP", "value": "Syrian pound"},
                    {"label": "SZL", "value": "Swazi lilangeni"},
                    {"label": "THB", "value": "Thai baht"},
                    {"label": "TJS", "value": "Tajikistani somoni"},
                    {"label": "TMT", "value": "Turkmen manat"},
                    {"label": "TND", "value": "Tunisian dinar"},
                    {"label": "TRY", "value": "Turkish new lira"},
                    {"label": "TTD", "value": "Trinidad and Tobago dollar"},
                    {"label": "TWD", "value": "New Taiwan dollar"},
                    {"label": "TZS", "value": "Tanzanian shilling"},
                    {"label": "UAH", "value": "Ukrainian hryvnia"},
                    {"label": "UGX", "value": "Ugandan shilling"},
                    {"label": "USD", "value": "United States dollar"},
                    {"label": "UYU", "value": "Uruguayan peso"},
                    {"label": "UZS", "value": "Uzbekistani som"},
                    {"label": "VEB", "value": "Venezuelan bolivar"},
                    {"label": "VND", "value": "Vietvaluese dong"},
                    {"label": "VUV", "value": "Vanuatu vatu"},
                    {"label": "WST", "value": "Samoan tala"},
                    {"label": "XAF", "value": "Central African CFA franc"},
                    {"label": "XCD", "value": "East Caribbean dollar"},
                    {"label": "XDR", "value": "Special Drawing Rights"},
                    {"label": "XOF", "value": "West African CFA franc"},
                    {"label": "XPF", "value": "CFP franc"},
                    {"label": "YER", "value": "Yemeni rial"},
                    {"label": "ZAR", "value": "South African rand"},
                    {"label": "ZMK", "value": "Zambian kwacha"},
                    {"label": "ZWR", "value": "Zimbabwean dollar"}
                ]);


                self.selectedAttachmentType = ko.observable();
                self.vairationSelectedAttachmentType = ko.observable();
                self.renewalSelectedAttachmentType = ko.observable();
                self.productSelectedAttachmentType = ko.observable();
                self.referenceStanderSelectedAttachmentType = ko.observable();
                self.generalDocumentSelectedAttachmentType = ko.observable();
                self.allAttachedData = ko.observableArray('');

                this.country = ko.observableArray([
                    {label: 'Afghanistan', value: 'AF'},
                    {label: 'Åland Islands', value: 'AX'},
                    {label: 'Albania', value: 'AL'},
                    {label: 'Algeria', value: 'DZ'},
                    {label: 'American Samoa', value: 'AS'},
                    {label: 'AndorrA', value: 'AD'},
                    {label: 'Angola', value: 'AO'},
                    {label: 'Anguilla', value: 'AI'},
                    {label: 'Antarctica', value: 'AQ'},
                    {label: 'Antigua and Barbuda', value: 'AG'},
                    {label: 'Argentina', value: 'AR'},
                    {label: 'Armenia', value: 'AM'},
                    {label: 'Aruba', value: 'AW'},
                    {label: 'Australia', value: 'AU'},
                    {label: 'Austria', value: 'AT'},
                    {label: 'Azerbaijan', value: 'AZ'},
                    {label: 'Bahamas', value: 'BS'},
                    {label: 'Bahrain', value: 'BH'},
                    {label: 'Bangladesh', value: 'BD'},
                    {label: 'Barbados', value: 'BB'},
                    {label: 'Belarus', value: 'BY'},
                    {label: 'Belgium', value: 'BE'},
                    {label: 'Belize', value: 'BZ'},
                    {label: 'Benin', value: 'BJ'},
                    {label: 'Bermuda', value: 'BM'},
                    {label: 'Bhutan', value: 'BT'},
                    {label: 'Bolivia', value: 'BO'},
                    {label: 'Bosnia and Herzegovina', value: 'BA'},
                    {label: 'Botswana', value: 'BW'},
                    {label: 'Bouvet Island', value: 'BV'},
                    {label: 'Brazil', value: 'BR'},
                    {label: 'British Indian Ocean Territory', value: 'IO'},
                    {label: 'Brunei Darussalam', value: 'BN'},
                    {label: 'Bulgaria', value: 'BG'},
                    {label: 'Burkina Faso', value: 'BF'},
                    {label: 'Burundi', value: 'BI'},
                    {label: 'Cambodia', value: 'KH'},
                    {label: 'Cameroon', value: 'CM'},
                    {label: 'Canada', value: 'CA'},
                    {label: 'Cape Verde', value: 'CV'},
                    {label: 'Cayman Islands', value: 'KY'},
                    {label: 'Central African Republic', value: 'CF'},
                    {label: 'Chad', value: 'TD'},
                    {label: 'Chile', value: 'CL'},
                    {label: 'China', value: 'CN'},
                    {label: 'Christmas Island', value: 'CX'},
                    {label: 'Cocos (Keeling) Islands', value: 'CC'},
                    {label: 'Colombia', value: 'CO'},
                    {label: 'Comoros', value: 'KM'},
                    {label: 'Congo', value: 'CG'},
                    {label: 'Congo, The Democratic Republic of the', value: 'CD'},
                    {label: 'Cook Islands', value: 'CK'},
                    {label: 'Costa Rica', value: 'CR'},
                    {label: 'Cote D\'Ivoire', value: 'CI'},
                    {label: 'Croatia', value: 'HR'},
                    {label: 'Cuba', value: 'CU'},
                    {label: 'Cyprus', value: 'CY'},
                    {label: 'Czech Republic', value: 'CZ'},
                    {label: 'Denmark', value: 'DK'},
                    {label: 'Djibouti', value: 'DJ'},
                    {label: 'Dominica', value: 'DM'},
                    {label: 'Dominican Republic', value: 'DO'},
                    {label: 'Ecuador', value: 'EC'},
                    {label: 'Egypt', value: 'EG'},
                    {label: 'El Salvador', value: 'SV'},
                    {label: 'Equatorial Guinea', value: 'GQ'},
                    {label: 'Eritrea', value: 'ER'},
                    {label: 'Estonia', value: 'EE'},
                    {label: 'Ethiopia', value: 'ET'},
                    {label: 'Falkland Islands (Malvinas)', value: 'FK'},
                    {label: 'Faroe Islands', value: 'FO'},
                    {label: 'Fiji', value: 'FJ'},
                    {label: 'Finland', value: 'FI'},
                    {label: 'France', value: 'FR'},
                    {label: 'French Guiana', value: 'GF'},
                    {label: 'French Polynesia', value: 'PF'},
                    {label: 'French Southern Territories', value: 'TF'},
                    {label: 'Gabon', value: 'GA'},
                    {label: 'Gambia', value: 'GM'},
                    {label: 'Georgia', value: 'GE'},
                    {label: 'Germany', value: 'DE'},
                    {label: 'Ghana', value: 'GH'},
                    {label: 'Gibraltar', value: 'GI'},
                    {label: 'Greece', value: 'GR'},
                    {label: 'Greenland', value: 'GL'},
                    {label: 'Grenada', value: 'GD'},
                    {label: 'Guadeloupe', value: 'GP'},
                    {label: 'Guam', value: 'GU'},
                    {label: 'Guatemala', value: 'GT'},
                    {label: 'Guernsey', value: 'GG'},
                    {label: 'Guinea', value: 'GN'},
                    {label: 'Guinea-Bissau', value: 'GW'},
                    {label: 'Guyana', value: 'GY'},
                    {label: 'Haiti', value: 'HT'},
                    {label: 'Heard Island and Mcdonald Islands', value: 'HM'},
                    {label: 'Holy See (Vatican City State)', value: 'VA'},
                    {label: 'Honduras', value: 'HN'},
                    {label: 'Hong Kong', value: 'HK'},
                    {label: 'Hungary', value: 'HU'},
                    {label: 'Iceland', value: 'IS'},
                    {label: 'India', value: 'IN'},
                    {label: 'Indonesia', value: 'ID'},
                    {label: 'Iran, Islamic Republic Of', value: 'IR'},
                    {label: 'Iraq', value: 'IQ'},
                    {label: 'Ireland', value: 'IE'},
                    {label: 'Isle of Man', value: 'IM'},
                    {label: 'Israel', value: 'IL'},
                    {label: 'Italy', value: 'IT'},
                    {label: 'Jamaica', value: 'JM'},
                    {label: 'Japan', value: 'JP'},
                    {label: 'Jersey', value: 'JE'},
                    {label: 'Jordan', value: 'JO'},
                    {label: 'Kazakhstan', value: 'KZ'},
                    {label: 'Kenya', value: 'KE'},
                    {label: 'Kiribati', value: 'KI'},
                    {label: 'Korea, Democratic People\'S Republic of', value: 'KP'},
                    {label: 'Korea, Republic of', value: 'KR'},
                    {label: 'Kuwait', value: 'KW'},
                    {label: 'Kyrgyzstan', value: 'KG'},
                    {label: 'Lao People\'S Democratic Republic', value: 'LA'},
                    {label: 'Latvia', value: 'LV'},
                    {label: 'Lebanon', value: 'LB'},
                    {label: 'Lesotho', value: 'LS'},
                    {label: 'Liberia', value: 'LR'},
                    {label: 'Libyan Arab Jamahiriya', value: 'LY'},
                    {label: 'Liechtenstein', value: 'LI'},
                    {label: 'Lithuania', value: 'LT'},
                    {label: 'Luxembourg', value: 'LU'},
                    {label: 'Macao', value: 'MO'},
                    {label: 'Macedonia, The Former Yugoslav Republic of', value: 'MK'},
                    {label: 'Madagascar', value: 'MG'},
                    {label: 'Malawi', value: 'MW'},
                    {label: 'Malaysia', value: 'MY'},
                    {label: 'Maldives', value: 'MV'},
                    {label: 'Mali', value: 'ML'},
                    {label: 'Malta', value: 'MT'},
                    {label: 'Marshall Islands', value: 'MH'},
                    {label: 'Martinique', value: 'MQ'},
                    {label: 'Mauritania', value: 'MR'},
                    {label: 'Mauritius', value: 'MU'},
                    {label: 'Mayotte', value: 'YT'},
                    {label: 'Mexico', value: 'MX'},
                    {label: 'Micronesia, Federated States of', value: 'FM'},
                    {label: 'Moldova, Republic of', value: 'MD'},
                    {label: 'Monaco', value: 'MC'},
                    {label: 'Mongolia', value: 'MN'},
                    {label: 'Montserrat', value: 'MS'},
                    {label: 'Morocco', value: 'MA'},
                    {label: 'Mozambique', value: 'MZ'},
                    {label: 'Myanmar', value: 'MM'},
                    {label: 'Namibia', value: 'NA'},
                    {label: 'Nauru', value: 'NR'},
                    {label: 'Nepal', value: 'NP'},
                    {label: 'Netherlands', value: 'NL'},
                    {label: 'Netherlands Antilles', value: 'AN'},
                    {label: 'New Caledonia', value: 'NC'},
                    {label: 'New Zealand', value: 'NZ'},
                    {label: 'Nicaragua', value: 'NI'},
                    {label: 'Niger', value: 'NE'},
                    {label: 'Nigeria', value: 'NG'},
                    {label: 'Niue', value: 'NU'},
                    {label: 'Norfolk Island', value: 'NF'},
                    {label: 'Northern Mariana Islands', value: 'MP'},
                    {label: 'Norway', value: 'NO'},
                    {label: 'Oman', value: 'OM'},
                    {label: 'Pakistan', value: 'PK'},
                    {label: 'Palau', value: 'PW'},
                    {label: 'Palestinian Territory, Occupied', value: 'PS'},
                    {label: 'Panama', value: 'PA'},
                    {label: 'Papua New Guinea', value: 'PG'},
                    {label: 'Paraguay', value: 'PY'},
                    {label: 'Peru', value: 'PE'},
                    {label: 'Philippines', value: 'PH'},
                    {label: 'Pitcairn', value: 'PN'},
                    {label: 'Poland', value: 'PL'},
                    {label: 'Portugal', value: 'PT'},
                    {label: 'Puerto Rico', value: 'PR'},
                    {label: 'Qatar', value: 'QA'},
                    {label: 'Reunion', value: 'RE'},
                    {label: 'Romania', value: 'RO'},
                    {label: 'Russian Federation', value: 'RU'},
                    {label: 'RWANDA', value: 'RW'},
                    {label: 'Saint Helena', value: 'SH'},
                    {label: 'Saint Kitts and Nevis', value: 'KN'},
                    {label: 'Saint Lucia', value: 'LC'},
                    {label: 'Saint Pierre and Miquelon', value: 'PM'},
                    {label: 'Saint Vincent and the Grenadines', value: 'VC'},
                    {label: 'Samoa', value: 'WS'},
                    {label: 'San Marino', value: 'SM'},
                    {label: 'Sao Tome and Principe', value: 'ST'},
                    {label: 'Saudi Arabia', value: 'SA'},
                    {label: 'Senegal', value: 'SN'},
                    {label: 'Serbia and Montenegro', value: 'CS'},
                    {label: 'Seychelles', value: 'SC'},
                    {label: 'Sierra Leone', value: 'SL'},
                    {label: 'Singapore', value: 'SG'},
                    {label: 'Slovakia', value: 'SK'},
                    {label: 'Slovenia', value: 'SI'},
                    {label: 'Solomon Islands', value: 'SB'},
                    {label: 'Somalia', value: 'SO'},
                    {label: 'South Africa', value: 'ZA'},
                    {label: 'South Georgia and the South Sandwich Islands', value: 'GS'},
                    {label: 'Spain', value: 'ES'},
                    {label: 'Sri Lanka', value: 'LK'},
                    {label: 'Sudan', value: 'SD'},
                    {label: 'Surilabel', value: 'SR'},
                    {label: 'Svalbard and Jan Mayen', value: 'SJ'},
                    {label: 'Swaziland', value: 'SZ'},
                    {label: 'Sweden', value: 'SE'},
                    {label: 'Switzerland', value: 'CH'},
                    {label: 'Syrian Arab Republic', value: 'SY'},
                    {label: 'Taiwan, Province of China', value: 'TW'},
                    {label: 'Tajikistan', value: 'TJ'},
                    {label: 'Tanzania, United Republic of', value: 'TZ'},
                    {label: 'Thailand', value: 'TH'},
                    {label: 'Timor-Leste', value: 'TL'},
                    {label: 'Togo', value: 'TG'},
                    {label: 'Tokelau', value: 'TK'},
                    {label: 'Tonga', value: 'TO'},
                    {label: 'Trinidad and Tobago', value: 'TT'},
                    {label: 'Tunisia', value: 'TN'},
                    {label: 'Turkey', value: 'TR'},
                    {label: 'Turkmenistan', value: 'TM'},
                    {label: 'Turks and Caicos Islands', value: 'TC'},
                    {label: 'Tuvalu', value: 'TV'},
                    {label: 'Uganda', value: 'UG'},
                    {label: 'Ukraine', value: 'UA'},
                    {label: 'United Arab Emirates', value: 'AE'},
                    {label: 'United Kingdom', value: 'GB'},
                    {label: 'United States', value: 'US'},
                    {label: 'United States Minor Outlying Islands', value: 'UM'},
                    {label: 'Uruguay', value: 'UY'},
                    {label: 'Uzbekistan', value: 'UZ'},
                    {label: 'Vanuatu', value: 'VU'},
                    {label: 'Venezuela', value: 'VE'},
                    {label: 'Viet Nam', value: 'VN'},
                    {label: 'Virgin Islands, British', value: 'VG'},
                    {label: 'Virgin Islands, U.S.', value: 'VI'},
                    {label: 'Wallis and Futuna', value: 'WF'},
                    {label: 'Western Sahara', value: 'EH'},
                    {label: 'Yemen', value: 'YE'},
                    {label: 'Zambia', value: 'ZM'},
                    {label: 'Zimbabwe', value: 'ZW'}
                ]);
                this.registerTypeList = ko.observableArray([
                    {label: 'pharmaceuticals', value: 'pharma'},
                    {label: 'Medical Device', value: 'medicalDevice'},
                    {label: 'veterinary', value: 'vet'},
                    {label: 'Cosmetic', value: 'cosmetic'},
                    {label: 'Food Suppliments', value: 'food_supply'},
                    {label: 'Agriculture', value: 'agriculture'},
                    {label: 'Special Food Suplliments', value: 'cosmetic'},
                    {label: 'Herbal', value: 'herbal'}

                ]);
                this.registerLocationList = ko.observableArray([
                    {label: 'KSA', value: 'ksa'},
                    {label: 'UAE', value: 'uae'},
                    {label: 'KUWAIT', value: 'kuwait'},
                    {label: 'EGYPT', value: 'egypt'},
                    {label: 'Qatar', value: 'qatar'},
                    {label: 'Bahrain', value: 'bahrain'},
                    {label: 'Oman', value: 'oman'},
                    {label: 'Lebnaon', value: 'lebnanon'},
                    {label: 'Turkey', value: 'turkey'}
                ]);
                this.storageConditionList = ko.observableArray([
                    {label: 'Ambient', value: 'ambient'},
                    {label: 'Freezer', value: 'freezer'},
                    {label: 'Chillers', value: 'chillers'},
                ]);
                this.registerationStatus = ko.observableArray([
                    {label: 'Valid', value: 'valid'},
                    {label: 'Expired', value: 'expired'},
                    {label: 'Cancelled', value: 'cancelled'},
                    {label: 'Suspended', value: 'suspended'},
                    {label: 'Terminated', value: 'terminated'}
                ]);





                // to show the oj-validation-group's valid property value
                self.productRegisgroupValid = ko.observable();

                // Translation
                var getTranslation = oj.Translations.getTranslatedString;

                self.reloadlanguage = function ()
                {
                    var newlang = localStorage.getItem('setLang');
                    if (!newlang)
                    {
                        localStorage.setItem('lang', newlang);
                    }
                    switch (newlang) {
                        case 'arabic':
                            newLang = 'ar-sa';
                            break;
                        default:
                            newLang = 'en-US';
                    }
                    oj.Config.setLocale(newLang,
                            function () {
                                $('html').attr('lang', newLang);
                                if (newLang === 'ar-sa') {
                                    $('html').attr('dir', 'rtl');


                                    self.productRegisterLblsModel.registerNumberLbl(getTranslation("productRegister.registerNumber"));
                                    self.productRegisterLblsModel.registerDateLbl(getTranslation("productRegister.registerDate"));
                                    self.productRegisterLblsModel.registerTypeLbl(getTranslation("productRegister.registerType"));
                                    self.productRegisterLblsModel.productNameLbl(getTranslation("productRegister.productName"));
                                    self.productRegisterLblsModel.itemCodeLbl(getTranslation("productRegister.itemCode"));
                                    self.productRegisterLblsModel.supplierNameLbl(getTranslation("productRegister.supplierName"));
                                    self.productRegisterLblsModel.telephoneLbl(getTranslation("productRegister.telephone"));
                                    self.productRegisterLblsModel.addressLbl(getTranslation("productRegister.address"));
                                    self.productRegisterLblsModel.emailLbl(getTranslation("productRegister.email"));
                                    self.productRegisterLblsModel.titleLbl(getTranslation("productRegister.title"));
                                    self.productRegisterLblsModel.contactLbl(getTranslation("productRegister.contact"));
                                    self.productRegisterLblsModel.productRegisterStatusLbl(getTranslation("productRegister.productRegisterStatus"));
                                    self.productRegisterLblsModel.companyNameLbl(getTranslation("productRegister.companyName"));
                                    self.productRegisterLblsModel.registerLocationLbl(getTranslation("productRegister.registerLocation"));
                                    self.productRegisterLblsModel.countryOfOriginLbl(getTranslation("productRegister.countryOfOrigin"));
                                    self.productRegisterLblsModel.shelfLifeLbl(getTranslation("productRegister.shelfLife"));
                                    self.productRegisterLblsModel.storageCoditionLbl(getTranslation("productRegister.storageCodition"));
                                    self.productRegisterLblsModel.cifPriceLbl(getTranslation("productRegister.cifPrice"));
                                    self.productRegisterLblsModel.wholeSalePriceLbl(getTranslation("productRegister.wholeSalePrice"));
                                    self.productRegisterLblsModel.retailPriceLbl(getTranslation("productRegister.retailPrice"));
                                    self.productRegisterLblsModel.exchangeRateLbl(getTranslation("productRegister.retailPrice"));
                                    self.productRegisterLblsModel.currencyLbl(getTranslation("productRegister.currency"));
                                    self.productRegisterLblsModel.primaryPackagingLbl(getTranslation("productRegister.currency"));
                                    self.productRegisterLblsModel.mahNameLbl(getTranslation("productRegister.mahName"));
                                    self.productRegisterLblsModel.manufacturingNameLbl(getTranslation("productRegister.manufacturingName"));
                                    self.productRegisterLblsModel.indicationLbl(getTranslation("productRegister.indication"));
                                    self.productRegisterLblsModel.pharmaceuticalFormLbl(getTranslation("productRegister.pharmaceuticalForm"));
                                    self.productRegisterLblsModel.activeIngredientNameLbl(getTranslation("productRegister.activeIngredientName"));
                                    self.productRegisterLblsModel.relativeSubstancesLbl(getTranslation("productRegister.relativeSubstances"));
                                    self.productRegisterLblsModel.siteRegistrationNameLbl(getTranslation("productRegister.siteRegistrationName"));
                                    self.productRegisterLblsModel.siteRegistrationValidityLbl(getTranslation("productRegister.siteRegistrationValidity"));
                                    self.productRegisterLblsModel.siteRegistrationNumberLbl(getTranslation("productRegister.siteRegistrationNumber"));
                                    self.productRegisterLblsModel.lastRenewalLbl(getTranslation("productRegister.lastRenewal"));
                                    self.productRegisterLblsModel.nextRenewalLbl(getTranslation("productRegister.nextRenewal"));
                                    self.productRegisterLblsModel.secondaryPackage_releaseSiteLbl(getTranslation("productRegister.secondaryPackage_releaseSite"));
                                    self.productRegisterLblsModel.noteLbl(getTranslation("Notes"));


                                    // Tabs Details Start 
                                    self.prodRegisTabsDetailsLblsModel.newRegistrationLbl(getTranslation("prodRegisTabsDetails.newRegistration"));
                                    self.prodRegisTabsDetailsLblsModel.variationLbl(getTranslation("prodRegisTabsDetails.variation"));
                                    self.prodRegisTabsDetailsLblsModel.kindOfVariationLbl(getTranslation("prodRegisTabsDetails.kindOfVariation"));
                                    self.prodRegisTabsDetailsLblsModel.renewalLbl(getTranslation("prodRegisTabsDetails.renewal"));
                                    self.prodRegisTabsDetailsLblsModel.kindOfRenewalLbl(getTranslation("prodRegisTabsDetails.kindOfRenewal"));
                                    self.prodRegisTabsDetailsLblsModel.nextRenewalDateLbl(getTranslation("prodRegisTabsDetails.nextRenewalDate"));
                                    self.prodRegisTabsDetailsLblsModel.productClassificationLbl(getTranslation("prodRegisTabsDetails.productClassification"));
                                    self.prodRegisTabsDetailsLblsModel.restrictedLbl(getTranslation("prodRegisTabsDetails.restricted"));
                                    self.prodRegisTabsDetailsLblsModel.importLicenceLbl(getTranslation("prodRegisTabsDetails.importLicence"));
                                    self.prodRegisTabsDetailsLblsModel.refStandAvaLbl(getTranslation("prodRegisTabsDetails.refStandAva"));
                                    self.prodRegisTabsDetailsLblsModel.refAvailabilityLbl(getTranslation("prodRegisTabsDetails.refAvailability"));
                                    self.prodRegisTabsDetailsLblsModel.kindOfRSTDLbl(getTranslation("prodRegisTabsDetails.kindOfRSTD"));
                                    self.prodRegisTabsDetailsLblsModel.dateOfRequestLbl(getTranslation("prodRegisTabsDetails.dateOfRequest"));
                                    self.prodRegisTabsDetailsLblsModel.generalDocumentsLbl(getTranslation("prodRegisTabsDetails.generalDocuments"));
                                    self.prodRegisTabsDetailsLblsModel.kindOfDocumentsLbl(getTranslation("prodRegisTabsDetails.kindOfDocuments"));
                                    self.prodRegisTabsDetailsLblsModel.dateOfSubmissionLbl(getTranslation("prodRegisTabsDetails.dateOfSubmission"));
                                    self.prodRegisTabsDetailsLblsModel.dateOfReceivedRequirementsLbl(getTranslation("prodRegisTabsDetails.dateOfReceivedRequirements"));
                                    self.prodRegisTabsDetailsLblsModel.dateOfRequestRequirementToSupplierLbl(getTranslation("prodRegisTabsDetails.dateOfRequestRequirementToSupplier"));
                                    self.prodRegisTabsDetailsLblsModel.followUpDateLbl(getTranslation("prodRegisTabsDetails.followUpDate"));
                                    self.prodRegisTabsDetailsLblsModel.approvedDateLbl(getTranslation("prodRegisTabsDetails.approvedDate"));
                                    self.prodRegisTabsDetailsLblsModel.commentsLbl(getTranslation("prodRegisTabsDetails.comments"));
                                    self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(getTranslation("prodRegisTabsDetails.attachmentType"));
                                    self.saveBtnLbl(getTranslation("Save"));
                                    self.prevBtnLbl(getTranslation("Previous"));
                                    //// Tabs Details End 


                                    self.submitBtnLbl(getTranslation("buttoms.submitBtn"));
                                    self.cancleBtnLbl(getTranslation("buttoms.cancleBtn"));

                                    self.placeholderInput(getTranslation("common.inputPlaceholder"));
                                    self.placeholderselect(getTranslation("common.selectPlaceholder"));
                                    self.resumeAttachmentLbl(getTranslation("productRegister.attachment"));
                                    self.attachmentTypeLbl(getTranslation("productRegister.attachmentType"));
                                    self.downloadLbl(getTranslation("productRegister.download"));
                                    self.UploadFileLbl(getTranslation("productRegister.UploadFile"));
                                    self.removeBtnLbl(getTranslation("productRegister.remove"));
                                    self.fileUploadStatusLbl(getTranslation("productRegister.fileUploadStatus"));
                                    self.cancel(getTranslation("productRegister.cancel"));
                                    self.registrationCertificate(getTranslation("productRegister.registeration"));
                                    self.renewalCertificate(getTranslation("productRegister.renewal"));
                                    self.siteCertificate(getTranslation("productRegister.site"));
                                    self.typeNameArray([]);
                                    self.typeNameArray.push({
                                        "value": 'registeration',
                                        "label": self.registrationCertificate()
                                    }, {
                                        "value": 'renewal',
                                        "label": self.renewalCertificate()
                                    },
                                            {
                                                "value": 'site',
                                                "label": self.siteCertificate()
                                            }
                                    );


                                } else {
                                    $('html').attr('dir', 'ltr');
                                    $('#enableBackButton').toggleClass('backButtonRotate');
                                    $('.action-menu').toggleClass('action-right');

                                }
                                self.productRegisterLblsModel.registerNumberLbl(getTranslation("productRegister.registerNumber"));
                                self.productRegisterLblsModel.registerDateLbl(getTranslation("productRegister.registerDate"));
                                self.productRegisterLblsModel.registerTypeLbl(getTranslation("productRegister.registerType"));
                                self.productRegisterLblsModel.productNameLbl(getTranslation("productRegister.productName"));
                                self.productRegisterLblsModel.itemCodeLbl(getTranslation("productRegister.itemCode"));
                                self.productRegisterLblsModel.supplierNameLbl(getTranslation("productRegister.supplierName"));
                                self.productRegisterLblsModel.telephoneLbl(getTranslation("productRegister.telephone"));
                                self.productRegisterLblsModel.addressLbl(getTranslation("productRegister.address"));
                                self.productRegisterLblsModel.emailLbl(getTranslation("productRegister.email"));
                                self.productRegisterLblsModel.titleLbl(getTranslation("productRegister.title"));
                                self.productRegisterLblsModel.contactLbl(getTranslation("productRegister.contact"));
                                self.productRegisterLblsModel.productRegisterStatusLbl(getTranslation("productRegister.productRegisterStatus"));
                                self.productRegisterLblsModel.companyNameLbl(getTranslation("productRegister.companyName"));
                                self.productRegisterLblsModel.registerLocationLbl(getTranslation("productRegister.registerLocation"));
                                self.productRegisterLblsModel.countryOfOriginLbl(getTranslation("productRegister.countryOfOrigin"));
                                self.productRegisterLblsModel.shelfLifeLbl(getTranslation("productRegister.shelfLife"));
                                self.productRegisterLblsModel.storageCoditionLbl(getTranslation("productRegister.storageCodition"));
                                self.productRegisterLblsModel.cifPriceLbl(getTranslation("productRegister.cifPrice"));
                                self.productRegisterLblsModel.wholeSalePriceLbl(getTranslation("productRegister.wholeSalePrice"));
                                self.productRegisterLblsModel.retailPriceLbl(getTranslation("productRegister.retailPrice"));
                                self.productRegisterLblsModel.exchangeRateLbl(getTranslation("productRegister.exchangeRate"));
                                self.productRegisterLblsModel.currencyLbl(getTranslation("productRegister.currency"));
                                self.productRegisterLblsModel.primaryPackagingLbl(getTranslation("productRegister.primaryPackaging"));
                                self.productRegisterLblsModel.mahNameLbl(getTranslation("productRegister.mahName"));
                                self.productRegisterLblsModel.manufacturingNameLbl(getTranslation("productRegister.manufacturingName"));
                                self.productRegisterLblsModel.indicationLbl(getTranslation("productRegister.indication"));
                                self.productRegisterLblsModel.pharmaceuticalFormLbl(getTranslation("productRegister.pharmaceuticalForm"));
                                self.productRegisterLblsModel.activeIngredientNameLbl(getTranslation("productRegister.activeIngredientName"));
                                self.productRegisterLblsModel.relativeSubstancesLbl(getTranslation("productRegister.relativeSubstances"));
                                self.productRegisterLblsModel.siteRegistrationNameLbl(getTranslation("productRegister.siteRegistrationName"));
                                self.productRegisterLblsModel.siteRegistrationValidityLbl(getTranslation("productRegister.siteRegistrationValidity"));
                                self.productRegisterLblsModel.siteRegistrationNumberLbl(getTranslation("productRegister.siteRegistrationNumber"));
                                self.productRegisterLblsModel.lastRenewalLbl(getTranslation("productRegister.lastRenewal"));
                                self.productRegisterLblsModel.nextRenewalLbl(getTranslation("productRegister.nextRenewal"));
                                self.productRegisterLblsModel.secondaryPackage_releaseSiteLbl(getTranslation("productRegister.secondaryPackage_releaseSite"));
                                self.productRegisterLblsModel.noteLbl(getTranslation("Notes"));


                                // Tabs Details Start 
                                self.prodRegisTabsDetailsLblsModel.newRegistrationLbl(getTranslation("prodRegisTabsDetails.newRegistration"));
                                self.prodRegisTabsDetailsLblsModel.variationLbl(getTranslation("prodRegisTabsDetails.variation"));
                                self.prodRegisTabsDetailsLblsModel.kindOfVariationLbl(getTranslation("prodRegisTabsDetails.kindOfVariation"));
                                self.prodRegisTabsDetailsLblsModel.renewalLbl(getTranslation("prodRegisTabsDetails.renewal"));
                                self.prodRegisTabsDetailsLblsModel.kindOfRenewalLbl(getTranslation("prodRegisTabsDetails.kindOfRenewal"));
                                self.prodRegisTabsDetailsLblsModel.nextRenewalDateLbl(getTranslation("prodRegisTabsDetails.nextRenewalDate"));
                                self.prodRegisTabsDetailsLblsModel.productClassificationLbl(getTranslation("prodRegisTabsDetails.productClassification"));
                                self.prodRegisTabsDetailsLblsModel.restrictedLbl(getTranslation("prodRegisTabsDetails.restricted"));
                                self.prodRegisTabsDetailsLblsModel.importLicenceLbl(getTranslation("prodRegisTabsDetails.importLicence"));
                                self.prodRegisTabsDetailsLblsModel.refStandAvaLbl(getTranslation("prodRegisTabsDetails.refStandAva"));
                                self.prodRegisTabsDetailsLblsModel.refAvailabilityLbl(getTranslation("prodRegisTabsDetails.refAvailability"));
                                self.prodRegisTabsDetailsLblsModel.kindOfRSTDLbl(getTranslation("prodRegisTabsDetails.kindOfRSTD"));
                                self.prodRegisTabsDetailsLblsModel.dateOfRequestLbl(getTranslation("prodRegisTabsDetails.dateOfRequest"));
                                self.prodRegisTabsDetailsLblsModel.generalDocumentsLbl(getTranslation("prodRegisTabsDetails.generalDocuments"));
                                self.prodRegisTabsDetailsLblsModel.kindOfDocumentsLbl(getTranslation("prodRegisTabsDetails.kindOfDocuments"));
                                self.prodRegisTabsDetailsLblsModel.dateOfSubmissionLbl(getTranslation("prodRegisTabsDetails.dateOfSubmission"));
                                self.prodRegisTabsDetailsLblsModel.dateOfReceivedRequirementsLbl(getTranslation("prodRegisTabsDetails.dateOfReceivedRequirements"));
                                self.prodRegisTabsDetailsLblsModel.dateOfRequestRequirementToSupplierLbl(getTranslation("prodRegisTabsDetails.dateOfRequestRequirementToSupplier"));
                                self.prodRegisTabsDetailsLblsModel.followUpDateLbl(getTranslation("prodRegisTabsDetails.followUpDate"));
                                self.prodRegisTabsDetailsLblsModel.approvedDateLbl(getTranslation("prodRegisTabsDetails.approvedDate"));
                                self.prodRegisTabsDetailsLblsModel.commentsLbl(getTranslation("prodRegisTabsDetails.comments"));
                                self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(getTranslation("prodRegisTabsDetails.attachmentType"));
                                self.prodRegisTabsDetailsLblsModel.fileUploadStatusLbl(getTranslation("prodRegisTabsDetails.fileUploadStatus"));
                                self.prodRegisTabsDetailsLblsModel.attachmentLbl(getTranslation("prodRegisTabsDetails.attachment"));
                                //// Tabs Details End 

//***********************************************************************************************************************
                                self.columnArrayNewRegistrationHistory([
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.newRegistrationLbl(), "field": "new_registration"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfSubmissionLbl(), "field": "new_regis_dos"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfReceivedRequirementsLbl(), "field": "new_regis_do_receiv_reqs"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfRequestRequirementToSupplierLbl(), "field": "new_regis_do_req_reqir_to_sup"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.followUpDateLbl(), "field": "new_regis_follow_up_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.approvedDateLbl(), "field": "new_regis_approved_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.commentsLbl(), "field": "new_regis_comments"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "attachment"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentLbl(), "field": "Rating", "template": "ratingCellTemplate"}
                                ]);


                                self.columnArrayVairationHistory([
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.variationLbl(), "field": "variation"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.kindOfVariationLbl(), "field": "kind_of_variation"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfSubmissionLbl(), "field": "variation_dos"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfReceivedRequirementsLbl(), "field": "variation_do_receiv_reqs"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfRequestRequirementToSupplierLbl(), "field": "variation_do_req_reqir_to_sup"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.followUpDateLbl(), "field": "variation_follow_up_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.approvedDateLbl(), "field": "variation_approved_date"},
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "prodRegisTabsDetails.attachmentType"},
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.fileUploadStatusLbl(), "field": "prodRegisTabsDetails.fileUploadStatus"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.commentsLbl(), "field": "variation_comments"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "vairation_attachment_type"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentLbl(), "field": "Rating", "template": "ratingCellTemplate"}
                                ]);

                                self.columnArrayRenewalHistory([
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.renewalLbl(), "field": "renewal"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.kindOfVariationLbl(), "field": "kind_of_renewal"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfSubmissionLbl(), "field": "renewal_dos"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfReceivedRequirementsLbl(), "field": "renewal_do_receiv_reqs"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfRequestRequirementToSupplierLbl(), "field": "renewal_do_req_reqir_to_sup"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.followUpDateLbl(), "field": "renewal_date_follow_up_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.approvedDateLbl(), "field": "renewal_date_approved_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.commentsLbl(), "field": "renewal_date_comments"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.nextRenewalDateLbl(), "field": "next_renewal_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "renewal_attachment_type"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentLbl(), "field": "Rating", "template": "ratingCellTemplate"}
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "prodRegisTabsDetails.attachmentType"},
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.fileUploadStatusLbl(), "field": "prodRegisTabsDetails.fileUploadStatus"}

                                ]);
                                self.columnArrayProductClassificationHistory([
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.productClassificationLbl(), "field": "pro_class"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.restrictedLbl(), "field": "restricted"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.importLicenceLbl(), "field": "import_licence"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfSubmissionLbl(), "field": "pro_class_dos"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.followUpDateLbl(), "field": "pro_class_follow_up_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.approvedDateLbl(), "field": "pro_class_approved_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.commentsLbl(), "field": "pro_class_comments"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "product_class_attachmen"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentLbl(), "field": "Rating", "template": "ratingCellTemplate"}
                                    //{"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "prodRegisTabsDetails.attachmentType"},
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.fileUploadStatusLbl(), "field": "prodRegisTabsDetails.fileUploadStatus"},
                                ]);

                                self.columnArrayReferenceStandardAvailabilityHistory([
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.refStandAvaLbl(), "field": "ref_stand_ava"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.refAvailabilityLbl(), "field": "ref_availability"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.kindOfRSTDLbl(), "field": "kind_of_rstd"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfSubmissionLbl(), "field": "ref_stand_ava_dos"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.followUpDateLbl(), "field": "ref_stand_ava_follow_up_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfReceivedRequirementsLbl(), "field": "ref_stand_ava_do_receiv_reqs"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.approvedDateLbl(), "field": "ref_stand_ava_approved_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfRequestLbl(), "field": "date_of_request"},
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "prodRegisTabsDetails.attachmentType"},
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.fileUploadStatusLbl(), "field": "prodRegisTabsDetails.fileUploadStatus"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.commentsLbl(), "field": "ref_stand_ava_comments"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "reference_stan_attachment_type"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentLbl(), "field": "Rating", "template": "ratingCellTemplate"}
                                ]);

                                self.columnArrayGeneralDocumentsHistory([
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.generalDocumentsLbl(), "field": "general_documents"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.kindOfDocumentsLbl(), "field": "kind_of_documents"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfSubmissionLbl(), "field": "gene_docs_dos"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfReceivedRequirementsLbl(), "field": "gene_docs_do_receiv_reqs"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.dateOfRequestRequirementToSupplierLbl(), "field": "gene_docs_do_req_reqir_to_sup"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.followUpDateLbl(), "field": "gene_docs_follow_up_date"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.approvedDateLbl(), "field": "gene_docs_approved_date"},
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "prodRegisTabsDetails.attachmentType"},
//                                    {"headerText": self.prodRegisTabsDetailsLblsModel.fileUploadStatusLbl(), "field": "prodRegisTabsDetails.fileUploadStatus"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.commentsLbl(), "field": "gene_docs_comments"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentTypeLbl(), "field": "general_doc_attachment_type"},
                                    {"headerText": self.prodRegisTabsDetailsLblsModel.attachmentLbl(), "field": "Rating", "template": "ratingCellTemplate"}
                                ]);




//****************************************************************************************************************************************
                                self.submitBtnLbl(getTranslation("buttoms.submitBtn"));
                                self.cancleBtnLbl(getTranslation("buttoms.cancleBtn"));

                                self.placeholderInput(getTranslation("common.inputPlaceholder"));
                                self.placeholderselect(getTranslation("common.selectPlaceholder"));
                                self.resumeAttachmentLbl(getTranslation("productRegister.attachment"));
                                self.attachmentTypeLbl(getTranslation("productRegister.attachmentType"));
                                self.downloadLbl(getTranslation("productRegister.download"));
                                self.UploadFileLbl(getTranslation("productRegister.UploadFile"));
                                self.removeBtnLbl(getTranslation("productRegister.remove"));
                                self.fileUploadStatusLbl(getTranslation("productRegister.fileUploadStatus"));
                                self.cancel(getTranslation("productRegister.cancel"));
                                self.saveBtnLbl(getTranslation("Save"));
                                self.prevBtnLbl(getTranslation("Previous"));
                                self.registrationCertificate(getTranslation("productRegister.registeration"));
                                self.renewalCertificate(getTranslation("productRegister.renewal"));
                                self.siteCertificate(getTranslation("productRegister.site"));
                                self.typeNameArray([]);
                                self.typeNameArray.push({
                                    "value": 'registeration',
                                    "label": self.registrationCertificate()
                                }, {
                                    "value": 'renewal',
                                    "label": self.renewalCertificate()
                                },
                                        {
                                            "value": 'site',
                                            "label": self.siteCertificate()
                                        }
                                );

                            }
                    );



                };


                //Date Formate Convert 

                self.comboBoxValue = ko.observable("dd/MM/yy");
                self.datePicker = {
                    changeMonth: 'none',
                    changeYear: 'none'
                };
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).
                        createConverter(
                                {
                                    pattern: "dd/MM/yy"
                                }));

                self.productRegisterLblsModel = {

                    registerNumberLbl: ko.observable(),
                    registerDateLbl: ko.observable(),
                    registerTypeLbl: ko.observable(),
                    productNameLbl: ko.observable(),
                    itemCodeLbl: ko.observable(),
                    supplierNameLbl: ko.observable(),
                    telephoneLbl: ko.observable(),
                    addressLbl: ko.observable(),
                    emailLbl: ko.observable(),
                    titleLbl: ko.observable(),
                    contactLbl: ko.observable(),
                    productRegisterStatusLbl: ko.observable(),
                    companyNameLbl: ko.observable(),
                    registerLocationLbl: ko.observable(),
                    countryOfOriginLbl: ko.observable(),
                    shelfLifeLbl: ko.observable(),
                    storageCoditionLbl: ko.observable(),
                    cifPriceLbl: ko.observable(),
                    wholeSalePriceLbl: ko.observable(),
                    retailPriceLbl: ko.observable(),
                    exchangeRateLbl: ko.observable(),
                    currencyLbl: ko.observable(),
                    primaryPackagingLbl: ko.observable(),
                    mahNameLbl: ko.observable(),
                    manufacturingNameLbl: ko.observable(),
                    indicationLbl: ko.observable(),
                    pharmaceuticalFormLbl: ko.observable(),
                    activeIngredientNameLbl: ko.observable(),
                    relativeSubstancesLbl: ko.observable(),
                    siteRegistrationNameLbl: ko.observable(),
                    siteRegistrationValidityLbl: ko.observable(),
                    siteRegistrationNumberLbl: ko.observable(),
                    lastRenewalLbl: ko.observable(),
                    nextRenewalLbl: ko.observable(),
                    secondaryPackage_releaseSiteLbl: ko.observable(),
                    noteLbl: ko.observable()};

                self.productRegisterModel = {
           registerNumber: ko.observable(),
                    registerDate: ko.observable(),
                    registerType: ko.observable(),
                    productName: ko.observable(),
                    itemCode: ko.observable(),
                    suppName: ko.observable(),
                    telephone: ko.observable(),
                    telephoneOne: ko.observable(),
                    telephoneTwo: ko.observable(),
                    telephoneThree: ko.observable(),
                    telephoneFour: ko.observable(),
                    address: ko.observable(),
                    email: ko.observable(),
                    emailOne: ko.observable(),
                    emailTwo: ko.observable(),
                    emailThree: ko.observable(),
                    emailFour: ko.observable(),
                    title: ko.observable(),
                    titleOne: ko.observable(),
                    titleTwo: ko.observable(),
                    titleThree: ko.observable(),
                    titleFour: ko.observable(),
                    contact: ko.observable(),
                    productRegisterStatus: ko.observable(),
                    companyName: ko.observable(),
                    registerLocation: ko.observable(),
                    countryOfOrigin: ko.observable(),
                    shelfLife: ko.observable(),
                    storageCodition: ko.observable(),
                    cifPrice: ko.observable(),
                    wholeSalePrice: ko.observable(),
                    retailPrice: ko.observable(),
                    exchangeRate: ko.observable(),
                    currency: ko.observable(),
                    primaryPackaging: ko.observable(),
                    mahName: ko.observable(),
                    manufacturingName: ko.observable(),
                    indication: ko.observable(),
                    pharmaceuticalForm: ko.observable(),
                    activeIngredientName: ko.observable(),
                    relatSubstances: ko.observable(),
                    siteRegistrationName: ko.observable(),
                    siteRegistrationValidity: ko.observable(),
                    siteRegistrationNumber: ko.observable(),
                    lastRenewal: ko.observable(),
                    nextRenewal: ko.observable(),
                    secondaryPackage_releaseSite: ko.observable(),
                    contactOne: ko.observable(),
                    contactTwo: ko.observable(),
                    contactThree: ko.observable(),
                    contactFour: ko.observable(),
                    siteOne: ko.observable(),
                    siteTwo: ko.observable(),
                    notes: ko.observable()
                };

                // define and Handle table *********************************************************************

                self.columnArrayNewRegistrationHistory = ko.observableArray([]);
                self.columnArrayVairationHistory = ko.observableArray([]);
                self.columnArrayRenewalHistory = ko.observableArray([]);
                self.columnArrayProductClassificationHistory = ko.observableArray([]);
                self.columnArrayReferenceStandardAvailabilityHistory = ko.observableArray([]);
                self.columnArrayGeneralDocumentsHistory = ko.observableArray([]);


                var deptArray = [];
                var tabledata;
                self.oprationdisabled = ko.observable(true);
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.productRegisDataArr = ko.observableArray(deptArray);
                self.newRegistrationHistoryDataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.productRegisDataArr, {idAttribute: 'id'}));
                self.vairationHistoryDataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.productRegisDataArr, {idAttribute: 'id'}));
                self.renewalHistoryDataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.productRegisDataArr, {idAttribute: 'id'}));
                self.productClassificationHistoryDataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.productRegisDataArr, {idAttribute: 'id'}));
                self.referenceStandardAvailabilityHistoryDataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.productRegisDataArr, {idAttribute: 'id'}));
                self.generalDocumentsHistoryDataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.productRegisDataArr, {idAttribute: 'id'}));


/////

                self.openListener = function (event)
                {
                    document.getElementById("modalDialog1").open();
                };

                this.cancelListener = function (event)
                {
                    document.getElementById("modalDialog1").close();
                    document.getElementById('attLinks').innerHTML = "";

                }

                self.test = ko.observable();

////////

                self.openDownAttach = function (idValue) {
                    var element = document.getElementById(idValue);
                    var currentRow = element.currentRow;

                    var imgsTemp = [];
                    for (var k = 0; k < self.attachmentData().length; k++)
                    {
                        if (self.attachmentData()[k].prod_regis_details_id == self.productRegisDataArr()[currentRow['rowIndex']].id)
                        {

                            var selectedHisTab = self.selectedItemHistory()
                            selectedHisTab = selectedHisTab.replace("History", "");

                            if (self.attachmentData()[k].tab_name === selectedHisTab) {
                                imgsTemp.push(self.attachmentData()[k]);

                                var div = document.getElementById('attLinks');
                                var newlink = document.createElement('span');
                                newlink.setAttribute("class", "down-link");
                                newlink.setAttribute("id", "id" + k);
                                newlink.innerHTML = self.attachmentData()[k].image_name;
                                newlink.style.cursor = "pointer";
                                div.appendChild(newlink);
                                var br = document.createElement("br");
                                newlink.appendChild(br);

                                newlink.onclick = function (event) {
                                    var idValue = event.target.id;
                                    document.getElementById(idValue).style.color = "blue";
                                    var selected = imgsTemp.find(e => {
                                        return e.image_name == $("#" + idValue).text()
                                    });

                                    var link = document.createElement("a");
                                    document.body.appendChild(link);
                                    link.href = selected.image_base_64;
                                    link.download = selected.image_name;
                                    link.click();
                                    document.body.removeChild(link);



                                };
                            } else {
                            }


                        }

                    }
                    self.openListener();


                };


                self.openDownAttachNewRegis = function () {

                    self.openDownAttach("newRegistable");

                };

                self.openDownAttachVairationt = function () {

                    self.openDownAttach("vairationtable");

                };

                self.openDownAttachRenewal = function () {

                    self.openDownAttach("renewaltable");

                };

                self.openDownAttachProductClassification = function () {

                    self.openDownAttach("productClassificationtable");

                };

                self.openDownAttachReferenceStandardAvailability = function () {

                    self.openDownAttach("referenceStandardAvailabilitytable");

                };

                self.openDownAttachGeneralDocuments = function () {

                    self.openDownAttach("generalDocumentstable");

                };

                self.fileDisabled = ko.observable(true);


                this.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    self.oprationdisabled(false);
                    self.selectedRowKey(currentRow['rowKey']);
                    self.selectedIndex(currentRow['rowIndex']);


                    if (currentRow['rowKey'] === null) {
                    }


                };

                function clearProdRegisContent() {

                  self.productRegisterModel.registerNumber("");
                    self.productRegisterModel.registerDate("");
                    self.productRegisterModel.registerType("");
                    self.productRegisterModel.productName("");
                    self.productRegisterModel.itemCode("");
                    self.productRegisterModel.suppName("");
                    self.productRegisterModel.telephone("");
                    self.productRegisterModel.telephoneOne("");
                    self.productRegisterModel.telephoneTwo("");
                    self.productRegisterModel.telephoneThree("");
                    self.productRegisterModel.telephoneFour("");
                    self.productRegisterModel.address("");
                    self.productRegisterModel.email("");
                    self.productRegisterModel.emailOne("");
                    self.productRegisterModel.emailTwo("");
                    self.productRegisterModel.emailThree("");
                    self.productRegisterModel.emailFour("");
                    self.productRegisterModel.title("");
                    self.productRegisterModel.titleOne("");
                    self.productRegisterModel.titleTwo("");
                    self.productRegisterModel.titleThree("");
                    self.productRegisterModel.titleFour("");
                    self.productRegisterModel.contact("");
                    self.productRegisterModel.productRegisterStatus("");
                    self.productRegisterModel.companyName("");
                    self.productRegisterModel.registerLocation("");
                    self.productRegisterModel.countryOfOrigin("");
                    self.productRegisterModel.shelfLife("");
                    self.productRegisterModel.storageCodition("");
                    self.productRegisterModel.cifPrice("");
                    self.productRegisterModel.wholeSalePrice("");
                    self.productRegisterModel.retailPrice("");
                    self.productRegisterModel.exchangeRate("");
                    self.productRegisterModel.currency("");
                    self.productRegisterModel.primaryPackaging("");
                    self.productRegisterModel.mahName("");
                    self.productRegisterModel.manufacturingName("");
                    self.productRegisterModel.indication("");
                    self.productRegisterModel.pharmaceuticalForm("");
                    self.productRegisterModel.activeIngredientName("");
                    self.productRegisterModel.relatSubstances("");
                    self.productRegisterModel.siteRegistrationName("");
                    self.productRegisterModel.siteRegistrationValidity("");
                    self.productRegisterModel.siteRegistrationNumber("");
                    self.productRegisterModel.lastRenewal("");
                    self.productRegisterModel.nextRenewal("");
                    self.productRegisterModel.secondaryPackage_releaseSite("");
                    self.productRegisterModel.contactOne("");
                    self.productRegisterModel.contactTwo("");
                    self.productRegisterModel.contactThree("");
                    self.productRegisterModel.contactFour("");
                    self.productRegisterModel.siteOne("");
                    self.productRegisterModel.siteTwo("");
                    self.productRegisterModel.notes("");
                }



                self.reset = function () {
                    clearProdRegisContent();
                    clearProdRegisDetailsContent();
                };
                var productMasterId;
                self.productRegisAction = function () {
                    var jsonData = ko.toJSON(self.productRegisterModel);


                    var productRegisCbFn = function (data) {
                        //var jsonData = JSON.stringify(data);
                        var productRegisId = data.productRegisId;

                        if (self.serviceType() == 'ADD') {
                            self.prodRegisTabsDetailsModel.productRegistrationId(productRegisId);
                            productMasterId = data.productRegisId;
                        }
                        self.prodRegisTabsDetailsAction();
                        self.goBackFunction();
                        self.prevBtnVisible(false);
                        self.saveBtnVisible(true);

                    };
                    var failCbFn = function () {
                    };


                    if (self.serviceType() == 'ADD') {
                        self.suppliernamedisabled(false);
                        var addURL = "ProductRegisRest/addProductRegis";

                        services.addGeneric(addURL, jsonData).then(productRegisCbFn, failCbFn);
                    }
                    if (self.serviceType() == 'EDIT') {
                        var editURL = "ProductRegisRest/updateProductRegis";

                        services.editGeneric(editURL, jsonData).then(productRegisCbFn, failCbFn);

                        //*******************************************************************
//                        var addURL = "ProductRegisDetailsRest/addProductRegisDetails";
//                        services.addGeneric(addURL, jsonData).then(productRegisCbFn, failCbFn);
                        //**********************************************************************
                    }

                };

                self.handleSubmit = function () {

                    self.productRegisAction();
                    self.isDisabledHandleSubmit(true);


                };

                self.handleCancle = function () {

                    self.goBackFunction();
                };
                self.handleSave = function () {
                    self.uploadBtnVisible(true);
                    self.isDisabled(true);
                    self.isDisabledContacts(true);
                    self.isDisabledSite(true);
                    self.prevBtnVisible(true);
                    self.addBtnVisible(true);
                    self.saveBtnVisible(false);

                };
                self.handlePrev = function () {
                    self.uploadBtnVisible(false);
                    self.isDisabled(false);
                    self.isDisabledContacts(false);
                    self.isDisabledSite(false);
                    self.prevBtnVisible(false);
                    self.saveBtnVisible(true);
                    self.addBtnVisible(false);

                };

                self.con1 = ko.observable(false);
                self.con2 = ko.observable(false);
                self.con3 = ko.observable(false);
                self.con4 = ko.observable(false);


                self.site1 = ko.observable(false);
                self.site2 = ko.observable(false);


                self.addVisi = ko.observable(true);
                self.addVisib = ko.observable(true);
                self.removeVisi = ko.observable(false);
                self.removeVisib = ko.observable(false);





                self.addContact = function () {
                    contactfieldscount++;
                    if (contactfieldscount == 1) {
                        self.con1(true);
                        //contactfieldscount++;
                    }

                    if (contactfieldscount == 2) {
                        self.con2(true);
                        self.addVisi(false);
                        self.removeVisi(true);
                    }

                    if (contactfieldscount == 3) {
                        self.con3(true);
                        self.removeVisi(true);
                    }

                    if (contactfieldscount == 4) {
                        self.con4(true);
                        //self.addVisi(false);
                        self.removeVisi(true);
                    }

                };
                self.addContact = function () {
                             contactfieldscount++;
                    if (contactfieldscount == 1) {
                        self.con1(true);
                        //contactfieldscount++;
                    }

                    if (contactfieldscount == 2) {
                        self.con2(true);
                        //self.addVisi(false);
                        self.removeVisi(true);
                    }

                    if (contactfieldscount == 3) {
                        self.con3(true);
                        self.removeVisi(true);
                    }

                    if (contactfieldscount == 4) {
                        self.con4(true);
                        self.addVisi(false);
                        self.removeVisi(true);
                    }
//                    contactfieldscount++;
//                    if (contactfieldscount == 1) {
//                        self.con1(true);
//                        //contactfieldscount++;
//                    }
//
//                    if (contactfieldscount == 2) {
//                        self.con2(true);
//                        self.addVisi(false);
//                        self.removeVisi(true);
//                    }

                };

                self.removeContact = function () {

                    if (contactfieldscount == 0)
                    {
                        contactfieldscount = 4;
                    }
                    contactfieldscount--;  //3
                    if (contactfieldscount == 0) {
                        self.con1(false);
                        self.addVisi(true);
                        self.removeVisi(false);

                    }

                    if (contactfieldscount == 1) {
                        self.con2(false);

                    }

                    if (contactfieldscount == 2) {
                        self.con3(false);

                    }

                    if (contactfieldscount == 3) {
                        self.con4(false);

                    }

//                    if (contactfieldscount == 0)
//                    {
//                        contactfieldscount = 2;
//                    }
//                    contactfieldscount--;  //1
//                    if (contactfieldscount == 0) {
//                        self.con1(false);
//                        self.addVisi(true);
//                        self.removeVisi(false);
//
//                    }
//
//                    if (contactfieldscount == 1) {
//                        self.con2(false);
//
//                    }

                };

                self.addSite = function () {
                    sitefieldscount++;
                    if (sitefieldscount == 1) {
                        self.site1(true);
                        //sitefieldscount++;
                    }

                    if (sitefieldscount == 2) {
                        self.site2(true);
                        self.addVisib(false);
                        self.removeVisib(true);
                    }

                };


                self.removeSite = function () {

                    if (sitefieldscount == 0)
                    {
                        sitefieldscount = 2;
                    }
                    sitefieldscount--;  //1
                    if (sitefieldscount == 0) {
                        self.site1(false);
                        self.addVisib(true);
                        self.removeVisib(false);

                    }

                    if (sitefieldscount == 1) {
                        self.site2(false);

                    }

                };
                //attachment
                self.viewOpenAttachmentResumBtn = function () {

                    self.openAttachmentViewerRe(dataFiles.data);
                };
                self.openAttachmentViewerRe = function (data)
                {

                };
                self.attachmentSelectListener = function (event) {

                    var files = event.detail.files;

                    fillArrayAttachedArrayData(
                            files,
                            self.selectedItem());
                };
                self.removeSelectedAttachment = function (event) {

                    if (self.selectedItem() === 'newRegistration') {
                        removeSelectedAttachments(
                                self.selectedItemsAttachment(),
                                self.koArrayRemoved(),
                                self.selectedAttachmentType(),
                                self.selectedItem());
                    } else if (self.selectedItem() === 'vairation') {
                        removeSelectedAttachments(
                                self.vairationSelectedItemsAttachment(),
                                self.koArrayRemoved(),
                                self.vairationSelectedAttachmentType(),
                                self.selectedItem());
                    } else if (self.selectedItem() === 'renewal') {
                        removeSelectedAttachments(
                                self.renewalSelectedItemsAttachment(),
                                self.koArrayRemoved(),
                                self.renewalSelectedAttachmentType(),
                                self.selectedItem());
                    } else if (self.selectedItem() === 'productClassification') {
                        removeSelectedAttachments(
                                self.productClassificationSelectedItemsAttachment(),
                                self.koArrayRemoved(),
                                self.productSelectedAttachmentType(),
                                self.selectedItem());
                    } else if (self.selectedItem() === 'referenceStandardAvailability') {
                        removeSelectedAttachments(
                                self.referenceStandardAvailabilitySelectedItemsAttachment(),
                                self.koArrayRemoved(),
                                self.referenceStanderSelectedAttachmentType(),
                                self.selectedItem());
                    } else if (self.selectedItem() === 'generalDocuments') {
                        removeSelectedAttachments(
                                self.generalDocumentsSelectedItemsAttachment(),
                                self.koArrayRemoved(),
                                self.generalDocumentSelectedAttachmentType(),
                                self.selectedItem());
                    }
                };

                self.openAttachmentViewer = function ()
                {

                    if (self.selectedItem() === 'newRegistration') {
                        $.each(self.selectedItemsAttachment(), function (index, value) {
//                            if (self.selectedAttachmentType() === 'registeration') {
                            viewAttachment(self.koArray(), value);
//                            } else if (self.selectedAttachmentType() === 'renewal') {
//                                viewAttachment(self.renwalArray(), value);
//                            } else if (self.selectedAttachmentType() === 'site') {
//                                viewAttachment(self.siteArray(), value);
//                            }
                        });
                    } else if (self.selectedItem() === 'vairation') {
                        self.selectedItemHistory = ko.observable("vairationHistory");
                        $.each(self.vairationSelectedItemsAttachment(), function (index, value) {

//                            if (self.vairationSelectedAttachmentType() === 'registeration') {
                            viewAttachment(self.variationKoArray(), value);
//                            } else if (self.vairationSelectedAttachmentType() === 'renewal') {
//                                viewAttachment(self.vairationRenwalArray(), value);
//                            } else if (self.vairationSelectedAttachmentType() === 'site') {
//                                viewAttachment(self.vairationSiteArray(), value);
//                            }
                        });
                    } else if (self.selectedItem() === 'renewal') {
                        $.each(self.renewalSelectedItemsAttachment(), function (index, value) {

//                            if (self.renewalSelectedAttachmentType() === 'registeration') {
                            viewAttachment(self.renewalKoArray(), value);
//                            } else if (self.renewalSelectedAttachmentType() === 'renewal') {
//                                viewAttachment(self.renewalRenwalArray(), value);
//                            } else if (self.renewalSelectedAttachmentType() === 'site') {
//                                viewAttachment(self.renewalSiteArray(), value);
//                            }
                        });
                    } else if (self.selectedItem() === 'productClassification') {
                        $.each(self.productClassificationSelectedItemsAttachment(), function (index, value) {

//                            if (self.productSelectedAttachmentType() === 'registeration') {
                            viewAttachment(self.productClassificationKoArray(), value);
//                            } else if (self.productSelectedAttachmentType() === 'renewal') {
//                                viewAttachment(self.productClassificationRenwalArray(), value);
//                            } else if (self.productSelectedAttachmentType() === 'site') {
//                                viewAttachment(self.productClassificationSiteArray(), value);
//                            }
                        });
                    } else if (self.selectedItem() === 'referenceStandardAvailability') {
                        $.each(self.referenceStandardAvailabilitySelectedItemsAttachment(), function (index, value) {

//                            if (self.referenceStanderSelectedAttachmentType() === 'registeration') {
                            viewAttachment(self.refereceStandardKoArray(), value);
//                            } else if (self.referenceStanderSelectedAttachmentType() === 'renewal') {
//                                viewAttachment(self.referenceStandardAvailabilityRenwalArray(), value);
//                            } else if (self.referenceStanderSelectedAttachmentType() === 'site') {
//                                viewAttachment(self.referenceStandardAvailabilitySiteArray(), value);
//                            }
                        });
                    } else if (self.selectedItem() === 'generalDocuments') {
                        $.each(self.generalDocumentsSelectedItemsAttachment(), function (index, value) {

//                            if (self.generalDocumentSelectedAttachmentType() === 'registeration') {
                            viewAttachment(self.generalDocumentKoArray(), value);
//                            } else if (self.generalDocumentSelectedAttachmentType() === 'renewal') {
//                                viewAttachment(self.generalDocumentsRenwalArray(), value);
//                            } else if (self.generalDocumentSelectedAttachmentType() === 'site') {
//                                viewAttachment(self.generalDocumentsSiteArray(), value);
//                            }
                        });
                    }

                };
                self.closeAttachmentViewer = function (event) {
                    self.attachmentViewerData('');
                    document.getElementById('attachmentDialog').close();
                };
                self.changeimage = function () {


                };
                self.listViewSelectionChange = function (event) {
                    //  console.log(event.detail.value);
                    if (event.detail.value !== null && event.detail.value.length > 0)
                    {
                        self.attachmentBtnVisible(true);
                    }
                };

                self.goBackFunction = function () {

                    oj.Router.rootInstance.go('searchProductRegistration');
                };



//Arrays for Lists Start

                this.newRegisterArr = [
                    //  {value: 'New_Product_Registration_MOH_submission', label: 'New Product Registration - MOH submission'},
                    {value: 'New_Product_Registration_Pending_for_requirements', label: 'New Product Registration - Pending for requirements'},
                    {value: 'New_Product_Registration_Review', label: 'New Product Registration - MOH Review'},
                    {value: 'New_Product_Registration_Registered', label: 'New Product Registration - Registered'}
                ];

                this.variationArr = [
                    // {value: 'Variation_MOH_submission', label: 'Variation - MOH submission'},
                    {value: 'Variation_Pending_for_requirements', label: 'Variation - Pending for requirements'},
                    {value: 'Variation_Review', label: 'Variation - MOH Review'},
                    {value: 'Variation_Registered', label: 'Variation - Approved'}
                ];


                this.renewalArr = [
                    //  {value: 'Renewal_MOH_submission', label: 'Renewal - MOH submission'},
                    {value: 'Renewal_Pending_for_requirements', label: 'Renewal - Pending for requirements'},
                    {value: 'Renewal_Review', label: 'Renewal - MOH Review'},
                    {value: 'Renewal_Registered', label: 'Renewal - Approved'}
                ];


                this.yesNoArr = [
                    {value: 'yes', label: 'Yes'},
                    {value: 'no', label: 'No'}
                ];




                this.refStandAvaArr = [
                    {value: 'MOH_submission', label: 'MOH submission'},
                    {value: 'Pending_for_requirements', label: 'Pending for requirements'}
                ];

                this.refAvailabilityArr = [
                    {value: 'Yes', label: 'Yes'},
                    {value: 'No', label: 'No'}
                ];
//Arrays for Lists Start 
                self.visibleTab = ko.observable();
// to show the oj-validation-group's valid property value
                self.productRegisTabsgroupValid = ko.observable();
                self.productRegisTabsgroupValid2 = ko.observable();
                self.productRegisTabsgroupValid3 = ko.observable();
                self.productRegisTabsgroupValid4 = ko.observable();
                self.productRegisTabsgroupValid5 = ko.observable();
                self.productRegisTabsgroupValid6 = ko.observable();
                self.selectedItem = ko.observable("newRegistration");


                self.prodRegisTabsDetailsLblsModel = {
                    newRegistrationLbl: ko.observable(),
                    variationLbl: ko.observable(),
                    kindOfVariationLbl: ko.observable(),
                    renewalLbl: ko.observable(),
                    kindOfRenewalLbl: ko.observable(),
                    nextRenewalDateLbl: ko.observable(),
                    productClassificationLbl: ko.observable(),
                    restrictedLbl: ko.observable(),
                    importLicenceLbl: ko.observable(),
                    refStandAvaLbl: ko.observable(),
                    refAvailabilityLbl: ko.observable(),
                    kindOfRSTDLbl: ko.observable(),
                    dateOfRequestLbl: ko.observable(),
                    generalDocumentsLbl: ko.observable(),
                    kindOfDocumentsLbl: ko.observable(),
                    dateOfSubmissionLbl: ko.observable(),
                    dateOfReceivedRequirementsLbl: ko.observable(),
                    dateOfRequestRequirementToSupplierLbl: ko.observable(),
                    followUpDateLbl: ko.observable(),
                    approvedDateLbl: ko.observable(),
                    commentsLbl: ko.observable(),
                    attachmentTypeLbl: ko.observable(),
                    fileUploadStatusLbl: ko.observable(),
                    attachmentLbl: ko.observable()

                };

                self.prodRegisTabsDetailsModel = {
                    detailsId: ko.observable(),
                    productRegistrationId: ko.observable(),
                    newRegistration: ko.observable(),
                    newRegisDos: ko.observable(),
                    newRegisDoReceivReqs: ko.observable(),
                    newRegisDoReqRequirToSup: ko.observable(),
                    newRegisFollowUpDate: ko.observable(),
                    newRegisApprovedDate: ko.observable(),
                    newRegisComments: ko.observable(),
                    variation: ko.observable(),
                    kindOfVariation: ko.observable(),
                    variationDos: ko.observable(),
                    variationDoReceivReqs: ko.observable(),
                    variationDoReqRequirToSup: ko.observable(),
                    variationFollowUpDate: ko.observable(),
                    variationApprovedDate: ko.observable(),
                    variationComments: ko.observable(),
                    renewal: ko.observable(),
                    kindOfRenewal: ko.observable(),
                    renewalDos: ko.observable(),
                    renewalDoReceivReqs: ko.observable(),
                    renewalDoReqRequirToSup: ko.observable(),
                    renewalFollowUpDate: ko.observable(),
                    renewalApprovedDate: ko.observable(),
                    renewalComments: ko.observable(),
                    nextRenewalDate: ko.observable(),
                    productClassification: ko.observable(),
                    restricted: ko.observable(),
                    importLicence: ko.observable(),
                    proClassDos: ko.observable(),
                    proClassFollowUpDate: ko.observable(),
                    proClassApprovedDate: ko.observable(),
                    proClassComments: ko.observable(),
                    refStandAva: ko.observable(),
                    refAvailability: ko.observable(),
                    kindOfRSTD: ko.observable(),
                    refStandAvaDos: ko.observable(),
                    refStandAvaFollowUpDate: ko.observable(),
                    refStandAvaDoReceivReqs: ko.observable(),
                    dateOfRequest: ko.observable(),
                    refStandAvaApprovedDate: ko.observable(),
                    refStandAvaComments: ko.observable(),
                    generalDocuments: ko.observable(),
                    kindOfDocuments: ko.observable(),
                    geneDocsDos: ko.observable(),
                    geneDocsDoReceivReqs: ko.observable(),
                    geneDocsDoReqRequirToSup: ko.observable(),
                    geneDocsFollowUpDate: ko.observable(),
                    geneDocsApprovedDate: ko.observable(),
                    geneDocsComments: ko.observable(),
                    attachmentType: ko.observable(),
                    vairationAttachmentType: ko.observable(),
                    renewalAttachmentType: ko.observable(),
                    productClassificationAttachmentType: ko.observable(),
                    referenceStanderAttachmentType: ko.observable(),
                    generalDocumentAttachmentType: ko.observable(),
                };

                self.ReChangeHisotryHandler = function (event) {
                    var selectedTab = self.selectedItem() + "History";

                    self.selectedItemHistory(selectedTab);
                    //console.log(self.selectedItemHistory());
                };

                self.attachmentTypeValueChangeHandler = function (event) {

                    var listEvent = event['detail'];
//                    if (self.retrieve.type == 'view') {
//                        self.uploadBtnVisible(true);
//                    }else if(self.retrieve.type == 'update'){
//                        self.uploadBtnVisible(false);
//                    }
                    if (self.selectedItem() === 'newRegistration')
                    {
                        self.selectedAttachmentType('');
                        self.selectedAttachmentType(listEvent.value);

                        self.koArray([]);
                        if (self.selectedAttachmentType() !== null && self.selectedAttachmentType() !== '')
                        {
                            if (self.selectedAttachmentType() === 'registeration') {
                                self.koArray(self.registrationArray());

                            } else if (self.selectedAttachmentType() === 'renewal') {
                                self.koArray(self.renwalArray());

                            } else if (self.selectedAttachmentType() === 'site') {
                                self.koArray(self.siteArray());

                            }
                            //  self.viewAttachmentBtnVisible(true);

                        } else
                        {
                            //  self.viewAttachmentBtnVisible(false);
                        }
                    } else if (self.selectedItem() === 'vairation') {
                        self.vairationSelectedAttachmentType('');
                        self.vairationSelectedAttachmentType(listEvent.value);

                        self.variationKoArray([]);
                        if (self.vairationSelectedAttachmentType() !== null && self.vairationSelectedAttachmentType() !== '')
                        {
                            if (self.vairationSelectedAttachmentType() === 'registeration') {
                                self.variationKoArray(self.vairationRegistrationArray());

                            } else if (self.vairationSelectedAttachmentType() === 'renewal') {
                                self.variationKoArray(self.vairationRenwalArray());

                            } else if (self.vairationSelectedAttachmentType() === 'site') {
                                self.variationKoArray(self.vairationSiteArray());

                            }
                            //     self.viewAttachmentBtnVisible2(true);
                        } else
                        {
                            //       self.viewAttachmentBtnVisible2(false);
                        }
                    } else if (self.selectedItem() === 'renewal') {
                        self.renewalSelectedAttachmentType('');
                        self.renewalSelectedAttachmentType(listEvent.value);

                        self.renewalKoArray([]);
                        if (self.renewalSelectedAttachmentType() !== null && self.renewalSelectedAttachmentType() !== '')
                        {
                            if (self.renewalSelectedAttachmentType() === 'registeration') {
                                self.renewalKoArray(self.renewalRegistrationArray());

                            } else if (self.renewalSelectedAttachmentType() === 'renewal') {
                                self.renewalKoArray(self.renewalRenwalArray());

                            } else if (self.renewalSelectedAttachmentType() === 'site') {
                                self.renewalKoArray(self.renewalSiteArray());

                            }
                            //     self.viewAttachmentBtnVisible3(true);
                        } else
                        {
                            //      self.viewAttachmentBtnVisible3(false);
                        }
                    } else if (self.selectedItem() === 'productClassification') {
                        self.productSelectedAttachmentType('');
                        self.productSelectedAttachmentType(listEvent.value);

                        self.productClassificationKoArray([]);
                        if (self.productSelectedAttachmentType() !== null && self.productSelectedAttachmentType() !== '')
                        {
                            if (self.productSelectedAttachmentType() === 'registeration') {
                                self.productClassificationKoArray(self.productClassificationRegistrationArray());

                            } else if (self.productSelectedAttachmentType() === 'renewal') {
                                self.productClassificationKoArray(self.productClassificationRenwalArray());

                            } else if (self.productSelectedAttachmentType() === 'site') {
                                self.productClassificationKoArray(self.productClassificationSiteArray());

                            }
                            //        self.viewAttachmentBtnVisible4(true);
                        } else
                        {
                            //        self.viewAttachmentBtnVisible4(false);
                        }
                    } else if (self.selectedItem() === 'referenceStandardAvailability') {
                        self.referenceStanderSelectedAttachmentType('');
                        self.referenceStanderSelectedAttachmentType(listEvent.value);

                        self.refereceStandardKoArray([]);
                        if (self.referenceStanderSelectedAttachmentType() !== null && self.referenceStanderSelectedAttachmentType() !== '')
                        {
                            if (self.referenceStanderSelectedAttachmentType() === 'registeration') {
                                self.refereceStandardKoArray(self.referenceStandardAvailabilityRegistrationArray());

                            } else if (self.referenceStanderSelectedAttachmentType() === 'renewal') {
                                self.refereceStandardKoArray(self.referenceStandardAvailabilityRenwalArray());

                            } else if (self.referenceStanderSelectedAttachmentType() === 'site') {
                                self.refereceStandardKoArray(self.referenceStandardAvailabilitySiteArray());

                            }
                            //           self.viewAttachmentBtnVisible5(true);
                        } else
                        {
                            //            self.viewAttachmentBtnVisible5(false);
                        }
                    } else if (self.selectedItem() === 'generalDocuments') {
                        self.generalDocumentSelectedAttachmentType('');
                        self.generalDocumentSelectedAttachmentType(listEvent.value);

                        self.generalDocumentKoArray([]);
                        if (self.generalDocumentSelectedAttachmentType() !== null && self.generalDocumentSelectedAttachmentType() !== '')
                        {
                            if (self.generalDocumentSelectedAttachmentType() === 'registeration') {
                                self.generalDocumentKoArray(self.generalDocumentsRegistrationArray());

                            } else if (self.generalDocumentSelectedAttachmentType() === 'renewal') {
                                self.generalDocumentKoArray(self.generalDocumentsRenwalArray());

                            } else if (self.generalDocumentSelectedAttachmentType() === 'site') {
                                self.generalDocumentKoArray(self.generalDocumentsSiteArray());

                            }
                            //      self.viewAttachmentBtnVisible6(true);
                        } else
                        {
                            //        self.viewAttachmentBtnVisible6(false);
                        }
                    }


                };





                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };

                self.prodRegisTabsDetailsAction = function () {

                    var type1 = self.prodRegisTabsDetailsModel.attachmentType();
                    var type2 = self.prodRegisTabsDetailsModel.vairationAttachmentType();
                    var type3 = self.prodRegisTabsDetailsModel.renewalAttachmentType();
                    var type4 = self.prodRegisTabsDetailsModel.productClassificationAttachmentType();
                    var type5 = self.prodRegisTabsDetailsModel.referenceStanderAttachmentType();
                    var type6 = self.prodRegisTabsDetailsModel.generalDocumentAttachmentType();
                    var jsonData = ko.toJSON(self.prodRegisTabsDetailsModel);

                    var prodRegisTabsDetailsCbFn = function (data) {

                        var attachmentId;
                        if (typeof data.prodRegisTabsDetId === 'undefined') {
                            attachmentId = productDetailsId;
                        } else {
                            attachmentId = data.prodRegisTabsDetId;
                        }

                        /////////////////////////////////////
                        if (self.koArray().length > 0) {
                            fillAttachedArray(self.koArray(), attachmentId, type1, productMasterId);
                        }
//                        if (self.registrationArray().length > 0) {
//                            fillAttachedArray(self.registrationArray(), attachmentId, 'registration', productMasterId);
//                        }
//                        if (self.renwalArray().length > 0) {
//                            fillAttachedArray(self.renwalArray(), attachmentId, 'renwal', productMasterId);
//                        }
//                        if (self.siteArray().length > 0) {
//                            fillAttachedArray(self.siteArray(), attachmentId, 'site', productMasterId);
//                        }
                        //////////////////////////////////////
                        if (self.variationKoArray().length > 0) {
                            fillAttachedArray(self.variationKoArray(), attachmentId, type2, productMasterId);
                        }
//                        if (self.vairationRenwalArray().length > 0) {
//                            fillAttachedArray(self.vairationRenwalArray(), attachmentId, 'renwal', productMasterId);
//                        }
//                        if (self.vairationSiteArray().length > 0) {
//                            fillAttachedArray(self.vairationSiteArray(), attachmentId, 'site', productMasterId);
//                        }
                        ///////////////////////////////////////////////
                        if (self.renewalKoArray().length > 0) {
                            fillAttachedArray(self.renewalKoArray(), attachmentId, type3, productMasterId);
                        }
//                        if (self.renewalRenwalArray().length > 0) {
//                            fillAttachedArray(self.renewalRenwalArray(), attachmentId, 'renwal', productMasterId);
//                        }
//                        if (self.renewalSiteArray().length > 0) {
//                            fillAttachedArray(self.renewalSiteArray(), attachmentId, 'site', productMasterId);
//                        }
                        ///////////////////////////////////////////
                        if (self.productClassificationKoArray().length > 0) {
                            fillAttachedArray(self.productClassificationKoArray(), attachmentId, type4, productMasterId);
                        }
//                        if (self.productClassificationRenwalArray().length > 0) {
//                            fillAttachedArray(self.productClassificationRenwalArray(), attachmentId, 'renwal', productMasterId);
//                        }
//                        if (self.productClassificationSiteArray().length > 0) {
//                            fillAttachedArray(self.productClassificationSiteArray(), attachmentId, 'site', productMasterId);
//                        }
                        //////////////////////////////////////
                        if (self.refereceStandardKoArray().length > 0) {
                            fillAttachedArray(self.refereceStandardKoArray(), attachmentId, type5, productMasterId);
                        }
//                        if (self.referenceStandardAvailabilityRenwalArray().length > 0) {
//                            fillAttachedArray(self.referenceStandardAvailabilityRenwalArray(), attachmentId, 'renwal', productMasterId);
//                        }
//                        if (self.referenceStandardAvailabilitySiteArray().length > 0) {
//                            fillAttachedArray(self.referenceStandardAvailabilitySiteArray(), attachmentId, 'site', productMasterId);
//                        }
                        /////////////////////////////////////////
                        if (self.generalDocumentKoArray().length > 0) {
                            fillAttachedArray(self.generalDocumentKoArray(), attachmentId, type6, productMasterId);
                        }
//                        if (self.generalDocumentsRenwalArray().length > 0) {
//                            fillAttachedArray(self.generalDocumentsRenwalArray(), attachmentId, 'renwal', productMasterId);
//                        }
//                        if (self.generalDocumentsSiteArray().length > 0) {
//                            fillAttachedArray(self.generalDocumentsSiteArray(), attachmentId, 'site', productMasterId);
//                        }
                        /////////////////////////////////////

                        if (self.koArrayRemoved().length > 0)
                        {
                            for (var i = 0; i < self.koArrayRemoved().length; i++) {
                                deleteAttachmentById(self.koArrayRemoved()[i].id);
                            }
                        }
                        var success = function () {
                            self.koArray([]);
                            self.renewalKoArray([]);
                            self.variationKoArray([]);
                            self.productClassificationKoArray([]);
                            self.refereceStandardKoArray([]);
                            self.generalDocumentKoArray([]);
                            self.registrationArray([]);
                            self.renwalArray([]);
                            self.siteArray([]);
                            self.vairationRegistrationArray([]);
                            self.vairationRenwalArray([]);
                            self.vairationSiteArray([]);
                            self.renewalRegistrationArray([]);
                            self.renewalRenwalArray([]);
                            self.renewalSiteArray([]);
                            self.productClassificationRegistrationArray([]);
                            self.productClassificationRenwalArray([]);
                            self.productClassificationSiteArray([]);
                            self.referenceStandardAvailabilityRegistrationArray([]);
                            self.referenceStandardAvailabilityRenwalArray([]);
                            self.referenceStandardAvailabilitySiteArray([]);
                            self.generalDocumentsRegistrationArray([]);
                            self.generalDocumentsRenwalArray([]);
                            self.generalDocumentsSiteArray([]);
                            self.allAttachedData([]);
                        };
                        var fail = function () {};

                        services.addGeneric("productAttachments/addAttachment", ko.toJSON(self.allAttachedData())).then(success, fail);

                    };
                    var failCbFn = function () {
                    };

                    function addDetails() {
                        var addURL = "ProductRegisDetailsRest/addProductRegisDetails";

                        services.addGeneric(addURL, jsonData).then(prodRegisTabsDetailsCbFn, failCbFn);
                    }
                    if (self.serviceType() == 'ADD') {
                        addDetails();
                    }
                    if (self.serviceType() == 'EDIT') {

                        addDetails();


                    }

                };

                var productDetailsId;
                self.getProdRegisTabsDetails = function () {


                    var getProdRegisTabsDetailsCbFn = function (data) {
                        //   console.log(data);
                        self.productRegisDataArr([]);

                        var i = data.items.length - 1;
                        if (data.items.length !== 0) {
                            //     console.log(data.items);
                            productDetailsId = data.items[i].id;
                            self.prodRegisTabsDetailsModel.detailsId(data.items[i].product_registration_id);
                            self.prodRegisTabsDetailsModel.productRegistrationId(data.items[i].product_registration_id);
                            productMasterId = data.items[i].product_registration_id;
                            getAttachmentById(data.items[i].product_registration_id);
                            self.attachmentID(data.items[i].productDetailsId);
                            self.prodRegisTabsDetailsModel.newRegistration(data.items[i].new_registration);



                            if (isEmpty(data.items[i].new_regis_dos))
                                self.prodRegisTabsDetailsModel.newRegisDos("");
                            else {

                                self.prodRegisTabsDetailsModel.newRegisDos(self.formatDate(new Date(data.items[i].new_regis_dos)));
                            }

                            if (isEmpty(data.items[i].new_regis_do_receiv_reqs))
                                self.prodRegisTabsDetailsModel.newRegisDoReceivReqs("");
                            else {

                                self.prodRegisTabsDetailsModel.newRegisDoReceivReqs(self.formatDate(new Date(data.items[i].new_regis_do_receiv_reqs)));
                            }
                            if (isEmpty(data.items[i].new_regis_do_req_reqir_to_sup))
                                self.prodRegisTabsDetailsModel.newRegisDoReqRequirToSup("");
                            else {

                                self.prodRegisTabsDetailsModel.newRegisDoReqRequirToSup(self.formatDate(new Date(data.items[i].new_regis_do_req_reqir_to_sup)));
                            }
                            if (isEmpty(data.items[i].new_regis_follow_up_date))
                                self.prodRegisTabsDetailsModel.newRegisFollowUpDate("");
                            else {

                                self.prodRegisTabsDetailsModel.newRegisFollowUpDate(self.formatDate(new Date(data.items[i].new_regis_follow_up_date)));
                            }
                            if (isEmpty(data.items[i].new_regis_approved_date))
                                self.prodRegisTabsDetailsModel.newRegisApprovedDate("");
                            else {

                                self.prodRegisTabsDetailsModel.newRegisApprovedDate(self.formatDate(new Date(data.items[i].new_regis_approved_date)));
                            }
                            self.prodRegisTabsDetailsModel.newRegisComments(data.items[i].new_regis_comments);
                            self.prodRegisTabsDetailsModel.variation(data.items[i].variation);
                            self.prodRegisTabsDetailsModel.kindOfVariation(data.items[i].kind_of_variation);
                            if (isEmpty(data.items[i].variation_dos))
                                self.prodRegisTabsDetailsModel.variationDos("");
                            else {

                                self.prodRegisTabsDetailsModel.variationDos(self.formatDate(new Date(data.items[i].variation_dos)));
                            }
                            if (isEmpty(data.items[i].variation_do_receiv_reqs))
                                self.prodRegisTabsDetailsModel.variationDoReceivReqs("");
                            else {

                                self.prodRegisTabsDetailsModel.variationDoReceivReqs(self.formatDate(new Date(data.items[i].variation_do_receiv_reqs)));
                            }
                            if (isEmpty(data.items[i].variation_do_req_reqir_to_sup))
                                self.prodRegisTabsDetailsModel.variationDoReqRequirToSup("");
                            else {

                                self.prodRegisTabsDetailsModel.variationDoReqRequirToSup(self.formatDate(new Date(data.items[i].variation_do_req_reqir_to_sup)));
                            }
                            if (isEmpty(data.items[i].variation_follow_up_date))
                                self.prodRegisTabsDetailsModel.variationFollowUpDate("");
                            else {

                                self.prodRegisTabsDetailsModel.variationFollowUpDate(self.formatDate(new Date(data.items[i].variation_follow_up_date)));
                            }
                            if (isEmpty(data.items[i].variation_approved_date))
                                self.prodRegisTabsDetailsModel.variationApprovedDate("");
                            else {

                                self.prodRegisTabsDetailsModel.variationApprovedDate(self.formatDate(new Date(data.items[i].variation_approved_date)));
                            }
                            self.prodRegisTabsDetailsModel.variationComments(data.items[i].variation_comments);
                            self.prodRegisTabsDetailsModel.renewal(data.items[i].renewal);
                            self.prodRegisTabsDetailsModel.kindOfRenewal(data.items[i].kind_of_renewal);
                            if (isEmpty(data.items[i].renewal_dos))
                                self.prodRegisTabsDetailsModel.renewalDos("");
                            else {

                                self.prodRegisTabsDetailsModel.renewalDos(self.formatDate(new Date(data.items[i].renewal_dos)));
                            }
                            if (isEmpty(data.items[i].renewal_do_receiv_reqs))
                                self.prodRegisTabsDetailsModel.renewalDoReceivReqs("");
                            else {

                                self.prodRegisTabsDetailsModel.renewalDoReceivReqs(self.formatDate(new Date(data.items[i].renewal_do_receiv_reqs)));
                            }
                            if (isEmpty(data.items[i].renewal_do_req_reqir_to_sup))
                                self.prodRegisTabsDetailsModel.renewalDoReqRequirToSup("");
                            else {

                                self.prodRegisTabsDetailsModel.renewalDoReqRequirToSup(self.formatDate(new Date(data.items[i].renewal_do_req_reqir_to_sup)));
                            }
                            if (isEmpty(data.items[i].renewal_date_follow_up_date))
                                self.prodRegisTabsDetailsModel.renewalFollowUpDate("");
                            else {

                                self.prodRegisTabsDetailsModel.renewalFollowUpDate(self.formatDate(new Date(data.items[i].renewal_date_follow_up_date)));
                            }
                            if (isEmpty(data.items[i].renewal_date_approved_date))
                                self.prodRegisTabsDetailsModel.renewalApprovedDate("");
                            else {

                                self.prodRegisTabsDetailsModel.renewalApprovedDate(self.formatDate(new Date(data.items[i].renewal_date_approved_date)));
                            }
                            self.prodRegisTabsDetailsModel.renewalComments(data.items[i].renewal_date_comments);
                            if (isEmpty(data.items[i].next_renewal_date))
                                self.prodRegisTabsDetailsModel.nextRenewalDate("");
                            else {

                                self.prodRegisTabsDetailsModel.nextRenewalDate(self.formatDate(new Date(data.items[i].next_renewal_date)));
                            }
                            self.prodRegisTabsDetailsModel.productClassification(data.items[i].pro_class);
                            self.prodRegisTabsDetailsModel.restricted(data.items[i].restricted);
                            self.prodRegisTabsDetailsModel.importLicence(data.items[i].import_licence);
                            if (isEmpty(data.items[i].pro_class_dos))
                                self.prodRegisTabsDetailsModel.proClassDos("");
                            else {

                                self.prodRegisTabsDetailsModel.proClassDos(self.formatDate(new Date(data.items[i].pro_class_dos)));
                            }
                            if (isEmpty(data.items[i].pro_class_follow_up_date))
                                self.prodRegisTabsDetailsModel.proClassFollowUpDate("");
                            else {

                                self.prodRegisTabsDetailsModel.proClassFollowUpDate(self.formatDate(new Date(data.items[i].pro_class_follow_up_date)));
                            }
                            if (isEmpty(data.items[i].pro_class_approved_date))
                                self.prodRegisTabsDetailsModel.proClassApprovedDate("");
                            else {

                                self.prodRegisTabsDetailsModel.proClassApprovedDate(self.formatDate(new Date(data.items[i].pro_class_approved_date)));
                            }
                            self.prodRegisTabsDetailsModel.proClassComments(data.items[i].pro_class_comments);
                            self.prodRegisTabsDetailsModel.refStandAva(data.items[i].ref_stand_ava);
                            self.prodRegisTabsDetailsModel.refAvailability(data.items[i].ref_availability);
                            self.prodRegisTabsDetailsModel.kindOfRSTD(data.items[i].kind_of_rstd);
                            if (isEmpty(data.items[i].ref_stand_ava_dos))
                                self.prodRegisTabsDetailsModel.refStandAvaDos("");
                            else {

                                self.prodRegisTabsDetailsModel.refStandAvaDos(self.formatDate(new Date(data.items[i].ref_stand_ava_dos)));
                            }
                            if (isEmpty(data.items[i].ref_stand_ava_follow_up_date))
                                self.prodRegisTabsDetailsModel.refStandAvaFollowUpDate("");
                            else {

                                self.prodRegisTabsDetailsModel.refStandAvaFollowUpDate(self.formatDate(new Date(data.items[i].ref_stand_ava_follow_up_date)));
                            }
                            if (isEmpty(data.items[i].ref_stand_ava_do_receiv_reqs))
                                self.prodRegisTabsDetailsModel.refStandAvaDoReceivReqs("");
                            else {

                                self.prodRegisTabsDetailsModel.refStandAvaDoReceivReqs(self.formatDate(new Date(data.items[i].ref_stand_ava_do_receiv_reqs)));
                            }
                            if (isEmpty(data.items[i].date_of_request))
                                self.prodRegisTabsDetailsModel.dateOfRequest("");
                            else {

                                self.prodRegisTabsDetailsModel.dateOfRequest(self.formatDate(new Date(data.items[i].date_of_request)));
                            }
                            if (isEmpty(data.items[i].ref_stand_ava_approved_date))
                                self.prodRegisTabsDetailsModel.refStandAvaApprovedDate("");
                            else {

                                self.prodRegisTabsDetailsModel.refStandAvaApprovedDate(self.formatDate(new Date(data.items[i].ref_stand_ava_approved_date)));
                            }
                            self.prodRegisTabsDetailsModel.refStandAvaComments(data.items[i].ref_stand_ava_comments);
                            self.prodRegisTabsDetailsModel.generalDocuments(data.items[i].general_documents);
                            self.prodRegisTabsDetailsModel.kindOfDocuments(data.items[i].kind_of_documents);
                            if (isEmpty(data.items[i].gene_docs_dos))
                                self.prodRegisTabsDetailsModel.geneDocsDos("");
                            else {

                                self.prodRegisTabsDetailsModel.geneDocsDos(self.formatDate(new Date(data.items[i].gene_docs_dos)));
                            }
                            if (isEmpty(data.items[i].gene_docs_do_receiv_reqs))
                                self.prodRegisTabsDetailsModel.geneDocsDoReceivReqs("");
                            else {

                                self.prodRegisTabsDetailsModel.geneDocsDoReceivReqs(self.formatDate(new Date(data.items[i].gene_docs_do_receiv_reqs)));
                            }
                            if (isEmpty(data.items[i].gene_docs_do_req_reqir_to_sup))
                                self.prodRegisTabsDetailsModel.geneDocsDoReqRequirToSup("");
                            else {

                                self.prodRegisTabsDetailsModel.geneDocsDoReqRequirToSup(self.formatDate(new Date(data.items[i].gene_docs_do_req_reqir_to_sup)));
                            }
                            if (isEmpty(data.items[i].gene_docs_follow_up_date))
                                self.prodRegisTabsDetailsModel.geneDocsFollowUpDate("");
                            else {

                                self.prodRegisTabsDetailsModel.geneDocsFollowUpDate(self.formatDate(new Date(data.items[i].gene_docs_follow_up_date)));
                            }
                            if (isEmpty(data.items[i].gene_docs_approved_date))
                                self.prodRegisTabsDetailsModel.geneDocsApprovedDate("");
                            else {

                                self.prodRegisTabsDetailsModel.geneDocsApprovedDate(self.formatDate(new Date(data.items[i].gene_docs_approved_date)));
                            }
                            self.prodRegisTabsDetailsModel.geneDocsComments(data.items[i].gene_docs_comments);
                            self.productRegisDataArr(data.items);



                        }
                        ;



                    };

                    var failCbFn = function () {
                    };

                    var service = "ProductRegisDetailsRest/getProductRegisDetails/" + self.retrieve.id;
                    services.getGeneric(service).then(getProdRegisTabsDetailsCbFn, failCbFn);

                };




                function clearProdRegisDetailsContent() {

                    self.prodRegisTabsDetailsModel.newRegistration("");
                    self.prodRegisTabsDetailsModel.newRegisDos("");
                    self.prodRegisTabsDetailsModel.newRegisDoReceivReqs("");
                    self.prodRegisTabsDetailsModel.newRegisDoReqRequirToSup("");
                    self.prodRegisTabsDetailsModel.newRegisFollowUpDate("");
                    self.prodRegisTabsDetailsModel.newRegisApprovedDate("");
                    self.prodRegisTabsDetailsModel.newRegisComments("");
                    self.prodRegisTabsDetailsModel.variation("");
                    self.prodRegisTabsDetailsModel.kindOfVariation("");
                    self.prodRegisTabsDetailsModel.variationDos("");
                    self.prodRegisTabsDetailsModel.variationDoReceivReqs("");
                    self.prodRegisTabsDetailsModel.variationDoReqRequirToSup("");
                    self.prodRegisTabsDetailsModel.variationFollowUpDate("");
                    self.prodRegisTabsDetailsModel.variationApprovedDate("");
                    self.prodRegisTabsDetailsModel.variationComments("");
                    self.prodRegisTabsDetailsModel.renewal("");
                    self.prodRegisTabsDetailsModel.kindOfRenewal("");
                    self.prodRegisTabsDetailsModel.renewalDos("");
                    self.prodRegisTabsDetailsModel.renewalDoReceivReqs("");
                    self.prodRegisTabsDetailsModel.renewalDoReqRequirToSup("");
                    self.prodRegisTabsDetailsModel.renewalFollowUpDate("");
                    self.prodRegisTabsDetailsModel.renewalApprovedDate("");
                    self.prodRegisTabsDetailsModel.renewalComments("");
                    self.prodRegisTabsDetailsModel.nextRenewalDate("");
                    self.prodRegisTabsDetailsModel.productClassification("");
                    self.prodRegisTabsDetailsModel.restricted("");
                    self.prodRegisTabsDetailsModel.importLicence("");
                    self.prodRegisTabsDetailsModel.proClassDos("");
                    self.prodRegisTabsDetailsModel.proClassFollowUpDate("");
                    self.prodRegisTabsDetailsModel.proClassApprovedDate("");
                    self.prodRegisTabsDetailsModel.proClassComments("");
                    self.prodRegisTabsDetailsModel.refStandAva("");
                    self.prodRegisTabsDetailsModel.refAvailability("");
                    self.prodRegisTabsDetailsModel.kindOfRSTD("");
                    self.prodRegisTabsDetailsModel.refStandAvaDos("");
                    self.prodRegisTabsDetailsModel.refStandAvaFollowUpDate("");
                    self.prodRegisTabsDetailsModel.refStandAvaDoReceivReqs("");
                    self.prodRegisTabsDetailsModel.dateOfRequest("");
                    self.prodRegisTabsDetailsModel.refStandAvaApprovedDate("");
                    self.prodRegisTabsDetailsModel.refStandAvaComments("");
                    self.prodRegisTabsDetailsModel.generalDocuments("");
                    self.prodRegisTabsDetailsModel.kindOfDocuments("");
                    self.prodRegisTabsDetailsModel.geneDocsDos("");
                    self.prodRegisTabsDetailsModel.geneDocsDoReceivReqs("");
                    self.prodRegisTabsDetailsModel.geneDocsDoReqRequirToSup("");
                    self.prodRegisTabsDetailsModel.geneDocsFollowUpDate("");
                    self.prodRegisTabsDetailsModel.geneDocsApprovedDate("");
                    self.prodRegisTabsDetailsModel.geneDocsComments("");
                    self.prodRegisTabsDetailsModel.attachmentType("");
                    self.prodRegisTabsDetailsModel.vairationAttachmentType("");
                    self.prodRegisTabsDetailsModel.renewalAttachmentType("");
                    self.prodRegisTabsDetailsModel.productClassificationAttachmentType("");
                    self.prodRegisTabsDetailsModel.referenceStanderAttachmentType("");
                    self.prodRegisTabsDetailsModel.generalDocumentAttachmentType("");

                }
//                  self.typeOfActionChangedHandler = function (){
//            
//        };

                function isEmpty(val) {
                    return (val === undefined || val == null || val.length <= 0) ? true : false;
                }




                //***************************************************************************************************
                //Product Regis Tabs Details History Start
                self.prodRegisTabsDetailsHistoryModel = {
                    detailsIdHistory: ko.observable(),
                    productRegistrationIdHistory: ko.observable(),
                    newRegistrationHistory: ko.observable(),
                    newRegisDosHistory: ko.observable(),
                    newRegisDoReceivReqsHistory: ko.observable(),
                    newRegisDoReqRequirToSupHistory: ko.observable(),
                    newRegisFollowUpDateHistory: ko.observable(),
                    newRegisApprovedDateHistory: ko.observable(),
                    newRegisCommentsHistory: ko.observable(),
                    variationHistory: ko.observable(),
                    kindOfVariationHistory: ko.observable(),
                    variationDosHistory: ko.observable(),
                    variationDoReceivReqsHistory: ko.observable(),
                    variationDoReqRequirToSupHistory: ko.observable(),
                    variationFollowUpDateHistory: ko.observable(),
                    variationApprovedDateHistory: ko.observable(),
                    variationCommentsHistory: ko.observable(),
                    renewalHistory: ko.observable(),
                    kindOfRenewalHistory: ko.observable(),
                    renewalDosHistory: ko.observable(),
                    renewalDoReceivReqsHistory: ko.observable(),
                    renewalDoReqRequirToSupHistory: ko.observable(),
                    renewalFollowUpDateHistory: ko.observable(),
                    renewalApprovedDateHistory: ko.observable(),
                    renewalCommentsHistory: ko.observable(),
                    nextRenewalDateHistory: ko.observable(),
                    productClassificationHistory: ko.observable(),
                    restrictedHistory: ko.observable(),
                    importLicenceHistory: ko.observable(),
                    proClassDosHistory: ko.observable(),
                    proClassFollowUpDateHistory: ko.observable(),
                    proClassApprovedDateHistory: ko.observable(),
                    proClassCommentsHistory: ko.observable(),
                    refStandAvaHistory: ko.observable(),
                    kindOfRSTDHistory: ko.observable(),
                    refStandAvaDosHistory: ko.observable(),
                    refStandAvaFollowUpDateHistory: ko.observable(),
                    refStandAvaDoReceivReqsHistory: ko.observable(),
                    dateOfRequestHistory: ko.observable(),
                    refStandAvaApprovedDateHistory: ko.observable(),
                    refStandAvaCommentsHistory: ko.observable(),
                    generalDocumentsHistory: ko.observable(),
                    kindOfDocumentsHistory: ko.observable(),
                    geneDocsDosHistory: ko.observable(),
                    geneDocsDoReceivReqsHistory: ko.observable(),
                    geneDocsDoReqRequirToSupHistory: ko.observable(),
                    geneDocsFollowUpDateHistory: ko.observable(),
                    geneDocsApprovedDateHistory: ko.observable(),
                    geneDocsCommentsHistory: ko.observable(),
                    attachmentTypeHistory: ko.observable(),
                    vairationAttachmentTypeHistory: ko.observable(),
                    renewalAttachmentTypeHistory: ko.observable(),
                    productClassificationAttachmentTypeHistory: ko.observable(),
                    referenceStanderAttachmentTypeHistory: ko.observable(),
                    generalDocumentAttachmentTypeHistory: ko.observable()
                };
                //Product Regis Tabs Details History End
                //***************************************************************************************************
// Handle Tabs Details End

// Handle History Tabs Start
                self.visibleHistoryTab = ko.observable(false);
                self.selectedItemHistory = ko.observable("newRegistrationHistory");
// Handle History Tabs End

                //**********************************************************************************************


// Handle Generated Number Start

                //get MOH Site Registration 
                self.getMOHCurrRegisNumber = function () {
                    var index = 0;
                    var getMOHCurrRegisNumberCbFn = function (data) {


                        self.productRegisterModel.siteRegistrationNumber(data.items[index].mohregisnumber);

                        self.getMOHRegisNumber();

                    };
                    var getMOHCurrRegisNumberfailCbFn = function () {
                        console.log("Failed");
                    };
                    var UrlPath = "MOHRegisNumber/MOHCurrRegisNumber";
                    services.getGeneric(UrlPath).then(getMOHCurrRegisNumberCbFn, getMOHCurrRegisNumberfailCbFn);
                };

                //get Seq from database
                self.getMOHRegisNumber = function () {
                    var getMOHRegisNumberCbFn = function (data) {

                    };
                    var getMOHRegisNumberfailCbFn = function () {
                        console.log("Failed");
                    };

                    var UrlPath = "MOHRegisNumber/MOHNextRegisNumber";
                    services.getGeneric(UrlPath).then(getMOHRegisNumberCbFn, getMOHRegisNumberfailCbFn);
                };

                // Handle Generated Number End

                //Fun to handle Fetching Data in list Start 

                self.handleDataList = function () {

                    //$(".prListClass").find('option').get(0).remove();

                };
                //Fun to handle Fetching Data in list Start 


//Handle Getting BR Id Start

                self.getProductRegis = function () {

                    var getProductRegisCbFn = function (data) {
//self.retrieve = data;
                    };

                    var failCbFn = function (err) {
                        console.log(err);
                    };


                    var UrlPath = "ProductRegisRest/getProductRegis/" + self.retrieve.id;
                    services.getGeneric(UrlPath).then(getProductRegisCbFn, failCbFn);

                };
//Handle Getting BR Id Start

                self.itemCodeArr = ko.observableArray([]);

                self.getItemCode = function () {
                    var paraPayload = {
                        "reportName": "ITEM_CODE"
                    };
                    var getItemCodeCbFn = function (data) {

                        if (data.length != 0) {

                            data = JSON.parse(data);

                            $.each(data, function (index) {

                                self.itemCodeArr.push({
                                    value: data[index].ITEM_NUMBER,
                                    label: data[index].ITEM_NUMBER,
                                    desc: data[index].DESCRIPTION
                                });
                            });
                        }
                    };
                    var failCbFn = function () {
                    };
                    var UrlPath = "report/commonbireport";
                    services.addGeneric(UrlPath, paraPayload).then(getItemCodeCbFn, failCbFn);
                };

                self.itemCodeChangeHandler = function () {

                    for (var i = 0; i < self.itemCodeArr().length; i++) {
                        if (self.itemCodeArr()[i].value == self.productRegisterModel.itemCode()) {

                            self.productRegisterModel.productName(self.itemCodeArr()[i].desc);
                        }
                    }

                };




                self.supplierNameArr = ko.observableArray([]);

                self.getSupplerName = function () {

                    var paraPayload = {
                        "reportName": "SUPPLIER_NAME"
                    };
                    var getSupplerNameCbFn = function (data) {
                        // self.supplierNameArr([]);
                        if (data.length != 0) {

                            data = JSON.parse(data);

                            $.each(data, function (index) {

                                self.supplierNameArr.push({
                                    value: data[index].SUPPLIERID,
                                    label: data[index].SUPPLIERNAME,
                                    ADDRESSLINE1: data[index].ADDRESSLINE1,
                                    ADDRESSLINE2: data[index].ADDRESSLINE2,
                                    CITY: data[index].CITY,
                                    PHONE: data[index].PHONE
                                });
                            });
                        }

                    };
                    var failCbFn = function () {
                    };
                    var UrlPath = "report/commonbireport";
                    services.addGeneric(UrlPath, paraPayload).then(getSupplerNameCbFn, failCbFn);
                };

                self.supplierNameChangeHandler = function () {
                    if (self.retrieve.type == 'view') {
                        self.isDisabledContacts(true);
                        self.isDisabledSite(true);
                    } else {
                        self.isDisabledContacts(false);
                        self.isDisabledSite(false);
                    }
                    self.addVisi(true);

                    for (var i = 0; i < self.supplierNameArr().length; i++) {

                        if (self.supplierNameArr()[i].value == self.productRegisterModel.suppName()) {
                            self.productRegisterModel.telephone(self.supplierNameArr()[i].PHONE);
                            self.productRegisterModel.address(self.supplierNameArr()[i].ADDRESSLINE1);
                        }
                    }
                };



                self.checkListData = function () {

                    if (isEmpty(self.productRegisterModel.registerType()))
                    {
                        $('#registerTypeId')
                                .append($('<option>', {value: ''})
                                        .text(''));
                    }


                    if (isEmpty(self.productRegisterModel.itemCode()))
                    {
                        $('#itemCodeId')
                                .append($('<option>', {value: ''})
                                        .text(''));
                    }


                    if (isEmpty(self.productRegisterModel.suppName()))
                    {
                        $('#suppNameId')
                                .append($('<option>', {value: ''})
                                        .text(''));
                    }

                    if (isEmpty(self.productRegisterModel.productRegisterStatus()))
                    {
                        $('#proRegStId')
                                .append($('<option>', {value: ''})
                                        .text(''));
                    }


                    if (isEmpty(self.productRegisterModel.registerLocation()))
                    {
                        $('#regLoId')
                                .append($('<option>', {value: ''})
                                        .text(''));
                    }


                    if (isEmpty(self.productRegisterModel.countryOfOrigin()))
                    {
                        $('#couOfOrId')
                                .append($('<option>', {value: ''})
                                        .text(''));
                    }


                    if (isEmpty(self.productRegisterModel.storageCodition()))
                    {
                        $('#stoCoId')
                                .append($('<option>', {value: ''})
                                        .text(''));
                    }

                    if (isEmpty(self.productRegisterModel.currency()))
                    {
                        $('#currencyId')
                                .append($('<option>', {value: ''})
                                        .text(''));
                    }



                };

                self.connected = function () {


                    self.retrieve = oj.Router.rootInstance.retrieve();
                    var idsArr = ['registerTypeId', 'itemCodeId', 'suppNameId', 'proRegStId', 'regLoId', 'couOfOrId', 'stoCoId', 'currencyId'];

                    if (self.retrieve === 'add') {
                        resetAttachment();
                        $('.prListClass')
                                .append($('<option>', {value: ''})
                                        .text(''));
                        self.reset();
                        self.uploadBtnVisible(false);
                        self.addBtnVisible(true);
                        self.con1(false);
                        self.con2(false);
                        self.con3(false);
                        self.con4(false);
                        self.serviceType('ADD');
                        self.addBtnVisible(false);
                        self.cancleBtnVisible(true);
                        self.isDisabled(false);
                        self.isDisabledContacts(false);
                        self.visibleTab(true);
                        self.visibleHistoryTab(false);
                        self.getMOHCurrRegisNumber();

                        self.addVisi(true);



                    } else if (self.retrieve.type === 'update') {
                      resetAttachment();
                        self.serviceType('EDIT');
                        self.isDisabled(false);
                        self.isDisabledContacts(true);
                        self.addBtnVisible(false);
                        self.saveBtnVisible(true);
                        self.cancleBtnVisible(true);
                        self.visibleTab(true);
                        self.visibleHistoryTab(true);
                        self.handleDataList();
                        self.productRegisterModel = {
                            id: ko.observable(self.retrieve.id),
                            registerNumber: ko.observable(self.retrieve.register_number),
                            registerDate: ko.observable(self.retrieve.register_date),
                            registerType: ko.observable(self.retrieve.register_type),
                            productName: ko.observable(self.retrieve.product_name),
                            itemCode: ko.observable(self.retrieve.item_code),
                            suppName: ko.observable(self.retrieve.supplier_name),
                            telephone: ko.observable(self.retrieve.telephone),
                            telephoneOne: ko.observable(self.retrieve.telephone_one),
                            telephoneTwo: ko.observable(self.retrieve.telephone_two),
                            telephoneThree: ko.observable(self.retrieve.telephone_three),
                            telephoneFour: ko.observable(self.retrieve.telephone_four),
                            address: ko.observable(self.retrieve.address),
                            email: ko.observable(self.retrieve.email),
                            emailOne: ko.observable(self.retrieve.email_one),
                            emailTwo: ko.observable(self.retrieve.email_two),
                            emailThree: ko.observable(self.retrieve.email_three),
                            emailFour: ko.observable(self.retrieve.email_four),
                            title: ko.observable(self.retrieve.title),
                            titleOne: ko.observable(self.retrieve.title_one),
                            titleTwo: ko.observable(self.retrieve.title_two),
                            titleThree: ko.observable(self.retrieve.title_three),
                            titleFour: ko.observable(self.retrieve.title_four),
                            contact: ko.observable(self.retrieve.contact),
                            productRegisterStatus: ko.observable(self.retrieve.product_registration_status),
                            companyName: ko.observable(self.retrieve.company_name),
                            registerLocation: ko.observable(self.retrieve.register_location),
                            countryOfOrigin: ko.observable(self.retrieve.country_of_origin),
                            shelfLife: ko.observable(self.retrieve.shelf_life),
                            storageCodition: ko.observable(self.retrieve.storage_codition),
                            cifPrice: ko.observable(self.retrieve.cif_price),
                            wholeSalePrice: ko.observable(self.retrieve.whole_sale_price),
                            retailPrice: ko.observable(self.retrieve.retail_price),
                            exchangeRate: ko.observable(self.retrieve.exchange_rate),
                            currency: ko.observable(self.retrieve.currency),
                            primaryPackaging: ko.observable(self.retrieve.primary_packaging),
                            mahName: ko.observable(self.retrieve.mah_name),
                            manufacturingName: ko.observable(self.retrieve.manufacturing_name),
                            indication: ko.observable(self.retrieve.indication),
                            pharmaceuticalForm: ko.observable(self.retrieve.pharamaceutical_form),
                            activeIngredientName: ko.observable(self.retrieve.active_ingredient_name),
                            relatSubstances: ko.observable(self.retrieve.relative_substances),
                            siteRegistrationName: ko.observable(self.retrieve.site_registration_name),
                            siteRegistrationValidity: ko.observable(self.retrieve.site_registration_validity),
                            siteRegistrationNumber: ko.observable(self.retrieve.site_registration_number),
                            lastRenewal: ko.observable(self.retrieve.last_renewal),
                            nextRenewal: ko.observable(self.retrieve.next_renewal),
                            secondaryPackage_releaseSite: ko.observable(self.retrieve.secondary_package_release_site),
                            contactOne: ko.observable(self.retrieve.contact_one),
                            contactTwo: ko.observable(self.retrieve.contact_two),
                            contactThree: ko.observable(self.retrieve.contact_three),
                            contactFour: ko.observable(self.retrieve.contact_four),
                            siteOne: ko.observable(self.retrieve.site_one),
                            siteTwo: ko.observable(self.retrieve.site_two),
                            notes: ko.observable(self.retrieve.notes)
                        };

                        var supp = self.productRegisterModel.suppName();

                        var integer = parseInt(supp);
                        self.productRegisterModel.suppName(integer);


                        if (self.productRegisterModel.contactOne())
                        {
                            self.con1(true);
                        } else
                        {
                            self.con1(false);
                        }
                        if (self.productRegisterModel.contactTwo())
                        {
                            self.con2(true);
                        } else
                        {
                            self.con2(false);
                        }

                        if (self.productRegisterModel.contactThree())
                        {
                            self.con3(true);
                        } else
                        {
                            self.con3(false);
                        }

                        if (self.productRegisterModel.contactFour())
                        {
                            self.con4(true);
                        } else
                        {
                            self.con4(false);
                        }

                        if (self.productRegisterModel.siteOne())
                        {
                            self.site1(true);
                        } else
                        {
                            self.site1(false);
                        }
                        if (self.productRegisterModel.siteTwo())
                        {
                            self.site2(true);
                        } else
                        {
                            self.site2(false);
                        }


                        if (self.productRegisterModel.contactOne() && self.productRegisterModel.contactTwo() && self.productRegisterModel.contactThree() && self.productRegisterModel.contactFour()) {

                            self.addVisi(false);
                            self.removeVisi(true);
                        } else {
                            self.addVisi(true);
                            self.removeVisi(false);
                        }

                        if (self.productRegisterModel.siteOne() && self.productRegisterModel.siteTwo()) {

                            self.addVisib(false);
                            self.removeVisib(true);
                        } else {
                            self.addVisib(true);
                            self.removeVisib(false);
                        }

                        self.getProdRegisTabsDetails();
                        //self.getProdRegisTabsDetailsHistory();
                        self.isDisabledContacts(false);
                        self.isDisabledSite(false);



                        self.checkListData();




                    } else if (self.retrieve.type === 'view') {
                        resetAttachment();
                        self.uploadBtnVisible(true);
                        self.saveBtnVisible(false);
                        self.serviceType('VIEW');
                        self.addBtnVisible(false);
                        self.cancleBtnVisible(true);
                        self.isDisabled(true);
                        self.isDisabledContacts(true);
//                        self.viewAttachmentBtnVisible2(false);
//                        self.viewAttachmentBtnVisible(false);
//                        self.viewAttachmentBtnVisible3(false);
//                        self.viewAttachmentBtnVisible4(false);
//                        self.viewAttachmentBtnVisible5(false);
//                        self.viewAttachmentBtnVisible6(false);
                        self.visibleHistoryTab(true);
                        self.handleDataList();
                        self.productRegisterModel = {
                            id: ko.observable(self.retrieve.id),
                            registerNumber: ko.observable(self.retrieve.register_number),
                            registerDate: ko.observable(self.retrieve.register_date),
                            registerType: ko.observable(self.retrieve.register_type),
                            productName: ko.observable(self.retrieve.product_name),
                            itemCode: ko.observable(self.retrieve.item_code),
                            suppName: ko.observable(self.retrieve.supplier_name),
                            telephone: ko.observable(self.retrieve.telephone),
                            telephoneOne: ko.observable(self.retrieve.telephone_one),
                            telephoneTwo: ko.observable(self.retrieve.telephone_two),
                            telephoneThree: ko.observable(self.retrieve.telephone_three),
                            telephoneFour: ko.observable(self.retrieve.telephone_four),
                            address: ko.observable(self.retrieve.address),
                            email: ko.observable(self.retrieve.email),
                            emailOne: ko.observable(self.retrieve.email_one),
                            emailTwo: ko.observable(self.retrieve.email_two),
                            emailThree: ko.observable(self.retrieve.email_three),
                            emailFour: ko.observable(self.retrieve.email_four),
                            title: ko.observable(self.retrieve.title),
                            titleOne: ko.observable(self.retrieve.title_one),
                            titleTwo: ko.observable(self.retrieve.title_two),
                            titleThree: ko.observable(self.retrieve.title_three),
                            titleFour: ko.observable(self.retrieve.title_four),
                            contact: ko.observable(self.retrieve.contact),
                            productRegisterStatus: ko.observable(self.retrieve.product_registration_status),
                            companyName: ko.observable(self.retrieve.company_name),
                            registerLocation: ko.observable(self.retrieve.register_location),
                            countryOfOrigin: ko.observable(self.retrieve.country_of_origin),
                            shelfLife: ko.observable(self.retrieve.shelf_life),
                            storageCodition: ko.observable(self.retrieve.storage_codition),
                            cifPrice: ko.observable(self.retrieve.cif_price),
                            wholeSalePrice: ko.observable(self.retrieve.whole_sale_price),
                            retailPrice: ko.observable(self.retrieve.retail_price),
                            exchangeRate: ko.observable(self.retrieve.exchange_rate),
                            currency: ko.observable(self.retrieve.currency),
                            primaryPackaging: ko.observable(self.retrieve.primary_packaging),
                            mahName: ko.observable(self.retrieve.mah_name),
                            manufacturingName: ko.observable(self.retrieve.manufacturing_name),
                            indication: ko.observable(self.retrieve.indication),
                            pharmaceuticalForm: ko.observable(self.retrieve.pharamaceutical_form),
                            activeIngredientName: ko.observable(self.retrieve.active_ingredient_name),
                            relatSubstances: ko.observable(self.retrieve.relative_substances),
                            siteRegistrationName: ko.observable(self.retrieve.site_registration_name),
                            siteRegistrationValidity: ko.observable(self.retrieve.site_registration_validity),
                            siteRegistrationNumber: ko.observable(self.retrieve.site_registration_number),
                            lastRenewal: ko.observable(self.retrieve.last_renewal),
                            nextRenewal: ko.observable(self.retrieve.next_renewal),
                            secondaryPackage_releaseSite: ko.observable(self.retrieve.secondary_package_release_site),
                            contactOne: ko.observable(self.retrieve.contact_one),
                            contactTwo: ko.observable(self.retrieve.contact_two),
                            contactThree: ko.observable(self.retrieve.contact_three),
                            contactFour: ko.observable(self.retrieve.contact_four),
                            siteOne: ko.observable(self.retrieve.site_one),
                            siteTwo: ko.observable(self.retrieve.site_two),
                            notes: ko.observable(self.retrieve.notes)
                        };

                        var supp = self.productRegisterModel.suppName();
                        var integer = parseInt(supp);
                        self.productRegisterModel.suppName(integer);

                        if (self.productRegisterModel.contactOne())
                        {
                            self.con1(true);
                        } else
                        {
                            self.con1(false);
                        }
                        if (self.productRegisterModel.contactTwo())
                        {
                            self.con2(true);
                        } else
                        {
                            self.con2(false);
                        }

                        if (self.productRegisterModel.contactThree())
                        {
                            self.con3(true);
                        } else
                        {
                            self.con3(false);
                        }

                        if (self.productRegisterModel.contactFour())
                        {
                            self.con4(true);
                        } else
                        {
                            self.con4(false);
                        }

                        if (self.productRegisterModel.siteOne())
                        {
                            self.site1(true);
                        } else
                        {
                            self.site1(false);
                        }
                        if (self.productRegisterModel.siteTwo())
                        {
                            self.site2(true);
                        } else
                        {
                            self.site2(false);
                        }

                        self.addVisi(false);
                        self.addVisib(false);
                        self.getProdRegisTabsDetails();
                    }

                    self.getItemCode();
                    self.getSupplerName();
                };
                self.disconnected = function () {
                    self.reset();
                    self.prevBtnVisible(false);
                    self.saveBtnVisible(true);
                    self.isDisabledHandleSubmit(false);
                    self.productRegisDataArr([]);
                    //self.supplierNameArr([]);


                };
                self.transitionCompleted = function () {
                    // Implement if needed
                };

                function getAttachmentById(attachId) {
                    var getAttachmentCbFn = function (data) {
                        self.attachmentData(data.items);
                        for (var i = 0; i < data.items.length; i++) {
                            ///////////////////////////////////////
                            if (data.items[i].prod_regis_details_id == productDetailsId) {
                                if (data.items[i].tab_name === 'newRegistration') {
//                                    if (data.items[i].list_type === 'registration') {
                                    if (!isEmpty(dataFiles)) {
                                        self.koArray.push(getAttachedReturn(data.items[i]));
                                        self.prodRegisTabsDetailsModel.attachmentType(data.items[i].list_type);
                                    }
//                                    } else if (data.items[i].list_type === 'renwal') {
//                                        if (!isEmpty(dataFiles))
//                                            self.renwalArray.push(getAttachedReturn(data.items[i]));
//                                    } else if (data.items[i].list_type === 'site') {
//                                        if (!isEmpty(dataFiles))
//                                            self.siteArray.push(getAttachedReturn(data.items[i]));
//                                    }
                                }
                                //////////////////////////////////////////////////
                                else if (data.items[i].tab_name === 'vairation') {
//                                    if (data.items[i].list_type === 'registration') {
                                    if (!isEmpty(dataFiles)) {
                                        self.variationKoArray.push(getAttachedReturn(data.items[i]));
                                        self.prodRegisTabsDetailsModel.vairationAttachmentType(data.items[i].list_type);
                                    }
//                                    } else if (data.items[i].list_type === 'renwal') {
//                                        if (!isEmpty(dataFiles))
//                                            self.vairationRenwalArray.push(getAttachedReturn(data.items[i]));
//                                    } else if (data.items[i].list_type === 'site') {
//                                        if (!isEmpty(dataFiles))
//                                            self.vairationSiteArray.push(getAttachedReturn(data.items[i]));
//                                    }
                                }
                                //////////////////////////////////////////////
                                else if (data.items[i].tab_name === 'renewal') {
//                                    if (data.items[i].list_type === 'registration') {
                                    if (!isEmpty(dataFiles)) {
                                        self.renewalKoArray.push(getAttachedReturn(data.items[i]));
                                        self.prodRegisTabsDetailsModel.renewalAttachmentType(data.items[i].list_type);
                                    }
//                                    } else if (data.items[i].list_type === 'renwal') {
//                                        if (!isEmpty(dataFiles))
//                                            self.renewalRenwalArray.push(getAttachedReturn(data.items[i]));
//                                    } else if (data.items[i].list_type === 'site') {
//                                        if (!isEmpty(dataFiles))
//                                            self.renewalSiteArray.push(getAttachedReturn(data.items[i]));
//                                    }
                                }
                                ///////////////////////////////////////////
                                else if (data.items[i].tab_name === 'productClassification') {
//                                    if (data.items[i].list_type === 'registration') {
                                    if (!isEmpty(dataFiles)) {
                                        self.productClassificationKoArray.push(getAttachedReturn(data.items[i]));
                                        self.prodRegisTabsDetailsModel.productClassificationAttachmentType(data.items[i].list_type);
                                    }
//                                    } else if (data.items[i].list_type === 'renwal') {
//                                        if (!isEmpty(dataFiles))
//                                            self.productClassificationRenwalArray.push(getAttachedReturn(data.items[i]));
//                                    } else if (data.items[i].list_type === 'site') {
//                                        if (!isEmpty(dataFiles))
//                                            self.productClassificationSiteArray.push(getAttachedReturn(data.items[i]));
//                                    }
                                }
                                ////////////////////////////////////////////
                                else if (data.items[i].tab_name === 'referenceStandardAvailability') {
//                                    if (data.items[i].list_type === 'registration') {
                                    if (!isEmpty(dataFiles)) {
                                        self.refereceStandardKoArray.push(getAttachedReturn(data.items[i]));
                                        self.prodRegisTabsDetailsModel.referenceStanderAttachmentType(data.items[i].list_type);
                                    }
//                                    } else if (data.items[i].list_type === 'renwal') {
//                                        if (!isEmpty(dataFiles))
//                                            self.referenceStandardAvailabilityRenwalArray.push(getAttachedReturn(data.items[i]));
//                                    } else if (data.items[i].list_type === 'site') {
//                                        if (!isEmpty(dataFiles))
//                                            self.referenceStandardAvailabilitySiteArray.push(getAttachedReturn(data.items[i]));
//                                    }
                                }
                                ////////////////////////////////////////////
                                else if (data.items[i].tab_name === 'generalDocuments') {
//                                    if (data.items[i].list_type === 'registration') {
                                    if (!isEmpty(dataFiles)) {
                                        self.generalDocumentKoArray.push(getAttachedReturn(data.items[i]));
                                        self.prodRegisTabsDetailsModel.generalDocumentAttachmentType(data.items[i].list_type);
                                    }
//                                    } else if (data.items[i].list_type === 'renwal') {
//                                        if (!isEmpty(dataFiles))
//                                            self.generalDocumentsRenwalArray.push(getAttachedReturn(data.items[i]));
//                                    } else if (data.items[i].list_type === 'site') {
//                                        if (!isEmpty(dataFiles))
//                                            self.generalDocumentsSiteArray.push(getAttachedReturn(data.items[i]));
//                                    }
                                }
                            }
                        }
//                        self.prodRegisTabsDetailsModel.attachmentType("registeration");


                    };

                    var failCbFn = function (err) {
                        console.log(err);
                    };
                    var service = "productAttachments/getAttachment/" + attachId;
                    services.getGeneric(service).then(getAttachmentCbFn, failCbFn);
                }
                ;
                function deleteAttachmentById(attachId) {

                    var deleteAttachmentCbFn = function (data) {

                    };

                    var failCbFn = function (err) {
                        console.log(err);
                    };

                    var service = "productAttachments/deleteAttachment/" + attachId;
                    services.deleteGeneric(service).then(deleteAttachmentCbFn, failCbFn);
                }
                ;
                function fillAttachedArray(data, attachmentId, listType, masterId) {
                    //    console.log(listType);
                    for (var i = 0; i < data.length; i++)
                    {
                        self.allAttachedData.push({
                            img_id: data[i].id,
                            image_data: data[i].data,
                            image_name: data[i].name,
                            pr_id: attachmentId,
                            list_type: listType,
                            tab_name: data[i].tab_name,
                            prm_id: masterId

                        });
                    }
                }
                ;
                function getAttachedReturn(data) {
                    self.attachmentId = self.attachmentId - 1;
                    var dataFiles = {};
                    dataFiles.name = data.image_name;
                    dataFiles.data = data.image_base_64;
                    dataFiles.id = self.attachmentId;
                    dataFiles.tab_name = data.tab_name;
                    return dataFiles;
                }
                ;
                function viewAttachment(data, value) {

                    var selected = data.find(e => {
                        return e.id == value
                    });
                    var link = document.createElement("a");
                    document.body.appendChild(link);
                    link.href = selected.data;
                    link.download = selected.name;
                    link.click();
                    document.body.removeChild(link);

                    $.each(data, function (indexInner, valueInner)
                    {

                    });
                }
                ;
                function fillArrayAttachedArrayData(files, selectedTab)
                {
                    if (files.length > 0) {
                        //add the new files at the beginning of the list
                        var dataFiles = {};
                        for (var i = 0; i < files.length; i++) {

                            function getBase64(file) {
                                return new Promise((resolve, reject) => {
                                    const reader = new FileReader();
                                    reader.readAsDataURL(file);
                                    reader.onload = () => resolve(reader.result);
                                    reader.onerror = error => reject(error);
                                });
                            }
                            dataFiles.name = files[i].name;
                            getBase64(files[i]).then(function (data) {
                                dataFiles.data = (data);
                                self.attachmentId = self.attachmentId - 1;
                                dataFiles.id = (self.attachmentId);
                                dataFiles.tab_name = selectedTab;

                                if (selectedTab === 'newRegistration') {
//                                    if (self.selectedAttachmentType() === 'registeration') {
                                    self.koArray.push(dataFiles);
//                                        self.koArray(self.registrationArray());
//                                    } else if (self.selectedAttachmentType() === 'renewal') {
//                                        self.renwalArray.push(dataFiles);
//                                        self.koArray(self.renwalArray());
//                                    } else if (self.selectedAttachmentType() === 'site') {
//                                        self.siteArray.push(dataFiles);
//                                        self.koArray(self.siteArray());
//                                    }

                                } else if (selectedTab === 'vairation') {
//                                    if (self.vairationSelectedAttachmentType() === 'registeration') {
                                    self.variationKoArray.push(dataFiles);
//                                        self.variationKoArray(self.vairationRegistrationArray());
//                                    } else if (self.vairationSelectedAttachmentType() === 'renewal') {
//                                        self.vairationRenwalArray.push(dataFiles);
//                                        self.variationKoArray(self.vairationRenwalArray());
//                                    } else if (self.vairationSelectedAttachmentType() === 'site') {
//                                        self.vairationSiteArray.push(dataFiles);
//                                        self.variationKoArray(self.vairationSiteArray());
//                                    }
                                } else if (selectedTab === 'renewal') {
//                                    if (self.renewalSelectedAttachmentType() === 'registeration') {
                                    self.renewalKoArray.push(dataFiles);
//                                        self.renewalKoArray(self.renewalRegistrationArray());
//                                    } else if (self.renewalSelectedAttachmentType() === 'renewal') {
//                                        self.renewalRenwalArray.push(dataFiles);
//                                        self.renewalKoArray(self.renewalRenwalArray());
//                                    } else if (self.renewalSelectedAttachmentType() === 'site') {
//                                        self.renewalSiteArray.push(dataFiles);
//                                        self.renewalKoArray(self.renewalSiteArray());
//                                    }
                                } else if (selectedTab === 'productClassification') {
//                                    if (self.productSelectedAttachmentType() === 'registeration') {
                                    self.productClassificationKoArray.push(dataFiles);
//                                        self.productClassificationKoArray(self.productClassificationRegistrationArray());
//                                    } else if (self.productSelectedAttachmentType() === 'renewal') {
//                                        self.productClassificationRenwalArray.push(dataFiles);
//                                        self.productClassificationKoArray(self.productClassificationRenwalArray());
//                                    } else if (self.productSelectedAttachmentType() === 'site') {
//                                        self.productClassificationSiteArray.push(dataFiles);
//                                        self.productClassificationKoArray(self.productClassificationSiteArray());
//                                    }
                                } else if (selectedTab === 'referenceStandardAvailability') {
//                                    if (self.referenceStanderSelectedAttachmentType() === 'registeration') {
                                    self.refereceStandardKoArray.push(dataFiles);
//                                        self.refereceStandardKoArray(self.referenceStandardAvailabilityRegistrationArray());
//                                    } else if (self.referenceStanderSelectedAttachmentType() === 'renewal') {
//                                        self.referenceStandardAvailabilityRenwalArray.push(dataFiles);
//                                        self.refereceStandardKoArray(self.referenceStandardAvailabilityRenwalArray());
//                                    } else if (self.referenceStanderSelectedAttachmentType() === 'site') {
//                                        self.referenceStandardAvailabilitySiteArray.push(dataFiles);
//                                        self.refereceStandardKoArray(self.referenceStandardAvailabilitySiteArray());
//                                    }

                                } else if (selectedTab === 'generalDocuments') {
//                                    if (self.generalDocumentSelectedAttachmentType() === 'registeration') {
                                    self.generalDocumentKoArray.push(dataFiles);
//                                        self.generalDocumentKoArray(self.generalDocumentsRegistrationArray());
//                                    } else if (self.generalDocumentSelectedAttachmentType() === 'renewal') {
//                                        self.generalDocumentsRenwalArray.push(dataFiles);
//                                        self.generalDocumentKoArray(self.generalDocumentsRenwalArray());
//                                    } else if (self.generalDocumentSelectedAttachmentType() === 'site') {
//                                        self.generalDocumentsSiteArray.push(dataFiles);
//                                        self.generalDocumentKoArray(self.generalDocumentsSiteArray());
//                                    }
                                }
                            }
                            );
                        }

                    }
                }
                ;
                function removeSelectedAttachments(selctedAttachment, removeArray, selectedAttachmentType, selectedTab) {
                    if (typeof selctedAttachment[0] !== 'undefined')
                        selctedAttachment[0] < 0 ? '' : removeArray.push({'id': selctedAttachment[0]});

                    $.each(selctedAttachment, function (index, value)
                    {

//                        if (selectedAttachmentType === 'registeration') {
                        if (selectedTab === 'newRegistration') {
                            self.koArray.remove(function (item) {
                                return (item.id == value);
                            });
//                                self.koArray(self.registrationArray());
                        } else if (selectedTab === 'vairation') {
                            self.variationKoArray.remove(function (item) {
                                return (item.id == value);
                            });
//                                self.variationKoArray(self.vairationRegistrationArray());
                        } else if (selectedTab === 'renewal') {
                            self.renewalKoArray.remove(function (item) {
                                return (item.id == value);
                            });
//                                self.renewalKoArray(self.renewalRegistrationArray());
                        } else if (selectedTab === 'productClassification') {
                            self.productClassificationKoArray.remove(function (item) {
                                return (item.id == value);
                            });
//                                self.productClassificationKoArray(self.productClassificationRegistrationArray());
                        } else if (selectedTab === 'referenceStandardAvailability') {
                            self.refereceStandardKoArray.remove(function (item) {
                                return (item.id == value);
                            });
//                                self.refereceStandardKoArray(self.referenceStandardAvailabilityRegistrationArray());
                        } else if (selectedTab === 'generalDocuments') {
                            self.generalDocumentKoArray.remove(function (item) {
                                return (item.id == value);
                            });
//                                self.generalDocumentKoArray(self.generalDocumentsRegistrationArray());
                        }
//                        }
                        /*  else if (selectedAttachmentType === 'renewal') {
                         if (selectedTab === 'newRegistration') {
                         self.renwalArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.koArray(self.renwalArray());
                         } else if (selectedTab === 'vairation') {
                         self.vairationRenwalArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.variationKoArray(self.vairationRenwalArray());
                         } else if (selectedTab === 'renewal') {
                         self.renewalRenwalArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.renewalKoArray(self.renewalRenwalArray());
                         } else if (selectedTab === 'productClassification') {
                         self.productClassificationRenwalArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.productClassificationKoArray(self.productClassificationRenwalArray());
                         } else if (selectedTab === 'referenceStandardAvailability') {
                         self.referenceStandardAvailabilityRenwalArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.refereceStandardKoArray(self.referenceStandardAvailabilityRenwalArray());
                         } else if (selectedTab === 'generalDocuments') {
                         self.generalDocumentsRenwalArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.generalDocumentKoArray(self.generalDocumentsRenwalArray());
                         }
                         } else if (selectedAttachmentType === 'site') {
                         if (selectedTab === 'newRegistration') {
                         self.siteArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.koArray(self.siteArray());
                         } else if (selectedTab === 'vairation') {
                         self.vairationSiteArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.variationKoArray(self.vairationSiteArray());
                         } else if (selectedTab === 'renewal') {
                         self.renewalSiteArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.renewalKoArray(self.renewalSiteArray());
                         } else if (selectedTab === 'productClassification') {
                         self.productClassificationSiteArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.productClassificationKoArray(self.productClassificationSiteArray());
                         } else if (selectedTab === 'referenceStandardAvailability') {
                         self.referenceStandardAvailabilitySiteArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.refereceStandardKoArray(self.referenceStandardAvailabilitySiteArray());
                         } else if (selectedTab === 'generalDocuments') {
                         self.generalDocumentsSiteArray.remove(function (item) {
                         return (item.id == value);
                         });
                         self.generalDocumentKoArray(self.generalDocumentsSiteArray());
                         }
                         }*/
                    });
                }
                ;
                function resetAttachment() {
                    ///////////////////////////////////////
                    self.koArrayRemoved([]);
                    self.koArray([]);
                    self.variationKoArray([]);
                    self.renewalKoArray([]);
                    self.productClassificationKoArray([]);
                    self.refereceStandardKoArray([]);
                    self.generalDocumentKoArray([]);
                    ////////////////////////////////
                    self.registrationArray([]);
                    self.renwalArray([]);
                    self.siteArray([]);
                    //////////
                    self.vairationRegistrationArray([]);
                    self.vairationRenwalArray([]);
                    self.vairationSiteArray([]);
                    /////////////
                    self.renewalRegistrationArray([]);
                    self.renewalRenwalArray([]);
                    self.renewalSiteArray([]);
                    ////////////////////////
                    self.productClassificationRegistrationArray([]);
                    self.productClassificationRenwalArray([]);
                    self.productClassificationSiteArray([]);
                    //////////////////////////
                    self.referenceStandardAvailabilityRegistrationArray([]);
                    self.referenceStandardAvailabilityRenwalArray([]);
                    self.referenceStandardAvailabilitySiteArray([]);
                    //////////////////////
                    self.generalDocumentsRegistrationArray([]);
                    self.generalDocumentsRenwalArray([]);
                    self.generalDocumentsSiteArray([]);
                }
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new guaranteeScreenModel();
        }
);