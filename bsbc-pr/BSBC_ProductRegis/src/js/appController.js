/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'config/services', 'notify', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
    'ojs/ojoffcanvas', 'ojs/ojbutton', 'jquery', 'ojs/ojarraytabledatasource', 'ojs/ojInputText', 'ojs/ojselectcombobox', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojlabel'],
        function (oj, ko, moduleUtils, services) {
            function ControllerViewModel() {
                var self = this;
                self.username = ko.observable();
                self.password = ko.observable();
                self.userLogin = ko.observable();

                self.empGrade = ko.observable();
                self.empDepartment = ko.observable();
                self.empNumber = ko.observable();
                self.empJob = ko.observable();
                self.empName = ko.observable();
                self.jwt = ko.observable();

                // Search Function for lables 
                self.searchArray = function (nameKey, searchArray) {
                    for (var i = 0; i < searchArray.length; i++) {
                        if (searchArray[i].value == nameKey) {
                            return searchArray[i].desc;
                        }
                    }
                };
                if (sessionStorage.empName != "null")
                {
                    self.empName(sessionStorage.empName);
                } else
                {
                    self.empName('');
                }
                if (sessionStorage.empJob != "null")
                {
                    self.empJob(sessionStorage.empJob);
                } else
                {
                    self.empJob('');
                }
                if (sessionStorage.empNumber != "null")
                {
                    self.empNumber(sessionStorage.empNumber);
                } else
                {
                    self.empNumber('');

                }
                if (sessionStorage.empDepartment != "null")
                {
                    self.empDepartment(sessionStorage.empDepartment);
                } else
                {
                    self.empDepartment('');
                }
                if (sessionStorage.empGrade != "null")
                {
                    self.empGrade(sessionStorage.empGrade);
                } else
                {
                    self.empGrade('');
                }



                // self.globalSupplierNameArr = ko.observableArray();

                //self.getSupplerName();

                self.isUserLoggedIn = ko.observable(false);

                console.log(self.isUserLoggedIn());
                console.log(sessionStorage);


                var url = new URL(window.location.href);
                var jwt;

                if (url.searchParams.get("jwt")) {
                    jwt = url.searchParams.get("jwt");
                    self.jwt(jwt);


                    jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
                    var username = jwtJSON.sub;
                    self.username(username.split('.')[0] + username.split('.')[1])
                    console.log(self.username());

                    self.userLogin(sessionStorage.Username);

                    if (self.isUserLoggedIn() === 'false') {
                        self.isUserLoggedIn(false);
                    } else {
                        self.isUserLoggedIn(true);
                    }

                } else if (sessionStorage.isUserLoggedIn) {
                    self.userLogin(sessionStorage.Username);


                    if (self.isUserLoggedIn() === 'false') {
                        self.isUserLoggedIn(false);
                    } else {
                        self.isUserLoggedIn(true);
                    }
                    console.log(self.isUserLoggedIn());

                }


                $(function () {
                    $('#username').on('keydown', function (ev) {
                        var mobEvent = ev;
                        var mobPressed = mobEvent.keyCode || mobEvent.which;
                        if (mobPressed == 13) {
                            $('#username').blur();
                            $('#password').focus();
                        }
                        return true;
                    });
                });

                //

                //close notify in when focus on login input
                $("#userValidation,#passValidation").focusout(function () {
                    $(".notifyjs-bootstrap-base").notify().hide();
                });
                //login button call function
                self.validateUser = function () {
                    var username = self.username();
                    var password = self.password();
                    // Here we check null value and show error to fill
                    if (!username && !password) {
                        $("#userValidation").notify(
                                "Please enter user name",
                                {position: "top right", autoHide: false, className: 'errorInput'}
                        );
                        $("#passValidation").notify(
                                "Please enter password",
                                {position: "top right", autoHide: false, className: 'errorInput'}
                        );
                    } else if (!username) {
                        $("#userValidation").notify(
                                "Please enter user name",
                                {position: "top right", autoHide: false, className: 'errorInput'}
                        );
                    } else if (!password) {
                        $("#passValidation").notify(
                                "Please enter user name",
                                {position: "top right", autoHide: false, className: 'errorInput'}
                        );
                    } else {
                        $('#loader-overlay').show();
                        console.log('validateUser');
                        console.log(username);
//                        req.validateUser(username, password, function (response) {
                        var loginCbFn = function (data) {
                            self.getEmployeeDetails()
                            var parsedData = jQuery.parseJSON(data.replace('\'', '"').replace('\'', '"'));


                            if (parsedData.result == true) {

                                console.log("Successfull authentication");


                                self.isUserLoggedIn(true);
                                sessionStorage.setItem('username', self.isUserLoggedIn());

//                                 oj.Router.rootInstance.go('searchProformaInvoice');
                                var userNamelabel = self.username();

                                sessionStorage.setItem('isUserLoggedIn', self.isUserLoggedIn());
                                sessionStorage.setItem('Username', userNamelabel);

                                oj.Router.rootInstance.go('searchProductRegistration');
                                $('#loader-overlay').hide();

                            } else {
                                // If authentication failed
                                // alert(response.Error);
                                $.notify(
                                        "Wrong user name or passowrd", "error")
                                $('#loader-overlay').hide();


                            }
                        }
                        var loginFailCbFn = function () {
                            $.notify(
                                    "Can't validate UserName")
                            $('#loader-overlay').hide();
                        }

                        var payload = {
                            "userName": username,
                            "password": password
                        };
                        services.authenticate(payload).then(loginCbFn, loginFailCbFn);
                        //oj.Router.rootInstance.go('home');
                    }
                    ;
                };



                self.getEmployeeDetails = function ()
                {
                    var getEmployeeDetailsCbFn = function (data)
                    {

                        sessionStorage.setItem('empName', data.displayName);
                        sessionStorage.setItem('empJob', data.jobName);
                        sessionStorage.setItem('empNumber', data.personNumber);
                        sessionStorage.setItem('empDepartment', data.departmentName);
                        sessionStorage.setItem('empGrade', data.grade);
                        console.log(sessionStorage);

                        if (sessionStorage.empName != "null")
                        {
                            self.empName(sessionStorage.empName);
                        } else
                        {
                            self.empName('');
                        }
                        if (sessionStorage.empJob != "null")
                        {
                            self.empJob(sessionStorage.empJob);
                        } else
                        {
                            self.empJob('');
                        }
                        if (sessionStorage.empNumber != "null")
                        {
                            self.empNumber(sessionStorage.empNumber);
                        } else
                        {
                            self.empNumber('');

                        }
                        if (sessionStorage.empDepartment != "null")
                        {
                            self.empDepartment(sessionStorage.empDepartment);
                        } else
                        {
                            self.empDepartment('');
                        }
                        if (sessionStorage.empGrade != "null")
                        {
                            self.empGrade(sessionStorage.empGrade);
                        } else
                        {
                            self.empGrade('');
                        }
                    };
                    var getEmployeeDetailsfailCbFn = function () {
                    };
                    var UrlPath = "getemployee/" + self.username();

                    services.getGeneric(UrlPath).then(getEmployeeDetailsCbFn, getEmployeeDetailsfailCbFn);
                };



                self.HomeAction = function (event) {
                    document.location.replace("https://bsbcjcssx-a527636.java.us2.oraclecloudapps.com/BSBC_ProductRegis/");
                };
                self.menuItemAction = function (event) {
                    //  if (event.target.value === 'out') {

                    self.isUserLoggedIn(false);
                    sessionStorage.setItem('username', '');
                    sessionStorage.setItem('isUserLoggedIn', '');
//                        location.reload();
//                        console.log(self.isUserLoggedIn());
//                        window.history.replaceState({}, document.title, "/" );
                    document.location.replace("https://bsbcjcssx-a527636.java.us2.oraclecloudapps.com/BSBC_ProductRegis/");
//                        window.history.replaceState(null, null, window.location.pathname);
                    console.log();


                    if (self.language() === 'english')
                    {
                        $('html').attr({'dir': 'ltr'});
                    } else if (self.language() === 'arabic')
                    {
                        $('html').attr({'dir': 'rtl'});
                    }

//                    }else{
//                          document.location.replace("https://bsbcjcssx-a527636.java.us2.oraclecloudapps.com/BSBC_ProInv/")
//                    }
                };

                self.loginErrorPopup = function () {
                    var popup = document.querySelector('#popup1');
                    popup.close('#btnGo');
                };

                startAnimationListener = function (data, event) {
                    var ui = event.detail;
                    if (!$(event.target).is("#popup1"))
                        return;

                    if ("open" === ui.action)
                    {
                        event.preventDefault();
                        var options = {"direction": "top"};
                        oj.AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
                    } else if ("close" === ui.action)
                    {
                        event.preventDefault();
                        ui.endCallback();
                    }
                };

                // Media queries for repsonsive layouts
                var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
                self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);

                // Router setup
                self.router = oj.Router.rootInstance;
                self.router.configure({

                    'productRegistration': {label: 'ProductRegistration', value: "create/productRegistration", title: "Product Registration"},
                    'searchProductRegistration': {label: 'SearchProductRegistration', value: "search/searchProductRegistration", title: "Search", isDefault: true}

                });
                oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();



//local text
                self.localeLogin = ko.observable();
                self.UsernameLabel = ko.observable();
                self.passwordLabel = ko.observable();
                self.loginword = ko.observable();
                self.forgotPasswordLabel = ko.observable();

                self.localeName = ko.observable();
                self.localeJop = ko.observable();
                self.localeempNO = ko.observable();
                self.localeDep = ko.observable();
                self.localeGrade = ko.observable();


                self.language = ko.observable();
                self.languagelable = ko.observable("English");
                var storage = window.localStorage;
                var isTrue, preferedLanguage;

                self.langSelected = function (event) {
                    var valueObj = buildValueChange(event['detail']);
                    console.log(JSON.stringify(valueObj));
                    self.setPreferedLanguage(valueObj.value);
                };

                function buildValueChange(valueParam) {
                    var valueObj = {};
                    //valueObj.previousValue = valueParam.previousValue;
                    if (valueParam.previousValue) {
                        valueObj.value = valueParam.value;
                    } else {
                        valueObj.value = valueParam.previousValue;
                    }
                    //console.log('Previous: ' + valueParam.previousValue);
                    //console.log('Current: ' + valueParam.value);
                    return valueObj;
                }
                ;

                $(function () {
                    storage.setItem('setLang', 'english');
//                    storage.setItem('setLang', 'arabic');
                    preferedLanguage = storage.getItem('setLang');
                    self.language(preferedLanguage);
                    changeToArabic(preferedLanguage);
                    //document.addEventListener("deviceready", selectLanguage, false);
                });

                // Choose prefered language
                function selectLanguage() {
                    oj.Config.setLocale('english',
                            function () {
                                $('html').attr({'lang': 'en-us', 'dir': 'ltr'});
                                self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                self.loginword(oj.Translations.getTranslatedString('login'));
                                self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                                self.localeName(oj.Translations.getTranslatedString('Name'));
                                self.localeJop(oj.Translations.getTranslatedString('JOP'));
                                self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                                self.localeDep(oj.Translations.getTranslatedString('Dep'));
                                self.localeGrade(oj.Translations.getTranslatedString('Grade'));

                            }
                    );
                }
                ;


                //
                // Set Prefered Language
                self.setPreferedLanguage = function (lang) {
                    var selecedLanguage = lang;
                    storage.setItem('setLang', selecedLanguage);
                    storage.setItem('setTrue', true);
                    preferedLanguage = storage.getItem('setLang');

                    // Call function to convert arabic
                    changeToArabic(preferedLanguage);
                };


                function changeToArabic(preferedLanguage) {
                    console.log('Preffered Lang = ' + preferedLanguage);
                    if (preferedLanguage == 'arabic') {

                        oj.Config.setLocale('ar-sa',
                                function () {
                                    $('html').attr({'lang': 'ar-sa', 'dir': 'rtl'});
                                    self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                    self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                    self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                    self.loginword(oj.Translations.getTranslatedString('login'));
                                    self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                                    self.localeName(oj.Translations.getTranslatedString('Name'));
                                    self.localeJop(oj.Translations.getTranslatedString('JOP'));
                                    self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                                    self.localeDep(oj.Translations.getTranslatedString('Dep'));
                                    self.localeGrade(oj.Translations.getTranslatedString('Grade'));
                                }
                        );

                    } else if (preferedLanguage == 'english') {
                        selectLanguage();


                    }
                    ;
                }
                ;
                $('.login-label').toggleClass('user-label-right');
                $('.input100').toggleClass('box-input-text');
                $('.wrap-input100').toggleClass('box-input-text');
                self.changeLanguage = function () {
                    //storage.setItem('setLang', 'arabic');
                    if (self.language() === 'english')
                    {
                        self.language("arabic");
                        self.setPreferedLanguage(self.language());
                        self.languagelable("English");
                        $('.login-label').toggleClass('user-label-right');
                        $('.login-label').removeClass('user-label-left');
                        $('.input100').toggleClass('box-input-text');
                        $('.wrap-input100').toggleClass('box-input-text');
                    } else if (self.language() === 'arabic')
                    {
                        self.language("english");
                        self.setPreferedLanguage(self.language());
                        self.languagelable("عربي");
                        $('.login-label').toggleClass('user-label-left');
                        $('.login-label').removeClass('user-label-right');
                        $('.input100').removeClass('box-input-text');
                        $('.wrap-input100').removeClass('box-input-text');


                    }
                    //  self.setPreferedLanguage('arabic');
                    //  self.languagelable("Arabic");
                };


                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});


                // Related to New Design Add BY Mohamed Yasser  //Start

                // Search Start
                this.keyword = ko.observableArray();


                //Business Unit Input search Start 

                self.searchWord = ko.observable();
                self.tags = [
                    {value: "Value 1", label: "Unit 1"},
                    {value: "Value 2", label: "Unit 2"},
                    {value: "Value 3", label: "Unit 3"},
                    {value: "Value 4", label: "Unit 4"},
                    {value: "Value 5", label: "Unit 5"},
                    {value: "Value 6", label: "Unit 6"},
                    {value: "Value 7", label: "Unit 7"}
                ];

                self.tagsDataProvider = new oj.ArrayDataProvider(this.tags, {keyAttributes: 'value'});

                self.search = function (event) {



                    var trigger = event.type;
                    var term;

                    if (trigger === "ojValueUpdated") {
                        // search triggered from input field
                        // getting the search term from the ojValueUpdated event
                        term = event['detail']['value'];
                        trigger += " event";
                    } else {
                        // search triggered from end slot
                        // getting the value from the element to use as the search term.
                        term = document.getElementById("search").value;
                        trigger = "click on search button";
                    }

                };

                //Business Unit Input search END

                // Search End

                // toolbar Start

                this.toolbarClassNames = ko.observableArray([]);

                this.toolbarClasses = ko.computed(function () {
                    return this.toolbarClassNames().join(" ");
                }, this);
                // toolbar End

                // Related to New Design Add BY Mohamed Yasser  //End

                self.loadModule = function () {
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath})
                        ]);
                        masterPromise.then(
                                function (values) {
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                },
                                function (reason) {}
                        );
                    });
                };

                // Navigation setup
                var navData = [
                    /* {name: 'Search', id: 'searchProformaInvoice',
                     iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'},
                     {name: 'Acknowledge', id: 'proFormaInvAckn',
                     iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'},
                     {name: "Search Invoices", id: 'search',
                     iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'}*/
//	   {name:  "Product Registration", id: 'productRegistration',
//       iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'},
                    {name: "Search", id: 'searchProductRegistration',
                        iconClass: 'oj-navigationlist-item-icon oj-fwk-icon-magnifier oj-fwk-icon'}
                ];
                self.navDataSource = new oj.ArrayTableDataSource(navData, {idAttribute: 'id'});

                // Drawer
                // Close offcanvas on medium and larger screens
                self.mdScreen.subscribe(function () {
                    oj.OffcanvasUtils.close(self.drawerParams);
                });
                self.drawerParams = {
                    displayMode: 'push',
                    selector: '#navDrawer',
                    content: '#pageContent'
                };
                // Called by navigation drawer toggle button and after selection of nav drawer item
                self.toggleDrawer = function () {
                    return oj.OffcanvasUtils.toggle(self.drawerParams);
                }
                // Add a close listener so we can move focus back to the toggle button when the drawer closes
                $("#navDrawer").on("ojclose", function () {
                    $('#drawerToggleButton').focus();
                });

                // Header
                // Application Name used in Branding Area
                self.appName = ko.observable("شركة بدر سلطان وإخوانه");
                // User Info used in Global Navigation area


                // Footer
                function footerLink(name, id, linkTarget) {
                    this.name = name;
                    this.linkId = id;
                    this.linkTarget = linkTarget;
                }
                self.footerLinks = ko.observableArray([
                    new footerLink('About Oracle', 'aboutOracle', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
                    new footerLink('Contact Us', 'contactUs', 'http://www.oracle.com/us/corporate/contact/index.html'),
                    new footerLink('Legal Notices', 'legalNotices', 'http://www.oracle.com/us/legal/index.html'),
                    new footerLink('Terms Of Use', 'termsOfUse', 'http://www.oracle.com/us/legal/terms/index.html'),
                    new footerLink('Your Privacy Rights', 'yourPrivacyRights', 'http://www.oracle.com/us/legal/privacy/index.html')
                ]);
            }



            return new ControllerViewModel();
        }
);
