/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.rest;

/**
 *
 * @author Husam Al-Masri
 */
import com.appspro.tawal.getSAASData.RetriveAbsenceTransaction;
import com.appspro.tawal.getSAASData.RetriveAbsenceTransactionWorkflow;
import com.appspro.tawal.getSAASData.RetriveAllEmployeeMasterData;
import com.appspro.tawal.getSAASData.RetriveAllPoAndPr;
import com.appspro.tawal.getSAASData.RetriveApPayment;
import com.appspro.tawal.getSAASData.RetriveGlAccount;
import com.appspro.tawal.getSAASData.RetrivePOStatus;
import com.appspro.tawal.getSAASData.RetrivePoProjectDetail;
//import com.appspro.tawal.getSAASData.RetriveGlAccount;
import com.appspro.tawal.getSAASData.RetrivePrPo;
import com.appspro.tawal.getSAASData.RetrivePrPoHeader;
import com.appspro.tawal.getSAASData.RetrivePrProjectDetail;
import com.appspro.tawal.getSAASData.RetriveSupplierRegistration;
import com.appspro.tawal.getSAASData.RetriveSupplierSite;
import com.appspro.tawal.getSAASData.RetriveSupplierStatement;
import com.appspro.tawal.getSAASData.RetriveTerminationData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("/appsProPaasRest")
public class AppsProPaasRest {

    @GET
    @Path("/GetPrPo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPrPo(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetrivePrPo retrivePrPo = new RetrivePrPo();
            JSONArray res
                    = retrivePrPo.getPrPoData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetAllPrPo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllPrPo(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveAllPoAndPr retriveAllPrPo = new RetriveAllPoAndPr();
            JSONArray res
                    = retriveAllPrPo.getAllPrPoData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();

        }
    }

    @GET
    @Path("/GetAllPrPoHeader")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllPrPoHeader(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetrivePrPoHeader retrivePrPoHeader = new RetrivePrPoHeader();
            JSONArray res
                    = retrivePrPoHeader.getPrPoHeaderData();
            res.toString();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();

            //      return "Unauthorized";
        }
    }

    @GET
    @Path("/GetAllAbsance")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllAbsance(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveAbsenceTransaction retriveAbsenceTransaction = new RetriveAbsenceTransaction();
            JSONArray res
                    = retriveAbsenceTransaction.getAbsanceTransactionData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetAllAbsanceWorkflow")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllAbsanceWorkflow(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveAbsenceTransactionWorkflow retriveAbsenceTransactionWorkflow = new RetriveAbsenceTransactionWorkflow();
            JSONArray res
                    = retriveAbsenceTransactionWorkflow.getAbsanceTransactionWorkflowData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetAllEmployee")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllEmployee(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveAllEmployeeMasterData retriveAllEmployeeMasterData = new RetriveAllEmployeeMasterData();
            JSONArray res
                    = retriveAllEmployeeMasterData.getAllEmployeeData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetAllTermination")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllTermination(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveTerminationData retriveTerminationData = new RetriveTerminationData();
            JSONArray res
                    = retriveTerminationData.getAllTerminationData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetApPayment")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllApPayment(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveApPayment retriveApPayment = new RetriveApPayment();
            JSONArray res
                    = retriveApPayment.getApPaymentData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetSupplierSite")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllSupplierSite(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveSupplierSite retriveSupplierSite = new RetriveSupplierSite();
            JSONArray res
                    = retriveSupplierSite.getSupplierSiteData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetSupplierRegistration")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllSupplierRegistration(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveSupplierRegistration retriveSupplierRegistration = new RetriveSupplierRegistration();
            JSONArray res
                    = retriveSupplierRegistration.getSupplierRegistrationData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetSupplierStatement")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllSupplierStatement(@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate, @Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString
    ) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveSupplierStatement retriveSupplierStatement = new RetriveSupplierStatement();
            JSONArray res
                    = retriveSupplierStatement.getSupplierStatementData(startDate, endDate);
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetGlAccount")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllGlAccount(@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetriveGlAccount retriveGlAccount = new RetriveGlAccount();
            JSONArray res
                    = retriveGlAccount.getGlAccountData(startDate, endDate);
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetPoStatus")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPoStatus(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetrivePOStatus retrivePOStatus = new RetrivePOStatus();
            JSONArray res
                    = retrivePOStatus.getPoStatusData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetPoProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPoProjectDetail(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetrivePoProjectDetail retrivePoProjectDetail = new RetrivePoProjectDetail();
            JSONArray res
                    = retrivePoProjectDetail.getPoProjectData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/GetPrProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response RetrivePrProjectDetail(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        if (authString.equalsIgnoreCase("VXNlcjpUYXdhbEAxMjNQYXNzd29yZDpUYXdhbEAxMjA5ODcyMyE=")) {
            RetrivePrProjectDetail retrivePrProjectDetail = new RetrivePrProjectDetail();
            JSONArray res
                    = retrivePrProjectDetail.getPrProjectData();
            return Response.status(Response.Status.OK).entity(res.toString()).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

}
