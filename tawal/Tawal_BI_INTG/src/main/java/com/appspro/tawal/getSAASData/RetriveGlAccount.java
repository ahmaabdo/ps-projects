/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.tawal.getSAASData;

import com.appspro.tawal.biReport.BIReportFinanceModel;
import static com.appspro.tawal.biReport.BIReportFinanceModel.runReportWithParam;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author DELL
 */
public class RetriveGlAccount {

    public JSONArray getGlAccountData(String START_DATE, String END_DATE) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if (START_DATE != null) {

                paramMap.put(BIReportFinanceModel.GET_GL_ACCOUNT_REPORT_PARAM.START_DATE.getValue(),
                        START_DATE);
            }
            if (END_DATE != null) {

                paramMap.put(BIReportFinanceModel.GET_GL_ACCOUNT_REPORT_PARAM.END_DATE.getValue(),
                        END_DATE);
            }
            JSONObject json
                    = runReportWithParam(BIReportFinanceModel.REPORT_NAME.GL_Account_Report.getValue(),paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return null;
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");

            if (!dataDS.isNull("G_2")) {
              //  Object obj = dataDS.get("G_1");
                Object obj2 = dataDS.get("G_2");
                if (obj2 instanceof JSONArray) {
                    JSONArray ja = new JSONArray();
                //    JSONArray g1 = dataDS.getJSONArray("G_1");
                    JSONArray g2 = dataDS.getJSONArray("G_2");
                  //  ja.put(g1);
                    ja.put(g2);
                    return ja;
                } else {
                 //   JSONObject g1 = dataDS.getJSONObject("G_1");
                    JSONObject g2 = dataDS.getJSONObject("G_2");
                    JSONArray ja = new JSONArray();

                //    ja.put(g1);
                    ja.put(g2);
                    return ja;
                }
            }
            return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
