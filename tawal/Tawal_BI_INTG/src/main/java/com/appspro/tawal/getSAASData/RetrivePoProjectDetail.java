/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.tawal.getSAASData;

import com.appspro.tawal.biReport.BIReportProcurementModel;
import static com.appspro.tawal.biReport.BIReportProcurementModel.runReport;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author DELL
 */
public class RetrivePoProjectDetail {

    public JSONArray getPoProjectData() {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();

            JSONObject json
                    = runReport(BIReportProcurementModel.REPORT_NAME.PO_Projects_Details_Report.getValue());
            System.out.println(json);
            if (json.length() < 1) {
                return null;
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");

            if (!dataDS.isNull("G_1")) {
                Object obj = dataDS.get("G_1");
              //  Object obj2 = dataDS.get("G_2");
                if (obj instanceof JSONArray) {
                    JSONArray ja = new JSONArray();
                    JSONArray g1 = dataDS.getJSONArray("G_1");
                   // JSONArray g2 = dataDS.getJSONArray("G_2");
//                    Object g3 = dataDS.get("CS_DEBIT");
//                    Object g4 = dataDS.get("CS_CREDIT");
                    ja.put(g1);
                 //   ja.put(g2);
//                    ja.put(g3);
//                    ja.put(g4);
                    return ja;
                } else {
                    JSONArray g1 = dataDS.getJSONArray("G_1");
                    //JSONObject g2 = dataDS.getJSONObject("G_2");
//                    Object g3 = dataDS.get("CS_DEBIT");
//                    Object g4 = dataDS.get("CS_CREDIT");
                    JSONArray ja = new JSONArray();

                    ja.put(g1);
                   // ja.put(g2);
//                    ja.put(g3);
//                    ja.put(g4);
                    return ja;
                }
            }
            return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
