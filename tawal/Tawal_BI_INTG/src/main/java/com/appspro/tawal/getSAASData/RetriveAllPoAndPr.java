/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.tawal.getSAASData;

import com.appspro.tawal.biReport.BIReportProcurementModel;
import static com.appspro.tawal.biReport.BIReportProcurementModel.runReport;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Husam Al-Masri
 */
public class RetriveAllPoAndPr {

    public JSONArray getAllPrPoData() {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();

            JSONObject json
                    = runReport(BIReportProcurementModel.REPORT_NAME.ALL_PR_POReport.getValue());
            System.out.println(json);
            if (json.length() < 1) {
                return null;
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");

            if (!dataDS.isNull("G_PO_AMOUNT_PER_DEPRT") && !dataDS.isNull("G_PO_PER_DEPART") && !dataDS.isNull("G_PO_STATUS") && !dataDS.isNull("G_PR_TO_PO") && !dataDS.isNull("G_PR_PER_DEPART") && !dataDS.isNull("G_PR_STATUS")) {
                Object obj = dataDS.get("G_PO_AMOUNT_PER_DEPRT");
                Object obj2 = dataDS.get("G_PO_PER_DEPART");
                Object obj3 = dataDS.get("G_PO_STATUS");
                Object obj4 = dataDS.get("G_PO_STATUS");
                Object obj5 = dataDS.get("G_PR_PER_DEPART");
                Object obj6 = dataDS.get("G_PR_STATUS");
                if (obj instanceof JSONArray && obj2 instanceof JSONArray && obj3 instanceof JSONArray && obj4 instanceof JSONArray && obj5 instanceof JSONArray && obj6 instanceof JSONArray) {
                    JSONArray ja = new JSONArray();
                    JSONArray g1 = dataDS.getJSONArray("G_PO_AMOUNT_PER_DEPRT");
                    JSONArray g2 = dataDS.getJSONArray("G_PO_PER_DEPART");
                    JSONArray g3 = dataDS.getJSONArray("G_PO_STATUS");
                    JSONArray g4 = dataDS.getJSONArray("G_PR_TO_PO");
                    JSONArray g5 = dataDS.getJSONArray("G_PR_PER_DEPART");
                    JSONArray g6 = dataDS.getJSONArray("G_PR_STATUS");
                    ja.put(g1);
                    ja.put(g2);
                    ja.put(g3);
                    ja.put(g4);
                    ja.put(g5);
                    ja.put(g6);
                    return ja;
                } else {
                    JSONObject g1 = dataDS.getJSONObject("G_PO_AMOUNT_PER_DEPRT");
                    JSONObject g2 = dataDS.getJSONObject("G_PO_PER_DEPART");
                    JSONObject g3 = dataDS.getJSONObject("G_PO_STATUS");
                    JSONObject g4 = dataDS.getJSONObject("G_PR_TO_PO");
                    JSONObject g5 = dataDS.getJSONObject("G_PR_PER_DEPART");
                    JSONObject g6 = dataDS.getJSONObject("G_PR_STATUS");
                    JSONArray ja = new JSONArray();

                    ja.put(g1);
                    ja.put(g2);
                    ja.put(g1);
                    ja.put(g3);
                    ja.put(g4);
                    ja.put(g5);
                    ja.put(g6);
                    return ja;
                }
            }
            return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
