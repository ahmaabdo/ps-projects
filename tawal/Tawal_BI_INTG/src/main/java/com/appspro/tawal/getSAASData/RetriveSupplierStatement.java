/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.tawal.getSAASData;

import com.appspro.tawal.biReport.BIReportFinanceModel;
import static com.appspro.tawal.biReport.BIReportFinanceModel.runReportWithParam;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Husam Al-Masri
 */
public class RetriveSupplierStatement {

    public JSONArray getSupplierStatementData(String START_DATE, String END_DATE) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if (START_DATE != null) {

                paramMap.put(BIReportFinanceModel.GET_SUPPLIER_STATEMENT_REPORT_PARAM.START_DATE.getValue(),
                        START_DATE);
            }
            if (END_DATE != null) {

                paramMap.put(BIReportFinanceModel.GET_SUPPLIER_STATEMENT_REPORT_PARAM.END_DATE.getValue(),
                        END_DATE);
            }
//            if (company != null) {
//
//                paramMap.put(BIReportFinanceModel.GET_SUPPLIER_STATEMENT_REPORT_PARAM.COMPANY.getValue(),
//                        company);
//            }
//
//            if (supplier != null) {
//
//                paramMap.put(BIReportFinanceModel.GET_SUPPLIER_STATEMENT_REPORT_PARAM.SUPPLIER.getValue(),
//                        supplier);
//            }
//
//            if (supplierNumber != null) {
//
//                paramMap.put(BIReportFinanceModel.GET_SUPPLIER_STATEMENT_REPORT_PARAM.SUPPLIER_NUMBER.getValue(),
//                        supplierNumber);
//            }
//            
//            if (supplierSite != null) {
//
//                paramMap.put(BIReportFinanceModel.GET_SUPPLIER_STATEMENT_REPORT_PARAM.SUPPLIER_SITE.getValue(),
//                        supplierSite);
//            }

            JSONObject json
                    = runReportWithParam(BIReportFinanceModel.REPORT_NAME.Supplier_Statement_Report.getValue(), paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return null;
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");

            if (!dataDS.isNull("G_1") && !dataDS.isNull("G_2")) {
                Object obj = dataDS.get("G_1");
                Object obj2 = dataDS.get("G_2");
                if (obj instanceof JSONArray && obj2 instanceof JSONArray) {
                    JSONArray ja = new JSONArray();
                    JSONArray g1 = dataDS.getJSONArray("G_1");
                    JSONArray g2 = dataDS.getJSONArray("G_2");
//                    Object g3 = dataDS.get("CS_DEBIT");
//                    Object g4 = dataDS.get("CS_CREDIT");
                    ja.put(g1);
                    ja.put(g2);
//                    ja.put(g3);
//                    ja.put(g4);
                    return ja;
                } else {
                    JSONArray g1 = dataDS.getJSONArray("G_1");
                    JSONObject g2 = dataDS.getJSONObject("G_2");
//                    Object g3 = dataDS.get("CS_DEBIT");
//                    Object g4 = dataDS.get("CS_CREDIT");
                    JSONArray ja = new JSONArray();

                    ja.put(g1);
                    ja.put(g2);
//                    ja.put(g3);
//                    ja.put(g4);
                    return ja;
                }
            }
            return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
