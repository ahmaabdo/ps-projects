/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.tawal.getSAASData;

import com.appspro.tawal.biReport.BIReportProcurementModel;
import static com.appspro.tawal.biReport.BIReportProcurementModel.runReport;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Husam Al-Masri
 */
public class RetrivePrPoHeader {
    
     public JSONArray getPrPoHeaderData() {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
           
            JSONObject json =
                runReport(BIReportProcurementModel.REPORT_NAME.PR_PO_HEADER_Report.getValue());
            System.out.println(json);
            if (json.length() < 1) {
                return null;
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");

            if (!dataDS.isNull("G_1")) {
                Object obj = dataDS.get("G_1");
                if (obj instanceof JSONArray) {
                    JSONArray g1 = dataDS.getJSONArray("G_1");
                    return g1;
                } else {
                    JSONObject g1 = dataDS.getJSONObject("G_1");
                    JSONArray ja = new JSONArray();

                    ja.put(g1);
                    return ja;
                }
            }
            return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    
}
