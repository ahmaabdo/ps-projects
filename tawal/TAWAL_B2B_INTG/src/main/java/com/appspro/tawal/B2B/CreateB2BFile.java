package com.appspro.tawal.B2B;

import com.appspro.bean.SadadBean;
import com.appspro.rest.AppsProPaasRest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Base64;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Husam Al-Masri
 */
public class CreateB2BFile {

    public static String GetDataAndTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        System.out.println(formatter.format(date));
        String dateTime = formatter.format(date).replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        System.out.println("R_" + dateTime);
        return dateTime;
    }

    public static String GetDataAndTimeForBody() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();
        String dateTime = formatter.format(date);//.replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        return dateTime;
    }

    public String encodeDecodeString(String stringToEncode) {
        // encode string using Base64 encoder
        Base64.Encoder encoder = Base64.getEncoder();
        String encoded = encoder.encodeToString(stringToEncode.getBytes());
        System.out.println("Encoded Data: " + encoded);

        // decode the encoded data
        Base64.Decoder decoder = Base64.getDecoder();
        String decoded = new String(decoder.decode(encoded));
        System.out.println("Decoded Data: " + decoded);
        return encoded;
    }

    public void CreateAndWriteToFile(List<String> listOfTransaction) {
//        System.out.println(listOfTransaction.toString().replaceAll("[\\[\\]]", "").replaceAll(",", " "););
        String transactionBody = listOfTransaction.toString().replaceAll("[\\[\\]]", "").replaceAll(",", " ");
        PostFileToBank postFileToBank = new PostFileToBank();
        try {
            FileWriter myWriter = new FileWriter("C:\\Users\\DELL\\Desktop\\" + GetDataAndTime() + ".txt");
            myWriter.write("<Message>\n"
                    + "<Header>\n"
                    + "<Sender>SABB000001</Sender>\n"
                    + "<AccountNumber>061000248032</AccountNumber>\n"
                    + "<MessageType>SADADTRN</MessageType>\n"
                    + "<FileRef>" + GetDataAndTime() + "</FileRef>\n"
                    + "<MessageDescription>SADAD Transaction</MessageDescription>\n"
                    + "<TimeStamp>" + GetDataAndTimeForBody() + "</TimeStamp>\n"
                    + "</Header>\n"
                    + "<Body>\n"
                    + transactionBody
                    + "</Body>\n"
                    + "<Signature>\n"
                    + "<SignatureValue> eOLUftt05CLl6eVwBzjQVthZUf44YSHuoumEkb6gBrc8NC+GrjbHwV6qOsekApGz5sQo5tUmNmMpTvKKvaPwI6D10D8G5IrBsTIZkHdAAHcL6BHRliiBVb/kSe4hOKpgkGmu/ti54YldBtK87mEzpIi3isezjMnpllePdZJPp9p67toTHA13tRS90DhClARUPnwmBOBhvemquUng==</SignatureValue>\n"
                    + "</Signature>\n"
                    + "</Message>");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
//            System.out.println(encoder("C:\\Users\\DELL\\Desktop\\" + GetDataAndTime() + ".txt"));
            String endodeFile = encoder("C:\\Users\\DELL\\Desktop\\" + GetDataAndTime() + ".txt");
            System.out.println(endodeFile);
            postFileToBank.postFileSadad(endodeFile);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
    
    public static String encoder(String filePath) {
        String base64File = "";
        File file = new File(filePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a file from file system
            byte fileData[] = new byte[(int) file.length()];
            imageInFile.read(fileData);
            base64File = Base64.getEncoder().encodeToString(fileData);
        } catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file " + ioe);
        }
        return base64File;
    }

    public void getDataAndCheckType() {
        AppsProPaasRest appsProPaasRest = new AppsProPaasRest();
        SadadBean sadadBean = new SadadBean();
        String bodyTransaction;
        List<String> listOfBodyTransaction = new ArrayList<>();
        CreateB2BFile createB2BFile = new CreateB2BFile();
        JSONObject jsonObj = new JSONObject(appsProPaasRest.getAllPaymentForSadad());
        int count = Integer.parseInt(jsonObj.get("count").toString());
        JSONArray items = jsonObj.getJSONArray("items");
        if (count >= 1) {
            for (int i = 0; i < items.length(); ++i) {
                JSONObject rec = items.getJSONObject(i);
                JSONArray paymentDff = rec.getJSONArray("paymentDff");
                for (int j = 0; j < paymentDff.length(); j++) {
                    JSONObject paymentDffObj = paymentDff.getJSONObject(j);
                    Long CheckId = paymentDffObj.getLong("CheckId");
                    String typeOfPayment = paymentDffObj.optString("__FLEX_Context_DisplayValue");
                    if (typeOfPayment.equalsIgnoreCase("SADAD ")) {
                        String latterReceiverStatues = paymentDffObj.optString("latterReceiverStatues");
                        if ("".equals(latterReceiverStatues)) {
                            String sadadNumber = paymentDffObj.optString("sadadNumber");
                            String receiverConfirmation = paymentDffObj.optString("receiverConfirmation");
//                            System.out.println(sadadNumber);
                            sadadBean.setBillerID(rec.optString("City"));
                            sadadBean.setConsumerID(rec.optString("PayeeSite"));
                            sadadBean.setReferenceNumber(rec.optString("VoucherNumber"));
                            sadadBean.setSubscriberNumber(paymentDffObj.optString("sadadNumber"));
                            bodyTransaction = buildTransactionBody(sadadBean);
//                            System.out.println(bodyTransaction);
                            listOfBodyTransaction.add(bodyTransaction);

                        }
                    }
                }
            }
//            System.out.println(listOfBodyTransaction);
            CreateAndWriteToFile(listOfBodyTransaction);

        }
    }

    public String buildTransactionBody(SadadBean sadadBean) {
        String transactionTag = "<Transaction>\n"
                + "<Reference_number>" + sadadBean.getReferenceNumber() + "</Reference_number>\n"
                + "<BillerID>" + sadadBean.getBillerID() + "</BillerID>\n"
                + "<SubscriberNumber>" + sadadBean.getSubscriberNumber() + "</SubscriberNumber>\n"
                + "<ConsumerID>" + sadadBean.getConsumerID() + "</ConsumerID>\n"
                + "<Amount>10</Amount>\n"
                + "<PayExactDue>Y</PayExactDue>\n"
                + "</Transaction>\n";
        return transactionTag;
    }

    public static void main(String args[]) {
        CreateB2BFile createB2BFile = new CreateB2BFile();
//        createB2BFile.CreateAndWriteToFile();
//        createB2BFile.GetDataAndTime();
//        createB2BFile.encodeDecodeString("BSS-TMS User:Tawal@123456");
        createB2BFile.getDataAndCheckType();
    }
}
