/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.rest;

import com.appspro.restHelper.RestHelper;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Husam Al-Masri
 */
@Path("/appsProPaasRest")
public class AppsProPaasRest {

    @GET
    @Path("/GetAllPayment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPaymentForSadad() {
        try {
            String json = RestHelper.callGetRest("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff&onlyData=true&limit=500", "d2FsZWVkLmVsa2FzaGVmQGFwcHNwcm8tbWUuY29tOkFwcHNwcm9AMTIzNDU2");
            return json;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
