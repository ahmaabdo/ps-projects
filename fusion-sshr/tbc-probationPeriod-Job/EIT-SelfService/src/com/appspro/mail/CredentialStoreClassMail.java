package com.appspro.mail;


import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import com.sun.istack.ByteArrayDataSource;

import java.io.OutputStream;

import java.net.URL;

import java.nio.charset.StandardCharsets;

import java.util.Base64;
import java.util.Iterator;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;


public class CredentialStoreClassMail {

    static private String smtpAuthUserName = "oracless@tbc.sa";
    static private String smtpAuthPassword = "Notification@1234@";
    static private String emailFrom = "oracless@tbc.sa";
    static private String clientWebsite = "TBC.com.sa";

    public static void SEND_MAIL(String emailToEmp, String subject,
                                  String body) {
        try {

            String emailTo = emailToEmp;
            String emailBcc = "abdullah.oweidi@appspro-me.com";
            //String emailBcc2 = "shadi.mansi@appspro-me.com";
            String emailBcc3 = "maha.mohamed@appspro-me.com";
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpAuthUserName,
                                                      smtpAuthPassword);
                }
            };
            Properties properties = new Properties();
            properties.setProperty("mail.smtp.host", "smtp.office365.com");
            properties.setProperty("mail.smtp.port", "587");
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(properties, authenticator);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom, "TBC HR Services"));
            InternetAddress[] to = { new InternetAddress(emailTo) };
            message.setRecipients(Message.RecipientType.TO, to);
            InternetAddress[] bcc =
            { new InternetAddress(emailBcc),  new InternetAddress(emailBcc3)};
            message.setRecipients(Message.RecipientType.BCC, bcc);
            message.setContent(body, "text/html");
            message.setSubject(subject);
            //message.setText(body);
            Transport.send(message);


        } catch (Exception e) {

            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }

    }

    public static void sendMailWithAttachment(String emailToEmp,
                                               String subject, String body,
                                               String attachment,
                                               String filename) {
        try {

            String emailTo = emailToEmp;
            String emailCC = null;
            String emailBcc = "abdullah.oweidi@appspro-me.com";
            String emailBcc2 = "maha.mohamed@appspro-me.com";
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpAuthUserName,
                                                      smtpAuthPassword);
                }
            };
            Properties properties = new Properties();
            properties.setProperty("mail.smtp.host", "smtp.office365.com");
            properties.setProperty("mail.smtp.port", "587");
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(properties, authenticator);
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom, "TBC HR Services"));
            InternetAddress[] to = { new InternetAddress(emailTo) };
            message.setRecipients(Message.RecipientType.TO, to);
            InternetAddress[] cc = { new InternetAddress(emailCC) };
            message.setRecipients(Message.RecipientType.CC, cc);
            InternetAddress[] bcc =
            { new InternetAddress(emailBcc), new InternetAddress(emailBcc2) };
            message.setRecipients(Message.RecipientType.BCC, bcc);
            message.setSubject(subject);
            Multipart multipart = new MimeMultipart();
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setContent(body, "text/html");
            multipart.addBodyPart(textBodyPart);
            if (!attachment.isEmpty()) {
                String string =
                    attachment.substring(attachment.indexOf("data:"),
                                         attachment.indexOf("base64,") - 1);
                String contentType = string.substring(5);
                String base64Data =
                    attachment.substring(attachment.lastIndexOf("base64,") +
                                         7);
                byte[] imgBytes =
                    Base64.getDecoder().decode(base64Data.getBytes(StandardCharsets.UTF_8));
                DataSource source =
                    new ByteArrayDataSource(imgBytes, contentType);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);
                multipart.addBodyPart(messageBodyPart);
            }


            message.setContent(multipart);

            System.out.println("Sending");

            Transport.send(message);

            System.out.println("Done");


        } catch (Exception e) {

            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }

    }

    public static void main(String[] args) {
        try {
            JSONObject jsonObject =
                new JSONObject("{\"dummy\":1584394431712,\"employeeLocation\":\"Head Office\",\"travelType\":\"Regular Business Trip\",\"tripDetails\":\"23456\",\"grade\":11,\"businessTripRoute\":\"International\",\"fromDestination\":\"Al Baha\",\"toDestinationCountry\":\"United States\",\"toDestinationCity\":\"Washington D.C.\",\"startDate\":\"2020-03-17\",\"endDate\":\"2020-03-18\",\"numberOfDays\":5,\"balanceDays\":27,\"dutyDelegatedTo\":\"000055 Alburaidi, Abdulrahman\",\"employeeClass\":\"Economic class\",\"travelBy\":\"Car\",\"ticketClass\":\"No Need\",\"ticketAmount\":\"\",\"numberOfKilometers\":100,\"totalPerDiemAmount\":7570,\"travelingOnBehalfOfOtherDepart\":\"CR Quality Control & HSSE\",\"attachment\":\"YES\"}");
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();

                System.out.println(key + "  " + jsonObject.get(key));

            }
            //            SEND_MAIL("abdallah.oweidi@appspro-me.com", "asd", "asds");
        } catch (Exception e) {
            AppsproConnection.LOG.error(e.getMessage());
        }
    }
}
