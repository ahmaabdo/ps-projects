package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.mail.CredentialStoreClassMail;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIPReports;

import common.biPReports.BIReportModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.XML;


public class ProbationPeriodAlert {
    JSONObject jObject = null; // json
    JSONArray reportListJSON = null;
    String fresponse = null;
    String reportName = null;
    CredentialStoreClassMail mailObj = new CredentialStoreClassMail();
    
    public void getProbationPeriodFromSaaSReport() throws ParseException,
                                                          AppsProException {
    try {
        reportName = "PROBATION_PERIOD_REPORT";
        java.util.Date currentDate = new java.util.Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat ft2 = new SimpleDateFormat("dd/MM/yyyy");
        BIPReports biPReports = new BIPReports();
        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

        if (reportName != null) {
                String paramName;


                SummaryReportDAO summary = new SummaryReportDAO();
                SummaryReportBean reportBeans = new SummaryReportBean();

                ArrayList arr = summary.getReportParamSummary(reportName);
                if (!arr.isEmpty()) {
                    reportBeans = (SummaryReportBean)arr.get(0);
                }


                if (reportBeans.getParameter1() != null) {
                    paramName = reportBeans.getParameter1();
                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }


                }
                if (reportBeans.getParameter2() != null) {
                    paramName = reportBeans.getParameter2();

                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }


                }
                if (reportBeans.getParameter3() != null) {
                    paramName = reportBeans.getParameter3();
                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }

                }
                if (reportBeans.getParameter4() != null) {
                    paramName = reportBeans.getParameter4();
                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }

                }
                if (reportBeans.getParameter5() != null) {
                    paramName = reportBeans.getParameter5();
                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }

                }

                if (reportBeans.getReportTypeVal() != null) {
                    if (reportBeans.getReportTypeVal().equals("xml")) {

                        biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                        biPReports.setReportAbsolutePath(reportBeans.getReport_Name());

                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj =
                            XML.toJSONObject(fresponse.toString());

                            if (!xmlJSONObj.isNull("DATA_DS") &&
                                !xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                if (xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) {

                                    System.out.println( Response.ok("[]",
                                                       MediaType.TEXT_PLAIN).build());
                                } else {
                                        JSONObject dataDS = null;
                                        JSONArray g1 = null;
                                        try {
                                             dataDS = xmlJSONObj.getJSONObject("DATA_DS");
                                             g1 = new JSONArray();
                                            Object jsonTokner1 =
                                                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
                                            if (jsonTokner1 instanceof JSONObject) {
                                                JSONObject arrJson = dataDS.getJSONObject("G_1");

                                                g1.put(arrJson);
                                            } else {
                                                JSONArray arrJson = dataDS.getJSONArray("G_1");
                                                g1 = arrJson;
                                            }
                                            for (int i = 0; i < g1.length();
                                                 i++) {
                                                JSONObject data =
                                                    (JSONObject)g1.get(i);
                                                EmployeeDetails obj = new EmployeeDetails();
                                                PeopleExtraInformationBean pro = new PeopleExtraInformationBean();
                                                PeopleExtraInformationDAO peid = new PeopleExtraInformationDAO();
                                                
                                                EmployeeBean obj2 = new EmployeeBean();
                                                obj2 = obj.searchPersonDetails("", "", data.get("PERSON_NUMBER").toString(), "", "", "", "");

                                                pro.setPerson_number(obj2.getPersonNumber());
                                                pro.setPerson_id(obj2.getPersonId());
                                                pro.setPersonName(obj2.getDisplayName());
                                                pro.setLine_manager(obj2.getManagerId());
                                                pro.setManage_of_manager(obj2.getManagerOfManager());
                                                pro.setPEI_INFORMATION1(obj2.getEmail());
                                                pro.setPEI_INFORMATION2(obj2.getDepartmentName());
                                                pro.setPEI_INFORMATION3(obj2.getPositionName());
                                                pro.setPEI_INFORMATION4(data.opt("NATIONALITY").toString());
                                                pro.setPEI_INFORMATION5(data.opt("MAN_EMP_NAME").toString());
                                                Date hireDate= ft.parse(obj2.getHireDate());
                                                pro.setPEI_INFORMATION6(ft2.format(hireDate));
                                                pro.setPEI_INFORMATION7(ft2.format(currentDate));
                                                pro.setManagerName(obj2.getManagerName());
                                                pro.setCode("XXX_PROBATION_PERIOD");
                                                pro.setEit_name("Probation Period");
                                                pro.setCreation_date(obj2.getPersonId());
                                                pro.setStatus("PENDING_APPROVED");

                                                 peid.insertOrUpdateXXX_EIT(pro, "ADD","Default");
                                               
                                                String subject =
                                                    "Probation Request Initiated - HC / " + data.opt("PERSON_NUMBER");
                                                String body = null;

                                                    body =
                                                    "<html><head><style>\n" + 
                                                    ".sig{\n" + 
                                                    "font-size: 20px;\n" + 
                                                    "font-weight: bold;\n" + 
                                                    "    font-family: Tajawal;\n" + 
                                                    "    color: #63A70A;\n" + 
                                                    "}\n" + 
                                                    ".mob{\n" + 
                                                    "color:#63A70A;\n" + 
                                                    "font-weight: bold;\n" + 
                                                    "font-size: 13px;\n" + 
                                                    "}\n" + 
                                                    ".parColor{\n" + 
                                                    "color:#1D4757;\n" + 
                                                    "font-size:13px;\n" + 
                                                    "}\n" + 
                                                    "</style></head><body>" +
                                                    "<p>Dear "+ data.get("MAN_EMP_NAME")+",</p>" +
                                                    "<p>Probation period evaluation for <strong>"+ data.get("EMP_NAME").toString() +"</strong> has started. Please use the following link to track it<br/><strong><a href=\"https://eoay.login.em2.oraclecloud.com\"> Oracle Fusion Application -> Me -> Self-Services</a></strong></p><p><strong>Regards,</strong></p><p><strong>Human Capital Department</strong></p>\n" +
                                                    "</body></html>";

                                                System.out.println(body);
                                
                                                try {
                                                    mailObj.SEND_MAIL(data.get("EMAIL_ADDRESS_MAN").toString(),
                                                                                            subject,
                                                                                            body);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                System.out.println(Response.ok(xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                                        "value").replaceAll("LABEL",
                                                                                                                                            "label"),
                                                   MediaType.APPLICATION_JSON).build());
                            } else {
                            System.out.println(Response.ok("[]",
                                                           MediaType.TEXT_PLAIN).build());
                        }
                    } else if (reportBeans.getReportTypeVal().equals("pdf")) {

                        biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                        byte[] reportBytes =
                            biPReports.executeReportsPDF();

                        fresponse =
                                javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                        System.out.println(Response.ok("{" +
                                                       "\"REPORT\":" +
                                                       "\"" + fresponse +
                                                       "\"" + "}",
                                                       MediaType.APPLICATION_JSON).build());

                    }
                }
            
        }
    } catch (JSONException e) {
        e.printStackTrace();
        AppsproConnection.LOG.error("ERROR", e);
        //            return Response.status(500).entity(e.getMessage()).build();
    }
}
    
    public void reminderProbationPeriod(){
        EmployeeBean empObj = new EmployeeBean();
        EmployeeDetails empDetailsObj = new EmployeeDetails();
        ArrayList<ApprovalListBean> approvalListBean = new ArrayList<ApprovalListBean>();
        ApprovalListDAO approvalList = new ApprovalListDAO();
        approvalListBean = approvalList.getApprovalTypeForProbationPeriod();
        ProbationPeriodAlert ppa = new ProbationPeriodAlert();
        if(!approvalListBean.isEmpty()){
        for(int i = 0; i < approvalListBean.size(); i++){
            empObj = empDetailsObj.getEmpNameAndEmail(approvalListBean.get(i).getRoleId(), "", "");
            ppa.reminderEmailForManager(approvalListBean.get(i).getEmployeeNumber(), empObj.getDisplayName(), approvalListBean.get(i).getEmployeeName(), empObj.getEmail());
        }
        ApprovalListBean approvalListObj = new ApprovalListBean();
        approvalListObj = approvalList.getApprovalTypeForProbationPeriodER(approvalListBean.get(0).getTransActionId());
            for(int i = 0; i < approvalListBean.size(); i++){
                ppa.reminderEmailForER(approvalListObj.getRoleId(), approvalListBean.get(i).getEmployeeName(), approvalListBean.get(i).getEmployeeNumber(), approvalListBean.get(i).getPersonName());
            }
        }
        else {
            System.out.println("The data approval is Empty");
        }
        
        
    }
    
    public void reminderEmailForManager(String personNumber, String managerName, String personName, String managerEmail){
        String subject =
            "Reminder Probation Request Initiated - HC / " + personNumber;
        String body = null;

            body =
            "<html><head><style>\n" + 
            ".sig{\n" + 
            "font-size: 20px;\n" + 
            "font-weight: bold;\n" + 
            "    font-family: Tajawal;\n" + 
            "    color: #63A70A;\n" + 
            "}\n" + 
            ".mob{\n" + 
            "color:#63A70A;\n" + 
            "font-weight: bold;\n" + 
            "font-size: 13px;\n" + 
            "}\n" + 
            ".parColor{\n" + 
            "color:#1D4757;\n" + 
            "font-size:13px;\n" + 
            "}\n" + 
            "</style></head><body>" +
            "<p>Dear "+ managerName+",</p>" +
            "<p>Kindly note that probation period evaluation for <strong>"+ personName +"</strong> is still pending and waiting for your action. Please use the following link to track it<br/><strong><a href=\"https://eoay.login.em2.oraclecloud.com\"> Oracle Fusion Application -> Me -> Self-Services</a></strong></p><p><strong>Regards,</strong></p><p><strong>Human Capital Department</strong></p>\n" +
            "</body></html>";

        System.out.println(body);
        
        try {
            mailObj.SEND_MAIL(managerEmail,
                                                    subject,
                                                    body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void reminderEmailForER(String roleId, String personName, String personNumber, String managerName){
        JSONObject dataDS=null;
        try {

            Map<String, String> paramMAp =
                new HashMap<String, String>();
            paramMAp.put("P_ROLEID", roleId);
            JSONObject respone =
                BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                        paramMAp);

            dataDS = respone.getJSONObject("DATA_DS");
            JSONArray g1 = dataDS.getJSONArray("G_1");
                                 for (int i = 0; i < g1.length(); i++) {
                                     JSONObject data = (JSONObject)g1.get(i);
                                     
                                     String body =
                                     "<html><head><style>\n" + 
                                     ".sig{\n" + 
                                     "font-size: 20px;\n" + 
                                     "font-weight: bold;\n" + 
                                     "    font-family: Tajawal;\n" + 
                                     "    color: #63A70A;\n" + 
                                     "}\n" + 
                                     ".mob{\n" + 
                                     "color:#63A70A;\n" + 
                                     "font-weight: bold;\n" + 
                                     "font-size: 13px;\n" + 
                                     "}\n" + 
                                     ".parColor{\n" + 
                                     "color:#1D4757;\n" + 
                                     "font-size:13px;\n" + 
                                     "}\n" + 
                                     "</style></head><body>" +
                                     "<p>Dear "+ data.getString("FULL_NAME")+",</p>" +
                                     "<p>Probation period evaluation for <strong>"+ personName +"</strong> is still pending, and the manager <strong>"+ managerName +"</strong> did not take any action.</p><p><strong>Regards,</strong></p>\n" +
                                     "</body></html>";
                                     System.out.println(body);
                                      String subject =
                                         "Reminder Probation Request Initiated - HC / " + personNumber;
                                                                 
                                     mailObj.SEND_MAIL(data.getString("EMAIL_ADDRESS"), subject, body);
                                 }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        ProbationPeriodAlert obj = new ProbationPeriodAlert();
        
        obj.reminderProbationPeriod();
    }
    
      
}
