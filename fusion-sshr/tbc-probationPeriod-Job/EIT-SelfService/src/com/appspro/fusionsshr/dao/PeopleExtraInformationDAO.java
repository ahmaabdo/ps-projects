package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import com.appspro.mail.CredentialStoreClassMail;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.joda.time.LocalDateTime;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utils.system;


/**
 *
 * @author Shadi Mansi-PC
 */
public class PeopleExtraInformationDAO extends AppsproConnection {

    private static PeopleExtraInformationDAO instance = null;
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    EmployeeDetails employeeDetails = new EmployeeDetails();
    CredentialStoreClassMail mailObj = new CredentialStoreClassMail();
    SelfServiceDAO selfServiceDao = new SelfServiceDAO();
    SelfServiceBean sfBean = new SelfServiceBean();
    ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
    ArrayList<ApprovalSetupBean> approvalSetupList =
        new ArrayList<ApprovalSetupBean>();
    ApprovalListDAO approvalListDao = new ApprovalListDAO();
    ApprovalListBean alBean = new ApprovalListBean();
    WorkflowNotificationDAO workflowNotificationDao =
        new WorkflowNotificationDAO();
    WorkFlowNotificationBean wfn = new WorkFlowNotificationBean();

    public static PeopleExtraInformationDAO getInstance() {
        if (instance == null)
            instance = new PeopleExtraInformationDAO();
        return instance;
    }
    //******************Start Insert Or Update PeopleExtraInfo******************

    public synchronized PeopleExtraInformationBean insertOrUpdateXXX_EIT(PeopleExtraInformationBean extraInformationBean,
                                                                         String transactionType,
                                                                         String conditionCode) throws AppsProException {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            if (transactionType.equals("ADD")) {

                extraInformationBean.setId(getMaxId(connection));
                query =
                        "insert into   " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO(\n" +
                        "ID,EIT_CODE,EIT,STATUS,PERSON_NUMBER,PERSON_ID,CREATION_DATE,CREATED_BY,UPDATED_BY,UPDATED_DATE,LINE_MANAGER\n" +
                        ",MANAGER_OF_MANAGER,URL,EIT_NAME,PERSON_EXTRA_INFO_ID,EFFECTIVE_START_DATE,EFFECTIVE_END_DATE,INFORMATION_TYPE,PEI_ATTRIBUTE_CATEGORY,PEI_ATTRIBUTE1,PEI_ATTRIBUTE2\n" +
                        ",PEI_ATTRIBUTE3,PEI_ATTRIBUTE4,PEI_ATTRIBUTE5,PEI_ATTRIBUTE6,PEI_ATTRIBUTE7,PEI_ATTRIBUTE8,PEI_ATTRIBUTE9,PEI_ATTRIBUTE10,PEI_ATTRIBUTE11,PEI_ATTRIBUTE12\n" +
                        ",PEI_ATTRIBUTE13,PEI_ATTRIBUTE14,PEI_ATTRIBUTE15,PEI_ATTRIBUTE16,PEI_ATTRIBUTE17,PEI_ATTRIBUTE18,PEI_ATTRIBUTE19,PEI_ATTRIBUTE20,PEI_ATTRIBUTE21,PEI_ATTRIBUTE22\n" +
                        ",PEI_ATTRIBUTE23,PEI_ATTRIBUTE24,PEI_ATTRIBUTE25,PEI_ATTRIBUTE26,PEI_ATTRIBUTE27,PEI_ATTRIBUTE28,PEI_ATTRIBUTE29,PEI_ATTRIBUTE30,PEI_INFORMATION_CATEGORY,PEI_INFORMATION1\n" +
                        ",PEI_INFORMATION2,PEI_INFORMATION3,PEI_INFORMATION4,PEI_INFORMATION5,PEI_INFORMATION6,PEI_INFORMATION7,PEI_INFORMATION8,PEI_INFORMATION9,PEI_INFORMATION10,PEI_INFORMATION11\n" +
                        ",PEI_INFORMATION12,PEI_INFORMATION13,PEI_INFORMATION14,PEI_INFORMATION15,PEI_INFORMATION16,PEI_INFORMATION17,PEI_INFORMATION18,PEI_INFORMATION19,PEI_INFORMATION20,PEI_INFORMATION21\n" +
                        ",PEI_INFORMATION22,PEI_INFORMATION23,PEI_INFORMATION24,PEI_INFORMATION25,PEI_INFORMATION26,PEI_INFORMATION27,PEI_INFORMATION28,PEI_INFORMATION29,PEI_INFORMATION30,PEI_INFORMATION_NUMBER1,PEI_INFORMATION_NUMBER2,PEI_INFORMATION_NUMBER3,PEI_INFORMATION_NUMBER4,PEI_INFORMATION_NUMBER5,PEI_INFORMATION_NUMBER6,PEI_INFORMATION_NUMBER7,PEI_INFORMATION_NUMBER8\n" +
                        ",PEI_INFORMATION_NUMBER9,PEI_INFORMATION_NUMBER10,PEI_INFORMATION_NUMBER11,PEI_INFORMATION_NUMBER12,PEI_INFORMATION_NUMBER13,PEI_INFORMATION_NUMBER14,PEI_INFORMATION_NUMBER15\n" +
                        ",PEI_INFORMATION_NUMBER16,PEI_INFORMATION_NUMBER17,PEI_INFORMATION_NUMBER18,PEI_INFORMATION_NUMBER19,PEI_INFORMATION_NUMBER20,PEI_INFORMATION_DATE1,PEI_INFORMATION_DATE2\n" +
                        ",PEI_INFORMATION_DATE3,PEI_INFORMATION_DATE4,PEI_INFORMATION_DATE5,PEI_INFORMATION_DATE6,PEI_INFORMATION_DATE7,PEI_INFORMATION_DATE8,PEI_INFORMATION_DATE9,PEI_INFORMATION_DATE10\n" +
                        ",PEI_INFORMATION_DATE11,PEI_INFORMATION_DATE12,PEI_INFORMATION_DATE13,PEI_INFORMATION_DATE14,PEI_INFORMATION_DATE15,OBJECT_VERSION_NUMBER,LAST_UPDATE_LOGIN,EITLBL,PERSON_NAME)\n" +
                        "VALUES(\n" +
                        "?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?\n" +
                        ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                // String X = extraInformationBean.getEit().toString();
                ps = connection.prepareStatement(query);
                ps.setInt(1, extraInformationBean.getId());
                ps.setString(2, extraInformationBean.getCode());
                ps.setString(3, extraInformationBean.getEit());
                ps.setString(4, extraInformationBean.getStatus());
                ps.setString(5, extraInformationBean.getPerson_number());
                ps.setString(6, extraInformationBean.getPerson_id());
                //                ps.setString(7, extraInformationBean.getCreated_by());
                ps.setString(7, extraInformationBean.getCreation_date());
                ps.setString(8, extraInformationBean.getUpdated_by());
                ps.setString(9, extraInformationBean.getUpdated_date());
                ps.setString(10, extraInformationBean.getLine_manager());
                ps.setString(11, extraInformationBean.getManage_of_manager());
                ps.setString(12, extraInformationBean.getUrl());
                ps.setString(13, extraInformationBean.getEit_name());
                //New
                ps.setString(14,
                             extraInformationBean.getPERSON_EXTRA_INFO_ID());
                ps.setString(15,
                             extraInformationBean.getEFFECTIVE_START_DATE());
                ps.setString(16, extraInformationBean.getEFFECTIVE_END_DATE());
                ps.setString(17, extraInformationBean.getINFORMATION_TYPE());
                ps.setString(18,
                             extraInformationBean.getPEI_ATTRIBUTE_CATEGORY());
                //PEI_Attribute
                ps.setString(19, extraInformationBean.getPEI_ATTRIBUTE1());
                ps.setString(20, extraInformationBean.getPEI_ATTRIBUTE2());
                ps.setString(21, extraInformationBean.getPEI_ATTRIBUTE3());
                ps.setString(22, extraInformationBean.getPEI_ATTRIBUTE4());
                ps.setString(23, extraInformationBean.getPEI_ATTRIBUTE5());
                ps.setString(24, extraInformationBean.getPEI_ATTRIBUTE6());
                ps.setString(25, extraInformationBean.getPEI_ATTRIBUTE7());
                ps.setString(26, extraInformationBean.getPEI_ATTRIBUTE8());
                ps.setString(27, extraInformationBean.getPEI_ATTRIBUTE9());
                ps.setString(28, extraInformationBean.getPEI_ATTRIBUTE10());
                ps.setString(29, extraInformationBean.getPEI_ATTRIBUTE11());
                ps.setString(30, extraInformationBean.getPEI_ATTRIBUTE12());
                ps.setString(31, extraInformationBean.getPEI_ATTRIBUTE13());
                ps.setString(32, extraInformationBean.getPEI_ATTRIBUTE14());
                ps.setString(33, extraInformationBean.getPEI_ATTRIBUTE15());
                ps.setString(34, extraInformationBean.getPEI_ATTRIBUTE16());
                ps.setString(35, extraInformationBean.getPEI_ATTRIBUTE17());
                ps.setString(36, extraInformationBean.getPEI_ATTRIBUTE18());
                ps.setString(37, extraInformationBean.getPEI_ATTRIBUTE19());
                ps.setString(38, extraInformationBean.getPEI_ATTRIBUTE20());
                ps.setString(39, extraInformationBean.getPEI_ATTRIBUTE21());
                ps.setString(40, extraInformationBean.getPEI_ATTRIBUTE22());
                ps.setString(41, extraInformationBean.getPEI_ATTRIBUTE23());
                ps.setString(42, extraInformationBean.getPEI_ATTRIBUTE24());
                ps.setString(43, extraInformationBean.getPEI_ATTRIBUTE25());
                ps.setString(44, extraInformationBean.getPEI_ATTRIBUTE26());
                ps.setString(45, extraInformationBean.getPEI_ATTRIBUTE27());
                ps.setString(46, extraInformationBean.getPEI_ATTRIBUTE28());
                ps.setString(47, extraInformationBean.getPEI_ATTRIBUTE29());
                ps.setString(48, extraInformationBean.getPEI_ATTRIBUTE30());
                //
                ps.setString(49,
                             extraInformationBean.getPEI_INFORMATION_CATEGORY());
                //Pei_INFORMATION
                //extraInformationBean.setPEI_INFORMATION8(rh.convertToGregorian(extraInformationBean.getPEI_INFORMATION8()));
                ps.setString(50, extraInformationBean.getPEI_INFORMATION1());
                ps.setString(51, extraInformationBean.getPEI_INFORMATION2());
                ps.setString(52, extraInformationBean.getPEI_INFORMATION3());
                ps.setString(53, extraInformationBean.getPEI_INFORMATION4());
                ps.setString(54, extraInformationBean.getPEI_INFORMATION5());
                ps.setString(55, extraInformationBean.getPEI_INFORMATION6());
                ps.setString(56, extraInformationBean.getPEI_INFORMATION7());
                ps.setString(57, extraInformationBean.getPEI_INFORMATION8());
                ps.setString(58, extraInformationBean.getPEI_INFORMATION9());
                ps.setString(59, extraInformationBean.getPEI_INFORMATION10());
                ps.setString(60, extraInformationBean.getPEI_INFORMATION11());
                ps.setString(61, extraInformationBean.getPEI_INFORMATION12());
                ps.setString(62, extraInformationBean.getPEI_INFORMATION13());
                ps.setString(63, extraInformationBean.getPEI_INFORMATION14());
                ps.setString(64, extraInformationBean.getPEI_INFORMATION15());
                ps.setString(65, extraInformationBean.getPEI_INFORMATION16());
                ps.setString(66, extraInformationBean.getPEI_INFORMATION17());
                ps.setString(67, extraInformationBean.getPEI_INFORMATION18());
                ps.setString(68, extraInformationBean.getPEI_INFORMATION19());
                ps.setString(69, extraInformationBean.getPEI_INFORMATION20());
                ps.setString(70, extraInformationBean.getPEI_INFORMATION21());
                ps.setString(71, extraInformationBean.getPEI_INFORMATION22());
                ps.setString(72, extraInformationBean.getPEI_INFORMATION23());
                ps.setString(73, extraInformationBean.getPEI_INFORMATION24());
                ps.setString(74, extraInformationBean.getPEI_INFORMATION25());
                ps.setString(75, extraInformationBean.getPEI_INFORMATION26());
                ps.setString(76, extraInformationBean.getPEI_INFORMATION27());
                ps.setString(77, extraInformationBean.getPEI_INFORMATION28());
                ps.setString(78, extraInformationBean.getPEI_INFORMATION29());
                ps.setString(79, extraInformationBean.getPEI_INFORMATION30());
                //Pei_INFORMATION_NUMBER
                ps.setString(80,
                             extraInformationBean.getPEI_INFORMATION_NUMBER1());
                ps.setString(81,
                             extraInformationBean.getPEI_INFORMATION_NUMBER2());
                ps.setString(82,
                             extraInformationBean.getPEI_INFORMATION_NUMBER3());
                ps.setString(83,
                             extraInformationBean.getPEI_INFORMATION_NUMBER4());
                ps.setString(84,
                             extraInformationBean.getPEI_INFORMATION_NUMBER5());
                ps.setString(85,
                             extraInformationBean.getPEI_INFORMATION_NUMBER6());
                ps.setString(86,
                             extraInformationBean.getPEI_INFORMATION_NUMBER7());
                ps.setString(87,
                             extraInformationBean.getPEI_INFORMATION_NUMBER8());
                ps.setString(88,
                             extraInformationBean.getPEI_INFORMATION_NUMBER9());
                ps.setString(89,
                             extraInformationBean.getPEI_INFORMATION_NUMBER10());
                ps.setString(90,
                             extraInformationBean.getPEI_INFORMATION_NUMBER11());
                ps.setString(91,
                             extraInformationBean.getPEI_INFORMATION_NUMBER12());
                ps.setString(92,
                             extraInformationBean.getPEI_INFORMATION_NUMBER13());
                ps.setString(93,
                             extraInformationBean.getPEI_INFORMATION_NUMBER14());
                ps.setString(94,
                             extraInformationBean.getPEI_INFORMATION_NUMBER15());
                ps.setString(95,
                             extraInformationBean.getPEI_INFORMATION_NUMBER16());
                ps.setString(96,
                             extraInformationBean.getPEI_INFORMATION_NUMBER17());
                ps.setString(97,
                             extraInformationBean.getPEI_INFORMATION_NUMBER18());
                ps.setString(98,
                             extraInformationBean.getPEI_INFORMATION_NUMBER19());
                ps.setString(99,
                             extraInformationBean.getPEI_INFORMATION_NUMBER20());
                //Pei_INFORMATION_DATE
                //extraInformationBean.setPEI_INFORMATION_DATE1(rh.convertToGregorian(extraInformationBean.getPEI_INFORMATION_DATE1()));
                //extraInformationBean.setPEI_INFORMATION_DATE2(rh.convertToGregorian(extraInformationBean.getPEI_INFORMATION_DATE2()));
                ps.setString(100,
                             extraInformationBean.getPEI_INFORMATION_DATE1());
                ps.setString(101,
                             extraInformationBean.getPEI_INFORMATION_DATE2());
                ps.setString(102,
                             extraInformationBean.getPEI_INFORMATION_DATE3());
                ps.setString(103,
                             extraInformationBean.getPEI_INFORMATION_DATE4());
                ps.setString(104,
                             extraInformationBean.getPEI_INFORMATION_DATE5());
                ps.setString(105,
                             extraInformationBean.getPEI_INFORMATION_DATE6());
                ps.setString(106,
                             extraInformationBean.getPEI_INFORMATION_DATE7());
                ps.setString(107,
                             extraInformationBean.getPEI_INFORMATION_DATE8());
                ps.setString(108,
                             extraInformationBean.getPEI_INFORMATION_DATE9());
                ps.setString(109,
                             extraInformationBean.getPEI_INFORMATION_DATE10());
                ps.setString(110,
                             extraInformationBean.getPEI_INFORMATION_DATE11());
                ps.setString(111,
                             extraInformationBean.getPEI_INFORMATION_DATE12());
                ps.setString(112,
                             extraInformationBean.getPEI_INFORMATION_DATE13());
                ps.setString(113,
                             extraInformationBean.getPEI_INFORMATION_DATE14());
                ps.setString(114,
                             extraInformationBean.getPEI_INFORMATION_DATE15());

                ps.setString(115,
                             extraInformationBean.getOBJECT_VERSION_NUMBER());
                ps.setString(116, extraInformationBean.getLAST_UPDATE_LOGIN());
                ps.setString(117, extraInformationBean.getEitLbl());
                ps.setString(118, extraInformationBean.getPersonName());
                ps.executeUpdate();
                sfBean.setType(extraInformationBean.getCode());
                sfBean.setTypeTableName("XXX_PER_PEOPLE_EXTRA_INFO");
                sfBean.setTransactionId(Integer.toString(extraInformationBean.getId()));
                sfBean.setPresonId(extraInformationBean.getPerson_id());
                sfBean.setCreated_by(extraInformationBean.getCreated_by());
                sfBean.setPersonNumber(extraInformationBean.getPerson_number());
                selfServiceDao.insertIntoSelfService(sfBean);
                processApproval(extraInformationBean, conditionCode);

            }
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return extraInformationBean;
    }
    //******************End Insert Or Update PeopleExtraInfo********************

    //******************Start Get Max ID****************************************

    public synchronized int getMaxId(Connection connection) throws AppsProException {
        int id = 0;
        try {

            //  connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO_SEQ.NEXTVAL AS NEXT_ID FROM dual";
            AppsproConnection.LOG.info(query);
            ps = connection.prepareStatement(query);
            // ps.executeUpdate();
            rs = ps.executeQuery();
            while (rs.next()) {
                id = Integer.parseInt(rs.getString("NEXT_ID"));
            }
        } catch (Exception e) {
            //("Error: ");

           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            // closeResources(connection, ps, rs);
        }
        return id;
    }
    //****************** End Get Max ID ****************************************

    private void processApproval(PeopleExtraInformationBean extraInformationBean,
                                 String conditionCode) throws AppsProException,Exception {
        String s = extraInformationBean.getStatus();
        boolean isInsertApprovalList=false;
        if (!s.equals("DRAFT")) {
            approvalSetupList =
                    approvalSetup.getApprovalByEITCodeAndConditionCode(extraInformationBean.getCode(),
                                                                       conditionCode);
            alBean.setStepLeval("0");
            alBean.setServiceType(extraInformationBean.getCode());
            alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
            alBean.setWorkflowId("");
            alBean.setRolrType("EMP");
            alBean.setRoleId(extraInformationBean.getPerson_id());
            alBean.setPersonName(extraInformationBean.getPersonName());
            alBean.setResponseCode("SUBMIT");
            alBean.setNotificationType("FYI");

            approvalListDao.insertIntoApprovalList(alBean);
            // String emailBody= "You Have "+ extraInformationBean.getEit_name()+ " Notification For your kind Approval" ;
            String body = "";
            EmployeeBean empObj = new EmployeeBean();
            String subject = "";

            alBean.setRoleId(extraInformationBean.getLine_manager());
            alBean = null;
            ApprovalSetupBean ItratorBean;
            Iterator<ApprovalSetupBean> approvalSetupListIterator =
                approvalSetupList.iterator();
            while (approvalSetupListIterator.hasNext()) {
                alBean = new ApprovalListBean();
                ItratorBean = approvalSetupListIterator.next();
                if (ItratorBean.getApprovalType().equals("JOB_LEVEL")) {
                    alBean.setServiceType(extraInformationBean.getCode());
                    alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
                    alBean.setWorkflowId("");
                    alBean.setRolrType(ItratorBean.getApprovalType());
                    alBean.setNotificationType(ItratorBean.getNotificationType());
                    /**************** call job level report *******************/
                    JSONObject dataDS = null;
                    JSONArray g1 = null;
                    BIReportDAO BIReportDAO = new BIReportDAO();
                    JSONObject obj = new JSONObject();
                    obj.put("reportName", "GET_JOB_LEVEL_REPORT");
                    obj.put("P_PERSON_NUMBER",
                            extraInformationBean.getPerson_number());
                    JSONObject jsonObj =
                        new JSONObject(BIReportDAO.getBiReport(obj));
                    if (!jsonObj.getJSONObject("DATA_DS").isNull("G_1")) {
                        dataDS = jsonObj.getJSONObject("DATA_DS");
                        g1 = dataDS.getJSONArray("G_1");
                    }

                    for (int i = 0; i < g1.length(); i++) {
                        JSONObject data = (JSONObject)g1.get(i);
                        String stpLevel =
                            approvalListDao.getMaxStepLeval(String.valueOf(extraInformationBean.getId()),
                                                            String.valueOf(extraInformationBean.getCode()));
                        if (ItratorBean.getOperation().equals(">=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) >=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals(">")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) >
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("<")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) <
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("<=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) <=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                            }
                        } else if (ItratorBean.getOperation().equals("==") ||
                                   ItratorBean.getOperation().equals("===")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) ==
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("!=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) !=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        }
                        approvalListDao.insertIntoApprovalList(alBean);
                    }
                } else {
                    System.out.println("ID  : "+ extraInformationBean.getId() );
                    System.out.println("Code  : "+ extraInformationBean.getCode() );
                    String stpLevel =
                        approvalListDao.getMaxStepLeval(String.valueOf(extraInformationBean.getId()),
                                                        String.valueOf(extraInformationBean.getCode()));
                    System.out.println("stpLevel : "+ stpLevel );
                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                        ItratorBean.getApprovalOrder() :
                                        stpLevel);
                    alBean.setServiceType(extraInformationBean.getCode());
                    alBean.setTransActionId(Integer.toString(extraInformationBean.getId()));
                    alBean.setWorkflowId("");
                    alBean.setRolrType(ItratorBean.getApprovalType());

                    if (!alBean.getRolrType().equals("POSITION")) {
                        alBean.setRoleId("");

                        if (alBean.getRoleId().isEmpty() ||
                            alBean.getRoleId().equals(null) ||
                            alBean.getRoleId() == "") {
                            if (alBean.getRolrType().equals("EMP")) {
                                alBean.setRoleId(extraInformationBean.getPerson_id());
                            } else if (alBean.getRolrType().equals("LINE_MANAGER") &&
                                 extraInformationBean.getLine_manager()!=null &&
                                       !extraInformationBean.getLine_manager().isEmpty()) {
                                alBean.setRoleId(extraInformationBean.getLine_manager());
                                alBean.setPersonName(extraInformationBean.getManagerName());

                            } else if (alBean.getRolrType().equals("LINE_MANAGER+1") &&
                                       extraInformationBean.getManage_of_manager()!= null
                                       &&!extraInformationBean.getManage_of_manager().isEmpty()) {
                                alBean.setRoleId(extraInformationBean.getManage_of_manager());
                                alBean.setPersonName(extraInformationBean.getManagerOfManagerName());
                                //  EmployeeBean obj = new EmployeeBean();


                            }
                        }


                        if (alBean.getRolrType().equals("Special_Case")) {

                            //Caall Dynamic Report Validation -- arg
                            BIReportDAO bIReportDAO = new BIReportDAO();
                            JSONObject obj = new JSONObject();
                            obj.put("reportName", "DynamicValidationReport");
                            obj.put("str", ItratorBean.getSpecialCase());

                            JSONObject jsonObj =
                                new JSONObject(bIReportDAO.getBiReport(obj));

                            alBean.setRoleId(jsonObj.get("value").toString());
                        } else if (alBean.getRolrType().equals("ROLES")) {
                            alBean.setRoleId(ItratorBean.getRoleName());
                            //("Role Name : ");
                            //(ItratorBean.getRoleName());
                        } else if (alBean.getRolrType().equals("AOR")) {
                            /**************** get AOR report  *******************/

                            BIReportDAO bIReportDAO = new BIReportDAO();
                            JSONObject obj = new JSONObject();
                            obj.put("reportName", "GET_AOR_REP");
                            obj.put("P_PERSON_NUMBER",
                                    extraInformationBean.getPerson_number());

                            JSONObject data =
                                new JSONObject(bIReportDAO.getBiReport(obj));
                            if (!data.getJSONObject("DATA_DS").isNull("G_1")) {
                                JSONObject dataDS =
                                    data.getJSONObject("DATA_DS");
                                JSONObject g1 = dataDS.getJSONObject("G_1");
                                alBean.setRoleId(g1.get("PERSON_ID").toString());
                                alBean.setLineManagerName(g1.get("FULL_NAME").toString());
                            }
                        }

                    } else if (alBean.getRolrType().equals("POSITION")) {
                        alBean.setRoleId(ItratorBean.getRoleName());
                    }
                    alBean.setNotificationType(ItratorBean.getNotificationType());
                    if( alBean.getRoleId()!= null&&!alBean.getRoleId().isEmpty()){
                        
                        approvalListDao.insertIntoApprovalList(alBean);
                        isInsertApprovalList=true;
                    }
                   
                }
                alBean = null;
            }
            if (isInsertApprovalList) {
                wfn.setMsgTitle(extraInformationBean.getCode() + " Request");
                wfn.setMsgBody(extraInformationBean.getCode() + ":" +
                               extraInformationBean.getPerson_id());

                ApprovalListBean rolesBean =
                    approvalListDao.getRoles(Integer.toString(extraInformationBean.getId()),
                                             extraInformationBean.getCode());

                //1617 XXX_HR_MEDICAL_INSUR_FOR_HR


                //null LINE_MANAGER

                wfn.setReceiverType(rolesBean.getRolrType());
                wfn.setReceiverId(rolesBean.getRoleId());
                wfn.setType(rolesBean.getNotificationType());
                wfn.setRequestId(Integer.toString(extraInformationBean.getId()));
                wfn.setStatus("OPEN");
                wfn.setSelfType(extraInformationBean.getCode());
                wfn.setPersonName(extraInformationBean.getPersonName());
                wfn.setNationalIdentity(extraInformationBean.getNationalIdentity());
                wfn.setResponsePersonId(rolesBean.getRoleId());
                workflowNotificationDao.insertIntoWorkflow(wfn);
                 rolesBean =
                    approvalListDao.getRoles3(Integer.toString(extraInformationBean.getId()),
                                             extraInformationBean.getCode());
                 System.out.println("Role Type : " +rolesBean.getRolrType() );
                if (rolesBean.getRolrType().equals("ROLES")) {
                                    JSONObject dataDS=null;
                                    try {

                                        Map<String, String> paramMAp =
                                            new HashMap<String, String>();
                                        paramMAp.put("P_ROLEID", rolesBean.getRoleId());
                                        JSONObject respone =
                                            BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                                                    paramMAp);

                                        dataDS = respone.getJSONObject("DATA_DS");
                                        JSONArray g1 = dataDS.getJSONArray("G_1");
                                                             for (int i = 0; i < g1.length(); i++) {
                                                                 JSONObject data = (JSONObject)g1.get(i);
                                                                 
                                                                 body =
                                                                 "<html><head><style>\n" + 
                                                                 ".sig{\n" + 
                                                                 "font-size: 20px;\n" + 
                                                                 "font-weight: bold;\n" + 
                                                                 "    font-family: Tajawal;\n" + 
                                                                 "    color: #63A70A;\n" + 
                                                                 "}\n" + 
                                                                 ".mob{\n" + 
                                                                 "color:#63A70A;\n" + 
                                                                 "font-weight: bold;\n" + 
                                                                 "font-size: 13px;\n" + 
                                                                 "}\n" + 
                                                                 ".parColor{\n" + 
                                                                 "color:#1D4757;\n" + 
                                                                 "font-size:13px;\n" + 
                                                                 "}\n" + 
                                                                 "</style></head><body>" +
                                                                 "<p>Dear "+ data.getString("FULL_NAME")+",</p>" +
                                                                 "<p>Probation period evaluation for <strong>"+ extraInformationBean.getPersonName() +"</strong> has started.<br/>Manager name: "+ extraInformationBean.getManagerName() +".</p><p><strong>Regards,</strong></p>\n" +
                                                                 "</body></html>";
                                                                 
                                                                  subject =
                                                                     "Probation Request Initiated - HC / " + extraInformationBean.getPerson_number();
                                                                                             
                                                                 mailObj.SEND_MAIL(data.getString("EMAIL_ADDRESS"), subject, body);
                                                             }


                                    } catch (Exception e) {
                                        //("Error: ");
                                        JSONObject g1 = dataDS.getJSONObject("G_1");                        
                                        body =
                                        "<html><head><style>\n" + 
                                        ".sig{\n" + 
                                        "font-size: 20px;\n" + 
                                        "font-weight: bold;\n" + 
                                        "    font-family: Tajawal;\n" + 
                                        "    color: #63A70A;\n" + 
                                        "}\n" + 
                                        ".mob{\n" + 
                                        "color:#63A70A;\n" + 
                                        "font-weight: bold;\n" + 
                                        "font-size: 13px;\n" + 
                                        "}\n" + 
                                        ".parColor{\n" + 
                                        "color:#1D4757;\n" + 
                                        "font-size:13px;\n" + 
                                        "}\n" + 
                                        "</style></head><body>" +
                                        "<p>Dear "+ g1.getString("DISPLAY_NAME")+",</p>" +
                                        "<p>Probation period evaluation for <strong>"+ extraInformationBean.getPersonName() +"</strong> has started.</p><p><strong>Regards,</strong></p><p><strong>Human Capital</strong></p>\n" +
                                        "</body></html>";
                                        
                                         subject =
                                            "Probation Request Initiated - HC / " + extraInformationBean.getPerson_number();
                                                                    
                                        mailObj.SEND_MAIL(g1.getString("EMAIL_ADDRESS"), subject, body);
                                      // e.printStackTrace();
                                       //AppsproConnection.LOG.error("ERROR", e);
                                       // throw new AppsProException(e);
                                    }
                                }
            }
        }
    }    
}
