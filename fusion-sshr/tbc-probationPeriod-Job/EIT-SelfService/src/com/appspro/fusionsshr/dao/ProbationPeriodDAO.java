package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ProbationPeriodBean;
import com.appspro.mail.CredentialStoreClassMail;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import static common.restHelper.RestHelper.getSchema_Name;

public class ProbationPeriodDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    String clearanceMaxId = null;
    EmployeeDetails employeeDetails = new EmployeeDetails();
    CredentialStoreClassMail mailObj = new CredentialStoreClassMail();
    private String probationRequestID;

    public ProbationPeriodBean methodAddAndUpdateProbationRequest(ProbationPeriodBean bean,
                                                                  String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            if (transactionType.equals("ADD")) {
                if(!bean.getRequestName().equals("XXX_CONTRACT_ACTION")){
                query =
                        "insert into" + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO(PERSON_ID,PERSON_NUMBER,PERSON_NAME,PEI_ATTRIBUTE1,PEI_ATTRIBUTE2,PEI_ATTRIBUTE3,PEI_ATTRIBUTE4,PEI_ATTRIBUTE5,PEI_ATTRIBUTE6,PEI_ATTRIBUTE7,PEI_ATTRIBUTE8\n" +
                        ",PEI_ATTRIBUTE9,PEI_ATTRIBUTE10,PEI_ATTRIBUTE11,PEI_ATTRIBUTE12,PEI_ATTRIBUTE13,PEI_ATTRIBUTE14,PEI_ATTRIBUTE15,PEI_ATTRIBUTE16,PEI_ATTRIBUTE17,PEI_ATTRIBUTE18,PEI_ATTRIBUTE19\n" +
                        ",PEI_ATTRIBUTE20,PEI_ATTRIBUTE21,PEI_ATTRIBUTE22,PEI_ATTRIBUTE23,PEI_ATTRIBUTE24,EIT_CODE,STATUS,CREATED_BY,CREATION_DATE)" +
                    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE)";
                ps = connection.prepareStatement(query,new String[]{"ID"});
                ps.setString(1, bean.getEmployeeId());
                ps.setString(2, bean.getPersonNumber());
                ps.setString(3, bean.getEmployeeName());
                ps.setString(4, bean.getJob());
                ps.setString(5, bean.getGrade());
                ps.setString(6, bean.getHiringDate());
                ps.setString(7, bean.getFunctionEmp());
                ps.setString(8, bean.getEvaluationDate());
                ps.setString(9, bean.getJobKnowledge());
                ps.setString(10, bean.getQualityOfWork());
                ps.setString(11, bean.getQuantityOfWork());
                ps.setString(12, bean.getCommunication());
                ps.setString(13, bean.getCommitment());
                ps.setString(14, bean.getWorkHabit());
                ps.setString(15, bean.getInnovationInitiative());
                ps.setString(16, bean.getLearningAgility());
                ps.setString(17, bean.getWorkComment());
                ps.setString(18, bean.getAttitudeComment());
                ps.setString(19, bean.getPotentialComment());
                ps.setString(20, bean.getOvarAllComment());
                ps.setString(21, bean.getRetainEmployee());
                ps.setString(22, bean.getExtendProbation());
                ps.setString(23, bean.getTerminateEmployee());
                ps.setString(24, bean.getTransferEmployee());
                ps.setString(25, bean.getAdditionalComments());
                ps.setString(26, bean.getSeqRequest());
                ps.setString(27, bean.getRequestDate());
                ps.setString(28, "XXX_PROBATION_PERIOD");
                ps.setString(29, bean.getStatus());
                ps.setString(30, bean.getCreatedBy());
                    int affectedRows = ps.executeUpdate();
                    System.out.println(affectedRows);

                    String teamId = "";
                    ResultSet generatedKeys = ps.getGeneratedKeys();
                    if (generatedKeys.next()) {
                        teamId = generatedKeys.getString(1);
                        bean.setId(Integer.parseInt(teamId));
                    }
                    System.out.println(teamId);

                //ps.executeUpdate();
                }
                else {
                    
                    query =
                            "insert into  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO(PERSON_ID,\n" + 
                            "PERSON_NUMBER,\n" + 
                            "PERSON_NAME,\n" + 
                            "PEI_ATTRIBUTE1,\n" + 
                            "PEI_ATTRIBUTE2,\n" + 
                            "PEI_ATTRIBUTE3,\n" + 
                            "PEI_ATTRIBUTE4,\n" + 
                            "PEI_ATTRIBUTE5,\n" + 
                            "PEI_ATTRIBUTE6,\n" + 
                            "PEI_ATTRIBUTE7,\n" + 
                            "PEI_ATTRIBUTE8\n" + 
                            ",PEI_ATTRIBUTE9\n" + 
                            ",PEI_ATTRIBUTE10\n" + 
                            ",PEI_ATTRIBUTE11\n" +
                            ",PEI_ATTRIBUTE12\n" +
                            ",EIT_CODE\n" + 
                            ",STATUS\n" + 
                            ",CREATED_BY\n" + 
                            ",CREATION_DATE)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE)";
                    ps = connection.prepareStatement(query,new String[]{"ID"});
                    ps.setString(1, bean.getEmployeeId());
                    ps.setString(2, bean.getPersonNumber());
                    ps.setString(3, bean.getEmployeeName());
                    ps.setString(4, bean.getJob());
                    ps.setString(5, bean.getGrade());
                    ps.setString(6, bean.getDirectManager());
                    ps.setString(7, bean.getDepartment());
                    ps.setString(8, bean.getContractStartDate());
                    ps.setString(9, bean.getContractEndDate());
                    ps.setString(10, bean.getRenew());
                    ps.setString(11, bean.getExtend());
                    ps.setString(12, bean.getNonrenewal());
                    ps.setString(13, bean.getJustificationComment());
                    ps.setString(14, bean.getSeqRequest());
                    ps.setString(15, bean.getRequestDate());
                    ps.setString(16, bean.getRequestName());
                    ps.setString(17, bean.getStatus());
                    ps.setString(18, bean.getCreatedBy());
                    int affectedRows = ps.executeUpdate();

                    String teamId = "";
                    ResultSet generatedKeys = ps.getGeneratedKeys();
                    if (generatedKeys.next()) {
                        teamId = generatedKeys.getString(1);
                        bean.setId(Integer.parseInt(teamId));
                    }
                    System.out.println(teamId);

                    //ps.executeUpdate();
                    
                }
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public ArrayList<ProbationPeriodBean> getAllProbationRequest() {

        ArrayList<ProbationPeriodBean> probationList =
            new ArrayList<ProbationPeriodBean>();
        ProbationPeriodBean probationBean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO WHERE EIT_CODE = 'XXX_PROBATION_PERIOD' order by ID DESC";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                probationBean = new ProbationPeriodBean();
                probationBean.setId(rs.getInt("ID"));
                probationBean.setSeqRequest(rs.getString("PEI_ATTRIBUTE23"));
                probationBean.setRequestDate(rs.getString("PEI_ATTRIBUTE24"));
                probationBean.setEmployeeName(rs.getString("PERSON_NAME"));
                probationBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                probationBean.setEmployeeId(rs.getString("PERSON_ID"));
                probationBean.setJob(rs.getString("PEI_ATTRIBUTE1"));
                probationBean.setGrade(rs.getString("PEI_ATTRIBUTE2"));
                probationBean.setHiringDate(rs.getString("PEI_ATTRIBUTE3"));
                probationBean.setFunctionEmp(rs.getString("PEI_ATTRIBUTE4"));
                probationBean.setEvaluationDate(rs.getString("PEI_ATTRIBUTE5"));
                probationBean.setJobKnowledge(rs.getString("PEI_ATTRIBUTE6"));
                probationBean.setQualityOfWork(rs.getString("PEI_ATTRIBUTE7"));
                probationBean.setQuantityOfWork(rs.getString("PEI_ATTRIBUTE8"));
                probationBean.setCommunication(rs.getString("PEI_ATTRIBUTE9"));
                probationBean.setCommitment(rs.getString("PEI_ATTRIBUTE10"));
                probationBean.setWorkHabit(rs.getString("PEI_ATTRIBUTE11"));
                probationBean.setInnovationInitiative(rs.getString("PEI_ATTRIBUTE12"));
                probationBean.setLearningAgility(rs.getString("PEI_ATTRIBUTE13"));
                probationBean.setWorkComment(rs.getString("PEI_ATTRIBUTE14"));
                probationBean.setAttitudeComment(rs.getString("PEI_ATTRIBUTE15"));
                probationBean.setPotentialComment(rs.getString("PEI_ATTRIBUTE16"));
                probationBean.setOvarAllComment(rs.getString("PEI_ATTRIBUTE17"));
                probationBean.setRetainEmployee(rs.getString("PEI_ATTRIBUTE18"));
                probationBean.setExtendProbation(rs.getString("PEI_ATTRIBUTE19"));
                probationBean.setTerminateEmployee(rs.getString("PEI_ATTRIBUTE20"));
                probationBean.setTransferEmployee(rs.getString("PEI_ATTRIBUTE21"));
                probationBean.setAdditionalComments(rs.getString("PEI_ATTRIBUTE22"));
                probationBean.setStatus(rs.getString("STATUS"));
                probationList.add(probationBean);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return probationList;

    }

    public ProbationPeriodBean getOneProbationRequestById(String Id) {

        ProbationPeriodBean probationBean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, Id);
            rs = ps.executeQuery();
            while (rs.next()) {
                probationBean = new ProbationPeriodBean();
                probationBean.setId(rs.getInt("ID"));
                probationBean.setSeqRequest(rs.getString("PEI_ATTRIBUTE23"));
                probationBean.setRequestDate(rs.getString("PEI_ATTRIBUTE24"));
                probationBean.setEmployeeName(rs.getString("PERSON_NAME"));
                probationBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                probationBean.setEmployeeId(rs.getString("PERSON_ID"));
                probationBean.setJob(rs.getString("PEI_ATTRIBUTE1"));
                probationBean.setGrade(rs.getString("PEI_ATTRIBUTE2"));
                probationBean.setHiringDate(rs.getString("PEI_ATTRIBUTE3"));
                probationBean.setFunctionEmp(rs.getString("PEI_ATTRIBUTE4"));
                probationBean.setEvaluationDate(rs.getString("PEI_ATTRIBUTE5"));
                probationBean.setJobKnowledge(rs.getString("PEI_ATTRIBUTE6"));
                probationBean.setQualityOfWork(rs.getString("PEI_ATTRIBUTE7"));
                probationBean.setQuantityOfWork(rs.getString("PEI_ATTRIBUTE8"));
                probationBean.setCommunication(rs.getString("PEI_ATTRIBUTE9"));
                probationBean.setCommitment(rs.getString("PEI_ATTRIBUTE10"));
                probationBean.setWorkHabit(rs.getString("PEI_ATTRIBUTE11"));
                probationBean.setInnovationInitiative(rs.getString("PEI_ATTRIBUTE12"));
                probationBean.setLearningAgility(rs.getString("PEI_ATTRIBUTE13"));
                probationBean.setWorkComment(rs.getString("PEI_ATTRIBUTE14"));
                probationBean.setAttitudeComment(rs.getString("PEI_ATTRIBUTE15"));
                probationBean.setPotentialComment(rs.getString("PEI_ATTRIBUTE16"));
                probationBean.setOvarAllComment(rs.getString("PEI_ATTRIBUTE17"));
                probationBean.setRetainEmployee(rs.getString("PEI_ATTRIBUTE18"));
                probationBean.setExtendProbation(rs.getString("PEI_ATTRIBUTE19"));
                probationBean.setTerminateEmployee(rs.getString("PEI_ATTRIBUTE20"));
                probationBean.setTransferEmployee(rs.getString("PEI_ATTRIBUTE21"));
                probationBean.setAdditionalComments(rs.getString("PEI_ATTRIBUTE22"));
                probationBean.setStatus(rs.getString("STATUS"));
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return probationBean;

    }

    public String getMaxProbationRequest() {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT  MAX(ID) AS max_ID FROM  " + " " + getSchema_Name() +
                    ".XXX_PER_PEOPLE_EXTRA_INFO";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                probationRequestID = rs.getString("max_ID");
            }
        } catch (Exception e) {
            System.out.println("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return probationRequestID;
    }
    
    public ArrayList<ProbationPeriodBean> getAllContractRequest() {

        ArrayList<ProbationPeriodBean> probationList =
            new ArrayList<ProbationPeriodBean>();
        ProbationPeriodBean probationBean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO WHERE EIT_CODE = 'XXX_CONTRACT_ACTION' order by ID DESC";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                probationBean = new ProbationPeriodBean();
                probationBean.setId(rs.getInt("ID"));
                probationBean.setSeqRequest(rs.getString("PEI_ATTRIBUTE11"));
                probationBean.setRequestDate(rs.getString("PEI_ATTRIBUTE12"));
                probationBean.setEmployeeName(rs.getString("PERSON_NAME"));
                probationBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                probationBean.setEmployeeId(rs.getString("PERSON_ID"));
                probationBean.setJob(rs.getString("PEI_ATTRIBUTE1"));
                probationBean.setGrade(rs.getString("PEI_ATTRIBUTE2"));
                probationBean.setDirectManager(rs.getString("PEI_ATTRIBUTE3"));
                probationBean.setDepartment(rs.getString("PEI_ATTRIBUTE4"));
                probationBean.setContractStartDate(rs.getString("PEI_ATTRIBUTE5"));
                probationBean.setContractEndDate(rs.getString("PEI_ATTRIBUTE6"));
                probationBean.setRenew(rs.getString("PEI_ATTRIBUTE7"));
                probationBean.setExtend(rs.getString("PEI_ATTRIBUTE8"));
                probationBean.setNonrenewal(rs.getString("PEI_ATTRIBUTE9"));
                probationBean.setJustificationComment(rs.getString("PEI_ATTRIBUTE10"));
                probationBean.setStatus(rs.getString("STATUS"));
                probationList.add(probationBean);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return probationList;

    }
    
    public ProbationPeriodBean getOneContractRequestById(String Id) {

        ProbationPeriodBean probationBean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, Id);
            rs = ps.executeQuery();
            while (rs.next()) {
                probationBean = new ProbationPeriodBean();
                probationBean.setId(rs.getInt("ID"));
                probationBean.setSeqRequest(rs.getString("PEI_ATTRIBUTE11"));
                probationBean.setRequestDate(rs.getString("PEI_ATTRIBUTE12"));
                probationBean.setEmployeeName(rs.getString("PERSON_NAME"));
                probationBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                probationBean.setEmployeeId(rs.getString("PERSON_ID"));
                probationBean.setJob(rs.getString("PEI_ATTRIBUTE1"));
                probationBean.setGrade(rs.getString("PEI_ATTRIBUTE2"));
                probationBean.setDirectManager(rs.getString("PEI_ATTRIBUTE3"));
                probationBean.setDepartment(rs.getString("PEI_ATTRIBUTE4"));
                probationBean.setContractStartDate(rs.getString("PEI_ATTRIBUTE5"));
                probationBean.setContractEndDate(rs.getString("PEI_ATTRIBUTE6"));
                probationBean.setRenew(rs.getString("PEI_ATTRIBUTE7"));
                probationBean.setExtend(rs.getString("PEI_ATTRIBUTE8"));
                probationBean.setNonrenewal(rs.getString("PEI_ATTRIBUTE9"));
                probationBean.setJustificationComment(rs.getString("PEI_ATTRIBUTE10"));
                probationBean.setStatus(rs.getString("STATUS"));
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return probationBean;

    }
    
    public String getPPSequence() {

        String day = null;
        String month = null;
        String year = null;
        String date = "";
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT  (1000 + COUNT(*)) +1 value FROM   "+ " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO ";
            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {

                date = Integer.toString(rs.getInt("value"));

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return "PP"+date;

    }

}
