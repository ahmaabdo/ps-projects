/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;

import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import com.appspro.mail.CredentialStoreClassMail;

import com.hha.appsproExceptionn.AppsProException;

import com.oracle.vmm.client.provider.ovm22.ws.sps.ArrayList;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;


import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;
public class SelfServiceDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    EmployeeDetails employeeDetails = new EmployeeDetails();
    CredentialStoreClassMail mailObj = new  CredentialStoreClassMail();

    public SelfServiceBean insertIntoSelfService(SelfServiceBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "insert into  "+ " " + getSchema_Name() + ".XX_SELF_SERVICE (TYPE,TYPE_TABLE_NAME,TRANSACTION_ID,PERSON_ID,CREATED_BY,PERSON_NUMBER,CREATION_DATE) "
                    + "VALUES(?,?,?,?,?,?,sysdate)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getType());
            ps.setString(2, bean.getTypeTableName());
            ps.setString(3, bean.getTransactionId());
            ps.setString(4, bean.getPresonId());
            ps.setString(5, bean.getPresonId());
            // ps.setString(6, "sysdate");
            ps.setString(6, bean.getPersonNumber());
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public void updateSelfService(String RESPONSE_CODE, String ssType, String transactionId) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            //(transactionId);
            query = "UPDATE   "+ " " + getSchema_Name() + ".XX_SELF_SERVICE  "
                    + "SET STATUS = ?"
                    + "WHERE TYPE = ? AND TRANSACTION_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, RESPONSE_CODE);
            ps.setString(2, ssType);
            ps.setString(3, transactionId);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }
   
    public String approvalCycleProbattionRequest(SelfServiceBean bean)throws AppsProException {
            SelfServiceBean sfBean = new SelfServiceBean();
            java.util.ArrayList<ApprovalSetupBean> approvalSetupList =
                new java.util.ArrayList<ApprovalSetupBean>();
            ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
            ApprovalListBean alBean = new ApprovalListBean();
            ApprovalListDAO approvalListDao = new ApprovalListDAO();
            WorkflowNotificationDAO workflowNotificationDao =
                new WorkflowNotificationDAO();
            WorkFlowNotificationBean wfn = new WorkFlowNotificationBean();
            ProbationPeriodDAO probationRequest = new ProbationPeriodDAO();
            int requestID = Integer.parseInt(bean.getTransactionId());
            
            sfBean.setType(bean.getType());
            sfBean.setTypeTableName("XXX_PROBATION_PERIOD");
            sfBean.setTransactionId(Integer.toString(requestID));
            sfBean.setPresonId(bean.getPresonId());
            sfBean.setCreated_by(bean.getCreated_by());
            sfBean.setPersonNumber(bean.getPersonNumber());
            insertIntoSelfService(sfBean);
            String s = "TEST";
            if (!s.equals("DRAFT")) {
                approvalSetupList =
                        approvalSetup.getApprovalByEITCodeAndApprovalCode(bean.getType(),bean.getApprovalCode());
                alBean.setStepLeval("0");
                alBean.setServiceType(bean.getType());
                alBean.setTransActionId(Integer.toString(requestID));
                alBean.setWorkflowId("");
                alBean.setRolrType("EMP");
                alBean.setRoleId(bean.getPresonId());
                alBean.setResponseCode("SUBMIT");
                alBean.setNotificationType("FYI");

                approvalListDao.insertIntoApprovalList(alBean);
                String emailBody = "";
                EmployeeBean empObj = new EmployeeBean();
                String emailSupject = "";
                try {
                    empObj =
                            employeeDetails.getEmpDetailsByPersonId(bean.getPresonId(),
                                                                    "", "");
                    
                    emailBody =
                        rh.getEmailBody("FYI", empObj.getDisplayName(), bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
                                            empObj.getDisplayName());
                    emailSupject =
                            rh.getEmailSubject("FYI", bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
                                               empObj.getDisplayName());
                    System.out.println(emailSupject);
                    //mailObj.SEND_MAIL(empObj.getEmail(), emailSupject, emailBody);
                } catch (Exception e) {
                    //("Error: ");
                    e.printStackTrace();
                }
                alBean = null;
                ApprovalSetupBean ItratorBean;
                Iterator<ApprovalSetupBean> approvalSetupListIterator =
                    approvalSetupList.iterator();
                while (approvalSetupListIterator.hasNext()) {
                    alBean = new ApprovalListBean();
                    ItratorBean = approvalSetupListIterator.next();
                    alBean.setStepLeval(ItratorBean.getApprovalOrder());
                    alBean.setServiceType(bean.getType());
                    alBean.setTransActionId(Integer.toString(requestID));
                    alBean.setWorkflowId("");
                    alBean.setRolrType(ItratorBean.getApprovalType());
                    if (!alBean.getRolrType().equals("POSITION")) {
                        if (alBean.getRolrType().equals("EMP")) {
                            alBean.setRoleId(bean.getPresonId());
                        } else if (alBean.getRolrType().equals("LINE_MANAGER")) {
                            alBean.setRoleId(bean.getManagerId());
                        } else if (alBean.getRolrType().equals("LINE_MANAGER+1") ) {
                            alBean.setRoleId(bean.getManagerOfManager());
                        } else if (alBean.getRolrType().equals("Special_Case")) {

                            //Caall Dynamic Report Validation -- arg
                            String strSpecial = ItratorBean.getSpecialCase();
                            BIReportDAO bIReportDAO = new BIReportDAO();
                            JSONObject obj = new JSONObject();
                            obj.put("reportName", "DynamicValidationReport");
                            String str =
                                strSpecial.replaceAll(":personNumber", "'" + bean.getPersonNumber() +
                                                      "'");

                            System.out.println(str);
                            obj.put("str", str);

                            JSONObject jsonObj =
                                new JSONObject(bIReportDAO.getBiReport(obj));

                            //Value - return
                            System.out.println("VALUE ====== " +
                                               jsonObj.get("value").toString());
                            alBean.setRoleId(jsonObj.get("value").toString());
                        } else if (alBean.getRolrType().equals("ROLES")) {
                        alBean.setRoleId(ItratorBean.getRoleName());
                        }

                    } else if (alBean.getRolrType().equals("POSITION")) {
                        alBean.setRoleId(ItratorBean.getRoleName());
                    }

                    alBean.setNotificationType(ItratorBean.getNotificationType());

                    approvalListDao.insertIntoApprovalList(alBean);
                    alBean = null;
                }
            if (!approvalSetupList.isEmpty()) {
                wfn.setMsgTitle(bean.getType() + " Request");
                wfn.setMsgBody(bean.getType() + ":" + bean.getPresonId());
                
                ApprovalListBean rolesBean =
                    approvalListDao.getRoles(Integer.toString(requestID),
                                             bean.getType());

                wfn.setReceiverType(rolesBean.getRolrType());
                wfn.setReceiverId(rolesBean.getRoleId());
                wfn.setType(rolesBean.getNotificationType());
                wfn.setRequestId(Integer.toString(requestID));
                wfn.setStatus("OPEN");
                wfn.setSelfType(bean.getType());
                wfn.setPersonName(bean.getPersonName());
                //wfn.setNationalIdentity(extraInformationBean.getNationalIdentity());
                wfn.setResponsePersonId(rolesBean.getRoleId());
                workflowNotificationDao.insertIntoWorkflow(wfn);


                if (rolesBean.getRolrType().equals("LINE_MANAGER")) {
                    try {
                        empObj =
                                employeeDetails.getEmpDetailsByPersonId(bean.getManagerId(),
                                                                        "",
                                                                        "");
                        emailBody =
                                rh.getEmailBody(rolesBean.getNotificationType(),
                                                empObj.getDisplayName(),
                                                bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
                                                empObj.getDisplayName());
                        emailSupject =
                                rh.getEmailSubject(rolesBean.getNotificationType(),
                                                   bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
                                                   empObj.getDisplayName());
                        //mailObj.SEND_MAIL(empObj.getEmail(), emailSupject,
                                     //     emailBody);
                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                    }
                } else if (rolesBean.getRolrType().equals("LINE_MANAGER+1")) {
                    try {
                        empObj =
                                employeeDetails.getEmpDetailsByPersonId(bean.getManagerOfManager(),
                                                                        "",
                                                                        "");
                        emailBody =
                                rh.getEmailBody(rolesBean.getNotificationType(),
                                                empObj.getDisplayName(),
                                                bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
                                                empObj.getDisplayName());
                        emailSupject =
                                rh.getEmailSubject(rolesBean.getNotificationType(),
                                                   bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
                                                   empObj.getDisplayName());
    //                        mailObj.SEND_MAIL(empObj.getEmail(), emailSupject,
    //                                          emailBody);
                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                    }
                } else if (rolesBean.getRolrType().equals("ROLES")) {
                    try {
                        Map<String, String> paramMAp =
                            new HashMap<String, String>();
                        paramMAp.put("P_ROLEID", rolesBean.getRoleId());
                        JSONObject respone =
                            BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                                    paramMAp);

                        JSONObject dataDS = respone.getJSONObject("DATA_DS");
                        JSONArray g1 = dataDS.getJSONArray("G_1");
                        for (int i = 0; i < g1.length(); i++) {
                            JSONObject data = (JSONObject)g1.get(i);

    //                            mailObj.SEND_MAIL(data.getString("EMAIL_ADDRESS"),
    //                                              rh.getEmailSubject(rolesBean.getNotificationType(),
    //                                                                 bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
    //                                                                 bean.getPersonName()),
    //                                              rh.getEmailBody(rolesBean.getNotificationType(),
    //                                                              data.getString("FULL_NAME"),
    //                                                              bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
    //                                                              bean.getPersonName()));
                        }


                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                    }
                } else if (rolesBean.getRolrType().equals("POSITION")) {
                    try {

                        Map<String, String> paramMAp =
                            new HashMap<String, String>();
                        paramMAp.put("positionId", rolesBean.getRoleId());
                        JSONObject respone =
                            BIReportModel.runReport("EmailForPositionsReport",
                                                    paramMAp);
                        try {

                            JSONObject dataDS =
                                respone.getJSONObject("DATA_DS");
                            JSONArray g1 = dataDS.getJSONArray("G_1");
                            for (int i = 0; i < g1.length(); i++) {
                                JSONObject data = (JSONObject)g1.get(i);

    //                                mailObj.SEND_MAIL(data.getString("EMAIL_ADDRESS"),
    //                                                  rh.getEmailSubject(rolesBean.getNotificationType(),
    //                                                                     bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
    //                                                                     bean.getPersonName()),
    //                                                  rh.getEmailBody(rolesBean.getNotificationType(),
    //                                                                  data.getString("NAME"),
    //                                                                  bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
    //                                                                  bean.getPersonName()));
                            }
                        } catch (Exception e) {
                            JSONObject dataDS =
                                respone.getJSONObject("DATA_DS").getJSONObject("G_1");
    //                            mailObj.SEND_MAIL(dataDS.getString("EMAIL_ADDRESS"),
    //                                              rh.getEmailSubject(rolesBean.getNotificationType(),
    //                                                                 bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
    //                                                                 bean.getPersonName()),
    //                                              rh.getEmailBody(rolesBean.getNotificationType(),
    //                                                              dataDS.getString("NAME"),
    //                                                              bean.getType().equals("XXX_PROBATION_PERIOD") ? "Probation Period" : "Contract Action",
    //                                                              bean.getPersonName()));
                        }

                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                    }
                }

            } else {
                    updateSelfService("APPROVED", sfBean.getType(),
                                      sfBean.getTransactionId());
                }

            }
            return "";
        }

}
