
package com.appspro.fusionsshr.bean.rest;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder( { "Salutation", "FirstName", "MiddleName", "LastName",
                      "PreviousLastName", "NameSuffix", "DisplayName",
                      "PreferredName", "Honors", "CorrespondenceLanguage",
                      "PersonNumber", "WorkPhoneCountryCode",
                      "WorkPhoneAreaCode", "WorkPhoneNumber",
                      "WorkPhoneExtension", "WorkPhoneLegislationCode",
                      "WorkFaxCountryCode", "WorkFaxAreaCode", "WorkFaxNumber",
                      "WorkFaxExtension", "WorkFaxLegislationCode",
                      "WorkMobilePhoneCountryCode", "WorkMobilePhoneAreaCode",
                      "WorkMobilePhoneNumber", "WorkMobilePhoneExtension",
                      "WorkMobilePhoneLegislationCode", "HomePhoneCountryCode",
                      "HomePhoneAreaCode", "HomePhoneNumber",
                      "HomePhoneExtension", "HomePhoneLegislationCode",
                      "HomeFaxCountryCode", "HomeFaxAreaCode", "HomeFaxNumber",
                      "HomeFaxExtension", "WorkEmail",
                      "HomeFaxLegislationCode", "AddressLine1", "AddressLine2",
                      "AddressLine3", "City", "Region", "Region2", "Country",
                      "PostalCode", "DateOfBirth", "Ethnicity",
                      "ProjectedTerminationDate", "LegalEntityId", "HireDate",
                      "TerminationDate", "Gender", "MaritalStatus",
                      "NationalIdType", "NationalId", "NationalIdCountry",
                      "NationalIdExpirationDate", "PersonId",
                      "EffectiveStartDate", "UserName", "CitizenshipId",
                      "CitizenshipStatus", "CitizenshipLegislationCode",
                      "CitizenshipToDate", "Religion", "ReligionId",
                      "PassportIssueDate", "PassportNumber",
                      "PassportIssuingCountry", "PassportId",
                      "PassportExpirationDate", "LicenseNumber",
                      "DriversLicenseExpirationDate",
                      "DriversLicenseIssuingCountry", "DriversLicenseId",
                      "MilitaryVetStatus", "CreationDate", "LastUpdateDate",
                      "WorkerType", "personExtraInformation", "photo",
                      "assignments" })
public class Item {

    @JsonProperty("Salutation")
    private String salutation;
    @JsonProperty("FirstName")
    private String firstName;
    @JsonProperty("MiddleName")
    private Object middleName;
    @JsonProperty("LastName")
    private String lastName;
    @JsonProperty("PreviousLastName")
    private Object previousLastName;
    @JsonProperty("NameSuffix")
    private Object nameSuffix;
    @JsonProperty("DisplayName")
    private String displayName;
    @JsonProperty("PreferredName")
    private Object preferredName;
    @JsonProperty("Honors")
    private Object honors;
    @JsonProperty("CorrespondenceLanguage")
    private Object correspondenceLanguage;
    @JsonProperty("PersonNumber")
    private String personNumber;
    @JsonProperty("WorkPhoneCountryCode")
    private Object workPhoneCountryCode;
    @JsonProperty("WorkPhoneAreaCode")
    private Object workPhoneAreaCode;
    @JsonProperty("WorkPhoneNumber")
    private Object workPhoneNumber;
    @JsonProperty("WorkPhoneExtension")
    private Object workPhoneExtension;
    @JsonProperty("WorkPhoneLegislationCode")
    private Object workPhoneLegislationCode;
    @JsonProperty("WorkFaxCountryCode")
    private Object workFaxCountryCode;
    @JsonProperty("WorkFaxAreaCode")
    private Object workFaxAreaCode;
    @JsonProperty("WorkFaxNumber")
    private Object workFaxNumber;
    @JsonProperty("WorkFaxExtension")
    private Object workFaxExtension;
    @JsonProperty("WorkFaxLegislationCode")
    private Object workFaxLegislationCode;
    @JsonProperty("WorkMobilePhoneCountryCode")
    private Object workMobilePhoneCountryCode;
    @JsonProperty("WorkMobilePhoneAreaCode")
    private Object workMobilePhoneAreaCode;
    @JsonProperty("WorkMobilePhoneNumber")
    private Object workMobilePhoneNumber;
    @JsonProperty("WorkMobilePhoneExtension")
    private Object workMobilePhoneExtension;
    @JsonProperty("WorkMobilePhoneLegislationCode")
    private Object workMobilePhoneLegislationCode;
    @JsonProperty("HomePhoneCountryCode")
    private Object homePhoneCountryCode;
    @JsonProperty("HomePhoneAreaCode")
    private Object homePhoneAreaCode;
    @JsonProperty("HomePhoneNumber")
    private Object homePhoneNumber;
    @JsonProperty("HomePhoneExtension")
    private Object homePhoneExtension;
    @JsonProperty("HomePhoneLegislationCode")
    private Object homePhoneLegislationCode;
    @JsonProperty("HomeFaxCountryCode")
    private Object homeFaxCountryCode;
    @JsonProperty("HomeFaxAreaCode")
    private Object homeFaxAreaCode;
    @JsonProperty("HomeFaxNumber")
    private Object homeFaxNumber;
    @JsonProperty("HomeFaxExtension")
    private Object homeFaxExtension;
    @JsonProperty("WorkEmail")
    private Object workEmail;
    @JsonProperty("HomeFaxLegislationCode")
    private Object homeFaxLegislationCode;
    @JsonProperty("AddressLine1")
    private Object addressLine1;
    @JsonProperty("AddressLine2")
    private Object addressLine2;
    @JsonProperty("AddressLine3")
    private Object addressLine3;
    @JsonProperty("City")
    private String city;
    @JsonProperty("Region")
    private Object region;
    @JsonProperty("Region2")
    private Object region2;
    @JsonProperty("Country")
    private String country;
    @JsonProperty("PostalCode")
    private Object postalCode;
    @JsonProperty("DateOfBirth")
    private String dateOfBirth;
    @JsonProperty("Ethnicity")
    private Object ethnicity;
    @JsonProperty("ProjectedTerminationDate")
    private Object projectedTerminationDate;
    @JsonProperty("LegalEntityId")
    private Long legalEntityId;
    @JsonProperty("HireDate")
    private String hireDate;
    @JsonProperty("TerminationDate")
    private Object terminationDate;
    @JsonProperty("Gender")
    private Object gender;
    @JsonProperty("MaritalStatus")
    private Object maritalStatus;
    @JsonProperty("NationalIdType")
    private String nationalIdType;
    @JsonProperty("NationalId")
    private String nationalId;
    @JsonProperty("NationalIdCountry")
    private Object nationalIdCountry;
    @JsonProperty("NationalIdExpirationDate")
    private Object nationalIdExpirationDate;
    @JsonProperty("PersonId")
    private Long personId;
    @JsonProperty("EffectiveStartDate")
    private String effectiveStartDate;
    @JsonProperty("UserName")
    private String userName;
    @JsonProperty("CitizenshipId")
    private Object citizenshipId;
    @JsonProperty("CitizenshipStatus")
    private Object citizenshipStatus;
    @JsonProperty("CitizenshipLegislationCode")
    private Object citizenshipLegislationCode;
    @JsonProperty("CitizenshipToDate")
    private Object citizenshipToDate;
    @JsonProperty("Religion")
    private String religion;
    @JsonProperty("ReligionId")
    private Long religionId;
    @JsonProperty("PassportIssueDate")
    private Object passportIssueDate;
    @JsonProperty("PassportNumber")
    private Object passportNumber;
    @JsonProperty("PassportIssuingCountry")
    private Object passportIssuingCountry;
    @JsonProperty("PassportId")
    private Object passportId;
    @JsonProperty("PassportExpirationDate")
    private Object passportExpirationDate;
    @JsonProperty("LicenseNumber")
    private Object licenseNumber;
    @JsonProperty("DriversLicenseExpirationDate")
    private Object driversLicenseExpirationDate;
    @JsonProperty("DriversLicenseIssuingCountry")
    private Object driversLicenseIssuingCountry;
    @JsonProperty("DriversLicenseId")
    private Object driversLicenseId;
    @JsonProperty("MilitaryVetStatus")
    private String militaryVetStatus;
    @JsonProperty("CreationDate")
    private String creationDate;
    @JsonProperty("LastUpdateDate")
    private String lastUpdateDate;
    @JsonProperty("WorkerType")
    private String workerType;
    @JsonProperty("personExtraInformation")
    private List<PersonExtraInformation> personExtraInformation = null;
    @JsonProperty("photo")
    private List<Photo> photo = null;
    @JsonProperty("assignments")
    private List<Assignment> assignments = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties =
        new HashMap<String, Object>();

    @JsonProperty("Salutation")
    public String getSalutation() {
        return salutation;
    }

    @JsonProperty("Salutation")
    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    @JsonProperty("FirstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("FirstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("MiddleName")
    public Object getMiddleName() {
        return middleName;
    }

    @JsonProperty("MiddleName")
    public void setMiddleName(Object middleName) {
        this.middleName = middleName;
    }

    @JsonProperty("LastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("LastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("PreviousLastName")
    public Object getPreviousLastName() {
        return previousLastName;
    }

    @JsonProperty("PreviousLastName")
    public void setPreviousLastName(Object previousLastName) {
        this.previousLastName = previousLastName;
    }

    @JsonProperty("NameSuffix")
    public Object getNameSuffix() {
        return nameSuffix;
    }

    @JsonProperty("NameSuffix")
    public void setNameSuffix(Object nameSuffix) {
        this.nameSuffix = nameSuffix;
    }

    @JsonProperty("DisplayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("DisplayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("PreferredName")
    public Object getPreferredName() {
        return preferredName;
    }

    @JsonProperty("PreferredName")
    public void setPreferredName(Object preferredName) {
        this.preferredName = preferredName;
    }

    @JsonProperty("Honors")
    public Object getHonors() {
        return honors;
    }

    @JsonProperty("Honors")
    public void setHonors(Object honors) {
        this.honors = honors;
    }

    @JsonProperty("CorrespondenceLanguage")
    public Object getCorrespondenceLanguage() {
        return correspondenceLanguage;
    }

    @JsonProperty("CorrespondenceLanguage")
    public void setCorrespondenceLanguage(Object correspondenceLanguage) {
        this.correspondenceLanguage = correspondenceLanguage;
    }

    @JsonProperty("PersonNumber")
    public String getPersonNumber() {
        return personNumber;
    }

    @JsonProperty("PersonNumber")
    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    @JsonProperty("WorkPhoneCountryCode")
    public Object getWorkPhoneCountryCode() {
        return workPhoneCountryCode;
    }

    @JsonProperty("WorkPhoneCountryCode")
    public void setWorkPhoneCountryCode(Object workPhoneCountryCode) {
        this.workPhoneCountryCode = workPhoneCountryCode;
    }

    @JsonProperty("WorkPhoneAreaCode")
    public Object getWorkPhoneAreaCode() {
        return workPhoneAreaCode;
    }

    @JsonProperty("WorkPhoneAreaCode")
    public void setWorkPhoneAreaCode(Object workPhoneAreaCode) {
        this.workPhoneAreaCode = workPhoneAreaCode;
    }

    @JsonProperty("WorkPhoneNumber")
    public Object getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    @JsonProperty("WorkPhoneNumber")
    public void setWorkPhoneNumber(Object workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }

    @JsonProperty("WorkPhoneExtension")
    public Object getWorkPhoneExtension() {
        return workPhoneExtension;
    }

    @JsonProperty("WorkPhoneExtension")
    public void setWorkPhoneExtension(Object workPhoneExtension) {
        this.workPhoneExtension = workPhoneExtension;
    }

    @JsonProperty("WorkPhoneLegislationCode")
    public Object getWorkPhoneLegislationCode() {
        return workPhoneLegislationCode;
    }

    @JsonProperty("WorkPhoneLegislationCode")
    public void setWorkPhoneLegislationCode(Object workPhoneLegislationCode) {
        this.workPhoneLegislationCode = workPhoneLegislationCode;
    }

    @JsonProperty("WorkFaxCountryCode")
    public Object getWorkFaxCountryCode() {
        return workFaxCountryCode;
    }

    @JsonProperty("WorkFaxCountryCode")
    public void setWorkFaxCountryCode(Object workFaxCountryCode) {
        this.workFaxCountryCode = workFaxCountryCode;
    }

    @JsonProperty("WorkFaxAreaCode")
    public Object getWorkFaxAreaCode() {
        return workFaxAreaCode;
    }

    @JsonProperty("WorkFaxAreaCode")
    public void setWorkFaxAreaCode(Object workFaxAreaCode) {
        this.workFaxAreaCode = workFaxAreaCode;
    }

    @JsonProperty("WorkFaxNumber")
    public Object getWorkFaxNumber() {
        return workFaxNumber;
    }

    @JsonProperty("WorkFaxNumber")
    public void setWorkFaxNumber(Object workFaxNumber) {
        this.workFaxNumber = workFaxNumber;
    }

    @JsonProperty("WorkFaxExtension")
    public Object getWorkFaxExtension() {
        return workFaxExtension;
    }

    @JsonProperty("WorkFaxExtension")
    public void setWorkFaxExtension(Object workFaxExtension) {
        this.workFaxExtension = workFaxExtension;
    }

    @JsonProperty("WorkFaxLegislationCode")
    public Object getWorkFaxLegislationCode() {
        return workFaxLegislationCode;
    }

    @JsonProperty("WorkFaxLegislationCode")
    public void setWorkFaxLegislationCode(Object workFaxLegislationCode) {
        this.workFaxLegislationCode = workFaxLegislationCode;
    }

    @JsonProperty("WorkMobilePhoneCountryCode")
    public Object getWorkMobilePhoneCountryCode() {
        return workMobilePhoneCountryCode;
    }

    @JsonProperty("WorkMobilePhoneCountryCode")
    public void setWorkMobilePhoneCountryCode(Object workMobilePhoneCountryCode) {
        this.workMobilePhoneCountryCode = workMobilePhoneCountryCode;
    }

    @JsonProperty("WorkMobilePhoneAreaCode")
    public Object getWorkMobilePhoneAreaCode() {
        return workMobilePhoneAreaCode;
    }

    @JsonProperty("WorkMobilePhoneAreaCode")
    public void setWorkMobilePhoneAreaCode(Object workMobilePhoneAreaCode) {
        this.workMobilePhoneAreaCode = workMobilePhoneAreaCode;
    }

    @JsonProperty("WorkMobilePhoneNumber")
    public Object getWorkMobilePhoneNumber() {
        return workMobilePhoneNumber;
    }

    @JsonProperty("WorkMobilePhoneNumber")
    public void setWorkMobilePhoneNumber(Object workMobilePhoneNumber) {
        this.workMobilePhoneNumber = workMobilePhoneNumber;
    }

    @JsonProperty("WorkMobilePhoneExtension")
    public Object getWorkMobilePhoneExtension() {
        return workMobilePhoneExtension;
    }

    @JsonProperty("WorkMobilePhoneExtension")
    public void setWorkMobilePhoneExtension(Object workMobilePhoneExtension) {
        this.workMobilePhoneExtension = workMobilePhoneExtension;
    }

    @JsonProperty("WorkMobilePhoneLegislationCode")
    public Object getWorkMobilePhoneLegislationCode() {
        return workMobilePhoneLegislationCode;
    }

    @JsonProperty("WorkMobilePhoneLegislationCode")
    public void setWorkMobilePhoneLegislationCode(Object workMobilePhoneLegislationCode) {
        this.workMobilePhoneLegislationCode = workMobilePhoneLegislationCode;
    }

    @JsonProperty("HomePhoneCountryCode")
    public Object getHomePhoneCountryCode() {
        return homePhoneCountryCode;
    }

    @JsonProperty("HomePhoneCountryCode")
    public void setHomePhoneCountryCode(Object homePhoneCountryCode) {
        this.homePhoneCountryCode = homePhoneCountryCode;
    }

    @JsonProperty("HomePhoneAreaCode")
    public Object getHomePhoneAreaCode() {
        return homePhoneAreaCode;
    }

    @JsonProperty("HomePhoneAreaCode")
    public void setHomePhoneAreaCode(Object homePhoneAreaCode) {
        this.homePhoneAreaCode = homePhoneAreaCode;
    }

    @JsonProperty("HomePhoneNumber")
    public Object getHomePhoneNumber() {
        return homePhoneNumber;
    }

    @JsonProperty("HomePhoneNumber")
    public void setHomePhoneNumber(Object homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }

    @JsonProperty("HomePhoneExtension")
    public Object getHomePhoneExtension() {
        return homePhoneExtension;
    }

    @JsonProperty("HomePhoneExtension")
    public void setHomePhoneExtension(Object homePhoneExtension) {
        this.homePhoneExtension = homePhoneExtension;
    }

    @JsonProperty("HomePhoneLegislationCode")
    public Object getHomePhoneLegislationCode() {
        return homePhoneLegislationCode;
    }

    @JsonProperty("HomePhoneLegislationCode")
    public void setHomePhoneLegislationCode(Object homePhoneLegislationCode) {
        this.homePhoneLegislationCode = homePhoneLegislationCode;
    }

    @JsonProperty("HomeFaxCountryCode")
    public Object getHomeFaxCountryCode() {
        return homeFaxCountryCode;
    }

    @JsonProperty("HomeFaxCountryCode")
    public void setHomeFaxCountryCode(Object homeFaxCountryCode) {
        this.homeFaxCountryCode = homeFaxCountryCode;
    }

    @JsonProperty("HomeFaxAreaCode")
    public Object getHomeFaxAreaCode() {
        return homeFaxAreaCode;
    }

    @JsonProperty("HomeFaxAreaCode")
    public void setHomeFaxAreaCode(Object homeFaxAreaCode) {
        this.homeFaxAreaCode = homeFaxAreaCode;
    }

    @JsonProperty("HomeFaxNumber")
    public Object getHomeFaxNumber() {
        return homeFaxNumber;
    }

    @JsonProperty("HomeFaxNumber")
    public void setHomeFaxNumber(Object homeFaxNumber) {
        this.homeFaxNumber = homeFaxNumber;
    }

    @JsonProperty("HomeFaxExtension")
    public Object getHomeFaxExtension() {
        return homeFaxExtension;
    }

    @JsonProperty("HomeFaxExtension")
    public void setHomeFaxExtension(Object homeFaxExtension) {
        this.homeFaxExtension = homeFaxExtension;
    }

    @JsonProperty("WorkEmail")
    public Object getWorkEmail() {
        return workEmail;
    }

    @JsonProperty("WorkEmail")
    public void setWorkEmail(Object workEmail) {
        this.workEmail = workEmail;
    }

    @JsonProperty("HomeFaxLegislationCode")
    public Object getHomeFaxLegislationCode() {
        return homeFaxLegislationCode;
    }

    @JsonProperty("HomeFaxLegislationCode")
    public void setHomeFaxLegislationCode(Object homeFaxLegislationCode) {
        this.homeFaxLegislationCode = homeFaxLegislationCode;
    }

    @JsonProperty("AddressLine1")
    public Object getAddressLine1() {
        return addressLine1;
    }

    @JsonProperty("AddressLine1")
    public void setAddressLine1(Object addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @JsonProperty("AddressLine2")
    public Object getAddressLine2() {
        return addressLine2;
    }

    @JsonProperty("AddressLine2")
    public void setAddressLine2(Object addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @JsonProperty("AddressLine3")
    public Object getAddressLine3() {
        return addressLine3;
    }

    @JsonProperty("AddressLine3")
    public void setAddressLine3(Object addressLine3) {
        this.addressLine3 = addressLine3;
    }

    @JsonProperty("City")
    public String getCity() {
        return city;
    }

    @JsonProperty("City")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("Region")
    public Object getRegion() {
        return region;
    }

    @JsonProperty("Region")
    public void setRegion(Object region) {
        this.region = region;
    }

    @JsonProperty("Region2")
    public Object getRegion2() {
        return region2;
    }

    @JsonProperty("Region2")
    public void setRegion2(Object region2) {
        this.region2 = region2;
    }

    @JsonProperty("Country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("Country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("PostalCode")
    public Object getPostalCode() {
        return postalCode;
    }

    @JsonProperty("PostalCode")
    public void setPostalCode(Object postalCode) {
        this.postalCode = postalCode;
    }

    @JsonProperty("DateOfBirth")
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @JsonProperty("DateOfBirth")
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @JsonProperty("Ethnicity")
    public Object getEthnicity() {
        return ethnicity;
    }

    @JsonProperty("Ethnicity")
    public void setEthnicity(Object ethnicity) {
        this.ethnicity = ethnicity;
    }

    @JsonProperty("ProjectedTerminationDate")
    public Object getProjectedTerminationDate() {
        return projectedTerminationDate;
    }

    @JsonProperty("ProjectedTerminationDate")
    public void setProjectedTerminationDate(Object projectedTerminationDate) {
        this.projectedTerminationDate = projectedTerminationDate;
    }

    @JsonProperty("LegalEntityId")
    public Long getLegalEntityId() {
        return legalEntityId;
    }

    @JsonProperty("LegalEntityId")
    public void setLegalEntityId(Long legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    @JsonProperty("HireDate")
    public String getHireDate() {
        return hireDate;
    }

    @JsonProperty("HireDate")
    public void setHireDate(String hireDate) {
        this.hireDate = hireDate;
    }

    @JsonProperty("TerminationDate")
    public Object getTerminationDate() {
        return terminationDate;
    }

    @JsonProperty("TerminationDate")
    public void setTerminationDate(Object terminationDate) {
        this.terminationDate = terminationDate;
    }

    @JsonProperty("Gender")
    public Object getGender() {
        return gender;
    }

    @JsonProperty("Gender")
    public void setGender(Object gender) {
        this.gender = gender;
    }

    @JsonProperty("MaritalStatus")
    public Object getMaritalStatus() {
        return maritalStatus;
    }

    @JsonProperty("MaritalStatus")
    public void setMaritalStatus(Object maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @JsonProperty("NationalIdType")
    public String getNationalIdType() {
        return nationalIdType;
    }

    @JsonProperty("NationalIdType")
    public void setNationalIdType(String nationalIdType) {
        this.nationalIdType = nationalIdType;
    }

    @JsonProperty("NationalId")
    public String getNationalId() {
        return nationalId;
    }

    @JsonProperty("NationalId")
    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    @JsonProperty("NationalIdCountry")
    public Object getNationalIdCountry() {
        return nationalIdCountry;
    }

    @JsonProperty("NationalIdCountry")
    public void setNationalIdCountry(Object nationalIdCountry) {
        this.nationalIdCountry = nationalIdCountry;
    }

    @JsonProperty("NationalIdExpirationDate")
    public Object getNationalIdExpirationDate() {
        return nationalIdExpirationDate;
    }

    @JsonProperty("NationalIdExpirationDate")
    public void setNationalIdExpirationDate(Object nationalIdExpirationDate) {
        this.nationalIdExpirationDate = nationalIdExpirationDate;
    }

    @JsonProperty("PersonId")
    public Long getPersonId() {
        return personId;
    }

    @JsonProperty("PersonId")
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @JsonProperty("EffectiveStartDate")
    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    @JsonProperty("EffectiveStartDate")
    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    @JsonProperty("UserName")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("UserName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("CitizenshipId")
    public Object getCitizenshipId() {
        return citizenshipId;
    }

    @JsonProperty("CitizenshipId")
    public void setCitizenshipId(Object citizenshipId) {
        this.citizenshipId = citizenshipId;
    }

    @JsonProperty("CitizenshipStatus")
    public Object getCitizenshipStatus() {
        return citizenshipStatus;
    }

    @JsonProperty("CitizenshipStatus")
    public void setCitizenshipStatus(Object citizenshipStatus) {
        this.citizenshipStatus = citizenshipStatus;
    }

    @JsonProperty("CitizenshipLegislationCode")
    public Object getCitizenshipLegislationCode() {
        return citizenshipLegislationCode;
    }

    @JsonProperty("CitizenshipLegislationCode")
    public void setCitizenshipLegislationCode(Object citizenshipLegislationCode) {
        this.citizenshipLegislationCode = citizenshipLegislationCode;
    }

    @JsonProperty("CitizenshipToDate")
    public Object getCitizenshipToDate() {
        return citizenshipToDate;
    }

    @JsonProperty("CitizenshipToDate")
    public void setCitizenshipToDate(Object citizenshipToDate) {
        this.citizenshipToDate = citizenshipToDate;
    }

    @JsonProperty("Religion")
    public String getReligion() {
        return religion;
    }

    @JsonProperty("Religion")
    public void setReligion(String religion) {
        this.religion = religion;
    }

    @JsonProperty("ReligionId")
    public Long getReligionId() {
        return religionId;
    }

    @JsonProperty("ReligionId")
    public void setReligionId(Long religionId) {
        this.religionId = religionId;
    }

    @JsonProperty("PassportIssueDate")
    public Object getPassportIssueDate() {
        return passportIssueDate;
    }

    @JsonProperty("PassportIssueDate")
    public void setPassportIssueDate(Object passportIssueDate) {
        this.passportIssueDate = passportIssueDate;
    }

    @JsonProperty("PassportNumber")
    public Object getPassportNumber() {
        return passportNumber;
    }

    @JsonProperty("PassportNumber")
    public void setPassportNumber(Object passportNumber) {
        this.passportNumber = passportNumber;
    }

    @JsonProperty("PassportIssuingCountry")
    public Object getPassportIssuingCountry() {
        return passportIssuingCountry;
    }

    @JsonProperty("PassportIssuingCountry")
    public void setPassportIssuingCountry(Object passportIssuingCountry) {
        this.passportIssuingCountry = passportIssuingCountry;
    }

    @JsonProperty("PassportId")
    public Object getPassportId() {
        return passportId;
    }

    @JsonProperty("PassportId")
    public void setPassportId(Object passportId) {
        this.passportId = passportId;
    }

    @JsonProperty("PassportExpirationDate")
    public Object getPassportExpirationDate() {
        return passportExpirationDate;
    }

    @JsonProperty("PassportExpirationDate")
    public void setPassportExpirationDate(Object passportExpirationDate) {
        this.passportExpirationDate = passportExpirationDate;
    }

    @JsonProperty("LicenseNumber")
    public Object getLicenseNumber() {
        return licenseNumber;
    }

    @JsonProperty("LicenseNumber")
    public void setLicenseNumber(Object licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    @JsonProperty("DriversLicenseExpirationDate")
    public Object getDriversLicenseExpirationDate() {
        return driversLicenseExpirationDate;
    }

    @JsonProperty("DriversLicenseExpirationDate")
    public void setDriversLicenseExpirationDate(Object driversLicenseExpirationDate) {
        this.driversLicenseExpirationDate = driversLicenseExpirationDate;
    }

    @JsonProperty("DriversLicenseIssuingCountry")
    public Object getDriversLicenseIssuingCountry() {
        return driversLicenseIssuingCountry;
    }

    @JsonProperty("DriversLicenseIssuingCountry")
    public void setDriversLicenseIssuingCountry(Object driversLicenseIssuingCountry) {
        this.driversLicenseIssuingCountry = driversLicenseIssuingCountry;
    }

    @JsonProperty("DriversLicenseId")
    public Object getDriversLicenseId() {
        return driversLicenseId;
    }

    @JsonProperty("DriversLicenseId")
    public void setDriversLicenseId(Object driversLicenseId) {
        this.driversLicenseId = driversLicenseId;
    }

    @JsonProperty("MilitaryVetStatus")
    public String getMilitaryVetStatus() {
        return militaryVetStatus;
    }

    @JsonProperty("MilitaryVetStatus")
    public void setMilitaryVetStatus(String militaryVetStatus) {
        this.militaryVetStatus = militaryVetStatus;
    }

    @JsonProperty("CreationDate")
    public String getCreationDate() {
        return creationDate;
    }

    @JsonProperty("CreationDate")
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @JsonProperty("LastUpdateDate")
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    @JsonProperty("LastUpdateDate")
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @JsonProperty("WorkerType")
    public String getWorkerType() {
        return workerType;
    }

    @JsonProperty("WorkerType")
    public void setWorkerType(String workerType) {
        this.workerType = workerType;
    }

    @JsonProperty("personExtraInformation")
    public List<PersonExtraInformation> getPersonExtraInformation() {
        return personExtraInformation;
    }

    @JsonProperty("personExtraInformation")
    public void setPersonExtraInformation(List<PersonExtraInformation> personExtraInformation) {
        this.personExtraInformation = personExtraInformation;
    }

    @JsonProperty("photo")
    public List<Photo> getPhoto() {
        return photo;
    }

    @JsonProperty("photo")
    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }

    @JsonProperty("assignments")
    public List<Assignment> getAssignments() {
        return assignments;
    }

    @JsonProperty("assignments")
    public void setAssignments(List<Assignment> assignments) {
        this.assignments = assignments;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
