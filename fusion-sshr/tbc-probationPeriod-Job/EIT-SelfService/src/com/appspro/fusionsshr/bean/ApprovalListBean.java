/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author user
 */
public class ApprovalListBean {

    private String Id;
    private String stepLeval;
    private String serviceType;
    private String transActionId;
    private String workflowId;
    private String rolrType;
    private String roleId;
    private String requestDatt;
    private String responseCode;
    private String notificationType;
    private String note;
    private String responseDate;
    private String reasonforReassign;
    private String lineManagerName;
    private String personName;
    private String rejectReason;  
    private String employeeName;
    private String employeeNumber;
    


    public void setId(String Id) {
        this.Id = Id;
    }

    public void setStepLeval(String stepLeval) {
        this.stepLeval = stepLeval;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void setTransActionId(String transActionId) {
        this.transActionId = transActionId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public void setRolrType(String rolrType) {
        this.rolrType = rolrType;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public void setRequestDatt(String requestDatt) {
        this.requestDatt = requestDatt;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    public String getId() {
        return Id;
    }

    public String getStepLeval() {
        return stepLeval;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getTransActionId() {
        return transActionId;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public String getRolrType() {
        return rolrType;
    }

    public String getRoleId() {
        return roleId;
    }

    public String getRequestDatt() {
        return requestDatt;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public String getNote() {
        return note;
    }

    public String getResponseDate() {
        return responseDate;
    }

    @Override
    public String toString() {
        return "ApprovalListBean{" + "Id=" + Id + ", stepLeval=" + stepLeval + ", serviceType=" + serviceType + ", transActionId=" + transActionId + ", workflowId=" + workflowId + ", rolrType=" + rolrType + ", roleId=" + roleId + ", requestDatt=" + requestDatt + ", responseCode=" + responseCode + ", notificationType=" + notificationType + ", note=" + note + ", responseDate=" + responseDate + '}';
    }


    public void setReasonforReassign(String reasonforReassign) {
        this.reasonforReassign = reasonforReassign;
    }

    public String getReasonforReassign() {
        return reasonforReassign;
    }
    public void setLineManagerName(String lineManagerName) {
           this.lineManagerName = lineManagerName;
       }

       public String getLineManagerName() {
           return lineManagerName;
       }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getRejectReason() {
        return rejectReason;
    }


    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }
}
