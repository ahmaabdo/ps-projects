/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Shadi Mansi-PC
 */
 @JsonIgnoreProperties(ignoreUnknown = true)
public class PeopleExtraInformationBean {

    private int id;
    private String code;
    private String status;
    private String person_number;
    private String person_id;
    private String created_by;
    private String creation_date;
    private String updated_by;
    private String updated_date;
    private String line_manager;
    private String manage_of_manager;
    private String url;
    private String eit;
    private String eitLbl;
    private String eit_name;
    private int reference_num;
    private String PERSON_EXTRA_INFO_ID;
    private String EFFECTIVE_START_DATE;
    private String EFFECTIVE_END_DATE;
    private String INFORMATION_TYPE;
    private String PEI_ATTRIBUTE_CATEGORY;
    @JsonProperty("PEI_ATTRIBUTE1")
    private String PEI_ATTRIBUTE1;
    @JsonProperty("PEI_ATTRIBUTE2")
    private String PEI_ATTRIBUTE2;
    @JsonProperty("PEI_ATTRIBUTE3")
    private String PEI_ATTRIBUTE3;
    @JsonProperty("PEI_ATTRIBUTE4")
    private String PEI_ATTRIBUTE4;
    @JsonProperty("PEI_ATTRIBUTE5")
    private String PEI_ATTRIBUTE5;
    @JsonProperty("PEI_ATTRIBUTE6")
    private String PEI_ATTRIBUTE6;
    @JsonProperty("PEI_ATTRIBUTE7")
    private String PEI_ATTRIBUTE7;
    @JsonProperty("PEI_ATTRIBUTE8")
    private String PEI_ATTRIBUTE8;
    @JsonProperty("PEI_ATTRIBUTE9")
    private String PEI_ATTRIBUTE9;
    @JsonProperty("PEI_ATTRIBUTE10")
    private String PEI_ATTRIBUTE10;
    @JsonProperty("PEI_ATTRIBUTE11")
    private String PEI_ATTRIBUTE11;
    @JsonProperty("PEI_ATTRIBUTE12")
    private String PEI_ATTRIBUTE12;
    @JsonProperty("PEI_ATTRIBUTE13")
    private String PEI_ATTRIBUTE13;
    @JsonProperty("PEI_ATTRIBUTE14")
    private String PEI_ATTRIBUTE14;
    @JsonProperty("PEI_ATTRIBUTE15")
    private String PEI_ATTRIBUTE15;
    @JsonProperty("PEI_ATTRIBUTE16")
    private String PEI_ATTRIBUTE16;
    @JsonProperty("PEI_ATTRIBUTE17")
    private String PEI_ATTRIBUTE17;
    @JsonProperty("PEI_ATTRIBUTE18")
    private String PEI_ATTRIBUTE18;
    @JsonProperty("PEI_ATTRIBUTE19")
    private String PEI_ATTRIBUTE19;
    @JsonProperty("PEI_ATTRIBUTE20")
    private String PEI_ATTRIBUTE20;
    @JsonProperty("PEI_ATTRIBUTE21")
    private String PEI_ATTRIBUTE21;
    @JsonProperty("PEI_ATTRIBUTE22")
    private String PEI_ATTRIBUTE22;
    @JsonProperty("PEI_ATTRIBUTE23")
    private String PEI_ATTRIBUTE23;
    @JsonProperty("PEI_ATTRIBUTE24")
    private String PEI_ATTRIBUTE24;
    @JsonProperty("PEI_ATTRIBUTE25")
    private String PEI_ATTRIBUTE25;
    @JsonProperty("PEI_ATTRIBUTE26")
    private String PEI_ATTRIBUTE26;
    @JsonProperty("PEI_ATTRIBUTE27")
    private String PEI_ATTRIBUTE27;
    @JsonProperty("PEI_ATTRIBUTE28")
    private String PEI_ATTRIBUTE28;
    @JsonProperty("PEI_ATTRIBUTE29")
    private String PEI_ATTRIBUTE29;
    @JsonProperty("PEI_ATTRIBUTE30")
    private String PEI_ATTRIBUTE30;
    
    private String PEI_INFORMATION_CATEGORY;
    @JsonProperty("PEI_INFORMATION1")
    private String PEI_INFORMATION1;
    @JsonProperty("PEI_INFORMATION2")
    private String PEI_INFORMATION2;
    @JsonProperty("PEI_INFORMATION3")
    private String PEI_INFORMATION3;
    @JsonProperty("PEI_INFORMATION4")
    private String PEI_INFORMATION4;
    @JsonProperty("PEI_INFORMATION5")
    private String PEI_INFORMATION5;
    @JsonProperty("PEI_INFORMATION6") 
    private String PEI_INFORMATION6;
    @JsonProperty("PEI_INFORMATION7")
    private String PEI_INFORMATION7;
    @JsonProperty("PEI_INFORMATION8")
    private String PEI_INFORMATION8;
    @JsonProperty("PEI_INFORMATION9")
    private String PEI_INFORMATION9;
    @JsonProperty("PEI_INFORMATION10")
    private String PEI_INFORMATION10;
    @JsonProperty("PEI_INFORMATION11")
    private String PEI_INFORMATION11;
    @JsonProperty("PEI_INFORMATION12")
    private String PEI_INFORMATION12;
    @JsonProperty("PEI_INFORMATION13")
    private String PEI_INFORMATION13;
    @JsonProperty("PEI_INFORMATION14")
    private String PEI_INFORMATION14;
    @JsonProperty("PEI_INFORMATION15")
    private String PEI_INFORMATION15;
    @JsonProperty("PEI_INFORMATION16")
    private String PEI_INFORMATION16;
    @JsonProperty("PEI_INFORMATION17")
    private String PEI_INFORMATION17;
    @JsonProperty("PEI_INFORMATION18")
    private String PEI_INFORMATION18; 
    @JsonProperty("PEI_INFORMATION19")
    private String PEI_INFORMATION19;
    @JsonProperty("PEI_INFORMATION20")
    private String PEI_INFORMATION20;
    @JsonProperty("PEI_INFORMATION21")
    private String PEI_INFORMATION21;
    @JsonProperty("PEI_INFORMATION22")
    private String PEI_INFORMATION22;
    @JsonProperty("PEI_INFORMATION23")
    private String PEI_INFORMATION23;
    @JsonProperty("PEI_INFORMATION24")
    private String PEI_INFORMATION24;
    @JsonProperty("PEI_INFORMATION25")
    private String PEI_INFORMATION25;
    @JsonProperty("PEI_INFORMATION26")
    private String PEI_INFORMATION26;
    @JsonProperty("PEI_INFORMATION27")
    private String PEI_INFORMATION27;
    @JsonProperty("PEI_INFORMATION28")
    private String PEI_INFORMATION28;
    @JsonProperty("PEI_INFORMATION29")
    private String PEI_INFORMATION29;
    @JsonProperty("PEI_INFORMATION30")
    private String PEI_INFORMATION30;
    

   @JsonProperty("PEI_INFORMATION_NUMBER1")
    private String PEI_INFORMATION_NUMBER1;
    @JsonProperty("PEI_INFORMATION_NUMBER2")
    private String PEI_INFORMATION_NUMBER2;
    
    @JsonProperty("PEI_INFORMATION_NUMBER3")
    private String PEI_INFORMATION_NUMBER3;
    
    @JsonProperty("PEI_INFORMATION_NUMBER4")
    private String PEI_INFORMATION_NUMBER4;
    
    @JsonProperty("PEI_INFORMATION_NUMBER5")
    private String PEI_INFORMATION_NUMBER5;
    
    @JsonProperty("PEI_INFORMATION_NUMBER6")
    private String PEI_INFORMATION_NUMBER6;
    
    @JsonProperty("PEI_INFORMATION_NUMBER7")
    private String PEI_INFORMATION_NUMBER7;
    
    @JsonProperty("PEI_INFORMATION_NUMBER8")
    private String PEI_INFORMATION_NUMBER8;
    
    @JsonProperty("PEI_INFORMATION_NUMBER9")
    private String PEI_INFORMATION_NUMBER9;
    
    @JsonProperty("PEI_INFORMATION_NUMBER10")
    private String PEI_INFORMATION_NUMBER10;
    
    @JsonProperty("PEI_INFORMATION_NUMBER11")
    private String PEI_INFORMATION_NUMBER11;
    
    @JsonProperty("PEI_INFORMATION_NUMBER12")
    private String PEI_INFORMATION_NUMBER12;
    
    @JsonProperty("PEI_INFORMATION_NUMBER13")
    private String PEI_INFORMATION_NUMBER13;
    
    @JsonProperty("PEI_INFORMATION_NUMBER14")
    private String PEI_INFORMATION_NUMBER14;
    
    @JsonProperty("PEI_INFORMATION_NUMBER15")
    private String PEI_INFORMATION_NUMBER15;
    
    @JsonProperty("PEI_INFORMATION_NUMBER16")
    private String PEI_INFORMATION_NUMBER16;
    
    @JsonProperty("PEI_INFORMATION_NUMBER17")
    private String PEI_INFORMATION_NUMBER17;
    
    @JsonProperty("PEI_INFORMATION_NUMBER18")
    private String PEI_INFORMATION_NUMBER18;
    
    @JsonProperty("PEI_INFORMATION_NUMBER19")
    private String PEI_INFORMATION_NUMBER19;
    
    @JsonProperty("PEI_INFORMATION_NUMBER20")
    private String PEI_INFORMATION_NUMBER20;
    
    @JsonProperty("PEI_INFORMATION_DATE1")
    private String PEI_INFORMATION_DATE1;
    @JsonProperty("PEI_INFORMATION_DATE2")
    private String PEI_INFORMATION_DATE2;
    
    @JsonProperty("PEI_INFORMATION_DATE3")
    private String PEI_INFORMATION_DATE3;
    
    @JsonProperty("PEI_INFORMATION_DATE4")
    private String PEI_INFORMATION_DATE4;
    
    @JsonProperty("PEI_INFORMATION_DATE5")
    private String PEI_INFORMATION_DATE5;
    @JsonProperty("PEI_INFORMATION_DATE6")
    private String PEI_INFORMATION_DATE6;
    
    @JsonProperty("PEI_INFORMATION_DATE7")
    private String PEI_INFORMATION_DATE7;
    
    @JsonProperty("PEI_INFORMATION_DATE8")
    private String PEI_INFORMATION_DATE8;
    
    @JsonProperty("PEI_INFORMATION_DATE9")
    private String PEI_INFORMATION_DATE9;
    
    @JsonProperty("PEI_INFORMATION_DATE10")
    private String PEI_INFORMATION_DATE10;
    
    @JsonProperty("PEI_INFORMATION_DATE11")
    private String PEI_INFORMATION_DATE11;
    
    @JsonProperty("PEI_INFORMATION_DATE12")
    private String PEI_INFORMATION_DATE12;
    
    @JsonProperty("PEI_INFORMATION_DATE13")
    private String PEI_INFORMATION_DATE13;
    @JsonProperty("PEI_INFORMATION_DATE14")
    private String PEI_INFORMATION_DATE14;
    
    @JsonProperty("PEI_INFORMATION_DATE15")
    private String PEI_INFORMATION_DATE15;
    
    @JsonProperty("OBJECT_VERSION_NUMBER")
    private String OBJECT_VERSION_NUMBER;
    @JsonProperty("LAST_UPDATE_LOGIN")
    private String LAST_UPDATE_LOGIN;
   
    private String personName;
   
    private String nationalIdentity;
   
    private String managerName;
   
    private String managerOfManagerName;
   
    private String rejectReason;
   
    
    
    
    

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPerson_number() {
        return person_number;
    }

    public void setPerson_number(String person_number) {
        this.person_number = person_number;
    }

    public String getPerson_id() {
        return person_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public String getLine_manager() {
        return line_manager;
    }

    public void setLine_manager(String line_manager) {
        this.line_manager = line_manager;
    }

    public String getManage_of_manager() {
        return manage_of_manager;
    }

    public void setManage_of_manager(String manage_of_manager) {
        this.manage_of_manager = manage_of_manager;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEit() {
        return eit;
    }

    public void setEit(String eit) {
        this.eit = eit;
    }

    public String getEit_name() {
        return eit_name;
    }

    public void setEit_name(String eit_name) {
        this.eit_name = eit_name;
    }


    public void setReference_num(int reference_num) {
        this.reference_num = reference_num;
    }

    public int getReference_num() {
        return reference_num;
    }

    public void setPERSON_EXTRA_INFO_ID(String PERSON_EXTRA_INFO_ID) {
        this.PERSON_EXTRA_INFO_ID = PERSON_EXTRA_INFO_ID;
    }

    public String getPERSON_EXTRA_INFO_ID() {
        return PERSON_EXTRA_INFO_ID;
    }

    public void setEFFECTIVE_START_DATE(String EFFECTIVE_START_DATE) {
        this.EFFECTIVE_START_DATE = EFFECTIVE_START_DATE;
    }

    public String getEFFECTIVE_START_DATE() {
        return EFFECTIVE_START_DATE;
    }

    public void setEFFECTIVE_END_DATE(String EFFECTIVE_END_DATE) {
        this.EFFECTIVE_END_DATE = EFFECTIVE_END_DATE;
    }

    public String getEFFECTIVE_END_DATE() {
        return EFFECTIVE_END_DATE;
    }

    public void setINFORMATION_TYPE(String INFORMATION_TYPE) {
        this.INFORMATION_TYPE = INFORMATION_TYPE;
    }

    public String getINFORMATION_TYPE() {
        return INFORMATION_TYPE;
    }

    public void setPEI_ATTRIBUTE_CATEGORY(String PEI_ATTRIBUTE_CATEGORY) {
        this.PEI_ATTRIBUTE_CATEGORY = PEI_ATTRIBUTE_CATEGORY;
    }

    public String getPEI_ATTRIBUTE_CATEGORY() {
        return PEI_ATTRIBUTE_CATEGORY;
    }

    public void setPEI_ATTRIBUTE1(String PEI_ATTRIBUTE1) {
        this.PEI_ATTRIBUTE1 = PEI_ATTRIBUTE1;
    }

    public String getPEI_ATTRIBUTE1() {
        return PEI_ATTRIBUTE1;
    }

    public void setPEI_ATTRIBUTE2(String PEI_ATTRIBUTE2) {
        this.PEI_ATTRIBUTE2 = PEI_ATTRIBUTE2;
    }

    public String getPEI_ATTRIBUTE2() {
        return PEI_ATTRIBUTE2;
    }

    public void setPEI_ATTRIBUTE3(String PEI_ATTRIBUTE3) {
        this.PEI_ATTRIBUTE3 = PEI_ATTRIBUTE3;
    }

    public String getPEI_ATTRIBUTE3() {
        return PEI_ATTRIBUTE3;
    }

    public void setPEI_ATTRIBUTE4(String PEI_ATTRIBUTE4) {
        this.PEI_ATTRIBUTE4 = PEI_ATTRIBUTE4;
    }

    public String getPEI_ATTRIBUTE4() {
        return PEI_ATTRIBUTE4;
    }

    public void setPEI_ATTRIBUTE5(String PEI_ATTRIBUTE5) {
        this.PEI_ATTRIBUTE5 = PEI_ATTRIBUTE5;
    }

    public String getPEI_ATTRIBUTE5() {
        return PEI_ATTRIBUTE5;
    }

    public void setPEI_ATTRIBUTE6(String PEI_ATTRIBUTE6) {
        this.PEI_ATTRIBUTE6 = PEI_ATTRIBUTE6;
    }

    public String getPEI_ATTRIBUTE6() {
        return PEI_ATTRIBUTE6;
    }

    public void setPEI_ATTRIBUTE7(String PEI_ATTRIBUTE7) {
        this.PEI_ATTRIBUTE7 = PEI_ATTRIBUTE7;
    }

    public String getPEI_ATTRIBUTE7() {
        return PEI_ATTRIBUTE7;
    }

    public void setPEI_ATTRIBUTE8(String PEI_ATTRIBUTE8) {
        this.PEI_ATTRIBUTE8 = PEI_ATTRIBUTE8;
    }

    public String getPEI_ATTRIBUTE8() {
        return PEI_ATTRIBUTE8;
    }

    public void setPEI_ATTRIBUTE9(String PEI_ATTRIBUTE9) {
        this.PEI_ATTRIBUTE9 = PEI_ATTRIBUTE9;
    }

    public String getPEI_ATTRIBUTE9() {
        return PEI_ATTRIBUTE9;
    }

    public void setPEI_ATTRIBUTE10(String PEI_ATTRIBUTE10) {
        this.PEI_ATTRIBUTE10 = PEI_ATTRIBUTE10;
    }

    public String getPEI_ATTRIBUTE10() {
        return PEI_ATTRIBUTE10;
    }

    public void setPEI_ATTRIBUTE11(String PEI_ATTRIBUTE11) {
        this.PEI_ATTRIBUTE11 = PEI_ATTRIBUTE11;
    }

    public String getPEI_ATTRIBUTE11() {
        return PEI_ATTRIBUTE11;
    }

    public void setPEI_ATTRIBUTE12(String PEI_ATTRIBUTE12) {
        this.PEI_ATTRIBUTE12 = PEI_ATTRIBUTE12;
    }

    public String getPEI_ATTRIBUTE12() {
        return PEI_ATTRIBUTE12;
    }

    public void setPEI_ATTRIBUTE13(String PEI_ATTRIBUTE13) {
        this.PEI_ATTRIBUTE13 = PEI_ATTRIBUTE13;
    }

    public String getPEI_ATTRIBUTE13() {
        return PEI_ATTRIBUTE13;
    }

    public void setPEI_ATTRIBUTE14(String PEI_ATTRIBUTE14) {
        this.PEI_ATTRIBUTE14 = PEI_ATTRIBUTE14;
    }

    public String getPEI_ATTRIBUTE14() {
        return PEI_ATTRIBUTE14;
    }

    public void setPEI_ATTRIBUTE15(String PEI_ATTRIBUTE15) {
        this.PEI_ATTRIBUTE15 = PEI_ATTRIBUTE15;
    }

    public String getPEI_ATTRIBUTE15() {
        return PEI_ATTRIBUTE15;
    }

    public void setPEI_ATTRIBUTE16(String PEI_ATTRIBUTE16) {
        this.PEI_ATTRIBUTE16 = PEI_ATTRIBUTE16;
    }

    public String getPEI_ATTRIBUTE16() {
        return PEI_ATTRIBUTE16;
    }

    public void setPEI_ATTRIBUTE17(String PEI_ATTRIBUTE17) {
        this.PEI_ATTRIBUTE17 = PEI_ATTRIBUTE17;
    }

    public String getPEI_ATTRIBUTE17() {
        return PEI_ATTRIBUTE17;
    }

    public void setPEI_ATTRIBUTE18(String PEI_ATTRIBUTE18) {
        this.PEI_ATTRIBUTE18 = PEI_ATTRIBUTE18;
    }

    public String getPEI_ATTRIBUTE18() {
        return PEI_ATTRIBUTE18;
    }

    public void setPEI_ATTRIBUTE19(String PEI_ATTRIBUTE19) {
        this.PEI_ATTRIBUTE19 = PEI_ATTRIBUTE19;
    }

    public String getPEI_ATTRIBUTE19() {
        return PEI_ATTRIBUTE19;
    }

    public void setPEI_ATTRIBUTE20(String PEI_ATTRIBUTE20) {
        this.PEI_ATTRIBUTE20 = PEI_ATTRIBUTE20;
    }

    public String getPEI_ATTRIBUTE20() {
        return PEI_ATTRIBUTE20;
    }

    public void setPEI_ATTRIBUTE21(String PEI_ATTRIBUTE21) {
        this.PEI_ATTRIBUTE21 = PEI_ATTRIBUTE21;
    }

    public String getPEI_ATTRIBUTE21() {
        return PEI_ATTRIBUTE21;
    }

    public void setPEI_ATTRIBUTE22(String PEI_ATTRIBUTE22) {
        this.PEI_ATTRIBUTE22 = PEI_ATTRIBUTE22;
    }

    public String getPEI_ATTRIBUTE22() {
        return PEI_ATTRIBUTE22;
    }

    public void setPEI_ATTRIBUTE23(String PEI_ATTRIBUTE23) {
        this.PEI_ATTRIBUTE23 = PEI_ATTRIBUTE23;
    }

    public String getPEI_ATTRIBUTE23() {
        return PEI_ATTRIBUTE23;
    }

    public void setPEI_ATTRIBUTE24(String PEI_ATTRIBUTE24) {
        this.PEI_ATTRIBUTE24 = PEI_ATTRIBUTE24;
    }

    public String getPEI_ATTRIBUTE24() {
        return PEI_ATTRIBUTE24;
    }

    public void setPEI_ATTRIBUTE25(String PEI_ATTRIBUTE25) {
        this.PEI_ATTRIBUTE25 = PEI_ATTRIBUTE25;
    }

    public String getPEI_ATTRIBUTE25() {
        return PEI_ATTRIBUTE25;
    }

    public void setPEI_ATTRIBUTE26(String PEI_ATTRIBUTE26) {
        this.PEI_ATTRIBUTE26 = PEI_ATTRIBUTE26;
    }

    public String getPEI_ATTRIBUTE26() {
        return PEI_ATTRIBUTE26;
    }

    public void setPEI_ATTRIBUTE27(String PEI_ATTRIBUTE27) {
        this.PEI_ATTRIBUTE27 = PEI_ATTRIBUTE27;
    }

    public String getPEI_ATTRIBUTE27() {
        return PEI_ATTRIBUTE27;
    }

    public void setPEI_ATTRIBUTE28(String PEI_ATTRIBUTE28) {
        this.PEI_ATTRIBUTE28 = PEI_ATTRIBUTE28;
    }

    public String getPEI_ATTRIBUTE28() {
        return PEI_ATTRIBUTE28;
    }

    public void setPEI_ATTRIBUTE29(String PEI_ATTRIBUTE29) {
        this.PEI_ATTRIBUTE29 = PEI_ATTRIBUTE29;
    }

    public String getPEI_ATTRIBUTE29() {
        return PEI_ATTRIBUTE29;
    }

    public void setPEI_ATTRIBUTE30(String PEI_ATTRIBUTE30) {
        this.PEI_ATTRIBUTE30 = PEI_ATTRIBUTE30;
    }

    public String getPEI_ATTRIBUTE30() {
        return PEI_ATTRIBUTE30;
    }

    public void setPEI_INFORMATION_CATEGORY(String PEI_INFORMATION_CATEGORY) {
        this.PEI_INFORMATION_CATEGORY = PEI_INFORMATION_CATEGORY;
    }

    public String getPEI_INFORMATION_CATEGORY() {
        return PEI_INFORMATION_CATEGORY;
    }

    public void setPEI_INFORMATION1(String PEI_INFORMATION1) {
        this.PEI_INFORMATION1 = PEI_INFORMATION1;
    }

    public String getPEI_INFORMATION1() {
        return PEI_INFORMATION1;
    }

    public void setPEI_INFORMATION2(String PEI_INFORMATION2) {
        this.PEI_INFORMATION2 = PEI_INFORMATION2;
    }

    public String getPEI_INFORMATION2() {
        return PEI_INFORMATION2;
    }

    public void setPEI_INFORMATION3(String PEI_INFORMATION3) {
        this.PEI_INFORMATION3 = PEI_INFORMATION3;
    }

    public String getPEI_INFORMATION3() {
        return PEI_INFORMATION3;
    }

    public void setPEI_INFORMATION4(String PEI_INFORMATION4) {
        this.PEI_INFORMATION4 = PEI_INFORMATION4;
    }

    public String getPEI_INFORMATION4() {
        return PEI_INFORMATION4;
    }

    public void setPEI_INFORMATION5(String PEI_INFORMATION5) {
        this.PEI_INFORMATION5 = PEI_INFORMATION5;
    }

    public String getPEI_INFORMATION5() {
        return PEI_INFORMATION5;
    }

    public void setPEI_INFORMATION6(String PEI_INFORMATION6) {
        this.PEI_INFORMATION6 = PEI_INFORMATION6;
    }

    public String getPEI_INFORMATION6() {
        return PEI_INFORMATION6;
    }

    public void setPEI_INFORMATION7(String PEI_INFORMATION7) {
        this.PEI_INFORMATION7 = PEI_INFORMATION7;
    }

    public String getPEI_INFORMATION7() {
        return PEI_INFORMATION7;
    }

    public void setPEI_INFORMATION8(String PEI_INFORMATION8) {
        this.PEI_INFORMATION8 = PEI_INFORMATION8;
    }

    public String getPEI_INFORMATION8() {
        return PEI_INFORMATION8;
    }

    public void setPEI_INFORMATION9(String PEI_INFORMATION9) {
        this.PEI_INFORMATION9 = PEI_INFORMATION9;
    }

    public String getPEI_INFORMATION9() {
        return PEI_INFORMATION9;
    }

    public void setPEI_INFORMATION10(String PEI_INFORMATION10) {
        this.PEI_INFORMATION10 = PEI_INFORMATION10;
    }

    public String getPEI_INFORMATION10() {
        return PEI_INFORMATION10;
    }

    public void setPEI_INFORMATION11(String PEI_INFORMATION11) {
        this.PEI_INFORMATION11 = PEI_INFORMATION11;
    }

    public String getPEI_INFORMATION11() {
        return PEI_INFORMATION11;
    }

    public void setPEI_INFORMATION12(String PEI_INFORMATION12) {
        this.PEI_INFORMATION12 = PEI_INFORMATION12;
    }

    public String getPEI_INFORMATION12() {
        return PEI_INFORMATION12;
    }

    public void setPEI_INFORMATION13(String PEI_INFORMATION13) {
        this.PEI_INFORMATION13 = PEI_INFORMATION13;
    }

    public String getPEI_INFORMATION13() {
        return PEI_INFORMATION13;
    }

    public void setPEI_INFORMATION14(String PEI_INFORMATION14) {
        this.PEI_INFORMATION14 = PEI_INFORMATION14;
    }

    public String getPEI_INFORMATION14() {
        return PEI_INFORMATION14;
    }

    public void setPEI_INFORMATION15(String PEI_INFORMATION15) {
        this.PEI_INFORMATION15 = PEI_INFORMATION15;
    }

    public String getPEI_INFORMATION15() {
        return PEI_INFORMATION15;
    }

    public void setPEI_INFORMATION16(String PEI_INFORMATION16) {
        this.PEI_INFORMATION16 = PEI_INFORMATION16;
    }

    public String getPEI_INFORMATION16() {
        return PEI_INFORMATION16;
    }

    public void setPEI_INFORMATION17(String PEI_INFORMATION17) {
        this.PEI_INFORMATION17 = PEI_INFORMATION17;
    }

    public String getPEI_INFORMATION17() {
        return PEI_INFORMATION17;
    }

    public void setPEI_INFORMATION18(String PEI_INFORMATION18) {
        this.PEI_INFORMATION18 = PEI_INFORMATION18;
    }

    public String getPEI_INFORMATION18() {
        return PEI_INFORMATION18;
    }

    public void setPEI_INFORMATION19(String PEI_INFORMATION19) {
        this.PEI_INFORMATION19 = PEI_INFORMATION19;
    }

    public String getPEI_INFORMATION19() {
        return PEI_INFORMATION19;
    }

    public void setPEI_INFORMATION20(String PEI_INFORMATION20) {
        this.PEI_INFORMATION20 = PEI_INFORMATION20;
    }

    public String getPEI_INFORMATION20() {
        return PEI_INFORMATION20;
    }

    public void setPEI_INFORMATION21(String PEI_INFORMATION21) {
        this.PEI_INFORMATION21 = PEI_INFORMATION21;
    }

    public String getPEI_INFORMATION21() {
        return PEI_INFORMATION21;
    }

    public void setPEI_INFORMATION22(String PEI_INFORMATION22) {
        this.PEI_INFORMATION22 = PEI_INFORMATION22;
    }

    public String getPEI_INFORMATION22() {
        return PEI_INFORMATION22;
    }

    public void setPEI_INFORMATION23(String PEI_INFORMATION23) {
        this.PEI_INFORMATION23 = PEI_INFORMATION23;
    }

    public String getPEI_INFORMATION23() {
        return PEI_INFORMATION23;
    }

    public void setPEI_INFORMATION24(String PEI_INFORMATION24) {
        this.PEI_INFORMATION24 = PEI_INFORMATION24;
    }

    public String getPEI_INFORMATION24() {
        return PEI_INFORMATION24;
    }

    public void setPEI_INFORMATION25(String PEI_INFORMATION25) {
        this.PEI_INFORMATION25 = PEI_INFORMATION25;
    }

    public String getPEI_INFORMATION25() {
        return PEI_INFORMATION25;
    }

    public void setPEI_INFORMATION26(String PEI_INFORMATION26) {
        this.PEI_INFORMATION26 = PEI_INFORMATION26;
    }

    public String getPEI_INFORMATION26() {
        return PEI_INFORMATION26;
    }

    public void setPEI_INFORMATION27(String PEI_INFORMATION27) {
        this.PEI_INFORMATION27 = PEI_INFORMATION27;
    }

    public String getPEI_INFORMATION27() {
        return PEI_INFORMATION27;
    }

    public void setPEI_INFORMATION28(String PEI_INFORMATION28) {
        this.PEI_INFORMATION28 = PEI_INFORMATION28;
    }

    public String getPEI_INFORMATION28() {
        return PEI_INFORMATION28;
    }

    public void setPEI_INFORMATION29(String PEI_INFORMATION29) {
        this.PEI_INFORMATION29 = PEI_INFORMATION29;
    }

    public String getPEI_INFORMATION29() {
        return PEI_INFORMATION29;
    }

    public void setPEI_INFORMATION30(String PEI_INFORMATION30) {
        this.PEI_INFORMATION30 = PEI_INFORMATION30;
    }

    public String getPEI_INFORMATION30() {
        return PEI_INFORMATION30;
    }

    public void setPEI_INFORMATION_NUMBER1(String PEI_INFORMATION_NUMBER1) {
        this.PEI_INFORMATION_NUMBER1 = PEI_INFORMATION_NUMBER1;
    }

    public String getPEI_INFORMATION_NUMBER1() {
        return PEI_INFORMATION_NUMBER1;
    }

    public void setPEI_INFORMATION_NUMBER2(String PEI_INFORMATION_NUMBER2) {
        this.PEI_INFORMATION_NUMBER2 = PEI_INFORMATION_NUMBER2;
    }

    public String getPEI_INFORMATION_NUMBER2() {
        return PEI_INFORMATION_NUMBER2;
    }

    public void setPEI_INFORMATION_NUMBER3(String PEI_INFORMATION_NUMBER3) {
        this.PEI_INFORMATION_NUMBER3 = PEI_INFORMATION_NUMBER3;
    }

    public String getPEI_INFORMATION_NUMBER3() {
        return PEI_INFORMATION_NUMBER3;
    }

    public void setPEI_INFORMATION_NUMBER4(String PEI_INFORMATION_NUMBER4) {
        this.PEI_INFORMATION_NUMBER4 = PEI_INFORMATION_NUMBER4;
    }

    public String getPEI_INFORMATION_NUMBER4() {
        return PEI_INFORMATION_NUMBER4;
    }

    public void setPEI_INFORMATION_NUMBER5(String PEI_INFORMATION_NUMBER5) {
        this.PEI_INFORMATION_NUMBER5 = PEI_INFORMATION_NUMBER5;
    }

    public String getPEI_INFORMATION_NUMBER5() {
        return PEI_INFORMATION_NUMBER5;
    }

    public void setPEI_INFORMATION_NUMBER6(String PEI_INFORMATION_NUMBER6) {
        this.PEI_INFORMATION_NUMBER6 = PEI_INFORMATION_NUMBER6;
    }

    public String getPEI_INFORMATION_NUMBER6() {
        return PEI_INFORMATION_NUMBER6;
    }

    public void setPEI_INFORMATION_NUMBER7(String PEI_INFORMATION_NUMBER7) {
        this.PEI_INFORMATION_NUMBER7 = PEI_INFORMATION_NUMBER7;
    }

    public String getPEI_INFORMATION_NUMBER7() {
        return PEI_INFORMATION_NUMBER7;
    }

    public void setPEI_INFORMATION_NUMBER8(String PEI_INFORMATION_NUMBER8) {
        this.PEI_INFORMATION_NUMBER8 = PEI_INFORMATION_NUMBER8;
    }

    public String getPEI_INFORMATION_NUMBER8() {
        return PEI_INFORMATION_NUMBER8;
    }

    public void setPEI_INFORMATION_NUMBER9(String PEI_INFORMATION_NUMBER9) {
        this.PEI_INFORMATION_NUMBER9 = PEI_INFORMATION_NUMBER9;
    }

    public String getPEI_INFORMATION_NUMBER9() {
        return PEI_INFORMATION_NUMBER9;
    }

    public void setPEI_INFORMATION_NUMBER10(String PEI_INFORMATION_NUMBER10) {
        this.PEI_INFORMATION_NUMBER10 = PEI_INFORMATION_NUMBER10;
    }

    public String getPEI_INFORMATION_NUMBER10() {
        return PEI_INFORMATION_NUMBER10;
    }

    public void setPEI_INFORMATION_NUMBER11(String PEI_INFORMATION_NUMBER11) {
        this.PEI_INFORMATION_NUMBER11 = PEI_INFORMATION_NUMBER11;
    }

    public String getPEI_INFORMATION_NUMBER11() {
        return PEI_INFORMATION_NUMBER11;
    }

    public void setPEI_INFORMATION_NUMBER12(String PEI_INFORMATION_NUMBER12) {
        this.PEI_INFORMATION_NUMBER12 = PEI_INFORMATION_NUMBER12;
    }

    public String getPEI_INFORMATION_NUMBER12() {
        return PEI_INFORMATION_NUMBER12;
    }

    public void setPEI_INFORMATION_NUMBER13(String PEI_INFORMATION_NUMBER13) {
        this.PEI_INFORMATION_NUMBER13 = PEI_INFORMATION_NUMBER13;
    }

    public String getPEI_INFORMATION_NUMBER13() {
        return PEI_INFORMATION_NUMBER13;
    }

    public void setPEI_INFORMATION_NUMBER14(String PEI_INFORMATION_NUMBER14) {
        this.PEI_INFORMATION_NUMBER14 = PEI_INFORMATION_NUMBER14;
    }

    public String getPEI_INFORMATION_NUMBER14() {
        return PEI_INFORMATION_NUMBER14;
    }

    public void setPEI_INFORMATION_NUMBER15(String PEI_INFORMATION_NUMBER15) {
        this.PEI_INFORMATION_NUMBER15 = PEI_INFORMATION_NUMBER15;
    }

    public String getPEI_INFORMATION_NUMBER15() {
        return PEI_INFORMATION_NUMBER15;
    }

    public void setPEI_INFORMATION_NUMBER16(String PEI_INFORMATION_NUMBER16) {
        this.PEI_INFORMATION_NUMBER16 = PEI_INFORMATION_NUMBER16;
    }

    public String getPEI_INFORMATION_NUMBER16() {
        return PEI_INFORMATION_NUMBER16;
    }

    public void setPEI_INFORMATION_NUMBER17(String PEI_INFORMATION_NUMBER17) {
        this.PEI_INFORMATION_NUMBER17 = PEI_INFORMATION_NUMBER17;
    }

    public String getPEI_INFORMATION_NUMBER17() {
        return PEI_INFORMATION_NUMBER17;
    }

    public void setPEI_INFORMATION_NUMBER18(String PEI_INFORMATION_NUMBER18) {
        this.PEI_INFORMATION_NUMBER18 = PEI_INFORMATION_NUMBER18;
    }

    public String getPEI_INFORMATION_NUMBER18() {
        return PEI_INFORMATION_NUMBER18;
    }

    public void setPEI_INFORMATION_NUMBER19(String PEI_INFORMATION_NUMBER19) {
        this.PEI_INFORMATION_NUMBER19 = PEI_INFORMATION_NUMBER19;
    }

    public String getPEI_INFORMATION_NUMBER19() {
        return PEI_INFORMATION_NUMBER19;
    }

    public void setPEI_INFORMATION_NUMBER20(String PEI_INFORMATION_NUMBER20) {
        this.PEI_INFORMATION_NUMBER20 = PEI_INFORMATION_NUMBER20;
    }

    public String getPEI_INFORMATION_NUMBER20() {
        return PEI_INFORMATION_NUMBER20;
    }

    public void setPEI_INFORMATION_DATE1(String PEI_INFORMATION_DATE1) {
        this.PEI_INFORMATION_DATE1 = PEI_INFORMATION_DATE1;
    }

    public String getPEI_INFORMATION_DATE1() {
        return PEI_INFORMATION_DATE1;
    }

    public void setPEI_INFORMATION_DATE2(String PEI_INFORMATION_DATE2) {
        this.PEI_INFORMATION_DATE2 = PEI_INFORMATION_DATE2;
    }

    public String getPEI_INFORMATION_DATE2() {
        return PEI_INFORMATION_DATE2;
    }

    public void setPEI_INFORMATION_DATE3(String PEI_INFORMATION_DATE3) {
        this.PEI_INFORMATION_DATE3 = PEI_INFORMATION_DATE3;
    }

    public String getPEI_INFORMATION_DATE3() {
        return PEI_INFORMATION_DATE3;
    }

    public void setPEI_INFORMATION_DATE4(String PEI_INFORMATION_DATE4) {
        this.PEI_INFORMATION_DATE4 = PEI_INFORMATION_DATE4;
    }

    public String getPEI_INFORMATION_DATE4() {
        return PEI_INFORMATION_DATE4;
    }

    public void setPEI_INFORMATION_DATE5(String PEI_INFORMATION_DATE5) {
        this.PEI_INFORMATION_DATE5 = PEI_INFORMATION_DATE5;
    }

    public String getPEI_INFORMATION_DATE5() {
        return PEI_INFORMATION_DATE5;
    }

    public void setPEI_INFORMATION_DATE6(String PEI_INFORMATION_DATE6) {
        this.PEI_INFORMATION_DATE6 = PEI_INFORMATION_DATE6;
    }

    public String getPEI_INFORMATION_DATE6() {
        return PEI_INFORMATION_DATE6;
    }

    public void setPEI_INFORMATION_DATE7(String PEI_INFORMATION_DATE7) {
        this.PEI_INFORMATION_DATE7 = PEI_INFORMATION_DATE7;
    }

    public String getPEI_INFORMATION_DATE7() {
        return PEI_INFORMATION_DATE7;
    }

    public void setPEI_INFORMATION_DATE8(String PEI_INFORMATION_DATE8) {
        this.PEI_INFORMATION_DATE8 = PEI_INFORMATION_DATE8;
    }

    public String getPEI_INFORMATION_DATE8() {
        return PEI_INFORMATION_DATE8;
    }

    public void setPEI_INFORMATION_DATE9(String PEI_INFORMATION_DATE9) {
        this.PEI_INFORMATION_DATE9 = PEI_INFORMATION_DATE9;
    }

    public String getPEI_INFORMATION_DATE9() {
        return PEI_INFORMATION_DATE9;
    }

    public void setPEI_INFORMATION_DATE10(String PEI_INFORMATION_DATE10) {
        this.PEI_INFORMATION_DATE10 = PEI_INFORMATION_DATE10;
    }

    public String getPEI_INFORMATION_DATE10() {
        return PEI_INFORMATION_DATE10;
    }

    public void setPEI_INFORMATION_DATE11(String PEI_INFORMATION_DATE11) {
        this.PEI_INFORMATION_DATE11 = PEI_INFORMATION_DATE11;
    }

    public String getPEI_INFORMATION_DATE11() {
        return PEI_INFORMATION_DATE11;
    }

    public void setPEI_INFORMATION_DATE12(String PEI_INFORMATION_DATE12) {
        this.PEI_INFORMATION_DATE12 = PEI_INFORMATION_DATE12;
    }

    public String getPEI_INFORMATION_DATE12() {
        return PEI_INFORMATION_DATE12;
    }

    public void setPEI_INFORMATION_DATE13(String PEI_INFORMATION_DATE13) {
        this.PEI_INFORMATION_DATE13 = PEI_INFORMATION_DATE13;
    }

    public String getPEI_INFORMATION_DATE13() {
        return PEI_INFORMATION_DATE13;
    }

    public void setPEI_INFORMATION_DATE14(String PEI_INFORMATION_DATE14) {
        this.PEI_INFORMATION_DATE14 = PEI_INFORMATION_DATE14;
    }

    public String getPEI_INFORMATION_DATE14() {
        return PEI_INFORMATION_DATE14;
    }

    public void setPEI_INFORMATION_DATE15(String PEI_INFORMATION_DATE15) {
        this.PEI_INFORMATION_DATE15 = PEI_INFORMATION_DATE15;
    }

    public String getPEI_INFORMATION_DATE15() {
        return PEI_INFORMATION_DATE15;
    }

    public void setOBJECT_VERSION_NUMBER(String OBJECT_VERSION_NUMBER) {
        this.OBJECT_VERSION_NUMBER = OBJECT_VERSION_NUMBER;
    }

    public String getOBJECT_VERSION_NUMBER() {
        return OBJECT_VERSION_NUMBER;
    }

    public void setLAST_UPDATE_LOGIN(String LAST_UPDATE_LOGIN) {
        this.LAST_UPDATE_LOGIN = LAST_UPDATE_LOGIN;
    }

    public String getLAST_UPDATE_LOGIN() {
        return LAST_UPDATE_LOGIN;
    }

    public void setEitLbl(String eitLbl) {
        this.eitLbl = eitLbl;
    }

    public String getEitLbl() {
        return eitLbl;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setNationalIdentity(String nationalIdentity) {
        this.nationalIdentity = nationalIdentity;
    }

    public String getNationalIdentity() {
        return nationalIdentity;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerOfManagerName(String managerOfManagerName) {
        this.managerOfManagerName = managerOfManagerName;
    }

    public String getManagerOfManagerName() {
        return managerOfManagerName;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getRejectReason() {
        return rejectReason;
    }
}
