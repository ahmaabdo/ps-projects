package com.appspro.fusionsshr.bean;

public class ProbationPeriodBean {
    private int id;
    private String requestDate;
    private String employeeName;
    private String employeeId;
    private String job;
    private String grade;
    private String hiringDate;
    private String functionEmp;
    private String evaluationDate;
    private String personNumber;
    private String workComment;
    private String attitudeComment;
    private String potentialComment;
    private String ovarAllComment;
    private String requestName;
    private String seqRequest;
    private String status;
    private String retainEmployee;
    private String extendProbation;
    private String terminateEmployee;
    private String transferEmployee;
    private String additionalComments;
    private String jobKnowledge;
    private String qualityOfWork;
    private String quantityOfWork;
    private String communication;
    private String commitment;
    private String workHabit;
    private String innovationInitiative;
    private String learningAgility;
    private String createdBy;
    private String updatedBy;
    private String renew;
    private String extend;
    private String nonrenewal;
    private String department;
    private String directManager;
    private String contractStartDate;
    private String contractEndDate;
    private String justificationComment;


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getJob() {
        return job;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public void setHiringDate(String hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getHiringDate() {
        return hiringDate;
    }

    public void setFunctionEmp(String functionEmp) {
        this.functionEmp = functionEmp;
    }

    public String getFunctionEmp() {
        return functionEmp;
    }

    public void setEvaluationDate(String evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluationDate() {
        return evaluationDate;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setWorkComment(String workComment) {
        this.workComment = workComment;
    }

    public String getWorkComment() {
        return workComment;
    }

    public void setAttitudeComment(String attitudeComment) {
        this.attitudeComment = attitudeComment;
    }

    public String getAttitudeComment() {
        return attitudeComment;
    }

    public void setPotentialComment(String potentialComment) {
        this.potentialComment = potentialComment;
    }

    public String getPotentialComment() {
        return potentialComment;
    }

    public void setOvarAllComment(String ovarAllComment) {
        this.ovarAllComment = ovarAllComment;
    }

    public String getOvarAllComment() {
        return ovarAllComment;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setSeqRequest(String seqRequest) {
        this.seqRequest = seqRequest;
    }

    public String getSeqRequest() {
        return seqRequest;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setRetainEmployee(String retainEmployee) {
        this.retainEmployee = retainEmployee;
    }

    public String getRetainEmployee() {
        return retainEmployee;
    }

    public void setExtendProbation(String extendProbation) {
        this.extendProbation = extendProbation;
    }

    public String getExtendProbation() {
        return extendProbation;
    }

    public void setTerminateEmployee(String terminateEmployee) {
        this.terminateEmployee = terminateEmployee;
    }

    public String getTerminateEmployee() {
        return terminateEmployee;
    }

    public void setTransferEmployee(String transferEmployee) {
        this.transferEmployee = transferEmployee;
    }

    public String getTransferEmployee() {
        return transferEmployee;
    }

    public void setAdditionalComments(String additionalComments) {
        this.additionalComments = additionalComments;
    }

    public String getAdditionalComments() {
        return additionalComments;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }


    public void setJobKnowledge(String jobKnowledge) {
        this.jobKnowledge = jobKnowledge;
    }

    public String getJobKnowledge() {
        return jobKnowledge;
    }

    public void setQualityOfWork(String qualityOfWork) {
        this.qualityOfWork = qualityOfWork;
    }

    public String getQualityOfWork() {
        return qualityOfWork;
    }

    public void setQuantityOfWork(String quantityOfWork) {
        this.quantityOfWork = quantityOfWork;
    }

    public String getQuantityOfWork() {
        return quantityOfWork;
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public String getCommunication() {
        return communication;
    }

    public void setCommitment(String commitment) {
        this.commitment = commitment;
    }

    public String getCommitment() {
        return commitment;
    }

    public void setWorkHabit(String workHabit) {
        this.workHabit = workHabit;
    }

    public String getWorkHabit() {
        return workHabit;
    }

    public void setInnovationInitiative(String innovationInitiative) {
        this.innovationInitiative = innovationInitiative;
    }

    public String getInnovationInitiative() {
        return innovationInitiative;
    }

    public void setLearningAgility(String learningAgility) {
        this.learningAgility = learningAgility;
    }

    public String getLearningAgility() {
        return learningAgility;
    }


    public void setRenew(String renew) {
        this.renew = renew;
    }

    public String getRenew() {
        return renew;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public String getExtend() {
        return extend;
    }

    public void setNonrenewal(String nonrenewal) {
        this.nonrenewal = nonrenewal;
    }

    public String getNonrenewal() {
        return nonrenewal;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setDirectManager(String directManager) {
        this.directManager = directManager;
    }

    public String getDirectManager() {
        return directManager;
    }

    public void setContractStartDate(String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getContractStartDate() {
        return contractStartDate;
    }

    public void setContractEndDate(String contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public String getContractEndDate() {
        return contractEndDate;
    }


    public void setJustificationComment(String justificationComment) {
        this.justificationComment = justificationComment;
    }

    public String getJustificationComment() {
        return justificationComment;
    }
}
