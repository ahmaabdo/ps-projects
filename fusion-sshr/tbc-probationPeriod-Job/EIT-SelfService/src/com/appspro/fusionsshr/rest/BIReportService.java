/*
	 * To change this license header, choose License Headers in Project Properties.
	 * To change this template file, choose Tools | Templates
	 * and open the template in the editor.
	 */
package com.appspro.fusionsshr.rest;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.ProbationPeriodBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.fusionsshr.dao.EmployeeDetails;
import com.appspro.fusionsshr.dao.PeopleExtraInformationDAO;
import com.appspro.fusionsshr.dao.ProbationPeriodDAO;
import com.appspro.fusionsshr.dao.SelfServiceDAO;
import com.appspro.fusionsshr.dao.SummaryReportDAO;

import com.appspro.mail.CredentialStoreClassMail;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIPReports;
import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.XML;

import org.json.simple.parser.JSONParser;


import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


@Path("/report")
public class BIReportService {

    @POST
    @Path("/commonbireport")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getBiReport(String incomingData, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) throws ParseException, JsonProcessingException,
                                  AppsProException {
        StringBuilder crunchifyBuilder = new StringBuilder();
        HttpSession session = request.getSession();
        String authorization =
            session.getAttribute("authorization") != null ? session.getAttribute("authorization").toString() :
            null;

        JSONObject jObject = null; // json
        JSONArray reportListJSON = null;
        String fresponse = null;
        String reportName = null;
        CredentialStoreClassMail mailObj = new CredentialStoreClassMail();
        try {
            if (incomingData != null && !incomingData.isEmpty()) {

                Object jsonTokner = new JSONTokener(incomingData).nextValue();
                if (jsonTokner instanceof JSONObject) {
                    jObject = new JSONObject(incomingData);
                    reportName = jObject.getString("reportName");
                } else {
                    reportListJSON = new JSONArray(incomingData);
                }

                java.util.Date currentDate = new java.util.Date();
                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat ft2 = new SimpleDateFormat("dd/MM/yyyy");
                BIPReports biPReports = new BIPReports();
                biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
                biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

                if (reportName != null) {
                        String paramName;


                        SummaryReportDAO summary = new SummaryReportDAO();
                        SummaryReportBean reportBeans =
                            new SummaryReportBean();

                        ArrayList arr =
                            summary.getReportParamSummary(reportName);
                        if (!arr.isEmpty()) {
                            reportBeans = (SummaryReportBean)arr.get(0);
                        }


                        if (reportBeans.getParameter1() != null) {
                            paramName = reportBeans.getParameter1();
                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }


                        }
                        if (reportBeans.getParameter2() != null) {
                            paramName = reportBeans.getParameter2();

                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }


                        }
                        if (reportBeans.getParameter3() != null) {
                            paramName = reportBeans.getParameter3();
                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }

                        }
                        if (reportBeans.getParameter4() != null) {
                            paramName = reportBeans.getParameter4();
                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }

                        }
                        if (reportBeans.getParameter5() != null) {
                            paramName = reportBeans.getParameter5();
                            if (jObject.has(paramName)) {
                                biPReports.getParamMap().put(paramName,
                                                             jObject.get(paramName).toString());
                            } else {
                                biPReports.getParamMap().put(paramName, "");
                            }

                        }

                        if (reportBeans.getReportTypeVal() != null) {
                            if (reportBeans.getReportTypeVal().equals("xml")) {

                                biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                                biPReports.setReportAbsolutePath(reportBeans.getReport_Name());

                                fresponse = biPReports.executeReports();
                                JSONObject xmlJSONObj =
                                    XML.toJSONObject(fresponse.toString());

                                if (!xmlJSONObj.isNull("DATA_DS") &&
                                    !xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
                                    if (xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) {

                                        return Response.ok("[]",
                                                           MediaType.TEXT_PLAIN).build();
                                    } else {
                                        JSONObject dataDS = null;
                                        JSONArray g1 = null;
                                        try {
                                             dataDS = xmlJSONObj.getJSONObject("DATA_DS");
                                             g1 = new JSONArray();
                                            Object jsonTokner1 =
                                                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
                                            if (jsonTokner1 instanceof JSONObject) {
                                                JSONObject arrJson = dataDS.getJSONObject("G_1");

                                                g1.put(arrJson);
                                            } else {
                                                JSONArray arrJson = dataDS.getJSONArray("G_1");
                                                g1 = arrJson;
                                            }
                                            for (int i = 0; i < g1.length();
                                                 i++) {
                                                JSONObject data =
                                                    (JSONObject)g1.get(i);
                                                EmployeeDetails obj = new EmployeeDetails();
                                                PeopleExtraInformationBean pro = new PeopleExtraInformationBean();
                                                PeopleExtraInformationDAO peid = new PeopleExtraInformationDAO();
                                                
                                                EmployeeBean obj2 = new EmployeeBean();
                                                obj2 = obj.searchPersonDetails("", "", data.get("PERSON_NUMBER").toString(), "", "", "", "");

                                                pro.setPerson_number(obj2.getPersonNumber());
                                                pro.setPerson_id(obj2.getPersonId());
                                                pro.setPersonName(obj2.getDisplayName());
                                                pro.setLine_manager(obj2.getManagerId());
                                                pro.setManage_of_manager(obj2.getManagerOfManager());
                                                pro.setPEI_INFORMATION1(obj2.getEmail());
                                                pro.setPEI_INFORMATION2(obj2.getDepartmentName());
                                                pro.setPEI_INFORMATION3(obj2.getPositionName());
                                                pro.setPEI_INFORMATION4(data.opt("NATIONALITY").toString());
                                                pro.setPEI_INFORMATION5(data.opt("MAN_EMP_NAME").toString());
                                                Date hireDate= ft.parse(obj2.getHireDate());
                                                pro.setPEI_INFORMATION6(ft2.format(hireDate));
                                                pro.setPEI_INFORMATION7(ft2.format(currentDate));
                                                pro.setManagerName(obj2.getManagerName());
                                                pro.setCode("XXX_PROBATION_PERIOD");
                                                pro.setEit_name("Probation Period");
                                                pro.setCreation_date(obj2.getPersonId());
                                                pro.setStatus("PENDING_APPROVED");

                                                 peid.insertOrUpdateXXX_EIT(pro, "ADD","Default");
                                               
                                                
                                                String subject =
                                                    "Probation Request Initiated - HC / " + data.opt("PERSON_NUMBER");
                                                String body = null;

                                                body =
                                                "<html><head><style>\n" + 
                                                ".sig{\n" + 
                                                "font-size: 20px;\n" + 
                                                "font-weight: bold;\n" + 
                                                "    font-family: Tajawal;\n" + 
                                                "    color: #63A70A;\n" + 
                                                "}\n" + 
                                                ".mob{\n" + 
                                                "color:#63A70A;\n" + 
                                                "font-weight: bold;\n" + 
                                                "font-size: 13px;\n" + 
                                                "}\n" + 
                                                ".parColor{\n" + 
                                                "color:#1D4757;\n" + 
                                                "font-size:13px;\n" + 
                                                "}\n" + 
                                                "</style></head><body>" +
                                                "<p>Dear "+ data.get("MAN_EMP_NAME")+",</p>" +
                                                "<p>Probation period evaluation for <strong>"+ data.get("EMP_NAME").toString() +"</strong> has started. Please use the following link to track it<br/><strong><a href=\"https://eoay.login.em2.oraclecloud.com\"> Oracle Fusion Application -> Me -> Self-Services</a></strong></p><p><strong>Regards,</strong></p><p><strong>Human Capital Department</strong></p>\n" +
                                                "</body></html>";

                                                System.out.println(body);
                                
                                                try {
                                                    mailObj.SEND_MAIL(data.get("EMAIL_ADDRESS_MAN").toString(),
                                                                                            subject,
                                                                                            body);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    return Response.ok(xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                                            "value").replaceAll("LABEL",
                                                                                                                                                "label"),
                                                       MediaType.APPLICATION_JSON).build();
                                } else {
                                    return Response.ok("[]",
                                                       MediaType.TEXT_PLAIN).build();
                                }
                            } else if (reportBeans.getReportTypeVal().equals("pdf")) {

                                biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                                biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                                biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                                byte[] reportBytes =
                                    biPReports.executeReportsPDF();

                                fresponse =
                                        javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                                return Response.ok("{" + "\"REPORT\":" + "\"" +
                                                   fresponse + "\"" + "}",
                                                   MediaType.APPLICATION_JSON).build();

                            }
                        }
                    
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            //            return Response.status(500).entity(e.getMessage()).build();
        }

        // return HTTP response 200 in case of success
        return Response.ok(fresponse, MediaType.APPLICATION_JSON).build();
    }

    public static void doSomething(Node node) {
        // do something with the current node instead of System.out
        if (node.getNodeName().equals("G_1")) {
            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                Node currentNode = node.getChildNodes().item(i);

            }
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                doSomething(currentNode);
            }
        }
    }
    
    public static void main(String[] args) throws ParseException {
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date strDate= formatter.parse("2020-03-29");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println(formatter2.format(strDate));
    }

}
