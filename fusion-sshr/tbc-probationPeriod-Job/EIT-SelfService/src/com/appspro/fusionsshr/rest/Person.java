/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.dao.EmployeeDetails;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author user
 */
@Path("/emp")
public class Person {

    @GET
    @Path("/info")
    public Response getEmpDetails(@QueryParam("username2")
        String username2, @QueryParam("token")
        String token, @QueryParam("hostURL")
        String hostURL, @QueryParam("mainData")
        boolean mainData, @Context
        HttpServletRequest request) {


        HttpSession session = request.getSession();
        ObjectMapper mapper = new ObjectMapper();
        EmployeeBean obj = new EmployeeBean();
        EmployeeDetails det = new EmployeeDetails();

        if (session.getAttribute(username2) != null) {
            obj = (EmployeeBean)session.getAttribute(username2);
        } else {
            if (username2 != null && !username2.isEmpty()) {
                obj = det.getEmpDetails(username2, token, hostURL, !mainData);
                if (!mainData)
                    session.setAttribute(username2, obj);
            } else {
                return Response.ok("", MediaType.APPLICATION_JSON).build();

            }

        }
        //        GenerateExcelForBulk.analysisExcelBulk(null);
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {

        }
        //(jsonInString);
        try {
            return Response.ok(new String(jsonInString.getBytes(), "UTF-8"),
                               MediaType.APPLICATION_JSON).build();

        } catch (UnsupportedEncodingException ex) {
            return Response.status(500).entity(ex.getMessage()).build();
        }


    }

    @GET
    @Path("/logout")
    public Response logoutUser(@QueryParam("username2")
        String username2, @QueryParam("token")
        String token, @QueryParam("hostURL")
        String hostURL, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        HttpSession session = request.getSession();


        session.invalidate();
        // session.removeAttribute("EmpDetails");

        String jsonInString = "";

        try {
            return Response.ok(new String(jsonInString.getBytes(), "UTF-8"),
                               MediaType.APPLICATION_JSON).build();
        } catch (UnsupportedEncodingException ex) {
            return Response.status(500).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("/searchEmployees")
    public Response getEmployeesForSpecialists(@QueryParam("name")
        String name, @QueryParam("nationalId")
        String nationalId, @QueryParam("personNumber")
        String personNumber, @QueryParam("managerId")
        String managerId, @QueryParam("effectiveAsof")
        String effectiveAsof, @QueryParam("token")
        String token, @QueryParam("hostURL")
        String hostURL, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        //("Here");


        try {
            ObjectMapper mapper = new ObjectMapper();
            EmployeeBean obj;
            EmployeeDetails det = new EmployeeDetails();

            obj =
det.searchPersonDetails(name, nationalId, personNumber, managerId,
                        effectiveAsof, token, hostURL);
            String jsonInString = null;
            jsonInString = mapper.writeValueAsString(obj);
            return Response.ok(jsonInString,
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            return Response.status(500).entity(e.getMessage()).build();
        }


    }
}
