/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.auth;

import com.appspro.db.AppsproConnection;

import java.io.IOException;
import sun.misc.BASE64Decoder;

/**
 *
 * @author Hp
 */
public class Authentication {

    public static boolean restAuth(String authString, String authorization) {
        if (authString == null && authorization == null) {
            return false;
        } else if (authString != null) {
            if (!isUserAuthenticated(authString, authorization)) {
                return false;
            }
        } else if (authorization == null) {
            return false;
        }

        return true;
    }

    private static boolean isUserAuthenticated(String authString, String authorization) {

        String decodedAuth = "";
        if (authString == null) {
            return false;
        }

        if (authorization != null) {
            if (authString.equals(authorization)) {
                return true;
            }
        }

        // Header is in the format "Basic 5tyc0uiDat4"
        // We need to extract data before decoding it back to original string
        String[] authParts = authString.split("\\s+");
        String authInfo = authParts[1];
        // Decode the data back to original string
        byte[] bytes = null;
        try {
            bytes = new BASE64Decoder().decodeBuffer(authInfo);
        } catch (IOException e) {
            // TODO Auto-generated catch block
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        decodedAuth = new String(bytes);
       
        String[] cridential = decodedAuth.split(":");
       // Login login = new Login();
        boolean result = true;

        /**
         * here you include your logic to validate user authentication. it can
         * be using ldap, or token exchange mechanism or your custom
         * authentication mechanism.
         */
        // your validation code goes here....
        return result;
    }
}
