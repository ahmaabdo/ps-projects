package jobSchedule;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import jobCode.ProbationPeriodJob;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;


public class ContextListener implements ServletContextListener{
    private ServletContext context = null;

    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
                System.out.println("----- listeners initiated");
        ProbationPeriodJob.runJob();

    }

    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
        ProbationPeriodJob.stopScheduler();
    }
}
