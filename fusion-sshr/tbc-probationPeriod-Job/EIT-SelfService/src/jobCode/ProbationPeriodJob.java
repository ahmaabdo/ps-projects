package jobCode;


import org.quartz.Job;
import org.quartz.JobExecutionContext;
import com.appspro.fusionsshr.dao.ProbationPeriodAlert;

import com.hha.appsproExceptionn.AppsProException;

import java.text.ParseException;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class ProbationPeriodJob implements Job {
    public ProbationPeriodJob() {
        super();
    }
    
    public static void runJob(){
        try {

            JobDetail jobProbation = JobBuilder.newJob(ProbationPeriodJob.class).build();

            Trigger triggerProbation = TriggerBuilder.newTrigger()
                    .withIdentity("job-Probation")
                    .withSchedule(
                            CronScheduleBuilder.dailyAtHourAndMinute(06, 30))
                    .forJob(jobProbation)
                    .build();
           
            Scheduler schProbation = StdSchedulerFactory.getDefaultScheduler();
            schProbation.start();

            schProbation.scheduleJob(jobProbation, triggerProbation);
        } catch (SchedulerException e) {
            System.out.println("Error" + e);
            e.printStackTrace();
        }
    }
    public void execute(JobExecutionContext jobExecutionContext)  {
        ProbationPeriodAlert probationPeriodObj = new ProbationPeriodAlert();

        try {
            probationPeriodObj.reminderProbationPeriod();
            probationPeriodObj.getProbationPeriodFromSaaSReport();
        } catch (AppsProException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    
    public static void stopScheduler() {
    //        log.warn("Stopping scheduler (pid:" + ServerUtil.getCurrentPid() + ")");
        try {
            // Grab the Scheduler instance from the Factory
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.shutdown();

        } catch (SchedulerException se) {
    //            log.error("Scheduler fail to stop", se);
        }
    }
}
