/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * messageNotificationSummary module
 */

define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojdialog', 'ojs/ojradioset', 'ojs/ojavatar', 'ojs/ojbutton', 'ojs/ojcolor', 'ojs/ojcolorspectrum'],
        function (oj, ko, $, app, services, commonhelper) {

            function messageNotificationSummaryContentViewModel() {

                //---------- observable delcleration section------------
                var self = this;
                self.columnArray = ko.observableArray();
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.backActionLbl = ko.observable();
                self.attachSourceLbl = ko.observable();
                self.choiceMessage = ko.observable();
                self.dataNotFoundMessage = ko.observable();
                self.giudMessage = ko.observable();
                self.deptObservableArray = ko.observableArray();
                self.EitName = ko.observableArray([]);
                self.editBtnDisabled = ko.observable(false);
                self.delBtnDisabled = ko.observable(false);
                self.messageNotification = ko.observable();
                self.checkResultSearch = ko.observable();
                var Url;
                var positionObj;
                self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'id'});
                // ---------table select listener---------------------------
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow) {
                        self.selectedIndex(currentRow['rowIndex']);
                        self.editBtnDisabled(false);
                        self.delBtnDisabled(false);
                    } else if (!currentRow) {
                        self.editBtnDisabled(true);
                        self.delBtnDisabled(true);
                    }
                };
                //-----------------add btn action -------------------
                self.addAdditionalDetails = function () {
                    app.loading(true);
                    positionObj = {};
                    positionObj.type = "ADD";
                    oj.Router.rootInstance.store(positionObj);
                    oj.Router.rootInstance.go('messageNotificationOperation');
                };
                //-----------------update btn action -------------------

                self.updateAdditionalDetails = function () {
                    if (self.deptObservableArray()[self.selectedIndex()] != null) {
                        document.querySelector("#editDialog").open();
                    } else
                    {
                        $.notify(self.ValidationMessage(), "error");
                    }
                };
                self.deleteAdditionalDetails = function () {
                    if (self.deptObservableArray()[self.selectedIndex()] != null) {
                        document.querySelector("#deleteDialog").open();
                    } else
                    {
                        $.notify(self.ValidationMessage(), "error");
                    }

                };
                //------------------ delete record function -------------------
                self.globalDelete = function () {
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.deptObservableArray()[currentRow['rowIndex']];
                    var id = self.deptObservableArray()[currentRow['rowIndex']].id;
                    var attachSouce = self.deptObservableArray()[currentRow['rowIndex']].attachSource;
                    var getdeleteCBF = function () {
                        self.deptObservableArray([]);
                    };
                    var serviceName = "MsgNotification/deleteMessageNotification/";
                    services.getGeneric(serviceName + id + '/' + attachSouce, {}).then(getdeleteCBF, app.failCbFn);
                    app.loading(true);
                    self.messages.push({severity: 'confirmation', summary: self.recordDeletedMessage(), autoTimeout: 0});
                    self.structureChangedHandler();
                };
                //-------------- summary btn back action ---------
                self.backAction = function () {
                    app.loading(true);
                    oj.Router.rootInstance.go('searchPosition');
                };
                //---------- handel attached method-------------
                self.handleAttached = function () {
                    app.loading(false);
                    self.editBtnDisabled(true);
                    self.delBtnDisabled(true);
                    positionObj = oj.Router.rootInstance.retrieve();
                    self.EitName(app.globalEitNameReport());
                };
                //------------------Option Change Section ----------------------------

                self.structureChangedHandler = function () {
                    var positionCode = self.selctedStruct();
                    Url = commonhelper.getSelfService;
                    var successCBFN = function (data) {
                        if (data.length !== 0) {
                            for (var i = 0; i < data.length; i++) {
                                for (var j = 0; j < app.globalEitNameReport().length; j++) {
                                    if (app.globalEitNameReport()[j].value == data[i].eitCode) {
                                        data[i].eitCodeLbl = app.globalEitNameReport()[j].label;
                                    }
                                }
                            }
                            self.deptObservableArray(data);
                        } else {
                            self.deptObservableArray([]);
                            self.messages.push({severity: 'info', summary: self.dataNotFoundMessage(), detail: self.giudMessage(), autoTimeout: 0
                            });
                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = "MsgNotification/getMessageNotification";
                    services.getGeneric(serviceName + '/' + positionCode, {}).then(successCBFN, failCbFn);
                };
                //--------------------------close dialog Section --------------------------------- 
                self.closeDialog = function () {
                    $("#modalDialog1").ojDialog("close");
                };
                //----------- btn add action --------------
                self.commitRecord = function (data, event) {
                    self.globalDelete();
                    app.loading(false);
                    document.querySelector("#deleteDialog").close();
                };
                //----------- btn edit action --------------
                self.commitEditRecord = function (data, event) {
                    app.loading(true);
                    self.deptObservableArray()[self.selectedIndex()].type = "EDIT";
                    oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go("messageNotificationOperation");
                    document.querySelector("#editDialog").close();
                };
                self.cancelButton = function () {
                    document.querySelector("#deleteDialog").close();
                    document.querySelector("#editDialog").close();
                };
                //*-------------go back  btn action----------
                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };
                //--------------- define messages notification ---------------//
                self.messages = ko.observableArray();
                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };
                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.deleteLbl = ko.observable();
                self.eitNameLbl = ko.observable();
                self.selfServiceLbl = ko.observable();
                self.backLbl = ko.observable();
                self.placeholder = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.oprationEditMessage = ko.observable();
                self.oprationDeleteMessage = ko.observable();
                self.addBtnDisabled = ko.observable(false);
                self.isExist = ko.observable();
                self.selfSerivce = ko.observable();
                self.emailBodyLbl = ko.observable();
                self.notificationTypeLbl = ko.observable();
                self.haveAttachLbl = ko.observable();
                self.notificationMessageLbl = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.messageNotificationLbl = ko.observable();
                self.ValidationMessage = ko.observable();
                self.recordDeletedMessage = ko.observable();
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
                    self.selfServiceLbl(getTranslation("manageOnlineHelp.selfServiceLbl"));
                    self.backLbl(getTranslation("others.back"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.deleteLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
                    self.oprationEditMessage(getTranslation("messageNotification.oprationEditMessage"));
                    self.oprationDeleteMessage(getTranslation("messageNotification.oprationDeleteMessage"));
                    self.oprationMessage(getTranslation("approvalScreen.oprationMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.attachSourceLbl(getTranslation("messageNotification.attachSourceLbl"));
                    self.selfSerivce(getTranslation("manageOnlineHelp.selfSerivce"));
                    self.emailBodyLbl(getTranslation("messageNotification.emailBodyLbl"));
                    self.notificationMessageLbl(getTranslation("messageNotification.notificationMessageLbl"));
                    self.notificationTypeLbl(getTranslation("messageNotification.notificationTypeLbl"));
                    self.haveAttachLbl(getTranslation("messageNotification.haveAttachLbl"));
                    self.isExist(getTranslation("common.isExist"));
                    self.ValidationMessage(getTranslation("labels.ValidationMessage"));
                    self.messageNotificationLbl(getTranslation("pages.messageNotificationLbl"));
                    self.recordDeletedMessage(getTranslation("messageNotification.recordDeletedMessage"));
                    self.choiceMessage(getTranslation("messageNotification.choiceMessage"));
                    self.dataNotFoundMessage(getTranslation("messageNotification.dataNotFoundMessage"));
                    self.giudMessage(getTranslation("messageNotification.giudMessage"));
                    self.columnArray([
                        {
                            "headerText": self.selfSerivce(), "field": "eitCodeLbl"
                        },
                        {
                            "headerText": self.emailBodyLbl(), "field": "emailBody"
                        },
                        {
                            "headerText": self.notificationMessageLbl(), "field": "notificationMessage"
                        },
                        {
                            "headerText": self.notificationTypeLbl(), "field": "notificationType"
                        },
                        {
                            "headerText": self.haveAttachLbl(), "field": "haveAttach"
                        },
                        {
                            "headerText": self.attachSourceLbl(), "field": "attachSource"
                        }
                    ]);
                }
                initTranslations();
            }


            return messageNotificationSummaryContentViewModel;
        }
);
