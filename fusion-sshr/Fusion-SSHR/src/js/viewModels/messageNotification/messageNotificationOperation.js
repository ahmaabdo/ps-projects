/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * messageNotificationOperation module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojcollapsible',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup', 'ojs/ojinputtext',
    'ojs/ojlabel', 'ojs/ojfilepicker', 'ojs/ojmessages', 'ojs/ojmessage', 'rich-text-editor/loader'],
        function (oj, ko, $, app, services) {
            function messageNotificationOperationContentViewModel() {
                var self = this;
                //---------- observable section decleration-------
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.isDisabled = ko.observable(false);
                self.attachDisabledBTN = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.disableBtnSubmit = ko.observable(false);
                self.deptObservableArray = ko.observableArray();
                self.attachSouceVisability = ko.observable();
                self.reportOptionsArr = ko.observableArray();
                self.innerHtm = ko.observable();
                self.attachDisabled = ko.observable(true);
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.messageNotificationDisabled = ko.observable(false);
                self.EitNameArr = ko.observableArray([]);
                self.messages = ko.observableArray();
                self.validateAttachment = ko.observable();
                self.confirmMessageDialog = ko.observable();
                self.attachementVisibilty = ko.observable(false);
                self.koArray = ko.observableArray([]);
                self.checkMessage = ko.observable();
                self.reportNameLbl = ko.observable();
                var dataFiles = {};
                var positionObj;
                //-------------------- data provider section declear  -----------------------//
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});

                //--------------- define messages notification ---------------//
                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };
                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };
                //------------------------------------------------------------//
                self.stepArray = ko.observableArray([]);
                self.haveAttachOption = ko.observableArray([]);
                self.attachSourceOption = ko.observableArray([{label: "PaaS", value: 'PaaS'},
                    {label: "SaaS", value: 'SaaS'}]);
                self.notificationTypeOption = ko.observableArray([]);

                //--------------- custom model--------------
                self.manageSetupModel = {
                    id: ko.observable(),
                    selfSerivce: ko.observable(),
                    emailBody: ko.observable(''),
                    notificationMessage: ko.observable(''),
                    notificationTypeValue: ko.observable(),
                    haveAtttachValue: ko.observable(),
                    attachSource: ko.observable(''),
                    attachmentId: ko.observable(),
                    reportSegmentName: ko.observable(),
                    notificationMsg: ko.observable(),
                };

                //----------------btn back action------------
                self.backAction = function () {
                    oj.Router.rootInstance.go('messageNotificationSummary');

                };

                //----------- retrieve email body data  ---------------
                self.retriveEmailBodyData = function () {
                    innerHtm = $("#richText2")[0].getUpdatedHTML();
                    return true;
                };
                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev !== null) {
                        self.currentStepValue(prev);
                        self.attachSouceVisability(false);
                        self.messageNotificationDisabled(false);
                        self.attachmentSourceChangeListiner();
                        $("#disText2").removeClass("disabledbutton");

                    }
                    if (self.pageMode() == 'EDIT') {
                        if (self.manageSetupModel.haveAtttachValue() == "NO") {
                            self.attachDisabled(true);
                            self.attachSouceVisability(true);

                        } else {
                            self.attachSouceVisability(false);
                            self.attachDisabled(false);
                        }
                    }
                    if (self.manageSetupModel.attachSource() == 'PaaS') {
                        self.attachementVisibilty(true);
                    } else {
                        self.attachementVisibilty(false);
                    }
                };

                //-----------------next step section----------
                self.nextStep = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                    {
                        if (positionObj.type == 'ADD') {
                            self.getAllSumary();
                            return;
                        }
                        if (self.koArray().length <= 0 && self.manageSetupModel.attachSource() == 'PaaS') {
                            self.messages.push({
                                severity: 'error',
                                summary: self.validateAttachment(),
                                autoTimeout: 0
                            });
                            self.currentStepValue('stp1');
                            return;

                        } else
                        {
                            var flag = true;
                            if (flag)
                            {
                                self.currentStepValue(next);
                                self.currentStepValueText();
                                self.attachDisabled(true);
                            }
                        }

                    }

                };

                //----------------current steps ------------------
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        self.attachSouceVisability(true);
                        self.messageNotificationDisabled(true);
                        $("#disText2").addClass("disabledbutton");
                        self.attachDisabledBTN(true);
                        self.attachDisabled(true);
                        if (positionObj.type == 'ADD') {
                            self.getAllSumary();
                            return;
                        }

                    } else {
                        self.attachDisabledBTN(false);
                        self.attachDisabled(false);
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        self.attachSouceVisability(false);
                        self.messageNotificationDisabled(false);
                        $("#disText2").removeClass("disabledbutton");

                        if (self.pageMode() == 'EDIT') {
                            if (self.manageSetupModel.haveAtttachValue() == "NO") {

                                self.attachSouceVisability(true);
                            } else {
                                self.attachSouceVisability(false);
                            }
                        }
                    }
                    return self.clothesAllowance();
                };
                //------------------End Of Section -------------------------------------------------

                self.attachmentChangeListiner = function (event) {
                    var data;
                    if (self.manageSetupModel.haveAtttachValue()) {
                        data = self.manageSetupModel.haveAtttachValue();
                        if (data == 'NO' && self.manageSetupModel.attachSource() == 'SaaS') {
                            self.attachSouceVisability(false);
                            self.manageSetupModel.reportSegmentName('');
                            self.koArray([]);
                            self.manageSetupModel.attachSource = ko.observable('');
                        } else if (data != 'YES') {
                            self.attachSouceVisability(false);
                            self.manageSetupModel.attachSource('');
                            self.attachementVisibilty(false);
                            self.koArray([]);
                        }
                    }
                };

                // ------------  attachment change listener------------
                self.attachmentSourceChangeListiner = function (event) {
                    var data = self.manageSetupModel.attachSource();
                    data != 'SaaS' ? self.attachDisabled(false) : self.attachDisabled(true);
                    if (self.manageSetupModel.attachSource() == 'SaaS') {
                        self.attachementVisibilty(false);
                        self.koArray([]);
                    } else {
                        self.attachementVisibilty(true);

                    }
                };
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                    app.loading(false);
                };
                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------

                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event) {

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                    if (self.koArray().length <= 0 && self.manageSetupModel.attachSource() == 'PaaS') {
                        self.messages.push({
                            severity: 'error',
                            summary: self.validateAttachment(),
                            autoTimeout: 0
                        });
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                function oprationServiceRecord() {
                    // ------- to retrieve data from rich text area---------
                    var notificationMsgBody = self.manageSetupModel.notificationMsg();
                    var emailBody = $("#richText2")[0].getUpdatedHTML();
                    var c = emailBody.includes("&lt;") ? emailBody.replace("&lt;", "<") : emailBody;
                    var emailBodyModified = c.includes("&gt;") ? c.replace("&gt;", ">") : c;

                    if (positionObj.type === 'ADD') {

                        var serviceName = "MsgNotification/addMsgNotification";

                        var OperationsCBFN = function () {
                            oj.Router.rootInstance.go('messageNotificationSummary');
                            self.messages.push({severity: 'confirmation', summary: self.addedMessage(), detail: self.showAddedMessage(), autoTimeout: 0
                            });
                        };

                        var failCbFn = function () {
                            $.notify(self.isExist(), "error");
                            document.querySelector("#yesNoDialog").close();
                        };
                        if (self.manageSetupModel.haveAtttachValue() == 'YES' && self.manageSetupModel.attachSource() == 'PaaS') {
                            var payloadWithAttach = {
                                "fileType": (self.koArray()[0].name).split('.').pop(),
                                "haveAttach": self.manageSetupModel.haveAtttachValue(),
                                "emailBody": emailBodyModified,
                                "notificationType": self.manageSetupModel.notificationTypeValue(),
                                "fileName": self.koArray()[0].name,
                                "attachment": self.koArray()[0].attachment,
                                "attachSource": self.manageSetupModel.attachSource(),
                                "eitCode": self.manageSetupModel.selfSerivce(),
                                "notificationMessage": notificationMsgBody
                            };
                        }
                        if (self.manageSetupModel.haveAtttachValue() == 'YES' && self.manageSetupModel.attachSource() == 'SaaS') {
                            var payloadNoAttach = {
                                "fileType": '',
                                "haveAttach": self.manageSetupModel.haveAtttachValue(),
                                "emailBody": emailBodyModified,
                                "notificationType": self.manageSetupModel.notificationTypeValue(),
                                "fileName": self.manageSetupModel.reportSegmentName(),
                                "attachment": '',
                                "attachSource": self.manageSetupModel.attachSource(),
                                "eitCode": self.manageSetupModel.selfSerivce(),
                                "notificationMessage": notificationMsgBody
                            };
                        }
                        if (self.manageSetupModel.haveAtttachValue() == 'YES' && self.manageSetupModel.attachSource() == 'SaaS') {
                            services.addGeneric(serviceName, payloadNoAttach).then(OperationsCBFN, failCbFn);

                        } else {
                            if (self.manageSetupModel.haveAtttachValue() == 'YES' && self.manageSetupModel.attachSource() == 'PaaS') {
                                services.addGeneric(serviceName, payloadWithAttach).then(OperationsCBFN, failCbFn);
                            }
                        }
                        if (self.manageSetupModel.attachSource() == '') {
                            var payload = {
                                "haveAttach": self.manageSetupModel.haveAtttachValue(),
                                "emailBody": emailBodyModified,
                                "notificationType": self.manageSetupModel.notificationTypeValue(),
                                "attachSource": self.manageSetupModel.attachSource(),
                                "eitCode": self.manageSetupModel.selfSerivce(),
                                "notificationMessage": notificationMsgBody
                            };
                            services.addGeneric(serviceName, payload).then(OperationsCBFN, failCbFn);
                        }
                    } else {
                        var serviceName = "MsgNotification/updateMsgNotification";
                        var OperationsEditCBFN = function () {
                            oj.Router.rootInstance.go('messageNotificationSummary');
                            self.messages.push({severity: 'confirmation', summary: self.updatedMessage(), detail: self.showAddedMessage(), autoTimeout: 0
                            });
                        };
                        var failEditCbFn = function () {
                            $.notify(self.isExist(), "error");
                            document.querySelector("#yesNoDialog").close();
                        };

                        if (self.manageSetupModel.haveAtttachValue() == 'YES' && self.manageSetupModel.attachSource() == 'PaaS') {
                            var payloadEditAttach = {
                                "fileType": (self.koArray()[0].name).split('.').pop(),
                                "haveAttach": self.manageSetupModel.haveAtttachValue(),
                                "emailBody": emailBodyModified,
                                "notificationType": self.manageSetupModel.notificationTypeValue(),
                                "fileName": self.koArray()[0].name,
                                "attachment": self.koArray()[0].attachment,
                                "attachSource": self.manageSetupModel.attachSource(),
                                "eitCode": self.manageSetupModel.selfSerivce(),
                                "notificationMessage": notificationMsgBody
                            };
                        }
                        if (self.manageSetupModel.haveAtttachValue() == 'YES' && self.manageSetupModel.attachSource() == 'SaaS') {
                            var payloadEditNoAttach = {
                                "fileType": '',
                                "haveAttach": self.manageSetupModel.haveAtttachValue(),
                                "emailBody": emailBodyModified,
                                "notificationType": self.manageSetupModel.notificationTypeValue(),
                                "fileName": self.manageSetupModel.reportSegmentName(),
                                "attachment": '',
                                "attachSource": self.manageSetupModel.attachSource(),
                                "eitCode": self.manageSetupModel.selfSerivce(),
                                "notificationMessage": notificationMsgBody
                            };
                        }
                        if (self.manageSetupModel.haveAtttachValue() == 'YES' && self.manageSetupModel.attachSource() == 'SaaS') {
                            services.addGeneric(serviceName + '/' + self.manageSetupModel.id(), payloadEditNoAttach).then(OperationsEditCBFN, failEditCbFn);
                        }

                        if (self.manageSetupModel.haveAtttachValue() == 'YES' && self.manageSetupModel.attachSource() == 'PaaS') {
                            services.addGeneric(serviceName + '/' + self.manageSetupModel.id(), payloadEditAttach).then(OperationsEditCBFN, failEditCbFn);
                        }
                        if (self.manageSetupModel.haveAtttachValue() == "NO") {
                            var payloadNoAttach = {
                                "haveAttach": self.manageSetupModel.haveAtttachValue(),
                                "emailBody": emailBodyModified,
                                "notificationType": self.manageSetupModel.notificationTypeValue(),
                                "attachSource": self.manageSetupModel.attachSource(),
                                "eitCode": self.manageSetupModel.selfSerivce(),
                                "notificationMessage": notificationMsgBody
                            };
                        }
                        ;
                        if (self.manageSetupModel.haveAtttachValue() == "NO") {
                            services.addGeneric(serviceName + '/' + self.manageSetupModel.id(), payloadNoAttach).then(OperationsEditCBFN, failEditCbFn);
                        }
                    }
                }
                ;
                // --------submit data action ----------
                self.getAllSumary = function () {
                    var checkEitInTable = function (data) {
                        if (data.length == 0) {
                            self.currentStepValue('stp2');
                        } else {
                            self.currentStepValue('stp1');
                            self.currentStepValueText();
                            let i = 0;
                            for (const e of data) {
                                i++;
                                if (data[0].eitCode == self.manageSetupModel.selfSerivce()) {
                                    self.messages.push({severity: 'error', summary: self.checkMessage(), autoTimeout: 0
                                    });
                                    app.loading(false);
                                    break;
                                    self.disableSubmit(true);

                                }
                            }
                            ;
                        }
                        ;
                    };
                    var serviceName = "MsgNotification/getMessageNotification";
                    services.getGeneric(serviceName + '/' + self.manageSetupModel.selfSerivce()).then(checkEitInTable, app.failCbFn);
                };

                //---------------Commit record operation -------------
                self.commitRecord = function () {
                    app.loading(true);
                    oprationServiceRecord();
                };
                //-------------------------Start Load Funcation------------------------------
                self.connected = function () {
                    //-------- retrieve data from session storage---------
                    self.manageSetupModel.selfSerivce(oj.Router.rootInstance.retrieve().eitCode);
                    self.manageSetupModel.notificationTypeValue(oj.Router.rootInstance.retrieve().notificationType);
                    self.manageSetupModel.attachSource(oj.Router.rootInstance.retrieve().attachSource);
                    self.manageSetupModel.notificationMsg(oj.Router.rootInstance.retrieve().notificationMessage);
                    self.attachmentChangeListiner();
                    self.manageSetupModel.haveAtttachValue(oj.Router.rootInstance.retrieve().haveAttach);
                    self.manageSetupModel.reportSegmentName(self.koArray().fileName);
                    self.manageSetupModel.id(oj.Router.rootInstance.retrieve().id);


                    //-----------------check if attach source equal paas--------
                    if (oj.Router.rootInstance.retrieve().attachSource == 'PaaS') {
                        self.attachDisabled(false);
                    } else {
                        self.attachDisabled(true);

                    }
                    ;

                    var dataRetrived = oj.Router.rootInstance.retrieve();
                    if (!Object.keys(dataRetrived).includes('notificationMessage')) {
                        self.manageSetupModel.notificationMessage('');
                    }
                    if (!Object.keys(dataRetrived).includes('emailBody')) {
                        self.manageSetupModel.emailBody('');
                    }
                    if (Object.keys(dataRetrived).includes('notificationMessage')) {
                        self.manageSetupModel.notificationMessage(oj.Router.rootInstance.retrieve().notificationMessage);
                    }
                    if (Object.keys(dataRetrived).includes('emailBody')) {
                        self.manageSetupModel.emailBody(oj.Router.rootInstance.retrieve().emailBody);
                    }

                    //-------get attachement details--------------

                    var getAttachDetails = function (data) {
                        if (data.length > 0) {
                            self.koArray.push({
                                "name": data[0].fileName,
                                "attachment": data[0].attachment
                            });
                            self.manageSetupModel.attachmentId(data[0].ID);
                            if (positionObj.type == 'EDIT' && oj.Router.rootInstance.retrieve().attachSource == "SaaS") {
                                self.manageSetupModel.reportSegmentName(data[0].fileName);
                                self.koArray([]);
                            }
                        }

                    };
                    var failCbFn = function () {};
                    var serviceName = "MsgNotification/AttachementDetails/";
                    services.getGeneric(serviceName + self.manageSetupModel.id(), {}).then(getAttachDetails, failCbFn);
                }
                ;
                //-------------------------End Load Function---------------------------------
                self.handleAttached = function () {
                    app.loading(false);
                    initTranslations();
                    self.getAllReports();
                    self.EitNameArr(app.globalEitNameReport());
                    self.notificationTypeOption(app.getPaaSLookup('NOTIFICATION_TYPE'));
                    self.haveAttachOption(app.getPaaSLookup('YES_NO'));
                    positionObj = oj.Router.rootInstance.retrieve();

                    if (positionObj.type == 'ADD') {
                        self.pageMode('ADD');
                        self.clothesAllowance(self.addnotificationMessageLbl());
                        self.validationMessage(self.manageAdd());
                    } else if (positionObj.type == 'EDIT') {
                        self.koArray([]);
                        self.connected();
                        self.clothesAllowance(self.editnotificationMessageLbl());
                        self.manageSetupModel.id(oj.Router.rootInstance.retrieve().id);
                        (positionObj.haveAttach == 'NO' || positionObj.attachSource == 'SaaS')
                                ? self.attachementVisibilty(false)
                                : self.attachementVisibilty(true);
                    }

                };
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable();
                self.eitCodeLbl = ko.observable();
                self.selfService = ko.observable();
                self.notificationMessageLbl = ko.observable();
                self.emailBodyLbl = ko.observable();
                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.manageAdd = ko.observable();
                self.addnotificationMessageLbl = ko.observable();
                self.validMessageLbl = ko.observable();
                self.editnotificationMessageLbl = ko.observable();
                self.validationMessage = ko.observable();
                self.confirmMessage = ko.observable();
                self.clothesAllowance = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.notificationTypeLbl = ko.observable();
                self.haveAttachLbl = ko.observable();
                self.attachSourceLbl = ko.observable();
                self.updatedMessage = ko.observable();
                self.addedMessage = ko.observable();
                self.showAddedMessage = ko.observable();
                self.isExist = ko.observableArray();
                self.attachmentLbl = ko.observable();
                self.UploadFileLbl = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.oprationEditMessage = ko.observable();
                self.help=ko.observable();
                self.noteMsg=ko.observable();
                self.note=ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                //-------------------------- icon attachment selection ---------------------//
                self.attachmentSelectListener = function (event) {


                    var files = event.detail.files;
                    if (files.length > 0) {
//                        self.attachDisabledBTN(false);

                        //add the new files at the beginning of the list
                        for (var i = 0; i < files.length; i++) {
                            function getBase64(file) {
                                return new Promise((resolve, reject) => {
                                    const reader = new FileReader();
                                    reader.readAsDataURL(file);
                                    reader.onload = () => resolve(reader.result);
                                    reader.onerror = error => reject(error);
                                });
                            }
                            dataFiles.name = files[i].name;
                            dataFiles.type = files[i].type;
                            dataFiles.id = i + 1;
                            getBase64(files[i]).then(function (data) {
                                dataFiles.data = (data);
                                self.attachmentId = self.attachmentId + 1;
                                dataFiles.id = (self.attachmentId);
                                self.koArray([]);
                                self.koArray.push({
                                    "name": dataFiles.name,
                                    "attachment": dataFiles.data,
                                    "id": dataFiles.id,
                                    "type": dataFiles.type
                                });
                            });
                        }
                    }

                };

                // delete attachment if exsist in edit screen--//
                self.deleteAttachmentEdit = function () {
                    var id = oj.Router.rootInstance.retrieve().id;
                    var successCBFN = function () {
                    };
                    var serviceName = "MsgNotification/deleteAttachment/";
                    services.getGeneric(serviceName + id).then(successCBFN, app.failCbFn);
                };
                //--------------remove attach acttion-----------//
                self.removeSelectedAttachment = function (event) {
                    self.koArray([]);
                    self.attachDisabledBTN(true);
                    if (positionObj.type == 'EDIT') {
                        self.pageMode('EDIT');
                        self.deleteAttachmentEdit();
                        self.attachDisabledBTN(false);
                    }
                };
                self.openAttachmentViewer = function () {

                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        $.each(self.koArray(), function (indexInner, data)
                        {
                            if (data.name.endsWith(".png") || data.name.endsWith(".PNG") || data.name.endsWith(".gif") || data.name.endsWith(".jpg") || data.name.endsWith(".jpeg") || data.name.endsWith(".pdf") || data.name.endsWith(".docx") || data.name.endsWith(".xlsx"))
                            {
                                self.attachmentViewerData(data.attachment);
                                app.pdfViewer(data.attachment);
                            }
                        });
                    });
                };
                //--------- colse attachment dialog----
                self.closeAttachmentViewer = function (event) {
                    self.attachmentViewerData('');
                    document.getElementById('attachmentDialog').close();
                };
                //--------- change handler for reportNameSegment 
                self.reportSegmentChangeListiner = function () {

                };
                //------------ to get report name in DDW----------
                self.getAllReports = function () {
                    var ReportNamesCbFn = function (data) {
                        for (var i = 0; i < data.length; i++) {
                            // push in array
                            self.reportOptionsArr.push({
                                value: data[i].report_Name,
                                label: data[i].report_Name
                            });
                        }
                        ;
                    };
                    var serviceName = "validation/reportnames";
                    services.getGeneric(serviceName).then(ReportNamesCbFn, app.failCbFn);
                };

                //--------------------translation part------------------//
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.eitCodeLbl(getTranslation("approvalScreen.eitCode"));
                    self.selfService(getTranslation("manageOnlineHelp.selfSerivce"));
                    self.emailBodyLbl(getTranslation("messageNotification.emailBodyLbl"));
                    self.notificationMessageLbl(getTranslation("messageNotification.notificationMessageLbl"));
                    self.cancel(getTranslation("others.cancel"));
                    self.manageAdd(getTranslation("manageOnlineHelp.manageAdd"));
                    self.addnotificationMessageLbl(getTranslation("messageNotification.addnotificationMessageLbl"));
                    self.editnotificationMessageLbl(getTranslation("messageNotification.editnotificationMessageLbl"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.oprationEditMessage(getTranslation("manageOnlineHelp.oprationEditMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.isExist(getTranslation("common.isExist"));
                    self.UploadFileLbl(getTranslation("common.UploadFileLbl"));
                    self.removeBtnLbl(getTranslation("common.removeBtnLbl"));
                    self.viewLbl(getTranslation("common.viewLbl"));
                    self.haveAttachLbl(getTranslation("messageNotification.haveAttachLbl"));
                    self.notificationTypeLbl(getTranslation("messageNotification.notificationTypeLbl"));
                    self.attachSourceLbl(getTranslation("messageNotification.attachSourceLbl"));
                    self.validateAttachment(getTranslation("iconSetup.validateAttachment"));
                    self.confirmMessageDialog(getTranslation("messageNotification.confirmMessageDialog"));
                    self.addedMessage(getTranslation("messageNotification.addedMessage"));
                    self.updatedMessage(getTranslation("messageNotification.updatedMessage"));
                    self.checkMessage(getTranslation("messageNotification.checkMessage"));
                    self.reportNameLbl(getTranslation("messageNotification.reportNameLbl"));
                    self.attachmentLbl(getTranslation("messageNotification.attachmentLbl"));
                    self.help(getTranslation("others.help"));
                    self.note(getTranslation("probation.note"));
                    self.noteMsg(getTranslation("messageNotification.noteMsg"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);

                }
                initTranslations();
            }
            return messageNotificationOperationContentViewModel;
        });
