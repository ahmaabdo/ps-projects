define(['ojs/ojcore', 'knockout', 'jquery', 'appController','config/services'],
        function (oj, ko, $, app,services) {

            function profileViewModel() {
                //vars
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;

                //observables
                self.Name = ko.observable();
                self.Manager = ko.observable();
                self.Position = ko.observable();
                self.Person_Number = ko.observable();
                self.Department = ko.observable();
                self.Grade = ko.observable();
                self.Details = ko.observable();
                self.citizenshipLegislationCode = ko.observable();
                self.hireDate = ko.observable();
                self.assignmentName = ko.observable();
                self.assignmentCategory = ko.observable();
                self.assignmentStatus = ko.observable();
                self.salaryAmount = ko.observable();
                self.department = ko.observable();
                self.grade = ko.observable();
                self.managerId = ko.observable();
                self.country = ko.observable();
                self.city = ko.observable();
                self.region = ko.observable();
                self.gradeId = ko.observable();
                self.managerName = ko.observable();
                self.employeeLocation = ko.observable();
                self.jobId = ko.observable();
                self.positionName = ko.observable();
                self.positionId = ko.observable();
                self.nationalId = ko.observable();
                self.managerOfManager = ko.observable();
                self.housingType = ko.observable();
                self.carAllowance = ko.observable();
                self.mobileAllowance = ko.observable();
                self.transportationAlowance = ko.observable();
                self.managerOfMnagerName = ko.observable();
                self.probationPeriodEndDate = ko.observable();
                self.dateOfBirth = ko.observable();
                self.legalEntityId = ko.observable();
                self.jobName = ko.observable();
                self.maritalStatus = ko.observable();
                self.email = ko.observable();
                self.mobileNumber = ko.observable();
                self.workPhone = ko.observable();
                self.workPhoneExt = ko.observable();
                self.departmentName = ko.observable();
                self.image = ko.observable();

                self.personDetails = ko.observable(app.personDetails());
                self.avatarSize = ko.observable("xl");
                self.firstName = ko.observable();
                self.lastName = ko.observable();
                self.initials = ko.observable();

                self.countPending = ko.observable();
                self.countApproved = ko.observable();
                self.countRejected = ko.observable();
                self.LeftMenu = ko.observable();


                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                //request

                self.getCountEITRequest = function (info) {
                    var getCountEITRequestCBF = function (data) {

                        self.countPending(data.countPendingApproved);
                        self.countApproved(data.countApproved);
                        self.countRejected(data.countRejected);

                    };
                    services.getGeneric("PeopleExtra/countRequest/" + self.personDetails().personId).then(getCountEITRequestCBF, app.failCbFn);
                };
                //end request


                self.connected = function () {
                    // Implement if needed
                };


                self.handleAttached = function (info) {
                    self.LeftMenu(app.hideLeftMenu());
                    self.getCountEITRequest();
                    app.loading(false);
                    initTranslations();
                    self.firstName(app.personDetails().firstName);
                    self.lastName(app.personDetails().lastName);
                    self.initials(oj.IntlConverterUtils.getInitials(self.firstName(),self.lastName()));
                     if(app.personDetails().picBase64===undefined ||app.personDetails().picBase64 ===null || app.personDetails().picBase64 ===""){
                        self.image('');
                     }else{
                         self.image("data:image/png;base64," + app.personDetails().picBase64);
                         
                     }
                    if (self.personDetails().picBase64) {
                            if ($(".profilepic")[0]) {
                                $(".profilepic")[0].src = "data:image/png;base64," + self.personDetails().picBase64;
                            }
                            if ($(".profilepic")[1]) {
                                $(".profilepic")[1].src = "data:image/png;base64," + self.personDetails().picBase64;
                            }
                        } else {
                            if ($(".profilepic")[0]) {
                                $(".profilepic")[0].src = "css/images/avatar_24px.png";
                            }
                            if ($(".profilepic")[1]) {
                                $(".profilepic")[1].src = "css/images/avatar_24px.png";
                            }
                        }
                };


                function initTranslations() {
                    self.Name(getTranslation("login.Name"));
                    self.positionName(getTranslation("login.positionName"));
                    self.positionId(getTranslation("login.positionId"));
                    self.Position(getTranslation("login.Position"));
                    self.Manager(getTranslation("login.Manager"));
                    self.Person_Number(getTranslation("login.Person_Number"));
                    self.Department(getTranslation("login.Department"));
                    self.Grade(getTranslation("login.Grade"));
                    self.Details(getTranslation("login.Details"));
                    self.citizenshipLegislationCode(getTranslation("login.citizenshipLegislationCode"));
                    self.hireDate(getTranslation("login.hireDate"));
                    self.assignmentName(getTranslation("login.assignmentName"));
                    self.assignmentCategory(getTranslation("login.assignmentCategory"));
                    self.assignmentStatus(getTranslation("login.assignmentStatus"));
                    self.salaryAmount(getTranslation("login.salaryAmount"));
                    self.department(getTranslation("login.department"));
                    self.grade(getTranslation("login.grade"));
                    self.managerId(getTranslation("login.managerId"));
                    self.country(getTranslation("login.country"));
                    self.city(getTranslation("login.city"));
                    self.region(getTranslation("login.region"));
                    self.gradeId(getTranslation("login.gradeId"));
                    self.managerName(getTranslation("login.managerName"));
                    self.employeeLocation(getTranslation("login.employeeLocation"));
                    self.jobId(getTranslation("login.jobId"));
                    self.nationalId(getTranslation("login.nationalId"));
                    self.managerOfManager(getTranslation("login.managerOfManager"));
                    self.housingType(getTranslation("login.housingType"));
                    self.carAllowance(getTranslation("login.carAllowance"));
                    self.mobileAllowance(getTranslation("login.mobileAllowance"));
                    self.transportationAlowance(getTranslation("login.transportationAlowance"));
                    self.managerOfMnagerName(getTranslation("login.managerOfMnagerName"));
                    self.probationPeriodEndDate(getTranslation("login.probationPeriodEndDate"));
                    self.legalEntityId(getTranslation("login.legalEntityId"));
                    self.dateOfBirth(getTranslation("login.dateOfBirth"));
                    self.jobName(getTranslation("login.jobName"));
                    self.maritalStatus(getTranslation("login.maritalStatus"));
                    self.email(getTranslation("login.email"));
                    self.mobileNumber(getTranslation("login.mobileNumber"));
                    self.workPhone(getTranslation("login.workPhone"));
                    self.workPhoneExt(getTranslation("login.workPhoneExt"));
                    self.departmentName(getTranslation("login.departmentName"));

                }
            }

            return new profileViewModel();
        }
);
