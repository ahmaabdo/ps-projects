/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, services, commonhelper) {

            function requiredQualificationsOprationScreen() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.dateMessage = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.qualificationNameList = ko.observableArray(rootViewModel.globalQualifications());
                self.gradesList = ko.observableArray(rootViewModel.globalHrGrades());
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };

                var Url;
                var jobObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);
                self.requiredQualificationModel = {
                    startDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                    endDate: ko.observable(),
                    grades: ko.observable(),
                    qualificationName: ko.observable(),
                    experienceYears: ko.observable(),
                    id: ko.observable(),
                    TransActionType: ko.observable(), //Edit View or Add 
                    flag: ko.observable('Y'), //Y or N
                    createdBy: ko.observable(),
                    jobName: ko.observable(),
                    creationDate: ko.observable(self.formatDate(new Date())),
                    updatedBy: ko.observable(),
                    updatedDate: ko.observable(),
                    code: ko.observable()

                };

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };

                self.handleAttached = function () {
                    app.loading(false);
                    initTranslations();
                    jobObj = oj.Router.rootInstance.retrieve();
                    if (jobObj.type == 'ADD') {
                        //hijri date
                        var calendar = $.calendars.instance();
                        var tempDate = calendar.parseDate('mm/dd/yyyy', '');
                        tempDate = calendar.formatDate('yyyy/mm/dd', tempDate);
                        $("#datePickerStartDate").val(tempDate);
                        $("#datePickerEndDate").val(tempDate);
                        $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var endDate = $('#datePickerEndDate').val();
                                self.requiredQualificationModel.endDate(endDate);
                            }});
                        $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var startDate = $('#datePickerStartDate').val();
                                self.requiredQualificationModel.startDate(startDate);
                            }});
                        //end of hijri date
                        self.pageMode('ADD');
                        self.requiredQualifications(self.requiredQualificationsScreenAddLbl());
                        self.oprationMessage(self.addMassageLbl());
                        self.requiredQualificationModel.jobName(jobObj.jobName);
                        self.requiredQualificationModel.createdBy(rootViewModel.personDetails().personId);
                    } else if (jobObj.type == 'EDIT') {
                        //hijri date
                        $("#datePickerStartDate").val(jobObj.startDate);
                        $("#datePickerEndDate").val(jobObj.endDate);
                        $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var endDate = $('#datePickerEndDate').val();
                                self.requiredQualificationModel.endDate(endDate);
                            }});
                        $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var startDate = $('#datePickerStartDate').val();
                                self.requiredQualificationModel.startDate(startDate);
                            }});

                        //
                        self.pageMode('EDIT');
                        self.requiredQualifications(self.requiredQualificationsScreenEditLbl());
                        self.requiredQualificationModel.createdBy(rootViewModel.personDetails().personId);
                        self.oprationMessage(self.editMassageLbl());
                        self.requiredQualificationModel.jobName(jobObj.jobName);
                        self.requiredQualificationModel.updatedBy(rootViewModel.personDetails().personId);
                        self.requiredQualificationModel.startDate(self.formatDate(new Date(jobObj.startDate)));
                        if (jobObj.endDate)
                            self.requiredQualificationModel.endDate(self.formatDate(new Date(jobObj.endDate)));
                        self.requiredQualificationModel.grades(jobObj.grades);
                        self.requiredQualificationModel.qualificationName(jobObj.qualificationName);
                        self.requiredQualificationModel.experienceYears(parseInt(jobObj.experienceYears));
                        self.requiredQualificationModel.updatedDate(self.formatDate(new Date()));
                        self.requiredQualificationModel.id(jobObj.id);
                        self.requiredQualificationModel.code(jobObj.code);
                    }

                };

                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {
                    function isValidDate(dateString)
                    {

                        // First check for the pattern
                        if (!/^\d{4}\/\d{1,2}\/\d{1,2}$/.test(dateString)) {
                            if (!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString)) {
                                $.notify(self.dateMessage());
                                return false;
                            }
                            return true;
                        }

                        // Parse the date parts to integers
                        var parts = dateString.split("/");
                        var month = parseInt(parts[1], 10);
                        var year = parseInt(parts[0], 10);
                        var day = parseInt(parts[2], 10);


                        // Check the ranges of month and year
                        if (year < 1300 || year > 3000 || month == 0 || month > 12) {
                            $.notify(self.dateMessage());
                            return false;
                        }

                        return true;


                    }
                    ;
                    function datevalidtion(d, d2) {
                        if (d >= d2) {
                            $.notify(self.dateMessage());
                            return false;
                        }
                        return true
                    }
                    ;

                    var s1 = self.requiredQualificationModel.startDate();
                    var d = Date.parse(s1);
                    var s2 = self.requiredQualificationModel.endDate();
                    var d2 = Date.parse(s2);
                    if (isValidDate(s1) && isValidDate(s2) && datevalidtion(d, d2)) {
                        var tracker = document.getElementById("tracker");
                        if (tracker.valid != "valid")
                        {
                            tracker.showMessages();
                            tracker.focusOn("@firstInvalidShown");
                            return;
                        }
                        var next = document.getElementById("train").getNextSelectableStep();
                        if (next != null)
                            self.currentStepValue(next);
                    }
                    ;

                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        $('#datePickerStartDate').calendarsPicker('enable');
                        $('#datePickerEndDate').calendarsPicker('enable');
                    }

                    return self.requiredQualifications();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    var jsonData = ko.toJSON(self.requiredQualificationModel);
                    var getValidGradeCBF = function (data) {
                        var Data = data;
                        self.disableSubmit(false);
                        if (oj.Router.rootInstance._navHistory.length > 1) {
                            oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                        } else {
                            oj.Router.rootInstance.go('home');
                        }
                    };
                    services.addGeneric(commonhelper.requiredqualificationUrl + self.pageMode(), jsonData).then(getValidGradeCBF, self.failCbFn);
                }
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.gradesLbl = ko.observable();
                self.qualificationLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();
                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.requiredQualificationsScreenAddLbl = ko.observable();
                self.requiredQualificationsScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.requiredQualifications = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.dateMessage(getTranslation("labels.datemessage"));

                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.gradeLbl(getTranslation("job.grade"));
                    self.gradesLbl(getTranslation("additionalDetails.grades"));
                    self.qualificationLbl(getTranslation("additionalDetails.qualificationName"));
                    self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
                    self.cancel(getTranslation("others.cancel"));
                    self.requiredQualificationsScreenAddLbl(getTranslation("requiredQualifications.requiredQualificationsAdd"));
                    self.requiredQualificationsScreenEditLbl(getTranslation("requiredQualifications.requiredQualificationsEdit"));
                    self.addMassageLbl(getTranslation("requiredQualifications.addMessage"));
                    self.editMassageLbl(getTranslation("requiredQualifications.editMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                    if (self.pageMode() == 'ADD') {
                        self.requiredQualifications(self.requiredQualificationsScreenAddLbl());
                        self.oprationMessage(self.addMassageLbl());
                    } else if (self.pageMode() == 'EDIT') {
                        self.requiredQualifications(self.requiredQualificationsScreenEditLbl());
                        self.oprationMessage(self.editMassageLbl());
                    }
                }
                initTranslations();




            }

            return requiredQualificationsOprationScreen;
        });
