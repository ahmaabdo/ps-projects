/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, services, commonhelper) {

            function supervisorAllowanceOprationScreen() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                self.dateMessage = ko.observable();
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };

                var Url;
                var jobObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);
                self.supervisorAllowanceModel = {
                    startDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                    endDate: ko.observable(),
                    supervisor: ko.observable(),
                    id: ko.observable(),
                    TransActionType: ko.observable(), //Edit View or Add 
                    flag: ko.observable('Y'), //Y or N
                    createdBy: ko.observable(),
                    jobName: ko.observable(),
                    creationDate: ko.observable(self.formatDate(new Date())),
                    updatedBy: ko.observable(),
                    updatedDate: ko.observable(),
                    code: ko.observable()

                };

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };
                self.handleAttached = function () {
                    app.loading(false);

                    initTranslations();
                    jobObj = oj.Router.rootInstance.retrieve();
                    //

                    //
                    if (jobObj.type == 'ADD') {
                        //hijri date
                        var calendar = $.calendars.instance();
                        var tempDate = calendar.parseDate('mm/dd/yyyy', '');
                        tempDate = calendar.formatDate('yyyy/mm/dd', tempDate);
                        $("#datePickerStartDate").val(tempDate);
                        $("#datePickerEndDate").val(tempDate);
                        $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var endDate = $('#datePickerEndDate').val();
                                self.supervisorAllowanceModel.endDate(endDate);
                            }});
                        $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var startDate = $('#datePickerStartDate').val();
                                self.supervisorAllowanceModel.startDate(startDate);
                            }});
                        //end of hijri date
                        self.pageMode('ADD');
//                        self.supervisorAllowanceModel(self.clothesAllowanceScreenAddLbl());
                        self.oprationMessage(self.addMassageLbl());
                        self.supervisorAllowanceModel.jobName(jobObj.jobName);
                        self.supervisorAllowanceModel.createdBy(rootViewModel.personDetails().personId);
                    } else if (jobObj.type == 'EDIT') {
                        //hijri date
                        $("#datePickerStartDate").val(jobObj.startDate);
                        $("#datePickerEndDate").val(jobObj.endDate);
                        $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var endDate = $('#datePickerEndDate').val();
                                self.supervisorAllowanceModel.endDate(endDate);
                            }});
                        $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                            onSelect: function (dates) {
                                var startDate = $('#datePickerStartDate').val();
                                self.supervisorAllowanceModel.startDate(startDate);
                            }});

                        //
                        self.pageMode('EDIT');
                        self.supervisorAllowance(self.supervisorAllowanceScreenEditLbl());
                        self.supervisorAllowanceModel.createdBy(rootViewModel.personDetails().personId);
                        self.oprationMessage(self.editMassageLbl());
                        self.supervisorAllowanceModel.jobName(jobObj.jobName);
                        self.supervisorAllowanceModel.updatedBy(rootViewModel.personDetails().personId);
                        self.supervisorAllowanceModel.startDate(self.formatDate(new Date(jobObj.startDate)));
                        self.supervisorAllowanceModel.endDate(self.formatDate(new Date(jobObj.endDate)));
                        self.supervisorAllowanceModel.supervisor(jobObj.supervisor);
                        self.supervisorAllowanceModel.id(jobObj.id);
                        self.supervisorAllowanceModel.updatedDate(self.formatDate(new Date()));
                        self.supervisorAllowanceModel.code(jobObj.code);
                    }

                };

                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {
                    function isValidDate(dateString)
                    {

                        // First check for the pattern
                        if (!/^\d{4}\/\d{1,2}\/\d{1,2}$/.test(dateString)) {
                            if (!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString)) {
                                $.notify(self.dateMessage());
                                return false;
                            }
                            return true;
                        }

                        // Parse the date parts to integers
                        var parts = dateString.split("/");
                        var month = parseInt(parts[1], 10);
                        var year = parseInt(parts[0], 10);
                        var day = parseInt(parts[2], 10);


                        // Check the ranges of month and year
                        if (year < 1300 || year > 3000 || month == 0 || month > 12) {
                            $.notify(self.dateMessage());
                            return false;
                        }

                        return true;


                    }
                    ;
                    function datevalidtion(d, d2) {
                        if (d >= d2) {
                            $.notify(self.dateMessage());
                            return false;
                        }
                        return true
                    }
                    ;

                    var s1 = self.supervisorAllowanceModel.startDate();
                    var d = Date.parse(s1);
                    var s2 = self.supervisorAllowanceModel.endDate();
                    var d2 = Date.parse(s2);
                    if (isValidDate(s1) && isValidDate(s2) && datevalidtion(d, d2)) {
                        var tracker = document.getElementById("tracker");
                        if (tracker.valid != "valid")
                        {
                            tracker.showMessages();
                            tracker.focusOn("@firstInvalidShown");
                            return;
                        }
                        var next = document.getElementById("train").getNextSelectableStep();
                        if (next != null)
                            self.currentStepValue(next);
                    }
                    ;
                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        $('#datePickerStartDate').calendarsPicker('disable');
                        $('#datePickerEndDate').calendarsPicker('disable');

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        $('#datePickerStartDate').calendarsPicker('enable');
                        $('#datePickerEndDate').calendarsPicker('enable');
                    }

                    return self.supervisorAllowance();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    var jsonData = ko.toJSON(self.supervisorAllowanceModel);
                    var getValidGradeCBF = function (data) {
                        var Data = data;
                        self.disableSubmit(false);
                        if (oj.Router.rootInstance._navHistory.length > 1) {
                            oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                        } else {
                            oj.Router.rootInstance.go('home');
                        }
                    };
                    services.addGeneric(commonhelper.supervisorallowanceUrl + self.pageMode(), jsonData).then(getValidGradeCBF, self.failCbFn);
                }
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.supervisorLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();
                self.supervisorLbl = ko.observable();
                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.superVisorScreenAddLbl = ko.observable();
                self.supervisorAllowanceScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.supervisorAllowance = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.dateMessage(getTranslation("labels.datemessage"));
                    self.gradeLbl(getTranslation("job.grade"));
                    self.supervisorLbl(getTranslation("additionalDetails.Supervisor"));
                    self.qualificationNameLbl(getTranslation("additionalDetails.qualificationName"));
                    self.supervisorLbl(getTranslation("additionalDetails.Supervisor"));
                    self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
                    self.cancel(getTranslation("others.cancel"));
                    self.superVisorScreenAddLbl(getTranslation("superVisorAllowance.supervisorAllowanceAdd"));
                    self.supervisorAllowanceScreenEditLbl(getTranslation("superVisorAllowance.supervisorAllowanceEdit"));
                    self.addMassageLbl(getTranslation("superVisorAllowance.addMessage"));
                    self.editMassageLbl(getTranslation("superVisorAllowance.editMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                    if (self.pageMode() == 'ADD') {
                        self.supervisorAllowance(self.superVisorScreenAddLbl());
                        self.oprationMessage(self.addMassageLbl());
                    } else if (self.pageMode() == 'EDIT') {
                        self.supervisorAllowance(self.supervisorAllowanceScreenEditLbl());
                        self.oprationMessage(self.editMassageLbl());
                    }
                }
                initTranslations();
            }
            return supervisorAllowanceOprationScreen;
        });
