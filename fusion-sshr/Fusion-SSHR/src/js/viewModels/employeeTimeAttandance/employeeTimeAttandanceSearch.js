define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber',
    'ojs/ojlabel', 'ojs/ojgauge', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup'],
    function (oj, ko, $, app, commonUtil, services) {
        function JobContentViewModel() {
            var self = this;
            self.columnArray = ko.observableArray();
            self.statusVal = ko.observable();
            self.statusArr = ko.observableArray();
            self.summaryObservableArray = ko.observableArray([]);
            self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, { idAttribute: 'personNumber' }));
            self.checkResultSearch = ko.observable();
            self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                pattern: 'dd/MM/yyyy'
            }));
            self.personNumberVal = ko.observable("");
            self.punch_dateval = ko.observable("");
            self.search = ko.observable();
            self.clear = ko.observable();
            self.checkResultSearch = ko.observable();
            self.punch_dateLbl = ko.observable();
            self.personNumberLbl = ko.observable();
            self.personNameLbl = ko.observable();
            self.statusLBL = ko.observable();
            self.first_punch_in_time = ko.observable();
            self.FIRST_PUNCH_OUT_TIME = ko.observable();
            self.SECOND_PUNCH_IN_TIME = ko.observable();
            self.SECOND_PUNCH_OUT_TIME = ko.observable();
            self.STATUS = ko.observable();
            self.MEASURE = ko.observable();
            self.RESPONE_DESC = ko.observable();
            self.REQUEST_BODY = ko.observable();


            self.searchPerson = function () {
                self.summaryObservableArray([]);
                self.searchReportName();
            };

            self.reset = function () {
                self.personNumberVal("");
                self.punch_dateval("");
                self.summaryObservableArray([]);
            };

            self.searchReportName = function () {

                let statusVal = self.statusVal() == "TRANSFERED TO SAAS" || self.statusVal() == "FAILED TO TRANSFER" ? self.statusVal() : "";
                if (statusVal == "" && !self.personNumberVal() && !self.punch_dateval()) {
                    app.messages.push({ severity: "error", summary: app.translate("common.selectOneAtLeast"), autoTimeout: 0 });//default time out
                    return;
                }
                self.summaryObservableArray([]);
                var payload = {
                    "status": statusVal,
                    "personNumber": self.personNumberVal(),
                    "date": self.punch_dateval()
                };


                var reportNameCodeCbFn = function (data) {
                    app.loading(false);
                    if (data.length !== 0) {

                        $.each(data, function (index) {
                            self.summaryObservableArray.push({
                                seq: index + 1,
                                punch_date: data[index].punch_date,
                                personNumber: data[index].personNumber,
                                first_punch_in_time: data[index].firstPunchInTime,
                                FIRST_PUNCH_OUT_TIME: data[index].firstPunchOutTime,
                                SECOND_PUNCH_IN_TIME: data[index].secondPunchInTime,
                                SECOND_PUNCH_OUT_TIME: data[index].secondPunchOutTime,
                                STATUS: data[index].status,
                                MEASURE: data[index].measure,
                                RESPONE_DESC: data[index].responseDescription,
                                REQUEST_BODY: data[index].requestBody,
                            });
                        });
                    } else {
                        $.notify(self.checkResultSearch(), "error");
                    }
                };
                var failCbFn = function () {
                    app.loading(false);
                };

                app.loading(true);
                setTimeout(() => {
                    var serviceName = "EmployeeTimeAttandance/getEmployeeAttendanceBySearch/";
                    services.addGeneric(serviceName, JSON.stringify(payload)).then(reportNameCodeCbFn, failCbFn);
                }, 100);
            };
            self.handleAttached = function (info) {
                app.loading(false);
                self.statusArr(app.getPaaSLookup('XXX_STATUS'));
            };

            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });
            function initTranslations() {
                self.search(app.translate("others.search"));
                self.clear(app.translate("others.clear"));
                self.checkResultSearch(app.translate("common.checkResultSearch"));

                self.punch_dateLbl(app.translate("common.punch_dateLbl"));
                self.personNumberLbl(app.translate("common.personNumberLbl"));
                self.personNameLbl(app.translate("common.personNameLbl"));
                self.statusLBL(app.translate("common.statusLBL"));
                self.first_punch_in_time(app.translate("common.first_punch_in_time"));
                self.FIRST_PUNCH_OUT_TIME(app.translate("common.FIRST_PUNCH_OUT_TIME"));
                self.SECOND_PUNCH_IN_TIME(app.translate("common.SECOND_PUNCH_IN_TIME"));
                self.SECOND_PUNCH_OUT_TIME(app.translate("common.SECOND_PUNCH_OUT_TIME"));
                self.STATUS(app.translate("common.STATUS"));
                self.MEASURE(app.translate("common.MEASURE"));
                self.RESPONE_DESC(app.translate("common.RESPONE_DESC"));
                self.REQUEST_BODY(app.translate("common.REQUEST_BODY"));

                self.columnArray([

                    {
                        "headerText": "#seq", "field": "seq"
                    },

                    {
                        "headerText": self.personNumberLbl(), "field": "personNumber"
                    },
                    {
                        "headerText": self.first_punch_in_time(), "field": "first_punch_in_time"
                    },
                    {
                        "headerText": self.FIRST_PUNCH_OUT_TIME(), "field": "FIRST_PUNCH_OUT_TIME"
                    },
                    {
                        "headerText": self.SECOND_PUNCH_IN_TIME(), "field": "SECOND_PUNCH_IN_TIME"
                    },
                    {
                        "headerText": self.SECOND_PUNCH_OUT_TIME(), "field": "SECOND_PUNCH_OUT_TIME"
                    },
                    {
                        "headerText": self.STATUS(), "field": "STATUS"
                    },
                    {
                        "headerText": self.MEASURE(), "field": "MEASURE"
                    },
                    {
                        "headerText": self.RESPONE_DESC(), "field": "RESPONE_DESC"
                    },
                    {
                        "headerText": self.REQUEST_BODY(), "field": "REQUEST_BODY"
                    }
                ]);
            }
            initTranslations();
        }
        return JobContentViewModel;
    });
