/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'config/buildScreen', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker','ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider', 'ojs/ojdialog'],
        function (oj, ko, $, app, commonUtil, services, buildScreen) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                //Vars
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var deptArray = [];
                var deptArray2 = [];
                var ArrKeys = [];
                var Keys = [];
                var disabeld = [];
                var naxtPagePram;
                var globalBulkTransactionNumber;

                //Observables
                self.lang = "";
                self.serchableFeieldName = [];
                self.sercheScreen = [];
                self.Arrs = {};
                self.ListSegment = [];
                self.columnArray = ko.observableArray();
                self.columnArray1 = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.reportNameLbl = ko.observable();
                self.parameter1Lbl = ko.observable();
                self.parameter2Lbl = ko.observable();
                self.parameter3Lbl = ko.observable();
                self.parameter4Lbl = ko.observable();
                self.parameter5Lbl = ko.observable();
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.summaryArrayDetails = ko.observableArray();
                self.summaryBulkTransaction = ko.observableArray(deptArray);
                self.deptObservableArray = ko.observableArray(deptArray2);
                self.isDisable = ko.observable(true);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'transactionnumber'}));
                self.dataSourceTB2 = new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'});
                self.checkResultSearch = ko.observable();
                self.personNumberLbl = ko.observable();
                self.businessType1Lbl = ko.observable();
                self.fromCountryLbl = ko.observable();
                self.fromCityLbl = ko.observable();
                self.toCountryLbl = ko.observable();
                self.toCityLbl = ko.observable();
                self.daysBeforeLbl = ko.observable();
                self.missionDaysLbl = ko.observable();
                self.fromDateLbl = ko.observable();
                self.toDateLbl = ko.observable();
                self.housingLbl = ko.observable();
                self.transpositionLbl = ko.observable();
                self.accommodationLbl = ko.observable();
                self.ticketLbl = ko.observable();
                self.daysAfterLbl = ko.observable();
                self.outNumberLbl = ko.observable();
                self.outDateLbl = ko.observable();
                self.detailsLbl = ko.observable();
                self.bulkTransactionIdLbl = ko.observable();
                self.Deatils = ko.observable();
                self.ok = ko.observable();

                self.btnAddSummary = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store(naxtPagePram);
                    oj.Router.rootInstance.go('dynamicOperationScreenBulkTransaction');
                };

                self.tableSelectionListener = function () {
                    self.isDisable(false);
                };

                self.getSummary = function () {
                    var getReportCbFn = function (data) {
                        if (data.length !== 0) {
                            var peiAttribute;
                            for (var i = 0; i < data.length; i++) {
                                if (peiAttribute !== data[i].PEI_ATTRIBUTE1) {
                                    self.summaryObservableArray.push({
                                        bulkTransactionId: data[i].id,
                                        transactionnumber: data[i].PEI_ATTRIBUTE1
                                    });
                                }
                                self.summaryArrayDetails.push({
                                    transactionnumber: data[i].PEI_ATTRIBUTE1,
                                    eit: JSON.parse(data[i].eit)
                                });
                                peiAttribute = data[i].PEI_ATTRIBUTE1;
                            }
                        } else {
                            //$.notify(self.checkResultSearch(), "error");
                        }

                    };
                    var failCbFn = function () {
                        //$.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getAllSummaryBulk;
                    services.getAllSummary(serviceName + "/" + oj.Router.rootInstance.retrieve()).then(getReportCbFn, failCbFn);
                };


                self.closeDialog = function () {
                    self.deptObservableArray([]);
                    self.summaryBulkTransaction([]);
                    $("#modalDialog1").ojDialog("close");
                };

                self.buttonClick = function () {
                    self.deptObservableArray([]);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    globalBulkTransactionNumber = self.summaryObservableArray()[currentRow['rowIndex']].transactionnumber;
                    for (var i = 0; i < self.summaryArrayDetails().length; i++) {
                        if (self.summaryArrayDetails()[i].transactionnumber == globalBulkTransactionNumber) {
                            self.summaryArrayDetails()[i].eit.id = i;
                            self.deptObservableArray.push(self.summaryArrayDetails()[i].eit);
                        }
                    }
                    self.dataSourceTB2 = new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'});
                    document.querySelector("#modalDialog1").open();
                };

                //For draw table
                var getEit = function (data) {
                    var Iterater = 0;
                    tempObject = data;
                    if (!tempObject.length) {
                        tempObject = [tempObject];
                    }
                    var sercheFeiled = 0;
                    for (var i = 0; i < tempObject.length; i++) {
                        if (tempObject[i].DISPLAY_FLAG == "Y") {

                            if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS")) {

                                if (tempObject[i].value_SET_TYPE == "X") {
                                    var listObject = {"Name": tempObject[i].DESCRIPTION,
                                        "listName": tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"};
                                    self.ListSegment.push(listObject);
                                    self.columnArray1.push({
                                        "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION + "Lbl"});
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var getReportCBCF = function (data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        var arrays = [];
                                        for (var x = 0; x < arr.length; x++) {
                                            arrays.push({"value": arr[x].FLEX_value, "label": arr[x].FLEX_value})
                                        }
                                        self.Arrs[ArrKeys[Iterater]](arrays);
                                    };
                                    var xx = {"reportName": "inDependentReport", "valueSet": tempObject[i].FLEX_value_SET_NAME};
                                    services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                                    Iterater = Iterater + 1;
                                } else if (tempObject[i].value_SET_TYPE == "Y") {
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    Iterater = Iterater + 1;
                                } else if (tempObject[i].value_SET_TYPE == "F") {
                                    var listObject = {"Name": tempObject[i].DESCRIPTION,
                                        "listName": tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"};
                                    self.ListSegment.push(listObject);
                                    self.columnArray1.push({
                                        "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION + "Lbl"});
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var getDynamicReport = function (data) {
                                        var val = "value";
                                        var lbl = "label";
                                        var q;
                                        var tempObject = data;
                                        q = "select " + tempObject.ID_COL + " " + val + " ,  " + tempObject.value_COL + "  " + lbl
                                                + " From " + tempObject.APPLICATION_TABLE_NAME + "  " + tempObject.ADDITIONAL_WHERE_CLAUSE;


                                        var str = q;
                                        var flex = ":";
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':') == -1) {
                                            is_HaveFlex = false;
                                        }
                                        while (is_HaveFlex) {
                                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                            var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));

                                            str = str.replace(strWitoutFlex, rootViewModel.personDetails().personId);
                                            if (str.indexOf(":") == -1) {
                                                is_HaveFlex = false;
                                            }
                                        }
                                        if (str.includes('undefined')) {
                                        } else {
                                            //Here Must Call Report Fpr This Perpus 
                                            var getArrCBCF = function (data) {
                                                var tempObject2 = data;
                                                var arr = tempObject2;
                                                if (tempObject2.length) {
                                                    self.Arrs[ArrKeys[Iterater]](arr);
                                                } else {
                                                    self.Arrs[ArrKeys[Iterater]]([arr]);
                                                }
                                            };
                                            services.getDynamicReport(str).then(getArrCBCF, app.failCbFn);

                                        }



                                    };
                                    services.getQueryForListReport(tempObject[i].FLEX_value_SET_NAME).then(getDynamicReport, app.failCbFn);
                                    Iterater = Iterater + 1;
                                }

                            } else {
                                self.columnArray1.push({
                                    "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION});
                            }
                        }
                        if (self.serchableFeieldName[sercheFeiled]) {
                            if (self.serchableFeieldName[sercheFeiled].rowName == tempObject[i].APPLICATION_COLUMN_NAME) {

                                self.sercheScreen.push(tempObject[i]);
                                Keys.push(tempObject[i].DESCRIPTION);
                                self.model[Keys[sercheFeiled]] = ko.observable();
                                //Build Disabeld Model 
                                disabeld.push("isDisabled" + tempObject[i].DESCRIPTION);
                                if (tempObject[i].ENABLED_FLAG == "Y") {
                                    self.isDisabledx[disabeld[i]] = ko.observable(false);
                                } else if (tempObject[i].ENABLED_FLAG == "N") {
                                    self.isDisabledx[disabeld[i]] = ko.observable(true);
                                }
                                //End Of Disabeld Model 
                                if (self.serchableFeieldName.length - 1 > sercheFeiled) {
                                    sercheFeiled++;
                                    self.hideSearch(true);
                                }
                            }
                        }

                    }
                    buildScreen.buildScreen(self.sercheScreen, "xx", self.model, self.isDisabledx, self.dataArray);
                };
                if (app.getLocale() == 'ar') {
                    self.lang = "AR";
                } else {
                    self.lang = "US";
                }

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.summaryArrayDetails([]);
                    self.columnArray1([]);
                    self.deptObservableArray([]);
                    self.EitCode = oj.Router.rootInstance.retrieve();
                    services.getEIT(self.EitCode, self.lang).then(getEit, app.failCbFn);
                    naxtPagePram = {eitCoed: oj.Router.rootInstance.retrieve(), pageMode: "ADD"};
                    self.getSummary();
                };

                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }

                });


                function initTranslations() {
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.personNumberLbl(getTranslation("report.personNumber"));
                    self.businessType1Lbl(getTranslation("report.businessType"));
                    self.fromCountryLbl(getTranslation("report.fromCountry"));
                    self.fromCityLbl(getTranslation("report.fromCity"));
                    self.toCountryLbl(getTranslation("report.toCountry"));
                    self.toCityLbl(getTranslation("report.toCity"));
                    self.daysBeforeLbl(getTranslation("report.daysBefore"));
                    self.missionDaysLbl(getTranslation("report.missionDays"));
                    self.fromDateLbl(getTranslation("report.fromDate"));
                    self.toDateLbl(getTranslation("report.toDate"));
                    self.housingLbl(getTranslation("report.housing"));
                    self.transpositionLbl(getTranslation("report.transposition"));
                    self.accommodationLbl(getTranslation("report.accommodation"));
                    self.ticketLbl(getTranslation("report.ticket"));
                    self.daysAfterLbl(getTranslation("report.daysAfter"));
                    self.outNumberLbl(getTranslation("report.outNumber"));
                    self.outDateLbl(getTranslation("report.outDate"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.bulkTransactionIdLbl(getTranslation("report.bulkTransactionId"));
                    self.detailsLbl(getTranslation("report.details"));
                    self.Deatils(getTranslation("report.details"));
                    self.ok(getTranslation("report.ok"));

                    self.columnArray([
                        {
                            "headerText": self.bulkTransactionIdLbl(), "field": "transactionnumber"

                        },
                        {
                            "headerText": self.detailsLbl(), "template": "detailsCellTemplate"
                        }
                    ]);

                }
                initTranslations();
            }
            return JobContentViewModel;
        });
