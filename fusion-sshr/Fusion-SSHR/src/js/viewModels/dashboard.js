define(['ojs/ojcore', 'knockout', 'jquery', 'appController','config/services', 'ojs/ojavatar'],
        function (oj, ko, $, app, services) {

            function IncidentsViewModel() {
                //Vars
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;

                //Observables
                self.empName = ko.observable();
                self.localePublicProfile = ko.observable();
                self.localeWorkerDirectory = ko.observable();
                self.localePayslip = ko.observable();
                self.localeAbsences = ko.observable();
                self.eitIconsLoaded = ko.observable(true);
                // self.tempIcons = ko.observableArray([]);
                // self.tempIcons().length = 6;
                self.valueofsharethouts = ko.observable("Share thoughts with colleagues..");
                self.localeSelfServices = ko.observable();
                self.localeBIReports = ko.observable();
                self.localeTimeCard = ko.observable();
                self.icons = ko.observableArray([]);
                self.personDetails = ko.observable(app.personDetails());
                self.followersLbl = ko.observable();
                self.followingLbl = ko.observable();
                self.employeeNewsLbl = ko.observable();
                self.myFlagsLbl = ko.observable();
                var BtnVisible=true;
                self.countPending = ko.observable();
                self.countApproved = ko.observable();
                self.countRejected = ko.observable();
                self.avatarSize = ko.observable("md");
                self.firstName = ko.observable();
                self.lastName = ko.observable();
                self.initials = ko.observable();
                self.image = ko.observable();
                self.LeftMenu = ko.observable();
                self.iconNavigation = function (router, code) {
                    oj.Router.rootInstance.store(code);
                    oj.Router.rootInstance.go(router);
                    return true;
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                
                // self.getCountEITRequest = function (info) {
                //                     var getCountEITRequestCBF = function (data) {
                //                         self.countPending(data.countPendingApproved);
                //                         self.countApproved(data.countApproved);
                //                         self.countRejected(data.countRejected);

                //     };
                //     services.getGeneric("PeopleExtra/countRequest/" + self.personDetails().personId).then(getCountEITRequestCBF, app.failCbFn);
                // };

                self.computeIconsArray = ko.computed(function () {
                    if (app.conutEitRequest()) {
                            self.countPending(app.conutEitRequest().countPendingApproved);
                            self.countApproved(app.conutEitRequest().countApproved);
                            self.countRejected(app.conutEitRequest().countRejected);
                        }
                    if (app.personDetails()) {
                        self.firstName(app.personDetails().firstName);
                        self.lastName(app.personDetails().lastName);
                        self.initials(oj.IntlConverterUtils.getInitials(self.firstName(),self.lastName()));
                        //condition to recompute to get the second image
                        //if (app.globalEmployeeEITLookup().length > 1){}
//                        if (typeof $(".profilepic")[0] !== 'undefined' && typeof $(".profilepic")[1] !== 'undefined') {
//                            if (app.personDetails().picBase64 && app.personDetails().picBase64 !== null) {
//                                $(".profilepic")[0].src = "data:image/png;base64," + app.personDetails().picBase64;
//                                $(".profilepic")[1].src = "data:image/png;base64," + app.personDetails().picBase64;
//                            } else {
//                                $(".profilepic")[0].src = "css/images/avatar_24px.png";
//                                $(".profilepic")[1].src = "css/images/avatar_24px.png";
//                            }
//                        }
                             if (self.personDetails().picBase64 === undefined || self.personDetails().picBase64 === null || self.personDetails().picBase64 === "") {
                            self.image('');
                        } else {
                            self.image("data:image/png;base64," + self.personDetails().picBase64);

                        }
                        if (self.personDetails().picBase64) {
                            if ($(".profilepic")[0]) {
                                $(".profilepic")[0].src = "data:image/png;base64," + self.personDetails().picBase64;
                            }
                            if ($(".profilepic")[1]) {
                                $(".profilepic")[1].src = "data:image/png;base64," + self.personDetails().picBase64;
                            }
                        } else {
                            if ($(".profilepic")[0]) {
                                $(".profilepic")[0].src = "css/images/avatar_24px.png";
                            }
                            if ($(".profilepic")[1]) {
                                $(".profilepic")[1].src = "css/images/avatar_24px.png";
                            }
                        }

                        self.personDetails(app.personDetails());
//                            self.icons([
////                            {
////                                label: 'Public Profile', value: 'demo-icon-circle-a', router: 'publicProfile',
////                                iconType: 'fa fa-user-circle', text: self.localePublicProfile(), visible: true, code: "publicProfile"
////                            },
////                            {
////                                label: 'Worker Directory', value: 'demo-icon-circle-a', router: 'workerDirectory',
////                                iconType: 'fa fa-users', text: self.localeWorkerDirectory(), visible: true, code: "workerDirectory"
////                            },
////                            {
////                                label: 'Payslip', value: 'demo-icon-circle-a', router: 'payslip',
////                                iconType: 'fa fa-credit-card', text: self.localePayslip(), visible: true, code: "payslip"
////                            },
////                            {
////                                label: 'Absences', value: 'demo-icon-circle-a', router: 'viewAbsence',
////                                iconType: 'fa fa-briefcase', text: self.localeAbsences(), visible: true, code: "absences"
////                            },
////                            {
////                                label: 'Time Card', value: 'demo-icon-circle-a', router: 'timeCard',
////                                iconType: 'fa fa-address-card', text: self.localeTimeCard(), visible: true, code: "timeCard"
////                            },
//                                {
//                                    label: 'Self Services', value: 'demo-icon-circle-a', router: 'home',
//                                    iconType: 'fa fa-taxi', text: self.localeSelfServices(), visible: true, code: "home"
//                                }
////                            {
////                                label: 'Report', value: 'demo-icon-circle-a', router: 'report',
////                                iconType: 'fa fa-file', text: self.localeBIReports(), visible: true, code: "report"
////                            }
//                            ]);
                        //self.eitIconsLoaded(true);
                    }

                   
                    

                });

                self.handleAttached = function () {
                    
                    self.LeftMenu(app.hideLeftMenu());
                    
//                    var isMobile = {
//                        Android: function () {
//                            return navigator.userAgent.match(/Android/i);
//                        },
//                        BlackBerry: function () {
//                            return navigator.userAgent.match(/BlackBerry/i);
//                        },
//                        iOS: function () {
//                            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
//                        },
//                        Opera: function () {
//                            return navigator.userAgent.match(/Opera Mini/i);
//                        },
//                        Windows: function () {
//                            return navigator.userAgent.match(/IEMobile/i);
//                        },
//                        any: function () {
//                            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
//                        }
//                    };
//                    if(isMobile.any()) {
//                        // It is mobile
//                        
//                        BtnVisible=true;
//                      }else{
//                          //It is web application
//                          
//                        oj.Router.rootInstance.go('home');
//                        BtnVisible=false;  
//                      }
                    app.loading(false);
//                     if(app.personDetails().picBase64===undefined || app.personDetails().picBase64 ===null ||app.personDetails().picBase64 ===""){
//                        self.image('');
//                   }else{
//                       self.image("data:image/png;base64," + app.personDetails().picBase64);
//                         
//                   }
                    if (rootViewModel.personDetails()){
                        self.empName(app.empName());
                        if (typeof $(".profilepic")[0] !== 'undefined' && typeof $(".profilepic")[1] !== 'undefined') {
                            if (rootViewModel.personDetails().picBase64 && rootViewModel.personDetails().picBase64 !== null) {
                                $(".profilepic")[0].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                                $(".profilepic")[1].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                            } else {
                                $(".profilepic")[0].src = "css/images/avatar_24px.png";
                                $(".profilepic")[1].src = "css/images/avatar_24px.png";
                            }
                        }
                    }
                    initTranslations();
                   //   self.empName(rootViewModel.personDetails().displayName);
                };

                function initTranslations() {
                    if (rootViewModel.personDetails())
                        self.empName(app.empName());
                    self.localePublicProfile(getTranslation('pages.publicProfile'));
                    self.localeWorkerDirectory(getTranslation('pages.workerDirectory'));
                    self.localePayslip(getTranslation('pages.payslip'));
                    self.localeAbsences(getTranslation('pages.absences'));
                    self.localeSelfServices(getTranslation('pages.selfServices'));
                    self.localeBIReports(getTranslation('pages.report'));
                    self.localeTimeCard(getTranslation('pages.timeCard'));
                    self.icons([
//                            {
//                                label: 'Public Profile', value: 'demo-icon-circle-a', router: 'publicProfile',
//                                iconType: 'fa fa-user-circle', text: self.localePublicProfile(), visible: true, code: "publicProfile"
//                            },
//                            {
//                                label: 'Worker Directory', value: 'demo-icon-circle-a', router: 'workerDirectory',
//                                iconType: 'fa fa-users', text: self.localeWorkerDirectory(), visible: true, code: "workerDirectory"
//                            },
//                            {
//                                label: 'Payslip', value: 'demo-icon-circle-a', router: 'payslip',
//                                iconType: 'fa fa-credit-card', text: self.localePayslip(), visible: true, code: "payslip"
//                            },
//                            {
//                                label: 'Absences', value: 'demo-icon-circle-a', router: 'viewAbsence',
//                                iconType: 'fa fa-briefcase', text: self.localeAbsences(), visible: true, code: "absences"
//                            },
//                            {
//                                label: 'Time Card', value: 'demo-icon-circle-a', router: 'timeCard',
//                                iconType: 'fa fa-address-card', text: self.localeTimeCard(), visible: true, code: "timeCard"
//                            },
                        {
                            label: 'Self Services', value: 'demo-icon-circle-a', router: 'home',
                            iconType: 'css/images/icons/self_service_01.png', text: self.localeSelfServices(), visible: BtnVisible, code: "home"
                        }
//                            {
//                                label: 'Report', value: 'demo-icon-circle-a', router: 'report',
//                                iconType: 'fa fa-file', text: self.localeBIReports(), visible: true, code: "report"
//                            }
                    ]);
                    self.valueofsharethouts(getTranslation('others.valueofsharethouts'));
                    self.followingLbl(getTranslation('others.following'));
                    self.followersLbl(getTranslation('others.Followers'));
                    self.employeeNewsLbl(getTranslation('others.employeeNews'));
                    self.myFlagsLbl(getTranslation("others.myFlags"));
                }
            }

            return new IncidentsViewModel();
        }
);