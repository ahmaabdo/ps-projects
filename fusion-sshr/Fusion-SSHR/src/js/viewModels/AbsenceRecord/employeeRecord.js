define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber',
    'ojs/ojlabel', 'ojs/ojgauge', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup','ojs/ojmessages'],
        function (oj, ko, $, app, commonUtil, services) {
            function employeeRecordContentViewModel() {
                var self = this;
                self.lblInVacation = ko.observable();
                self.lblNoVacation = ko.observable();
                self.lblNoPunchIn = ko.observable();
                self.lblNoPunchOut = ko.observable();
                self.lblNoPunchOutOrIn = ko.observable();
                
                self.columnArray = ko.observableArray();

                self.summaryObservableArray = ko.observableArray([]);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'personNumber'}));
                self.checkResultSearch = ko.observable();
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'
                }));

                self.absenceArr = ko.observableArray([]);
                self.absenceLbl = ko.observable();
                self.absenceVal = ko.observable();
                self.absenceArr = ko.observableArray();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.startDate = ko.observable();
                self.endDate = ko.observable();
                self.totalLbl = ko.observable();
                self.totalVal = ko.observable(0);
                self.search = ko.observable();
                self.clear = ko.observable();
                self.checkResultSearch = ko.observable();
                self.isAbsence = ko.observable();
                self.actual = ko.observable("");
                self.personNumberLbl = ko.observable();
                self.id = ko.observable();
                self.deviceintime = ko.observable();
                self.checkedintime = ko.observable();
                self.deviceouttime = ko.observable();
                self.checkedouttime = ko.observable();
                self.lateminutes = ko.observable();
                self.Day = ko.observable();
                self.officialWorking = ko.observable();
                self.actualWorking = ko.observable();
                self.employeeRecordLbl = ko.observable();
                self.placeholder = ko.observable();
                self.punch = ko.observable("No");
                self.Date = ko.observable();
                self.tableHeader = ko.observable();
                self.personName = ko.observable(app.personDetails().displayName);
                self.lateminutesVal=ko.observable(0);
                self.Date = ko.observable();
                self.Names = ko.observableArray();
                self.TableArr = ko.observableArray();
                self.visiData = ko.observable();
                self.visiNoOutPunch = ko.observable();
                self.min = ko.observable();
                self.hoursMinArr = ko.observableArray();              
                self.cheakSearch = ko.observable(false);
                self.errorDate = ko.observable();
                self.errorDateMsg = ko.observable();
                self.messages = ko.observableArray();
                self.SignInOrOut = ko.observable();
                self.SignIn = ko.observable();
                self.SignOut = ko.observable();
                self.inVacation = ko.observable();
                self.fromLbl = ko.observable();

                self.searchPerson = function () {
                    self.summaryObservableArray([]);
                    self.searchReportName();
                };


                self.reset = function () {
                    self.startDate("");
                    self.endDate("");
                    self.totalVal(0);
                    self.absenceVal('');
                    self.getAllAbsenceForEMP();
                };

                self.absenceValueChangeHandler = function (event) {
                    self.isAbsence("");                    
                    self.punch("No");
                    if (self.absenceVal()) {
                        if (self.absenceVal() == "No vacation") {
                            self.isAbsence("N");                          
                        } else if (self.absenceVal() == "In vacation") {
                            self.isAbsence("Y");  
                        }else{
                           self.punch(self.absenceVal());
                           self.isAbsence(""); 
                        }

                    }
                };
                
                function validDate(){
                    if (self.startDate() && self.endDate()) {
                        var date = new Date(self.startDate());
                        
                        var date2 = new Date(self.endDate());
                        var diffTime = Math.abs(date2 - date);
                        var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                      
                        if(diffDays > 30){
                            self.messages.push(
                                {
                                    severity: 'error',
                                    summary: self.errorDate(),
                                    detail: self.errorDateMsg()
                                });
                            self.startDate("");
                            self.endDate("");
                        }                      
                    }
                }
                self.startDateChange = function () {
                    validDate();
                };
                self.endDateChange = function () {
                    validDate();
                };

                self.searchReportName = function () {
                    self.cheakSearch(true);
                    self.totalVal(0);
                    self.lateminutesVal(0)
                    self.summaryObservableArray([]);
                    var payload = {
                        "punch": self.punch(),
                        "personNumber": app.personDetails().personNumber,
                        "startDate": self.startDate(),
                        "endDate": self.endDate(),
                        "isAbsence": self.isAbsence(),

                    };


                    var reportNameCodeCbFn = function (data) {
                        app.loading(false);
                        if (data.length !== 0) {
                            self.cheakSearch(true);

                            $.each(data, function (index) {
                                self.summaryObservableArray.push({
                                    personNumber: data[index].personNumber,
                                    personName: data[index].personName,
                                    personData:data[index].personData

                                });   
                            });

                        tableValidationData();

                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }


                    };
                    var failCbFn = function () {
                        app.loading(false);
                    };

                    app.loading(true);
                    setTimeout(() => {
                        var serviceName = "AbsenceRecord/getPunchinOrPunchout/test";
                        services.addGeneric(serviceName, JSON.stringify(payload)).then(reportNameCodeCbFn, failCbFn);
                    }, 100);
                };

                self.getAllAbsenceForEMP = function () {                    
                    self.cheakSearch(false);
                    self.summaryObservableArray([]);
                    var getDataCbFn = function (data) {
                        if (data.length !== 0) {
                            
                            $.each(data, function (index) {
                                self.summaryObservableArray.push({
                                    personNumber: data[index].personNumber,
                                    personName: data[index].personName,
                                    personData:data[index].personData

                                });

                            });
                         tableValidationData();
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    
                    }
                    var serviceName = "AbsenceRecord/getAbsenceByEmployee/" + app.personDetails().personNumber;
                    services.getGeneric(serviceName).then(getDataCbFn, app.failCbFn())
                }

                self.handleAttached = function (info) {
                    self.getAllAbsenceForEMP();
                    app.loading(false);
                     self.absenceArr(app.getPaaSLookup('absence'));
                };
                                function tableValidationData() {
                    self.TableArr([]);
                    self.Names([]);
                    // for all rows


                    for (var j in self.summaryObservableArray()) {

                        

                        var personDetails = self.summaryObservableArray()[j].personData;
                        var total = 0;
                        var min = 0;
                        for (var h in personDetails) {
                                    
                             if (personDetails[h].isAbsence == "Y") {
                                personDetails[h].visiData = false;
                                personDetails[h].visiNoOutPunch = true;
                                personDetails[h].checkedInTime = '';
                                personDetails[h].deviceInTime = '';
                                personDetails[h].checkedOutTime = '';
                                personDetails[h].deviceOutTime = '';
                               typeof personDetails[h].actualWorkingHours  == 'undefined' ? personDetails[h].actualWorkingHours='0':'';
                                typeof personDetails[h].lateMinutes == 'undefined' ? personDetails[h].lateMinutes = '0' : '';

                                typeof personDetails[h].officialWorkingHours == 'undefined' ? personDetails[h].officialWorkingHours = '0' : '';
                                personDetails[h].label = self.inVacation();
                            } else if (typeof personDetails[h].checkedInTime != 'undefined' && typeof personDetails[h].checkedOutTime == 'undefined') {
                                personDetails[h].visiData = false;
                                personDetails[h].visiNoOutPunch = true;
                                personDetails[h].deviceInTime = '';
                                personDetails[h].checkedOutTime = '';
                                personDetails[h].deviceOutTime = '';
                                typeof personDetails[h].lateMinutes == 'undefined' ? personDetails[h].officialWorkingHours = '0' : '';
                                typeof personDetails[h].officialWorkingHours == 'undefined' ? personDetails[h].lateMinutes = '0' : ''
                               typeof personDetails[h].actualWorkingHours  == 'undefined' ? personDetails[h].actualWorkingHours='0':'';

                                personDetails[h].label = self.SignOut();
                            } else if (typeof personDetails[h].checkedInTime == 'undefined' && typeof personDetails[h].checkedOutTime != 'undefined') {
                                personDetails[h].visiData = false;
                                personDetails[h].visiNoOutPunch = true;
                                personDetails[h].checkedInTime = '';
                                personDetails[h].deviceInTime = '';
                                personDetails[h].deviceOutTime = '';
                                typeof personDetails[h].officialWorkingHours == 'undefined' ? personDetails[h].officialWorkingHours = '0' : ''
                                typeof personDetails[h].lateMinutes == 'undefined' ? personDetails[h].lateMinutes = '0' : '';
                               typeof personDetails[h].actualWorkingHours  == 'undefined' ? personDetails[h].actualWorkingHours='0':'';

                                personDetails[h].label = self.SignIn();
                            } else if (typeof personDetails[h].checkedInTime == 'undefined' || typeof personDetails[h].checkedOutTime == 'undefined'
                                    || typeof personDetails[h].officialWorkingHours == 'undefined' || typeof personDetails[h].deviceInTime == 'undefined'
                                    || typeof personDetails[h].deviceOutTime == 'undefined' || typeof personDetails[h].actualWorkingHours == 'undefined' 
                                    || typeof personDetails[h].lateMinutes == 'undefined') {
                                personDetails[h].visiData = false;
                                personDetails[h].visiNoOutPunch = true;
                                personDetails[h].checkedInTime = '';
                                personDetails[h].deviceInTime = '';
                                personDetails[h].checkedOutTime = '';
                                personDetails[h].deviceOutTime = '';
                               typeof personDetails[h].actualWorkingHours  == 'undefined' ? personDetails[h].actualWorkingHours='0':'';

                                typeof personDetails[h].officialWorkingHours == 'undefined' ? personDetails[h].officialWorkingHours = '0' : ''
                                typeof personDetails[h].lateMinutes == 'undefined' ? personDetails[h].lateMinutes = '0' : '';

                                personDetails[h].label = self.SignInOrOut();

                            } else {
                                self.visiNoOutPunch(false);
                                self.visiData(true);
                                personDetails[h].visiData = true;
                                personDetails[h].visiNoOutPunch = false;
                                personDetails[h].label = '';
                            }


                            if (typeof personDetails[h].actualWorkingHours != 'undefined') {
                                var s = personDetails[h].actualWorkingHours.replace(":", ".");
                                total += parseFloat(s);
                                var n=total.toFixed(2);
                                    min =  40 - n ;
                                    var m=min.toFixed(2);

                            }
                        }
                       
                       
                        self.Names.push({personName: self.summaryObservableArray()[j].personName
                            , personData: personDetails, personHead: [{Day: self.Day(),
                                    Date: self.Date(),
                                    checkedintime: self.checkedintime(),
                                    deviceintime: self.deviceintime(),
                                    checkedouttime: self.checkedouttime(),
                                    deviceouttime: self.deviceouttime(),
                                    officialWorking: self.officialWorking(),
                                    actualWorking: self.actualWorking(),
                                    lateminutes: self.lateminutes()}], visiData: self.visiData(), visiNoOutPunch: self.visiNoOutPunch(),
                            totalLbl: self.totalLbl(), hours: n, min: m,from:self.fromLbl()
                        });

                    }




                }

                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {

                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.startDateLbl(getTranslation("absenceRecord.startDate"));
                    self.endDateLbl(getTranslation("absenceRecord.endDate"));
                    self.totalLbl(getTranslation("absenceRecord.totalOfficialWorkingHours"));
                    self.id(getTranslation("absenceRecord.id"));
                    self.deviceintime(getTranslation("absenceRecord.deviceintime"));
                    self.checkedintime(getTranslation("absenceRecord.checkedintime"));
                    self.deviceouttime(getTranslation("absenceRecord.deviceouttime"));
                    self.checkedouttime(getTranslation("absenceRecord.checkedouttime"));
                    self.lateminutes(getTranslation("absenceRecord.lateminutes"));
                    self.Day(getTranslation("absenceRecord.day"));
                    self.officialWorking(getTranslation("absenceRecord.officialWorkingHours"));
                    self.actualWorking(getTranslation("absenceRecord.actualWorkingHours"));
                    self.absenceLbl(getTranslation("absenceRecord.absence"));
                    self.employeeRecordLbl(getTranslation("pages.employeeRecord"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.Date(getTranslation("absenceRecord.date"));
                    self.tableHeader(getTranslation("absenceRecord.tableHeader"));
                    self.errorDate(getTranslation("absenceRecord.dateError"));
                    self.errorDateMsg(getTranslation("absenceRecord.dateErrorMsg"));
                    self.lblInVacation(getTranslation("absenceRecord.lblInVacation"));
                    self.lblNoVacation(getTranslation("absenceRecord.lblNoVacation"));
                    self.lblNoPunchIn(getTranslation("absenceRecord.lblNoPunchIn"));
                    self.lblNoPunchOut(getTranslation("absenceRecord.lblNoPunchOut"));
                    self.lblNoPunchOutOrIn(getTranslation("absenceRecord.lblNoPunchOutOrIn"));
                    self.SignInOrOut(getTranslation("absenceRecord.SignInOrOut"));
                    self.SignIn(getTranslation("absenceRecord.SignIn"));
                    self.SignOut(getTranslation("absenceRecord.SignOut"));
                    self.inVacation(getTranslation("absenceRecord.inVacation"));
                    self.fromLbl(getTranslation("absenceRecord.from"));


                    
                }
                initTranslations();
            }
            return employeeRecordContentViewModel;
        });
