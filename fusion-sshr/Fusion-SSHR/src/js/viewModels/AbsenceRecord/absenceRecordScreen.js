/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * absenceRecordScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'knockout-mapping',
    'ojs/ojmodel', 'ojs/ojknockout',
    'ojs/ojknockout-model', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox',
    'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup',
    'ojs/ojknockout-validation', 'ojs/ojfilmstrip', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset',
    'ojs/ojradioset', 'ojs/ojlabel', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'jquery-calendar-picker',
    'ojs/ojinputtext', 'ojs/ojavatar'
], function (oj, ko, $, app) {
    /**
     * The view model for the main content view template
     */
    function absenceRecordScreenContentViewModel() {
        var self = this;
        self.valueofsharethouts = ko.observable("Share thoughts with colleagues..");
        self.agreement = ko.observableArray();
        self.currentColor = ko.observable("red");
        var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
        var getTranslation = oj.Translations.getTranslatedString;
        self.empName = ko.observable();
        self.nam = ko.observable();
        self.employeeRecordLbl = ko.observable();
        self.managerSpecialistRecordLbl = ko.observable();
        self.countPending = ko.observable();
        self.countApproved = ko.observable();
        self.countRejected = ko.observable();
        self.avatarSize = ko.observable("md");
        self.firstName = ko.observable();
        self.lastName = ko.observable();
        self.initials = ko.observable();
        self.image = ko.observable();
        var checkUser=false;
        var userRoles;
        self.visible=ko.observable();
        self.error = function () {
            app.hasErrorOccured(true);
            app.router.go("error");
        };



        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
//                app.showPreloader();
                initTranslations();
//                app.hidePreloader();
            }
        });
        self.icons = ko.observableArray([]);

        self.computeIconsArray = ko.computed(function () {
            if (rootViewModel.personDetails()) {
                self.firstName(app.personDetails().firstName);
                self.lastName(app.personDetails().lastName);
                self.initials(oj.IntlConverterUtils.getInitials(self.firstName(), self.lastName()));
                self.icons([
                            
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'employeeRecord', iconType: 'fa fa-user', text: self.employeeRecordLbl(), visible: true, code: "Job"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'managerAndSpecialistRecord', iconType: 'fa fa-users', text: self.managerSpecialistRecordLbl(), visible: self.visible(), code: "Report",
                            }
                        ]);
            }
        });

        self.pagingModel = null;

        self.loading = function () {
            app.loading(true);
        };

        self.iconNavigation = function (router, code) {
            app.loading(true);
            oj.Router.rootInstance.store(code);
            oj.Router.rootInstance.go(router);
            return true;
        };

        getPagingModel = function () {
            if (!self.pagingModel) {
                var filmStrip = $("#filmStrip");
                var pagingModel = filmStrip.ojFilmStrip("getPagingModel");
                self.pagingModel = pagingModel;
            }
            return self.pagingModel;
        };
        self.handleAttached = function (info) {
            app.loading(false);
            
            console.log(app.accessRoleName());
            if (app.conutEitRequest()) {
                self.countPending(app.conutEitRequest().countPendingApproved);
                self.countApproved(app.conutEitRequest().countApproved);
                self.countRejected(app.conutEitRequest().countRejected);
            }
            if (app.personDetails().picBase64 === undefined || app.personDetails().picBase64 === null || app.personDetails().picBase64 === "") {
                self.image('');
            } else {
                self.image("data:image/png;base64," + app.personDetails().picBase64);

            }
            
            userRoles = JSON.parse(sessionStorage.getItem("userRole"));
            for (var x = 0; x < userRoles.length; x++) {
                for (var i = 0; i < app.accessRoleName().length; i++) {
                    if (userRoles[x].ROLE_NAME == app.accessRoleName()[i].roleName) {
                        self.visible(true);
                    } 
                }
                    if ( !userRoles[x].ROLE_NAME == 'Line Manager'
                            && userRoles[x].ROLE_NAME == 'TBC HR Specialist (ER)') {
                        checkUser = false;
                    } else if (userRoles[x].ROLE_NAME == 'Line Manager') {
                        checkUser = true;
                    }
                
            };
            console.log(checkUser);
            console.log( self.visible());
            initTranslations();
        };

        function initTranslations() {
            // function to add translations for the home page module
            if (rootViewModel.personDetails())
                self.empName(rootViewModel.personDetails().displayName);
            self.nam(getTranslation("common.name"));
            self.employeeRecordLbl(getTranslation("pages.employeeRecord"));
            if (checkUser) {

                self.managerSpecialistRecordLbl(getTranslation("pages.managerRecord"));
            } else {
                self.managerSpecialistRecordLbl(getTranslation("pages.SpecialistRecord"));

            }

        }
    }

    return absenceRecordScreenContentViewModel;
});
