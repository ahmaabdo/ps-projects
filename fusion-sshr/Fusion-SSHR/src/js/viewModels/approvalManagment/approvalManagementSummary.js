/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * approvalManagementSummary module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox','ojs/ojtable',
    'ojs/ojdialog'], function (oj, ko, $, app, services, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function approvalManagementSummaryContentViewModel() {
        var self = this;

        self.columnArray = ko.observableArray();
        self.roleOptionType = ko.observableArray([]);
        self.placeholder = ko.observable();
        self.dataSourceTB2 = ko.observable();
        var deptArray = [];
        self.selctedStruct = ko.observable();
        self.isRequired = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.nextPage = ko.observable();
        self.deptObservableArray = ko.observableArray(deptArray);
        self.isDisable = ko.observable(true);
        self.backActionLbl = ko.observable();
        var Url;
        var positionObj;
        self.checkResultSearch = ko.observable();
        self.dataprovider = new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'});
        self.typeFlag=ko.observable();
        function clearContent() {

        }


        // Call EIT Code
        self.EitName = ko.observableArray([]);


        self.tableSelectionListener = function (event) {
            var data = event.detail;
            self.isDisable(false);
            var currentRow = data.currentRow;
            if (currentRow) {
            self.oprationdisabled(false);
            self.selectedRowKey(currentRow['rowKey']);
            self.selectedIndex(currentRow['rowIndex']);
        }

        };

        self.addAdditionalDetails = function () {
            app.loading(true);
            positionObj = {}
            positionObj.type = "ADD";
            //positionObj.type="xx";

            oj.Router.rootInstance.store(positionObj);
            oj.Router.rootInstance.go('approvalManagementOperation');
            
        };

        self.searchBtn = function () {
            self.SearchEit();
        };
        self.validationModel = {
            id: ko.observable(),
            eit_code: ko.observable(),
            approval_order: ko.observable()


        };

             self.globalDelete = function () {

            var eit_code = self.deptObservableArray()[0].eitCode;
            var type = self.deptObservableArray()[0].type;
             var id ;
             if( type=="LINE_MANAGER" ||type == "LINE_MANAGER+1" || type == "EMP"){
                 id=null;
             }else if(type=="ROLES" || type=="POSITION"){
                  id=self.selctedApprovalType();
              }

            var getValidGradeCBF = function () {
                self.SearchEit();

            };
           
            services.getGeneric("approvalManagement/delapproval/"  + eit_code+"/"+type+"/"+id  ).then(getValidGradeCBF, app.failCbFn);

            self.structureChangedHandler();
        };
        self.backAction = function () {
            app.loading(true);
            oj.Router.rootInstance.go('searchPosition');
        };
        self.backAction = function () {
            oj.Router.rootInstance.go('setup');
        };
        self.handleAttached = function () {
            app.loading(false);
            self.roleOptionType(app.roleOptionType());
            var arr = JSON.parse(sessionStorage.eitName).map(e => ({label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE}));
            self.EitName(arr);
        };
        
           
        //------------------Option Change Section ----------------------------
        /*
         * Tyis Function Load Date From Web Servise Dependant On Selected Structure Name 
         * And Set The Next Page To Direct So When Crate The Your Pape Set The True Value 
         * in Var self.nextPage 
         * 
         */
        self.selctedApprovalType = ko.observable();
        self.ApprovalTypeOption = ko.observableArray([]);
        self.structureChangedHandler = function (event, data) {
    
           
            var eitCode = self.selctedStruct();
            Url = commonhelper.approvalSetupUrl;
            var getValidGradeCBF = function (data) {
                var Data = data;
               
                self.ApprovalTypeOption([]);
                if (data.length !== 0) {
                    for (var i = 0; i < data.length; i++)
                    {

                        if (data[i].approvalType === "POSITION")
                        {
                            for (var j = 0; j < self.roleOptionType().length; j++) {
                                if (self.roleOptionType()[j].value == data[i].roleName) {
                                 
                                    self.ApprovalTypeOption.push({
                                        value: data[i].roleName,
                                        label: self.roleOptionType()[j].label,
                                        type : "POSITION"
                                    });
                                }
                            }
                        } else if(data[i].approvalType === "ROLES"){
                           
                              for (var j = 0; j < app.allRoles().length; j++) {
                         
                                if (app.allRoles()[j].ROLE_ID == data[i].roleName) {
                                    
                                    self.ApprovalTypeOption.push({
                                        value: data[i].roleName,
                                        label: app.allRoles()[j].ROLE_NAME,
                                        type : "ROLES"
                                    });
                                }
                            }
                        }else
                        {
                            self.ApprovalTypeOption.push({
                                value: data[i].approvalType,
                                label: data[i].specialCase,
                                type : "OTHER"
                            });
                        }
                    }

                } else {
                    $.notify(self.checkResultSearch(), "error");
                }
            };
            var failCbFn = function () {
                $.notify(self.checkResultSearch(), "error");
            };
            services.getGeneric(Url + "approvalType/" + eitCode, {}).then(getValidGradeCBF, failCbFn);
            //self.buildTable();
        };

        self.SearchEit = function () {
            self.isDisable(true);
            var eitCode = self.selctedStruct();
            var approvalType = self.selctedApprovalType();
            self.deptObservableArray([]);
            var getValidGradeCBF = function (data) {
//                var Data = data;
                if (data.length !== 0) {
                 
                   
                   for (var i = 0; i < data.length; i++) {
                            var obj = {};
                               obj.id= data[i].id;
                              for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].value==data[i].eitCode){  
                                  obj.eitCodelbl= app.globalEitNameReport()[j].label;
                                }
                             }     
                               obj.eitCode=data[i].eitCode;
                               obj.type= data[i].type;
                               obj.segementName= data[i].segementName;
                               obj.hide= data[i].hide; 
                               obj.enable= data[i].enable;
                               obj.required= data[i].required;
                               obj.transactionNumber= data[i].transactionNumber;
                               obj.positionName=approvalType;
                               self.deptObservableArray.push(obj);
                       };
                   
                   
                   
                   
                } else {
                    $.notify(self.checkResultSearch(), "error");
                }
            };
            var failCbFn = function () {
                $.notify(self.checkResultSearch(), "error");
            };
            if (approvalType == "LINE_MANAGER") {
                self.typeFlag("LINE_MANAGER");
                services.getGeneric("approvalManagement/search/" + eitCode + "/" + approvalType, {}).then(getValidGradeCBF, failCbFn);
            } else if (approvalType == "LINE_MANAGER+1") {
                self.typeFlag("LINE_MANAGER+1");
                services.getGeneric("approvalManagement/search/" + eitCode + "/" + approvalType, {}).then(getValidGradeCBF, failCbFn);
            } else if (approvalType == "EMP") {
                self.typeFlag("EMP");
                services.getGeneric("approvalManagement/search/" + eitCode + "/EMP" + "/" + approvalType, {}).then(getValidGradeCBF, app.failCbFn);
            } else {
                if (self.ApprovalTypeOption()[0].type == "ROLES") {
                    self.typeFlag("ROLES")
                    services.getGeneric("approvalManagement/search/" + eitCode + "/ROLES" + "/" + approvalType, {}).then(getValidGradeCBF, app.failCbFn);
                } else if (self.ApprovalTypeOption()[0].type == "POSITION")
                {
                    self.typeFlag("POSITION")
                    services.getGeneric("approvalManagement/search/" + eitCode + "/POSITION" + "/" + approvalType, {}).then(getValidGradeCBF, app.failCbFn);
                }
            }
            //self.buildTable();
        };
        //------------------------End Of Section ----------------------------

        //---------------------------Function For Build Table-----------------------------
        self.buildTable = function () {
            //if (self.selctedStruct() == "Work_Nature_Allowance") {
            self.columnArray([
                {
                    "headerText": self.eitNameLbl(), "field": "eitCode"
                },
                {   
                    "headerText": self.approvalTypeLbl(), "field": "type"
                },
                {
                    "headerText": self.segmintNameLbl(), "field": "segementName"
                },
                {
                    "headerText": self.hideLbl(), "field": "hide"
                },
                {
                    "headerText": self.enableLbl(), "field": "enable"
                }
            ]);
            //}
        };
        //---------------------------End Of Function ---------------------------------
        //--------------------------History Section --------------------------------- 
        self.historyDetails = ko.observable();
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };

        //--------------------------End Of Section ----------------------------------
        self.commitRecord = function (data, event) {
            self.globalDelete();
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButtonValid = function () {
            document.querySelector("#ValidationDialog").close();
        };

        self.editBtn = function () {
           var element = document.getElementById('table');
            var currentRow = element.currentRow;
            self.deptObservableArray()[currentRow['rowIndex']].typeFlag = self.typeFlag();
            self.deptObservableArray()[currentRow['rowIndex']].eitCode;
            self.deptObservableArray()[currentRow['rowIndex']].selectedVal = self.selctedApprovalType();;
           oj.Router.rootInstance.store(self.deptObservableArray()[currentRow['rowIndex']]);
//            oj.Router.rootInstance.store(self.deptObservableArray()[0]);
            
            oj.Router.rootInstance.go('approvalManagementOperation');
        };
            self.deleteBtn = function () {
            
                document.querySelector("#yesNoDialog").open();
            

        };
        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();
        self.startDateLbl = ko.observable();
        self.endDateLbl = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.deleteLbl = ko.observable();
        self.gradeLbl = ko.observable();
        self.validGradeLbl = ko.observable();
        self.eitNameLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable();
        self.percentageLbl = ko.observable();
        self.qualificationNameLbl = ko.observable();
        self.experienceYearsLbl = ko.observable();
        self.amountLbl = ko.observable();
        self.historyLbl = ko.observable();
        self.addtionalDetails = ko.observable();
        self.approvalTypeLbl = ko.observable();
        self.roleNameLbl = ko.observable();
        self.specialCaseLbl = ko.observable();
        self.notificationTypeLbl = ko.observable();
        self.approvalOrderLbl = ko.observable();
        self.approvalManagmentLbl = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.hideLbl = ko.observable();
        self.enableLbl = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.ValidationMessage = ko.observable();
        self.segmintNameLbl = ko.observable();
        self.searchLbl = ko.observable();
        self.requiredLbl = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        function initTranslations() {
            self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.deleteLbl(getTranslation("others.delete"));
            self.validGradeLbl(getTranslation("others.validGrade"));
            self.startDateLbl(getTranslation("job.startDate"));
            self.endDateLbl(getTranslation("job.endDate"));
            self.gradeLbl(getTranslation("job.grade"));
            self.percentageLbl(getTranslation("position.percentage"));
            self.qualificationNameLbl(getTranslation("additionalDetails.qualificationName"));
            self.amountLbl(getTranslation("additionalDetails.amount"));
            self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
            self.historyLbl(getTranslation("others.history"));
            self.addtionalDetails(getTranslation("position.positionSummaryAdditionalDetails"));
            self.approvalTypeLbl(getTranslation("approvalScreen.approvalType"));
            self.roleNameLbl(getTranslation("approvalScreen.roleName"));
            self.specialCaseLbl(getTranslation("approvalScreen.specialCase"));
            self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
            self.approvalManagmentLbl(getTranslation("position.approvalManagment"));
            self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
            self.oprationMessage(getTranslation("approvalScreen.oprationMessage"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.enableLbl(getTranslation("labels.enableLbl"));
            self.hideLbl(getTranslation("labels.hideLbl"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.ValidationMessage(getTranslation("labels.ValidationMessage"));
            self.approvalOrderLbl(getTranslation("approvalScreen.aprovalOrder"));
            self.checkResultSearch(getTranslation("common.checkResultSearch"));
            self.segmintNameLbl(getTranslation("labels.segmentName"));
            self.searchLbl(getTranslation("login.Search"));
            self.backActionLbl(getTranslation("approvalScreen.backAction"));
            self.requiredLbl(getTranslation("others.required"));
            self.columnArray([
                {
                    "headerText": self.eitNameLbl(), "field": "eitCodelbl"
                },
                {
                    "headerText": self.approvalTypeLbl(), "field": "type"
                },
                {
                    "headerText": self.segmintNameLbl(), "field": "segementName"
                },
                {
                    "headerText": self.hideLbl(), "field": "hide"
                },
                {
                    "headerText": self.enableLbl(), "field": "enable"
                },
                 {
                    "headerText": self.requiredLbl(), "field": "required"
                }
            ]);
        }
        initTranslations();
    }

    return approvalManagementSummaryContentViewModel;
});
