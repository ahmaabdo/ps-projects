/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * approvalManagementOperationScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'notify', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox', 'ojs/ojarraytabledatasource',
    'ojs/ojdialog'], function (oj, ko, $, app, services, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function approvalManagementOperationScreenContentViewModel() {
        var self = this;
        
        self.columnArray = ko.observableArray();
        self.placeholder = ko.observable();
        self.pageModel=ko.observable();
        self.dataSourceTB2 = ko.observable();
        var deptArray = [];
        self.selctedStruct = ko.observable();
        self.selctedApprovalType = ko.observable();
        self.isRequired = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.nextBtnVisible = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.nextPage = ko.observable();
        self.requiredLbl = ko.observable();
        self.deptObservableArray = ko.observableArray(deptArray);
        var Url;
        var positionObj;
        self.Addvis=ko.observable(false);
        self.checkResultSearch = ko.observable();
        self.dataprovider=ko.observableArray();
        self.dataprovider(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'segementName'}));
        self.lang = "";
        self.checkHide = ko.observableArray([]);
        self.checkEnable = ko.observableArray([]);
        self.approvalManagementTable_Visable = ko.observable(false);
        self.datacrad = ko.observableArray([]);
        self.isDisable = ko.observable(true);
        self.disself=ko.observable(true);       
        self.validErorrMessage = ko.observable();
        var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
        var validEitCount;
        self.roleOptionType=ko.observableArray()
        self.ApprovalTypeOption = ko.observableArray([]);
        function clearContent() {

        }
        // Call EIT Code
        self.EitName = ko.observableArray([]);
     
//        self.callEITCode = function () {
//
//            var EITCodeCbFn = function (data) {
//
//                var tempObject = data;
//                for (var i = 0; i < tempObject.length; i++) {
//                    // push in array
//                    self.EitName.push({
//                        value: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_CODE,
//                        label: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_NAME
//                    });
//
//                }
//            };
//            var failCbFn = function () {
//            };
//
//            services.getEitNameReportReport(app.lang).then(EITCodeCbFn, failCbFn);
//        };


        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var selectRow = data.currentRow;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.oprationdisabled(false);

                self.selectedRowKey(currentRow['rowKey']);
                if (self.selectedIndex() != 'null') {
                    self.selectedIndex(currentRow['rowIndex']);
                }
                if (self.selectedIndex() != null)
                {
                    self.isDisable(false);
                }

            }
        };
//--------------------------------------------------------------------------------------------------EditCheckBox-----------------------------------
      

        self.selectedMessages = ko.observableArray([]);





 self.newMessagesOptions = ko.observableArray(['autoTimeout']);
        self.computedAutoTimeout = ko.computed(function () {

            return self.newMessagesOptions().indexOf('autoTimeout') !== -1 ? 0 : -1;
        }.bind(self));

        self.createMessage = function (severity) {
            var initCapSeverity = severity.charAt(0).toUpperCase() + severity.slice(1);
            return {
//                severity: severity,
                severity: 'error',
                summary: 'Error message ',
                detail: "plz select hide only or enable and required only",
                autoTimeout: self.computedAutoTimeout()
            };
        };


        self.applicationMessages = ko.observableArray([]);
        self.dataproviderMessage = new oj.ArrayDataProvider(self.applicationMessages);






        self.ShowMsg = function () {

            var selectedMessages = event.detail.value;
            document.getElementById('oj-messages-id').closeAll(function (message) {
                return selectedMessages.indexOf(message.severity) === -1;
            });
            for (var i = 0; i < selectedMessages.length; i++)
            {
                var isSelectedMessageShown = self.applicationMessages().find(function (item) {
                    return item.severity === selectedMessages[i];
                });
                if (!isSelectedMessageShown)
                {
                    self.applicationMessages.push(self.createMessage(selectedMessages[i]));
                }
            }
        };

        self.checkboxListenerHide = function (event)
        {
            if (self.deptObservableArray()[self.selectedIndex()].hide == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].hide = "false";
            } else if (self.deptObservableArray()[self.selectedIndex()].enable == "true" || self.deptObservableArray()[self.selectedIndex()].required == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].hide = "false";
                self.dataprovider(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'segementName'}));
                self.ShowMsg();
            } else {
                self.deptObservableArray()[self.selectedIndex()].hide = "true";
            }
//            setTimeout(function(){
//                self.deptObservableArray()[self.selectedIndex()].hide = true
//            }, 1000)
        };


        self.checkboxListenerEnable = function (event)
        {

            if (self.deptObservableArray()[self.selectedIndex()].enable == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].enable = "false";
            } else if (self.deptObservableArray()[self.selectedIndex()].hide == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].enable = "false";
                self.dataprovider(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'segementName'}));
                self.ShowMsg();
            } else {
                self.deptObservableArray()[self.selectedIndex()].enable = "true";
            }

        };


        self.checkboxListenerRequired = function (event)
        {
            if (self.deptObservableArray()[self.selectedIndex()].required == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].required = "false";
            } else if (self.deptObservableArray()[self.selectedIndex()].hide == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].required = "false";
                self.dataprovider(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'segementName'}));
                self.ShowMsg();

            } else {
                self.deptObservableArray()[self.selectedIndex()].required = "true";

            }

        };

        self.closeMessageHandler = function (event)
        {
            self.applicationMessages.remove(event.detail.message);

            self.deptObservableArray.remove(function (severity) {
//                var val = event.detail.message.severity;
//                var index = val.split("-")[1];
//                self.deptObservableArray()[index].hide;
                return severity === event.detail.message.severity
            }.bind(self));
        }.bind(self);
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
        self.handleAttached = function () {
            self.retrieve = oj.Router.rootInstance.retrieve();
            app.loading(false);
            self.type = self.retrieve.typeFlag;
            var arr = JSON.parse(sessionStorage.eitName).map(e => ({label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE}));
            self.EitName(arr);
            self.roleOptionType(app.roleOptionType());
            if (self.retrieve.type == 'ADD') {
                self.pageModel('ADD');
                self.Addvis(true);
                self.disself(false);
                self.nextBtnVisible(false);
                
            } else {
                self.pageModel('EDIT');
                self.eitCode = self.retrieve.eitCode;
                self.type = self.retrieve.typeFlag;
                self.positionName = self.retrieve.positionName;
                self.segementName = self.retrieve.segementName;
                self.SearchEit();
                self.selctedStruct(self.retrieve.eitCode);
                self.structureChangedHandler();
                self.selctedApprovalType(self.retrieve.selectedVal);
                self.Addvis(false);
                self.disself(true);
            }
        };
        self.currentValue=ko.observableArray([]);
        self.CheckBoxArr=ko.observableArray([]);
        self.SearchEit = function (event, data) {

            self.deptObservableArray([]);
            var getValidGradeCBF = function (data) {
                var Data = data;
                            
                if (data.length !== 0) {

                    
                       $.each(Data, function (i) {
                           
                                self.deptObservableArray.push({
                                    id: Data[i].id,
                                    eitCode:Data[i].eitCode,
                                    hide: Data[i].hide,
                                    enable: Data[i].enable,
                                    required: Data[i].required,
                                    segementName: Data[i].segementName,
                                    type: Data[i].type,
                                    positionName: Data[i].positionName
                                    

                                });
                            });
                    
                    
                    
                    
                    self.deptObservableArray(Data);
                } else {
                    $.notify("Error", "error");
                }

            };
            var failCbFn = function () {
                $.notify("Err", "error");
            };

            
            
            if (self.type== "LINE_MANAGER") {
                services.getGeneric("approvalManagement/search/" + self.eitCode + "/" + self.type, {}).then(getValidGradeCBF, failCbFn);
            } else if (self.type == "LINE_MANAGER+1") {
                services.getGeneric("approvalManagement/search/" + self.eitCode + "/" + self.type, {}).then(getValidGradeCBF, failCbFn);
            } else if (self.type == "EMP")
            {
                services.getGeneric("approvalManagement/search/" + self.eitCode + "/EMP" + "/" + self.positionName, {}).then(getValidGradeCBF, app.failCbFn);
            } else if (self.type == "ROLES") {
                services.getGeneric("approvalManagement/search/" + self.eitCode + "/ROLES" + "/" + self.positionName, {}).then(getValidGradeCBF, app.failCbFn);
            } else if (self.type == "POSITION")
            {
                services.getGeneric("approvalManagement/search/" + self.eitCode + "/POSITION" + "/" + self.positionName, {}).then(getValidGradeCBF, app.failCbFn);
            }
            
            //self.buildTable();
        };

        //------------------Option Change Section ----------------------------
        /*
         * Tyis Function Load Date From Web Servise Dependant On Selected Structure Name 
         * And Set The Next Page To Direct So When Crate The Your Pape Set The True Value 
         * in Var self.nextPage 
         * 
         */

        //**************************************************************

        //**************************************************************


        //------------------------End Of Section ----------------------------
        function oprationServiceRecordEdit() {
            var getValidGradeCBF = function (data) {

                oj.Router.rootInstance.go("approvalManagmentSummary");
            };


            services.addGeneric("approvalManagement/EDIT", ko.toJSON(self.deptObservableArray())).then(getValidGradeCBF, app.failCbFn);
        }
        ;
        //---------------------------Function For Build Table-----------------------------
        self.buildTable = function () {
            //if (self.selctedStruct() == "Work_Nature_Allowance") {
          
       
                   self.columnArray([
                {
                    "headerText": self.segmintNameLbl(), "field": "segementName"
                },
                {
                    "headerText": self.hideLbl(), "template": "hideTemplate"
                },
                {
                    "headerText": self.enableLbl(), "template": "enableTemplate"
                },
                {
                    "headerText": self.requiredLbl(), "template": "requiredTemplate"
                }
            
               
            ]);
            //}
        };
        //---------------------------End Of Function ---------------------------------
        //--------------------------History Section --------------------------------- 
        self.historyDetails = ko.observable();
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };

        //--------------------------End Of Section ----------------------------------
        self.commitRecord = function (data, event) {
            if (self.pageModel() == 'ADD') {
                oprationServiceRecordAdd();
                document.querySelector("#yesNoDialogAdd").close();

            } else {
                oprationServiceRecordEdit();
                document.querySelector("#yesNoDialog").close();

            }
        };

        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
            document.querySelector("#yesNoDialogAdd").close();

        };

        self.cancelButtonValid = function () {
            document.querySelector("#ValidationDialog").close();
        };
        self.groupValid = ko.observable();
        self.editApprovalManagement = function () {

            // if(validEitCount == 0){
            document.querySelector("#yesNoDialog").open();
            //}
            //else
            //{
            //  $.notify(self.validErorrMessage(), "error");
            //}
        };
        self.addApprovalManagement = function () {
            if (validEitCount == 0) {
                document.querySelector("#yesNoDialogAdd").open();
            } else
            {
                $.notify(self.validErorrMessage(), "error");
            }
        };
        self.backAction = function () {
            if (oj.Router.rootInstance._navHistory.length > 1) {
                app.loading(true);
                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
            }
        };
// ______________________________________________Add section___________________________________       

        //**************************************************************

        self.structureChangedHandlerApprovalType = function (event, data) {
            
                if(self.pageModel()=='ADD'){
                self.ValidationEitCode();
                self.approvalManagementTable_Visable(true);
            }
                if (self.selctedApprovalType() != null)
                {
              self.approvalManagementTable_Visable(true);
            }
            if (self.selectedIndex() != null && self.selctedApprovalType() != null)
            {
                self.isDisable(false);
            }

        };
        self.structureChangedHandler = function (event, data) {
            self.ApprovalTypeOption([]);
     
            var positionCode = self.selctedStruct();
            Url = commonhelper.approvalSetupUrl;


            var getValidGradeCBF = function (data) {
                var Data = data;
                 
                self.ApprovalTypeOption([]);

                if (data.length !== 0) {
                    for (var i = 0; i < data.length; i++)
                    {

                        if (data[i].approvalType === "POSITION")
                        {
                            for (var j = 0; j < self.roleOptionType().length; j++) {
                                if (self.roleOptionType()[j].value == data[i].roleName) {
                                  
                                    self.ApprovalTypeOption.push({
                                        value: data[i].roleName,
                                        label: self.roleOptionType()[j].label,
                                        type : "POSITION"
                                    });
                                    break;
                                }
                            }
                        }else if(data[i].approvalType === "ROLES"){
                          
                              for (var j = 0; j < app.allRoles().length; j++) {
                         
                                if (app.allRoles()[j].ROLE_ID == data[i].roleName) {
                                    
                                    self.ApprovalTypeOption.push({
                                        value: data[i].roleName,
                                        label: app.rolesOption()[j].label,
                                        type : "ROLES"
                                    });
                                    break;
                                }
                            }
                        } else
                        {
                            self.ApprovalTypeOption.push({
                                value: data[i].approvalType,
                                label: data[i].specialCase,
                                type : "OTHER"
                            });
                        }
                    }

                } else {
                    $.notify(self.checkResultSearch(), "error");
                }
            };
            var failCbFn = function () {
                $.notify(self.checkResultSearch(), "error");
            };
            if (self.pageModel() == 'ADD') {
                self.SegmintEit();
                services.getGeneric(Url + "approvalType/" + positionCode, {}).then(getValidGradeCBF, failCbFn);
                //self.buildTable();
            } else {
                services.getGeneric(Url + "approvalType/" + positionCode, {}).then(getValidGradeCBF, failCbFn);
                //self.buildTable();
            };
    };




        self.SegmintEit = function (event, data) {
            self.deptObservableArray([]);
            var positionCode = self.selctedStruct();
            Url = commonhelper.approvalSetupUrl;
            var getEitCBFN = function (data) {
                var Data = data;
                for (var i = 0; i < Data.length; i++) {
                    var obj = {};
                    obj.eitCode = self.selctedStruct();
                    obj.hide = false;
                    obj.enable = false;
                    obj.required = false;
                    obj.FORM_LEFT_PROMPT = Data[i].FORM_LEFT_PROMPT;
                    obj.segementName = Data[i].DESCRIPTION;
                    obj.type = self.selctedApprovalType();
                    obj.createdBy = rootViewModel.personDetails().personId;
                    obj.positionName = "";
                    self.deptObservableArray.push(obj);
                }



            };
            var failCbFn = function () {
                $.notify(self.checkResultSearch(), "error");
            };
            if (app.getLocale() == 'ar') {
                self.lang = "AR";
            } else {
                self.lang = "US";
            }
            services.getEIT(positionCode, self.lang).then(getEitCBFN, failCbFn);
            //self.buildTable();
        };

        self.ValidationEitCode = function (event, data) {
             for (var i = 0; i < self.EitName().length; i++) {
            for (var j = 0; j < self.ApprovalTypeOption().length; j++) {
                var eitCode = self.selctedStruct();
                var approvalType;
                if (self.selctedApprovalType() == "LINE_MANAGER+1" || self.selctedApprovalType() == "LINE_MANAGER"&& self.selctedStruct()==self.EitName()[i].value) {
                     approvalType = self.selctedApprovalType();
                } else if(self.selctedApprovalType()=='EMP'&& self.selctedStruct()==self.EitName()[i].value)  {
                     approvalType = self.deptObservableArray().type = "EMP";
                } else if(self.selctedApprovalType()==self.ApprovalTypeOption()[j].value && 
                          self.selctedStruct()==self.EitName()[i].value &&
                          self.ApprovalTypeOption()[j].type == 'POSITION')  {
                     approvalType = self.deptObservableArray().type = "POSITION";
                    var positionId = self.ApprovalTypeOption()[j].value;
                }else if(self.selctedApprovalType()==self.ApprovalTypeOption()[j].value && 
                        self.selctedStruct()==self.EitName()[i].value &&
                        self.ApprovalTypeOption()[j].type == 'ROLES')  {
                     approvalType = self.deptObservableArray().type = "ROLES";
                    var positionId = self.ApprovalTypeOption()[j].value;
                }
                var getValidEitCBFN = function (data) {
                    var Data = data;
                    validEitCount = Data;
                                         
            }
            }
        }
            ;
            var failCbFn = function () {
                $.notify("Error", "error");
            };
                if(approvalType == "POSITION" || approvalType == "ROLES"){
                   services.getGeneric("approvalManagement/validation/" + eitCode + "/" + approvalType + "/" + positionId, {}).then(getValidEitCBFN, failCbFn); 
                }
                else{
            services.getGeneric("approvalManagement/validation/" + eitCode + "/" + approvalType, {}).then(getValidEitCBFN, failCbFn);
        }
            //self.buildTable();
        };
//        //------------------------End Of Section ----------------------------
        function oprationServiceRecordAdd() {
            var getValidGradeCBF = function (data) {
                oj.Router.rootInstance.go("approvalManagmentSummary");
            };
            for (var i = 0; i < self.deptObservableArray().length; i++) {

                if (self.selctedApprovalType() == "LINE_MANAGER+1" || self.selctedApprovalType() == "LINE_MANAGER")
                {
                    self.deptObservableArray()[i].positionName = "";
                    self.deptObservableArray()[i].type = self.selctedApprovalType();
                } else if(self.selctedApprovalType() == "EMP")
                {
                    self.deptObservableArray()[i].type = "EMP";
                    self.deptObservableArray()[i].positionName = self.selctedApprovalType();
                } else{
                    if(self.ApprovalTypeOption()[0].type == "POSITION")
                {
                    self.deptObservableArray()[i].type = "POSITION";
                    self.deptObservableArray()[i].positionName = self.selctedApprovalType();
                }else if(self.ApprovalTypeOption()[0].type == "ROLES")
                {
                    self.deptObservableArray()[i].type = "ROLES";
                    self.deptObservableArray()[i].positionName = self.selctedApprovalType();
                }
                }
            }
            services.addGeneric("approvalManagement/"+ self.pageModel(), ko.toJSON(self.deptObservableArray().filter(o=>o.segementName))).then(getValidGradeCBF, app.failCbFn);
        }
        ;
        //---------------------------Function For Build Table-----------------------------
       
            //if (self.selctedStruct() == "Work_Nature_Allowance") {
          
            //}
        
        
        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();
        self.startDateLbl = ko.observable();
        self.endDateLbl = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.deleteLbl = ko.observable();
        self.gradeLbl = ko.observable();
        self.validGradeLbl = ko.observable();
        self.eitNameLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable();
        self.percentageLbl = ko.observable();
        self.qualificationNameLbl = ko.observable();
        self.experienceYearsLbl = ko.observable();
        self.amountLbl = ko.observable();
        self.historyLbl = ko.observable();
        self.addtionalDetails = ko.observable();
        self.approvalTypeLbl = ko.observable();
        self.roleNameLbl = ko.observable();
        self.specialCaseLbl = ko.observable();
        self.notificationTypeLbl = ko.observable();
        self.validErorrMessage = ko.observable();
        self.approvalOrderLbl = ko.observable();
        self.approvalManagmentLbl = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.oprationMessageAdd=ko.observable();
        self.ValidationMessage = ko.observable();
        self.hideLbl = ko.observable();
        self.enableLbl = ko.observable();
        self.segmintNameLbl = ko.observable();
        self.cancel = ko.observable();
        self.hidePreviousLbl = ko.observable();
        self.enablePreviousLbl = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });


        ///////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////

        function initTranslations() {
            self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.deleteLbl(getTranslation("others.delete"));
            self.validGradeLbl(getTranslation("others.validGrade"));
            self.startDateLbl(getTranslation("job.startDate"));
            self.endDateLbl(getTranslation("job.endDate"));
            self.gradeLbl(getTranslation("job.grade"));
            self.percentageLbl(getTranslation("position.percentage"));
            self.qualificationNameLbl(getTranslation("additionalDetails.qualificationName"));
            self.amountLbl(getTranslation("additionalDetails.amount"));
            self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
            self.historyLbl(getTranslation("others.history"));
            self.addtionalDetails(getTranslation("position.positionSummaryAdditionalDetails"));
            self.approvalTypeLbl(getTranslation("approvalScreen.approvalType"));
            self.roleNameLbl(getTranslation("approvalScreen.roleName"));
            self.specialCaseLbl(getTranslation("approvalScreen.specialCase"));
            self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
            self.approvalManagmentLbl(getTranslation("position.ApprovalManagmentOperation"));
            self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
            self.oprationMessage(getTranslation("approvalScreen.oprationMessageEditApprovalManagement"));
            self.oprationMessageAdd(getTranslation("approvalScreen.oprationMessageApprovalManagement"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.enableLbl(getTranslation("labels.enableLbl"));
            self.hideLbl(getTranslation("labels.hideLbl"));
            self.segmintNameLbl(getTranslation("labels.segmentName"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.validErorrMessage(getTranslation("labels.validApprovalManagement"));
            self.checkResultSearch(getTranslation("common.checkResultSearch"));
            self.cancel(getTranslation("others.cancel"));
            self.hidePreviousLbl(getTranslation("others.hidePrevious"));
            self.enablePreviousLbl(getTranslation("others.enablePrevious"));
            self.requiredLbl(getTranslation("others.required"));
     
      
                   self.columnArray([
                {
                    "headerText": self.segmintNameLbl(), "field": "segementName"
                },
                {
                    "headerText": self.hideLbl(), "template": "hideTemplate"
                },
                {
                    "headerText": self.enableLbl(), "template": "enableTemplate"
                },
                {
                    "headerText": self.requiredLbl(), "template": "requiredTemplate"
                }
            ]);
        }
        
        initTranslations();
    }

    return approvalManagementOperationScreenContentViewModel;
});
