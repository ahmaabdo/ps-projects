/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * approvalManagementEditScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'notify', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox', 'ojs/ojarraytabledatasource',
    'ojs/ojdialog'], function (oj, ko, $, app, services, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function approvalManagementEditScreenContentViewModel() {
        var self = this;
        self.retrieve = oj.Router.rootInstance.retrieve();
        self.eitCode = self.retrieve.eitCode;
        self.type = self.retrieve.type;
        self.positionName = self.retrieve.positionName;
        self.segementName = self.retrieve.segementName;
        self.columnArray = ko.observableArray();
        self.placeholder = ko.observable();
        self.dataSourceTB2 = ko.observable();
        var deptArray = [];
        self.selctedStruct = ko.observable();
        self.isRequired = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.nextBtnVisible = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.nextPage = ko.observable();
        self.requiredLbl = ko.observable();
        self.deptObservableArray = ko.observableArray(deptArray);
        var Url;
        var positionObj;
        self.checkResultSearch = ko.observable();
        self.dataprovider = new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'segementName'});
        self.selctedApprovalType = ko.observable();
        self.lang = "";
        self.checkHide = ko.observableArray([]);
        self.checkEnable = ko.observableArray([]);
        self.approvalManagementTable_Visable = ko.observable(false);
        self.datacrad = ko.observableArray([]);
        self.isDisable = ko.observable(true);
        var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
        var validEitCount;

        function clearContent() {

        }
        // Call EIT Code
        self.EitName = ko.observableArray([]);
        self.ApprovalTypeOption = ko.observableArray([]);
        self.callEITCode = function () {

            var EITCodeCbFn = function (data) {

                var tempObject = data;
                for (var i = 0; i < tempObject.length; i++) {
                    // push in array
                    self.EitName.push({
                        value: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_CODE,
                        label: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_NAME
                    });

                }
            };
            var failCbFn = function () {
            };

            services.getEitNameReportReport(app.lang).then(EITCodeCbFn, failCbFn);
        };


        self.tableSelectionListener = function (event) {
            var data = event.detail;
            selectRow = data.currentRow;
            var currentRow = data.currentRow;

            self.oprationdisabled(false);
            self.selectedRowKey(currentRow['rowKey']);
            self.selectedIndex(currentRow['rowIndex']);
            if (self.selectedIndex() != null)
            {
                self.isDisable(false);
            }


        };

        self.checkboxListenerHide = function (event)
        {
            if (self.deptObservableArray()[self.selectedIndex()].hide == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].hide = false;

            } else
            {
                self.deptObservableArray()[self.selectedIndex()].hide = true;
            }
        };

        self.checkboxListenerEnable = function (event)
        {
            if (self.deptObservableArray()[self.selectedIndex()].enable == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].enable = false;
            } else
            {
                self.deptObservableArray()[self.selectedIndex()].enable = true;
            }
            ;
        };
          self.checkboxListenerRequired = function (event)
        {
            if (self.deptObservableArray()[self.selectedIndex()].required == "true")
            {
                self.deptObservableArray()[self.selectedIndex()].required = false;
            } else
            {
                self.deptObservableArray()[self.selectedIndex()].required = true;
            }
            ;
        };

        self.handleAttached = function () {
            self.retrieve = oj.Router.rootInstance.retrieve();
            app.loading(false);
            self.type=self.retrieve.type;
            // positionObj = oj.Router.rootInstance.retrieve();
            self.EitName(app.globalEitNameReport());
            self.SearchEit();
            self.selctedStruct(self.retrieve.eitCode);
            self.structureChangedHandler();
            self.selctedApprovalType(self.retrieve.type);

        };
        self.currentValue=ko.observableArray([]);
        self.CheckBoxArr=ko.observableArray([]);
        self.SearchEit = function (event, data) {

            self.deptObservableArray([]);
            var getValidGradeCBF = function (data) {
                var Data = data;
                            
                if (data.length !== 0) {

                    
                       $.each(Data, function (i) {
                           
                                self.deptObservableArray.push({
                                    id: Data[i].id,
                                    eitCode:Data[i].eitCode,
                                    hide: Data[i].hide,
                                    enable: Data[i].enable,
                                    required: Data[i].required,
                                    segementName: Data[i].segementName,
                                    type: Data[i].type,
                                    positionName: Data[i].positionName
                                    

                                });
                            });
                    
                    
                    
                    
                    self.deptObservableArray(Data);
                } else {
                    $.notify("Error", "error");
                }

            };
            var failCbFn = function () {
                $.notify("Err", "error");
            };

            
            
            if (self.type== "LINE_MANAGER") {
                services.getGeneric("approvalManagement/search/" + self.eitCode + "/" + self.type, {}).then(getValidGradeCBF, failCbFn);
            } else if (self.type == "LINE_MANAGER+1") {
                services.getGeneric("approvalManagement/search/" + self.eitCode + "/" + self.type, {}).then(getValidGradeCBF, failCbFn);
            } else
            {
                services.getGeneric("approvalManagement/search/" + self.eitCode + "/POSITION" + "/" + self.positionName, {}).then(getValidGradeCBF, app.failCbFn);
            }
            //self.buildTable();
        };
        //------------------Option Change Section ----------------------------
        /*
         * Tyis Function Load Date From Web Servise Dependant On Selected Structure Name 
         * And Set The Next Page To Direct So When Crate The Your Pape Set The True Value 
         * in Var self.nextPage 
         * 
         */

        //**************************************************************
        self.roleOptionType = ko.observableArray([]);

        //**************************************************************

        self.structureChangedHandlerApprovalType = function (event, data) {
            if (self.selctedApprovalType() != null)
            {
//                self.ValidationEitCode();
                self.approvalManagementTable_Visable(true);
            }
            if (self.selectedIndex() != null && self.selctedApprovalType() != null)
            {
                self.isDisable(false);
            }

        };
        self.structureChangedHandler = function (event, data) {
            self.ApprovalTypeOption([]);
            //self.SegmintEit(); 
            var positionCode = self.selctedStruct();
            Url = commonhelper.approvalSetupUrl;


            var getValidGradeCBF = function (data) {
                var Data = data;
                self.ApprovalTypeOption([]);

                if (data.length !== 0) {
                    for (var i = 0; i < data.length; i++)
                    {

                        if (data[i].approvalType === "POSITION")
                        {
                            self.ApprovalTypeOption.push({
                                value: data[i].roleName,
                                label: data[i].roleName
                            });
                        } else
                        {
                            self.ApprovalTypeOption.push({
                                value: data[i].approvalType,
                                label: data[i].approvalType
                            });
                        }
                    }


                } else {
                    $.notify("eere", "error");
                }
            };
            var failCbFn = function () {
                $.notify("ERE", "error");
            };
            services.getGeneric(Url + "approvalType/" + positionCode, {}).then(getValidGradeCBF, failCbFn);
            //self.buildTable();
        };




//        self.SegmintEit = function (event, data) {
//            self.deptObservableArray([]);
//            var positionCode = self.selctedStruct();
//            Url = commonhelper.approvalSetupUrl;
//            var getEitCBFN = function (data) {
//                var Data = data;
//                for(var i =0 ; i<Data.length;i++ ){
//                    var obj = {};
//                    
//                    obj.eitCode = self.selctedStruct();
//                    obj.hide = false ; 
//                    obj.enable = false;
//                    obj.FORM_LEFT_PROMPT =Data[i].FORM_LEFT_PROMPT ;
//                    obj.segementName = Data[i].DESCRIPTION;
//                    obj.type = self.selctedApprovalType();
//                    obj.createdBy = rootViewModel.personDetails().personId;
//                    obj.positionName = "";
//                    self.deptObservableArray.push(obj);
//                }
//                
//                 
//                    
//            };
//            var failCbFn=function (){
//                $.notify("self.checkResultSearch()", "error");
//            };
//            if(app.getLocale() == 'ar'){
//            self.lang ="AR";
//            }else{
//                self.lang ="US";
//            }
//            services.getEIT(positionCode,self.lang).then(getEitCBFN,failCbFn);
//            //self.buildTable();
//        };
        //------------------------End Of Section ----------------------------
        function oprationServiceRecord() {
            var getValidGradeCBF = function (data) {

                oj.Router.rootInstance.go("approvalManagmentSummary");
            };


            services.addGeneric("approvalManagement/EDIT", ko.toJSON(self.deptObservableArray())).then(getValidGradeCBF, app.failCbFn);
        }
        ;
        //---------------------------Function For Build Table-----------------------------
        self.buildTable = function () {
            //if (self.selctedStruct() == "Work_Nature_Allowance") {
            self.columnArray([
                {
                    "headerText": self.segmintNameLbl(), "field": "segementName"
                },
                 {
                    "headerText": 'hide', "field": "hide"
                },
                {
                    "headerText": 'enable', "field": "enable"
                },
                {
                    "headerText": self.hideLbl(), "template": "hideTemplate"
                },
                {
                    "headerText": self.enableLbl(), "field": "enableTemplate"
                }
               
            ]);
            //}
        };
        //---------------------------End Of Function ---------------------------------
        //--------------------------History Section --------------------------------- 
        self.historyDetails = ko.observable();
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };

        //--------------------------End Of Section ----------------------------------
        self.commitRecord = function (data, event) {
            oprationServiceRecord();
            document.querySelector("#yesNoDialog").close();

        };

        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButtonValid = function () {
            document.querySelector("#ValidationDialog").close();
        };
        self.groupValid = ko.observable();
        self.editApprovalManagement = function () {

            // if(validEitCount == 0){
            document.querySelector("#yesNoDialog").open();
            //}
            //else
            //{
            //  $.notify(self.validErorrMessage(), "error");
            //}
        };

        self.backAction = function () {
            if (oj.Router.rootInstance._navHistory.length > 1) {
                app.loading(true);
                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
            }
        };
        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();
        self.startDateLbl = ko.observable();
        self.endDateLbl = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.deleteLbl = ko.observable();
        self.gradeLbl = ko.observable();
        self.validGradeLbl = ko.observable();
        self.eitNameLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable();
        self.percentageLbl = ko.observable();
        self.qualificationNameLbl = ko.observable();
        self.experienceYearsLbl = ko.observable();
        self.amountLbl = ko.observable();
        self.historyLbl = ko.observable();
        self.addtionalDetails = ko.observable();
        self.approvalTypeLbl = ko.observable();
        self.roleNameLbl = ko.observable();
        self.specialCaseLbl = ko.observable();
        self.notificationTypeLbl = ko.observable();
        self.validErorrMessage = ko.observable();
        self.approvalOrderLbl = ko.observable();
        self.approvalManagmentLbl = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.ValidationMessage = ko.observable();
        self.hideLbl = ko.observable();
        self.enableLbl = ko.observable();
        self.segmintNameLbl = ko.observable();
        self.cancel = ko.observable();
        self.hidePreviousLbl = ko.observable();
        self.enablePreviousLbl = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });


        ///////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////

        function initTranslations() {
            self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.deleteLbl(getTranslation("others.delete"));
            self.validGradeLbl(getTranslation("others.validGrade"));
            self.startDateLbl(getTranslation("job.startDate"));
            self.endDateLbl(getTranslation("job.endDate"));
            self.gradeLbl(getTranslation("job.grade"));
            self.percentageLbl(getTranslation("position.percentage"));
            self.qualificationNameLbl(getTranslation("additionalDetails.qualificationName"));
            self.amountLbl(getTranslation("additionalDetails.amount"));
            self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
            self.historyLbl(getTranslation("others.history"));
            self.addtionalDetails(getTranslation("position.positionSummaryAdditionalDetails"));
            self.approvalTypeLbl(getTranslation("approvalScreen.approvalType"));
            self.roleNameLbl(getTranslation("approvalScreen.roleName"));
            self.specialCaseLbl(getTranslation("approvalScreen.specialCase"));
            self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
            self.approvalManagmentLbl(getTranslation("position.editApprovalManagment"));
            self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
            self.oprationMessage(getTranslation("approvalScreen.oprationMessageEditApprovalManagement"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.enableLbl(getTranslation("labels.enableLbl"));
            self.hideLbl(getTranslation("labels.hideLbl"));
            self.segmintNameLbl(getTranslation("labels.segmentName"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.validErorrMessage(getTranslation("labels.validApprovalManagement"));
            self.cancel(getTranslation("others.cancel"));
            self.hidePreviousLbl(getTranslation("others.hidePrevious"));
            self.enablePreviousLbl(getTranslation("others.enablePrevious"));
            self.requiredLbl(getTranslation("others.required"));
            self.columnArray([
                {
                    "headerText": self.segmintNameLbl(), "field": "segementName"
                },
                {
                    "headerText": self.hideLbl(), "template": "hideTemplate"
                },
                {
                    "headerText": self.enableLbl(), "template": "enableTemplate"
                },
                {
                    "headerText": self.requiredLbl(), "template": "requiredTemplate"
                }
            ]);
        }
        initTranslations();
    }

    return approvalManagementEditScreenContentViewModel;
});
