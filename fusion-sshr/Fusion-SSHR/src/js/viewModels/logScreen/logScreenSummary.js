define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojvalidationgroup',
    'ojs/ojlabel', 'ojs/ojvalidationgroup', 'ojs/ojformlayout'],
        (oj, ko, $, app, commonUtil, services, ArrayDataProvider) => {

    function logScreenSummaryViewModel() {
        var self = this;

        self.title = ko.observable();
        self.eitNameLbl = ko.observable();
        self.searchLbl = ko.observable();
        self.backLbl = ko.observable();
        self.eitNameVal = ko.observable();
        self.eitNameOpts = ko.observableArray([]);
        self.searchDisable = ko.observable(false);
        self.statusUpdate = ko.observable();
        self.attachDisable = ko.observable(true);
        self.selectedIndex = ko.observable();
        self.current = ko.observable();
        self.personNumberVal = ko.observable();
        self.eitCodeLbl = ko.observable();
        self.personNumberLbl = ko.observable();
        self.placeHolder = ko.observable();
        self.columnArray = ko.observableArray();
        self.summaryObservableArray = ko.observableArray([]);
        self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));
        self.eitNameOpts = ko.observableArray([]);
        self.messages = ko.observableArray();
        self.dataNotFoundMsg = ko.observable();
        self.clearLbl = ko.observable();
        self.resubmitDialogMsg = ko.observable();
        self.groupValid = ko.observable();
        self.submit = ko.observable();
        self.cancel = ko.observable();
        self.fileContentValue = ko.observable();
        self.selectedRow = ko.observable();
        self.fileContentLbl = ko.observable();
        self.resubmitLbl = ko.observable();
        self.submittedMsgNofity = ko.observable();
        self.dataAddedMsg = ko.observable();
        self.dataErrMsg = ko.observable();
        self.isResubmitBtnDisabled = ko.observable(true);
        var requestPayload;





        self.searchBtnAction = function () {
            self.getDataBySearch();

        };


        self.clearContent = function () {
            self.personNumberVal('');
            self.eitNameVal("");
            self.summaryObservableArray([]);
        };


        self.getDataBySearch = function () {

            var eitName;
            var payload = {
                "eitCode": self.eitNameVal(),
                "personNumber": self.personNumberVal()
            };

            var success = function (data) {
                $("#searchBtn").removeClass("loading");
                if (data.length !== 0)
                {
                    self.summaryObservableArray([]);
                    $.each(data, function (index) {

                        self.eitNameOpts().map(e => {
                            if (data[index].eitCode == e.value) {
                                eitName = e.label;
                            }
                        });
                        self.summaryObservableArray.push({
                            "URL": data[index].url,
                            "payload": data[index].payload,
                            projectName: data[index].projectName,
                            eitCode: data[index].eitCode,
                            eitName: eitName,
                            eitResponseCode: data[index].eitResponseCode,
                            eitResponseDesc: data[index].eitResponseDesc,
                            eitResponseDate: data[index].eitResponseDate,
                            elementResponseCode: data[index].elementResponseCode,
                            elementResponseDesc: data[index].elementResponseDesc,
                            elementResponseDate: data[index].elementResponseDate,
                            requestId: data[index].requestId,
                            personNumber: data[index].personNumber,
                            id: data[index].id

                        });

                    });
                } else {
                    self.summaryObservableArray([]);
                }
            };

            var failCbFn = function () {

                $("#searchBtn").removeClass("loading");
            };


            $("#searchBtn").addClass("loading");
            services.addGeneric("loggingRest/searchTrackServerRequest", payload).then(success, failCbFn);
        };



        self.backAction = function () {
            oj.Router.rootInstance.go('setup');
        };

        self.tableSelectionListener = function (e) {
            var data = e.detail;
            var currentRow = data.currentRow;

            if (currentRow !== null) {
                self.selectedRow(self.summaryObservableArray().find(obj => obj.id == currentRow['rowKey']));
                self.selectedIndex(currentRow['rowIndex']);
            };
    
            var checkArray = self.summaryObservableArray()[self.selectedIndex()];
            if (checkArray) {
                self.statusUpdate(checkArray.eitResponseCode);
                if (currentRow) {
                    var rowIndex = currentRow.rowIndex;
                    var selectedRow = self.summaryObservableArray()[rowIndex];
                    if (selectedRow.eitResponseCode >= 400) {//status Condition
                        self.isResubmitBtnDisabled(false);
                    } else {
                        self.isResubmitBtnDisabled(true);
                    }
                }
            }

        };


        self.handleAttached = function (info) {
            var arr = JSON.parse(sessionStorage.eitName).map(e => ({label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE}));
            self.eitNameOpts(arr);
            app.loading(false);
            self.isResubmitBtnDisabled(true);
            self.clearContent();
            self.getDataBySearch();
        };



        //--------------- define messages notification ---------------//
        self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
        self.position =
                {
                    "my": {"vertical": "top", "horizontal": "end"},
                    "at": {"vertical": "top", "horizontal": "end"},
                    "of": "window"
                };
        self.closeMessageHandler = function (event) {
            self.messages.remove(event.detail.message);
        };


        //------------clear action -------------
        self.clearBtnAction = function () {
            self.summaryObservableArray([]);
            self.eitNameVal("");
            self.personNumberVal("");
            self.isResubmitBtnDisabled(true);

        };



        self.cancelButton = function () {
            document.querySelector('#resubmitDialog').close();

        };



        self.resubmitBtnDialog = function () {
            $("#ee-resubmit-btn").addClass("loading");
            self.fileContentValue(JSON.stringify({"payload": self.summaryObservableArray()[self.selectedIndex()].payload, 
                "url": self.summaryObservableArray()[self.selectedIndex()].URL}));
            document.querySelector('#resubmitDialog').open();
            $("#ee-resubmit-btn").removeClass("loading");
        };


        self.submitAction = async function () {
            var tracker = document.getElementById("tracker");
            if (tracker.valid !== "valid") {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            document.querySelector("#resubmitDialog").close();
            requestPayload = self.fileContentValue().replace(/\\/g, "");
            requestPayload = JSON.parse(self.fileContentValue());
            var storedPaload = requestPayload.payload.replace(/\\/g, "");
            var payload = {
                "payload": storedPaload ? storedPaload : "",
                "url": requestPayload.url ? requestPayload.url : "",
                "eitCode": self.selectedRow().eitCode ? self.selectedRow().eitCode : "",
                "eitName": self.selectedRow().eitName ? self.selectedRow().eitName : "",
                "requestId": self.selectedRow().requestId ? self.selectedRow().requestId : "",
                "eitResponseDesc": self.selectedRow().eitResponseDesc ? self.selectedRow().eitResponseDesc : "",
                "eitResponseDate": self.selectedRow().eitResponseDate ? self.selectedRow().eitResponseDate : "",
                "projectName": self.selectedRow().projectName ? self.selectedRow().projectName : "",
                "eitResponseCode": self.selectedRow().eitResponseCode ? self.selectedRow().eitResponseCode : ""

            };
            app.messages.push({severity: "info", summary: self.submittedMsgNofity(), autoTimeout: 0});
            var serviceName = "eff/editRequestTracker/";
            await services.addGeneric(serviceName + self.selectedRow().requestId, payload).then(data => {
                self.resendEitAction();
            }, {});
        };


        self.resendEitAction = function () {
            app.loading(true);
            var payload = {
                "URL": requestPayload.url,
                "payload": requestPayload.payload,
                "eitCode": self.selectedRow().eitCode,
                "eitName": self.selectedRow().eitName,
                "requestId": self.selectedRow().requestId,
                "personNumber": self.selectedRow().personNumber
            };
            var ResendReqCbFn = function () {
                app.loading(false);
                self.getDataBySearch();
                setTimeout(() => {
                    if (self.summaryObservableArray()[self.selectedIndex()].eitResponseCode >= 400) {
                        app.messages.push({severity: "error", summary: self.dataErrMsg(), autoTimeout: 0});
                    } else {
                        app.messages.push({severity: "confirmation", summary: self.dataAddedMsg(), autoTimeout: 0});
                    }
                }, 3000);
            };
            var serviceName = "eff/resend/";
            services.addGeneric(serviceName, payload).then(ResendReqCbFn, app.failCbFn);
        };




        //------------------------------------------------------------//

        function initTranslations() {
            self.title(app.translate("pages.logScreenSummary"));
            self.eitNameLbl(app.translate("logscreen.EITNAME"));
            self.searchLbl(app.translate("others.search"));
            self.backLbl(app.translate("labels.backlbl"));
            self.placeHolder(app.translate("labels.placeHolder"));
            self.personNumberLbl(app.translate("common.personNumberLbl"));
            self.dataNotFoundMsg(app.translate("logscreen.dataNotFoundMsg"));
            self.clearLbl(app.translate("common.clearLbl"));
            self.submit(app.translate("logscreen.submit"));
            self.cancel(app.translate("logscreen.cancel"));
            self.resubmitDialogMsg(app.translate("logscreen.resubmitDialogMsg"));
            self.fileContentLbl(app.translate("logscreen.fileContentLbl"));
            self.resubmitLbl(app.translate("logscreen.resubmitLbl"));
            self.submittedMsgNofity(app.translate("logscreen.submittedMsgNofity"));
            self.dataAddedMsg(app.translate("logscreen.dataAddedMsg"));
            self.dataErrMsg(app.translate("logscreen.dataErrMsg"));

            self.columnArray([
                {"headerText": app.translate("common.personNumberLbl"), "field": "personNumber", "resizable": "enabled"},
                {"headerText": app.translate("logscreen.PROJECTNAME"), "field": "projectName", "resizable": "enabled"},
                {"headerText": app.translate("logscreen.EITCODE"), "field": "eitCode", "resizable": "enabled"},
                {"headerText": app.translate("logscreen.EITNAME"), "field": "eitName", "resizable": "enabled"},
                {"headerText": app.translate("logscreen.EIT_RESPONSE_CODE"), "field": "eitResponseCode", "resizable": "enabled"},
                {"headerText": app.translate("logscreen.EIT_RESPONSE_DESC"), "field": "eitResponseDesc", "resizable": "enabled"},
                {"headerText": app.translate("logscreen.EIT_RESPONSE_DATE"), "field": "eitResponseDate", "resizable": "enabled"},
                {"headerText": app.translate("logscreen.requestLbl"), "field": "requestId", "resizable": "enabled"}

            ]);
        }
        initTranslations();
    }
    return new logScreenSummaryViewModel();
});
