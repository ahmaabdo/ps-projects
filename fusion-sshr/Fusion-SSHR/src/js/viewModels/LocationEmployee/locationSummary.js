

define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'ojs/ojtable',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addBtnLbl = ko.observable();
                self.clearBtnLbl = ko.observable();
                self.SequanceLbl = ko.observable();
                self.typelbl = ko.observable();
                self.backBtnLbl = ko.observable();
                self.isDisable = ko.observable();
                self.stepArray = ko.observableArray([]);
                self.currentStepValue = ko.observable('stp1');
                self.locationNameLbl=ko.observable();
                self.type=ko.observable();
                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'personId'}));

                self.tableSelectionListener = function () {
                    self.isDisable(false);
                };


                self.btnAddSummary = function ()
                {
                    app.loading(true);
                    oj.Router.rootInstance.go('locationEmployee');

                };

                self.getAllSumary = function () {
                    var LocationEmployeeCbFn = function (data) {
                        
                        $.each(data, function (index) {

                            self.summaryObservableArray.push({

                                sequance: index + 1,
                                locationName: data[index].locationName,
                                personId: data[index].personId,
                                type: data[index].type

                            });
                        });
                    };
                    var serviceName = "locationEmp/getAll";
                    services.getGeneric(serviceName, {}).then(LocationEmployeeCbFn, app.failCbFn);
                };

                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.summaryObservableArray([]);
                }

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getAllSumary();


                };
                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };


                self.cancelButtonDelete = function () {
                    document.querySelector("#yesNoDialog").close();
                };


                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {

                    self.SequanceLbl(getTranslation("common.SequanceLbl"));
                    self.addBtnLbl(getTranslation("common.addBtnLbl"));
                    self.backBtnLbl(getTranslation("common.backBtnLbl"));
                    self.clearBtnLbl(getTranslation("common.clearBtnLbl"));
                    self.typelbl(getTranslation("common.typelbl"));
                    self.locationNameLbl(getTranslation("common.locationNameLbl"));


                    self.columnArray([

                        {
                            "headerText": self.SequanceLbl(),
                            "field": "sequance"
                        },

                        {"headerText": self.locationNameLbl(),
                            "field": "locationName"},
                        
                         {"headerText": self.typelbl(),
                            "field": "type"}



                    ]);
                 

                }
                initTranslations();
            }

            return JobContentViewModel;
        });
