/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} services
 * @param {type} commonhelper
 * @returns {searchPositionL#15.homeministryJobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojselectcombobox', 'ojs/ojvalidationgroup', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojtrain', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojknockout-validation',
    'ojs/ojdialog', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'ojs/ojpagingtabledatasource',
    'jquery-calendar-picker'], function (oj, ko, $, app, services, commonhelper, commonUtil) {

    function SummaryPositionbContentViewModel() {
        var self = this;
        var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
        self.columnArray = ko.observableArray();
        self.columnArray1 = ko.observableArray();
        self.isRequired = ko.observable(true);
        self.placeholder = ko.observable();
        self.dataSourceTB2 = ko.observable();
        var deptArray = [];
        self.oprationdisabled = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.deptObservableArray = ko.observableArray(deptArray);
        self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'DepartmentId'});
        self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'}));
        self.jobCatagoryList = ko.observableArray();
        self.jobCodeArr = ko.observableArray(app.globalJobCodes());
        self.gradeArr = ko.observableArray(app.globalHrGrades());
        self.positionNameArr = ko.observableArray(app.globalPositionsName());
        self.testDate = ko.observable();
        self.searchdisabled = ko.observable(true);
        self.cleardisabled = ko.observable(true);
        self.checkReturnSearch = ko.observable();
        self.checkResultSearch = ko.observable();
        self.formatDate = function (date) {
            //var date = new Date()
            var month = '' + (date.getMonth() + 1),
                    day = '' + date.getDate(),
                    year = date.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        };

        self.getDate = function (date) {
            //var date = new Date()
            var number_Of_DaysInMonth = 0;
            var month = '' + (date.getMonth() + 1),
                    day = '' + (date.getDate()),
                    year = date.getFullYear();
            if (month == "1") {
                number_Of_DaysInMonth = 31;
            } else if (month == "2")
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                    number_Of_DaysInMonth = 29;
                } else {
                    number_Of_DaysInMonth = 28;
                }
            else if (month == "3") {
                number_Of_DaysInMonth = 31;
            } else if (month == "4") {
                number_Of_DaysInMonth = 30;
            } else if (month == "5") {
                number_Of_DaysInMonth = 31;
            } else if (month == "6") {
                number_Of_DaysInMonth = 30;
            } else if (month == "7") {
                number_Of_DaysInMonth = 31;
            } else if (month == "8") {
                number_Of_DaysInMonth = 31;
            } else if (month == "9") {
                number_Of_DaysInMonth = 30;
            } else if (month == "10") {
                number_Of_DaysInMonth = 31;
            } else if (month == "11") {
                number_Of_DaysInMonth = 30;
            } else if (month == "12") {
                number_Of_DaysInMonth = 31;
            }
            if (number_Of_DaysInMonth == 28 || number_Of_DaysInMonth == 29)
            {
                if (day <= "27")
                {
                    month = '' + (date.getMonth() + 1),
                            day = '' + (date.getDate() + 1),
                            year = date.getFullYear();
                } else
                {
                    month = '' + (date.getMonth() + 1),
                            day = '' + 01,
                            year = date.getFullYear();
                }

            } else if (number_Of_DaysInMonth == 30 || number_Of_DaysInMonth == 31)
            {
                if (day < "30") {
                    month = '' + (date.getMonth() + 1),
                            day = '' + (date.getDate() + 1),
                            year = date.getFullYear();
                } else
                {
                    month = '' + (date.getMonth() + 2),
                            day = '' + 01,
                            year = date.getFullYear();
                }
            }
            return [year, month, day].join('-');
        };
        self.dateEffictive = ko.observable("");
//        self.currentDate = ko.observable(self.formatDate(new Date()));
        self.currentDate = ko.observable();

        self.orgnizationArr = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);
        self.generalGroupList = ko.observableArray([]);
        self.ministryPositionbSearch = {
            taxonomicCode: ko.observable(),
            ranked: ko.observable(),
            postionNumber: ko.observable(),
            managing: ko.observable(),
            positionName: ko.observable(),
            postionNumber: ko.observable()

        };
        self.searchPerson = function () {


            var searchePositionCbFn = function (data) {
                if (data.length !== 0) {

                    self.deptObservableArray(data);

                    for (var i = 0; i < data.length; i++) {
                        data[i].taxonomicCodeLbl = searchArray(parseInt(data[i].taxonomicCode), self.jobCodeArr());
                        data[i].rankedLbl = searchArray(data[i].ranked, self.gradeArr());
                        data[i].positionNameLbl = searchArray(data[i].positionName, self.positionNameArr());

                    }
                    //self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'jobId'});  
                } else {
                    $.notify(self.checkResultSearch(), "error");

                    self.deptObservableArray([]);
                }
            };
            var failCbFn = function () {
                $.notify(self.checkReturnSearch(), "error");
            };
            services.searcheGeneric(commonhelper.positionSearchUrl, self.ministryPositionbSearch).then(searchePositionCbFn, failCbFn);
        };

        self.searchPerson2 = function () {
            var searchePositionCbFn = function (data) {
                self.deptObservableArray([]);
            };
            services.searcheGeneric(commonhelper.positionSearchUrl, "").then(searchePositionCbFn, app.failCbFn);
        };

        self.getGrade = function () {
            var gradeCbFn = function (data) {
                $.each(data, function (index) {
                    self.gradeArr.push({
                        value: data[index].gradeId,
                        label: data[index].gradeName

                    });
                });
            };
            var failCbFn = function () {
            };

            services.getGrade(commonhelper.gradeUrl).then(gradeCbFn, failCbFn);
        };




        self.handleAttached = function (info) {
            app.loading(false);

            self.getGrade();
//        var input = document.createElement("oj-input-text");
//           input.setAttribute('disabled', 'true');
//           input.setAttribute('value','{{ministryPositionbSearch.postionNumber}}');
//           
//           
//           var parent = document.getElementById("xx");
//                  parent.appendChild(input);
            self.HijrahDate();
            app.globalEffictiveDate(self.currentDate());
            $("#datePickerStartDate2").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                onSelect: function (dates) {
                    var endDate = $('#datePickerStartDate2').val();
                    self.positionModle.endDate(endDate);
                }});
            $("#datePickerStartDate1").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                onSelect: function (dates) {
                    var startDate = $('#datePickerStartDate1').val();
                    self.positionModle.startDate(startDate);
                }});
            var getOrgnizationCBF = function (data) {
                self.orgnizationArr([]);
                for (var i = 0; i < data.length; i++) {
                    self.orgnizationArr.push({
                        "value": data[i].organizationCode, "label": data[i].name, "labelAr": data[i].name
                    });
                }
            };
            services.getGeneric(commonhelper.orgnizationUrl, {}).then(getOrgnizationCBF, self.failCbFn);
        };

        self.reset = function () {

            // 

            clearContent();

        };
        function clearContent() {

            self.ministryPositionbSearch.positionName("");
            self.ministryPositionbSearch.ranked("");
            self.ministryPositionbSearch.taxonomicCode("");
            self.ministryPositionbSearch.managing("");
//            self.ministryPositionbSearch.postionNumber("");
//            self.searchPerson2();
            self.deptObservableArray([]);
            self.searchdisabled(true);
            self.cleardisabled(true);

            //location.reload();

        }
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.oprationdisabled(false);
            self.selectedRowKey(currentRow['rowKey']);
            self.selectedIndex(currentRow['rowIndex']);
        };

        self.addJob = function () {
            oj.Router.rootInstance.store('add');
            oj.Router.rootInstance.go('position');
        };
        self.updateJob = function () {
            ////oj.Router.rootInstance.store('update');
            self.deptObservableArray()[self.selectedIndex()].type = "update";
            oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
            oj.Router.rootInstance.go('job');
        };
        self.viewJob = function () {
            self.deptObservableArray()[self.selectedIndex()].type = "view";
            oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
            oj.Router.rootInstance.go('position');
        };
        self.historyDetails = ko.observable();

        //---------History Section ----------------------------------------
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };
        self.openDialog = function () {
            var positionHistoryCBF = function (data) {
                if (data) {


                    for (var i = 0; i < data.length; i++) {
                        data[i].taxonomicCodeLbl = searchArray(parseInt(data[i].taxonomicCode), self.jobCodeArr());
                        data[i].rankedLbl = searchArray(data[i].ranked, self.gradeArr());
                        data[i].positionNameLbl = searchArray(data[i].positionName, self.positionNameArr());
                    }
                }
                var Data = data;
                self.dataSourceTB2(new oj.ArrayTableDataSource(Data));
            };
            var code = {
                code: self.deptObservableArray()[self.selectedIndex()].code
            };
            services.searcheGeneric(commonhelper.positionSearchCodeUrl, code).then(positionHistoryCBF, app.failCbFn);
            document.querySelector("#modalDialog1").open();
        };
        //-----------------------End History Section -------------------------------------
        ////----------------------------Effective Date Section--------------------
        self.closeCancelDialog = function () {
            $("#effictiveDateDialog").ojDialog("close");
        };
        self.openEffectiveDate = function () {
            document.querySelector("#effictiveDateDialog").open();


        };
        self.closeDialogDate = function () {
            $("#effictiveDateDialog").ojDialog("close");
            self.getEffictiveDate();
            if (self.dateEffictive()) {
                var x = self.dateEffictive();
                app.globalEffictiveDate(x);
            } else {
                self.dateEffictive("");
                app.globalEffictiveDate(self.currentDate());
            }
        };
        self.restEffectiveDate = function () {
            self.dateEffictive("");
            self.currentDate("");
        };
        app.globalEffictiveDate(self.currentDate());
        //--------------------------End Effective Date Section--------------------
        //------------------Section For Additional Details -----------------------------
        self.detailsOfJob = function () {
            oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
            oj.Router.rootInstance.go('positionSummaryAdditionalDetails');


        };
        //-------------------------End Of Secttion 
        //-------------------------Start Get Effictive Start Date And End Date
        self.getEffictiveDate = function () {
        };
        ////---------Edit Section ----------------------------------------
        self.nextBtnVisible = ko.observable(true);
        self.addBtnVisible = ko.observable(false);
        self.serviceType = ko.observable('EDIT');
        self.isDisabled = ko.observable(false);
        self.currentStepValue = ko.observable('stp1');
        self.confirmMessage = ko.observable();
        self.disableSubmit = ko.observable(false);
        self.stepArray = ko.observableArray([]);
        self.groupValid = ko.observable();
        self.isSelectAction = ko.observable(false);
        self.isDisabledStartDate = ko.observable(true);
        self.isDisabledEndDate = ko.observable(true);
        self.isDisabledTaxonomicCode = ko.observable(true);
        self.isDisabledRanked = ko.observable(true);
        self.isDisabledPositionNumber = ko.observable(true);
        self.isDisabledManaginge = ko.observable(true);
        self.isDisabledPositionName = ko.observable(true);
        self.isVisible = ko.observable(true);
        self.messages = ko.observable();
        self.stepArray([{label: 'hh', id: 'stp1'},
            {label: 'kk', id: 'stp2'}
        ]);
//       self.actionsArr = ko.observableArray([
//      {value: 'Downgrade_Position', label: 'خفض وضيفة'},
//      {value: 'Upgrade_Position',  label: 'رفع وظيفة'},
//      {value: 'Update_Position_Name\Code_Position',   label: 'تحوير وظيفة'},
//      {value: 'Transfer_From_out_of_Ministry_Create',    label: 'سلخ وظيفة من جهة (إحداث)'},
//      {value: 'Transfer_out_of_Ministry_End',   label: 'سلخ وظيفة الى جهة (إلغاء)'},
//      {value: 'End_Position',   label: 'الغاء وظيفة'},
//      {value: 'Transfer_Position',   label: 'نقل وظيفة'},
//      {value: 'Upgrade_Transfer_Position',   label: 'رفع و نقل وظيفة'},
//      {value: 'Downgrade_Transfer_Position',   label: 'خفض و نقل وظيفة'},
//      {value: 'Update_Transfer_Position',   label: 'تحوير ونقل وظيفة'},
//      {value: 'Upgrade_Update_Position_Name\Code_Position',   label: 'رفع و تحوير وظيفة'},
//      {value: 'Downgrade_Update_Position_Name\Code_Position',   label: 'خفض و تحوير وظيفة'},
//      {value: 'Upgrade_Update_Position_Name\Code_Transfer_Position',   label: 'رفع و تحوير ونقل وظيفة'},
//      {value: 'Downgrade_Update_Position Name\Code_Transfer_Position',   label: 'خفض و تحوير ونقل وظيفة'}
//    ]);
        self.actionsArr = ko.observableArray(app.globalPositionsAction());


        self.positionModle = {
//            startDate: ko.observable(self.formatDate(new Date())),
            startDate: ko.observable(),
            endDate: ko.observable(),
            id: ko.observable(),
            taxonomicCode: ko.observable(),
            ranked: ko.observable(),
            managing: ko.observable(),
            positionNumber: ko.observable(),
            positionName: ko.observable(),

            code: ko.observable(),
            action: ko.observable(),
            flag: ko.observable('Y'),

            createdBy: ko.observable(rootViewModel.personDetails().personId),
            createdDate: ko.observable(self.formatDate(new Date())),
            updatedBy: ko.observable(rootViewModel.personDetails().personId),
            updateDate: ko.observable(self.formatDate(new Date())),
            startDateGregorian: ko.observable()
        };
        self.previousStep = function () {
            var prev = document.getElementById("train").getPreviousSelectableStep();
            if (prev != null)
                self.currentStepValue(prev);
        };
        self.nextStep = function () {
            var tracker = document.getElementById("tracker");
            if (tracker.valid !== "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            var next = document.getElementById("train").getNextSelectableStep();
            if (next !== null) {
                if (self.positionModle.startDate() >= self.positionModle.endDate())
                {
                    $.notify(self.DateValidMessage(), "error");


                } else
                {
                    self.currentStepValue(next);
                }
            }
        };
        self.currentStepValueText = function () {
            if (self.currentStepValue() === 'stp2') {
                self.isDisabled(true);
                self.addBtnVisible(true);
                self.nextBtnVisible(false);
                $('#datePickerStartDate').calendarsPicker('disable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(true);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(true);
            } else {
                self.isDisabled(false);
                self.addBtnVisible(false);
                self.nextBtnVisible(true);
                controllScreen();
            }
            return self.positionTitel();
        };
        self.stopSelectListener = function (event, ui) {
            var tracker = document.getElementById("tracker");
            if (tracker.valid !== "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                event.preventDefault();
                return;
            }
        };
        self.submitButton = function () {
            document.querySelector("#yesNoDialog").open();
        };
        self.closeDialogEdit = function () {
            var popup = document.querySelector('#CloseEditPopup');
            popup.open();
        };
        self.YesCancelDialogButton = function ()
        {
            self.positionModle.action('');
            self.isSelectAction(false);
            controllScreen();
            var popup = document.querySelector('#editDialog1');
            popup.close();
            var popup = document.querySelector('#CloseEditPopup');
            popup.close();
            //$("#editDialog1").ojDialog("close");
        };
        self.NoCancelDialogButton = function () {
            var popup = document.querySelector('#CloseEditPopup');
            popup.close();
        };
        self.setModelData = function () {
            self.positionModle.startDate(self.formatDate(new Date(self.deptObservableArray()[self.selectedIndex()].startDate)));
            self.positionModle.taxonomicCode(self.deptObservableArray()[self.selectedIndex()].taxonomicCodeLbl);
            self.positionModle.ranked(self.deptObservableArray()[self.selectedIndex()].ranked);
            self.positionModle.positionNumber(parseInt(self.deptObservableArray()[self.selectedIndex()].positionNumber));
            self.positionModle.positionName(self.deptObservableArray()[self.selectedIndex()].positionName);
            self.positionModle.managing(self.deptObservableArray()[self.selectedIndex()].managing);
            self.positionModle.code(self.deptObservableArray()[self.selectedIndex()].code);
            self.positionModle.id(self.deptObservableArray()[self.selectedIndex()].id);
            if (self.deptObservableArray()[self.selectedIndex()].endDate) {
                self.positionModle.endDate(self.formatDate(new Date(self.deptObservableArray()[self.selectedIndex()].endDate)));
            }
        };

        self.setModelDataTwo = function () {
            var EffStartDate = self.getDate(new Date(self.currentDate()));
            var converStartDate = self.formatDate(new Date(EffStartDate));

            if (self.deptObservableArray()[self.selectedIndex()].startDate) {
                self.positionModle.startDate(converStartDate);
            }
            self.positionModle.taxonomicCode(self.deptObservableArray()[self.selectedIndex()].taxonomicCodeLbl);
            self.positionModle.ranked(self.deptObservableArray()[self.selectedIndex()].ranked);
            self.positionModle.positionNumber(parseInt(self.deptObservableArray()[self.selectedIndex()].positionNumber));
            self.positionModle.positionName(self.deptObservableArray()[self.selectedIndex()].positionName);
            self.positionModle.managing(self.deptObservableArray()[self.selectedIndex()].managing);
            self.positionModle.code(self.deptObservableArray()[self.selectedIndex()].code);
            self.positionModle.id(self.deptObservableArray()[self.selectedIndex()].id);
            if (self.deptObservableArray()[self.selectedIndex()].endDate) {
                self.positionModle.endDate(self.formatDate(new Date(self.deptObservableArray()[self.selectedIndex()].endDate)));
            }
        };
        self.openDialogEdit = function (event) {
            document.querySelector("#editDialog1").open();
            self.positionModle.startDateGregorian(self.formatDate(new Date(self.deptObservableArray()[self.selectedIndex()].startDate)));
            if (self.positionModle.startDateGregorian() == app.globalEffictiveDate()) {
                correction();
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(true);
                // self.isVisible(false);
                self.setModelData();
                self.positionModle.action('Correction Record');
            } else if (self.positionModle.startDateGregorian() > app.globalEffictiveDate())
            {
                correction();
                self.serviceType('Update_Change_Insert');
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(true);
                // self.isVisible(false);
                self.setModelDataTwo();
                self.positionModle.action('Update Change Insert Record');
            } else {

                self.serviceType('EDIT');
                self.isSelectAction(true);


                self.oprationMessage(self.editMassageLbl());
//				 $('#datePicker').calendarsPicker('disable');
                $('#datePickerStartDate').calendarsPicker('disable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.positionModle.startDate(self.formatDate(new Date(self.deptObservableArray()[self.selectedIndex()].startDate)));
                self.positionModle.taxonomicCode(self.deptObservableArray()[self.selectedIndex()].taxonomicCode);
                self.positionModle.ranked(self.deptObservableArray()[self.selectedIndex()].ranked);
                self.positionModle.positionNumber(parseInt(self.deptObservableArray()[self.selectedIndex()].positionNumber));
                self.positionModle.positionName(self.deptObservableArray()[self.selectedIndex()].positionName);
                self.positionModle.managing(self.deptObservableArray()[self.selectedIndex()].managing);
                self.positionModle.code(self.deptObservableArray()[self.selectedIndex()].code);
                self.positionModle.id(self.deptObservableArray()[self.selectedIndex()].id);
                self.positionModle.startDateGregorian(self.deptObservableArray()[self.selectedIndex()].startDateGregorian);

                if (self.deptObservableArray()[self.selectedIndex()].endDate) {
                    self.positionModle.endDate(self.formatDate(new Date(self.deptObservableArray()[self.selectedIndex()].endDate)));
                }


                self.isVisible(true);

                self.setModelData();

            }
        };
        self.oprationServiceRecord = function () {
            var jsonData = ko.toJSON(self.positionModle);
            var editPositionCBF = function (data) {
                var Data = data;
                self.positionModle.action('');
                self.isSelectAction(false);
                controllScreen();
                $("#editDialog1").ojDialog("close");
                self.disableSubmit(false);

            };
            services.addGeneric(commonhelper.positionUrl + self.serviceType(), jsonData).then(editPositionCBF, self.failCbFn);
        };
        self.commitRecord = function (data, event) {
            if (!self.disableSubmit()) {
                self.disableSubmit(true);
                self.oprationServiceRecord();
                location.reload();
                return true;
            }
        };
        function controllScreen() {
            $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                onSelect: function (dates) {
                    var endDate = $('#datePickerEndDate').val();
                    self.positionModle.endDate(endDate);
                }});
            $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                onSelect: function (dates) {
                    var startDate = $('#datePickerStartDate').val();
                    self.positionModle.startDate(startDate);
                }});


            if (self.positionModle.action() == 'DWG') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(true);
            } else if (self.positionModle.action() == 'UPG') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(true);
            } else if (self.positionModle.action() == 'UPD') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(false);
                self.isDisabledRanked(true);
                self.isDisabledPositionNumber(false);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(false);
            } else if (self.positionModle.action() == 'TRANS_FROM') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(false);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(false);
                self.isDisabledManaginge(false);
                self.isDisabledPositionName(false);
            } else if (self.positionModle.action() == 'TRANS_OUT') {
                $('#datePickerStartDate').calendarsPicker('disable');
                $('#datePickerEndDate').calendarsPicker('enable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(true);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(true);
            } else if (self.positionModle.action() == 'ELIMINATE') {
                $('#datePickerStartDate').calendarsPicker('disable');
                $('#datePickerEndDate').calendarsPicker('enable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(true);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(true);
            } else if (self.positionModle.action() == 'TRANS') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(true);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(false);
                self.isDisabledPositionName(true);
            } else if (self.positionModle.action() == 'UPG_TRANS') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(false);
                self.isDisabledPositionName(true);
            } else if (self.positionModle.action() == 'DWG_TRANS') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(true);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(true);
                self.isDisabledManaginge(false);
                self.isDisabledPositionName(true);
            } else if (self.positionModle.action() == 'UPD_TRANS') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(false);
                self.isDisabledRanked(true);
                self.isDisabledPositionNumber(false);
                self.isDisabledManaginge(false);
                self.isDisabledPositionName(false);
            } else if (self.positionModle.action() == 'UPG_ UPD') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(false);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(false);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(false);
            } else if (self.positionModle.action() == 'DWG_ UPD') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(false);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(false);
                self.isDisabledManaginge(true);
                self.isDisabledPositionName(false);
            } else if (self.positionModle.action() == 'UPG_ UPD_ TRANS') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(false);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(false);
                self.isDisabledManaginge(false);
                self.isDisabledPositionName(false);
            } else if (self.positionModle.action() == 'DWG _ UPD_ TRANS') {
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('disable');
                self.isDisabledTaxonomicCode(false);
                self.isDisabledRanked(false);
                self.isDisabledPositionNumber(false);
                self.isDisabledManaginge(false);
                self.isDisabledPositionName(false);
            } else {
                self.isSelectAction(false);
            }
        }

        /////////////Correction Section///////////
        function correction() {

            $('#datePickerStartDate').calendarsPicker('enable');
            $('#datePickerEndDate').calendarsPicker('disable');
            self.serviceType('CORRECTION');
            self.isSelectAction(true);


//            self.oprationMessage(self.editMassageLbl());
//				 $('#datePicker').calendarsPicker('disable');
            $('#datePickerEndDate').calendarsPicker('disable');
            self.positionModle.startDate(self.formatDate(new Date(self.deptObservableArray()[self.selectedIndex()].startDate)));
            self.positionModle.taxonomicCode(self.deptObservableArray()[self.selectedIndex()].taxonomicCode);
            self.positionModle.ranked(self.deptObservableArray()[self.selectedIndex()].ranked);
            self.positionModle.positionNumber(parseInt(self.deptObservableArray()[self.selectedIndex()].positionNumber));
            self.positionModle.positionName(self.deptObservableArray()[self.selectedIndex()].positionName);
            self.positionModle.managing(self.deptObservableArray()[self.selectedIndex()].managing);
            self.positionModle.code(self.deptObservableArray()[self.selectedIndex()].code);
            self.positionModle.id(self.deptObservableArray()[self.selectedIndex()].id);
            self.positionModle.startDateGregorian(self.deptObservableArray()[self.selectedIndex()].startDateGregorian);

            if (self.deptObservableArray()[self.selectedIndex()].endDate) {
                self.positionModle.endDate(self.formatDate(new Date(self.deptObservableArray()[self.selectedIndex()].endDate)));
            }
        }
        ;
        //////////End Correction Section///////////////////
        self.actionChangedHandler = function () {
            self.isSelectAction(true);
            self.setModelData();
            controllScreen();
        };
        //-----------------------End Edit Section -------------------------------------
        //-------------------Correction  Section -------------------------------------
        self.openDialogCorrection = function () {
            self.serviceType('CORRECTION');
            self.setModelData();
            document.querySelector("#editDialog1").open();
        };
        //-------------------End Of Correction Section -------------------------------

        self.cancelButton = function () {
            var popup = document.querySelector("#yesNoDialog");
            popup.close();
        };
        //***********************Validation Date************************************************
        self.ValidEndDate = function () {
            if (self.positionModle.startDate() >= self.positionModle.endDate())
            {
                $.notify(self.DateValidMessage(), "error");


            }
        };

        self.cancelValidDateButton = function () {
            self.previousStep();
            document.querySelector("#ValidDate").close();
        };
        //***********************End Validation Date*******************************************

        self.positionNameChangedHandler = function (event, data) {
            if (event.detail.value != "")
            {
                self.searchdisabled(false);
                self.cleardisabled(false);
            } else
            {
                self.searchdisabled(true);
                self.cleardisabled(true);
            }

        };

        self.taxonomicCodeChangedHandler = function (event, data) {
            if (event.detail.value != "")
            {
                self.searchdisabled(false);
                self.cleardisabled(false);
            } else
            {
                self.searchdisabled(true);
                self.cleardisabled(true);
            }
        };

        self.rankedChangedHandler = function (event, data) {
            if (event.detail.value != "")
            {
                self.searchdisabled(false);
                self.cleardisabled(false);
            } else
            {
                self.searchdisabled(true);
                self.cleardisabled(true);
            }
        };

        self.managingChangedHandler = function (event, data) {
            if (event.detail.value != "")
            {
                self.searchdisabled(false);
                self.cleardisabled(false);
            } else
            {
                self.searchdisabled(true);
                self.cleardisabled(true);
            }
        };

        self.postionNumberChangedHandler = function (event, data) {
            if (event.detail.value != "")
            {
                self.searchdisabled(false);
                self.cleardisabled(false);
            } else
            {
                self.searchdisabled(true);
                self.cleardisabled(true);
            }
        };
        //*********************//
        self.HijrahDate = function () {

            var getHijrahDateCbFn = function (data) {

                self.currentDate(data);
                ;

            };
            var failCbFn = function () {
            };

            var serivename = commonUtil.getDate;
            services.getGeneric(serivename).then(getHijrahDateCbFn, failCbFn);
        };
        //*********************//

        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();
        self.startDateLbl = ko.observable();
        self.endDateLbl = ko.observable();
        self.positionNameLbl = ko.observable();
        self.positionNumberLbl = ko.observable();
        self.classificationSymbolLbl = ko.observable();
        self.labeledPositionLbl = ko.observable();
        self.rankedLbl = ko.observable();
        self.positionDepartmentLbl = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.additionalDetailsLbl = ko.observable();
        self.historyLbl = ko.observable();
        self.correctionLbl = ko.observable();
        self.validGradeLbl = ko.observable();
        self.positionLbl = ko.observable();
        self.submit = ko.observable();
        self.next = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.pervious = ko.observable();
        self.positionTitel = ko.observable();
        self.trainView = ko.observable();
        self.cancel = ko.observable();
        self.validGradeLbl = ko.observable();
        self.search = ko.observable();
        self.clear = ko.observable();
        self.view = ko.observable();
        self.oprationMessage = ko.observable();
        self.actionLbl = ko.observable();
        self.effectiveDate = ko.observable();
        self.todayDay = ko.observable();
        self.resetLabel = ko.observable();
        self.DateValidMessage = ko.observable();
        self.sequenceLbl = ko.observable();
        self.CancelDialogMessage = ko.observable();
        self.editMassageLbl = ko.observable();
        self.ActionTypeLbl = ko.observable();
        self.EditDialogPopup = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();

            }

        });
        function initTranslations() {
            self.positionTitel(getTranslation("position.positionEdit"));
            self.next(getTranslation("others.next"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.validGradeLbl(getTranslation("others.validGrade"));
            self.additionalDetailsLbl(getTranslation("others.additonalDetail"));
            self.historyLbl(getTranslation("others.history"));
            self.correctionLbl(getTranslation("others.correction"));
            self.startDateLbl(getTranslation("job.startDate"));
            self.endDateLbl(getTranslation("job.endDate"));
            self.positionNameLbl(getTranslation("position.positionName"));
            self.positionNumberLbl(getTranslation("position.positionNumber"));
            self.classificationSymbolLbl(getTranslation("position.classificationSymbol"));
            self.labeledPositionLbl(getTranslation("position.labeledPosition"));
            self.rankedLbl(getTranslation("position.ranked"));
            self.positionDepartmentLbl(getTranslation("position.positionDepartment"));
            self.positionLbl(getTranslation("pages.position"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.trainView(getTranslation("others.review"));
            self.pervious(getTranslation("others.pervious"));
            self.cancel(getTranslation("others.cancel"));
            self.submit(getTranslation("others.submit"));
            self.actionLbl(getTranslation("position.action"));
            self.search(getTranslation("others.search"));
            self.clear(getTranslation("others.clear"));
            self.view(getTranslation("others.view"));
            self.effectiveDate(getTranslation("others.effectiveDate"));
            self.todayDay(getTranslation("others.todayDay"));
            self.cancel(getTranslation("others.cancel"));
            self.resetLabel(getTranslation("login.resetLabel"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.sequenceLbl(getTranslation("labels.Sequence"));
            self.DateValidMessage(getTranslation("labels.DateValidMessage"));
            self.historyDetails(getTranslation("position.historyDetails"));
            self.CancelDialogMessage(getTranslation("labels.CancelMessage"));
            self.confirmMessage(getTranslation("labels.confirmMessage"));
            self.editMassageLbl(getTranslation("position.EditMessage"));
            self.ActionTypeLbl(getTranslation("position.ActionType"));
            self.EditDialogPopup(getTranslation("position.EditDialog"));
            self.checkReturnSearch(getTranslation("position.checkReturnSearch"));
            self.checkResultSearch(getTranslation("common.checkResultSearch"));

            self.columnArray([
                {
                    "headerText": self.labeledPositionLbl(), "field": "positionNameLbl"
                },
                {
                    "headerText": self.classificationSymbolLbl(), "field": "taxonomicCodeLbl"
                },
                {
                    "headerText": self.rankedLbl(), "field": "rankedLbl"
                },
                {
                    "headerText": self.positionNumberLbl(), "field": "positionNumber"
                },
                {
                    "headerText": self.positionDepartmentLbl(), "field": "managing"
                },
                {
                    "headerText": self.startDateLbl(), "field": "startDate"
                },
                {
                    "headerText": self.endDateLbl(), "field": "endDate"
                }
            ]);

            self.columnArray1([
                {
                    "headerText": self.sequenceLbl(), "field": "sequence"
                },
                {
                    "headerText": self.startDateLbl(), "field": "startDate"
                },
                {
                    "headerText": self.endDateLbl(), "field": "endDate"
                },
                {
                    "headerText": self.classificationSymbolLbl(), "field": "taxonomicCode"
                },
                {
                    "headerText": self.rankedLbl(), "field": "ranked"
                },
                {
                    "headerText": self.positionNumberLbl(), "field": "positionNumber"
                },
                {
                    "headerText": self.positionDepartmentLbl(), "field": "managing"
                },
                {
                    "headerText": self.labeledPositionLbl(), "field": "positionNameLbl"
                },
                {
                    "headerText": self.ActionTypeLbl(), "field": "action"
                }
            ]);
            self.stepArray([{label: self.positionTitel(), id: 'stp1'},
                {label: self.trainView(), id: 'stp2'}
            ]);
        }
        initTranslations();




    }

    return SummaryPositionbContentViewModel;
});
