/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * position module
 *//**
  * @param {type} oj
  * @param {type} ko
  * @param {type} $
  * @param {type} app
  * @param {type} commonUtil
  * @param {type} services
  * @returns {positionL#18.positionContentViewModel}  */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup'], function (oj, ko, $, app, commonUtil, services) {
    /**
     * The view model for the main content view template
     */
    function positionContentViewModel() {
        var self = this;
        var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
        self.canceheaderMessage = ko.observable();
        self.cancelbodymessage = ko.observable();

        self.currentStepValue = ko.observable('stp1');

        self.currentStepValueText = function () {

            if (self.currentStepValue() === 'stp2' && self.serviceType() !== 'VIEW') {
                self.isDisabled(true);
                self.addBtnVisible(true);
                self.nextBtnVisible(false);
                $('#datePickerStartDate').calendarsPicker('disable');
                $('#datePickerEndDate').calendarsPicker('disable');
            } else if (self.serviceType() === 'VIEW') {
                self.nextBtnVisible(false);
                self.addBtnVisible(false);
                self.isDisabled(true);
                $('#datePickerStartDate').calendarsPicker('disable');
                $('#datePickerEndDate').calendarsPicker('disable');
            } else {
                self.isDisabled(false);
                self.addBtnVisible(false);
                self.nextBtnVisible(true);
                $('#datePickerStartDate').calendarsPicker('enable');
                $('#datePickerEndDate').calendarsPicker('enable');
            }
            return self.jobTitel();
        };
        self.previousStep = function () {
            var prev = document.getElementById("train").getPreviousSelectableStep();
            if (prev !== null)
                self.currentStepValue(prev);
        };
        self.nextStep = function () {
//            var zz =$('#datePicker').val();                          
//             $('#datePicker').calendarsPicker({ 
//            dateFormat: 'yyyy-mm-dd', showTrigger: '#calImg'});
//            self.positionModle.endDate(zz);
            self.ValidEndDate();
            var tracker = document.getElementById("tracker");
            if (tracker.valid !== "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            var next = document.getElementById("train").getNextSelectableStep();
            if (next !== null)
                self.currentStepValue(next);
        };
        self.stopSelectListener = function (event, ui) {
            var tracker = document.getElementById("tracker");
            if (tracker.valid !== "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                event.preventDefault();
                return;
            }
        };
        self.cancelAction = function () {
//            if (oj.Router.rootInstance._navHistory.length > 1) {
//                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
//            }
            document.querySelector("#cancelDialog").open();
        };

        self.cancelActionposition = function () {

            if (oj.Router.rootInstance._navHistory.length > 1) {
                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
            }
            oj.Router.rootInstance.go('searchPosition');
        };

        self.canceldposition = function () {

            document.querySelector("#cancelDialog").close();

        };
        self.oprationServiceRecord = function () {
//            self.positionModle.code(self.positionModle.positionName() + "." + self.positionModle.positionNumber());
            var jsonData = ko.toJSON(self.positionModle);
            var getValidGradeCBF = function (data) {
                var Data = data;
                self.disableSubmit(false);
                if (oj.Router.rootInstance._navHistory.length > 1) {
                    oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                } else {
                    oj.Router.rootInstance.go('home');
                }
            };
            services.addGeneric(commonUtil.positionUrl + self.serviceType(), jsonData).then(getValidGradeCBF, self.failCbFn);
        };
        self.commitRecord = function (data, event) {
            if (!self.disableSubmit()) {
                app.loading(true);
                self.disableSubmit(true);
                self.oprationServiceRecord();
                return true;
            }
        };
        self.submitButton = function () {
            document.querySelector("#yesNoDialog").open();
        };
        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
        };
        self.failCbFn = function (xhr) {
            self.messages([{
                    severity: 'error',
                    summary: 'Error to Add data',
                    detail: '',
                    autoTimeout: 0
                }
            ]);
        };
        self.commitDraft = function (data, event) {
            return true;
        };
        self.reset = function () {
            clearContent();
        };
        function clearContent() {
        }
        self.handleAttached = function (info) {
            app.loading(false);
            var calendar = $.calendars.instance();
//                var tempDate = calendar.parseDate('mm/dd/yyyy', '');
//                tempDate = calendar.formatDate('yyyy/mm/dd', tempDate);
            $("#datePickerStartDate").val(self.retrieve.startDate);
            $("#datePickerEndDate").val(self.retrieve.endDate);
            $("#datePickerEndDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                onSelect: function (dates) {
                    var endDate = $('#datePickerEndDate').val();
                    self.positionModle.endDate(endDate);
                }});
            $("#datePickerStartDate").calendarsPicker({calendar: $.calendars.instance('ummalqura'),
                onSelect: function (dates) {
                    var startDate = $('#datePickerStartDate').val();
                    self.positionModle.startDate(startDate);
                }});

            self.retrieve = oj.Router.rootInstance.retrieve();
            self.isRequired = ko.observable(true);
            if (self.retrieve === 'add') {
                self.serviceType('ADD');
                self.oprationMessage(self.addMassageLbl());
                self.jnVisibility(false);
                self.trainVisibility(true);
                self.positionModle = {
                    startDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                    endDate: ko.observable(),

                    taxonomicCode: ko.observable(),
                    ranked: ko.observable(),
                    managing: ko.observable(),
                    positionNumber: ko.observable(),
                    positionName: ko.observable(),

                    code: ko.observable(),
                    action: ko.observable('Insert new record'),
                    flag: ko.observable('Y'),

                    createdBy: ko.observable(rootViewModel.personDetails().personId),
                    createdDate: ko.observable(self.formatDate(new Date())),
                    lastUpdatedBy: ko.observable(),
                    lastUpdatedDate: ko.observable(),
                    codeOfTaxonomicCode: ko.observable()
                };
            } else if (self.retrieve.type === 'view') {
                self.serviceType('VIEW');
                self.currentStepValue('stp2');

                self.jnVisibility(true);
                self.trainVisibility(false);
                self.isDisabled(true);
                self.addBtnVisible(false);
                self.nextBtnVisible(false);
                self.positionModle = {
                    startDate: ko.observable(self.formatDate(new Date(self.retrieve.startDate))),
                    endDate: ko.observable(self.formatDate(new Date(self.retrieve.endDate))),

                    taxonomicCode: ko.observable(self.retrieve.taxonomicCodeLbl),
                    ranked: ko.observable(self.retrieve.ranked),
                    managing: ko.observable(self.retrieve.managing),
                    positionNumber: ko.observable(parseInt(self.retrieve.positionNumber)),
                    positionName: ko.observable(self.retrieve.positionName),

                    code: ko.observable(self.retrieve.code),
                    action: ko.observable(self.retrieve.action),
                    flag: ko.observable(self.retrieve.flag),

                    createdBy: ko.observable(self.retrieve.createdBy),
                    createdDate: ko.observable(self.formatDate(new Date())),
                    lastUpdatedBy: ko.observable(app.userName()),
                    lastUpdatedDate: ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date())),
                    codeOfTaxonomicCode: ko.observable(self.retrieve.taxonomicCodeLbl)

                };
            }
            var getOrgnizationCBF = function (data) {
                self.orgnizationArr([]);
                for (var i = 0; i < data.length; i++) {
                    self.orgnizationArr.push({
                        "value": data[i].organizationCode, "label": data[i].name, "labelAr": data[i].name
                    });
                }
            };
            services.getGeneric(commonUtil.orgnizationUrl, {}).then(getOrgnizationCBF, self.failCbFn);
        };



        self.jobCodeArr = ko.observableArray(rootViewModel.globalJobCodes());
        self.gradeArr = ko.observableArray(app.globalHrGrades());
        self.positionNameArr = ko.observableArray(app.globalPositionsName());
        self.orgnizationArr = ko.observableArray([{
                "value": '',
                "label": '',
                "labelAr": ''
            }]);

        self.disableSubmit = ko.observable(false);
        self.endDate = ko.observable();
        self.startDate = ko.observable();

        self.taxonomicCode = ko.observable();
        self.ranked = ko.observable();
        self.positionNumber = ko.observable();
        self.positionName = ko.observable();
        self.managing = ko.observable();

        self.jobName = ko.observable();
        self.jobTitel = ko.observable();
        self.trainView = ko.observable();
        self.confirmMessage = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.pervious = ko.observable();
        self.next = ko.observable();
        self.cancel = ko.observable();
        self.submit = ko.observable();
        self.create = ko.observable();
        self.review = ko.observable();
        self.ok = ko.observable();
        self.oprationMessage = ko.observable();
        self.addMassageLbl = ko.observable();
        self.editMassageLbl = ko.observable();
        self.placeholder = ko.observable();
        self.DateValidMessage = ko.observable();
        self.pageMode = ko.observable();
        self.cancel = ko.observable();
        self.serviceType = ko.observable();
        self.groupValid = ko.observable();
        self.jnVisibility = ko.observable();
        self.trainVisibility = ko.observable();
        self.tracker = ko.observable();
        self.isDisabled = ko.observable(false);
        self.nextBtnVisible = ko.observable(true);
        self.addBtnVisible = ko.observable(false);
        self.columnArray = ko.observableArray();
        self.isRequired = ko.observable(true);
        self.stepArray = ko.observableArray([]);
        self.messages = ko.observable();
        self.codeOfTaxonomicCode = ko.observable();
        self.formatDate = function (date) {
            //var date = new Date()
            var month = '' + (date.getMonth() + 1),
                    day = '' + date.getDate(),
                    year = date.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        };

        //***********************Validation Date************************************************
        self.ValidEndDate = function () {
            if (self.positionModle.startDate() >= self.positionModle.endDate())
            {
                // document.querySelector("#ValidDate").open();
                $.notify(self.DateValidMessage(), "error");
            }
        };

        self.cancelValidDateButton = function () {
            self.previousStep();
            document.querySelector("#ValidDate").close();
        };
        //***********************End Validation Date*******************************************



        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                self.stepArray([{label: self.create(), id: 'stp1'},
                    {label: self.review(), id: 'stp2'}]);
                initTranslations();
            }
        });
        function initTranslations() {
            self.retrieve = oj.Router.rootInstance.retrieve();
            if (self.retrieve === 'add') {
                self.jobTitel(getTranslation("position.positionAdd"));
                self.cancel(getTranslation("labels.Cancel"));
            } else if (self.retrieve.type === 'view') {
                self.jobTitel(getTranslation("position.positionview"));
                self.cancel(getTranslation("labels.CancelView"));
            }
            self.endDate(getTranslation("job.endDate"));
            self.startDate(getTranslation("job.startDate"));

            self.taxonomicCode(getTranslation("position.taxonomicCode"));
            self.ranked(getTranslation("position.ranked"));
            self.positionNumber(getTranslation("position.positionNumber"));
            self.positionName(getTranslation("position.positionName"));
            self.managing(getTranslation("position.managing"));
            self.DateValidMessage(getTranslation("labels.DateValidMessage"));
            self.addMassageLbl(getTranslation("position.addMessage"));
            self.editMassageLbl(getTranslation("position.editMessage"));
            self.trainView(getTranslation("others.review"));

            self.placeholder(getTranslation("labels.placeHolder"));
            self.pervious(getTranslation("others.pervious"));
            self.next(getTranslation("others.next"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.submit(getTranslation("others.submit"));
            self.confirmMessage(getTranslation("labels.confirmMessage"));
            self.create(getTranslation("labels.create"));
            self.review(getTranslation("others.review"));
            self.ok(getTranslation("others.ok"));
            self.codeOfTaxonomicCode(getTranslation("position.codeOfTaxonomicCode"));
            self.canceheaderMessage(getTranslation("position.canceheaderMessage"));
            self.cancelbodymessage(getTranslation("position.cancelbodymessage"));
            if (self.serviceType() === 'ADD') {
                self.oprationMessage(self.addMassageLbl());
            }
            self.stepArray([{label: self.jobTitel(), id: 'stp1'},
                {label: self.trainView(), id: 'stp2'}
            ]);
        }
        initTranslations();
    }

    return positionContentViewModel;
});
