/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojselectcombobox'],
        function (oj, ko, $) {
            /**
             * The view model for the main content view template
             */
            function homeministryJobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.placeholder = ko.observable("Please Select Value");

                var deptArray = [{PersonNumber: 1001, DisplayName: 'ADFPM 1001 neverending', NationalId: 200, Department: 300, Position: 300, Location: 300},
                    {PersonNumber: 556, DisplayName: 'BB', NationalId: 200, Department: 300, Position: 300, Location: 300}];
                self.deptObservableArray = ko.observableArray(deptArray);
                self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'DepartmentId'});

                self.testData = ko.observable([
                    {value: "value", label: "label"},
                    {value: "value", label: "label"},
                    {value: "value", label: "label"},
                    {value: "value", label: "label"},
                    {value: "value", label: "label"}
                ]);
                self.searchPerson = function () {
                };

                self.reset = function () {
                    clearContent();
                };
                function clearContent() {

                }
                self.tableSelectionListener = function (event) {

                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow['rowKey']) {
//                self.selectedRowKey(currentRow['rowKey']);
//                self.selectedIndex(currentRow['rowIndex']);
//                self.editDisabled(false);
                    }
                };

                self.addJob = function () {
                    self.router.go('addJob');
                };
                self.updateJob = function () {};
                self.detailsOfJob = function () {};
                self.showHistory = function () {};

                self.columnArray([
                    {
                        "headerText": "Start Date", "field": "PersonNumber"
                    },
                    {
                        "headerText": "End Date", "field": "DisplayName"
                    },
                    {
                        "headerText": "نوع الدليل التصنيفي", "field": "NationalId"
                    },
                    {
                        "headerText": "المجموعة العامة", "field": "Department"
                    },
                    {
                        "headerText": "المجموعة النوعية", "field": "Position"
                    },
                    {
                        "headerText": "سلسلة الفئات", "field": "Location"
                    }
                ]);




            }

            return homeministryJobContentViewModel;
        });
