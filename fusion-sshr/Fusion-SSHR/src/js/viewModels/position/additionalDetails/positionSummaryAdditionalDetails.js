/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox',
    'ojs/ojdialog'], function (oj, ko, $, app, services, commonhelper) {

    function positionSummaryAdditionalDetails() {
        var self = this;
        self.columnArray = ko.observableArray();
        self.placeholder = ko.observable();
        self.dataSourceTB2 = ko.observable();
        var deptArray = [];
        self.selctedStruct = ko.observable();
        self.isRequired = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.nextPage = ko.observable();
        self.deptObservableArray = ko.observableArray(deptArray);
        var Url;
        var positionObj;
        self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'DepartmentId'});
        function clearContent() {

        }
        self.structureName = ko.observable([
            {value: "Work_Nature_Allowance", label: "Work Nature Allowance"}
        ]);
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.oprationdisabled(false);
            self.selectedRowKey(currentRow['rowKey']);
            self.selectedIndex(currentRow['rowIndex']);

        };

        self.addAdditionalDetails = function () {
            positionObj.type = "ADD";
            oj.Router.rootInstance.store(positionObj);
            oj.Router.rootInstance.go('workNatureAllowanceOprationScreen');
        };
        self.updateAdditionalDetails = function () {
            self.deptObservableArray()[self.selectedIndex()].type = "EDIT";
            oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
            oj.Router.rootInstance.go(self.nextPage());
        };
        self.backAction = function () {
            app.loading(true);
            oj.Router.rootInstance.go('searchPosition');
        };
        self.handleAttached = function () {
            app.loading(false);
            positionObj = oj.Router.rootInstance.retrieve();
        };
        //------------------Option Change Section ----------------------------
        /*
         * Tyis Function Load Date From Web Servise Dependant On Selected Structure Name 
         * And Set The Next Page To Direct So When Crate The Your Pape Set The True Value 
         * in Var self.nextPage 
         * 
         */
        self.structureChangedHandler = function (event, data) {
            var positionCode = positionObj.code;
            if (self.selctedStruct() == "Work_Nature_Allowance") {
                Url = commonhelper.worknatureallowanceUrl;
                positionCode = positionObj.code;
                self.nextPage('workNatureAllowanceOprationScreen');
            }
            var getValidGradeCBF = function (data) {
                var Data = data;
                self.deptObservableArray(Data);
            };
            services.getGeneric(Url + positionCode, {}).then(getValidGradeCBF, self.failCbFn);
            self.buildTable();
        };
        //------------------------End Of Section ----------------------------

        //---------------------------Function For Build Table-----------------------------
        self.buildTable = function () {
            if (self.selctedStruct() == "Work_Nature_Allowance") {
                self.columnArray([

                    {
                        "headerText": self.startDateLbl(), "field": "startDate"
                    },
                    {
                        "headerText": self.endDateLbl(), "field": "endDate"
                    },
                    {
                        "headerText": self.percentageLbl(), "field": "percentage"
                    }
                ]);
            }
        };
        //---------------------------End Of Function ---------------------------------
        //--------------------------History Section --------------------------------- 
        self.historyDetails = ko.observable();
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };
        self.openDialog = function () {
            var positionHistoryCBF = function (data) {
                var Data = data;
                self.dataSourceTB2(new oj.ArrayTableDataSource(Data));
            };
            var code = self.deptObservableArray()[self.selectedIndex()].code;

            services.getGeneric(commonhelper.workNatureAllowanceUrl + 'search/' + code, {}).then(positionHistoryCBF, app.failCbFn);
            document.querySelector("#modalDialog1").open();
        };


        //--------------------------End Of Section ----------------------------------

        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();
        self.startDateLbl = ko.observable();
        self.endDateLbl = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.gradeLbl = ko.observable();
        self.validGradeLbl = ko.observable();
        self.structureNameLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable("Please Select Value");
        self.percentageLbl = ko.observable();
        self.qualificationNameLbl = ko.observable();
        self.experienceYearsLbl = ko.observable();
        self.amountLbl = ko.observable();
        self.historyLbl = ko.observable();
        self.addtionalDetails = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        function initTranslations() {
            self.structureNameLbl(getTranslation("additionalDetails.structureName"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.validGradeLbl(getTranslation("others.validGrade"));
            self.startDateLbl(getTranslation("job.startDate"));
            self.endDateLbl(getTranslation("job.endDate"));
            self.gradeLbl(getTranslation("job.grade"));
            self.percentageLbl(getTranslation("position.percentage"));
            self.qualificationNameLbl(getTranslation("additionalDetails.qualificationName"));
            self.amountLbl(getTranslation("additionalDetails.amount"));
            self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
            self.historyLbl(getTranslation("others.history"));
            self.addtionalDetails(getTranslation("position.positionSummaryAdditionalDetails"));
            self.placeholder(getTranslation("labels.placeHolder"));

            self.columnArray([
                {
                    "headerText": self.gradeLbl(), "field": "grade"
                },
                {
                    "headerText": self.startDateLbl(), "field": "startDate"
                },
                {
                    "headerText": self.endDateLbl(), "field": "endDate"
                }
            ]);
        }
        initTranslations();
    }
    return positionSummaryAdditionalDetails;
});
