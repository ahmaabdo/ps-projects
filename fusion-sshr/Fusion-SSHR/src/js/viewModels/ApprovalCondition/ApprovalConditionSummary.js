/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker','ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.reportNameLbl = ko.observable();
                self.parameter1Lbl = ko.observable();
                self.parameter2Lbl = ko.observable();
                self.parameter3Lbl = ko.observable();
                self.parameter4Lbl = ko.observable();
                self.parameter5Lbl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.selectedIndex = ko.observable();
                self.backActionLbl = ko.observable();
                self.DeleteSummaryLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));
                self.isDisable = ko.observable(true);
                self.isDisableBtnDelete = ko.observable(true);
                self.dataSource;
                self.placeholder = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.EITCodeArr = ko.observableArray([]);
                self.checkResultSearch = ko.observable();
                 self.tableSelectionListener = function () {
                   self.isDisable(false);
                   self.isDisableBtnDelete(false); 


                };

                self.btnAddSummary = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('approvalConditionOperation');
                };
                self.btnEditSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "update";
                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('approvalConditionOperation');

                };
                self.btnViewSummary = function () {
                    app.loading(true);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "view";
                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('approvalConditionOperation');

                };
                 self.btnDeleteSummary=function(){
                  document.querySelector("#yesNoDialog").open();
                };
                self.commitRecordDelete = function () {
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    var id = self.summaryObservableArray()[currentRow['rowIndex']].id;
                    var deleteReportCBF = function (data) {                        
                        self.getSummary();                       
                        document.querySelector("#yesNoDialog").close();
                        self.isDisableBtnDelete(true);
                    };

                    services.getGeneric("ApprovalCondition/deleteApprovalCondition/" + id, {}).then(deleteReportCBF, app.failCbFn);
                };
                self.cancelButtonDelete = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                
                self.approvalSearch = {
                    eitCode: ko.observable()
                };
             
                self.getSummary = function () {
                      self.summaryObservableArray([]);
                    var getReportCbFn = function (data) {
                     
                        if (data.length !== 0)
                        {

                          for (var i = 0; i < data.length; i++) {
                            var obj = {};
                               obj.id= data[i].id;
                              for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].value==data[i].eitCode){  
                                  obj.eitCodeLBL  = app.globalEitNameReport()[j].label;
                                }
                             }       
                               obj.eitCode =data[i].eitCode;
                               obj.firstKeyType= data[i].firstKeyType;
                               obj.firstKeyValue= data[i].firstKeyValue;
                               obj.firstEitSegment= data[i].firstEitSegment; 
                               obj.operation= data[i].operation;
                               obj.secondKeyType= data[i].secondKeyType;
                               obj.secondKeyValue= data[i].secondKeyValue;
                               obj.secondEitSegment= data[i].secondEitSegment;
                               obj.approvalCode= data[i].approvalCode;
                               self.summaryObservableArray.push(obj);
                       };
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getAllApprovalCondition;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };

                self.backAction=function(){
                   oj.Router.rootInstance.go('setup');  
                };

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getSummary();
                    self.EITCodeArr(app.globalEitNameReport());
                 
                };

                self.rolenamelbl=ko.observable();
                self.fristKeyTypelbl=ko.observable();
                self.fristKeyValueLbl=ko.observable();
                self.fristPersonalInformationLbl=ko.observable();
                self.operationLbl=ko.observable();
                self.secondKeyTypeLbl=ko.observable();
                self.secondKeyValueLbl=ko.observable();
                self.secondPersonalInfromationlbl=ko.observable();
                self.fristEitSegmentLbl=ko.observable();
                self.secondEitSegmentbl=ko.observable();
                self.approvalCodeLbl=ko.observable();
                self.eitCodelbl=ko.observable();


                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                
                self.searchPerson = function () {

                    if (!self.approvalSearch.eitCode()) {

                        self.getSummary();
                    } else {
                        self.summaryObservableArray([]);
                        self.searchApprovalCondition();

                    }
                };
                
                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.approvalSearch.eitCode("");
                    self.summaryObservableArray([]);
                }
                self.searchApprovalCondition = function () {
                    var jsonData = ko.toJSON(self.approvalSearch);
                    var ApprovalConditionCbFn = function (data) {

                        if (data.length !== 0)
                        {

                          for (var i = 0; i < data.length; i++) {
                            var obj = {};
                               obj.id= data[i].id;
                              for (var j=0;j<app.globalEitNameReport().length;j++){                                
                              if(app.globalEitNameReport()[j].value==data[i].eitCode){  
                                  obj.eitCodeLBL  = app.globalEitNameReport()[j].label;
                                }
                             }       
                               obj.eitCode =data[i].eitCode;
                               obj.firstKeyType= data[i].firstKeyType;
                               obj.firstKeyValue= data[i].firstKeyValue;
                               obj.firstEitSegment= data[i].firstEitSegment; 
                               obj.operation= data[i].operation;
                               obj.secondKeyType= data[i].secondKeyType;
                               obj.secondKeyValue= data[i].secondKeyValue;
                               obj.secondEitSegment= data[i].secondEitSegment;
                               obj.approvalCode= data[i].approvalCode;
                               self.summaryObservableArray.push(obj);
                       };
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    services.addGeneric("ApprovalCondition/search", jsonData).then(ApprovalConditionCbFn, failCbFn);
                };
                function initTranslations() {
                    
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.editSummaryLbl(getTranslation("report.editSummary"));
                    self.viewSummaryLbl(getTranslation("report.viewSummary"));
                    self.rolenamelbl(getTranslation("report.roleName"));
                    self.fristKeyTypelbl(getTranslation("report.fristKeyType"));
                    self.fristKeyValueLbl(getTranslation("report.fristKeyValue"));
                    self.fristPersonalInformationLbl(getTranslation("report.fristPersonalInformation"));
                    self.secondEitSegmentbl(getTranslation("report.secondEitSegmentbl"));
                    self.operationLbl(getTranslation("validation.operation"));
                    self.secondKeyTypeLbl(getTranslation("validation.secondKeyType"));
                    self.secondKeyValueLbl(getTranslation("validation.secondKeyValue"));
                    self.secondPersonalInfromationlbl(getTranslation("report.secondPersonalInfromation"));
                    self.fristEitSegmentLbl(getTranslation("report.fristEitSegmentLbl"));
                    self.approvalCodeLbl(getTranslation("report.approvalCodeLbl"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));  
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));  
                    self.eitCodelbl(getTranslation("common.eitCode"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.DeleteSummaryLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("labels.message"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes")); 
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.columnArray([
                        {
                            "headerText": self.eitCodelbl(), "field": "eitCodeLBL"
                        },
                        {
                            "headerText": self.fristKeyTypelbl(), "field": "firstKeyType"
                        },
                        {
                            "headerText": self.fristKeyValueLbl(), "field": "firstKeyValue"
                        },
                        {
                            "headerText": self.fristEitSegmentLbl(), "field": "firstEitSegment"
                        },
                        {
                            "headerText": self.operationLbl(), "field": "operation"
                        },
                        {
                            "headerText": self.secondKeyTypeLbl(), "field": "secondKeyType"
                        },
                        {
                            "headerText": self.secondKeyValueLbl(), "field": "secondKeyValue"
                        },
                        {
                            "headerText": self.secondEitSegmentbl(), "field": "secondEitSegment"
                        },
                        {
                            "headerText": self.approvalCodeLbl(), "field": "approvalCode"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
