/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */


define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper','ojs/ojknockout' ,'ojs/ojarraydataprovider' ,
    'ojs/ojselectcombobox','ojs/ojtable','ojs/ojdialog' , 'ojs/ojradioset', 'ojs/ojavatar', 'ojs/ojbutton', 'ojs/ojcolor', 'ojs/ojcolorspectrum'],


function (oj, ko, $, app, services, commonhelper) {

    function iconSetupSummaryModel() {
        var self = this;
        self.columnArray = ko.observableArray();
        self.placeholder = ko.observable();
        self.dataSourceTB2 = ko.observable();
        var deptArray = [];
        self.selectedService = ko.observable();
        self.isRequired = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.nextPage = ko.observable();
        self.backActionLbl = ko.observable();        
        self.clearLbl = ko.observable();
        self.historyDetails = ko.observable();
        self.deptObservableArray = ko.observableArray(deptArray);
        var Url;
        var positionObj;
        self.checkResultSearch = ko.observable();
        self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'id'});


     

        // Call EIT Code
        self.EitName = ko.observableArray([]);
        self.callEITCode = function () {

            var EITCodeCbFn = function (data) {

                var tempObject = data;
                for (var i = 0; i < tempObject.length; i++) {

                    // push in array
                    self.EitName.push({
                        value: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_CODE,
                        label: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_NAME
                    });
                }
            };
            var failCbFn = function () {
            };

            services.getEitNameReportReport(app.lang).then(EITCodeCbFn, failCbFn);
        };


        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.oprationdisabled(false);
//            self.selectedRowKey(currentRow['rowKey']);
            if(currentRow){             
                self.selectedIndex(currentRow['rowIndex']);
            }
          
        };

        self.addAdditionalDetails = function () {
            app.loading(true);
            positionObj = {}
            positionObj.type = "ADD";
            localStorage.setItem('reterieveIconData',JSON.stringify(positionObj));
            app.reterieveIconData.push(positionObj);
            oj.Router.rootInstance.go('iconSetup');
        };
        self.updateAdditionalDetails = function () {
            if (self.deptObservableArray()[self.selectedIndex()] != null) {
                document.querySelector("#editDialog").open();
                
            } else
            {
                 $.notify(self.ValidationMessage(), "error");
              
            }
        };
        self.deleteAdditionalDetails = function () {
            if (self.deptObservableArray()[self.selectedIndex()] != null) {
                document.querySelector("#yesNoDialog").open();
            } else
            {
                $.notify(self.ValidationMessage(), "error");
            }

        };
       
       
        self.globalDelete = function () {
           
            var element = document.getElementById('table');
            var currentRow = element.currentRow;
           var removeObj= self.deptObservableArray()[currentRow['rowIndex']];     
            var id = self.deptObservableArray()[currentRow['rowIndex']].ID;
            self.deptObservableArray.remove(removeObj);
            var getdeleteCBF=function (){
                getEITIconsSetup();
            };
             
            services.addGeneric(commonhelper.iconSetupUrl + id,{}).then(getdeleteCBF, app.failCbFn);
           
        };
        function getEITIconsSetup() {
                    var getIcons = function (data) {
                        app.EITIcons([]);
                        for (var i = 0; i < data.length; i++) {
                            app.EITIcons.push({
                                "ID": data[i].id, "eitCode": data[i].EITCODE, "icons": data[i].ICONS, "name": data[i].NAME
                            });
                        }
                    };
                    services.getGenericAsync(commonhelper.getIconSetup).then(getIcons, app.failCbFn);
                }
        self.nextStep = function ()
                {

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                    {
                        self.globalSearch();
                        if (parseInt(countValid) > 0)
                        {
                            //var popup = document.querySelector('#NoDialog');
                            $.notify(self.validMessageLbl(), "error");
                            //popup.open();                             
                        } else
                        {
                            var flag = true;
                            if (self.lastNotification() === "FYI" && positionObj.type === 'ADD')
                            {
                                if (self.approvalSetupModel.notificationType() === "FYA") {
                                    $.notify(self.validNotificationTypeLbl(), "error");
                                    flag = false;
                                }
                            }
                            if (flag)
                            {

                                self.currentStepValue(next);

                                self.currentStepValueText();
                            }
                        }
                    }

                };
        self.backAction = function () {
            app.loading(true);
            oj.Router.rootInstance.go('searchPosition');
        };
        self.clearAction = function () {
            self.deptObservableArray([]);
            self.selectedService('');
            self.addBtnDisabled(false);
        };
        self.handleAttached = function () {
            app.loading(false);
            // positionObj = oj.Router.rootInstance.retrieve();
//            self.EitName(app.globalEitNameReport());
              var arr=[{value: "XXX_RECRUITMENT_REQUEST",
                                label:self.EmployeeProbationaryPeriodEvaluationLBL()}];
                    var totalArr=[];
                       totalArr = arr.concat(app.globalEitNameReport());  
                   
                    
                    self.EitName(totalArr);
        };
        //------------------Option Change Section ----------------------------
        /*
         * This Function Load Date From Web Servise Dependant On Selected Structure Name 
         * And Set The Next Page To Direct So When Crate The Your Pape Set The True Value 
         * in Var self.nextPage 
         * 
         */
        self.selfServiceChangedHandler = function (event, data) {
            var positionCode = self.selectedService();
            Url = commonhelper.iconSetupUrl;
           
            var getIcons = function (data) {
                var Data = data;

                if (data.length !== 0) {
                    self.deptObservableArray(Data);
                    self.addBtnDisabled(true);
                } else {
                   // $.notify(self.checkResultSearch(), "error");
                     self.deptObservableArray([]);
                     self.addBtnDisabled(false);
                }
            };
            var failCbFn = function () {
                $.notify(self.checkResultSearch(), "error");
            };
            services.getGeneric(Url + positionCode, {}).then(getIcons, failCbFn);
        };
        //------------------------End Of Section ----------------------------

       
        //--------------------------History Section --------------------------------- 
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };

        //--------------------------End Of Section ----------------------------------
        self.commitRecord = function (data, event) {
            self.globalDelete();
            document.querySelector("#yesNoDialog").close();
        };
        
        self.commitEditRecord = function (data, event) {
                app.loading(true);
                self.deptObservableArray()[self.selectedIndex()].type = "EDIT";
                localStorage.setItem('reterieveIconData',JSON.stringify(self.deptObservableArray()[self.selectedIndex()]));
                oj.Router.rootInstance.go("iconSetup");
                document.querySelector("#editDialog").close();
        };

        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
            document.querySelector("#editDialog").close();
            
        };

        self.cancelButtonValid = function () {
            document.querySelector("#ValidationDialog").close();
        };
        self.backAction=function(){
                   oj.Router.rootInstance.go('setup');  
                };
                

                
        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();
        self.startDateLbl = ko.observable();
        self.endDateLbl = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.deleteLbl = ko.observable();
        self.gradeLbl = ko.observable();
        self.validGradeLbl = ko.observable();
        self.eitNameLbl = ko.observable();
        self.selfServiceLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable();
        self.percentageLbl = ko.observable();
        self.qualificationNameLbl = ko.observable();
        self.experienceYearsLbl = ko.observable();
        self.amountLbl = ko.observable();
        self.historyLbl = ko.observable();
        self.addtionalDetails = ko.observable();
        self.roleNameLbl = ko.observable();
        self.approvalSetupLbl = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.oprationEditMessage = ko.observable();
        self.ValidationMessage = ko.observable();
        self.EmployeeProbationaryPeriodEvaluationLBL=ko.observable();
        self.addBtnDisabled = ko.observable(false);
        self.editBtnDisabled  = ko.observable(false);
        self.delBtnDisabled  = ko.observable(false);
        self.selfSerivce = ko.observable();
        self.selfServiceConfMessageArLbl = ko.observable();
        self.selfServiceConfMessageEnLbl = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.iconSetupLbl = ko.observable();
        self.name = ko.observable();
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        function initTranslations() {
            self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
            self.selfServiceLbl(getTranslation("manageOnlineHelp.selfServiceLbl"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.deleteLbl(getTranslation("others.delete"));
            self.validGradeLbl(getTranslation("others.validGrade"));
            self.startDateLbl(getTranslation("job.startDate"));
            self.endDateLbl(getTranslation("job.endDate"));
            self.gradeLbl(getTranslation("job.grade"));
            self.percentageLbl(getTranslation("position.percentage"));
            self.qualificationNameLbl(getTranslation("additionalDetails.qualificationName"));
            self.amountLbl(getTranslation("additionalDetails.amount"));
            self.experienceYearsLbl(getTranslation("additionalDetails.experienceYears"));
            self.historyLbl(getTranslation("others.history"));
            self.addtionalDetails(getTranslation("position.positionSummaryAdditionalDetails"));
            self.roleNameLbl(getTranslation("approvalScreen.roleName"));
            self.approvalSetupLbl(getTranslation("position.approvalSetup"));
            self.confirmMessage(getTranslation("iconSetup.confirmMessage"));
            self.oprationEditMessage(getTranslation("iconSetup.oprationEditMessage"));
            self.oprationMessage(getTranslation("iconSetup.deleteMessage"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.ValidationMessage(getTranslation("labels.ValidationMessage"));
            self.checkResultSearch(getTranslation("common.checkResultSearch"));
            self.backActionLbl(getTranslation("approvalScreen.backAction"));
            self.EmployeeProbationaryPeriodEvaluationLBL(getTranslation("approvalScreen.EmployeeProbationaryPeriodEvaluation"));
            self.selfSerivce(getTranslation("manageOnlineHelp.selfSerivce"));
            self.selfServiceConfMessageArLbl(getTranslation("manageOnlineHelp.selfServiceConfMessageArLbl"));
            self.selfServiceConfMessageEnLbl(getTranslation("manageOnlineHelp.selfServiceConfMessageEnLbl"));
            self.iconSetupLbl(getTranslation("iconSetup.iconSetupLbl"));
            self.name(getTranslation("iconSetup.name"));
            self.clearLbl(getTranslation("iconSetup.clearLbl"));
            self.columnArray([
                {
                    "headerText": self.selfSerivce(), "field": "EITCODE"
                },
                {
                    "headerText": self.name(), "field": "NAME"
                }
            ]);
            

        }
        initTranslations();
    }
    return iconSetupSummaryModel;
});