/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'ojs/ojtable',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.parameter1Lbl = ko.observable();
                self.parameter2Lbl = ko.observable();
                self.parameter3Lbl = ko.observable();
                self.parameter4Lbl = ko.observable();
                self.parameter5Lbl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.backActionLbl = ko.observable();
                self.DeleteSummaryLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource = ko.observable();
                self.filter = ko.observable();
                self.isDisable = ko.observable(true);
                self.checkResultSearch = ko.observable();
                self.searchPlaceHolder = ko.observable();
                self.reportName = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.filterlbl = ko.observable();
                self.reportnameVal = ko.observable();



                self.btnAddSummary = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('reportSummary');
                };


                self.btnEditSummary = function () {
                    app.loading(true);
                    var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].id[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var payload = {
                        clickType: 'Edit',
                        data: self.summaryObservableArray()[myRowIndex]
                    };
                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('reportSummary');
                };


                self.btnViewSummary = function () {
                    app.loading(true);
                    var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].id[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var payload = {
                        clickType: 'view',
                        data: self.summaryObservableArray()[myRowIndex]
                    };
                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('reportSummary');
                };
                var selectedRowKey;
                self.tableSelectionListener = function (event) {
                    selectedRowKey = event.detail.currentRow.rowKey[0];
                    self.isDisable(false);
                };


                self.getSummary = function () {

                    var getReportCbFn = function (data) {
                        if (data.length !== 0)
                        {
                            $.each(data, function (index) {
                                self.summaryObservableArray.push({
                                    id: [data[index].id],
                                    report_Name: [data[index].report_Name],
                                    parameter1: [data[index].parameter1],
                                    parameter2: [data[index].parameter2],
                                    parameter3: [data[index].parameter3],
                                    parameter4: [data[index].parameter4],
                                    parameter5: [data[index].parameter5],
                                    reportTypeVal: [data[index].reportTypeVal]

                                });
                            });
                            self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }

                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getAllSummary;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };


                self.reportSearch = {
                };
                self.reset = function () {
                    self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));
                    clearContent();
                };
                function clearContent() {
                    self.reportnameVal("");
                    self.isDisable(true);
                    
                }

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getSummary();
                };
                
                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };
                
                self.btnDeleteSummary = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                
                self.commitRecordDelete = function () {
                    var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].id[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var id = self.summaryObservableArray()[myRowIndex].id;
                    var deleteReportCBF = function (data) {
                        self.summaryObservableArray([]);
                        self.getSummary();
                        document.querySelector("#yesNoDialog").close();
                    };
                    services.getGeneric("summary/deleteReport/" + id, {}).then(deleteReportCBF, app.failCbFn);
                };
                self.cancelButtonDelete = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
         
                //------------change value of search--------------------
                self.handleValueChanged = function () {
                    var deptArray = [];
                    self.tempSummaryObservableArray = ko.observableArray([]);
                    var filter = document.getElementById('filter').rawValue;
                    if (typeof filter !== 'undefined' && filter !== null) {
                        for (var i = self.summaryObservableArray().length - 1; i >= 0; i--) {
                            Object.keys(self.summaryObservableArray()[i]).forEach(function (field) {
                                if (typeof self.summaryObservableArray()[i].report_Name[0] !== 'undefined') {
                                    if (self.summaryObservableArray()[i].report_Name[0].toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0 || self.summaryObservableArray()[i].parameter1.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0 || self.summaryObservableArray()[i].parameter2.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0) {
                                        if (deptArray.indexOf(self.summaryObservableArray()[i]) < 0) {
                                            deptArray.push(self.summaryObservableArray()[i]);
                                            self.tempSummaryObservableArray = deptArray;
                                            self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tempSummaryObservableArray, {idAttribute: 'id'})));
                                        }
                                    }
                                }
                            });
                        }
                    }
                };


                //----------------end criteria seach section------------------


                function initTranslations() {
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.editSummaryLbl(getTranslation("report.editSummary"));
                    self.viewSummaryLbl(getTranslation("report.viewSummary"));
                    self.filterlbl(getTranslation("report.filterlbl"));
                    self.parameter1Lbl(getTranslation("report.Parameter1"));
                    self.parameter2Lbl(getTranslation("report.Parameter2"));
                    self.parameter3Lbl(getTranslation("report.Parameter3"));
                    self.parameter4Lbl(getTranslation("report.Parameter4"));
                    self.parameter5Lbl(getTranslation("report.Parameter5"));
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.DeleteSummaryLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.searchPlaceHolder(getTranslation("report.searchPlaceHolder"));
                    self.oprationMessage(getTranslation("labels.message"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.reportName(getTranslation("report.reportName"));
                    self.columnArray([
                        {
                            "headerText": self.reportName(), "field": "report_Name"
                        },
                        {
                            "headerText": self.parameter1Lbl(), "field": "parameter1"
                        },
                        {
                            "headerText": self.parameter2Lbl(), "field": "parameter2"
                        },
                        {
                            "headerText": self.parameter3Lbl(), "field": "parameter3"
                        },
                        {
                            "headerText": self.parameter4Lbl(), "field": "parameter4"
                        },
                        {
                            "headerText": self.parameter5Lbl(), "field": "parameter5"
                        },
                        {
                            "headerText": self.reportTypelbl(), "field": "reportTypeVal"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
