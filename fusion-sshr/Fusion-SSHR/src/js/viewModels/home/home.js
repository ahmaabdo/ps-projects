define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojbutton', 'ojs/ojinputtext',
    'ojs/ojlabel', 'ojs/ojavatar'],
    function (oj, ko, $, app) {

        function homeViewModel() {
            var self = this;
            self.eitIconsLoaded = ko.observable(false);
            self.loading = function () {
                app.loading(true);
            };

            self.error = function () {
                app.hasErrorOccured(false);
                app.router.go("error");
            };

            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });

            self.computeIconsArray = ko.computed(function () {
                if (typeof $(".profilepic")[0] !== 'undefined' && typeof $(".profilepic")[1] !== 'undefined') {
                    if (rootViewModel.personDetails().picBase64 && rootViewModel.personDetails().picBase64 !== null) {
                        $(".profilepic")[0].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                        $(".profilepic")[1].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                    } else {
                        $(".profilepic")[0].src = "css/images/avatar_24px.png";
                        $(".profilepic")[1].src = "css/images/avatar_24px.png";
                    }
                }
                if (app.isGettingMaster())
                    self.eitIconsLoaded(false);


                if (app.iconsArr().length > 0 && !self.eitIconsLoaded() && !app.isGettingMaster()) {
                    self.eitIconsLoaded(true);
                }
            });

            self.pagingModel = null;

            self.iconNavigation = function (router, code) {
                app.loading(true);
                oj.Router.rootInstance.store(code);
                oj.Router.rootInstance.go(router);
                return true;
            };

            getPagingModel = function () {
                if (!self.pagingModel) {
                    var filmStrip = $("#filmStrip");
                    var pagingModel = filmStrip.ojFilmStrip("getPagingModel");
                    self.pagingModel = pagingModel;
                }
                return self.pagingModel;
            };

            //--------------------Start Validation------------------//

            self.handleAttached = function (info) {
                app.loading(false);
                app.backBtnVis(false);
                initTranslations();
//               app.getElementFile();
//                 app.pushAbsence();
            };

            function initTranslations() {
            }

            return true;
        }

        return new homeViewModel();
    });
