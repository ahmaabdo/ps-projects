/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Probation Review module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojdialog', 'ojs/ojvalidationgroup',
    'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojradioset', 'ojs/ojselectcombobox'],
        function (oj, ko, $, app, services, commonhelper, ArrayDataProvider) {

            function probationreviewViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.rejectReason = ko.observable();
                self.labels = ko.observable();
                self.radioOptions = ko.observableArray([
                    {id: 'one', value: '1', name: '1'},
                    {id: 'two', value: '2', name: '2'},
                    {id: 'three', value: '3', name: '3'},
                    {id: 'four', value: '4', name: '4'},
                    {id: 'five', value: '5', name: '5'}
                ]);
                self.submitSelectionList = ko.observableArray([]);
                // first group radio buttons
                self.sec1Radio1Selection = ko.observable();
                self.sec1Radio2Selection = ko.observable();
                self.sec1Radio3Selection = ko.observable();
                self.sec1Radio4Selection = ko.observable();
                self.sec1Radio5Selection = ko.observable();
                // second group radio buttons
                self.sec2Radio1Selection = ko.observable();
                self.sec2Radio2Selection = ko.observable();
                self.sec2Radio3Selection = ko.observable();
                self.sec2Radio4Selection = ko.observable();
                self.sec2Radio5Selection = ko.observable();
                // over all radio button
                self.overAllRateRadio1Selection = ko.observable();
                // action list selection
                self.selectedActionType = ko.observable();
                self.downloadAttachmentVisible = ko.observable(false);
                self.pageMode = ko.observable();
                self.workFlowApprovalId = ko.observable();
                self.disabled = ko.observable(true);
                self.submetBtnVisable = ko.observable(true);
                self.disableSubmit = ko.observable(false);
                self.rejectBtnVisable = ko.observable(false);
                self.rejectReasonER = ko.observable();
                self.rejectCommentVisable = ko.observable(false);
                self.disabledRejectText = ko.observable(false);


                //--------------- define messages notification ---------------//
                self.messages = ko.observableArray();
                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };
                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };
                //------------------------------------------------------------//
                //----------------- for language change ----------------------//
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                //------------------------------------------//
                self.handleAttached = function () {
                    self.rejectReason(" ");
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    self.pageMode(self.retrieve.typeScreen);
                    // set employee data
                    if (self.pageMode() == 'ADD') {
                        app.loading(false);
                        self.probationDataModel.employeeName('employee name');
                        self.probationDataModel.employeeId('employee id');
                        self.probationDataModel.employeeEmail('employee mail');
                        self.probationDataModel.employeeDepartment('employee department');
                        self.probationDataModel.employeePosition('employee position');
                        self.probationDataModel.employeeNationality('employee nationality');
                        self.probationDataModel.manager('manager');
                        self.probationDataModel.hireDate('hire date');
                        self.probationDataModel.evaluationDate('evaluation date');
                    } else if (self.pageMode() == 'EDIT') {
                        self.labels().submitLbl(getTranslation('others.submit'));
                        self.disabled = ko.observable(false);
                        self.workFlowApprovalId(self.retrieve.id);
                        self.getProbationPeriodByID();
                    } else if (self.pageMode() == 'REVIWE') {
                        if(self.retrieve.receiver_type == "EMP"){
                        self.labels().submitLbl(getTranslation('labels.acknowledgeBtn'));
                        self.labels().rejectLbl(getTranslation('probationReview.moreInfoRequired'));
                    }
                    else{
                        self.labels().submitLbl(getTranslation('others.approve'));
                        self.labels().rejectLbl(getTranslation('others.reject'));
                    }
                        self.workFlowApprovalId(self.retrieve.id);
                        self.rejectBtnVisable(true);
                        self.getProbationPeriodByID();
                        self.rejectCommentVisable(true);
                        self.disabled = ko.observable(true);
                        if (self.retrieve.TYPE == "FYI") {
                            self.submetBtnVisable(false);
                        }
                    }
                };
                //-------------------------------------------------------//
                //-------------------------- pageModel --------------------------//
                self.probationDataModel = ko.observable();
                self.probationDataModel = {
                    employeeName: ko.observable(),
                    employeeId: ko.observable(),
                    employeeEmail: ko.observable(),
                    employeeDepartment: ko.observable(),
                    employeePosition: ko.observable(),
                    employeeNationality: ko.observable(),
                    manager: ko.observable(),
                    hireDate: ko.observable(),
                    evaluationDate: ko.observable(),
                    firstGroupRadio: ko.observable(),
                    secondGroupRadio: ko.observable(),
                    overAllRate: ko.observable(),
                    actionTypeSelected: ko.observable(),
                    status: ko.observable(),
                    eitCode: ko.observable(),
                    eitName: ko.observable(),
                    createdBy: ko.observable(),
                    requestId: ko.observable(),
                    id: ko.observable(),
                    personNumber: ko.observable(),
                    lineManager: ko.observable(),
                    managerOfManager: ko.observable()
                };

                //-------------------------------------------------------------//
                self.getProbationPeriodByID = function () {
                    var success = function (data) {
                        app.loading(false);
                        if (self.pageMode() == "EDIT") {
                            // fill model with data
                            self.probationDataModel.employeeName(data.personName);
                            self.probationDataModel.personNumber(data.person_number);
                            self.probationDataModel.employeeId(data.person_id);
                            self.probationDataModel.employeeEmail(data.PEI_INFORMATION1);
                            self.probationDataModel.employeeDepartment(data.PEI_INFORMATION2);
                            self.probationDataModel.employeePosition(data.PEI_INFORMATION3);
                            self.probationDataModel.employeeNationality(data.PEI_INFORMATION4);
                            self.probationDataModel.manager(data.PEI_INFORMATION5);
                            self.probationDataModel.hireDate(data.PEI_INFORMATION6);
                            self.probationDataModel.evaluationDate(data.PEI_INFORMATION7);
                            self.probationDataModel.lineManager(data.line_manager);
                            self.probationDataModel.managerOfManager(data.manage_of_manager);
                            self.probationDataModel.createdBy(data.created_by);
                            if(data.PEI_INFORMATION9){
                                var group1 = JSON.parse(data.PEI_INFORMATION8);
                            self.sec1Radio1Selection(group1.radio1);
                            self.sec1Radio2Selection(group1.radio2);
                            self.sec1Radio3Selection(group1.radio3);
                            self.sec1Radio4Selection(group1.radio4);
                            self.sec1Radio5Selection(group1.radio5);
                            // get second groub radio
                            var group2 = JSON.parse(data.PEI_INFORMATION9);
                            self.sec2Radio1Selection(group2.radio1);
                            self.sec2Radio2Selection(group2.radio2);
                            self.sec2Radio3Selection(group2.radio3);
                            self.sec2Radio4Selection(group2.radio4);
                            self.sec2Radio5Selection(group2.radio5);

                            self.overAllRateRadio1Selection(data.PEI_INFORMATION10);
                            self.selectedActionType(data.PEI_INFORMATION11);
                            if(data.rejectReason){
                                self.disabledRejectText(true);
                                self.rejectCommentVisable(true);
                            self.rejectReasonER(data.rejectReason);
                        }
                            }
                        } else {
                            self.probationDataModel.employeeName(data.personName);
                            self.probationDataModel.personNumber(data.person_number);
                            self.probationDataModel.employeeId(data.person_id);
                            self.probationDataModel.employeeEmail(data.PEI_INFORMATION1);
                            self.probationDataModel.employeeDepartment(data.PEI_INFORMATION2);
                            self.probationDataModel.employeePosition(data.PEI_INFORMATION3);
                            self.probationDataModel.employeeNationality(data.PEI_INFORMATION4);
                            self.probationDataModel.manager(data.PEI_INFORMATION5);
                            self.probationDataModel.hireDate(data.PEI_INFORMATION6);
                            self.probationDataModel.evaluationDate(data.PEI_INFORMATION7);
                            self.probationDataModel.lineManager(data.line_manager);
                            self.probationDataModel.managerOfManager(data.manage_of_manager);
                            self.probationDataModel.createdBy(data.created_by);
                            // get first groub radio
                            var group1 = JSON.parse(data.PEI_INFORMATION8);
                            self.sec1Radio1Selection(group1.radio1);
                            self.sec1Radio2Selection(group1.radio2);
                            self.sec1Radio3Selection(group1.radio3);
                            self.sec1Radio4Selection(group1.radio4);
                            self.sec1Radio5Selection(group1.radio5);
                            // get second groub radio
                            var group2 = JSON.parse(data.PEI_INFORMATION9);
                            self.sec2Radio1Selection(group2.radio1);
                            self.sec2Radio2Selection(group2.radio2);
                            self.sec2Radio3Selection(group2.radio3);
                            self.sec2Radio4Selection(group2.radio4);
                            self.sec2Radio5Selection(group2.radio5);

                            self.overAllRateRadio1Selection(data.PEI_INFORMATION10);
                            self.selectedActionType(data.PEI_INFORMATION11);
//             if (self.selectedActionType() == 'extend' ) {
//              self.downloadAttachmentVisible(true);
//            } else {
//              self.downloadAttachmentVisible(false);
//            }
                        }
                    };
                    var fail = function (data) {
                        app.loading(false);
                    }
                    services.getGeneric('probationReview/getById/' + self.retrieve.TRS_ID).then(success, fail);
                };
                //--------------------- button actions --------------------//
                self.backAction = function () {
                    oj.Router.rootInstance.go('notificationScreen');
                };

                self.downloadAttachmentAction = function () {
                    app.loading(true);

                    var payload = {"reportName": "XXX_HANDOVER_EXTEND_FORM_REPORT", "personId": self.probationDataModel.employeeId()};

                    var success = function (data) {
                        app.loading(false);
                        // download attachment
                        downloadPDF(data.REPORT);
                    };

                    var fail = function (data) {
                    };

                    services.getGenericReport(payload).then(success, fail);
                };

                // change handler for selected action 
                self.valueChangedHandler = function (event) {
                    self.selectedActionType(event.detail.value);
                    if (self.selectedActionType() == 'Extend') {
                        self.downloadAttachmentVisible(true);
                    } else {
                        self.downloadAttachmentVisible(false);
                    }
                };

                self.submitActionBtn = function () {
                    // set data to be send
                    self.probationDataModel.firstGroupRadio({
                        radio1: self.sec1Radio1Selection(),
                        radio2: self.sec1Radio2Selection(),
                        radio3: self.sec1Radio3Selection(),
                        radio4: self.sec1Radio4Selection(),
                        radio5: self.sec1Radio5Selection()
                    });
                    self.probationDataModel.secondGroupRadio({
                        radio1: self.sec2Radio1Selection(),
                        radio2: self.sec2Radio2Selection(),
                        radio3: self.sec2Radio3Selection(),
                        radio4: self.sec2Radio4Selection(),
                        radio5: self.sec2Radio5Selection()
                    });
                    self.probationDataModel.overAllRate(self.overAllRateRadio1Selection());
                    self.probationDataModel.actionTypeSelected(self.selectedActionType());
                    self.probationDataModel.status('PENDING_APPROVED');
                    self.probationDataModel.eitCode('XXX_PROBATION_PERIOD');
                    self.probationDataModel.eitName('Probation Period');
                    self.probationDataModel.createdBy(rootViewModel.personDetails().personId);

                    // check for all rates
                    if ((self.sec1Radio1Selection() == '' || self.sec1Radio1Selection() == null) || (self.sec1Radio2Selection() == '' || self.sec1Radio2Selection() == null)
                            || (self.sec1Radio3Selection() == '' || self.sec1Radio3Selection() == null) || (self.sec1Radio4Selection() == '' || self.sec1Radio4Selection() == null)
                            || (self.sec1Radio5Selection() == '' || self.sec1Radio5Selection() == null) || (self.sec2Radio1Selection() == '' || self.sec2Radio1Selection() == null)
                            || (self.sec2Radio2Selection() == '' || self.sec2Radio2Selection() == null) || (self.sec2Radio3Selection() == '' || self.sec2Radio3Selection() == null)
                            || (self.sec2Radio4Selection() == '' || self.sec2Radio4Selection() == null) || (self.sec2Radio5Selection() == '' || self.sec2Radio5Selection() == null)) {
                        self.messages.push({
                            severity: 'error', summary: self.labels().groupsRadioErrorMsg()
                        });

                    } else if (self.overAllRateRadio1Selection() == '' || self.overAllRateRadio1Selection() == null) {
                        self.messages.push({
                            severity: 'error', summary: self.labels().overallRateRadioErrorMsg()
                        });
                    } else {
                        document.getElementById('submitDialog').open();
                    }
                };
                //-------------------- submit actions ---------------------//
                self.submitYes = function () {
                    if (self.pageMode() == "EDIT") {
                        app.loading(true);
                        document.getElementById('submitDialog').close();
                        self.probationDataModel.id(self.retrieve.TRS_ID);
                        var payload = ko.toJSON(self.probationDataModel);
                        var success = function (data) {
                            self.updateProbationRequest();
                            //app.loading(false);
                        };
                        var fail = function (data) {
                            app.loading(false);
                        };
                        services.addGeneric("probationReview/createOrUpdatePR/" + self.pageMode() + "/condetionCode", payload).then(success, fail);
                    } else
                    {
                        app.loading(true);
                        self.updateProbationRequest();
                    }
                };

                self.updateProbationRequest = function () {
                    //self.disableSubmit(true);
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": app.personDetails().personId,
                        "RESPONSE_CODE": "APPROVED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "PERSON_NAME": self.retrieve.personName,
                        "rejectReason": self.rejectReason(),
                        "requestTypeLbl": self.retrieve.msg_Title_Lbl,
                        "PERSON_EMP_FYI_ID": self.retrieve.person_id,
                        "workflowId": self.workFlowApprovalId()

                    };

                    var getUpdateWorkFlowNotificationCBF = function (data)
                    {
                        var updateStatusCBFN = function (data) {

                            oj.Router.rootInstance.go("notificationScreen");
                        };

                        //services.getGenericAsync("PeopleExtra/updateStatus?id=" + self.retrieve.TRS_ID, {}).then(updateStatusCBFN, app.failCbFn);
                        oj.Router.rootInstance.go("notificationScreen");

                    };


                    services.workflowAction("workflowApproval/", headers).then(app.failCbFn, getUpdateWorkFlowNotificationCBF);

                };

                self.submitNo = function () {
                    document.getElementById('submitDialog').close();
                };
                self.rejectActionBtn = function () {
                    if(self.retrieve.receiver_type == "ROLES" || self.retrieve.receiver_type == "LINE_MANAGER+1")
                    {
                    if(!isEmpty(self.rejectReasonER())){
                        document.getElementById('rejectDialog').open();
                    }
                    else
                    {
                        self.messages([{
                            severity: 'error',
                            summary: "Please fill the reject message text",
                            autoTimeout: 5000
                        }]);
                    }
                }
                else
                    {
                      document.getElementById('rejectDialog').open();  
                    }
                };
                self.rejectCancelButton = function () {
                    document.querySelector("#rejectDialog").close();
                };
                self.rejectRewaardRequst = function (data, event) {
                    self.disableSubmit(true);
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "RESPONSE_CODE": "REJECTED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "rejectReason": self.rejectReasonER(),
                        "workflowId": self.workFlowApprovalId(),
                        "PERSON_NAME": self.retrieve.personName
                    };

                    var approvalActionCBFN = function (data) {
                        oj.Router.rootInstance.go('notificationScreen');
                    };
                    services.workflowAction("workflowApproval/", headers).then(app.failCbFn, approvalActionCBFN)

                };
                
                function isEmpty(val) {
            return (val === undefined || val == null || val.length <= 0) ? true : false;
        }

                //-------------------------------------------------------------//

                //------------------- download pdf function -----------------//
                function downloadPDF(pdf) {
                    var link = document.createElement("a");
                    var data = 'data:application/msword;base64,' + pdf;
                    link.setAttribute('href', data);
                    link.setAttribute('download', "Handover extend form.docx");
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                //-----------------------------------------------//
                function initTranslations() {

                    self.labels({
                        yes: ko.observable(getTranslation('others.yes')),
                        no: ko.observable(getTranslation('others.no')),
                        back: ko.observable(getTranslation('others.back')),
                        submitLbl: ko.observable(getTranslation('')),
                        submitTitle: ko.observable(getTranslation('probationReview.submitDialogTitle')),
                        submitMessage: ko.observable(getTranslation('probationReview.submitConfirmMessage')),
                        probationInformationLbl: ko.observable(getTranslation('probationReview.probationInformation')),
                        employeeNameLbl: ko.observable(getTranslation('probationReview.employeeName')),
                        employeeIdLbl: ko.observable(getTranslation('probationReview.employeeId')),
                        employeeEmailLbl: ko.observable(getTranslation('probationReview.employeeEmail')),
                        employeeDepartmentLbl: ko.observable(getTranslation('probationReview.employeeDepartment')),
                        employeePositionLbl: ko.observable(getTranslation('probationReview.employeePosition')),
                        employeeNationalityLbl: ko.observable(getTranslation('probationReview.employeeNationality')),
                        managerLbl: ko.observable(getTranslation('probationReview.manager')),
                        hireDateLbl: ko.observable(getTranslation('probationReview.hireDate')),
                        evaluationDateLbl: ko.observable(getTranslation('probationReview.evaluationDate')),
                        objectiveOverAllLbl: ko.observable(getTranslation('probationReview.sec1Header')),
                        // section 1
                        sec1Header: ko.observable(getTranslation('probationReview.sec1Details')),
                        sec1RadioHeader: ko.observable(getTranslation('probationReview.sec1RadioHeader')),
                        sec1Radio1Lbl: ko.observable(getTranslation('probationReview.sec1Radio1')),
                        sec1Radio2Lbl: ko.observable(getTranslation('probationReview.sec1Radio2')),
                        sec1Radio3Lbl: ko.observable(getTranslation('probationReview.sec1Radio3')),
                        sec1Radio4Lbl: ko.observable(getTranslation('probationReview.sec1Radio4')),
                        sec1Radio5Lbl: ko.observable(getTranslation('probationReview.sec1Radio5')),
                        //section 2
                        sec2HeaderLbl: ko.observable(getTranslation('probationReview.sec2Header')),
                        sec2Radio1Lbl: ko.observable(getTranslation('probationReview.sec2Radio1')),
                        sec2radio2Lbl: ko.observable(getTranslation('probationReview.sec2Radio2')),
                        sec2Radio3Lbl: ko.observable(getTranslation('probationReview.sec2Radio3')),
                        sec2Radio4Lbl: ko.observable(getTranslation('probationReview.sec2Radio4')),
                        sec2Radio5Lbl: ko.observable(getTranslation('probationReview.sec2Radio5')),
                        //section 3
                        overAllRateLbl: ko.observable(getTranslation('probationReview.overAllRate')),
                        rateCriteriaLbl: ko.observable(getTranslation('probationReview.ratingCriteria')),
                        sec3Header1Lbl: ko.observable(getTranslation('probationReview.rateC1Header')),
                        sec3detail1Lbl: ko.observable(getTranslation('probationReview.rateC1Detail')),
                        sec3Header2Lbl: ko.observable(getTranslation('probationReview.rateC2Header')),
                        sec3detail2Lbl: ko.observable(getTranslation('probationReview.rateC2Detail')),
                        sec3Header3Lbl: ko.observable(getTranslation('probationReview.rateC3Header')),
                        sec3detail3Lbl: ko.observable(getTranslation('probationReview.rateC3Detail')),
                        sec3Header4Lbl: ko.observable(getTranslation('probationReview.rateC4Header')),
                        sec3detail4Lbl: ko.observable(getTranslation('probationReview.rateC4Detail')),
                        sec3Header5Lbl: ko.observable(getTranslation('probationReview.rateC5Header')),
                        sec3detail5Lbl: ko.observable(getTranslation('probationReview.rateC4Detail')),
                        //submit choice text
                        submitChoiceLbl: ko.observable(getTranslation('probationReview.submitChoice')),
                        pageTitle: ko.observable(getTranslation('probationReview.pageTitle')),
                        rejectLbl: ko.observable(getTranslation('')),
                        rejectMessage: ko.observable(getTranslation('probationReview.moreInfoRequiredMessage')),
                        submitTitleReject: ko.observable(getTranslation('probationReview.submitTitleReject')),
                        groupsRadioErrorMsg: ko.observable(getTranslation('probationReview.groupRadioError')),
                        overallRateRadioErrorMsg: ko.observable(getTranslation('probationReview.overallRateError')),
                        downloadAttachment: ko.observable(getTranslation('probationReview.downloadAttachment'))
                    });
                    self.submitSelectionList([
                        {value: 'Appropriate', label: getTranslation('probationReview.Appropriate')},
                        {value: 'Unappropriated', label: getTranslation('probationReview.Unappropriated')},
                        {value: 'Extend', label: getTranslation('probationReview.Extend')}
                    ]);
                    self.selectedActionType('retain');

                }
                ;
                initTranslations();
            }
            return probationreviewViewModel;
        });