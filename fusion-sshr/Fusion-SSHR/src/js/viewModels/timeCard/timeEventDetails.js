define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol',
    'ojs/ojpagingtabledatasource', 'ojs/ojselectcombobox', 'ojs/ojlabel', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojtable', 'ojs/ojbutton'],
        function (oj, ko, $, app, services, commonhelper) {

            function TimeEventDetailsViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var tempUrl = "https://ehxm-test.fa.us6.oraclecloud.com:443/hcmRestApi/resources/11.13.17.11/timeEventRequests/300000003404212";

                self.timeCardTitle = ko.observable();

                var getAbsencesCbFn = function (data) {
                    if (data.length !== 0) {
                        //self.deptObservableArray(data.items);
                    } else {
                        //self.deptObservableArray([]);
                        //$.notify(self.checkResultSearch(), "error");
                    }
                };
                var failCbFn = function () {
                    //$.notify(self.checkResultSearch(), "error");
                };

                services.getAbsences(tempUrl, {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);

                self.searchByDate = function () {
//                    if (typeof self.startDateVal() == 'undefined') {
//                        services.getAbsences(tempUrl + ";endDate='" + self.endDateVal() + "'", {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);
//                    } else if (typeof self.endDateVal() == 'undefined') {
//                        services.getAbsences(tempUrl + ";startDate='" + self.startDateVal() + "'", {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);
//                    } else if (typeof self.startDateVal() == 'undefined' && typeof self.endDateVal() == 'undefined') {
//                        services.getAbsences(tempUrl, {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);
//                    } else {
//                        services.getAbsences(tempUrl + ";startDate='" + self.startDateVal() + "';endDate='" + self.endDateVal() + "'", {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);
//                    }
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.handleAttached = function (info) {
                    app.loading(false);
                    initTranslations();
                };

                function initTranslations() {
                    self.timeCardTitle(getTranslation("pages.timeCard"));

                }

            }

            return new TimeEventDetailsViewModel();
        }
);
