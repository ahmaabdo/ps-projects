define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojlabel', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojarraydataprovider'],
    function (oj, ko, $, app, commonUtil, services) {

        function timeCardSummaryViewModel() {
            var self = this;
            self.pageTitle = ko.observable();
            self.search = ko.observable();
            self.process = ko.observable();
            self.loading = ko.observable();
            self.searching = ko.observable();
            self.processing = ko.observable();

            self.clear = ko.observable();
            self.deleteLbl = ko.observable('');
            self.startDateLbl = ko.observable('');
            self.startDateVal = ko.observable();
            self.endDateLbl = ko.observable('');
            self.endDateVal = ko.observable();
            self.legalEntityLbl = ko.observable('');
            self.legalEntityOps = ko.observableArray([]);
            self.legalEntityVal = ko.observable();
            self.locationLbl = ko.observable('');
            self.locationOps = ko.observableArray([]);
            self.locationVal = ko.observable();
            self.departmentLbl = ko.observable('');
            self.departmentOps = ko.observableArray([]);
            self.departmentVal = ko.observable();
            self.positionLbl = ko.observable('');
            self.positionOps = ko.observableArray([]);
            self.positionVal = ko.observable();
            self.jobLbl = ko.observable('Job');
            self.jobOps = ko.observableArray([]);
            self.jobVal = ko.observable();
            self.addressLineLbl = ko.observable('');
            self.addressLineOps = ko.observableArray([]);
            self.addressLineVal = ko.observable();
            self.loading = ko.observable(false);
            self.response = ko.observableArray([]);
            var payload = {}
            // app.actionStatus = ko.observable("0");//0->normal status, 1->searching, 2->deleting, 3->getting LOV

            self.dateConverter = oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({ pattern: 'dd-MM-yyyy' });


            self.searchAction = function () {

                if (!self.startDateVal() || self.startDateVal().length == 0) {
                    app.messages.push({ severity: "error", summary: app.translate("timeCard.required").replace("#input#", app.translate("timeCard.startDate")), autoTimeout: 0 });
                    return;
                }

                if (!self.endDateVal() || self.endDateVal().length == 0) {
                    app.messages.push({ severity: "error", summary: app.translate("timeCard.required").replace("#input#", app.translate("timeCard.endDate")), autoTimeout: 0 });
                    return;
                }
                

                if (self.startDateVal())
                    payload["P_FROM_DATE"] = self.startDateVal();
                if (self.endDateVal())
                    payload["P_TO_DATE"] = self.endDateVal();
                if (self.legalEntityVal())
                    payload["P_BU_ID"] = self.legalEntityVal();
                if (self.locationVal())
                    payload["P_LOCATION_ID"] = self.locationVal();
                if (self.positionVal())
                    payload["P_POSITION_ID"] = self.positionVal();
                if (self.departmentVal())
                    payload["P_DEPART_ID"] = self.departmentVal();
                if (self.jobVal())
                    payload["P_JOB_ID"] = self.jobVal();
                if (self.addressLineVal())
                    payload["P_ADDRESS_LINE"] = self.addressLineVal();

                payload["P_FROM_DATE"] = payload["P_FROM_DATE"].split("-").length != 4 ? payload["P_FROM_DATE"].split("-").reverse().join("-") : payload["P_FROM_DATE"];
                payload["P_TO_DATE"] = payload["P_TO_DATE"].split("-").length != 4 ? payload["P_TO_DATE"].split("-").reverse().join("-") : payload["P_TO_DATE"];

                // app.loading(true);
                app.actionStatus("1");
                services.addGeneric('timeCard', payload).then(res => {
                    app.actionStatus("0");
                    // app.loading(false);
                    if (res.length == 0) {
                        app.messages.push({ severity: "error", summary: app.translate("timeCard.empty_result"), autoTimeout: 0 });
                        self.response([]);
                    }
                    else {
                        if (res.length)
                            self.response(res);
                        else
                            self.response([res]);
                        self.delAction();
                    }


                }, error => {
                    app.actionStatus("0");
                    app.messages.push({ severity: "error", summary: 'Error', autoTimeout: 0 });
                    // app.loading(false);
                })
            }


            self.resetAction = function () {
                self.startDateVal("");
                self.endDateVal("");
                self.legalEntityVal("");
                self.locationVal("");
                self.departmentVal("");
                self.positionVal("");
                self.jobVal("");
                self.addressLineVal("");
            };

            self.delAction = function () {
                app.confirmMsg(app.translate('timeCard.delMsgConf').replace("#num#", self.response().length), yes => {
                    if (yes) {
                        // app.loading(true);
                        app.actionStatus("2");
                        services.addGeneric('timeCard/del', payload).then(response => {
                            app.actionStatus("0");
                            try { response = JSON.parse(response); } catch (ignored) { }

                            if (response.status == 'Done') {
                                app.infoMsg("Data has been Deleted Sucessfully!");
                                // app.messages.push({ severity: "info", summary: "Data has been Deleted Sucessfully!", autoTimeout: 0 });
                            } else if (response.status == 'Info') {
                                var msg = response.message;
                                try { msg = JSON.parse(msg) } catch (ignored) { }
                                app.infoMsg(localStorage.selectedLanguage == "ar" && msg.ar ? msg.ar : (msg.en || msg));
                                // app.messages.push({ severity: "info", summary: localStorage.selectedLanguage == "ar" && msg.ar ? msg.ar : (msg.en || msg), autoTimeout: 0 });
                            } else {
                                app.infoMsg(response.message);
                                // app.messages.push({ severity: "error", summary: response.message, autoTimeout: 0 });
                            }
                            // app.loading(false);
                            self.response([])
                        }, e => {
                            app.actionStatus("0");
                            // app.loading(false);
                            app.infoMsg( "can't deleted!");
                            // app.messages.push({ severity: "error", summary: "can't deleted!", autoTimeout: 0 });
                        })
                    }
                })
            }

            self.handleAttached = function (info) {
                // $(document).on("keyup", ".inpt", e => {
                //     if (e.keyCode === 13 && app.actionStatus() == 0)
                //         setTimeout(() => {
                //             self.searchAction();
                //         }, 100);

                // })
                if (app.actionStatus() == '0')
                    loadLov();
                else
                    app.loading(false);
            };

            function loadLov() {
                app.actionStatus("3");
                self.loading(true);
                services.getGeneric('timeCard').then(res => {
                    app.actionStatus("0");
                    self.loading(false);
                    app.loading(false);
                    self.legalEntityOps(res.find(i => i.name == 'P_BU_ID').values.item.map((o, i) => ({ label: res.find(i => i.name == 'P_BU_ID').lovLabels.item[i], value: o })));
                    self.departmentOps(res.find(i => i.name == 'P_DEPART_ID').values.item.map((o, i) => ({ label: res.find(i => i.name == 'P_DEPART_ID').lovLabels.item[i], value: o })));
                    self.locationOps(res.find(i => i.name == 'P_LOCATION_ID').values.item.map((o, i) => ({ label: res.find(i => i.name == 'P_LOCATION_ID').lovLabels.item[i], value: o })));
                    self.positionOps(res.find(i => i.name == 'P_POSITION_ID').values.item.map((o, i) => ({ label: res.find(i => i.name == 'P_POSITION_ID').lovLabels.item[i], value: o })));
                    self.jobOps(res.find(i => i.name == 'P_JOB_ID').values.item.map((o, i) => ({ label: res.find(i => i.name == 'P_JOB_ID').lovLabels.item[i], value: o })));
                    self.addressLineOps(res.find(i => i.name == 'P_ADDRESS_LINE').values.item.map((o, i) => ({ label: res.find(i => i.name == 'P_ADDRESS_LINE').lovLabels.item[i], value: o })));
                }, e => {
                    app.actionStatus("0");
                    self.loading(false);
                    app.failCbFn(e);
                });
            }

            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });
            function initTranslations() {
                self.pageTitle(app.translate("pages.timeCard"));
                self.search(app.translate("others.search"));
                self.process(app.translate("others.process"));
                self.loading(app.translate("timeCard.loading"));
                self.searching(app.translate("timeCard.searching"));
                self.processing(app.translate("timeCard.processing"));

                self.clear(app.translate("others.clear"));
                self.deleteLbl(app.translate("others.delete"));

                self.startDateLbl(app.translate("timeCard.startDate"))
                self.endDateLbl(app.translate("timeCard.endDate"))
                self.legalEntityLbl(app.translate("timeCard.legalEntity"))
                self.locationLbl(app.translate("timeCard.location"))
                self.departmentLbl(app.translate("timeCard.department"))
                self.positionLbl(app.translate("timeCard.position"))
                self.addressLineLbl(app.translate("timeCard.addressLine"))
                self.jobLbl(app.translate("timeCard.job"))
            }
            initTranslations();
        };
        return timeCardSummaryViewModel;
    });
