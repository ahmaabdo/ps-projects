/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * paasDefaultValueSummary module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox', 'ojs/ojtable',
    'ojs/ojdialog'], function (oj, ko, $, app, services, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function paasDefaultValueSummaryContentViewModel() {
        var self = this;


//---------- Observable decleration----------------
        self.columnArray = ko.observableArray();
        self.placeholder = ko.observable();
        self.selctedStruct = ko.observable();
        self.isRequired = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.deptObservableArray = ko.observableArray();
        self.isDisable = ko.observable(true);
        self.backActionLbl = ko.observable();
        self.addBtnDisabled = ko.observable(false);
        self.giudMessage = ko.observable();
        self.viewBtnAction = ko.observable();
        self.viewLbl = ko.observable();
        self.dataNotFoundMessage = ko.observable();
        self.giudMessage = ko.observable();
        self.paasDefaultValueScreenSummary = ko.observable();
        self.checkResultSearch = ko.observable();
        self.messages = ko.observableArray();
        self.EitName = ko.observableArray([]);

        self.dataprovider = new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'});

        //---------- warring message decleration-----------------
        self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
        self.position =
                {
                    "my": {"vertical": "top", "horizontal": "end"},
                    "at": {"vertical": "top", "horizontal": "end"},
                    "of": "window"
                };

        self.closeMessageHandler = function (event) {
            self.messages.remove(event.detail.message);
        };

        //-------------- table selection ----------------------
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.oprationdisabled(false);
                self.selectedRowKey(currentRow['rowKey']);
                self.selectedIndex(currentRow['rowIndex']);
            }
            if (!currentRow) {
                self.addBtnDisabled(false);
                self.oprationdisabled(true);

            }

        };

        //------ add button action-----------------
        self.addAdditionalDetails = function () {
            app.loading(true);
            var positionObj = {};
            positionObj.type = "ADD";
            oj.Router.rootInstance.store(positionObj);
            oj.Router.rootInstance.go('paasDefaultValueOperation');
        };

        //-------view button action----------------
        self.viewBtn = function () {
            app.loading(true);
            var element = document.getElementById('table');
            var currentRow = element.currentRow;
            self.deptObservableArray()[currentRow['rowIndex']].type = 'VIEW';
            oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
            oj.Router.rootInstance.go('paasDefaultValueOperation');

        };


        //------------ Edit button action ----------------
        self.editBtn = function () {
            var element = document.getElementById('table');
            var currentRow = element.currentRow;
            self.deptObservableArray()[currentRow['rowIndex']].type = 'EDIT';
            oj.Router.rootInstance.store(self.deptObservableArray()[currentRow['rowIndex']]);
            oj.Router.rootInstance.go('paasDefaultValueOperation');
        };
        //--------------------Global Delete action------------------
        self.globalDelete = function () {
            var element = document.getElementById('table');
            var currentRow = element.currentRow;
            var id = self.deptObservableArray()[currentRow['rowIndex']].id;

            var deletePaasValueCBF = function () {
                self.deptObservableArray([]);
                self.addBtnDisabled(true);
                self.messages.push({severity: 'confirmation', summary: self.recordDeletedMessage(), autoTimeout: 0});
                self.structureChangedHandler();
            };
            var serviceName = "paasdefultvalue/deletePaasDefaultValue/";
            services.getGeneric(serviceName + id).then(deletePaasValueCBF, app.failCbFn);
        };

        //---------------  self service change handler-----------------
        self.structureChangedHandler = function () {
            var positionCode = self.selctedStruct();
            var successCBFN = function (data) {
                if (data.length !== 0) {
                    for (var i = 0; i < data.length; i++) {
                        for (var j = 0; j < app.globalEitNameReport().length; j++) {
                            if (app.globalEitNameReport()[j].value == data[i].eitCode) {
                                data[i].eitCodeLbl = app.globalEitNameReport()[j].label;
                            }
                        }
                    }
                    self.deptObservableArray(data);
                } else {
                    self.deptObservableArray([]);
                    self.messages.push({severity: 'info', summary: self.dataNotFoundMessage(), detail: self.giudMessage(), autoTimeout: 0
                    });
                }
            };
            var failCbFn = function () {
                $.notify(self.checkResultSearch(), "error");
            };
            var serviceName = "paasdefultvalue/getPaasDefaultValue";
            services.getGeneric(serviceName + '/' + positionCode, {}).then(successCBFN, failCbFn);

        };
        //-------------- Back button action-------------         
        self.backAction = function () {
            oj.Router.rootInstance.go('setup');
        };

        //----------- handel attached method------------
        self.handleAttached = function () {
            app.loading(false);
            var arr = JSON.parse(sessionStorage.eitName).map(e => ({label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE}));
            self.EitName(arr);
        };

        //--------------------------End Of Section ----------------------------------
        self.commitRecord = function (data, event) {
            self.globalDelete();
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButtonValid = function () {
            document.querySelector("#ValidationDialog").close();
        };


        //--------- open delete button dialog------------------
        self.deleteBtn = function () {
            document.querySelector("#yesNoDialog").open();
        };
        //---------------------Translate Section ------------------------
        self.ok = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.deleteLbl = ko.observable();
        self.eitNameLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable(),
                self.yes = ko.observable();
        self.no = ko.observable();
        self.queryLbl = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.deleteValidationMessage = ko.observable();
        self.segmintNameLbl = ko.observable();
        self.requiredLbl = ko.observable();
        self.recordDeletedMessage = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        function initTranslations() {
            self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.deleteLbl(getTranslation("others.delete"));
            self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
            self.oprationMessage(getTranslation("approvalScreen.oprationMessage"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.checkResultSearch(getTranslation("common.checkResultSearch"));
            self.segmintNameLbl(getTranslation("labels.segmentName"));
            self.backActionLbl(getTranslation("approvalScreen.backAction"));
            self.requiredLbl(getTranslation("others.required"));
            self.queryLbl(getTranslation("paasDefaultValueScreen.queryLbl"));
            self.dataNotFoundMessage(getTranslation("messageNotification.dataNotFoundMessage"));
            self.giudMessage(getTranslation("messageNotification.giudMessage"));
            self.viewLbl(getTranslation("paasDefaultValueScreen.viewLbl"));
            self.paasDefaultValueScreenSummary(getTranslation("paasDefaultValueScreen.paasDefaultValueScreenSummary"));
            self.recordDeletedMessage(getTranslation("messageNotification.recordDeletedMessage"));
            self.deleteValidationMessage(getTranslation("paasDefaultValueScreen.deleteValidationMessage"));
            ;

            self.columnArray([
                {
                    "headerText": self.eitNameLbl(), "field": "eitCodeLbl"
                },

                {
                    "headerText": self.segmintNameLbl(), "field": "segmentName"
                },
                {
                    "headerText": self.queryLbl(), "field": "query"
                }
            ]);
        }
        initTranslations();
    }
    return paasDefaultValueSummaryContentViewModel;
});
