/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * paasDefaultValueOperation module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'notify', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox', 'ojs/ojarraytabledatasource', 'ojs/ojmessages', 'ojs/ojtrain', 'ojs/ojvalidationgroup',
    'ojs/ojdialog'], function (oj, ko, $, app, services, commonhelper) {
    /**
     * The view model for the main content view template
     */
    function paasDefaultValueOperationContentViewModel() {
        var self = this;
        self.pageMode = ko.observable();
        self.selctedStruct = ko.observable();
        self.isRequired = ko.observable(true);
        self.isDisableAdd = ko.observable(false);
        self.isDisable = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.nextBtnVisible = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.deptObservableArray = ko.observableArray();
        self.queryListArr = ko.observableArray([]);
        self.checkResultSearch = ko.observable();
        self.dataprovider = ko.observableArray();
        self.dataprovider(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'segementName'}));
        self.messages = ko.observableArray();
        self.selfServiceDisabled = ko.observable(false);
        self.segmentDisabled = ko.observable(false);
        self.queryDisabled = ko.observable(false);
        self.addBtnVisible = ko.observable(true);
        self.editBtnVisible = ko.observable(true);
        self.EitName = ko.observableArray([]);
        self.segementOption = ko.observableArray([]);
        self.QueryTypeOption = ko.observableArray([]);
        self.selctedApprovalType = ko.observable();
        self.updatedMessage = ko.observable();
        self.groupValid = ko.observable();
        self.nextBtnVisible = ko.observable(true);
        self.trainVisable = ko.observable(true);
        self.lang = "";
        //----------- train Setps -------------------------------
        self.currentStepValue = ko.observable('stp1');
        self.stepArray = ko.observableArray([]);

        // ------- train current step ----------------
        self.currentStepValueText = function () {

            if (self.currentStepValue() == 'stp1') {
                if (self.retrieve.type == 'VIEW') {
                    self.selfServiceDisabled(true);
                    self.segmentDisabled(true);
                    self.queryDisabled(true);
                    self.nextBtnVisible(false);
                    self.addBtnVisible(false);
                    self.editBtnVisible(false);
                    self.trainVisable(false);
                }
            }
            if (self.currentStepValue() == 'stp2') {
                if (self.retrieve.type == 'ADD') {
                    self.selfServiceDisabled(true);
                    self.segmentDisabled(true);
                    self.queryDisabled(true);
                    self.nextBtnVisible(false);
                    self.addBtnVisible(true);
                    self.editBtnVisible(false);
                } else
                if (self.currentStepValue() == 'stp2') {
                    if (self.retrieve.type == 'EDIT') {
                        self.selfServiceDisabled(true);
                        self.segmentDisabled(true);
                        self.queryDisabled(true);
                        self.nextBtnVisible(false);
                        self.editBtnVisible(true);
                        self.addBtnVisible(false);
                    }
                }
            }
        };


        //----------- stop train station--------------------
        self.stopSelectListener = function (event) {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                event.preventDefault();
                return;
            }
            self.previousStep();
        };

        //------ previuos Step-----------------
        self.previousStep = function () {
            var prev = document.getElementById("train").getPreviousSelectableStep();
            if (prev != null) {
                self.currentStepValue('stp1');
                self.selfServiceDisabled(false);
                self.segmentDisabled(false);
                self.queryDisabled(false);
                self.addBtnVisible(false);
                self.editBtnVisible(false);
                self.nextBtnVisible(true);
            }

        };

        //------------  next step------------------
        self.nextStep = function () {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid") {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            var next = document.getElementById("train").getNextSelectableStep();
            self.currentStepValue(next);
        };

        //--------------- define messages notification ---------------//

        self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
        self.position =
                {
                    "my": {"vertical": "top", "horizontal": "end"},
                    "at": {"vertical": "top", "horizontal": "end"},
                    "of": "window"
                };
        self.closeMessageHandler = function (event) {
            self.messages.remove(event.detail.message);
        };
        // ---------table select listener----------
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.oprationdisabled(false);
                self.selectedRowKey(currentRow['rowKey']);
            }
        };

        //-----------------------------handel attached method --------------------------------------------
        self.handleAttached = function () {
            self.retrieve = oj.Router.rootInstance.retrieve();
            app.loading(false);
            self.queryListArr(app.getPaaSLookup('SaaS_SS_DefaultValue'));
            self.queryListArr().map((e, idx) => {
                self.QueryTypeOption().push({label: self.queryListArr()[idx].label, value: self.queryListArr()[idx].value});
            });

            // ------------- retrieve eit from session storage ---------------
            var arr = JSON.parse(sessionStorage.eitName).map(e => ({label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE}));
            self.EitName(arr);

            //  -------------------Check  Screen opreation type --------------
            if (self.retrieve.type == 'ADD') {
                self.selfServiceDisabled = ko.observable(false);
                self.segmentDisabled = ko.observable(false);
                self.queryDisabled = ko.observable(false);
                self.nextBtnVisible(true);
                self.addBtnVisible(false);
                self.editBtnVisible(false);
            }
            if (self.retrieve.type == 'EDIT') {
                self.selctedStruct(self.retrieve.eitCode);
                self.selctedsegementType(self.retrieve.segmentName);
                self.queryName(self.retrieve.query);
                self.selfServiceDisabled(false);
                self.segmentDisabled(false);
                self.queryDisabled(false);
                self.addBtnVisible(false);
                self.editBtnVisible(false);
                self.nextBtnVisible(true);
                self.isDisable(false);
                self.structureChangedHandler();

            }
            if (self.retrieve.type == 'VIEW') {
                self.selctedStruct(self.retrieve.eitCode);
                self.selctedsegementType(self.retrieve.segmentName);
                self.queryName(self.retrieve.query);
                self.structureChangedHandler();
                self.currentStepValueText();
            }
        };


        //------------------------operation edit section ----------------------------
        function oprationServiceRecordEdit() {
            var id = self.retrieve.id;
            var payload = {
                "eitCode": self.selctedStruct(),
                "segmentName": self.selctedsegementType(),
                "query": self.queryName()
            };
            var updateDefaultValueCbFn = function () {
                oj.Router.rootInstance.go("paasDefaultValueSummary");
            };
            var serviceName = "paasdefultvalue/updatePaasDefaultVal";
            services.addGeneric(serviceName + "/" + id, payload).then(updateDefaultValueCbFn, app.failCbFn);
            self.messages.push({severity: 'confirmation', summary: self.updatedMessage(), detail: self.showAddedMessage(), autoTimeout: 0
            });
        }
        ;

        //--------------------------End Of Section ----------------------------------
        self.commitRecord = function (data, event) {
            if (self.retrieve.type == 'ADD') {
                app.loading(true);
                oprationServiceRecordAdd();
                document.querySelector("#yesNoDialogAdd").close();
            }
            if (self.retrieve.type == 'EDIT') {
                app.loading(true);
                oprationServiceRecordEdit();
                document.querySelector("#yesNoDialog").close();
            }
        };
        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
            document.querySelector("#yesNoDialogAdd").close();
        };
        self.cancelButtonValid = function () {
            document.querySelector("#ValidationDialog").close();
        };

        self.editDefualtPaaSAction = function () {
            document.getElementById('yesNoDialog').open();
        };
        self.addDefaultValueAction = function () {
            document.getElementById('yesNoDialogAdd').open();
        };

        // ------------- store data from eit------------------
        var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
        self.SegmintEit = function (event, data) {
            self.deptObservableArray([]);
            var positionCode = self.selctedStruct();
            var getEitCBFN = function (data) {
                var Data = data;
                for (var i = 0; i < Data.length; i++) {
                    var obj = {};
                    obj.eitCode = self.selctedStruct();
                    obj.hide = false;
                    obj.enable = false;
                    obj.required = false;
                    obj.FORM_LEFT_PROMPT = Data[i].FORM_LEFT_PROMPT;
                    obj.segementName = Data[i].DESCRIPTION;
                    obj.type = self.selctedApprovalType();
                    obj.createdBy = rootViewModel.personDetails().personId;
                    obj.positionName = "";
                }
                self.segementOption([]);
                data.map((e, idx) => {
                    self.segementOption.push({lable: data[idx].DESCRIPTION,
                        value: data[idx].DESCRIPTION});
                }
                );
            };
            var failCbFn = function () {
                $.notify(self.checkResultSearch(), "error");
            };
            if (app.getLocale() == 'ar') {
                self.lang = "AR";
            } else {
                self.lang = "US";
            }
            services.getEIT(positionCode, self.lang).then(getEitCBFN, failCbFn);
        };
        //------ structureChangedHandler ----------
        self.structureChangedHandler = function () {
            self.SegmintEit();
        };
        //------ structureChangedHandlerSegement ----------
        self.SegementChangedHandler = function () {
        };
        //---------- check values ---------------
        self.addDisable = ko.observable(true);
        if (self.selctedStruct() != null && self.selctedsegementType() != null && self.queryName() != null) {
            self.addDisable(false);
        }

        //---------------back button action-------------------
        self.backAction = function () {
            if (oj.Router.rootInstance._navHistory.length > 1) {
                app.loading(true);
                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
            }
        };
        //------------------------End Of Section ----------------------------
        function oprationServiceRecordAdd() {
            var payload = {
                "eitCode": self.selctedStruct(),
                "segmentName": self.selctedsegementType(),
                "query": self.queryName()
            };
            var addDefaultValueCbFn = function () {
                oj.Router.rootInstance.go('paasDefaultValueSummary');
            };
            var serviceName = 'paasdefultvalue/addDefaultValue';
            services.addGeneric(serviceName, payload).then(addDefaultValueCbFn, app.failCbFn);
            self.messages.push({severity: 'confirmation', summary: self.addedMessage(),
                detail: self.showAddedMessage(), autoTimeout: 0});
        }
        ;
        //--------structureChangedHandlerSegement-----
        self.structureChangedHandlerSegement = function () {
        };
        //----------QueryStructureChangedHandler----
        self.QueryStructureChangedHandler = function () {

        };

        //----------addDefaultValueAction----
        self.addDefaultValueAction = function () {
            self.isDisableAdd(false);
            document.querySelector('#yesNoDialogAdd').open();

        };
        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();

        self.submitLbl = ko.observable();
        self.deleteLbl = ko.observable();
        self.eitNameLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.oprationMessageAdd = ko.observable();
        self.segmintNameLbl = ko.observable();
        self.cancel = ko.observable();
        self.nextLbl = ko.observable();
        self.showAddedMessage = ko.observable();
        self.addedMessage = ko.observable();
        self.selctedsegementType = ko.observable();
        self.paasDefaultValueScreenLbl = ko.observable();
        self.queryName = ko.observable();
        self.segementLbl = ko.observable();
        self.queryNameLbl = ko.observable();
        self.requiredLbl = ko.observable();
        self.create = ko.observable();
        self.review = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        //----------------------------------------------------
        function initTranslations() {
            self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.submitLbl(getTranslation("paasDefaultValueScreen.submitLbl"));
            self.deleteLbl(getTranslation("others.delete"));
            self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
            self.oprationMessageAdd(getTranslation("paasDefaultValueScreen.oprationMessageAdd"));
            self.oprationMessage(getTranslation("paasDefaultValueScreen.oprationMessage"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.segmintNameLbl(getTranslation("labels.segmentName"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.checkResultSearch(getTranslation("common.checkResultSearch"));
            self.cancel(getTranslation("others.cancel"));
            self.requiredLbl(getTranslation("others.required"));
            self.queryNameLbl(getTranslation("paasDefaultValueScreen.queryNameLbl"));
            self.segementLbl(getTranslation("paasDefaultValueScreen.segementLbl"));
            self.paasDefaultValueScreenLbl(getTranslation("paasDefaultValueScreen.paasDefaultValueScreenLbl"));
            self.addedMessage(getTranslation("messageNotification.addedMessage"));
            self.updatedMessage(getTranslation("messageNotification.updatedMessage"));
            self.nextLbl(getTranslation("others.next"));
            self.create(getTranslation("labels.create"));
            self.review(getTranslation("others.review"));
            self.stepArray([{label: self.create(), id: 'stp1'},
                {label: self.review(), id: 'stp2'}]);

                
        }

        initTranslations();
    }

    return paasDefaultValueOperationContentViewModel;
});
