/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'ojs/ojfilepicker',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojarraydataprovider', 'ojs/ojformlayout'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function EducationExpenseOperationViewModel() {
                var self = this;
                self.currentStepValue = ko.observable('stp1');
                self.currentStepValueDetails = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.addBtnVisibleDetails = ko.observable(false);
                self.previousBtnVisibleDetails = ko.observable(false);
                self.nextBtnVisible = ko.observable(false);
                self.nextBtnVisibleDetails = ko.observable(true);
                self.isRequired = ko.observable();
                self.isDisabled = ko.observable(true);
                self.isDisabledDetails = ko.observable(false);
                self.isDisabledSemister = ko.observable(false);
                self.addStudentVisible = ko.observable(false);
                self.stepArray = ko.observableArray([]);
                self.stepArrayDetails = ko.observableArray([]);
                self.trainVisibility = ko.observable(false);
                self.trainVisibilityDetails = ko.observable(false);
                self.oprationdisabled = ko.observable(false);
                self.groupValid = ko.observable();
                self.groupValidDetails = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.approvlModel = {};
                self.SemesterArr = ko.observable();
                self.StudentNameArr = ko.observable();
                self.viewAttachmentBtnVisible = ko.observable(true);
                self.oprationVisible = ko.observable(false);
                self.oprationVisibleView = ko.observable();
                self.summaryObservableArray = ko.observableArray([]);
                self.operationType = ko.observable();
                self.columnArray = ko.observableArray();
                self.detailsRowId = ko.observable();
                self.koArray = ko.observableArray([]);
                self.approve = ko.observable();
                self.reject = ko.observable();
                self.rejectMessage = ko.observable();
                self.isLastApprover = ko.observable(false);
                self.fyaFlag = ko.observable(true);
                self.disableSubmit = ko.observable(false);
                self.disableReject = ko.observable(false);
                self.workFlowApprovalId = ko.observable();
                self.checkResultSearch = ko.observable();
                self.employeeGrade = ko.observable();
                self.joinDateLbl = ko.observable();
                self.employeeNumberLbl = ko.observable();
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'}));
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));
                self.StudentArr = ko.observableArray([]);
                self.semisterArray = ko.observableArray([
//                    {"value": "semisterOne", "label": "Semister One"},
//                    {"value": "semisterTwo", "label": "Semister Two"},
//                    {"value":"fullYear","label":"Full Year"}
                ]);
                self.semisterArray(app.getPaaSLookup("eduSemisterArr"));
                self.paymentMethodArray = ko.observableArray([
//                    {"value": "Schoolaccount", "label": "School account"},
//                    {"value": "EmployeeAccount", "label": "Employee Account"}
                ]);
                self.paymentMethodArray(app.getPaaSLookup("eduPaymentMethod"));
                self.bankNameArray = ko.observableArray([]);
                var globalRowData;
                var indexRowData;
                var lastHireDate;

                self.EducationExpenseRequestModle = {
                    requestDate: ko.observable(),
                    displayName: ko.observable(),
                    empDepartment: ko.observable(),
                    positionClassification: ko.observable(),
                    AcadmicYear: ko.observable(),
                    Semester: ko.observable(),
                    nationality:ko.observable(),
//                    employeeGrade: ko.observable(),
//                    joiningDate: ko.observable(),
                    employeeNumber: ko.observable(),
                    requestNumber: ko.observable()
                };
                self.EducationExpenseRequestModleDetails = {
                    StudentName: ko.observable(),
                    StudentId:ko.observable(),
                    StudentAge:ko.observable(),
                    StudentGrade: ko.observable(),
                    StudentSchoolName: ko.observable(),
//                    StudentInvoiceNo: ko.observable(),
//                    StudentInvoiceDate: ko.observable(),
                    bankName:ko.observable(),
                    iban:ko.observable(),
                    paymentMethod:ko.observable(),
                    note: ko.observable(),
                    StudentInvoiceAmount:ko.observable(),
                    StudentInvoicePayment:ko.observable()
                };
                self.payedVisibleDetails=ko.observable(true);
                self.StudentInvoiceAmountLbl = ko.observable();
                self.StudentInvoicePaymentLbl=ko.observable();
                                    
                //--------------method to formate date----------//
                function formatDateToday() {
                    var d = new Date(),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                ;
                //----------------Emd method  for formate Date-------------//
                self.approveRequst = function () {
                    self.disableSubmit(true);

                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": app.personDetails().personId,
                        "RESPONSE_CODE": "APPROVED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "PERSON_NAME": self.retrieve.personName,
                        "rejectReason": " ",
                        "workflowId": self.workFlowApprovalId(),
                        "PERSON_NAME":self.retrieve.personName,
                        "PERSON_EMP_FYI_ID":self.retrieve.person_id
                    };
                    var approvalActionCBFN = function (data) {
                        //                     
                        if (self.isLastApprover()) {

                            var payload = {
                                "status": "Approved",
                                "masterId": self.retrieve.TRS_ID
                            };
                            var totalAmt = 0;
                            for (var i = 0; i < self.summaryObservableArray().length; i++) {
                                totalAmt += Number(self.summaryObservableArray()[i].invoicePayment);
                            }

                            var updateStatusCBFN = function (data) {
                                app.loading(false);
                                oj.Router.rootInstance.go('notificationScreen');
                                var date = new Date();
                                var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getDate().toString();
                                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate().toString();
                                var month = (date.getMonth() + 1).toString();
                                var year = date.getFullYear();
                                if (month.length < 2)
                                    month = '0' + month;
                                if (firstDay.length < 2)
                                    firstDay = '0' + firstDay;
                                if (lastDay.length < 2)
                                    lastDay = '0' + lastDay;
                                var effectiveStartDate = [year, month, firstDay].join('/');
                                var effectiveEndDate = [year, month, lastDay].join('/');
                                var payloadElemntEntry = {
                                    "entryType": "E",
                                    "creatorType": "H",
                                    "assignmentNumber": "E" + self.retrieve.person_number,
                                    "legislativeDataGroupName": "SA Legislative Data Group",
                                    "effectiveStartDate": effectiveStartDate,
                                    "effectiveEndDate": effectiveEndDate,
                                    "sourceSystemOwner": "PAAS",
                                    "SourceSystemId": "EDU_ALW_12_1_2020",
                                    "amount": totalAmt,
                                    "childrenNumber": (self.summaryObservableArray().length).toString(),
                                    "personNumber": self.retrieve.person_number,
                                    "requestDetailsId": self.retrieve.TRS_ID
                                };
                                var createEducationAllowanceMethodCBFN = function (data) {

                                };
                                //services.submitHDLFile("EducationAllowancePlanFile", JSON.stringify(payloadElemntEntry)).then(createEducationAllowanceMethodCBFN, app.failCbFn);
                            };
                            services.addGeneric("expense/UpdateStatus/", payload).then(updateStatusCBFN, app.failCbFn);



//                            var jsonData = {
//                                url: self.personDetails().employeeURL["XXX_EXPENSE_REQUEST"],
//                                effectiveDate: todayreverse,
//                                employeeNumber: self.personNumber(),
//                                finalDecision: self.validationModle.finalDecisionVal()
//
//                            };
//                            var supmitEFFCBFN = function (data) {
//                                var Data = data;
//                                // self.disableSubmit(false);
//                                if (oj.Router.rootInstance._navHistory.length > 1) {
//                                    oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
//                                } else {
//                                    oj.Router.rootInstance.go('home');
//                                }
//
//                            };

                            // services.addGeneric("eff/" + "ADD",JSON.stringify(jsonData)).then(supmitEFFCBFN, app.failCbFn);


                        } else {
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('home');
                            }
                        }
                        document.querySelector("#yesNoDialog").close();
                        // oj.Router.rootInstance.go('notificationScreen');
                    };

                    services.workflowAction("workflowApproval/", headers).then(app.failCbFn,approvalActionCBFN);
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.cancelRewaardRequst = function () {
                    oj.Router.rootInstance.go('notificationScreen');
                };
                self.rejectButton = function () {
                    document.querySelector("#rejectDialog").open();
                };
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.rejectCancelButton = function () {
                    document.querySelector("#rejectDialog").close();
                };
                self.commitRecord = function () {
                    app.loading(true);
                    self.approveRequst();
                };
                self.updateStatus = function () {
                    var payload = {
                        "status": "REJECTED",
                        "masterId": self.retrieve.TRS_ID
                    };
                    var rejectedActionCBFN = function (data) {
                        oj.Router.rootInstance.go('notificationScreen');
                    };


                    services.addGeneric("expense/UpdateStatus/", payload).then(rejectedActionCBFN, app.failCbFn)
                };
                self.rejectRewaardRequst = function (data, event) {
                    self.disableReject(true);
                    app.loading(false);
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": app.personDetails().personId,
                        "RESPONSE_CODE": "REJECTED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "rejectReason": " ",
                        "workflowId": self.workFlowApprovalId(),
                        "PERSON_NAME":self.retrieve.personName,
                        "PERSON_EMP_FYI_ID":self.retrieve.person_id
                    };

                    var approvalActionCBFN = function (data) {
                        self.updateStatus();
                        document.querySelector("#rejectDialog").close();
                        oj.Router.rootInstance.go('notificationScreen');
                    };
                    var failCbFn = function () {
                        self.updateStatus();
                        document.querySelector("#rejectDialog").close();
                        oj.Router.rootInstance.go('notificationScreen');
                        app.loading(true);

                    };
                    services.workflowAction("workflowApproval/", headers).then(approvalActionCBFN, failCbFn)

                };


                self.currentStepValueText = function () {};

                //----------- Start return to clearanceRequestSummary page----------------//
                self.cancelAction = function () {
                    oj.Router.rootInstance.go('EducationExpenseSummary');
                };
                //----------- End return to clearanceRequestSummary page----------------//
                self.previousStep = function () {
                    self.addBtnVisible(false);
                    self.nextBtnVisible(true);
                    self.addBtnVisible(false);
                    self.isDisabledSemister(false);
                    self.currentStepValue("stp1");

                };
                self.nextStep = function () {
                    self.addBtnVisible(true);
                    self.nextBtnVisible(false);
                    self.addBtnVisible(true);
                    self.isDisabledSemister(true);
                    self.currentStepValue("stp2");

                };
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.stopSelectListener = function () {};

                //------------------- get last hire date---------------//
                self.getLastHireDate = function () {
                    var personId = app.globalspecialistEMP().personId;
                    var getLastDateCBCF = function (data) {

                        if (data === "[]") {
                            lastHireDate = "";
                        } else {
                            lastHireDate = data;
                        }

                    };
                    services.getLastDate(personId).then(getLastDateCBCF, app.failCbFn);
                };

                //------------------------------------------------------//
                self.getDependents = function () {
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    var payload = {"reportName": 'XXX_HR_EMP_CONTACTS', "personId":self.retrieve.person_id};
                    var getDependentsSuccess = function (data) {
                        if (data != '[]') {
                            if (data.length > 0) {
                                for (var i = 0; i < data.length; i++) {
                                    self.StudentArr.push({
                                        "value": data[i].NAME,
                                        "label": data[i].NAME,
                                        "date": data[i].BIRTHDATE,
                                        "childId":data[i].CHILDID
                                    });
                                }
                            } else {
                                self.StudentArr.push({
                                    "value": data.NAME,
                                    "label": data.NAME,
                                    "date": data.BIRTHDATE
                                });
                            }
                        } else {
                            self.StudentArr([]);
                        }
                    };
                    services.getGenericReport(payload).then(getDependentsSuccess, app.failCbFn);

                };

                //-----------Start call method when page reload----------------//
                self.handleAttached = function () {
                    self.getDependents();
                    self.getBanks();
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    self.payedVisibleDetails(false);                                       
                    self.workFlowApprovalId(self.retrieve.id);
                    if (self.retrieve.TYPE == "FYI" || self.retrieve.status == "CLOSED") {
                        self.fyaFlag(false);
                    } else {
                        self.fyaFlag(true);
                    }
                    self.oprationVisible(false);
                    self.getMaster();
                    self.getDetails();
                    self.isLastApprover(app.isLastApprover(self.retrieve.TRS_ID, "XXX_EDU_EXPENSE_REQUEST", false));

                };
                self.getMaster = function () {
                    var getReportCbFn = function (data) {
                        if (data.length !== 0)
                        {
                           self.headerEmployeeText(self.expenseRequestHeader() + " " + self.employeeNameHeader()+" " + data.displayName + " " + self.employeeNumberHeader()+" " + data.personNumber);
                            self.EducationExpenseRequestModle.requestDate(data.requestDate);
                            self.EducationExpenseRequestModle.displayName(data.displayName);
                            self.EducationExpenseRequestModle.empDepartment(data.department);
                            self.EducationExpenseRequestModle.positionClassification(data.position);
                            self.EducationExpenseRequestModle.AcadmicYear(data.academicYear);
                            self.EducationExpenseRequestModle.Semester(data.semister);
                            self.EducationExpenseRequestModle.requestNumber(data.requestNumber);
                            self.EducationExpenseRequestModle.nationality(data.nationality);
//                            self.EducationExpenseRequestModle.employeeGrade(data.grade);
//                            self.EducationExpenseRequestModle.joiningDate(data.joiningDate);
                            self.EducationExpenseRequestModle.employeeNumber(data.personNumber);
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = "expense/getAll/" + self.retrieve.TRS_ID;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                }
                self.getDetails = function () {
                    self.summaryObservableArray([]);

                    var getReportCbFn = function (data) {
                        if (data.length !== 0)
                        {

                            for (var index = 0; index < data.length; index++) {
                                var obj = {};
                                obj.studentName = data[index].studentName;
                                obj.studentId=data[index].studentId;
                                obj.studentAge = data[index].studentAge;
                                obj.studentGrade = data[index].studentGrade;
                                obj.studentSchoolName = data[index].studentSchool;
                                obj.studentInvoiceNo = data[index].studentInvoiceNo;
                                obj.studentInvoiceDate = data[index].studentInvoiceDate;
                                obj.note = data[index].note;
                                obj.id = data[index].detailID;
                                obj.masterId = data[index].masterId;
                                obj.attachment = data[index].attachment;
                                obj.invoiceAmount = data[index].invoiceAmount;
                                obj.invoicePayment = data[index].invoicePayment;
                                obj.bankName=data[index].bankName;
                                self.bankNameArray.push({"value":data[index].bankName,"label":data[index].bankName});
                                obj.iban=data[index].iban;
                                obj.paymentMethod=data[index].paymentMethod;

                                self.summaryObservableArray.push(obj);
                            }
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = "expense/getAllDetails/" + self.retrieve.TRS_ID;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };
                //----------------------------------------------------------------//

                //******************************Add new student**********************
                self.AddStudent = function () {
                    self.operationType("Add");
                    self.clearDialog();
                    document.querySelector("#detailDialog").open();
                };
                self.clearDialog = function () {
                    self.previousStepDetails();
                    self.EducationExpenseRequestModleDetails.StudentName('');
                    self.EducationExpenseRequestModleDetails.StudentDateOfBirth('');
                    self.EducationExpenseRequestModleDetails.StudentGrade('');
                    self.EducationExpenseRequestModleDetails.StudentSchoolName('');
                    self.EducationExpenseRequestModleDetails.StudentInvoiceNo('');
                    self.EducationExpenseRequestModleDetails.StudentInvoiceDate('');
                    self.EducationExpenseRequestModleDetails.note('');
                    self.koArray([]);

                };
                //****************************Edit new student*********************
                self.EditStudent = function () {
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    indexRowData = currentRow['rowIndex'];
                    var rowData = self.summaryObservableArray()[currentRow['rowIndex']];
                    self.koArray([]);
                    var data = rowData.attachment;
                    for (var i = 0; i < data.length; i++) {
                        self.koArray.push({
                            "id": i,
                            "name": data[i].name,
                            "data": data[i].data
                        });
                    }
                    document.querySelector("#detailDialog").open();
                    self.isDisabledDetails(false);
                    self.nextBtnVisibleDetails(true);
                    self.addBtnVisibleDetails(false);
                    self.previousBtnVisibleDetails(false);
                    self.viewAttachmentBtnVisible(true);
                    self.currentStepValueDetails('stp1');
                    self.EducationExpenseRequestModleDetails.StudentName(rowData.studentName);
                    self.EducationExpenseRequestModleDetails.StudentDateOfBirth(rowData.studentDateOfBirth);
                    self.EducationExpenseRequestModleDetails.StudentGrade(rowData.studentGrade);
                    self.EducationExpenseRequestModleDetails.StudentSchoolName(rowData.studentSchoolName);
                    self.EducationExpenseRequestModleDetails.StudentInvoiceNo(rowData.studentInvoiceNo);
                    self.EducationExpenseRequestModleDetails.StudentInvoiceDate(rowData.studentInvoiceDate);
                    self.EducationExpenseRequestModleDetails.note(rowData.note);
                    globalRowData = rowData;
                    self.operationType("Edit");
                    self.detailsRowId(rowData.id);
                };
                self.onBankSelected = function (event) {

                };
                self.onPaymethodSelected = function (event) {

                };
                self.getBanks = function () {
                    var payload ={"reportName":"DependentReport","valueSet":"XXX_HR_DIRECT_TO","parent":"Installment Letter"};
                    var getDependentsSuccess = function (data) {
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                self.bankNameArray.push({
                                    "value": data[i].value_DESCRIPTION,
                                    "label": data[i].value_DESCRIPTION,
                                });
                            }
                        } else {
                            self.StudentArr.push([]);
                        }
                    };
                    services.getGenericReport(payload).then(getDependentsSuccess, app.failCbFn);

                };
                //******************************View new student******************
                self.ViewStudent = function () {
                    self.koArray([]);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    indexRowData = currentRow['rowIndex'];
                    var rowData = self.summaryObservableArray()[currentRow['rowIndex']];
                    var data = rowData.attachment;
                    for (var i = 0; i < data.length; i++) {
                        self.koArray.push({
                            "id": i,
                            "name": data[i].name,
                            "data": data[i].data
                        });
                    }
                    self.isDisabledDetails(true);
                    self.nextBtnVisibleDetails(false);
                    self.previousBtnVisibleDetails(false);
                    self.viewAttachmentBtnVisible(false);
                    self.trainVisibilityDetails(false);
                    var rowData = self.summaryObservableArray()[currentRow['rowIndex']];
                    self.EducationExpenseRequestModleDetails.StudentName(rowData.studentName);
                    self.EducationExpenseRequestModleDetails.StudentId(rowData.studentId);
                    self.EducationExpenseRequestModleDetails.StudentAge(Number(rowData.studentAge));
                    self.EducationExpenseRequestModleDetails.StudentGrade(rowData.studentGrade);
                    self.EducationExpenseRequestModleDetails.StudentSchoolName(rowData.studentSchoolName);
                    self.EducationExpenseRequestModleDetails.StudentInvoiceAmount(Number(rowData.invoiceAmount));
                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(Number(rowData.invoicePayment));
                    self.EducationExpenseRequestModleDetails.bankName(rowData.bankName);
                    self.EducationExpenseRequestModleDetails.iban(rowData.iban);
                    self.EducationExpenseRequestModleDetails.paymentMethod(rowData.paymentMethod);
                    self.EducationExpenseRequestModleDetails.note(rowData.note);
                    self.operationType("View");
                    document.querySelector("#detailDialog").open();

                };
                //***********************table Listener**************************
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    self.koArray([]);
                    self.oprationVisible(false);
                    self.addStudentVisible(false);
                    self.oprationVisibleView(true);

                };

                //****************************Details**********************************
                self.cancelDetailDialog = function () {
                    document.querySelector("#detailDialog").close();
                };
                //*********************cancelActionDetails***************************
                self.cancelActionDetails = function () {


                };
                //***************************previousStepDetails*********************
                self.previousStepDetails = function () {
                    self.isDisabledDetails(false);
                    self.nextBtnVisibleDetails(true);
                    self.addBtnVisibleDetails(false);
                    self.previousBtnVisibleDetails(false);
                    self.viewAttachmentBtnVisible(true);
                    self.currentStepValueDetails('stp1');
                };
                //*************************nextStepDetails*******************************
                self.nextStepDetails = function () {

                    var tracker = document.getElementById("trackerDetails");
                    if (tracker.valid != "valid" && tracker.valid) {
                        tracker.showMessages();
                    } else {
                        self.isDisabledDetails(true);
                        self.nextBtnVisibleDetails(false);
                        self.addBtnVisibleDetails(true);
                        self.previousBtnVisibleDetails(true);
                        self.currentStepValueDetails('stp2');
                        self.viewAttachmentBtnVisible(false);
                    }


                };
                //************************submitButtonDetails***************************
                self.submitButtonDetails = function () {
                    // if detail operation edit a
                    if (self.operationType() == "Edit") {
                        // if master was edit and details edit then update with webservice otherwise it will update only view by fill symmary
                        if (self.retrieve.operation == "EDIT") {
                            var payload = {
                                studentName: self.EducationExpenseRequestModleDetails.StudentName(),
                                studentDateOfBirth: self.EducationExpenseRequestModleDetails.StudentDateOfBirth(),
                                studentGrade: self.EducationExpenseRequestModleDetails.StudentGrade(),
                                studentSchool: self.EducationExpenseRequestModleDetails.StudentSchoolName(),
                                studentInvoiceNo: self.EducationExpenseRequestModleDetails.StudentInvoiceNo(),
                                studentInvoiceDate: self.EducationExpenseRequestModleDetails.StudentInvoiceDate(),
                                note: self.EducationExpenseRequestModleDetails.note(),
                                attachment: self.koArray()
                            };
                            var ClearanceRequestCbFn = function () {
                                document.querySelector("#detailDialog").close();
                                self.getDetails();

                            };
                            services.addGeneric("expense/updateDetails/" + self.detailsRowId(), JSON.stringify(payload)).then(ClearanceRequestCbFn, app.failCbFn);

                        }
                        // if details wasn't edit then it is add
                    } else if (self.operationType() == "View") {

                    } else {
                        // if master was edit then update details by web service otherwise wont update by webservice, only view and summary
                        if (self.retrieve.operation == "EDIT") {
                            var payload = {
                                studentName: self.EducationExpenseRequestModleDetails.StudentName(),
                                studentDateOfBirth: self.EducationExpenseRequestModleDetails.StudentDateOfBirth(),
                                studentGrade: self.EducationExpenseRequestModleDetails.StudentGrade(),
                                studentSchool: self.EducationExpenseRequestModleDetails.StudentSchoolName(),
                                studentInvoiceNo: self.EducationExpenseRequestModleDetails.StudentInvoiceNo(),
                                studentInvoiceDate: self.EducationExpenseRequestModleDetails.StudentInvoiceDate(),
                                note: self.EducationExpenseRequestModleDetails.note(),

                                attachment: self.koArray()
                            };
                            var ClearanceRequestCbFn = function () {
                                document.querySelector("#detailDialog").close();
                                self.getDetails();
                            };
                            services.addGeneric("expense/addExpenseDetails/" + self.retrieve.id, JSON.stringify(payload)).then(ClearanceRequestCbFn, app.failCbFn);

                        } else {

                            self.summaryObservableArray.push({
                                studentName: self.EducationExpenseRequestModleDetails.StudentName(),
                                studentDateOfBirth: self.EducationExpenseRequestModleDetails.StudentDateOfBirth(),
                                studentGrade: self.EducationExpenseRequestModleDetails.StudentGrade(),
                                studentSchoolName: self.EducationExpenseRequestModleDetails.StudentSchoolName(),
                                studentInvoiceNo: self.EducationExpenseRequestModleDetails.StudentInvoiceNo(),
                                studentInvoiceDate: self.EducationExpenseRequestModleDetails.StudentInvoiceDate(),
                                note: self.EducationExpenseRequestModleDetails.note(),
                                attachment: self.koArray(),
                                id: ""
                            });
                        }




                    }
                    document.querySelector("#detailDialog").close();

                };

                //****************************stopSelectListenerDetails*****************
                self.stopSelectListenerDetails = function () {

                };
                //-----------------attachment----------------------------//
                var dataFiles = {};
                //create file upload data provider
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.attachmentSelectListener = function (event) {
                    var files = event.detail.files;
                    for (var i = 0; i < files.length; i++) {
                        if (files.length > 0) {
                            //add the new files at the beginning of the list
                            if ((files[i].size / 1024 / 1024) > 2.5) {
                                $.notify(self.errorMessageAttachSize(), "error");

                            } else {
                                function getBase64(file) {
                                    return new Promise((resolve, reject) => {
                                        const reader = new FileReader();
                                        reader.readAsDataURL(file);
                                        reader.onload = () => resolve(reader.result);
                                        reader.onerror = error => reject(error);
                                    });
                                }
                                dataFiles.name = files[i].name;
                                dataFiles.id = i + 1;
                                getBase64(files[i]).then(function (data) {
                                    dataFiles.data = (data);
                                    self.attachmentId = self.attachmentId + 1;
                                    dataFiles.id = (self.attachmentId);
                                    self.koArray.push({
                                        "id": dataFiles.id,
                                        "name": dataFiles.name,
                                        "data": dataFiles.data
                                    });
                                }
                                );
                            }
                        }
                    }
                };
                self.removeSelectedAttachment = function (event) {
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        self.koArray.remove(function (item)
                        {
                            return (item.id == value);
                        });
                    });
                };
                function downloadURI(uri, name) {
                    var link = document.createElement("a");
                    link.setAttribute('href', uri);
                    link.setAttribute('download', name);
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                function downloadPDF(payloadAttachData) {
                    var link = document.createElement("a");
                    var data = payloadAttachData.data;
                    link.setAttribute('href', data);
                    link.setAttribute('download', payloadAttachData.name);
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }                
                self.openAttachmentViewer = function ()
                {

                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        var data = self.koArray().find(e => e.id == value);
                        downloadPDF(data);

                    });
                };

                function ApprovalOprationServiceRecord() {

                    self.approvlModel.type = "XXX_EDU_EXPENSE_REQUEST";
                    self.approvlModel.personNumber = app.personDetails().personNumber;
                    self.approvlModel.personName = app.personDetails().displayName;
                    self.approvlModel.presonId = app.personDetails().personId;
                    self.approvlModel.managerId = app.personDetails().managerId;
                    self.approvlModel.managerName = app.personDetails().managerName;//
                    self.approvlModel.managerOfManager = app.personDetails().managerOfManager;
                    self.approvlModel.managerOfMnagerName = app.personDetails().managerOfMnagerName;//
                    self.approvlModel.created_by = app.personDetails().personId;
                    self.approvlModel.creation_date = app.personDetails().personId;
                    var jsonData = ko.toJSON(self.approvlModel);
                    var getValidGradeCBF = function (data) {

                    };

                    services.addGeneric("expense/approval", jsonData).then(getValidGradeCBF, self.failCbFn);
                }

                //    define 
                self.cancel = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.submit = ko.observable();
                self.RequestDateLbl = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.EmployeeNameAndIdLbl = ko.observable();
                self.PositionLbl = ko.observable();
                self.DepartmentLbl = ko.observable();
                self.HireDateLbl = ko.observable();
                self.LastWorkingDayLbl = ko.observable();
                self.headerEmployeeText = ko.observable();
                self.employeeNumberHeader = ko.observable();
                self.employeeNameHeader = ko.observable();
                self.clearanceRequestHeader = ko.observable();
                self.expenseRequestHeader = ko.observable();

                self.displayNameLbl = ko.observable();
                self.EmailLbl = ko.observable();
                self.positionClassificationLBL = ko.observable();
                self.AcadmicYearLBL = ko.observable();
                self.SemesterLBL = ko.observable();
                self.StudentNameLBL = ko.observable();
                self.StudentDateOfBirthLBL = ko.observable();
                self.StudentGradeLBL = ko.observable();
                self.StudentSchoolNameLbl = ko.observable();
                self.StudentInvoiceNoLbl = ko.observable();
                self.noteLbl = ko.observable();
                self.StudentInvoiceDateLbl = ko.observable();
                self.AddStudentLBL = ko.observable();
                self.EditStudentLBL = ko.observable();
                self.removeStudentLBL = ko.observable();
                self.EducationExpenseLbl = ko.observable();
                self.UploadFileLbl = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.requestNumberLbl = ko.observable();
                self.departmentLbl = ko.observable();
                self.nationalityLbl=ko.observable();
                self.StudentIdLbl=ko.observable();
                self.StudentAgeLbl=ko.observable();
                self.bankNameLBL=ko.observable();
                self.ibanLbl=ko.observable();
                self.paymentMethodLBL=ko.observable();
                self.back = ko.observable();
                self.viewAttachLbl=ko.observable();
                // self.EducationExpenseRequestModle.employeeGrade(app.personDetails().grade);
                //self.EducationExpenseRequestModle.joiningDate = (app.personDetails().hireDate);
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {

                    self.cancel(getTranslation("others.cancel"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.submit(getTranslation("others.submit"));
                    self.create(getTranslation("EducationExpense.create"));
                    self.review(getTranslation("EducationExpense.review"));
                    self.displayNameLbl(getTranslation("EducationExpense.displayName"));//
                    self.EmailLbl(getTranslation("EducationExpense.Email"));//
                    self.positionClassificationLBL(getTranslation("EducationExpense.positionClassification"));//
                    self.SemesterLBL(getTranslation("EducationExpense.Semester"));//
                    self.StudentNameLBL(getTranslation("EducationExpense.StudentName"));//
                    self.AcadmicYearLBL(getTranslation("EducationExpense.AcadmicYear"));//
                    self.StudentDateOfBirthLBL(getTranslation("EducationExpense.StudentDateOfBirth"));//
                    self.StudentGradeLBL(getTranslation("EducationExpense.StudentGrade"));//
                    self.StudentSchoolNameLbl(getTranslation("EducationExpense.StudentSchoolName"));//
                    self.StudentInvoiceNoLbl(getTranslation("EducationExpense.StudentInvoiceNo"));//
                    self.noteLbl(getTranslation("EducationExpense.note"));//
                    self.StudentInvoiceDateLbl(getTranslation("EducationExpense.StudentInvoiceDate"));//
                    self.RequestDateLbl(getTranslation("EducationExpense.RequestDate"));//
                    self.AddStudentLBL(getTranslation("EducationExpense.AddStudent"));//
                    self.EditStudentLBL(getTranslation("EducationExpense.EditStudent"));//
                    self.removeStudentLBL(getTranslation("EducationExpense.removeStudent"));//
                    self.EducationExpenseLbl(getTranslation("EducationExpense.EducationExpense"));//
                    self.UploadFileLbl(getTranslation("common.UploadFileLbl"));
                    self.removeBtnLbl(getTranslation("common.removeBtnLbl"));
                    self.viewLbl(getTranslation("common.viewLbl"));
                    self.employeeNumberLbl(getTranslation("EducationExpense.employeeNumberHeader"));
                    self.joinDateLbl(getTranslation("EducationExpense.joinDateLbl"));
                    self.employeeGrade(getTranslation("EducationExpense.employeeGrade"));
                    self.EmployeeNameAndIdLbl(getTranslation("ClearanceRequest.EmployeeNameAndId"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("others.oprationMessage"));
                    self.employeeNumberHeader(getTranslation("EducationExpense.employeeNumberHeader"));
                    self.employeeNameHeader(getTranslation("EducationExpense.employeeNameHeader"));
                    self.clearanceRequestHeader(getTranslation("EducationExpense.educationExpenseRequestHeader"));
                    self.approve(getTranslation("others.approve"));
                    self.reject(getTranslation("others.reject"));
                    self.rejectMessage(getTranslation("others.rejectMSG"));
                    self.expenseRequestHeader(getTranslation("EducationExpense.educationExpenseRequestHeader"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"))
                    self.requestNumberLbl(getTranslation("EducationExpense.requestNumber"));
                    self.departmentLbl(getTranslation("EducationExpense.departmentLbl"));
                    self.nationalityLbl(getTranslation("EducationExpense.nationalityLbl"));
                    self.StudentIdLbl(getTranslation("EducationExpense.StudentIdLbl"));
                    self.StudentAgeLbl(getTranslation("EducationExpense.StudentAgeLbl"));
                    self.bankNameLBL(getTranslation("EducationExpense.bankNameLBL"));
                    self.ibanLbl(getTranslation("EducationExpense.ibanLbl"));
                    self.paymentMethodLBL(getTranslation("EducationExpense.paymentMethodlbl"));
                    self.back(getTranslation("others.back"));
                    self.StudentInvoiceAmountLbl(getTranslation("EducationExpense.StudentInvoiceAmount"));
                    self.StudentInvoicePaymentLbl(getTranslation("EducationExpense.StudentInvoicePayment"));
                    self.viewAttachLbl(getTranslation("common.download"));

                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                    self.stepArrayDetails([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);


                    self.columnArray([
                        {
                            "headerText": self.StudentNameLBL(), "field": "studentName"
                        },
                        {
                            "headerText": self.StudentDateOfBirthLBL(), "field": "studentAge"
                        },
                        {
                            "headerText": self.StudentIdLbl(), "field": "studentId"
                        },
                        {
                            "headerText": self.StudentGradeLBL(), "field": "studentGrade"
                        },
                        {
                            "headerText": self.StudentSchoolNameLbl(), "field": "studentSchoolName"
                        },
//                        {
//                            "headerText": self.StudentInvoiceNoLbl(), "field": "studentInvoiceNo"
//                        },
//                        {
//                            "headerText": self.StudentInvoiceDateLbl(), "field": "studentInvoiceDate"
//                        },
                        {
                            "headerText": self.noteLbl(), "field": "note"
                        }

                    ]);
                }
                ;
                initTranslations();
            }
            return EducationExpenseOperationViewModel;
        });
