/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function EducationExpenseSummarySummaryViewModel() {
                var self = this;



                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.AprovalListLbl = ko.observable();
                self.RequestDateLbl = ko.observable();
                self.EmployeeNameAndIdLbl = ko.observable();
                self.HireDateLbl = ko.observable();
                self.LastWorkingDayLbl = ko.observable();
                self.statusLBL = ko.observable();
                self.checkResultSearch = ko.observable();
                self.ok = ko.observable();
                self.summaryObservableArray = ko.observableArray([]);
                self.columnArray = ko.observableArray();
                self.isDisable = ko.observable(true);
                self.isDisableEdit = ko.observable(true);
                self.Approval_list = ko.observable();
                self.headerEmployeeText = ko.observable();
                self.employeeNumberHeader = ko.observable();
                self.employeeNameHeader = ko.observable();
                self.clearanceRequestHeader = ko.observable();
                self.requestDateLbl = ko.observable();
                self.displayNameLbl = ko.observable();
                self.EmailLbl = ko.observable();
                self.GradeLbl = ko.observable();
                self.positionClassificationLBL = ko.observable();
                self.AcadmicYearLBL = ko.observable();
                self.SemesterLBL = ko.observable();
                self.StudentNameLBL = ko.observable();
                self.StudentDateOfBirthLBL = ko.observable();
                self.StudentNationalIdLBL = ko.observable();
                self.StudentGradeLBL = ko.observable();
                self.educationExpenseRequestHeader = ko.observable();
                self.currentRow = ko.observable();
                self.empDepartmentLbl=ko.observable();
                self.nationalityLbl=ko.observable();
                self.HELPLbl = ko.observable();
                self.onlineHelpLbl = ko.observable();
                self.helpBody = ko.observable();
                self.disableHelpBtn=ko.observable();
                self.personDetails = ko.observableArray();
                self.checkSubmitUser=ko.observable();

                self.EmbedPDFLink = ko.observable("");
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));


                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));


                //----------------- Start add new Clearance Request-----------------//
                self.btnAdd = function () {
                    oj.Router.rootInstance.store({"operation": "ADD","checkUser": self.checkSubmitUser()})
                    oj.Router.rootInstance.go('EducationExpenseOperation');
                };
                //--------------------------------------------------------------//
                function formatDate(dateToByFormate) {
                    var date = new Date(dateToByFormate)
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [day, month, year].join('-');
                }
                ;
//                  self.ShowReport = function () {
//                    // self.status_Approval();
//                    var element = document.getElementById('table');
//                    var currentRow = element.currentRow;          
//                    if (self.summaryObservableArray()[currentRow['rowIndex']].status == "APPROVED")
//                    {
//                        var personNumber = self.summaryObservableArray()[currentRow['rowIndex']].personNumber;
//                        var requestDate = self.summaryObservableArray()[currentRow['rowIndex']].requestDate;
//                        var getReportCbFn = function (data) {
//
//                            var base64pdf = 'data:application/pdf;base64,' + data.REPORT;
//                            self.EmbedPDFLink(base64pdf);
//                            document.querySelector("#pdfViewer").open();
//                        };
//
//                        var payload = {"reportName": "Clearance Report", "P_PERSON_NUMBER":personNumber, "P_REQUEST_DATE": formatDate(requestDate)};
//                        services.getGenericPdfReport(payload).then(getReportCbFn, app.failCbFn);
//
//
//                    } else {
//                        $.notify("Not Approved ", "error");
//
//                    }
//
//                };



                //------------------End Add new Clearance Request-------------------//
                //------------------Start Edit Clearance Request-------------------//
                self.btnEdit = function () {
                    app.loading(true);
                    self.summaryObservableArray()[self.currentRow()].operation = 'EDIT';
                    self.summaryObservableArray()[self.currentRow()].checkUser=self.checkSubmitUser();
                    oj.Router.rootInstance.store(self.summaryObservableArray()[self.currentRow()]);
                    oj.Router.rootInstance.go('EducationExpenseOperation');
                };
                //------------------End Edit Clearance Request-------------------//
                //------------------Start View Clearance Request-------------------//
                self.btnView = function () {
                    app.loading(true);
                    self.summaryObservableArray()[self.currentRow()].operation = 'VIEW';
                    self.summaryObservableArray()[self.currentRow()].checkUser=self.checkSubmitUser();
                    oj.Router.rootInstance.store(self.summaryObservableArray()[self.currentRow()]);
                    oj.Router.rootInstance.go('EducationExpenseOperation');
                };
                //------------------End View Clearance Request-------------------//
                //------------------Start show aPProval list for Clearance Request--------------//
                self.datacrad = ko.observableArray([]);
                self.roleOptionType = ko.observableArray([]);
                self.positionName = ko.observable();

                self.rolrType = ko.observableArray([]);
                self.notificationType = ko.observableArray([]);
                self.responseCode = ko.observableArray([]);
                self.responseDate = ko.observableArray([]);
                self.approvalResponseArr = ko.observableArray([]);
                self.rejectReason = ko.observable();
                self.btnApprovalList = async function () {

                    $(".approval-btn").addClass("loading");
                    self.datacrad([]);
                    var eitcode = self.EitCode;

                    var getValidGradeCBF = function (data) {
                        var dataLength = data.length;
                        for (var c = 0; c < data.length; c++) {
                            if (data[c].responseCode == 'REJECTED') {
                                dataLength = c + 1;
                            }
                        }
                        if (data.length != 0) {
                            var aprrovalStatus = "PENDING";

                            for (var i = 0; i < dataLength; i++) {

                                var notificationStatus, bodyCardStatus, cardStatus, rejectReason;
                                if (data[i].notificationType === "FYA") {
                                    notificationStatus = 'app-type-a';
                                } else {
                                    cardStatus = 'badge-secondary';
                                    notificationStatus = 'app-type-i';
                                    bodyCardStatus = 'app-crd-bdy-border-ntre';
                                }
                                if (!data[i].responseCode) {
                                    cardStatus = 'badge-warning';
                                    bodyCardStatus = 'app-crd-bdy-border-pen';
                                    data[i].responseDate = '';
                                } else if (data[i].responseCode === 'APPROVED') {
                                    cardStatus = 'badge-success';
                                    bodyCardStatus = 'app-crd-bdy-border-suc';
                                } else if (data[i].responseCode === 'REJECTED') {
                                    cardStatus = 'badge-danger';
                                    bodyCardStatus = 'app-crd-bdy-border-fail';
                                } else {

                                }
                                if (data[i].rejectReason) {
                                    rejectReason = data[i].rejectReason;
                                } else {
                                    rejectReason = "There is no comment";
                                }


                                if (data[i].rolrType === "EMP") {
//                            data[i].rolrType= app.personDetails().displayName
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: self.personDetails().displayName,
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                }
                                        );
                                    } else {
                                        self.datacrad.push(
                                                {personName: self.personDetails().displayName,
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                }
                                        );
                                    }

                                } else if (data[i].rolrType == "LINE_MANAGER") {
//                            data[i].rolrType= app.personDetails().managerName;
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: self.personDetails().managerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });
                                    } else {
                                        self.datacrad.push(
                                                {personName: self.personDetails().managerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });

                                    }

                                } else if (data[i].rolrType == "LINE_MANAGER+1") {
//                               data[i].rolrType= app.globalspecialistEMP().managerOfMnagerName;
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: self.personDetails().managerOfMnagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });
                                    } else {
                                        self.datacrad.push(
                                                {personName: self.personDetails().managerOfMnagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });


                                    }

                                } else if (data[i].rolrType == "POSITION") {

//                               data[i].rolrType= app.globalspecialistEMP().managerOfMnagerName;
                                    for (var j = 0; j < app.roleOptionType().length; j++) {
                                        if (app.roleOptionType()[j].value == data[i].roleId)
                                        {
                                            self.positionName(app.roleOptionType()[j].label);
                                        }
                                    }
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });
                                    } else {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });


                                    }

                                } else if (data[i].rolrType == "ROLES") {
//                               data[i].rolrType= app.globalspecialistEMP().managerOfMnagerName;

                                    for (var j = 0; j < rootViewModel.allRoles().length; j++) {
                                        if (rootViewModel.allRoles()[j].ROLE_ID == data[i].roleId)
                                        {

                                            self.positionName(rootViewModel.allRoles()[j].ROLE_NAME);
                                        }
                                    }
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });
                                    } else {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });


                                    }

                                } else if (data[i].rolrType == "ROLES") {
//                               data[i].rolrType= app.globalspecialistEMP().managerOfMnagerName;

                                    for (var j = 0; j < rootViewModel.allRoles().length; j++) {
                                        if (rootViewModel.allRoles()[j].value == data[i].roleId)
                                        {
                                            self.positionName(rootViewModel.allRoles()[j].label);
                                        }
                                    }
                                    if (data[i].responseCode === null) {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });
                                    } else {
                                        self.datacrad.push(
                                                {personName: self.positionName(),
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                });

                                    }

                                } else if (data[i].rolrType == "JOB_LEVEL") {
//                            data[i].rolrType= app.globalspecialistEMP().managerName;
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: data[i].lineManagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                }, );
                                    } else {
                                        self.datacrad.push(
                                                {personName: data[i].lineManagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                }, );

                                    }

                                } else if (data[i].rolrType == "AOR") {
//                            data[i].rolrType= app.globalspecialistEMP().managerName;
                                    if (!data[i].responseCode) {
                                        self.datacrad.push(
                                                {personName: data[i].lineManagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: aprrovalStatus,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                }, );
                                    } else {
                                        self.datacrad.push(
                                                {personName: data[i].lineManagerName,
                                                    FYA: data[i].notificationType,
                                                    Approved: data[i].responseCode,
                                                    date: data[i].responseDate,
                                                    status: notificationStatus,
                                                    bodyStatus: bodyCardStatus,
                                                    appCardStatus: cardStatus,
                                                    rejectReason: rejectReason
                                                }, );

                                    }

                                }
                            }
                        } else
                        {
                            self.datacrad.push(
                                    {personName: 'There is no approval list to this request',
                                        FYA: '',
                                        Approved: '',
                                        date: '',
                                        status: '',
                                        bodyStatus: '',
                                        appCardStatus: ''
                                    });
                        }


                    };
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    var transactionId = self.summaryObservableArray()[currentRow['rowIndex']].id;

                    var eitcode = "XXX_EDU_EXPENSE_REQUEST";
                    await services.getGeneric("approvalList/approvaltype/" + eitcode + "/" + transactionId).then(getValidGradeCBF, app.failCbFn);
                    $(".approval-btn").removeClass("loading");
                    document.querySelector("#modalDialog1").open();
                };
                self.closeDialog = function () {
                    $("#modalDialog1").ojDialog("close");
                };

                //-----------------------get Data By person Number------------------//
                self.getSummary = function () {
                    self.summaryObservableArray([]);

                    var getReportCbFn = function (data) {
                        app.loading(false);
                        if (data.length !== 0)
                        {

                            for (var index = 0; index < data.length; index++) {
                                var obj = {};
                                obj.id = data[index].masterId;
                                obj.requestDate = data[index].requestDate;
                                obj.displayName = data[index].displayName;
                                obj.position = data[index].position;
                                obj.email = data[index].email;
                                obj.academicYear = data[index].academicYear;
                                obj.personNumber = data[index].personNumber;
                                obj.grade = self.personDetails().grade;
                                obj.semister = data[index].semister;
                                obj.requestNumber = data[index].requestNumber;
                                obj.nationality=data[index].nationality;
                                obj.department=data[index].department;
                                if (data[index].status == "Pending Approval") {
                                    obj.status = "Pending For Approval";
                                } else {
                                    obj.status = data[index].status;
                                }

                                self.summaryObservableArray.push(obj);
                            }
                        } else {
                            //  $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = "expense/getAllExpense/" + self.personDetails().personNumber;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };
                //-----------------------------------------------------------------//




                //------------------Start show aPProval list for Clearance Request----------------// 
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    self.currentRow(currentRow['rowIndex']);
                    if (self.summaryObservableArray()[self.currentRow()].status == "Approved" ||
                            self.summaryObservableArray()[self.currentRow()].status == "REJECTED") {
                        self.isDisableEdit(true);
                    } else {
                        self.isDisableEdit(false);
                    }
                    self.isDisable(false);
                };


                self.handleAttached = function () {
                    var routerCode = oj.Router.rootInstance.retrieve();
                    if (routerCode.checkUserSubmit === "ManagersubmitUser") {
                        self.personDetails(app.globalspecialistEMP());
                        self.checkSubmitUser("Specialist");

                    } else if (routerCode.returnToSymary === "backAction") {
                        if (routerCode.checkUser === "Specialist") {
                            self.personDetails(app.globalspecialistEMP());
                            self.checkSubmitUser("Specialist");
                        } else if (routerCode.checkUser === "employee") {

                            self.personDetails(rootViewModel.personDetails());
                            self.checkSubmitUser("employee");
                        }
                    } else {
                        self.personDetails(rootViewModel.personDetails());
                        self.checkSubmitUser("employee");
                    }
                    if(self.checkSubmitUser()==="Specialist"){
                      self.headerEmployeeText(self.educationExpenseRequestHeader() + " Employee Name: " + self.personDetails().displayName + " Employee Number:" + self.personDetails().personNumber);
                    }
            
                    app.loading(false);
                    self.getSummary();
                };
                self.closeHelpDialog = function () {
                    $("#onlineHelp").ojDialog("close");
                };
                self.HelpEFF = function () {


                    var getReportCbFn = function (data) {

                        if (!data || data.length == 0)
                        {
                            $.notify("No help Message found ", "error");
                            self.disableHelpBtn(true);

                            return;
                        }


                        document.querySelector("#onlineHelp").open();

                        if (app.getLocale() == 'ar') {
                            self.helpBody(data[0].descriptionAr);
                        } else {

                            self.helpBody(data[0].descriptionEn);
                        }

                    };

                    var failCbFn = function (data) {


                    }

                    var serviceName = "ManageSelfSerivce/XXX_EDU_EXPENSE_REQUEST";
                    services.getAllSummary(serviceName).then(getReportCbFn, app.failCbFn);
                };


                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }
                });
                function initTranslations() {
                    self.addLbl(getTranslation("EducationExpense.addLbl"));
                    self.editLbl(getTranslation("EducationExpense.editLbl"));
                    self.viewLbl(getTranslation("EducationExpense.viewLbl"));
                    self.AprovalListLbl(getTranslation("EducationExpense.AprovalListLbl"));
                    self.requestDateLbl(getTranslation("EducationExpense.requestDate"));//
                    self.displayNameLbl(getTranslation("EducationExpense.displayName"));
                    self.EmailLbl(getTranslation("EducationExpense.Email"));
                    self.GradeLbl(getTranslation("EducationExpense.Grade"));
                    self.positionClassificationLBL(getTranslation("EducationExpense.positionClassification"));
                    self.AcadmicYearLBL(getTranslation("EducationExpense.AcadmicYear"));
                    self.SemesterLBL(getTranslation("EducationExpense.Semester"));
                    self.StudentNameLBL(getTranslation("EducationExpense.StudentName"));
                    self.StudentDateOfBirthLBL(getTranslation("EducationExpense.StudentDateOfBirth"));
                    self.StudentNationalIdLBL(getTranslation("EducationExpense.StudentNationalId"));
                    self.StudentGradeLBL(getTranslation("EducationExpense.StudentGrade"));
                    self.educationExpenseRequestHeader(getTranslation("EducationExpense.educationExpenseRequestHeader"));
                    self.ok(getTranslation("report.ok"));
                    self.statusLBL(getTranslation("common.statusLBL"));
                    self.empDepartmentLbl(getTranslation("EducationExpense.departmentLbl"));
                    self.nationalityLbl(getTranslation("EducationExpense.nationalityLbl"));
                    self.HELPLbl(getTranslation("labels.help"));
                    self.onlineHelpLbl(getTranslation("labels.onlineHelpLbl"));


                    // column of table 
                    self.columnArray([
                        {
                            "headerText": self.statusLBL(), "field": "status"
                        },
                        {
                            "headerText": self.requestDateLbl(), "field": "requestDate"
                        },
                        {
                            "headerText": self.displayNameLbl(), "field": "displayName"
                        },
                        {
                            "headerText": self.empDepartmentLbl(), "field": "department"
                        },
                        {
                            "headerText": self.positionClassificationLBL(), "field": "position"
                        },
                        {
                            "headerText": self.nationalityLbl(), "field": "nationality"
                        },
                        {
                            "headerText": self.AcadmicYearLBL(), "field": "academicYear"
                        },
                        {
                            "headerText": self.SemesterLBL(), "field": "semister"
                        }
                      

                    ]);
                    self.headerEmployeeText(self.educationExpenseRequestHeader());
                }
                ;

                initTranslations();
            }
            return EducationExpenseSummarySummaryViewModel;
        });
