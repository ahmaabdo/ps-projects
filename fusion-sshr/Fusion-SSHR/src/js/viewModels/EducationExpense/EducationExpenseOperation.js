

/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'ojs/ojfilepicker',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojformlayout'],
        function (oj, ko, $, app, commonUtil, services, ArrayDataProvider) {
            /**
             * The view model for the main content view template
             */
            function EducationExpenseOperationViewModel() {
                var self = this;
                self.currentStepValue = ko.observable('stp1');
                self.currentStepValueDetails = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.addBtnVisibleDetails = ko.observable(false);
                self.previousBtnVisibleDetails = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.nextBtnVisibleDetails = ko.observable(true);
                self.isRequired = ko.observable();
                self.isDisabled = ko.observable(true);
                self.isDisabledDetails = ko.observable(false);
                self.isDisabledSemister = ko.observable(false);
                self.addStudentVisible = ko.observable(false);
                self.stepArray = ko.observableArray([]);
                self.stepArrayDetails = ko.observableArray([]);
                self.trainVisibility = ko.observable();
                self.trainVisibilityDetails = ko.observable(true);
                self.oprationdisabled = ko.observable(false);
                self.groupValid = ko.observable();
                self.groupValidDetails = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.approvlModel = {};
                self.SemesterArr = ko.observable();
                self.StudentNameArr = ko.observable();
                self.viewAttachmentBtnVisible = ko.observable(true);
                self.oprationVisible = ko.observable(false);
                self.oprationVisibleView = ko.observable(false);
                self.summaryObservableArray = ko.observableArray([]);
                self.operationType = ko.observable();
                self.columnArray = ko.observableArray();
                self.detailsRowId = ko.observable();
                self.koArray = ko.observableArray([]);
                self.koArrayOfAddView = ko.observableArray([]);
                self.messages = ko.observableArray();
                self.studentHeaderError = ko.observable();
                self.studentDetailError = ko.observable();
                self.dateHeaderError = ko.observable();
                self.dateDetailError = ko.observable();
                self.isDisabledAfterClick = ko.observable(false);
                self.approvalFlag = ko.observable();
                self.status = ko.observable();
                self.today = ko.observable(formatDateToday());
                self.totalAmountApproved = ko.observable();
                self.pendingErrorSummary = ko.observable();
                self.pendingErrorDetails = ko.observable();
                self.payedAmount = ko.observable();
                self.remainAmount = ko.observable();
                self.payedAmountLbl = ko.observable();
                self.remainAmountLbl = ko.observable();
                self.payedVisible = ko.observable(false);
                self.studentExist = ko.observable(false);
                self.studentObserveExist = ko.observable(false);
                self.studentExistSummaryMsg = ko.observable();
                self.studentExistDetailsMsg = ko.observable();
                self.summarylength = ko.observable();
                self.isLastApprover = ko.observable();
                self.errorMessageAttachSize = ko.observable();
                self.trsId = ko.observable();
                self.errorMessageAttachSize = ko.observable();
                self.employeeGrade = ko.observable();
                self.joinDateLbl = ko.observable();
                self.employeeNumberLbl = ko.observable();
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'}));
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray));
                self.StudentArr = ko.observableArray([]);
                self.GradeArr = ko.observableArray([
//                    {"value": "KG1", "label": "KG1"},
//                    {"value": "KG2", "label": "KG2"},
//                    {"value": "KG3", "label": "KG3"},
//                    {"value": "1st Grade", "label": "1st Grade"},
//                    {"value": "2nd Grade", "label": "2nd Grade"},
//                    {"value": "3rd Grade", "label": "3rd Grade"},
//                    {"value": "4th Grade", "label": "4th Grade"},
//                    {"value": "5th Grade", "label": "5th Grade"},
//                    {"value": "6th Grade", "label": "6th Grade"},
//                    {"value": "7th Grade", "label": "7th Grade"},
//                    {"value": "8th Grade", "label": "8th Grade"},
//                    {"value": "9th Grade", "label": "9th Grade"},
//                    {"value": "10th Grade", "label": "10th Grade"},
//                    {"value": "11th Grade", "label": "11th Grade"},
//                    {"value": "12th Grade", "label": "12th Grade"}
                ]);
                self.GradeArr(app.getPaaSLookup("eduGradeArr"));
                self.semisterArray = ko.observableArray([
//                    {"value": "semisterOne", "label": "Semister One"},
//                    {"value": "semisterTwo", "label": "Semister Two"},
//                    {"value":"fullYear","label":"Full Year"}
                ]);
                self.semisterArray(app.getPaaSLookup("eduSemisterArr"));
                self.paymentMethodArray = ko.observableArray([
//                    {"value": "Schoolaccount", "label": "School account"},
//                    {"value": "EmployeeAccount", "label": "Employee Account"}
                ]);
                self.paymentMethodArray(app.getPaaSLookup("eduPaymentMethod"));
                self.bankNameArray=ko.observableArray([]);
                var globId = 1;
                var globalRowData;
                var indexRowData;
                var lastHireDate;
                var totalAmount = 0;
                var exceedfifty=false;
                self.semisterMessage=ko.observable();
                self.previousSemister=ko.observable();
                self.attachMessage=ko.observable();
                self.personDetails = ko.observableArray();
                self.checkSubmitUser = ko.observable();
                
                self.EducationExpenseRequestModle = {
                    requestDate: ko.observable(formatDateToday()),
                    displayName: ko.observable(self.personDetails().displayName),
                    empDepartment: ko.observable(self.personDetails().departmentName),
                    positionClassification: ko.observable(self.personDetails().positionName),
                    AcadmicYear: ko.observable(new Date().getFullYear().toString() + "-" + (new Date().getFullYear() + 1).toString()),
                    Semester: ko.observable(),
                    nationality:ko.observable(self.personDetails().citizenshipLegislationCode),
                    //employeeGrade: ko.observable(app.personDetails().grade),
                    //joiningDate: ko.observable(app.personDetails().hireDate),
                    employeeNumber: ko.observable(self.personDetails().personNumber),
                    requestNumber: ko.observable()
                };
                self.EducationExpenseRequestModleDetails = {
                    StudentName: ko.observable(),
                    StudentId:ko.observable(),
                    StudentAge:ko.observable(),
                   // StudentDateOfBirth: ko.observable(),
                    StudentGrade: ko.observable(),
                    StudentSchoolName: ko.observable(),
                   // StudentInvoiceNo: ko.observable(),
                   // StudentInvoiceDate: ko.observable(),
                    note: ko.observable(),
                    bankName:ko.observable(),
                    iban:ko.observable(),
                    StudentInvoicePayment: ko.observable(),
                    StudentInvoiceAmount: ko.observable(),
                    paymentMethod:ko.observable()
                };
                self.payedVisibleDetails=ko.observable(true);
                self.totaleditwithoutcurrent=ko.observable();

                self.changeSemister = function () {                   
                    self.getTotalExpenseAmount();
                    document.querySelector("#yesNoDialog2").close();
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    if(self.retrieve.operation=="EDIT"){
                        var tot=0;
                        for(var i=0;i<self.summaryObservableArray().length;i++){
                            tot+=Number(self.summaryObservableArray()[i].invoiceAmount);
                        }
                        self.remainAmount(self.remainAmount()+tot);
                        self.payedAmount(50000-self.remainAmount());
                    }
                    self.summaryObservableArray([]);

                };
                self.cancelSemisterChange = function () {
                    self.EducationExpenseRequestModle.Semester(self.previousSemister());
                    document.querySelector("#yesNoDialog2").close();
                    document.querySelector("#yesNoDialog2").close();
                };


                //--------------method to formate date----------//
                function formatDateToday() {
                    var d = new Date(),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                ;
                //----------------Emd method  for formate Date-------------//
                self.getRequestNumber = function () {
                    var payload = {};
                    var success = function (data) {
                        self.EducationExpenseRequestModle.requestNumber(data);
                    };
                    var fail = function () {
                    };
                    services.getGeneric("expense/requestNumber").then(success, fail);
                };

                self.currentStepValueText = function () {};

                //----------- Start return to clearanceRequestSummary page----------------//
                self.cancelAction = function () {
                    app.loading(true);
                    oj.Router.rootInstance.go('EducationExpenseSummary');
                };
                //----------- End return to clearanceRequestSummary page----------------//
                self.previousStep = function () {
                    self.addBtnVisible(false);
                    self.nextBtnVisible(true);
                    self.addBtnVisible(false);
                    self.isDisabledSemister(false);
                    self.currentStepValue("stp1");
                    self.addStudentVisible(true);
                    self.oprationVisible(true);

                };
                self.nextStep = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid" && tracker.valid) {
                        tracker.showMessages();
                    } else {
                        if (self.summaryObservableArray().length > 0) {
                            self.addBtnVisible(true);
                            self.nextBtnVisible(false);
                            self.addBtnVisible(true);
                            self.isDisabledSemister(true);
                            self.currentStepValue("stp2");
                            self.addStudentVisible(false);
                            self.oprationVisible(false);
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: self.studentHeaderError(),
                                detail: self.studentDetailError()
                            });
                            self.currentStepValue("stp1");

                        }
                    }

                };
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.stopSelectListener = function () {};

                //------------------- get last hire date---------------//
                self.getLastHireDate = function () {
                    var personId = app.globalspecialistEMP().personId;
                    var getLastDateCBCF = function (data) {

                        if (data === "[]") {
                            lastHireDate = "";
                        } else {
                            lastHireDate = data;
                        }

                    };
                    services.getLastDate(personId).then(getLastDateCBCF, app.failCbFn);
                };

                //------------------------------------------------------//


                //-----------Start call method when page reload----------------//
                self.handleAttached = function () {  
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    if (self.retrieve.checkUser === "employee") {
                        self.checkSubmitUser("employee");
                        self.personDetails(app.personDetails());
                    } else if (self.retrieve.checkUser === "Specialist") {
                        self.checkSubmitUser("Specialist");
                        self.personDetails(app.globalspecialistEMP());
                    }
                    if (self.checkSubmitUser() === "Specialist") {
                        self.headerEmployeeText(self.expenseRequestHeader() + " Employee Name: " + self.personDetails().displayName + " Employee Number:" + self.personDetails().personNumber);
                    }
                    self.getApprovalList();
                    self.getDependents();
                    self.getBanks();
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    if (self.retrieve.operation == "EDIT") {
                        self.getTotalExpenseAmount();
                        self.addStudentVisible(true);
                        self.EducationExpenseRequestModle.requestDate(self.retrieve.requestDate);
                        self.EducationExpenseRequestModle.displayName(self.retrieve.displayName);
                        self.EducationExpenseRequestModle.empDepartment(self.retrieve.department);
                        self.EducationExpenseRequestModle.positionClassification(self.retrieve.position);
                        self.EducationExpenseRequestModle.AcadmicYear(self.retrieve.academicYear);
                        self.EducationExpenseRequestModle.Semester(self.retrieve.semister);
                        //self.EducationExpenseRequestModle.employeeGrade(app.personDetails().grade);
                        //self.EducationExpenseRequestModle.joiningDate(app.personDetails().hireDate);
                        self.EducationExpenseRequestModle.requestNumber(self.retrieve.requestNumber);
                        self.EducationExpenseRequestModle.employeeNumber(self.personDetails().personNumber);
                        self.EducationExpenseRequestModle.nationality(self.retrieve.nationality);

                        self.getDetails();

                    } else if (self.retrieve.operation == "VIEW") {
                        self.EducationExpenseRequestModle.requestDate(self.retrieve.requestDate);
                        self.EducationExpenseRequestModle.displayName(self.retrieve.displayName);
                        self.EducationExpenseRequestModle.empDepartment(self.retrieve.department);
                        self.EducationExpenseRequestModle.positionClassification(self.retrieve.position);
                        self.EducationExpenseRequestModle.AcadmicYear(self.retrieve.academicYear);
                        self.EducationExpenseRequestModle.Semester(self.retrieve.semister);
                       // self.EducationExpenseRequestModle.employeeGrade(app.personDetails().grade);
                        //self.EducationExpenseRequestModle.joiningDate(app.personDetails().hireDate);
                        self.EducationExpenseRequestModle.employeeNumber(self.personDetails().personNumber);
                        self.EducationExpenseRequestModle.requestNumber(self.retrieve.requestNumber);
                        self.EducationExpenseRequestModle.nationality(self.retrieve.nationality);
                        self.getDetails();
                        self.addStudentVisible(false);
                        self.oprationVisible(false);
                        self.oprationVisibleView(true);
                        self.isDisabledSemister(true);
                        self.nextBtnVisible(false);
                        self.payedVisible(false);
                        self.payedVisibleDetails(false);
                    } else {
                        self.EducationExpenseRequestModle.displayName(self.personDetails().displayName);
                        self.EducationExpenseRequestModle.empDepartment(self.personDetails().departmentName);
                        self.EducationExpenseRequestModle.positionClassification(self.personDetails().positionName);
                        self.EducationExpenseRequestModle.employeeNumber(self.personDetails().personNumber);
                        self.EducationExpenseRequestModle.nationality(self.personDetails().citizenshipLegislationCode);

                        self.getRequestNumber();
                        self.getTotalExpenseAmount();
                    }
                };
                self.getDetails = function () {
                    self.summaryObservableArray([]);

                    var getReportCbFn = function (data) {
                        self.summarylength(data.length);
                        app.loading(false);
                        if (data.length !== 0)
                        {
                            var totalAmt=0;
                            for (var index = 0; index < data.length; index++) {
                                var obj = {};
                                obj.studentName = data[index].studentName;
                                obj.studentId=data[index].studentId;
                                obj.studentAge = data[index].studentAge;
                                obj.studentGrade = data[index].studentGrade;
                                obj.studentSchoolName = data[index].studentSchool;
                                //obj.studentInvoiceNo = data[index].studentInvoiceNo;
                                //obj.studentInvoiceDate = data[index].studentInvoiceDate;
                                obj.note = data[index].note;
                                obj.id = data[index].detailID;
                                obj.masterId = data[index].masterId;
                                obj.invoiceAmount = data[index].invoiceAmount;
                                obj.invoicePayment = data[index].invoicePayment;
                                self.bankNameArray.push({"value":data[index].bankName,"label":data[index].bankName});
                                obj.bankName=data[index].bankName;
                                obj.iban=data[index].iban;
                                obj.paymentMethod=data[index].paymentMethod;
                                obj.attachment = data[index].attachment;
                                self.summaryObservableArray.push(obj);
                                totalAmt+=Number(data[index].invoiceAmount);
                            }
                            self.totaleditwithoutcurrent(totalAmt);
                        } else {
                        }



                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = "expense/getAllDetails/" + self.retrieve.id;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };
                //----------------------------------------------------------------//
                self.getApprovalList = function () {
                    var getDataFn = function (data) {
                        if (data) {
                            self.approvalFlag(true);
                            self.status("Pending Approval");
                        } else {
                            self.approvalFlag(false);
                            self.status("APPROVED");
                        }
                    };

                    services.getGeneric("expense/getExpenseApprovalSetup").then(getDataFn, self.failCbFn);
                };
                //**************** get dependent report *************
                self.getDependents = function () {
                    var payload = {"reportName": 'XXX_HR_EMP_CONTACTS', "personId": self.personDetails().personId};
                    var getDependentsSuccess = function (data) {
                        if (data != '[]') {
                            if (data.length > 0) {
                                for (var i = 0; i < data.length; i++) {
                                    self.StudentArr.push({
                                        "value": data[i].NAME,
                                        "label": data[i].NAME,
                                        "date": data[i].BIRTHDATE,
                                        "childId":data[i].CHILDID
                                    });
                                }
                            } else {
                                self.StudentArr.push({
                                    "value": data.NAME,
                                    "label": data.NAME,
                                    "date": data.BIRTHDATE,
                                    "childId":data.CHILDID
                                });
                            }
                        } else {
                            self.StudentArr([]);
                        }
                    };
                    services.getGenericReport(payload).then(getDependentsSuccess, app.failCbFn);

                };
                self.getBanks = function () {
                    if (localStorage.getItem("selectedLanguage") == 'ar') {
                        lang = "AR";
                    } else {
                        lang = "US";
                    }
                    var payload ={"reportName":"DependentReport","valueSet":"XXX_HR_DIRECT_TO","parent":"Installment Letter"};
                    var getDependentsSuccess = function (data) {
                            if (data.length > 0) {
                                for (var i = 0; i < data.length; i++) {
                                    self.bankNameArray.push({
                                        "value": data[i].value_DESCRIPTION ,
                                        "label": data[i].value_DESCRIPTION ,
                                    });
                                }
                            } else {
                                self.StudentArr.push([]);
                            }
                    };
                    services.getGenericReport(payload).then(getDependentsSuccess, app.failCbFn);

                };
                self.namechanged = function (event) {
                    for (var i = 0; i < self.StudentArr().length; i++) {
                        if (event.detail.value == self.StudentArr()[i].value) {
                            var birthdate=new Date(self.StudentArr()[i].date.split('T')[0]);
                            const getAge = birthDate => Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e+10);
                            var age=getAge(birthdate);            
                            self.EducationExpenseRequestModleDetails.StudentAge(age);
                            self.getChildId(self.StudentArr()[i].childId);
                           // self.EducationExpenseRequestModleDetails.StudentId(self.StudentArr()[i].childId);
                        }
                    }
                };
                self.getChildId=function(id){
                    var payload = {"reportName": "XXX_HR_EMP_NAT_ID", "personId":id};
                    var getDependentsSuccess = function (data) {
                        if (data!='[]') {
                            self.EducationExpenseRequestModleDetails.StudentId(data.value);
                        } else {   
                            self.EducationExpenseRequestModleDetails.StudentId("");
                        }
                    };
                    services.getGenericReport(payload).then(getDependentsSuccess, app.failCbFn);
                }
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };

                //******************************Add new student**********************
                self.AddStudent = function () {
                    self.operationType("Add");
                    self.clearDialog();
                    document.querySelector("#detailDialog").open();
                };
                self.clearDialog = function () {
                    self.previousStepDetails();
                    self.EducationExpenseRequestModleDetails.StudentName('');
                    self.EducationExpenseRequestModleDetails.StudentId('');
                    self.EducationExpenseRequestModleDetails.StudentAge(0);
                    //self.EducationExpenseRequestModleDetails.StudentDateOfBirth('');
                    self.EducationExpenseRequestModleDetails.StudentGrade('');
                    self.EducationExpenseRequestModleDetails.StudentSchoolName('');
                    //self.EducationExpenseRequestModleDetails.StudentInvoiceNo('');
                    //self.EducationExpenseRequestModleDetails.StudentInvoiceDate('');
                    self.EducationExpenseRequestModleDetails.note('');
                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);
                    self.EducationExpenseRequestModleDetails.StudentInvoiceAmount(0);
                    self.EducationExpenseRequestModleDetails.paymentMethod('');
                    self.EducationExpenseRequestModleDetails.bankName('');
                    self.EducationExpenseRequestModleDetails.iban('');
                    self.koArray([]);

                };
                //****************************Edit new student*********************
                self.EditStudent = function () {
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    var rowData = self.summaryObservableArray()[currentRow['rowIndex']];
                    indexRowData = currentRow['rowIndex'];
                    self.koArray([]);
                    var data = rowData.attachment;
                    for (var i = 0; i < data.length; i++) {
                        self.koArray.push({
                            "id": i,
                            "name": data[i].name,
                            "data": data[i].data
                        });
                    }
                    document.querySelector("#detailDialog").open();
                    self.isDisabledDetails(false);
                    self.nextBtnVisibleDetails(true);
                    self.addBtnVisibleDetails(false);
                    self.previousBtnVisibleDetails(false);
                    self.viewAttachmentBtnVisible(true);
                    self.currentStepValueDetails('stp1');
                    self.EducationExpenseRequestModleDetails.StudentName(rowData.studentName);
                    self.EducationExpenseRequestModleDetails.StudentId(rowData.studentId);
                    self.EducationExpenseRequestModleDetails.StudentAge(Number(rowData.studentAge));
                    self.EducationExpenseRequestModleDetails.StudentGrade(rowData.studentGrade);
                    self.EducationExpenseRequestModleDetails.StudentSchoolName(rowData.studentSchoolName);
//                    self.EducationExpenseRequestModleDetails.StudentInvoiceNo(rowData.studentInvoiceNo);
//                    self.EducationExpenseRequestModleDetails.StudentInvoiceDate(rowData.studentInvoiceDate);
                    self.EducationExpenseRequestModleDetails.note(rowData.note);
                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(Number(rowData.invoicePayment));
                    self.EducationExpenseRequestModleDetails.StudentInvoiceAmount(Number(rowData.invoiceAmount));
                    self.bankNameArray.push({"value":rowData.bankName,"label":rowData.bankName});
                    self.EducationExpenseRequestModleDetails.bankName(rowData.bankName);
                    self.EducationExpenseRequestModleDetails.iban(rowData.iban);
                    self.EducationExpenseRequestModleDetails.paymentMethod(rowData.paymentMethod);
                    globalRowData = rowData;
                    self.operationType("Edit");
                    self.detailsRowId(rowData.id);
                };
                //******************************View new student******************
                self.ViewStudent = function () {
                    self.koArray([]);
                    self.previousBtnVisibleDetails(false);
                    self.nextBtnVisibleDetails(false);
                    self.addBtnVisibleDetails(false);
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    if(currentRow==null){
                        $.notify("No Row Selected To View", "error");
                    }else{
                    var rowData = self.summaryObservableArray()[indexRowData];
                    var data = rowData.attachment;
                    for (var i = 0; i < data.length; i++) {
                        self.koArray.push({
                            "id": i,
                            "name": data[i].name,
                            "data": data[i].data
                        });
                    }

                    self.isDisabledDetails(true);
                    self.nextBtnVisibleDetails(false);
                    self.previousBtnVisibleDetails(false);
                    self.viewAttachmentBtnVisible(false);
                    self.trainVisibilityDetails(false);
                    var rowData = self.summaryObservableArray()[currentRow['rowIndex']];
                    self.EducationExpenseRequestModleDetails.StudentName(rowData.studentName);
                    self.EducationExpenseRequestModleDetails.StudentId(rowData.studentId);
                    self.EducationExpenseRequestModleDetails.StudentAge(Number(rowData.studentAge));
                    self.EducationExpenseRequestModleDetails.StudentGrade(rowData.studentGrade);
                    self.EducationExpenseRequestModleDetails.StudentSchoolName(rowData.studentSchoolName);
                   // self.EducationExpenseRequestModleDetails.StudentInvoiceNo(rowData.studentInvoiceNo);
                   // self.EducationExpenseRequestModleDetails.StudentInvoiceDate(rowData.studentInvoiceDate);
                    self.EducationExpenseRequestModleDetails.note(rowData.note);
                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(Number(rowData.invoicePayment));
                    self.EducationExpenseRequestModleDetails.StudentInvoiceAmount(Number(rowData.invoiceAmount));
                    self.EducationExpenseRequestModleDetails.bankName(rowData.bankName);
                    self.EducationExpenseRequestModleDetails.iban(rowData.iban);
                    self.EducationExpenseRequestModleDetails.paymentMethod(rowData.paymentMethod);
                    self.operationType("View");
                    document.querySelector("#detailDialog").open();
                }

                };
                //**********remove student *********************************
                self.RemoveStudent = function () {
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.payedAmount(self.payedAmount() - Number(self.summaryObservableArray()[indexRowData].invoiceAmount));
                    self.remainAmount(50000 - self.payedAmount());
                    self.summaryObservableArray.splice(currentRow['rowIndex'], 1);
                };
                //***********************table Listener**************************
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    indexRowData = currentRow['rowIndex'];
                    self.koArray([]);
                    if (self.retrieve.operation == "VIEW") {
                        self.oprationVisible(false);
                        self.oprationVisibleView(true);
                        self.addStudentVisible(false);
                    } else if (self.retrieve.operation == "ADD") {
                        if(self.currentStepValue() == "stp2"){
                           self.oprationVisible(false);

                        }else{
                           self.oprationVisible(true); 
                        }
                        
                        self.oprationVisibleView(true);
                        self.koArrayOfAddView(self.summaryObservableArray()[currentRow['rowIndex']].attachment);
                    } else {
                        if (self.currentStepValue() == "stp2") {
                            self.oprationVisible(false);

                        } else {
                            self.oprationVisible(true);
                        }
                        self.oprationVisibleView(true);
                    }
                };

                //********** semister selection*********
                self.onSemisterSelected = function (event) {
                    self.previousSemister(event.detail.previousValue);
                    self.addStudentVisible(true);
                    if(event.detail.updatedFrom=="internal"&&event.detail.previousValue != undefined){
                     document.querySelector("#yesNoDialog2").open(); 
                     }
//                    if (event.detail.value == "semisterOne" || event.detail.value == "semisterTwo") {
////                        var personNumber = app.personDetails().personNumber;
////                        var semister = event.detail.value;
////                        self.payedVisible(true);
////                        var getDataFn = function (data) {
////                            self.addStudentVisible(true);
////                            self.totalAmountApproved(data[0]);
////                            self.payedAmount(data[0]);
////                            var remain = 50000 - data[0];
////                            self.remainAmount(remain);
////
////                        };
////                        services.getGeneric("expense/getTotalExpenseAmount/" + personNumber).then(getDataFn, app.failCbFn);
//
//
//                    }
                };
                self.getTotalExpenseAmount = function () {
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    var personNumber = self.personDetails().personNumber;
                    self.payedVisible(true);
                    var getDataFn = function (data) {
                        self.totalAmountApproved(data[0]);
                        self.payedAmount(data[0]);
                        var remain = 50000 - data[0];
                        self.remainAmount(remain);

                    };
                    services.getGeneric("expense/getTotalExpenseAmount/" + personNumber).then(getDataFn, app.failCbFn);

                };
                self.checkStudentExistence = function () {
                    var personNumber = self.personDetails().personNumber;
                    var semister = self.EducationExpenseRequestModle.Semester();
                    var getDataFn = function (data) {
                        self.studentExist(data);
                        if (self.retrieve.operation == "ADD") {
                            if (self.operationType() == "Add") {
                                if (self.summaryObservableArray().length > 0) {
                                    for (var i = 0; i < self.summaryObservableArray().length; i++) {
                                        var first = self.summaryObservableArray()[i].studentName;
                                        var second = self.EducationExpenseRequestModleDetails.StudentName();
                                        if (first == second) {
                                            self.studentObserveExist(true);
                                            break;
                                        } else {
                                            self.studentObserveExist(false);
                                        }
                                    }
                                } else {
                                    self.studentObserveExist(false);
                                }

                            } else if (self.operationType() == "Edit") {
                                for (var i = 0; i < self.summaryObservableArray().length; i++) {
                                    if (i == indexRowData) {
                                        self.studentObserveExist(false);
                                        continue;
                                    } else {
                                        var first = self.summaryObservableArray()[i].studentName;
                                        var second = self.EducationExpenseRequestModleDetails.StudentName();
                                        if (first == second) {
                                            self.studentObserveExist(true);
                                            break;
                                        } else {
                                            self.studentObserveExist(false);
                                        }
                                    }
                                }

                            }
                        } else if (self.retrieve.operation == "EDIT") {
                            if (self.operationType() == "Add") {
                                for (var i = 0; i < self.summaryObservableArray().length; i++) {
                                    var first = self.summaryObservableArray()[i].studentName;
                                    var second = self.EducationExpenseRequestModleDetails.StudentName();
                                    if (first == second) {
                                        self.studentObserveExist(true);
                                        break;
                                    } else {
                                        self.studentObserveExist(false);
                                    }
                                }

                            } else if (self.operationType() == "Edit") {
                                for (var i = 0; i < self.summaryObservableArray().length; i++) {
                                    if (i == indexRowData) {
                                        self.studentObserveExist(false);
                                        continue;
                                    } else {
                                        var first = self.summaryObservableArray()[i].studentName;
                                        var second = self.EducationExpenseRequestModleDetails.StudentName();
                                        if (first == second) {
                                            self.studentObserveExist(true);
                                            break;
                                        } else {
                                            self.studentObserveExist(false);
                                        }
                                    }
                                }

                            }
                        }
                    };
                    if (self.operationType() == "Add") {
                        for (var i = 0; i < self.summaryObservableArray().length; i++) {
                            var first = self.summaryObservableArray()[i].studentName;
                            var second = self.EducationExpenseRequestModleDetails.StudentName();
                            if (first == second) {
                                self.studentObserveExist(true);
                                break;
                            } else {
                                self.studentObserveExist(false);
                            }
                        }
                    } else if (self.operationType() == "Edit") {
                        for (var i = 0; i < self.summaryObservableArray().length; i++) {
                            if (i == indexRowData) {
                                continue;
                            } else {
                                var first = self.summaryObservableArray()[i].studentName;
                                var second = self.EducationExpenseRequestModleDetails.StudentName();
                                if (first == second) {
                                    self.studentObserveExist(true);
                                    break;
                                } else {
                                    self.studentObserveExist(false);
                                }
                            }
                        }
                    }
                    if (self.retrieve.operation == "ADD") {
                        services.getGeneric("expense/checkStudentExistence/" + personNumber + "/" + semister + "/" + self.EducationExpenseRequestModleDetails.StudentName()).then(getDataFn, app.failCbFn);
                    } else if (self.retrieve.operation == "EDIT") {
                        services.getGeneric("expense/checkStudentExistence/Edit/" + self.retrieve.id + "/" + personNumber + "/" + semister + "/" + self.EducationExpenseRequestModleDetails.StudentName()).then(getDataFn, app.failCbFn);
                    }
                };
                self.amountentered = function (event) {
                    if (self.operationType() == "Add") {
                        totalAmount = self.totalAmountApproved();
                        if (self.retrieve.operation == "ADD") {
                            var totalOfSummaryAmount = 0;
                            if (self.summaryObservableArray().length > 0) {
                                for (var i = 0; i < self.summaryObservableArray().length; i++) {
                                    totalAmount += Number(self.summaryObservableArray()[i].invoiceAmount);
                                    totalOfSummaryAmount += Number(self.summaryObservableArray()[i].invoiceAmount);
                                }
                                var totalAfterCurrent = totalAmount + self.EducationExpenseRequestModleDetails.StudentInvoiceAmount();
                                if (totalAmount >= 50000 || totalAfterCurrent >= 50000) {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);
                                } else {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(50000-totalAfterCurrent);
                                }
                                if(totalAfterCurrent>50000){
                                    exceedfifty=true;
                                }else{
                                    exceedfifty=false;
                                }

                            } else {
                                var remainingInCover = 50000 - totalAmount;
                                if (self.EducationExpenseRequestModleDetails.StudentInvoiceAmount() >= remainingInCover) {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);
                                } else {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(remainingInCover-self.EducationExpenseRequestModleDetails.StudentInvoiceAmount());
                                }
                                if(totalAmount+self.EducationExpenseRequestModleDetails.StudentInvoiceAmount()>50000){
                                    exceedfifty=true;
                                }else{
                                    exceedfifty=false;
                                }

                            }
                        } else if (self.retrieve.operation == "EDIT") {
                            totalAmount = self.totalAmountApproved()-self.totaleditwithoutcurrent();
                            for(var k=0;k<self.summaryObservableArray().length;k++){
                                totalAmount+=Number(self.summaryObservableArray()[k].invoiceAmount);
                            }
                            if (self.summaryObservableArray().length > self.summarylength()) {
                                for (var i = self.summarylength(); i < self.summaryObservableArray().length; i++) {
                                    if (i == indexRowData) {
                                        continue;
                                    } else {
                                        totalAmount += Number(self.summaryObservableArray()[i].invoiceAmount);
                                    }
                                }
                                var totalAfterCurrent = totalAmount + self.EducationExpenseRequestModleDetails.StudentInvoiceAmount();
                                if (totalAmount >= 50000 || totalAfterCurrent >= 50000) {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);

                                } else {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(50000-totalAfterCurrent);
                                }
                                if(totalAfterCurrent>50000){
                                    exceedfifty=true;
                                }else{
                                    exceedfifty=false;
                                }
                            } else {
                                var totalAfterCurrent = totalAmount + self.EducationExpenseRequestModleDetails.StudentInvoiceAmount();
                                if (totalAmount >= 50000 || totalAfterCurrent >= 50000) {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);

                                }else {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(50000-totalAfterCurrent);
                                }
                                if(totalAfterCurrent>50000){
                                    exceedfifty=true;
                                }else{
                                    exceedfifty=false;
                                }
                            }

                        }

                    } else if (self.operationType() == "Edit") {
                        totalAmount = self.totalAmountApproved();
                        if (self.retrieve.operation == "ADD") {
                            if (self.summaryObservableArray().length > 0) {
                                for (var i = 0; i < self.summaryObservableArray().length; i++) {

                                    if (i == indexRowData) {
                                        continue;
                                    } else {
                                        totalAmount += Number(self.summaryObservableArray()[i].invoiceAmount);
                                    }
                                }
                                var totalAfterCurrent = totalAmount + self.EducationExpenseRequestModleDetails.StudentInvoiceAmount();
                                if (totalAmount >= 50000 || totalAfterCurrent >= 50000) {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);

                                }else {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(50000-totalAfterCurrent);
                                }
                                if(totalAfterCurrent>50000){
                                    exceedfifty=true;
                                }else{
                                    exceedfifty=false;
                                }

                            } else {
                                var remainingInCover = 50000 - totalAmount;
                                if (self.EducationExpenseRequestModleDetails.StudentInvoiceAmount() >= remainingInCover) {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);
                                } else {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(remainingInCover-self.EducationExpenseRequestModleDetails.StudentInvoiceAmount());
                                }
                                if(totalAmount+self.EducationExpenseRequestModleDetails.StudentInvoiceAmount>50000){
                                    exceedfifty=true;
                                }else{
                                    exceedfifty=false;
                                }
                            }
                        } else if (self.retrieve.operation == "EDIT") {
                            totalAmount = self.totalAmountApproved();
                            if (self.summaryObservableArray().length > self.summarylength()) {
                                for (var i = self.summarylength(); i < self.summaryObservableArray().length; i++) {
                                    if (i == indexRowData) {
                                        continue;
                                    } else {
                                        totalAmount += Number(self.summaryObservableArray()[i].invoiceAmount);
                                    }
                                }
                                if (indexRowData < self.summarylength()) {
                                    totalAmount = totalAmount - Number(self.summaryObservableArray()[indexRowData].invoiceAmount);

                                }

                                var totalAfterCurrent = totalAmount + self.EducationExpenseRequestModleDetails.StudentInvoiceAmount();
                                if (totalAmount >= 50000 || totalAfterCurrent >= 50000) {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);

                                }else {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(50000-totalAfterCurrent);
                                }
                                if(totalAfterCurrent>50000){
                                    exceedfifty=true;
                                }else{
                                    exceedfifty=false;
                                }
                            } else {
                                totalAmount=totalAmount-self.totaleditwithoutcurrent();
                                for(var i=0;i<self.summaryObservableArray().length;i++){
                                   totalAmount+= Number(self.summaryObservableArray()[i].invoiceAmount);
                                }
                                var totalAfterCurrent = 0;
                                if (Number(self.summaryObservableArray()[indexRowData].invoiceAmount)> self.EducationExpenseRequestModleDetails.StudentInvoiceAmount()) {
                                   totalAfterCurrent+=totalAmount;
                                    totalAfterCurrent-=Number(self.summaryObservableArray()[indexRowData].invoiceAmount);
                                    totalAfterCurrent += self.EducationExpenseRequestModleDetails.StudentInvoiceAmount();
                                } else {
                                    totalAfterCurrent = totalAmount + (self.EducationExpenseRequestModleDetails.StudentInvoiceAmount() - Number(self.summaryObservableArray()[indexRowData].invoiceAmount));
                                }
                                if (totalAmount >= 50000 || totalAfterCurrent >= 50000) {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(0);

                                } else {
                                    self.EducationExpenseRequestModleDetails.StudentInvoicePayment(50000-totalAfterCurrent);
                                }
                                if(totalAfterCurrent>50000){
                                    exceedfifty=true;
                                }else{
                                    exceedfifty=false;
                                }
                            }

                        }
                    }

                };
                self.onBankSelected=function(event){
                    
                };
                self.onPaymethodSelected=function(event){
                    if(event.detail.value=="EmployeeAccount"){
                    self.attachMessage("Please attach the voucher receipt in your request on the system and provide us with the voucher receipt soft copy.");
                    var payload = {"reportName": 'XXX_HR_IBAN',"personId":self.personDetails().personId};
                    var getDependentsSuccess = function (data) {
                        if (data!='[]') {
                            self.EducationExpenseRequestModleDetails.iban(data.value);
                        } else {
                            
                        }
                    };
                    services.getGenericReport(payload).then(getDependentsSuccess, app.failCbFn);
                        var payload2 = {"reportName": 'XXX_HR_EMP_BANK_NAME', "personId": self.personDetails().personId};
                        var getDependentsSuccess2 = function (data) {
                            if (data != '[]') {
                                self.bankNameArray.push({
                                    "value":data.BANK_NAME,
                                    "label":data.BANK_NAME
                                });
                                self.EducationExpenseRequestModleDetails.bankName(data.BANK_NAME);
                            } else {

                            }
                        };
                        services.getGenericReport(payload2).then(getDependentsSuccess2, app.failCbFn);
                    }else if(event.detail.value=="Schoolaccount"){
                        self.attachMessage("Please attach a letter from school with all the fess details plus the school bank IBAN");
                    }
                };

                //****************************Details**********************************
                self.cancelDetailDialog = function () {
                    document.querySelector("#detailDialog").close();
                };
                //*********************cancelActionDetails***************************
                self.cancelActionDetails = function () {


                };
                //***************************previousStepDetails*********************
                self.previousStepDetails = function () {
                    self.isDisabledDetails(false);
                    self.nextBtnVisibleDetails(true);
                    self.addBtnVisibleDetails(false);
                    self.previousBtnVisibleDetails(false);
                    self.viewAttachmentBtnVisible(true);
                    self.currentStepValueDetails('stp1');
                };
                //*************************nextStepDetails*******************************
                self.nextStepDetails = function () {

                    var tracker = document.getElementById("trackerDetails");
                    if (tracker.valid != "valid" && tracker.valid) {
                        tracker.showMessages();
                    } else {
                        self.checkStudentExistence();
                        if(self.EducationExpenseRequestModleDetails.StudentInvoiceAmount() > 0){
                        if (self.EducationExpenseRequestModleDetails.StudentAge() <= 18 && self.EducationExpenseRequestModleDetails.StudentAge() >= 0) {
                            if (self.koArray().length > 0) {
                                if (self.studentExist() || self.studentObserveExist()) {
                                    self.messages.push({
                                        severity: 'error',
                                        summary: self.studentExistSummaryMsg(),
                                        detail: self.studentExistDetailsMsg()
                                    });
                                } else {
                                    if (!exceedfifty) {
                                        // self.checkValidation();
                                        self.isDisabledDetails(true);
                                        self.nextBtnVisibleDetails(false);
                                        self.addBtnVisibleDetails(true);
                                        self.previousBtnVisibleDetails(true);
                                        self.currentStepValueDetails('stp2');
                                        self.viewAttachmentBtnVisible(false);
                                    } else {
                                        self.messages.push({
                                            severity: 'error',
                                            summary: self.exceedfiftySummary(),
                                            detail: self.exceedfiftyDetail()
                                        });
                                    }
                                }
                            } else {
                                self.messages.push({
                                    severity: 'error',
                                    summary: self.attachmentSummaryErrLbl(),
                                    detail: self.attachmentDetailErrLbl()
                                });
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: self.ageSummaryErrLbl(),
                                detail: self.ageDetailErrLbl()
                            });
                        }
                    }
                    else{
                        self.messages.push({
                                severity: 'error',
                                summary: "The Amount should be greater than 0 and less than Remaining amount",
                            });
                    }
                    }


                };
                //************************submitButtonDetails***************************
                self.submitButtonDetails = function () {
                    // if operation edit in student
                    if (self.operationType() == "Edit") {
                        var element = document.getElementById('table');
                        var currentRow = element.currentRow;
                        self.payedAmount((self.payedAmount() - Number(self.summaryObservableArray()[indexRowData].invoiceAmount)) + self.EducationExpenseRequestModleDetails.StudentInvoiceAmount());
                        self.remainAmount(50000 - self.payedAmount());
                        self.summaryObservableArray.splice(indexRowData, 1, {
                            studentName: self.EducationExpenseRequestModleDetails.StudentName(),
                            studentId:self.EducationExpenseRequestModleDetails.StudentId(),
                            studentAge: self.EducationExpenseRequestModleDetails.StudentAge(),
                            studentGrade: self.EducationExpenseRequestModleDetails.StudentGrade(),
                            studentSchoolName: self.EducationExpenseRequestModleDetails.StudentSchoolName(),
//                            studentInvoiceNo: self.EducationExpenseRequestModleDetails.StudentInvoiceNo(),
//                            studentInvoiceDate: self.EducationExpenseRequestModleDetails.StudentInvoiceDate(),
                            note: self.EducationExpenseRequestModleDetails.note(),
                            invoiceAmount: self.EducationExpenseRequestModleDetails.StudentInvoiceAmount(), //---invoiceAmount
                            invoicePayment: self.EducationExpenseRequestModleDetails.StudentInvoicePayment(),
                            bankName:self.EducationExpenseRequestModleDetails.bankName(),
                            iban:self.EducationExpenseRequestModleDetails.iban(),
                            paymentMethod:self.EducationExpenseRequestModleDetails.paymentMethod(),
                            attachment: self.koArray(),
                            masterId: self.summaryObservableArray()[indexRowData].masterId,
                            id: self.summaryObservableArray()[indexRowData].id

                        });
                        element.refresh();

                    } else {
                        self.payedAmount(self.payedAmount() + self.EducationExpenseRequestModleDetails.StudentInvoiceAmount());
                        self.remainAmount(50000 - self.payedAmount());
                        self.summaryObservableArray.push({
                            studentName: self.EducationExpenseRequestModleDetails.StudentName(),
                            studentId: self.EducationExpenseRequestModleDetails.StudentId(),
                            studentAge: self.EducationExpenseRequestModleDetails.StudentAge(),
                            studentGrade: self.EducationExpenseRequestModleDetails.StudentGrade(),
                            studentSchoolName: self.EducationExpenseRequestModleDetails.StudentSchoolName(),
//                            studentInvoiceNo: self.EducationExpenseRequestModleDetails.StudentInvoiceNo(),
//                            studentInvoiceDate: self.EducationExpenseRequestModleDetails.StudentInvoiceDate(),
                            note: self.EducationExpenseRequestModleDetails.note(),
                            invoiceAmount: self.EducationExpenseRequestModleDetails.StudentInvoiceAmount(), //---invoiceAmount
                            invoicePayment: self.EducationExpenseRequestModleDetails.StudentInvoicePayment(), //----invoicePayment
                            bankName: self.EducationExpenseRequestModleDetails.bankName(),
                            iban: self.EducationExpenseRequestModleDetails.iban(),
                            paymentMethod:self.EducationExpenseRequestModleDetails.paymentMethod(),
                            attachment: self.koArray(),
                            id: "0",
                            masterId: self.retrieve.id

                        });

                    }
                    document.querySelector("#detailDialog").close();

                };

                //****************************stopSelectListenerDetails*****************
                self.stopSelectListenerDetails = function (event) {
                    var train = document.getElementById('trainLine');
                    var step = event.detail.value;
                    if (step == "stp2") {
                        var tracker = document.getElementById("trackerDetails");
                        if (tracker.valid != "valid" && tracker.valid) {
                            tracker.showMessages();
                            self.currentStepValueDetails("stp1");
                        } else {
                            self.checkStudentExistence();
                            if (self.EducationExpenseRequestModleDetails.StudentAge() <= 18 && self.EducationExpenseRequestModleDetails.StudentAge() >= 0) {
                                if (self.koArray().length > 0) {
                                    if (self.studentExist() || self.studentObserveExist()) {
                                        self.currentStepValueDetails("stp1");
                                        self.messages.push({
                                            severity: 'error',
                                            summary: self.studentExistSummaryMsg(),
                                            detail: self.studentExistDetailsMsg()
                                        });
                                    } else {
                                        if (!exceedfifty) {
                                            //self.checkValidation();
                                            self.currentStepValueDetails("stp2");
                                            self.addBtnVisibleDetails(true);
                                            self.nextBtnVisibleDetails(false);
                                            self.previousBtnVisibleDetails(true);
                                            self.isDisabledDetails(true);
                                        } else {
                                            self.messages.push({
                                                severity: 'error',
                                                summary: self.exceedfiftySummary(),
                                                detail: self.exceedfiftyDetail(),
                                            });
                                            self.currentStepValueDetails("stp1");
                                        }
                                    }
                                } else {
                                    self.messages.push({
                                        severity: 'error',
                                        summary: self.attachmentSummaryErrLbl(),
                                        detail: self.attachmentDetailErrLbl()
                                    });
                                    self.currentStepValueDetails("stp1");
                                }
                            } else {
                                self.messages.push({
                                    severity: 'error',
                                    summary: self.ageSummaryErrLbl(),
                                    detail: self.ageDetailErrLbl()
                                });
                                self.currentStepValueDetails("stp1");
                            }

                        }

                    } else {
                        self.currentStepValueDetails("stp1");
                        self.isDisabledDetails(false);
                        self.addBtnVisibleDetails(false);
                        self.nextBtnVisibleDetails(true);
                        self.previousBtnVisibleDetails(false);

                    }
                };
                //-----------------attachment----------------------------//
                var dataFiles = {};
                //create file upload data provider
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.attachmentSelectListener = function (event) {
                    var files = event.detail.files;
                    for (var i = 0; i < files.length; i++) {
                        if (files.length > 0) {
                            //add the new files at the beginning of the list
                            if ((files[i].size / 1024 / 1024) > 2.5) {
                                $.notify(self.errorMessageAttachSize(), "error");

                            } else {
                                function getBase64(file) {
                                    return new Promise((resolve, reject) => {
                                        const reader = new FileReader();
                                        reader.readAsDataURL(file);
                                        reader.onload = () => resolve(reader.result);
                                        reader.onerror = error => reject(error);
                                    });
                                }
                                dataFiles.name = files[i].name;
                                dataFiles.id = i + 1;
                                getBase64(files[i]).then(function (data) {
                                    dataFiles.data = (data);
                                    self.attachmentId = self.attachmentId + 1;
                                    dataFiles.id = (self.attachmentId);
                                    self.koArray.push({
                                        "id": dataFiles.id,
                                        "name": dataFiles.name,
                                        "data": dataFiles.data
                                    });
                                }
                                );
                            }
                        }
                    }
                };
                self.removeSelectedAttachment = function (event) {
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        self.koArray.remove(function (item)
                        {
                            return (item.id == value);
                        });
                    });
                };
                function downloadURI(uri, name) {
                    var link = document.createElement("a");
                    link.setAttribute('href', uri);
                    link.setAttribute('download', name);
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                function downloadPDF(payloadAttachData) {
                    var link = document.createElement("a");
                    var data = payloadAttachData.data;
                    link.setAttribute('href', data);
                    link.setAttribute('download', payloadAttachData.name);
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }


                self.openAttachmentViewer = function ()
                {

                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        var data = self.koArray().find(e => e.id == value);
                        downloadPDF(data);

                    });
                };

                self.SubmitAdd = function () {
                    self.isDisabledAfterClick(true);
                    app.loading(true);
                    var details = [];
                    var payload = {};
                    var ExpenseRequestCbFn = function (data) {
                        self.trsId(data.masterId);
                        if (self.approvalFlag()) {
                            ApprovalOprationServiceRecord();
                        } else {
                            SubmitElementEntry();
                            oj.Router.rootInstance.go('EducationExpenseSummary');
                            app.loading(false);
                        }
                    };

                    var ExpenseUpdateSuccess = function () {
//                      ApprovalOprationServiceRecord();
                        document.querySelector("#yesNoDialog").close();
                        oj.Router.rootInstance.go('EducationExpenseSummary');
                        app.loading(false);
                    };
                    if (self.retrieve.operation == "EDIT") {
                        for (var i = 0; i < self.summaryObservableArray().length; i++) {
                            details.push({
                                "studentName": self.summaryObservableArray()[i].studentName,
                                "studentId": self.summaryObservableArray()[i].studentId,
                                "studentAge": self.summaryObservableArray()[i].studentAge,
                                "studentGrade": self.summaryObservableArray()[i].studentGrade,
                                "studentSchool": self.summaryObservableArray()[i].studentSchoolName,
//                                "studentInvoiceNo": self.summaryObservableArray()[i].studentInvoiceNo,
//                                "studentInvoiceDate": self.summaryObservableArray()[i].studentInvoiceDate,
                                "note": self.summaryObservableArray()[i].note,
                                "invoiceAmount": self.summaryObservableArray()[i].invoiceAmount,
                                "invoicePayment": self.summaryObservableArray()[i].invoicePayment,
                                "bankName":self.summaryObservableArray()[i].bankName,
                                "iban":self.summaryObservableArray()[i].iban,
                                "paymentMethod":self.summaryObservableArray()[i].paymentMethod,
                                "attachment": self.summaryObservableArray()[i].attachment,
                                "detailID": self.summaryObservableArray()[i].id,
                                "masterId": self.summaryObservableArray()[i].masterId
                            });
                        }
                        payload = {
                            "personNumber": self.personDetails().personNumber,
                            "requestDate": self.EducationExpenseRequestModle.requestDate(),
                            "displayName": self.EducationExpenseRequestModle.displayName(),
                            "position": self.EducationExpenseRequestModle.positionClassification(),
                            "department": self.EducationExpenseRequestModle.empDepartment(),
                            "academicYear": self.EducationExpenseRequestModle.AcadmicYear(),
                            "semister": self.EducationExpenseRequestModle.Semester(),
                            "studentDetails": details,
                            "requestNumber": self.EducationExpenseRequestModle.requestNumber(),
                            //"grade": self.EducationExpenseRequestModle.employeeGrade(),
                           // "joiningDate": self.EducationExpenseRequestModle.joiningDate(),
                            "nationality":self.EducationExpenseRequestModle.nationality()
                        };
                        services.addGeneric("expense/editExpense/" + self.retrieve.id, JSON.stringify(payload)).then(ExpenseUpdateSuccess, app.failCbFn);
                    } else {
                        for (var i = 0; i < self.summaryObservableArray().length; i++) {
                            details.push({
                                "studentName": self.summaryObservableArray()[i].studentName,
                                "studentId": self.summaryObservableArray()[i].studentId,
                                "studentAge": self.summaryObservableArray()[i].studentAge,
                                "studentGrade": self.summaryObservableArray()[i].studentGrade,
                                "studentSchool": self.summaryObservableArray()[i].studentSchoolName,
//                                "studentInvoiceNo": self.summaryObservableArray()[i].studentInvoiceNo,
//                                "studentInvoiceDate": self.summaryObservableArray()[i].studentInvoiceDate,
                                "note": self.summaryObservableArray()[i].note,
                                "invoiceAmount": self.summaryObservableArray()[i].invoiceAmount, //---invoiceAmount
                                "invoicePayment": self.summaryObservableArray()[i].invoicePayment, //---invoicePayment
                                "bankName":self.summaryObservableArray()[i].bankName,
                                "iban":self.summaryObservableArray()[i].iban,
                                "paymentMethod":self.summaryObservableArray()[i].paymentMethod,
                                "attachment": self.summaryObservableArray()[i].attachment
                            });
                        }
                        payload = {
                            "personNumber": self.personDetails().personNumber,
                            "requestDate": self.EducationExpenseRequestModle.requestDate(),
                            "displayName": self.EducationExpenseRequestModle.displayName(),
                            "position": self.EducationExpenseRequestModle.positionClassification(),
                            "department": self.EducationExpenseRequestModle.empDepartment(),
                            "academicYear": self.EducationExpenseRequestModle.AcadmicYear(),
                            "semister": self.EducationExpenseRequestModle.Semester(),
                            "studentDetails": details,
                            "status": self.status(),
                            "requestNumber": self.EducationExpenseRequestModle.requestNumber(),
                            "nationality":self.EducationExpenseRequestModle.nationality()
                            //"grade": self.EducationExpenseRequestModle.employeeGrade(),
                           // "joiningDate": self.EducationExpenseRequestModle.joiningDate()

                        };

                        services.addGeneric("expense/addExpense", JSON.stringify(payload)).then(ExpenseRequestCbFn, app.failCbFn);

                    }



                };
                function SubmitElementEntry() {
                    var totalAmt = 0;
                    for (var i = 0; i < self.summaryObservableArray().length; i++) {
                        totalAmt += Number(self.summaryObservableArray()[i].invoicePayment);
                    }
                    var date = new Date();
                    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getDate().toString();
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate().toString();
                    var month = (date.getMonth() + 1).toString();
                    var year = date.getFullYear();
                    if (month.length < 2)
                        month = '0' + month;
                    if (firstDay.length < 2)
                        firstDay = '0' + firstDay;
                    if (lastDay.length < 2)
                        lastDay = '0' + lastDay;
                    var effectiveStartDate = [year, month, firstDay].join('/');
                    var effectiveEndDate = [year, month, lastDay].join('/');
                    var payloadElemntEntry = {
                        "entryType": "E",
                        "creatorType": "H",
                        "assignmentNumber": "E" + self.personDetails().personNumber,
                        "legislativeDataGroupName": "SA Legislative Data Group",
                        "effectiveStartDate": effectiveStartDate,
                        "effectiveEndDate": effectiveEndDate,
                        "sourceSystemOwner": "PAAS",
                        "SourceSystemId": "EDU_ALW_12_1_2020",
                        "amount": totalAmt,
                        "childrenNumber": (self.summaryObservableArray().length).toString(),
                        "personNumber": self.personDetails().personNumber,
                        "requestDetailsId": self.trsId()
                    };
                    var createEducationAllowanceMethodCBFN = function (data) {

                    };
                   // services.submitHDLFile("EducationAllowancePlanFile", JSON.stringify(payloadElemntEntry)).then(createEducationAllowanceMethodCBFN, app.failCbFn);
                }

                function ApprovalOprationServiceRecord() {

                    self.approvlModel.type = "XXX_EDU_EXPENSE_REQUEST";
                    self.approvlModel.personNumber = self.personDetails().personNumber;
                    self.approvlModel.personName = self.personDetails().displayName;
                    self.approvlModel.presonId = self.personDetails().personId;
                    self.approvlModel.managerId = self.personDetails().managerId;
                    self.approvlModel.managerName = self.personDetails().managerName;//
                    self.approvlModel.managerOfManager = self.personDetails().managerOfManager;
                    self.approvlModel.managerOfMnagerName = self.personDetails().managerOfMnagerName;//
                    self.approvlModel.created_by = self.personDetails().personId;
                    self.approvlModel.creation_date = self.personDetails().personId;
                    var jsonData = ko.toJSON(self.approvlModel);
                    var getValidGradeCBF = function (data1) {
                        document.querySelector("#yesNoDialog").close();
                        oj.Router.rootInstance.go('EducationExpenseSummary');
                        app.loading(false);
                        self.isLastApprover(app.isLastApprover(self.trsId(), "XXX_EDU_EXPENSE_REQUEST", false));
                        if (self.isLastApprover()) {
                            SubmitElementEntry();
                        }
                    };

                    services.addGenericAsync("expense/approval", jsonData).then(getValidGradeCBF(), app.failCbFn);
                }
                
                function buildBackObject() {
                    var cancelBack = {
                        EIT: "XXX_EDU_EXPENSE_REQUEST",
                        returnToSymary: "backAction",
                        checkUser: self.checkSubmitUser()
                    };
                    oj.Router.rootInstance.store(cancelBack);
                }
                self.handleDetached = function (info) {
                    buildBackObject();
                };

                //    define 
                self.cancel = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.submit = ko.observable();
                self.RequestDateLbl = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.EmployeeNameAndIdLbl = ko.observable();
                self.PositionLbl = ko.observable();
                self.DepartmentLbl = ko.observable();
                self.HireDateLbl = ko.observable();
                self.LastWorkingDayLbl = ko.observable();
                self.headerEmployeeText = ko.observable();
                self.employeeNumberHeader = ko.observable();
                self.employeeNameHeader = ko.observable();
                self.clearanceRequestHeader = ko.observable();
                self.expenseRequestHeader = ko.observable();

                self.displayNameLbl = ko.observable();
                self.departmentLbl = ko.observable();
                self.positionClassificationLBL = ko.observable();
                self.AcadmicYearLBL = ko.observable();
                self.SemesterLBL = ko.observable();
                self.StudentNameLBL = ko.observable();
                self.StudentDateOfBirthLBL = ko.observable();
                self.StudentGradeLBL = ko.observable();
                self.StudentSchoolNameLbl = ko.observable();
                self.StudentInvoiceNoLbl = ko.observable();
                self.noteLbl = ko.observable();
                self.StudentInvoiceDateLbl = ko.observable();
                self.AddStudentLBL = ko.observable();
                self.EditStudentLBL = ko.observable();
                self.removeStudentLBL = ko.observable();
                self.EducationExpenseLbl = ko.observable();
                self.UploadFileLbl = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.StudentInvoiceAmountLbl = ko.observable();
                self.StudentInvoicePaymentLbl = ko.observable();
                self.requestNumberLbl = ko.observable();
                self.nationalityLbl=ko.observable();
                self.StudentIdLbl=ko.observable();
                self.StudentAgeLbl=ko.observable();
                self.bankNameLBL=ko.observable();
                self.ibanLbl=ko.observable();
                self.paymentMethodLBL=ko.observable();
                self.attachmentSummaryErrLbl=ko.observable();
                self.attachmentDetailErrLbl=ko.observable();
                self.ageSummaryErrLbl=ko.observable();
                self.ageDetailErrLbl=ko.observable();
                self.exceedfiftySummary=ko.observable();
                self.exceedfiftyDetail=ko.observable();
                self.viewAttachLbl=ko.observable();
                self.placeHolder=ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                function initTranslations() {

                    self.cancel(getTranslation("others.cancel"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.submit(getTranslation("others.submit"));
                    self.create(getTranslation("EducationExpense.create"));
                    self.review(getTranslation("EducationExpense.review"));
                    self.displayNameLbl(getTranslation("EducationExpense.displayName"));//
                    self.departmentLbl(getTranslation("EducationExpense.departmentLbl"));//
                    self.positionClassificationLBL(getTranslation("EducationExpense.positionClassification"));//
                    self.SemesterLBL(getTranslation("EducationExpense.Semester"));//
                    self.StudentNameLBL(getTranslation("EducationExpense.StudentName"));//
                    self.AcadmicYearLBL(getTranslation("EducationExpense.AcadmicYear"));//
                    self.StudentDateOfBirthLBL(getTranslation("EducationExpense.StudentDateOfBirth"));//
                    self.StudentGradeLBL(getTranslation("EducationExpense.StudentGrade"));//
                    self.StudentSchoolNameLbl(getTranslation("EducationExpense.StudentSchoolName"));//
                    self.StudentInvoiceNoLbl(getTranslation("EducationExpense.StudentInvoiceNo"));//
                    self.noteLbl(getTranslation("EducationExpense.note"));//
                    self.StudentInvoiceDateLbl(getTranslation("EducationExpense.StudentInvoiceDate"));//
                    self.RequestDateLbl(getTranslation("EducationExpense.RequestDate"));//
                    self.AddStudentLBL(getTranslation("EducationExpense.AddStudent"));//
                    self.EditStudentLBL(getTranslation("EducationExpense.EditStudent"));//
                    self.removeStudentLBL(getTranslation("EducationExpense.removeStudent"));//
                    self.EducationExpenseLbl(getTranslation("EducationExpense.EducationExpense"));//
                    self.UploadFileLbl(getTranslation("common.UploadFileLbl"));
                    self.removeBtnLbl(getTranslation("common.removeBtnLbl"));
                    self.viewLbl(getTranslation("common.viewLbl"));
                    self.studentHeaderError(getTranslation("EducationExpense.studentHeaderError"));
                    self.studentDetailError(getTranslation("EducationExpense.studentDetailError"));
                    self.dateHeaderError(getTranslation("EducationExpense.dateHeaderError"));
                    self.dateDetailError(getTranslation("EducationExpense.dateDetailError"));
                    self.StudentInvoiceAmountLbl(getTranslation("EducationExpense.StudentInvoiceAmount"));
                    self.StudentInvoicePaymentLbl(getTranslation("EducationExpense.StudentInvoicePayment"));
                    self.payedAmountLbl(getTranslation("EducationExpense.payedAmountLbl"));
                    self.remainAmountLbl(getTranslation("EducationExpense.remainAmountLbl"));
                    self.studentExistSummaryMsg(getTranslation("EducationExpense.studentExistSummary"));
                    self.studentExistDetailsMsg(getTranslation("EducationExpense.studentExistDetails"));
                    self.employeeNumberLbl(getTranslation("EducationExpense.employeeIdLbl"));
                    self.joinDateLbl(getTranslation("EducationExpense.joinDateLbl"));
                    self.employeeGrade(getTranslation("EducationExpense.employeeGrade"));
                    self.EmployeeNameAndIdLbl(getTranslation("EducationExpense.EmployeeNameAndId"));
                    self.PositionLbl(getTranslation("ClearanceRequest.Position"));
                    self.DepartmentLbl(getTranslation("ClearanceRequest.Department"));
                    self.HireDateLbl(getTranslation("ClearanceRequest.HireDate"));
                    self.LastWorkingDayLbl(getTranslation("ClearanceRequest.LastWorkingDay"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("EducationExpense.oprationMessage"));
                    self.employeeNumberHeader(getTranslation("EducationExpense.employeeNumberHeader"));
                    self.employeeNameHeader(getTranslation("EducationExpense.employeeNameHeader"));
                    self.clearanceRequestHeader(getTranslation("EducationExpense.educationExpenseRequestHeader"));
                    self.expenseRequestHeader(getTranslation("EducationExpense.educationExpenseRequestHeader"));
                    self.pendingErrorSummary(getTranslation("EducationExpense.pendingErrorSummary"));
                    self.pendingErrorDetails(getTranslation("EducationExpense.pendingErrorDetails"));
                    self.errorMessageAttachSize(getTranslation("others.errorMessageAttachSize"));
                    self.requestNumberLbl(getTranslation("EducationExpense.requestNumber"));
                    self.nationalityLbl(getTranslation("EducationExpense.nationalityLbl"));
                    self.StudentIdLbl(getTranslation("EducationExpense.StudentIdLbl"));
                    self.StudentAgeLbl(getTranslation("EducationExpense.StudentAgeLbl"));
                    self.bankNameLBL(getTranslation("EducationExpense.bankNameLBL"));
                    self.ibanLbl(getTranslation("EducationExpense.ibanLbl"));
                    self.paymentMethodLBL(getTranslation("EducationExpense.paymentMethodlbl"));
                    self.attachmentSummaryErrLbl(getTranslation("EducationExpense.attachmentSummaryErrLbl"));
                    self.attachmentDetailErrLbl(getTranslation("EducationExpense.attachmentDetailErrLbl"));
                    self.ageSummaryErrLbl(getTranslation("EducationExpense.ageSummaryErrLbl"));
                    self.ageDetailErrLbl(getTranslation("EducationExpense.ageDetailErrLbl"));
                    self.exceedfiftySummary(getTranslation("EducationExpense.exceedfiftySummary"));
                    self.exceedfiftyDetail(getTranslation("EducationExpense.exceedfiftyDetail"));
                    self.semisterMessage(getTranslation("EducationExpense.semisterchange"));
                    self.viewAttachLbl(getTranslation("common.download"));
                    self.placeHolder(getTranslation("EducationExpense.placeHolder"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                    self.stepArrayDetails([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);

                    self.headerEmployeeText(self.expenseRequestHeader());

                    self.columnArray([
                        {
                            "headerText": self.StudentNameLBL(), "field": "studentName"
                        },
                        {
                            "headerText": self.StudentDateOfBirthLBL(), "field": "studentAge"
                        },
                        {
                            "headerText": self.StudentIdLbl(), "field": "studentId"
                        },
                        {
                            "headerText": self.StudentGradeLBL(), "field": "studentGrade"
                        },
                        {
                            "headerText": self.StudentSchoolNameLbl(), "field": "studentSchoolName"
                        },
//                        {
//                            "headerText": self.StudentInvoiceNoLbl(), "field": "studentInvoiceNo"
//                        },
//                        {
//                            "headerText": self.StudentInvoiceDateLbl(), "field": "studentInvoiceDate"
//                        },
                        {
                            "headerText": self.noteLbl(), "field": "note"
                        }

                    ]);
                }
                ;
                initTranslations();
            }
            return EducationExpenseOperationViewModel;
        });
