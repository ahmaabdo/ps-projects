/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'ojs/ojtable',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.columnArray = ko.observableArray();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.lookupNameLbl = ko.observable();
                self.lookupcode1Lbl = ko.observable();
                self.lookupArabicVallLbl = ko.observable();
                self.lookupEnglishValLbl = ko.observable();
                self.keyWordSearch = ko.observable();
                self.searchval = ko.observable();
                self.Search = ko.observable();
                self.deleteLbl = ko.observable();
                self.RestLbl = ko.observable();
                self.searchLable = ko.observable();
                self.checkResultSearch = ko.observable();
                self.backActionLbl = ko.observable();
                self.clearLbl = ko.observable();
                self.dataSource = ko.observable();
                self.deleteBtnLbl = ko.observable();
                self.filter = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessageDelete = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.searchPlaceHolder = ko.observable();
                self.passLookupMessage = ko.observable();
                self.messages = ko.observableArray();
                self.isDisable = ko.observable(true);
                self.messageDeleteSuccess = ko.observable();

                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));


                self.btnAdd = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('lookup');
                };
                self.btnEdit = function () {
                    app.loading(true);
                    var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].id[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var payload = {
                        clickType: 'Edit',
                        data: self.summaryObservableArray()[myRowIndex]
                    };

                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('lookup');

                };
                self.btnView = function () {
                    app.loading(true);
                    var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].id[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var payload = {
                        clickType: 'view',
                        data: self.summaryObservableArray()[myRowIndex]
                    };

                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('lookup');

                };
                var selectedRowKey;
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    selectedRowKey = data.currentRow.rowKey[0];
                    var currentRow = data.currentRow;
                    if (currentRow) {
                        self.isDisable(false);
                    }
                };

                self.getSummary = function () {

                    var getReportCbFn = function (data) {
                        if (data.length !== 0) {
                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    id: [data[index].id],
                                    name: [data[index].name],
                                    code: [data[index].code],
                                    arabicVal: [data[index].valueArabic],
                                    englishVal: [data[index].valueEnglish]

                                });
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getAllLookup;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };



                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };


                self.handleAttached = function () {
                    app.loading(false);
                    self.getSummary();
                };


                //-------------------- filter data function----------------- 
                self.handleValueChanged = function () {
                    var deptArray = [];
                    self.tempSummaryObservableArray = ko.observableArray([]);
                    var filter = document.getElementById('filter').rawValue;
                    if (typeof filter !== 'undefined' && filter !== null) {
                        for (var i = self.summaryObservableArray().length - 1; i >= 0; i--) {
                            Object.keys(self.summaryObservableArray()[i]).forEach(function (e) {
                                if (typeof self.summaryObservableArray()[i].code !== 'undefined') {
                                    if (self.summaryObservableArray()[i].code.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0 || self.summaryObservableArray()[i].name.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0 || self.summaryObservableArray()[i].arabicVal.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0 || self.summaryObservableArray()[i].englishVal.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0) {
                                        if (deptArray.indexOf(self.summaryObservableArray()[i]) < 0) {
                                            deptArray.push(self.summaryObservableArray()[i]);
                                            self.tempSummaryObservableArray = deptArray;
                                            self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tempSummaryObservableArray, {idAttribute: 'id'})));
                                        }
                                    }
                                }
                            });
                        }
                    }
                };



                //------Reset Function--------------------
                self.reset = function () {
                    self.searchval('');
                    self.isDisable(true);
                };
                //-------------- Delete popup action--------
                self.deleteBtnAction = function () {
                    document.getElementById('deleteLookupDialog').open();
                };

                //----------------delete Method----------------------
                self.deleteRecordAction = function () {
                    var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].id[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var id = self.summaryObservableArray()[myRowIndex].id;
                    var deleteLookupRecordCbFn = function () {
                        self.cancelDeleteAction();
                        self.messages.push({severity: 'confirmation', summary: self.messageDeleteSuccess(), autoTimeout: 0});
                        self.reset();
                        self.summaryObservableArray([]);
                        self.getSummary();
                    };
                    var serviceName = commonUtil.deleteLookup;
                    services.getGeneric(serviceName + '/' + id, {}).then(deleteLookupRecordCbFn, app.failCbFn);
                };


                //---------close popup------------------
                self.cancelDeleteAction = function () {
                    document.getElementById('deleteLookupDialog').close();

                };
                //--------------- define messages notification ---------------//

                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };

                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };
                //------------ translation part---------------------

                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.addLbl(getTranslation("lookUp.add"));
                    self.editLbl(getTranslation("lookUp.edit"));
                    self.viewLbl(getTranslation("lookUp.view"));
                    self.deleteBtnLbl(getTranslation("lookUp.deleteBtnLbl"));
                    self.lookupNameLbl(getTranslation("lookUp.name"));
                    self.lookupcode1Lbl(getTranslation("lookUp.code"));
                    self.lookupArabicVallLbl(getTranslation("lookUp.arabicVal"));
                    self.lookupEnglishValLbl(getTranslation("lookUp.englishVal"));
                    self.keyWordSearch(getTranslation("lookUp.keyWordSearch"));
                    self.Search(getTranslation("lookUp.Search"));
                    self.deleteLbl(getTranslation("lookUp.deleteLbl"));
                    self.RestLbl(getTranslation("lookUp.RestLbl"));
                    self.searchLable(getTranslation("lookUp.searchLable"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.confirmMessage(getTranslation("lookUp.confirmMessage"));
                    self.oprationMessageDelete(getTranslation("lookUp.oprationMessageDelete"));
                    self.yes(getTranslation("lookUp.yes"));
                    self.no(getTranslation("lookUp.no"));
                    self.clearLbl(getTranslation("lookUp.clearLbl"));
                    self.searchPlaceHolder(getTranslation("lookUp.searchPlaceHolder"));
                    self.messageDeleteSuccess(getTranslation("lookUp.messageDeleteSuccess"));

                    self.columnArray([
                        {
                            "headerText": self.lookupNameLbl(), "field": "name"
                        },
                        {
                            "headerText": self.lookupcode1Lbl(), "field": "code"
                        },
                        {
                            "headerText": self.lookupArabicVallLbl(), "field": "arabicVal"
                        },
                        {
                            "headerText": self.lookupEnglishValLbl(), "field": "englishVal"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
