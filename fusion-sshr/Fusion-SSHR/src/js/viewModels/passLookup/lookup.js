/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var operationType;


                self.lookupTitleLabl = ko.observable();
                self.lookupName = ko.observable();
                self.lookupcode = ko.observable();
                self.valueArabic = ko.observable();
                self.valueEnglish = ko.observable();
                self.lookupNameVal = ko.observable();
                self.lookupcodeVal = ko.observable();
                self.valueArabicVal = ko.observable();
                self.valueEnglishVal = ko.observable();

                self.cancel = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.submit = ko.observable();
                self.trainView = ko.observable();
                self.createlookup = ko.observable();
                self.errormessagecheckInDatabase = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.oprationMessageUpdate = ko.observable();
                self.groupValid = ko.observable();
                self.checkEquel = ko.observable();

                self.disableSubmit = ko.observable(false);

                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.errormessage = ko.observable();
                self.validationMessage = ko.observable();
                self.trainVisible = ko.observable(true); 


                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                  
                  self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.nextStep();
                    } else {
                        self.hidenext(true);
                        self.hidesubmit(false);
                        self.hidepervious(false);
                        $(".disable-element").prop('disabled', false);
                    }
                };


                self.previousStep = function () {
                    self.hidecancel(true);
                    self.hidepervious(false);
                    self.hidesubmit(false);
                    self.hideUpdatesubmit(false);
                    self.hidenext(true);
                    $(".disable-element").prop('disabled', false);
                    self.currentStepValue('stp1');

                };



                self.nextStep = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid === "valid") {
                        if (self.lookupcodeVal() !== self.lookupNameVal()) {
                            self.hidenext(false);
                            self.hidesubmit(true);
                            self.hidepervious(true);
                            $(".disable-element").prop('disabled', true);
                            if (oj.Router.rootInstance.retrieve() === 'ADD') {
                                self.getLookupCode();
                            } else if (oj.Router.rootInstance.retrieve().clickType === 'Edit') {
                                self.getLookupCodeToCheckUpdate();
                            }
                        } else{
                            self.currentStepValue('stp1');
                            $.notify(self.checkEquel(), "error");
                        }
                    } else {
                        tracker.showMessages();
                    };
                };

                self.disableInput = function () {
                    self.hidecancel(true);
                    self.hidepervious(true);
                    self.hidenext(false);

                    $(".disable-element").prop('disabled', true);
                    self.currentStepValue('stp2');
                };

                self.cancelAction = function () {

                    oj.Router.rootInstance.go('getPassLookup');

                };


                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();

                };
                self.commitRecordupdate = function () {
                    operationType = 'EDIT';
                    self.callLookup(operationType);
                };
                self.cancelButtonupdate = function () {
                    document.querySelector("#yesNoDialogUpdate").close();

                };
                self.submitUpdateButton = function () {
                    document.querySelector("#yesNoDialogUpdate").open();
                };

                self.getLookupCode = function () {
                    var code;
                    var name;
                    var getLookupCbFn = function (data) {
                        code = data;
                    };
                    var failCbFn = function () {
                    };

                    var namegetLookupCbFn = function (data) {
                        name = data;
                        if (code === "" || name === "")
                        {
                            self.disableInput();
                            self.hidesubmit(true);
                        } else
                        {
                            $.notify(self.errormessagecheckInDatabase(), "error")
                        }
                    };
                    var namefailCbFn = function () {
                    };

                    var serviceCode = commonUtil.lookupCode + self.lookupcodeVal();
                    var serviceName = commonUtil.lookupNmae + self.lookupNameVal();
                    services.getLookUpCode(serviceCode).then(getLookupCbFn, failCbFn);
                    services.getLookUpName(serviceName).then(namegetLookupCbFn, namefailCbFn);
                };


                self.getLookupCodeToCheckUpdate = function () {
//                    var name;
//                    var code;
//
//                    var getLookupCodeCbFn = function (data) {
//                        code = data;
//
//                    };
//                    var failCbFn = function () {
//                    };
//
//                    var namegetLookupCbFn = function (data) {
//                        name = data;
//                        if (code === "" || name === "")
//                        {
//                            self.disableInput();
//                            self.hideUpdatesubmit(true);
//                        } else
//                        {
//                            $.notify(self.errormessagecheckInDatabase(), "error");
//                        }
//                    };
//                    var namefailCbFn = function () {
//                    };

//                    var serviceCode = commonUtil.lookupCode + oj.Router.rootInstance.retrieve().data.code[0] + "/" + self.lookupcodeVal();
                    var namegetLookupCbFn = function (data) {
                        if (data === "") {
                            self.disableInput();
                            self.hideUpdatesubmit(true);

                        } else {
                            $.notify(self.errormessagecheckInDatabase(), "error");
                        }
                    };
                    var namefailCbFn = function () {
                    };

                    var servicename = commonUtil.lookupNmae + self.lookupNameVal() + "/" + self.lookupcodeVal() + "/" + oj.Router.rootInstance.retrieve().data.id[0];
//                      services.getLookUpCode(serviceCode).then(getLookupCodeCbFn, failCbFn);
                    services.getLookUpName(servicename).then(namegetLookupCbFn, namefailCbFn);
                };

                self.restInput = function () {
                    self.reportNameVal("");
                    self.Parameter1Val("");
                    self.Parameter2Val("");
                    self.Parameter3Val("");
                    self.Parameter4Val("");
                    self.Parameter5Val("");
                };



                self.callLookup = function (operationType) {
                    var reportSummaryId;
                    if (operationType === 'ADD')
                    {
                        reportSummaryId = 0;
                    } else if (operationType === 'EDIT')
                    {
                        reportSummaryId = oj.Router.rootInstance.retrieve().data.id[0];
                    }

                    var payload = {

                        "name": self.lookupNameVal(),
                        "code": self.lookupcodeVal(),
                        "valueArabic": self.valueArabicVal(),
                        "valueEnglish": self.valueEnglishVal(),
                        "id": reportSummaryId


                    };

                    var createReportCbFn = function (data) {
                        // oj.Router.rootInstance.go('getSummary');
                        if (operationType === 'ADD') {
                            document.querySelector("#yesNoDialog").close();
                        } else if (operationType === 'EDIT')
                        {
                            document.querySelector("#yesNoDialogUpdate").close();
                        }
                        oj.Router.rootInstance.go('getPassLookup');

                    };
                    var failCbFn = function () {
                    };

                    services.addReport(commonUtil.addOrUpdateLookup + operationType, JSON.stringify(payload)).then(createReportCbFn, failCbFn);
                };


                self.commitRecord = function () {
                    operationType = 'ADD';
                    self.callLookup(operationType);
                };


                self.cancelButton = function () {

                    document.querySelector("#yesNoDialog").close();
                };

                self.commitDraft = function (data, event) {
                    return true;
                };
                self.stopSelectListener = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };

                self.handleAttached = function (info) {
                    app.loading(false);

                    if (oj.Router.rootInstance.retrieve().clickType === 'view')
                    {

                        self.lookupNameVal(oj.Router.rootInstance.retrieve().data.name[0]);
                        self.lookupcodeVal(oj.Router.rootInstance.retrieve().data.code[0]);
                        self.valueArabicVal(oj.Router.rootInstance.retrieve().data.arabicVal[0]);
                        self.valueEnglishVal(oj.Router.rootInstance.retrieve().data.englishVal[0]);

                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(false);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', true);
                        self.currentStepValue('stp2');
                        self.trainVisible(false);

                    } else if (oj.Router.rootInstance.retrieve().clickType === 'Edit') {

                        self.lookupNameVal(oj.Router.rootInstance.retrieve().data.name[0]);
                        self.lookupcodeVal(oj.Router.rootInstance.retrieve().data.code[0]);
                        self.valueArabicVal(oj.Router.rootInstance.retrieve().data.arabicVal[0]);
                        self.valueEnglishVal(oj.Router.rootInstance.retrieve().data.englishVal[0]);

                    }

                };

                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                function initTranslations() {
                    if (oj.Router.rootInstance.retrieve() === 'ADD') {
                        self.lookupTitleLabl(getTranslation("lookUp.add"));

                    } else if (oj.Router.rootInstance.retrieve().clickType === 'view') {
                        self.lookupTitleLabl(getTranslation("lookUp.view"));
                    } else if (oj.Router.rootInstance.retrieve().clickType === 'Edit') {
                        self.lookupTitleLabl(getTranslation("lookUp.edit"));
                    }
//                    self.lookupTitleLabl(getTranslation("lookUp.lookupTitle"));
                    self.lookupName(getTranslation("lookUp.lookupName"));
                    self.lookupcode(getTranslation("lookUp.lookupcode"));
                    self.valueArabic(getTranslation("lookUp.valueArabic"));
                    self.valueEnglish(getTranslation("lookUp.valueEnglish"));
                    self.cancel(getTranslation("lookUp.cancel"));
                    self.pervious(getTranslation("lookUp.pervious"));
                    self.next(getTranslation("lookUp.next"));
                    self.submit(getTranslation("lookUp.submit"));
                    self.createlookup(getTranslation("lookUp.createlookup"));
                    self.trainView(getTranslation("lookUp.trainView"));
                    self.errormessagecheckInDatabase(getTranslation("lookUp.errormessagecheckInDatabase"));
                    self.confirmMessage(getTranslation("lookUp.confirmMessage"));
                    self.oprationMessage(getTranslation("lookUp.oprationMessage "));
                    self.yes(getTranslation("lookUp.yes"));
                    self.no(getTranslation("lookUp.no"));
                    self.oprationMessageUpdate(getTranslation("lookUp.oprationMessageUpdate"));
                    self.checkEquel(getTranslation("lookUp.checkEquel"));



                    self.stepArray([{label: self.createlookup(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
