/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Individual Development Plan Summary module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage',
    'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojtable'],
    function (oj, ko, $, app, services, commonhelper) {

        function individualDevelopmentPlanSummaryViewModel() {
            var self = this;
            var getTranslation = oj.Translations.getTranslatedString;
            var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
            self.labels = ko.observable();
            self.oprationdisabled = ko.observable(true);
            self.arrayTableData = ko.observableArray([]);
            self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.arrayTableData, { idAttribute: 'headerId' })));
            self.datacrad = ko.observableArray([]);
            self.tableSelectedRowKey = ko.observable();
            self.tableSelectedIndex = ko.observable();
            self.selectedDataTable = ko.observable();
            self.personDetails = ko.observableArray();
            self.checkSubmitUser=ko.observable();
            //----------------- for language change ----------------------//
            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });
            //------------------------------------------//
            //--------------- define messages notification ---------------//
            self.messages = ko.observableArray();
            self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
            self.position =
            {
                "my": { "vertical": "top", "horizontal": "end" },
                "at": { "vertical": "top", "horizontal": "end" },
                "of": "window"
            };
            self.closeMessageHandler = function (event) {
                self.messages.remove(event.detail.message);
            };
            //------------------------------------------------------------//

            self.handleAttached = function () {
                app.loading(false);
                    var routerCode = oj.Router.rootInstance.retrieve();
                    if (routerCode.checkUserSubmit === "ManagersubmitUser") {
                        self.personDetails(app.globalspecialistEMP());
                        self.checkSubmitUser("Specialist");

                    } else if (routerCode.returnToSymary === "backAction") {
                        if (routerCode.checkUser === "Specialist") {
                            self.personDetails(app.globalspecialistEMP());
                            self.checkSubmitUser("Specialist");
                        } else if (routerCode.checkUser === "employee") {

                            self.personDetails(rootViewModel.personDetails());
                            self.checkSubmitUser("employee");
                        }
                    } else {
                        self.personDetails(rootViewModel.personDetails());
                        self.checkSubmitUser("employee");
                    }
                    if(self.checkSubmitUser()==="Specialist"){
                    self.labels().pageTitle(self.labels().pageTitle()+" Employee Name: "+self.personDetails().displayName+" Employee Number:  "+self.personDetails().personNumber);
                    }

                var success = function (data) {
                    app.loading(false);
                    data.forEach(function (x) {
                        document.documentElement.lang === 'ar' ? x.statusCurrent = x.statusAr : x.statusCurrent = x.statusEn
                    });
                    self.arrayTableData(data);
                }
                var fail = function (data) {
                    app.loading(false);
                }
                services.getGeneric('handover/getAllHandover/' + self.personDetails().personId).then(success, fail);
            };
            //------------------ actions for button ---------------------//

            self.addActionBtn = function () {
                var dataToSend = { pageMode: 'ADD',"checkUser": self.checkSubmitUser() };
                localStorage.setItem('handoverData', JSON.stringify(dataToSend));
                // oj.Router.rootInstance.store(dataToSend);
                oj.Router.rootInstance.go('handOverOperation');
                app.loading(true);
            };

            self.editActionBtn = function () {
                var dataToSend = { pageMode: 'EDIT', data: self.selectedDataTable(),"checkUser": self.checkSubmitUser()  };
                localStorage.setItem('handoverData', JSON.stringify(dataToSend));
                // oj.Router.rootInstance.store(dataToSend);
                oj.Router.rootInstance.go('handOverOperation');
                app.loading(true);
            };

            self.viewActionBtn = function () {
                var dataToSend = { pageMode: 'VIEW', data: self.selectedDataTable() ,"checkUser": self.checkSubmitUser() };
                localStorage.setItem('handoverData', JSON.stringify(dataToSend));
                // oj.Router.rootInstance.store(dataToSend);
                oj.Router.rootInstance.go('handOverOperation');
                app.loading(true);
            };

            self.viewApprovalListActionBtn = async function () {
                self.rolrType = ko.observableArray([]);
                self.notificationType = ko.observableArray([]);
                self.responseCode = ko.observableArray([]);
                self.responseDate = ko.observableArray([]);
                self.approvalResponseArr = ko.observableArray([]);
                // self.datacrad = ko.observableArray([]);
                self.positionName = ko.observable();
                self.rejectReason = ko.observable();
                self.oprationdisabled(true);
                $(".approval-btn").addClass("loading");
                self.datacrad([]);
                var eitcode = "XXX_HANDOVER_REQUEST";

                var getValidGradeCBF = function (data) {
                    var dataLength = data.length;
                    for (var c = 0; c < data.length; c++) {
                        if (data[c].responseCode == 'REJECTED') {
                            dataLength = c + 1;
                        }
                    }
                    if (data.length != 0) {
                        var aprrovalStatus = "PENDING";

                        for (var i = 0; i < dataLength; i++) {
                            if (data[i].notificationType == "FYA") {
                                data[i].notificationType = "For your action";
                            }
                            else if (data[i].notificationType == "FYI") {
                                data[i].notificationType = "For your information";
                            }
                            else {
                                data[i].notificationType;
                            }

                            var notificationStatus, bodyCardStatus, cardStatus, rejectReason;
                            if (data[i].notificationType === "For your action") {
                                notificationStatus = 'app-type-a';
                            } else {
                                cardStatus = 'badge-secondary';
                                notificationStatus = 'app-type-i';
                                bodyCardStatus = 'app-crd-bdy-border-ntre';
                            }
                            if (!data[i].responseCode) {
                                cardStatus = 'badge-warning';
                                bodyCardStatus = 'app-crd-bdy-border-pen';
                                data[i].responseDate = '';
                            } else if (data[i].responseCode === 'APPROVED') {
                                cardStatus = 'badge-success';
                                bodyCardStatus = 'app-crd-bdy-border-suc';
                            } else if (data[i].responseCode === 'REJECTED') {
                                cardStatus = 'badge-danger';
                                bodyCardStatus = 'app-crd-bdy-border-fail';
                            } else {

                            } if (data[i].rejectReason) {
                                rejectReason = data[i].rejectReason;
                            } else {
                                rejectReason = "There is no comment";
                            }


                            if (data[i].rolrType === "EMP") {
                                //                            data[i].rolrType= app.personDetails().displayName
                                if (!data[i].responseCode) {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        }
                                    );
                                } else {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        }
                                    );
                                }

                            } else if (data[i].rolrType == "LINE_MANAGER") {
                                //                            data[i].rolrType= app.personDetails().managerName;
                                if (!data[i].responseCode) {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });
                                } else {
                                    self.datacrad.push(
                                        {
                                            personName: self.personDetails().managerName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });

                                }

                            } else if (data[i].rolrType == "LINE_MANAGER+1") {
                                //                               data[i].rolrType= app.personDetails().managerOfMnagerName;
                                if (!data[i].responseCode) {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });
                                } else {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });


                                }

                            } else if (data[i].rolrType == "POSITION") {

                                //                               data[i].rolrType= app.personDetails().managerOfMnagerName;
                                for (var j = 0; j < app.roleOptionType().length; j++) {
                                    if (app.roleOptionType()[j].value == data[i].roleId) {
                                        self.positionName(app.roleOptionType()[j].label);
                                    }
                                }
                                if (!data[i].responseCode) {
                                    self.datacrad.push(
                                        {
                                            personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });
                                } else {
                                    self.datacrad.push(
                                        {
                                            personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });


                                }

                            } else if (data[i].rolrType == "ROLES") {
                                //                               data[i].rolrType= app.personDetails().managerOfMnagerName;

                                for (var j = 0; j < rootViewModel.allRoles().length; j++) {
                                    if (rootViewModel.allRoles()[j].ROLE_ID == data[i].roleId) {

                                        self.positionName(rootViewModel.allRoles()[j].ROLE_NAME);
                                    }
                                }
                                if (!data[i].responseCode) {
                                    self.datacrad.push(
                                        {
                                            personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });
                                } else {
                                    self.datacrad.push(
                                        {
                                            personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });


                                }

                            } else if (data[i].rolrType == "ROLES") {
                                //                               data[i].rolrType= app.personDetails().managerOfMnagerName;

                                for (var j = 0; j < rootViewModel.allRoles().length; j++) {
                                    if (rootViewModel.allRoles()[j].value == data[i].roleId) {
                                        self.positionName(rootViewModel.allRoles()[j].label);
                                    }
                                }
                                if (data[i].responseCode === null) {
                                    self.datacrad.push(
                                        {
                                            personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });
                                } else {
                                    self.datacrad.push(
                                        {
                                            personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });

                                }

                            } else if (data[i].rolrType == "JOB_LEVEL") {
                                //                            data[i].rolrType= app.personDetails().managerName;
                                if (!data[i].responseCode) {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].lineManagerName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });
                                } else {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].lineManagerName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });

                                }

                            } else if (data[i].rolrType == "AOR") {
                                //                            data[i].rolrType= app.personDetails().managerName;
                                if (!data[i].responseCode) {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].lineManagerName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });
                                } else {
                                    self.datacrad.push(
                                        {
                                            personName: data[i].lineManagerName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        });

                                }

                            }


                            //                       self.notificationType.push(data[i].notificationType);
                            //                       self.responseCode.push(data[i].responseCode);
                            //                       self.responseDate.push(data[i].responseDate);  

                            //  data[i].approvalType = searchArray(data[i].approvalType, self.approvalType());
                            //  data[i].roleName = searchArray(data[i].roleName, self.roleName());

                        }
                    } else {
                        self.datacrad.push(
                            {
                                personName: 'There is no approval list to this request',
                                FYA: '',
                                Approved: '',
                                date: '',
                                status: '',
                                bodyStatus: '',
                                appCardStatus: '',
                                rejectReason: ''
                            });
                    }
                    //                    var Data = data;
                    //          
                    //                self.dataSourceTB2(new oj.ArrayTableDataSource(Data));
                    document.getElementById('approvalDialog').open();
                };
                // ... leave firstName and lastName unchanged ...

                //self.lbl = ko.observable(self.approvaltype());
                await services.getGenericAsync("approvalList/approvaltype/" + eitcode + "/" + self.selectedDataTable().headerId).then(getValidGradeCBF, app.failCbFn);
                self.oprationdisabled(false);
                $(".approval-btn").removeClass("loading");
            };

            self.backActionBtn = function () {
                oj.Router.rootInstance.go('home');
            };

            self.approvalDialogCloseAction = function () {
                document.getElementById('approvalDialog').close();
            };

            //------------------------------------------------------------//
            //----------------------- table selection -------------------//
            self.tableSelectionListener = function (event) {
                var data = event.detail;
                var currentRow = data.currentRow;
                if (currentRow !== null) {
                    var rowObj = document.getElementById('table').getDataForVisibleRow(currentRow['rowIndex']);

                    self.tableSelectedRowKey(currentRow['rowKey']);
                    //store current index to pass it to other form when update or view
                    self.tableSelectedIndex(currentRow['rowIndex']);
                    self.oprationdisabled(false);
                    self.selectedDataTable(self.arrayTableData()[self.tableSelectedIndex()]);
                }
            };
            //----------------------------------------------------------//
            //----------------- translations -----------------------//
            function initTranslations() {

                self.labels({
                    pageTitle: ko.observable(getTranslation('handOver.pageTitle')),
                    add: ko.observable(getTranslation('others.add')),
                    edit: ko.observable(getTranslation('others.edit')),
                    view: ko.observable(getTranslation('others.view')),
                    approval_list: ko.observable(getTranslation('labels.Approval_list')),
                    back: ko.observable(getTranslation('labels.backlbl')),
                    personName: ko.observable(getTranslation('others.personName')),
                    approved: ko.observable(getTranslation('labels.approvalstatus')),
                    date: ko.observable(getTranslation('common.datelbl')),
                    ok: ko.observable(getTranslation('others.ok')),
                    columnArray: ko.observableArray([
                        {
                            "headerText": getTranslation('handOver.id'), "field": "headerId"
                        },
                        {
                            "headerText": getTranslation('labels.status'), "field": "statusCurrent"
                        },
                        {
                            "headerText": getTranslation('handOver.creationDate'), "field": "creationDate"
                        },
                        {
                            "headerText": getTranslation('handOver.manager'), "field": "employeeManager"
                        },
                        {
                            "headerText": getTranslation('handOver.employeePosition'), "field": "employeePosition"
                        },
                        {
                            "headerText": getTranslation('handOver.reason'), "field": "reason"
                        },
                        {
                            "headerText": getTranslation('handOver.notes'), "field": "notes"
                        },
                        // {
                        //     "headerText": getTranslation('handOver.employeeName'), "field": "employeeName"
                        // }, 
                        // {
                        //     "headerText": getTranslation('handOver.employeeDepartment'), "field": "employeeDepartment"
                        // }
                    ])


                });

            }
            initTranslations();

        }
        return individualDevelopmentPlanSummaryViewModel;
    });