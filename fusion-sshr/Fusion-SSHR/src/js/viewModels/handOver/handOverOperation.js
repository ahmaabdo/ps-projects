/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * handOver module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
'ojs/ojdatacollection-utils','ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojdialog',
'ojs/ojvalidationgroup','ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojmessages', 'ojs/ojmessage', 
'ojs/ojradioset', 'ojs/ojselectcombobox','ojs/ojformlayout','ojs/ojtrain','ojs/ojtable'],
    function (oj, ko, $, app, services, commonhelper,DataCollectionEditUtils) {

        function handOverViewModel() {
            var self = this;
            var getTranslation = oj.Translations.getTranslatedString;
            var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
            self.labels = ko.observable();
            self.pageMode = ko.observable();
            self.handOverList = ko.observableArray([]);
            self.reasonList = ko.observableArray([]);
            self.fieldsDisabled = ko.observable(false);
            self.tableData=ko.observableArray([]);
            self.approvalsBtnVisible=ko.observable(false);
            self.previousBtnVisible=ko.observable(false);
            self.nextBtnVisible=ko.observable(true);
            self.submitBtnVisible=ko.observable(false);
            self.currentStepValue=ko.observable('stp1');
            self.trainVisibility=ko.observable(true);
            self.placeHolder=ko.observable('Please select value');
            self.rejectReason = ko.observable();
            self.workFlowApprovalId=ko.observable();
            self.disableSubmit = ko.observable(false);
            self.approvlModel = {};
            self.tasksStatusArray=ko.observableArray();
            self.tasksDocTypeArray=ko.observableArray();
            self.editRow=ko.observable();
            self.deptObservableArray = ko.observableArray([]);
            self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, { keyAttributes: 'taskId' })
            self.count=ko.observable(1);
            //--------------- define messages notification ---------------//
            self.messages = ko.observableArray();
            self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
            self.personDetails = ko.observableArray();
            self.checkSubmitUser = ko.observable();
            self.position =
            {
                "my": { "vertical": "top", "horizontal": "end" },
                "at": { "vertical": "top", "horizontal": "end" },
                "of": "window"
            };
            self.closeMessageHandler = function (event) {
                self.messages.remove(event.detail.message);
            };
            //************************************************* */
            //----------------- for language change ----------------------//
            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });
           //************************************************* */
            self.handleAttached = function () {
                self.retrieve =JSON.parse(localStorage.getItem('handoverData')); //oj.Router.rootInstance.retrieve();
                    if (self.retrieve.checkUser === "employee") {
                        self.checkSubmitUser("employee");
                        self.personDetails(app.personDetails());
                    } else if (self.retrieve.checkUser === "Specialist") {
                        self.checkSubmitUser("Specialist");
                        self.personDetails(app.globalspecialistEMP());
                    }
                    if (self.checkSubmitUser() === "Specialist") {
                        self.labels().pageTitle(self.labels().pageTitle() + " Employee Name: " + self.personDetails().displayName + " Employee Number:  " + self.personDetails().personNumber);
                    }             
                self.pageMode(self.retrieve.pageMode);
                self.rejectReason(" ");
                if(self.personDetails()){
                    getHandOverReport();
                }
                // set employee data
                if (self.pageMode() == 'ADD') {
                    self.handOverDataModel.employeeId(self.personDetails().personId);
                    self.handOverDataModel.employeeNumber(self.personDetails().personNumber);
                    self.handOverDataModel.createBy(self.personDetails().personId);
                    self.handOverDataModel.employeeName(self.personDetails().displayName);
                    self.handOverDataModel.employeeDepartment(self.personDetails().departmentName);
                    self.handOverDataModel.employeePosition(self.personDetails().positionName);
                    self.handOverDataModel.employeeNationality(self.personDetails().country);
                    self.handOverDataModel.employeeManager(self.personDetails().managerName);
                    self.handOverDataModel.status('PENDING_APPROVED');
                } else if (self.pageMode() == 'EDIT') {
                    self.currentStepValue('stp1');
                    self.trainVisibility(true);
                    self.fieldsDisabled(false);
                    var data=self.retrieve.data;
                    self.handOverDataModel.headerId(data.headerId);
                    self.handOverDataModel.employeeId(data.employeeId);
                    self.handOverDataModel.employeeNumber(data.employeeNumber);
                    self.handOverDataModel.employeeName(data.employeeName);
                    self.handOverDataModel.employeeDepartment(data.employeeDepartment);
                    self.handOverDataModel.employeePosition(data.employeePosition);
                    self.handOverDataModel.employeeNationality(data.employeeNationality);
                    self.handOverDataModel.employeeManager(data.employeeManager);
                    self.handOverDataModel.handoverTo1(parseInt(data.handoverTo1));
                    if(data.handoverTo2){
                    self.handOverDataModel.handoverTo2(parseInt(data.handoverTo2));
                    }
                    if(data.handoverTo3){
                    self.handOverDataModel.handoverTo3(parseInt(data.handoverTo3));
                    }
                    self.handOverDataModel.reason(data.reason);
                    self.handOverDataModel.notes(data.notes);
                    self.handOverDataModel.status('PENDING_APPROVED');
                    self.handOverDataModel.tasksList(data.tasksList);
                    self.count(data.tasksList.length);
                    // self.deptObservableArray(data.tasksList);
                    self.handOverDataModel.updateBy(self.personDetails().personId);
                    // add remove button in table if page mode is view 
                    self.addDataToTable(true);

                } else if (self.pageMode() == 'VIEW') {
                    self.currentStepValue('stp2');
                    self.fieldsDisabled(true);
                    self.trainVisibility(false);
                    var data=self.retrieve.data;
                    self.handOverDataModel.employeeId(data.employeeId);
                    self.handOverDataModel.employeeNumber(data.employeeNumber);
                    self.handOverDataModel.employeeName(data.employeeName);
                    self.handOverDataModel.employeeDepartment(data.employeeDepartment);
                    self.handOverDataModel.employeePosition(data.employeePosition);
                    self.handOverDataModel.employeeNationality(data.employeeNationality);
                    self.handOverDataModel.employeeManager(data.employeeManager);
                    self.handOverDataModel.handoverTo1(parseInt(data.handoverTo1));
                    if(data.handoverTo2){
                    self.handOverDataModel.handoverTo2(parseInt(data.handoverTo2));
                    }
                    if(data.handoverTo3){
                    self.handOverDataModel.handoverTo3(parseInt(data.handoverTo3));
                    }
                    self.handOverDataModel.reason(data.reason);
                    self.handOverDataModel.notes(data.notes);
                    self.handOverDataModel.tasksList(data.tasksList);
                    // self.deptObservableArray(data.tasksList);
                    // don't add remove button in table if page mode is view 
                    self.addDataToTable(false);

                } else if (self.pageMode() == 'Notfi') {

                    self.currentStepValue('stp2');
                    self.fieldsDisabled(true);
                    self.trainVisibility(false);
                    if(self.retrieve.TYPE == "FYA"){
                    self.approvalsBtnVisible(true);
                }else{
                   self.approvalsBtnVisible(false); 
                }
                    //set id for the request
                    //self.retrieve = oj.Router.rootInstance.retrieve();
                    self.workFlowApprovalId(self.retrieve.id);
                    self.getHandOverRequestByID();
                }
            };
            
            self.getHandOverRequestByID = function () {
                    
                    
                    var getHandOverRequestByIDCBF = function (data)
                    {
                        self.handOverDataModel.employeeId(data[0].employeeId);
                    self.handOverDataModel.employeeNumber(data[0].employeeNumber);
                    self.handOverDataModel.employeeName(data[0].employeeName);
                    self.handOverDataModel.employeeDepartment(data[0].employeeDepartment);
                    self.handOverDataModel.employeePosition(data[0].employeePosition);
                    self.handOverDataModel.employeeNationality(data[0].employeeNationality);
                    self.handOverDataModel.employeeManager(data[0].employeeManager);
                    self.handOverDataModel.handoverTo1(parseInt(data[0].handoverTo1));
                    if(data[0].handoverTo2){
                    self.handOverDataModel.handoverTo2(parseInt(data[0].handoverTo2));
                    }
                    if(data[0].handoverTo3){
                    self.handOverDataModel.handoverTo3(parseInt(data[0].handoverTo3));
                    }
                    self.handOverDataModel.reason(data[0].reason);
                    self.handOverDataModel.notes(data[0].notes);
                    self.handOverDataModel.tasksList(data[0].tasksList);
                    self.addDataToTable(false);
                    };
                    

                        services.getGeneric("handover/getAllHandoverByHeaderId/" + self.retrieve.TRS_ID).then(getHandOverRequestByIDCBF, app.failCbFn);

                };
                
                self.updateHandoverRequest = function () {
                    self.disableSubmit(true);
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": self.personDetails().personId,
                        "RESPONSE_CODE": "APPROVED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "PERSON_NAME": self.retrieve.personName,
                        "rejectReason": self.rejectReason(),
                        "requestTypeLbl": self.retrieve.msg_Title_Lbl,
                        "PERSON_EMP_FYI_ID": self.retrieve.person_id,
                        "workflowId":self.workFlowApprovalId()
                        
                    };
                    
                    var getUpdateWorkFlowNotificationCBF = function (data)
                    {
                        var updateStatusCBFN = function (data) {

                            oj.Router.rootInstance.go("notificationScreen");
                            };
                        oj.Router.rootInstance.go("notificationScreen");
                        
                    };
                        services.workflowAction("workflowApproval/", headers).then(app.failCbFn, getUpdateWorkFlowNotificationCBF);

                };
                
                self.rejectRewaardRequst = function (data, event) {
                    self.disableSubmit(true);
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": self.personDetails().personId,
                        "RESPONSE_CODE": "REJECTED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "rejectReason":self.rejectReason()  ,
                        "workflowId":self.workFlowApprovalId(),
                        "PERSON_NAME":self.retrieve.personName,
                        "PERSON_EMP_FYI_ID": self.retrieve.person_id,
                    };
                    var payload = {
                        "headerId" : self.retrieve.TRS_ID,
                        "updateBy" : self.personDetails().personId,
                        "status" : "REJECTED"
                    };
                    var approvalActionCBFN = function (data) {
                        var updateStatusCBFN =function (data){
                               
                                
                            };
                        services.addGeneric("handover/updateHandOverStatus/", payload).then(updateStatusCBFN,app.failCbFn);
                        oj.Router.rootInstance.go('notificationScreen');
                    };
                    services.workflowAction("workflowApproval/", headers).then( app.failCbFn,approvalActionCBFN)

                };


            // --------------- handOver report -----------//
            var getHandOverReport = function () {
                var payload = { "reportName": "XXX_EMPLOYEE_LIST_BYID_REPORT"};

                var success = function (data) {
                    self.handOverList(data);
                    app.loading(false);
                };

                var fail = function (data) {
                    app.loading(false);
                    $.notify('Fail to get handover list', "error");
                };

                services.getGenericReportNotAsync(payload).then(success, fail);
            };
            //------------- buttons Action --------------//
            self.backAction = function () {
                oj.Router.rootInstance.go('handOverSummary');
            };
            
            function isEmpty(val) {
            return (val === undefined || val == null || val.length <= 0) ? true : false;
        }

            self.submitYes = function () {
                // self.handOverDataModel.tasksList(self.deptObservableArray());
                self.handOverDataModel.tasksList(self.tableData());
                var handoverTo1Name = self.handOverList().filter(e => e.value == self.handOverDataModel.handoverTo1());
                self.handOverDataModel.handoverTo1Name(handoverTo1Name[0].label);
                if(self.handOverDataModel.handoverTo2()){
                    var handoverTo2Name = self.handOverList().filter(e => e.value == self.handOverDataModel.handoverTo2());
                    self.handOverDataModel.handoverTo2Name(handoverTo2Name[0].label);
                }
                if(self.handOverDataModel.handoverTo3()){
                var handoverTo3Name = self.handOverList().filter(e => e.value == self.handOverDataModel.handoverTo3());
                self.handOverDataModel.handoverTo3Name(handoverTo3Name[0].label);
            }
                
                
                
                self.handOverDataModel.requestCode('XXX_HANDOVER_REQUEST');
                app.loading(true);
                document.getElementById('submitDialog').close();
                var payload=ko.toJSON(self.handOverDataModel);
                var success=function(data){
                    ApprovalOprationServiceRecord();
                  
                  //oj.Router.rootInstance.go('handOverSummary');
                };
                var fail=function(data){
                  app.loading(false);
                };
                services.addGeneric("handover/insertOrUpdateHandover/" + self.pageMode(), payload).then(success,fail);
            };
            
            function ApprovalOprationServiceRecord() {

                    self.approvlModel.type = "XXX_HANDOVER_REQUEST";
                    self.approvlModel.personNumber = self.personDetails().personNumber;
                    self.approvlModel.personName = self.personDetails().displayName;
                    self.approvlModel.presonId = self.personDetails().personId;
                    self.approvlModel.managerId = self.personDetails().managerId;
                    self.approvlModel.managerName = self.personDetails().managerName;//
                    self.approvlModel.managerOfManager = self.personDetails().managerOfManager;
                    self.approvlModel.managerOfMnagerName = self.personDetails().managerOfMnagerName;//
                    self.approvlModel.created_by = self.personDetails().personId;
                    self.approvlModel.creation_date = self.personDetails().personId;
                    if(self.pageMode() == "EDIT"){
                    self.approvlModel.transactionId = self.handOverDataModel.headerId();
                }
                    var jsonData = ko.toJSON(self.approvlModel);
                    var approvalCondition = "";
                if(!isEmpty(self.handOverDataModel.handoverTo1())){
                    approvalCondition = "One_HandOver_Approval";
                }
                if(!isEmpty(self.handOverDataModel.handoverTo2())){
                    approvalCondition = "Two_HandOver_Approval";
                }
                if(!isEmpty(self.handOverDataModel.handoverTo3())){
                    approvalCondition = "Three_HandOver_Approval";
                }

                    var getValidGradeCBF = function (data) {
                        oj.Router.rootInstance.go('handOverSummary');
                    };

                    services.addGeneric("selfService/handoverApproval/" + self.pageMode() + "/" + approvalCondition, jsonData).then(getValidGradeCBF, self.failCbFn);
                }

            self.submitNo = function () {
                document.getElementById('submitDialog').close();
            };

            //************************************************* */
            //-------------------- table actions -------------------//

            self.beforeRowEditListener = function (event) {
                var key = event.detail.rowContext.status.rowKey;
                self.dataprovider.fetchByKeys({keys: [key]}).then(function (fetchResult) {
                  self.rowData = {};
                  Object.assign(self.rowData, fetchResult.results.get(key).data);
                });
              };
              self.beforeRowEditEndListener = function (event) {
                var detail = event.detail;
                if (detail.cancelEdit == true) {
                  return;
                }
                if (oj.DataCollectionEditUtils.basicHandleRowEditEnd(event, detail) === false) {
                  event.preventDefault();
                } else {
                  self.deptObservableArray.splice(detail.rowContext.status.rowIndex, 1, self.rowData);
                }
              };

              self.tableBtnEdit = function (event, context) {
                self.editRow({ rowKey: context.key });
              };
          
              // eslint-disable-next-line no-unused-vars
              self.tableBtnSave = function (event, context) {
                self.editRow({ rowKey: null });
              };
            

            self.addNewRowAction = function () {
                var html = '<tr><td><button id="removeTableRow" class="idpPageRemoveBtnTable"><span class="fa fa-minus"></span></button></td><td><textarea class="idpPageInput" name="name1"/></td><td><select id="taskStatusSelect'+self.count()+'"  class="idpPageInput" name="name2"></td><td><select id="taskDocumentTypeSelect'+self.count()+'"  class="idpPageInput" name="name3"></td></tr>';
                $('tbody').append(html);
                var statusListId = $("#taskStatusSelect"+self.count());
                var docListId = $("#taskDocumentTypeSelect"+self.count());
                $.each(self.tasksStatusArray(), function (index, item) {
                    statusListId.append(new Option(item.label, item.value));
                });
                $.each(self.tasksDocTypeArray(), function (index, item) {
                    docListId.append(new Option(item.label, item.value));
                });
                self.count(self.count()+1);
            };
            // remove button in table
            $(document).on("click", "#removeTableRow", function () {
                $(this).parents('tr').remove();
            });
           // get all data in table
            self.getTableData = function () {
                self.tableData([]);
                var dataIsNull = false;
                var table = document.getElementById("myTable");
                var totalRowCount = table.rows.length;
                for (var c = 1; c < totalRowCount; c++) {
                    var x = table.rows[c].cells;
                    var tableContent = {};
                    for (var i = 1; i < x.length; i++) {
                        var elem =typeof x[i].getElementsByTagName("textarea")[0]=='undefined'?
                        x[i].getElementsByTagName("select")[0].value:x[i].getElementsByTagName("textarea")[0].value;
                        if (elem == "") {
                            dataIsNull = true;
                            break;
                        } else {
                            if (i == 1) {
                                tableContent.taskName = elem;
                            } else if (i == 2) {
                                tableContent.status = elem;
                            } else if (i == 3) {
                                tableContent.documentType = elem;
                            }
                        }
                    }
                    self.tableData.push(tableContent);
                }
                // SHOW THE RESULT IN THE CONSOLE WINDOW.
                if (dataIsNull || self.tableData().length == 0) {
                    self.tableData([]);
                }
            };

            self.tableFieldsDisabled=function(disable){
                $("#myTable :input").prop("disabled", disable); 
             };

             self.addDataToTable=function(hasRemove){
                for(var i=0;i<self.handOverDataModel.tasksList().length ;i++){
                    var task=self.handOverDataModel.tasksList()[i];
                    if(hasRemove){
                        var html = '<tr><td><button id="removeTableRow" class="idpPageRemoveBtnTable"><span class="fa fa-minus"></span></button></td><td><textarea class="idpPageInput" name="name1">'+task.taskName+'</textarea></td><td><select id="taskStatusSelect'+i+'" class="idpPageInput" name="name2"></td><td><select id="taskDocumentTypeSelect'+i+'" class="idpPageInput" name="name3"></td></tr>';
                        $('tbody').append(html);
                        var statusListId = $('#taskStatusSelect'+i);
                        var docListId = $("#taskDocumentTypeSelect"+i);
                        $.each(self.tasksStatusArray(), function (index, item) {
                            statusListId.append(new Option(item.label, item.value));
                        });
                        $.each(self.tasksDocTypeArray(), function (index, item) {
                            docListId.append(new Option(item.label, item.value));
                        });
                        $("#taskStatusSelect"+i).val(task.status);
                        $("#taskDocumentTypeSelect"+i).val(task.documentType);
                    }else{
                        var html = '<tr><td></td><td><textarea class="idpPageInput" name="name1" disabled="true">'+task.taskName+'</textarea></td><td><select id="taskStatusSelect'+i+'"  class="idpPageInput" name="name2" disabled="true"></td><td><select id="taskDocumentTypeSelect'+i+'" class="idpPageInput" name="name3" disabled="true" ></td></tr>';
                        $('tbody').append(html); 
                        var statusListId = $('#taskStatusSelect'+i);
                        var docListId = $("#taskDocumentTypeSelect"+i);
                        $.each(self.tasksStatusArray(), function (index, item) {
                            statusListId.append(new Option(item.label, item.value));
                        });
                        $.each(self.tasksDocTypeArray(), function (index, item) {
                            docListId.append(new Option(item.label, item.value));
                        });
                        $("#taskStatusSelect"+i).val(task.status);
                        $("#taskDocumentTypeSelect"+i).val(task.documentType);
                    }
                }
             };
            // self.addNewRowAction1 = function () {
            //     var task=self.deptObservableArray()[self.deptObservableArray().length-1];
            //     self.deptObservableArray.push({
            //         taskId: typeof task=='undefined' ? 1:task.taskId+1,
            //         taskName:"",
            //         status:'Pending',
            //         documentType:'Electronic'
            //     });
            // };
            // DELETE TABLE ROW.
            // self.tableBtnRemove=function(event,context){
            //     self.deptObservableArray.splice(context.index, 1);
            // }
             //************************************************* */
             //---------------- approval actions button --------------//

             self.approveActionBtn=function(){
                 document.getElementById('yesNoDialog').open();
             };
             self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };

             self.rejectActionBtn=function(){
                 document.getElementById('rejectDialog').open();
             };
             self.rejectCancelButton = function () {
                    document.querySelector("#rejectDialog").close();
                };
             //************************************************* */
             //------------------- train buttons action ------------------//

             self.previousStep=function(){
                var train = document.getElementById('train');
                var prev = train.getPreviousSelectableStep();
                if (prev != null) {
                  self.submitBtnVisible(false);
                  self.nextBtnVisible(true);
                  self.previousBtnVisible(false);
                  self.fieldsDisabled(false);
                  self.currentStepValue(prev);
                  self.tableFieldsDisabled(false);
                }
             };

             self.nextStep=function(){
                 if(self.handOverDataModel.reason() == "Other" && isEmpty(self.handOverDataModel.notes())){
                     self.messages.push({
                    severity: 'error',
                    summary: 'Please fill the notes text',
                    autoTimeout: 5000
                });
                 }
                 else{
                self.getTableData();
                var train = document.getElementById('train');
                var next = train.getNextSelectableStep();
                var tracker = document.getElementById("tracker");
                if (tracker.valid != "valid")
                {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    return;
                }else if(self.tableData().length == 0){
                  self.messages.push({
                    severity: 'error',
                    summary: self.labels().checkTableDataMsg(),
                    autoTimeout: 5000
                });
                return;
                }else if(next != null){
                  self.submitBtnVisible(true);
                  self.nextBtnVisible(false);
                  self.previousBtnVisible(true);
                  self.fieldsDisabled(true);
                  self.currentStepValue(next);
                  self.tableFieldsDisabled(true);
                }
                 }
             };

             self.submitActionBtn=function(){
                document.getElementById('submitDialog').open();
             };

             self.stopSelectListener=function(){
                self.getTableData();
                var tracker = document.getElementById("tracker");
                if (tracker.valid != "valid")
                {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    event.preventDefault();
                    return;
                }else if(self.tableData().length == 0){
                  self.messages.push({
                    severity: 'error',  summary: self.labels().checkTableDataMsg()
                });
                event.preventDefault();
                return;
                }else{
                  if(self.currentStepValue()=='stp1'){
                    self.nextStep();
                  }else{
                    self.previousStep();
                  }
                }
             };
             
                function buildBackObject() {
                    var cancelBack = {
                        EIT: "XXX_HANDOVER_REQUEST",
                        returnToSymary: "backAction",
                        checkUser: self.checkSubmitUser()
                    };
                    oj.Router.rootInstance.store(cancelBack);
                }
                self.handleDetached = function (info) {
                    buildBackObject();
                };

             //************************************************* */
            //-------------------------- pageModel --------------------------//
            self.handOverDataModel = ko.observable();
            self.handOverDataModel = {
                headerId:ko.observable(),
                employeeId: ko.observable(),
                employeeNumber:ko.observable(),
                status:ko.observable(),
                createBy:ko.observable(),
                creationDate:ko.observable(),
                updateBy:ko.observable(),
                updatedDate:ko.observable(),
                employeeName: ko.observable(),
                employeeDepartment: ko.observable(),
                employeePosition: ko.observable(),
                employeeNationality: ko.observable(),
                employeeManager: ko.observable(),
                handoverTo1: ko.observable(),
                handoverTo2: ko.observable(),
                handoverTo3: ko.observable(),
                reason: ko.observable(),
                notes: ko.observable(),
                handoverTo1Name:ko.observable(),
                handoverTo2Name:ko.observable(),
                handoverTo3Name:ko.observable(),
                requestCode:ko.observable(),
                tasksList:ko.observableArray([])

            };

            function initTranslations() {

                self.labels({
                    pageTitle: ko.observable(getTranslation('handOver.pageTitle')),
                    yes: ko.observable(getTranslation('others.yes')),
                    no: ko.observable(getTranslation('others.no')),
                    back: ko.observable(getTranslation('others.back')),
                    submitLbl: ko.observable(getTranslation('others.submit')),
                    submitTitle: ko.observable(getTranslation('handOver.submitDialogTitle')),
                    submitMessage: ko.observable(getTranslation('handOver.submitDialogMessage')),
                    employeeIdLbl: ko.observable(getTranslation('handOver.employeeId')),
                    employeeNameLbl: ko.observable(getTranslation('handOver.employeeName')),
                    employeeDepartmentLbl: ko.observable(getTranslation('handOver.employeeDepartment')),
                    employeeNationalityLbl: ko.observable(getTranslation('handOver.employeeNationality')),
                    managerLbl: ko.observable(getTranslation('handOver.manager')),
                    handOver1Lbl: ko.observable(getTranslation('handOver.handOver1')),
                    handOver2Lbl: ko.observable(getTranslation('handOver.handOver2')),
                    handOver3Lbl: ko.observable(getTranslation('handOver.handOver3')),
                    reasonLbl: ko.observable(getTranslation('handOver.reason')),
                    notesLbl: ko.observable(getTranslation('handOver.notes')),
                    employeePositionLbl: ko.observable(getTranslation('handOver.employeePosition')),
                    employeeInformation: ko.observable(getTranslation('handOver.employeeInformation')),
                    tasks: ko.observable(getTranslation('handOver.tasks')),
                    taskName: ko.observable(getTranslation('handOver.taskName')),
                    status: ko.observable(getTranslation('handOver.status')),
                    documentType: ko.observable(getTranslation('handOver.documentType')),
                    addNewRow: ko.observable(getTranslation('handOver.addNewRow')),
                    approveLbl:ko.observable(getTranslation('others.approve')),
                    rejectLbl:ko.observable(getTranslation('others.reject')),
                    perviousLbl:ko.observable(getTranslation('others.pervious')),
                    nextLbl:ko.observable(getTranslation('others.next')),
                    approveMessage:ko.observable(getTranslation("others.approvalMSG")),
                    confirmMessage:ko.observable(getTranslation("labels.confirmMessage")),
                    checkTableDataMsg:ko.observable(getTranslation('handOver.checkTableDataMsg')),
                    rejectMessage:ko.observable(getTranslation("labels.rejectHandover")),
                    stepArray:ko.observableArray([{label: getTranslation("labels.create"), id: 'stp1'},
                    {label: getTranslation("labels.review"), id: 'stp2'}]),
                    columnArray: ko.observableArray([
                        {
                            "headerText": '',"template":"removeBtnTemplate"
                        },
                        {
                            "headerText": getTranslation('handOver.taskName'), "field": "taskName","template":"taskNameTemplate"
                        }, 
                        {
                            "headerText": getTranslation('handOver.status'), "field": "status","template":"statusTemplate"
                        }, 
                        {
                            "headerText": getTranslation('handOver.documentType'), "field": "documentType","template":"docTypeTemplate"
                        }, 
                        // {
                        //     "headerText": '',"template":"editBtnTemplate"
                        // }
                    ])
                });
                
                self.reasonList(app.getPaaSLookup('handover-reason'));
                self.tasksStatusArray(app.getPaaSLookup('Handover_Task_Status'));
                self.tasksDocTypeArray(app.getPaaSLookup('Handover_Doucment_type'));
            };
            initTranslations();
        }
        return handOverViewModel;
    });