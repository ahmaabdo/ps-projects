define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services',
    'ojs/ojknockout', 'ojs/ojselectcombobox', 'ojs/ojpopup', 'ojs/ojpagingtabledatasource',
    'ojs/ojvalidationgroup', 'ojs/ojinputtext', 'ojs/ojtable', 'ojs/ojlabel'],
    function (oj, ko, $, app, services) {

        function manageStoredFunctionsViewModel() {
            var self = this;
            var selectedName = "";
            var getTranslation = oj.Translations.getTranslatedString;
            self.labels = ko.observable([]);

            self.statusArr = ko.observableArray([]);
            self.isOperationBtnDisabled = ko.observable(true);
            self.isContentAreaDisabled = ko.observable(true);
            self.operationBtnLbl = ko.observable("");
            self.isSubmitVisible = ko.observable(true);
            self.isSearchBtnDisabled = ko.observable(false);
            self.isDeleteMsgVisible = ko.observable(false);
            self.transactionType = ko.observable('EDIT');
            self.operationDialogHeader = ko.observable('EDIT');
            self.bodyContentValue = ko.observable("");
            self.functionNameSearchVal = ko.observable("");
            self.statusSearch = ko.observable("");
            self.groupValid = ko.observable();
            self.columnArray = ko.observableArray();
            self.selectedRow = ko.observable();
            self.deptObservableArray = ko.observableArray([]);
            self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, { idAttribute: 'id' })));


            var failCbFn = function () {
                app.loading(false);
                app.messages.push({
                    severity: "error",
                    summary: self.labels().unknownErrMsg,
                    autoTimeout: 0
                });
            };

            self.cancelButton = function () {
                document.querySelector("#operationDialog").close();
            };

            self.resetSearch = function () {
                self.functionNameSearchVal('');
                self.statusSearch('');

                self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, { idAttribute: 'id' })));
            };

            self.backAction = function () {
                oj.Router.rootInstance.go('setup');
            };

            self.refreshDataBtn = function () {
                app.loading(true);
                self.getAllStoredFunctions();
            };


            self.getAllStoredFunctions = async function () {

                await services.getGenericAsync("manageStoredFunctions", {}).then(data => {
                    self.deptObservableArray([]);
                    if (data.length !== 0) {

                        self.isOperationBtnDisabled(true);
                        self.deptObservableArray(data);
                        self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, { idAttribute: 'id' })));
                    }

                    app.loading(false);
                }, failCbFn);
            };


            self.submitAction = async function () {

                var tracker = document.getElementById("tracker");
                if (tracker.valid != "valid") {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    return;
                }

                var functionName = self.bodyContentValue().substring(
                    self.bodyContentValue().lastIndexOf("FUNCTION") + 8,
                    self.bodyContentValue().lastIndexOf("(")
                );

                functionName = functionName.replace(/\s/g, '');

                if(self.transactionType() == 'EDIT' && selectedName != functionName){
                    app.messages.push({
                        severity: "error",
                        summary: self.labels().functionNameErrMsg
                    });
                    return;
                }
                

                if(self.transactionType() == 'ADD' && self.deptObservableArray().find(obj => obj.name == functionName)){
                    app.messages.push({
                        severity: "error",
                        summary: self.labels().functionNameExistsErrMsg,
                        autoTimeout: 5000
                    });
                    return;
                }
                    
                document.querySelector("#operationDialog").close();
                app.loading(true);

                var payload = {
                    name: functionName ? functionName : "",
                    bodyContent: self.bodyContentValue() ? self.bodyContentValue() : "",
                    createdBy: app.personDetails().personId,
                    updatedBy: app.personDetails().personId
                };

                if(self.transactionType() != 'ADD'){
                    payload.status = self.selectedRow().status ? self.selectedRow().status : "",
                    payload.id = self.selectedRow().id ? self.selectedRow().id : ""
                }

                await services.addGeneric("manageStoredFunctions/" + self.transactionType(), ko.toJSON(payload)).then(data => {
                    if (data == 'Success') {
                        self.getAllStoredFunctions();
                        app.messages.push({
                            severity: "confirmation",
                            summary: self.labels().dataAddedMsg,
                            autoTimeout: 0
                        });
                    } else {
                        failCbFn();
                    }
                }, {});

                app.loading(false);
            };
            var currentRow = null;
            self.tableSelectionListener = async function (event) {
                var data = event.detail;
                currentRow= data.currentRow;

                if (currentRow) {
                    self.selectedRow(self.deptObservableArray().find(obj => obj.id == currentRow['rowKey']));
                    self.isOperationBtnDisabled(false);
                }
            };

            self.contentDialogCreateBtn = function () {
                self.isContentAreaDisabled(false);
                self.isSubmitVisible(true);
                self.isDeleteMsgVisible(false);
                self.transactionType("ADD");
                self.operationDialogHeader(getTranslation("manageStoredFunctions.createFunLbl"));
                self.operationBtnLbl(getTranslation("lookUp.submit"));
                self.bodyContentValue("");
                document.querySelector('#operationDialog').open();
            };


            self.contentDialogDeleteBtn = async function () {
                if(!currentRow){
                    self.isOperationBtnDisabled(true);
                    return
                }
                self.isOperationBtnDisabled(true);
                self.isContentAreaDisabled(true);
                self.isSubmitVisible(true);
                self.isDeleteMsgVisible(true);
                self.transactionType("DELETE");
                self.operationDialogHeader(getTranslation("manageStoredFunctions.deleteFunLbl"));
                self.operationBtnLbl(getTranslation("labels.Delete"));
                $("#de-resubmit-btn").addClass("loading");
                self.bodyContentValue("");

                await services.getGenericAsync("manageStoredFunctions/" + self.selectedRow().id, {}).then(data => {
                    self.bodyContentValue(data);
                    document.querySelector('#operationDialog').open();
                }, failCbFn);

                $("#de-resubmit-btn").removeClass("loading");
                self.isOperationBtnDisabled(false);
            };

            self.contentDialogViewBtn = async function () {
                if(!currentRow){
                    self.isOperationBtnDisabled(true);
                    return
                }
                self.isOperationBtnDisabled(true);
                self.isContentAreaDisabled(true);
                self.isSubmitVisible(false);
                self.isDeleteMsgVisible(false);
                self.transactionType("VIEW");
                self.operationDialogHeader(getTranslation("manageStoredFunctions.viewFunLbl"));
                $("#ve-resubmit-btn").addClass("loading");
                self.bodyContentValue("");

                await services.getGenericAsync("manageStoredFunctions/" + self.selectedRow().id, {}).then(data => {
                    self.bodyContentValue(data);
                    document.querySelector('#operationDialog').open();
                }, failCbFn);

                $("#ve-resubmit-btn").removeClass("loading");
                self.isOperationBtnDisabled(false);
            };

            self.contentDialogEditBtn = async function () {
                if(!currentRow){
                    self.isOperationBtnDisabled(true);
                    return
                }
                selectedName = self.selectedRow().name;
                self.isOperationBtnDisabled(true);
                self.isContentAreaDisabled(false);
                self.isDeleteMsgVisible(false);
                self.isSubmitVisible(true);
                self.transactionType("EDIT");
                self.operationDialogHeader(getTranslation("manageStoredFunctions.editFunLbl"));
                self.operationBtnLbl(getTranslation("lookUp.submit"));
                $("#ee-resubmit-btn").addClass("loading");
                self.bodyContentValue("");

                await services.getGenericAsync("manageStoredFunctions/" + self.selectedRow().id, {}).then(data => {
                    self.bodyContentValue(data);
                    document.querySelector('#operationDialog').open();
                }, failCbFn);

                $("#ee-resubmit-btn").removeClass("loading");
                self.isOperationBtnDisabled(false);
            };


            self.searchStoredFunctions = async function () {

                self.isSearchBtnDisabled(true);
                $("#ee-search-btn").addClass("loading");

                var payload = {
                    name: self.functionNameSearchVal() ? '%' + self.functionNameSearchVal() + '%' : "%",
                    status: self.statusSearch()
                };


                await services.addGeneric("manageStoredFunctions/Search", ko.toJSON(payload)).then(data => {

                    if (data.length !== 0) {

                        self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(data, { idAttribute: 'id' })));

                    } else {
                        app.messages.push({
                            severity: "error",
                            summary: self.labels().noDataFound,
                            autoTimeout: 0
                        });
                    }

                    app.loading(false);
                }, failCbFn);


                self.isSearchBtnDisabled(false);
                $("#ee-search-btn").removeClass("loading");
            };


            self.handleAttached = function (info) {
                initTranslations();
                self.getAllStoredFunctions();
            };


            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });


            function initTranslations() {
                self.statusArr(app.getPaaSLookup('STORED_FUNCTION_STATUS'));

                self.labels({
                    placeHolder: getTranslation("labels.placeHolder"),
                    cancel: getTranslation("others.cancel"),
                    resetSearchLbl: getTranslation("common.resetSearch"),
                    unknownErrMsg: getTranslation("labels.unknownError"),
                    functionNameErrMsg: getTranslation("manageStoredFunctions.functionNameErrMsg"),
                    functionNameExistsErrMsg: getTranslation("manageStoredFunctions.functionNameExistsErrMsg"),
                    backActionLbl: getTranslation("approvalScreen.backAction"),
                    searchLbl: getTranslation("login.Search"),
                    deleteLbl: getTranslation("manageStoredFunctions.dropLbl"),
                    deleteConfirmLbl: getTranslation("manageStoredFunctions.deleteConfirmLbl"),
                    undoneDeleteConfirmLbl: getTranslation("manageStoredFunctions.undoneDeleteConfirmLbl"),
                    manageStoredFunctionsLbl: getTranslation("pages.manageStoredFunctionsLbl"),
                    bodyContentLbl: getTranslation("manageStoredFunctions.bodyContentLbl"),
                    resubmitBtnLbl: getTranslation("mailtemplate.resendLbl"),
                    refreshLbl: getTranslation("common.refresh"),
                    submit: getTranslation("lookUp.submit"),
                    noDataFound: getTranslation("common.checkResultSearch"),
                    nameLbl: getTranslation("labels.name"),
                    dataAddedMsg: getTranslation("logscreen.dataAddedMsg"),
                    dataErrMsg: getTranslation("logscreen.dataErrMsg"),
                    recieverLbl: getTranslation("mailtemplate.templateEmail"),
                    contentLbl: getTranslation("labels.content"),
                    createLbl: getTranslation("common.addBtnLbl"),
                    editLbl: getTranslation("others.edit"),
                    viewLbl: getTranslation("others.view"),
                    statusLbl: getTranslation("common.statusLBL"),
                    updatedDateLbl: getTranslation("common.updatedDateLbl"),
                    creationDateLbl: getTranslation("notification.creationDate")
                });


                self.columnArray([
                    {
                        "headerText": self.labels().nameLbl, "field": "name"
                    },
                    {
                        "headerText": self.labels().statusLbl, "field": "status"
                    },
                    {
                        "headerText": self.labels().creationDateLbl, "field": "creationDate"
                    },
                    {
                        "headerText": self.labels().updatedDateLbl, "field": "updatedDate"
                    }
                ]);
            }


        }

        return manageStoredFunctionsViewModel;
    }
);
