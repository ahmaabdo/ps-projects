define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services',
    'ojs/ojknockout', 'ojs/ojselectcombobox', 'ojs/ojpopup','ojs/ojpagingtabledatasource',
    'ojs/ojvalidationgroup', 'ojs/ojinputtext', 'ojs/ojtable', 'ojs/ojlabel'],
        function (oj, ko, $, app, services) {

            function elementEntryObservationViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                self.labels = ko.observable([]);

                self.EITCodeArr = ko.observableArray([]);
                self.statusArr = ko.observableArray([]);
                self.isResubmitBtnDisabled = ko.observable(true);
                self.isSearchBtnDisabled = ko.observable(false);
                self.fileContentValue = ko.observable("");
                self.eitCodeSearch = ko.observable("");
                self.personNumberSearch = ko.observable("");
                self.statusSearch = ko.observable("");
                self.ucmIdSearch = ko.observable("");
                self.groupValid = ko.observable();
                self.columnArray = ko.observableArray();
                self.selectedRow = ko.observable();
                self.deptObservableArray = ko.observableArray([]);
                self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'})));


                var failCbFn = function () {
                    app.loading(false);
                    app.messages.push({
                        severity: "error",
                        summary: self.labels().unknownErrMsg,
                        autoTimeout: 0
                    });
                };


                self.cancelButton = function () {
                    document.querySelector("#resubmitDialog").close();
                };

                self.resetSearch = function () {
                    self.eitCodeSearch('');
                    self.personNumberSearch('');
                    self.statusSearch('');
                    self.ucmIdSearch('');
                    self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'})));
                };

                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };

                self.refreshDataBtn = function () {
                    app.loading(true);
                    self.getAllElEntries();
                };


                self.getAllElEntries = async function () {
//                    app.loading(true);

                    await services.getGenericAsync("elementEntryObservation", {}).then(data => {
                        var eitCodeArr = [];

                        self.deptObservableArray([]);
                        if (data.length !== 0) {

                            for (var i in data) {

                                for (var j = 0; j < app.globalEitNameReport().length; j++) {
                                    if (app.globalEitNameReport()[j].value == data[i].eitCode) {
                                        data[i].eitCodeLBL = app.globalEitNameReport()[j].label;
                                    }
                                }

                                eitCodeArr.push({
                                    label: data[i].eitCodeLBL,
                                    value: data[i].eitCode
                                });
                            }

                            self.deptObservableArray(data);
                            self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'})));

                            self.EITCodeArr(eitCodeArr.filter((arr, index, s) =>
                                index === s.findIndex((t) => (t.value === arr.value))));

                        }

                        app.loading(false);
                    }, failCbFn);
                };


                self.submitAction = async function () {

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid") {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }

                    document.querySelector("#resubmitDialog").close();

                    var payload = {
                        id: self.selectedRow().id,
                        startDate: self.selectedRow().startDate ? self.selectedRow().startDate : "",
                        fileContent: self.fileContentValue() ? self.fileContentValue() : "",
                        filePath: self.selectedRow().filePath ? self.selectedRow().filePath : "",
                        fileName: self.selectedRow().fileName ? self.selectedRow().fileName : "",
                        ssType: self.selectedRow().ssType ? self.selectedRow().ssType : "",
                        ssId: self.selectedRow().ssId ? self.selectedRow().ssId : "",
                        ucmId: self.selectedRow().ucmId ? self.selectedRow().ucmId : "",
                        personNumber: self.selectedRow().personNumber ? self.selectedRow().personNumber : "",
                        eitCode: self.selectedRow().eitCode ? self.selectedRow().eitCode : ""
                    };


                    app.messages.push({
                        severity: "info",
                        summary: self.labels().submittedMsg
                    });

                    await services.addGeneric("elementEntryObservation/DELETE", ko.toJSON(payload)).then(data => {
                        if (data == 'Success') {
                            self.getAllElEntries();
                            app.messages.push({
                                severity: "confirmation",
                                summary: self.labels().dataAddedMsg
                            });
                        } else {
                            app.messages.push({
                                severity: "error",
                                summary: self.labels().dataErrMsg
                            });
                        }
                    }, {});

                    app.loading(false);
                };


                self.tableSelectionListener = async function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;

                    if (currentRow) {
                        self.selectedRow(self.deptObservableArray().find(obj => obj.id == currentRow['rowKey']));
                        self.selectedRow().status != 'Import Status = SUCCESS, Load Status = SUCCESS' ? self.isResubmitBtnDisabled(false) : self.isResubmitBtnDisabled(true);
                    }
                };

                self.resubmitBtnDialog = async function () {
                    self.isResubmitBtnDisabled(true);
                    $("#ee-resubmit-btn").addClass("loading");
                    self.fileContentValue("");

                    await services.getGenericAsync("elementEntryObservation/" + self.selectedRow().id, {}).then(data => {
                        self.fileContentValue(data);
                        document.querySelector('#resubmitDialog').open();
                    }, failCbFn);

                    $("#ee-resubmit-btn").removeClass("loading");
                    self.selectedRow().status != 'Import Status = SUCCESS, Load Status = SUCCESS' ? self.isResubmitBtnDisabled(false) : self.isResubmitBtnDisabled(true);
                };



                self.searchElementEntries = async function () {

                    self.isSearchBtnDisabled(true);
                    $("#ee-search-btn").addClass("loading");

                    var payload = {
                        eitCode: self.eitCodeSearch(),
                        personNumber: self.personNumberSearch(),
                        status: self.statusSearch(),
                        ucmId: self.ucmIdSearch()
                    };


                    await services.addGeneric("elementEntryObservation/Search", ko.toJSON(payload)).then(data => {

                        if (data.length !== 0) {

                            for (var i in data) {

                                for (var j = 0; j < app.globalEitNameReport().length; j++) {
                                    if (app.globalEitNameReport()[j].value == data[i].eitCode) {
                                        data[i].eitCodeLBL = app.globalEitNameReport()[j].label;
                                    }
                                }

                            }

                            self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(data, {idAttribute: 'id'})));

                        } else {
                            app.messages.push({
                                severity: "error",
                                summary: self.labels().noDataFound,
                                autoTimeout: 0
                            });
                        }

                        app.loading(false);
                    }, failCbFn);


                    self.isSearchBtnDisabled(false);
                    $("#ee-search-btn").removeClass("loading");
                };


                self.handleAttached = function (info) {
                    initTranslations();
                    self.getAllElEntries();
                };


                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });


                function initTranslations() {

                    self.statusArr(app.getPaaSLookup('ELEMENT_ENTRY_STATUS'));

                    self.labels({
                        placeHolder: getTranslation("labels.placeHolder"),
                        cancel: getTranslation("others.cancel"),
                        resetSearchLbl: getTranslation("common.resetSearch"),
                        eitCodeLbl: getTranslation("manageOnlineHelp.eitCode"),
                        selfServiceLbl: getTranslation("validation.eitCode"),
                        unknownErrMsg: getTranslation("labels.unknownError"),
                        backActionLbl: getTranslation("approvalScreen.backAction"),
                        searchLbl: getTranslation("login.Search"),
                        elementEntryLbl: getTranslation("pages.elementEntryObservation"),
                        processIdLbl: getTranslation("elementEntry.processIdLbl"),
                        fileContentLbl: getTranslation("elementEntry.fileContentLbl"),
                        parentUcmId: getTranslation("elementEntry.parentUcmId"),
                        resubmitDialogMsg: getTranslation("elementEntry.resubmitDialogMsg"),
                        resubmitBtnLbl: getTranslation("common.resubmit"),
                        refreshLbl: getTranslation("common.refresh"),
                        submit: getTranslation("lookUp.submit"),
                        noDataFound: getTranslation("common.checkResultSearch"),
                        ucmIdLbl: getTranslation("elementEntry.ucmIdLbl"),
                        ssIdLbl: getTranslation("elementEntry.ssIdLbl"),
                        submittedMsg: getTranslation("elementEntry.submittedMsgNofity"),
                        dataAddedMsg: getTranslation("elementEntry.dataAddedMsg"),
                        dataErrMsg: getTranslation("elementEntry.dataErrMsg"),
                        personNumberLbl: getTranslation("common.PersonNumber"),
                        statusLbl: getTranslation("common.statusLBL"),
                        advancedLeaveLbl: getTranslation("payroll.advancedLeave"),
                        createtionDateLbl: getTranslation("notification.creationDate")
                    });

                    self.columnArray([
                        {
                            "headerText": self.labels().selfServiceLbl, "field": "eitCodeLBL"
                        },
                        {
                            "headerText": self.labels().processIdLbl, "field": "processId"
                        },
                        {
                            "headerText": self.labels().ucmIdLbl, "field": "ucmId"
                        },
                        {
                            "headerText": self.labels().ssIdLbl, "field": "ssId"
                        },
                        {
                            "headerText": self.labels().personNumberLbl, "field": "personNumber"
                        },
                        {
                            "headerText": self.labels().statusLbl, "field": "status"
                        },
                        {
                            "headerText": self.labels().advancedLeaveLbl, "field": "advancedLeave"
                        },
                        {
                            "headerText": self.labels().parentUcmId, "field": "parentUcmId"
                        },
                        {
                            "headerText": self.labels().createtionDateLbl, "field": "creationDate"
                        }
                    ]);
                }


            }

            return elementEntryObservationViewModel;
        }
);
