define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'config/buildScreen', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojknockout-validation', 'ojs/ojlabel', 'ojs/ojtable','ojs/ojtable'],
        function (oj, ko, $, app, commonUtil, services, buildScreen, ArrayDataProvider) {
            function elementEntrySummaryViewModel() {
                //Vars
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;
                var positionObj;

                //Observables
                self.lang = "";
                self.serchableFeieldName = [];
                self.sercheScreen = [];
                self.Arrs = {};
                self.ListSegment = [];
                self.highlightChars = [];
                self.filter = ko.observable();
                self.columnArray = ko.observableArray();
                self.selectedRowKey = ko.observable();
                self.messages = ko.observable();
                self.unknownErrMsg = ko.observable();
                self.selectedIndex = ko.observable();
                self.addSummaryLbl = ko.observable();
                self.eitCodeLbl = ko.observable();
                self.editLbl = ko.observable();
                self.typeOfActionLbl = ko.observable();
                self.elementNameLbl = ko.observable();
                self.elementEntryLbl = ko.observable();
                self.absencesLbl = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.ViewLbl = ko.observable();
                self.backActionLbl = ko.observable();
                self.DeleteLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.errorMessage=ko.observable();
                self.deleteMessageLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.BtnDeletedisabled=ko.observable(true);
                self.BtnUpdatedisabled = ko.observable(true);
                self.tempSummaryObservableArray = [];
                self.summaryObservableArray = [];
                self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));

                self.btnAddElementEntry = function () {
                    app.loading(true);
                    positionObj = {};
                    positionObj.type = "ADD";
                    oj.Router.rootInstance.store(positionObj);
                    oj.Router.rootInstance.go('elementEntryAdd');
                };

                self.btnUpdateElementEntry = function () {
                    app.loading(true);
                    positionObj = {};
                    positionObj.type = "EDIT";
                    oj.Router.rootInstance.store(positionObj);

                    if (self.tempSummaryObservableArray[self.selectedIndex()] != null) {
                        self.tempSummaryObservableArray[self.selectedIndex()].type = "EDIT";
                        localStorage.removeItem("elementEntryEditedArray");
                        localStorage.setItem("elementEntryEditedArray", JSON.stringify(self.tempSummaryObservableArray[self.selectedIndex()]));
                        oj.Router.rootInstance.go("elementEntryAdd");
                    } else {
                        self.messages([{
                                severity: 'error',
                                summary: self.unknownErrMsg(),
                                autoTimeout: 5000
                            }]);
                    }
                };

              self.btnDeleteElementEntry = function () {
                 document.querySelector("#NoDialog").open();
              
              };

                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                };
                 self.SubmitButtonDialog = function () {
                    var id=self.tempSummaryObservableArray[self.selectedIndex()].id;
                  
                   var getElementEntryCbFn=function(data){
                        self.BtnUpdatedisabled(true);
                        self.BtnDeletedisabled(true);    
                        self.getElementEntries();
                        document.querySelector("#NoDialog").close();
                   };
           
                   var serviceName="elementEntrySetup/deleteElementEntry/"+id;
                   services.getGeneric(serviceName,{}).then(getElementEntryCbFn, app.failCbFn);
                };




                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;

                    if (currentRow != null) {
                        self.selectedRowKey(currentRow['rowKey']);
                        self.selectedIndex(currentRow['rowIndex']);
                        self.BtnDeletedisabled(false);
                        self.BtnUpdatedisabled(false);
                    } else {
                         self.BtnDeletedisabled(true);
                         self.BtnUpdatedisabled(true);
                    }
                };


                self.handleValueChanged = function () {
                    self.highlightChars = [];
                    var deptArray = [];
                    var i, id;
                    var filter = document.getElementById('filter').rawValue;

                    if (typeof filter !== 'undefined') {
                        if (filter.length == 0) {
                            self.clearClick();
                            return;
                        }
                    }

                    for (i = self.summaryObservableArray.length - 1; i >= 0; i--) {
                        id = self.summaryObservableArray[i].DepartmentId;
                        Object.keys(self.summaryObservableArray[i]).forEach(function (field) {
                            if (typeof filter !== 'undefined') {
                                if (typeof self.summaryObservableArray[i].elementName !== 'undefined') {
                                    if (self.summaryObservableArray[i].elementName !== null) {
                                        if (self.summaryObservableArray[i].elementName.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0) {

                                            self.highlightChars[id] = self.highlightChars[id] || {};
                                            self.highlightChars[id][field] = getHighlightCharIndexes(filter, self.summaryObservableArray[i].elementName);

                                            if (deptArray.indexOf(self.summaryObservableArray[i]) < 0) {
                                                deptArray.push(self.summaryObservableArray[i]);
                                                self.tempSummaryObservableArray = deptArray;
                                                self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tempSummaryObservableArray, {idAttribute: 'id'})));
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }

                    function getHighlightCharIndexes(highlightChars, text) {
                        var highlightCharStartIndex = text.toString().toLowerCase().indexOf(highlightChars.toString().toLowerCase());
                        return {startIndex: highlightCharStartIndex, length: highlightChars.length};
                    }
                };

                self.clearClick = function (event) {
                    self.BtnUpdatedisabled(true);
                    self.BtnDeletedisabled(true);
                    self.filter('');
                    self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));
                    self.highlightChars = [];
                    document.getElementById('filter').value = "";
                    return true;
                };

                self.highlightingCellRenderer = function (context) {
                    var id = context.row.eitCode;
                    var startIndex = null;
                    var length = null;
                    var field = null;
                    if (context.columnIndex === 0) {
                        field = 'eitCode';
                    } else if (context.columnIndex === 1) {
                        field = 'typeOfActionLbl';
                    } else if (context.columnIndex === 2) {
                        field = 'elementName';
                    }

                    if (context.row[field] !== null) {
                        if (typeof context.row[field] !== 'undefined') {
                            var data = context.row[field].toString();

                            if (self.highlightChars[id] != null && self.highlightChars[id][field] != null) {
                                startIndex = self.highlightChars[id][field].startIndex;
                                length = self.highlightChars[id][field].length;
                            }

                            if (startIndex != null && length != null) {
                                var highlightedSegment = data.substr(startIndex, length);
                                data = data.substr(0, startIndex) + '<b>' + highlightedSegment + '</b>' + data.substr(startIndex + length, data.length - 1);
                            }
                            $(context.cellContext.parentElement).append(data);
                        }
                    }
                };

                self.getElementEntries = function () {
                    var getAllElementsCBF = function (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].typeOfAction === 'ElementEntry') {
                                data[i].typeOfActionLbl = self.elementEntryLbl();
                            } else {
                                data[i].typeOfActionLbl = self.absencesLbl();
                            }
                        }
                        if (data.length !== 0) {
                            self.tempSummaryObservableArray = data;
                            self.summaryObservableArray = data;
                            self.clearClick();
                        } else {
                            self.messages([{
                                    severity: 'error',
                                    summary: self.unknownErrMsg(),
                                    autoTimeout: 5000
                                }]);
                        }
                    };
                    var failCbFn = function () {
                        self.messages([{
                                severity: 'error',
                                summary: self.unknownErrMsg(),
                                autoTimeout: 5000
                            }]);
                    };
                    services.getGeneric(commonUtil.allElementEntries, {}).then(getAllElementsCBF, failCbFn);
                };

                if (app.getLocale() == 'ar') {
                    self.lang = "AR";
                } else {
                    self.lang = "US";
                }

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getElementEntries();
                    self.EitCode = oj.Router.rootInstance.retrieve();
                };
               
                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }

                });

                function initTranslations() {
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.eitCodeLbl(getTranslation("notification.eitCode"));
                    self.typeOfActionLbl(getTranslation("validation.typeOFAction"));
                    self.elementNameLbl(getTranslation("labels.elementName"));
                    self.elementEntryLbl(getTranslation("pages.elementEntry"));
                    self.absencesLbl(getTranslation("pages.absences"));
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.editLbl(getTranslation("others.edit"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.unknownErrMsg(getTranslation("labels.unknownError"));
                    self.ViewLbl(getTranslation("labels.View"));
                    self.DeleteLbl(getTranslation("labels.Delete"));
                    self.errorMessage(getTranslation("labels.errorMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.deleteMessageLbl(getTranslation("labels.deleteElementEntryMessage"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));

                    self.columnArray([
                        {
                            "headerText": self.eitCodeLbl(), renderer: self.highlightingCellRenderer

                        },
                        {
                            "headerText": self.typeOfActionLbl(), renderer: self.highlightingCellRenderer

                        },
                        {
                            "headerText": self.elementNameLbl(), renderer: self.highlightingCellRenderer
                        }
                    ]);

                }
                initTranslations();
            }
            return elementEntrySummaryViewModel;
        });
