/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'config/buildScreen', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup', 'ojs/ojformlayout',
    'ojs/ojtimezonedata', 'ojs/ojdatetimepicker', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojfilepicker','ojs/ojlabelvalue'],
        function (oj, ko, $, app, services, commonhelper, buildScreen) {

            function dynamicOperationScreenViewModel() {
                var self = this;
                self.showForm = ko.observable();
                var Typs = ['date', 'date', 'number'];
                var isErrMsg;
                self.pageMode = ko.observable();
                self.eit_id = ko.observable();
                self.validationMsg = ko.observable();
                self.nextBtnShow = ko.observable(true);
                self.showAttachment = ko.observable();
                var Keys = [];
                var PaaSKeys = [];
                self.EitName = ko.observableArray();
                self.validationMessagesArr = ko.observableArray();
                //self.validationMessagesProvider = ko.observableArray();
                self.validationMessagesProvider = new oj.ArrayDataProvider(self.validationMessagesArr);
                self.eit_n = ko.observable();
                self.selfServiceName = ko.observable();
                self.submitdraft = ko.observable(false);
                self.getStatus = ko.observable();
                self.status = ko.observable();
                self.columnArray1 = ko.observableArray();
                self.lang = "";
                self.ishide = ko.observable(false);
                self.nextStepDisabled = ko.observable(false);
                self.roleOptionType = ko.observableArray([]);
                self.workflowdisabled = ko.observable(false);
                self.errorMessageAttachSize=ko.observable();
                const nextStepBtnStyle = {
                    showAsEnabled() {
                        try {
                            $(".next-step-btn").removeClass("loading");
                            self.nextStepDisabled(false);
                        } catch (e) {
                        }
                    },
                    showAsDisabled() {
                        try {
                            self.nextStepDisabled(true);
                            $(".next-step-btn").addClass("loading");
                        } catch (e) {
                        }
                    }
                };
                var disabeld = [];
                var VALUES = [];
                var whenChangeValue = [];
                var actionQeuery = [];
                self.actionModelsArr = [];
                self.positionName = ko.observable();
                //When Change Any Feield Must go to for chech if this value Are avilable in action Model 
                //action Model Must Contains array of object evrey object contain 
                //Segment Name set value into (value Name )
                //Array of flex observable that include in this Action (on change must update values )
                //and str qeurry 



                self.blookAction = false;
                self.dataArray = [{value: 'zz', label: 'zz'}];
                self.validationCheck = ko.observable(true);
                self.paasActions;
                //This Var used for save paas Actions (Hide , Enabeled ) Feield 
                self.attch = ko.observable();
                var key = 'dynamicKey';
                var key2 = 'dynamicKey2';
                self.isDisabledx = {};
                var ArrKeys = [];
                self.Arrs = {};
                self.model = {};
                self.PaaSmodel = {};
                self.approvlModel = {};
                self.actionAvilable = ko.observable(true);
                var tempObject;
                self.elementEntryArr = ko.observableArray([]);
                function getElementEntryByEitCode() {
                    var EitCode = self.EitCode;
                    var getValidGradeCBF = function (data) {
                      
                        self.elementEntryArr(data);


                    };

                    services.getGeneric("elementEntrySetup/getElementEntry/" + self.EitCode).then(getValidGradeCBF, self.failCbFn);
                }

                // Start add attachment


                var dataFiles = {};
                self.koArray = ko.observableArray([]);
                //create file upload data provider
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.acceptArr = ko.pureComputed(function () {
                    var accept = self.acceptStr();
                    return accept ? accept.split(",") : [];
                }, self);


                // End add attachment
         
        
        
        self.employeeNameVal = ko.observable();
        self.locationVal = ko.observable();
        self.employeeNumberVal = ko.observable();
        self.organizationVal = ko.observable();
        self.hireDateVal = ko.observable();
        self.jobVal = ko.observable();
        self.gradeVal = ko.observable();
        self.positionVal = ko.observable();
        
        self.resumeAttachmentLbl=ko.observable();
        self.viewAttachmentBtnVisible=ko.observable(true);
        self.trainVisible=ko.observable(true);
        self.attachmentBtnVisible=ko.observable();
        self.removeBtnLbl=ko.observable();
        self.viewLbl=ko.observable();
        self.AttachmentsBtnVisible=ko.observable();
        self.UploadFileLbl=ko.observable();
        self.fileUploadStatusLbl=ko.observable();
        self.dataProviderAttachment=ko.observable();
        self.selectedItemsAttachment=ko.observable();
        var dataFiles = {};
       self.koArray = ko.observableArray([]);
       //create file upload data provider
       self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
       self.attachmentId = 0;
       self.selectedItemsAttachment = ko.observableArray([]);
       self.attachmentViewerData = ko.observable();
       self.acceptArr = ko.pureComputed(function () {
           var accept = self.acceptStr();
           return accept ? accept.split(",") : [];
       }, self);   
                
                 var cloneObject = function (object) {
                            var clone ={};
                            for( var key in object ){
                                if(object.hasOwnProperty(key)) //ensure not adding inherited props
                                    clone[key]=object[key];
                            }
                            return clone;
                        };
                        
                self.handleAttached = function (info) {
                    self.showForm(app.showForm());
                    self.workflowdisabled(false);
                     self.showAttachment(false);
                    self.EitName(app.globalEitNameReport());
                    app.loading(false);
                    var EitCodeLable;
                    self.paasActions = [];
                    self.validationMessagesArr([]);
                    //self.getPositionrole();
                    self.roleOptionType(rootViewModel.roleOptionType());
                    
//                    self.prevPram = oj.Router.rootInstance.retrieve();
                    self.prevPram = JSON.parse(sessionStorage.getItem("StoreObject"));
                    sessionStorage.removeItem("StoreObject");
                    self.EitCode = self.prevPram.eitCoed;
                    getElementEntryByEitCode();
                     for (var i = 0; i < app.globalEitNameReport().length; i++) {
                         if( app.globalEitNameReport()[i].value===self.EitCode){
                             EitCodeLable=app.globalEitNameReport()[i].label;
                         }
                         
                     }
                   
                   
                    var Eit_n = searchArray(self.EitCode, self.EitName());
                    self.selfServiceName(Eit_n);
                     
                    self.checkSubmitUser = ko.observable();
                    self.personDetails = ko.observableArray();
                    if (self.prevPram.checkUser === "employee") {
                        self.checkSubmitUser("employee");
                        self.personDetails(app.personDetails());
                        self.eit_n(Eit_n);

                    } else if (self.prevPram.checkUser === "Specialist") {
                        self.checkSubmitUser("Specialist");
                        self.personDetails(app.globalspecialistEMP());
                        self.eit_n(Eit_n + " Employee name :" + self.personDetails().displayName + " Employee Number: " + self.personDetails().personNumber);
                    }
                    self.setUrl(self.EitCode);
                    self.employeeNameVal(self.personDetails().displayName);
                    self.locationVal(self.personDetails().employeeLocation);
                    self.employeeNumberVal(self.personDetails().personNumber);
                    self.organizationVal(self.personDetails().departmentName);
                    self.hireDateVal(self.personDetails().hireDate);
                    self.jobVal(self.personDetails().jobName);
                    self.gradeVal(self.personDetails().grade);
                    self.positionVal(self.personDetails().positionName);

//    var getActionsCBFN= function (data){
//       if (!data.length){
//           data = [data]
//       }
//        self.paasActions = data ; 
//    }; 
//       self.validationActionModel = {
//                    eitCode:self.EitCode
//                };
//    var actionService = "validation/action";
//    services.searcheGeneric(actionService, self.validationActionModel).then(getActionsCBFN, app.failCbFn);

                    self.createMessage = function (summary) {
                        return {
                            severity: 'error',
                            summary: summary
                           
                        };
                    };
                    ArrKeys = [];
                    self.Arrs = {};
                    self.EitName(app.globalEitNameReport());
                    self.currentStepValue('stp1');

                    self.status(self.prevPram.status);
                    self.pageMode(self.prevPram.pageMode);
                    self.eit_id(self.prevPram.id);
                    self.actionModelsArr = [];
                    self.dependentListModelsArr = [];
//                    var Eit_n = searchArray(self.EitCode, self.EitName());
//                    self.eit_n(Eit_n);
                    disabeld = [];
                    self.isDisabledx = {};
                    self.selectedPage(self.pageMode());

                    if (app.getLocale() === 'ar') {
                        self.lang = "AR";
                       
                    } else {
                        
                        self.lang = "US";
                        
                    }
                     var getReportCbFn = function (data) {
     
                         if (!data || data.length === 0)
                            {
                               //  self.disableHelpBtn(true);
                                self.oprationMessage(self.defaultConfirmMessage());
                                self.draftOprationMessage(self.defaultConfirmMessage());
                                return;
                            }
                        
                            if (app.getLocale() === 'ar') {
                            
                             self.oprationMessage(data[0].selfServConfMessageAr);
                             self.draftOprationMessage(data[0].selfServDraftMessageAr);

                            }else {
                               self.oprationMessage(data[0].selfServConfMessageEn);
                               self.draftOprationMessage(data[0].selfServDraftMessageEn); 

                            }
                            

                    };
                   
                    var serviceName = "ManageSelfSerivce/"+self.EitCode;
                    services.getAllSummary(serviceName).then(getReportCbFn, app.failCbFn);
                    
                    
                    var segments = app.getEITSegments(self.EitCode);
                    self.getEit(segments);

                    self.getLocale = function () {
                        return oj.Config.getLocale();


                    };
                    if (self.getLocale() === "ar") {
                        $('.approval-card-body').removeClass('app-crd-bdy-border-en');
                        $('.approval-card-body').addClass('app-crd-bdy-border-ar');
                        $('.app-crd-type').removeClass('app-card-for-en');
                        $('.app-crd-type').addClass('app-card-for-ar');
                        $('.blockquote-footer').removeClass('app-card-for-en');
                        $('.blockquote-footer').addClass('app-card-for-ar');

                    } else {
                        $('.approval-card-body').removeClass('app-crd-bdy-border-ar');
                        $('.approval-card-body').addClass('app-crd-bdy-border-en');
                        $('.app-crd-type').removeClass('app-card-for-ar');
                        $('.app-crd-type').addClass('app-card-for-en');
                        $('.blockquote-footer').removeClass('app-card-for-ar');
                        $('.blockquote-footer').addClass('app-card-for-en');
                    }

                };
                self.getEit = function (data) {
                    tempObject = data;
                    if (tempObject.length) {

                    } else {
                        tempObject = [tempObject];
                    }
                    Keys = [];
                    var Iterater = 0;
                    for (var i = 0; i < tempObject.length; i++) {
                        //Build Model   
                         if (self.pageMode() == "EDIT" || self.pageMode() == "VIEW"){
                        if(tempObject[i].DESCRIPTION=="referenceNumber"){
                           tempObject[i].IN_FIELD_HELP_TEXT=null;
                        }}
                        Keys.push(tempObject[i].DESCRIPTION);
                        if (Keys[i] == "dummy") {
                            self.model[Keys[i]] = ko.observable(new Date().getTime());
                        } else {
                            self.model[Keys[i]] = ko.observable();
                            if (tempObject[i].IN_FIELD_HELP_TEXT && tempObject[i].IN_FIELD_HELP_TEXT != "PAAS") {
                                var actionModel = {
                                    reportName: tempObject[i].IN_FIELD_HELP_TEXT,
                                    whenChangeValue: [],
                                    segmentName: Keys[i]
                                };


                                actionModel.whenChangeValue = searchArrayContainValue(tempObject[i].IN_FIELD_HELP_TEXT, app.globalEITDefultValueParamaterLookup());
                                if (actionModel.whenChangeValue[0]) {
                                    actionModel.whenChangeValue = actionModel.whenChangeValue[0].split(',');
                                }
                              if(actionModel.whenChangeValue[0]==undefined){
                                  actionModel.whenChangeValue=[] ;
                              } 

                                self.actionModelsArr.push(actionModel);
                                for (var index = 0; index < actionModel.whenChangeValue.length; index++) {


                                    var reportPaylod = {"reportName": tempObject[i].IN_FIELD_HELP_TEXT};
                                    var completeParametar = true;
                                    for (var index = 0; index < actionModel.whenChangeValue.length; index++) {
                                        if (Object.keys(self.model).indexOf(actionModel.whenChangeValue[index]) != -1) {
                                            if (self.model[actionModel.whenChangeValue[index]]()) {
                                                reportPaylod[actionModel.whenChangeValue[index]] = self.model[actionModel.whenChangeValue[index]]();
                                            } else {
                                                completeParametar = false;
                                            }

                                        } else {
                                            if (self.personDetails()[actionModel.whenChangeValue[index]]) {
                                                reportPaylod[actionModel.whenChangeValue[index]] = self.personDetails()[actionModel.whenChangeValue[index]];
                                            } else {
                                                completeParametar = false;
                                            }

                                        }
                                    }
                                    var getReportValidationCBCF = function (iterator1, dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;
                                          if (dataSaaSQuery != '[]'){
                                                self.model[Keys[iterator1]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                                          }else{
//                                              self.model[Keys[iterator1]]('');
                                          }
                                      

                                    };
                                    if (completeParametar) {
                                        services.getGenericReportAsync(reportPaylod).then(getReportValidationCBCF.bind(data,i), app.failCbFn);
                                    }



                                }
                                if (actionModel.whenChangeValue.length == 0) {
                                    var reportPaylod = {"reportName": tempObject[i].IN_FIELD_HELP_TEXT};
                                    var getReportValidationCBCF = function (iterator2, dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;
                                        
                                      if(resultOfQueryObj!='[]'){
                                           self.model[Keys[iterator2]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                                      }
                                       

                                    };
                                    services.getGenericReportAsync(reportPaylod).then(getReportValidationCBCF.bind(data,i), app.failCbFn);
                                }
                                





                            } else if (tempObject[i].DEFAULT_value && tempObject[i].DEFAULT_value != "PAAS" && tempObject[i].DEFAULT_TYPE != 'C') {
                                var actionModel = {
                                    str: tempObject[i].DEFAULT_value,
                                    whenChangeValue: [],
                                    segmentName: Keys[i]
                                };
                                var str = tempObject[i].DEFAULT_value;
                                var flex = ":";
                                var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                var is_HaveFlex = true;
                                if (str.indexOf(':') == -1) {
                                    is_HaveFlex = false;
                                }
                                while (is_HaveFlex) {
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    actionModel.whenChangeValue.push(ModelName);
                                    strWitoutFlex.substring(1, strWitoutFlex.length);

                                    if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.length)) != -1) {
                                        str = str.replace(strWitoutFlex, self.personDetails().personId);
                                    } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, strWitoutFlex.length - 1)) != -1) {
                                        str = str.replace(strWitoutFlex, self.model[strWitoutFlex.substring(1, strWitoutFlex.length - 1)]());
                                    }

                                    is_HaveFlex = false;
                                    if (str.indexOf(":") == -1) {
                                        is_HaveFlex = false;
                                    }
                                }


                                if (str.includes('undefined')) {
                                } else {
                                    //Here Must Call Report Fpr This Perpus 
                                    var getReportValidationCBCF = function (iterator3, dataSaaSQuery) {

                                        var resultOfQueryObj = dataSaaSQuery;

                                        self.model[Keys[iterator3]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);

                                    };
                                    var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
                                    services.getGenericReportAsync(validationQueryReportModel).then(getReportValidationCBCF.bind(data,i), app.failCbFn);

                                }
                            } else if (tempObject[i].DEFAULT_TYPE == 'C') {
                                self.model[Keys[i]](tempObject[i].DEFAULT_value);
                            } else if (tempObject[i].IN_FIELD_HELP_TEXT == "PAAS") {
                                var defultUrl = getPaaSDefultValue(tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_CODE, tempObject[i].DESCRIPTION, app.allPaaSDefultValuearry());
                                var getDefultPaaSCbfn = function (iterator4, data) {
                                    var jsonData = JSON.parse(data);
                                    self.model[Keys[iterator4]](jsonData[Object.keys(jsonData)[0]]);
                                };
                                
                                
                                
                                
                                
                                

                                services.getGenericAsync(defultUrl).then(getDefultPaaSCbfn.bind(data,i), app.failCbFn);

                            }

                        }
                        PaaSKeys.push(tempObject[i].APPLICATION_COLUMN_NAME);
                        self.PaaSmodel[PaaSKeys[i]] = ko.observable(self.model[Keys[i]]());
                        //end OF Build Model 
                        if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_NUMBER_10") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_VIEW_REPORT")&&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_VIEW_REPORT")&&  
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME")
                                ) {
                            if (tempObject[i].value_SET_TYPE == "X" || tempObject[i].value_SET_TYPE == "I") {
                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                var getReportCBCF = function (iterator5, data) {
                                    var tempObject2 = data;
                                    var arr = tempObject2;
                                    var arrays = [];
                                    for (var x = 0; x < arr.length; x++) {
                                        arrays.push({"value": arr[x].FLEX_value, "label": arr[x].value_DESCRIPTION});

                                    }

                                    self.Arrs[ArrKeys[iterator5]](arrays);


                                };
                                var lang = '';
                                if (localStorage.getItem("selectedLanguage") == 'ar') {
                                    lang = "AR";
                                } else {
                                    lang = "US";
                                }
                                var xx = {"reportName": "inDependentReport", "valueSet": tempObject[i].FLEX_value_SET_NAME,"langCode":lang};
                                services.getGenericReportAsync(xx).then(getReportCBCF.bind(data,Iterater), app.failCbFn);
                                Iterater = Iterater + 1;
                            } else if (tempObject[i].value_SET_TYPE == "Y" || tempObject[i].value_SET_TYPE == "D") {
                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();

                                Iterater = Iterater + 1;

                            } else if (tempObject[i].value_SET_TYPE == "F") {
                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                var val = "value";
                                var lbl = "label";
                                var q;
                                // var tempObject = data;
                                q = "select " + tempObject[i].ID_COL + " " + val + " ,  " + tempObject[i].value_COL + "  " + lbl
                                        + " From " + tempObject[i].APPLICATION_TABLE_NAME + "  ";
                                if (tempObject[i].ADDITIONAL_WHERE_CLAUSE) {
                                    q = q + tempObject[i].ADDITIONAL_WHERE_CLAUSE;
                                }

                                var str = q;

                                var flex = ":";
                                var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));

                                var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                var is_HaveFlex = true;
                                if (str.indexOf(':') == -1) {
                                    is_HaveFlex = false;
                                }
                                while (is_HaveFlex) {
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    actionModel.whenChangeValue.push(ModelName);
                                    str = str.replace(strWitoutFlex, self.personDetails().personId);
                                    if (str.indexOf(":") == -1) {
                                        is_HaveFlex = false;
                                    }
                                }




                                if (str.includes('undefined')) {
                                } else {
                                    //Here Must Call Report Fpr This Perpus 
                                    var getArrCBCF = function (iterator6, data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        if (tempObject2.length) {
                                            self.Arrs[ArrKeys[iterator6]](arr);
                                        } else {
                                            self.Arrs[ArrKeys[iterator6]]([arr]);
                                        }
                                    };
                                    services.getDynamicReport(str).then(getArrCBCF.bind(data,Iterater), app.failCbFn);

                                }
                                Iterater = Iterater + 1;
                            } else if (tempObject[i].FLEX_value_SET_NAME == "XXX_HR_DYNAMIC_COLUMN") {

                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();

                                var reportName = searchArray(tempObject[i].DESCRIPTION, app.globalEITReportLookup());

                                var reportParamter = searchArrayContainDescription(tempObject[i].DESCRIPTION, app.globalEITReportLookup());

                                var reportPaylad = {"reportName": reportName};
                                reportParamter = reportParamter[0].split(',');
                                for (var reportIndex = 0; reportIndex < reportParamter.length; reportIndex++) {
                                    if (Object.keys(self.model).indexOf(reportParamter[reportIndex]) != -1) {
                                        reportPaylad[reportParamter[reportIndex]] = self.model[reportParamter[reportIndex]]();
                                    } else {
                                        reportPaylad[reportParamter[reportIndex]] = self.personDetails()[reportParamter[reportIndex]];
                                    }

                                }
                                var getReportCBCF = function (iterator7, data) {
                                    var tempObject2 = data;
                                    var arr = tempObject2;
                                    if (tempObject2.length) {
                                        self.Arrs[ArrKeys[iterator7]](arr);
                                    } else {
                                        self.Arrs[ArrKeys[iterator7]]([arr]);
                                    }

                                };//personId
                                var lang = '';
                                if (localStorage.getItem("selectedLanguage") == 'ar') {
                                    lang = "AR";
                                } else {
                                    lang = "US";
                                }
                                var reportPaylad = {"reportName": reportName, "personId": self.personDetails().personId,"langCode":lang};
                                services.getGenericReportAsync(reportPaylad).then(getReportCBCF.bind(data,Iterater), app.failCbFn);
                                Iterater = Iterater + 1;

                            }



                        } else if (tempObject[i].FLEX_value_SET_NAME === "XXX_HR_EIT_ATTACHMENTS") {
                            self.ishide(true);
                            ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + "Arr");
                            self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                            Iterater = Iterater + 1;
                            self.showAttachment(true);
                        }

                        //Build Disabeld Model 
                        disabeld.push("isDisabled" + tempObject[i].DESCRIPTION);
                        if (tempObject[i].READ_ONLY_FLAG == "Y") {
                            self.isDisabledx[disabeld[i]] = ko.observable(true);
                        } else if (tempObject[i].READ_ONLY_FLAG == "N") {
                            self.isDisabledx[disabeld[i]] = ko.observable(false);
                        }
                        //End Of Disabeld Model 





                        Typs.push(tempObject[i].FLEX_value_SET_NAME);
                    }
                    buildScreen.buildScreen(tempObject, "xx", self.model, self.isDisabledx, self.dataArray);
                    self.actionAvilable(true);
                    if (self.pageMode() == "ADD") {
                        self.submitdraft(false);
                        self.nextBtnShow(true);
                        self.viewAttachmentBtnVisible(true);
                        self.trainVisible(true);
                        self.saveDraftBtnVisible(true);
                    }
                    if (self.pageMode() == "EDIT" || self.pageMode() == "VIEW") {
                        self.submitdraft(false);
                        self.saveDraftBtnVisible(false);
                        if (self.pageMode() == "EDIT" && self.status() == "DRAFT") {
                            self.submitdraft(true);
                            self.saveDraftBtnVisible(true);
                        }

                        self.nextBtnShow(true);

                        //self.status = ko.observable();
                        self.setModelValue();


                        if (self.pageMode() == "VIEW") {
                            self.submitdraft(false);
                            self.ishide(false);
                            self.nextBtnShow(false);
                            self.isDisabledView(false);
                            self.nextBtnVisible(false);
                            self.viewAttachmentBtnVisible(false);
                            self.trainVisible(false);
                            //self.currentStepValue("stp2")
                            self.setToDisabeld();
                        } else if (self.pageMode() == "EDIT") {
                            self.viewAttachmentBtnVisible(true);
                            self.trainVisible(true);   
                        }
                        if (self.pageMode() == "VIEW" || self.pageMode() == "EDIT") {
                            var attachmentView = function (data) {
                                self.koArray([]);
                                for (var i = 0; i < data.length; i++) {

//                                    var baseStr64 = data[i].attachment;
//                                    drawAttachment2(i, baseStr64);
                                    self.koArray.push({
                                        "id": data[i].id,
                                        "name": data[i].name,
                                        "data": data[i].attachment
                                    });

                                }

                                // imgElem.setAttribute('src', baseStr64);
                            };
                            services.getGenericAsync("attachment/" + self.eit_id().toString()).then(attachmentView, app.failCbFn);
//                            self.actionAvilable(false);

                        }
                    }
                    //Here Hide 

                };











                function drawAttachment2(count, baseStr64) {
                    //drawLabel(lblValue, div);
                    var input = document.createElement("img");

                    input.setAttribute('class', 'attClass' + count);
                    input.setAttribute('alt', "Image preview...");
                    input.setAttribute('height', "60");
                    input.setAttribute('width', "60");
                    input.setAttribute('src', baseStr64);
                    var parent = document.getElementById("attachedfile");

                    // return input ;
                    parent.appendChild(input);
                }
                self.setModelValue = function () {

                    for (var i = 0; i < Object.keys(self.model).length; i++) {
                        for (var j = 0; Object.keys(self.prevPram).length > j; j++) {
                            if (Object.keys(self.model)[i] == Object.keys(self.prevPram)[j]) {
                                //  break;
                                // This For Set Value In Observable From Prev Page 
                                //self.prevPram[Object.keys(self.prevPram)[j]] (Value We Set in Observable )
//                   // self.model[Object.keys(self.model)[i]] Observable Are Link To Screen 

                                self.model[Object.keys(self.model)[i]](self.prevPram[Object.keys(self.prevPram)[j]]);
                                break;
                            }
                        }
                    }
                    self.model.PersonExtraInfoId = self.prevPram.PersonExtraInfoId;
                    self.model.PersonId = self.prevPram.PersonId;
                };


                self.segmentChangedHandler = async function (event , data) {
                    //document.body.style.cursor='wait';
                    
                    var isEdge;
                    if (navigator.userAgent.indexOf('Edge') >= 0) {
                        isEdge = true;
                    } else {                       
                        isEdge = false;
                    }
                    if ((!isEdge&&rootViewModel.BrowserDetect.browser=='Chrome'&&typeof event.path[0].attributes.name!= 'undefined')||
                            (typeof event.target.attributes.name!= 'undefined' && rootViewModel.BrowserDetect.browser=='Firefox' )||isEdge) {
                       if(!isEdge&&rootViewModel.BrowserDetect.browser=='Chrome'){
                            var id = event.path[0].attributes.name.nodeValue;
                       }else if ( rootViewModel.BrowserDetect.browser=='Firefox' ){
                            var id =event.target.attributes.name.nodeValue ; 
                       }else if(isEdge){
                            id =event.target.attributes.name.value;
                       }
                       
                                 
                        var res = id.substr(6, id.length);

                        var modelArr = Object.keys(self.model);
                        var realIndex = modelArr.indexOf(res);
                        var indexOfModel = modelArr.indexOf(res) + 1;
                        if(self.EitCode == 'XXX_HR_Identification_Letter' && realIndex == 2) {
                            var indexOfModel2 = modelArr.indexOf(res) + 5;
                        }
                        //  value_SET_TYPE
                        if (tempObject[indexOfModel].value_SET_TYPE == "Y" || tempObject[indexOfModel].value_SET_TYPE == "D") {

                            var getReportCBCF = function (data) {

                                var tempObject2 = data;
                                var arr = tempObject2;
                                var arrays = [];
                                if (arr.length) {
                                    for (var ind = 0; ind < arr.length; ind++) {
                                        arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION});
                                    }
                                } else {
                                    arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                }
                                //DESCRIPTION

                                self.Arrs[tempObject[indexOfModel].FLEX_value_SET_NAME + tempObject[indexOfModel].DESCRIPTION + "Arr"](arrays);


                            };
                            var xx = {"reportName": "DependentReport", "valueSet": tempObject[indexOfModel].FLEX_value_SET_NAME, "parent": self.model[modelArr[realIndex]]()};

                            await services.getGenericReportAsync(xx).then(getReportCBCF, app.failCbFn);
                        }
                         if(self.EitCode == 'XXX_HR_Identification_Letter' && realIndex == 2) {
                            if (tempObject[indexOfModel2].value_SET_TYPE == "Y" || tempObject[indexOfModel2].value_SET_TYPE == "D") {

                                var getReportCBCF = function (data) {

                                    var tempObject2 = data;
                                    var arr = tempObject2;
                                    var arrays = [];
                                    if (arr.length) {
                                        for (var ind = 0; ind < arr.length; ind++) {
                                            arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION});
                                        }
                                    } else {
                                        arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                    }
                                    //DESCRIPTION

                                    self.Arrs[tempObject[indexOfModel2].FLEX_value_SET_NAME + tempObject[indexOfModel2].DESCRIPTION + "Arr"](arrays);


                                };
                                var xx = {"reportName": "DependentReport", "valueSet": tempObject[indexOfModel2].FLEX_value_SET_NAME, "parent": self.model[modelArr[realIndex]]()};

                                await services.getGenericReportAsync(xx).then(getReportCBCF, app.failCbFn);
                            }
                         }

                    }
                    if ((!isEdge&&rootViewModel.BrowserDetect.browser=='Chrome'&&event.path[0]!= 'undefined')||
                            (rootViewModel.BrowserDetect.browser=='Firefox'&&event.originalTarget.classList[0]!='undefined' )||isEdge) {
                      if(!isEdge&&rootViewModel.BrowserDetect.browser=='Chrome'){
                           var curuntElement = event.path[0].classList[0];
                      }else if(rootViewModel.BrowserDetect.browser=='Firefox'){
                           var curuntElement = event.originalTarget.classList[0];
                      }else if(isEdge){
                          var curuntElement = event.srcElement.classList[0];
                      }
                       
                        var reportList = [];
                        //$('.' + curuntElement).find('[class*="loading"]').addClass("loading");

                        for (var index = 0; index < self.actionModelsArr.length; index++) {
                            var actionObject = self.actionModelsArr[index];
                            if (self.actionModelsArr[index].whenChangeValue.indexOf(curuntElement) != -1) {
                                var haveUndifindValue = false;
                                var reportPaylod = {"reportName": actionObject.reportName};
                                for (var whenCangeIndex = 0; whenCangeIndex < actionObject.whenChangeValue.length; whenCangeIndex++)
                                    if (Object.keys(self.model).indexOf(actionObject.whenChangeValue[whenCangeIndex]) != -1) {
                                        if (self.model[actionObject.whenChangeValue[whenCangeIndex]]() || self.model[actionObject.whenChangeValue[whenCangeIndex]]() == 0) {
                                            reportPaylod[actionObject.whenChangeValue[whenCangeIndex]] = self.model[actionObject.whenChangeValue[whenCangeIndex]]().toString();

                                        }else {
                                        haveUndifindValue = true;
                                    }
                                    } else if (Object.keys(self.personDetails()).indexOf(actionObject.whenChangeValue[whenCangeIndex]) != -1) {
                                        if (self.personDetails()[actionObject.whenChangeValue[whenCangeIndex]]) {
                                            reportPaylod[actionObject.whenChangeValue[whenCangeIndex]] = self.personDetails()[actionObject.whenChangeValue[whenCangeIndex]];
                                        }else {
                                        haveUndifindValue = true;
                                    }
                                    } else {
                                        haveUndifindValue = true;
                                    }
                                    if (!haveUndifindValue) {

                                if (reportPaylod && reportPaylod.reportName) {
                                    reportPaylod.segmentName = actionObject.segmentName;
                                    reportList.push({nameSeg: cloneObject(reportPaylod)});
                                }
                            }
                            }
                            


//                           if(str.includes('undefined')){
//                           }else {
//                               //Here Must Call Report Fpr This Perpus 
//                                        var getReportValidationCBCF = function (dataSaaSQuery) {
//                                            var resultOfQueryObj = dataSaaSQuery;
//                                           self.model[self.actionModelsArr[index].segmentName](resultOfQueryObj[Object.keys(dataSaaSQuery)[0]]); 
//                                            
//                                        }
//                                        var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
//                                        services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);
//                                   
//                           }
                        }
                        
                       
                        
                        var getReportDefaultDataBulk = function (dataSaaSQuery) {

                            var resultOfQueryObj = dataSaaSQuery;
//                                       errorcheck=resultOfQueryObj;
                           
                                
                                    for (var i = 0; i < resultOfQueryObj.length; i++) {
                                        try {
                                        var segmantkey = Object.keys(resultOfQueryObj[i])[0];
                                        var reportValue = Object.values(resultOfQueryObj[i][segmantkey])[0];
                                        if (reportValue != '[]') {
                                            self.model[segmantkey](reportValue);
                                           
                                        } else {
                                            try {
                                                for (var i = 0; i < tempObject.length; i++) {
                                                    if (tempObject[i].DESCRIPTION == actionObject.segmentName) {
                                                             
                                                        if(tempObject[i].FLEX_value_SET_NAME === "XXX_HR_PAAS_NUMBER") {
                                                            self.model[actionObject.segmentName](0);
                                                            break;
                                                        } else {
                                                            self.model[actionObject.segmentName]('');
                                                        }

                                                    }

                                                }

            //                                             self.model[actionObject.segmentName]('');    
                                            } catch (e) {
                                                
                                                self.model[actionObject.segmentName](0);
                                            }


                                        }
                                        
                                          
                                        } catch (e) {
                                           
                                   // self.model[actionObject.segmentName](0);
                                }
                                }
//                                     for (var i = 0; i < resultOfQueryObj.length; i++) {
                                           
//                                         if (tempObject[i].DESCRIPTION == actionObject.segmentName) {

//                                             if(tempObject[i].FLEX_value_SET_NAME === "XXX_HR_PAAS_NUMBER") {
//                                                 self.model[actionObject.segmentName](0);
//                                                 break;
//                                             } else {
//                                                 self.model[actionObject.segmentName]('');
//                                             }

//                                         }

//                                     }

//                                             self.model[actionObject.segmentName]('');    
                              
                            $('.' + curuntElement).find('[class*="loading"]').addClass("loading");

                        };
                        if(reportList.length > 0){
                            app.loading(true);
                            //$('input').closest('[class*="oj-disabled"]').find('input').addClass("loading");
                            nextStepBtnStyle.showAsDisabled();
                            await services.getGenericReportAsync(reportList).then(getReportDefaultDataBulk, app.failCbFn);
                        }
                        app.loading(false);
                        $('input').closest('[class*="oj-disabled"]').find('input').removeClass("loading");
                        //if(self.actionModelsArr[index].whenChangeValue)
                        nextStepBtnStyle.showAsEnabled();
                    }
                    //document.body.style.cursor='default';
                };


                //--Start PaaS Actions ------------------------
//                    var segmentsName = event.path[0].classList[0];
//                    for (var passActionsIndex = 0; passActionsIndex < self.paasActions.length; passActionsIndex++) {
//                        if ((self.paasActions[passActionsIndex].firstKeyType == "Application_Value" &&
//                                self.paasActions[passActionsIndex].applicationValueFamily == "EIT") ||
//                                (self.paasActions[passActionsIndex].secondKeyType == "Application_Value" &&
//                                        self.paasActions[passActionsIndex].secondapplicationValueFamily == "EIT")) {
//                            var firstValue;
//                            var sucendValue;
//                            var operation;
//
//                            if (self.paasActions[passActionsIndex].firstKeyType == "Application_Value") {
//                                if (self.paasActions[passActionsIndex].applicationValueFamily == "EIT") {
//                                    firstValue = self.model[data[i].applicationKey]();
//                                }
//                            }
//
//                        }
//
//
////           if(segmentsName == self.paasActions[passActionsIndex].eitSegment){
////              
////           }
//                    }
                //------------------End Of PaaS Actions -----------------------





                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'
                }));





                //----------------------------Standerd Screen -------------------------------------------
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.isDisabledView = ko.observable(true);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.draftOprationMessage = ko.observable();

                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.saveDraftBtnVisible=ko.observable(true);

                var Url;
                var positionObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance

                self.backAction = function () {
                    app.loading(true);
//                    if (oj.Router.rootInstance._navHistory.length > 1) {
//                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
//                    }
//                    oj.Router.rootInstance.go('dynamicSummary');
                    var cancelBack = {
                        EIT: self.EitCode,
                        returnToSymary: "backAction",
                        checkUser: self.checkSubmitUser()
                    };
                    oj.Router.rootInstance.store(cancelBack);
                    oj.Router.rootInstance.go('dynamicSummary');
                };
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };
             //-------------this Function for check  length ----------------------  
                self.lengthValidator = ko.computed(function () {
                    return [{
                            type: 'length',
                            options: {
                                min: 0, max: 100,
                                countBy: 'codeUnit',
                                hint: {
                                    inRange: 'value must have at least 0  characters but not more than 100'
                                },
                                messageSummary: {
                                    tooLong: ' Too many characters',
                                    tooShort: ' Too few characters'
                                },
                                messageDetail: {
                                    tooLong: 'Number of characters is too high. Enter at most 100 characters',
                                    tooShort: ' Number of characters is too low. Enter at least 0 characters.'
                                }
                            }
                        }];
                });
                
                //----------------END-----------------------------


                //-------------This Function For Update Train Step To previousStep------------------
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };

                self.nextStep = async () => {
                    nextStepBtnStyle.showAsDisabled();
                     self.workflowdisabled(true);
                  for (var i = 0 ; i<tempObject.length;i++){
                      if(tempObject[i].FLEX_value_SET_NAME == "XXX_HR_EIT_ATTACHMENTS"){
                          if(tempObject[i].REQUIRED_FLAG == "Y"){
                             
                              if(self.koArray().length==0){
                                   self.validationMessagesArr.push(self.createMessage(self.attachmentNotify()));
                                   nextStepBtnStyle.showAsEnabled();
                                  return ;
                              }else{
                                  self.model[tempObject[i].DESCRIPTION]('XXYY') ; 
                              }
                          }
                      }
                  }
                  
                  
                  
                  
                  
                  
                  //-----------End Of Attachment ----------------------------------------
                  
                  
                  
            
                    var jsonData = ko.toJSON(self.model);
                    var cx = JSON.stringify(jsonData);
                    isErrMsg = false;

                    cx.replace("{", "");
                    cx.replace("}", "");

                    self.validationSearch = {
                        eitCode: ko.observable(self.EitCode)
                    };

                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid" && tracker.valid) {
                        tracker.showMessages();
                        app.loading(false);
                        tracker.focusOn("@firstInvalidShown");
                        nextStepBtnStyle.showAsEnabled();
                        return;
                    }

                    //----------------------------Dynamic Validation ------------------------    

                    var EITCodeCbFn = function (data) {
                        var firstValue;
                        var sucendValue;
                        var operation;
                        var reportCallModel = {};
                        if (data) {



                            if (!data.length) {
                                self.validationCheck(false);
                            }
                            for (var i = 0; i < data.length; i++) {
                                reportCallModel = {};
                                //--------------First Key Value ----------------
                                if (data[i].firstKeyType == "Application_Value") {
                                    if (data[i].applicationValueFamily == "EIT") {
                                        firstValue = self.model[data[i].applicationKey]();
                                    } else if (data[i].applicationValueFamily == "Validation_Value") {
                                        //app.validation[data[i].validationValuesName]

                                        firstValue = app.validation[data[i].applicationKey];


                                    } else if (data[i].applicationValueFamily == "Personal_Information") {

                                        firstValue = self.personDetails()[data[i].applicationKey];

                                    }
                                } else if (data[i].firstKeyType == "Static") {
                                    firstValue = data[i].firstKeyValue;

                                } else if (data[i].firstKeyType == "SaaS") {
                                    if (data[i].saaSSource == "Report") {
                                        //reportName
                                        reportCallModel.reportName = data[i].reportName;
                                        //reportCallModel.reportName ="PersonAbsenceReport";
                                        var reportPramCbFn = function (dataReport) {

                                            var getReportCBCF = function (datac) {
                                                var valueObj = datac;
                                                firstValue = valueObj.value;

                                            };
                                            // reportCallModel[dataReport[0].parameter1]=

                                            var xx = {"reportName": data[i].reportName};

                                            if (dataReport[0].parameter1) {

                                                if (Object.keys(self.model).indexOf(data[i].parameter1Name) != -1) {
                                                    xx[dataReport[0].parameter1] = self.model[data[i].parameter1Name]();
                                                } else if (data[i].parameter1Name == 'P_STRUCTURE_CODE') {
                                                    xx[dataReport[0].parameter1] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter1] = self.personDetails()[data[i].parameter1Name];
                                                }


                                            }

                                            if (dataReport[0].parameter2) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter2Name) != -1) {
                                                    xx[dataReport[0].parameter2] = self.model[data[i].parameter2Name]();
                                                } else if (data[i].parameter2Name == 'EIT_CODE') {

                                                    xx[dataReport[0].parameter2] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter2] = self.personDetails()[data[i].parameter2Name];
                                                }
                                            }
                                            if (dataReport[0].parameter3) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter3Name) != -1) {
                                                    xx[dataReport[0].parameter3] = self.model[data[i].parameter3Name]();
                                                } else if (data[i].parameter3Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter3] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter3] = self.personDetails()[data[i].parameter3Name];
                                                }
                                            }
                                            if (dataReport[0].parameter4) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter4Name) != -1) {
                                                    xx[dataReport[0].parameter4] = self.model[data[i].parameter4Name]();
                                                } else if (data[i].parameter4Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter4] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter4] = self.personDetails()[data[i].parameter4Name];
                                                }

                                            }
                                            if (dataReport[0].parameter5) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter5Name) != -1) {
                                                    xx[dataReport[0].parameter5] = self.model[data[i].parameter5Name]();
                                                } else if (data[i].parameter5Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter5] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter5] = self.personDetails()[data[i].parameter5Name];
                                                }

                                            }
                                            services.getGenericReportNotAsync(xx).then(getReportCBCF, app.failCbFn);
                                        };
                                        services.getGeneric(commonhelper.reportPram + reportCallModel.reportName).then(reportPramCbFn, failCbFn);

                                    } else if (data[i].saaSSource == "Query") {
                                        var str = data[i].saaSQuery;
                                        str = str + "  ";
                                        var flex = ":";
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':') == -1) {
                                            is_HaveFlex = false;
                                        }
                                        while (is_HaveFlex) {

                                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                            var ModelName = strWitoutFlex;
                                            strWitoutFlex.substring(1, strWitoutFlex.length);
                                            ModelName = strWitoutFlex.substring(0, strWitoutFlex.indexOf(' '));
                                            if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {

                                                str = str.replace(ModelName, self.personDetails()[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]);
                                            } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, '')) != -1) {
                                                str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                                str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                            }
                                            is_HaveFlex = false;
                                            if (str.indexOf(":") == -1) {
                                                is_HaveFlex = false;
                                            }

                                        }
                                        if (str.substr('undefined ') != -1) {
                                        }


                                        var getReportValidationCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery;
                                            firstValue = resultOfQueryObj[Object.keys(resultOfQueryObj)[0]];
                                        };
                                        var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
                                        services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);
                                    }
                                } else if (data[i].firstKeyType == "PaaS") {
                                    var str = data[i].paaSQuery;
                                    var flex = ":";
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    var is_HaveFlex = true;
                                    if (str.indexOf(':') == -1) {
                                        is_HaveFlex = false;
                                    }

                                    while (is_HaveFlex) {

                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf(' '));
                                        strWitoutFlex.substring(1, strWitoutFlex.length);


                                        if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {
                                            str = str.replace(ModelName, self.personDetails().personId);
                                        } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {
                                            str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                        }
                                        // is_HaveFlex = false;
                                        if (str.indexOf(":") == -1) {
                                            is_HaveFlex = false;
                                        }
                                    }

                                    if (str.includes('unsdkadefined')) {
                                    } else {
                                        //Here Must Call Dynamic Web Service Function Fpr This Perpus 
                                        var getDynamicQueryCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery[0];

                                            firstValue = resultOfQueryObj[Object.keys(resultOfQueryObj)[0]];


                                        };
                                        var validationQueryReportModel = {"query": str};
                                        services.searcheGeneric("PeopleExtra/getQuery/", validationQueryReportModel).then(getDynamicQueryCBCF, app.failCbFn);

                                    }
                                }
                                operation = data[i].operation;
                                //-------Secound Key Value --------------------
                                if (data[i].secondKeyType == "Application_Value") {
                                    if (data[i].secondapplicationValueFamily == "EIT") {


                                        sucendValue = self.model[data[i].secondapplicationKey]();
                                    } else if (data[i].secondapplicationValueFamily == "Validation_Value") {
                                        //app.validation[data[i].validationValuesName]
                                        sucendValue = app.validation[data[i].secondapplicationKey];

                                    } else if (data[i].secondapplicationValueFamily == "Personal_Information") {
                                        sucendValue = self.personDetails()[data[i].secondapplicationKey];
                                    }
                                } else if (data[i].secondKeyType == "Static") {
                                    sucendValue = data[i].secondKeyValue;
                                } else if (data[i].secondKeyType == "SaaS") {
                                    if (data[i].secondsaaSSource == "Report") {
                                        reportCallModel.reportName = data[i].secondreportName;
                                        var reportPramCbFn = function (dataReport) {
                                            var getReportCBCF = function (datac) {
                                                var valueObj = datac;
                                                sucendValue = valueObj.value;
                                            };
                                            // reportCallModel[dataReport[0].parameter1]=

                                            var xx = {"reportName": data[i].secondreportName};

                                            if (dataReport[0].parameter1) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter1Name) != -1) {
                                                    xx[dataReport[0].parameter1] = self.model[data[i].secondparameter1Name]();
                                                } else {
                                                    xx[dataReport[0].parameter1] = self.personDetails()[data[i].secondparameter1Name];
                                                }


                                            }
                                            if (dataReport.parameter2) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter2Name) != -1) {
                                                    xx[dataReport[0].parameter2] = self.model[data[i].secondparameter2Name]();
                                                } else {
                                                    xx[dataReport[0].parameter2] = self.personDetails()[data[i].secondparameter2Name];
                                                }

                                            }
                                            if (dataReport.parameter3) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter3Name) != -1) {
                                                    xx[dataReport[0].parameter3] = self.model[data[i].secondparameter3Name]();
                                                } else {
                                                    xx[dataReport[0].parameter3] = self.personDetails()[data[i].secondparameter3Name];
                                                }

                                            }
                                            if (dataReport.parameter4) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter4Name) != -1) {
                                                    xx[dataReport[0].parameter4] = self.model[data[i].secondparameter4Name]();
                                                } else {
                                                    xx[dataReport[0].parameter4] = self.personDetails()[data[i].secondparameter4Name];
                                                }

                                            }
                                            if (dataReport.parameter5) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter5Name) != -1) {
                                                    xx[dataReport[0].parameter5] = self.model[data[i].secondparameter5Name]();
                                                } else {
                                                    xx[dataReport[0].parameter5] = self.personDetails()[data[i].secondparameter5Name];
                                                }

                                            }
                                            services.getGenericReportNotAsync(xx).then(getReportCBCF, app.failCbFn);
                                        };
                                        services.getGeneric(commonhelper.reportPram + reportCallModel.reportName).then(reportPramCbFn, failCbFn);
                                    } else if (data[i].secondsaaSSource == "Query") {
                                        var str = data[i].secondsaaSQuery;
                                        str = str + "  ";
                                        var flex = ":";
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':') == -1) {
                                            is_HaveFlex = false;
                                        }
                                        var xxxxx = 0;
                                        while (is_HaveFlex) {

                                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                            var ModelName = strWitoutFlex;
                                            strWitoutFlex.substring(1, strWitoutFlex.length);
                                            ModelName = strWitoutFlex.substring(0, strWitoutFlex.indexOf(' '));
                                            if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {

                                                str = str.replace(ModelName, self.personDetails()[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]);
                                            } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, '')) != -1) {
                                                str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                            }
                                            //    is_HaveFlex = false;
                                            if (str.indexOf(":") == -1) {
                                                is_HaveFlex = false;
                                            }
                                        }
                                        if (str.substr('undefined ') != -1) {
                                        }


                                        var getReportValidationCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery;
                                            sucendValue = resultOfQueryObj[Object.keys(resultOfQueryObj)[0]];
                                        };
                                        var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
                                        services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);
                                    }
                                } else if (data[i].secondKeyType == "PaaS") {
                                    var str = data[i].secondpaaSQuery;
                                    var flex = ":";
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    var is_HaveFlex = true;
                                    if (str.indexOf(':') == -1) {
                                        is_HaveFlex = false;
                                    }

                                    while (is_HaveFlex) {

                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf(' '));

                                        strWitoutFlex.substring(1, strWitoutFlex.length);


                                        if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {
                                            str = str.replace(ModelName, self.personDetails().personId);
                                        } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {
                                            str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                        }
                                        // is_HaveFlex = false;
                                        if (str.indexOf(":") == -1) {
                                            is_HaveFlex = false;
                                        }
                                    }

                                    if (str.includes('unsdesfinsed')) {
                                    } else {
                                        //Here Must Call Dynamic Web Service Function Fpr This Perpus 
                                        var getDynamicQueryCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery[0];

                                            sucendValue = resultOfQueryObj[Object.keys(resultOfQueryObj)[0]];


                                        };
                                        var validationQueryReportModel = {"query": str};
                                        services.searcheGeneric("PeopleExtra/getQuery/", validationQueryReportModel).then(getDynamicQueryCBCF, app.failCbFn);

                                    }
                                }
                                if (data[i].typeOFAction == "Error_Massage") {
                                    if (app.compare(firstValue, operation, sucendValue)) {
                                        self.validationCheck(true);
                                        isErrMsg = true;
                                        if (app.getLocale() == 'ar') {
                                           
                                            self.validationMessagesArr.push(self.createMessage(data[i].errorMassageAr));
                                        } else {
                                            
                                            self.validationMessagesArr.push(self.createMessage(data[i].errorMassageEn));
                                        }
                                    } else {
                                        //  self.validationMessagesArr([]);
                                        self.validationCheck(false);

                                    }
                                } else {
                                    isErrMsg = false;
                                }

                                if (data[i].saveResultToValidationsValues == "Yes") {
                                    app.validation[data[i].validationValuesName] = app.compare(firstValue, operation, sucendValue);

                                }


                            }
                        } else {
                            self.deptObservableArray([]);
                        }

                        if (self.validationCheck()) {
                            nextStepBtnStyle.showAsEnabled();
                            
                            return;
                        }

                        if (isErrMsg) {
                            nextStepBtnStyle.showAsEnabled();
                           
                            return;
                        }
//------------End Of Valilidationn -----------------
                        // app.loading(false);
                        var next = document.getElementById("train").getNextSelectableStep();
                        if (next != null)
                            self.currentStepValue(next);
                        nextStepBtnStyle.showAsEnabled();

                    };
                    
                    var failCbFn = function (err) {
                       
                        //  app.loading(true);
                        nextStepBtnStyle.showAsEnabled();
                    };

                    await services.searchGenericUncodeedAsync("validation/search", self.validationSearch).then(EITCodeCbFn, failCbFn);
                };


                self.setToDisabeld = function () {
                    for (var i = 0; i < Object.keys(self.isDisabledx).length; i++) {
                        self.isDisabledx[Object.keys(self.isDisabledx)[i]](true);
                    }
                };
                self.setToDefultDisabeld = function () {
                    for (var i = 0; i < tempObject.length; i++) {
                        if (tempObject[i].READ_ONLY_FLAG == "Y") {
                            self.isDisabledx[Object.keys(self.isDisabledx)[i]](true);
                        } else {
                            self.isDisabledx[Object.keys(self.isDisabledx)[i]](false);
                        }

                    }
                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        if (self.pageMode() == "EDIT" && self.status()== "DRAFT") {
                            self.saveDraftBtnVisible(true);
                        } else if (self.pageMode() == "ADD") {
                            self.saveDraftBtnVisible(true);
                        }
                        else{
                            self.saveDraftBtnVisible(false);
                        }
                        self.setToDisabeld();
                        self.viewAttachmentBtnVisible(false);

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);   
                        self.saveDraftBtnVisible(false);
                        if (self.pageMode() != "VIEW") {
                            self.setToDefultDisabeld();
                            self.viewAttachmentBtnVisible(true);
                            
                        }

                    }

                    return self.eit_n();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };

                self.submitButton2 = function () {
                    document.querySelector("#yesNoDialog2").open();
                };
                self.cancelButton2 = function () {
                    document.querySelector("#yesNoDialog2").close();
                };
                self.submitButton3 = function () {
                    document.querySelector("#yesNoDialog3").open();
                };
                self.cancelButton3 = function () {
                    document.querySelector("#yesNoDialog3").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {

                    if (self.currentStepValue() != 'stp2') {
//                        self.nextStep();
                        self.validationCheck(true);
                    }
                    if (self.validationCheck() === true) {
                        self.currentStepValue('stp1');
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        event.preventDefault();
                        nextStepBtnStyle.showAsEnabled();
                        self.nextStep();
                        return;
                    } 
//                    else if (self.validationCheck() === false) {
//                        self.currentStepValue('stp2');
//                    }


                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        nextStepBtnStyle.showAsEnabled();
                        return;
                    }
                };
                //-------------------End Of Function ---------------------------------------------
                //------------------ This Section For Call Web Service For Insert Or Update ---------

                //----------This Function To Set Url Call 
                self.setUrl = function (EitCode) {
//                   if(EitCode == "XXX_ABS_CREATE_DECREE"){                     
//                       //self.hidden("block");
//                    return rootViewModel.personDetails().absenceRequestUrl;
//                   }else if (EitCode == "XXX_ABS_UPDATE_DECREE") {
//                    return rootViewModel.personDetails().absenceUpdateRequestUrl;
//                   } else if (EitCode == "XXX_ABS_DELETE_DECREE") {
//                   return rootViewModel.personDetails().absenceCancelRequestUrl;
//                 }else if (EitCode =="XXX_EMP_MINISTRY_JOB"){
//                    return rootViewModel.personDetails().ministryJobForEmployee;
//                }else if (EitCode =="XXX_HR_BUSINESS_PAYMENT"){
//                    return rootViewModel.personDetails().businessMissionPaymentUrl;
//                }else if (EitCode =="XXX_HR_BUSINESS_MISSION"){
//                    return rootViewModel.personDetails().businessMissionRequestUrl;
//                }else if(EitCode =="XXX_OVERTIME_REQUEST"){
//                   return rootViewModel.personDetails().overTimeURL ;
//                }else if(EitCode =="XXX_EMPLOYMENT_CERTIFICATE"){
//                   return rootViewModel.personDetails().employeeCertificate ;
//                }
                    return self.personDetails().employeeURL[EitCode];
                };
                function oprationServiceRecord() {

                    var jsonData = ko.toJSON(self.model);
                    // var cx = JSON.stringify(jsonData);
                    self.model.url = self.setUrl(self.EitCode);
                    self.approvlModel.code = self.EitCode;
                    if (self.getStatus() == "draft"){
                        self.approvlModel.status = "DRAFT";
                    }
                    else{
                        self.approvlModel.status = "PENDING_APPROVED";
                    }
                    self.approvlModel.person_number = self.personDetails().personNumber;
                    self.approvlModel.person_id = self.personDetails().personId;
                    self.approvlModel.created_by = self.personDetails().personId;
                    self.approvlModel.managerName = self.personDetails().managerName;
                    self.approvlModel.managerOfManagerName = self.personDetails().managerOfMnagerName;
                    //self.approvlModel.creation_date = self.personDetails().personId;
                    self.approvlModel.line_manager = self.personDetails().managerId;
                    self.approvlModel.manage_of_manager = self.personDetails().managerOfManager;
                    self.approvlModel.url = self.setUrl(self.EitCode);
                    self.approvlModel.eit = JSON.stringify(jsonData);
                    self.approvlModel.personName = self.personDetails().displayName;
                    self.approvlModel.nationalIdentity = self.personDetails().nationalId;
                    self.approvlModel.eitLbl = JSON.stringify(self.buildModelWithLabel());
                    self.approvlModel.eit_name = self.selfServiceName();
                    self.approvlModel.assignmentNumber = self.personDetails().assignmentNumber;

                    if (self.pageMode() == "EDIT" || self.pageMode() == "draft") {
                        self.approvlModel.id = self.eit_id();
                    }
                    
                    for (var eitModelIndex = 0; eitModelIndex < tempObject.length; eitModelIndex++) {
                        self.approvlModel[tempObject[eitModelIndex].APPLICATION_COLUMN_NAME] = self.model[tempObject[eitModelIndex].DESCRIPTION]();
                    }
                    jsonData.id = self.eit_id();
                    
                    var jsonData = ko.toJSON(self.approvlModel);
                    var getValidGradeCBF = function (data) {
                        var attchArr = [];
                        for (var i = 0; i < self.koArray().length; i++) {
                            var preview = document.querySelector('.attClass' + i);
                            // self.image = ko.observable(preview.src);
                            if (self.pageMode() == "EDIT") {
                                att = {"SS_id": self.eit_id(), "attachment": self.koArray()[i].data, "name": self.koArray()[i].name};
                               
                            } else {
                                att = {"SS_id": data.id, "attachment": self.koArray()[i].data, "name": self.koArray()[i].name};
                            }

                            attchArr.push(att);

                        }
                        //attchArr

                        attJson = ko.toJSON(attchArr);

                        var addAttachedCbfn = function (data2) {
                            var Data = data;

                            self.disableSubmit(false);
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                app.loading(false);
                                // oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                                var cancelBack = {
                                    EIT: self.EitCode,
                                    returnToSymary: "backAction",
                                    checkUser: self.checkSubmitUser()
                                };
                                oj.Router.rootInstance.store(cancelBack);
                                oj.Router.rootInstance.go('dynamicSummary');

                            } else {
                                app.loading(false);
                                oj.Router.rootInstance.go('home');
                            }
                            //Add Call Here                         
                        };
                        
                       


                        services.addGeneric("attachment/" + self.pageMode() + "/", attJson).then(addAttachedCbfn, app.failCbFn);

                        count = 0;
                        
                        if(data.id == 0){
                            $.notify("Your request is not submitted successfully,Please Contact With Technical Support Team", "error");
                        }else{
                        var LastApproval = rootViewModel.isLastApprover(data.id, self.EitCode,true);
                        if (LastApproval&&self.approvlModel.status != "DRAFT") {
                                        var jsonBody = {
                                            trsId: data.id
                                        };
                                        var submitElement = function (data1) {
                                        };

                                        services.submitElementEntry(JSON.stringify(jsonBody)).then(submitElement, app.failCbFn);
                                    if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('home');
                            }
                        }
                    }
                    };
                
                   var approvalCondetion = app.getApprovalCondetion( self.EitCode,self.model) ; 
                    services.addGeneric("PeopleExtra/createOrUpdateEIT/" + self.pageMode()+'/'+approvalCondetion, jsonData).then(getValidGradeCBF, app.failCbFn);
                }
                ;
                self.buildModelWithLabel = function () {
                    var jsonDataLbl = self.model;
                    for (var i = 0; i < tempObject.length; i++) {
                        if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_NUMBER_10") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_VIEW_REPORT")&&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME")
                                ) {

                            jsonDataLbl[tempObject[i].DESCRIPTION + "Lbl"] = searchArray(self.model[tempObject[i].DESCRIPTION](), self.Arrs[tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"]());

                        }
                    }
                    jsonDataLbl = ko.toJSON(self.model);

                    return jsonDataLbl;
                };
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        app.loading(true);
                        self.disableSubmit(true);
                        self.getStatus("PENDING_APPROVED");
                        oprationServiceRecord();
                        return true;
                    }
                };

                self.commitRecord2 = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.getStatus("draft");
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();

                        return true;
                    }
                };

                self.commitRecord3 = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.pageMode("draft");
                        self.getStatus("PENDING_APPROVED");
                        app.loading(true);
                        self.disableSubmit(true);
                        oprationServiceRecord();

                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //--------------------Back Action Object --------------------
                function buildBackObject() {
                    var cancelBack = {
                        EIT: self.EitCode,
                        returnToSymary: "backAction",
                        checkUser: self.checkSubmitUser()
                    };
                    oj.Router.rootInstance.store(cancelBack);
                }
                //----------------------End---------------------------
                //-------------------Leave Page -----------------------------
                self.handleDetached = function (info) {
                    //  oj.Router.rootInstance.store(self.EitCode);
                    self.koArray([]);
                    buildBackObject();
                    self.isDisabledx = {};
                    self.model = {};

                };

                //-----------------attachment----------------------------//
                self.attachmentSelectListener = function (event) {
                   var files = event.detail.files;
                       for (var i = 0; i < files.length; i++) {
                           if (files.length > 0) {
                       //add the new files at the beginning of the list
                           if((files[i].size/1024/1024) > 2.5){
                               $.notify(self.errorMessageAttachSize(), "error");

                           }
                           else{
                           function getBase64(file) {
                               return new Promise((resolve, reject) => {
                                   const reader = new FileReader();
                                   reader.readAsDataURL(file);
                                   reader.onload = () => resolve(reader.result);
                                   reader.onerror = error => reject(error);
                               });
                           }
                           dataFiles.name = files[i].name;
                           dataFiles.id = i + 1;
                           getBase64(files[i]).then(function (data) {
                               dataFiles.data = (data);
                               self.attachmentId = self.attachmentId + 1;
                               dataFiles.id = (self.attachmentId);
//                        self.koArray.push(dataFiles);
                               self.koArray.push({
                                   "id": dataFiles.id,
                                   "name": dataFiles.name,
                                   "SS_id": self.eit_id(),
                                   "data": dataFiles.data
                               });
                               for(var r = 0; r < tempObject.length; r++){
                                   if(tempObject[r].DESCRIPTION == "attachment"){
                                       self.model[tempObject[r].DESCRIPTION]("YES");
                                   }
                               }
                               
                                    
                               
//                        self.approvalSetupModel.resumeAttachment(dataFiles.data);
                           }
                           );
                       }
                           }
               }
               };

                self.removeSelectedAttachment = function (event) {
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        self.koArray.remove(function (item)
                        {
                            return (item.id == value);
                        });
                        if(self.koArray().length == 0){
                        for(var r = 0; r < tempObject.length; r++){
                                   if(tempObject[r].DESCRIPTION == "attachment"){
                                       self.model[tempObject[r].DESCRIPTION]("No");
                                   }
                               }
                    }
                    });
                };
                function downloadPDF(payloadAttachData) {
                    var link = document.createElement("a");
                    var data = payloadAttachData.data;
                    link.setAttribute('href', data);
                    link.setAttribute('download', payloadAttachData.name);
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                self.openAttachmentViewer = function ()
                {
    
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        var data = self.koArray().find(e=>e.id == value);
                        downloadPDF(data);

                    });
                };
                //-----------------attachment----------------------------//

                //-------------------- view approvals ---------------------------
                self.approvalType = ko.observableArray([]);
                self.datacrad = ko.observableArray([]);
                self.roleName = ko.observableArray([]);

                self.workglow_Approval = function () {
                    var approvalCondetion = app.getApprovalCondetion(self.EitCode, self.model);
                    self.datacrad([]);
                    self.positionName("");
                    var eitcode = self.EitCode;

                    var getValidGradeCBF = function (data) {
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                self.approvalType.push(data[i].approvalType);
                                self.roleName.push(data[i].roleName);
                                //  data[i].approvalType = searchArray(data[i].approvalType, self.approvalType());
                                //  data[i].roleName = searchArray(data[i].roleName, self.roleName());


                                var notificationStatus, FYAValue, approvalTypeValue;
                                if (data[i].notificationType === "FYA") {
                                    notificationStatus = 'app-type-a';
                                    FYAValue = self.FYALabel();

                                } else {

                                    notificationStatus = 'app-type-i';
                                    FYAValue = self.FYILabel();
                                }
                                if (data[i].approvalType === "EMP") {
                                    approvalTypeValue = self.approvalTypeValueEmpLabel();
                                    self.positionName(self.personDetails().displayName);
                                } else if (data[i].approvalType === "LINE_MANAGER") {
                                    approvalTypeValue = self.approvalTypeValueMngLabel();
                                    self.positionName(self.personDetails().managerName);
                                } else if (data[i].approvalType === "LINE_MANAGER+1") {
                                    approvalTypeValue = self.approvalTypeValueLnMngLabel();
                                    self.positionName(self.personDetails().managerOfMnagerName);
                                } else if (data[i].approvalType === "POSITION") {
                                    approvalTypeValue = self.approvalTypeValuePositionLabel();
                                    for (var j = 0; j < self.roleOptionType().length; j++) {
                                        if (self.roleOptionType()[j].value == data[i].roleName)
                                        {
                                            self.positionName(self.roleOptionType()[j].label);
                                        }
                                    }

                                } else if (data[i].approvalType === "JOB_LEVEL") {
                                    approvalTypeValue = data[i].approvalType;
                                    self.positionName(data[i].personName);
                                } else if (data[i].approvalType === "AOR") {
                                    approvalTypeValue = data[i].approvalType;
                                    self.positionName(data[i].personName);
                                } else if (data[i].approvalType == "Special_Case") {
                                    approvalTypeValue = self.approvalTypeValueSpicalCaseLabel();
                                } else if (data[i].approvalType === "ROLES") {

                                    approvalTypeValue = "Role";
                                    for (var j = 0; j < app.allRoles().length; j++) {
                                if (app.allRoles()[j].ROLE_ID == data[i].roleName)
                                {

                                    self.positionName(app.allRoles()[j].ROLE_NAME);
                                }
                            }


                                }
                                self.datacrad.push({
                                    approvalType: approvalTypeValue,
                                    roleType: self.positionName(),
                                    FYA: FYAValue,
                                    status: notificationStatus
                                });

                            }
                        }
                        var Data = data;

                        self.dataSourceTB2(new oj.ArrayTableDataSource(Data));
                    };
                    self.lbl = ko.observable(self.approvaltype());
                    services.getGeneric(commonhelper.approvalSetupUrl + eitcode + '/' + approvalCondetion).then(getValidGradeCBF, app.failCbFn);
                    document.querySelector("#modalDialog1").open();
                };

                self.closeDialog = function () {
                    $("#modalDialog1").ojDialog("close");
                };
                
                self.getPositionrole = function () {
                    var getValidGradeCBF = function (data) {
                        $.each(data, function (index) {

                            self.roleOptionType.push({
                                value: this.positionId,
                                label: this.name
                            });
                        });
                    };
                    // services.getGeneric("saasposition/").then(getValidGradeCBF);
                };



                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.approvaltype = ko.observable();
                self.approvalrole = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.submitdraftLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.aproval = ko.observable();
                self.savedraft = ko.observable();
                self.attachment = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.percentageLbl = ko.observable();

                self.c = ko.observable();
                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.noApprovalLabel = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.defaultConfirmMessage = ko.observable();
                self.selectedPage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.FYILabel = ko.observable();
                self.FYALabel = ko.observable();
                self.approvalTypeValueEmpLabel = ko.observable();
                self.approvalTypeValueMngLabel = ko.observable();
                self.approvalTypeValueLnMngLabel = ko.observable();
                self.UploadFileLbl = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.attachmentNotify = ko.observable();
                self.approvalTypeValuePositionLabel = ko.observable();
                self.approvalTypeValueSpicalCaseLabel = ko.observable();
                self.employeeNameLbl = ko.observable();
                self.locationLbl = ko.observable();
                self.employeeNumberLbl = ko.observable();
                self.organizationLbl = ko.observable();
                self.hireDateLbl = ko.observable();
                self.jobLbl = ko.observable();
                self.positionLbl = ko.observable();
                self.employeeDetailsLbl = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.attachmentNotify(getTranslation("others.attachmentNotify"));
                    self.errorMessageAttachSize(getTranslation("others.errorMessageAttachSize"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.approvalrole(getTranslation("labels.approvalrole"));
                    self.approvaltype(getTranslation("labels.approvaltype"));
                    self.savedraft(getTranslation("labels.saveDraft"));
                    self.aproval(getTranslation("labels.aproval"));
                    self.attachment(getTranslation("others.attachment"));
                    self.submitdraftLbl(getTranslation("labels.submitdraft"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.gradeLbl(getTranslation("common.grade"));
                    self.percentageLbl(getTranslation("position.percentage"));
                    self.cancel(getTranslation("others.cancel"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.draftOprationMessage(getTranslation("others.addGe"));
                    self.oprationMessage(getTranslation("others.addGe"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.defaultConfirmMessage(getTranslation("labels.defaultConfirmMessage"));
                    self.noApprovalLabel(getTranslation("common.noApproval"));
                    self.FYALabel(getTranslation("common.FYALabel"));
                    self.FYILabel(getTranslation("common.FYILabel"));
                    self.approvalTypeValueEmpLabel(getTranslation("common.approvalTypeValueEmpLabel"));
                    self.approvalTypeValueMngLabel(getTranslation("common.approvalTypeValueMngLabel"));
                    self.approvalTypeValueLnMngLabel(getTranslation("common.approvalTypeValueLnMngLabel"));
                    self.viewLbl(getTranslation("common.download"));
                    self.UploadFileLbl(getTranslation("common.UploadFileLbl"));
                    self.removeBtnLbl(getTranslation("common.removeBtnLbl"));
                    self.fileUploadStatusLbl(getTranslation("common.fileUploadStatusLbl"));
                    self.approvalTypeValuePositionLabel(getTranslation("common.approvalTypeValuePositionLabel"));
                    self.approvalTypeValueSpicalCaseLabel(getTranslation("common.approvalTypeValueSpicalCaseLabel"));
                    self.resumeAttachmentLbl(getTranslation("messageNotification.attachmentLbl"));
                    self.employeeNameLbl(getTranslation("common.personNameLbl"));
                    self.locationLbl(getTranslation("labels.location"));
                    self.employeeNumberLbl(getTranslation("common.personNumberLbl"));
                    self.organizationLbl(getTranslation("login.organizationName"));
                    self.hireDateLbl(getTranslation("labels.hireDate"));
                    self.jobLbl(getTranslation("common.job"));
                    self.positionLbl(getTranslation("common.position"));
                    self.employeeDetailsLbl(getTranslation("common.employeeDetailsLbl"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                    self.columnArray1([

                        {
                            "headerText": self.approvaltype(), "field": "approvalType"
                        },
                        {
                            "headerText": self.approvalrole(), "field": "roleName"
                        }
                    ]);
                }
                initTranslations();

                self.validationFunction = function () {

                    //----------------------------Dynamic Validation ------------------------    
                    var EITCodeCbFn = function (data) {
                        var firstValue;
                        var sucendValue;
                        var operation;
                        var reportCallModel = {};
                        if (data) {

                            for (var i = 0; i < data.length; i++) {
                                reportCallModel = {};
                                //--------------First Key Value ----------------
                                if (data[i].firstKeyType == "Application_Value") {
                                    if (data[i].applicationValueFamily == "EIT") {
                                        firstValue = self.model[data[i].applicationKey]();
                                    } else if (data[i].applicationValueFamily == "Validation_Value") {
                                        //app.validation[data[i].validationValuesName]

                                        firstValue = app.validation[data[i].applicationKey];


                                    } else if (data[i].applicationValueFamily == "Personal_Information") {

                                        firstValue = self.personDetails()[data[i].applicationKey];

                                    }
                                } else if (data[i].firstKeyType == "Static") {
                                    firstValue = data[i].firstKeyValue;

                                } else if (data[i].firstKeyType == "SaaS") {
                                    if (data[i].saaSSource == "Report") {
                                        //reportName
                                        reportCallModel.reportName = data[i].reportName;
                                        //reportCallModel.reportName ="PersonAbsenceReport";
                                        var reportPramCbFn = function (dataReport) {

                                            var getReportCBCF = function (datac) {
                                                var valueObj = datac;
                                                firstValue = valueObj.value;

                                            };
                                            // reportCallModel[dataReport[0].parameter1]=

                                            var xx = {"reportName": data[i].reportName};

                                            if (dataReport[0].parameter1) {

                                                if (Object.keys(self.model).indexOf(data[i].parameter1Name) != -1) {
                                                    xx[dataReport[0].parameter1] = self.model[data[i].parameter1Name]();
                                                } else if (data[i].parameter1Name == 'P_STRUCTURE_CODE') {
                                                    xx[dataReport[0].parameter1] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter1] = self.personDetails()[data[i].parameter1Name];
                                                }


                                            }

                                            if (dataReport[0].parameter2) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter2Name) != -1) {
                                                    xx[dataReport[0].parameter2] = self.model[data[i].parameter2Name]();
                                                } else if (data[i].parameter2Name == 'EIT_CODE') {

                                                    xx[dataReport[0].parameter2] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter2] = self.personDetails()[data[i].parameter2Name];
                                                }
                                            }
                                            if (dataReport[0].parameter3) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter3Name) != -1) {
                                                    xx[dataReport[0].parameter3] = self.model[data[i].parameter3Name]();
                                                } else if (data[i].parameter3Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter3] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter3] = self.personDetails()[data[i].parameter3Name];
                                                }
                                            }
                                            if (dataReport[0].parameter4) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter4Name) != -1) {
                                                    xx[dataReport[0].parameter4] = self.model[data[i].parameter4Name]();
                                                } else if (data[i].parameter4Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter4] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter4] = self.personDetails()[data[i].parameter4Name];
                                                }

                                            }
                                            if (dataReport[0].parameter5) {
                                                if (Object.keys(self.model).indexOf(data[i].parameter5Name) != -1) {
                                                    xx[dataReport[0].parameter5] = self.model[data[i].parameter5Name]();
                                                } else if (data[i].parameter5Name == 'EIT_CODE') {
                                                    xx[dataReport[0].parameter5] = tempObject[0].DESCRIPTIVE_FLEX_CONTEXT_CODE;
                                                } else {
                                                    xx[dataReport[0].parameter5] = self.personDetails()[data[i].parameter5Name];
                                                }

                                            }
                                            services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                                        };
                                        services.getGeneric(commonhelper.reportPram + reportCallModel.reportName).then(reportPramCbFn, failCbFn);

                                    } else if (data[i].saaSSource == "Query") {
                                        var str = data[i].saaSQuery;
                                        str = str + "  ";
                                        var flex = ":";
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':') == -1) {
                                            is_HaveFlex = false;
                                        }
                                        var xxxxx = 0;
                                        while (is_HaveFlex) {

                                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                            var ModelName = strWitoutFlex;
                                            strWitoutFlex.substring(1, strWitoutFlex.length);
                                            ModelName = strWitoutFlex.substring(0, strWitoutFlex.indexOf(' '));
                                            if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {

                                                str = str.replace(ModelName, self.personDetails()[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]);
                                            } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, '')) != -1) {
                                                str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                                str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                            }
                                            is_HaveFlex = false;
                                            if (str.indexOf(":") == -1) {
                                                is_HaveFlex = false;
                                            }
//                                        xxxxx++;
//                                        if (xxxxx == 2) {
//                                            is_HaveFlex = false;
//                                        }

                                        }
                                        if (str.substr('undefined ') != -1) {
                                        }


                                        var getReportValidationCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery;
                                            firstValue = resultOfQueryObj[Object.keys(resultOfQueryObj)[0]];
                                        };
                                        var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
                                        services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);
                                    }
                                } else if (data[i].firstKeyType == "PaaS") {
                                    var str = data[i].paaSQuery;
                                    var flex = ":";
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    var is_HaveFlex = true;
                                    if (str.indexOf(':') == -1) {
                                        is_HaveFlex = false;
                                    }

                                    while (is_HaveFlex) {

                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf(' '));
                                        strWitoutFlex.substring(1, strWitoutFlex.length);


                                        if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {
                                            str = str.replace(ModelName, self.personDetails().personId);
                                        } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {
                                            str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                        }
                                        // is_HaveFlex = false;
                                        if (str.indexOf(":") == -1) {
                                            is_HaveFlex = false;
                                        }
                                    }

                                    if (str.includes('unsdesefined')) {
                                    } else {
                                        //Here Must Call Dynamic Web Service Function Fpr This Perpus 
                                        var getDynamicQueryCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery[0];

                                            firstValue = resultOfQueryObj[Object.keys(resultOfQueryObj)[0]];


                                        };
                                        var validationQueryReportModel = {"query": str};
                                        services.searcheGeneric("PeopleExtra/getQuery/", validationQueryReportModel).then(getDynamicQueryCBCF, app.failCbFn);

                                    }
                                }
                                operation = data[i].operation;
                                //-------Secound Key Value --------------------
                                if (data[i].secondKeyType == "Application_Value") {
                                    if (data[i].secondapplicationValueFamily == "EIT") {


                                        sucendValue = self.model[data[i].secondapplicationKey]();
                                    } else if (data[i].secondapplicationValueFamily == "Validation_Value") {
                                        //app.validation[data[i].validationValuesName]
                                        sucendValue = app.validation[data[i].secondapplicationKey];

                                    } else if (data[i].secondapplicationValueFamily == "Personal_Information") {
                                        sucendValue = self.personDetails()[data[i].secondapplicationKey];
                                    }
                                } else if (data[i].secondKeyType == "Static") {
                                    sucendValue = data[i].secondKeyValue;
                                } else if (data[i].secondKeyType == "SaaS") {
                                    if (data[i].secondsaaSSource == "Report") {
                                        reportCallModel.reportName = data[i].secondreportName;
                                        var reportPramCbFn = function (dataReport) {
                                            var getReportCBCF = function (datac) {
                                                var valueObj = datac;
                                                sucendValue = valueObj.value;
                                            };
                                            // reportCallModel[dataReport[0].parameter1]=

                                            var xx = {"reportName": data[i].secondreportName};

                                            if (dataReport[0].parameter1) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter1Name) != -1) {
                                                    xx[dataReport[0].parameter1] = self.model[data[i].secondparameter1Name]();
                                                } else {
                                                    xx[dataReport[0].parameter1] = self.personDetails()[data[i].secondparameter1Name];
                                                }


                                            }
                                            if (dataReport.parameter2) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter2Name) != -1) {
                                                    xx[dataReport[0].parameter2] = self.model[data[i].secondparameter2Name]();
                                                } else {
                                                    xx[dataReport[0].parameter2] = self.personDetails()[data[i].secondparameter2Name];
                                                }

                                            }
                                            if (dataReport.parameter3) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter3Name) != -1) {
                                                    xx[dataReport[0].parameter3] = self.model[data[i].secondparameter3Name]();
                                                } else {
                                                    xx[dataReport[0].parameter3] = self.personDetails()[data[i].secondparameter3Name];
                                                }

                                            }
                                            if (dataReport.parameter4) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter4Name) != -1) {
                                                    xx[dataReport[0].parameter4] = self.model[data[i].secondparameter4Name]();
                                                } else {
                                                    xx[dataReport[0].parameter4] = self.personDetails()[data[i].secondparameter4Name];
                                                }

                                            }
                                            if (dataReport.parameter5) {
                                                if (Object.keys(self.model).indexOf(data[i].secondparameter5Name) != -1) {
                                                    xx[dataReport[0].parameter5] = self.model[data[i].secondparameter5Name]();
                                                } else {
                                                    xx[dataReport[0].parameter5] = self.personDetails()[data[i].secondparameter5Name];
                                                }

                                            }
                                            services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                                        };
                                        services.getGeneric(commonhelper.reportPram + reportCallModel.reportName).then(reportPramCbFn, failCbFn);
                                    } else if (data[i].secondsaaSSource == "Query") {
                                        var str = data[i].secondsaaSQuery;
                                        str = str + "  ";
                                        var flex = ":";
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':') == -1) {
                                            is_HaveFlex = false;
                                        }
                                        var xxxxx = 0;
                                        while (is_HaveFlex) {

                                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                            var ModelName = strWitoutFlex;
                                            strWitoutFlex.substring(1, strWitoutFlex.length);
                                            ModelName = strWitoutFlex.substring(0, strWitoutFlex.indexOf(' '));
                                            if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {

                                                str = str.replace(ModelName, self.personDetails()[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]);
                                            } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, '')) != -1) {
                                                str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                            }
                                            //    is_HaveFlex = false;
                                            if (str.indexOf(":") == -1) {
                                                is_HaveFlex = false;
                                            }
//                                        xxxxx++;
//                                        if (xxxxx == 2) {
//                                            is_HaveFlex = false;
//                                        }

                                        }
                                        if (str.substr('undefined ') != -1) {
                                        }


                                        var getReportValidationCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery;
                                            firstValue = resultOfQueryObj[Object.keys(resultOfQueryObj)[0]];
                                        };
                                        var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
                                        services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);
                                    }
                                } else if (data[i].secondKeyType == "PaaS") {
                                    var str = data[i].secondpaaSQuery;
                                    var flex = ":";
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    var is_HaveFlex = true;
                                    if (str.indexOf(':') == -1) {
                                        is_HaveFlex = false;
                                    }

                                    while (is_HaveFlex) {

                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf(' '));

                                        strWitoutFlex.substring(1, strWitoutFlex.length);


                                        if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {
                                            str = str.replace(ModelName, self.personDetails().personId);
                                        } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))) != -1) {
                                            str = str.replace(ModelName, "'" + self.model[strWitoutFlex.substring(1, strWitoutFlex.indexOf(' '))]() + "'");
                                        }
                                        // is_HaveFlex = false;
                                        if (str.indexOf(":") == -1) {
                                            is_HaveFlex = false;
                                        }
                                    }

                                    if (str.includes('undefined')) {
                                    } else {
                                        //Here Must Call Dynamic Web Service Function Fpr This Perpus 
                                        var getDynamicQueryCBCF = function (dataSaaSQuery) {
                                            var resultOfQueryObj = dataSaaSQuery[0];

                                            sucendValue = resultOfQueryObj[Object.keys(resultOfQueryObj)[0]];


                                        };
                                        var validationQueryReportModel = {"query": str};
                                        services.searcheGeneric("PeopleExtra/getQuery/", validationQueryReportModel).then(getDynamicQueryCBCF, app.failCbFn);

                                    }
                                }
                                if (data[i].typeOFAction == "Error_Massage") {
                                    if (app.compare(firstValue, operation, sucendValue)) {
                                        self.validationCheck(true);
                                        isErrMsg = true;
                                        if (app.getLocale() == 'ar') {
                                            self.validationMessagesArr.push(self.createMessage(data[i].errorMassageAr));
                                        } else {
                                            self.validationMessagesArr.push(self.createMessage(data[i].errorMassageEn));
                                        }
                                    } else {
                                        self.validationMessagesArr([]);
                                        self.validationCheck(false);
                                    }
                                } else {
                                    isErrMsg = false;
                                }

                                if (data[i].saveResultToValidationsValues == "Yes") {
                                    app.validation[data[i].validationValuesName] = app.compare(firstValue, operation, sucendValue);

                                }


                            }
                        } else {
                            self.deptObservableArray([]);
                        }
                    };
                    var failCbFn = function () {
                    };

                    services.searcheGenericAsync("validation/search", self.validationSearch).then(EITCodeCbFn, failCbFn);

                    if (self.validationCheck()) {
                        nextStepBtnStyle.showAsEnabled();
                       
                        return;
                    }

                    if (isErrMsg) {
                        nextStepBtnStyle.showAsEnabled();
                        
                        return;
                    }
                };
                self.attachmentSelection = function (event, data) {
                    

                };

            }

            return new dynamicOperationScreenViewModel();
        }
);
