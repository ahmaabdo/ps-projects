/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} services
 * @param {type} commonhelper
 * @returns {searchPositionL#15.homeministryJobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'config/buildScreen',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojtable', 'ojs/ojpagingtabledatasource',
    'ojs/ojselectcombobox', 'ojs/ojvalidationgroup', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojtrain',
    'ojs/ojdialog', 'ojs/ojpagingcontrol'], 
function (oj, ko, $, app, services, commonhelper, buildScreen) {

    function SummaryDynamicViewModel() {
        var self = this;
        self.dataSourceTB2 = ko.observable();
        self.datacrad = ko.observableArray([]);
        self.columnArray1 = ko.observableArray();
        self.lang = "";
        var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
        self.columnArray = ko.observableArray();
        self.oprationdisabled = ko.observable(true);
        self.oprationdisabled2 = ko.observable(true);
        self.oprationdisabled3 = ko.observable(false);
        var deptArray = [];
        var deptArrayId = [];
        var deptArrayStatus = [];
        self.selectedRowKey = ko.observable();
        self.t_type = ko.observable();
        self.t_type = ko.observable();
        self.selectedIndex = ko.observable();
        self.helpHeader = ko.observable();
        self.hideSearch = ko.observable(false);
        self.deptObservableArray = ko.observableArray(deptArray);
        self.deptObservableArrayid = ko.observableArray(deptArrayId);
        self.deptArrayStatus = ko.observableArray(deptArrayStatus);
        self.HELPLbl = ko.observable();
        self.onlineHelpLbl = ko.observable();
        self.disableHelpBtn = ko.observable(false);
        self.dataprovider = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'id'}));
        //Some screens dosn't include dummy
        //self.dataprovider = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'dummy'}));
        var ArrKeys = [];
        self.Arrs = {};
        self.ListSegment = [];
        self.hideReportPdf = ko.observable(false);
        var tempObject;
        //-----Attrubute For Draw Serach Dynamic ----------------------------
        self.model = {};
        self.globalEFFUDT = rootViewModel.globalEFFUDT();
        self.serchableFeieldName = [];
        var Keys = [];
        self.sercheScreen = [];
        var disabeld = [];
        self.isDisabledx = {};
        self.dataArray = [{value: 'zz', label: 'zz'}];
        self.EmbedPDFLink = ko.observable("");
        self.positionName = ko.observable();
        self.urlEit = ko.observable();
        self.bodymessage = ko.observable();
        self.deleteDisabled = ko.observable(true);
        self.saasApproved = ko.observableArray([]);
        self.disableHelpBtn = ko.observable(false);
	self.helpLbl= ko.observable();
        self.onlineHelpLbl = ko.observable();
        self.roleOptionType = ko.observableArray([]);
        //-----------End ------------------------------
         self.BtnRejectReason=function(){
            var element = document.getElementById('table');
            var currentRow = element.currentRow;  
           var id=self.deptObservableArray()[currentRow['rowIndex']].id;
           
           var getRejectReasonCbFn=function(data){
               
               if(data.rejectReason==null || data.rejectReason===""){
                   self.bodymessage("there is no reason");
               }else{
                  self.bodymessage(data.rejectReason);
               }
               
               
                 document.querySelector("#RejectReasonDialog").open();
           };
           
             var serviceName="PeopleExtra/rejectReason/"+id;
            services.getGeneric(serviceName).then(getRejectReasonCbFn , app.failCbFn);
           
         };   
         
         self.CancelDialog=function(){
                 document.querySelector("#RejectReasonDialog").close();
         };
        self.handleDetached = function (info) {
            app.isHideLeftMenue(false);
        }
        self.handleAttached = function (info) {
            app.isHideLeftMenue(false);
            self.columnArray.push({
                     "headerText": 'Status', "field": 'statusLBL'
                      });
            //self.getPositionrole();
            self.roleOptionType(rootViewModel.roleOptionType());
            self.checkSubmitUser=ko.observable();
            ArrKeys = [];
            self.EitCode = oj.Router.rootInstance.retrieve();
            var setEitName;
            self.currentStepValueText = ko.observable();
            self.getEITName = function (EITCODE){
                setEitName = app.globalEitNameReport().find(xx=>xx.value == EITCODE).label;//xx
                // for (var i = 0; i < app.globalEitNameReport().length; i++) {
                //     if (app.globalEitNameReport()[i].value === EITCODE) {

                //         setEitName = app.globalEitNameReport()[i].label;
                //     }
                //     ;
                // }
                
            };


            self.getApprovedEitFromSaaS();
            initTranslations();
            var routerCode = oj.Router.rootInstance.retrieve();

            self.personDetails = ko.observableArray();

//            var Eit_n = searchArray("test"); 



            if (routerCode.checkUserSubmit === "ManagersubmitUser") {
                self.EitCode = routerCode.EIT;
                self.personDetails(app.globalspecialistEMP());
                self.checkSubmitUser("Specialist");
                self.getEITName(self.EitCode);
                self.currentStepValueText(setEitName + " Employee name :" + self.personDetails().displayName + " Employee Number: " + self.personDetails().personNumber);

            } else if (routerCode.returnToSymary === "backAction") {
                if (routerCode.checkUser === "Specialist") {


                    self.EitCode = routerCode.EIT;
                    self.getEITName(self.EitCode);
                    self.personDetails(app.globalspecialistEMP());
                    self.currentStepValueText(setEitName + " Employee name :" + self.personDetails().displayName + " Employee Number: " + self.personDetails().personNumber);
                    self.checkSubmitUser("Specialist");
                } else if (routerCode.checkUser === "employee") {

                    self.personDetails(rootViewModel.personDetails());
                    self.EitCode = routerCode.EIT;
                    self.checkSubmitUser("employee");
                    self.getEITName(self.EitCode);
                    self.currentStepValueText(setEitName);
                }
            } else {
                self.personDetails(rootViewModel.personDetails());
                self.EitCode = oj.Router.rootInstance.retrieve();
                self.checkSubmitUser("employee");
                self.getEITName(self.EitCode);
                self.currentStepValueText(setEitName);
            }


            self.setUrl(self.EitCode);

            self.serchableFeieldName = self.globalEFFUDT.filter(x=>x.userColumnName == self.EitCode);//xx
            // for (var x = 0; x < self.globalEFFUDT.length; x++) {
            //     if (self.globalEFFUDT[x].userColumnName == self.EitCode) {
            //         // self.globalEFFUDT.re
            //         //  self.globalEFFUDT.splice(x,1);

            //         self.serchableFeieldName.push(self.globalEFFUDT[x]);
            //     }
            // }
            //--------------For Draw Table ----------------
            self.lang = app.getLocale() == 'ar' ? 'AR':'US';//XX
            // if (app.getLocale() == 'ar') {
            //     self.lang = "AR";
            // } else {
            //     self.lang = "US";
            // }

            //services.getEIT(self.EitCode, self.lang).then(getEit, app.failCbFn);
            self.getEit(app.getEITSegments(self.EitCode));//XX
            // var segments = app.getEITSegments(self.EitCode);
            // self.getEit(segments);
            //------------End Draw Table -------------------------

            //-----------For Get Table Data --------------------
//       var jsonData = {"url":self.setUrl(self.EitCode)};
//        var getEffCBF = function (data) {
//            self.deptObservableArray(data);                  
//            };
//           services.addGeneric(commonhelper.getEffData , jsonData).then(getEffCBF, self.failCbFn);
//           
            var getEffCBF = function (data) {
                
                 
                for (var i = 0; i < data.length; i++) {

                    var str = data[i]['eitLbl'];
                    var rowObj;
                    try {
                        //all eit at least has paresed to string once so try to parse it to json
                        rowObj = JSON.parse(str);
//                        eval(JSON.parse(JSON.parse(data[i]['eitLbl'])));
//                        rowObj = JSON.parse(JSON.parse(data[i]['eitLbl']));
                    try{
                        //if any eit was parsed to string twice so rowObj still is a string try to parse it again if cant parsed so it's already paresed fro prev step
                          rowObj = JSON.parse(rowObj);
                    }catch(cc){
                    }
                    
                    } catch (e) {
//                        rowObj = JSON.parse(data[i]['eitLbl']);
                    }
                    if(rowObj){
                        
                    eval(rowObj);
                    
                    
                    // rowObj.id = data[i].id;
                    rowObj.id = data[i].id;

                    if (data[i].status === "PENDING_APPROVED") {

                        rowObj.status =data[i].status;
                        rowObj.statusLBL = self.pendingAprovalLbl();

                    } else if (data[i].status === "APPROVED") {

                        rowObj.status = data[i].status;
                        rowObj.statusLBL = self.approvalLBl();
                    } else if (data[i].status === "REJECTED") {             
                        rowObj.status = data[i].status;
                        rowObj.statusLBL = self.rejectedLbl();
                    }else if (data[i].status === "Withdraw") {             
                        rowObj.status = data[i].status;
                        rowObj.statusLBL = self.withdrawLbl();
                    }                   
                    else {
                        rowObj.status = data[i].status;
                        rowObj.statusLBL = self.draftLbl();
                    }

                    for (var segment = 0; segment < self.ListSegment.length; segment++) {

                        if (Object.keys(rowObj).indexOf(self.ListSegment[segment].Name) != -1) {

                            // rowObj[self.ListSegment[segment].Name + "Lbl"] = searchArray(rowObj[self.ListSegment[segment].Name], self.Arrs[self.ListSegment[segment].listName]());
                            rowObj[self.ListSegment[segment].Name + "Lbl"] = rowObj[self.ListSegment[segment].Name];

                            //searchArray(rowObj[self.ListSegment[segment].Name],self.Arrs[self.ListSegment[segment].listName]());
                        }
                    }
                    self.deptObservableArrayid.push(rowObj);
                    self.deptArrayStatus.push(data[i].status);
                    self.deptObservableArray.push(rowObj);
                }
                }
                    if (typeof self.saasApproved()[0] !== 'undefined') {
                    for (var i = 0; i < self.saasApproved()[0].length; i++) {
                        for (var segment = 0; segment < self.ListSegment.length; segment++) {
                            if (Object.keys(self.saasApproved()[0][i]).indexOf(self.ListSegment[segment].Name) != -1) {
                                self.saasApproved()[0][i][self.ListSegment[segment].Name + "Lbl"] = searchArray(self.saasApproved()[0][i][self.ListSegment[segment].Name], self.Arrs[self.ListSegment[segment].listName]());
                            }
                        }
                        self.deptObservableArray.push(self.saasApproved()[0][i]);
                    }
                }

            };

            services.getGeneric(commonhelper.getPeopleExtra + self.EitCode + '/' + self.personDetails().personId).then(getEffCBF, self.failCbFn);
            //-----------End Of Get And Set Table Data -------------------------

            self.getLocale = function () {
                return oj.Config.getLocale();

            };


            if (self.getLocale() === "ar") {
                $('.approval-card-body').removeClass('app-crd-bdy-border-en');
                $('.approval-card-body').addClass('app-crd-bdy-border-ar');
                $('.app-crd-type').removeClass('app-card-for-en');
                $('.app-crd-type').addClass('app-card-for-ar');
                $('.blockquote-footer').removeClass('app-card-for-en');
                $('.blockquote-footer').addClass('app-card-for-ar');


            } else {
                $('.approval-card-body').removeClass('app-crd-bdy-border-ar');
                $('.approval-card-body').addClass('app-crd-bdy-border-en');
                $('.app-crd-type').removeClass('app-card-for-ar');
                $('.app-crd-type').addClass('app-card-for-en');
                $('.blockquote-footer').removeClass('app-card-for-ar');
                $('.blockquote-footer').addClass('app-card-for-en');

            }

            app.loading(false);
        };
        self.setUrl = function (EitCode) {
//            if (EitCode == "XXX_ABS_CREATE_DECREE") {
//                self.urlEit(rootViewModel.personDetails().employeeURL.XXX_ABS_CREATE_DECREE);
//            } else if (EitCode == "XXX_ABS_UPDATE_DECREE") {
//                self.urlEit(rootViewModel.personDetails().employeeURL.XXX_ABS_UPDATE_DECREE);
//            } else if (EitCode == "XXX_ABS_DELETE_DECREE") {
//                self.urlEit(rootViewModel.personDetails().employeeURL.XXX_ABS_DELETE_DECREE);
//            } else if (EitCode == "XXX_EMP_MINISTRY_JOB") {
//                self.urlEit(rootViewModel.personDetails().employeeURL.ministryJobForEmployee);
//            } else if (EitCode == "XXX_HR_BUSINESS_PAYMENT") {
//                self.urlEit(rootViewModel.personDetails().employeeURL.XXX_HR_BUSINESS_PAYMENT);
//            } else if (EitCode == "XXX_HR_BUSINESS_MISSION") {
//                self.urlEit(rootViewModel.personDetails().employeeURL.XXX_HR_BUSINESS_MISSION);
//            }
//            else if (EitCode == "XXX_EMPLOYMENT_CERTIFICATE") {
//                self.urlEit(rootViewModel.personDetails().employeeURL.XXX_EMPLOYMENT_CERTIFICATE);
//            }
//            else if (EitCode == "XXX_HR_OVERTIME_PAYMENT") {
//                self.urlEit(rootViewModel.personDetails().employeeURL.XXX_HR_OVERTIME_PAYMENT);
//            }
//            else if (EitCode == "XXX_OVERTIME_REQUEST") {
            self.urlEit(self.personDetails().employeeURL[EitCode]);
//                self.urlEit(app.personDetails().employeeURL);

            //}
        };
        //End OF Handel Attached 
        self.getEit = function (data) {
            var Iterater = 0;
            tempObject = data;
            if (!tempObject.length) {
                tempObject = [tempObject];

            }
            var sercheFeiled = 0;

            for (var i = 0; i < tempObject.length; i++) {

                if (tempObject[i].DISPLAY_FLAG == "Y") {

                    if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                            (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                            (tempObject[i].FLEX_value_SET_NAME != "HRC_NUMBER_10") &&
                            (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                            (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                            (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                            (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE") &&
                            (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_VIEW_REPORT") &&
                            (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_DYNAMIC_COLUMN")&&
                            (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME")
                            ) {

                        if (tempObject[i].value_SET_TYPE == "X" || tempObject[i].value_SET_TYPE == "I") {
                            var listObject = {"Name": tempObject[i].DESCRIPTION,
                                "listName": tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"};
                            self.ListSegment.push(listObject);
                            self.columnArray.push({
                                "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION + "Lbl"});
                            ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                            self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                            var getReportCBCF = function (data) {
                                var tempObject2 = data;
                                var arr = tempObject2;
                                var arrays = [];
                                for (var x = 0; x < arr.length; x++) {
                                    arrays.push({"value": arr[x].FLEX_value, "label": arr[x].FLEX_value})
                                }
                                self.Arrs[ArrKeys[Iterater]](arrays);
                            };
                            var xx = {"reportName": "inDependentReport", "valueSet": tempObject[i].FLEX_value_SET_NAME};
                            //    services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                            Iterater = Iterater + 1;
                        } else if (tempObject[i].value_SET_TYPE == "Y") {
                            ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                            self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                            Iterater = Iterater + 1;
                            self.columnArray.push({
                                "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION + "Lbl"});
                        } else if (tempObject[i].value_SET_TYPE == "F") {
                            var listObject = {"Name": tempObject[i].DESCRIPTION,
                                "listName": tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"};
                            self.ListSegment.push(listObject);
                            self.columnArray.push({
                                "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION + "Lbl"});
                            ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                            self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                            //     var getDynamicReport = function (data) {

                            var val = "value";
                            var lbl = "label";
                            var q;
                            //   var tempObject = data;
                            q = "select " + tempObject[i].ID_COL + " " + val + " ,  " + tempObject[i].value_COL + "  " + lbl
                                    + " From " + tempObject[i].APPLICATION_TABLE_NAME + "  " + tempObject[i].ADDITIONAL_WHERE_CLAUSE;


                            var str = q;

                            var flex = ":";
                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));

                            var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                            var is_HaveFlex = true;
                            if (str.indexOf(':') == -1) {
                                is_HaveFlex = false;
                            }
                            while (is_HaveFlex) {
                                var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));

                                str = str.replace(strWitoutFlex, self.personDetails().personId);
                                if (str.indexOf(":") == -1) {
                                    is_HaveFlex = false;
                                }
                            }
                            if (str.includes('undefined')) {
                            } else {
                                //Here Must Call Report Fpr This Perpus 

                                var getArrCBCF = function (data) {
                                    var tempObject2 = data;
                                    var arr = tempObject2;
                                    if (tempObject2.length) {
                                        self.Arrs[ArrKeys[Iterater]](arr);
                                    } else {
                                        self.Arrs[ArrKeys[Iterater]]([arr])
                                    }
                                };
                                // services.getDynamicReport(str).then(getArrCBCF, app.failCbFn);

                            }



//                                };
                            //services.getQueryForListReport(tempObject[i].FLEX_value_SET_NAME).then(getDynamicReport, app.failCbFn);
                            Iterater = Iterater + 1;
                        }

                    } else if (tempObject[i].FLEX_value_SET_NAME === "XXX_VIEW_REPORT") {

                        //call report from sass
                        self.oprationdisabled2(true);
                    } else if (tempObject[i].FLEX_value_SET_NAME === "XXX_HR_PAAS_VIEW_REPORT") {


                        self.hideReportPdf(true);
                    } else if (tempObject[i].FLEX_value_SET_NAME == "XXX_HR_DYNAMIC_COLUMN") {
                        self.columnArray.push({
                            "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION + "Lbl"});
                    }else if (tempObject[i].FLEX_value_SET_NAME =='XXX_HR_EIT_ATTACHMENTS'){
                        // Attachment 
                    }else {
                        self.columnArray.push({
                            "headerText": tempObject[i].FORM_ABOVE_PROMPT, "field": tempObject[i].DESCRIPTION});
                    }

                }
                if (self.serchableFeieldName[sercheFeiled]) {
                    if (self.serchableFeieldName[sercheFeiled].rowName == tempObject[i].APPLICATION_COLUMN_NAME) {

                        self.sercheScreen.push(tempObject[i]);
                        Keys.push(tempObject[i].DESCRIPTION);
                        self.model[Keys[sercheFeiled]] = ko.observable();
                        //Build Disabeld Model 
                        disabeld.push("isDisabled" + tempObject[i].DESCRIPTION);
                        if (tempObject[i].ENABLED_FLAG == "Y") {
                            self.isDisabledx[disabeld[i]] = ko.observable(false);
                        } else if (tempObject[i].ENABLED_FLAG == "N") {
                            self.isDisabledx[disabeld[i]] = ko.observable(true);
                        }
                        //End Of Disabeld Model 
                        if (self.serchableFeieldName.length - 1 > sercheFeiled) {
                            sercheFeiled++;
                            self.hideSearch(true);
                        }
                    }
                }

            }

//                self.columnArray.push({
//               "headerText": 'Status', "field": 'status'});
            //buildScreen.buildScreen (self.sercheScreen,"xx" ,self.model ,self.isDisabledx ,self.dataArray);
        };
        //End Of Get Eit Segments 
        //------------- List Approvals

        self.rolrType = ko.observableArray([]);
        self.notificationType = ko.observableArray([]);
        self.responseCode = ko.observableArray([]);
        self.responseDate = ko.observableArray([]);
        self.approvalResponseArr = ko.observableArray([]);
        self.rejectReason=ko.observable();
        self.workglow_Approval = async function () {

            self.oprationdisabled(true);
            $(".approval-btn").addClass("loading");
            self.datacrad([]);
            var eitcode = self.EitCode;

            var getValidGradeCBF = function (data) {
                var dataLength = data.length;
                for (var c = 0; c < data.length; c++) {
                    if (data[c].responseCode == 'REJECTED') {
                        dataLength = c + 1;
                    }
                }
                if (data.length != 0) {
                    var aprrovalStatus = "PENDING";

                    for (var i = 0; i < dataLength; i++) {
                        if(data[i].notificationType == "FYA"){
                           data[i].notificationType = "For your action"; 
                        }
                        else if(data[i].notificationType == "FYI"){
                          data[i].notificationType = "For your information";  
                        }
                        else{
                            data[i].notificationType;
                        }
                        
                        var notificationStatus, bodyCardStatus, cardStatus,rejectReason;
                        if (data[i].notificationType === "For your action") {
                            notificationStatus = 'app-type-a';
                        } else {
                            cardStatus = 'badge-secondary';
                            notificationStatus = 'app-type-i';
                            bodyCardStatus = 'app-crd-bdy-border-ntre';
                        }
                        if (!data[i].responseCode) {
                            cardStatus = 'badge-warning';
                            bodyCardStatus = 'app-crd-bdy-border-pen';
                            data[i].responseDate = '';
                        } else if (data[i].responseCode === 'APPROVED') {
                            cardStatus = 'badge-success';
                            bodyCardStatus = 'app-crd-bdy-border-suc';
                        }else if (data[i].responseCode === 'REJECTED') {
                            cardStatus = 'badge-danger';
                            bodyCardStatus = 'app-crd-bdy-border-fail';
                        } else {

                        }if(data[i].rejectReason){
                            rejectReason=data[i].rejectReason;
                        }else{
                            rejectReason="There is no comment";
                        }
                        
                        
                        if (data[i].rolrType === "EMP") {
//                            data[i].rolrType= app.personDetails().displayName
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: self.personDetails().displayName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        }
                                        );
                            } else {
                                self.datacrad.push(
                                        {personName: self.personDetails().displayName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        }
                                        );
                            }

                        } else if (data[i].rolrType == "LINE_MANAGER") {
//                            data[i].rolrType= app.personDetails().managerName;
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );
                            } else {
                                self.datacrad.push(
                                        {personName: self.personDetails().managerName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );

                            }

                        } else if (data[i].rolrType == "LINE_MANAGER+1") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName:data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );
                            } else {
                                self.datacrad.push(
                                        {personName: data[i].personName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );


                            }

                        } else if (data[i].rolrType == "POSITION") {

//                               data[i].rolrType= app.personDetails().managerOfMnagerName;
                            for (var j = 0; j < app.roleOptionType().length; j++) {
                                if (app.roleOptionType()[j].value == data[i].roleId)
                                {
                                    self.positionName(app.roleOptionType()[j].label);
                                }
                            }
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );
                            } else {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );


                            }

                        } else if (data[i].rolrType == "ROLES") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;

                            for (var j = 0; j < rootViewModel.allRoles().length; j++) {
                                if (rootViewModel.allRoles()[j].ROLE_ID == data[i].roleId)
                                {

                                    self.positionName(rootViewModel.allRoles()[j].ROLE_NAME);
                                }
                            }
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );
                            } else {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );


                            }

                        } else if (data[i].rolrType == "ROLES") {
//                               data[i].rolrType= app.personDetails().managerOfMnagerName;

                            for (var j = 0; j < rootViewModel.allRoles().length; j++) {
                                if (rootViewModel.allRoles()[j].value == data[i].roleId)
                                {
                                    self.positionName(rootViewModel.allRoles()[j].label);
                                }
                            }
                            if (data[i].responseCode === null) {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );
                            } else {
                                self.datacrad.push(
                                        {personName: self.positionName(),
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        } );

                            }

                        }else if (data[i].rolrType == "JOB_LEVEL") {
//                            data[i].rolrType= app.personDetails().managerName;
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: data[i].lineManagerName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        }, );
                            } else {
                                self.datacrad.push(
                                        {personName: data[i].lineManagerName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        }, );

                            }

                        }else if (data[i].rolrType == "AOR") {
//                            data[i].rolrType= app.personDetails().managerName;
                            if (!data[i].responseCode) {
                                self.datacrad.push(
                                        {personName: data[i].lineManagerName,
                                            FYA: data[i].notificationType,
                                            Approved: aprrovalStatus,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        }, );
                            } else {
                                self.datacrad.push(
                                        {personName: data[i].lineManagerName,
                                            FYA: data[i].notificationType,
                                            Approved: data[i].responseCode,
                                            date: data[i].responseDate,
                                            status: notificationStatus,
                                            bodyStatus: bodyCardStatus,
                                            appCardStatus: cardStatus,
                                            rejectReason: rejectReason
                                        }, );

                            }

                        }


//                       self.notificationType.push(data[i].notificationType);
//                       self.responseCode.push(data[i].responseCode);
//                       self.responseDate.push(data[i].responseDate);  

                        //  data[i].approvalType = searchArray(data[i].approvalType, self.approvalType());
                        //  data[i].roleName = searchArray(data[i].roleName, self.roleName());

                    }
                } else
                {
                         self.datacrad.push(
                                        {personName: 'There is no approval list to this request',
                                            FYA: '',
                                            Approved: '',
                                            date: '',
                                            status: '',
                                            bodyStatus: '',
                                            appCardStatus: ''
                                        });
                }
//                    var Data = data;
//          
//                self.dataSourceTB2(new oj.ArrayTableDataSource(Data));

            };
            // ... leave firstName and lastName unchanged ...

            //self.lbl = ko.observable(self.approvaltype());
            await services.getGenericAsync("approvalList/approvaltype/" + eitcode + "/" + self.t_type()).then(getValidGradeCBF, app.failCbFn);
            self.oprationdisabled(false);
            $(".approval-btn").removeClass("loading");
            document.querySelector("#modalDialog1").open();
        };

        self.getPositionrole = function () {
            var getValidGradeCBF = function (data) {
               
                $.each(data, function (index) {

                    self.roleOptionType.push({
                        value: this.positionId,
                        label: this.name
                    });
                });
               
            };
            services.getGeneric("saasposition/").then(getValidGradeCBF);
        };


        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };

        self.closeHelpDialog = function () {
            $("#onlineHelp").ojDialog("close");
        };



        //----------Searche Eff ---------------------------------------
        self.globalLookupReport = ko.observableArray();
        self.globalLookupReport(app.globalFuseModel());
        self.pdfReportName = ko.observable();
        self.ShowReport = function () {
           // self.status_Approval();
          
                    
                if (self.globalDataRow().status == "APPROVED")
                  {
                  
                   
                        self.pdfReportName("");
             var letterTypeFromTbale= self.globalDataRow().letterType;
             var letterLangFromTable= self.globalDataRow().letterLanguage;
                      for( var counter=0; counter<self.globalLookupReport().length;counter++){
                         if(self.globalLookupReport()[counter].LOOKUP_TYPE==="XXX_HR_PAAS_REPORTS"){
                             var str=self.globalLookupReport()[counter].MEANING;
                             var commaIndex=str.indexOf(",");
                             var lastIndex= str.length;
                             var lettrLang=str.substring(0,commaIndex);
                             var letterType=str.substring(commaIndex+1,lastIndex);
                            
                             
                            if(letterTypeFromTbale===letterType &&letterLangFromTable === lettrLang){
                               self.pdfReportName(self.globalLookupReport()[counter].LOOKUP_CODE);
                               self.callReportPdf();
                            }     
                             
                        }                    
                  }
                   
                   
                 }else {
                       $.notify("Not Approved ", "error");
                    
                 }
            
        };
        self.callReportPdf = function () {
            var getReportCbFn = function (data) {

                var base64pdf = 'data:application/pdf;base64,' + data.REPORT;
                self.EmbedPDFLink(base64pdf);
                document.querySelector("#pdfViewer").open();
            };

            var payload = {"reportName": self.pdfReportName(), "personId": self.personDetails().personId};

            services.getGenericPdfReport(payload).then(getReportCbFn, app.failCbFn);
        };
        
        self.ShowReportIdentificationLetter = function () {
            app.loading(true);
             var getReportCbFn=function (data){
                 if(self.globalDataRow().chamberOfCommerceStamp == "Yes"){
                     var base64Word =  data[0].attachment;
                        
                self.EmbedPDFLink(base64Word);
                document.querySelector("#pdfViewer").open();
                app.loading(false);
                 }
                 else{
                     var base64pdf = 'data:application/pdf;base64,' + data.REPORT;
                self.EmbedPDFLink(base64pdf);
                document.querySelector("#pdfViewer").open();
                app.loading(false);
                 }
                 
             };
             var iDletterType=self.globalDataRow().letterType;
             var withSalaryType=self.globalDataRow().withSalary;
             var entityEnglishName = self.globalDataRow().entityEnglishName;
             var entityArabicName = self.globalDataRow().entityArabicName;
             var chamberOfCommerceStamp = self.globalDataRow().chamberOfCommerceStamp;
             var id = self.globalDataRow().id;
            if (iDletterType == "ID Letter" && withSalaryType == "No") {
              
                
                    var payload={"reportName":"TBC Certificate of Service Without Salary REP","PERSON_NUMBER":self.personDetails().personNumber, "entityEnglishName" : entityEnglishName, "entityArabicName" : entityArabicName};                      
              
                               
            }
            else if(iDletterType == "ID Letter" && withSalaryType == "Yes (With total)"){
                var payload={"reportName":"Certificate of Service with Salary Rep","PERSON_NUMBER":self.personDetails().personNumber, "entityEnglishName" : entityEnglishName, "entityArabicName" : entityArabicName};                      
            }
            else if(iDletterType == "Installment Letter" && withSalaryType == "Yes"){
                var payload={"reportName":"Identification Letter V1 Rep","PERSON_NUMBER":self.personDetails().personNumber, "entityEnglishName" : entityEnglishName, "entityArabicName" : entityArabicName};                      
            }
            else if(iDletterType == "ID Letter" && withSalaryType == "YesBreak"){
                var payload={"reportName":"Identification Letter V2 Rep","PERSON_NUMBER":self.personDetails().personNumber, "entityEnglishName" : entityEnglishName, "entityArabicName" : entityArabicName};                      
            }
            if(chamberOfCommerceStamp != 'Yes'){
            services.getGenericReport(payload).then(getReportCbFn,app.failCbFn);
            }
            else{
                services.getGenericAsync("attachment/" + id.toString()).then(getReportCbFn, app.failCbFn);
            }
               
        };

//        self.checkResponseCode()=function(){
//            services.getGeneric("approvalList/approvaltype/" + eitcode + "/" + self.t_type()).then(getValidGradeCBF, app.failCbFn);
//        }



        //---------End Of Searche EFF --------------------------------
        //----------Screen Operation -----------------------------------
        self.searchEFF = function () {
            var url = self.setUrl(self.EitCode);
            url = url + "?q=";

            var str;

            if (Keys.length > 1) {
                str = Keys[0] + "=" + self.model[Keys[0]]();
            }
            for (var z = 1; Keys.length > z; z++) {

                if (self.model[Keys[z]]()) {
                    str = str + "," + Keys[z] + "=" + self.model[Keys[z]]();
                }

            }
            url = url + str;

            var jsonData = {"url": url};
            var getEffCBF = function (data) {
                self.deptObservableArray(data);
            };
            services.addGeneric(commonhelper.getEffData, jsonData).then(getEffCBF, self.failCbFn);
            //-----------End Of Get And Set Table Data -------------------------
        };

        self.addEFF = function () {
            app.loading(true);
            var naxtPagePram = {eitCoed: self.EitCode, pageMode: "ADD", checkUser: self.checkSubmitUser()};
//            oj.Router.rootInstance.store(naxtPagePram);
            sessionStorage.setItem("StoreObject", JSON.stringify(naxtPagePram));
            oj.Router.rootInstance.go('dynamic');

        };
          
        
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };

        self.closeHelpDialog = function () {
            $("#onlineHelp").ojDialog("close");
        };

        self.helpBody = ko.observable();
        self.HelpEFF = function () {


            var getReportCbFn = function (data) {
                
                if (!data || data.length == 0)
                {
                     $.notify("No help Message found ", "error");
                     self.disableHelpBtn(true);
                     
                    return;
                }


                document.querySelector("#onlineHelp").open();

                if (app.getLocale() == 'ar') {
                    self.helpBody(data[0].descriptionAr);
                } else {

                    self.helpBody(data[0].descriptionEn);
                }

            };

            var failCbFn = function (data) {


            }

            var serviceName = "ManageSelfSerivce/" + self.EitCode;
            services.getAllSummary(serviceName).then(getReportCbFn, app.failCbFn);




        };
        self.updateEFF = function () {
            app.loading(true);
            
            ////oj.Router.rootInstance.store('update');
            self.deptObservableArray()[self.selectedIndex()].pageMode = "EDIT";
            self.deptObservableArray()[self.selectedIndex()].checkUser = self.checkSubmitUser();
//            self.deptObservableArray()[self.selectedIndex()].specialistPersonNumber = self.personDetails().personNumber;
//            self.deptObservableArray()[self.selectedIndex()].specialistPersonId = self.personDetails().personId;
//            self.deptObservableArray()[self.selectedIndex()].specialistManagerId = self.personDetails().managerId;
//            self.deptObservableArray()[self.selectedIndex()].specialistManagerOfManager = self.personDetails().managerOfMnagerName;
//            self.deptObservableArray()[self.selectedIndex()].specialistDisplayNmae = self.personDetails().displayName;
//            self.deptObservableArray()[self.selectedIndex()].specialistManagerName = self.personDetails().self.personDetails().managerName;
            self.deptObservableArray()[self.selectedIndex()].eitCoed = self.EitCode;
            self.deptObservableArray()[self.selectedIndex()].id = self.deptObservableArrayid()[self.selectedIndex()].id;
            self.deptObservableArray()[self.selectedIndex()].status = self.deptArrayStatus()[self.selectedIndex()];
            var sendObJ = self.deptObservableArray()[self.selectedIndex()];
            delete sendObJ["links"];

//            oj.Router.rootInstance.store(sendObJ);
            sessionStorage.setItem("StoreObject", JSON.stringify(sendObJ));
            oj.Router.rootInstance.go('dynamic');
        };
        self.viewEFF = function () {
            app.loading(true);
            self.deptObservableArray()[self.selectedIndex()].pageMode = "VIEW";
            self.deptObservableArray()[self.selectedIndex()].checkUser = self.checkSubmitUser();
//            self.deptObservableArray()[self.selectedIndex()].specialistPersonNumber = self.personDetails().personNumber;
//            self.deptObservableArray()[self.selectedIndex()].specialistPersonId = self.personDetails().personId;
//            self.deptObservableArray()[self.selectedIndex()].specialistManagerId = self.personDetails().managerId;
//            self.deptObservableArray()[self.selectedIndex()].specialistManagerOfManager = self.personDetails().managerOfMnagerName;
            self.deptObservableArray()[self.selectedIndex()].eitCoed = self.EitCode;
            self.deptObservableArray()[self.selectedIndex()].id = self.deptObservableArrayid()[self.selectedIndex()].id;
            var sendObJ = self.deptObservableArray()[self.selectedIndex()];
            delete sendObJ["links"];

//            oj.Router.rootInstance.store(sendObJ);
 sessionStorage.setItem("StoreObject", JSON.stringify(sendObJ));
            oj.Router.rootInstance.go('dynamic');
        };    
         self.closeHelpDialog = function () {
            $("#onlineHelp").ojDialog("close");
        };	
	    self.helpBody = ko.observable();
        self.HelpEFF = function () {


            var getReportCbFn = function (data) {
                
                if (!data || data.length == 0)
                {
                     $.notify("No help Message found ", "error");
                     self.disableHelpBtn(true);
                     
                    return;
                }


                document.querySelector("#onlineHelp").open();

                if (app.getLocale() == 'ar') {
                    self.helpBody(data[0].descriptionAr);
                } else {

                    self.helpBody(data[0].descriptionEn);
                }

            };

            var failCbFn = function (data) {


            }

            var serviceName = "ManageSelfSerivce/" + self.EitCode;
            services.getAllSummary(serviceName).then(getReportCbFn, app.failCbFn);




        };
//        self.ShowReport = function () {
//            
//             var getReportCbFn=function (data){
//                 var base64pdf = 'data:application/pdf;base64,' + data.REPORT;
//                self.EmbedPDFLink(base64pdf);
//                document.querySelector("#pdfViewer").open();
//             };
//            
//            
//            var payload={"reportName":"MOE Employment letter 1"};
//            services.getGenericReport(payload).then(getReportCbFn,app.failCbFn)
//               
//        };
        //----------End Of Operation ------------------------------
        self.getApprovedEitFromSaaS = function () {
            var urlEitCode = {"url": self.urlEit()};
            var getValidGradeCBF = function (data) {

                self.saasApproved.push(data);

            };
            // services.addGeneric("eff/getApproval/",urlEitCode).then(getValidGradeCBF, app.failCbFn);

        };
        self.status_Approval = async function () {
            self.datacrad([]);
            var eitcode = self.EitCode;

            var getValidGradeCBF = function (data) {

                self.approvalResponseArr(data);

            };

            await services.getGenericAsync("approvalList/approvaltype/" + eitcode + "/" + self.t_type()).then(getValidGradeCBF, app.failCbFn);

        };
        var countApproved = 0;
        self.globalDataRow = ko.observableArray();
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.oprationdisabled(false);


            self.selectedRowKey(currentRow['rowKey']);
            self.selectedIndex(currentRow['rowIndex']);

            self.globalDataRow(self.deptObservableArrayid()[self.selectedIndex()]);
            self.t_type(self.deptObservableArrayid()[self.selectedIndex()].id);
            self.status_Approval();
            for (var i = 1; i < self.approvalResponseArr().length; i++) {
                if (self.approvalResponseArr()[i].responseCode == "APPROVED")
                {
                    countApproved++;
                }
            }

            if (self.deptArrayStatus()[self.selectedIndex()] == "APPROVED") {
                 if (self.EitCode === "XXX_HR_Identification_Letter") {
                self.hideReportPdf(true);
                self.oprationdisabled2(true);
                self.oprationdisabled(false);
            }
            else
            {
                self.hideReportPdf(false);
                self.oprationdisabled2(true);
                self.oprationdisabled(false);
                self.deleteDisabled(true);
            }

            } else if(self.deptArrayStatus()[self.selectedIndex()] == "REJECTED" ) {
                self.oprationdisabled2(true);
                self.oprationdisabled(false);
                self.hideReportPdf(false);
                self.deleteDisabled(true);
            }
            else if(self.deptArrayStatus()[self.selectedIndex()] == "Withdraw" ) {
                self.oprationdisabled2(true);
                self.oprationdisabled(true);
                self.hideReportPdf(false);
                self.deleteDisabled(true);
            }
            else{
                self.oprationdisabled2(false);
                self.oprationdisabled(false);
                self.hideReportPdf(false);
                self.deleteDisabled(true);
                if(self.deptArrayStatus()[self.selectedIndex()] == "DRAFT")
                self.deleteDisabled(false);
            }
            
            if (countApproved >= 2) {
                self.oprationdisabled3(false);
                //self.hideReportPdf(false);
            }


        };

        self.BackEFF = function () {
            oj.Router.rootInstance.go('home');
        };

        //-----------------Translation Section --------------------
        self.ok = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.searchLbl = ko.observable();
        self.Approval_list = ko.observable();
        self.onlineHelpLbl = ko.observable();
        self.Approval_name = ko.observable();
        self.Approval_noti = ko.observable();
        self.Approval_status = ko.observable();
        self.Approval_date = ko.observable();
        self.viewLbl = ko.observable();
        self.showReportLbl = ko.observable();
        self.BackLbl = ko.observable();
        self.approvalLBl=ko.observable();
        self.pendingAprovalLbl=ko.observable();
        self.rejectedLbl=ko.observable();
        self.withdrawLbl=ko.observable();
        self.draftLbl=ko.observable();
        self.yes=ko.observable();
        self.RejectReasonLbl=ko.observable();
        self.Reason=ko.observable();
        self.deleteLbl = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.no = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        function initTranslations() {
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.viewLbl(getTranslation("others.view"));
            self.searchLbl(getTranslation("others.search"));
            self.Approval_list(getTranslation("labels.Approval_list"));
            self.onlineHelpLbl(getTranslation("labels.onlineHelpLbl"));
            self.Approval_name(getTranslation("labels.Approval_name"));
            self.Approval_noti(getTranslation("labels.Approval_noti"));
            self.Approval_status(getTranslation("labels.Approval_status"));
            self.Approval_date(getTranslation("labels.Approval_date"));
            self.showReportLbl(getTranslation("labels.showReport"));
            self.BackLbl(getTranslation("labels.backlbl"));
            self.pendingAprovalLbl(getTranslation("labels.pendingAproval"));
            self.approvalLBl(getTranslation("labels.approvalstatus"));
            self.rejectedLbl(getTranslation("labels.rejected"));
            self.draftLbl(getTranslation("labels.draft"));
            self.HELPLbl(getTranslation("labels.help"));
            self.helpHeader(getTranslation("labels.help"));
            self.yes(getTranslation("others.yes"));
            self.helpLbl(getTranslation("labels.help"));
            self.RejectReasonLbl(getTranslation("labels.rejectReason"));
            self.Reason(getTranslation("labels.reason"));
            self.withdrawLbl(getTranslation("Withdraw"));
            self.deleteLbl(getTranslation("labels.Delete"));
            self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
            self.oprationMessage(getTranslation("attachElement.oprationMessage"));
            self.no(getTranslation("others.no"));
//             self.columnArray.push({
//               "headerText": self.statusLbl(), "field": 'status'});
            self.columnArray1([

                {
                    "headerText": self.Approval_name(), "field": "rolrType"
                },
                {
                    "headerText": self.Approval_noti(), "field": "notificationType"
                },
                {
                    "headerText": self.Approval_status(), "field": "responseCode"
                },
                {
                    "headerText": self.Approval_date(), "field": "responseDate"
                }
            ]);
        }
        //-----------------End Of Translation Section -------------

        self.deleteEFF = function () {
            document.querySelector("#yesNoDialog").open();
        };
        self.deleteRecord = function () {
            app.loading(true);
          var element = document.getElementById('table');
            var currentRow = element.currentRow;
            var id = currentRow['rowKey'];
            var getdeleteCBF = function (data) { 
                document.querySelector("#yesNoDialog").close();
                self.deptObservableArray([]);
                self.handleAttached();
                app.loading(false);
            };

            services.getGeneric("PeopleExtra/delete/" + id).then(getdeleteCBF, app.failCbFn);
           
        };
        self.closeDialog=function(){
            document.querySelector("#yesNoDialog").close();
         };
            }

    return SummaryDynamicViewModel;
});
