/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'config/buildScreen', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojbutton',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup', 'ojs/ojformlayout', , 'ojs/ojselectcombobox', 'ojs/ojtimezonedata', 'ojs/ojdatetimepicker','ojs/ojfilepicker','ojs/ojinputtext'],
        function (oj, ko, $, app, services, commonhelper, buildScreen) {

            function dynamicOperationScreenViewModel() {
                var self = this;
                self.showForm = ko.observable();
                self.lang = "";
                var Typs = ['date', 'date', 'number'];
                self.pageMode = ko.observable();
                var Keys = [];
                var PaaSKeys = [];
                self.isLastApprover = ko.observable(false);
                var disabeld = [];
                var VALUES = [];
                self.dataArray = [{value: 'zz', label: 'zz'}];
                self.validationCheck = ko.observable(false);
                self.currentValueText = ko.observable();
                self.rejectDialogBody = ko.observable();
                self.valueText = ko.observable();
                self.url;
                self.actionModelsArr = [];
                self.reqestedID;
                self.workFlowApprovalId=ko.observable();
                var tempObject;
                var approvalManagmant;
                self.reqeustedPersonNumber;
                var key = 'dynamicKey';
                var key2 = 'dynamicKey2';
                self.isDisabledx = {};
                var ArrKeys = [];
                self.Arrs = {};
                self.model = {};
                self.PaaSmodel = {};
                self.approvlModel = {};
                var globalRowData;
                self.messages = ko.observable();
                self.showAttachment = ko.observable(); 
                self.hideReportPdf = ko.observable(false);
                self.disableRejectReason = ko.observable(false);
                self.EmbedPDFLink = ko.observable("");
                self.showReportLbl = ko.observable();
                self.reportType = ko.observable();
                self.employeeNameVal = ko.observable();
                self.locationVal = ko.observable();
                self.employeeNumberVal = ko.observable();
                self.organizationVal = ko.observable();
                self.hireDateVal = ko.observable();
                self.jobVal = ko.observable();
                self.gradeVal = ko.observable();
                self.positionVal = ko.observable();
                 var cloneObject = function (object) {
                            var clone ={};
                            for( var key in object ){
                                if(object.hasOwnProperty(key)) //ensure not adding inherited props
                                    clone[key]=object[key];
                            }
                            return clone;
                        };
                self.actionAvilable = ko.observable(true);
                self.paasDef = ko.observable();
                self.getPaaSEitesValues = function () {
                    var getPaasEitsValuesCBFN = function (data) {
                        self.rejectReason(data.rejectReason);
                        globalRowData=data;
                        self.reqeustedPersonNumber = data.person_number;
                        self.reqeustedAssignmentNumber = data.assignmentNumber;
                        var jsonData = JSON.parse(data.eit);
                        var values = JSON.parse(jsonData);
                        
                        for (var i = 0; i < Object.keys(self.model).length; i++) {
                                for (var j = 0; j < Object.keys(values).length; j++) {
                                if (Object.keys(self.model)[i] == Object.keys(values)[j]) {

                                    self.model[Object.keys(self.model)[i]](values[Object.keys(values)[j]]);
                                    //For Set Dependent List 
                                    if (tempObject[i].value_SET_TYPE == "X" && tempObject[i + 1].value_SET_TYPE == "Y") {
                                        var getDependentReportCBCF = function (data) {
                                            var tempObject2 = data;
                                            var arr = tempObject2;
                                            var arrays = [];
                                            if (arr.length) {
                                                for (var ind = 0; ind < arr.length; ind++) {
                                                    arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION})
                                                }
                                            } else {
                                                arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                            }
                                            
                                            self.Arrs[tempObject[i + 1].FLEX_value_SET_NAME + tempObject[i + 1].DESCRIPTION + "Arr"](arrays);
                                        }
                                        var xx = {"reportName": "DependentReport", "valueSet": tempObject[i + 1].FLEX_value_SET_NAME, "parent": self.model[Object.keys(self.model)[i]]()};
                                        services.getGenericReport(xx).then(getDependentReportCBCF, app.failCbFn);
                                    }
                                    //End Of Set Dependent Value Set 

                                }


                            }
//                     if (Object.keys(self.model)[i]==Object.keys(values)[i]){                        
//                          self.model[Object.keys(self.model)[i]](values[Object.keys(values)[i]]);
//                     }
                        }
                        if (globalRowData.code == "XXX_BUSINESS_TRIP_REQUEST" ||globalRowData.code == "XXX_HR_OVERTIME_REQUEST" || globalRowData.code == "XXX_MOBILE_ALLOWANCE_REQUEST" || globalRowData.code == "XXX_FIELD_ALLOWANCE_REQUEST") {
                            
                            self.paasDef(values.referenceNumber);
                            //self.model.referenceNumber(values.referenceNumber);
                        }
                        if(app.showForm()){
                        self.searchPerson();
                    }
                        self.url = data.url;
                    };
                    services.getGeneric("PeopleExtra/" + self.retrieve.TRS_ID).then(getPaasEitsValuesCBFN, app.failCbFn);
                };
                self.Employeedata = ko.observableArray([]);
                var getPersonDetailsCbFn = function (data) {
                    self.Employeedata.push({
                                    department: data.department, displayName: data.displayName, personNumber: data.personNumber,
                                    employeeLocation: data.employeeLocation, grade: data.grade, hireDate: data.hireDate, jobName: data.jobName,
                                    organizationName: data.organizationName, positionName: data.positionName, departmentName:data.departmentName
                     });
                     self.employeeNameVal(self.Employeedata()[0].displayName);
                    self.locationVal(self.Employeedata()[0].employeeLocation);
                    self.employeeNumberVal(self.Employeedata()[0].personNumber);
                    self.organizationVal(self.Employeedata()[0].departmentName);
                    self.hireDateVal(self.Employeedata()[0].hireDate);
                    self.jobVal(self.Employeedata()[0].jobName);
                    self.gradeVal(self.Employeedata()[0].grade);
                    self.positionVal(self.Employeedata()[0].positionName);
              
                };

                self.searchPerson = function () {
                    self.Employeedata([]);            
                            services.searchEmployees("", "", self.reqeustedPersonNumber, "", "").then(getPersonDetailsCbFn, app.failCbFn); 
                        

                };
                //-----------get attchment-------------------//
                self.getAttachment=function(){
                    self.koArray([]);
                      var attachmentView = function (data) {
                    for (var i = 0; i < data.length; i++) {

//                                    var baseStr64 = data[i].attachment;
//                                    drawAttachment2(i, baseStr64);
                        self.koArray.push({
                            "id": data[i].id,
                            "name": data[i].name,
                            "data": data[i].attachment
                        });

                    }

                    // imgElem.setAttribute('src', baseStr64);
                };
                services.getGeneric("attachment/" + self.retrieve.TRS_ID).then(attachmentView, app.failCbFn);
            }
                //-----------get attchment-------------------//
                self.handleAttached = function (info) {
                    self.showForm(app.showForm());
                    self.isShownBtnCloseNotification(false);
                    self.rejectReason(" ");
                    app.loading(false);
                    self.showAttachment(false);
                    //self.viewAttachmentBtnVisible(true);
                    self.disableSubmit(false);
                     ArrKeys = [];  
                    self.retrieve = oj.Router.rootInstance.retrieve();    
                    self.workFlowApprovalId(self.retrieve.id);
                    //MSG_Body_Lbl
                    self.headerLbl(self.retrieve.MSG_Body_Lbl+' / '+self.retrieve.person_number);
                    self.EitCode = rootViewModel.reviewNotiType();
                    if (self.EitCode === "XXX_HR_Identification_Letter") {
                        if(app.personDetails().positionId == "300000002116731"){
                           self.showReportLbl(getTranslation("labels.downloadDoc")); 
                           self.reportType("word");
                        }
                        else{
                            self.reportType("pdf");
                            self.showReportLbl(getTranslation("labels.showReport"));
                        }     
                        self.hideReportPdf(true);
                    } else {
                        self.hideReportPdf(false);
                    }
                    self.reqestedID = self.retrieve.person_id;
                     approvalManagmant = [];
                    self.personDetails = ko.observableArray();
                        self.personDetails(app.personDetails());
                     initTranslations();
                    //getElementEntryByEitCode();
                    if (self.retrieve.TYPE == "FYI") {
                        self.isShown(false);
                        self.isShownBtnCloseNotification(true);
                        self.showAttachment(true);
                        self.disableRejectReason(true);
                    }
                    else{
                        self.isShown(true);
                     }
                    self.valueText("Person Number :"+self.retrieve.person_number);
                    self.currentValueText()
                    self.currentStepValue('stp1');
                    //self.prevPram= oj.Router.rootInstance.retrieve();
                    
                    //self.pageMode(self.prevPram.pageMode);
                    self.selectedPage(self.pageMode());
                    
                    self.isLastApprover(rootViewModel.isLastApprover(self.retrieve.TRS_ID, self.EitCode,false));
                    
                    self.approvalType = self.retrieve.receiver_type;
                    self.roleID = self.retrieve.roleId;
                    var getApprovalManagmantCBFN = function (data) {

                        approvalManagmant = data;
                    }
                    if (self.approvalType == "POSITION") {
                        services.getGeneric("approvalManagement/search/" + self.EitCode + "/" + self.approvalType + "/" + self.roleID, {}).then(getApprovalManagmantCBFN, app.failCbFn);
                    } else
                    {
                        services.getGeneric("approvalManagement/search/" + self.EitCode + "/" + self.approvalType, {}).then(getApprovalManagmantCBFN, app.failCbFn);
                    }
                    var getEit = function (data) {
                                          tempObject = data;
                    if (tempObject.length) {

                    } else {
                        tempObject = [tempObject];
                    }
                    Keys = [];
                    var Iterater = 0;
                        disabeld=[];
                        for (var i = 0; i < tempObject.length; i++) {
                            Keys.push(tempObject[i].DESCRIPTION);
                        if (Keys[i] == "dummy") {
                            self.model[Keys[i]] = ko.observable(new Date().getTime());
                        } else {
                            self.model[Keys[i]] = ko.observable();
                            if (tempObject[i].IN_FIELD_HELP_TEXT && tempObject[i].IN_FIELD_HELP_TEXT != "PAAS") {
                                var actionModel = {
                                    reportName: tempObject[i].IN_FIELD_HELP_TEXT,
                                    whenChangeValue: [],
                                    segmentName: Keys[i]
                                };


                                actionModel.whenChangeValue = searchArrayContainValue(tempObject[i].IN_FIELD_HELP_TEXT, app.globalEITDefultValueParamaterLookup());
                                if (actionModel.whenChangeValue[0]) {
                                    actionModel.whenChangeValue = actionModel.whenChangeValue[0].split(',');
                                }
                              if(actionModel.whenChangeValue[0]==undefined){
                                  actionModel.whenChangeValue=[] ;
                              } 

                                self.actionModelsArr.push(actionModel);
                                for (var index = 0; index < actionModel.whenChangeValue.length; index++) {


                                    var reportPaylod = {"reportName": tempObject[i].IN_FIELD_HELP_TEXT};
                                    var completeParametar = true;
                                    for (var index = 0; index < actionModel.whenChangeValue.length; index++) {
                                        if (Object.keys(self.model).indexOf(actionModel.whenChangeValue[index]) != -1) {
                                            if (self.model[actionModel.whenChangeValue[index]]()) {
                                                reportPaylod[actionModel.whenChangeValue[index]] = self.model[actionModel.whenChangeValue[index]]();
                                            } else {
                                                completeParametar = false;
                                            }

                                        } else {
                                            if (self.personDetails()[actionModel.whenChangeValue[index]]) {
                                                reportPaylod[actionModel.whenChangeValue[index]] = self.retrieve.person_id;//self.personDetails()[actionModel.whenChangeValue[index]];
                                            } else {
                                                completeParametar = false;
                                            }

                                        }
                                    }
                                    var getReportValidationCBCF = function (iterator1, dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;
                                          if (dataSaaSQuery != '[]'){
                                                self.model[Keys[iterator1]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                                          }else{
//                                              self.model[Keys[iterator1]]('');
                                          }
                                      

                                    };
                                    if (completeParametar) {
                                        services.getGenericReportAsync(reportPaylod).then(getReportValidationCBCF.bind(data,i), app.failCbFn);
                                    }



                                }
                                if (actionModel.whenChangeValue.length == 0) {
                                    var reportPaylod = {"reportName": tempObject[i].IN_FIELD_HELP_TEXT};
                                    var getReportValidationCBCF = function (iterator2, dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;
                                        
                                      if(resultOfQueryObj!='[]'){
                                           self.model[Keys[iterator2]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                                      }
                                       

                                    };
                                    services.getGenericReportAsync(reportPaylod).then(getReportValidationCBCF.bind(data,i), app.failCbFn);
                                }
                                





                            }
//                            else if (tempObject[i].DEFAULT_value && tempObject[i].DEFAULT_value != "PAAS" && tempObject[i].DEFAULT_TYPE != 'C') {
//                                var actionModel = {
//                                    str: tempObject[i].DEFAULT_value,
//                                    whenChangeValue: [],
//                                    segmentName: Keys[i]
//                                };
//                                var str = tempObject[i].DEFAULT_value;
//                                var flex = ":";
//                                var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
//                                var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
//                                var is_HaveFlex = true;
//                                if (str.indexOf(':') == -1) {
//                                    is_HaveFlex = false;
//                                }
//                                while (is_HaveFlex) {
//                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
//                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
//                                    actionModel.whenChangeValue.push(ModelName);
//                                    strWitoutFlex.substring(1, strWitoutFlex.length);
//
//                                    if (Object.keys(self.personDetails()).indexOf(strWitoutFlex.substring(1, strWitoutFlex.length)) != -1) {
//                                        str = str.replace(strWitoutFlex, self.retrieve.person_id);
//                                    } else if (Object.keys(self.model).indexOf(strWitoutFlex.substring(1, strWitoutFlex.length - 1)) != -1) {
//                                        str = str.replace(strWitoutFlex, self.model[strWitoutFlex.substring(1, strWitoutFlex.length - 1)]());
//                                    }
//
//                                    is_HaveFlex = false;
//                                    if (str.indexOf(":") == -1) {
//                                        is_HaveFlex = false;
//                                    }
//                                }
//
//
//                                if (str.includes('undefined')) {
//                                } else {
//                                    //Here Must Call Report Fpr This Perpus 
//                                    var getReportValidationCBCF = function (iterator3, dataSaaSQuery) {
//
//                                        var resultOfQueryObj = dataSaaSQuery;
//
//                                        self.model[Keys[iterator3]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
//
//                                    };
//                                    var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
//                                    services.getGenericReportAsync(validationQueryReportModel).then(getReportValidationCBCF.bind(data,i), app.failCbFn);
//
//                                }
//                            }
                            else if (tempObject[i].DEFAULT_TYPE == 'C') {
                                self.model[Keys[i]](tempObject[i].DEFAULT_value);
                            } else if (tempObject[i].IN_FIELD_HELP_TEXT == "PAAS") {
                                var defultUrl = getPaaSDefultValue(tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_CODE, tempObject[i].DESCRIPTION, app.allPaaSDefultValuearry());
                                var getDefultPaaSCbfn = function (iterator4, data) {
                                    var jsonData = JSON.parse(data);
                                    self.model[Keys[iterator4]](self.paasDef());
                                };
                                
                                
                                
                                
                                
                                

                                services.getGenericAsync(defultUrl).then(getDefultPaaSCbfn.bind(data,i), app.failCbFn);

                            }

                        }
                         
                        PaaSKeys.push(tempObject[i].APPLICATION_COLUMN_NAME);
                        self.PaaSmodel[PaaSKeys[i]] = ko.observable(self.model[Keys[i]]());
                            //Build Model 
                           
                            //end OF Build Model 
                            if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE")&&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME")
                                    ) {
                            if (tempObject[i].value_SET_TYPE == "X" || tempObject[i].value_SET_TYPE == "I") {
//                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                var getReportCBCF = function (iterator5, data) {
                                    var tempObject2 = data;
                                    var arr = tempObject2;
                                    var arrays = [];
                                    for (var x = 0; x < arr.length; x++) {
                                        arrays.push({"value": arr[x].FLEX_value, "label": arr[x].value_DESCRIPTION});

                                    }

                                    self.Arrs[ArrKeys[iterator5]](arrays);


                                };
                                var lang = '';
                                if (localStorage.getItem("selectedLanguage") == 'ar') {
                                    lang = "AR";
                                } else {
                                    lang = "US";
                                }
                                var xx = {"reportName": "inDependentReport", "valueSet": tempObject[i].FLEX_value_SET_NAME,"langCode":lang};
                                services.getGenericReportAsync(xx).then(getReportCBCF.bind(data,Iterater), app.failCbFn);
                                Iterater = Iterater + 1;
                            } else if (tempObject[i].value_SET_TYPE == "Y" || tempObject[i].value_SET_TYPE == "D") {
//                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();

                                Iterater = Iterater + 1;

                            } else if (tempObject[i].value_SET_TYPE == "F") {
//                                self.ishide(false);
                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                var val = "value";
                                var lbl = "label";
                                var q;
                                // var tempObject = data;
                                q = "select " + tempObject[i].ID_COL + " " + val + " ,  " + tempObject[i].value_COL + "  " + lbl
                                        + " From " + tempObject[i].APPLICATION_TABLE_NAME + "  ";
                                if (tempObject[i].ADDITIONAL_WHERE_CLAUSE) {
                                    q = q + tempObject[i].ADDITIONAL_WHERE_CLAUSE;
                                }

                                var str = q;

                                var flex = ":";
                                var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));

                                var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                var is_HaveFlex = true;
                                if (str.indexOf(':') == -1) {
                                    is_HaveFlex = false;
                                }
                                while (is_HaveFlex) {
                                    var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                    var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                    actionModel.whenChangeValue.push(ModelName);
                                    str = str.replace(strWitoutFlex, self.retrieve.person_id);
                                    if (str.indexOf(":") == -1) {
                                        is_HaveFlex = false;
                                    }
                                }




                                if (str.includes('undefined')) {
                                } else {
                                    //Here Must Call Report Fpr This Perpus 
                                    var getArrCBCF = function (iterator6, data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        if (tempObject2.length) {
                                            self.Arrs[ArrKeys[iterator6]](arr);
                                        } else {
                                            self.Arrs[ArrKeys[iterator6]]([arr]);
                                        }
                                    };
                                    services.getDynamicReport(str).then(getArrCBCF.bind(data,Iterater), app.failCbFn);

                                }
                                Iterater = Iterater + 1;
                            } else if (tempObject[i].FLEX_value_SET_NAME == "XXX_HR_DYNAMIC_COLUMN") {

                                ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                self.Arrs[ArrKeys[Iterater]] = ko.observableArray();

                                var reportName = searchArray(tempObject[i].DESCRIPTION, app.globalEITReportLookup());

                                var reportParamter = searchArrayContainDescription(tempObject[i].DESCRIPTION, app.globalEITReportLookup());

                                var reportPaylad = {"reportName": reportName};
                                reportParamter = reportParamter[0].split(',');
                                for (var reportIndex = 0; reportIndex < reportParamter.length; reportIndex++) {
                                    if (Object.keys(self.model).indexOf(reportParamter[reportIndex]) != -1) {
                                        reportPaylad[reportParamter[reportIndex]] = self.model[reportParamter[reportIndex]]();
                                    } else {
                                        reportPaylad[reportParamter[reportIndex]] = self.retrieve.person_id;//self.personDetails()[reportParamter[reportIndex]];
                                    }

                                }
                                var getReportCBCF = function (iterator7, data) {
                                    var tempObject2 = data;
                                    var arr = tempObject2;
                                    if (tempObject2.length) {
                                        self.Arrs[ArrKeys[iterator7]](arr);
                                    } else {
                                        self.Arrs[ArrKeys[iterator7]]([arr]);
                                    }

                                };//personId
                                var lang = '';
                                if (localStorage.getItem("selectedLanguage") == 'ar') {
                                    lang = "AR";
                                } else {
                                    lang = "US";
                                }
                                var reportPaylad = {"reportName": reportName, "personId": self.retrieve.person_id,"langCode":lang};
                                services.getGenericReportAsync(reportPaylad).then(getReportCBCF.bind(data,Iterater), app.failCbFn);
                                Iterater = Iterater + 1;

                            }
                            
                        }
                        else if (tempObject[i].FLEX_value_SET_NAME === "XXX_HR_EIT_ATTACHMENTS") {
                           // self.ishide(true);
                                if (approvalManagmant.length != 0) {
                                for(var xx = 0; xx < approvalManagmant.length; xx++){
                                   
                                   
                                    if(approvalManagmant[xx].segementName == "attachment"){
                                         if (approvalManagmant[xx].enable == "true")
                                            {
                                                self.viewAttachmentBtnVisible(true);
                                            } else if (approvalManagmant[xx].enable == "false")
                                            {
                                                self.viewAttachmentBtnVisible(false);
                                            }else {
                                                self.viewAttachmentBtnVisible(true);
                                            }
                                        if (approvalManagmant[xx].hide == "true")
                                                {
                                                    self.showAttachment(false);
                                                } else if (approvalManagmant[xx].hide == "false")
                                                {
                                                    self.showAttachment(true);
                                                } else {
                                                    self.showAttachment(true);
                                                }
                                    }
                                }
                            }
                            else{
                                self.showAttachment(true);
                                //self.viewAttachmentBtnVisible(true);
                            }
                            ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + "Arr");
                            self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                            Iterater = Iterater + 1;
                        }

                            //Build Disabeld Model 
                            var checkSegmentApproval = false;
                            if (approvalManagmant.length != 0) {
                           for (var j = 0; j < approvalManagmant.length; j++) {
                                        if(checkSegmentApproval){
                                                    checkSegmentApproval = false;
                                                    break;
                                    }
                               if (tempObject[i].DESCRIPTION == approvalManagmant[j].segementName) {
                                   if(tempObject[i].DESCRIPTION == 'dummy') {
                                       tempObject[i].DISPLAY_FLAG = 'N'
                                     
                                   }
                                   if (approvalManagmant[j].enable == "true") {
                                       disabeld.push("isDisabled" +i);
                                   } else if (approvalManagmant[j].enable == "false"){
                                       disabeld.push("isDisabled" + tempObject[i].DESCRIPTION);
                                   }
                                   self.isDisabledx[disabeld[i]] = ko.observable(true);
                                   if (approvalManagmant[j].hide == "true")
                                   {
                                       tempObject[i].DISPLAY_FLAG = 'N';
                                   } else if (approvalManagmant[j].hide == "false" && tempObject[i].DESCRIPTION != 'dummy')
                                   {
                                       tempObject[i].DISPLAY_TYPE= "TEXT_BOX";
                                       tempObject[i].DISPLAY_FLAG = 'Y';
                                   } 
                                   if (approvalManagmant[j].required == "true") {
                                       tempObject[i].REQUIRED_FLAG = 'Y';
                                   } else if (approvalManagmant[j].required == "false") {
                                       tempObject[i].REQUIRED_FLAG = 'N';
                                   }
                               }
                               else {
                                   if (tempObject[i].ENABLED_FLAG == "Y") {
                                       self.isDisabledx[disabeld[i]] = ko.observable(true);
                                   } else if (tempObject[i].ENABLED_FLAG == "N") {
                                       self.isDisabledx[disabeld[i]] = ko.observable(true);
                                   }
                               }
                           }
                       } 
                       else {
                           disabeld.push("isDisabled" + tempObject[i].DESCRIPTION);
                           if (tempObject[i].ENABLED_FLAG == "Y") {
                               self.isDisabledx[disabeld[i]] = ko.observable(true);
                           } else if (tempObject[i].ENABLED_FLAG == "N") {
                               self.isDisabledx[disabeld[i]] = ko.observable(true);
                           }
                       }
                       //End Of Disabeld Model
                       





                            Typs.push(tempObject[i].FLEX_value_SET_NAME);
                        }
                        buildScreen.buildScreen(tempObject, "xx", self.model, self.isDisabledx, self.dataArray);
                        // self.actionAvilable(true);
                        //  self.setToDisabeld();
                        self.getPaaSEitesValues();
                        //getElementEntryByEitCode();
                        self.getAttachment();
                    };
                    if (app.getLocale() == 'ar') {
                        self.lang = "AR";
                    } else {
                        self.lang = "US";
                    }
                    services.getEIT(self.EitCode, self.lang).then(getEit, app.failCbFn);
                };
                self.setModelValue = function () {
                    for (var i = 0; i < Object.keys(self.model).length; i++) {
                        for (var j = 0; Object.keys(self.prevPram).length; j++) {
                            if (Object.keys(self.model)[i] == Object.keys(self.prevPram)[j]) {
                                // This For Set Value In Observable From Prev Page 
                                //self.prevPram[Object.keys(self.prevPram)[j]] (Value We Set in Observable )
//                   // self.model[Object.keys(self.model)[i]] Observable Are Link To Screen 
                                self.model[Object.keys(self.model)[i]](self.prevPram[Object.keys(self.prevPram)[j]]);
                                break;
                            }
                        }
                    }
                    self.model.PersonExtraInfoId = self.prevPram.PersonExtraInfoId;
                    self.model.PersonId = self.prevPram.PersonId;
                };




                self.segmentChangedHandler = async function (event , data) {
                    //document.body.style.cursor='wait';
                    
                    var isEdge;
                    if (navigator.userAgent.indexOf('Edge') >= 0) {
                        isEdge = true;
                    } else {                       
                        isEdge = false;
                    }
                    if ((!isEdge&&rootViewModel.BrowserDetect.browser=='Chrome'&&typeof event.path[0].attributes.name!= 'undefined')||
                            (typeof event.target.attributes.name!= 'undefined' && rootViewModel.BrowserDetect.browser=='Firefox' )||isEdge) {
                       if(!isEdge&&rootViewModel.BrowserDetect.browser=='Chrome'){
                            var id = event.path[0].attributes.name.nodeValue;
                       }else if ( rootViewModel.BrowserDetect.browser=='Firefox' ){
                            var id =event.target.attributes.name.nodeValue ; 
                       }else if(isEdge){
                            id =event.target.attributes.name.value;
                       }
                       
                                 
                        var res = id.substr(6, id.length);

                        var modelArr = Object.keys(self.model);
                        var realIndex = modelArr.indexOf(res);
                        var indexOfModel = modelArr.indexOf(res) + 1;
                         if(self.EitCode == 'XXX_HR_Identification_Letter' && realIndex == 2) {
                            var indexOfModel2 = modelArr.indexOf(res) + 5;
                        }
                        //  value_SET_TYPE
                        if (tempObject[indexOfModel].value_SET_TYPE == "Y" || tempObject[indexOfModel].value_SET_TYPE == "D") {

                            var getReportCBCF = function (data) {

                                var tempObject2 = data;
                                var arr = tempObject2;
                                var arrays = [];
                                if (arr.length) {
                                    for (var ind = 0; ind < arr.length; ind++) {
                                        arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION});
                                    }
                                } else {
                                    arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                }
                                //DESCRIPTION

                                self.Arrs[tempObject[indexOfModel].FLEX_value_SET_NAME + tempObject[indexOfModel].DESCRIPTION + "Arr"](arrays);


                            };
                            var xx = {"reportName": "DependentReport", "valueSet": tempObject[indexOfModel].FLEX_value_SET_NAME, "parent": self.model[modelArr[realIndex]]()};

                            await services.getGenericReportAsync(xx).then(getReportCBCF, app.failCbFn);
                        }
                        
                        if(self.EitCode == 'XXX_HR_Identification_Letter' && realIndex == 2) {
                            if (tempObject[indexOfModel2].value_SET_TYPE == "Y" || tempObject[indexOfModel2].value_SET_TYPE == "D") {

                                var getReportCBCF = function (data) {

                                    var tempObject2 = data;
                                    var arr = tempObject2;
                                    var arrays = [];
                                    if (arr.length) {
                                        for (var ind = 0; ind < arr.length; ind++) {
                                            arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION});
                                        }
                                    } else {
                                        arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                    }
                                    //DESCRIPTION

                                    self.Arrs[tempObject[indexOfModel2].FLEX_value_SET_NAME + tempObject[indexOfModel2].DESCRIPTION + "Arr"](arrays);


                                };
                                var xx = {"reportName": "DependentReport", "valueSet": tempObject[indexOfModel2].FLEX_value_SET_NAME, "parent": self.model[modelArr[realIndex]]()};

                                await services.getGenericReportAsync(xx).then(getReportCBCF, app.failCbFn);
                            }
                         }

                    }
                    if ((!isEdge&&rootViewModel.BrowserDetect.browser=='Chrome'&&event.path[0]!= 'undefined')||
                            (rootViewModel.BrowserDetect.browser=='Firefox'&&event.originalTarget.classList[0]!='undefined' )||isEdge) {
                      if(!isEdge&&rootViewModel.BrowserDetect.browser=='Chrome'){
                           var curuntElement = event.path[0].classList[0];
                      }else if(rootViewModel.BrowserDetect.browser=='Firefox'){
                           var curuntElement = event.originalTarget.classList[0];
                      }else if(isEdge){
                          var curuntElement = event.srcElement.classList[0];
                      }
                       
                        var reportList = [];
                        //$('.' + curuntElement).find('[class*="loading"]').addClass("loading");

                        for (var index = 0; index < self.actionModelsArr.length; index++) {
                            var actionObject = self.actionModelsArr[index];
                            if (self.actionModelsArr[index].whenChangeValue.indexOf(curuntElement) != -1) {
                                var haveUndifindValue = false;
                                var reportPaylod = {"reportName": actionObject.reportName};
                                for (var whenCangeIndex = 0; whenCangeIndex < actionObject.whenChangeValue.length; whenCangeIndex++)
                                    if (Object.keys(self.model).indexOf(actionObject.whenChangeValue[whenCangeIndex]) != -1) {
                                        if (self.model[actionObject.whenChangeValue[whenCangeIndex]]() || self.model[actionObject.whenChangeValue[whenCangeIndex]]() == 0) {
                                            reportPaylod[actionObject.whenChangeValue[whenCangeIndex]] = self.model[actionObject.whenChangeValue[whenCangeIndex]]().toString();

                                        }else {
                                        haveUndifindValue = true;
                                    }
                                    } else if (Object.keys(self.personDetails()).indexOf(actionObject.whenChangeValue[whenCangeIndex]) != -1) {
                                        if (self.personDetails()[actionObject.whenChangeValue[whenCangeIndex]]) {
                                            reportPaylod[actionObject.whenChangeValue[whenCangeIndex]] = self.retrieve.person_id;//self.personDetails()[actionObject.whenChangeValue[whenCangeIndex]];
                                        }else {
                                        haveUndifindValue = true;
                                    }
                                    } else {
                                        haveUndifindValue = true;
                                    }
                                    if (!haveUndifindValue) {

                                if (reportPaylod && reportPaylod.reportName) {
                                    reportPaylod.segmentName = actionObject.segmentName;
                                    reportList.push({nameSeg: cloneObject(reportPaylod)});
                                }
                            }
                            }
                            


//                           if(str.includes('undefined')){
//                           }else {
//                               //Here Must Call Report Fpr This Perpus 
//                                        var getReportValidationCBCF = function (dataSaaSQuery) {
//                                            var resultOfQueryObj = dataSaaSQuery;
//                                           self.model[self.actionModelsArr[index].segmentName](resultOfQueryObj[Object.keys(dataSaaSQuery)[0]]); 
//                                            
//                                        }
//                                        var validationQueryReportModel = {"reportName": 'DynamicValidationReport', "str": str};
//                                        services.getGenericReport(validationQueryReportModel).then(getReportValidationCBCF, app.failCbFn);
//                                   
//                           }
                        }
                        
                       
                        
                        var getReportDefaultDataBulk = function (dataSaaSQuery) {

                            var resultOfQueryObj = dataSaaSQuery;
//                                       errorcheck=resultOfQueryObj;
                           
                                
                                    for (var i = 0; i < resultOfQueryObj.length; i++) {
                                        try {
                                        var segmantkey = Object.keys(resultOfQueryObj[i])[0];
                                        var reportValue = Object.values(resultOfQueryObj[i][segmantkey])[0];
                                        if (reportValue != '[]') {
                                            self.model[segmantkey](reportValue);
                                           
                                        } else {
                                            try {
                                                for (var i = 0; i < tempObject.length; i++) {
                                                    if (tempObject[i].DESCRIPTION == actionObject.segmentName) {
                                                             
                                                        if(tempObject[i].FLEX_value_SET_NAME === "XXX_HR_PAAS_NUMBER") {
                                                            self.model[actionObject.segmentName](0);
                                                            break;
                                                        } else {
                                                            self.model[actionObject.segmentName]('');
                                                        }

                                                    }

                                                }

            //                                             self.model[actionObject.segmentName]('');    
                                            } catch (e) {
                                                
                                                self.model[actionObject.segmentName](0);
                                            }


                                        }
                                        
                                          
                                        } catch (e) {
                                           
                                   // self.model[actionObject.segmentName](0);
                                }
                                }
//                                     for (var i = 0; i < resultOfQueryObj.length; i++) {
                                           
//                                         if (tempObject[i].DESCRIPTION == actionObject.segmentName) {

//                                             if(tempObject[i].FLEX_value_SET_NAME === "XXX_HR_PAAS_NUMBER") {
//                                                 self.model[actionObject.segmentName](0);
//                                                 break;
//                                             } else {
//                                                 self.model[actionObject.segmentName]('');
//                                             }

//                                         }

//                                     }

//                                             self.model[actionObject.segmentName]('');    
                              
                            $('.' + curuntElement).find('[class*="loading"]').addClass("loading");

                        };
                        if(reportList.length > 0){
                            $('input').closest('[class*="oj-disabled"]').find('input').addClass("loading");
                            await services.getGenericReportAsync(reportList).then(getReportDefaultDataBulk, app.failCbFn);
                        }
                        $('input').closest('[class*="oj-disabled"]').find('input').removeClass("loading");
                        //if(self.actionModelsArr[index].whenChangeValue)
                    }
                    //document.body.style.cursor='default';
                };
self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                   pattern: 'dd/MM/yyyy'
               }));


                //----------------------------Standerd Screen -------------------------------------------
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.dataSourceTB2 = ko.observable();
                var deptArray = [];
                self.isDisabled = ko.observable(false);
                self.selctedStruct = ko.observable();
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);





                self.headerLbl = ko.observable('Noor');
                self.isShown = ko.observable(true);
                self.isShownBtnCloseNotification = ko.observable(false);



                self.supervisorHRCYesNoList = rootViewModel.globalHRCYesNo();
                self.oprationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);

                var Url;
                var positionObj;
                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);//workNatureAllowance

                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };
            //-------------this Function for check  length ----------------------  
            self.lengthValidator = ko.computed(function () {
                    return [{
                            type: 'length',
                            options: {
                                min: 0, max: 100,
                                countBy: 'codeUnit',
                                hint: {
                                    inRange: 'value must have at least 0  characters but not more than 100'
                                },
                                messageSummary: {
                                    tooLong: ' Too many characters',
                                    tooShort: ' Too few characters'
                                },
                                messageDetail: {
                                    tooLong: 'Number of characters is too high. Enter at most 100 characters',
                                    tooShort: ' Number of characters is too low. Enter at least 0 characters.'
                                }
                            }
                        }];
                });
                //----------------------------End------------------


                //-------------This Function For Update Train Step To previousStep------------------


                self.setToDisabeld = function () {
                    for (var i = 0; i < Object.keys(self.isDisabledx).length; i++) {
                        self.isDisabledx[Object.keys(self.isDisabledx)[i]](true);
                    }
                };
                self.setToDefultDisabeld = function () {
                    for (var i = 0; i < Object.keys(self.isDisabledx).length; i++) {
                        self.isDisabledx[Object.keys(self.isDisabledx)[i]](false);
                    }
                };
                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);
                        self.setToDisabeld();

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                        self.setToDefultDisabeld();
                    }

                    return self.selectedPage();
                };
                //------------------End Of Section -------------------------------------------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                //------------------End Of Section ----------------------------------------------
                //-------------------This Function For Reject -------------------------
                self.rejectButton = function () {
                    if(self.rejectReason() == null || self.rejectReason().length <= 0){
                       
                        self.messages([{
                            severity: 'error',
                            summary: "Please fill the reject reason text",
                            autoTimeout: 5000
                        }]);
                    }
                    else{
                   document.querySelector("#rejectDialog").open();
                   var getValidGradeCBF=function(data){
                       if(!data==="[]"){
                       if(localStorage.getItem("selectedLanguage") == 'ar'){
                         self.rejectDialogBody(data[0].rejectConfirmMessageArVal);
                        }else{
                             self.rejectDialogBody(data[0].rejectConfirmMessageVal);
                        }
                    }else{
                         if(localStorage.getItem("selectedLanguage") == 'ar'){
                             self.rejectDialogBody("هل انت متاكد من الرفض؟");

                        }else{
                       self.rejectDialogBody("Are your sure to reject?");

                        }
                        
                    }
                       
                   };
               }
                    services.getGeneric(commonhelper.getSelfService + self.EitCode, {}).then(getValidGradeCBF, app.failCbFnfailCbFn);
                    
                };
                self.rejectCancelButton = function () {
                    document.querySelector("#rejectDialog").close();
                };
                //-------------------End Of Reject --------------------------------
                //
                self.cancelRewaardRequst = function () {
                    //  rootViewModel.updateNotificaiton(self.notiId());
                    oj.Router.rootInstance.go('notificationScreen');

                    return true;
                };
                //
                //
                //
                self.rejectRewaardRequst = function (data, event) {
                    self.disableSubmit(true);
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "RESPONSE_CODE": "REJECTED",
                        "ssType": self.retrieve.SELF_TYPE,
                        "rejectReason":self.rejectReason()  ,
                        "workflowId":self.workFlowApprovalId(),
                        "PERSON_NAME":self.retrieve.personName
                    };

                    var approvalActionCBFN = function (data) {
                        oj.Router.rootInstance.go('notificationScreen');
                    }
                    services.workflowAction("workflowApproval/", headers).then( app.failCbFn,approvalActionCBFN)

                };
                //
                self.approveRequst = function (data, event) {
                    self.disableSubmit(true);
                    var headers = {
                        "MSG_TITLE": self.retrieve.MSG_TITLE,
                        "MSG_BODY": self.retrieve.MSG_BODY,
                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "RESPONSE_CODE": "APPROVED",
                        "ssType": self.retrieve.SELF_TYPE ,
                        "PERSON_NAME":self.retrieve.personName,
                        "rejectReason":self.rejectReason(),
                        "workflowId":self.workFlowApprovalId()
                    };
                    var approvalActionCBFN = function (data) {
                        //     
                        if (self.isLastApprover()) {
                            oprationServiceRecord();
                            var updateStatusCBFN =function (data){
                                
                            };
                            
                             services.getGenericAsync("PeopleExtra/updateStatus?id="+self.retrieve.TRS_ID, {}).then(updateStatusCBFN, app.failCbFn);
                    
                                 var addAttachedCbfn = function (data2) {
                                 
                                 };
                                 if(self.koArray().length > 0){
                           services.addGeneric("attachment/EDIT/", self.koArray()).then(addAttachedCbfn, app.failCbFn);
                           }
                                        var jsonBody = {
                                            trsId: self.retrieve.TRS_ID
                                        };
                                        var submitElement = function (data1) {
                                        };
                                        services.submitElementEntry(JSON.stringify(jsonBody)).then(submitElement, app.failCbFn);
                                        if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('home');
                            }
                        document.querySelector("#yesNoDialog").close();
                        // oj.Router.rootInstance.go('notificationScreen');
                    }
                    else {
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('home');
                            }
                        }
                };
                if (approvalManagmant.length != 0){
                                   oprationServiceRecord(); 
                               }
                    services.workflowAction("workflowApproval/", headers).then( app.failCbFn,approvalActionCBFN);
                };
                //------------------ This Section For Call Web Service For Insert Or Update ---------

                //----------This Function To Set Url Call 
                self.setUrl = function (EitCode) {
                    return app.personDetails().employeeURL[EitCode];
                };
                 self.buildModelWithLabel = function () {
                    var jsonDataLbl = self.model;
                    for (var i = 0; i < tempObject.length; i++) {
                        if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_NUMBER_10") &&
                                (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_TIME") &&
                                (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_VIEW_REPORT")
                                ) {

                            jsonDataLbl[tempObject[i].DESCRIPTION + "Lbl"] = searchArray(self.model[tempObject[i].DESCRIPTION](), self.Arrs[tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr"]());

                        }
                    }
                    jsonDataLbl = ko.toJSON(self.model);

                    return jsonDataLbl;
                };
                function oprationServiceRecord() {
                    if (Object.keys(self.model).indexOf("approvedBy") != -1) {
                       self.model.approvedBy=ko.observable(rootViewModel.personDetails().personNumber);
                    } else {
                        //No Approved By Segment
                    }
                    
                    var jsonData = ko.toJSON(self.model);
                    // var cx = JSON.stringify(jsonData);
//                    self.model.url = self.setUrl(self.EitCode);
                    self.approvlModel.code = self.EitCode;
                    if(self.isLastApprover()){
                         self.approvlModel.status = "APPROVED";
                    }else{
                          self.approvlModel.status = "PENDING_APPROVED";
                    }
                    self.approvlModel.person_number = self.retrieve.person_number;
                    self.approvlModel.person_id = self.retrieve.person_id;
                    self.approvlModel.created_by = self.retrieve.person_id;
//                    self.approvlModel.managerName = self.personDetails().managerName;
//                    self.approvlModel.managerOfManagerName = self.personDetails().managerOfMnagerName;
                    //self.approvlModel.creation_date = self.personDetails().personId;
                    self.approvlModel.line_manager = globalRowData.line_manager;
                    self.approvlModel.manage_of_manager =globalRowData.manage_of_manager;
//                    self.approvlModel.url = self.setUrl(self.EitCode);
                    self.approvlModel.eit = JSON.stringify(jsonData);
                   // self.approvlModel.personName = ;
                   // self.approvlModel.nationalIdentity = self.personDetails().nationalId;
//                    self.approvlModel.eitLbl = JSON.stringify(self.buildModelWithLabel());
//                    self.approvlModel.eit_name =self.retrieve.SELF_TYPE;
                    self.approvlModel.id= self.retrieve.TRS_ID;
        
                    
                    for (var eitModelIndex = 0; eitModelIndex < tempObject.length; eitModelIndex++) {
                        self.approvlModel[tempObject[eitModelIndex].APPLICATION_COLUMN_NAME] = self.model[tempObject[eitModelIndex].DESCRIPTION]();
                    }
                   
                    
                    var jsonData = ko.toJSON(self.approvlModel);
                    var getUpdateExtraInformationCBFN = function (data) {            
                        //
                    };
                
                   var approvalCondetion = app.getApprovalCondetion( self.EitCode,self.model) ; 
                    services.addGeneric("PeopleExtra/createOrUpdateEIT/" + 'EDIT'+'/'+approvalCondetion, jsonData).then(getUpdateExtraInformationCBFN, app.failCbFn);
                };
                self.elementEntryArr = ko.observableArray([]);
                function getElementEntryByEitCode() {
                    var EitCode = self.EitCode;
                    var getValidGradeCBF = function (data) {
                        if(self.EitCode == "XXX_BUSINESS_TRIP_REQUEST"){
                            if(self.model.travelType() == "Working")
                            {
                                for(var i = 0; i < data.length; i++){
                                    if(data[i].elementName == "Business Trip Lumpsum Payment")
                                    {
                                        var xx = new Array(data[i]);
                                        self.elementEntryArr(xx);
                                    }
                                }
                            }
                            else if(self.model.travelType() == "Training")
                            {
                               for(var i = 0; i < data.length; i++){
                                    if(data[i].elementName == "Training Business Trip Lumpsum Payment")
                                    {
                                        var xx = new Array(data[i]);
                                        self.elementEntryArr(xx);
                                    }
                                } 
                            }
                        }
                        else{
                          self.elementEntryArr(data);  
                        }
                        
                    };

                    services.getGeneric("elementEntrySetup/getElementEntry/" + EitCode).then(getValidGradeCBF, self.failCbFn);
                }
                ;
                //----------------------------Attachment------------------------------//
                self.resumeAttachmentLbl = ko.observable();
                self.viewAttachmentBtnVisible = ko.observable(false);
                self.attachmentBtnVisible = ko.observable();
                self.removeBtnLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.AttachmentsBtnVisible = ko.observable();
                self.UploadFileLbl = ko.observable();
                self.fileUploadStatusLbl = ko.observable();
                self.dataProviderAttachment = ko.observable();
                self.selectedItemsAttachment = ko.observable();
                var dataFiles = {};
                self.koArray = ko.observableArray([]);
                //create file upload data provider
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.attachmentId = 0;
                self.selectedItemsAttachment = ko.observableArray([]);
                self.attachmentViewerData = ko.observable();
                self.acceptArr = ko.pureComputed(function () {
                    var accept = self.acceptStr();
                    return accept ? accept.split(",") : [];
                }, self);
                self.attachmentSelectListener = function (event) {
                    var files = event.detail.files;
                    if (files.length > 0) {

                        //add the new files at the beginning of the list

                        for (var i = 0; i < files.length; i++) {
                            function getBase64(file) {
                                return new Promise((resolve, reject) => {
                                    const reader = new FileReader();
                                    reader.readAsDataURL(file);
                                    reader.onload = () => resolve(reader.result);
                                    reader.onerror = error => reject(error);
                                });
                            }
                            dataFiles.name = files[i].name;
                            dataFiles.id = i + 1;
                            getBase64(files[i]).then(function (data) {
                                dataFiles.data = (data);
                                self.attachmentId = self.attachmentId + 1;
                                dataFiles.id = (self.attachmentId);
//                        self.koArray.push(dataFiles);

                                self.koArray.push({
                                    "id": dataFiles.id,
                                    "name": dataFiles.name,
                                    "SS_id": self.retrieve.TRS_ID,
                                    "attachment": dataFiles.data


                                });


                               
//                        self.approvalSetupModel.resumeAttachment(dataFiles.data);
                            }
                            );
                        }
                    }

                }
                self.removeSelectedAttachment = function (event) {
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        self.koArray.remove(function (item)
                        {
                            return (item.id == value);
                        });
                    });
                };
                function downloadPDF(payloadAttachData) {
                    var link = document.createElement("a");
                    var data = payloadAttachData.data;
                    link.setAttribute('href', data);
                    link.setAttribute('download', payloadAttachData.name);
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                self.openAttachmentViewer = function ()
                {
    
                    $.each(self.selectedItemsAttachment(), function (index, value)
                    {
                        var data = self.koArray().find(e=>e.id == value);
                        downloadPDF(data);

                    });
                };
                //----------------------------Attachment------------------------------//
                
                
                
                
                self.commitRecord = function (data, event) {
                    if (!self.disableSubmit()) {
                        self.disableSubmit(true);
//                        oprationServiceRecord();
                        return true;
                    }
                };
                //-------------------End Of Section ------------------------------------------------
                //-------------------Leave Page -----------------------------
                self.handleDetached = function (info) {
                    oj.Router.rootInstance.store(self.EitCode);
                    self.isDisabledx = {};
                    if (self.retrieve.SELF_TYPE != 'XXX_HR_PERSON_PAY_METHOD') {
                       self.model = {};
                    }
                    //self.model = {};
                };
                //Update Bank File Function //
                self.updateBankFile=function(){
                    var bankFileJsonBody = {
                        "bankName": self.model.bank(),
                        "bankBranchName": self.model.bankBranch(),
                        "accountNumber": self.model.newAccountNumber(),
                        "IBAN": self.model.iban(),
                        "trsId": self.retrieve.TRS_ID,
                        "personNumber": self.reqeustedPersonNumber,
                        "newBankName": self.model.bank(),
                        "newBankBranchName": self.model.bankBranch(),
                        "newAccountNumber": self.model.newAccountNumber()
                    };
                    var submitBankFile= function(data){
                        var createPersonalPaymentMethodJsonBody = {"legislativeDataGroupName": "SA Legislative Data Group",
                            "assignmentNumber": "E"+self.reqeustedPersonNumber,
                            "personalPaymentMethodCode": "Bank Transfer",
                            "effectiveStartDate": self.model.personPayEffectiveStartDate().replace('-','/').replace('-','/'),
                            "paymentAmountType": "P",
                            "processingOrder": "1",
                            "organizationPaymentMethodCode": "Bank Transfer",
                            "percentage": "100",
                            "bankName": self.model.bank(),
                            "bankBranchName": self.model.bankBranch(),
                            "bankAccountNumber": self.model.newAccountNumber(),
                            "personNumber": self.reqeustedPersonNumber
                            ,"trsId": self.retrieve.TRS_ID
                        }
                        var createPersonalPaymentMethodCBFN = function(data){
                            self.model = {};
                        }
                      services.submitHDLFile("createPersonalPaymentMethod",JSON.stringify(createPersonalPaymentMethodJsonBody)).then(createPersonalPaymentMethodCBFN, app.failCbFn);  
                    }
                  services.submitHDLFile("createBankFile",JSON.stringify(bankFileJsonBody)).then(submitBankFile, app.failCbFn);
                };
                //End Of Update Bank File ------------------
                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.gradeLbl = ko.observable();
                self.validGradeLbl = ko.observable();
                self.structureNameLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.percentageLbl = ko.observable();
                self.qualificationNameLbl = ko.observable();
                self.experienceYearsLbl = ko.observable();

                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenAddLbl = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.selectedPage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.approveMessage = ko.observable();
                self.approve = ko.observable();
                self.reject = ko.observable();
                self.back = ko.observable();
                self.rejectReason = ko.observable();
                self.rejectMessage = ko.observable();
                self.closeNotificationLbl = ko.observable();
                self.employeeNameLbl = ko.observable();
                self.locationLbl = ko.observable();
                self.employeeNumberLbl = ko.observable();
                self.organizationLbl = ko.observable();
                self.hireDateLbl = ko.observable();
                self.jobLbl = ko.observable();
                self.positionLbl = ko.observable();
                self.employeeDetailsLbl = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.structureNameLbl(getTranslation("additionalDetails.structureName"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.validGradeLbl(getTranslation("others.validGrade"));
                    self.gradeLbl(getTranslation("common.grade"));
                    self.percentageLbl(getTranslation("position.percentage"));
                    self.cancel(getTranslation("others.cancel"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.approve(getTranslation("others.approve"));
                    self.reject(getTranslation("others.reject"));
                    self.back(getTranslation("others.back"));
                    self.approveMessage(getTranslation("others.approvalMSG") );
                    self.rejectMessage(getTranslation("others.rejectMSG"));
                    self.viewLbl(getTranslation("common.download"));
                    self.UploadFileLbl(getTranslation("common.UploadFileLbl"));
                    self.removeBtnLbl(getTranslation("common.removeBtnLbl"));
                    self.fileUploadStatusLbl(getTranslation("common.fileUploadStatusLbl"));
                    self.closeNotificationLbl(getTranslation("common.closeNotificationLbl"));
                    self.employeeNameLbl(getTranslation("common.personNameLbl"));
                    self.locationLbl(getTranslation("labels.location"));
                    self.employeeNumberLbl(getTranslation("common.personNumberLbl"));
                    self.organizationLbl(getTranslation("login.organizationName"));
                    self.hireDateLbl(getTranslation("labels.hireDate"));
                    self.jobLbl(getTranslation("common.job"));
                    self.positionLbl(getTranslation("common.position"));
                    self.employeeDetailsLbl(getTranslation("common.employeeDetailsLbl"));
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                
                function callFailReport(eitCode, segmentName){
                    //this for call report only 
                    var res;
                 var paylod =    getSegmentDetails(eitCode, segmentName);
                    var getReportValidationCBCF = function ( dataSaaSQuery) {
                        var resultOfQueryObj = dataSaaSQuery;
                       res=  (resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                         
                    };
                 services.getGenericReportNotAsync(paylod).then(getReportValidationCBCF, app.failCbFn);
                 return res;
                }
                function getSegmentDetails(eitCode, segmentName){
                    //this function for get segment Details 
                   var allSegment=  app.getEITSegments(eitCode);
                   var paylod={};
                   var reportName; 
                   for (var i = 0; i < allSegment.length; i++){
                       if (allSegment[i].DESCRIPTION==segmentName){
                        reportName= tempObject[i].IN_FIELD_HELP_TEXT ;
                        var pram=searchArrayContainValue(tempObject[i].IN_FIELD_HELP_TEXT, app.globalEITDefultValueParamaterLookup());                       
                       }      
                   }
                  paylod= {"reportName": reportName};
                    for (var zz = 0; zz < pram.length; zz++) {
                        if (Object.keys(self.model).indexOf(pram[zz]) != -1) {
                            if (self.model[pram[zz]]()) {
                                paylod[pram[zz]] = self.model[pram[zz]]();
                            }

                        } else {
                            if (app.personDetails()[pram[zz]]) {
                                if(pram[zz]=='personId'){
                                    paylod[pram[zz]] =  self.approvlModel.person_id;
                                }else{
                                }
                                
                            }

                        }
                    }
                   
                   return  paylod ;
                    
                }
                
                self.ShowReportIdentificationLetter = function () {
             var getReportCbFn=function (data){
                 if(self.reportType() == "pdf"){
                 var base64pdf = 'data:application/pdf;base64,' + data.REPORT;
                self.EmbedPDFLink(base64pdf);
                document.querySelector("#pdfViewer").open();
            }
            else{
                var base64word = 'data:application/msword;base64,' + data.REPORT;
                self.EmbedPDFLink(base64word);
                document.querySelector("#pdfViewer").open();
            }
             };
             
             
             var iDletterType=self.model.letterType();
             var withSalaryType=self.model.withSalary();
             var entityEnglishName = self.model.entityEnglishName();
             var entityArabicName = self.model.entityArabicName();
             var payload = "";
             if(self.reportType() == "pdf"){
                if (iDletterType == "ID Letter" && withSalaryType == "No") {
                        payload = {"reportName": "TBC Certificate of Service Without Salary REP", "PERSON_NUMBER": self.reqeustedPersonNumber, "entityEnglishName": entityEnglishName, "entityArabicName": entityArabicName};
                    } else if (iDletterType == "ID Letter" && withSalaryType == "YesTotal") {
                         payload = {"reportName": "Certificate of Service with Salary Rep", "PERSON_NUMBER": self.reqeustedPersonNumber, "entityEnglishName": entityEnglishName, "entityArabicName": entityArabicName};
                    } else if (iDletterType == "Installment Letter" && withSalaryType == "Yes") {
                         payload = {"reportName": "Identification Letter V1 Rep", "PERSON_NUMBER": self.reqeustedPersonNumber, "entityEnglishName": entityEnglishName, "entityArabicName": entityArabicName};
                    } else if (iDletterType == "ID Letter" && withSalaryType == "YesBreak") {
                         payload = {"reportName": "Identification Letter V2 Rep", "PERSON_NUMBER": self.reqeustedPersonNumber, "entityEnglishName": entityEnglishName, "entityArabicName": entityArabicName};
                    }
                }
                else{
                    if (iDletterType == "ID Letter" && withSalaryType == "No") {
                        payload = {"reportName": "TBC Certificate of Service Without Salary REP Word", "PERSON_NUMBER": self.reqeustedPersonNumber, "entityEnglishName": entityEnglishName, "entityArabicName": entityArabicName};
                    } else if (iDletterType == "ID Letter" && withSalaryType == "YesTotal") {
                         payload = {"reportName": "Certificate of Service with Salary Rep Word", "PERSON_NUMBER": self.reqeustedPersonNumber, "entityEnglishName": entityEnglishName, "entityArabicName": entityArabicName};
                    } else if (iDletterType == "Installment Letter" && withSalaryType == "Yes") {
                         payload = {"reportName": "Identification Letter V1 Rep Word", "PERSON_NUMBER": self.reqeustedPersonNumber, "entityEnglishName": entityEnglishName, "entityArabicName": entityArabicName};
                    } else if (iDletterType == "ID Letter" && withSalaryType == "YesBreak") {
                         payload = {"reportName": "Identification Letter V2 Rep Word", "PERSON_NUMBER": self.reqeustedPersonNumber, "entityEnglishName": entityEnglishName, "entityArabicName": entityArabicName};
                    }
                }
                
                    services.getGenericReport(payload).then(getReportCbFn, app.failCbFn);

                };

                self.closeNotificationButton = function () {
                    document.querySelector("#cancelDialog").open();
                };
                self.closeNotificationPopup = function () {
                    document.querySelector("#cancelDialog").close();
                };
                self.closeNotificationAction = function () {
                    var headers = {

                        "TRS_ID": self.retrieve.TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "ssType": self.retrieve.SELF_TYPE,
                        "workflowId": self.workFlowApprovalId()
                    };
                    var closeNotificationActionCBFN = function (data) {
                        oj.Router.rootInstance.go('notificationScreen');
                    };
                    services.workflowAction("workflowApproval/FYI", headers).then(closeNotificationActionCBFN, app.failCbFn);
                };

            }

            return new dynamicOperationScreenViewModel();
        }
);
