define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup'],
        (oj, ko, $, app, commonUtil, services, ArrayDataProvider) => {
    /**
     * The view model for the main content view template
     */
    function MailSummaryViewModel() {
        var self = this;

        self.isItemSelected = ko.observable(false);

        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();

        self.templateId = ko.observable();
        self.backLbl = ko.observable("Back");
        self.addLbl = ko.observable("Add");
        self.searchLbl = ko.observable("Search");
        self.editLbl = ko.observable("Edit");
        self.viewLbl = ko.observable("View");
        self.deleteLbl = ko.observable("Delete");

        self.backAction = function () {
            oj.Router.rootInstance.go('setup');
        };

        self.addAction = function () {
            oj.Router.rootInstance.store({action: 'add'});
            oj.Router.rootInstance.go('mailOperations');
        };

        self.editAction = function () {
            //pass object and isView=false
            var element = document.getElementById('table');
            var currentRow = element.currentRow;
            var id = self.summaryObservableArray()[currentRow['rowIndex']].id;
            var row = self.summaryObservableArray().find(e => e.id === id);

            oj.Router.rootInstance.store({action: 'edit', data: row});
            oj.Router.rootInstance.go('mailOperations');
        };

        self.viewAction = function () {

            //pass object and isView=true
            var element = document.getElementById('table');
            var currentRow = element.currentRow;
            var id = self.summaryObservableArray()[currentRow['rowIndex']].id;
            var row = self.summaryObservableArray().find(e => e.id === id);

            oj.Router.rootInstance.store({action: 'view', data: row});
            oj.Router.rootInstance.go('mailOperations');
        };

        self.reqNameVal = ko.observable();
        self.languageVal = ko.observable('english');
        self.languageOptions = ko.observableArray([
            {value: 'arabic', label: 'arabic'},
            {value: 'english', label: 'english'},
            {value: 'both', label: 'both'}
        ]);

        self.columnArray = ko.observableArray();

        self.templateIdlbl = ko.observable();
        self.templateNameLbl = ko.observable();
        self.languageLbl = ko.observable();
        self.requestNameLbl = ko.observable();
        self.emailSubjectLbl = ko.observable();
        self.statusLbl = ko.observable();

        self.yes = ko.observable();
        self.no = ko.observable();

        self.summaryObservableArray = ko.observableArray([]);
        self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));

        self.tableSelectionListener = function (e) {
            self.isItemSelected(true);
        };


        var getTranslation = oj.Translations.getTranslatedString;

        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        self.searchAction = function () {
            self.isItemSelected(false);
            self.getData(self.templateId(), self.reqNameVal(), self.languageVal());
        };

        self.getData = function (id, name, language) {
            self.summaryObservableArray([]);

            var success = function (data) {
                app.loading(false);
                if (data.length !== 0)
                {
                    self.summaryObservableArray(data);
                } else {
//                    $.notify(self.checkResultSearch(), "error");
                }
            };
            var failure = function () {
//                $.notify(self.checkResultSearch(), "error");
            };
            app.loading(true);
            services.getGeneric("mailTemplate/" + id + "/" + name + "/" + language).then(success, failure);
        };


        self.handleAttached = function (info) {
            app.loading(false);
             self.isItemSelected(false);
//            self.getData();
        };


        self.deleteAction = function () {
            document.querySelector("#yesNoDialog").open();
        };

        self.commitRecordDelete = function () {
            app.loading(false);
            var element = document.getElementById('table');
            var currentRow = element.currentRow;
            var id = self.summaryObservableArray()[currentRow['rowIndex']].id;


            var deleteReportCBF = function (isDeleted) {
                if (isDeleted === 'Done') {
                    self.summaryObservableArray(
                            self.summaryObservableArray().filter(e => e.id !== id)//reasign the array after deleting the record (json object)
                            );
                    //close alert
                    self.cancelButtonDelete();
                    self.isItemSelected(false);
                } else {
                }
            };

            app.loading(true);
            services.addGeneric("mailTemplate/" + id, {}).then(deleteReportCBF, app.failCbFn);
        };
        self.cancelButtonDelete = function () {
            document.querySelector("#yesNoDialog").close();
        };

//----------------------------


//        self.addSummaryLbl = ko.observable();
//        self.editSummaryLbl = ko.observable();
//        self.viewSummaryLbl = ko.observable();
//        self.reportNameLbl = ko.observable();
//        self.parameter1Lbl = ko.observable();
//        self.parameter2Lbl = ko.observable();
//        self.parameter3Lbl = ko.observable();
//        self.parameter4Lbl = ko.observable();
//        self.parameter5Lbl = ko.observable();
//        self.reportTypelbl = ko.observable();
//        self.selectedIndex = ko.observable();
//        self.backActionLbl = ko.observable();
//        self.DeleteSummaryLbl = ko.observable();

//

//        self.isDisableBtnDelete = ko.observable(true);
//        self.dataSource;
//        self.checkResultSearch = ko.observable();
//       
//
//        self.btnAddSummary = function () {
//            app.loading(true);
//            oj.Router.rootInstance.store('ADD');
//            oj.Router.rootInstance.go('approvalConditionOperation');
//        };
//        self.btnEditSummary = function () {
//            app.loading(true);
//            var element = document.getElementById('table');
//            var currentRow = element.currentRow;
//            self.summaryObservableArray()[currentRow['rowIndex']].type = "update";
//            oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
//            oj.Router.rootInstance.go('approvalConditionOperation');
//
//        };
//        self.btnViewSummary = function () {
//            app.loading(true);
//            var element = document.getElementById('table');
//            var currentRow = element.currentRow;
//            self.summaryObservableArray()[currentRow['rowIndex']].type = "view";
//            oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
//            oj.Router.rootInstance.go('approvalConditionOperation');
//
//        };




        function initTranslations() {

            self.backLbl(getTranslation("mailtemplate.back"));
            self.addLbl(getTranslation("mailtemplate.add"));
            self.editLbl(getTranslation("mailtemplate.edit"));
            self.searchLbl(getTranslation("mailtemplate.search"));
            self.viewLbl(getTranslation("mailtemplate.view"));
            self.deleteLbl(getTranslation("mailtemplate.delete"));

            self.templateIdlbl(getTranslation("mailtemplate.templateId"));
            self.templateNameLbl(getTranslation("mailtemplate.templateName"));
            self.languageLbl(getTranslation("mailtemplate.templateLanguage"));
            self.requestNameLbl(getTranslation("mailtemplate.templateRequestName"));
            self.emailSubjectLbl(getTranslation("mailtemplate.templateEmailSubject"));
            self.statusLbl(getTranslation("mailtemplate.templateStatus"));
            self.confirmMessage(getTranslation("report.confirmMessage"));
            self.oprationMessage(getTranslation("labels.message"));
            self.no(getTranslation("others.no"));
            self.yes(getTranslation("others.yes"));

            self.columnArray([
                {
                    "headerText": self.templateIdlbl(), "field": "id"
                },
                {
                    "headerText": self.templateNameLbl(), "field": "templateName"
                },
                {
                    "headerText": self.languageLbl(), "field": "templateLanguage"
                },
                {
                    "headerText": self.requestNameLbl(), "field": "templateRequestName"
                },
                {
                    "headerText": self.emailSubjectLbl(), "field": "templateEmailSubject"
                },
                {
                    "headerText": self.statusLbl(), "field": "templateStatus"
                }
            ]);

            //-------------------------


//            self.fristKeyTypelbl(getTranslation("report.fristKeyType"));
//            self.fristKeyValueLbl(getTranslation("report.fristKeyValue"));
//            self.fristPersonalInformationLbl(getTranslation("report.fristPersonalInformation"));
//            self.secondEitSegmentbl(getTranslation("report.secondEitSegmentbl"));
//            self.operationLbl(getTranslation("validation.operation"));
//            self.secondKeyTypeLbl(getTranslation("validation.secondKeyType"));
//            self.secondKeyValueLbl(getTranslation("validation.secondKeyValue"));
//            self.secondPersonalInfromationlbl(getTranslation("report.secondPersonalInfromation"));
//            self.fristEitSegmentLbl(getTranslation("report.fristEitSegmentLbl"));
//            self.approvalCodeLbl(getTranslation("report.approvalCodeLbl"));
//            self.checkResultSearch(getTranslation("common.checkResultSearch"));
//            self.checkResultSearch(getTranslation("common.checkResultSearch"));
//            self.eitCodelbl(getTranslation("common.eitCode"));
//            self.backActionLbl(getTranslation("approvalScreen.backAction"));
//            self.DeleteSummaryLbl(getTranslation("others.delete"));

        }
        initTranslations();
    }
    return new MailSummaryViewModel;
});
