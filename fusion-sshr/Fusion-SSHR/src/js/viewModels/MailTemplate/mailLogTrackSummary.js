define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services',
    'ojs/ojknockout', 'ojs/ojselectcombobox', 'ojs/ojpopup', 'ojs/ojpagingtabledatasource',
    'ojs/ojvalidationgroup', 'ojs/ojinputtext', 'ojs/ojtable', 'ojs/ojlabel'],
    function (oj, ko, $, app, services) {

        function mailLogTrackSummaryViewModel() {
            var self = this;
            var getTranslation = oj.Translations.getTranslatedString;
            self.labels = ko.observable([]);

            self.statusArr = ko.observableArray([]);
            self.isResubmitBtnDisabled = ko.observable(true);
            self.isSearchBtnDisabled = ko.observable(false);
            self.subjectBodyVal = ko.observable("");
            self.msgBodyValue = ko.observable("");
            self.personNameSearchVal = ko.observable("");
            self.recieverSearchVal = ko.observable("");
            self.subjectSearchVal = ko.observable("");
            self.statusSearch = ko.observable("");
            self.groupValid = ko.observable();
            self.columnArray = ko.observableArray();
            self.selectedRow = ko.observable();
            self.deptObservableArray = ko.observableArray([]);
            self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, { idAttribute: 'id' })));


            var failCbFn = function () {
                app.loading(false);
                app.messages.push({
                    severity: "error",
                    summary: self.labels().unknownErrMsg,
                    autoTimeout: 0
                });
            };


            self.cancelButton = function () {
                document.querySelector("#resubmitDialog").close();
            };

            self.resetSearch = function () {
                self.personNameSearchVal('');
                self.recieverSearchVal('');
                self.subjectSearchVal('');
                self.statusSearch('');

                self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, { idAttribute: 'id' })));
            };

            self.backAction = function () {
                oj.Router.rootInstance.go('setup');
            };

            self.refreshDataBtn = function () {
                app.loading(true);
                self.getAllEmails();
            };


            self.getAllEmails = async function () {

                await services.getGenericAsync("emailLogTracker", {}).then(data => {
                    self.deptObservableArray([]);
                    if (data.length !== 0) {

                        self.deptObservableArray(data);
                        self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, { idAttribute: 'id' })));
                    }

                    app.loading(false);
                }, failCbFn);
            };


            self.submitAction = async function () {

                var tracker = document.getElementById("tracker");
                if (tracker.valid != "valid") {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    return;
                }

                //                    app.loading(true);
                document.querySelector("#resubmitDialog").close();

                var payload = {
                    id: self.selectedRow().id,
                    reciever: self.selectedRow().reciever ? self.selectedRow().reciever : "",
                    subject: self.subjectBodyVal() ? self.subjectBodyVal() : "",
                    msgBody: self.msgBodyValue() ? self.msgBodyValue() : "",
                    personName: self.selectedRow().personName ? self.selectedRow().personName : ""
                };

                app.messages.push({
                    severity: "info",
                    summary: self.labels().submittedMsg
                });

                await services.addGeneric("mailLogTrackSummary/EDIT", ko.toJSON(payload)).then(data => {
                    if (data == 'Success') {
                        self.getAllEmails();
                        app.messages.push({
                            severity: "confirmation",
                            summary: self.labels().dataAddedMsg
                        });
                    } else {
                        app.messages.push({
                            severity: "error",
                            summary: self.labels().dataErrMsg
                        });
                    }
                }, {});

                app.loading(false);
            };


            self.tableSelectionListener = async function (event) {
                var data = event.detail;
                var currentRow = data.currentRow;

                if (currentRow) {
                    self.selectedRow(self.deptObservableArray().find(obj => obj.id == currentRow['rowKey']));
                    self.selectedRow().status != 'SUCCESS' ? self.isResubmitBtnDisabled(false) : self.isResubmitBtnDisabled(true);
                }
            };

            self.resubmitBtnDialog = async function () {
                self.isResubmitBtnDisabled(true);
                $("#ee-resubmit-btn").addClass("loading");
                self.msgBodyValue("");
                self.subjectBodyVal(self.selectedRow().subject ? self.selectedRow().subject : "");

                await services.getGenericAsync("emailLogTracker/" + self.selectedRow().id, {}).then(data => {
                    self.msgBodyValue(data);
                    document.querySelector('#resubmitDialog').open();
                }, failCbFn);

                $("#ee-resubmit-btn").removeClass("loading");
                self.selectedRow().status != 'SUCCESS' ? self.isResubmitBtnDisabled(false) : self.isResubmitBtnDisabled(true);
            };


            self.searchEmails = async function () {

                self.isSearchBtnDisabled(true);
                $("#ee-search-btn").addClass("loading");

                var payload = {
                    reciever: self.recieverSearchVal() ? self.recieverSearchVal() : "%",
                    personName: self.personNameSearchVal() ? '%' + self.personNameSearchVal() + '%' : "%",
                    subject: self.subjectSearchVal() ? '%' + self.subjectSearchVal() + '%' : "%",
                    status: self.statusSearch() ? self.statusSearch() : "%"
                };


                await services.addGeneric("emailLogTracker/Search", ko.toJSON(payload)).then(data => {

                    if (data.length !== 0) {

                        self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(data, { idAttribute: 'id' })));

                    } else {
                        app.messages.push({
                            severity: "error",
                            summary: self.labels().noDataFound,
                            autoTimeout: 0
                        });
                    }

                    app.loading(false);
                }, failCbFn);


                self.isSearchBtnDisabled(false);
                $("#ee-search-btn").removeClass("loading");
            };


            self.handleAttached = function (info) {
                initTranslations();
                self.getAllEmails();
            };


            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });


            function initTranslations() {

                self.statusArr(app.getPaaSLookup('EMAIL_LOG_TRACK_STATUS'));

                self.labels({
                    placeHolder: getTranslation("labels.placeHolder"),
                    cancel: getTranslation("others.cancel"),
                    resetSearchLbl: getTranslation("common.resetSearch"),
                    unknownErrMsg: getTranslation("labels.unknownError"),
                    backActionLbl: getTranslation("approvalScreen.backAction"),
                    searchLbl: getTranslation("login.Search"),
                    mailLogTrackSummaryLbl: getTranslation("pages.mailLogTrackSummaryLbl"),
                    msgBodyLbl: getTranslation("mailtemplate.msgBodyLbl"),
                    resubmitDialogMsg: getTranslation("mailtemplate.resendEmail"),
                    resubmitBtnLbl: getTranslation("mailtemplate.resendLbl"),
                    refreshLbl: getTranslation("common.refresh"),
                    submit: getTranslation("mailtemplate.sendLbl"),
                    noDataFound: getTranslation("common.checkResultSearch"),
                    submittedMsg: getTranslation("elementEntry.submittedMsgNofity"),
                    dataAddedMsg: getTranslation("logscreen.dataAddedMsg"),
                    dataErrMsg: getTranslation("logscreen.dataErrMsg"),
                    recieverLbl: getTranslation("mailtemplate.templateEmail"),
                    personNameLbl: getTranslation("others.personName"),
                    subjectLbl: getTranslation("mailtemplate.templateEmailSubject"),
                    statusLbl: getTranslation("common.statusLBL"),
                    updatedDateLbl: getTranslation("common.updatedDateLbl"),
                    creationDateLbl: getTranslation("notification.creationDate")
                });


                self.columnArray([
                    {
                        "headerText": self.labels().personNameLbl, "field": "personName"
                    },
                    {
                        "headerText": self.labels().recieverLbl, "field": "reciever"
                    },
                    {
                        "headerText": self.labels().subjectLbl, "field": "subject"
                    },
                    {
                        "headerText": self.labels().statusLbl, "field": "status"
                    },
                    {
                        "headerText": self.labels().creationDateLbl, "field": "creationDate"
                    },
                    {
                        "headerText": self.labels().updatedDateLbl, "field": "updatedDate"
                    }
                ]);
            }


        }

        return mailLogTrackSummaryViewModel;
    }
);
