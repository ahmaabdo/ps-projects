/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * searchPayroll module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper', 'config/services', 'ojs/ojarraydataprovider', 'knockout-mapping', 'ojs/ojcollectiontabledatasource', 'ojs/ojtable', 'ojs/ojarraytabledatasource',
    'ojs/ojpagingcontrol', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox','ojs/ojtable', 'ojs/ojradioset',
    'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojdialog', 'ojs/ojcheckboxset', 'ojs/ojmodel', 'ojs/ojlabel'],
        function (oj, ko, $, app, commonUtil, services, km) {
            /**
             * The view model for the main content view template
             */
            function searchPayrollContentViewModel() {
                var self = this;
                self.tt = ko.observable("")
                self.datatable = ko.observableArray([]);
                self.data = ko.observableArray([]);

                self.EitName = ko.observableArray([]);
                self.callEITCode = function () {

                    var EITCodeCbFn = function (data) {

                        var tempObject = data;
                        for (var i = 0; i < tempObject.length; i++) {

                            // push in array
                            self.EitName.push({
                                value: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_CODE,
                                label: tempObject[i].DESCRIPTIVE_FLEX_CONTEXT_NAME
                            });
                        }
                    };
                    var failCbFn = function () {
                    };

                    services.getEitNameReportReport(app.lang).then(EITCodeCbFn, failCbFn);
                };

                self.yesNo = ko.observableArray([]);

                this.allService = ko.observableArray([
                    {
                        "value": '', "label": ''
                    }
                ]);
                this.yesNoLookupDP = ko.observableArray([]);

                this.assigmentStatusDP = ko.observableArray([]);


                self.yesNoLookup = ko.observableArray([]);

                self.mode = ko.observable();
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.data,
                        {
                            idAttribute: 'id'
                        }));
                self.columnArray = ko.observableArray([]);

                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));

                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'
                }));

                self.startdate = ko.observable();
                self.searchModel = {
                    personName: ko.observable(""),
                    personNumber: ko.observable(""),
                    employeeName: ko.observable(""),
                    hireDate: ko.observable(""),
                    position: ko.observable(""),
                    grade: ko.observable(""),
                    location: ko.observable(""),
                    assignmentStatus: ko.observable(""),
                    latestLeaveStartDate: ko.observable(""),
                    latestLeaveEndDate: ko.observable(""),
                    advancedLeave: ko.observable(""),
                    lastApprover: ko.observable(""),
                    Action: ko.observable(""),
                    Purge: ko.observable(""),
                    selfServiceType: ko.observable(""),
                    processedInPayroll: ko.observable(""),
                    effectiveDate: ko.observable("")
                };

                self.handleActivated = function (info) {

                };

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.data([]);
                    self.EitName(app.globalEitNameReport());

                    //         buildAllSelfService();
                    initTranslations();
                    $('#tbl1').on('click', '.oj-checkboxset', self.syncCheckboxes);
                    self.allService(app.getPaaSLookup("SELF_TYPE"));
                    //yesNoLookupDP
                    self.yesNoLookupDP(app.getPaaSLookup("YES_NO"));
                    self.assigmentStatusDP(app.getPaaSLookup("ASSIGNMENT_STATUS_TYPE"));
                };
                self.handleDetached = function (info) {
                    clearContent();
                };

                this.reset = function () {
                    clearContent();
                    self.data([]);
                }

                function clearContent() {
                    self.searchModel.personNumber("");
                    self.searchModel.effectiveDate("");
                    self.searchModel.processedInPayroll("");
                    self.searchModel.selfServiceType("");
                }

                self.editEmployeeGrievance = function () {

                    if (self.selectedRowKey()) {
                        self.mode('EditEmployeeGrievance');
                        rootViewModel.selectedTableRowKey(self.selectedRowKey());
                        self.selectedEmployeeGrievance(self.data()[self.selectedIndex()]);
                        self.selectedEmployee(self.data()[self.selectedIndex()]);
                        oj.Router.rootInstance.go('editEmployeeGrievance');
                    }

                }

                var getElementDetailsCbFn = function (data1) {
                    self.datatable(data1);
                    self.data([]);
//                    $.each(data, function (index, data) {
//                        self.data.push({
//                            Selected: ko.observable([]),
//                            id: data[index].id,
//                            UCM_ID: data[index].UCM_ID,
//                            SS_TYPE: document.documentElement.lang === 'ar' ? data[index].ss_ar : data[index].ss_en,
//                            ss_type_en: data[index].ss_en,
//                            ss_type_ar: data[index].ss_ar,
//                            ast_type: document.documentElement.lang === 'ar' ? data[index].ast_ar : data[index].ast_en,
//                            ast_en: data[index].ast_en,
//                            ast_ar: data[index].ast_ar,
//                            ss_id: data[index].ss_id,
//                            process_id: data[index].process_id,
//                            PERSON_NUMBER: data[index].PERSON_NUMBER,
//                            FULL_NAME: data[index].FULL_NAME,
//                            HIRE_DATE: data[index].HIRE_DATE,
//                            POSITION_NAME: data[index].POSITION_NAME,
//                            GRADE_NAME: data[index].GRADE_NAME,
//                            STATUS: data[index].STATUS,
//                            sstype: data[index].ss_type,
//                            start_date: data[index].start_date,
//                            end_date: data[index].end_date,
//                            advanced_leave: data[index].advanced_leave,
//                            file_bytes: data[index].file_bytes,
//                            file_name: data[index].file_name
//
//                        });
//                    });
                    $.each(data1, function (i, item) {
                        self.data.push({
                            Selected: ko.observable([]),
                            id: data1[i].id,
                            ADVANCED_LEAVE: data1[i].ADVANCED_LEAVE,
                            END_DATE: data1[i].END_DATE,
                            FULL_NAME: data1[i].FULL_NAME,
                            GRADE_NAME: data1[i].GRADE_NAME,
                            HIRE_DATE: data1[i].HIRE_DATE,
                            PERSON_NUMBER: data1[i].PERSON_NUMBER,
                            POSITION_NAME: data1[i].POSITION_NAME,
                            SS_TYPE: data1[i].SS_TYPE,
                            START_DATE: data1[i].START_DATE,
                            STATUS: data1[i].STATUS,
                            FILE_BYTES: data1[i].FILE_BYTES,
                            FILE_NAME: data1[i].FILE_NAME
                        });

                    });

                };
                this.searchElement = function () {
                    var bayload = {
                        "SS_TYPE": self.searchModel.selfServiceType(),
                        "PERSON_NUMBER": self.searchModel.personNumber(),
                        "creationDate": self.startdate(),
                        "ASSIGNMENT_STATUS_TYPE": self.searchModel.assignmentStatus(),
                        "STATUS": self.searchModel.processedInPayroll()
                    };
                    //  if (self.searchModel.name() || self.searchModel.nationalId() || self.searchModel.personNumber() || self.searchModel.effectiveDate()) {
                    services.addGeneric("HDL/searchHDL", bayload).then(getElementDetailsCbFn, app.failCbFn);
                    //}
                }

                //language support =========================
                self.name = ko.observable();
                self.position = ko.observable();
                self.department = ko.observable();
                self.personNumber = ko.observable();
                self.goTo = ko.observable();
                self.efficitveDate = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.personnumber = ko.observable();
                self.nationalId = ko.observable();
                self.effictiveDate = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.goTo = ko.observable();
                self.location = ko.observable();
                self.selfServicePlaceHolder = ko.observable();
                self.yesPlaceHolder = ko.observable();
                self.processedInPayroll = ko.observable();
                self.review = ko.observable();
                self.process = ko.observable();
                self.grade = ko.observable();
                self.position = ko.observable();
                self.hireDate = ko.observable();
                self.employeeName = ko.observable();
                self.assigmentStatus = ko.observable();
                self.latestLeaveStartDate = ko.observable();
                self.latestLeaveEndDate = ko.observable();
                self.advancedLeave = ko.observable();
                self.effectiveDate = ko.observable();
                self.processNotify = ko.observable();
                self.selectionNotify = ko.observable();
                self.oneRowNotify = ko.observable();
                self.payrollValidatorLbl = ko.observable();

                self.language = ko.observable(document.documentElement.lang);
                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {



                        initTranslations();
                    }
                });

                function initTranslations() {
                    //   buildAllSelfService();
                    self.payrollValidatorLbl(getTranslation("pages.payrollValidator"))
                    self.allService(app.getPaaSLookup("SELF_TYPE"));
                    self.yesNoLookupDP(app.getPaaSLookup("YES_NO"));
                    self.assigmentStatusDP(app.getPaaSLookup("ASSIGNMENT_STATUS_TYPE"));
                    self.selfServicePlaceHolder(getTranslation("labels.placeHolder"));
                    self.yesPlaceHolder(getTranslation("labels.placeHolder"));
                    self.clear(getTranslation("others.clear"));
                    self.search(getTranslation("others.search"));
                    self.personnumber(getTranslation("common.personNumber"));
                    self.name(getTranslation("common.selfService"));
                    self.nationalId(getTranslation("personSearch.nationalId"));
                    self.effictiveDate(getTranslation("payroll.effictiveDate"));
                    self.search(getTranslation("others.search"));
                    self.goTo(getTranslation("personSearch.goTo"));
                    self.location(getTranslation("labels.location"));
                    self.department(getTranslation("common.department"));
                    self.processedInPayroll(getTranslation("payroll.processedInPayroll"));
                    self.review(getTranslation("others.review"));
                    self.process(getTranslation("payroll.processing"));
                    self.grade(getTranslation("common.grade"));
                    self.position(getTranslation("common.position"));
                    self.hireDate(getTranslation("labels.hireDate"));
                    self.employeeName(getTranslation("common.name"));
                    self.assigmentStatus(getTranslation("payroll.assigmentStatus"));
                    self.latestLeaveStartDate(getTranslation("payroll.latestLeaveStartDate"));
                    self.latestLeaveEndDate(getTranslation("payroll.latestLeaveEndDate"));
                    self.advancedLeave(getTranslation("payroll.advancedLeave"));
                    self.effectiveDate(getTranslation("others.effectiveDate"));
                    self.processNotify(getTranslation("payroll.processNotify"));
                    self.selectionNotify(getTranslation("payroll.selectionNotify"));
                    self.oneRowNotify(getTranslation("payroll.oneRowNotify"));

                    //             id : val.id, 
                    self.columnArray([
                        {
                            "renderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_tmpl", true), "headerRenderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_hdr_tmpl", true), "id": "column1"
                        },
                        {
                            "headerText": self.name(), "field": "SS_TYPE"
                        },
                        {
                            "headerText": self.personnumber(), "field": "PERSON_NUMBER"
                        },
                        {
                            "headerText": self.employeeName(), "field": "FULL_NAME"
                        },
                        {
                            "headerText": self.hireDate(), "field": "HIRE_DATE"
                        },
                        {
                            "headerText": self.position(), "field": "POSITION_NAME"
                        },
                        {
                            "headerText": self.grade(), "field": "GRADE_NAME"
                        },
                        {
                            "headerText": self.assigmentStatus(), "field": "ASSIGNMENT_STATUS_TYPE"
                        },
                        {
                            "headerText": self.latestLeaveStartDate(), "field": "START_DATE"
                        },
                        {
                            "headerText": self.latestLeaveEndDate(), "field": "END_DATE"
                        },
                        {
                            "headerText": self.advancedLeave(), "field": "ADVANCED_LEAVE"
                        }
                    ]);
                }

                self.selectionListener = function (event) {
                    var data = event.detail;
                    if (data != null) {
                        var selectionObj = data.value;

                        var totalSize = self.dataSource.totalSize();
                        var i, j;
                        for (i = 0; i < totalSize; i++) {
                            self.dataSource.at(i).then(function (row) {
                                var foundInSelection = false;
                                if (selectionObj) {

                                    for (j = 0; j < selectionObj.length; j++) {
                                        var range = selectionObj[j];
                                        var startIndex = range.startIndex;
                                        var endIndex = range.endIndex;

                                        if (startIndex != null && startIndex.row != null) {
                                            if (row.index >= startIndex.row && row.index <= endIndex.row && row.data.status === 'NO') {
                                                row.data.Selected(['checked']);
                                                foundInSelection = true;
                                            }
                                        }
                                    }
                                }
                                if (!foundInSelection) {
                                    row.data.Selected([]);
                                }
                            });
                        }
                    }
                };

                self.syncCheckboxes = function (event) {
                    event.stopPropagation();
                    setTimeout(function () {
                        // sync the checkboxes with selection obj
                        var selectionObj = [];
                        var totalSize = self.dataSource.totalSize();
                        var table = document.getElementById('tbl1');
                        var i;
                        for (i = 0; i < totalSize; i++) {
                            self.dataSource.at(i).then(function (row) {
                                if (row.data.Selected().length > 0 && row.data.Selected()[0] == 'checked') {
                                    selectionObj.push({
                                        startIndex: {
                                            row: row.index
                                        },
                                        endIndex: {
                                            row: row.index
                                        },
                                        data: {
                                            data: row.data
                                        }
                                    });
                                }

                                if (row.index == totalSize - 1) {
                                    table.selection = selectionObj;
                                }
                            });
                        }
                    },
                            0);
                }

                self.selectAllListener = function (event) {
                    var data = event.detail;
                    if (data != null) {
                        var table = document.getElementById('tbl1');
                        if (data['value'].length > 0) {
                            var totalSize = self.dataSource.totalSize();
                            table.selection = [
                                {
                                    startIndex: {
                                        "row": 0
                                    }, endIndex: {
                                        "row": totalSize - 1
                                    }
                                }
                            ];
                        } else {
                            table.selection = [];
                        }
                    }
                };

                this.reviewNotification = function () {
                    var element = document.getElementById('tbl1');
                    var selectionObj = element.selection;
                    var len = selectionObj ? selectionObj.length : 0;

                    var i = 0;
                    var elementIds = [];

                    for (i = 0; i < len; i++) {
                        var range = selectionObj[i].data.data.id;
                        var trsid = selectionObj[i].data.data.ss_id;
                        var ssType = selectionObj[i].data.data.sstype;

                        elementIds.push({
                            id: range, trsId: trsid, ssType: ssType
                        });

                    }

                    if (elementIds.length == 1) {
                        if (elementIds[0].trsId) {
                            //if(self.selectedRowKey()) {
                            //rootViewModel.recieveType(self.receiverType());
                            rootViewModel.selectedTableRowKeyNotifiation(elementIds[0].trsId);
                            var trsId = elementIds[0].trsId;
                            var rowSsType = elementIds[0].ssType;
                            if (rowSsType === "BT") {
                                rootViewModel.selectedTableRowKey(trsId);
                                oj.Router.rootInstance.go('viewBusinessTrip');

                            } else if (rowSsType == "AH") {
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewNotificationAdvHousing');
                                return true;

                            } else if (rowSsType == "BTA") {
                                rootViewModel.reviewNotiType("FYI");
                                oj.Router.rootInstance.go('reviewBankTransferAccNotification');
                                return true;

                            } else if (rowSsType == "FVR") {
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewNewFamilyVisaRefund');
                                return true;

                            } else if (rowSsType == "BTD") {
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewNotificationBusinessTripDriver');

                            } else if (rowSsType == 'XIL') {
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewIdentificationLetter');
                                return true;

                            } else if (rowSsType == "RRS") {
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewNotificationRewardRequst');

                            } else if (rowSsType == "CEE") {
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewChildrenEductionExpense');

                            } else if (rowSsType == 'EA') {
                                //added
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewNotificationEmployeeAllowance');
                                return true;

                            } else if (rowSsType == 'BTR') {
                                //added
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewBusinessTripReturns');
                                return true;
                            } else if (rowSsType == 'STA') {
                                //added
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewStopAllowanceRequest');
                                return true;
                            } else if (rowSsType == 'CHT') {
                                //added
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewChangeHousingType');
                                return true;
                            } else if (rowSsType == 'TR') {
                                //added
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewNotificationTicketRequest');
                                return true;
                            } else if (rowSsType == 'TRF') {
                                //added
                                rootViewModel.reviewNotiType("FYI");

                                oj.Router.rootInstance.go('reviewNotificationTicketRequestRefund');
                                return true;
                            } else if (rowSsType == 'AAL') {
                                //added
                                rootViewModel.selectedTableRowKey(trsId);
//                        rootViewModel.reviewNotiType("FYI");


                                oj.Router.rootInstance.go('reviewAdvancedAnnualLeave');
                                return true;
                            }

                        }


                    } else {
                        showNotify('warning', self.oneRowNotify());
                    }

                };

                this.openDialog = function () {
                    var element = document.getElementById('tbl1');
                    var selectionObj = element.selection;
                    var len = selectionObj ? selectionObj.length : 0;
                    var i = 0;
                    var elementIds = [];

                    for (i = 0; i < len; i++) {
                        var range = selectionObj[i].data.data.id;

                        elementIds.push({
                            id: range
                        });

                    }

                    if (elementIds.length > 0) {
                        elementIds.push({
                            startDate: self.startdate()
                        });
                    }
                    if (elementIds.length > 0) {
                        $("#modalDialog1").ojDialog("open");
                    } else {
                        showNotify('warning', self.selectionNotify());
                    }


                }

                this.closeDialog = function () {
                    var element = document.getElementById('tbl1');
                    var selectionObj = element.selection;
                    var len = selectionObj ? selectionObj.length : 0;

                    var i = 0;
                    var elementIds = [];
                    for (i = 0; i < len; i++) {
                        var range = selectionObj[i].data.data.id;
                        var sstype = selectionObj[i].data.data.SS_TYPE;
                        var file_bytes = selectionObj[i].data.data.FILE_BYTES;
                        var file_name = selectionObj[i].data.data.FILE_NAME;
                        elementIds.push({
                            id: range,
                            sstype: sstype,
                            file_bytes: file_bytes,
                            file_name: file_name
                        });

                    }

                    if (elementIds.length > 0) {
                        elementIds.push({
                            startDate: self.startdate()
                        });
                    }

                    if (elementIds.length > 0) {
                        var submitElement = function (data1) {

                        };
                        services.processElementEntry(JSON.stringify(elementIds)).then(submitElement, app.failCbFn);
                        self.updateHdlModel = {
                            id: ko.observable(range), STATUS: ko.observable("COMPLETED")
                        }
                        var jsonData1 = ko.toJSON(self.updateHdlModel);
                        var updateHdlCbfn = function (data) {

                        };
                        services.addGeneric("HDL/editHDL",jsonData1).then(updateHdlCbfn, app.failCbFn);

                        $("#modalDialog1").ojDialog("close");
                        showNotify('success', self.processNotify());
                    } else {
                        showNotify('warning', self.selectionNotify());
                    }

                }

            }

            return searchPayrollContentViewModel;
        });
