/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * AccessSetupOperation module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function AccessSetupOperationContentViewModel() {
                var self = this;
                var Role;
                self.retrieve;
                self.disableSubmit = ko.observable(false);
                self.disableSubmitEdit = ko.observable(false);
                self.trainVisible = ko.observable(true);
                self.addMessage = ko.observable();
                self.placeholder = ko.observable();
                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                self.RoleNameLbl = ko.observable();
                self.AccessListLbl = ko.observable();
                self.RoleName = ko.observable();
                self.AccessList = ko.observable();
                self.RoleNameArr = ko.observableArray();
                self.AccessListArr = ko.observableArray();
                self.reportTitleLabl = ko.observable();
                self.isDisabled = ko.observable(false);
                self.groupValid = ko.observable();
                self.messages = ko.observable();
                self.IconListArr=ko.observableArray();
                self.IconListLbl=ko.observable();
                self.IconList=ko.observable();
                var IconClass;
                self.ScreenNameLbl=ko.observable();
                self.ScreenName=ko.observable();
                self.ScreenListArr=ko.observableArray();
                
                self.cancelAction = function () {

                    oj.Router.rootInstance.go('AccessSetupSummary');

                };
                
                self.previousStep = function () {
                    self.hidecancel(true);
                    self.hidepervious(false);
                    self.hidesubmit(false);
                    self.hideUpdatesubmit(false);
                    self.hidenext(true);
                    self.isDisabled(false);
                    self.currentStepValue('stp1');

                };
                self.nextStep = function () {
                    var tracker = document.getElementById("tracker");

                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        self.currentStepValue('stp1');

                        return;
                    } else {

                        if (self.retrieve.type === 'ADD') {
                            self.hidesubmit(true);
                            self.hideUpdatesubmit(false);
                        } else if (self.retrieve.type === 'update') {
                            self.hidesubmit(false);
                            self.hideUpdatesubmit(true);

                        }
                        self.isDisabled(true);
                        self.hidepervious(true);
                        self.hidenext(false);
                        self.currentStepValue('stp2');
                    }



                };
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();

                };
                self.commitRecord = function () {

                    self.addAccessSetup();
                    self.disableSubmit(true);
                };
                self.commitRecordupdate = function () {
                    self.updateAccessSetup();
                    self.disableSubmitEdit(true);

                };
                self.submitUpdateButton = function () {
                    document.querySelector("#yesNoDialogUpdate").open();
                };

                self.cancelButton = function () {

                    document.querySelector("#yesNoDialog").close();
                };

                self.cancelButtonupdate = function () {
                    document.querySelector("#yesNoDialogUpdate").close();

                };

                self.stopSelectListener = function () {
                   if( self.currentStepValue()== 'stp2'){
                       self.nextStep();
                   }else{
                       self.previousStep();
                   }
                };
                
                self.currentStepValueText = function () {

                };


                self.handleAttached = function (info) {


                    app.loading(false);
                    self.AccessListArr(app.getPaaSLookup('Access_List'));
                    self.IconListArr(app.getPaaSLookup('Icon_List'));
                    self.ScreenListArr(app.getPaaSLookup('Screen_Name'));
                    self.RoleNameArr(app.rolesOption());
                    
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    console.log(self.retrieve);
                    if (self.retrieve.type === 'ADD') {
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(true);
                        self.hidesubmit(false);
                        self.currentStepValue('stp1');


                    } else if (self.retrieve.type === 'view') {

                        self.RoleName(Number(self.retrieve.data.roleId[0]));
                        self.AccessList(self.retrieve.data.accessList[0]);
                        self.IconList(self.retrieve.data.iconClass[0]);
                        self.ScreenName(self.retrieve.data.screenName[0]);
                        Role = self.retrieve.data.roleName[0];

                        self.isDisabled(true);
                        self.hidenext(false);
                        self.trainVisible(false);
                        self.currentStepValue('stp2');
                    } else if (self.retrieve.type === 'update') {
                    
                        self.RoleName(Number(self.retrieve.data.roleId[0]));
                        self.AccessList(self.retrieve.data.accessList[0]);
                        self.IconList(self.retrieve.data.iconClass[0]);
                        self.ScreenName(self.retrieve.data.screenName[0]);
                        Role = self.retrieve.data.roleName[0];
//                         self.structureChangedHandler();

                    }


                };
                self.structureChangedHandler=function(event){
                    
                         Role = event.detail.originalEvent.target.ariaLabel;
                        console.log(Role);
                    
                       

                };
                self.IconChangedHandler=function(event){
                      IconClass = event.detail.originalEvent.target.ariaLabel;
                    
                };
                
                self.addAccessSetup = function () {

                    var payload = {
                        "roleName": Role,
                        "accessList": self.AccessList(),
                        "roleId":self.RoleName(),
                        "iconClass":self.IconList(),
                        "screenName":self.ScreenName()

                    };
                    var AccessCbFn = function (data) {
                        document.querySelector("#yesNoDialog").close();
                        oj.Router.rootInstance.go('AccessSetupSummary');
                    };
                    services.addGeneric("Access/addAccessSetup", JSON.stringify(payload)).then(AccessCbFn, app.failCbFn);
                };
                
                self.updateAccessSetup = function () {
                    var payload = {
                        "roleName": Role,
                        "accessList": self.AccessList(),
                        "roleId":self.RoleName(),
                        "iconClass":self.IconList(),
                        "screenName":self.ScreenName(),
                        "accessId": self.retrieve.data.accessId[0]

                    };
                    var AccessCbFn = function (data) {
                        document.querySelector("#yesNoDialogUpdate").close();
                        oj.Router.rootInstance.go('AccessSetupSummary');
                    };
                    services.addGeneric("Access/updateAccessSetup", JSON.stringify(payload)).then(AccessCbFn, app.failCbFn);
                };  
             
                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                self.addJob = ko.observable();
                self.updateJob = ko.observable();
                self.viewJob = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.oprationMessageUpdate = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.createReport = ko.observable();
                self.stepArray = ko.observableArray([]);
                self.OperationName=ko.observable();

                function initTranslations() {

                    self.reportTitleLabl(getTranslation("Access.OperationLbl"));
                    self.oprationMessage(getTranslation("report.oprationMessage"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessageUpdate(getTranslation("report.oprationMessageUpdate"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.addJob(getTranslation("job.addJob"));
                    self.updateJob(getTranslation("job.updateJob"));
                    self.viewJob(getTranslation("job.viewJob"));
                    self.addMassageLbl(getTranslation("job.addMessage"));
                    self.editMassageLbl(getTranslation("job.editMessage"));
                    self.createReport(getTranslation("report.create"));
                    self.trainView(getTranslation("others.review"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));
                    self.submit(getTranslation("others.submit"));
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                    self.RoleNameLbl(getTranslation("Access.roleName"));
                    self.AccessListLbl(getTranslation("Access.accessList"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.IconListLbl(getTranslation("Access.iconList"));
                    self.ScreenNameLbl(getTranslation("Access.screenName"));
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    
                    if (self.retrieve.type === 'update') {
                        self.OperationName(getTranslation("Access.editAccess"));

                    } else if (self.retrieve.type == 'view') {
                        self.OperationName(getTranslation("Access.viewAccess"));

                    } else {
                        self.OperationName(getTranslation("Access.addAccess"));

                    }

                    self.stepArray([{label: self.createReport(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }

            return AccessSetupOperationContentViewModel;
        });
