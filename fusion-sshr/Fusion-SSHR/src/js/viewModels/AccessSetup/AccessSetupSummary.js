/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * AccessSetupSummary module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function AccessSetupSummaryContentViewModel() {
                var self = this;

                self.summaryObservableArray = ko.observableArray([]);
                self.dataSource = ko.observable();
                self.isDisable = ko.observable(true);
                self.isDisableBtnDelete = ko.observable(true);
                self.placeholder = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.checkResultSearch = ko.observable();
                self.reportTitleLabl = ko.observable();
                var selectedRowKey;
                self.access = {
                    accessList: ko.observable()
                };


                self.addACT = function () {
                    app.loading(true);
                    var addStore = [];
                    addStore.type = 'ADD'
                    oj.Router.rootInstance.store(addStore);
                    oj.Router.rootInstance.go('AccessSetupOperation');
                };
                self.editACT = function () {
                    app.loading(true);
                     var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].accessId[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var payload = {
                        type: 'update',
                        data: self.summaryObservableArray()[myRowIndex]
                    };
                    oj.Router.rootInstance.store(payload);
                    oj.Router.rootInstance.go('AccessSetupOperation');

                };
                self.viewACT = function () {
                    app.loading(true);
                  var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].accessId[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var payload = {
                        type: 'view',
                        data: self.summaryObservableArray()[myRowIndex]
                    };
                    oj.Router.rootInstance.store(payload);
                   
                    oj.Router.rootInstance.go('AccessSetupOperation');

                };
                self.deleteACT = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.commitRecordDelete = function () {
                    var myRowIndex;
                    for(var i=0; i<self.summaryObservableArray().length; i++){
                        if(self.summaryObservableArray()[i].accessId[0] ==  selectedRowKey){
                            myRowIndex = i;
                        }
                    }
                    var id = self.summaryObservableArray()[myRowIndex].accessId;
                    var deleteReportCBF = function (data) {
                        self.getSummary();
                        document.querySelector("#yesNoDialog").close();
                        self.isDisableBtnDelete(true);
                    };

                    services.getGeneric("Access/deleteAccessSetup/" + id, {}).then(deleteReportCBF, app.failCbFn);
                };
                self.cancelButtonDelete = function () {
                    document.querySelector("#yesNoDialog").close();
                };

                self.getSummary = function () {
                    self.summaryObservableArray([]);
                    var getReportCbFn = function (data) {

                        if (data.length !== 0)
                        {
                            $.each(data, function (index) {
                                self.summaryObservableArray.push({
                                    accessId: [data[index].accessId],
                                    roleName: [data[index].roleName],
                                    roleId:[data[index].roleId],
                                    accessList: [data[index].accessList],
                                    iconClass: [data[index].iconClass],
                                    screenName: [data[index].screenName],
                                    accessDate: [data[index].accessDate]
                                });
                                self.dataSource(new oj.PagingTableDataSource(new
                                        oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'accessId'})));
                            });
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }

                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = "Access/getAllAccess";
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);
                };


                
                self.tableSelectionListener = function (event) {
                    selectedRowKey = event.detail.currentRow.rowKey[0];
                      self.isDisable(false);
                    self.isDisableBtnDelete(false);
                };
              


                self.backACT = function () {
                    oj.Router.rootInstance.go('setup');
                };

                self.searchACT = function () {
                    var jsonData = ko.toJSON(self.access);
                    var AccessCbFn = function (data) {

                        if (data.length !== 0)
                        {
                            self.summaryObservableArray(data);
                            console.log(self, summaryObservableArray());
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    services.addGeneric("Access/search", jsonData).then(AccessCbFn, failCbFn);
                };


                self.reset = function () {
                    clearContent();
                    self.dataSource(new oj.PagingTableDataSource
                    (new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'accessId'})));

                };

                function clearContent() {
                    self.access.accessList("");
                    self.getSummary();

                }

                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getSummary();

                };


                //---------- filter change value function --------------
                self.handleValueChanged = function () {
                    var deptArray = [];
                    self.tempSummaryObservableArray = ko.observableArray([]);
                    var filter = document.getElementById('filter').rawValue;
                    if (typeof filter !== 'undefined' && filter !== null) {
                        for (var i = self.summaryObservableArray().length - 1; i >= 0; i--) {
                            Object.keys(self.summaryObservableArray()[i]).forEach(function () {
                                if (typeof self.summaryObservableArray()[i].roleName[0] !== 'undefined') {
                                    if (self.summaryObservableArray()[i].roleName[0].toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0 ||
                                            self.summaryObservableArray()[i].accessList.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0
                                            || self.summaryObservableArray()[i].screenName.toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0) {
                                        if (deptArray.indexOf(self.summaryObservableArray()[i]) < 0) {
                                            deptArray.push(self.summaryObservableArray()[i]);
                                            self.tempSummaryObservableArray = deptArray;
                                            self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tempSummaryObservableArray, {idAttribute: 'accessId'})));
                                        }
                                    }
                                }
                            });
                        }
                    }
                };

                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });

                self.columnArray = ko.observableArray();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.viewLbl = ko.observable();
                self.backLbl = ko.observable();
                self.deleteLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.idLbl = ko.observable();
                self.roleNameLbl = ko.observable();
                self.accessIconLbl = ko.observable();
                self.accessDateLbl = ko.observable();
                self.AccessListbl = ko.observable();
                self.IconListLbl = ko.observable();
                self.ScreenNameLbl = ko.observable();
                self.filterPlaceHolder = ko.observable();


                function initTranslations() {

                    self.addLbl(getTranslation("report.addSummary"));
                    self.editLbl(getTranslation("report.editSummary"));
                    self.viewLbl(getTranslation("report.viewSummary"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.backLbl(getTranslation("approvalScreen.backAction"));
                    self.deleteLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("labels.message"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.idLbl(getTranslation("Access.id"));
                    self.roleNameLbl(getTranslation("Access.roleName"));
                    self.accessIconLbl(getTranslation("Access.accessIcon"));
                    self.accessDateLbl(getTranslation("Access.accessDate"));
                    self.AccessListbl(getTranslation("Access.filter"));
                    self.reportTitleLabl(getTranslation("Access.SummaryLbl"));
                    self.IconListLbl(getTranslation("Access.iconList"));
                    self.ScreenNameLbl(getTranslation("Access.screenName"));
                    self.filterPlaceHolder(getTranslation("Access.filterPlaceHolder"));
                    self.columnArray([
                        {
                            "headerText": self.idLbl(), "field": "accessId"
                        },
                        {
                            "headerText": self.roleNameLbl(), "field": "roleName"
                        },
                        {
                            "headerText": self.accessIconLbl(), "field": "accessList"
                        }, {
                            "headerText": self.IconListLbl(), "field": "iconClass"
                        }, {
                            "headerText": self.ScreenNameLbl(), "field": "screenName"
                        },
                        {
                            "headerText": self.accessDateLbl(), "field": "accessDate"
                        }
                    ]);
                }
                initTranslations();


            }

            return AccessSetupSummaryContentViewModel;
        });
