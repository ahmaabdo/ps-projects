/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * trackRequestRoleSummary module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojtable',
    'ojs/ojpopup', 'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function trackRequestRoleSummaryContentViewModel() {
                var self = this;
                var arr = [];
                var str;


                self.summaryObservableArray = ko.observableArray([]);
                self.dataSource = ko.observable();
                self.isDisable = ko.observable(true);
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.actionType = ko.observable();
                self.messages = ko.observableArray();

                self.roleSearch = {
                    roleName: ko.observable()
                };


                //--- GET ALL SELF SERVICES -------
                self.getAllEITCode = function (id) {
                    var ROLeEitCodeCbFn = function (data) {
                        for (var i = 0; i < data.length; i++) {
                            arr.push(data[i].eitCodeOperation);
                        }
                        str = JSON.stringify(arr);
                        self.isDisable(false);
                    };

                    var ObjRoleName = {"ID": id};
                    var serviceName = "trackRoles/getAllEitCode";
                    services.addGeneric(serviceName, JSON.stringify(ObjRoleName)).then(ROLeEitCodeCbFn, app.failCbFn);


                };

                //----------------------------------------------
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    currentRow = data.currentRow;
                    if (currentRow) {
                        var data = event.detail;
                        var currentRow = data.currentRow;
                        var id = self.summaryObservableArray()[currentRow['rowIndex']].id;
                        self.getAllEITCode(id);
                    } else {
                        self.isDisable(true);
                    }
                };
                self.btnAddSummary = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('trackRequestRoleOperation');
                };
                self.btnEditSummary = function () {
                    app.loading(true);

                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.actionType('update');
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "update";
                    self.summaryObservableArray()[currentRow['rowIndex']].selfService = arr;
                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('trackRequestRoleOperation');

                };
                self.btnViewSummary = function () {
                    app.loading(true);
                    self.actionType('view');
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    self.summaryObservableArray()[currentRow['rowIndex']].type = "view";
                    self.summaryObservableArray()[currentRow['rowIndex']].selfService = arr;
                    oj.Router.rootInstance.store(self.summaryObservableArray()[currentRow['rowIndex']]);
                    oj.Router.rootInstance.go('trackRequestRoleOperation');
                };

                self.btnDeleteSummary = function () {
                    document.querySelector("#yesNoDialog").open();
                };


                //--------  OPERATION ACTIONS-----------------
                self.commitRecordDelete = function () {
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    var id = self.summaryObservableArray()[currentRow['rowIndex']].id;
                    var deleteRecordCBFn = function () {
                        self.getSummary();
                        document.querySelector("#yesNoDialog").close();

                    };
                    services.getGeneric("trackRoles/deleteTrackRoles/" + id, {}).then(deleteRecordCBFn, app.failCbFn);
                };

                self.cancelButtonDelete = function () {
                    document.querySelector("#yesNoDialog").close();
                };

                //------- GET ALL SUMMARY DATA----------
                self.getSummary = function () {
                    app.loading(true);
                    self.summaryObservableArray([]);
                    var getSummaryCbFn = function (data) {
                        if (data.length !== 0) {
                            for (var i = 0; i < data.length; i++) {
                                var obj = {};
                                obj.roleDescription = data[i].roleDescription;
                                obj.roleName = data[i].roleName;
                                obj.roleId = data[i].roleId;
                                obj.id = data[i].id;
                                obj.flag = data[i].flag;

                                for (var j = 0; j < app.globalEitNameReport().length; j++) {
                                    if (app.globalEitNameReport()[j].value == data[i].eitCodeOperation) {
                                        obj.selfService = app.globalEitNameReport()[j].label;
                                    }
                                }
                                ;
                                self.summaryObservableArray.push(obj);
                            }
                            self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));
                            app.loading(false);
                        } else {
                            //$.notify(self.checkResultSearch(), "error");
                        }
                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    app.loading(false);
                    var serviceName = commonUtil.getAllTrackRequestsRoles;
                    services.getGeneric(serviceName).then(getSummaryCbFn, failCbFn);
                };
                //------------------------------------------


                self.backAction = function () {
                    oj.Router.rootInstance.go('setup');
                };


                self.handleAttached = function () {
                    app.loading(false);
                    self.getSummary();

                };

                self.reset = function () {
                    self.isDisable(true);
                    clearContent();
                };
                function clearContent() {
                    self.roleSearch.roleName("");
                    self.summaryObservableArray([]);
                    self.getSummary();
                }


                //------- BTN SEARCH BY TYPE ACTION---------
                self.searchByType = function () {
                    if (!self.roleSearch.roleName()) {

                        self.getSummary();
                    } else {
                        self.summaryObservableArray([]);
                        self.searchByTypes();

                    }

                };
                //---------------- SEACH BY TYPE METHOD------

                self.searchByTypes = function () {
                    app.loading(true);
                    var jsonData = ko.toJSON(self.roleSearch);
                    var searchByTypesCbFn = function (data) {

                        if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    roleName: data[index].roleName,
                                    roleDescription: data[index].roleDescription,
                                    roleId: data[index].roleId,
                                    id: data[index].id
                                });
                            });
                        } else {
                            app.loading(false);
                            $.notify(self.checkResultSearch(), "error");
                        }
                    };
                    app.loading(false);

                    var failCbFn = function () {
                    };
                    services.addGeneric("trackRoles/search", jsonData).then(searchByTypesCbFn, failCbFn);
                };


                //------- TRANSLATIONS SECTION-----------------
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });

                //---------- warring message decleration-----------------

                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position = {
                    "my": {"vertical": "top", "horizontal": "end"},
                    "at": {"vertical": "top", "horizontal": "end"},
                    "of": "window"
                };

                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };

                //------translations section ---------------
                self.filterRoleNameLbl = ko.observable();
                self.clear = ko.observable();
                self.columnArray = ko.observableArray();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.backActionLbl = ko.observable();
                self.DeleteSummaryLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.trackRolesScreenSummary = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.searchPlaceHolder = ko.observable();
                self.roleNamelbl = ko.observable();
                self.roleDescriptionlbl = ko.observable();
                self.search = ko.observable();
                self.placeholder = ko.observable();
                self.checkResultSearch = ko.observable();
                self.selfServiceLbl = ko.observable();
                function initTranslations() {
                    self.filterRoleNameLbl(getTranslation("trackRequestRoleScreen.filterRoleNameLbl"));
                    self.searchPlaceHolder(getTranslation("trackRequestRoleScreen.searchPlaceHolder"));
                    self.clear(getTranslation("others.clear"));
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.editSummaryLbl(getTranslation("report.editSummary"));
                    self.viewSummaryLbl(getTranslation("report.viewSummary"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.roleNamelbl(getTranslation("report.roleName"));
                    self.roleDescriptionlbl(getTranslation("trackRequestRoleScreen.roleDescription"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.DeleteSummaryLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("labels.message"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.trackRolesScreenSummary(getTranslation("trackRequestRoleScreen.trackRequestRoleSummary"));
                    self.selfServiceLbl(getTranslation("trackRequestRoleScreen.selfService"));
                    self.search(getTranslation("others.search"));

                    self.columnArray([
                        {
                            "headerText": self.roleNamelbl(), "field": "roleName"
                        },
                        {
                            "headerText": self.selfServiceLbl(), "field": "selfService"
                        },
                        {
                            "headerText": self.roleDescriptionlbl(), "field": "roleDescription"
                        }

                    ]);
                }
                initTranslations();
            }
            return trackRequestRoleSummaryContentViewModel;
        });
