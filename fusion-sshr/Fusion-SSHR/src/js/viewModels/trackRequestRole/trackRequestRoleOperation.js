/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * trackRequestRoleOperation module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox',
    'ojs/ojtimezonedata', 'ojs/ojpopup', 'ojs/ojlabel', , 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar-plugin',
    , 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function trackRequestRoleOperationContentViewModel() {
                var self = this;
                var operationType;
                var roleNameValue;
                var roleIdValue;

                //----------------Observables Section------------------
                self.reportTitleLabl = ko.observable();
                self.trainVisible = ko.observable(true);
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.groupValid = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.isDisabled = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.messages = ko.observableArray();
                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                self.RoleDescriptionLblVal = ko.observable();
                self.EITCodeArr = ko.observableArray();
                self.EITCodeArr(app.globalEitNameReport());
                self.actionType = ko.observable();
                self.disableSubmit = ko.observable();
                self.rolesOption = ko.observableArray([]);
                self.rolesDisabled = ko.observable(false);


                //---------CustomModel-------------
                self.customModel = {
                    RoleNameVal: ko.observable(),
                    RoleDescriptionLblVal: ko.observable(),
                    eitCodeVal: ko.observableArray(),
                    roleId: ko.observable(),
                    flag: ko.observable(),
                    id: ko.observable()
                };

                //------------- Role DDW list change handeler--------------
                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position = {
                    "my": {"vertical": "top", "horizontal": "end"},
                    "at": {"vertical": "top", "horizontal": "end"},
                    "of": "window"
                };

                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };

                self.selectManyChangedHandler = function () {
                };
                ///----------------------------

                self.roleTypeChangedHandler = function () {
                    var xx = self.rolesOption().find(e => e.value == self.customModel.RoleNameVal());
                    roleNameValue = xx.label;
                    roleIdValue = xx.value;
                };


                //----------------Add track request Roles--------------
                self.addTrackRequestRole = function () {
                    if (self.retrieve.type == 'update') {
                        self.roleTypeChangedHandler();
                    }
                    ;
                    var slefServices = self.customModel.eitCodeVal();
                    var payload = {
                        "roleName": roleNameValue,
                        "roleId": roleIdValue,
                        "roleDescription": self.customModel.RoleDescriptionLblVal(),
                        "selfService": slefServices
                    };

                    var createroleCbFn = function () {
                        oj.Router.rootInstance.go('trackRequestRoleSummary');
                    };
                    var serviceName = "trackRoles/createOrUpdateTrackRoles/" + self.actionType();
                    services.addGeneric(serviceName, payload).then(createroleCbFn, app.failcb);

                };

                //----------------update track request Roles--------------

                self.deleteBeforeInsert = function () {
                    app.loading(true);
                    var editTrackRequestRoleCbFn = function () {
                        self.actionType('ADD');
                        self.addTrackRequestRole();
                    };
                    var serviceName = "trackRoles/deleteTrackRoles/" + self.retrieve.id;
                    services.getGeneric(serviceName).then(editTrackRequestRoleCbFn, app.failcb);
                };


                //-------------- train section--------------
                self.previousStep = function () {
                    self.hidecancel(true);
                    self.hidepervious(false);
                    self.hidesubmit(false);
                    self.hideUpdatesubmit(false);
                    self.hidenext(true);
                    self.rolesDisabled(false);
                    $(".disable-element").prop('disabled', false);
                    self.currentStepValue('stp1');
                };

                self.nextStep = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid !== "valid") {
                        self.currentStepValue('stp1');
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    } else {
                        self.hidenext(false);
                        self.hidesubmit(true);
                        self.hidepervious(true);
                        $(".disable-element").prop('disabled', true);
                        self.currentStepValue('stp2');

                    }
                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        if (self.retrieve == 'ADD' || self.retrieve.type == 'update') {
                            self.checkForSelfServiceValidation();
                            app.loading(false);

                            return;
                        }
                        self.nextStep();
                    } else {
                        self.hidenext(true);
                        self.hidesubmit(false);
                        self.hidepervious(false);
                        $(".disable-element").prop('disabled', false);
                    }

                };



                //-----------------bttuns actions----------------
                self.cancelAction = function () {
                    oj.Router.rootInstance.go('trackRequestRoleSummary');
                };


                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };

                //-------update commit action--------
                self.commitRecordupdate = function () {
                    operationType = 'EDIT';
                    self.callReport(operationType);
                    self.disableSubmitEdit(true);

                };
                self.cancelButtonupdate = function () {
                    document.querySelector("#yesNoDialogUpdate").close();

                };
                self.submitUpdateButton = function () {
                    document.querySelector("#yesNoDialogUpdate").open();

                };


                self.commitRecord = function () {
                    app.loading(true);
                    if (self.actionType() === 'ADD') {
                        self.addTrackRequestRole();

                    } else if (self.actionType() === 'update') {
                        app.loading(true);
                        self.deleteBeforeInsert();
                    }
                    self.disableSubmit(true);
                };


                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };

                self.stopSelectListener = function () {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };

                // check for self service already exists

                self.checkForSelfServiceValidation = function ( ) {
                    app.loading(true);
                    var ROLeEitCodeCbFn = function (data) {
                        self.currentStepValue('stp1');
                        self.currentStepValueText();
                        var found = self.customModel.eitCodeVal().some(e => data.find(o => o.eitCodeOperation == e && o.roleId == self.customModel.RoleNameVal()));

                        if (found) {
                            self.messages.push({severity: 'error', summary: self.checkMessage(), detail: self.MsgDetail(), autoTimeout: 0
                            });
                            return;
                        } else {
                            self.nextStep();
                        }
                    };

                    var serviceName = "trackRoles/getAllTrackRoles";
                    services.getGeneric(serviceName).then(ROLeEitCodeCbFn, app.failCbFn);

                };

                self.connected = function () {
                    self.customModel.RoleNameVal(parseInt(self.retrieve.roleId));
                    self.customModel.roleId(self.retrieve.roleName);
                    self.customModel.RoleDescriptionLblVal(self.retrieve.roleDescription);
                    self.customModel.eitCodeVal(self.retrieve.selfService);
                    self.customModel.id(self.retrieve.id);
                    self.customModel.flag(self.retrieve.flag);
                };
                //---------handle Attached-----------------
                self.handleAttached = function () {

                    app.loading(false);
                    // convert type of value to int
                    var newArr = app.roleOptionType().forEach(e => e.value = parseInt(e.value));
                    self.rolesOption(app.rolesOption().concat(newArr));
                    self.retrieve = oj.Router.rootInstance.retrieve();
                    if (self.retrieve === 'ADD') {
                        self.actionType('ADD');
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(true);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', false);
                        self.currentStepValue('stp1');

                    } else if (self.retrieve.type === 'view') {
                        self.connected();
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(false);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', true);
                        self.currentStepValue('stp2');
                        self.trainVisible(false);
                        id: ko.observable(self.retrieve.id);

                    } else if (self.retrieve.type === 'update') {
                        self.actionType('update');
                        self.connected();
                    }
                };
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                //------------ TRANSLATION SECTION -------------
                self.createReport = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.placeholder = ko.observable();
                self.stepArray = ko.observableArray([]);
                self.oprationMessageUpdate = ko.observable();
                self.errormessage = ko.observable();
                self.RoleNameLbl = ko.observable();
                self.RoleDescriptionLbl = ko.observable();
                self.EitCodeLbl = ko.observable();
                self.trackRequestRoleOperationScreen = ko.observable();
                self.checkMessage = ko.observable();
                self.MsgDetail = ko.observable();

                function initTranslations() {
                    self.createReport(getTranslation("report.createReport"));
                    self.errormessage(getTranslation("report.errormessage"));
                    self.createReport(getTranslation("report.create"));
                    self.oprationMessage(getTranslation("report.oprationMessage"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessageUpdate(getTranslation("report.oprationMessageUpdate"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.trainView(getTranslation("others.review"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));
                    self.submit(getTranslation("others.submit"));
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                    self.EitCodeLbl(getTranslation("report.EitCode"));
                    self.RoleNameLbl(getTranslation("trackRequestRoleScreen.RoleName"));
                    self.RoleDescriptionLbl(getTranslation("trackRequestRoleScreen.roleDescription"));
                    self.trackRequestRoleOperationScreen(getTranslation("trackRequestRoleScreen.trackRequestRoleOperationScreen"));
                    self.checkMessage(getTranslation("trackRequestRoleScreen.checkMessage"));
                    self.checkMessage(getTranslation("trackRequestRoleScreen.MsgDetail"));

                    self.stepArray([{label: self.createReport(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return trackRequestRoleOperationContentViewModel;
        });
