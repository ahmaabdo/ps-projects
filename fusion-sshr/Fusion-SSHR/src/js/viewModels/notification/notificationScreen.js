/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojarraydataprovider'
            , 'ojs/ojnavigationlist','ojs/ojtable','ojs/ojlistview',
    'ojs/ojswitcher'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.deptArray = ko.observableArray([]);
                self.notification = ko.observable(self.deptArray());
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptArray));
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.msgTitle = ko.observable();
                self.msgBody = ko.observable();
                self.requesterNumber = ko.observable();
                self.type = ko.observable();
                self.creationDate = ko.observable();
                self.view = ko.observable();
                self.review = ko.observable();
                self.selectedRowKey = ko.observable();
                self.oprationdisabled = ko.observable(true);
                self.eitNameLbl = ko.observable();
                self.placeholder = ko.observable();
                self.selctedStruct = ko.observable("");
                self.typeSelected = ko.observable('REG');
                self.rwindex = ko.observable();
                self.personIdSelected = ko.observable();
                self.notificationType = ko.observable();
                self.notificationStatus = ko.observable();
                self.selfServiceType = ko.observable();
                self.reviewDisabled = ko.observable(true);
                self.notiId = ko.observable();
                self.receiverType = ko.observable();
                self.RequesterNumberLbl = ko.observable();
                self.RequesterNumber = ko.observable("");
                self.search = ko.observable();
                self.selctedEITType = ko.observable("");
                self.selctedEITType2 = ko.observable("");
                self.notificationTypeLbl = ko.observable();
                self.notificationActionLbl = ko.observable();
                self.notificationActionArr = ko.observableArray();
                self.eitCodeLbl = ko.observable();
                self.mesBodyAndPersonNum = ko.observable();
                self.typeNotifyLbl = ko.observable();
                self.empName = ko.observable();
                // Call EIT Code
                self.EitName = ko.observableArray([]);
                self.employeeNameArr = ko.observableArray([]);
                self.notificationTypeArr = ko.observableArray([]);
                 this.selectedItems = ko.observableArray([]);
                //-------Approval Model 
                self.approvlModel = {};
                var globalRowData;
                self.actionModelsArr = ko.observableArray();
                //New 
                self.isShown = ko.observable(true);
                self.reject = ko.observable();
                self.approve = ko.observable();
                self.confirmMessage = ko.observable();
                self.approveMessage = ko.observable();
                self.disableSubmit = ko.observable(true);
                self.yes = ko.observable();
                self.no = ko.observable();
                self.rejectMessage = ko.observable();
                self.isLastApprover = ko.observable(false);
                var Keys = [];
                var PaaSKeys = [];
                var tempObject;
                var ArrKeys = [];
                var Typs = ['date', 'date', 'number'];
                 self.model = {};
                 self.PaaSmodel = {};
                 self.Arrs = {};
                 self.koArray = ko.observableArray([]);
                //create file upload data provider
                self.dataProviderAttachment = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
                self.NotifyModel = {
                    self_type: ko.observable(),
                    request_id: ko.observable(),
                    type: ko.observable(),
                    receiver_id: ko.observable()

                };

                self.handleAttached = function (info) {
                    
                    app.loading(false);
                  
                   self.notificationActionArr(app.getPaaSLookup('XXX_SELECT_WITH_STATUS'));
                    sessionStorage.setItem("countCheck",app.notificationsCount());
               
                 
                    self.EitName([]);
                    
                      
                     self.EitName(app.globalEitNameReport());
//                    self.EitName(app.globalEitNameReport());
                    self.getAllNotifi();
//                    setInterval(function(){ 
//                     self.getAllNotifi();
////                     notifyMe();
//                    }, 10*60*1000);
                };
                
                
                function notifyMe() {
                  if (!("Notification" in window)) {
                    alert("This browser does not support desktop notification");
                  }

                
                  else if (Notification.permission === "granted") {
                    // If it's okay let's create a notification
                    var notification = new Notification("please need approval!",{
                      icon:'../css/images/icons/logo-icon.png'  
                    });
                    
                  }

                  // Otherwise, we need to ask the user for permission
                  else if (Notification.permission !== "denied") {
                    Notification.requestPermission().then(function (permission) {
                      // If the user accepts, let's create a notification
                      if (permission === "granted") {
                        var notification = new Notification("please need approval!");
                      }
                    });
                  }

  
}
                
                self.getAllNotifi = function (){
                  
//                  notifyMe();
                    self.noticationModel = {
                        "MANAGER_ID": rootViewModel.personDetails().personId,
                        "managerOfManager": "x",
                        "position": rootViewModel.personDetails().positionId,
                        "emp": rootViewModel.personDetails().personId,
                        "userRoles": rootViewModel.userRoles()

                    };
                    var jsonData = ko.toJSON(self.noticationModel);
                    var getNotificationCBF = function (data) {
                        
                        self.deptArray([]);
                        app.notificationsCount(data.length);
                      
                       if(data.length>sessionStorage.getItem("countCheck")){
                           notifyMe();
                           sessionStorage.setItem("countCheck",app.notificationsCount());
                           app.notificationsCount();
                       }
                     
                       
                        //lOCAL 
//                        for (var i = 0; i < data.length; i++) {
//                            for (var r = 0; r < rootViewModel.PersonNumberArr().length; r++) {
//                                if (rootViewModel.PersonNumberArr()[r].value == data[i].person_NUMBER) {
//                                    self.empName(rootViewModel.PersonNumberArr()[r].label);
//                                }
//                            }
//                            for (var j = 0; j < self.EitName().length; j++) {
//                                if (self.EitName()[j].value === data[i].self_TYPE) {
//                                    self.eitCodeLbl(self.EitName()[j].label);
//                                    self.mesBodyAndPersonNum(self.EitName()[j].label + " / " + self.empName());
//                                }
//                                for (var v = 0; v < self.notificationTypeArr().length; v++) {
//                                    if (data[i].type === self.notificationTypeArr()[v].value) {
//
//                                        self.typeNotifyLbl(self.notificationTypeArr()[v].label);
//                                    }
//                                }
//                                }
//                                var obj = {};
//                                obj.rowNumberRenderer = (i+1);
//                                obj.id = data[i].ID;
//                                obj.MSG_TITLE = data[i].MSG_TITLE;
//                                obj.MSG_BODY = data[i].MSG_BODY;
//                                obj.CREATION_DATE = data[i].CREATION_DATE;
//                                obj.TYPE = data[i].TYPE;
//                                obj.TRS_ID = data[i].REQUEST_ID;
//                                obj.SELF_TYPE = data[i].SELF_TYPE;
//                                obj.receiver_type = data[i].RECEIVER_TYPE;
//                                obj.person_number = data[i].RESPONSE_PERSON_ID;
//                                obj.status = data[i].STATUS;
//                                obj.roleId = data[i].RECEIVER_ID;
//                                obj.person_id = data[i].PERSON_NUMBER;
//                                obj.msg_Title_Lbl = self.eitCodeLbl();
//                                obj.MSG_Body_Lbl = self.mesBodyAndPersonNum();
//                                obj.type_Lbl = self.typeNotifyLbl();
//                                self.deptArray.push(obj);
//
//                        }
                        //Deployed 
                            for(var i = 0; i < data.length; i++){
                                for(var r = 0; r < rootViewModel.PersonNumberArr().length; r++){
                                    if(rootViewModel.PersonNumberArr()[r].value == data[i].PERSON_NUMBER){
                                        self.empName(rootViewModel.PersonNumberArr()[r].label);
                                    }
                                }
                                
                                
                               
                            for (var j = 0; j < self.EitName().length; j++){
                                if(self.EitName()[j].value === data[i].SELF_TYPE){
                                self.eitCodeLbl(self.EitName()[j].label);
                                 self.mesBodyAndPersonNum(self.EitName()[j].label + " / " + data[i].PERSON_NAME);
                                }
                                for (var v = 0; v < self.notificationTypeArr().length; v++) {
                                    if (data[i].TYPE === self.notificationTypeArr()[v].value) {

                                        self.typeNotifyLbl(self.notificationTypeArr()[v].label);
                                    }
                                }
                                }
                                var obj = {};
                                obj.rowNumberRenderer = (i+1);
                                obj.id = data[i].ID;
                                obj.MSG_TITLE = data[i].MSG_TITLE;
                                obj.MSG_BODY = data[i].MSG_BODY;
                                obj.CREATION_DATE = data[i].CREATION_DATE;
                                obj.TYPE = data[i].TYPE;
                                obj.TRS_ID = data[i].REQUEST_ID;
                                obj.SELF_TYPE = data[i].SELF_TYPE;
                                obj.receiver_type = data[i].RECEIVER_TYPE;
                                obj.person_number = data[i].RESPONSE_PERSON_ID;
                                obj.status = data[i].STATUS;
                                obj.roleId = data[i].RECEIVER_ID;
                                obj.person_id = data[i].PERSON_NUMBER;
                                obj.msg_Title_Lbl = self.eitCodeLbl();
                                obj.MSG_Body_Lbl = self.mesBodyAndPersonNum();
                                obj.type_Lbl = self.typeNotifyLbl();
                                obj.personName = data[i].PERSON_NAME;
                                obj.disableView = ko.observable(true);
                                self.deptArray.push(obj);
                            }

                        self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptArray, {idAttribute: 'id'}));

                        // self.notification(self.deptArray())
                        // self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.notification,{idAttribute : 'MSG_TITLE'}));
                    }
                    services.addGeneric(commonUtil.allNotifications, jsonData).then(getNotificationCBF, app.failCbFn);
                };

           
                self.tableSelectionListener = function (event) {
                     var key = event.detail.value;
                    var items = self.deptArray();
                    for (var i = 0; i < items.length; i++) {
                        self.deptArray()[i].disableView(true);
                    }
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].rowNumberRenderer === key[0]) {
                            var index  = i;
                            //self.selectedRowKey(index);
                            self.rwindex(index);
                        self.selectedRowKey(self.deptArray()[index].TRS_ID);
                        self.reviewDisabled(false);
                        self.personIdSelected(self.deptArray()[index].personId);
                        self.notificationType(self.deptArray()[index].TYPE);
                        self.selfServiceType(self.deptArray()[index].SELF_TYPE);
                        rootViewModel.reviewNotiType(self.deptArray()[index].SELF_TYPE);
                        self.notiId(self.deptArray()[index].id);
                        self.receiverType(self.deptArray()[index].RECEIVER_TYPE);
                        self.notificationStatus(self.deptArray()[index].status);
//                        self.oprationdisabled(false);
                        self.deptArray()[index].disableView(false);
                        var rejectMessageLbl = getTranslation("others.rejectMSG") + self.deptArray()[self.rwindex()].msg_Title_Lbl + " ?";
                        var approveMessageLbl = getTranslation("others.approvalMSG") + self.deptArray()[self.rwindex()].msg_Title_Lbl + " ?";
                        self.rejectMessage(getTranslation(rejectMessageLbl));
                        self.approveMessage(getTranslation(approveMessageLbl));
                        if(self.deptArray()[index].TYPE == "FYI"){ 
                        self.disableSubmit(true);
                        }
                        else
                        {
                          self.disableSubmit(false);  
                        }
                            break;
                        }
                    }
//                    var currentRow = key.currentRow;
//                    var index = currentRow['rowIndex'];
//                        self.selectedRowKey(self.deptArray()[index].TRS_ID);
                    
//                    var currentRow = data.currentRow;
//                    if (currentRow === null) {
//                    } else {
//                        self.rwindex(currentRow['rowIndex']);
//                        var index = currentRow['rowIndex'];
//                        self.selectedRowKey(self.deptArray()[index].TRS_ID);
//                        self.reviewDisabled(false);
//                        self.personIdSelected(self.deptArray()[index].personId);
//                        self.notificationType(self.deptArray()[index].TYPE);
//                        self.selfServiceType(self.deptArray()[index].SELF_TYPE);
//                        rootViewModel.reviewNotiType(self.deptArray()[index].SELF_TYPE);
//                        self.notiId(self.deptArray()[index].id);
//                        self.receiverType(self.deptArray()[index].RECEIVER_TYPE);
//                        self.notificationStatus(self.deptArray()[index].status);
//                        self.oprationdisabled(false);
//                        var rejectMessageLbl = getTranslation("others.rejectMSG") + self.deptArray()[self.rwindex()].msg_Title_Lbl + " ?";
//                        var approveMessageLbl = getTranslation("others.approvalMSG") + self.deptArray()[self.rwindex()].msg_Title_Lbl + " ?";
//                        self.rejectMessage(getTranslation(rejectMessageLbl));
//                        self.approveMessage(getTranslation(approveMessageLbl));
//                        if(self.deptArray()[index].TYPE == "FYI"){ 
//                        self.disableSubmit(true);
//                        }
//                        else
//                        {
//                          self.disableSubmit(false);  
//                        }
//                    }
                };
                self.showReview = function () {
                  
                    if(self.deptArray()[self.rwindex()].SELF_TYPE==='XXX_PROBATION_PERIOD')
                    {
                        if(self.deptArray()[self.rwindex()].receiver_type == "LINE_MANAGER"){
                            self.deptArray()[self.rwindex()].typeScreen = "EDIT";
                        }
                        else
                        {
                        self.deptArray()[self.rwindex()].typeScreen = "REVIWE";
                    }
                       oj.Router.rootInstance.store(self.deptArray()[self.rwindex()]); 
                 
                       oj.Router.rootInstance.go('probationReview');
                  }
                  else if(self.deptArray()[self.rwindex()].SELF_TYPE==='XXX_HANDOVER_REQUEST'){
                      self.deptArray()[self.rwindex()].pageMode = "Notfi";
                      self.deptArray()[self.rwindex()].checkUser = "employee";
                      localStorage.setItem('handoverData', JSON.stringify(self.deptArray()[self.rwindex()]));
                    //   oj.Router.rootInstance.store(self.deptArray()[self.rwindex()]);
                      oj.Router.rootInstance.go('handOverOperation');
                  }
                  else if (self.deptArray()[self.rwindex()].SELF_TYPE === 'XXX_EDU_EXPENSE_REQUEST') {
                        self.deptArray()[self.rwindex()].pageMode = "Notfi";
                        oj.Router.rootInstance.store(self.deptArray()[self.rwindex()]);
                        oj.Router.rootInstance.go('EducationExpenseReview');
                    }
                    else{
                     oj.Router.rootInstance.store(self.deptArray()[self.rwindex()]);  
                     oj.Router.rootInstance.go('dynamicReview');
                   
                   }
                };

                
                
                self.reviewNotification = function () {
                    if (self.selectedRowKey()) {
                        rootViewModel.recieveType(self.receiverType());
                        rootViewModel.selectedTableRowKeyNotifiation(self.selectedRowKey());
                    }
                }

                var getTranslation = oj.Translations.getTranslatedString;

                self.searchPerson = function () {
                  
                     
                    self.searchNotification();
                   
                };

                //**********************************************************************
                
                    self.nationalityIdentityLbl=ko.observable();
                    self.nationalityIdentityVal=ko.observable("");
                self.searchNotification = function (info) {
                    
                    self.deptArray([]);
                  app.loading(true);
                    self.searchModel = {
                        "MANAGER_ID": rootViewModel.personDetails().personId,
                                       
                        "managerOfManager": "x",
                        "position": rootViewModel.personDetails().positionId,
                        "emp": rootViewModel.personDetails().personId,
                        "SELF_TYPE": self.selctedStruct(),
                        "REQUEST_ID": self.RequesterNumber(),
                        "TYPE": self.selctedEITType(),
                        "nationalIdentity":self.nationalityIdentityVal(),
                        "STATUS":self.selctedEITType2(),
                        "personNumber":self.RequesterNumber(),
                        "userRoles": rootViewModel.userRoles()

                    };

                   
                
                    //var jsonData = ko.toJSON(self.searchModel);
                    var getNotificationCBF = function (data) {
                        self.deptArray([]);
                       
                        //lOCAL 
                        for(var i = 0; i < data.length; i++){
                                for(var r = 0; r < rootViewModel.PersonNumberArr().length; r++){
                                    if(rootViewModel.PersonNumberArr()[r].value == data[i].PERSON_NUMBER){
                                        self.empName(rootViewModel.PersonNumberArr()[r].label);
                                    }
                                }
                                
                                
                               
                            for (var j = 0; j < self.EitName().length; j++){
                                if(self.EitName()[j].value === data[i].SELF_TYPE){
                                self.eitCodeLbl(self.EitName()[j].label);
                                 self.mesBodyAndPersonNum(self.EitName()[j].label + " / " + data[i].PERSON_NAME);
                                }
                                for (var v = 0; v < self.notificationTypeArr().length; v++) {
                                    if (data[i].TYPE === self.notificationTypeArr()[v].value) {

                                        self.typeNotifyLbl(self.notificationTypeArr()[v].label);
                                    }
                                }
                                }
                                var obj = {};
                                obj.rowNumberRenderer = (i+1);
                                obj.id = data[i].ID;
                                obj.MSG_TITLE = data[i].MSG_TITLE;
                                obj.MSG_BODY = data[i].MSG_BODY;
                                obj.CREATION_DATE = data[i].CREATION_DATE;
                                obj.TYPE = data[i].TYPE;
                                obj.TRS_ID = data[i].REQUEST_ID;
                                obj.SELF_TYPE = data[i].SELF_TYPE;
                                obj.receiver_type = data[i].RECEIVER_TYPE;
                                obj.person_number = data[i].RESPONSE_PERSON_ID;
                                obj.status = data[i].STATUS;
                                obj.roleId = data[i].RECEIVER_ID;
                                obj.person_id = data[i].PERSON_NUMBER;
                                obj.msg_Title_Lbl = self.eitCodeLbl();
                                obj.MSG_Body_Lbl = self.mesBodyAndPersonNum();
                                obj.type_Lbl = self.typeNotifyLbl();
                                obj.personName = data[i].PERSON_NAME;
                                obj.disableView = ko.observable(true);
                                self.deptArray.push(obj);
                            }
                            app.loading(false);

                        self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptArray, {idAttribute: 'id'}));

                    };
                    //services.searcheGeneric(commonUtil.searchNotifications, self.searchModel).then(getNotificationCBF, app.failCbFn);
                    services.addGeneric(commonUtil.allNotifications, self.searchModel).then(getNotificationCBF, app.failCbFn);
                };

                //**********************************************************************

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                        //self.deptArray([]);
                        self.getAllNotifi();

                    }

                });
                //New Task 
                self.rejectButton = function () {
                    document.querySelector("#rejectDialog").open();
                };
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                    
                    self.isLastApprover(rootViewModel.isLastApprover(self.deptArray()[self.rwindex()].TRS_ID, self.deptArray()[self.rwindex()].SELF_TYPE,false));
                  
                    if(self.deptArray()[self.rwindex()].SELF_TYPE!='XXX_HR_PROBATION_PERFORMANCE'){
                    getElementEntryByEitCode();
                    self.approvedAction(); 
                   }else if (self.deptArray()[self.rwindex()].SELF_TYPE=='XXX_HR_PROBATION_PERFORMANCE'){
                       self.getAllPropationAndEvaluation(self.deptArray()[self.rwindex()].TRS_ID);
                   }


                    
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.rejectCancelButton = function () {
                    document.querySelector("#rejectDialog").close();
                };
                
                self.approveRequst = function (data, event) {                  
                    self.disableSubmit(true);
                    if(self.deptArray()[self.rwindex()].SELF_TYPE =='XXX_HR_PROBATION_PERFORMANCE'){
                        self.approveProbationRequst();
                    }else{
                        
            
                    var headers = {
                        "MSG_TITLE": self.deptArray()[self.rwindex()].MSG_TITLE,
                        "MSG_BODY": self.deptArray()[self.rwindex()].MSG_BODY,
                        "TRS_ID": self.deptArray()[self.rwindex()].TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "RESPONSE_CODE": "APPROVED",
                        "ssType": self.deptArray()[self.rwindex()].SELF_TYPE ,
                        "PERSON_NAME":self.deptArray()[self.rwindex()].personName,
                        "rejectReason":" ",
                        "workflowId":self.deptArray()[self.rwindex()].id
                    };
                 
                    var approvalActionCBFN = function (data) {
                        if (self.isLastApprover()) {
                            oprationServiceRecord();
                            var updateStatusCBFN =function (data){
   
                            };
                            
                             services.getGenericAsync("PeopleExtra/updateStatus?id="+self.deptArray()[self.rwindex()].TRS_ID, {}).then(updateStatusCBFN, app.failCbFn);
                              
                           if(self.deptArray()[self.rwindex()].SELF_TYPE =='XXX_HR_PERSON_PAY_METHOD'){
                               self.updateBankFile();
                           }
                           //XXX_HR_PERSON_PAY_METHOD
                           
                           //Update Bank File 
                           //
                           //
                            self.model.url = self.url;
                            //var jsonData = ko.toJSON(self.model);
                            var jsonData = {model:self.model,TRS_ID:self.deptArray()[self.rwindex()].TRS_ID,eitCode:self.deptArray()[self.rwindex()].SELF_TYPE}
                            var supmitEFFCBFN = function (data) {
                                var Data = data;
                                   
                                // self.disableSubmit(false);
                                if (oj.Router.rootInstance._navHistory.length > 1) {
                                    oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                                } else {
                                    oj.Router.rootInstance.go('home');
                                }

                            };

                            services.addGeneric("eff/" + "ADD"+"/"+self.deptArray()[self.rwindex()].person_number, ko.toJSON(jsonData)).then(supmitEFFCBFN, app.failCbFn);
                           
                            if (self.elementEntryArr().length > 0) {
                                for (var i = 0; i < self.elementEntryArr().length; i++) {
                                    self.elementEntryArr()[i].inputValue =  JSON.parse(self.elementEntryArr()[i].inputValue) ;
                                    if (self.elementEntryArr()[i].typeOfAction == "Absence") {
                                        var absenseModel = {
                                            "personNumber": self.reqeustedPersonNumber,
                                            "employer": "HHA",
                                            "absenceType": self.model[self.elementEntryArr()[i].absenceType](),
                                            "startDate": self.model[self.elementEntryArr()[i].startDate](),
                                            "endDate": self.model[self.elementEntryArr()[i].endDate](),
                                            "absenceStatusCd": self.elementEntryArr()[i].absenceStatus,
                                            "startDateDuration": 1
                                        };
                                        var supmitAbsenceCBFN = function (data) {

                                        }

                                        services.addGeneric("absence/", absenseModel).then(supmitAbsenceCBFN, app.failCbFn);
                                    } else {
                                        for(var inputIndex = 0 ; inputIndex<self.elementEntryArr()[i].inputValue.length;inputIndex ++ ){
                                           
                                            if (self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue != "Transaction_ID") {
                                                if (self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue) {
                                                    self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = self.model[self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue ]();
                                                } else {
                                                    self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = "";
                                                }
                                            }else{
                                                 self.elementEntryArr()[i].inputValue[inputIndex].inputEitValue = "P"+self.deptArray()[self.rwindex()].TRS_ID;
                                            }
                                           
                                           
                                        }
                                        var enotiffivtiveStartDate ;
                                        var reason; 
                                        if(self.elementEntryArr()[i].effivtiveStartDate){
                                          effivtiveStartDate =  self.model[self.elementEntryArr()[i].effivtiveStartDate]() ;
                                           //effivtiveStartDate =  self.elementEntryArr()[i].effivtiveStartDate ;
                                        }else{
                                            effivtiveStartDate = "%EffectiveStartDate%";
                                        }
                                        if(self.elementEntryArr()[i].reason){
                                          reason =  self.model[self.elementEntryArr()[i].reason]() ; 
                                           //effivtiveStartDate =  self.elementEntryArr()[i].effivtiveStartDate ;
                                        }else{
                                            reason = "";
                                        }
                                        var jsonBody = {
                                            personNumber: self.reqeustedPersonNumber,
                                            elementName: self.elementEntryArr()[i].elementName,
                                            legislativeDataGroupName: self.elementEntryArr()[i].legislativeDatagroupName,
                                            assignmentNumber: "E" + self.reqeustedPersonNumber,
                                            entryType: "E",
                                            creatorType: self.elementEntryArr()[i].creatorType,
                                            sourceSystemId:"PaaS_"+self.deptArray()[self.rwindex()].TRS_ID,
                                            trsId: self.deptArray()[self.rwindex()].TRS_ID,
                                            SSType: self.elementEntryArr()[i].recurringEntry,
                                            eitCode: self.elementEntryArr()[i].eitCode,
                                            inputValues: self.elementEntryArr()[i].inputValue,
                                            effivtiveStartDate:effivtiveStartDate,
                                            reason:reason
                                            
                                        };
                                        var submitElement = function (data1) {
                                        };

                                        services.submitElementEntry(JSON.stringify(jsonBody)).then(submitElement, app.failCbFn);
                                    }
                                }
                            }
                        } else {
                            app.notificationsCount();
                            self.getAllNotifi();
                            document.querySelector("#yesNoDialog").close();
                        }
                        document.querySelector("#yesNoDialog").close();
                        // oj.Router.rootInstance.go('notificationScreen');
                    }
                    //services.workflowAction("workflowApproval/", headers).then(approvalActionCBFN, app.failCbFn)
                    services.workflowAction("workflowApproval/", headers).then( app.failCbFn,approvalActionCBFN);
                }
            };
                
                
                self.rejectRewaardRequst = function (data, event) {
                    self.disableSubmit(true);
                    var headers = {
                        "MSG_TITLE": self.deptArray()[self.rwindex()].MSG_TITLE,
                        "MSG_BODY": self.deptArray()[self.rwindex()].MSG_BODY,
                        "TRS_ID": self.deptArray()[self.rwindex()].TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "RESPONSE_CODE": "REJECTED",
                        "ssType": self.deptArray()[self.rwindex()].SELF_TYPE,
                        "rejectReason":" ",
                        "workflowId":self.deptArray()[self.rwindex()].id
                    };
                    var approvalActionCBFN = function (data) {
                        app.notificationsCount();
                        self.getAllNotifi();
                        document.querySelector("#rejectDialog").close();
                        
                    };
                    services.workflowAction("workflowApproval/", headers).then(app.failCbFn, approvalActionCBFN);

                };
                
                //Update Bank File Function //
                self.updateBankFile=function(){
                    var bankFileJsonBody = {
                        "bankName": self.model.bank(),
                        "bankBranchName": self.model.bankBranch(),
                        "accountNumber": self.model.newAccountNumber(),
                        "IBAN": self.model.iban(),
                        "trsId": self.deptArray()[self.rwindex()].TRS_ID,
                        "personNumber": self.reqeustedPersonNumber,
                        "newBankName": self.model.bank(),
                        "newBankBranchName": self.model.bankBranch(),
                        "newAccountNumber": self.model.newAccountNumber()
                    };
                    var submitBankFile= function(data){
                        var createPersonalPaymentMethodJsonBody = {"legislativeDataGroupName": "SA Legislative Data Group",
                            "assignmentNumber": "E"+self.reqeustedPersonNumber,
                            "personalPaymentMethodCode": "Bank Transfer",
                            "effectiveStartDate": self.model.personPayEffectiveStartDate().replace('-','/').replace('-','/'),
                            "paymentAmountType": "P",
                            "processingOrder": "1",
                            "organizationPaymentMethodCode": "Bank Transfer",
                            "percentage": "100",
                            "bankName": self.model.bank(),
                            "bankBranchName": self.model.bankBranch(),
                            "bankAccountNumber": self.model.newAccountNumber(),
                            "personNumber": self.reqeustedPersonNumber
                            ,"trsId": self.deptArray()[self.rwindex()].TRS_ID
                        };
                        var createPersonalPaymentMethodCBFN = function(data){
                            self.model = {};
                        };
                      services.submitHDLFile("createPersonalPaymentMethod",JSON.stringify(createPersonalPaymentMethodJsonBody)).then(createPersonalPaymentMethodCBFN, app.failCbFn);  
                    };
                  services.submitHDLFile("createBankFile",JSON.stringify(bankFileJsonBody)).then(submitBankFile, app.failCbFn);
                };
                //End Of Update Bank File ------------------
                self.elementEntryArr = ko.observableArray([]);
                function getElementEntryByEitCode() {
                    var EitCode = self.deptArray()[self.rwindex()].SELF_TYPE;
                    var getValidGradeCBF = function (data) {
                        
                        self.elementEntryArr(data);

                    
                    };

                    services.getGeneric("elementEntrySetup/getElementEntry/" + EitCode).then(getValidGradeCBF, self.failCbFn);
                };
                //----------This Function To Set Url Call 
                self.setUrl = function (EitCode) {
                    return app.personDetails().employeeURL[EitCode];
                };
                self.segmentChangedHandler = function (event, data) {
                    if (event.path[0].attributes.id) {
                        var id = event.path[0].attributes.id.nodeValue;
                        var res = id.substr(6, id.length);
                        var modelArr = Object.keys(self.model);
                        var realIndex = modelArr.indexOf(res);
                        var indexOfModel = modelArr.indexOf(res) + 1;
                        //  value_SET_TYPE
                        if (tempObject[indexOfModel].value_SET_TYPE == "Y") {

                            var getReportCBCF = function (data) {
                                var tempObject2 = data;
                                var arr = tempObject2;
                                var arrays = [];
                                if (arr.length) {
                                    for (var ind = 0; ind < arr.length; ind++) {
                                        arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION})
                                    }
                                } else {
                                    arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                }
                                //DESCRIPTION
                                self.Arrs[tempObject[indexOfModel].FLEX_value_SET_NAME + tempObject[indexOfModel].DESCRIPTION + "Arr"](arrays);


                            };
                            var xx = {"reportName": "DependentReport", "valueSet": tempObject[indexOfModel].FLEX_value_SET_NAME, "parent": self.model[modelArr[realIndex]]()};
                            services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                        }

                    }


                };
                //-----------get attchment-------------------//
                self.approvedAction = function (info) {
                    self.reqestedID = self.deptArray()[self.rwindex()].person_id;

                   
                    var getEit = function (data) {
                        tempObject = data;
                        Keys = [];
                        var Iterater = 0;
                        for (var i = 0; i < tempObject.length; i++) {
                            if (tempObject[i].DEFAULT_value && tempObject[i].DEFAULT_TYPE == 'C' && tempObject[i].DEFAULT_value != "PAAS") {
                                var actionModel = {
                                    reportName: tempObject[i].DEFAULT_value,
                                    whenChangeValue: [],
                                    segmentName: Keys[i]
                                };

                                actionModel.whenChangeValue = searchArrayContainValue(tempObject[i].DEFAULT_value, app.globalEITDefultValueParamaterLookup());
                                if (actionModel.whenChangeValue[0]) {
                                    actionModel.whenChangeValue = actionModel.whenChangeValue[0].split(',');
                                }
                                self.actionModelsArr.push(actionModel);

                                for (var index = 0; index < actionModel.whenChangeValue.length; index++) {

                                    var reportPaylod = {"reportName": tempObject[i].DEFAULT_value};
                                    for (var index = 0; index < actionModel.whenChangeValue.length; index++) {
                                        if (Object.keys(self.model).indexOf(actionModel.whenChangeValue[index]) != -1) {
                                            reportPaylod[actionModel.whenChangeValue[index]] = self.model[actionModel.whenChangeValue[index]]();
                                        } else {
                                            reportPaylod[actionModel.whenChangeValue[index]] = app.personDetails()[actionModel.whenChangeValue[index]];
                                        }
                                    }
                                    var getReportValidationCBCF = function (dataSaaSQuery) {
                                        var resultOfQueryObj = dataSaaSQuery;
                                        self.model[Keys[i]](resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);
                                    }
                                    services.getGenericReport(reportPaylod).then(getReportValidationCBCF, app.failCbFn);


                                }




                            }
                            //Build Model 
                            Keys.push(tempObject[i].DESCRIPTION);
                            self.model[Keys[i]] = ko.observable();
                            PaaSKeys.push(tempObject[i].APPLICATION_COLUMN_NAME);
                            self.PaaSmodel[PaaSKeys[i]] = ko.observable(self.model[Keys[i]]());
                            //end OF Build Model 
                            if ((tempObject[i].FLEX_value_SET_NAME != "100 Character No Validation") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "10 Number") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "HRC_STANDARD_DATE") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_NUMBER") &&
                                    (tempObject[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_DATE")
                                    ) {
                                if (tempObject[i].value_SET_TYPE == "X" || tempObject[i].value_SET_TYPE == "I") {
                                    //  self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var getReportCBCF = function (data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        var arrays = [];
                                        for (var x = 0; x < arr.length; x++) {
                                            arrays.push({"value": arr[x].FLEX_value, "label": arr[x].value_DESCRIPTION})
                                        }
                                        self.Arrs[ArrKeys[Iterater]](arrays);


                                    };
                                    var xx = {"reportName": "inDependentReport", "valueSet": tempObject[i].FLEX_value_SET_NAME};
                                    services.getGenericReport(xx).then(getReportCBCF, app.failCbFn);
                                    Iterater = Iterater + 1;
                                } else if (tempObject[i].value_SET_TYPE == "Y") {
                                    //         self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();

                                    Iterater = Iterater + 1;

                                } else if (tempObject[i].value_SET_TYPE == "F") {
                                    //    self.ishide(false);
                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var getDynamicReport = function (data) {
                                        var val = "value";
                                        var lbl = "label";
                                        var q;
                                        var tempObject = data;
                                        q = "select " + tempObject.ID_COL + " " + val + " ,  " + tempObject.value_COL + "  " + lbl
                                                + " From " + tempObject.APPLICATION_TABLE_NAME + "  " + tempObject.ADDITIONAL_WHERE_CLAUSE;


                                        var str = q;
                                        var flex = ":";
                                        var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                        var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                        var is_HaveFlex = true;
                                        if (str.indexOf(':') == -1) {
                                            is_HaveFlex = false;
                                        }
                                        while (is_HaveFlex) {
                                            var strWitoutFlex = str.substr((str.indexOf(":") + flex.length - 1));
                                            var ModelName = strWitoutFlex.substr(0, strWitoutFlex.indexOf('#'));
                                            actionModel.whenChangeValue.push(ModelName);
                                            str = str.replace(strWitoutFlex, self.reqestedID);
                                            if (str.indexOf(":") == -1) {
                                                is_HaveFlex = false;
                                            }
                                        }



                                        if (str.includes('undefined')) {
                                        } else {
                                            //Here Must Call Report Fpr This Perpus 
                                            var getArrCBCF = function (data) {
                                                var tempObject2 = data;
                                                var arr = tempObject2;
                                                if (tempObject2.length) {
                                                    self.Arrs[ArrKeys[Iterater]](arr);
                                                } else {
                                                    self.Arrs[ArrKeys[Iterater]]([arr])
                                                }
                                            };

                                            services.getDynamicReport(str).then(getArrCBCF, app.failCbFn);

                                        }



                                    };
                                    services.getQueryForListReport(tempObject[i].FLEX_value_SET_NAME).then(getDynamicReport, app.failCbFn);
                                    Iterater = Iterater + 1;
                                } else if (tempObject[i].FLEX_value_SET_NAME == "XXX_HR_DYNAMIC_COLUMN") {

                                    ArrKeys.push(tempObject[i].FLEX_value_SET_NAME + tempObject[i].DESCRIPTION + "Arr");
                                    self.Arrs[ArrKeys[Iterater]] = ko.observableArray();
                                    var reportName = searchArray(tempObject[i].DESCRIPTION, app.globalEITReportLookup());
                                    var getReportCBCF = function (data) {
                                        var tempObject2 = data;
                                        var arr = tempObject2;
                                        if (tempObject2.length) {
                                            self.Arrs[ArrKeys[Iterater]](arr);
                                        } else {
                                            self.Arrs[ArrKeys[Iterater]]([arr])
                                        }

                                    }
                                    var reportPaylad = {"reportName": reportName, "personId": self.reqestedID};

                                    services.getGenericReport(reportPaylad).then(getReportCBCF, app.failCbFn);
                                    Iterater = Iterater + 1;

                                }



                            }
                            Typs.push(tempObject[i].FLEX_value_SET_NAME);
                        }
                        //buildScreen.buildScreen(tempObject, "xx", self.model, self.isDisabledx, self.dataArray);
                        // self.actionAvilable(true);
                        //  self.setToDisabeld();
                        self.getPaaSEitesValues();
                        self.getAttachment();
                    };
                    if (app.getLocale() == 'ar') {
                        self.lang = "AR";
                    } else {
                        self.lang = "US";
                    }
                    services.getEIT(self.deptArray()[self.rwindex()].SELF_TYPE, self.lang).then(getEit, app.failCbFn);
                };
                self.setModelValue = function () {
                    for (var i = 0; i < Object.keys(self.model).length; i++) {
                        for (var j = 0; Object.keys(self.prevPram).length; j++) {
                            if (Object.keys(self.model)[i] == Object.keys(self.prevPram)[j]) {
 
                                self.model[Object.keys(self.model)[i]](self.prevPram[Object.keys(self.prevPram)[j]]);
                                break;
                            }
                        }
                    }
                    self.model.PersonExtraInfoId = self.prevPram.PersonExtraInfoId;
                    self.model.PersonId = self.prevPram.PersonId;
                };
                self.getPaaSEitesValues = function () {
                    var getPaasEitsValuesCBFN = function (data) {
                        globalRowData=data;
                        
                        self.reqeustedPersonNumber = data.person_number;
                        var jsonData = JSON.parse(data.eit);
                        var values = JSON.parse(jsonData);
                        for (var i = 0; i < Object.keys(self.model).length; i++) {
                            for (var j = 0; j < Object.keys(values).length; j++) {

                                if (Object.keys(self.model)[i] == Object.keys(values)[j]) {

                                    self.model[Object.keys(self.model)[i]](values[Object.keys(values)[j]]);
                                    //For Set Dependent List 
                                    if (tempObject[i].value_SET_TYPE == "X" && tempObject[i + 1].value_SET_TYPE == "Y") {
                                        var getDependentReportCBCF = function (data) {
                                            var tempObject2 = data;
                                            var arr = tempObject2;
                                            var arrays = [];
                                            if (arr.length) {
                                                for (var ind = 0; ind < arr.length; ind++) {
                                                    arrays.push({"value": arr[ind].FLEX_value, "label": arr[ind].value_DESCRIPTION})
                                                }
                                            } else {
                                                arrays.push({"value": arr.FLEX_value, "label": arr.value_DESCRIPTION});
                                            }
                                            self.Arrs[tempObject[i + 1].FLEX_value_SET_NAME + tempObject[i + 1].DESCRIPTION + "Arr"](arrays);
                                        }
                                        var xx = {"reportName": "DependentReport", "valueSet": tempObject[i + 1].FLEX_value_SET_NAME, "parent": self.model[Object.keys(self.model)[i]]()};
                                        services.getGenericReport(xx).then(getDependentReportCBCF, app.failCbFn);
                                    }
                                    //End Of Set Dependent Value Set 

                                }


                            }
//                     if (Object.keys(self.model)[i]==Object.keys(values)[i]){                        
//                          self.model[Object.keys(self.model)[i]](values[Object.keys(values)[i]]);
//                     }
                        }

                        self.url = data.url;
                    }
                    services.getGeneric("PeopleExtra/" + self.deptArray()[self.rwindex()].TRS_ID).then(getPaasEitsValuesCBFN, app.failCbFn);
                };
                  //-----------get attchment-------------------//
                self.getAttachment=function(){
                    self.koArray([]);
                      var attachmentView = function (data) {
                    for (var i = 0; i < data.length; i++) {

//                                    var baseStr64 = data[i].attachment;
//                                    drawAttachment2(i, baseStr64);
                        self.koArray.push({
                            "id": data[i].id,
                            "name": data[i].name,
                            "data": data[i].attachment
                        });

                    }

                    // imgElem.setAttribute('src', baseStr64);
                };
                services.getGeneric("attachment/" + self.deptArray()[self.rwindex()].TRS_ID).then(attachmentView, app.failCbFn);
            }
                
                function initTranslations() {

                    self.msgTitle(getTranslation("notification.msgTitle"));
                    self.nationalityIdentityLbl(getTranslation("notification.nationalityIdentity"));
                    self.msgBody(getTranslation("notification.msgBody"));
                    self.requesterNumber(getTranslation("notification.requesterNumber"));
                    self.type(getTranslation("notification.type"));
                    self.notificationActionLbl(getTranslation("notification.notificationActionLbl"));
                    self.creationDate(getTranslation("notification.creationDate"));
                    self.review(getTranslation("notification.review"));
                    self.view(getTranslation("others.view"));
                    self.eitNameLbl(getTranslation("notification.eitCode"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.RequesterNumberLbl(getTranslation("notification.nationalNumber"));
                    self.search(getTranslation("others.search"));
                    self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
                    self.notificationTypeArr(app.getPaaSLookup('NOTIFICATION_TYPE'));
                    self.reject(getTranslation("others.reject"));
                    self.approve(getTranslation("others.approve"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.approveMessage(getTranslation("others.approvalMSG"));
                    self.rejectMessage(getTranslation("others.rejectMSG"));
                    


                }
                initTranslations();
                //Call Update EIT function 
                  function oprationServiceRecord() {
                      if(Object.keys(self.model).indexOf("approvedBy")!=-1){
                          self.model.approvedBy=ko.observable(rootViewModel.personDetails().personNumber);
                      }else{
                         //No Approved By Segment
                      }
                    
                    var jsonData = ko.toJSON(self.model);
                    // var cx = JSON.stringify(jsonData);
                    self.model.url = self.setUrl(self.deptArray()[self.rwindex()].SELF_TYPE);
                    self.approvlModel.code = self.deptArray()[self.rwindex()].SELF_TYPE;
                    if(self.isLastApprover()){
                         self.approvlModel.status = "APPROVED";
                    }else{
                          self.approvlModel.status = "PENDING_APPROVED";
                    }
                    self.approvlModel.person_number = self.deptArray()[self.rwindex()].person_number;
                    self.approvlModel.person_id =self.deptArray()[self.rwindex()].person_id;
                    self.approvlModel.created_by = self.deptArray()[self.rwindex()].person_id;
//                    self.approvlModel.managerName = self.personDetails().managerName;
//                    self.approvlModel.managerOfManagerName = self.personDetails().managerOfMnagerName;
                    //self.approvlModel.creation_date = self.personDetails().personId;
                    self.approvlModel.line_manager = globalRowData.line_manager;
                    self.approvlModel.manage_of_manager =globalRowData.manage_of_manager;
                    self.approvlModel.eit = JSON.stringify(jsonData);
                   // self.approvlModel.personName = ;
                   // self.approvlModel.nationalIdentity = self.personDetails().nationalId;
                   // self.approvlModel.eitLbl = JSON.stringify(self.buildModelWithLabel());
                    self.approvlModel.eit_name =self.deptArray()[self.rwindex()].SELF_TYPE;
                    self.approvlModel.id= self.deptArray()[self.rwindex()].TRS_ID;
        
                    
                    for (var eitModelIndex = 0; eitModelIndex < tempObject.length; eitModelIndex++) {
                        self.approvlModel[tempObject[eitModelIndex].APPLICATION_COLUMN_NAME] = self.model[tempObject[eitModelIndex].DESCRIPTION]();
                    }
                   
                    
                    var jsonData = ko.toJSON(self.approvlModel);
                    var getUpdateExtraInformationCBFN = function (data) {            
                        //
                    };
                
                   var approvalCondetion = app.getApprovalCondetion( self.EitCode,self.model) ; 
                    services.addGeneric("PeopleExtra/createOrUpdateEIT/" + 'EDIT'+'/'+approvalCondetion, jsonData).then(getUpdateExtraInformationCBFN, app.failCbFn);
                };
                //End 
                
                
                
                self.approveProbationRequst = function () {
                    var pNumber=self.deptArray()[self.rwindex()].person_number;
                    var transactionId=self.deptArray()[self.rwindex()].TRS_ID;
                    var headers = {
                        "MSG_TITLE": self.deptArray()[self.rwindex()].MSG_TITLE,
                        "MSG_BODY": self.deptArray()[self.rwindex()].MSG_BODY,
                        "TRS_ID": self.deptArray()[self.rwindex()].TRS_ID,
                        "PERSON_ID": rootViewModel.personDetails().personId,
                        "RESPONSE_CODE": "APPROVED",
                        "ssType": self.deptArray()[self.rwindex()].SELF_TYPE ,
                        "PERSON_NAME":self.deptArray()[self.rwindex()].personName,
                        "rejectReason":" ",
                        "workflowId":self.deptArray()[self.rwindex()].id
                    };
                    //self.deptArray()[self.rwindex()]person_number
                    var approvalActionCBFN = function (data) {
                        //                     
                        if (self.isLastApprover()) {

                            var payload = {
                                "status": "Approval",
                                "id": self.deptArray()[self.rwindex()].TRS_ID
                            };

                            var updateStatusCBFN = function (data) {

                            };

                            services.addGeneric("ProbationPerformanceEvaluation/UpdateStatus/", payload).then(updateStatusCBFN, app.failCbFn);

                            var modelXX = {
                                url: self.validationModle.url(),
                                effectiveDate: getSysDate(),
                                employeeNumber: pNumber,
                                finalDecision: self.validationModle.finalDecisionVal()

                            };

                            var jsonData = {model: modelXX, TRS_ID: transactionId, eitCode: 'XXX_HR_PROBATION_PERFORMANCE'}
                            var supmitEFFCBFN = function (data) {
                                var Data = data;
                                // self.disableSubmit(false);
                                if (oj.Router.rootInstance._navHistory.length > 1) {
                                    oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                                } else {
                                    oj.Router.rootInstance.go('home');
                                }

                            };

                            services.addGeneric("eff/" + "ADD" + "/" + pNumber, JSON.stringify(jsonData)).then(supmitEFFCBFN, app.failCbFn);


                        } else {
                            app.notificationsCount();
                            self.getAllNotifi();
                            document.querySelector("#yesNoDialog").close();
                        }
                         document.querySelector("#yesNoDialog").close();
                        // oj.Router.rootInstance.go('notificationScreen');
                    };
                    //services.workflowAction("workflowApproval/", headers).then(approvalActionCBFN, app.failCbFn)
                    services.workflowAction("workflowApproval/", headers).then(app.failCbFn, approvalActionCBFN)
                };   
                
                function getSysDate() {
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd;
                    }

                    if (mm < 10) {
                        mm = '0' + mm;
                    }

                    today = dd + '-' + mm + '-' + yyyy;
                    return   yyyy + '-' + mm + '-' + dd;
                }
                 self.summaryObservableArray = ko.observableArray();
                self.getAllPropationAndEvaluation = function (id) {

                    self.summaryObservableArray([]);

                    var getReportCbFn = function (data) {

                        if (data.length !== 0)
                        {


                            self.validationModle = {
                                radioCommitmentAnddisciplineq1: ko.observable(data[0].commitmentanddisciplineq1),
                                letterNameVal: ko.observable(data[0].letterName),
                                radioCommitmentAnddisciplineq2: ko.observable(data[0].commitmentanddisciplineq2),
                                radioCommitmentAnddisciplineq3: ko.observable(data[0].commitmentanddisciplineq3),
                                radioCommitmentAnddisciplineq4: ko.observable(data[0].commitmentanddisciplineq4),
                                radioCommitmentAnddisciplineq5: ko.observable(data[0].commitmentanddisciplineq5),
                                appearanceAndbehavior1: ko.observable(data[0].appearanceandbehavior1),
                                appearanceAndbehavior2: ko.observable(data[0].appearanceandbehavior2),
                                appearanceAndbehavior3: ko.observable(data[0].appearanceandbehavior3),
                                appearanceAndbehavior4: ko.observable(data[0].appearanceandbehavior4),
                                appearanceAndbehavior5: ko.observable(data[0].appearanceandbehavior5),
                                InteractingWithothers1: ko.observable(data[0].interactionwithothers1),
                                InteractingWithothers2: ko.observable(data[0].interactionwithothers2),
                                InteractingWithothers3: ko.observable(data[0].interactionwithothers3),
                                InteractingWithothers4: ko.observable(data[0].interactionwithothers4),
                                InteractingWithothers5: ko.observable(data[0].interactionwithothers5),
                                qualityOfcommunication1: ko.observable(data[0].qualityofcommunication1),
                                qualityOfcommunication2: ko.observable(data[0].qualityofcommunication2),
                                qualityOfcommunication3: ko.observable(data[0].qualityofcommunication3),
                                qualityOfcommunication4: ko.observable(data[0].qualityofcommunication4),
                                qualityOfcommunication5: ko.observable(data[0].qualityofcommunication5),
                                finalDecisionVal: ko.observable(data[0].finaldecision),
                                DirectManagerVal: ko.observable(data[0].finalEvaluationSummary),
                                url: ko.observable(data[0].url)

                            };



                        } else {
//                            $.notify(self.checkResultSearch(), "error");
                        }



                    };
                    var failCbFn = function () {
//                        $.notify(self.checkResultSearch(), "error");
                    };
                    var serviceName = commonUtil.getprobationAndEvaluation + "/" + id;
                    services.getAllSummary(serviceName).then(getReportCbFn, failCbFn);

                };
            }
            return JobContentViewModel;
        });
