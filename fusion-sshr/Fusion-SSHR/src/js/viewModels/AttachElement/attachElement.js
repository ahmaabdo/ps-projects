/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin','ojs/ojtable',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function AttachElementContentViewModel() {
                var self = this;
                self.monthArr = ko.observableArray([{label:"JAN",value:"JAN"}, {label:"FEB",value:'FEB'}, 
                    {label:"MAR",value:"MAR"},{ label:"APR",value:'APR'}, {label:"MAY",value:'MAY'}, 
                    {label:"JUN",value:'JUN'}, {label:"JUL",value:'JUL'},
                    {label:"AUG",value:'AUG'},{ label:"SEP",value:'SEP'}, {label:"OCT",value:'OCT'},
                    {label:"NOV",value:'NOV'}, {label:"DEC",value:'DEC'}]);
                self.columnArray = ko.observableArray();
                self.monthVal=ko.observable();
                self.addSummaryLbl = ko.observable();
                self.editSummaryLbl = ko.observable();
                self.viewSummaryLbl = ko.observable();
                self.eitCodeLbl = ko.observable();
                self.creationDateLbl = ko.observable();
                self.statusLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.transActionIdLbl = ko.observable();
                self.eitLbl=ko.observable();
                self.backActionLbl = ko.observable();
                self.DeleteSummaryLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                var deptArray = [];
                self.summaryObservableArray = ko.observableArray(deptArray);
                self.allElementArr = ko.observableArray([]);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'}));
                self.isDisable = ko.observable(true);
                self.dataSource;
                self.checkResultSearch = ko.observable();

                self.btnAddSummary = function () {
                    app.loading(true);
                    oj.Router.rootInstance.store('ADD');
                    oj.Router.rootInstance.go('reportSummary');
                };



                self.tableSelectionListener = function () {
                    self.isDisable(false);

                };





//////////////////////////////


                self.reportName = ko.observable();
                self.search = ko.observable();
                self.clear = ko.observable();
                self.getAllLbl = ko.observable();


                self.reportSearch = {
                    reportname: ko.observable()

                };
                var el = document.getElementById("searchName");

                self.getSaaSAttachedElement = function () {
                    var response;
                    var getReportCBCF = function (datac) {
                        response = datac;
                    };
                    var xx = {"reportName": "PAAS_EE_Transaction_ID"};

                    services.getGenericReportNotAsync(xx).then(getReportCBCF, self.failCbFn);
                    return response;
                };
                
                
                self.searchElementFile = function () {
                    var attachedElementId = self.getSaaSAttachedElement();
                    var mode = Math.ceil(attachedElementId.length / 1000);
                    var internalloopItt = 0;
                    var q = "AND ( ID NOT  IN  ";
                    for (var i = 1; i <= mode; i++) {
                        q = q + "("
                        for (internalloopItt; (internalloopItt < (i * 1000) && internalloopItt < attachedElementId.length); internalloopItt++) {
                            attachedElementId[internalloopItt].TRANSACTION_ID_value = attachedElementId[internalloopItt].TRANSACTION_ID_value.substr(1);
                            attachedElementId[internalloopItt].TRANSACTION_ID_value = attachedElementId[internalloopItt].TRANSACTION_ID_value.replace("Noor", "");
                            if (internalloopItt == ((i * 1000) - 1)) {
                                q = q + attachedElementId[internalloopItt].TRANSACTION_ID_value;
                            } else if (internalloopItt == (attachedElementId.length - 1)) {
                                q = q + attachedElementId[internalloopItt].TRANSACTION_ID_value;
                            } else {
                                q = q + attachedElementId[internalloopItt].TRANSACTION_ID_value + ",";
                            }

                        }
                        if (i == mode) {
                            q = q + ") ";
                        } else {
                            q = q + ")  And  ID NOT IN";
                        }
                    }
                    q = q + ")";
                    var boody = {"Q": q};
                    var getNotAttachedElement = function (data) {
                        self.summaryObservableArray([]);
                        
                       if (data.length !== 0)
                        {

                            $.each(data, function (index) {

                                self.summaryObservableArray.push({
                                    id: [data[index].id],
                                    code: [data[index].code],
                                    creation_date: [data[index].creation_date],
                                    eit: [data[index].eit],
                                    person_number: [data[index].person_number],
                                    status: [data[index].status]    ,
                                    person_id:data[index].person_id 
                                });
                                
                                
                            });
                             self.allElementArr( self.summaryObservableArray());
                        } else {
                            $.notify(self.checkResultSearch(), "error");
                        }
                    }
                    var jsonData = ko.toJSON(boody);
                    services.addGeneric("PeopleExtra/getNotAttachedElement/", jsonData).then(getNotAttachedElement, self.failCbFn);
                };
                
                self.searchNotAttachedElementDate=function(){
                    self.summaryObservableArray([]);
                    var month='OCT';
                    
                    
                    
                    for(var i=0;i<self.allElementArr().length;i++){
                        if(self.allElementArr()[i].creation_date.toString().includes( self.monthVal())){
                            self.summaryObservableArray.push(self.allElementArr()[i]);
                        }
                         
                    }
                    
                };
                self.getAllNotAttachedElement = function () {

                   self.searchElementFile();
                };

                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                    self.reportSearch.reportname("");
                    self.summaryObservableArray([]);
                }

                self.checkResultSearch = ko.observable();

                ////////////////////

                 self.btnAddAction=function(){
                  document.querySelector("#yesNoDialog").open();
                };
                self.cancelButtonDelete = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('/');
                };
                self.handleAttached = function (info) {
              var date = new Date();
              var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
              var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    app.loading(false);
                   self.searchElementFile();
                    
                };

                 self.backAction=function(){
                   oj.Router.rootInstance.go('setup');  
                };

                 self.commitRecordDelete=function(){
                     var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                   var id = self.summaryObservableArray()[currentRow['rowIndex']].id;
                   var deleteReportCBF=function(data){
                      
                       document.querySelector("#yesNoDialog").close();
                   };
                   
                  services.getGeneric("summary/deleteReport/" + id, {}).then(deleteReportCBF, app.failCbFn);
                };
                 self.cancelButtonDelete=function(){
                  document.querySelector("#yesNoDialog").close();
                };
                
                
                
                
                
 
                self.getElementFile = function () {
                    var ElementSetup;
                    var inputArray;
                    var elementSetup=[];
                    var getAllElmentCBF = function (data) {
                       for(var i=0;i<data.length;i++){
                         if(data[i].typeOfAction=='ElementEntry')  {
                             elementSetup.push(data[i]);
                         }
                       }
                        inputArray = JSON.parse(data[0].inputValue);
//                        ElementSetup = data;
                    };
                    services.getGeneric("elementEntrySetup/getAllElements/").then(getAllElmentCBF, self.failCbFn);
                  
                    var date = new Date();
              var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
              var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                     var date = new Date();
              var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
              var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var EffectiveStartDate = self.formatDate (firstDay);
                    var EffectiveEndDate = self.formatDate(lastDay);
                    var ElementName = "Overtime Payment";
                    var LegislativeDataGroupName = "SA Legislative Data Group";
                    var EntryType = "E";
                    var CreatorType = "F";
                    var ELEMENT_HEADER =
                            "METADATA|ElementEntry|SourceSystemOwner|SourceSystemId|EffectiveStartDate|EffectiveEndDate|ElementName|LegislativeDataGroupName|AssignmentNumber|EntryType|CreatorType" + "\n";
                    var ELEMENT_LINE =
                            "METADATA|ElementEntryValue|SourceSystemOwner|SourceSystemId|ElementEntryId(SourceSystemId)|EffectiveStartDate|EffectiveEndDate|ElementName|LegislativeDataGroupName|AssignmentNumber|InputValueName|ScreenEntryValue"+ "\n";
                    for (var x = 0; x < self.summaryObservableArray().length; x++) {
                        if (self.summaryObservableArray()[x].eit) {
                            var obj = JSON.parse(self.summaryObservableArray()[x].eit);
                            var sourceSystemId = "PaaS_" + self.summaryObservableArray()[x].id;
                            var AssignmentNumber = "E" + self.summaryObservableArray()[x].person_number;
                            ELEMENT_HEADER = ELEMENT_HEADER + "MERGE|ElementEntry|" + "HHA" + "|" +
                                    sourceSystemId + "|" + EffectiveStartDate + "|" +
                                    EffectiveEndDate + "|" + getElementName(elementSetup,self.summaryObservableArray()[x]) + "|" +
                                    LegislativeDataGroupName + "|" + AssignmentNumber + "|" +
                                    EntryType + "|" + CreatorType + "\n";
                            var inputArray =getElementInputs(elementSetup,self.summaryObservableArray()[x]);
                            for (var z = 0; z < inputArray.length; z++) {
                                if (obj[inputArray[z].inputEitValue]) {
                                    var lineSourceSystemOwner = self.summaryObservableArray()[x].person_number + "_" + self.summaryObservableArray()[x].code + "_" + self.summaryObservableArray()[x].id + "_" + inputArray[z].inputEitValue;


                                    ELEMENT_LINE = ELEMENT_LINE + "MERGE|ElementEntryValue|" +
                                            "HHA" + "|" +
                                            lineSourceSystemOwner +
                                            "|" + sourceSystemId + "|" +
                                            EffectiveStartDate + "|" +
                                            EffectiveEndDate + "|" +
                                            getElementName(elementSetup,self.summaryObservableArray()[x]) + "|" +
                                            LegislativeDataGroupName +
                                            "|" + AssignmentNumber + "|" +
                                            inputArray[z].inputValueLbl +
                                            "|" +
                                            obj[inputArray[z].inputEitValue] + "\n";
                                } else if (inputArray[z].inputEitValue == "Transaction_ID") {
                                    var lineSourceSystemOwner = self.summaryObservableArray()[x].person_number + "_" + self.summaryObservableArray()[x].code + "_" + self.summaryObservableArray()[x].id + "_" + inputArray[z].inputEitValue;
                                    ELEMENT_LINE = ELEMENT_LINE + "MERGE|ElementEntryValue|" +
                                            "HHA" + "|" +
                                            lineSourceSystemOwner +
                                            "|" + sourceSystemId + "|" +
                                            EffectiveStartDate + "|" +
                                            EffectiveEndDate + "|" +
                                            getElementName(elementSetup,self.summaryObservableArray()[x])  + "|" +
                                            LegislativeDataGroupName +
                                            "|" + AssignmentNumber + "|" +
                                            inputArray[z].inputValueLbl +
                                            "|" +
                                            "P" + self.summaryObservableArray()[x].id + "\n";
                                } else {
                                    var lineSourceSystemOwner =self.summaryObservableArray()[x].person_number + "_" + self.summaryObservableArray()[x].code + "_" + self.summaryObservableArray()[x].id + "_" + inputArray[z].inputEitValue;
                                    var segmentName = inputArray[z].inputEitValue;
//                                callReportWhenField("XXX_HR_OVERTIME_PAYMENT",obj[inputArray[z].inputEitValue,123);
                                    var val = callReportWhenField(self.summaryObservableArray()[x], inputArray[z].inputEitValue);
                                    if (val) {
                                        ELEMENT_LINE = ELEMENT_LINE + "MERGE|ElementEntryValue|" +
                                                "HHA" + "|" +
                                                lineSourceSystemOwner +
                                                "|" + sourceSystemId + "|" +
                                                EffectiveStartDate + "|" +
                                                EffectiveEndDate + "|" +
                                                ElementName + "|" +
                                                LegislativeDataGroupName +
                                                "|" + AssignmentNumber + "|" +
                                                inputArray[z].inputValueLbl +
                                                "|" +
                                                val + "\n";
                                    }

                                }
                            }
                       }
                    }

//
                    var datFile=ELEMENT_HEADER + ELEMENT_LINE;
                     var jsonBody = {
                         effivtiveStartDate:"%EffectiveStartDate%",
                         DAT:datFile,
                         personNumber:app.personDetails().personNumber,
                         eitCode:"Attach Element"                      
                     };
                    var submitElement = function (data1) {
                        clearContent();
                        document.querySelector("#yesNoDialog").close();
                    };
                    
                    services.submitElementEntryBulk(JSON.stringify(jsonBody)).then(submitElement, app.failCbFn);
                };
                function getElementName(elementSetup,record){
                    var elementName;
                    for(var t=0;t<elementSetup.length;t++){
                        if(elementSetup[t].eitCode==record.code){
                            elementName=elementSetup[t].elementName;
                        }
                    }
                    return elementName;
                }
                function getElementInputs(elementSetup, record) {
                    var elementInputs;
                    for (var t = 0; t < elementSetup.length; t++) {
                        if (elementSetup[t].eitCode == record.code) {
                            
                            elementInputs  = JSON.parse(elementSetup[t].inputValue) ;
                        }
                    }
                    return elementInputs;
                }
                
                
                function getSegment(allSegment,segmentName){
                    var segment;
                    for(var bb=0;bb<allSegment.length;bb++){
                        if(allSegment[bb].DESCRIPTION==segmentName){
                            segment=allSegment[bb];
                        }
                    }
                    return segment;
                }
                function buildPaylod(eit,reportName,parameter,personId){
                    var paylod={"reportName":reportName};                    
                    eit=JSON.parse(eit);
                    for(var tt=0;tt<parameter.length;tt++){
                        if(parameter[tt]!='personId'){
                             paylod[parameter[tt]]=eit[parameter[tt]];
                        }else{
                            paylod[parameter[tt]]=personId;
                        }
                       
                    }
                    return paylod;
                }
                function callReportWhenField(eitCode,segmentName  ){
                  var allSegment =   app.getEITSegments(eitCode.code);
                  var obj = JSON.parse(eitCode.eit);
                  var missingSegment= getSegment(allSegment,segmentName);
                    if (missingSegment) {
                        if (missingSegment.IN_FIELD_HELP_TEXT && missingSegment.IN_FIELD_HELP_TEXT != "PAAS") {
                          var parameter= searchArrayContainValue(missingSegment.IN_FIELD_HELP_TEXT, app.globalEITDefultValueParamaterLookup());
                          if(parameter[0]){
                              parameter=parameter[0].split(',');
                          }else{
                              parameter=[];
                          }                                             
                            var paylod =buildPaylod(obj,missingSegment.IN_FIELD_HELP_TEXT ,parameter,eitCode.person_id);
                            return callAnyReport(paylod);
                        } else if (missingSegment.DEFAULT_value && missingSegment.DEFAULT_value != "PAAS" && missingSegment.DEFAULT_TYPE != 'C') {
                           var paylod = {"reportName": 'DynamicValidationReport', "str": missingSegment.DEFAULT_value};
                           return callAnyReport(paylod);
                        } else if (missingSegment.DEFAULT_TYPE == 'C') {
                            return missingSegment.DEFAULT_value;                           
                        } else if (missingSegment.IN_FIELD_HELP_TEXT == "PAAS") {
                        }

                    }
                  var  paylod;
//                  getReportName
//                  if(segmentName=="totalPaymentHours"){
//                      //Overtime_Total_Assigned_Report
//                       paylod= {"reportName": "Overtime_Total_Assigned_Report", "overtimeAssignmentDetails":overtimeAssignmentDetails};
//                      //overtimeAssignmentDetails
//                      return callAnyReport(paylod);
//                  }else if(segmentName=="overtimeStartDate"){
//                      //Overtime_Start_Date_Report
//                      paylod= {"reportName": "Overtime_Start_Date_Report", "overtimeAssignmentDetails":overtimeAssignmentDetails};
//                      //overtimeAssignmentDetails
//                      return callAnyReport(paylod);
//                      
//                  }else if (segmentName=="overtimeEndDate"){
//                      //Overtime_End_Date_Report
//                    paylod= {"reportName": "Overtime_End_Date_Report", "overtimeAssignmentDetails":overtimeAssignmentDetails};
//                       //overtimeAssignmentDetails
//                      return  callAnyReport(paylod);
//                  }else if (segmentName=="totalActualHours"){
//                      return 1 ; 
//                  }
                  
                  
                  
                }
                function callAnyReport (paylod){
                    var res ; 
                    var getReportValidationCBCF = function (dataSaaSQuery) {
                        var resultOfQueryObj = dataSaaSQuery;
                        res = (resultOfQueryObj[ Object.keys(dataSaaSQuery)[0]]);

                    };
                    services.getGenericReportNotAsync(paylod).then(getReportValidationCBCF, self.failCbFn);
                    return res;
                }
                



                var getTranslation = oj.Translations.getTranslatedString;

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();

                    }

                });
                function initTranslations() {
                    self.search(getTranslation("others.search"));
                    self.clear(getTranslation("others.clear"));
                    self.addSummaryLbl(getTranslation("report.addSummary"));
                    self.editSummaryLbl(getTranslation("report.editSummary"));
                    self.viewSummaryLbl(getTranslation("report.viewSummary"));
                    self.eitCodeLbl(getTranslation("attachElement.Eit_Code"));
                    self.personNumberLbl(getTranslation("attachElement.PersonNumber"));
                    self.statusLbl(getTranslation("attachElement.status"));
                    self.transActionIdLbl(getTranslation("attachElement.transActionId"));
                    self.creationDateLbl(getTranslation("attachElement.creation_date"));
                    self.eitLbl(getTranslation("attachElement.EIT"));
                    self.getAllLbl(getTranslation("attachElement.getAll"));
                    
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));
                    self.backActionLbl(getTranslation("approvalScreen.backAction"));
                    self.DeleteSummaryLbl(getTranslation("others.delete"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessage(getTranslation("attachElement.oprationMessage"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.columnArray([
                        {
                            "headerText": self.transActionIdLbl(), "field": "id"
                        },
                        {
                            "headerText": self.creationDateLbl(), "field": "creation_date"
                        },
                        {
                            "headerText": self.eitCodeLbl(), "field": "code"
                        },
                        {
                            "headerText": self.personNumberLbl(), "field": "person_number"
                        },
                        {
                            "headerText": self.statusLbl(), "field": "status"
                        },
                        {
                            "headerText": self.eitLbl(), "field": "eit"
                        }
                    ]);
                }
                initTranslations();
            }
            return AttachElementContentViewModel;
        });
