/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * baseScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojtrain', 'config/services',
    'util/commonhelper', 'knockout-mapping', 'ojs/ojmodel', 'ojs/ojknockout',
    'ojs/ojknockout-model', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox',
    'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup',
    'ojs/ojknockout-validation', 'ojs/ojfilmstrip', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset',
    'ojs/ojradioset', 'ojs/ojlabel', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'jquery-calendar-picker',
    'ojs/ojinputtext', 'ojs/ojavatar'],
        function (oj, ko, $, app, ojtrain, services, commonhelper, km) {
    /**
     * The view model for the main content view template
     */
    function baseScreenContentViewModel() {
        var self = this;
        
        self.valueofsharethouts = ko.observable("Share thoughts with colleagues..");
                self.agreement = ko.observableArray();
                self.currentColor = ko.observable("red");
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;
                self.empName = ko.observable();
                self.nam = ko.observable();
                self.trackRequestLbl = ko.observable();
                self.reassignLbl = ko.observable();
                self.countPending = ko.observable();
                self.countApproved = ko.observable();
                self.countRejected = ko.observable();
                self.avatarSize = ko.observable("md");
                self.firstName = ko.observable();
                self.lastName = ko.observable();
                self.initials = ko.observable();
                self.delegationRequestLbl = ko.observable();
                self.image = ko.observable(); 

                self.error = function () {
                    app.hasErrorOccured(true);
                    app.router.go("error");
                };



                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
//                app.showPreloader();
                        initTranslations();
//                app.hidePreloader();
                    }
                });
                self.icons = ko.observableArray([]);

                self.computeIconsArray = ko.computed(function () {
                    if (rootViewModel.personDetails()) {
                        self.firstName(app.personDetails().firstName);
                    self.lastName(app.personDetails().lastName);
                    self.initials(oj.IntlConverterUtils.getInitials(self.firstName(),self.lastName()));
                        self.icons([
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'trackRequestScreen', iconType: 'fa fa-briefcase', text: self.trackRequestLbl(), visible: true, code: "Job"
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'reassignRequestScreen', iconType: 'fa fa-file-pdf-o', text: self.reassignLbl(), visible: true, code: "Report",
                            },
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'delegationSummary', iconType: 'fa fa-file-pdf-o', text: self.delegationRequestLbl(), visible: true, code: "delegation"
                            }
                        ]);


                    }
                });

                self.pagingModel = null;

                self.loading = function () {
                    app.loading(true);
                };
                
                self.iconNavigation = function (router, code) {
                    app.loading(true);
                    oj.Router.rootInstance.store(code);
                    oj.Router.rootInstance.go(router);
                    return true;
                };

                getPagingModel = function () {
                    if (!self.pagingModel) {
                        var filmStrip = $("#filmStrip");
                        var pagingModel = filmStrip.ojFilmStrip("getPagingModel");
                        self.pagingModel = pagingModel;
                    }
                    return self.pagingModel;
                };
                self.handleAttached = function (info) {
                    app.loading(false);
                    initTranslations();
                    if (app.conutEitRequest()) {
                            self.countPending(app.conutEitRequest().countPendingApproved);
                            self.countApproved(app.conutEitRequest().countApproved);
                            self.countRejected(app.conutEitRequest().countRejected);
                        }
                     if(app.personDetails().picBase64===undefined ||app.personDetails().picBase64 ===null || app.personDetails().picBase64 ===""){
                        self.image('');
                          }else{
                       self.image("data:image/png;base64," + app.personDetails().picBase64);
                         
                   }         
                };

                function initTranslations() {
                    // function to add translations for the home page module
                    if (rootViewModel.personDetails())
                        self.empName(rootViewModel.personDetails().displayName);
                    self.nam(getTranslation("common.name"));
                    self.trackRequestLbl(getTranslation("pages.trackRequest"));
                    self.reassignLbl(getTranslation("pages.reassignRequest"));
                    self.delegationRequestLbl(getTranslation("pages.delegationRequest"));
                }
    }
    
    return baseScreenContentViewModel;
});
