/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * reassignRequestScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper', 'config/services',
    'ojs/ojtable',
    'config/buildScreen', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojknockout-validation', 'ojs/ojlabel', 'ojs/ojtable'],
        function (oj, ko, $, app, commonUtil, services, buildScreen, ArrayDataProvider) {
            /**
             * The view model for the main content view template
             */
            function reassignRequestScreenContentViewModel() {
                var self = this;

                // Start Declare All Observable
                var getTranslation = oj.Translations.getTranslatedString;
                self.messages = ko.observable();
                self.reassignRequestLbl = ko.observable();
                self.oprationdisabled = ko.observable(true);
                self.oprationdisabledApprovalList = ko.observable(true);
                self.reassignLbl = ko.observable();
                self.unknownErrMsg = ko.observable();
                self.columnArray = ko.observableArray([]);
                self.columnArrayApprovalList = ko.observableArray([]);
                self.selectedRowKey = ko.observable();
                self.selectedIndex = ko.observable();
                self.statusLbl = ko.observable();
                self.summaryObservableArray = ko.observableArray([]);
                self.approvalListArray = ko.observableArray([]);
                self.eitNameLbl = ko.observable();
                self.personNameLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.roleNameLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.deleteMessageLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.statusDataLbl = ko.observable();
                self.searchLbl = ko.observable();
                self.clearLbl = ko.observable();
                self.notificationTypeLbl = ko.observable();
                // self.personNumberSelected = ko.observable();
                self.creationDate = ko.observable();
                self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'id'})));
                self.dataSourceApprovalList = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.approvalListArray, {idAttribute: 'id'})));
                self.personNameArrForApproval = ko.observableArray([]);
                self.eitPlaceHolder = ko.observable();
                self.EitName = ko.observableArray([]);
                self.EitName(app.globalEitNameReport());
                self.roleIdLbl = ko.observable();
                self.roleTypeLbl = ko.observable();
                self.statusLbl = ko.observable();
                self.pendingApprovalLbl = ko.observable();
                self.withdrawLbl = ko.observable();
                self.rejectedLbl = ko.observable();
                self.approveLbl = ko.observable();
                self.submitLbl = ko.observable();

                // End Declare All Observable

                // Start Declare Model               
                self.searchModel = {
                    person_number: ko.observable(""),
                    code: ko.observable(""),
                    status: ko.observable("")
                };
                //End Declare Model

                // Start Function Handle attached And handle Detached Run On open Screen and Close Screen
                self.handleAttached = function (info) {
                    app.loading(false);
                    self.getEmpNameById();
                };
                self.handleDetached = function (info) {

                };
                // End Function Handle attached And handle Detached Run On open Screen and Close Screen

                // Start Funcation Calling for Select row from Table
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;

                    if (currentRow != null) {
                        self.selectedRowKey(currentRow['rowKey']);
                        self.selectedIndex(currentRow['rowIndex']);
                        // self.personNumberSelected(self.summaryObservableArray()[self.selectedIndex()].person_number);
                        self.oprationdisabled(false);
                        // console.log(currentRow)
                    } else {
                        self.oprationdisabled(true);

                    }


                };
                // End Funcation Calling for Select row from Table

                // Start Funcation Calling for Select row from Table Approval List
                self.tableApprovalListSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    if (currentRow != null) {
                        self.selectedRowKey(currentRow['rowKey']);
                        self.selectedIndex(currentRow['rowIndex']);
                        if (self.approvalListArray()[self.selectedIndex()].responseCode == "PENDING APPROVAL") {
                            self.oprationdisabledApprovalList(false);
                        } else {
                            self.oprationdisabledApprovalList(true);
                        }
                    }
                };
                // End Funcation Calling for Select row from Table Approval List
                // Start Function Get All Eit Self Service

                self.getSelfServiceRequestByPersonNumberOrEIT = function () {
                    var jsonData = ko.toJSON(self.searchModel);
                    self.summaryObservableArray([]);
                    var getSelfServiceRequestByPersonNumberCBF = function (data) {
                        //console.log(data);
                        $("#searchBtn").removeClass("loading");
                        self.summaryObservableArray([]);
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].status === "PENDING_APPROVED" || data[i].status === "Pending Approval") {
                                self.statusDataLbl("Pending Approved");
                                var obj = {};
                                obj.id = data[i].id;
                                obj.created_by = data[i].creation_date;
                                obj.person_id = data[i].person_id;
                                obj.status = data[i].status;
                                obj.reference_num = data[i].reference_num;
                                obj.eit_name = data[i].eit_name;
                                obj.person_number = data[i].person_number;
                                obj.codeService = data[i].code;
                                for (var j = 0; j < app.globalEitNameReport().length; j++) {
                                    if (app.globalEitNameReport()[j].value == data[i].code) {
                                        obj.code = app.globalEitNameReport()[j].label;
                                    }
                                }

                                obj.line_manager = data[i].line_manager;
                                obj.personName = data[i].personName;
                                obj.statusDataLbl = self.statusDataLbl();
                                self.summaryObservableArray.push(obj);
                            }
                        }

                    };
                    var failCbFn = function () {
                        self.messages([{
                                severity: 'error',
                                summary: self.unknownErrMsg(),
                                autoTimeout: 5000
                            }]);
                        $("#searchBtn").removeClass("loading");

                    };
                    $("#searchBtn").addClass("loading");

                    services.addGeneric("reassignRequest/getEit", jsonData).then(getSelfServiceRequestByPersonNumberCBF, failCbFn);

                };
                // End Function Get All Eit Self Service

                self.searchPerson = function () {
                    self.getSelfServiceRequestByPersonNumberOrEIT();

                };

                self.reset = function () {
                    clearContent();
                    self.summaryObservableArray([]);
                };

                function clearContent() {

                    self.searchModel.person_number("");
                    self.searchModel.code("");
                    self.oprationdisabled(true);

                }

                // Start Button Open And Close Popup
                self.btnReAssignSelfServiceRequest = function () {
                    //document.querySelector("#NoDialog").open();
                    self.workflow_Approval();
                };

                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                };
                // End Button Open And Close Popup

                // Start Button Submit Delete Eit Self Service
                self.SubmitButtonDialog = function () {
                    self.approvalListArray()[self.selectedIndex()].type = "EDIT";
                    // self.approvalListArray()[self.selectedIndex()].personNumber = self.personNumberSelected();
                    oj.Router.rootInstance.store(self.approvalListArray()[self.selectedIndex()]);
                    oj.Router.rootInstance.go('reassignRequestOperation');
                };
                // End Button Submit Delete Eit Self Service

                // function to get all employees details
                self.workflow_Approval = async function () {
//                    var id = self.summaryObservableArray()[self.selectedIndex()].id;


                    var workflowApprovalCBF = function (data) {
                       //console.log(data);
                        self.approvalListArray([]);
                        for (var i = 0; i < data.length; i++) {
                            var obj = {};
                            if (data[i].statuworkflow_ApprovalCBFs === "PENDING_APPROVED") {
                                self.statusDataLbl("Pending Approved");
                            }

                            if (data[i].hasOwnProperty('responseCode')) {
                                obj.responseCode = data[i].responseCode;
                                if (data[i].responseCode === "APPROVED") {
                                    obj.responseCode = self.approveLbl();
                                } else if (data[i].responseCode === "REJECTED") {
                                    obj.responseCode = self.rejectedLbl();
                                } else if (data[i].responseCode === "Withdraw") {
                                    obj.responseCode = self.withdrawLbl();
                                } else if (data[i].responseCode === "SUBMIT") {
                                    obj.responseCode = self.submitLbl();
                                }
                            } else {
                                obj.responseCode = self.pendingApprovalLbl();
                            }

                            obj.id = data[i].id;
                            obj.notificationType = data[i].notificationType;
                            obj.responseDate = data[i].responseDate;
                            obj.roleId = data[i].roleId;
                            obj.rolrType = data[i].rolrType;
                            obj.serviceType = self.summaryObservableArray()[idOfRow].code;

                            if (data[i].rolrType == "ROLES") {
                                for (var x = 0; x < app.rolesOption().length; x++) {
                                    if (app.rolesOption()[x].value == data[i].roleId) {
                                        obj.roleNameLbl = app.rolesOption()[x].label;

                                    }
                                }
                            } else if (data[i].rolrType == "POSITION") {
                                for (var c = 0; c < app.roleOptionType().length; c++) {
                                    if (app.roleOptionType()[c].value == data[i].roleId) {
                                        obj.roleNameLbl = app.roleOptionType()[c].label;
                                    }
                                }

                            } else if (data[i].rolrType == "LINE_MANAGER" || data[i].rolrType == "LINE_MANAGER+1" || data[i].rolrType == "EMP") {
                                for (var x = 0; x < self.personNameArrForApproval().length; x++) {
                                    if (data[i].roleId == self.personNameArrForApproval()[x].value) {
                                        obj.roleNameLbl = self.personNameArrForApproval()[x].label;
                                    }
                                }
                            }

                            obj.stepLeval = data[i].stepLeval;
                            obj.transActionId = data[i].transActionId;
                            obj.code = eitcode;
                            obj.reasonforReassign = data[i].reasonforReassign;
                            self.approvalListArray.push(obj);

                        }
                    };
                    var element = document.getElementById('tableDetails');
                    var currentRow = element.currentRow;

                    var type_id = currentRow.rowKey;
                    var idOfRow;
                    for (var k = 0; k < self.summaryObservableArray().length; k++) {
                        if (type_id == self.summaryObservableArray()[k].id) {
                            idOfRow = k;
                            break;
                        }
                    }
                    var eitcode;
                    for (var j = 0; j < app.globalEitNameReport().length; j++) {
                        if (app.globalEitNameReport()[j].label == self.summaryObservableArray()[idOfRow].code) {
                            eitcode = app.globalEitNameReport()[j].value;
                        }
                    }
                    // console.log(self.summaryObservableArray()[idOfRow]);
                    await services.getGenericAsync("approvalList/approvaltype/" + eitcode + "/" + type_id).then(workflowApprovalCBF, app.failCbFn);

                    document.querySelector("#NoDialog").open();

                };



                //Start function
                self.personNameArr = ko.observableArray([]);
                self.getEmpNameById = function () {
                    var reportPaylod = {"reportName": "XXX_GET_EMP_PERSONID_REPORT"};
                    app.loading(true);
                    var getRolesCBCF = function (data) {
                        for (var i = 0; i < data.length; i++) {
                            self.personNameArr.push({
                                value: data[i].PERSON_NUMBER,
                                label: data[i].label
                            });
                            self.personNameArrForApproval.push({value: data[i].value,
                                label: data[i].label});

                        }
                        ;
                        app.loading(false);
                    };
                    services.getGenericReport(reportPaylod).then(getRolesCBCF, self.failCbFn);
                };



                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }

                });

                function initTranslations() {
                    //New
                    self.reassignRequestLbl(getTranslation("pages.reassignRequest"));

                    self.reassignLbl(getTranslation("pages.reassignRequest"));
                    self.unknownErrMsg(getTranslation("labels.ValidationMessage"));
                    self.statusLbl(getTranslation("labels.status"));
                    self.eitNameLbl(getTranslation("approvalScreen.eitCode"));
                    self.personNameLbl(getTranslation("common.personNameLbl"));
                    self.personNumberLbl(getTranslation("common.person_number1Lbl"));
                    self.roleNameLbl(getTranslation("approvalScreen.roleName"));
                    self.confirmMessage(getTranslation("others.confirmMessage"));
                    self.deleteMessageLbl(getTranslation("labels.deleteSelfServiceMessage"));
                    self.no(getTranslation("others.no"));
                    self.yes(getTranslation("others.yes"));
                    self.searchLbl(getTranslation("special.search"));
                    self.clearLbl(getTranslation("special.clear"));
                    self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
                    self.creationDate(getTranslation("notification.creationDate"));
                    self.eitPlaceHolder(getTranslation("others.eitPlaceHolder"));
                    self.roleIdLbl(getTranslation("others.roleIdLbl"));
                    self.roleTypeLbl(getTranslation("others.roleTypeLbl"));
                    self.roleNameLbl(getTranslation("others.roleNameLbl"));
                    self.pendingApprovalLbl(getTranslation("others.pendingApprovalLbl"));
                    self.withdrawLbl(getTranslation("others.withdrawLbl"));
                    self.rejectedLbl(getTranslation("others.rejectedLbl"));
                    self.approveLbl(getTranslation("others.approveLbl"));
                    self.submitLbl(getTranslation("others.submitLbl"));
                    self.columnArray([
                        {
                            "headerText": self.eitNameLbl(), "field": "code"
                        },
                        {
                            "headerText": self.personNameLbl(), "field": "personName"
                        },
                        {
                            "headerText": self.personNumberLbl(), "field": "person_number"
                        },
                        {
                            "headerText": self.statusLbl(), "field": "statusDataLbl"
                        },
                        {
                            "headerText": self.creationDate(), "field": "created_by"
                        }
                    ]);

                    self.columnArrayApprovalList([
                        {
                            "headerText": self.eitNameLbl(), "field": "serviceType"
                        },
                        {
                            "headerText": self.roleTypeLbl(), "field": "rolrType"
                        },
                        {
                            "headerText": self.roleIdLbl(), "field": "roleId"
                        },
                        {
                            "headerText": self.notificationTypeLbl(), "field": "notificationType"
                        },
                        {
                            "headerText": self.roleNameLbl(), "field": "roleNameLbl"
                        },
                        {
                            "headerText": self.statusLbl(), "field": "responseCode"
                        }
                    ]);

                }
                initTranslations();
            }

            return reassignRequestScreenContentViewModel;
        });
