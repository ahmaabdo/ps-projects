/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * reassignRequestOperation module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectcombobox', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, services, commonhelper) {
            /**
             * The view model for the main content view template
             */
            function reassignRequestOperationContentViewModel() {
                var self = this;

                self.columnArray = ko.observableArray();
                self.tracker = ko.observable();
                self.groupValid = ko.observable();
                self.placeholder = ko.observable("Please Select Value");
                self.isVisibleRoleName = ko.observable(false);
                self.isVisibleSpecialCase = ko.observable(false);
                self.isDisabled = ko.observable(false);
                self.isRequired = ko.observable(true);
                self.currentStepValue = ko.observable('stp1');
                self.addBtnVisible = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.pageMode = ko.observable();
                self.oprationMessage = ko.observable();
                self.validationMessage = ko.observable();
                self.disableSubmit = ko.observable(false);
                self.isRequiredRoleName = ko.observable(false);
                self.isRequiredSpecialCase = ko.observable(false);
                self.roleOptionType = ko.observableArray([]);
                self.rolesOption = ko.observableArray([]);
                self.isVisibleRoles = ko.observable(false);
                self.isRequiredRoles = ko.observable(false);
                self.roleIdMessageLbl = ko.observable();
                self.EitNameArr = ko.observableArray([]);
                self.staticRequestArr = ko.observableArray();
                self.approvalTypeArr = ko.observable([]);
                self.notificationTypeArr = ko.observable([]);
                self.lastNotification = ko.observable();
                self.reasonforReassignLbl = ko.observable();
                self.searchModel = ko.observable();
                var role;
                var countValid = "";
                var positionObj;


                self.searchModel = {
                    name: ko.observable(""),
                    PersonNumber: ko.observable(""),
                    generalGroup: ko.observable(""),
                    nationalId: ko.observable(""),
                    effectiveDate: ko.observable(""),
                    selfServiceType: ko.observable(""),
                    managerId: ko.observable("")
                };


                self.stepArray = ko.observableArray([{label: "XXXX", id: 'stp1'},
                    {label: "YYYY", id: 'stp2'}]);

                self.approvalSetupModel = {
                    eitCode: ko.observable(),
                    approvalOrder: ko.observable(),
                    approvalType: ko.observable(),
                    id: ko.observable(),
                    roleName: ko.observable(), 
                    specialCase: ko.observable(''), 
                    notificationType: ko.observable(),
                    position: ko.observable()
                };

                self.validationModel = {
                    eid_code: ko.observable(),
                    role_name: ko.observable(),
                    approval_type: ko.observable(),
                    notification_type: ko.observable(),
                    approval: ko.observable()
                };

                self.approvalListModel = {
                    id: ko.observable(),
                    rolrType: ko.observable(),
                    roleId: ko.observable(),
                    responseCode: ko.observable(),
                    serviceType: ko.observable(),
                    reasonforReassign: ko.observable()
                };


                self.backAction = function () {
                    if (oj.Router.rootInstance._navHistory.length > 1) {
                        app.loading(true);
                        oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                    }
                };


                self.handleAttached = function () {
                    app.loading(false);
                    initTranslations();
                    self.roleOptionType(app.roleOptionType());
                    self.rolesOption(app.rolesOption());
                    self.EitNameArr(app.globalEitNameReport());
                    positionObj = oj.Router.rootInstance.retrieve();
                      // console.log(positionObj);
                    //------------page mode section--------------------
                    if (positionObj.type == 'EDIT') {
                        var roleId = oj.Router.rootInstance.retrieve().roleId;
                        self.pageMode('EDIT');
                        if (oj.Router.rootInstance.retrieve().rolrType == "POSITION")
                        {

                            self.approvalSetupModel.position(roleId);
                            self.isVisibleRoleName(true);
                            self.isRequiredRoleName(false);

                        }
                        if (oj.Router.rootInstance.retrieve().rolrType == "Special_Case")
                        {
                            self.isVisibleSpecialCase(true);
                            self.isRequiredSpecialCase(true);
                        }
                        if (oj.Router.rootInstance.retrieve().rolrType == "ROLES")
                        {

                            self.approvalSetupModel.roleName(Number(roleId));
                            self.isVisibleRoles(true);
                            self.isRequiredRoles(true);


                        }

                        if (oj.Router.rootInstance.retrieve().rolrType == "LINE_MANAGER" || oj.Router.rootInstance.retrieve().rolrType == "LINE_MANAGER+1" || oj.Router.rootInstance.retrieve().rolrType == "EMP") {
                            self.approvalSetupModel.approvalType(oj.Router.rootInstance.retrieve().rolrType);
                            self.approvalSetupModel.roleName(oj.Router.rootInstance.retrieve().roleId);
                            role = oj.Router.rootInstance.retrieve().roleId;

                        }

                        self.connected();
                        self.clothesAllowance(self.workNatureAllowanceScreenEditLbl());
                        self.oprationMessage(self.editMassageLbl());
                        self.approvalSetupModel.positionCode = positionObj.positionCode;
                        self.approvalSetupModel.id = positionObj.id;
                        self.approvalSetupModel.code = positionObj.code;

                    }

                };

                //-------------This Function For Update Train Step To previousStep------------------
                function isEmpty(val) {
                    return (val === undefined || val == null || val.length <= 0) ? true : false;
                }
                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev != null)
                        self.currentStepValue(prev);
                };
                self.nextStep = function ()
                {
                    self.getNotificationType();
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next != null)
                    {
                        if (parseInt(countValid) > 0)
                        {
                            $.notify(self.validMessageLbl(), "error");
                        } else
                        {
                            var flag = true;

                            if (flag)
                            {
                                self.currentStepValue(next);
                                self.currentStepValueText();
                            }
                        }
                    }
                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        self.isDisabled(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);

                    } else {
                        self.isDisabled(false);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);
                    }

                    return self.clothesAllowance();
                };
                // ---------This Section For Change Handler --------------------
                self.approvalTypeChangedHandler = function () {
                    if (self.approvalSetupModel.approvalType() == "POSITION") {
                        self.isVisibleRoleName(true);
                        self.isRequiredRoleName(true);
                        self.approvalSetupModel.position("");

                    } else {
                        self.isVisibleRoleName(false);
                        self.isRequiredRoleName(false);
                        self.approvalSetupModel.roleName("");

                    }
                    if (self.approvalSetupModel.approvalType() == "Special_Case") {
                        self.isVisibleSpecialCase(true);
                        self.isRequiredSpecialCase(true);
                    } else {
                        self.isVisibleSpecialCase(false);
                        self.isRequiredSpecialCase(false);
                        self.approvalSetupModel.specialCase("");
                    }
                    if (self.approvalSetupModel.approvalType() == "ROLES") {
                        self.isVisibleRoles(true);
                        self.isRequiredRoles(true);
                    } else {
                        self.isVisibleRoles(false);
                        self.isRequiredRoles(false);
                        self.approvalSetupModel.roleName('');
                    }
                    if (self.approvalSetupModel.approvalType() == "LINE_MANAGER" || self.approvalSetupModel.approvalType() == "LINE_MANAGER+1") {
                        self.searchPerson();
                    } else if (self.approvalSetupModel.approvalType() == "EMP") {
                        self.searchPerson();
                        {

                        }
                    }
                };

                self.roleNameChangedHandler = function () {
                    if (!isEmpty(event.detail.value)) {
                        self.approvalListModel.roleId(event.detail.value);
                    }
                };

                self.eitCodeChangedHandler = function () {
                    self.getNotificationType();
                    self.approvalSetupModel.notificationType("");
                };
                // ---------End Of Change Handler -----------------
                //------------------This Section For Dialog Action (Open ,Close)
                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.cancelButtonDialog = function () {
                    document.querySelector("#NoDialog").close();
                    document.querySelector("#yesNoDialog").close();
                };
                //-------------This Function To Stop Train Action If Action Are Not Valid -------
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");
                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };
                //------------------ This Section For Call Web Service For Insert Or Update ---------
                function oprationServiceRecord() {
                    self.approvalListModel.id(self.approvalSetupModel.id);
                    self.approvalListModel.rolrType(self.approvalSetupModel.approvalType);
                    self.approvalListModel.serviceType(self.approvalSetupModel.eitCode);

                    if (self.approvalSetupModel.approvalType() == "POSITION") {
                        app.roleOptionType().filter((e) => {
                            if (e.value == self.approvalSetupModel.position()) {
                                role = e.value;
                            }
                        });
                    } else if (self.approvalSetupModel.approvalType() == "ROLES") {
                        app.rolesOption().filter((e) => {
                            if (e.value == self.approvalSetupModel.roleName()) {
                                role = JSON.stringify(e.value);
                            }
                        });
                    }

                    var jsonData = {
                        id: positionObj.id,
                        reasonforReassign: self.approvalListModel.reasonforReassign(),
                        roleId: role,
                        rolrType: self.approvalSetupModel.approvalType(),
                        serviceType:positionObj.code,
                        responseCode:positionObj.transActionId
                        
                    };
                    if (parseInt(countValid) > 0)
                    {
                        document.querySelector("#yesNoDialog").close();
                        var popup = document.querySelector('#NoDialog');
                        popup.open();

                        self.isDisabled(false);

                    } else
                    {
                        var operationEditCbFn = function () {
                            self.disableSubmit(false);
                            if (oj.Router.rootInstance._navHistory.length > 1) {
                                oj.Router.rootInstance.go(oj.Router.rootInstance._navHistory[oj.Router.rootInstance._navHistory.length - 1]);
                            } else {
                                oj.Router.rootInstance.go('reassignRequestScreen');
                            }
                        };
                       // console.log(jsonData);
                        services.addGeneric("reassignRequest/updateApprovalList", jsonData).then(operationEditCbFn, self.failCbFn);
                    }
                }
                ;
                self.commitRecord = function () {
                    if (!self.disableSubmit()) {
                        self.disableSubmit(true);
                        oprationServiceRecord();
                        return true;
                    }
                };


                self.searchPerson = function () {
                    var getPersonDetailsCbFn = function (data) {
                        if (self.approvalSetupModel.approvalType() == "LINE_MANAGER") {
                            role = data.managerId;

                        } else if (self.approvalSetupModel.approvalType() == "LINE_MANAGER+1") {
                            role = data.managerOfManager;

                        } else if (self.approvalSetupModel.approvalType() == "EMP") {
                            role = data.personId;
                        }
                    };
                    services.searchEmployees(self.searchModel.name(), self.searchModel.nationalId(), self.searchModel.PersonNumber(), self.searchModel.managerId(), self.searchModel.effectiveDate()).then(getPersonDetailsCbFn, app.failCbFn);
                };

                self.getNotificationType = function () {
                    var eit_COde = self.approvalSetupModel.eitCode();
                    var getNotificationTypeCBF = function (data) {
                        self.lastNotification(data.notificationType);
                    };
                    services.getGeneric(commonhelper.approvalSetupUrl + "notificationtype/" + eit_COde).then(getNotificationTypeCBF, app.failCbFn);
                };

                self.notificationTypeChangedHandler = function (){

                };
                //-------------------------Start Load Funcation------------------------------

                self.connected = function () {
                    self.approvalSetupModel.eitCode(oj.Router.rootInstance.retrieve().code);
                    self.approvalSetupModel.approvalType(oj.Router.rootInstance.retrieve().rolrType);
                    self.approvalSetupModel.specialCase(oj.Router.rootInstance.retrieve().specialCase);
                    self.approvalSetupModel.notificationType(oj.Router.rootInstance.retrieve().notificationType);
                    self.approvalSetupModel.id(oj.Router.rootInstance.retrieve().id);
                    self.approvalListModel.responseCode(oj.Router.rootInstance.retrieve().transActionId);
                    self.searchModel.PersonNumber(oj.Router.rootInstance.retrieve().personNumber);
                    self.approvalListModel.reasonforReassign(oj.Router.rootInstance.retrieve().reasonforReassign);
                };

                //---------------------Translate Section ----------------------------------------
                self.ok = ko.observable();
                self.addLbl = ko.observable();
                self.editLbl = ko.observable();
                self.backLbl = ko.observable();
                self.nextLbl = ko.observable();
                self.submitLbl = ko.observable();
                self.placeholder = ko.observable();
                self.eitCodeLbl = ko.observable();
                self.cancel = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.workNatureAllowanceScreenEditLbl = ko.observable();
                self.validMessageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.confirmMessage = ko.observable();
                self.clothesAllowance = ko.observable();
                self.approvalTypeLbl = ko.observable();
                self.roleNameLbl = ko.observable();
                self.specialCaseLbl = ko.observable();
                self.notificationTypeLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.rolesLbl = ko.observable();
                
                var getTranslation = oj.Translations.getTranslatedString;
                
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {
                    self.notificationTypeArr(app.getPaaSLookup('NOTIFICATION_TYPE'));
                    self.approvalTypeArr(app.getPaaSLookup('APPROVAL_TYPE'));
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.backLbl(getTranslation("others.back"));
                    self.nextLbl(getTranslation("others.next"));
                    self.submitLbl(getTranslation("others.submit"));
                    self.ok(getTranslation("others.ok"));
                    self.addLbl(getTranslation("others.add"));
                    self.editLbl(getTranslation("others.edit"));
                    self.eitCodeLbl(getTranslation("approvalScreen.eitCode"));
                    self.cancel(getTranslation("others.cancel"));
                    self.workNatureAllowanceScreenEditLbl(getTranslation("approvalScreen.ApprovalEdit"));
                    self.validMessageLbl(getTranslation("workNatureAllowance.validMessage"));
                    self.editMassageLbl(getTranslation("approvalScreen.editMessage"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.approvalTypeLbl(getTranslation("approvalScreen.approvalType"));
                    self.roleNameLbl(getTranslation("approvalScreen.roleName"));
                    self.specialCaseLbl(getTranslation("approvalScreen.specialCase"));
                    self.notificationTypeLbl(getTranslation("approvalScreen.notificationType"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.rolesLbl(getTranslation("approvalScreen.roles"));
                    self.reasonforReassignLbl(getTranslation("approvalScreen.reasonForReassign"));
                    self.roleIdMessageLbl(getTranslation("approvalScreen.roleIdMessage"));
                    
                    self.stepArray([{label: self.create(), id: 'stp1'},
                        {label: self.review(), id: 'stp2'}]);
                }
                initTranslations();
            }

            return reassignRequestOperationContentViewModel;
        });
