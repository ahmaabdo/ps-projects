/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'ojs/ojvalidation-datetime', 'ojs/ojtimezonedata',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var operationType;
                self.disableSubmit = ko.observable(false);
                self.disableSubmitEdit = ko.observable(false);
                self.endDate = ko.observable();
                self.reportName = ko.observable();
                self.Parameter1 = ko.observable();
                self.Parameter2 = ko.observable();
                self.Parameter3 = ko.observable();
                self.Parameter4 = ko.observable();
                self.Parameter5 = ko.observable();
                self.createReport = ko.observable();
                self.reportTitleLabl = ko.observable();
                self.reportTypelbl = ko.observable();
                self.trainVisible = ko.observable(true);
                self.reportTypeArr = ko.observableArray();

                self.addJob = ko.observable();
                self.updateJob = ko.observable();
                self.viewJob = ko.observable();
                self.jobTitel = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.addMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.placeholder = ko.observable();
                self.date1 = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));

                self.serviceType = ko.observable();
                self.ministryJobModle = ko.observable();
                self.groupValid = ko.observable();
                self.jnVisibility = ko.observable();
                self.jnVisibility = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.isDisabled = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.errormessage = ko.observable();
                self.errormessagecheckInDatabase = ko.observable();
                self.validationMessage = ko.observable();
                self.oprationMessageUpdate = ko.observable();
                self.reportNamelbl = ko.observable();
                self.Parameter1lbl = ko.observable();
                self.Parameter2lbl = ko.observable();
                self.Parameter3lbl = ko.observable();
                self.Parameter4lbl = ko.observable();
                self.Parameter5lbl = ko.observable();



                self.currentStepValue = ko.observable('stp1');
                self.hidecancel = ko.observable(true);
                self.hidepervious = ko.observable(false);
                self.hidenext = ko.observable(true);
                self.hidesubmit = ko.observable(false);
                self.hideUpdatesubmit = ko.observable(false);
                self.messages = ko.observable();
                self.statMinValue = ko.observable();
                self.personNameArrForApproval = ko.observableArray([]);
                self.personNameArr = ko.observableArray([]);

                //define new
                self.validationMessagesArr = ko.observableArray();
                self.validationMessagesProvider = new oj.ArrayDataProvider(self.validationMessagesArr);
                self.createMessage = function (summary) {
                    return {
                        severity: 'error',
                        summary: summary

                    };
                };
                self.RoleNameVal = ko.observable();
                self.RoleDescriptionLblVal = ko.observable();
                self.EITCodeArr = ko.observableArray();
                self.eitCodeVal = ko.observable();
                self.EITCodeArr(app.globalEitNameReport());
                self.actionType = ko.observable();
                self.personId = ko.observable();
                self.Notify = ko.observable();
                self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                    pattern: 'dd/MM/yyyy'
                }));
                var deptArray = [];
                self.Employeedata = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.Employeedata, {idAttribute: 'nationalId'}));
                self.searchModel = ko.observable();
                self.searchModel = {
                    EffectiveStartDateVal: ko.observable(""),
                    EffectiveEnddateVal: ko.observable(""),
                    personNumberVal: ko.observable(""),

                };


                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (mm < 10) {
                    mm = '0' + mm;
                }

                today = yyyy + '-' + mm + '-' + dd;


                self.statMinValue(today);
                self.tableSelectionListener = function (event) {

                };
                self.SearchBTn = function () {
                    self.Employeedata([]);
                    services.searchEmployees("", "", self.searchModel.personNumberVal(), "", "").then(getPersonDetailsCbFn, app.failCbFn);
                };

                var getPersonDetailsCbFn = function (data) {
                    self.personId(data.personId);
                    self.Employeedata.push({
                        assignmentName: data.assignmentName, assignmentProjectedEndDate: data.assignmentProjectedEndDate, assignmentStatus: data.assignmentStatus,
                        assignmentStatusTypeId: data.assignmentStatusTypeId, carAllowance: data.carAllowance, citizenshipLegislationCode: data.citizenshipLegislationCode, nationalId: (data.nationalId === 'null' || data.nationalId === null) ? '' : data.nationalId,
                        city: data.city, country: data.country, dateOfBirth: data.dateOfBirth, department: data.department, displayName: data.displayName, personNumber: data.personNumber,
                        email: data.email, employeeCertificate: data.employeeCertificate, employeeLocation: data.employeeLocation, fax: data.fax, faxExt: data.faxExt,
                        grade: data.grade, gradeId: data.gradeId, hireDate: data.hireDate, housingType: data.housingType,
                        jobId: data.jobId, jobName: data.jobName, legalEntityId: data.legalEntityId, managerId: data.managerId, managerName: data.managerName, managerOfManager: data.managerOfManager,
                        managerOfMnagerName: data.managerOfMnagerName, managerType: data.managerType, maritalStatus: data.maritalStatus, ministryJobForEmployee: data.ministryJobForEmployee, mobileAllowance: data.mobileAllowance, mobileNumber: data.mobileNumber,
                        organizationName: data.organizationName, overTimePaymentUrl: data.overTimePaymentUrl, overTimeURL: data.overTimeURL, peopleGroup: data.peopleGroup, personId: data.personId, picBase64: data.picBase64,
                        positionName: data.positionName, probationPeriodEndDate: data.probationPeriodEndDate, projectedStartDate: data.projectedStartDate, region: data.region, salaryAmount: data.salaryAmount, transportationAlowance: data.transportationAlowance,
                        workPhone: data.workPhone, workPhoneExt: data.workPhoneExt, employeeURL: data.employeeURL
                    });

                };


                self.getdelegationCount = function () {

                    self.checkValidate = ko.observable();
                    var getDelegationCountCbFn = function (data) {

                        self.validationMessagesArr([]);
                        if (data.length === 0) {
                            self.checkValidate("valide");
                            self.hidesubmit(true);
                            self.isDisabled(true);
                            self.hidepervious(true)
                            self.hidenext(false);
                            $(".disable-element").prop('disabled', true);
                            self.currentStepValue('stp2');
                        } else {

                            for (var i = 0; i < data.length; i++) {
                                if (data[i].effectiveEndDate < self.searchModel.EffectiveStartDateVal()) {
                                    self.checkValidate("valide");
                                    self.hidesubmit(true);
                                    self.isDisabled(true);
                                    self.hidepervious(true)
                                    self.hidenext(false);
                                    $(".disable-element").prop('disabled', true);
                                    self.currentStepValue('stp2');
                                } else {
                                    self.validationMessagesArr.push(self.createMessage(self.Notify()));
                                }

                            }
                        }
//                                               
                    };

                    var payload = {
//                      "personNumber":self.searchModel.personNumberVal(),  
                        "createByPersonNumber": app.personDetails().personNumber
                    };

                    var serviceName = "Delegation/validationDelegationPeriod?personNumber=" + app.personDetails().personNumber;
                    services.getGeneric(serviceName, {}).then(getDelegationCountCbFn, app.failCbFn);
                };



                self.ValidationUpdateDelegation = function () {
                    self.validationMessagesArr([]);
                    self.checkValidate = ko.observable();
                    var getDelegationCountCbFn = function (data) {

                        if (data.length == 0) {
                            self.checkValidate("valide");
                            self.hidesubmit(true);
                            self.isDisabled(true);
                            self.hidepervious(true)
                            self.hidenext(false);
                            $(".disable-element").prop('disabled', true);
                            self.currentStepValue('stp2');
                        } else {
                            for (var i = 0; i < data.length; i++) {
                                if (data[i].effectiveEndDate <= self.searchModel.EffectiveStartDateVal()) {
                                    self.checkValidate("valide");
                                    self.hidesubmit(true);
                                    self.isDisabled(true);
                                    self.hidepervious(true)
                                    self.hidenext(false);
                                    $(".disable-element").prop('disabled', true);
                                    self.currentStepValue('stp2');
                                } else {

                                    self.validationMessagesArr.push(self.createMessage(self.Notify()));
                                }
                            }
                        }
                    };

                    var payload = {
                        "id": self.retrieve.id,
//                      "personNumber":self.searchModel.personNumberVal(),  
                        "createByPersonNumber": app.personDetails().personNumber
                    };

                    var serviceName = "Delegation/UpdatevalidationDelegationPeriod";
                    services.addGeneric(serviceName, payload).then(getDelegationCountCbFn, app.failCbFn);
                };




                self.previousStep = function () {
                    self.hidecancel(true);
                    self.hidepervious(false);
                    self.hidesubmit(false);
                    self.hideUpdatesubmit(false);
                    self.hidenext(true);
                    self.isDisabled(true);
                    $(".disable-element").prop('disabled', false);
                    self.currentStepValue('stp1');

                };


                self.nextStep = function () {
                    var tracker = document.getElementById("tracker");

                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    } else {
                        if (self.searchModel.EffectiveStartDateVal() <= self.searchModel.EffectiveEnddateVal()) {
                            if (self.Employeedata().length > 0) {
                                if (app.personDetails().personNumber !== self.searchModel.personNumberVal()) {
                                    if (self.actionType() === "ADD") {
                                        self.getdelegationCount();
                                    } else if (self.actionType() === "EDIT") {
                                        self.ValidationUpdateDelegation();
                                    }
                                } else {
                                    //i will write 
                                    self.validationMessagesArr.push(self.createMessage(self.checkUserDelegated()));
                                    self.Employeedata([]);
                                }
                            } else {
                                self.validationMessagesArr.push(self.createMessage(self.validateSearch()));
                            }

                        } else {
                            self.validationMessagesArr.push(self.createMessage(self.CheckStartDateLessEndDate()));
                        }
                    }
                };


                self.validationModle = ko.observable();
                self.validationModle = {
                    RoleNameVal: ko.observable(),
                    RoleDescriptionLblVal: ko.observable(),
                    eitCodeVal: ko.observable()

                };

                self.insertOrUpdateDelegation = function () {
                    var payload = {
                        "effectiveStartDate": self.searchModel.EffectiveStartDateVal(),
                        "effectiveEndDate": self.searchModel.EffectiveEnddateVal(),
                        "personNumber": self.searchModel.personNumberVal(),
                        "personId": self.personId(),
                        "createdByPersonId": app.personDetails().personId,
                        "createByPersonNumber": app.personDetails().personNumber,
                        "id": self.retrieve.id
                    };

                    var getDelegationCbFn = function (data) {
                        document.querySelector("#yesNoDialog").close();
                        oj.Router.rootInstance.go('delegationSummary');


                    };




                    var serviceName = "Delegation/addDelegation/" + self.actionType();
                    services.addGeneric(serviceName, payload).then(getDelegationCbFn, app.failCbFn);
                };


                self.disableInput = function () {
                    self.hidecancel(false);
                    self.hidepervious(true);
                    self.hidenext(false);

                    $(".disable-element").prop('disabled', true);
                    self.currentStepValue('stp2');
                };

                self.cancelAction = function () {

                    oj.Router.rootInstance.go('delegationSummary');

                };


                self.submitButton = function () {
                    document.querySelector("#yesNoDialog").open();

                };
                self.commitRecordupdate = function () {
                    operationType = 'EDIT';
                    self.callReport(operationType);
                    self.disableSubmitEdit(true);

                };
                self.cancelButtonupdate = function () {
                    document.querySelector("#yesNoDialogUpdate").close();

                };
                self.submitUpdateButton = function () {
                    document.querySelector("#yesNoDialogUpdate").open();
                };








                self.commitRecord = function () {
                    self.insertOrUpdateDelegation();
                    self.disableSubmit(true);
                };


                self.cancelButton = function () {

                    document.querySelector("#yesNoDialog").close();
                };

                self.commitDraft = function (data, event) {
                    return true;
                };
                self.stopSelectListener = function () {
                    var tracker = document.getElementById("tracker");

                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }

                };

                self.handleAttached = function () {
                    app.loading(false);
                    self.getEmpNameById();



                    self.retrieve = oj.Router.rootInstance.retrieve();


                    if (self.retrieve === 'ADD') {

                        self.actionType('ADD');
                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(true);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', false);
                        self.currentStepValue('stp1');
                        self.servicetype = ko.observable();

                    } else if (self.retrieve.type === 'view')
                    {

                        self.searchModel = {
                            EffectiveStartDateVal: ko.observable(self.retrieve.effectiveStartDateVal),
                            EffectiveEnddateVal: ko.observable(self.retrieve.effectiveEndDateVal),
                            personNumberVal: ko.observable(self.retrieve.personNumberVal)

                        };
                        self.Employeedata([]);
                        services.searchEmployees("", "", self.retrieve.personNumberVal, "", "").then(getPersonDetailsCbFn, app.failCbFn);

                        self.hidecancel(true);
                        self.hidepervious(false);
                        self.hidenext(false);
                        self.hidesubmit(false);
                        $(".disable-element").prop('disabled', true);
                        self.currentStepValue('stp2');
                        self.trainVisible(false);
                        $(".disable-element").prop('disabled', true);
                        $(".disable-elementSearch").css("display", "none");
                    } else if (self.retrieve.type === 'update') {
                        self.actionType('EDIT');
                        self.searchModel = {
                            EffectiveStartDateVal: ko.observable(self.retrieve.effectiveStartDateVal),
                            EffectiveEnddateVal: ko.observable(self.retrieve.effectiveEndDateVal),
                            personNumberVal: ko.observable(self.retrieve.personNumberVal)

                        };
                        self.Employeedata([]);
//                        services.searchEmployees("", "", self.retrieve.personNumberVal, "", "").then(getPersonDetailsCbFn, app.failCbFn);      


                    }

                };

                self.currentStepValueText = function () {
                    if (self.currentStepValue() == 'stp2') {
                        $(".disable-element").prop('disabled', true);
                    } else {
                        $(".disable-element").prop('disabled', false);
                    }

                };


                // get employees name by id 

                self.getEmpNameById = function () {
                    var reportPaylod = {"reportName": "XXX_GET_EMP_PERSONID_REPORT"};
                    app.loading(true);
                    var getRolesCBCF = function (data) {
                        for (var i = 0; i < data.length; i++) {
                            self.personNameArr.push({
                                value: data[i].PERSON_NUMBER,
                                label: data[i].label
                            });
                            self.personNameArrForApproval.push({value: data[i].value,
                                label: data[i].label});

                        }
                        ;
                        app.loading(false);
                    };
                    services.getGenericReport(reportPaylod).then(getRolesCBCF, self.failCbFn);
                };


                //define new label 
                self.RoleNameLbl = ko.observable();
                self.RoleDescriptionLbl = ko.observable();
                self.EitCodeLbl = ko.observable();
                self.roleSetupLbl = ko.observable();

                //new
                self.EffectiveStartDateLBl = ko.observable();
                self.EffectiveEnddateLBL = ko.observable();
                self.personNumberLbl = ko.observable();
                self.employeeNmaeLbl = ko.observable();
                self.NationalIdLbl = ko.observable();
                self.departmentLbl = ko.observable();
                self.positionLbl = ko.observable();
                self.locationLbl = ko.observable();
                self.searchLbl = ko.observable();
                self.CheckStartDateLessEndDate = ko.observable();
                self.validateSearch = ko.observable();
                self.checkUserDelegated = ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });



                function initTranslations() {
                    self.retrieve = oj.Router.rootInstance.retrieve();

                    self.reportTypeArr(app.getPaaSLookup('reportType'));
                    if (self.retrieve === 'ADD') {

                        self.reportTitleLabl(getTranslation("report.addDelegationSetup"));

                    } else if (self.retrieve.type === 'view') {

                        self.reportTitleLabl(getTranslation("report.viewDelegationSetup"));
                    } else if (self.retrieve.type === 'update') {

                        self.reportTitleLabl(getTranslation("report.editDelegationSetup"));
                    }
                    self.reportName(getTranslation("report.reportName"));
                    self.Parameter1(getTranslation("report.Parameter1"));
                    self.Parameter2(getTranslation("report.Parameter2"));
                    self.Parameter3(getTranslation("report.Parameter3"));
                    self.Parameter4(getTranslation("report.Parameter4"));
                    self.Parameter5(getTranslation("report.Parameter5"));
                    self.createReport(getTranslation("report.createReport"));
                    self.errormessage(getTranslation("report.errormessage"));
                    self.createReport(getTranslation("report.create"));
                    self.reportNamelbl(getTranslation("report.reportNamelbl"));
                    self.Parameter1lbl(getTranslation("report.Parameter1lbl"));
                    self.Parameter2lbl(getTranslation("report.Parameter2lbl"));
                    self.Parameter3lbl(getTranslation("report.Parameter3lbl"));
                    self.Parameter4lbl(getTranslation("report.Parameter4lbl"));
                    self.Parameter5lbl(getTranslation("report.Parameter5lbl"));
                    self.reportTypelbl(getTranslation("report.reportType"));
                    self.errormessagecheckInDatabase(getTranslation("report.errormessagecheckInDatabase"));
                    self.oprationMessage(getTranslation("report.oprationMessage"));
                    self.confirmMessage(getTranslation("report.confirmMessage"));
                    self.oprationMessageUpdate(getTranslation("report.oprationMessageUpdate"));
                    self.yes(getTranslation("report.yes"));
                    self.no(getTranslation("report.no"));
                    self.addJob(getTranslation("job.addJob"));
                    self.updateJob(getTranslation("job.updateJob"));
                    self.viewJob(getTranslation("job.viewJob"));
                    self.addMassageLbl(getTranslation("job.addMessage"));
                    self.editMassageLbl(getTranslation("job.editMessage"));

                    self.trainView(getTranslation("others.review"));

                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));

                    self.submit(getTranslation("others.submit"));

                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));


                    self.RoleNameLbl(getTranslation("report.RoleName"));
                    self.RoleDescriptionLbl(getTranslation("report.RoleDescription"));
                    self.EitCodeLbl(getTranslation("report.EitCode"));
                    //new
                    self.EffectiveStartDateLBl(getTranslation("common.EffectiveStartDate"));
                    self.EffectiveEnddateLBL(getTranslation("common.EffectiveEnddate"));
                    self.personNumberLbl(getTranslation("common.personNumberLbl"));
                    self.personNumberLbl(getTranslation("special.personNumberTh"));
                    self.employeeNmaeLbl(getTranslation("special.employeeNmaeTh"));
                    self.NationalIdLbl(getTranslation("special.NationalIdTh"));
                    self.departmentLbl(getTranslation("special.departmentTh"));
                    self.positionLbl(getTranslation("special.positionTh"));
                    self.locationLbl(getTranslation("special.locationTh"));
                    self.searchLbl(getTranslation("login.Search"));
                    self.Notify(getTranslation("special.Notify"));
                    self.CheckStartDateLessEndDate(getTranslation("special.CheckStartDateLessEndDate"));
                    self.validateSearch(getTranslation("special.validateSearch"));
                    self.checkUserDelegated(getTranslation("special.checkUserDelegated"));

                    self.columnArray([
                        {
                            "headerText": self.personNumberLbl(), "field": "personNumber"
                        },
                        {
                            "headerText": self.employeeNmaeLbl(), "field": "displayName"
                        },
                        {
                            "headerText": self.NationalIdLbl(), "field": "nationalId"
                        },
                        {
                            "headerText": self.departmentLbl(), "field": "department"
                        },
                        {
                            "headerText": self.positionLbl(), "field": "positionName"
                        },
                        {
                            "headerText": self.locationLbl(), "field": "employeeLocation"
                        }
                    ]);



                    self.stepArray([{label: self.createReport(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
