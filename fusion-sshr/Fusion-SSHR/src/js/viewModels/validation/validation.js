/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup'],
        function (oj, ko, $, app, commonUtil, services, commonhelper) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                self.showparameter1=ko.observable(false);
                self.showparameter2=ko.observable(false);
                self.showparameter3=ko.observable(false);
                self.addToSassQuery=ko.observable();
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.showhints = ko.observable();
                self.errorMessage = ko.observable();
                self.callTransaction = function () {
                    var jsonData = ko.toJSON(self.validationModle);
                    
                    var transactionCbFn = function (data) {
                    };
                    var failCbFn = function () {
                    };
                    var service = "validation/";
                    services.genericAddOrEdit(service, jsonData, self.serviceType()).then(transactionCbFn, failCbFn);
                };



                self.getValidationCount = function () {
                    var jsonData = ko.toJSON(self.validationModle);

                    var payload = {
                        "eitCode": self.validationModle.eitCode(),
                        "firstKeyType": self.validationModle.firstKeyType(),
                        "firstKeyValue": self.validationModle.firstKeyValue(),
                        "applicationValueFamily": self.validationModle.applicationValueFamily,
                        "applicationKey": self.validationModle.applicationKey,
                        "saaSSource": self.validationModle.saaSSource(),
                        "reportName": self.validationModle.reportName(),
                        "parameter1Name": self.validationModle.parameter1Name(),
                        "parameter2Name": self.validationModle.parameter2Name(),
                        "parameter3Name": self.validationModle.parameter3Name(),
                        "parameter4Name": self.validationModle.parameter4Name(),
                        "parameter5Name": self.validationModle.parameter5Name(),
                        "saaSQuery": self.validationModle.saaSQuery(),
                        "paaSQuery": self.validationModle.paaSQuery(),
                        "operation": self.validationModle.operation(),
                        "secondKeyType": self.validationModle.secondKeyType(),
                        "secondKeyValue": self.validationModle.secondKeyValue(),
                        "secondapplicationValueFamily": self.validationModle.secondapplicationValueFamily,
                        "secondapplicationKey": self.validationModle.secondapplicationKey(),
                        "secondsaaSSource": self.validationModle.secondsaaSSource(),
                        "secondreportName": self.validationModle.secondreportName(),
                        "secondparameter1Name": self.validationModle.secondparameter1Name(),
                        "secondparameter2Name": self.validationModle.secondparameter2Name(),
                        "secondparameter3Name": self.validationModle.secondparameter3Name(),
                        "secondparameter4Name": self.validationModle.secondparameter4Name(),
                        "secondparameter5Name": self.validationModle.secondparameter5Name(),
                        "secondsaaSQuery": self.validationModle.secondsaaSQuery(),
                        "secondpaaSQuery": self.validationModle.secondpaaSQuery(),
                        "typeOFAction": self.validationModle.typeOFAction(),
                        "errorMassageEn": self.validationModle.errorMassageEn(),
                        "errorMassageAr": self.validationModle.errorMassageAr(),
                        "setValueOfEIT": self.validationModle.setValueOfEIT(),
                        "value": self.validationModle.value(),
                        "saveResultToValidationsValues": self.validationModle.saveResultToValidationsValues(),
                        "validationValuesName": self.validationModle.validationValuesName(),
                        "actiontype": self.validationModle.actiontype(),
                        "newValue": self.validationModle.newValue(),
                        "eitSegment": self.validationModle.eitSegment(),
                        "typeOfValue": self.validationModle.typeOfValue(),
                        "typeOfFristValue": self.validationModle.typeOfFristValue(),
                        "fristValues": self.validationModle.fristValues(),
                        "secondOperation": self.validationModle.secondOperation(),
                        "typeOfSecondValue": self.validationModle.typeOfSecondValue(),
                        "secondValue": self.validationModle.secondValue(),
                        "errorMassageAr": self.validationModle.errorMassageAr()


                    };

                    var transactionCbFn = function (data) {

                    };
                    var failCbFn = function () {
                    };
                    var service = "validation/validationCount";
                    services.addGeneric(service, JSON.stringify(payload)).then(transactionCbFn, failCbFn);
                };

                // Call EIT Code
                self.EITCodeArr = ko.observableArray([]);

                // Call person details
                self.applicationKeyArr = ko.observableArray([
//                     {'value':'',
//                     'label':''}
                ]);

                self.secondapplicationKeyArr = ko.observableArray([
//                     {'value':'',
//                     'label':''}
                ]);
                self.parameterNameArr = ko.observableArray([]);
                self.secondparameterNameArr = ko.observableArray([]);
                self.personalDetailsArr = ko.observableArray([]);
                self.getPersonDetailsCode = function (arg) {

                    var tempObject = Object.keys(rootViewModel.personDetails());
                    //  self.applicationKeyArr([]);
                    //  self.secondapplicationKeyArr([]);

                    for (var i = 0; i < tempObject.length - 1; i++) {
                        if (tempObject[i].includes('Url') || tempObject[i].includes('url')) {

                        } else {
                            // push in array
                            if (arg === "first") {

                                self.applicationKeyArr.push({
                                    value: tempObject[i],
                                    label: tempObject[i]
                                });

                            } else if (arg === "second") {

                                self.secondapplicationKeyArr.push({
                                    value: tempObject[i],
                                    label: tempObject[i]
                                });
                            } else if (arg === "personalVal") {
                                self.fristValuesArr.push({
                                    value: tempObject[i],
                                    label: tempObject[i]
                                });

                            } else if (arg === "SecondpersonalVal") {
                                self.secondValueArr.push({
                                    value: tempObject[i],
                                    label: tempObject[i]
                                });
                            } else if (arg === "other") {
                                self.personalDetailsArr.push({
                                    value: tempObject[i],
                                    label: tempObject[i]
                                });
//
//                                self.secondparameterNameArr.push({
//                                    value: tempObject[i],
//                                    label: tempObject[i]
//                                });
//                             self.SegmintEit ();
                            }

                        }
                    }
                };


                self.structureChangedHandler = function (event, data) {
                    self.SegmintEit(self.validationModle.eitCode());
                };
                var counter=0;
                
                  self.buttonClick=function (){
                       
                         counter=counter+1;
                         if(counter==1){
                            self.showparameter1(true); 
                         }else if(counter==2){
                             self.showparameter2(true);  
                         }else if(counter==3){
                              self.showparameter3(true); 
                         }
                       
                   };

                self.SegmintEit = function (eitcode) {  
                    self.parameterNameArr([]);
                    self.secondparameterNameArr([]);
                 
                    //start
                      self.applicationKeyArr([]); 
                      self.geteitCode("first");
                      self.secondapplicationKeyArr([]);
                      self.geteitCode("second");
                       self.fristValuesArr([]);
                      self.geteitCode("thirdEit");
                        self.secondValueArr([]);
                      self.geteitCode("FourthdEit");
                    var tempObject = Object.keys(rootViewModel.personDetails());
                    for (var i = 0; i < tempObject.length - 1; i++) {

                        self.parameterNameArr.push({
                            value: tempObject[i],
                            label: tempObject[i]
                        });

                        self.secondparameterNameArr.push({
                            value: tempObject[i],
                            label: tempObject[i]
                        });
                    }
                    //end 
                    //full parameter with eit code 
                    for (var i = 0; i < self.EITCodeArr().length; i++) {
                        self.parameterNameArr.push({
                            value: self.EITCodeArr()[i].value,
                            label: self.EITCodeArr()[i].label
                        });
                        self.secondparameterNameArr.push({
                            value: self.EITCodeArr()[i].value,
                            label: self.EITCodeArr()[i].label
                        });
                    }
                    //end full parameter with eit 



                    var positionCode = self.validationModle.eitCode();
                    Url = commonhelper.approvalSetupUrl;
                    var getEitCBFN = function (data) {
                        var Data = data;

                        for (var i = 0; i < Data.length; i++) {

                            self.parameterNameArr.push({
                                value: Data[i].DESCRIPTION,
                                label: Data[i].FORM_LEFT_PROMPT
                            });


                            self.secondparameterNameArr.push({
                                value: Data[i].DESCRIPTION,
                                label: Data[i].FORM_LEFT_PROMPT
                            });
                        }

                    };
                    var failCbFn = function () {
                        $.notify(self.checkResultSearch(), "error");
                    };
                    if (app.getLocale() == 'ar') {
                        self.lang = "AR";
                    } else {
                        self.lang = "US";
                    }
                    services.getEIT(positionCode, self.lang).then(getEitCBFN, failCbFn);
                    //self.buildTable();
                };
                
                
                
                
                self.geteitCode=function(arg){
                    var tempObject=app.getEITSegments(self.validationModle.eitCode());
                  for(var i=0; i<tempObject.length ; i++){
                            if (arg === "first") {

                                self.applicationKeyArr.push({
                                    value: tempObject[i].DESCRIPTION,
                                    label: tempObject[i].DESCRIPTION
                                });

                            } else if (arg === "second") {

                                self.secondapplicationKeyArr.push({
                                    value: tempObject[i].DESCRIPTION,
                                    label: tempObject[i].DESCRIPTION
                                });
                            } else if (arg === "thirdEit") {
                                self.fristValuesArr.push({
                                    value: tempObject[i].DESCRIPTION,
                                    label: tempObject[i].DESCRIPTION
                                });

                            } else if (arg === "FourthdEit") {
                                self.secondValueArr.push({
                                    value: tempObject[i].DESCRIPTION,
                                    label: tempObject[i].DESCRIPTION
                                });
                            } else if (arg === "other") {


                            } 
                  }  
                };
                
                

//                self.geteitCode = function (arg) {
//                    //self.applicationKeyArr([]);
//                    //self.secondapplicationKeyArr([]);
//                    
//                    
//                    var eitCodeCbFn = function (data) {
//                        var tempObject = data;
//                        for (var i = 0; i < tempObject.length; i++) {
//                            // push in array
//                            if (arg === "first") {
//
//                                self.applicationKeyArr.push({
//                                    value: tempObject[i].DESCRIPTION,
//                                    label: tempObject[i].DESCRIPTION
//                                });
//
//                            } else if (arg === "second") {
//
//                                self.secondapplicationKeyArr.push({
//                                    value: tempObject[i].DESCRIPTION,
//                                    label: tempObject[i].DESCRIPTION
//                                });
//                            } else if (arg === "thirdEit") {
//                                self.fristValuesArr.push({
//                                    value: tempObject[i].DESCRIPTION,
//                                    label: tempObject[i].DESCRIPTION
//                                });
//
//                            } else if (arg === "FourthdEit") {
//                                self.secondValueArr.push({
//                                    value: tempObject[i].DESCRIPTION,
//                                    label: tempObject[i].DESCRIPTION
//                                });
//                            } else if (arg === "other") {
//
//
//                            }
//                        }
//                    };
//                    self.lang = '';
//                    if (app.getLocale() == 'ar') {
//                        self.lang = "AR";
//                    } else {
//                        self.lang = "US";
//                    }
//                    services.getEIT(self.validationModle.eitCode(), self.lang).then(eitCodeCbFn, app.failCbFn);
//                };
                
                
                
                

                self.getValuesNamesArr = function () {
                    var Arr = [];
                    var getValidationValueCBCF = function (data) {
                        for (var xx = 0; xx < data.length; xx++) {
                            Arr.push({
                                value: data[xx].validationValuesName,
                                label: data[xx].validationValuesName
                            });
                        }
                    };

                    services.getGeneric(commonUtil.validationValuesName + self.validationModle.eitCode()).then(getValidationValueCBCF, app.failCbFn);
                    return Arr;
                };
                self.handlerChange = function () {
                    self.applicationKeyArr([]);
                    var arg = "first";
                    if (self.validationModle.applicationValueFamily() === "Personal_Information") {

                        self.getPersonDetailsCode(arg);
                    } else if (self.validationModle.applicationValueFamily() === "EIT") {

                        self.geteitCode(arg);
                    } else {
                        self.getValuesNamesArr();
                        self.applicationKeyArr(self.getValuesNamesArr());
                    }
                };

                self.handlerChangeFristValue = function () {

                    if (self.validationModle.typeOfFristValue() === "EIT") {

                        self.fristValuesArr([]);
                        self.geteitCode("thirdEit");

                    } else if (self.validationModle.typeOfFristValue() === "PERSONAL_INFORMATION") {
                        self.fristValuesArr([]);
                        self.getPersonDetailsCode("personalVal");
                    }

                };
                self.handlerChangeSecondeValue = function () {
                    if (self.validationModle.typeOfSecondValue() === "EIT") {

                        self.secondValueArr([]);
                        self.geteitCode("FourthdEit");

                    } else if (self.validationModle.typeOfSecondValue() === "PERSONAL_INFORMATION") {
                        self.secondValueArr([]);
                        self.getPersonDetailsCode("SecondpersonalVal");
                    }

                };

                self.handlerChange2 = function () {
                    self.secondapplicationKeyArr([]);
                    var arg = "second";
                    if (self.validationModle.secondapplicationValueFamily() === "Personal_Information") {

                        self.getPersonDetailsCode(arg);
                    } else if (self.validationModle.secondapplicationValueFamily() === "EIT") {

                        self.geteitCode(arg);
                    } else {
                        self.secondapplicationKeyArr(self.getValuesNamesArr());
                    }
                };


                self.handelApplicationKey = function () {

                    if (self.validationModle.firstKeyType() === "Application_Value" && self.validationModle.applicationValueFamily() === "EIT") {
                        self.geteitCode("first");
                    } else if (self.validationModle.firstKeyType() === "Application_Value" && self.validationModle.applicationValueFamily() === "Personal_Information") {
                        self.getPersonDetailsCode("first");
                    }

                    if (self.validationModle.secondKeyType() === "Application_Value" && self.validationModle.secondapplicationValueFamily() === "EIT") {
                        self.geteitCode("second");
                    } else if (self.validationModle.secondKeyType() === "Application_Value" && self.validationModle.secondapplicationValueFamily() === "Personal_Information") {
                        self.getPersonDetailsCode("second");
                    }

                };



                // Call Report Names 
                self.reportNameArr = ko.observableArray([]);
                self.secondreportNameArr = ko.observableArray([]);
                self.globalreportNameArr = ko.observableArray([]);
                var searchParameters;
                self.getReportNames = function () {

                    var ReportNamesCbFn = function (data) {
                        searchParameters = data;
                        
                        //var tempObject = jQuery.parseJSON(data);
                        for (var i = 0; i < data.length; i++) {

                            // push in array
                            self.reportNameArr.push({
                                value: data[i].report_Name,
                                label: data[i].report_Name
                            });

                            // push in array
                            self.secondreportNameArr.push({
                                value: data[i].report_Name,
                                label: data[i].report_Name
                            });
                        }
                    };
                    var failCbFn = function () {
                    };
                    var serviceName = "validation/reportnames";
                    services.getGeneric(serviceName, "").then(ReportNamesCbFn, failCbFn);
                };
                
                
                
                self.getReportParameters = function () {

                    for (var i = 0; i < searchParameters.length; i++) {

                        if ((searchParameters[i].report_Name) == self.validationModle.reportName()) {

                            {
                                if ((searchParameters[i].parameter1) != null) {
                                    self.isDisabledParameter(false);
                                    self.parameter1NameLBlRep(searchParameters[i].parameter1);
                                    $(".para-rep-display").css("display", "inline");
                                    $(".para-display").css("display", "none");

                                } else if ((searchParameters[i].parameter1) == null) {
                                    self.isDisabledParameter(true);
                                    self.parameter1NameLBlRep("");
                                    $(".para-rep-display").css("display", "none");
                                    $(".para-display").css("display", "inline");
                                } else {

                                }
                            }

                            {
                                if ((searchParameters[i].parameter2) != null) {
                                    self.isDisabledParameter2(false);
                                    self.parameter2NameLBlRep(searchParameters[i].parameter2);
                                    $(".para-rep-display2").css("display", "inline");
                                    $(".para-display2").css("display", "none");

                                } else if ((searchParameters[i].parameter2) == null) {
                                    self.isDisabledParameter2(true);
                                    self.parameter2NameLBlRep("");
                                    $(".para-rep-display2").css("display", "none");
                                    $(".para-display2").css("display", "inline");
                                } else {

                                }
                            }


                            {
                                if ((searchParameters[i].parameter3) != null) {
                                    self.isDisabledParameter3(false);
                                    self.parameter3NameLBlRep(searchParameters[i].parameter3);
                                    $(".para-rep-display3").css("display", "inline");
                                    $(".para-display3").css("display", "none");

                                } else if ((searchParameters[i].parameter3) == null) {
                                    self.isDisabledParameter3(true);
                                    self.parameter3NameLBlRep("");
                                    $(".para-rep-display3").css("display", "none");
                                    $(".para-display3").css("display", "inline");
                                } else {

                                }
                            }


                            {
                                if ((searchParameters[i].parameter4) != null) {
                                    self.isDisabledParameter4(false);
                                    self.parameter4NameLBlRep(searchParameters[i].parameter4);
                                    $(".para-rep-display4").css("display", "inline");
                                    $(".para-display4").css("display", "none");

                                } else if ((searchParameters[i].parameter4) == null) {
                                    self.isDisabledParameter4(true);
                                    self.parameter4NameLBlRep("");
                                    $(".para-rep-display4").css("display", "none");
                                    $(".para-display4").css("display", "inline");
                                } else {

                                }
                            }


                            {
                                if ((searchParameters[i].parameter5) != null) {
                                    self.isDisabledParameter5(false);
                                    self.parameter5NameLBlRep(searchParameters[i].parameter5);
                                    $(".para-rep-display5").css("display", "inline");
                                    $(".para-display5").css("display", "none");

                                } else if ((searchParameters[i].parameter5) == null) {
                                    self.isDisabledParameter5(true);
                                    self.parameter5NameLBlRep("");
                                    $(".para-rep-display5").css("display", "none");
                                    $(".para-display5").css("display", "inline");
                                } else {

                                }
                            }

                        }
                    }
                };

                self.getReportParameters2 = function () {

                    for (var i = 0; i < searchParameters.length; i++) {

                        if ((searchParameters[i].report_Name) == self.validationModle.secondreportName()) {

                            {
                                if ((searchParameters[i].parameter1) != null) {
                                    self.isDisabledParameterSec(false);
                                    self.secondparameter1NameLBlRep(searchParameters[i].parameter1);
                                    $(".sec-para-rep-display").css("display", "inline");
                                    $(".sec-para-display").css("display", "none");

                                } else if ((searchParameters[i].parameter1) == null) {
                                    self.isDisabledParameterSec(true);
                                    self.secondparameter1NameLBlRep("");
                                    $(".sec-para-rep-display").css("display", "none");
                                    $(".sec-para-display").css("display", "inline");
                                } else {

                                }
                            }

                            {
                                if ((searchParameters[i].parameter2) != null) {
                                    self.isDisabledParameterSec2(false);
                                    self.secondparameter2NameLBlRep(searchParameters[i].parameter2);
                                    $(".sec-para-rep-display2").css("display", "inline");
                                    $(".sec-para-display2").css("display", "none");

                                } else if ((searchParameters[i].parameter2) == null) {
                                    self.isDisabledParameterSec2(true);
                                    self.secondparameter2NameLBlRep("");
                                    $(".sec-para-rep-display2").css("display", "none");
                                    $(".sec-para-display2").css("display", "inline");
                                } else {

                                }
                            }


                            {
                                if ((searchParameters[i].parameter3) != null) {
                                    self.isDisabledParameterSec3(false);
                                    self.secondparameter3NameLBlRep(searchParameters[i].parameter3);
                                    $(".sec-para-rep-display3").css("display", "inline");
                                    $(".sec-para-display3").css("display", "none");

                                } else if ((searchParameters[i].parameter3) == null) {
                                    self.isDisabledParameterSec3(true);
                                    self.secondparameter3NameLBlRep("");
                                    $(".sec-para-rep-display3").css("display", "none");
                                    $(".sec-para-display3").css("display", "inline");
                                } else {

                                }
                            }


                            {
                                if ((searchParameters[i].parameter4) != null) {
                                    self.isDisabledParameterSec4(false);
                                    self.secondparameter4NameLBlRep(searchParameters[i].parameter4);
                                    $(".sec-para-rep-display4").css("display", "inline");
                                    $(".sec-para-display4").css("display", "none");

                                } else if ((searchParameters[i].parameter4) == null) {
                                    self.isDisabledParameterSec4(true);
                                    self.secondparameter4NameLBlRep("");
                                    $(".sec-para-rep-display4").css("display", "none");
                                    $(".sec-para-display4").css("display", "inline");
                                } else {

                                }
                            }


                            {
                                if ((searchParameters[i].parameter5) != null) {
                                    self.isDisabledParameterSec5(false);
                                    self.secondparameter5NameLBlRep(searchParameters[i].parameter5);
                                    $(".sec-para-rep-display5").css("display", "inline");
                                    $(".sec-para-display5").css("display", "none");

                                } else if ((searchParameters[i].parameter5) == null) {
                                    self.isDisabledParameterSec5(true);
                                    self.secondparameter5NameLBlRep("");
                                    $(".sec-para-rep-display5").css("display", "none");
                                    $(".sec-para-display5").css("display", "inline");
                                } else {

                                }
                            }

                        }
                    }
                };
                self.handlerChangeParameters = function () {
                    self.getReportParameters();
                };

                self.handlerChangeParametersSec = function () {
                    self.getReportParameters2();
                };

                
                self.isHaveFuture = ko.observable();

                self.currentStepValue = ko.observable('stp1');

//
//                self.currentStepValueText = function () {
//                    return self.jobTitel();
//                };

                self.currentStepValueText = function () {


                    if (self.currentStepValue() === 'stp2' && self.serviceType() != 'VIEW') {
                        self.isDisabled(true);
                        self.isDisabledParameter(true);
                        self.isDisabledParameter2(true);
                        self.isDisabledParameter3(true);
                        self.isDisabledParameter4(true);
                        self.isDisabledParameter5(true);
                        self.isDisabledParameterSec(true);
                        self.isDisabledParameterSec2(true);
                        self.isDisabledParameterSec3(true);
                        self.isDisabledParameterSec4(true);
                        self.isDisabledParameterSec5(true);
                        self.addBtnVisible(true);
                        self.nextBtnVisible(false);


                    } else if (self.serviceType() == 'VIEW') {
                        self.nextBtnVisible(false);
                        self.addBtnVisible(false);
                        self.isDisabled(true);
                        self.isDisabledParameter(true);
                        self.isDisabledParameter2(true);
                        self.isDisabledParameter3(true);
                        self.isDisabledParameter4(true);
                        self.isDisabledParameter5(true);
                        self.isDisabledParameterSec(true);
                        self.isDisabledParameterSec2(true);
                        self.isDisabledParameterSec3(true);
                        self.isDisabledParameterSec4(true);
                        self.isDisabledParameterSec5(true);


                    } else {
                        self.isDisabled(false);
                        self.getReportParameters();
                        self.getReportParameters2();
                        self.addBtnVisible(false);
                        self.nextBtnVisible(true);


                    }
                    return self.jobTitel();
                };


                self.previousStep = function () {
                    var prev = document.getElementById("train").getPreviousSelectableStep();
                    if (prev !== null)
                        self.currentStepValue(prev);

                    self.isDisabled(false);
                    self.getReportParameters();
                    self.getReportParameters2();
                    self.addBtnVisible(false);
                    self.nextBtnVisible(true);
                };


//                self.nextStep = function () {
//
//                    var tracker = document.getElementById("tracker");
//
//                    if (tracker.valid !== "valid")
//                    {
//                        tracker.showMessages();
//                        tracker.focusOn("@firstInvalidShown");
//
//                    } else {
//                        self.isDisabled(true);
//                        self.addBtnVisible(true);
//                        self.nextBtnVisible(false);
//                        var next = document.getElementById("train").getNextSelectableStep();
//                        if (next !== null)
//                            self.currentStepValue(next);
//                    }
//
//     
//     hasWhiteSpace(self.validationModle.validationValuesName()) ;
//     
//
//
//                };


                self.nextStep = function () {
                    //  var dates = $('#' + $('#datePicker').val()).calendarsPicker('getDate'); 

                    var tracker = document.getElementById("tracker");
                    var x;




//           if(self.validationModle.saveResultToValidationsValues()=="Yes"){
//               
//            hasWhiteSpace(self.validationModle.validationValuesName()) ;
//            function hasWhiteSpace(s) {
//                if( s.includes(' '))
//                {
//                    x=true;
//                    //alert("HAVE SPACE");
//                    //$(".disable-element").prop('disabled', false);
//                   // self._updateMessages(self.types.WARNING(), "Warning Summary Text", "Spaces; Don’t allowed.");
//                }
//                else {
//                    //$(".disable-element").prop('disabled', true);
//                }
// 
//            }   
//        }

                    function hasWhiteSpace(s) {
                        if (/\s/.test(s))
                        {
                        }
                    }

                    hasWhiteSpace(self.validationModle.validationValuesName());






                    if (tracker.valid !== "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        return;
                    } else if (x) {
                        $.notify(self.showhints(), "error");
//                self.messages([{
//                        severity: 'error',
//                        summary: 'Spaces; Don’t allowed in Validation Values Name.',
//                        detail: '',
//                        autoTimeout: 0
//                    }
//                       ]);
//                       
//self._updateMessages(self.types.WARNING(), "Warning Summary Text", "Spaces; Don’t allowed.");
                        return;
                    }
                    var next = document.getElementById("train").getNextSelectableStep();
                    if (next !== null)
                        self.currentStepValue(next);
                };
                self.stopSelectListener = function (event, ui) {
                    var tracker = document.getElementById("tracker");



                    if (tracker.valid != "valid")
                    {
                        tracker.showMessages();
                        tracker.focusOn("@firstInvalidShown");
                        event.preventDefault();
                        return;
                    }
                };








                /*******************/

                // for messagesCustom attribute

                self.types = {
                    FATAL: ko.observable("fatal"),
                    ERROR: ko.observable("error"),
                    WARNING: ko.observable("warning"),
                    INFO: ko.observable("info"),
                    CONFIRMATION: ko.observable("confirmation")

                };

                self.appMessages = ko.observable();
                // compute messages observable based on the current value for severityType

                self._updateMessages = function (newValue, summary, detail)
                {
                    var msgs = [];
                    if (summary && detail)
                    {
                        msgs.push({summary: summary, detail: detail, severity: newValue});
                    }
                    self.appMessages(msgs);
                };

////******************
                self.cancelAction = function () {
                    history.go(-1);
                };

                self.submitButton = function () {

                    document.querySelector('#yesNoDialog').open();
                    // self.callTransaction();
                };

                self.commitAdd = function () {
//                   self.authonticateValidation();
                    self.callTransaction();
                    document.querySelector("#yesNoDialog").close();
                    oj.Router.rootInstance.go('validationScreen');
//     
                };
                self.cancelButton = function () {
                    document.querySelector("#yesNoDialog").close();
                };


                self.reset = function () {
                    clearContent();
                };
                function clearContent() {
                }

                var messageDetailString = 'No Space';
                self.hasWhiteSpace = ko.pureComputed(function () {
                    return [{
                            type: 'regExp',
                            options: {
                                pattern: '/^\s+$/',
                                hint: 'enter Value name without a space.',
                                messageDetail: messageDetailString}}];
                });

// Start new 
                self.eitCodeLbl = ko.observable();
                self.firstKeyTypeLbl = ko.observable();
                self.firstKeyValueLbl = ko.observable();
                self.applicationValueFamilyLbl = ko.observable();
                self.applicationKeyLbl = ko.observable();
                self.saaSSourceLbl = ko.observable();
                self.reportNameLbl = ko.observable();
                self.parameter1NameLBl = ko.observable();
                self.parameter2NameLBl = ko.observable();
                self.parameter3NameLBl = ko.observable();
                self.parameter4NameLBl = ko.observable();
                self.parameter5NameLBl = ko.observable();
                self.parameter1NameLBlRep = ko.observable();
                self.parameter2NameLBlRep = ko.observable();
                self.parameter3NameLBlRep = ko.observable();
                self.parameter4NameLBlRep = ko.observable();
                self.parameter5NameLBlRep = ko.observable();
                self.saaSQueryLbl = ko.observable();
                self.paaSQueryLbl = ko.observable();
                self.operationLbl = ko.observable();

                // Second Start

                self.secondKeyTypeLbl = ko.observable();
                self.secondKeyValueLbl = ko.observable();
                self.secondapplicationValueFamilyLbl = ko.observable();
                self.secondapplicationKeyLbl = ko.observable();
                self.secondsaaSSourceLbl = ko.observable();
                self.secondreportNameLbl = ko.observable();
                self.secondparameter1NameLBl = ko.observable();
                self.secondparameter2NameLBl = ko.observable();
                self.secondparameter3NameLBl = ko.observable();
                self.secondparameter4NameLBl = ko.observable();
                self.secondparameter5NameLBl = ko.observable();
                self.secondparameter1NameLBlRep = ko.observable();
                self.secondparameter2NameLBlRep = ko.observable();
                self.secondparameter3NameLBlRep = ko.observable();
                self.secondparameter4NameLBlRep = ko.observable();
                self.secondparameter5NameLBlRep = ko.observable();
                self.secondsaaSQueryLbl = ko.observable();
                self.secondpaaSQueryLbl = ko.observable();

                // Second  End

                self.typeOFActionLbl = ko.observable();
                self.errorMassageEnLbl = ko.observable();
                self.errorMassageArLbl = ko.observable();
                self.setValueOfEITLbl = ko.observable();
                self.valueLbl = ko.observable();
                self.saveResultToValidationsValuesLbl = ko.observable();
                self.validationValuesNameLbl = ko.observable();
                self.actionType = ko.observable();
                self.newValueLbl = ko.observable();
                self.EITSegmentLbl = ko.observable();
                self.errorMassageARLbl = ko.observable();



//                self.EITCodeArr = ko.observableArray([
//                    {"value": 'Value 1', "label": 'EIT 1'},
//                    {"value": 'Value 2', "label": 'EIT 2'},
//                    {"value": 'Value 3', "label": 'EIT 3'}]);

                self.firstKeyTypeArr = ko.observableArray([
//                    {"value": 'Application_Value', "label": 'Application_Value'},
//                    {"value": 'Static', "label": 'Static'},
//                    {"value": 'SaaS', "label": 'SaaS'},
//                    {"value": 'PaaS', "label": 'PaaS'}
                ]);

                self.applicationValueFamilyArr = ko.observableArray([
//                    {"value": 'EIT', "label": 'EIT'},
//                    {"value": 'Personal_Information', "label": 'Personal_Information'},
//                    {"value": 'Validation_Value', "label": 'Validation_Value'}
                ]);


//                self.applicationKeyArr = ko.observableArray([
//                    {"value": 'Value 1', "label": 'EIT'},
//                    {"value": 'Value 2', "label": 'Personal Information'},
//                    {"value": 'Value 3', "label": 'Validation Value'}
//                ]);

                self.saaSSourceArr = ko.observableArray([
//                    {"value": 'Report', "label": 'Report'},
//                    {"value": 'Query', "label": 'Query'}
                ]);

//                self.reportNameArr = ko.observableArray([
//                    {"value": 'Reports Screen', "label": 'Reports Screen'}
//                ]);

                self.parameterNameArr = ko.observableArray([
                    {"value": 'EIT_CODE', "label": 'Eit Code'}
                ]);

                self.operationArr = ko.observableArray([
//                    {"value": '>', "label": '>'},
//                    {"value": '<', "label": '<'},
//                    {"value": '>=', "label": '>='},
//                    {"value": '<=', "label": '<='},
//                    {"value": '==', "label": '=='},
//                    {"value": '!=', "label": '!='},
//                    {"value": '===', "label": '==='}
                ]);


                self.typeOFActionArr = ko.observableArray([
//                    {"value": 'Error_Massage', "label": 'Error_Massage'}
//                    ,
//                    {"value": 'Action', "label": 'Action'}
                ]);
                // Seconds Start 



                self.secondKeyTypeArr = ko.observableArray([
//                    {"value": 'Application_Value', "label": 'Application Value'},
//                    {"value": 'Static', "label": 'Static'},
//                    {"value": 'SaaS', "label": 'SaaS'},
//                    {"value": 'PaaS', "label": 'PaaS'}
                ]);

                self.secondapplicationValueFamilyArr = ko.observableArray([
//                    {"value": 'EIT', "label": 'EIT'},
//                    {"value": 'Personal_Information', "label": 'Personal Information'},
//                    {"value": 'Validation_Value', "label": 'Validation Value'}
                ]);


//                self.secondapplicationKeyArr = ko.observableArray([
//                    {"value": 'Value 1', "label": 'EIT'},
//                    {"value": 'Value 2', "label": 'Personal Information'},
//                    {"value": 'Value 3', "label": 'Validation Value'}
//                ]);

                self.secondsaaSSourceArr = ko.observableArray([
//                    {"value": 'Report', "label": 'Report'},
//                    {"value": 'Query', "label": 'Query'}
                ]);

//                self.secondreportNameArr = ko.observableArray([
//                    {"value": 'Reports Screen', "label": 'Reports Screen'}
//                ]);

//                self.secondparameterNameArr = ko.observableArray([
//                    {"value": 'EIT Report', "label": 'EIT Report'}
//                ]);

                self.actionTypeArr = ko.observableArray([]);
                self.typeOfValueArr = ko.observableArray([]);
                self.typeOfFristValueArr = ko.observableArray([]);
                self.fristValuesArr = ko.observableArray([]);
                self.secondOperationArr = ko.observableArray([]);
                self.secondValueArr = ko.observableArray([]);

                // Second First

                self.setValueOfEITArr = ko.observableArray([
                    {"value": 'EIT Fields', "label": 'EIT Fields'}
                ]);



                self.saveResultToValidationsValuesArr = ko.observableArray([
//                    {"value": 'Yes', "label": 'Yes'},
//                    {"value": 'No', "label": 'No'}
                ]);

                self.validationModle = ko.observable();


                self.disableSubmit = ko.observable(false);
                self.jobName = ko.observable();
                self.addValidation = ko.observable();
                self.updateValidation = ko.observable();
                self.viewValidation = ko.observable();
                self.jobTitel = ko.observable();
                self.trainView = ko.observable();
                self.confirmMessage = ko.observable();
                self.addMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.pervious = ko.observable();
                self.next = ko.observable();
                self.cancel = ko.observable();
                self.submit = ko.observable();
                self.create = ko.observable();
                self.review = ko.observable();
                self.ok = ko.observable();
                self.oprationMessage = ko.observable();
                self.addMassageLbl = ko.observable();
                self.editMassageLbl = ko.observable();
                self.placeholder = ko.observable();
                self.serviceType = ko.observable();
                self.ministryJobModle = ko.observable();
                self.groupValid = ko.observable();
                self.jnVisibility = ko.observable();
                self.jnVisibility = ko.observable();
                self.trainVisibility = ko.observable();
                self.tracker = ko.observable();
                self.isDisabled = ko.observable(false);
                self.isDisabledParameter = ko.observable(false);
                self.isDisabledParameter2 = ko.observable(false); 
                self.isDisabledParameter3 = ko.observable(false); 
                self.isDisabledParameter4 = ko.observable(false); 
                self.isDisabledParameter5 = ko.observable(false);
                self.isDisabledParameterSec = ko.observable(false);
                self.isDisabledParameterSec2 = ko.observable(false);
                self.isDisabledParameterSec3 = ko.observable(false);
                self.isDisabledParameterSec4 = ko.observable(false);
                self.isDisabledParameterSec5 = ko.observable(false);
                self.nextBtnVisible = ko.observable(true);
                self.addBtnVisible = ko.observable(false);
                self.columnArray = ko.observableArray();
                self.isRequired = ko.observable(true);
                self.stepArray = ko.observableArray([]);
                self.messages = ko.observable();
                self.actiontype = ko.observable();
                self.newValue = ko.observable();
                self.eitSegment = ko.observable();
                self.TypeOfValueLbl = ko.observable();
                self.typeOfFristValueLbl = ko.observable();
                self.typeOfFristValue = ko.observable();
                self.typeOfSecondValue = ko.observable();
                self.secondValue = ko.observable();
                self.fristValuesLbl = ko.observable();
                self.fristValues = ko.observable();
                self.secondOperation = ko.observable();
                self.secondOperationLbl = ko.observable();
                self.typeOfSecondValueLbl = ko.observable();
                self.secondValuesLbl = ko.observable();


                self.validationModle = {
                    eitCode: ko.observable(),
                    firstKeyType: ko.observable(),
                    firstKeyValue: ko.observable(),
                    applicationValueFamily: ko.observable(),
                    applicationKey: ko.observable(),
                    saaSSource: ko.observable(),
                    reportName: ko.observable(),
                    parameter1Name: ko.observable(),
                    parameter2NameTOSASSQuery: ko.observable(),
                    parameter3NameTOSASSQuery: ko.observable(),
                    parameter2Name: ko.observable(),
                    parameter3Name: ko.observable(),
                    parameter4Name: ko.observable(),
                    parameter5Name: ko.observable(),
                    saaSQuery: ko.observable(),
                    paaSQuery: ko.observable(),
                    operation: ko.observable(),
                    secondKeyType: ko.observable(),
                    secondKeyValue: ko.observable(),
                    secondapplicationValueFamily: ko.observable(),
                    secondapplicationKey: ko.observable(),
                    secondsaaSSource: ko.observable(),
                    secondreportName: ko.observable(),
                    secondparameter1Name: ko.observable(),
                    secondparameter2Name: ko.observable(),
                    secondparameter3Name: ko.observable(),
                    secondparameter4Name: ko.observable(),
                    secondparameter5Name: ko.observable(),
                    secondsaaSQuery: ko.observable(),
                    secondpaaSQuery: ko.observable(),
                    typeOFAction: ko.observable(),
                    errorMassageEn: ko.observable(),
                    errorMassageAr: ko.observable(),
                    setValueOfEIT: ko.observable(),
                    value: ko.observable(),
                    saveResultToValidationsValues: ko.observable(),
                    validationValuesName: ko.observable(),
                    actiontype: ko.observable(),
                    newValue: ko.observable(),
                    eitSegment: ko.observable(),
                    typeOfValue: ko.observable(),
                    typeOfFristValue: ko.observable(),
                    fristValues: ko.observable(),
                    secondOperation: ko.observable(),
                    typeOfSecondValue: ko.observable(),
                    secondValue: ko.observable(),
//                    parameter1NameTOSASSQuery: ko.observable()

                };




                self.handleAttached = function (info) {
                    app.loading(false);

                    self.getPersonDetailsCode("personalVal");
                    self.getPersonDetailsCode("SecondpersonalVal");
                    self.retrieve = oj.Router.rootInstance.retrieve();

                    self.isRequired = ko.observable(true);
                    if (self.retrieve === 'add') {
                        self.serviceType('ADD');
                        self.oprationMessage(self.addMassageLbl());
                        self.jnVisibility(false);
                        self.trainVisibility(true);




                        self.jobName(self.ministryJobModle.jobName);
                    } else if (self.retrieve.type === 'update') {
                        self.serviceType('EDIT');
                        self.oprationMessage(self.editMassageLbl());
                        self.jnVisibility(true);
                        self.trainVisibility(true);
                        self.geteitCode("thirdEit");
                        self.geteitCode("FourthdEit");
                        self.validationModle = {
                            id: ko.observable(self.retrieve.id),
                            eitCode: ko.observable(self.retrieve.eitCode),
                            firstKeyType: ko.observable(self.retrieve.firstKeyType),
                            firstKeyValue: ko.observable(self.retrieve.firstKeyValue),
                            applicationValueFamily: ko.observable(self.retrieve.applicationValueFamily),
                            applicationKey: ko.observable(self.retrieve.applicationKey),
                            saaSSource: ko.observable(self.retrieve.saaSSource),
                            reportName: ko.observable(self.retrieve.reportName),
                            parameter1Name: ko.observable(self.retrieve.parameter1Name),
//                            parameter1NameTOSASSQuery: ko.observable(self.retrieve.parameter1NameTOSASSQuery),
//                            parameter2NameTOSASSQuery: ko.observable(self.retrieve.parameter2NameTOSASSQuery),
//                            parameter3NameTOSASSQuery: ko.observable(self.retrieve.parameter3NameTOSASSQuery),
                            parameter2Name: ko.observable(self.retrieve.parameter2Name),
                            parameter3Name: ko.observable(self.retrieve.parameter3Name),
                            parameter4Name: ko.observable(self.retrieve.parameter4Name),
                            parameter5Name: ko.observable(self.retrieve.parameter5Name),
                            saaSQuery: ko.observable(self.retrieve.saaSQuery),
                            paaSQuery: ko.observable(self.retrieve.paaSQuery),
                            operation: ko.observable(self.retrieve.operation),
                            secondKeyType: ko.observable(self.retrieve.secondKeyType),
                            secondKeyValue: ko.observable(self.retrieve.secondKeyValue),
                            secondapplicationValueFamily: ko.observable(self.retrieve.secondapplicationValueFamily),
                            secondapplicationKey: ko.observable(self.retrieve.secondapplicationKey),
                            secondsaaSSource: ko.observable(self.retrieve.secondsaaSSource),
                            secondreportName: ko.observable(self.retrieve.secondreportName),
                            secondparameter1Name: ko.observable(self.retrieve.secondparameter1Name),
                            secondparameter2Name: ko.observable(self.retrieve.secondparameter2Name),
                            secondparameter3Name: ko.observable(self.retrieve.secondparameter3Name),
                            secondparameter4Name: ko.observable(self.retrieve.secondparameter4Name),
                            secondparameter5Name: ko.observable(self.retrieve.secondparameter5Name),
                            secondsaaSQuery: ko.observable(self.retrieve.secondsaaSQuery),
                            secondpaaSQuery: ko.observable(self.retrieve.secondpaaSQuery),
                            typeOFAction: ko.observable(self.retrieve.typeOFAction),
                            errorMassageEn: ko.observable(self.retrieve.errorMassageEn),
                            errorMassageAr: ko.observable(self.retrieve.errorMassageAr),
                            setValueOfEIT: ko.observable(self.retrieve.setValueOfEIT),
                            value: ko.observable(self.retrieve.value),
                            saveResultToValidationsValues: ko.observable(self.retrieve.saveResultToValidationsValues),
                            validationValuesName: ko.observable(self.retrieve.validationValuesName),
                            actiontype: ko.observable(self.retrieve.actiontype),
                            newValue: ko.observable(self.retrieve.newValue),
                            eitSegment: ko.observable(self.retrieve.eitSegment),
                            typeOfValue: ko.observable(self.retrieve.typeOfValue),
                            typeOfFristValue: ko.observable(self.retrieve.typeOfFristValue),
                            fristValues: ko.observable(self.retrieve.fristValues),
                            secondOperation: ko.observable(self.retrieve.secondOperation),
                            typeOfSecondValue: ko.observable(self.retrieve.typeOfSecondValue),
                            secondValue: ko.observable(self.retrieve.secondValue)

                        };

                        self.handelApplicationKey();
                        self.SegmintEit(self.retrieve.eitCode);

                    } else if (self.retrieve.type === 'view') {
                        self.serviceType('VIEW');
                        self.currentStepValue('stp2');
                        self.geteitCode("thirdEit");
                        self.geteitCode("FourthdEit");
                        self.jnVisibility(true);
                        self.trainVisibility(false);
                        self.isDisabled(true);
                        self.isDisabledParameter(true);
                        self.isDisabledParameter2(true);
                        self.isDisabledParameter3(true);
                        self.isDisabledParameter4(true);
                        self.isDisabledParameter5(true);
                        self.isDisabledParameterSec(true);
                        self.isDisabledParameterSec2(true);
                        self.isDisabledParameterSec3(true);
                        self.isDisabledParameterSec4(true);
                        self.isDisabledParameterSec5(true);
                        self.addBtnVisible(false);
                        self.nextBtnVisible(false);


                        self.validationModle = {
                            eitCode: ko.observable(self.retrieve.eitCode),
                            firstKeyType: ko.observable(self.retrieve.firstKeyType),
                            firstKeyValue: ko.observable(self.retrieve.firstKeyValue),
                            applicationValueFamily: ko.observable(self.retrieve.applicationValueFamily),
                            applicationKey: ko.observable(self.retrieve.applicationKey),
                            saaSSource: ko.observable(self.retrieve.saaSSource),
                            reportName: ko.observable(self.retrieve.reportName),
                            parameter1Name: ko.observable(self.retrieve.parameter1Name),
//                            parameter1NameTOSASSQuery: ko.observable(self.retrieve.parameter1NameTOSASSQuery),
//                            parameter2NameTOSASSQuery: ko.observable(self.retrieve.parameter2NameTOSASSQuery),
//                            parameter3NameTOSASSQuery: ko.observable(self.retrieve.parameter3NameTOSASSQuery),
                            parameter2Name: ko.observable(self.retrieve.parameter2Name),
                            parameter3Name: ko.observable(self.retrieve.parameter3Name),
                            parameter4Name: ko.observable(self.retrieve.parameter4Name),
                            parameter5Name: ko.observable(self.retrieve.parameter5Name),
                            saaSQuery: ko.observable(self.retrieve.saaSQuery),
                            paaSQuery: ko.observable(self.retrieve.paaSQuery),
                            operation: ko.observable(self.retrieve.operation),
                            secondKeyType: ko.observable(self.retrieve.secondKeyType),
                            secondKeyValue: ko.observable(self.retrieve.secondKeyValue),
                            secondapplicationValueFamily: ko.observable(self.retrieve.secondapplicationValueFamily),
                            secondapplicationKey: ko.observable(self.retrieve.secondapplicationKey),
                            secondsaaSSource: ko.observable(self.retrieve.secondsaaSSource),
                            secondreportName: ko.observable(self.retrieve.secondreportName),
                            secondparameter1Name: ko.observable(self.retrieve.secondparameter1Name),
                            secondparameter2Name: ko.observable(self.retrieve.secondparameter2Name),
                            secondparameter3Name: ko.observable(self.retrieve.secondparameter3Name),
                            secondparameter4Name: ko.observable(self.retrieve.secondparameter4Name),
                            secondparameter5Name: ko.observable(self.retrieve.secondparameter5Name),
                            secondsaaSQuery: ko.observable(self.retrieve.secondsaaSQuery),
                            secondpaaSQuery: ko.observable(self.retrieve.secondpaaSQuery),
                            typeOFAction: ko.observable(self.retrieve.typeOFAction),
                            errorMassageEn: ko.observable(self.retrieve.errorMassageEn),
                            errorMassageAr: ko.observable(self.retrieve.errorMassageAr),
                            setValueOfEIT: ko.observable(self.retrieve.setValueOfEIT),
                            value: ko.observable(self.retrieve.value),
                            saveResultToValidationsValues: ko.observable(self.retrieve.saveResultToValidationsValues),
                            validationValuesName: ko.observable(self.retrieve.validationValuesName),
                            actiontype: ko.observable(self.retrieve.actiontype),
                            newValue: ko.observable(self.retrieve.newValue),
                            eitSegment: ko.observable(self.retrieve.eitSegment),
                            typeOfValue: ko.observable(self.retrieve.typeOfValue),
                            typeOfFristValue: ko.observable(self.retrieve.typeOfFristValue),
                            fristValues: ko.observable(self.retrieve.fristValues),
                            secondOperation: ko.observable(self.retrieve.secondOperation),
                            typeOfSecondValue: ko.observable(self.retrieve.typeOfSecondValue),
                            secondValue: ko.observable(self.retrieve.secondValue)


                        };
                        self.handelApplicationKey();
                        self.SegmintEit(self.retrieve.eitCode);
                    } 
                    var arr = JSON.parse(sessionStorage.eitName).map(e => ({label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE}));
                    self.EITCodeArr(arr);
                    self.getReportNames();
                    self.geteitCode("other");
                    self.getPersonDetailsCode("other");
                    self.getReportParameters();
                   self.getReportParameters2();
                };
                self.yeslabel=ko.observable();
                self.Nolabel=ko.observable();
                var getTranslation = oj.Translations.getTranslatedString;
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        self.stepArray([{label: self.create(), id: 'stp1'},
                            {label: self.review(), id: 'stp2'}]);
                        initTranslations();
                    }
                });
                function initTranslations() {

                    self.firstKeyTypeArr(app.getPaaSLookup('FIRST_KEY_TYPE'));
                    self.secondKeyTypeArr(app.getPaaSLookup('FIRST_KEY_TYPE'));
                    self.applicationValueFamilyArr(app.getPaaSLookup('APP_VALUE_FAMILY'));
                    self.secondapplicationValueFamilyArr(app.getPaaSLookup('APP_VALUE_FAMILY'));
                    self.saaSSourceArr(app.getPaaSLookup('SAAS_SOURCE'));
                    self.secondsaaSSourceArr(app.getPaaSLookup('SAAS_SOURCE'));
                    self.typeOFActionArr(app.getPaaSLookup('TYPE_OF_ACTION'));
                    self.saveResultToValidationsValuesArr(app.getPaaSLookup('SAVE_VAL_VALUES'));
                    self.operationArr(app.getPaaSLookup('OPERATION'));
                    self.actionTypeArr(app.getPaaSLookup('ACTION_TYPE'));
                    self.typeOfValueArr(app.getPaaSLookup('TYPE_OF_Value'));
                    self.typeOfFristValueArr(app.getPaaSLookup('TYPE_OF_FRIST_VALUE'));
                    self.secondOperationArr(app.getPaaSLookup('SECOND_OPERATION'));


                    self.retrieve = oj.Router.rootInstance.retrieve();
                    if (self.retrieve === 'add') {
                        self.jobTitel(getTranslation("validation.addValidation"));
                    } else if (self.retrieve.type === 'update') {
                        self.jobTitel(getTranslation("validation.editValidation"));
                    } else if (self.retrieve.type === 'view') {
                        self.jobTitel(getTranslation("validation.viewValidation"));
                    }

                    self.eitCodeLbl(getTranslation("validation.eitCode"));
                    self.firstKeyTypeLbl(getTranslation("validation.firstKeyType"));
                    self.firstKeyValueLbl(getTranslation("validation.firstKeyValue"));
                    self.applicationValueFamilyLbl(getTranslation("validation.applicationValueFamily"));
                    self.applicationKeyLbl(getTranslation("validation.applicationKey"));
                    self.saaSSourceLbl(getTranslation("validation.saaSSource"));
                    self.reportNameLbl(getTranslation("validation.reportName"));
                    self.parameter1NameLBl(getTranslation("validation.parameter1Name"));
                    self.parameter2NameLBl(getTranslation("validation.parameter2Name"));
                    self.parameter3NameLBl(getTranslation("validation.parameter3Name"));
                    self.parameter4NameLBl(getTranslation("validation.parameter4Name"));
                    self.parameter5NameLBl(getTranslation("validation.parameter5Name"));
                    self.saaSQueryLbl(getTranslation("validation.saaSQuery"));
                    self.paaSQueryLbl(getTranslation("validation.paaSQuery"));
                    self.operationLbl(getTranslation("validation.operation"));
                    self.secondKeyTypeLbl(getTranslation("validation.secondKeyType"));
                    self.secondKeyValueLbl(getTranslation("validation.secondKeyValue"));
                    self.secondapplicationValueFamilyLbl(getTranslation("validation.secondapplicationValueFamily"));
                    self.secondapplicationKeyLbl(getTranslation("validation.secondapplicationKey"));
                    self.secondsaaSSourceLbl(getTranslation("validation.secondsaaSSource"));
                    self.secondreportNameLbl(getTranslation("validation.secondreportName"));
                    self.secondparameter1NameLBl(getTranslation("validation.secondparameter1Name"));
                    self.secondparameter2NameLBl(getTranslation("validation.secondparameter2Name"));
                    self.secondparameter3NameLBl(getTranslation("validation.secondparameter3Name"));
                    self.secondparameter4NameLBl(getTranslation("validation.secondparameter4Name"));
                    self.secondparameter5NameLBl(getTranslation("validation.secondparameter5Name"));
                    self.secondsaaSQueryLbl(getTranslation("validation.secondsaaSQuery"));
                    self.secondpaaSQueryLbl(getTranslation("validation.secondpaaSQuery"));
                    self.typeOFActionLbl(getTranslation("validation.typeOFAction"));
                    self.errorMassageEnLbl(getTranslation("validation.errorMassageEn"));
                    self.errorMassageArLbl(getTranslation("validation.errorMassageAr"));
                    self.setValueOfEITLbl(getTranslation("validation.setValueOfEIT"));
                    self.valueLbl(getTranslation("validation.value"));
                    self.saveResultToValidationsValuesLbl(getTranslation("validation.saveResultToValidationsValues"));
                    self.validationValuesNameLbl(getTranslation("validation.validationValuesName"));
                    self.addValidation(getTranslation("validation.addValidation"));
                    self.updateValidation(getTranslation("validation.editValidation"));
                    self.viewValidation(getTranslation("validation.viewValidation"));
                    self.addMassageLbl(getTranslation("validation.addValidationMessage"));
                    self.editMassageLbl(getTranslation("validation.editValidationMessage"));
                    self.trainView(getTranslation("others.review"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.pervious(getTranslation("others.pervious"));
                    self.next(getTranslation("others.next"));
                    self.cancel(getTranslation("others.cancel"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.submit(getTranslation("others.submit"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.create(getTranslation("labels.create"));
                    self.review(getTranslation("others.review"));
                    self.ok(getTranslation("others.ok"));
                    self.showhints(getTranslation("common.showhint"));
                    self.actionType(getTranslation("others.actionType"));
                    self.newValueLbl(getTranslation("others.newValue"));
                    self.EITSegmentLbl(getTranslation("others.EITSegment"));
                    self.TypeOfValueLbl(getTranslation("others.TypeOfValue"));
                    self.typeOfFristValueLbl(getTranslation("others.typeOfFristValue"));
                    self.fristValuesLbl(getTranslation("others.fristvalues"));
                    self.secondOperationLbl(getTranslation("others.secondOperation"));
                    self.typeOfSecondValueLbl(getTranslation("others.typeOfSecondValue"));
                    self.secondValuesLbl(getTranslation("others.secondValues"));
                    self.errorMassageARLbl(getTranslation("others.errorMassageAR"));
                    self.errorMessage(getTranslation("others.errorMassage"));
                    self.addToSassQuery(getTranslation("others.addToSassQuery"));
                    self.yeslabel(getTranslation("others.yeslabel"));
                    self.Nolabel(getTranslation("others.Nolabel"));

                    if (self.serviceType() === 'ADD') {
                        self.oprationMessage(self.addMassageLbl());
                    } else if (self.serviceType() === 'EDIT') {
                        self.oprationMessage(self.editMassageLbl());
                    }
                    self.stepArray([{label: self.jobTitel(), id: 'stp1'},
                        {label: self.trainView(), id: 'stp2'}
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
