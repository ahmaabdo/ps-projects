/**
 * 
 * @param {type} oj
 * @param {type} ko
 * @param {type} $
 * @param {type} app
 * @param {type} commonUtil
 * @param {type} services
 * @param {type} translatehelper
 * @returns {JobL#13.JobContentViewModel}
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojinputtext', 'ojs/ojknockout-validation', 'ojs/ojselectcombobox', 'ojs/ojdatetimepicker','ojs/ojtable',
    'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup', 'ojs/ojcheckboxset', 'ojs/ojradioset',
    'ojs/ojlabel', 'ojs/ojgauge', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage', 'jquery-calendar',
    'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin',
    'jquery-calendar-picker', 'ojs/ojvalidationgroup', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function JobContentViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var deptArray = [];
                var pesonId = app.personDetails().personId;
                var globalCodeTOSelfServices;
                
                self.columnArray = ko.observableArray();
                self.Employeedata = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.Employeedata, {idAttribute: 'nationalId'}));
                self.isDisable = ko.observable(true);
                self.dataSource;
                self.roelsArray = [];
                self.selfServices = ko.observableArray([]);
                self.editDisabled = ko.observable(true);
                self.personSearch = ko.observable();
                self.name = ko.observable();
                self.personnumber = ko.observable();
                self.nationalId = ko.observable();
                self.effictiveDate = ko.observable();
                self.searchLbl = ko.observable();
                self.clearLbl = ko.observable();
                self.personNumberLbl = ko.observable();
                self.employeeNmaeLbl = ko.observable();
                self.NationalIdLbl = ko.observable();
                self.departmentLbl = ko.observable();
                self.positionLbl = ko.observable();
                self.locationLbl = ko.observable();
                self.actionLbl = ko.observable();
                self.goTo = ko.observable();
                self.insertValue = ko.observable();
                self.selfServiceType = ko.observable(true);
                self.EITCodeArr = ko.observableArray([]);
                self.checkUseRole=ko.observable("");
                self.checkUseRoleSpecialist=ko.observable("");
                self.issecretary=ko.observable(false);
                self.isspecialist=ko.observable(true);
                 self.placeholder=ko.observable();
                 self.personNameArr = ko.observableArray([]);
                // Start Search Method    
                function buildValueChange(valueParam) {
                    var valueObj = {};
                    valueObj.value = valueParam.value;
                    return valueObj;
                }

                self.PersonChangeHandler = function (event) {
                    var valueObj = buildValueChange(event['detail']);
                    globalCodeTOSelfServices = valueObj.value;
                };
//                


                var getPersonDetailsCbFn = function (data) {
                    self.Employeedata.push({
                                    assignmentName: data.assignmentName, assignmentProjectedEndDate: data.assignmentProjectedEndDate, assignmentStatus: data.assignmentStatus,
                                    assignmentStatusTypeId: data.assignmentStatusTypeId, carAllowance: data.carAllowance, citizenshipLegislationCode: data.citizenshipLegislationCode, nationalId: (data.nationalId === 'null' || data.nationalId === null) ? '' : data.nationalId,
                                    city: data.city, country: data.country, dateOfBirth: data.dateOfBirth, department: data.department, displayName: data.displayName, personNumber: data.personNumber,
                                    email: data.email, employeeCertificate: data.employeeCertificate, employeeLocation: data.employeeLocation, fax: data.fax, faxExt: data.faxExt,
                                    grade: data.grade, gradeId: data.gradeId, hireDate: data.hireDate, housingType: data.housingType,
                                    jobId: data.jobId, jobName: data.jobName, legalEntityId: data.legalEntityId, managerId: data.managerId, managerName: data.managerName, managerOfManager: data.managerOfManager,
                                    managerOfMnagerName: data.managerOfMnagerName, managerType: data.managerType, maritalStatus: data.maritalStatus, ministryJobForEmployee: data.ministryJobForEmployee, mobileAllowance: data.mobileAllowance, mobileNumber: data.mobileNumber,
                                    organizationName: data.organizationName, overTimePaymentUrl: data.overTimePaymentUrl, overTimeURL: data.overTimeURL, peopleGroup: data.peopleGroup, personId: data.personId, picBase64: data.picBase64,
                                    positionName: data.positionName, probationPeriodEndDate: data.probationPeriodEndDate, projectedStartDate: data.projectedStartDate, region: data.region, salaryAmount: data.salaryAmount, transportationAlowance: data.transportationAlowance,
                                    workPhone: data.workPhone, workPhoneExt: data.workPhoneExt,assignmentNumber:data.assignmentNumber, employeeURL: data.employeeURL,departmentName:data.departmentName
                     });
              
                };

                self.searchPerson = function () {
                    self.Employeedata([]);            
                             if(self.checkUseRole()==="TBC Line Manager" && isEmpty(self.checkUseRoleSpecialist())){
                                 self.searchModel.managerId("");
                            services.searchEmployees(self.searchModel.name(), self.searchModel.nationalId(), self.searchModel.PersonNumber(), self.searchModel.managerId(), self.searchModel.effectiveDate()).then(getPersonDetailsCbFn, app.failCbFn); 
                        
                             }else if(self.checkUseRoleSpecialist()==="TBC HR Specialist (ER)" && isEmpty(self.checkUseRole())){
                             
                               self.searchModel.managerId("");
                               services.searchEmployees(self.searchModel.name(), self.searchModel.nationalId(), self.searchModel.PersonNumber(), self.searchModel.managerId(), self.searchModel.effectiveDate()).then(getPersonDetailsCbFn, app.failCbFn);
                        
                             }else if (self.checkUseRole()==="TBC Line Manager" && self.checkUseRoleSpecialist()==="TBC HR Specialist (ER)" ){
                              self.searchModel.managerId("");
                               services.searchEmployees(self.searchModel.name(), self.searchModel.nationalId(), self.searchModel.PersonNumber(), self.searchModel.managerId(), self.searchModel.effectiveDate()).then(getPersonDetailsCbFn, app.failCbFn);    
                                 
                             }             

                };


                self.gotoSelfService = function () {
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    var specialistData = {
                        checkUserSubmit: "ManagersubmitUser",
                        EIT: globalCodeTOSelfServices
                    };
                    oj.Router.rootInstance.store(specialistData);
                    app.globalspecialistEMP(self.Employeedata()[currentRow['rowIndex']]);
                    if (specialistData.EIT == "XXX_HANDOVER_REQUEST") {
                        oj.Router.rootInstance.go("handOverSummary");
                    }else if(specialistData.EIT == "XXX_EDU_EXPENSE_REQUEST"){
                        oj.Router.rootInstance.go("EducationExpenseSummary");
                    } else {
                        oj.Router.rootInstance.go("dynamicSummary");
                    }
                        
                };


                self.reset = function () {
                    clearContent();
                    self.Employeedata([]);
                };

                function clearContent() {
                    self.searchModel.name("");
                    self.searchModel.PersonNumber("");
                    self.searchModel.effectiveDate("");
                    self.searchModel.nationalId("");
                    self.editDisabled(true);

                }
                self.searchModel = ko.observable();
                self.searchModel = {
                    name: ko.observable(""),
                    PersonNumber: ko.observable(""),
                    generalGroup: ko.observable(""),
                    nationalId: ko.observable(""),
                    effectiveDate: ko.observable(""),
                    selfServiceType: ko.observable(""),
                    managerId: ko.observable(app.personDetails().personId)
                };

                self.tableSelectionListener = function (event) {
                    self.editDisabled(false);
                };

                self.handleAttached = function (info) {
                    var isOnlyManager = false;
                    self.roelsArray = [];
                    app.loading(false);
                     
                     self.EITCodeArr(app.globalEitNameReport());
                       
                       
                     var  userRoles = JSON.parse(sessionStorage.getItem("userRole"));
                     var  pushedEIT = []; 
                     for (var i=0 ; i<userRoles.length;i++){ 
                         self.roelsArray.push(userRoles[i].ROLE_NAME);
                         if ( userRoles[i].ROLE_NAME==="TBC Line Manager" ||
                                 userRoles[i].ROLE_NAME==="Line Manager"){
                             
                          isOnlyManager = true;
                            
                         }else if(userRoles[i].ROLE_NAME ==="TBC HR Specialist (ER)") {
                            self.checkUseRoleSpecialist("TBC HR Specialist (ER)");
                            isOnlyManager = false;
                            break;
                           
                         }
                     }
                     
                     if(isOnlyManager) {
                         self.checkUseRole("TBC Line Manager");
                             self.isspecialist(false);
                             self.issecretary(true);
                            var reportPaylod = {"reportName": "XXX_HR_GET_EMPLOYEE_DEPT", "personId": app.personDetails().personId};
                            var getRolesCBCF = function (data) {
                                for (var i = 0; i < data.length; i++) {
                                    self.personNameArr.push({
                                        value: data[i].PERSON_NUMBER,
                                        label: data[i].DISPLAY_NAME+"-"+data[i].PERSON_NUMBER
                                    });

                                };
                            };
                            services.getGenericReportAsync(reportPaylod).then(getRolesCBCF, self.failCbFn);
                     }
//                     if(self.roelsArray.indexOf('HHA-Line Manager Custom V3')!=-1){
//                       
//                        for (var i = 0; i < app.globalManagerLookup().length; i++) {
//                            if (pushedEIT.indexOf(app.globalManagerLookup()[i].value) == -1) {
//                                self.EITCodeArr.push({"value": app.globalManagerLookup()[i].value,
//                                    "label": searchArray(app.globalManagerLookup()[i].value, app.globalEitNameReport())});
//                                pushedEIT.push(app.globalManagerLookup()[i].value);
//                            }
//
//                        }
//                     } 
//                    if (self.roelsArray.indexOf('HHA PAAS Specialist Custom') != -1) {
//                        
//                        for (var i = 0; i < app.globalSpecialistLookup().length; i++) {
//                            if (self.roelsArray.indexOf(app.globalSpecialistLookup()[i].description) != -1) {
//                               
//                                var valueLabel = app.globalSpecialistLookup()[i].label.split(',');
//                                if(pushedEIT.indexOf(valueLabel[0])==-1){
//                                    self.EITCodeArr.push({"value": valueLabel[0],
//                                    "label": searchArray(valueLabel[0], app.globalEitNameReport())})
//                                pushedEIT.push(valueLabel[0]);
//                                }
//                                
//                            }
//                        }
//                    }
                };
                self.handleDetached = function (info) {
                    self.checkUseRole("");
                    self.checkUseRoleSpecialist("");
                    self.EITCodeArr(app.globalEitNameReport());
                    app.globalspecialistEMP();
                };
                //---------------------------Translate Section---------------------------------------------

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });
                
                function isEmpty(val) {
            return (val === undefined || val == null || val.length <= 0) ? true : false;
        }

                function initTranslations() {
                    self.personSearch(getTranslation("special.personSearch"));
                    self.name(getTranslation("common.name"));
                    self.personnumber(getTranslation("common.personNumber"));
                    self.nationalId(getTranslation("special.nationalId"));
                    self.effictiveDate(getTranslation("special.effictiveDate"));
                    self.searchLbl(getTranslation("special.search"));
                    self.clearLbl(getTranslation("special.clear"));
                    self.personNumberLbl(getTranslation("special.personNumberTh"));
                    self.employeeNmaeLbl(getTranslation("special.employeeNmaeTh"));
                    self.NationalIdLbl(getTranslation("special.NationalIdTh"));
                    self.departmentLbl(getTranslation("special.departmentTh"));
                    self.positionLbl(getTranslation("special.positionTh"));
                    self.locationLbl(getTranslation("special.locationTh"));
                    self.actionLbl(getTranslation("special.actionTh"));
                    self.goTo(getTranslation("special.goTo"));
                    self.insertValue(getTranslation("special.goTo"));
                    self.placeholder(getTranslation("labels.placeHolder"));
                    self.columnArray([
                        {
                            "headerText": self.personNumberLbl(), "field": "personNumber"
                        },
                        {
                            "headerText": self.employeeNmaeLbl(), "field": "displayName"
                        },
                        {
                            "headerText": self.NationalIdLbl(), "field": "nationalId"
                        },
                        {
                            "headerText": self.departmentLbl(), "field": "department"
                        },
                        {
                            "headerText": self.positionLbl(), "field": "positionName"
                        },
                        {
                            "headerText": self.locationLbl(), "field": "employeeLocation"
                        }
                    ]);
                }
                initTranslations();
            }
            return JobContentViewModel;
        });
