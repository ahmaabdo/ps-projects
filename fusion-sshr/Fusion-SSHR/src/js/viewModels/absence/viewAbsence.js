define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol',
    'ojs/ojpagingtabledatasource', 'ojs/ojselectcombobox', 'ojs/ojlabel', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojtable', 'ojs/ojbutton'],
        function (oj, ko, $, app, services, commonhelper) {

            function ViewAbsenceViewModel() {
                var self = this;
                var getTranslation = oj.Translations.getTranslatedString;
                var deptArray = [];
                var tempUrl = "https://ehxm-test.fa.us6.oraclecloud.com:443/hcmRestApi/resources/11.13.17.11/absences?q=personId='300000003655687'";

                self.deptObservableArray = ko.observableArray(deptArray);
                self.dataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.deptObservableArray, {idAttribute: 'objectVersionNumber'}));
                self.startDateVal = ko.observable();
                self.endDateVal = ko.observable();

                self.columnArray = ko.observableArray();
                self.startDateLbl = ko.observable();
                self.endDateLbl = ko.observable();
                self.durationLbl = ko.observable();
                self.absenceStatusLbl = ko.observable();
                self.approvalStatusLbl = ko.observable();
                self.search = ko.observable();
                self.checkResultSearch = ko.observable();
                self.absencesTitle = ko.observable();
                self.reset = ko.observable();


                var getAbsencesCbFn = function (data) {
                    if (data.length !== 0) {
                        self.deptObservableArray(data.items);
                    } else {
                        self.deptObservableArray([]);
                        //$.notify(self.checkResultSearch(), "error");
                    }
                };
                var failCbFn = function () {
                    //$.notify(self.checkResultSearch(), "error");
                };

                services.getAbsences(tempUrl, {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);

                self.resetDates = function () {
                    self.startDateVal('');
                    self.endDateVal('');
                }
                self.searchByDate = function () {
                    if (typeof self.startDateVal() == 'undefined') {
                        services.getAbsences(tempUrl + ";endDate='" + self.endDateVal() + "'", {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);
                    } else if (typeof self.endDateVal() == 'undefined') {
                        services.getAbsences(tempUrl + ";startDate='" + self.startDateVal() + "'", {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);
                    } else if (typeof self.startDateVal() == 'undefined' && typeof self.endDateVal() == 'undefined') {
                        services.getAbsences(tempUrl, {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);
                    } else {
                        services.getAbsences(tempUrl + ";startDate='" + self.startDateVal() + "';endDate='" + self.endDateVal() + "'", {"Authorization": app.tempAuthkey()}).then(getAbsencesCbFn, failCbFn);
                    }
                };

                self.tableSelectionListener = function (event) {

                };
                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.handleAttached = function (info) {
                    app.loading(false);
                    initTranslations();
                };

                function initTranslations() {
                    self.absencesTitle(getTranslation("pages.absences"));
                    self.reset(getTranslation("login.resetLabel"));
                    self.startDateLbl(getTranslation("job.startDate"));
                    self.endDateLbl(getTranslation("job.endDate"));
                    self.durationLbl(getTranslation("absence.duration"));
                    self.absenceStatusLbl(getTranslation("labels.status"));
                    self.approvalStatusLbl(getTranslation("absence.approval"));
                    self.search(getTranslation("others.search"));
                    self.checkResultSearch(getTranslation("common.checkResultSearch"));

                    self.columnArray([
                        {
                            "headerText": self.startDateLbl(), "field": "startDate"
                        },
                        {
                            "headerText": self.endDateLbl(), "field": "endDate"
                        },
                        {
                            "headerText": self.durationLbl(), "field": "duration"
                        },
                        {
                            "headerText": self.absenceStatusLbl(), "field": "absenceStatusCd"
                        },
                        {
                            "headerText": self.approvalStatusLbl(), "field": "approvalStatusCd"
                        }
                    ]);
                }

            }

            return new ViewAbsenceViewModel();
        }
);
