/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your customer ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'promise', 'ojs/ojtable', 'ojs/ojarraydataprovider',
    'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojlabel', 'ojs/ojbutton', 'ojs/ojchart', 'ojs/ojtoolbar',
    'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojdefer','ojs/ojtable','ojs/ojpagingcontrol','ojs/ojpagingtabledatasource', 'ojs/ojmodule','xlsx','moment','fullCalendar','ojs/ojselectcombobox'],
    function (oj, ko, $, app, ArrayDataProvider, commonUtil, services, postbox) {

        function TimeAndAbsence() {
            var self = this;
            var getTranslation = oj.Translations.getTranslatedString;
            this.requestDate = ko.observable(formatDateToday());
            this.calendarDate = ko.observable(formatDateToday());
            this.selectedItem = ko.observable("off");
            self.businessEventarr = ko.observableArray([

                    {
                        title: 'Independent Day',
                        start: '2020-06-01', 
                        end: '2020-06-03', // +1 to end on 2020-06-02
                        color: 'blue'
                    },
                    {
                        title: 'Eid Al-Adha',
                        start: '2020-06-05',
                        end: '2020-06-09',
                        color: 'green'
                    },
                    {
                        title: 'Eid Al-Fatar',
                        start: '2020-07-09',
                        end: '2020-07-15',
                        color: 'red'
                    }
            ])
            
            self.absenceEventarr = ko.observableArray([

                {
                    id: 1,
                    title: 'Sick Leave',
                    start: '2020-06-01', 
                    end: '2020-06-02', 
                    color: 'red'
                },
                {
                    id: 2,
                    title: 'Sick Leave',
                    start: '2020-07-06', 
                    end: '2020-07-10', 
                    color: 'red'
                }
        ])
            self.allAbsencearr = ko.observableArray();
            self.upComingarr = ko.observableArray()
            self.stackValue = ko.observable('on');
            self.stackLabelValue = ko.observable('on');
            self.orientationValue = ko.observable('horizontal');
            self.chartType = ko.observableArray([{value: 'vertical'},{value: 'horizontal'}])
            self.labelPosition = ko.observable('auto');
            self.currentDate = ko.observable(formatDateToday());
            this.dataProvider = new ArrayDataProvider([], {keyAttributes: 'id'})

            /******************************Start Chart Data************************************* */
            self.data = [
               {id : "5",series: "Entitlement", value: 15, label: "15",group :"Entitlement", date: "2020-06-10"},
               {id : "6",series: "Remaining", value: 2, label: "2",group :"Used", date: "2020-06-11"},
               {id : "7", series: "Carryover", value: 7, label: "7",group :"Entitlement", date: "2020-06-12"},
               {id : "8",series: "Used", value: 20, label: "20",group :"Used", date: "2020-06-13"} 
               
            ];


            var barGroups = ["Entitlement", "Used"];
            // self.barSeriesValue = ko.observableArray(self.data.filter(e => e.date == self.currentDate()));
            self.barSeriesValue = ko.observableArray(self.data)
            self.barGroupsValue = ko.observableArray(barGroups);
            ///////////////////////////////
            this.dataProvider = new ArrayDataProvider( self.data, {keyAttributes: 'id'});
      
            /*******************************Start Table*********************************/
            var deptArrayUpcoming = [{date: 3, upcoming: 'Upcoming absence '}];
            var deptArrayAll = [{date: 3, all: 'All absence '}];

            self.upComingAbsence = ko.observableArray(deptArrayUpcoming)
            self.allAbsence = ko.observableArray(deptArrayAll)
            // self.dataProviderUpcomig = new ArrayDataProvider(deptArrayUpcoming, {keyAttributes: 'date'});
            // self.dataProviderAll = new ArrayDataProvider(deptArrayAll, {keyAttributes: 'date'});

            self.dataProviderUpcomig = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.upComingAbsence, {keyAttributes: 'date'}));
            self.dataProviderAll = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.allAbsence, {keyAttributes: 'date'}));
            ////////////////////////////////////////////////////////////////////////////

            /******************** Start Date Format********************************** */
            function formatDateToday() {
                var nwday = new Date(),
                    month = '' + (nwday.getMonth() + 1),
                    day = '' + nwday.getDate(),
                    year = nwday.getFullYear();

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;

                return [year, month, day].join('-');
            }
            /*************************End********************************** */

            /*******************Start ADD Button******************************/
            self.addButton = function () {
            };
            /*********************End***********************************/

            /************************Start Select Date (chart) *************************/
            self.selectDate = function (event) {
                if (event && event.detail)
                    self.barSeriesValue(self.data.filter(e => e.date == event.detail.value))
            };
            /************************End*************************/

            this.datePickerWeek = {
                weekDisplay: 'number'
            };

            self.handleAttached = function (info) {
                app.loading(false);
                initTranslations();
                
            };

            /////////////////////calendar/////////////////////////
 
            /////////////////////////////////////////////////////
                self.timeAndAbsenceLbl = ko.observable();
                self.absenceBalanceLbl = ko.observable();
                self.balancesEffectiveAtLbl = ko.observable();
                self.selectChartTypeLbl = ko.observable();
                self.CalendarLbl = ko.observable();
                self.openAndUpcomingLbl = ko.observable();
                self.allLbl = ko.observable();
                self.andLbl = ko.observable();

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

          
            function initTranslations() {
                self.upComingarr([
                    {
                        "headerText": "upcoming Absence",
                        "field": "upcoming"
                    }
                ])

                self.allAbsencearr([
                    {
                        "headerText": "All Absence",
                        "field": "all"
                    }
                ])
                    self.timeAndAbsenceLbl(getTranslation("timeAndAbsence.timeAndAbsenceLbl"));
                    self.absenceBalanceLbl(getTranslation("timeAndAbsence.absenceBalanceLbl"));
                    self.balancesEffectiveAtLbl(getTranslation("timeAndAbsence.balancesEffectiveAtLbl"));
                    self.selectChartTypeLbl(getTranslation("timeAndAbsence.selectChartTypeLbl"));
                    self.CalendarLbl(getTranslation("timeAndAbsence.CalendarLbl"));
                    self.openAndUpcomingLbl(getTranslation("timeAndAbsence.openAndUpcomingLbl"));
                    self.allLbl(getTranslation("timeAndAbsence.allLbl"));
                    self.andLbl(getTranslation("timeAndAbsence.andLbl"));
                
                

            }
            

        }
        return new TimeAndAbsence();
    }
);
