define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojarraydataprovider', 'ojs/ojfilepicker'],
    function (oj, ko, $, app, commonUtil, services) {

        function BulkOperationViewModel() {
            var self = this;
            var selected_file = null;
            self.bulkData = ko.observable({});//{code, trans_name},

            self.title = ko.observable();
            self.picker_text = ko.observable();
            self.upload = ko.observable();
            self.download = ko.observable();


            self.selectListener = function (event) {
                var files = event.detail.files;
                selected_file = files[0];
                self.picker_text(selected_file.name);
            }


            self.acceptArr = ko.observableArray([]);
            self.acceptArr = ko.pureComputed(function () {
                var accept = ".xls,.xlsx";
                return accept ? accept.split(",") : [];
            }, self);


            self.uploadAction = function (e) {
                // app.loading(true)
                self.picker_text(app.translate("common.uploadingandprocessingdata"));
                if (selected_file != null) {
                    var formData = new FormData();
                    formData.append('file', selected_file);
                    setTimeout(() => {
                        services.addFile("bulk/upload", formData)
                            .then(e => {
                                try{e=JSON.parse(e)}catch(ignored){}
                                // app.loading(false)
                                self.picker_text(app.translate("common.click_or_dragdrop"));
                                selected_file = null;
                                if (e.status == "Done") {
                                    e=e.message;
                                    if (e === '0') {
                                        app.messages.push({ severity: "info", summary: app.translate("common.upload_file_done"), autoTimeout: 0 });//default time out
                                    } else if (e === '-1') {
                                        app.messages.push({ severity: "error", summary: app.translate("common.upload_file_error"), autoTimeout: 0 });//default time out
                                    } else {
                                        var link = document.createElement("a");
                                        document.body.appendChild(link);
                                        link.setAttribute("type", "hidden");
                                        link.href = "data:text/plain;base64," + e;
                                        link.download = self.title().replace(/ /g, "_") + ".xls";
                                        link.click();
                                        document.body.removeChild(link);
                                        app.messages.push({ severity: "error", summary: "please check downloaded file for not updated data", autoTimeout: 0 });//default time out
                                    }
                                }else{
                                    app.messages.push({ severity: "error", summary: e.message, autoTimeout: 0 });//default time out
                                }
                            },er=>{
                                // app.loading(false)
                            });
                    }, 100);
                } else {
                    app.messages.push({ severity: "error", summary: app.translate("common.upload_file_not_selected"), autoTimeout: 0 });//default time out
                }
                return true;
            };

            self.downloadAction = function (e) {
                app.loading(true)
                setTimeout(() => {
                    services.getGeneric("bulk/download/" + self.bulkData().code).then(res => {
                        app.loading(false)
                        if (res === '-1') {
                            return;
                        }
                        if (res === '-2') {
                            return;
                        }

                        var link = document.createElement("a");
                        document.body.appendChild(link);
                        link.setAttribute("type", "hidden");
                        link.href = "data:text/plain;base64," + res;
                        link.download = self.title().replace(/ /g, "_") + ".xls";
                        link.click();
                        document.body.removeChild(link);
                    });
                }, 100);
            };

            self.handleAttached = function (info) {
                self.bulkData(oj.Router.rootInstance.retrieve())
                app.loading(false);
                initTranslations();
            }


            ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }

                
            });

            function initTranslations() {
                self.upload(app.translate("common.upload"));
                self.download(app.translate("common.download"));
                self.title(app.translate(self.bulkData().trans_name));
                self.picker_text(app.translate("common.click_or_dragdrop"));
            }
            initTranslations();
        }

        return new BulkOperationViewModel();
    });
