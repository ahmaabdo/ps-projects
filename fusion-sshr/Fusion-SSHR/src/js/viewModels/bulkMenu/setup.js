define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojtrain', 'config/services',
    'util/commonhelper', 'knockout-mapping', 'ojs/ojarraydataprovider', 'ojs/ojmodel', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojtable', 'ojs/ojcheckboxset', 'ojs/ojinputtext', 'ojs/ojdialog','ojs/ojselectcombobox'],
    function (oj, ko, $, app, ojtrain, services, commonhelper, km, ArrayDataProvider) {
        function bulkSetupModule() {
            var self = this;
            self.title = ko.observable();
            self.add = ko.observable();
            self.back = ko.observable();
            self.delBtn = ko.observable();
            self.save = ko.observable();
            self.select_selfservice_lbl = ko.observable();
            self.EitsNotInListOpts = ko.observableArray([]);
            self.selectedAddEitCodeVal = ko.observableArray([]);
            self.enabledVal = ko.observableArray([]);
            self.disableDelete = ko.observable(true);

            self.backAction = function () {
                oj.Router.rootInstance.go('setup');
            };

            self.showAddDialogAction = function () {
                document.getElementById("addDialog").open();
            }

            self.columnArray = ko.observableArray([]);
            self.summaryObservableArray = ko.observableArray([]);
            self.dataSource = new oj.ArrayDataProvider(self.summaryObservableArray, { idAttribute: 'id' });

            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });


            function setData(arr) {
                arr = arr.map(code => {
                    var e = {};
                    e.NAME = (eitNames.find(ee => ee.DESCRIPTIVE_FLEX_CONTEXT_CODE == code) ||{} ).DESCRIPTIVE_FLEX_CONTEXT_NAME;
                    e.CODE = code;
                    e.Selected = ko.observable([])
                    return e;
                });
                self.summaryObservableArray(arr);

                var allEits = JSON.parse(sessionStorage.eitName);
                var usedEitsCodes = self.summaryObservableArray().map(e => e.CODE);
                self.EitsNotInListOpts(allEits
                    .map(e => { return { label: e.DESCRIPTIVE_FLEX_CONTEXT_NAME, value: e.DESCRIPTIVE_FLEX_CONTEXT_CODE } })
                    .filter(e => usedEitsCodes.indexOf(e.value) == -1));

            }
            self.handleAttached = function (info) {
                app.loading(true);
                eitNames = JSON.parse(sessionStorage.eitName);

                services.getGeneric("bulk/menu")
                    .then(data => {
                        app.loading(false);
                        var arr = JSON.parse(data);
                        setData(arr);
                    }, error => {
                        app.loading(false);
                    });

                initTranslations();
            };

            self.currentEditingCode = ko.observable('');

            self.onDelete = function (event) {
                var names = self.summaryObservableArray().filter(e => e.Selected().length > 0).map(e => e.NAME);
                app.confirmMsg(app.translate('bulksetup.delmsg').replace("#name#", names.join(", ")), ok => {
                    if (ok) {
                        app.loading(true);
                        setTimeout(() => {//show loading before screen freeze while deleting
                            codes = self.summaryObservableArray().filter(e => e.Selected().length > 0).map(e => e.CODE);
                            services.deleteGeneric('bulk/delMenu/' + codes, {}).then(response => {
                                try { response = JSON.parse(response) } catch (ignored) { }
                                app.loading(false);
                                if (response.status == 'Done') {
                                    arr = self.summaryObservableArray().filter(e => e.Selected().length == 0).map(e => e.CODE);
                                    setData(arr);
                                } else {
                                    app.messages.push({ severity: "error", summary: response.message, autoTimeout: 0 });//default time out
                                }
                            }, error => {
                                app.loading(false);
                                app.messages.push({ severity: "error", summary: error, autoTimeout: 0 });//default time out
                            })
                        }, 500);
                    }
                })
            }

            self.addNewBtnAction = function () {
                var codes = self.selectedAddEitCodeVal();
                document.getElementById("addDialog").close();
                var names = self.EitsNotInListOpts().filter(e =>codes.find(ee=>ee== e.value)).map(e=>e.label);
                app.confirmMsg(app.translate('bulksetup.addmsg').replace("#name#", names.join(",")), ok => {
                    if (ok) {
                        app.loading(true);
                        services.addGeneric('bulk/addToMenu', { codes : codes })
                            .then(response => {
                                try { response = JSON.parse(response) } catch (ignored) { }
                                app.loading(false);
                                if (response.status == 'Done') {
                                    var newArr = self.summaryObservableArray().map(e => e.CODE);
                                    newArr.push(...codes);
                                    self.selectedAddEitCodeVal([]);
                                    setData(newArr);
                                } else {
                                    app.messages.push({ severity: "error", summary: response.message, autoTimeout: 0 });//default time out
                                }
                            }, error => {
                                app.loading(false);
                            })
                    }
                })

            }

            self.selectOneListener = function (e) {
                setTimeout(() => {
                    var isDisabled = !self.summaryObservableArray().find(e => e.Selected().length > 0);
                    self.disableDelete(isDisabled)
                }, 10);
            }

            function initTranslations() {
                self.title(app.translate('pages.bulkSetup'))
                self.add(app.translate('others.add'));
                self.back(app.translate('others.back'));
                self.delBtn(app.translate('others.delete'));
                self.save(app.translate('others.save'));
                self.select_selfservice_lbl(app.translate('bulksetup.select_self_lbl'));
                self.columnArray([
                    {
                        "headerText": "Delete",
                        "renderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_tmpl", true),
                    },
                    {
                        "headerText": app.translate('bulksetup.code'), "field": "CODE"
                    },
                    {
                        "headerText": app.translate('bulksetup.name'), "field": "NAME"
                    },
                ]);

            }
        }
        return new bulkSetupModule();
    });
