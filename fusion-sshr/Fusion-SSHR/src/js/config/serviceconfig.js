/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['jquery'], function ($) {

    /**
     * The view model for managing service calls
     */
    function serviceConfig() {

        var self = this;
        var authCode = "Basic YW1yby5hbGZhcmVzQGFwcHNwcm8tbWUuY29tOkFwcHNwcm9AMTIzNA==";
        self.contentTypeApplicationJSON = 'application/json';
        self.contentTypeApplicationXWWw = 'application/x-www-form-urlencoded; charset=UTF-8';
        self.contentTypeMultiPart = 'multipart/form-data';
         var paasAuth = "aGNtdXNlcjpPcmFjbGVAMTIz";
//        var paasAuth = "";
        self.headers = {
        };
        self.callGetService = function (serviceUrl, contentType, headers, asynchronized) {
            var defer = $.Deferred();

            if (headers) {
                headers.Authorization = paasAuth;
                headers.xxx_data_decode = self.JCookie();
            }
            $.ajax({
                type: "GET",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,               
                contentType: contentType,

                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callPostService = function (serviceUrl, payload, contentType, asynchronized, headers) {

            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }
            var defer = $.Deferred();
            headers.Authorization = paasAuth;
            headers.xxx_data_decode = self.JCookie();

            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,

                success: function (data) {
                    defer.resolve(data);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr, thrownError);
                }
            });
            return $.when(defer);
        };

        self.callAddFileService = function (serviceUrl, payload, headers) {
            var defer = $.Deferred();
            headers.Authorization = paasAuth;
            headers.xxx_data_decode = self.JCookie();

            $.ajax({
                type: "POST",
                async: false,
                headers: headers,
                url: serviceUrl,
                cache: false,
                contentType: false,
                processData: false,
                data: payload,

                success: function (data) {

                    defer.resolve(data);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr, thrownError);
                }
            });
              return $.when(defer);
        };

        self.callPutService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }

            var defer = $.Deferred();
            headers.Authorization = paasAuth;
            headers.xxx_data_decode = self.JCookie();
            $.ajax({
                type: "PUT",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.callPatchService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }
            var defer = $.Deferred();
            headers.Authorization = paasAuth;
            headers.xxx_data_decode = self.JCookie();
            $.ajax({
                type: "PATCH",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };


        self.callGetApexService = function (serviceUrl, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            headers.Authorization = paasAuth;
            headers.xxx_data_decode = self.JCookie();
            $.ajax({
                type: "GET",
                async: asynchronized,
                url: serviceUrl,
                headers: headers,
                async: true,

                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
            return $.when(defer);
        };

        self.callPostApexService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            headers.Authorization = paasAuth;
            headers.xxx_data_decode = self.JCookie();
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }

            var defer = $.Deferred();
            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callPutApexService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            headers.Authorization = paasAuth;
            headers.xxx_data_decode = self.JCookie();
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }

            var defer = $.Deferred();
            $.ajax({
                type: "PUT",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.callDeleteApexService = function (serviceUrl, payload, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            headers.Authorization = paasAuth;
            headers.xxx_data_decode = self.JCookie();
            $.ajax({
                type: "DELETE",
                async: asynchronized,
                url: serviceUrl,
                headers: headers,
                data: payload,
                contentType: contentType,
                success: function (e) {
                    defer.resolve(e)
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });

            return $.when(defer);
        };
        self.callPostServiceUncodeed = function (serviceUrl, payload, contentType, asynchronized, headers) {

            var defer = $.Deferred();  
                headers.Authorization = paasAuth;
                headers.xxx_data_decode = self.JCookie();
            $.ajax({
                type: "POST",
                async: false,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payload,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.callPostServiceUncodeed2 = function (serviceUrl, payload, contentType, asynchronized, headers) {

            var defer = $.Deferred();
                 headers.Authorization = paasAuth;
                 headers.xxx_data_decode = self.JCookie();
            $.ajax({
                type: "POST",
                async: true,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payload,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        
         self.callDeleteService = function (serviceUrl, payload, contentType, asynchronized, headers) {

            var defer = $.Deferred();
              headers.Authorization = paasAuth;
              headers.xxx_data_decode = self.JCookie();
            $.ajax({
                type: "DELETE",
                async: false,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payload,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        

        self.callPostServiceUncodeedAsync = function (serviceUrl, payload, contentType, asynchronized, headers) {

            var defer = $.Deferred();
                headers.Authorization = paasAuth;
                headers.xxx_data_decode = self.JCookie();
            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payload,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.JCookie = function getCookie() {
            var name = 'JSESSIONSID' + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                c = c.substring(1);
              }
              if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
              }
            }
            return "";
          }
        
    }

    return new serviceConfig();
});