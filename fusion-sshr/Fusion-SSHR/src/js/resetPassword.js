function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

function validation(){
    var email = document.getElementById('email').value
    var newPassword = document.getElementById('newPass').value;
    var confirmPassword = document.getElementById('confirmPassword').value;
    var emailError = document.getElementById('emailError')
    var errorMsg = document.getElementById('errorMsg')
    
    if(!email || email == ''){
        emailError.innerHTML = "**please Enter Your Email"
        return;
    } emailError.innerHTML = ""

    if(!validateEmail(email)){
        emailError.innerHTML = "**please Enter Your Valid Email"
        return;
    } emailError.innerHTML = ""

    if(newPassword.length < 6){
        errorMsg.innerHTML = "**Paasword must be at least 6 characters, contain at least one uppercase letter, at least one number"
        return;
    }errorMsg.innerHTML = ""

    if (newPassword.search(/[A-Z]/) ==-1) {
        errorMsg.innerHTML ="**Password must contain at least one uppercase letter"
        return;
    }errorMsg.innerHTML =""

    if(newPassword != confirmPassword){
        errorMsg.innerHTML = "**New Password and Confirm Password do not match, please try again."
        return;
    }errorMsg.innerHTML = ""

    //valid code
    console.log('all data is valid')
}