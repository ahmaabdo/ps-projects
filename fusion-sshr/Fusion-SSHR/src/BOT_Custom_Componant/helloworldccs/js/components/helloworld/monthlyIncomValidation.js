"use strict";
module.exports = {
    metadata: () => ({
            "name": "monthlyIncomValidation",
            "properties": {
                "monthlyIncome": {"type": "double", "required": true},
                "keepTurn": {"type": "boolean", "required": false},
				"count": {"type": "int", "required": false}
            },
            "supportedActions": ["valid", "invalid","threeTriel"]
        }),
    invoke: (conversation, done) => {
        var monthlyIncome = conversation.properties().monthlyIncome;
        var keepTurn = conversation.properties().keepTurn;
        var lang = conversation.variable("lang");
		var count = conversation.properties().count;
		if(monthlyIncome > 2500) {
			conversation.transition("valid");
		}else {
			if(count < 3){
				conversation.variable("count" , count+1);
				if (lang === 'en') {
					conversation.reply({text: 'Monthly income should be at least 2500.'});
				} else {
					conversation.reply({text: 'الدخل الشهري يجب ان لا يقل عن 2500.'});
				}
				
				conversation.transition("invalid");
			}else if(count >= 3){
				if (lang === 'en') {
					conversation.reply({text: 'You entered 3 wrong trail.'});
				} else {
					conversation.reply({text: 'لقد ادخلت 3 محاولات خاطئة.'});
				}
				conversation.transition("threeTriel");
			}
		}
		done();
    }
};