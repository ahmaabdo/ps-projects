package utilities;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.MessageBeanSetup;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import com.appspro.fusionsshr.dao.MessageBeanSetupDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import java.util.List;
import java.util.Map;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.json.JSONArray;
import org.json.JSONObject;

public class NotificationUtility {


    public static MessageBeanSetup buildEmailBody(JSONObject eitLbl,
                                        HashMap<String, String> extraInfo,
                                        String eitName, String notiType) {
        MessageBeanSetupDAO dao = new MessageBeanSetupDAO();
        MessageBeanSetup bean = new MessageBeanSetup(getAll().get(eitName+ "/" + notiType) != null ? getAll().get(eitName+ "/" + notiType) : getAll().get("Default/" + notiType));

        
        String message = bean.getEmailBody();

            if(eitLbl != null) {
        Iterator<String> keys = eitLbl.keys();
       
            while (keys.hasNext()) {
                String key = keys.next();
                message =
                        message.replaceAll("#" + key + "#", eitLbl.get(key) != null ?
                                                            eitLbl.get(key).toString() :
                                                            "");
            }
        }
        Iterator it = extraInfo.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            message =
                    message.replaceAll("#" + pair.getKey() + "#", pair.getValue().toString());
        }


        bean.setEmailBody(message);
        return bean;
    }
    
    
    public static MessageBeanSetup buildNotificationBody(JSONObject eitLbl,
                                        HashMap<String, String> extraInfo,
                                        String eitName, String notiType) {
        
        MessageBeanSetupDAO dao = new MessageBeanSetupDAO();
        MessageBeanSetup bean = new MessageBeanSetup(getAll().get(eitName+ "/" + notiType) != null ? getAll().get(eitName+ "/" + notiType) : getAll().get("Default/" + notiType));

        String message = bean.getNotiMessage();

        if(eitLbl != null) {
            Iterator<String> keys = eitLbl.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                message =
                        message.replaceAll("#" + key + "#", eitLbl.get(key) != null ?
                                                            eitLbl.get(key).toString() :
                                                            "");
            }
        }
        

        Iterator it = extraInfo.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            message =
                    message.replaceAll("#" + pair.getKey() + "#", pair.getValue().toString());
        }


        bean.setNotiMessage(message);
        return bean;
    }
    
    
    public static Map<String,MessageBeanSetup> getAll() {
        CacheManager cm = AppsproConnection.getCacheManager();

        Cache cache = cm.getCache("MessagesSetupCache");
        Element ele = cache.get("MessagesSetup");
        MessageBeanSetupDAO dao = new MessageBeanSetupDAO();
        Map<String,MessageBeanSetup> beanList = null;
        if (ele == null) {
            beanList = dao.getAll();
            cache.put(new Element("MessagesSetup", beanList));
        } else {
            beanList =
                (HashMap<String,MessageBeanSetup>)(ele == null ? null : ele.getObjectValue());
        }
        
        return beanList;
    }
    
    
    
    public static void main(String[] args) {
        
        HashMap<String, String> extraInfo = new HashMap<String, String>();
        MessageBeanSetup messageBean = new MessageBeanSetup();
        extraInfo.put("Reciver", "Shadi");
        extraInfo.put("employeeName", "Ahmad");
        extraInfo.put("EIT_NAME", "Identification Letter");
        
        JSONObject obj = new JSONObject("{\"dummy\":1585483653622,\"employeeId\":\"000332\",\"employeeName\":\"Abdulelah Aldhuwayhi\",\"employeeDepartment\":\"Employee Relations & Engagement\",\"manager\":\"Ahmed Hamad  Al Othimin\",\"employeePosition\":\"Employee Relations Specialist\",\"employeeNationality\":\"Saudi\",\"hireDate\":\"2019-04-01T00:00:00.000+00:00\",\"basicSalary\":12000,\"eos\":6550,\"loanInfo\":\"Three Basic Salary as a Maximum,the repayment period should not exceed 12 months\",\"loanAmount\":\"1000\",\"PaymentPeriodMonths\":6,\"firstPaymentDate\":\"2020/03/01\",\"lastPaymentDate\":\"2020/08/01\",\"monthlyPayment\":166.67,\"url\":\"https://eoay-test.fa.em2.oraclecloud.com:443/hcmRestApi/resources/11.13.18.05/emps/00020000000EACED00057708000110D9318F7F790000004AACED00057372000D6A6176612E73716C2E4461746514FA46683F3566970200007872000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001712396880078/child/personExtraInformation/300000002146169/child/PersonExtraInformationContextXXX_5FLOAN_5FREQUESTprivateVO\",\"PaymentPeriodMonthsLbl\":6}");
        messageBean = NotificationUtility.buildNotificationBody(obj, extraInfo, "XXX_HR_Identificastion_Letter", "FYA");
        System.out.println(messageBean.getEmailBody());
        System.out.println(messageBean.getAttachment());
        System.out.println(messageBean.getFileName());
        System.out.println(messageBean.getFileType());
        messageBean = NotificationUtility.buildNotificationBody(obj, extraInfo, "XXX_HR_Identification_Letter", "FYA");
    }
}
