package com.appspro.mail;


import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import com.appspro.fusionsshr.dao.EmailLogTrackerDAO;

import com.sun.istack.ByteArrayDataSource;

import java.io.OutputStream;

import java.net.URL;

import java.nio.charset.StandardCharsets;

import java.util.Base64;
import java.util.Iterator;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;


public class CredentialStoreClassMail {
    
    private static EmailLogTrackerDAO emailLogger = new EmailLogTrackerDAO();
    private static boolean EMAIL_STATUS = false;
    
    static private String smtpAuthUserName = "oracless@tbc.sa";
    static private String smtpAuthPassword = "Notification@1234@";
    static private String emailFrom = "oracless@tbc.sa";
    static private String clientWebsite = "TBC.com.sa";

    static private String emailToEmp;
    static private String subject;
    static private String body;
    static private String attachment;
    static private String filename;
    static private String notificationType;
    static private String trsId;
    static private String selfType;
    static private boolean withAttachment;
    static private PeopleExtraInformationBean eit;

    public static void SEND_NOTIFICATION(String emailToEmp, String subject,
                                         String body, String attachment,
                                         String filename,
                                         String notificationType, String trsId,
                                         String selfType,
                                         boolean withAttachment,
                                         PeopleExtraInformationBean eit) {

        //SEND_MAIL(emailToEmp, subject, body);
        //sendMailWithAttachment(emailToEmp, subject, body, attachment,
        //                       filename);
        // sendMailProbationAndContract(emailToEmp, subject, body);
        return;
//        if (withAttachment) {
//            sendMailWithAttachment(emailToEmp, eit.getPersonName(), subject, body, attachment,
//                                   filename);
//        } else {
//            SendMailTemplate(emailToEmp, subject, notificationType, trsId,
//                             selfType, eit, body);
//        }

    }

    public static void SEND_MAIL(String emailToEmp, String subject,
                                 String body) {
        try {
            String emailTo = emailToEmp;
            String emailBcc = "abdullah.oweidi@appspro-me.com";
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpAuthUserName,
                                                      smtpAuthPassword);
                }
            };
            Properties properties = new Properties();
            properties.setProperty("mail.smtp.host", "smtp.office365.com");
            properties.setProperty("mail.smtp.port", "587");
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(properties, authenticator);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom, "TBC HR Services"));
            InternetAddress[] to = { new InternetAddress(emailTo) };
            message.setRecipients(Message.RecipientType.TO, to);
            InternetAddress[] bcc = { new InternetAddress(emailBcc) };
            message.setRecipients(Message.RecipientType.BCC, bcc);
            message.setContent(body, "text/html");
            message.setSubject(subject);
            //message.setText(body);
            Transport.send(message);
            EMAIL_STATUS = true;

            
        } catch (Exception e) {

            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            EMAIL_STATUS = false;
        }
        
        //emailLogger.logMailInfo(emailToEmp, null, subject, body, EMAIL_STATUS);

    }

    private static void sendMailWithAttachment(String emailToEmp,
                                               String empName,
                                               String subject, String body,
                                               String attachment,
                                               String filename) {
        try {

            String emailTo = emailToEmp;
            String emailCC = "";
            String emailBcc = "abdullah.oweidi@appspro-me.com";
            String emailBcc2 = "shadi.mansi@appspro-me.com";
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpAuthUserName,
                                                      smtpAuthPassword);
                }
            };
            Properties properties = new Properties();
            properties.setProperty("mail.smtp.host", "smtp.office365.com");
            properties.setProperty("mail.smtp.port", "587");
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(properties, authenticator);
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom, "TBC HR Services"));
            InternetAddress[] to = { new InternetAddress(emailTo) };
            message.setRecipients(Message.RecipientType.TO, to);
            //            InternetAddress[] cc = { new InternetAddress(emailCC) };
            //            message.setRecipients(Message.RecipientType.CC, cc);
            InternetAddress[] bcc =
            { new InternetAddress(emailBcc), new InternetAddress(emailBcc2) };
            message.setRecipients(Message.RecipientType.BCC, bcc);
            message.setSubject(subject);
            Multipart multipart = new MimeMultipart();
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setContent(body, "text/html");
            multipart.addBodyPart(textBodyPart);
            if (!attachment.isEmpty()) {
                String string =
                    attachment.substring(attachment.indexOf("data:"),
                                         attachment.indexOf("base64,") - 1);
                String contentType = string.substring(5);
                String base64Data =
                    attachment.substring(attachment.lastIndexOf("base64,") +
                                         7);
                byte[] imgBytes = Base64.getDecoder().decode(base64Data.getBytes(StandardCharsets.UTF_8));

                DataSource source =
                    new ByteArrayDataSource(imgBytes, contentType);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);
                multipart.addBodyPart(messageBodyPart);
            }


            message.setContent(multipart);

            System.out.println("Sending");

            Transport.send(message);

            System.out.println("Done");
            EMAIL_STATUS = true;

        } catch (Exception e) {

            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            EMAIL_STATUS = false;
        }
        
        //emailLogger.logMailInfo(emailToEmp, empName, subject, body, EMAIL_STATUS);
    }


    private static void SEND_MAIL_OutLook(String email, String subject,
                                          String body) {
        try {

            String emailTo = email;
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpAuthUserName,
                                                      smtpAuthPassword);
                }
            };
            Properties properties = new Properties();
            properties.setProperty("mail.smtp.host", "smtp.office365.com");
            properties.setProperty("mail.smtp.port", "587");
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(properties, authenticator);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom));
            InternetAddress[] to = { new InternetAddress(emailTo) };
            message.setRecipients(Message.RecipientType.TO, to);
            message.setSubject(subject);
            message.setText(body);
            Transport.send(message);
            EMAIL_STATUS = true;
        } catch (Exception e) {
            AppsproConnection.LOG.error("ERROR", e);
            EMAIL_STATUS = false;
        }

        //TODO: Add person name when this method is fired
        //emailLogger.logMailInfo(email, null, subject, body, EMAIL_STATUS);

    }

    private static void sendMailProbationAndContract(String emailToEmp,
                                                     String subject,
                                                     String body) {

        try {
            String emailTo = emailToEmp;
            String emailCC = null;
            String emailBcc = "abdullah.oweidi@appspro-me.com";
            String emailBcc2 = "abdullah.oweidi@appspro-me.com";
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpAuthUserName,
                                                      smtpAuthPassword);
                }
            };
            Properties properties = new Properties();
            properties.setProperty("mail.smtp.host", "smtp.office365.com");
            properties.setProperty("mail.smtp.port", "587");
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(properties, authenticator);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom, "TBC HR Services"));
            InternetAddress[] to = { new InternetAddress(emailTo) };
            message.setRecipients(Message.RecipientType.TO, to);
            InternetAddress[] cc = { new InternetAddress(emailCC) };
            message.setRecipients(Message.RecipientType.CC, cc);
            InternetAddress[] bcc =
            { new InternetAddress(emailBcc), new InternetAddress(emailBcc2) };
            message.setRecipients(Message.RecipientType.BCC, bcc);
            message.setSubject(subject);
            message.setText(body);
            Transport.send(message);
            EMAIL_STATUS = true;

        } catch (Exception e) {
            AppsproConnection.LOG.error("ERROR", e);
            EMAIL_STATUS = false;
        }
        //TODO: Add person name when this method is fired
        //emailLogger.logMailInfo(emailToEmp, null, subject, body, EMAIL_STATUS);
    }

    private static void SendMailTemplate(String email, String subject,
                                         String notificationType, String trsId,
                                         String selfType,
                                         PeopleExtraInformationBean eit,
                                         String body) {


        String requestName = eit.getEit_name();
        String requestor = eit.getPersonName();
        String recipient = null;
        String comment = null;

        String htmlPart = null;
        if ("FYA".equalsIgnoreCase(notificationType)) {
            System.out.println("FYA");
            htmlPart =
                    "<!DOCTYPE html><html><head> <style type=text/css>body{background:linear-gradient(to right,#828763,#6b6b83,#738d80)}.body-content{background-color:#e0dbdb;box-shadow:3px 3px 3px #767974;width:600px;height:550px;margin:auto;margin-top:50px;padding-left:30px;border-radius:10px}.body-content h5{font-family:'Roboto',sans-serif;font-weight:bold;padding-bottom:5px;padding-top:30px;font-size:20px}.body-content h3{font-weight:bold;padding-bottom:15px}.body-content p{font-family:'Oswald',sans-serif;font-size:18px}.heading{padding-top:30px}.buttons-cont{margin-left:110px;margin-top:50px}.buttons-cont a{color:#fff;text-decoration:none;border-radius:5px;margin-left:15px}.buttons-cont .approve:hover{background-color:#090;border:#090}.buttons-cont .reject:hover{background-color:#ef101a;border:#ef101a}.buttons-cont .approve{background-color:#060;border:#060;padding:10px 45px}.buttons-cont .reject{background-color:#9a0007;border:#9a0007;padding:10px 55px}</style></head>" +
                    body +
                    "<script type=text/javascript></script><script src=https://code.jquery.com/jquery-3.3.1.slim.min.js integrity=sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo crossorigin=anonymous></script><script src=https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js integrity=sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1 crossorigin=anonymous></script><script src=https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js integrity=sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM crossorigin=anonymous></script></html>\n";
            htmlPart =
                    htmlPart.replaceAll("#Recipient#", recipient).replaceAll("#Request Name#",
                                                                             requestName).replaceAll("#Requestor#",
                                                                                                     requestor).replaceAll("#Notes or other comment#",
                                                                                                                           comment).replaceAll("#Client_WEB_SITE#",
                                                                                                                                               clientWebsite).replaceAll("#TRS_ID#",
                                                                                                                                                                         trsId).replaceAll("#S_TYPE#",
                                                                                                                                                                                           selfType);
            System.out.println(htmlPart);
        }

        else if ("FYI".equalsIgnoreCase(notificationType)) {
            System.out.println("FYI");
            htmlPart =
                    "<!DOCTYPE html><html><head> <style type=text/css>body{background:linear-gradient(to right,#828763,#6b6b83,#738d80)}.body-content{background-color:#e0dbdb;box-shadow:3px 3px 3px #767974;width:600px;height:570px;margin:auto;margin-top:50px;padding-left:40px;border-radius:10px;margin-bottom:40px}.body-content img{margin-left:495px;margin-top:30px}h2{font-size:20px;font-weight:bold;padding-left:25px}.body-content h5{font-family:'Roboto',sans-serif;font-weight:bold;padding-bottom:5px;font-size:20px}.body-content h3{font-weight:bold}.body-content p{font-family:'Oswald',sans-serif;font-size:18px}.heading{padding-top:30px}</style></head>" +
                    body +
                    "<script type=text/javascript></script><script src=https://code.jquery.com/jquery-3.3.1.slim.min.js integrity=sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo crossorigin=anonymous></script><script src=https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js integrity=sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1 crossorigin=anonymous></script><script src=https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js integrity=sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM crossorigin=anonymous></script></html>";
            htmlPart =
                    htmlPart.replaceAll("#Recipient#", recipient).replaceAll("#Request Name#",
                                                                             requestName).replaceAll("#Requestor#",
                                                                                                     requestor).replaceAll("#Notes or other comment#",
                                                                                                                           comment);
        }

        try {

            String emailTo = email;
            String emailBcc = "abdullah.oweidi@appspro-me.com";
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpAuthUserName,
                                                      smtpAuthPassword);
                }
            };
            Properties properties = new Properties();
            properties.setProperty("mail.smtp.host", "smtp.office365.com");
            properties.setProperty("mail.smtp.port", "587");
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(properties, authenticator);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom, "TBC HR Services"));
            InternetAddress[] to = { new InternetAddress(emailTo) };
            message.setRecipients(Message.RecipientType.TO, to);
            InternetAddress[] bcc = { new InternetAddress(emailBcc) };
            message.setRecipients(Message.RecipientType.BCC, bcc);
            message.setSubject(subject);
            message.setContent(htmlPart, "text/html; charset=utf-8");
            Transport.send(message);

            EMAIL_STATUS = true;

        } catch (Exception e) {
            EMAIL_STATUS = false;
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        
        //emailLogger.logMailInfo(email, requestor, subject, body, EMAIL_STATUS);
    }

    public static void main(String[] args) {
        try {
            JSONObject jsonObject =
                new JSONObject("{\"dummy\":1584394431712,\"employeeLocation\":\"Head Office\",\"travelType\":\"Regular Business Trip\",\"tripDetails\":\"23456\",\"grade\":11,\"businessTripRoute\":\"International\",\"fromDestination\":\"Al Baha\",\"toDestinationCountry\":\"United States\",\"toDestinationCity\":\"Washington D.C.\",\"startDate\":\"2020-03-17\",\"endDate\":\"2020-03-18\",\"numberOfDays\":5,\"balanceDays\":27,\"dutyDelegatedTo\":\"000055 Alburaidi, Abdulrahman\",\"employeeClass\":\"Economic class\",\"travelBy\":\"Car\",\"ticketClass\":\"No Need\",\"ticketAmount\":\"\",\"numberOfKilometers\":100,\"totalPerDiemAmount\":7570,\"travelingOnBehalfOfOtherDepart\":\"CR Quality Control & HSSE\",\"attachment\":\"YES\"}");
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();

                System.out.println(key + "  " + jsonObject.get(key));

            }
            //            SEND_MAIL("abdallah.oweidi@appspro-me.com", "asd", "asds");
        } catch (Exception e) {
            AppsproConnection.LOG.error(e.getMessage());
        }
    }
}
