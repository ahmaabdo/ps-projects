package com.appspro.fusionsshr.rest;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.EmployeeTimeAttandanceBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.fusionsshr.dao.EmployeeTimeAttandanceDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.biPReports.BIReportModel;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;


@Path("/EmployeeTimeAttandance")
@JWTTokenNeeded
public class EmployeeTimeAttandanceRest {
    EmployeeTimeAttandanceDAO serives = new EmployeeTimeAttandanceDAO();

    @POST
    @Path("/getAllEmployeeTimeAttandance")
    @Produces(MediaType.APPLICATION_JSON)//"application/xls"
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllEmployeeAttandance(String body) {
        JSONObject res = new JSONObject();
        
        List<EmployeeTimeAttandanceBean> list = new ArrayList<EmployeeTimeAttandanceBean>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            EmployeeTimeAttandanceBean bean = new EmployeeTimeAttandanceBean();
            bean = mapper.readValue(body, EmployeeTimeAttandanceBean.class);
            list = serives.getAllEmployeeTimeAtandance(bean);
            res.put("message", "Done");
            res.put("data", new JSONArray(list).toString());
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            res.put("message", "Error");
            res.put("data",e.getMessage());
        }
        
        return Response.ok(res.toString()).build();
    }

    @POST
    @Path("/getAllEmployeeTimeAttandanceByPersonNumber")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getAllAttandanceByPersonNumber(String body, @Context
        HttpServletRequest request) {

        List<EmployeeTimeAttandanceBean> list =
            new ArrayList<EmployeeTimeAttandanceBean>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            EmployeeTimeAttandanceBean bean =
                mapper.readValue(body, EmployeeTimeAttandanceBean.class);
            //(bean);
            Map<String, String> paramMap = new HashMap<String, String>();
            paramMap.put(BIReportModel.SupervisorHierarchyDataParam.P_MGR_NUMBER.getValue(),
                         bean.getEmployeeNumber());
            String effictiveDate =
                new SimpleDateFormat("MM-dd-yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(bean.getPunch_date()));
            paramMap.put(BIReportModel.SupervisorHierarchyDataParam.P_EFFECTIVE_DATE.getValue(),
                         effictiveDate);
            JSONObject response =
                BIReportModel.runReport(BIReportModel.REPORT_NAME.SupervisorHierarchyDataReport.getValue(),
                                        paramMap);
            JSONObject dataDS = response.getJSONObject("DATA_DS");
            JSONArray g1 = null;
            try {
                g1 = dataDS.getJSONArray("G_1");
            } catch (Exception e) {

                JSONObject g2 = dataDS.getJSONObject("G_1");
                String employeeNumber = g2.get("EMPLOYEE_NUMBER").toString();

                list =
serives.getAllEmployeeTimeAtandanceByPersonNumber(bean, employeeNumber, "",
                                                  "");
            }

            //            JSONObject g2 = dataDS.getJSONObject("G_1");

            String employeeNumbers = "(" + bean.getEmployeeNumber() + ",";
            String employeeNumbers2 = "(";
            String employeeNumbers3 = "(";
            for (int i = 0; i < g1.length(); i++) {
                JSONObject data = g1.getJSONObject(i);

                if (i < 999) {
                    if (i == 998 || i == g1.length() - 1)
                        employeeNumbers +=
                                data.optString("EMPLOYEE_NUMBER") + ")";
                    else
                        employeeNumbers +=
                                data.optString("EMPLOYEE_NUMBER") + ",";
                } else if (i < 1999) {
                    if (i == 1998 || i == g1.length() - 1)
                        employeeNumbers2 +=
                                data.optString("EMPLOYEE_NUMBER") + ")";
                    else
                        employeeNumbers2 +=
                                data.optString("EMPLOYEE_NUMBER") + ",";
                } else {
                    if (i == g1.length() - 1)
                        employeeNumbers3 +=
                                data.optString("EMPLOYEE_NUMBER") + ")";
                    else
                        employeeNumbers3 +=
                                data.optString("EMPLOYEE_NUMBER") + ",";
                }


            }

            list =
serives.getAllEmployeeTimeAtandanceByPersonNumber(bean, employeeNumbers,
                                                  employeeNumbers2,
                                                  employeeNumbers3);
            Map<String, String> personNumberMap =
                new HashMap<String, String>();
            for (int i = 0; i < list.size(); i++) {
                EmployeeTimeAttandanceBean employeeTimeAttandanceBean =
                    list.get(i);
                personNumberMap.put(employeeTimeAttandanceBean.getPersonNumber(),
                                    employeeTimeAttandanceBean.getPersonNumber());

            }
            if (bean.getPersonNumber().isEmpty()) {

                for (int i = 0; i < g1.length(); i++) {
                    JSONObject data = g1.getJSONObject(i);
                    if (personNumberMap.get(data.optString("EMPLOYEE_NUMBER")) ==
                        null) {
                        EmployeeTimeAttandanceBean employeeTimeAttandanceBean =
                            new EmployeeTimeAttandanceBean();
                        employeeTimeAttandanceBean.setPunch_date("Absent So Far");
                        employeeTimeAttandanceBean.setPersonNumber(data.optString("EMPLOYEE_NUMBER"));
                        employeeTimeAttandanceBean.setPersonName(data.optString("EMPLOYEE_NAME"));
                        employeeTimeAttandanceBean.setSr_number("Absent So Far");
                        employeeTimeAttandanceBean.setTodaytDate("Absent So Far");
                        employeeTimeAttandanceBean.setDeviceName("Absent So Far");
                        employeeTimeAttandanceBean.setPerviousDatePunchOut("Absent So Far");
                        employeeTimeAttandanceBean.setPerviousDateDate("Absent So Far");
                        employeeTimeAttandanceBean.setPerviousDatePunchIn("Absent So Far");

                        list.add(employeeTimeAttandanceBean);
                    }

                }

            }

            return new JSONArray(list).toString();
        } catch (Exception e) {
            return new JSONArray(list).toString();
        }


    }


//    public static String generateGeneral(String objStr) throws FileNotFoundException,
//                                                               IOException {
//        //        JSONObject obj = new JSONObject(objStr);
//        JSONArray arr = new JSONArray(objStr); //obj.getJSONArray("data");
//
//
//        JSONObject o;
//
//        //create main sheet
//        HSSFWorkbook workbook = new HSSFWorkbook();
//        HSSFSheet realSheet = workbook.createSheet("Sheet xls");
//
//        CellStyle style = workbook.createCellStyle();
//        style.setFillBackgroundColor(IndexedColors.RED.getIndex());
//
//
//        //CREATE HEADER
//        Row row = realSheet.createRow(0);
//        row.createCell(0).setCellValue("sr_number");
//        row.createCell(1).setCellValue("deviceName");
//        row.createCell(2).setCellValue("personName");
//        row.createCell(3).setCellValue("todaytDate");
//        row.createCell(4).setCellValue("punch_date");
//        row.createCell(5).setCellValue("personNumber");
//        row.createCell(6).setCellValue("perviousDatePunchOut");
//        row.createCell(7).setCellValue("perviousDatePunchIn");
//        row.createCell(8).setCellValue("perviousDateDate");
//
//
//        for (int x = 0; x < arr.length(); x++) {
//            //CREATE Body
//            JSONObject object = arr.getJSONObject(x);
//
//            row = realSheet.createRow(x + 1);
//            //
//
//            row.createCell(0).setCellValue(object.has("sr_number") ?
//                                           object.get("sr_number").toString() :
//                                           "Absent So Far");
//            row.createCell(1).setCellValue(object.has("deviceName") ?
//                                           object.get("deviceName").toString() :
//                                           "Absent So Far");
//            row.createCell(2).setCellValue(object.has("personName") ?
//                                           object.get("personName").toString() :
//                                           "Absent So Far");
//            row.createCell(3).setCellValue(object.has("todaytDate") ?
//                                           object.get("todaytDate").toString() :
//                                           "Absent So Far");
//            row.createCell(4).setCellValue(object.has("punch_date") ?
//                                           object.get("punch_date").toString() :
//                                           "Absent So Far");
//            row.createCell(5).setCellValue(object.has("personNumber") ?
//                                           object.get("personNumber").toString() :
//                                           "Absent So Far");
//            row.createCell(6).setCellValue(object.has("perviousDatePunchOut") ?
//                                           object.get("perviousDatePunchOut").toString() :
//                                           "Absent So Far");
//            row.createCell(7).setCellValue(object.has("perviousDatePunchIn") ?
//                                           object.get("perviousDatePunchIn").toString() :
//                                           "Absent So Far");
//            row.createCell(8).setCellValue(object.has("perviousDateDate") ?
//                                           object.get("perviousDateDate").toString() :
//                                           "Absent So Far");
//
//
//        }
//
//        //--end save metadata-----------
//
//        for (int x = 0; x <= arr.length(); x++) {
//            realSheet.autoSizeColumn(x);
//        }
//
//
//        return workBookToString(workbook);
//    }
//
//    private static String workBookToString(Workbook workbook) {
//        try {
//            ByteArrayOutputStream byteArrayOutputStream =
//                new ByteArrayOutputStream();
//
//            workbook.write(byteArrayOutputStream);
//            String b64 =
//                Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
//            return b64;
//        } catch (Exception e) {
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//        }
//        return null;
//    }
    
    @POST
    @Path("/getAllEmployeeTimeAttandanceByPersonNumberExcelSheet")
    @Produces("application/xls")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllAttandanceByPersonNumberExcelSheet(String body) {

        List<EmployeeTimeAttandanceBean> list =
            new ArrayList<EmployeeTimeAttandanceBean>();
        String file_base64 = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            EmployeeTimeAttandanceBean bean =
                mapper.readValue(body, EmployeeTimeAttandanceBean.class);
            //(bean);
            Map<String, String> paramMap = new HashMap<String, String>();
            paramMap.put(BIReportModel.SupervisorHierarchyDataParam.P_MGR_NUMBER.getValue(),
                         bean.getEmployeeNumber());
            String effictiveDate =
                new SimpleDateFormat("MM-dd-yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(bean.getPunch_date()));
            paramMap.put(BIReportModel.SupervisorHierarchyDataParam.P_EFFECTIVE_DATE.getValue(),
                         effictiveDate);
            JSONObject response =
                BIReportModel.runReport(BIReportModel.REPORT_NAME.SupervisorHierarchyDataReport.getValue(),
                                        paramMap);
            JSONObject dataDS = response.getJSONObject("DATA_DS");
            JSONArray g1 = null;
            try {
                g1 = dataDS.getJSONArray("G_1");
            } catch (Exception e) {

                JSONObject g2 = dataDS.getJSONObject("G_1");
                String employeeNumber = g2.get("EMPLOYEE_NUMBER").toString();

                list =
    serives.getAllEmployeeTimeAtandanceByPersonNumber(bean, employeeNumber, "",
                                                  "");
            }

            //            JSONObject g2 = dataDS.getJSONObject("G_1");

            String employeeNumbers = "(" + bean.getEmployeeNumber() + ",";
            String employeeNumbers2 = "(";
            String employeeNumbers3 = "(";
            for (int i = 0; i < g1.length(); i++) {
                JSONObject data = g1.getJSONObject(i);

                if (i < 999) {
                    if (i == 998 || i == g1.length() - 1)
                        employeeNumbers +=
                                data.optString("EMPLOYEE_NUMBER") + ")";
                    else
                        employeeNumbers +=
                                data.optString("EMPLOYEE_NUMBER") + ",";
                } else if (i < 1999) {
                    if (i == 1998 || i == g1.length() - 1)
                        employeeNumbers2 +=
                                data.optString("EMPLOYEE_NUMBER") + ")";
                    else
                        employeeNumbers2 +=
                                data.optString("EMPLOYEE_NUMBER") + ",";
                } else {
                    if (i == g1.length() - 1)
                        employeeNumbers3 +=
                                data.optString("EMPLOYEE_NUMBER") + ")";
                    else
                        employeeNumbers3 +=
                                data.optString("EMPLOYEE_NUMBER") + ",";
                }


            }

            list =
    serives.getAllEmployeeTimeAtandanceByPersonNumber(bean, employeeNumbers,
                                                  employeeNumbers2,
                                                  employeeNumbers3);
            Map<String, String> personNumberMap =
                new HashMap<String, String>();
            for (int i = 0; i < list.size(); i++) {
                EmployeeTimeAttandanceBean employeeTimeAttandanceBean =
                    list.get(i);
                personNumberMap.put(employeeTimeAttandanceBean.getPersonNumber(),
                                    employeeTimeAttandanceBean.getPersonNumber());

            }
            if (bean.getPersonNumber().isEmpty()) {

                for (int i = 0; i < g1.length(); i++) {
                    JSONObject data = g1.getJSONObject(i);
                    if (personNumberMap.get(data.optString("EMPLOYEE_NUMBER")) ==
                        null) {
                        EmployeeTimeAttandanceBean employeeTimeAttandanceBean =
                            new EmployeeTimeAttandanceBean();
                        employeeTimeAttandanceBean.setPunch_date("Absent So Far");
                        employeeTimeAttandanceBean.setPersonNumber(data.optString("EMPLOYEE_NUMBER"));
                        employeeTimeAttandanceBean.setPersonName(data.optString("EMPLOYEE_NAME"));
                        employeeTimeAttandanceBean.setSr_number("Absent So Far");
                        employeeTimeAttandanceBean.setTodaytDate("Absent So Far");
                        employeeTimeAttandanceBean.setDeviceName("Absent So Far");
                        employeeTimeAttandanceBean.setPerviousDatePunchOut("Absent So Far");
                        employeeTimeAttandanceBean.setPerviousDateDate("Absent So Far");
                        employeeTimeAttandanceBean.setPerviousDatePunchIn("Absent So Far");

                        list.add(employeeTimeAttandanceBean);
                    }

                }

            }

//            file_base64 = generateGeneral(new JSONArray(list).toString());
            } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            }
            Response.ResponseBuilder response = Response.ok(file_base64);
            //   response.header("Content-Length", file_base64.length());//fbytes, "application/xls"
            return response.build();


    }
//*************************************new get Employee Attandance by search************/

    @POST
    @Path("/getEmployeeAttendanceBySearch")
    @Produces(MediaType.APPLICATION_JSON)
    public String getEmployeeAttendanceBySearch(String body) {
        List<EmployeeTimeAttandanceBean> list =
            new ArrayList<EmployeeTimeAttandanceBean>();
        
        try{
                ObjectMapper mapper = new ObjectMapper();
                EmployeeTimeAttandanceBean bean = mapper.readValue(body, EmployeeTimeAttandanceBean.class);
                list =
                   serives.getEmployeeTimeAtandanceBySearch(bean);
            }catch (Exception e) {
            return new  JSONArray(list).toString();
        }
        return new  JSONArray(list).toString();
    }


    public static void main(String[] arg) throws ParseException {


    }
}
