/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.JobBean;
import com.appspro.fusionsshr.dao.JobDAO;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import common.restHelper.RestHelper;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import utilities.JWTTokenNeeded;

/**
 *
 * @author user
 */
@Path("/ConverDate")
@JWTTokenNeeded
public class DateCovert extends RestHelper {

    /**
     * This is a sample web service operation
     *
     * @param jobName
     * @return
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{jobName}")
    public Response getAllJobs(@PathParam("jobName") String jobName, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        //(jobName);
//        String D1 = convertToGregorian();
//        String D2= convertToHijri();
//        //(D1);

        return Response.ok(jobName, MediaType.APPLICATION_JSON).build();
    }
}
