package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.ElementEntrySetupBean;
import com.appspro.fusionsshr.dao.ElementEntrySetupDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import common.restHelper.RestHelper;

import javax.ws.rs.DELETE;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("/elementEntrySetup")
@JWTTokenNeeded
public class ElementEntrySetupService extends RestHelper {

    ElementEntrySetupDAO service = new ElementEntrySetupDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateJob(String body,
                                                   @PathParam("transactionType")
        String transactionType) {
        ElementEntrySetupBean list = new ElementEntrySetupBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ElementEntrySetupBean bean = mapper.readValue(body, ElementEntrySetupBean.class);
            list = service.insertOrUpdateElementEntry(bean, transactionType);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }
    }

    @GET
    @Path("/getAllElements")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllElementEntries() {
        ArrayList<ElementEntrySetupBean> list =
            new ArrayList<ElementEntrySetupBean>();
        try {
          //  ArrayList<IconBean> list = new ArrayList<IconBean>();
            list = service.getAllElementEntries();
            return  new  JSONArray(list).toString();
            //    service.getIcons();
        } catch (Exception e) {
            return new  JSONArray(list).toString();
        }
        
    }

    public static void main(String[] args) {
        ApprovalSetupService obj = new ApprovalSetupService();
        //(obj.getApprovalTypeByEIT_CODE("XXX_ABS_CREATE_DECREE"));
    }


    @GET
    @Path("getElementEntry/{eitcode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getElementEntrySetupByEIT_CODE(@PathParam("eitcode")
        String eid_code) {
        ArrayList<ElementEntrySetupBean> list =
            new ArrayList<ElementEntrySetupBean>();
        try {
          //  ArrayList<IconBean> list = new ArrayList<IconBean>();
            list = service.getElementEntryByEIT(eid_code);
            return  new  JSONArray(list).toString();
            //    service.getIcons();
        } catch (Exception e) {
            return new  JSONArray(list).toString();
        }
    }

    @GET
    @Path("deleteElementEntry/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteElementEntrySetupByEIT_CODE(@PathParam("id")
        String id) {
        String status = "";
         
       status=  service.getDELETEEIT(id);
       return status;
    }

}
