/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.dao.dateSequenceDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import utilities.JWTTokenNeeded;

/**
 *
 * @author lenovo
 */
@Path("/sequenceDate")
@JWTTokenNeeded
public class dateSequenceRest {

    dateSequenceDAO service = new dateSequenceDAO();

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getDateSequence(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            //"{"+"\"count\":"+new Integer(list)+"}"
            int seqNumber = service.getDateSequence();
            String seq = "SS_" + seqNumber;
            return Response.ok("{" + "\"seq\":" + "\"" + seq + "\"" + "}",
                               MediaType.TEXT_PLAIN).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

    }

}
