/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.OrgnizationBean;
import com.appspro.fusionsshr.dao.OrganizationDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import utilities.JWTTokenNeeded;

/**
 *
 * @author user
 */
@Path("/org")
@JWTTokenNeeded
public class OrganizationRest {

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<OrgnizationBean> getEmpDetails(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

        ObjectMapper mapper = new ObjectMapper();
        OrgnizationBean obj = new OrgnizationBean();
        OrganizationDetails det = new OrganizationDetails();

        ArrayList<OrgnizationBean> organizationList = det.getOrgDetails();
        return organizationList;

//        
//
//        
        //  String jsonInString = null;
//        try {
//            jsonInString = mapper.writeValueAsString(obj);
//        } catch (JsonProcessingException e) {
//            
//        }
    }

}
