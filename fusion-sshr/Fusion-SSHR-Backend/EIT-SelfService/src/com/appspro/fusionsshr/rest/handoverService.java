package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.HandoverHeaderBean;
import com.appspro.fusionsshr.dao.handoverDAO;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("/handover")
@JWTTokenNeeded
public class handoverService {

    handoverDAO handDao = new handoverDAO();
    
    @GET
    @Path("getAllHandover/{personId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllHandovers(@PathParam("personId") String personId) {
        ArrayList<HandoverHeaderBean> handoverList=new ArrayList<HandoverHeaderBean>();
        try {
                handoverList=handDao.getAllHandover(personId);
               } catch (Exception e) {
               e.printStackTrace();
               }
        return new JSONArray(handoverList).toString();
    }


    @GET
    @Path("getAllHandoverByHeaderId/{headerId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllHandoverByHeaderId(@PathParam("headerId") int headerId) {
        ArrayList<HandoverHeaderBean> handoverList=new ArrayList<HandoverHeaderBean>();
        try {
                handoverList=handDao.getAllHandoverByHeaderId(headerId);
               } catch (Exception e) {
               e.printStackTrace();
               }
        return new JSONArray(handoverList).toString();
    }
    
    @POST
    @Path("insertOrUpdateHandover/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateHandover(String body,@PathParam("transactionType") String transactionType) {
        
        HandoverHeaderBean returnedHeaderBean=new HandoverHeaderBean();
        try {
                ObjectMapper mapper = new ObjectMapper();
                HandoverHeaderBean headerBean = mapper.readValue(body, HandoverHeaderBean.class);
                returnedHeaderBean=handDao.insertOrUpdateXXX_handover(headerBean, transactionType);
               } catch (Exception e) {
               e.printStackTrace();
               }
        return new JSONObject(returnedHeaderBean).toString();
    }
    
    @POST
    @Path("updateHandOverStatus/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateHandOverStatus(String body) {
        
        HandoverHeaderBean returnedHeaderBean=new HandoverHeaderBean();
        try {
                ObjectMapper mapper = new ObjectMapper();
                HandoverHeaderBean headerBean = mapper.readValue(body, HandoverHeaderBean.class);
                returnedHeaderBean=handDao.updateHandoverStatus(headerBean);
               } catch (Exception e) {
               e.printStackTrace();
               }
        return new JSONObject(returnedHeaderBean).toString();
    }

}
