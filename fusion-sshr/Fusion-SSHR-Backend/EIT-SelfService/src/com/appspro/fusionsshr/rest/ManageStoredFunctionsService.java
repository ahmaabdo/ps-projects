package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.ManageStoredFunctionsBean;

import com.appspro.fusionsshr.dao.ManageStoredFunctionsDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;

@Path("/manageStoredFunctions")
public class ManageStoredFunctionsService {

    ManageStoredFunctionsDAO manageStoredFunctionsDAO = new ManageStoredFunctionsDAO();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllStoredFunctions() {
        try {
            return new JSONArray(manageStoredFunctionsDAO.getAllStoredFunctions()).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "[]";
        }
    }
    
    @GET
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getBodyContentById(@PathParam("id") String id) {
        try {
            return manageStoredFunctionsDAO.getBodyContentById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }


    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String createOrUpdateStoredFunction(String body, @PathParam("transactionType") String transactionType) {
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            ManageStoredFunctionsBean bean = mapper.readValue(body, ManageStoredFunctionsBean.class);
            return manageStoredFunctionsDAO.createOrUpdateStoredFunction(bean, transactionType);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }
    
    
    @POST
    @Path("/Search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getElementEntrySearchResult(String body) {
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            ManageStoredFunctionsBean bean = mapper.readValue(body, ManageStoredFunctionsBean.class);
            return new JSONArray(manageStoredFunctionsDAO.getAllStoredFunctionsSearchResult(bean)).toString();
             
        } catch (Exception e) {
            e.printStackTrace();
            return "[]";
        }
    }



}
