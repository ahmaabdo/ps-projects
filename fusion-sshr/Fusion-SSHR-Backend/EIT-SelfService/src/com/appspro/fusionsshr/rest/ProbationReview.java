package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.dao.PeopleExtraInformationDAO;
import com.appspro.fusionsshr.dao.ProbationReviewDAo;
import com.hha.appsproExceptionn.AppsProException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("probationReview")
@JWTTokenNeeded
public class ProbationReview {
    
    private PeopleExtraInformationDAO service = new PeopleExtraInformationDAO();
    private ProbationReviewDAo prDao=new ProbationReviewDAo();
    
        @POST
        @Path("/createOrUpdatePR/{transactionType}/{condetionCode}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public String insertNew(String body,@PathParam("transactionType") String transactionType
                                ,@PathParam("condetionCode") String condetionCode )  {
            
            PeopleExtraInformationBean list = new PeopleExtraInformationBean();
            
            PeopleExtraInformationBean bean =prDao.createInformationBean(body);
            try {
                list = service.insertOrUpdateXXX_EIT(bean, transactionType,condetionCode);
            } catch (AppsProException e) {
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
            
            return  new  JSONObject(list).toString();
        }
        
    @GET
    @Path("/getById/{requestId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getById(@PathParam("requestId") String requestId)  {
        
        PeopleExtraInformationBean bean=new PeopleExtraInformationBean();
        
        try {
            bean =prDao.getProbationReviewById(requestId);
        } catch (AppsProException e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        
        return  new  JSONObject(bean).toString();
    }
}
