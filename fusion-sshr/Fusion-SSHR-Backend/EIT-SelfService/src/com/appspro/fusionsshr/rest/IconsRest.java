/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.IconBean;
import com.appspro.fusionsshr.bean.PaaSDefultValueBean;
import com.appspro.fusionsshr.dao.IconsDAO;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

import utilities.JWTTokenNeeded;

/**
 *
 * @author lenovo
 */
@Path("Icons")
@JWTTokenNeeded
public class IconsRest {

    IconsDAO service = new IconsDAO();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getPassLookup(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

//        return service.getIcons();
          ArrayList<IconBean> list = new ArrayList<IconBean>();
        try {
          //  ArrayList<IconBean> list = new ArrayList<IconBean>();
            list = service.getIcons();
            return  new  JSONArray(list).toString();
            //    service.getIcons();
        } catch (Exception e) {
            return new  JSONArray(list).toString();
        }

    }

}
