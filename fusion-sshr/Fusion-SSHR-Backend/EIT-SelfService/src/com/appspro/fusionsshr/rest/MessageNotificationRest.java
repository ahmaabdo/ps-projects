package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ManageSelfServiceBean;
import com.appspro.fusionsshr.bean.MessageNotificationBean;
import com.appspro.fusionsshr.dao.MessageNotificationDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("/MsgNotification")
@JWTTokenNeeded
public class MessageNotificationRest {

    MessageNotificationDAO services = new MessageNotificationDAO();

    @GET
    @Path("/getMessageNotification/{selfService}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllMessagesNotification(@PathParam("selfService")
        String selfService) {

        List<MessageNotificationBean> list =
            new ArrayList<MessageNotificationBean>();
        list = services.getMessageNotificationBySelfService(selfService);
        return new JSONArray(list).toString();
    }

    @POST
    @Path("addMsgNotification")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addMessageNotification(String body) {

        MessageNotificationBean list = new MessageNotificationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            MessageNotificationBean bean =
                mapper.readValue(body, MessageNotificationBean.class);
            list = services.addMessageNotification(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            return new JSONObject(list).toString();
        }
    }

    @POST
    @Path("updateMsgNotification/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateMessageNotification(String body, @PathParam("id")
        String id) {

        MessageNotificationBean list = new MessageNotificationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            MessageNotificationBean bean =
                mapper.readValue(body, MessageNotificationBean.class);
            list = services.updateMessageNotification(bean, id);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            return new JSONObject(list).toString();
        }
    }

    @GET
    @Path("deleteMessageNotification/{id}/{attachSource}")
    public String deleteMessageNotification(@PathParam("id")
        String id, @PathParam("attachSource")
        String attachSource) {
        String status = "";

        services.deleteNotificationMessage(id, attachSource);
        return status;
    }


    @GET
    @Path("AttachementDetails/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getMsgNotiAttachmentByESelfServ(@PathParam("id")
        String id) {

        ArrayList<MessageNotificationBean> list =
            new ArrayList<MessageNotificationBean>();
        list = services.getAttachmentByESelfServ(id);
        return new JSONArray(list).toString();
    }

    
    @GET
    @Path("deleteAttachment/{id}")
    public String deleteAttachment(@PathParam("id")
        String id ) {
        String status = "";

        services.deleteAttachment(id);
        return status;
    }
}


