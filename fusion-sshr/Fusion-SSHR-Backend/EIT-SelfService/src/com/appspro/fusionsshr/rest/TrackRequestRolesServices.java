package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.TrackRequestRolesBean;
import com.appspro.fusionsshr.dao.TrackRequestRolesDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("trackRoles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@JWTTokenNeeded
public class TrackRequestRolesServices {

    private TrackRequestRolesDAO services = new TrackRequestRolesDAO();

    @POST
    @Path("/createOrUpdateTrackRoles/{transactionType}")
    public String insertOrUpdateTrackRoles(String body,
                                           @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        HttpSession session = request.getSession();
        TrackRequestRolesBean list = new TrackRequestRolesBean();
        try {
            AppsproConnection.LOG.info(body);
            ObjectMapper mapper = new ObjectMapper();
            TrackRequestRolesBean bean =
                mapper.readValue(body, TrackRequestRolesBean.class);
            list =services.insertOrUpdateTrackRequestRoles(bean, transactionType);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONObject(list).toString();
    }


    @POST
    @Path("/search")
    public String searchByRoles(String body) {
        ArrayList<TrackRequestRolesBean> roleList =
            new ArrayList<TrackRequestRolesBean>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            TrackRequestRolesBean bean =
                mapper.readValue(body, TrackRequestRolesBean.class);
            roleList = services.searchByRoles(bean);
            return new JSONArray(roleList).toString();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(roleList).toString();
    }

    @GET
    @Path("/getAllTrackRoles")
    public String getAllTrackRoles(@Context
        HttpServletRequest request) {
        List<TrackRequestRolesBean> list =
            new ArrayList<TrackRequestRolesBean>();
        list = services.getAllTrackRoles();
        return new JSONArray(list).toString();
    }

    @GET
    @Path("/getAllByFlag/{flagNum}")
    public String getAllByFlag(@Context
        HttpServletRequest request, @PathParam("flagNum")
        int flagNum) {
        List<TrackRequestRolesBean> list =
            new ArrayList<TrackRequestRolesBean>();
        list = services.getAllByFlag(flagNum);
        return new JSONArray(list).toString();
    }

    @GET
    @Path("deleteTrackRoles/{id}")
    public String deleteTrakRequestRoles(@PathParam("id")
        String id) {
        String status = "";

        status = services.deleteTrakRequestRoles(id);
        return status;
    }

    @GET
    @Path("deleteTrackRolesByFlag/{flag}")
    public String deleteTrackRolesByFlag(@PathParam("flag")
        int flag) {
        String status = "";

        status = services.deleteTrakRequestRolesByFlag(flag);
        return status;
    }

    @POST
    @Path("/getAllEitCode")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllEit(String body, @Context
        HttpServletRequest request) {
        JSONObject jsonObj = new JSONObject(body);

        String ID = jsonObj.get("ID").toString();
        List<TrackRequestRolesBean> list =
            new ArrayList<TrackRequestRolesBean>();
        list = services.getAllEit(ID);
        return new JSONArray(list).toString();
    }

   
}
