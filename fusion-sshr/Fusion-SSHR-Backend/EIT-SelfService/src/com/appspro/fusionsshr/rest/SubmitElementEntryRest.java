/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.db.CommonConfigReader;
import com.appspro.fusionsshr.bean.ElementEntrySetupBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.TrackServerRequestBean;
import com.appspro.fusionsshr.dao.AbsenceDAO;
import com.appspro.fusionsshr.dao.EFFDetails;
import com.appspro.fusionsshr.dao.ElementEntryHelper;

import com.appspro.fusionsshr.dao.ElementEntrySetupDAO;
import com.appspro.fusionsshr.dao.PeopleExtraInformationDAO;

import com.bea.core.repackaged.apache.bcel.classfile.ExceptionTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.text.SimpleDateFormat;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import javax.ws.rs.PUT;
import javax.ws.rs.core.Response;

import org.joda.time.LocalDate;
import java.util.Date;

import utilities.JWTTokenNeeded;

/**
 *
 * @author Shadi Mansi-PC
 */
@Path("/upload")
@JWTTokenNeeded
public class SubmitElementEntryRest extends ElementEntryHelper {

    public SubmitElementEntryRest() {
        super();
    }

    @POST
    @Path("/elementEntry")
    @Consumes(MediaType.APPLICATION_JSON)
    public String upload(InputStream incomingData, @Context
        HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        
        JSONObject jObject;
        JSONObject jObjectRespones = null;
        EFFDetails effServices = new EFFDetails();

        try {
            BufferedReader in =
                new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
            jObject = new JSONObject(crunchifyBuilder.toString());

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            return "error";
        }

        try {
            AbsenceDAO service = new AbsenceDAO();
            String trsId = jObject.get("trsId").toString();
            PeopleExtraInformationDAO peopleExtraInformationDAO = new PeopleExtraInformationDAO();
            PeopleExtraInformationBean eitBean = peopleExtraInformationDAO.getEITById(trsId);
            
            JSONObject effJson = new JSONObject();
            String eitLblString = eitBean.getEit().replaceAll("\\\\", "");
            eitLblString = eitLblString.substring(1, eitLblString.length() - 1);
            
            effJson.put("model", eitLblString);
            effJson.put("TRS_ID", eitBean.getId());
            effJson.put("eitCode", eitBean.getCode());
            effJson.put("url", eitBean.getUrl());
            
            effServices.addEitOnSaaS(effJson, trsId);
            
            //Create Element Entry File 
            
            ElementEntrySetupDAO elementEntrySetupDAO = new ElementEntrySetupDAO();
                    JSONObject eitJSON = new JSONObject(eitLblString); //Replace with EIT
                    ArrayList<ElementEntrySetupBean> list = null;
            if("XXX_BUSINESS_TRIP_REQUEST".equals(eitBean.getCode())){
                    list =
                        elementEntrySetupDAO.getElementEntryByEITForBT(eitBean.getCode(), eitJSON.optString("travelType"));
            }
            else{
                list =
                    elementEntrySetupDAO.getElementEntryByEIT(eitBean.getCode());
            }
                    JSONArray elementJSONArray = new JSONArray(list);
                    JSONObject absenceJson = new JSONObject();
                    for (int j = 0; j < list.size(); j++) {
                        if("ElementEntry".equals(list.get(j).getTypeOfAction())){
                        JSONArray inputValueArray =
                            new JSONArray(elementJSONArray.getJSONObject(j).get("inputValue").toString());
                        System.out.println(new JSONArray(list).toString());
                        System.out.println();
                        
                        for (int i = 0; i < inputValueArray.length(); i++) {
                            
                            JSONObject inputvalue = inputValueArray.getJSONObject(i);
                            if (!inputvalue.isNull("inputEitValue") &&
                                !inputvalue.optString("inputEitValue").isEmpty()) {
                                if (inputvalue.optString("inputEitValue").equals("Transaction_ID")) {
                                    inputvalue.put("inputEitValue",
                                                   "P" + eitBean.getId());
                                } else if (inputvalue.optString("inputEitValue").equals("effivtiveStartDate")) {
                                    inputvalue.put("inputEitValue",
                                                   "%EffectiveStartDate%");
                                } else {
                                    inputvalue.put("inputEitValue",
                                                   eitJSON.optString(inputvalue.optString("inputEitValue")));
                                }
                            }
                        }
                        System.out.println(inputValueArray);
                        
                        JSONObject obj = new JSONObject();
                        
                        obj.put("personNumber", eitBean.getPerson_number());
                        obj.put("elementName", elementJSONArray.getJSONObject(j).optString("elementName"));
                        obj.put("legislativeDataGroupName", elementJSONArray.getJSONObject(j).optString("legislativeDatagroupName"));
                        obj.put("assignmentNumber", eitBean.getAssignmentNumber());
                        obj.put("entryType","E");
                        obj.put("creatorType", elementJSONArray.getJSONObject(j).optString("creatorType"));
                        obj.put("sourceSystemId","PaaS_" + eitBean.getId() +"_"+j);
                        obj.put("trsId",eitBean.getId());
                        obj.put("SSType", elementJSONArray.getJSONObject(j).optString("recurringEntry"));
                        obj.put("eitCode",eitBean.getCode());
                        obj.put("inputValues", inputValueArray);
                        obj.put("effivtiveStartDate", "%EffectiveStartDate%");
                        
                        System.out.println(obj.toString());
                        respone = createHelperBT("", obj, null);
                        }
                        else if("Absence".equals(list.get(j).getTypeOfAction())){
                            absenceJson.put("personNumber", eitBean.getPerson_number());
                            absenceJson.put("employer", "tbc");
                            absenceJson.put("absenceType", list.get(j).getAbsenceType());
                            absenceJson.put("startDate", eitJSON.optString(list.get(j).getStartDate()));
                            absenceJson.put("endDate", eitJSON.optString(list.get(j).getEndDate()));
                            absenceJson.put("absenceStatusCd", list.get(j).getAbsenceStatus());
                            absenceJson.put("startDateDuration", 1);
                            absenceJson.put("endDateDuration", 1);
                            System.out.println(absenceJson);
                            service.postAbsence(absenceJson.toString());
                        }
                    }
            
            // End 

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            TrackServerRequestBean bean = new TrackServerRequestBean();
            bean.setRequestId(jObject.get("trsId").toString());
            bean.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
            bean.setElementResponseCode(500 + "");
            bean.setElementResponseDesc(e.getMessage());
            EFFDetails.getInstance().editElementResponseDetails(bean);
        }

        return respone;
    }


    @POST
    @Path("/createBankFile")
    @Consumes(MediaType.APPLICATION_JSON)
    public String uploadCreateBankFile(InputStream incomingData, @Context
        HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        try {
            BufferedReader in =
                new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
        } catch (Exception e) {
            //("Error Parsing: - ");
        }
        JSONObject jObject;
        JSONObject jObjectRespones = null;
        try {
            jObject = new JSONObject(crunchifyBuilder.toString());
            HttpSession session = request.getSession();
            ServletContext context = session.getServletContext();
            String path = context.getRealPath(request.getContextPath());

            if (null != path && path.contains("HHA-SSHR-BackEnd")) {
                path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
            }
            if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
                path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
            } else {
                path = System.getProperty("java.scratch.dir");
            }


            respone = createHelperBankFile(path, jObject);

            jObjectRespones = new JSONObject("{'status' : '" + respone + "'}");
            //            //(jObject.toString());
        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        //(respone);
        return respone;
    }

    @POST
    @Path("/createPersonalPaymentMethod")
    @Consumes(MediaType.APPLICATION_JSON)
    public String uploadCreatePersonalPaymentMethod(InputStream incomingData,
                                                    @Context
        HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        try {
            BufferedReader in =
                new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
        } catch (Exception e) {
            //("Error Parsing: - ");
        }
        JSONObject jObject;
        JSONObject jObjectRespones = null;
        try {
            jObject = new JSONObject(crunchifyBuilder.toString());
            HttpSession session = request.getSession();
            ServletContext context = session.getServletContext();
            String path = context.getRealPath(request.getContextPath());

            if (null != path && path.contains("HHA-SSHR-BackEnd")) {
                path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
            }
            if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
                path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
            } else {
                path = System.getProperty("java.scratch.dir");
            }


            respone = createHelperPersonalPaymentMethod(path, jObject);

            jObjectRespones = new JSONObject("{'status' : '" + respone + "'}");
            //            //(jObject.toString());
        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }

        //(respone);
        return respone;
    }

    @PUT
    @Path("/elementEntry")
    @Consumes(MediaType.APPLICATION_JSON)
    public String submit(InputStream incomingData, @Context
        HttpServletRequest request) {
     
        HttpSession session = request.getSession();
        ServletContext context = session.getServletContext();
        StringBuilder crunchifyBuilder = new StringBuilder();
        String path = context.getRealPath(request.getContextPath());

        if (null != path && path.contains("HHA-SSHR-BackEnd")) {
            path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
        }
        if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
            path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
        } else {
            path = System.getProperty("java.scratch.dir");
        }

        //        try {
        //            BufferedReader in =
        //                new BufferedReader(new InputStreamReader(incomingData));
        //            String line = null;
        //            while ((line = in.readLine()) != null) {
        //                crunchifyBuilder.append(line);
        //            }
        //        } catch (Exception e) {
        //            //("Error Parsing: - ");
        //        }
        JSONArray jObject;

        try {
            //            //(crunchifyBuilder.toString().replaceAll("[\\[\\]]", ""));
            jObject =
                    new JSONArray(crunchifyBuilder.toString()); //.replaceAll("[\\[\\]]", ""));
            //            //(jObject);
            for (int i = 0; i < jObject.length() - 1; i++) {
                //                //(jObject.getJSONObject(i).getString("sstype"));
                //jObject.getJSONObject(i).getString("sstype")

                callUCM(jObject.getJSONObject(i).get("id").toString(),
                        jObject.getJSONObject(jObject.length() -
                                              1).getString("startDate"), path,
                        jObject.getJSONObject(i).getString("sstype"),
                        jObject.getJSONObject(i).getString("file_bytes"),
                        jObject.getJSONObject(i).getString("file_name"));
            }

        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }


        return Response.ok().build().toString();
    }
    
    
    
    @POST
    @Path("/elementEntryBulk")
    @Consumes(MediaType.APPLICATION_JSON)
    public String uploadElementBulk(InputStream incomingData, @Context
        HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        
        JSONObject jObject;
        JSONObject jObjectRespones = null;

        try {
            BufferedReader in =
                new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
            jObject = new JSONObject(crunchifyBuilder.toString());

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            return "error";
        }

        try {
            HttpSession session = request.getSession();
            ServletContext context = session.getServletContext();
            String path = context.getRealPath(request.getContextPath());

            if (null != path && path.contains("HHA-SSHR-BackEnd")) {
                path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
            }
            if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
                path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
            } else {
                path = System.getProperty("java.scratch.dir");
            }

            respone = createHelperBulkElement(path, jObject, null);
        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            TrackServerRequestBean bean = new TrackServerRequestBean();
            bean.setRequestId(jObject.get("trsId").toString());
            bean.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
            bean.setElementResponseCode(500 + "");
            bean.setElementResponseDesc(e.getMessage());
            EFFDetails.getInstance().editElementResponseDetails(bean);
        }

        return respone;
    }
    @POST
      @Path("/EducationAllowancePlanFile")
      @Consumes(MediaType.APPLICATION_JSON)
      public String uploadEducationAllowancePlanFile(InputStream incomingData, @Context HttpServletRequest request) {
          StringBuilder crunchifyBuilder = new StringBuilder();
          String respone = null;
          try {
              BufferedReader in =
                  new BufferedReader(new InputStreamReader(incomingData));
              String line = null;
              while ((line = in.readLine()) != null) {
                  crunchifyBuilder.append(line);
              }
          } catch (Exception e) {
              //("Error Parsing: - ");
          }
          JSONObject jObject;
          JSONObject jObjectRespones = null;
          try {
              jObject = new JSONObject(crunchifyBuilder.toString());
              HttpSession session = request.getSession();
              ServletContext context = session.getServletContext();
              String path = context.getRealPath(request.getContextPath());

              if (null != path && path.contains("HHA-SSHR-BackEnd")) {
                  path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
              }
              if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
                  path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
              } else {
                  path = System.getProperty("java.scratch.dir");
              }


              respone = createEducationAllowanceFile(path, jObject);

              jObjectRespones = new JSONObject("{'status' : '" + respone + "'}");
              //            //(jObject.toString());
          } catch (JSONException e) {
             e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
          }

          //(respone);
          return respone;
      }

}
