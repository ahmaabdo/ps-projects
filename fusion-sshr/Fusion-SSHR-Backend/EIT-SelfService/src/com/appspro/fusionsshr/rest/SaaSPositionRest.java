/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.SaaSPositionBean;
import com.appspro.fusionsshr.dao.SaaSPositionDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;

import utilities.JWTTokenNeeded;

/**
 *
 * @author user
 */
@Path("/saasposition")
@JWTTokenNeeded
public class SaaSPositionRest {

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEmpDetails(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

     
        SaaSPositionDetails det = new SaaSPositionDetails();

        ArrayList<SaaSPositionBean> organizationList = det.getOrgDetails();
       
        return  new  JSONArray(organizationList).toString();
       // return organizationList;

    }

}
