package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.TrackServerRequestBean;
import com.appspro.fusionsshr.dao.EFFDetails;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

import utilities.JWTTokenNeeded;

@Path("loggingRest")
@JWTTokenNeeded
public class loggingRest {

    @GET
    public Response get() {
        List<TrackServerRequestBean> res = EFFDetails.getInstance().getLog();
        return Response.ok(new JSONArray(res).toString()).build();
    }
    
    
    
    

    @POST
    @Path("/searchTrackServerRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public String getRequestsBySearch(String body) {
        List<TrackServerRequestBean> list =
            new ArrayList<TrackServerRequestBean>();
        EFFDetails services = new EFFDetails();
        try {
            ObjectMapper mapper = new ObjectMapper();
            TrackServerRequestBean bean =
                mapper.readValue(body, TrackServerRequestBean.class);
            list = services.getLogData(bean);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return new JSONArray(list).toString();
    }


}
