package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.ElementEntryObservationBean;

import com.appspro.fusionsshr.dao.ElementEntryHelper;
import com.appspro.fusionsshr.dao.ElementEntryObservationDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("/elementEntryObservation")
@JWTTokenNeeded
public class ElementEntryObservationService {

    ElementEntryObservationDAO elementEntryObservationDAO = new ElementEntryObservationDAO();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getElementEntries() {
        try {
            return new JSONArray(elementEntryObservationDAO.getAllElementEntries()).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "[]";
        }
    }
    
    @GET
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getElementEntry(@PathParam("id") String id) {
        try {
            return elementEntryObservationDAO.getElementEntryById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }


    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String insertOrUpdateElementEntry(String body, @PathParam("transactionType") String transactionType) {
        ElementEntryObservationBean list = new ElementEntryObservationBean();
        ElementEntryHelper elementEntryHelper = new ElementEntryHelper();
        
        try {            
            ObjectMapper mapper = new ObjectMapper();
            ElementEntryObservationBean bean = mapper.readValue(body, ElementEntryObservationBean.class);
            list = elementEntryObservationDAO.updateElementEntry(bean, transactionType);
            elementEntryHelper.createElementFromPaaS(new JSONObject(body));
            return "Success";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }
    
    
    
    @POST
    @Path("/Search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getElementEntrySearchResult(String body) {
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            ElementEntryObservationBean bean = mapper.readValue(body, ElementEntryObservationBean.class);
            return new JSONArray(elementEntryObservationDAO.getElementEntrySearchResult(bean)).toString();
            
        } catch (Exception e) {
            e.printStackTrace();
            return "[]";
        }
    }



}
