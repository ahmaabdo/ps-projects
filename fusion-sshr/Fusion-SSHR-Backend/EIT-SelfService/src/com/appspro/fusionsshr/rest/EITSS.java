/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.EitsBean;
import com.appspro.fusionsshr.dao.EitsDAO;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

import utilities.JWTTokenNeeded;

/**
 *
 * @author user
 */
@Path("EITSS")
@JWTTokenNeeded
public class EITSS {

    /**
     * This is a sample web service operation
     */
    EitsDAO service = new EitsDAO();

    @POST
    @Path("/createOrUpdateEIT/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertXXX_EIT(EitsBean XXX_edit,
                                  @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            return Response.ok(service.insertOrUpdateXXX_EIT(XXX_edit,
                                                             transactionType),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/{ID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchPositionsByPositionName(@PathParam("ID")
        String ID, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            return Response.ok(service.getEITById(ID),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/details/{eitCode}/{personId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEitByPersonId(@PathParam("eitCode")
        String eitCode, @PathParam("personId")
        String personId) {
        ArrayList<EitsBean> list = new ArrayList<EitsBean>();
        list = service.getEITsByPersonId(eitCode, personId);
        return  new  JSONArray(list).toString();
    }

}
