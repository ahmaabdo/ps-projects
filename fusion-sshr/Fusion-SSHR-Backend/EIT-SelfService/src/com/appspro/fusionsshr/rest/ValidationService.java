/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.fusionsshr.bean.ValidationBean;
import com.appspro.fusionsshr.dao.ValidationDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;


/**
 *
 * @author lenovo
 */
@Path("validation")
@JWTTokenNeeded
public class ValidationService {

    ValidationDAO validationDAO = new ValidationDAO();

    //    @POST
    //    @Path("/addValidation/")
    //    @Consumes(MediaType.APPLICATION_JSON)
    //    @Produces(MediaType.APPLICATION_JSON)
    //    public ValidationBean addValidation(ValidationBean validationBean) {
    //        return validationDAO.addValidation(validationBean);
    //    }


    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateValidation(String body,
                                             @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        
        ValidationBean list = new ValidationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ValidationBean bean = mapper.readValue(body, ValidationBean.class);
            list = validationDAO.insertOrUpdateValidation(bean,transactionType);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }
    }

    @POST
    @Path("/action")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String actionValidation(@FormParam("eitCode")
        String eitCode) {
        ArrayList<ValidationBean> validationList =
            new ArrayList<ValidationBean>();
        try {
          //  ArrayList<IconBean> list = new ArrayList<IconBean>();
            validationList = validationDAO.actionValidation(eitCode);
            return  new  JSONArray(validationList).toString();
            //    service.getIcons();
        } catch (Exception e) {
            return new  JSONArray(validationList).toString();
        }
    }

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchValidation(@FormParam("eitCode")
        String eitCode) {
        ArrayList<ValidationBean> validationList =
            new ArrayList<ValidationBean>();
        try {
          //  ArrayList<IconBean> list = new ArrayList<IconBean>();
            validationList = validationDAO.searchValidation(eitCode);
            return  new  JSONArray(validationList).toString();
            //    service.getIcons();
        } catch (Exception e) {
            return new  JSONArray(validationList).toString();
        }
    }


    @GET
    @Path("/reportnames")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllJobs() {
        ArrayList<SummaryReportBean> list = new ArrayList<SummaryReportBean>();
        
        try {
          //  ArrayList<IconBean> list = new ArrayList<IconBean>();
          list = validationDAO.getAllReportName();
            return  new  JSONArray(list).toString();
            //    service.getIcons();
        } catch (Exception e) {
            return new  JSONArray(list).toString();
        }
    }


    @GET
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteValidation(@PathParam("id")
        String id) {

        validationDAO.deleteValidationRow(id);

    }

    @GET
    @Path("/validationValue/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String validationValues(@PathParam("code")
        String code) {
        ArrayList<ValidationBean> list =
            new ArrayList<ValidationBean>();
        try {
          list = validationDAO.validationValueName(code);
            return  new  JSONArray(list).toString();
        } catch (Exception e) {
            return new  JSONArray(list).toString();
        }
    }


    //    @POST
    //    @Path("/validationCount/{transactipnType}")
    //    @Consumes(MediaType.APPLICATION_JSON)
    //    @Produces(MediaType.APPLICATION_JSON)
    //    public Response  getValidationCount(ValidationBean validationBean,@PathParam("transactipnType") String transactipnType,@Context HttpServletRequest request,@Context HttpServletResponse response,@HeaderParam("Authorization") String authString) {
    //        int count = -1;
    //                   try{
    //                        count = validationDAO.getValidationCount(validationBean,transactipnType);
    //        } catch(Exception e) {
    //           return Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode(),e.getMessage()).build();
    //        }
    //
    //         return  Response.ok(new Integer(count), MediaType.APPLICATION_JSON).build();
    //
    //    }


}
