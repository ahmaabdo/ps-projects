/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.dao.EmployeeDetails;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.login.Login;

import io.jsonwebtoken.Jwts;

import io.jsonwebtoken.SignatureAlgorithm;

import java.io.IOException;

import java.security.NoSuchAlgorithmException;

import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;


@Path("/login")
public class LogInRest {

    @POST
    @Consumes("application/json")
    @Path("/login2")
    public Response getEmpDetails(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        String jsonInString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(body);
            String userName = actualObj.get("userName").asText();
            String password = actualObj.get("password").asText();
            boolean mainData = actualObj.get("mainData").asBoolean();
            Login login = new Login();
            boolean result = login.validateLogin(userName, password);
            JSONObject json = new JSONObject();
            json.put("result", result);
            if (result) {
                String encoding =
                    DatatypeConverter.printBase64Binary((userName + ":" +
                                                         password).getBytes("UTF-8"));

                HttpSession session = request.getSession();
                session.setAttribute("authorization", "Basic " + encoding);
                String token = issueToken(userName);
                json.put("jwt", token);
            }

            return Response.ok(json.toString(),
                               MediaType.APPLICATION_JSON).build();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            return Response.status(500).entity(e.getMessage()).build();
        } catch (IOException e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return Response.ok(jsonInString, MediaType.APPLICATION_JSON).build();

    }

    public LogInRest() {
        super();
    }

    private String issueToken(String username) throws NoSuchAlgorithmException {


        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, 60);

        String jwtToken =
            Jwts.builder().setSubject(username).setIssuer("Abdullah Oweidi").setIssuedAt(new Date()).setExpiration(calendar.getTime()).signWith(SignatureAlgorithm.HS512,
                                                                                                                                                "P@$$w0rdOW".getBytes()).compact();
        System.out.println("#### generating token for a key : " + jwtToken);
        return jwtToken;

    }

    @POST
    @Consumes("application/json")
    @Path("/generatetoken")
    public Response generateToken(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        String jsonInString = null;
        JSONObject json = new JSONObject();
        try {
            JSONObject jwtJson = new JSONObject(body);
            String saasJWT = jwtJson.getString("jwt");
            String[] split_string = saasJWT.split("\\.");
            String base64EncodedBody = split_string[1];
            String tokenBody =
                new String(Base64.getDecoder().decode(base64EncodedBody));
            System.out.println("JWT Body : " + tokenBody);
            JSONObject paasTokernJSON = new JSONObject(tokenBody);
            String username = paasTokernJSON.getString("sub");
            String token = issueToken(username);
            json.put("jwt", token);
            return Response.ok(json.toString(),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

}
