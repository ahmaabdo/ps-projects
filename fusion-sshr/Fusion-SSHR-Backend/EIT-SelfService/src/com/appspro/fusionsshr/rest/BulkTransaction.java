/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.ValidationBean;
import com.appspro.fusionsshr.dao.BulkTransactionDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import utilities.JWTTokenNeeded;


/**
 *
 * @author Shadi Mansi-PC
 */
@Path("/bulkTransaction")
@JWTTokenNeeded
public class BulkTransaction {

    BulkTransactionDAO service = new BulkTransactionDAO();

    @POST
    @Path("/createBulkTransactionPersonId/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getBulkTransactionByPersonsId(String body,
                                                  @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        PeopleExtraInformationBean list = new PeopleExtraInformationBean();
        AppsproConnection.LOG.info("test"+body);
        try {
            
            
            list = service.executeBulkTransactionByPersonId(body, transactionType);
            return  new  JSONObject(list).toString();
            
        } catch (Exception e) {
            return  new  JSONObject(list).toString();
        }
    }

    @POST
    @Path("/createBulkTransactionDept/{deptId}/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBulkTransactionByDepartment(PeopleExtraInformationBean informationBean,
                                                   @PathParam("deptId")
        String deptId, @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            return Response.ok(service.executeBulkTransactionByDept(informationBean,
                                                                    deptId,
                                                                    transactionType),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

}
