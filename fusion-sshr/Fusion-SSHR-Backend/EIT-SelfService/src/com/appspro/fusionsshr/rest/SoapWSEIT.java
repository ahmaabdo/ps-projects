package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.EducationRequestSoapBean;
import com.appspro.fusionsshr.bean.EitsBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.dao.ExpenseDAO;
import com.appspro.fusionsshr.dao.PeopleExtraInformationDAO;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIPReports;
import common.biPReports.BIReportModel;

import java.util.ArrayList;

import javax.jws.WebMethod;
import javax.jws.WebService;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.json.XML;

import utilities.JWTTokenNeeded;

@WebService(serviceName = "EITWebService", portName = "EITWebServicePort")
@JWTTokenNeeded
public class SoapWSEIT {
   
   @WebMethod(operationName = "busniessTripReport")
    public ArrayList<EitsBean> BusniessTripReport() throws AppsProException {
        PeopleExtraInformationDAO dao =  new PeopleExtraInformationDAO();
        ArrayList<EitsBean> eitlist = new ArrayList<EitsBean>();
        try{
        eitlist = dao.getBusniessTripRequest();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return eitlist;
    }  
   
    @WebMethod(operationName = "educationExpenceReport")
     public ArrayList<EducationRequestSoapBean> EducationExpenceReport() throws AppsProException {
         ExpenseDAO dao =  new ExpenseDAO();
         ArrayList<EducationRequestSoapBean> eitlist = new ArrayList<EducationRequestSoapBean>();
         try{
         eitlist = dao.GetAllEducationExpenseRequestForWS();
         }
         catch(Exception e){
             e.printStackTrace();
         }
         return eitlist;
     }   
}
