package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.EmailLogTrackerBean;

import com.appspro.fusionsshr.dao.EmailLogTrackerDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;

@Path("/emailLogTracker")
public class EmailLogTrackerService {

    EmailLogTrackerDAO emailLogTrackerDAO = new EmailLogTrackerDAO();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllEmailsLogs() {
        try {
            return new JSONArray(emailLogTrackerDAO.getAllEmailsLogs()).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "[]";
        }
    }
    
    @GET
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getEmailMsgBodyById(@PathParam("id") String id) {
        try {
            return emailLogTrackerDAO.getEmailMsgBodyById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }


    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String resendEmailFromPaaS(String body, @PathParam("transactionType") String transactionType) {
        EmailLogTrackerBean list = new EmailLogTrackerBean();
        
        try {            
            ObjectMapper mapper = new ObjectMapper();
            EmailLogTrackerBean bean = mapper.readValue(body, EmailLogTrackerBean.class);
            list = emailLogTrackerDAO.resendEmailFromPaaS(bean, transactionType);
            return "Success";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }
    
    
    @POST
    @Path("/Search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getElementEntrySearchResult(String body) {
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            EmailLogTrackerBean bean = mapper.readValue(body, EmailLogTrackerBean.class);
            return new JSONArray(emailLogTrackerDAO.getAllEmailsLogsSearchResult(bean)).toString();
            
        } catch (Exception e) {
            e.printStackTrace();
            return "[]";
        }
    }



}
