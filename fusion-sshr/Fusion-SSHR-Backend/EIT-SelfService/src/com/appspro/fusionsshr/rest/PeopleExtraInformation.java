/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.CountEITRequest;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.dao.PeopleExtraInformationDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.hha.appsproExceptionn.AppsProException;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;


/**
 *
 * @author Shadi Mansi-PC
 */
@Path("PeopleExtra")
@JWTTokenNeeded
public class PeopleExtraInformation {

    private PeopleExtraInformationDAO service =
        new PeopleExtraInformationDAO();

    //******************Start Insert Or Update People Extra (POST)**************

    @POST
    @Path("/createOrUpdateEIT/{transactionType}/{condetionCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertXXX_EIT(String body,
                                  @PathParam("transactionType")
                                 
        String transactionType , @PathParam("condetionCode") String condetionCode, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        HttpSession session = request.getSession();
        PeopleExtraInformationBean list = new PeopleExtraInformationBean();
        try {
            AppsproConnection.LOG.info(body);
            ObjectMapper mapper = new ObjectMapper();
            PeopleExtraInformationBean bean = mapper.readValue(body, PeopleExtraInformationBean.class);
            list = service.insertOrUpdateXXX_EIT(bean, transactionType,condetionCode);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            AppsproConnection.LOG.error("ERROR", e);
        }
        return  new  JSONObject(list).toString();
    }
    //********************End Insert Or Update People Extra (POST)**************

    //******************Start Get People Extra People Extra By Id (GET)*********

    @GET
    @Path("/{ID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchPositionsByPositionName(@PathParam("ID")
        String ID, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        
        PeopleExtraInformationBean obj = new PeopleExtraInformationBean();

        try {
            obj = service.getEITById(ID);
        } catch (Exception e) {
            AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONObject(obj).toString();
    }
    //******************End Get People Extra By Id (GET)************************

    //******************Start Get People Extra By Person Id (GET)***************

    @GET
    @Path("/details/{eitCode}/{personId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEitByPersonId(@PathParam("eitCode")
        String eitCode, @PathParam("personId")
        String personId) throws AppsProException {
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        list = service.getEITsByPersonId(eitCode, personId);
         return  new  JSONArray(list).toString();
    }
    
    @GET
    @Path("/updateStatus")
   // @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateStatus(@QueryParam("id")
        int id) throws AppsProException {
        
        service.updateStatus("APPROVED","",id);
         return "";
    }

    //******************End Get People Extra By Person Id (GET)*****************

    @POST
    @Path("/getQuery")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getDymanicQueryReport(@FormParam("query")
        String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            return service.getDynamicQuery(body) ;
        } catch (Exception e) {
            return "";
        }
    }
    
    @GET
    @Path("countRequest/{personId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getPendingApprovalByPersonID(@PathParam("personId") String personId, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws AppsProException {
        CountEITRequest list = new CountEITRequest();

        list = service.getAllCountRequestByPersonId(personId);

         return  new  JSONObject(list).toString();
    }
    @GET
    @Path("rejectReason/{Id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getRejectReason(@PathParam("Id") String Id) throws AppsProException {
        PeopleExtraInformationBean bean=new PeopleExtraInformationBean();
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        list= service.getRejectReason(Id);
        return  new  JSONArray(list).toString();
    }
    
    @POST
    @Path("/getNotAttachedElement")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getNotAttachedElement(String body) {

        try {
            JSONObject jsonObj = new JSONObject(body);
            String Q = jsonObj.getString("Q");
            ArrayList<PeopleExtraInformationBean> list =
                new ArrayList<PeopleExtraInformationBean>();
            System.out.println("Q = "+Q);
            list= service.getNotAttachedElement(Q);
            
            return  new  JSONArray(list).toString();
           
        } catch (Exception e) {
            return "";
        }
    }

    @GET
    @Path("delete/{Id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteEitById(@PathParam("Id")
        String Id) {
        JSONObject response = new JSONObject();
        try {
            response = service.deleteEitRequestById(Id);
            return Response.ok(response.toString(),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error(e);
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

}
