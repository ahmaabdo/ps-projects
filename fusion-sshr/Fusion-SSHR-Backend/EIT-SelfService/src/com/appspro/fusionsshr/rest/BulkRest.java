package com.appspro.fusionsshr.rest;

import com.appspro.bulk.excel.GenerateDynamicBulk;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.dao.BulkDao;

import common.biPReports.BIReportModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;


@Path("/bulk")
@JWTTokenNeeded
public class BulkRest {


//    @POST
//    @Path("/upload")
//    @Consumes(MediaType.MULTIPART_FORM_DATA)
//    @Produces(MediaType.TEXT_PLAIN)
//    public Response upload(@FormDataParam("file")
//        File file) {
//        try {
//            JSONObject res =
//                GenerateDynamicBulk.dynamicanalysisExcelBulk(file);
//            return Response.ok(res.toString()).build();
//        } catch (Exception e) {
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//            
//            return Response.ok("-1").build();
//        }
//    }
//
//    @GET
//    @Path("/download/{code}")
//    @Produces("application/xls")
//    public Response download(@PathParam("code")
//        String code) {
//        try {
//            String file_base64 = GenerateDynamicBulk.dynamicGenerate(code);
//            //            byte[] fbytes = Base64.decodeBase64(file_base64);
//            Response.ResponseBuilder response =
//                Response.ok(file_base64); //fbytes, "application/xls"
//            return response.build();
//        } catch (FileNotFoundException e) {
//            return Response.ok("-1").build(); //file not found
//        } catch (IOException e) {
//            return Response.ok("-2").build(); //error while generating bulk file
//        }
//    }
//
//    @GET
//    @Path("/menu")
//    public Response menu() {
//        return Response.ok(new JSONArray(BulkDao.getInstance().getAll()).toString()).build();
//    }
//
//
////    public static byte[] getBytesFromFile(File file) throws IOException {
////        // Get the size of the file
////        long length = file.length();
////
////        // You cannot create an array using a long type.
////        // It needs to be an int type.
////        // Before converting to an int type, check
////        // to ensure that file is not larger than Integer.MAX_VALUE.
////        if (length > Integer.MAX_VALUE) {
////            // File is too large
////            throw new IOException("File is too large!");
////        }
////
////        // Create the byte array to hold the data
////        byte[] bytes = new byte[(int)length];
////
////        // Read in the bytes
////        int offset = 0;
////        int numRead = 0;
////
////        InputStream is = new FileInputStream(file);
////        try {
////            while (offset < bytes.length &&
////                   (numRead = is.read(bytes, offset, bytes.length - offset)) >=
////                   0) {
////                offset += numRead;
////            }
////        } finally {
////            is.close();
////        }
////
////        // Ensure all the bytes have been read in
////        if (offset < bytes.length) {
////            throw new IOException("Could not completely read file " +
////                                  file.getName());
////        }
////        return bytes;
////    }
//
//    @POST
//    @Path("/get")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response get(String arr) {
//        JSONArray resArr = new JSONArray();
//        try {
//            JSONArray ar = new JSONArray(arr);
//            for (int x = 0; x < ar.length(); x++) {
//                JSONObject o = ar.getJSONObject(x);
//                String report_name = o.getString("reportName");
//                String param_key = o.getString("key");
//                String param_val = o.getString("value");
//
//                Map<String, String> paramMAp = new HashMap<String, String>();
//                paramMAp.put(param_key, param_val);
//                JSONObject respone =
//                    BIReportModel.runReport(report_name, paramMAp);
//                if (respone.has("DATA_DS") &&
//                    respone.getJSONObject("DATA_DS").has("G_1"))
//                    resArr.put(respone.getJSONObject("DATA_DS").getJSONArray("G_1")); //.getJSONObject("DATA_DS").getJSONObject("G_1")
//            }
//        } catch (JSONException e) {
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//        }
//        return Response.ok(resArr.toString()).build();
//    }
//
//    //    @Consumes(MediaType.APPLICATION_JSON)
//
//    @DELETE
//    @Path("/delMenu/{code}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response delMenu(@PathParam("code")
//        String codes) {
//        JSONObject res = BulkDao.getInstance().delete(codes);
//        return Response.ok(res.toString()).status(200).build();
//    }
//                                                       

    //        
    @POST
    @Path("/addToMenu")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addToMenu(String body) {
        JSONObject obj = new JSONObject(body);
        JSONObject res = BulkDao.getInstance().add(obj.getJSONArray("codes"));
        return Response.ok(res.toString()).status(200).build();
    }
}
