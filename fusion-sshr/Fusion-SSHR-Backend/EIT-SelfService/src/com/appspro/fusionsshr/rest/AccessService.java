package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.AccessBean;
import com.appspro.fusionsshr.dao.AccessDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/Access")
public class AccessService {
    AccessDAO serivces = new AccessDAO();
    
    @POST
    @Path("/addAccessSetup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addAccessSetup(String body) {

        AccessBean list = new AccessBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            AccessBean bean =
                mapper.readValue(body, AccessBean.class);
            list = serivces.addAccessSetup(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }
    @POST
    @Path("/updateAccessSetup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateAccessSetup(String body) {

        AccessBean list = new AccessBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            AccessBean bean =
                mapper.readValue(body, AccessBean.class);
            list = serivces.updateAccessSetup(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }
    @GET
    @Path("/deleteAccessSetup/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteAccessSetup(@PathParam("id")
        String id) {
        String status;

        status = serivces.deleteAccessSetup(id);
        return status;
    }
    @GET
    @Path("/getAllAccess")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllAccess() {

        List<AccessBean> list =
            new ArrayList<AccessBean>();
        list = serivces.getAllAccess();
        return new JSONArray(list).toString();
    }
    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchAccessSetup(String body) {
        ArrayList<AccessBean> approvalList =
            new ArrayList<AccessBean>();
        try {
        ObjectMapper mapper = new ObjectMapper();
        AccessBean bean = mapper.readValue(body, AccessBean.class);
        approvalList = serivces.searchAccessSetup(bean);
        return new JSONArray(approvalList).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(approvalList).toString();
    }
}
