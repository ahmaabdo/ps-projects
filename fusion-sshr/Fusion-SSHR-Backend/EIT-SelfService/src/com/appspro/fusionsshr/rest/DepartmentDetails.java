package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.OrgnizationBean;
import com.appspro.fusionsshr.bean.gradeBean;
import com.appspro.fusionsshr.bean.rest.departmentBean;
import com.appspro.fusionsshr.dao.DepartmentDAO;
import com.appspro.fusionsshr.dao.OrganizationDetails;
import com.appspro.fusionsshr.dao.gradeDetailsDAO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import utilities.JWTTokenNeeded;

@Path("/department")
@JWTTokenNeeded
public class DepartmentDetails {

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDepartmentDetails(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        departmentBean obj = new departmentBean();
        DepartmentDAO det = new DepartmentDAO();

        ArrayList<departmentBean> departmentList = det.getOrgDepartment();
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(departmentList);
        return Response.ok(new String(jsonInString.getBytes(), "UTF-8"), MediaType.APPLICATION_JSON).build();

    }
    
}

