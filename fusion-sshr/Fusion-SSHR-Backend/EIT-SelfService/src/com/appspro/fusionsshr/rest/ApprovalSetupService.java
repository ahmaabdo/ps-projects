/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.dao.ApprovalSetupDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.restHelper.RestHelper;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;


/**
 *
 * @author user
 */
@Path("/approvalSetup")
@JWTTokenNeeded
public class ApprovalSetupService extends RestHelper {

    ApprovalSetupDAO service = new ApprovalSetupDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateJob(String body, @PathParam("transactionType")
        String transactionType) {
 
        ApprovalSetupBean list = new ApprovalSetupBean();
       
        try {
            ObjectMapper mapper = new ObjectMapper();
            ApprovalSetupBean bean =
                mapper.readValue(body, ApprovalSetupBean.class);

            list = service.insertOrUpdateApprovalSetup(bean, transactionType);
    
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }

    @POST
    @Path("/vaildation/")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getJobCount(@FormParam("eid_code")
        String eid_code, @FormParam("role_name")
        String role_name, @FormParam("approval_type")
        String approval_type, @FormParam("notification_type")
        String notification_type,@FormParam("approval")
         String approval   ) {
        int count = -1;
        count =
                service.getApprovalForPositionValidation(eid_code, role_name, approval_type,
                                                         notification_type,approval);
        JSONObject obj = new JSONObject();
        obj.put("count", count);

        try {
            return new String(obj.toString().getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null,
                                                         ex);
        }

        return obj.toString();
        //generalGroup
    }

    @GET
    @Path("/{eitcode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllWorkNatureAllowance(@PathParam("eitcode")
        String eid_code) {
        ArrayList<ApprovalSetupBean> list = new ArrayList<ApprovalSetupBean>();
        list = service.getApprovalByEITCode(eid_code);
        return new JSONArray(list).toString();
    }

    @GET
    @Path("/maxorder/{eitcode}/{approvalCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMaxApprovalOrder(@PathParam("eitcode")
        String eit_Code,@PathParam("approvalCode") String approvalCode) {
        int list = 0;
        list = service.getMAxApprovalOrder(eit_Code,approvalCode);
        JSONObject json = new JSONObject();
        json.append("count", list);
        return Response.ok("{" + "\"count\":" + new Integer(list) + "}",
                           MediaType.APPLICATION_JSON).build();
        // return  Response.ok(, MediaType.APPLICATION_JSON).build();
        //return json;
    }

    @POST
    @Path("/delapproval/{ID}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public void getDeleteApproval(@PathParam("ID") String ID) {
        int ID_Eit = Integer.parseInt(ID);
        service.deleteApprovalEitCode(ID_Eit);
        //("Delete Success");
        //generalGroup
    }

    @GET
    @Path("/notificationtype/{eitcode}/{approvalCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNotificationType(@PathParam("eitcode")
        String eit_Code,@PathParam("approvalCode")
        String approvalCode) {
        String list = "";
        list = service.getLastNotificationType(eit_Code,approvalCode);
        return Response.ok("{" + "\"notificationType\":" + "\"" + list + "\"" +
                           "}", MediaType.APPLICATION_JSON).build();

    }

    @GET
    @Path("approvalType/{eitcode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getApprovalTypeByEIT_CODE(@PathParam("eitcode")
        String eid_code) {
        ArrayList<ApprovalSetupBean> list = new ArrayList<ApprovalSetupBean>();
        list = service.getApprovalTypeByEITCode(eid_code);
        return new JSONArray(list).toString();
    }
    
    @GET
    @Path("approvalType/{eitcode}/{approvalCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getApprovalTypeByEIT_CODEAndApprovalCode(@PathParam("eitcode")
        String eid_code ,@PathParam("approvalCode") String approvalCode) {
        ArrayList<ApprovalSetupBean> list = new ArrayList<ApprovalSetupBean>();
        list = service.getApprovalByEITCodeAndApprovalCode(eid_code , approvalCode);
        return new JSONArray(list).toString();
    }
    
    @GET
        @Path("/{eitcode}/{approvalCode}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public String getAllWorkNatureAllowance(@PathParam("eitcode")
            String eid_code,@PathParam("approvalCode")
            String approvalCode) {
            ArrayList<ApprovalSetupBean> list = new ArrayList<ApprovalSetupBean>();
            list = service.getApprovalByEITCodeAndConditionCode(eid_code,approvalCode);
            return new JSONArray(list).toString();
        }
    

    public static void main(String[] args) {
        ApprovalSetupService obj = new ApprovalSetupService();

        //(obj.getApprovalTypeByEIT_CODE("XXX_ABS_CREATE_DECREE"));
    }
}
