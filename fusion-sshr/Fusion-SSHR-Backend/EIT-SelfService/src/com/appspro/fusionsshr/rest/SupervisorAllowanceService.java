/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.SupervisorAllowanceBean;
import com.appspro.fusionsshr.dao.SupervisorAllowanceDAO;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import utilities.JWTTokenNeeded;

/**
 *
 * @author amro
 */
@Path("supervisorallowance")
@JWTTokenNeeded
public class SupervisorAllowanceService {

    SupervisorAllowanceDAO dao = new SupervisorAllowanceDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertSupervisorAllowance(SupervisorAllowanceBean bean,
                                              @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            bean = dao.insertSupervisorAllowance(bean, transactionType);

        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
        return Response.ok(bean, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<SupervisorAllowanceBean> getAllSupervisorAllowance(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<SupervisorAllowanceBean> list =
            new ArrayList<SupervisorAllowanceBean>();

        list = dao.getAllSupervisorAllowance();

        return list;

    }

    @GET
    @Path("/{jobName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<SupervisorAllowanceBean> getAllSupervisorAllowanceByJobName(@PathParam("jobName")
        String jobName, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<SupervisorAllowanceBean> list =
            new ArrayList<SupervisorAllowanceBean>();

        list = dao.getAllSupervisorAllowanceByJobName(jobName);

        return list;

    }

    @GET
    @Path("/search/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<SupervisorAllowanceBean> getAllSupervisorAllowanceByCode(@PathParam("code")
        String code, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<SupervisorAllowanceBean> list =
            new ArrayList<SupervisorAllowanceBean>();

        list = dao.getAllSupervisorAllowanceByCode(code);

        return list;

    }
}
