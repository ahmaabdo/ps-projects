package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.NotificationBean;
import com.appspro.fusionsshr.dao.MailApprovalDAO;
import com.appspro.fusionsshr.dao.WorkflowNotificationDAO;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;


@Path("/mailApproval")
@JWTTokenNeeded
public class MailApproval {
    @POST
    @Path("/searchnotification")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getSearchNotification(@FormParam("TRS_ID") String transactionId,@FormParam("Approve_Type") String approveType) {
//       NotificationBean notificationList = new NotificationBean();
        MailApprovalDAO det = new MailApprovalDAO();
       det.NotificationSearch(transactionId,approveType);
       // return notificationList;
//        String returnedData="";
//        if(notificationList !=null){
//          returnedData =  new  JSONObject(notificationList).toString() ;
//        }else{
//            returnedData="";  
//        }
        return "done";
        
    }
}
