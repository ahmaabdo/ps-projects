/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.SecurityAllowanceBean;
import com.appspro.fusionsshr.dao.SecurityAllowanceDAO;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import utilities.JWTTokenNeeded;

/**
 *
 * @author amro
 */
@Path("securityallowance")
@JWTTokenNeeded
public class SecurityAllowanceService {

    SecurityAllowanceDAO dao = new SecurityAllowanceDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertOrUpdateSecurityAllowance(SecurityAllowanceBean bean, @PathParam("transactionType") String transactionType, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        return Response.ok(bean, MediaType.APPLICATION_JSON).build();

    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<SecurityAllowanceBean> getAllSecurityAllowance(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<SecurityAllowanceBean> list = new ArrayList<SecurityAllowanceBean>();

        list = dao.getAllSecurityAllowance();

        return list;

    }

    @GET
    @Path("/{jobName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<SecurityAllowanceBean> getAllSecurityAllowanceeByJobName(@PathParam("jobName") String jobName, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<SecurityAllowanceBean> list = new ArrayList<SecurityAllowanceBean>();

        list = dao.getAllSecurityAllowanceByJobName(jobName);

        return list;

    }

    @GET
    @Path("/search/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<SecurityAllowanceBean> getAllSecurityAllowanceeByCode(@PathParam("code") String code, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        ArrayList<SecurityAllowanceBean> list = new ArrayList<SecurityAllowanceBean>();

        list = dao.getAllSecurityAllowanceByCode(code);

        return list;

    }

}
