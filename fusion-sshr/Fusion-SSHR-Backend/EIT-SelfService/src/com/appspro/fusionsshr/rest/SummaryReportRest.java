/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;
import com.appspro.fusionsshr.dao.SummaryReportDAO;
import javax.servlet.http.HttpSession;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;


/**
 *
 * @author lenovo
 */
@Path("/summary")
@JWTTokenNeeded
public class SummaryReportRest {

    SummaryReportDAO service = new SummaryReportDAO();

    @POST
    @Path("addSummary/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String summaryAddOrUpdate(String body,
                                       @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        SummaryReportBean list = new SummaryReportBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            SummaryReportBean bean = mapper.readValue(body, SummaryReportBean.class);
            list = service.addSummary(bean, transactionType);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }

    }

    @GET
    @Path("reportName/{reportName}")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getReportName(@PathParam("reportName")
        String reportName, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            return Response.status(Response.Status.OK).entity(service.getReportName(reportName)).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

    }

    @GET
    @Path("/Report")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllSummaryReport() {
        List<SummaryReportBean> list = new ArrayList<SummaryReportBean>();
        list = service.getAllSummary();
        return  new  JSONArray(list).toString();
    }

    @GET
    @Path("reportName/{reportName}/{reportNamebefor}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response checkReportNameForUpdate(@PathParam("reportName")
        String reportName, @PathParam("reportNamebefor")
        String reportNamebefor, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            return Response.status(Response.Status.OK).entity(service.checkReportName(reportName,
                                                                                      reportNamebefor)).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

    }

    @GET
    @Path("reportNamePram/{reportName}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getReportPram(@PathParam("reportName")
        String reportName) {

//        return service.getReportParamSummary(reportName);
        ArrayList<SummaryReportBean> reportList =
            new ArrayList<SummaryReportBean>();
        reportList = service.getReportParamSummary(reportName);
        return new JSONArray(reportList).toString();

    }

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchValidation(@FormParam("reportname")
        String reportname) {
        ArrayList<SummaryReportBean> reportList =
            new ArrayList<SummaryReportBean>();
        reportList = service.searchReports(reportname);
        return new JSONArray(reportList).toString();
    }
    @GET
    @Path("/ReportCashing")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllReport(@Context
        HttpServletRequest request) {
        HttpSession session = request.getSession();
        List<SummaryReportBean> list = new ArrayList<SummaryReportBean>();
        list = service.getAllSummary();
        session.setAttribute("Reports", list);
        return  new  JSONArray(list).toString();
    }
    
    
    @GET
    @Path("deleteReport/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteElementEntrySetupByEIT_CODE(@PathParam("id")
            String id) {
            String status = "";
             
       status=  service.getDeleteReport(id);
       return status;
    }
    
  
    
    
    

}
