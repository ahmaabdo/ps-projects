package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.EmployeeTimeAttandanceBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import com.appspro.fusionsshr.dao.ExportToExcelStaticSSDAO;
import com.appspro.fusionsshr.dao.ReAssignRequestDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.biPReports.BIReportModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.OutputStream;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.text.SimpleDateFormat;

import java.util.ArrayList;

import java.util.Collections;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import javax.ws.rs.core.StreamingOutput;

import org.apache.poi.ss.usermodel.Workbook;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

/**
 *
 * @author Shadi Mansi-PC
 */
@Path("reassignRequest")
public class ReAssignRequestService {

    ReAssignRequestDAO service = new ReAssignRequestDAO();

    @GET
    @Path("trackRequest/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchPositionsByPositionName(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        ArrayList<PeopleExtraInformationBean> listB =
            new ArrayList<PeopleExtraInformationBean>();

        try {
            listB = service.getPendingSelfServices();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(listB).toString();
    }

    @GET
    @Path("/delete/{id}/{eitName}")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteSelfServiceByID(@PathParam("id")
        String id, @PathParam("eitName")
        String eitName, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        String status = "";
        try {
            status = service.deleteSelfServiceRow(id, eitName);
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return status;

    }

    //******************Start et EIT From People Extra By Person Number**************

    @POST
    @Path("/getEit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertXXX_EIT(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        ArrayList<PeopleExtraInformationBean> list2 =
            new ArrayList<PeopleExtraInformationBean>();
        ArrayList<PeopleExtraInformationBean> list3 =
            new ArrayList<PeopleExtraInformationBean>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            PeopleExtraInformationBean bean =
                mapper.readValue(body, PeopleExtraInformationBean.class);
            if (bean.getCode().equals("XXX_EDU_EXPENSE_REQUEST")) {
                list = service.getEducationExpenseByPersonNumberOrStatus(bean);
            } else if (bean.getCode().equals("XXX_HANDOVER_REQUEST")) {
                list = service.getHandOverByPersonNumberOrStatus(bean);
            } else if (bean.getCode().equals("")) {
                list =
service.getPendingAndApprovedSelfServicesByPersonNumber(bean);
                list2 = service.getHandOverByPersonNumberOrStatus(bean);
                list3 =
                        service.getEducationExpenseByPersonNumberOrStatus(bean);
                service.getHandOverByPersonNumberOrStatus(bean); //handover
                list.addAll(list2);
                list.addAll(list3);
            } else {
                list =
service.getPendingAndApprovedSelfServicesByPersonNumber(bean);
            }
            return new JSONArray(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(list).toString();
    }
    //********************End Get EIT From People Extra By Person Number**************

    @POST
    @Path("/getPendingAndApprovedByPersonNumber")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getPendingAndApprovedByPersonNumber(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            PeopleExtraInformationBean bean =
                mapper.readValue(body, PeopleExtraInformationBean.class);
            if (bean.getCode().equals("XXX_EDU_EXPENSE_REQUEST")) {
                list = service.getEducationExpenseByPersonNumberOrStatus(bean);
            } else if (bean.getCode().equals("XXX_HANDOVER_REQUEST")) {
                list = service.getHandOverByPersonNumberOrStatus(bean);
            } else if (bean.getCode().equals("") || bean.getCode() != null) {
                list =service.getPendingAndApprovedSelfServicesByPersonNumber(bean);
            }
            return new JSONArray(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return new JSONArray(list).toString();
    }
    //----------------------------update status to become withdraw

    @GET
    @Path("updateWithdraw/{transactionId}/{eitName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateStatusWithdraw(@PathParam("transactionId")
        String transactionId, @PathParam("eitName")
        String eitName, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        int status = 0;
        String result = null;
        try {
            status = service.updateStatusByWithdraw(transactionId, eitName);
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        if (status == 1) {
            result = "Sucess";
        } else {
            result = "fail";
        }

        return result;
    }

    @GET
    @Path("approvaltype/{s_type}/{t_type}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getapprovaltype(@PathParam("s_type")
        String s_type, @PathParam("t_type")
        String t_type, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<ApprovalListBean> list = new ArrayList<ApprovalListBean>();

        list = service.getApprovalType(s_type, t_type);

        return new JSONArray(list).toString();
    }

    @POST
    @Path("/updateApprovalList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateApprovalList(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        String list = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            ApprovalListBean bean =
                mapper.readValue(body, ApprovalListBean.class);
            list = service.updateApprovalList(bean);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return list;
    }


    @GET
    @Path("/ExcelReassign")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getAllAttandanceByPersonNumberExcelSheet(@QueryParam("personNumber")
        String personNumber, @QueryParam("eitCode")
        String eitCode, @QueryParam("status")
        String status, @QueryParam("roles")
        String roles) throws FileNotFoundException, IOException {
        try{
        File tempFile = File.createTempFile("SelfService", "xls");
        OutputStream fileOut = new FileOutputStream(tempFile);
        PeopleExtraInformationBean bean = new PeopleExtraInformationBean();
            JSONArray arrayRole = new JSONArray(roles);
            ArrayList<String> rolesList = new ArrayList<String>();
            if (arrayRole != null) { 
               for (int i=0;i<arrayRole.length();i++){ 
                rolesList.add(arrayRole.get(i).toString());
               } 
            } 
            bean.setRoleId(rolesList);
        if ("null".equals(personNumber)) {
            bean.setPerson_number(null);
        } else {
            bean.setPerson_number(personNumber);
        }
        if ("null".equals(eitCode)) {
            bean.setCode(null);
        } else {
            bean.setCode(eitCode);
        }
        if ("null".equals(status)) {
            bean.setStatus(null);
        } else {
            bean.setStatus(status);
        }
        Map<String, ArrayList<PeopleExtraInformationBean>> map =
            service.getPendingSelfServicesExcel(bean);
        Workbook general = service.generateGeneral(map, bean);
            ExportToExcelStaticSSDAO staticSS = new ExportToExcelStaticSSDAO();
         Workbook generalAll =  staticSS.getOtherStaticRequest(general,bean);
        generalAll.write(fileOut);
        fileOut.close();
        Response.ResponseBuilder rb = Response.ok(tempFile);
        rb.header("content-disposition",
                  "attachment; filename=Self Services Report.xls");
        //tempFile.delete();
        tempFile.deleteOnExit();
        return rb.build();
        }catch(Exception e){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
