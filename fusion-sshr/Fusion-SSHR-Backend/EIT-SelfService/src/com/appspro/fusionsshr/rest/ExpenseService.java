package com.appspro.fusionsshr.rest;
import com.appspro.fusionsshr.bean.ExpenseAttachmentDetails;
import com.appspro.fusionsshr.bean.ExpenseBean;
import com.appspro.fusionsshr.bean.ExpenseStudentBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;
import com.appspro.fusionsshr.dao.ExpenseDAO;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("/expense")
@JWTTokenNeeded
public class ExpenseService {
    ExpenseDAO service=new ExpenseDAO();
    
    @POST
    @Path("/addExpense")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addExpense(String body){
        ExpenseBean expensebeanobj = new ExpenseBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ExpenseBean bean=mapper.readValue(body,ExpenseBean.class);
            expensebeanobj=service.insertExpense(bean);
            return new JSONObject(expensebeanobj).toString();
            }catch(Exception e){
                e.printStackTrace();
            }
                return  new  JSONObject(expensebeanobj).toString();
            }
    @POST
    @Path("/editExpense/{masterId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String editExpense(String body,@PathParam("masterId") int masterId){
        ExpenseBean expensebeanobj = new ExpenseBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ExpenseBean bean=mapper.readValue(body,ExpenseBean.class);
            expensebeanobj=service.updateExpense(bean,masterId);
            return new JSONObject(expensebeanobj).toString();
            }catch(Exception e){
                e.printStackTrace();
            }
                return  new  JSONObject(expensebeanobj).toString();
            }
     @GET
     @Path("/getAllExpense/{personNumber}")
     @Consumes(MediaType.APPLICATION_JSON)
     @Produces(MediaType.APPLICATION_JSON)
     public String getAllExpenseByPersonNumber(@PathParam("personNumber")
        String personNumber){
            ArrayList<ExpenseBean> expenseList=new ArrayList<ExpenseBean>();
             expenseList=service.getMasterData(personNumber);
             return new JSONArray(expenseList).toString();
         }
    @GET
    @Path("/getAll/{requestId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllExpenseByRequestId(@PathParam("requestId")
       String requestId){
           ExpenseBean expenseList=new ExpenseBean();
            expenseList=service.getMasterDataById(requestId);
            return new JSONObject(expenseList).toString();
        }
     
    @GET
    @Path("/getAllDetails/{masterId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllDetailsByMasterId(@PathParam("masterId")
       int masterId){
           ArrayList<ExpenseStudentBean> detailsList=new ArrayList<ExpenseStudentBean>();
            detailsList=service.getDetailsData(masterId);
            return new JSONArray(detailsList).toString();
        }    
    @GET
    @Path("/getAllDetails/attachments/{detailId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllDetailsAttachmentByDetailId(@PathParam("detailId")
       int detailId){
           ArrayList<ExpenseAttachmentDetails> detailsList=new ArrayList<ExpenseAttachmentDetails>();
            detailsList=service.getDetailsAttachments(detailId);
            return new JSONArray(detailsList).toString();
        }
    
    @POST
    @Path("/approval")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String ApprovalClearanceRequest(String body) {
        SelfServiceBean list = new SelfServiceBean();
        String res="";
        try {
            ObjectMapper mapper = new ObjectMapper();
            SelfServiceBean bean =
                mapper.readValue(body, SelfServiceBean.class);
            res= service.approvalExpense(bean);
            return res;
        } catch (Exception e) {
            //
            e.printStackTrace();
            return res;
        }
       
    }
    @POST
    @Path("/UpdateStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdateStatus(String body) {
        ExpenseBean expenseBean = new  ExpenseBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ExpenseBean bean = mapper.readValue(body, ExpenseBean.class);
            expenseBean = service.UpdateStatus(bean);
            return  new  JSONObject(expenseBean).toString();
        } catch (Exception e) {
            return new  JSONObject(expenseBean).toString();
        }

    }
    @DELETE
    @Path("/removeDetail/{detailId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON) 
    public String removeDetailById(@PathParam("detailId") int detailId){
        try{
            String result=service.removeDetailByDetailId(detailId);
        }catch(Exception e){
            e.printStackTrace();
            return "fail to remove";
            }
        return "removed";
        
        }
    @GET
    @Path("getExpenseApprovalSetup")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCleartanceApprovalSetup() {
        
    String Approval= service.getExpenseApprovalSetup();
        return Approval;

    }
    @GET
    @Path("getTotalExpenseAmount/{personNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getMaxExpenseAmount(String body,@PathParam("personNumber") String personNumber) {
        ArrayList<String> result=new ArrayList<String>();
        try {
             result = service.getAllExpenseAmountPerEmp(personNumber);
            return  result.toString();
        } catch (Exception e) {
            return result.toString();
        }
    
}
    @GET
    @Path("checkStudentExistence/{personNumber}/{semister}/{studentName}")
    @Produces(MediaType.APPLICATION_JSON)
    public String checkStudentExistence(String body,@PathParam("personNumber") String personNumber,
                                        @PathParam("semister") String semister,
                                        @PathParam("studentName") String studentName) {
        boolean result=false;
        try {
             result = service.checkStudentRequestPerSemister(personNumber,semister,studentName);
            return  Boolean.toString(result);
        } catch (Exception e) {
            return Boolean.toString(result);
        }
    
    }
    @GET
    @Path("checkStudentExistence/Edit/{masterId}/{personNumber}/{semister}/{studentName}")
    @Produces(MediaType.APPLICATION_JSON)
    public String checkStudentExistence(String body,@PathParam("masterId") String masterId,@PathParam("personNumber") String personNumber,
                                        @PathParam("semister") String semister,
                                        @PathParam("studentName") String studentName) {
        boolean result=false;
        try {
             result = service.checkStudentRequestPerSemisterEdit(masterId,personNumber,semister,studentName);
            return  Boolean.toString(result);
        } catch (Exception e) {
            return Boolean.toString(result);
        }
    
    }
    @GET
    @Path("/requestNumber")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String getovertimerequestnumber(){
    String requestNumber="";
    requestNumber=service.getEducationSequence();
    return requestNumber;
    }
}
