package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalConditionBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.ManageSelfServiceBean;

import com.appspro.fusionsshr.dao.ApprovalConditionDAO;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.GET;
import javax.ws.rs.DELETE;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.appspro.fusionsshr.dao.ManageSelfServiceDAO;

import com.appspro.fusionsshr.bean.ManageSelfServiceBean;


import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;

import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("/ManageSelfSerivce")
@JWTTokenNeeded
public class ManageSelfServiceRest {
    
    ManageSelfServiceDAO serivces=new ManageSelfServiceDAO(); 

    
    @GET
    @Path("/getManageSelfService")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllManageSelfService() {
        
        List<ManageSelfServiceBean> list = new ArrayList<ManageSelfServiceBean>();
        list = serivces.getAllManageSelfService();
        return  new  JSONArray(list).toString();
    }  
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addManageSelfService(String body) {

        ManageSelfServiceBean list = new ManageSelfServiceBean();
        try {
          
                ObjectMapper mapper = new ObjectMapper();
                ManageSelfServiceBean bean = mapper.readValue(body, ManageSelfServiceBean.class);
                list = serivces.addManageSelfService(bean);
          
            
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            return new  JSONObject(list).toString();
        }
    }
    
    
    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateApprovalCondition(String body) {

        ManageSelfServiceBean list = new ManageSelfServiceBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ManageSelfServiceBean bean = mapper.readValue(body, ManageSelfServiceBean.class);
            list = serivces.updateManageSelfService(bean);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            return new  JSONObject(list).toString();
        }
    }
    
    @DELETE
      @Path("/{id}")
      public String deleteManageSelfServiceById(@PathParam("id") String id) {
              String status = "";
               
         serivces.deleteManageSelfService(id);
         return status;
      }
    
    @GET
       @Path("/{selfService}")
       @Consumes(MediaType.APPLICATION_JSON)
       @Produces(MediaType.APPLICATION_JSON)
       public String getManageBySelfService(@PathParam("selfService") String selfService) {
        
           ArrayList<ManageSelfServiceBean> list = new ArrayList<ManageSelfServiceBean>();
           list = serivces.getManageByESelfServ(selfService);
           return new JSONArray(list).toString();
       }
    
    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateJobManageSelfServ(String body,
                                                  @PathParam("transactionType") String transactionType) {
      
        ManageSelfServiceBean list = new ManageSelfServiceBean();

       
        try {
              ObjectMapper mapper = new ObjectMapper();
              ManageSelfServiceBean bean = mapper.readValue(body, ManageSelfServiceBean.class);
              
              ArrayList lst = serivces.getManageByESelfServ(bean.getSelfSerivce());
            
              if(transactionType.equalsIgnoreCase("ADD"))
              {
              
           
                  if(lst.size() == 0)
                  {
                     
                      list = serivces.insertOrUpdateManageSelfServ(bean,transactionType);
                  }
                  else {
                      return "already exists";
                  }
              }
              else {
                  list = serivces.insertOrUpdateManageSelfServ(bean,transactionType);

                   }
    
            return new JSONObject(list).toString();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            return new JSONObject(list).toString();
        }
    }
    
    
    
    
  
  
}
