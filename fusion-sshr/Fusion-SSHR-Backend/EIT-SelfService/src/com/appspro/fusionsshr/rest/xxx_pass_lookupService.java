/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.ValidationBean;
import com.appspro.fusionsshr.bean.xxx_pass_lookupBean;
import com.appspro.fusionsshr.dao.xxx_pass_lookupDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

/**
 *
 * @author lenovo
 */
@Path("/xxx_paas_lookup")
@JWTTokenNeeded
public class xxx_pass_lookupService {

    xxx_pass_lookupDAO service = new xxx_pass_lookupDAO();

    @GET
    @Path("/getLookup/{searchKey}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getXXX_PAAS_LOOKUP(@PathParam("searchKey")
        String searchKey, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        List<xxx_pass_lookupBean> list = new ArrayList<xxx_pass_lookupBean>();
        list = service.getXXX_pass_lookup(searchKey);
        return new JSONArray(list).toString();
    }

    @GET
    @Path("/getPassLookup")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPassLookup(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        //  return
        return new JSONArray(service.getAllPassLookup()).toString();
    }

    @POST
    @Path("/addOrEditPassLookup/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertXXX_EIT(String body, @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        xxx_pass_lookupBean list = new xxx_pass_lookupBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            xxx_pass_lookupBean bean =
                mapper.readValue(body, xxx_pass_lookupBean.class);
            list = service.insertOrUpdateXXX_PassLookup(bean, transactionType);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }

    @GET
    @Path("codeLookup/{code}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getCodeLookup(@PathParam("code")
        String code, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            return Response.ok(service.getCodeLookup(code),
                               MediaType.TEXT_PLAIN).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

    }

    @GET
    @Path("nameLookup/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getNameLookup(@PathParam("name")
        String name, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            return Response.ok(service.getnameLookup(name),
                               MediaType.TEXT_PLAIN).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

    }
    //    @GET
    //    @Path("codeLookup/{codeExit}/{codeChange}")
    //    @Produces(MediaType.TEXT_PLAIN)
    //    public Response getCodeLookupCheckUpdate(@PathParam("codeExit") String codeExit,@PathParam("codeChange") String codeChange,@Context HttpServletRequest request,@Context HttpServletResponse response,@HeaderParam("Authorization") String authString) {
    //      try{
    //                   return Response.ok(service.getCodeLookupCheckUpdate(codeExit,codeChange), MediaType.TEXT_PLAIN).build();
    //        } catch(Exception e) {
    //           return Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode(),e.getMessage()).build();
    //        }

    //    }

    @GET
    @Path("nameLookup/{name}/{code}/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getLookupCheckUpdate(@PathParam("name")
        String name, @PathParam("code")
        String code, @PathParam("id")
        String id, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            return Response.ok(service.getLookupCheckUpdate(name, code, id),
                               MediaType.TEXT_PLAIN).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

    }


    @GET
    @Path("deleteLookup/{id}")
    public String deleteLookupRecordById(@PathParam("id")
        String id) {
        String status = "";
        service.deleteLookupRecord(id);
        return status;
    }

}
