/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.PaaSDefultValueBean;
import com.appspro.fusionsshr.dao.PaaSDefultValueDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;


import org.json.JSONObject;

import utilities.JWTTokenNeeded;

/**
 *
 * @author Lenovo
 */
@Path("/paasdefultvalue")
public class PaaSDefultValueService {

    PaaSDefultValueDao service = new PaaSDefultValueDao();

    //  @Produces(MediaType.APPLICATION_JSON)

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllJobs(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<PaaSDefultValueBean> list =
            new ArrayList<PaaSDefultValueBean>();

        //  List list = new ArrayList();
        list = service.getAllPaaSDefultValue();
        //(new JSONArray(list));
        return new JSONArray(list).toString();
        //  return Response.status(200).entity( new JSONArray.toString()).build();
    }


    @POST
    @Path("addDefaultValue")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertDefaultValue(String body) {

        PaaSDefultValueBean list = new PaaSDefultValueBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            PaaSDefultValueBean bean =
                mapper.readValue(body, PaaSDefultValueBean.class);
            list = service.addPaasDefaultValue(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }


    @GET
    @Path("/getPaasDefaultValue/{selfService}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPaasDefaultValueFn(@PathParam("selfService")
        String selfService) {
        List<PaaSDefultValueBean> list = new ArrayList<PaaSDefultValueBean>();
        list = service.getPaaSDefultValueBySelfService(selfService);
        return new JSONArray(list).toString();
    }

    @POST
    @Path("updatePaasDefaultVal/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updatePaasDefaultValue(String body, @PathParam("id")
        String id) {

        PaaSDefultValueBean list = new PaaSDefultValueBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            PaaSDefultValueBean bean =
                mapper.readValue(body, PaaSDefultValueBean.class);
            list = service.updatePaasDefaultValue(bean, id);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            return new JSONObject(list).toString();
        }
    }

    @GET
    @Path("deletePaasDefaultValue/{id}")
    public String deletePaasDefaultValue(@PathParam("id")
        String id) {
        String status = "";
        service.deletePaasDefaultValue(id);
        return status;
    }
}
