/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.TrackServerRequestBean;
import com.appspro.fusionsshr.dao.EFFDetails;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringReader;

import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import oracle.javatools.parser.java.v2.internal.compiler.Obj;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;

import utils.system;


/**
 *
 * @author user
 */
@Path("/eff")
@JWTTokenNeeded
public class EFFRest {

    @POST
    @Path("/details")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEFFDetails(String bean) {
//        JSONObject jsonObj = new JSONObject(bean);
        JSONObject jsonObj = new JSONObject(bean);
        String Url = jsonObj.getString("url");
        System.out.println(jsonObj);
        //(bean);
        EFFDetails det = new EFFDetails();
        JSONArray EFFLinks = new JSONArray();
        try {

            EFFLinks = det.getEFFDetails(Url);
            EFFLinks.toList().toString();

        } catch (Exception e) {
            // return Response.status(500, e.getMessage()).build();
        }
        //return Response.ok(EFFLinks, MediaType.APPLICATION_JSON).build();
        System.out.println( EFFLinks.toString());
        return  EFFLinks.toString();
    }

    @POST
    @Path("/{transactionType}/{transactionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertEFF(String data, @PathParam("transactionType")
        String transactionType,@PathParam("transactionId")String transactionId, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonData = mapper.readTree(data);
                String jsonObj = jsonData.get("model").toString();
                String TRS_ID = jsonData.get("TRS_ID").asText();
                String EIT_CODE = jsonData.get("eitCode").asText();
                System.out.println(jsonObj);
                mapper = new ObjectMapper();
                jsonData = mapper.readTree(jsonObj);
                String Url = jsonData.get("url").asText();
                jsonObj = jsonData.toString();
                String string =
                    jsonObj.substring(jsonObj.indexOf(",\"url"), jsonObj.indexOf("VO\"") +
                                      3);
                jsonObj = jsonObj.replaceAll(string, "");
        
        //String Url=jsonObj.get("url").toString();
        //(jsonObj);
        //(Url);
        
        //jsonObj.remove("url");
        //url
        //
        //(jsonObj);
    //        JsonObject jsonObj =
    //            Json.createReader(new StringReader(bean)).readObject();
    //        //JSONObject jsonObj = new JSONObject(bean);
    //        String Url = jsonObj.getString("url");
    //        jsonObj.remove("url");
        
        
        
        
    //        //(jsonObj);
    //        //(bean);

        try {
            EFFDetails det = new EFFDetails();
            if (transactionType.equals("ADD")) {
                PeopleExtraInformationBean o = new PeopleExtraInformationBean() ;
                
               o.setPerson_number(transactionId);//sent as person NUmber
                o.setEit(jsonObj.toString());
                o.setCode(EIT_CODE);
                o.setUrl(Url);
                o.setId(Integer.parseInt(TRS_ID));
                det.postEFF(o);
            } else if (transactionType.equals("EDIT")) {
                det.editEFF(jsonObj.toString(), Url);
            }
        } catch (Exception e) {
            Response.ok(e.getMessage()).status(500);
        }
        
        return jsonObj.toString();
    }
    
    
    @POST
    @Path("/resend")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response resendData(String model) throws Exception {
        EFFDetails e= new EFFDetails();
        PeopleExtraInformationBean  bean= new PeopleExtraInformationBean();

       JSONObject  obj= new JSONObject(model);
        bean.setEit_name(obj.getString("eitName"));
        bean.setUrl(obj.getString("URL"));
        bean.setCode(obj.getString("eitCode"));
        bean.setEit(obj.getString("payload"));
        bean.setId(obj.getInt("requestId"));
        
        bean.setPEI_ATTRIBUTE1("UPDATE");
             e.postEFF(bean);   
        return Response.ok(new JSONArray(EFFDetails.getInstance().getLog()).toString()).build();
    }

    @POST
    @Path("/getApproval")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEFFApprovalDetails(String bean) {
        JSONObject jsonObj = new JSONObject(bean);
        String Url = jsonObj.getString("url");
        EFFDetails det = new EFFDetails();
        JSONArray EFFLinks = new JSONArray();
        try {

            EFFLinks = det.getEFFDetails(Url);

        } catch (Exception e) {
            //    return Response.status(500, e.getMessage()).build();
        }
        // return Response.ok(EFFLinks, MediaType.APPLICATION_JSON).build();
      //  return ;
        return  new  JSONArray(EFFLinks.toList()).toString();
    }
    
    @POST
    @Path("editRequestTracker/{requestId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateRequestDetails(String body, @PathParam("requestId")
        String requestId) {
        EFFDetails services = new EFFDetails();
        TrackServerRequestBean list = new TrackServerRequestBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            TrackServerRequestBean bean =
                mapper.readValue(body, TrackServerRequestBean.class);
            list = services.updateRequestDetails(bean, requestId);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject(list).toString();
        }
    }
        
        
  
        
        
        
}
