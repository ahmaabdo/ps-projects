/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.ClothesAllowanceBean;
import com.appspro.fusionsshr.dao.ClothesAllowanceDAO;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import utilities.JWTTokenNeeded;

/**
 *
 * @author amro
 */
@Path("clothesallowance")
@JWTTokenNeeded
public class ClothesAllowanceService {

    ClothesAllowanceDAO dao = new ClothesAllowanceDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertOrUpdateClothesAllowance(ClothesAllowanceBean bean,
                                                   @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            bean = dao.insertOrUpdateClothesAllowance(bean, transactionType);
        } catch (Exception e) {
            //(e);
            return Response.status(500).entity(e.getMessage()).build();
        }

        return Response.ok(bean, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<ClothesAllowanceBean> getAllClothesAllowance(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<ClothesAllowanceBean> list =
            new ArrayList<ClothesAllowanceBean>();

        list = dao.getAllClothesAllowance();

        return list;
    }

    @GET
    @Path("/{jobName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<ClothesAllowanceBean> getAllClothesAllowanceByJobName(@PathParam("jobName")
        String jobName, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<ClothesAllowanceBean> list =
            new ArrayList<ClothesAllowanceBean>();

        list = dao.getAllClothesAllowanceByJobName(jobName);

        return list;
    }

    @GET
    @Path("/search/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<ClothesAllowanceBean> getAllClothesAllowanceCode(@PathParam("code")
        String code, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<ClothesAllowanceBean> list =
            new ArrayList<ClothesAllowanceBean>();

        list = dao.getAllClothesAllowanceByCode(code);

        return list;
    }

}
