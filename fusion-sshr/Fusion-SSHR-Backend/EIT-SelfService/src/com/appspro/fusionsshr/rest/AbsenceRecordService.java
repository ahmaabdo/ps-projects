package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.AbsenceRecordBean;
import com.appspro.fusionsshr.dao.AbsenceRecordDAO;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.JWTTokenNeeded;


@Path("/AbsenceRecord")
@JWTTokenNeeded
public class AbsenceRecordService {
    AbsenceRecordDAO serivces = new AbsenceRecordDAO();

    @GET
    @Path("/getAllAbsence/{startDate}/{endDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllAbsenceRecord( @PathParam("startDate")
        String StartDate, @PathParam("endDate")
        String EndDate) {

        List<AbsenceRecordBean> list = new ArrayList<AbsenceRecordBean>();
        list = serivces.getAllAbsenceRecord(StartDate,EndDate);
        return new JSONArray(list).toString();
    }

    @GET
    @Path("/getAllAbsenceByManager/{managerNum}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllAbsenceByManager(@Context
        HttpServletRequest request, @PathParam("managerNum")
        String managerNum) {

        List<AbsenceRecordBean> list = new ArrayList<AbsenceRecordBean>();
        list = serivces.getAllAbsenceRecordByManager(managerNum);
        return new JSONArray(list).toString();
    }

    @GET
    @Path("/getAbsenceByEmployee/{personNum}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAbsenceRecordByEmployee(@Context
        HttpServletRequest request, @PathParam("personNum")
        String personNum) {

        List<AbsenceRecordBean> list = new ArrayList<AbsenceRecordBean>();
        list = serivces.getAbsenceRecordByEmployee(personNum);
        return new JSONArray(list).toString();
    }

    @POST
    @Path("/searchAbsenceRecord")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getsearchAbsenceRecord(String body) {

        List<AbsenceRecordBean> list = new ArrayList<AbsenceRecordBean>();

        try {
            ObjectMapper mapper = new ObjectMapper();
            AbsenceRecordBean bean =
                mapper.readValue(body, AbsenceRecordBean.class);

            list = serivces.SearchAbsenceRecord(bean);
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONArray(list).toString();
        }
        return new JSONArray(list).toString();

    }

    @POST
    @Path("/getPunchinOrPunchout/test")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPunchInorPunchOut(@Context
        HttpServletRequest request,
        String body) {

        List<AbsenceRecordBean> list = new ArrayList<AbsenceRecordBean>();

        try {
            ObjectMapper mapper = new ObjectMapper();
            AbsenceRecordBean bean =
                mapper.readValue(body, AbsenceRecordBean.class);

            list = serivces.getPunchInOrPunchOut(bean);
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONArray(list).toString();
        }
        return new JSONArray(list).toString();


    }

    @GET
    @Path("/getAllEmp/{manager}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllEmp(@Context
        HttpServletRequest request, @PathParam("manager")
        String managerNum) {

        List<AbsenceRecordBean> list = new ArrayList<AbsenceRecordBean>();
        list = serivces.getAllEmpByManag(managerNum);
        return new JSONArray(list).toString();
    }
    @GET
    @Path("/getAllPersonNumber")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPersonNumber(@Context
        HttpServletRequest request, @PathParam("manager")
        String managerNum) {

        List<AbsenceRecordBean> list = new ArrayList<AbsenceRecordBean>();
        list = serivces.getAllPersonNumber();
        return new JSONArray(list).toString();
    }

    @GET
    @Path("/getTotal/for/{person}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTotal(@Context
        HttpServletRequest request, @PathParam("person")
        String person) {

        List<AbsenceRecordBean> list = new ArrayList<AbsenceRecordBean>();
        list = serivces.getAllTotal(person);
        return new JSONArray(list).toString();
    }

}
