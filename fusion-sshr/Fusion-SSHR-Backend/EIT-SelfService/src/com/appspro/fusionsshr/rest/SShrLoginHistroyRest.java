package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.SShrLOginHistroyBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import com.appspro.fusionsshr.dao.SShrLoginHistroyDAO;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import utilities.JWTTokenNeeded;

@Path("/SShrLoginHistory")
public class SShrLoginHistroyRest {
    
    SShrLoginHistroyDAO  service=new SShrLoginHistroyDAO();
    
    
    @POST
    @Path("/addSShrLoginHistory")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addSShrLoginHistory(String body,@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        SShrLOginHistroyBean list = new SShrLOginHistroyBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            SShrLOginHistroyBean bean = mapper.readValue(body, SShrLOginHistroyBean.class);
            list = service.addLoginHistory(bean);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }

    }
    
    
    
    @POST
    @Path("/UpdateSShrLoginHistory")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdateSShrLoginHistory(String body,@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        SShrLOginHistroyBean list = new SShrLOginHistroyBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            SShrLOginHistroyBean bean = mapper.readValue(body, SShrLOginHistroyBean.class);
            list = service.UpdateLoginHistory(bean);
            return  new  JSONObject(list).toString();
        } catch (Exception e) {
            return new  JSONObject(list).toString();
        }

    }
    
    
    
}
