/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.JobBean;
import com.appspro.fusionsshr.dao.JobDAO;

import common.restHelper.RestHelper;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import utilities.JWTTokenNeeded;


/**
 *
 * @author amro
 */
@Path("/jobs")
@JWTTokenNeeded
public class JobService extends RestHelper {

    JobDAO service = new JobDAO();

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertOrUpdateJob(JobBean jobBean,
                                      @PathParam("transactionType")
        String transactionType, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            service.insertOrUpdateJob(jobBean, transactionType);
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

        return Response.ok(jobBean, MediaType.APPLICATION_JSON).build();

    }

    //    @POST
    //    @Consumes(MediaType.APPLICATION_JSON)
    //    @Produces(MediaType.APPLICATION_JSON)
    //    public JobBean insertJob(JobBean bean) {
    //        service.insertJob(bean);
    //        return bean;
    //    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateJob(JobBean bean, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        try {
            service.updateJob(bean);
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

        return Response.ok(bean, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteJob(JobBean bean, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        return Response.ok(null, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<JobBean> getAllJobs(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        //        String x = convertToGregorian();
        //        //(x);
        ArrayList<JobBean> list = new ArrayList<JobBean>();

        list = service.getAllJobs();

        return list;
    }

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<JobBean> searchJobs(@FormParam("jobName")
        String jobName, @FormParam("jobClassificationType")
        String jobClassificationType, @FormParam("generalGroup")
        String generalGroup, @FormParam("qualityGroup")
        String qualityGroup, @FormParam("seriesCategory")
        String seriesCategory, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<JobBean> jobsList = new ArrayList<JobBean>();

        jobsList =
                service.searchJobs(jobName, jobClassificationType, generalGroup,
                                   qualityGroup, seriesCategory);

        return jobsList;

    }

    @POST
    @Path("/jobobCount")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getJobCount(@FormParam("generalGroup")
        String generalGroup, @FormParam("groupType")
        String groupType, @FormParam("jobClassificationType")
        String jobClassificationType, @FormParam("seriesCategory")
        String seriesCategory, @FormParam("jobId")
        int jobId, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        int count = -1;
        try {
            count =
                    service.getJobCount(generalGroup, groupType, jobClassificationType,
                                        seriesCategory, jobId);
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

        return Response.ok("{" + "\"count\":" + new Integer(count) + "}",
                           MediaType.APPLICATION_JSON).build();
        //generalGroup
    }

    @GET
    @Path("/search/{jobName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<JobBean> searchJobsByJobName(@PathParam("jobName")
        String jobName, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        ArrayList<JobBean> jobsList = new ArrayList<JobBean>();

        jobsList = service.searchJobs(jobName);

        return jobsList;
    }

    @POST
    @Path("/jobFuture")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getJobFuture(@FormParam("code")
        int code, @FormParam("startDate")
        String startDate, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        try {
            int count = -1;
            count = service.getFutureCount(code, startDate);
            return Response.ok("{" + "\"count\":" + new Integer(count) + "}",
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }

        //generalGroup
    }

    //

    @GET
    @Path("/jobsNames")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<JobBean> getAllJobsNames(@Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        //        String x = convertToGregorian();
        //        //(x);
        ArrayList<JobBean> jobsNameist = new ArrayList<JobBean>();

        jobsNameist = service.getAllJobs();

        return jobsNameist;
    }

}
