/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

public class SelfServiceBean {

    private String id;
    private String type;
    private String typeTableName;
    private String transactionId;
    private String presonId;
    private String status;
    private String managerPersonId;
    private String created_by;
    private String creation_date;
    private String updated_by;
    private String updated_date;
    private String personNumber;
    private String personName;
    private String managerId;
    private String managerOfManager;
    private String managerName;
    private String managerOfMnagerName;
    
    
    
    
    
    
    

    public void setId(String id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTypeTableName(String typeTableName) {
        this.typeTableName = typeTableName;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setPresonId(String presonId) {
        this.presonId = presonId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setManagerPersonId(String managerPersonId) {
        this.managerPersonId = managerPersonId;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getTypeTableName() {
        return typeTableName;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getPresonId() {
        return presonId;
    }

    public String getStatus() {
        return status;
    }

    public String getManagerPersonId() {
        return managerPersonId;
    }

    public String getCreated_by() {
        return created_by;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerOfManager(String managerOfManager) {
        this.managerOfManager = managerOfManager;
    }

    public String getManagerOfManager() {
        return managerOfManager;
    }



    public void setManagerOfMnagerName(String managerOfMnagerName) {
        this.managerOfMnagerName = managerOfMnagerName;
    }

    public String getManagerOfMnagerName() {
        return managerOfMnagerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerName() {
        return managerName;
    }
}
