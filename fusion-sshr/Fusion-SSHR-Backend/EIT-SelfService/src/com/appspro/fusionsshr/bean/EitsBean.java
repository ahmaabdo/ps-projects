/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author user
 */
public class EitsBean {

    private int id;
    private String code;
    private String status;
    private String person_number;
    private String person_id;
    private String created_by;
    private String creation_date;
    private String updated_by;
    private String updated_date;
    private String line_manager;
    private String manage_of_manager;
    private String url;
    private String eit;
    private String eit_name;
    
    private String requestedBy;
    private String department;
    private String departmentCode;
    private String tripType;
    private String travelType;
    private String destinationTo;
    private String startDate;
    private String endDate;
    private String NoOfDays;
    private String referenceNumber;
    private String destinationFrom;
    private String totalAmount;
    private String tripReason;
    private String pendingWith;
    

    public void setEit_name(String eit_name) {
        this.eit_name = eit_name;
    }

    public String getEit_name() {
        return eit_name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPerson_number(String person_number) {
        this.person_number = person_number;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public void setLine_manager(String line_manager) {
        this.line_manager = line_manager;
    }

    public void setManage_of_manager(String manage_of_manager) {
        this.manage_of_manager = manage_of_manager;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setEit(String eit) {
        this.eit = eit;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public String getPerson_number() {
        return person_number;
    }

    public String getPerson_id() {
        return person_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public String getLine_manager() {
        return line_manager;
    }

    public String getManage_of_manager() {
        return manage_of_manager;
    }

    public String getUrl() {
        return url;
    }

    public String getEit() {
        return eit;
    }


    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTravelType(String travelType) {
        this.travelType = travelType;
    }

    public String getTravelType() {
        return travelType;
    }

    public void setDestinationTo(String destinationTo) {
        this.destinationTo = destinationTo;
    }

    public String getDestinationTo() {
        return destinationTo;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setNoOfDays(String NoOfDays) {
        this.NoOfDays = NoOfDays;
    }

    public String getNoOfDays() {
        return NoOfDays;
    }


    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setDestinationFrom(String destinationFrom) {
        this.destinationFrom = destinationFrom;
    }

    public String getDestinationFrom() {
        return destinationFrom;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }


    public void setTripReason(String tripReason) {
        this.tripReason = tripReason;
    }

    public String getTripReason() {
        return tripReason;
    }

    public void setPendingWith(String pendingWith) {
        this.pendingWith = pendingWith;
    }

    public String getPendingWith() {
        return pendingWith;
    }
}
