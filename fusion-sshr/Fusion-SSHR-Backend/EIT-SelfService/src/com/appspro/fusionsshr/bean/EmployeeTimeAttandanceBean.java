package com.appspro.fusionsshr.bean;

public class EmployeeTimeAttandanceBean {
   
   private int ID;
   private String punch_date;
   private String personNumber;
   private String personName;
   private String sr_number;
   private String todaytDate;
   private String deviceName;
   private String perviousDatePunchOut;
   private String perviousDateDate;
   private String perviousDatePunchIn;
   private String employeeNumber ;
   private String startDate;
   private String endDate;
   private String status;
   private String transactionType;
   private String measure;
   private String responseDescription;
   private String requestBody;
   private String firstPunchInTime;
   private String firstPunchOutTime;
   private String secondPunchOutTime;
   private String secondPunchInTime;
   private String creationDate;
   private String date ;
   
   


    public void setPunch_date(String punch_date) {
        this.punch_date = punch_date;
    }

    public String getPunch_date() {
        return punch_date;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setSr_number(String sr_number) {
        this.sr_number = sr_number;
    }

    public String getSr_number() {
        return sr_number;
    }

    public void setTodaytDate(String todaytDate) {
        this.todaytDate = todaytDate;
    }

    public String getTodaytDate() {
        return todaytDate;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setPerviousDatePunchOut(String perviousDatePunchOut) {
        this.perviousDatePunchOut = perviousDatePunchOut;
    }

    public String getPerviousDatePunchOut() {
        return perviousDatePunchOut;
    }

    public void setPerviousDateDate(String perviousDateDate) {
        this.perviousDateDate = perviousDateDate;
    }

    public String getPerviousDateDate() {
        return perviousDateDate;
    }

    public void setPerviousDatePunchIn(String perviousDatePunchIn) {
        this.perviousDatePunchIn = perviousDatePunchIn;
    }

    public String getPerviousDatePunchIn() {
        return perviousDatePunchIn;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getMeasure() {
        return measure;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setFirstPunchInTime(String firstPunchInTime) {
        this.firstPunchInTime = firstPunchInTime;
    }

    public String getFirstPunchInTime() {
        return firstPunchInTime;
    }

    public void setFirstPunchOutTime(String firstPunchOutTime) {
        this.firstPunchOutTime = firstPunchOutTime;
    }

    public String getFirstPunchOutTime() {
        return firstPunchOutTime;
    }

    public void setSecondPunchOutTime(String secondPunchOutTime) {
        this.secondPunchOutTime = secondPunchOutTime;
    }

    public String getSecondPunchOutTime() {
        return secondPunchOutTime;
    }

    public void setSecondPunchInTime(String secondPunchInTime) {
        this.secondPunchInTime = secondPunchInTime;
    }

    public String getSecondPunchInTime() {
        return secondPunchInTime;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }
}
