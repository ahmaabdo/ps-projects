package com.appspro.fusionsshr.bean;

public class HandoverTasksBean {
  
  private int taskId;
  private int headerId;
  private String taskName;
  private String status;
  private String documentType;

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setHeaderId(int headerId) {
        this.headerId = headerId;
    }

    public int getHeaderId() {
        return headerId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getTaskId() {
        return taskId;
    }
}
