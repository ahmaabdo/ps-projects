/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author user
 */
public class AbsenceBean {

    private String personNumber;
    private String employer;
    private String absenceType;
    private String startDate;
    private String endDate;
    private String absenceStatusCd;
    private String startDateDuration;

    @Override
    public String toString() {
        return "{" + "personNumber:" + personNumber + ", employer:" + employer + ", absenceType:" + absenceType + ", startDate:" + startDate + ", endDate:" + endDate + ", absenceStatusCd:" + absenceStatusCd + ", startDateDuration:" + startDateDuration + '}';
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getAbsenceType() {
        return absenceType;
    }

    public void setAbsenceType(String absenceType) {
        this.absenceType = absenceType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAbsenceStatusCd() {
        return absenceStatusCd;
    }

    public void setAbsenceStatusCd(String absenceStatusCd) {
        this.absenceStatusCd = absenceStatusCd;
    }

    public String getStartDateDuration() {
        return startDateDuration;
    }

    public void setStartDateDuration(String startDateDuration) {
        this.startDateDuration = startDateDuration;
    }

}
