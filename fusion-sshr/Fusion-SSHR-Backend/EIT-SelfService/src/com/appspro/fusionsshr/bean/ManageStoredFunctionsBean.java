package com.appspro.fusionsshr.bean;


import org.apache.commons.lang.builder.ToStringBuilder;

public class ManageStoredFunctionsBean {
        
    private int id;
    private String name;
    private String status;
    private String bodyContent;
    private String creationDate;
    private String updatedDate;
    private String createdBy;
    private String updatedBy;
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }
    

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBodyContent(String bodyContent) {
        this.bodyContent = bodyContent;
    }

    public String getBodyContent() {
        return bodyContent;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }
}
