package com.appspro.fusionsshr.bean;

import java.util.ArrayList;

public class ExpenseStudentBean {
private String detailID;
private String studentName;
    private String studentGrade;
    private String studentSchool;
    private String masterId;
    private String studentInvoiceNo;
    private String studentInvoiceDate;
    private String note;
    private String invoiceAmount;
    private String invoicePayment; 
    private String studentId;
    private String bankName;
    private String iban;
    private String studentAge;
    private String paymentMethod;
    
    private  ArrayList<ExpenseAttachmentDetails>  attachment = new ArrayList<ExpenseAttachmentDetails>();     
    public void setDetailID(String detailID) {
        this.detailID = detailID;
    }

    public String getDetailID() {
        return detailID;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentGrade(String studentGrade) {
        this.studentGrade = studentGrade;
    }

    public String getStudentGrade() {
        return studentGrade;
    }

    public void setStudentSchool(String studentSchool) {
        this.studentSchool = studentSchool;
    }

    public String getStudentSchool() {
        return studentSchool;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setStudentInvoiceNo(String studentInvoiceNo) {
        this.studentInvoiceNo = studentInvoiceNo;
    }

    public String getStudentInvoiceNo() {
        return studentInvoiceNo;
    }

    public void setStudentInvoiceDate(String studentInvoiceDate) {
        this.studentInvoiceDate = studentInvoiceDate;
    }

    public String getStudentInvoiceDate() {
        return studentInvoiceDate;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setAttachment(ArrayList<ExpenseAttachmentDetails> attachment) {
        this.attachment = attachment;
    }

    public ArrayList<ExpenseAttachmentDetails> getAttachment() {
        return attachment;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoicePayment(String invoicePayment) {
        this.invoicePayment = invoicePayment;
    }

    public String getInvoicePayment() {
        return invoicePayment;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getIban() {
        return iban;
    }

    public void setStudentAge(String studentAge) {
        this.studentAge = studentAge;
    }

    public String getStudentAge() {
        return studentAge;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }
}
