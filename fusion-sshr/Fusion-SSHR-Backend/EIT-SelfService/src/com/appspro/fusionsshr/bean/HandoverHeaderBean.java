package com.appspro.fusionsshr.bean;

import java.util.ArrayList;

public class HandoverHeaderBean {
   
   private int headerId;
   private String employeeId;
   private String employeeNumber;
   private String status;
   private String createBy;
   private String creationDate;
   private String updateBy;
   private String updatedDate;
   private String employeeName;
   private String employeeDepartment;
   private String employeePosition;
   private String employeeNationality;
   private String employeeManager;
   private String handoverTo1;
   private String handoverTo2;
   private String handoverTo3;
   private String reason;
   private String notes;
   private String handoverTo1Name;
   private String handoverTo2Name;
   private String handoverTo3Name;
   private String requestCode; 
   private String statusAr;
   private String statusEn;
   private ArrayList<HandoverTasksBean> tasksList;


    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeDepartment(String employeeDepartment) {
        this.employeeDepartment = employeeDepartment;
    }

    public String getEmployeeDepartment() {
        return employeeDepartment;
    }

    public void setEmployeePosition(String employeePosition) {
        this.employeePosition = employeePosition;
    }

    public String getEmployeePosition() {
        return employeePosition;
    }

    public void setEmployeeNationality(String employeeNationality) {
        this.employeeNationality = employeeNationality;
    }

    public String getEmployeeNationality() {
        return employeeNationality;
    }

    public void setEmployeeManager(String employeeManager) {
        this.employeeManager = employeeManager;
    }

    public String getEmployeeManager() {
        return employeeManager;
    }

    public void setHandoverTo1(String handoverTo1) {
        this.handoverTo1 = handoverTo1;
    }

    public String getHandoverTo1() {
        return handoverTo1;
    }

    public void setHandoverTo2(String handoverTo2) {
        this.handoverTo2 = handoverTo2;
    }

    public String getHandoverTo2() {
        return handoverTo2;
    }

    public void setHandoverTo3(String handoverTo3) {
        this.handoverTo3 = handoverTo3;
    }

    public String getHandoverTo3() {
        return handoverTo3;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public void setTasksList(ArrayList<HandoverTasksBean> tasksList) {
        this.tasksList = tasksList;
    }

    public ArrayList<HandoverTasksBean> getTasksList() {
        return tasksList;
    }

    public void setHeaderId(int headerId) {
        this.headerId = headerId;
    }

    public int getHeaderId() {
        return headerId;
    }

    public void setHandoverTo1Name(String handoverTo1Name) {
        this.handoverTo1Name = handoverTo1Name;
    }

    public String getHandoverTo1Name() {
        return handoverTo1Name;
    }

    public void setHandoverTo2Name(String handoverTo2Name) {
        this.handoverTo2Name = handoverTo2Name;
    }

    public String getHandoverTo2Name() {
        return handoverTo2Name;
    }

    public void setHandoverTo3Name(String handoverTo3Name) {
        this.handoverTo3Name = handoverTo3Name;
    }

    public String getHandoverTo3Name() {
        return handoverTo3Name;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public String getRequestCode() {
        return requestCode;
    }

    public void setStatusAr(String statusAr) {
        this.statusAr = statusAr;
    }

    public String getStatusAr() {
        return statusAr;
    }

    public void setStatusEn(String statusEn) {
        this.statusEn = statusEn;
    }

    public String getStatusEn() {
        return statusEn;
    }
}
