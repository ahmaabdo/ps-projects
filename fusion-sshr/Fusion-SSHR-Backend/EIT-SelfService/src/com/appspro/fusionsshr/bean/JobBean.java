/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author amro
 */
public class JobBean {

    private String jobId;
    private String jobName;
    private String startDate;
    private String endDate;
    private String jobClassificationType;
    private String generalGroup;
    private String groupType;
    private String seriesCategory;
    private String createdBy;
    private String creationDate;
    private String lastUpdatedBy;
    private String lastUpdatedDate;
    private Integer code;
    private String flag;
    private String startDateGregorian;

    public JobBean() {
    }

    public String getJobId() {
        return jobId;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getJobClassificationType() {
        return jobClassificationType;
    }

    public void setJobClassificationType(String jobClassificationType) {
        this.jobClassificationType = jobClassificationType;
    }

    public String getGeneralGroup() {
        return generalGroup;
    }

    public void setGeneralGroup(String generalGroup) {
        this.generalGroup = generalGroup;
    }

    public String getSeriesCategory() {
        return seriesCategory;
    }

    public void setSeriesCategory(String seriesCategory) {
        this.seriesCategory = seriesCategory;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStartDateGregorian() {
        return startDateGregorian;
    }

    public void setStartDateGregorian(String startDateGregorian) {
        this.startDateGregorian = startDateGregorian;
    }

    @Override
    public String toString() {
        return "JobBean{" + "jobId=" + jobId + ", jobName=" + jobName + ", startDate=" + startDate + ", endDate=" + endDate + ", jobClassificationType=" + jobClassificationType + ", generalGroup=" + generalGroup + ", groupType=" + groupType + ", seriesCategory=" + seriesCategory + ", createdBy=" + createdBy + ", creationDate=" + creationDate + ", lastUpdatedBy=" + lastUpdatedBy + ", lastUpdatedDate=" + lastUpdatedDate + ", code=" + code + '}';
    }

}
