/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class trackRequestBean {
    
    private String id;
    private String projectName;
    private String eitCode;
    private String eitName;
    private String errorCode;
    private String errorDescription;
    private String requestId;

    public trackRequestBean(String id, String projectName, String eitCode, String eitName, String errorCode, String errorDescription, String requestId) {
        this.id = id;
        this.projectName = projectName;
        this.eitCode = eitCode;
        this.eitName = eitName;
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
        this.requestId = requestId;
    }

    public trackRequestBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getEitName() {
        return eitName;
    }

    public void setEitName(String eitName) {
        this.eitName = eitName;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return "trackRequestBean{" + "id=" + id + ", projectName=" + projectName + ", eitCode=" + eitCode + ", eitName=" + eitName + ", errorCode=" + errorCode + ", errorDescription=" + errorDescription + ", requestId=" + requestId + '}';
    }
    
}
