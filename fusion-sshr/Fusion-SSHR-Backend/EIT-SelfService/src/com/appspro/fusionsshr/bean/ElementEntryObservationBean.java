package com.appspro.fusionsshr.bean;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ElementEntryObservationBean {

    private int id;
    private String ssId;
    private String ssType;
    private String ucmId;
    private String status;
    private String processId;
    private String creationDate;
    private String updatedDate;
    private String personNumber;
    private String fullName;
    private String HireDate;
    private String positionName;
    private String gradeName;
    private String assignmentStatusType;
    private String startDate;
    private String endDate;
    private String advancedLeave;
    private String fileName;
    private String fileContent;
    private String fileBytes;
    private String filePath;
    private String eitCode;
    private String parentUcmId;
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setSsId(String ssId) {
        this.ssId = ssId;
    }

    public String getSsId() {
        return ssId;
    }

    public void setSsType(String ssType) {
        this.ssType = ssType;
    }

    public String getSsType() {
        return ssType;
    }

    public void setUcmId(String ucmId) {
        this.ucmId = ucmId;
    }

    public String getUcmId() {
        return ucmId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setHireDate(String HireDate) {
        this.HireDate = HireDate;
    }

    public String getHireDate() {
        return HireDate;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setAssignmentStatusType(String assignmentStatusType) {
        this.assignmentStatusType = assignmentStatusType;
    }

    public String getAssignmentStatusType() {
        return assignmentStatusType;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setAdvancedLeave(String advancedLeave) {
        this.advancedLeave = advancedLeave;
    }

    public String getAdvancedLeave() {
        return advancedLeave;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileBytes(String fileBytes) {
        this.fileBytes = fileBytes;
    }

    public String getFileBytes() {
        return fileBytes;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setParentUcmId(String parentUcmId) {
        this.parentUcmId = parentUcmId;
    }

    public String getParentUcmId() {
        return parentUcmId;
    }
}
