package com.appspro.fusionsshr.bean;

import java.util.ArrayList;

public class ExpenseBean {
private String masterId;
private String requestDate;
private String displayName;
private String email;
private String position;
private String academicYear;
private String semister;
private String personNumber;
private String status;
private String requestNumber;
private String grade;
private String joiningDate;
private String department;
private String nationality;
private  ArrayList<ExpenseStudentBean>  studentDetails = new ArrayList<ExpenseStudentBean>();

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setSemister(String semister) {
        this.semister = semister;
    }

    public String getSemister() {
        return semister;
    }

    public void setStudentDetails(ArrayList<ExpenseStudentBean> studentDetails) {
        this.studentDetails = studentDetails;
    }

    public ArrayList<ExpenseStudentBean> getStudentDetails() {
        return studentDetails;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNationality() {
        return nationality;
    }
}
