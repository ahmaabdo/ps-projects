/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author CPBSLV
 */
public class ActualPosition {

    private String id;
    private String startDate;
    private String endDate;
    private String status;
    private String positionNumber;
    private String managing;
    private String positionName;
    private String createdBy;
    private String createdDate;
    private String updatedBy;
    private String updateDate;
    private String flag;
    private String familyJobCode;
    private String familyJob;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setFamilyJobCode(String familyJobCode) {
        this.familyJobCode = familyJobCode;
    }

    public void setFamilyJob(String familyJob) {
        this.familyJob = familyJob;
    }

    public String getFamilyJobCode() {
        return familyJobCode;
    }

    public String getFamilyJob() {
        return familyJob;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
    private String sequence;

    public ActualPosition() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        this.positionNumber = positionNumber;
    }

    public String getManaging() {
        return managing;
    }

    public void setManaging(String managing) {
        this.managing = managing;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "ActualPosition{" + "id=" + id + ", startDate=" + startDate + ", endDate=" + endDate + ", positionNumber=" + positionNumber + ", managing=" + managing + ", positionName=" + positionName + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", updatedBy=" + updatedBy + ", updateDate=" + updateDate + ", flag=" + flag + ", familyJobCode=" + familyJobCode + ", familyJob=" + familyJob + ", sequence=" + sequence + '}';
    }

}
