package com.appspro.fusionsshr.bean;

public class ProbationaryPeriodEvaluationBean {
 
 private String id;
    private String  commitmentanddisciplineq1 ;
    private String  commitmentanddisciplineq2 ;
    private String   commitmentanddisciplineq3;
    private String   commitmentanddisciplineq4;
    private String   commitmentanddisciplineq5;
    private String   totalcommitmentanddisciplineq;
    private String   appearanceandbehavior1;
    private String   appearanceandbehavior2;
    private String   appearanceandbehavior3 ;
    private String   appearanceandbehavior4 ;
    private String   appearanceandbehavior5;
    private String   totalappearanceandbehavior;
    private String   interactionwithothers1;
    private String   interactionwithothers2;
    private String   interactionwithothers3;
    private String   interactionwithothers4;
    private String   interactionwithothers5;
    private String  totalinteractionwithothers;
    private String  qualityofcommunication1;
    private String  qualityofcommunication2;
    private String  qualityofcommunication3;
    private String  qualityofcommunication4;
    private String  qualityofcommunication5;
    private String  totalqualityofcommunication;
    private String  employeename;
    private String  jobnumber;
    private String  jobname;
    private String  hiredate;
    private String  gradecandidate;
    private String location;
    private String letterName;
    private String finalEvaluationSummary;    
    private String status;
    private String finaldecision;
    private String department;
    private String url;
    
    
    
 

   
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setCommitmentanddisciplineq1(String commitmentanddisciplineq1) {
        this.commitmentanddisciplineq1 = commitmentanddisciplineq1;
    }

    public String getCommitmentanddisciplineq1() {
        return commitmentanddisciplineq1;
    }

    public void setCommitmentanddisciplineq2(String commitmentanddisciplineq2) {
        this.commitmentanddisciplineq2 = commitmentanddisciplineq2;
    }

    public String getCommitmentanddisciplineq2() {
        return commitmentanddisciplineq2;
    }

    public void setCommitmentanddisciplineq3(String commitmentanddisciplineq3) {
        this.commitmentanddisciplineq3 = commitmentanddisciplineq3;
    }

    public String getCommitmentanddisciplineq3() {
        return commitmentanddisciplineq3;
    }

    public void setCommitmentanddisciplineq4(String commitmentanddisciplineq4) {
        this.commitmentanddisciplineq4 = commitmentanddisciplineq4;
    }

    public String getCommitmentanddisciplineq4() {
        return commitmentanddisciplineq4;
    }

    public void setCommitmentanddisciplineq5(String commitmentanddisciplineq5) {
        this.commitmentanddisciplineq5 = commitmentanddisciplineq5;
    }

    public String getCommitmentanddisciplineq5() {
        return commitmentanddisciplineq5;
    }

    public void setTotalcommitmentanddisciplineq(String totalcommitmentanddisciplineq) {
        this.totalcommitmentanddisciplineq = totalcommitmentanddisciplineq;
    }

    public String getTotalcommitmentanddisciplineq() {
        return totalcommitmentanddisciplineq;
    }

    public void setAppearanceandbehavior1(String appearanceandbehavior1) {
        this.appearanceandbehavior1 = appearanceandbehavior1;
    }

    public String getAppearanceandbehavior1() {
        return appearanceandbehavior1;
    }

    public void setAppearanceandbehavior2(String appearanceandbehavior2) {
        this.appearanceandbehavior2 = appearanceandbehavior2;
    }

    public String getAppearanceandbehavior2() {
        return appearanceandbehavior2;
    }

    public void setAppearanceandbehavior3(String appearanceandbehavior3) {
        this.appearanceandbehavior3 = appearanceandbehavior3;
    }

    public String getAppearanceandbehavior3() {
        return appearanceandbehavior3;
    }

    public void setAppearanceandbehavior4(String appearanceandbehavior4) {
        this.appearanceandbehavior4 = appearanceandbehavior4;
    }

    public String getAppearanceandbehavior4() {
        return appearanceandbehavior4;
    }

    public void setAppearanceandbehavior5(String appearanceandbehavior5) {
        this.appearanceandbehavior5 = appearanceandbehavior5;
    }

    public String getAppearanceandbehavior5() {
        return appearanceandbehavior5;
    }

    public void setTotalappearanceandbehavior(String totalappearanceandbehavior) {
        this.totalappearanceandbehavior = totalappearanceandbehavior;
    }

    public String getTotalappearanceandbehavior() {
        return totalappearanceandbehavior;
    }

    public void setInteractionwithothers1(String interactionwithothers1) {
        this.interactionwithothers1 = interactionwithothers1;
    }

    public String getInteractionwithothers1() {
        return interactionwithothers1;
    }

    public void setInteractionwithothers2(String interactionwithothers2) {
        this.interactionwithothers2 = interactionwithothers2;
    }

    public String getInteractionwithothers2() {
        return interactionwithothers2;
    }

    public void setInteractionwithothers3(String interactionwithothers3) {
        this.interactionwithothers3 = interactionwithothers3;
    }

    public String getInteractionwithothers3() {
        return interactionwithothers3;
    }

    public void setInteractionwithothers4(String interactionwithothers4) {
        this.interactionwithothers4 = interactionwithothers4;
    }

    public String getInteractionwithothers4() {
        return interactionwithothers4;
    }

    public void setInteractionwithothers5(String interactionwithothers5) {
        this.interactionwithothers5 = interactionwithothers5;
    }

    public String getInteractionwithothers5() {
        return interactionwithothers5;
    }

    public void setTotalinteractionwithothers(String totalinteractionwithothers) {
        this.totalinteractionwithothers = totalinteractionwithothers;
    }

    public String getTotalinteractionwithothers() {
        return totalinteractionwithothers;
    }

    public void setQualityofcommunication1(String qualityofcommunication1) {
        this.qualityofcommunication1 = qualityofcommunication1;
    }

    public String getQualityofcommunication1() {
        return qualityofcommunication1;
    }

    public void setQualityofcommunication2(String qualityofcommunication2) {
        this.qualityofcommunication2 = qualityofcommunication2;
    }

    public String getQualityofcommunication2() {
        return qualityofcommunication2;
    }

    public void setQualityofcommunication3(String qualityofcommunication3) {
        this.qualityofcommunication3 = qualityofcommunication3;
    }

    public String getQualityofcommunication3() {
        return qualityofcommunication3;
    }

    public void setQualityofcommunication4(String qualityofcommunication4) {
        this.qualityofcommunication4 = qualityofcommunication4;
    }

    public String getQualityofcommunication4() {
        return qualityofcommunication4;
    }

    public void setQualityofcommunication5(String qualityofcommunication5) {
        this.qualityofcommunication5 = qualityofcommunication5;
    }

    public String getQualityofcommunication5() {
        return qualityofcommunication5;
    }

    public void setTotalqualityofcommunication(String totalqualityofcommunication) {
        this.totalqualityofcommunication = totalqualityofcommunication;
    }

    public String getTotalqualityofcommunication() {
        return totalqualityofcommunication;
    }

    public void setEmployeename(String employeename) {
        this.employeename = employeename;
    }

    public String getEmployeename() {
        return employeename;
    }

    public void setJobnumber(String jobnumber) {
        this.jobnumber = jobnumber;
    }

    public String getJobnumber() {
        return jobnumber;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public String getJobname() {
        return jobname;
    }

    public void setHiredate(String hiredate) {
        this.hiredate = hiredate;
    }

    public String getHiredate() {
        return hiredate;
    }

    public void setGradecandidate(String gradecandidate) {
        this.gradecandidate = gradecandidate;
    }

    public String getGradecandidate() {
        return gradecandidate;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLetterName(String letterName) {
        this.letterName = letterName;
    }

    public String getLetterName() {
        return letterName;
    }

    public void setFinalEvaluationSummary(String finalEvaluationSummary) {
        this.finalEvaluationSummary = finalEvaluationSummary;
    }

    public String getFinalEvaluationSummary() {
        return finalEvaluationSummary;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setFinaldecision(String finaldecision) {
        this.finaldecision = finaldecision;
    }

    public String getFinaldecision() {
        return finaldecision;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
