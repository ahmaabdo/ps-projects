/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class IconBean {

    private String id;
    private String eitCode;
    private String icons;
    private String leftIcons;

    public IconBean(String id, String eitCode, String icons) {
        this.id = id;
        this.eitCode = eitCode;
        this.icons = icons;
        this.leftIcons = leftIcons;
    }


    public IconBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getIcons() {
        return icons;
    }

    public void setIcons(String icons) {
        this.icons = icons;
    }

    @Override
    public String toString() {
        return "IconBean{" + "id=" + id + ", eitCode=" + eitCode + ", icons=" + icons + '}';
    }

    public void setLeftIcons(String leftIcons) {
        this.leftIcons = leftIcons;
    }

    public String getLeftIcons() {
        return leftIcons;
    }
}
