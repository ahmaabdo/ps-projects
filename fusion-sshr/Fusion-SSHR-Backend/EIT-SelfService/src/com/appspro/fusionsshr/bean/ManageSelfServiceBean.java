package com.appspro.fusionsshr.bean;

public class ManageSelfServiceBean  {
  
  private String id;
  private String selfSerivce;
  private String descriptionAr;
  private String descriptionEn;
  private String selfServConfMessageAr;
  private String selfServConfMessageEn;
  private String rejectConfirmMessageVal;
  private String rejectConfirmMessageArVal;
  private String iconData;
  private String iconName;
  private String iconId;
  private String selfServDraftMessageAr;
  private String selfServDraftMessageEn;

  


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setSelfSerivce(String selfSerivce) {
        this.selfSerivce = selfSerivce;
    }

    public String getSelfSerivce() {
        return selfSerivce;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setSelfServConfMessageAr(String selfServConfMessageAr) {
        this.selfServConfMessageAr = selfServConfMessageAr;
    }

    public String getSelfServConfMessageAr() {
        return selfServConfMessageAr;
    }

    public void setSelfServConfMessageEn(String selfServConfMessageEn) {
        this.selfServConfMessageEn = selfServConfMessageEn;
    }

    public String getSelfServConfMessageEn() {
        return selfServConfMessageEn;
    }


    public void setRejectConfirmMessageVal(String rejectConfirmMessageVal) {
        this.rejectConfirmMessageVal = rejectConfirmMessageVal;
    }

    public String getRejectConfirmMessageVal() {
        return rejectConfirmMessageVal;
    }

    public void setRejectConfirmMessageArVal(String rejectConfirmMessageArVal) {
        this.rejectConfirmMessageArVal = rejectConfirmMessageArVal;
    }

    public String getRejectConfirmMessageArVal() {
        return rejectConfirmMessageArVal;
    }


    public void setIconData(String iconData) {
        this.iconData = iconData;
    }

    public String getIconData() {
        return iconData;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    public String getIconId() {
        return iconId;
    }

    public void setSelfServDraftMessageAr(String selfServDraftMessageAr) {
        this.selfServDraftMessageAr = selfServDraftMessageAr;
    }

    public String getSelfServDraftMessageAr() {
        return selfServDraftMessageAr;
    }

    public void setSelfServDraftMessageEn(String selfServDraftMessageEn) {
        this.selfServDraftMessageEn = selfServDraftMessageEn;
    }

    public String getSelfServDraftMessageEn() {
        return selfServDraftMessageEn;
    }
}
