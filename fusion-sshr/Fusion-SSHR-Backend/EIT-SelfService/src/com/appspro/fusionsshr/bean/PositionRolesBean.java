package com.appspro.fusionsshr.bean;


import com.appspro.db.AppsproConnection;

import common.biPReports.BIReportModel;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;


public class PositionRolesBean {
    private String positionName;
    private String employeeName;
    private String roleName;
    private String employeeRoleName;


    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setEmployeeRoleName(String employeeRoleName) {
        this.employeeRoleName = employeeRoleName;
    }

    public String getEmployeeRoleName() {
        return employeeRoleName;
    }

    public PositionRolesBean getPositionReport(String roleId) {
        PositionRolesBean obj = new PositionRolesBean();
        try {
            Map<String, String> paramMAp = new HashMap<String, String>();
            paramMAp.put("positionId", roleId);
            JSONObject respone =
                BIReportModel.runReport("EmailForPositionsReport", paramMAp);
            JSONObject dataDS = respone.getJSONObject("DATA_DS");
            JSONArray g1 = new JSONArray();
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject arrJson = dataDS.getJSONObject("G_1");

                g1.put(arrJson);
            } else {
                JSONArray arrJson = dataDS.getJSONArray("G_1");
                g1 = arrJson;
            }


            for (int i = 0; i < g1.length(); i++) {
                JSONObject data = (JSONObject)g1.get(i);
                obj = new PositionRolesBean();
                obj.setPositionName(data.getString("NAME"));
                obj.setEmployeeName(data.getString("DISPLAY_NAME"));
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return obj;
    }

    public PositionRolesBean getRolesReport(String roleId) {
        PositionRolesBean obj = new PositionRolesBean();
        try {
            Map<String, String> paramMAp = new HashMap<String, String>();
            paramMAp.put("P_ROLEID", roleId);
            JSONObject respone =
                BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                        paramMAp);

            JSONObject dataDS = respone.getJSONObject("DATA_DS");
            JSONArray g1 = new JSONArray();
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject arrJson = dataDS.getJSONObject("G_1");

                g1.put(arrJson);
            } else {
                JSONArray arrJson = dataDS.getJSONArray("G_1");
                g1 = arrJson;
            }
            for (int i = 0; i < g1.length(); i++) {
                JSONObject data = (JSONObject)g1.get(i);
                obj = new PositionRolesBean();
                obj.setEmployeeRoleName(data.getString("FULL_NAME"));
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        }
        return obj;
    }
}
