/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author amro
 */
public class PositionBean {

    private String id;
    private String startDate;
    private String endDate;
    private String taxonomicCode;
    private String ranked;
    private String positionNumber;
    private String managing;
    private Integer code;
    private String action;
    private String positionName;
    private String createdBy;
    private String createdDate;
    private String updatedBy;
    private String updateDate;
    private String flag;
    private String codeOfTaxonomicCode;
    private String startDateGregorian;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
    private String sequence;

    public PositionBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTaxonomicCode() {
        return taxonomicCode;
    }

    public void setTaxonomicCode(String taxonomicCode) {
        this.taxonomicCode = taxonomicCode;
    }

    public String getRanked() {
        return ranked;
    }

    public void setRanked(String ranked) {
        this.ranked = ranked;
    }

    public String getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        this.positionNumber = positionNumber;
    }

    public String getManaging() {
        return managing;
    }

    public void setManaging(String managing) {
        this.managing = managing;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCodeOfTaxonomicCode() {
        return codeOfTaxonomicCode;
    }

    public void setCodeOfTaxonomicCode(String codeOfTaxonomicCode) {
        this.codeOfTaxonomicCode = codeOfTaxonomicCode;
    }

    public String getStartDateGregorian() {
        return startDateGregorian;
    }

    public void setStartDateGregorian(String startDateGregorian) {
        this.startDateGregorian = startDateGregorian;
    }

    @Override
    public String toString() {
        return "PositionBean{" + "id=" + id + ", startDate=" + startDate + ", endDate=" + endDate + ", taxonomicCode=" + taxonomicCode + ", ranked=" + ranked + ", positionNumber=" + positionNumber + ", managing=" + managing + ", code=" + code + ", action=" + action + ", positionName=" + positionName + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", updatedBy=" + updatedBy + ", updateDate=" + updateDate + ", flag=" + flag + ", codeOfTaxonomicCode=" + codeOfTaxonomicCode + '}';
    }

}
