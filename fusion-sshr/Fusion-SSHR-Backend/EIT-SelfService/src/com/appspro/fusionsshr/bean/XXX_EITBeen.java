/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class XXX_EITBeen {

    private String id;
    private String code;
    private String eit_code;
    private String status;
    private String person_number;
    private String person_id;
    private String created_by;
    private String creation_date;
    private String updated_by;
    private String updated_date;
    private String line_manager;
    private String manage_of_manager;
    private String pei_information1;
    private String pei_information2;
    private String pei_information3;
    private String pei_information4;
    private String pei_information5;
    private String pei_information6;
    private String pei_information7;
    private String pei_information8;
    private String pei_information9;
    private String pei_information10;
    private String pei_information11;
    private String pei_information12;
    private String pei_information13;
    private String pei_information14;
    private String pei_information15;
    private String pei_information16;
    private String pei_information17;
    private String pei_information18;
    private String pei_information19;
    private String pei_information20;
    private String pei_information21;
    private String pei_information22;
    private String pei_information23;
    private String pei_information24;
    private String pei_information25;
    private String pei_information26;
    private String pei_information27;
    private String pei_information28;
    private String pei_information29;
    private String pei_information30;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEit_code() {
        return eit_code;
    }

    public void setEit_code(String eit_code) {
        this.eit_code = eit_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPerson_number() {
        return person_number;
    }

    public void setPerson_number(String person_number) {
        this.person_number = person_number;
    }

    public String getPerson_id() {
        return person_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public String getLine_manager() {
        return line_manager;
    }

    public void setLine_manager(String line_manager) {
        this.line_manager = line_manager;
    }

    public String getManage_of_manager() {
        return manage_of_manager;
    }

    public void setManage_of_manager(String manage_of_manager) {
        this.manage_of_manager = manage_of_manager;
    }

    public String getPei_information1() {
        return pei_information1;
    }

    public void setPei_information1(String pei_information1) {
        this.pei_information1 = pei_information1;
    }

    public String getPei_information2() {
        return pei_information2;
    }

    public void setPei_information2(String pei_information2) {
        this.pei_information2 = pei_information2;
    }

    public String getPei_information3() {
        return pei_information3;
    }

    public void setPei_information3(String pei_information3) {
        this.pei_information3 = pei_information3;
    }

    public String getPei_information4() {
        return pei_information4;
    }

    public void setPei_information4(String pei_information4) {
        this.pei_information4 = pei_information4;
    }

    public String getPei_information5() {
        return pei_information5;
    }

    public void setPei_information5(String pei_information5) {
        this.pei_information5 = pei_information5;
    }

    public String getPei_information6() {
        return pei_information6;
    }

    public void setPei_information6(String pei_information6) {
        this.pei_information6 = pei_information6;
    }

    public String getPei_information7() {
        return pei_information7;
    }

    public void setPei_information7(String pei_information7) {
        this.pei_information7 = pei_information7;
    }

    public String getPei_information8() {
        return pei_information8;
    }

    public void setPei_information8(String pei_information8) {
        this.pei_information8 = pei_information8;
    }

    public String getPei_information9() {
        return pei_information9;
    }

    public void setPei_information9(String pei_information9) {
        this.pei_information9 = pei_information9;
    }

    public String getPei_information10() {
        return pei_information10;
    }

    public void setPei_information10(String pei_information10) {
        this.pei_information10 = pei_information10;
    }

    public String getPei_information11() {
        return pei_information11;
    }

    public void setPei_information11(String pei_information11) {
        this.pei_information11 = pei_information11;
    }

    public String getPei_information12() {
        return pei_information12;
    }

    public void setPei_information12(String pei_information12) {
        this.pei_information12 = pei_information12;
    }

    public String getPei_information13() {
        return pei_information13;
    }

    public void setPei_information13(String pei_information13) {
        this.pei_information13 = pei_information13;
    }

    public String getPei_information14() {
        return pei_information14;
    }

    public void setPei_information14(String pei_information14) {
        this.pei_information14 = pei_information14;
    }

    public String getPei_information15() {
        return pei_information15;
    }

    public void setPei_information15(String pei_information15) {
        this.pei_information15 = pei_information15;
    }

    public String getPei_information16() {
        return pei_information16;
    }

    public void setPei_information16(String pei_information16) {
        this.pei_information16 = pei_information16;
    }

    public String getPei_information17() {
        return pei_information17;
    }

    public void setPei_information17(String pei_information17) {
        this.pei_information17 = pei_information17;
    }

    public String getPei_information18() {
        return pei_information18;
    }

    public void setPei_information18(String pei_information18) {
        this.pei_information18 = pei_information18;
    }

    public String getPei_information19() {
        return pei_information19;
    }

    public void setPei_information19(String pei_information19) {
        this.pei_information19 = pei_information19;
    }

    public String getPei_information20() {
        return pei_information20;
    }

    public void setPei_information20(String pei_information20) {
        this.pei_information20 = pei_information20;
    }

    public String getPei_information21() {
        return pei_information21;
    }

    public void setPei_information21(String pei_information21) {
        this.pei_information21 = pei_information21;
    }

    public String getPei_information22() {
        return pei_information22;
    }

    public void setPei_information22(String pei_information22) {
        this.pei_information22 = pei_information22;
    }

    public String getPei_information23() {
        return pei_information23;
    }

    public void setPei_information23(String pei_information23) {
        this.pei_information23 = pei_information23;
    }

    public String getPei_information24() {
        return pei_information24;
    }

    public void setPei_information24(String pei_information24) {
        this.pei_information24 = pei_information24;
    }

    public String getPei_information25() {
        return pei_information25;
    }

    public void setPei_information25(String pei_information25) {
        this.pei_information25 = pei_information25;
    }

    public String getPei_information26() {
        return pei_information26;
    }

    public void setPei_information26(String pei_information26) {
        this.pei_information26 = pei_information26;
    }

    public String getPei_information27() {
        return pei_information27;
    }

    public void setPei_information27(String pei_information27) {
        this.pei_information27 = pei_information27;
    }

    public String getPei_information28() {
        return pei_information28;
    }

    public void setPei_information28(String pei_information28) {
        this.pei_information28 = pei_information28;
    }

    public String getPei_information29() {
        return pei_information29;
    }

    public void setPei_information29(String pei_information29) {
        this.pei_information29 = pei_information29;
    }

    public String getPei_information30() {
        return pei_information30;
    }

    public void setPei_information30(String pei_information30) {
        this.pei_information30 = pei_information30;
    }

    @Override
    public String toString() {
        return "XXX_EITBeen{" + "id=" + id + ", code=" + code + ", eit_code=" + eit_code + ", status=" + status + ", person_number=" + person_number + ", person_id=" + person_id + ", created_by=" + created_by + ", creation_date=" + creation_date + ", updated_by=" + updated_by + ", updated_date=" + updated_date + ", line_manager=" + line_manager + ", manage_of_manager=" + manage_of_manager + ", pei_information1=" + pei_information1 + ", pei_information2=" + pei_information2 + ", pei_information3=" + pei_information3 + ", pei_information4=" + pei_information4 + ", pei_information5=" + pei_information5 + ", pei_information6=" + pei_information6 + ", pei_information7=" + pei_information7 + ", pei_information8=" + pei_information8 + ", pei_information9=" + pei_information9 + ", pei_information10=" + pei_information10 + ", pei_information11=" + pei_information11 + ", pei_information12=" + pei_information12 + ", pei_information13=" + pei_information13 + ", pei_information14=" + pei_information14 + ", pei_information15=" + pei_information15 + ", pei_information16=" + pei_information16 + ", pei_information17=" + pei_information17 + ", pei_information18=" + pei_information18 + ", pei_information19=" + pei_information19 + ", pei_information20=" + pei_information20 + ", pei_information21=" + pei_information21 + ", pei_information22=" + pei_information22 + ", pei_information23=" + pei_information23 + ", pei_information24=" + pei_information24 + ", pei_information25=" + pei_information25 + ", pei_information26=" + pei_information26 + ", pei_information27=" + pei_information27 + ", pei_information28=" + pei_information28 + ", pei_information29=" + pei_information29 + ", pei_information30=" + pei_information30 + '}';
    }

}
