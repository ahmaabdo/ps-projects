/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

//import javax.persistence.Lob;

/**
 *
 * @author CPBSLV
 */
public class AttachmentBean {

    int id;
    String SS_id;
    //    @Lob
    String attachment;
    String name;
    
    

    public int getId() {
        return id;
    }

    public String getSS_id() {
        return SS_id;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSS_id(String SS_id) {
        this.SS_id = SS_id;
    }

    @Override
    public String toString() {
        return "AttachmentBean{" + "id=" + id + ", SS_id=" + SS_id +
            ", attachment=" + attachment + '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
