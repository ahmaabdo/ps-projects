/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class sequenceDateBean {

    private String id;
    private String year;
    private String month;
    private String day;

    public sequenceDateBean(String id, String year, String month, String day) {
        this.id = id;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public sequenceDateBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "sequenceDateBean{" + "id=" + id + ", year=" + year + ", month=" + month + ", day=" + day + '}';
    }

}
