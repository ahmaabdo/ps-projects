package com.appspro.fusionsshr.bean;

public class AccessBean {
    private String accessId;
    private String roleName;
    private String accessList;
    private String accessDate;
    private String roleId;
    private String iconClass;
    private String screenName;

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getAccessId() {
        return accessId;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setAccessList(String accessList) {
        this.accessList = accessList;
    }

    public String getAccessList() {
        return accessList;
    }


    public void setAccessDate(String accessDate) {
        this.accessDate = accessDate;
    }

    public String getAccessDate() {
        return accessDate;
    }


    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setIconClass(String iconClass) {
        this.iconClass = iconClass;
    }

    public String getIconClass() {
        return iconClass;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getScreenName() {
        return screenName;
    }
}
