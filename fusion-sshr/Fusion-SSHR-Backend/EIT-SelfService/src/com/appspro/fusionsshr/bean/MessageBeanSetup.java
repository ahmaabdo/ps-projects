package com.appspro.fusionsshr.bean;

import java.io.Serializable;

public class MessageBeanSetup implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String eitCode;
    private String emailBody;
    private String notiMessage;
    private String haveAtt;
    private String sourceAtt;
    private String attachment;
    private String fileName;
    private String fileType;
    private String notiType;

    public MessageBeanSetup() {
    }
    public MessageBeanSetup(MessageBeanSetup bean) {
        
        this.id = bean.id;
        this.eitCode = bean.eitCode;
        this.emailBody = bean.emailBody;
        this.notiMessage = bean.notiMessage;
        this.haveAtt = bean.haveAtt;
        this.sourceAtt = bean.sourceAtt;
        this.attachment = bean.attachment;
        this.fileName = bean.fileName;
        this.fileType = bean.fileType;
        this.notiType = bean.notiType;

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setNotiMessage(String notiMessage) {
        this.notiMessage = notiMessage;
    }

    public String getNotiMessage() {
        return notiMessage;
    }

    public void setHaveAtt(String haveAtt) {
        this.haveAtt = haveAtt;
    }

    public String getHaveAtt() {
        return haveAtt;
    }

    public void setSourceAtt(String sourceAtt) {
        this.sourceAtt = sourceAtt;
    }

    public String getSourceAtt() {
        return sourceAtt;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setNotiType(String notiType) {
        this.notiType = notiType;
    }

    public String getNotiType() {
        return notiType;
    }
}
