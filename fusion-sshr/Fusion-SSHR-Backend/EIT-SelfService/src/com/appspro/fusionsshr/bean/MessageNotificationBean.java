package com.appspro.fusionsshr.bean;

public class MessageNotificationBean {
    private String id;
    private String eitCode;
    private String emailBody;
    private String notificationMessage;
    private String notificationType;
    private String haveAttach;
    private String attachSource;
    private String attachment;
    private String fileName;
    private String fileType;
    private String reportName;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setHaveAttach(String haveAttach) {
        this.haveAttach = haveAttach;
    }

    public String getHaveAttach() {
        return haveAttach;
    }

    public void setAttachSource(String attachSource) {
        this.attachSource = attachSource;
    }

    public String getAttachSource() {
        return attachSource;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportName() {
        return reportName;
    }
}
