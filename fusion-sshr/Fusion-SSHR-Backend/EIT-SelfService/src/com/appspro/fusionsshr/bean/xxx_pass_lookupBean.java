/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class xxx_pass_lookupBean {

    private String id;
    private String name;
    private String code;
    private String valueEnglish;
    private String valueArabic;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValueEnglish() {
        return valueEnglish;
    }

    public void setValueEnglish(String valueEnglish) {
        this.valueEnglish = valueEnglish;
    }

    public String getValueArabic() {
        return valueArabic;
    }

    public void setValueArabic(String valueArabic) {
        this.valueArabic = valueArabic;
    }

    @Override
    public String toString() {
        return "xxx_pass_lookupBean{" + "id=" + id + ", name=" + name + ", code=" + code + ", valueEnglish=" + valueEnglish + ", valueArabic=" + valueArabic + '}';
    }

}
