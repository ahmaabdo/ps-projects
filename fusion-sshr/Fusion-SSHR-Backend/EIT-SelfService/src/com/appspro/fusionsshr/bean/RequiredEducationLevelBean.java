/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author amro
 */
public class RequiredEducationLevelBean {

    private String id;
    private String startDate;
    private String endDate;
    private String flag;
    private String jobName;
    private String createdBy;
    private String creationDate;
    private String updatedBy;
    private String updatedDate;
    private String requiredEducationLevel;
    private String code;

    public RequiredEducationLevelBean() {
    }

    public String getId() {
        return id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getRequiredEducationLevel() {
        return requiredEducationLevel;
    }

    public void setRequiredEducationLevel(String requiredEducationLevel) {
        this.requiredEducationLevel = requiredEducationLevel;
    }

    @Override
    public String toString() {
        return "RequiredEducationLevelBean{" + "id=" + id + ", startDate=" + startDate + ", endDate=" + endDate + ", flag=" + flag + ", jobName=" + jobName + ", createdBy=" + createdBy + ", creationDate=" + creationDate + ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate + ", requiredEducationLevel=" + requiredEducationLevel + '}';
    }

}
