/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class ValidationBean {

    private String id;
    private String eitCode;
    private String firstKeyType;
    private String firstKeyValue;
    private String applicationValueFamily;
    private String applicationKey;
    private String saaSSource;
    private String reportName;
    private String parameter1Name;
    private String parameter2Name;
    private String parameter3Name;
    private String parameter4Name;
    private String parameter5Name;
    private String saaSQuery;
    private String paaSQuery;
    private String operation;
    private String secondKeyType;
    private String secondKeyValue;
    private String secondapplicationValueFamily;
    private String secondapplicationKey;
    private String secondsaaSSource;
    private String secondreportName;
    private String secondparameter1Name;
    private String secondparameter2Name;
    private String secondparameter3Name;
    private String secondparameter4Name;
    private String secondparameter5Name;
    private String secondsaaSQuery;
    private String secondpaaSQuery;
    private String typeOFAction;
    private String errorMassageEn;
    private String errorMassageAr;
    private String setValueOfEIT;
    private String value;
    private String saveResultToValidationsValues;
    private String validationValuesName;
    private String actiontype;
    private String eitSegment;
    private String newValue;
    private String typeOfValue;
    private String typeOfFristValue;
    private String fristValues;
    private String secondOperation;
    private String typeOfSecondValue;
    private String secondValue;

    public ValidationBean() {

    }

    public ValidationBean(String id, String eitCode, String firstKeyType, String firstKeyValue, String applicationValueFamily, String applicationKey, String saaSSource, String reportName, String parameter1Name, String parameter2Name, String parameter3Name, String parameter4Name, String parameter5Name, String saaSQuery, String paaSQuery, String operation, String secondKeyType, String secondKeyValue, String secondapplicationValueFamily, String secondapplicationKey, String secondsaaSSource, String secondreportName, String secondparameter1Name, String secondparameter2Name, String secondparameter3Name, String secondparameter4Name, String secondparameter5Name, String secondsaaSQuery, String secondpaaSQuery, String typeOFAction, String errorMassageEn, String errorMassageAr, String setValueOfEIT, String value, String saveResultToValidationsValues, String validationValuesName, String actiontype, String eitSegment, String newValue, String typeOfValue, String typeOfFristValue, String fristValues, String secondOperation, String typeOfSecondValue, String secondValue) {
        this.id = id;
        this.eitCode = eitCode;
        this.firstKeyType = firstKeyType;
        this.firstKeyValue = firstKeyValue;
        this.applicationValueFamily = applicationValueFamily;
        this.applicationKey = applicationKey;
        this.saaSSource = saaSSource;
        this.reportName = reportName;
        this.parameter1Name = parameter1Name;
        this.parameter2Name = parameter2Name;
        this.parameter3Name = parameter3Name;
        this.parameter4Name = parameter4Name;
        this.parameter5Name = parameter5Name;
        this.saaSQuery = saaSQuery;
        this.paaSQuery = paaSQuery;
        this.operation = operation;
        this.secondKeyType = secondKeyType;
        this.secondKeyValue = secondKeyValue;
        this.secondapplicationValueFamily = secondapplicationValueFamily;
        this.secondapplicationKey = secondapplicationKey;
        this.secondsaaSSource = secondsaaSSource;
        this.secondreportName = secondreportName;
        this.secondparameter1Name = secondparameter1Name;
        this.secondparameter2Name = secondparameter2Name;
        this.secondparameter3Name = secondparameter3Name;
        this.secondparameter4Name = secondparameter4Name;
        this.secondparameter5Name = secondparameter5Name;
        this.secondsaaSQuery = secondsaaSQuery;
        this.secondpaaSQuery = secondpaaSQuery;
        this.typeOFAction = typeOFAction;
        this.errorMassageEn = errorMassageEn;
        this.errorMassageAr = errorMassageAr;
        this.setValueOfEIT = setValueOfEIT;
        this.value = value;
        this.saveResultToValidationsValues = saveResultToValidationsValues;
        this.validationValuesName = validationValuesName;
        this.actiontype = actiontype;
        this.eitSegment = eitSegment;
        this.newValue = newValue;
        this.typeOfValue = typeOfValue;
        this.typeOfFristValue = typeOfFristValue;
        this.fristValues = fristValues;
        this.secondOperation = secondOperation;
        this.typeOfSecondValue = typeOfSecondValue;
        this.secondValue = secondValue;
    }

    public String getTypeOfValue() {
        return typeOfValue;
    }

    public void setTypeOfValue(String typeOfValue) {
        this.typeOfValue = typeOfValue;
    }

    public String getTypeOfFristValue() {
        return typeOfFristValue;
    }

    public void setTypeOfFristValue(String typeOfFristValue) {
        this.typeOfFristValue = typeOfFristValue;
    }

    public String getFristValues() {
        return fristValues;
    }

    public void setFristValues(String fristValues) {
        this.fristValues = fristValues;
    }

    public String getSecondOperation() {
        return secondOperation;
    }

    public void setSecondOperation(String secondOperation) {
        this.secondOperation = secondOperation;
    }

    public String getTypeOfSecondValue() {
        return typeOfSecondValue;
    }

    public void setTypeOfSecondValue(String typeOfSecondValue) {
        this.typeOfSecondValue = typeOfSecondValue;
    }

    public String getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(String secondValue) {
        this.secondValue = secondValue;
    }

    public String getActiontype() {
        return actiontype;
    }

    public void setActiontype(String actiontype) {
        this.actiontype = actiontype;
    }

    public String getEitSegment() {
        return eitSegment;
    }

    public void setEitSegment(String eitSegment) {
        this.eitSegment = eitSegment;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getFirstKeyType() {
        return firstKeyType;
    }

    public void setFirstKeyType(String firstKeyType) {
        this.firstKeyType = firstKeyType;
    }

    public String getFirstKeyValue() {
        return firstKeyValue;
    }

    public void setFirstKeyValue(String firstKeyValue) {
        this.firstKeyValue = firstKeyValue;
    }

    public String getApplicationValueFamily() {
        return applicationValueFamily;
    }

    public void setApplicationValueFamily(String applicationValueFamily) {
        this.applicationValueFamily = applicationValueFamily;
    }

    public String getApplicationKey() {
        return applicationKey;
    }

    public void setApplicationKey(String applicationKey) {
        this.applicationKey = applicationKey;
    }

    public String getSaaSSource() {
        return saaSSource;
    }

    public void setSaaSSource(String saaSSource) {
        this.saaSSource = saaSSource;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getParameter1Name() {
        return parameter1Name;
    }

    public void setParameter1Name(String parameter1Name) {
        this.parameter1Name = parameter1Name;
    }

    public String getParameter2Name() {
        return parameter2Name;
    }

    public void setParameter2Name(String parameter2Name) {
        this.parameter2Name = parameter2Name;
    }

    public String getParameter3Name() {
        return parameter3Name;
    }

    public void setParameter3Name(String parameter3Name) {
        this.parameter3Name = parameter3Name;
    }

    public String getParameter4Name() {
        return parameter4Name;
    }

    public void setParameter4Name(String parameter4Name) {
        this.parameter4Name = parameter4Name;
    }

    public String getParameter5Name() {
        return parameter5Name;
    }

    public void setParameter5Name(String parameter5Name) {
        this.parameter5Name = parameter5Name;
    }

    public String getSaaSQuery() {
        return saaSQuery;
    }

    public void setSaaSQuery(String saaSQuery) {
        this.saaSQuery = saaSQuery;
    }

    public String getPaaSQuery() {
        return paaSQuery;
    }

    public void setPaaSQuery(String paaSQuery) {
        this.paaSQuery = paaSQuery;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getSecondKeyType() {
        return secondKeyType;
    }

    public void setSecondKeyType(String secondKeyType) {
        this.secondKeyType = secondKeyType;
    }

    public String getSecondKeyValue() {
        return secondKeyValue;
    }

    public void setSecondKeyValue(String secondKeyValue) {
        this.secondKeyValue = secondKeyValue;
    }

    public String getSecondapplicationValueFamily() {
        return secondapplicationValueFamily;
    }

    public void setSecondapplicationValueFamily(String secondapplicationValueFamily) {
        this.secondapplicationValueFamily = secondapplicationValueFamily;
    }

    public String getSecondapplicationKey() {
        return secondapplicationKey;
    }

    public void setSecondapplicationKey(String secondapplicationKey) {
        this.secondapplicationKey = secondapplicationKey;
    }

    public String getSecondsaaSSource() {
        return secondsaaSSource;
    }

    public void setSecondsaaSSource(String secondsaaSSource) {
        this.secondsaaSSource = secondsaaSSource;
    }

    public String getSecondreportName() {
        return secondreportName;
    }

    public void setSecondreportName(String secondreportName) {
        this.secondreportName = secondreportName;
    }

    public String getSecondparameter1Name() {
        return secondparameter1Name;
    }

    public void setSecondparameter1Name(String secondparameter1Name) {
        this.secondparameter1Name = secondparameter1Name;
    }

    public String getSecondparameter2Name() {
        return secondparameter2Name;
    }

    public void setSecondparameter2Name(String secondparameter2Name) {
        this.secondparameter2Name = secondparameter2Name;
    }

    public String getSecondparameter3Name() {
        return secondparameter3Name;
    }

    public void setSecondparameter3Name(String secondparameter3Name) {
        this.secondparameter3Name = secondparameter3Name;
    }

    public String getSecondparameter4Name() {
        return secondparameter4Name;
    }

    public void setSecondparameter4Name(String secondparameter4Name) {
        this.secondparameter4Name = secondparameter4Name;
    }

    public String getSecondparameter5Name() {
        return secondparameter5Name;
    }

    public void setSecondparameter5Name(String secondparameter5Name) {
        this.secondparameter5Name = secondparameter5Name;
    }

    public String getSecondsaaSQuery() {
        return secondsaaSQuery;
    }

    public void setSecondsaaSQuery(String secondsaaSQuery) {
        this.secondsaaSQuery = secondsaaSQuery;
    }

    public String getSecondpaaSQuery() {
        return secondpaaSQuery;
    }

    public void setSecondpaaSQuery(String secondpaaSQuery) {
        this.secondpaaSQuery = secondpaaSQuery;
    }

    public String getTypeOFAction() {
        return typeOFAction;
    }

    public void setTypeOFAction(String typeOFAction) {
        this.typeOFAction = typeOFAction;
    }

    public String getErrorMassageEn() {
        return errorMassageEn;
    }

    public void setErrorMassageEn(String errorMassageEn) {
        this.errorMassageEn = errorMassageEn;
    }

    public String getErrorMassageAr() {
        return errorMassageAr;
    }

    public void setErrorMassageAr(String errorMassageAr) {
        this.errorMassageAr = errorMassageAr;
    }

    public String getSetValueOfEIT() {
        return setValueOfEIT;
    }

    public void setSetValueOfEIT(String setValueOfEIT) {
        this.setValueOfEIT = setValueOfEIT;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSaveResultToValidationsValues() {
        return saveResultToValidationsValues;
    }

    public void setSaveResultToValidationsValues(String saveResultToValidationsValues) {
        this.saveResultToValidationsValues = saveResultToValidationsValues;
    }

    public String getValidationValuesName() {
        return validationValuesName;
    }

    public void setValidationValuesName(String validationValuesName) {
        this.validationValuesName = validationValuesName;
    }

    @Override
    public String toString() {
        return "ValidationBean{" + "id=" + id + ", eitCode=" + eitCode + ", firstKeyType=" + firstKeyType + ", firstKeyValue=" + firstKeyValue + ", applicationValueFamily=" + applicationValueFamily + ", applicationKey=" + applicationKey + ", saaSSource=" + saaSSource + ", reportName=" + reportName + ", parameter1Name=" + parameter1Name + ", parameter2Name=" + parameter2Name + ", parameter3Name=" + parameter3Name + ", parameter4Name=" + parameter4Name + ", parameter5Name=" + parameter5Name + ", saaSQuery=" + saaSQuery + ", paaSQuery=" + paaSQuery + ", operation=" + operation + ", secondKeyType=" + secondKeyType + ", secondKeyValue=" + secondKeyValue + ", secondapplicationValueFamily=" + secondapplicationValueFamily + ", secondapplicationKey=" + secondapplicationKey + ", secondsaaSSource=" + secondsaaSSource + ", secondreportName=" + secondreportName + ", secondparameter1Name=" + secondparameter1Name + ", secondparameter2Name=" + secondparameter2Name + ", secondparameter3Name=" + secondparameter3Name + ", secondparameter4Name=" + secondparameter4Name + ", secondparameter5Name=" + secondparameter5Name + ", secondsaaSQuery=" + secondsaaSQuery + ", secondpaaSQuery=" + secondpaaSQuery + ", typeOFAction=" + typeOFAction + ", errorMassageEn=" + errorMassageEn + ", errorMassageAr=" + errorMassageAr + ", setValueOfEIT=" + setValueOfEIT + ", value=" + value + ", saveResultToValidationsValues=" + saveResultToValidationsValues + ", validationValuesName=" + validationValuesName + ", actiontype=" + actiontype + ", eitSegment=" + eitSegment + ", newValue=" + newValue + ", typeOfValue=" + typeOfValue + ", typeOfFristValue=" + typeOfFristValue + ", fristValues=" + fristValues + ", secondOperation=" + secondOperation + ", typeOfSecondValue=" + typeOfSecondValue + ", secondValue=" + secondValue + '}';
    }

}
