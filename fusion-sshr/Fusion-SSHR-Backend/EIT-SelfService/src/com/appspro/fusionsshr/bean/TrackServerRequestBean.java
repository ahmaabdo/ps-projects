package com.appspro.fusionsshr.bean;

import java.text.SimpleDateFormat;

import java.util.Date;

public class TrackServerRequestBean {
    private long id;
    private String projectName;
    private String eitCode;
    private String eitName;
    private String eitResponseCode;
    private String eitResponseDesc;
    private String eitResponseDate;
    private String elementResponseCode;
    private String elementResponseDesc;
    private String elementResponseDate;
    private String url;
    private String payload;
    private String personNumber;
    private String requestId;

    private static SimpleDateFormat format = null;


    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEitName(String eitName) {
        this.eitName = eitName;
    }

    public String getEitName() {
        return eitName;
    }

    public void setEitResponseCode(String eitResponseCode) {
        this.eitResponseCode = eitResponseCode;
    }

    public String getEitResponseCode() {
        return eitResponseCode;
    }

    public void setEitResponseDesc(String eitResponseDesc) {
        this.eitResponseDesc = eitResponseDesc;
    }

    public String getEitResponseDesc() {
        return eitResponseDesc;
    }

    public void setEitResponseDate(String eitResponseDate) {
        this.eitResponseDate = eitResponseDate;
    }

    public String getEitResponseDate() {
        return eitResponseDate;
    }

    public void setElementResponseCode(String elementResponseCode) {
        this.elementResponseCode = elementResponseCode;
    }

    public String getElementResponseCode() {
        return elementResponseCode;
    }

    public void setElementResponseDesc(String elementResponseDesc) {
        this.elementResponseDesc = elementResponseDesc;
    }

    public String getElementResponseDesc() {
        return elementResponseDesc;
    }

    public void setElementResponseDate(String elementResponseDate) {
        this.elementResponseDate = elementResponseDate;
    }

    public String getElementResponseDate() {
        return elementResponseDate;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public static String getCurrentDateTime() {
        if (format == null)
            format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return format.format(new Date());
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getPayload() {
        return payload;
    }

    public static void setFormat(SimpleDateFormat format) {
        TrackServerRequestBean.format = format;
    }

    public static SimpleDateFormat getFormat() {
        return format;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }
}
