package com.appspro.fusionsshr.bean;

import java.util.ArrayList;

public class TrackRequestRolesBean {
    private int id;
    private String roleId;
    private String roleName;
    private String roleDescription;
    private  ArrayList<String>  selfService = new ArrayList();
    private String eitCodeOperation;
    private int flag;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setSelfService(ArrayList<String> selfService) {
        this.selfService = selfService;
    }

    public  ArrayList<String> getSelfService() {
        return selfService;
    }

    public void setEitCodeOperation(String eitCodeOperation) {
        this.eitCodeOperation = eitCodeOperation;
    }

    public String getEitCodeOperation() {
        return eitCodeOperation;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }
}
