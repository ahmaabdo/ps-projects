package com.appspro.fusionsshr.bean;

public class SShrLOginHistroyBean {
 private String personNumber;
 private String loginDate;
 private String signOut;
 private String browserAndDeviceDetails;
 private String deviceIp;

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setLoginDate(String loginDate) {
        this.loginDate = loginDate;
    }

    public String getLoginDate() {
        return loginDate;
    }

    public void setBrowserAndDeviceDetails(String browserAndDeviceDetails) {
        this.browserAndDeviceDetails = browserAndDeviceDetails;
    }

    public String getBrowserAndDeviceDetails() {
        return browserAndDeviceDetails;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setSignOut(String signOut) {
        this.signOut = signOut;
    }

    public String getSignOut() {
        return signOut;
    }
}
