package com.appspro.fusionsshr.bean;

public class ApprovalConditionBean {
   
   private String id;
   private String eitCode;
   private String firstKeyType;
   private String firstKeyValue;
   private String firstEitSegment;
   private String operation;
   private String secondKeyType;
   private String secondKeyValue;
   private String secondEitSegment;
   private String approvalCode;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setFirstKeyType(String firstKeyType) {
        this.firstKeyType = firstKeyType;
    }

    public String getFirstKeyType() {
        return firstKeyType;
    }

    public void setFirstKeyValue(String firstKeyValue) {
        this.firstKeyValue = firstKeyValue;
    }

    public String getFirstKeyValue() {
        return firstKeyValue;
    }

    public void setFirstEitSegment(String firstEitSegment) {
        this.firstEitSegment = firstEitSegment;
    }

    public String getFirstEitSegment() {
        return firstEitSegment;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setSecondKeyType(String secondKeyType) {
        this.secondKeyType = secondKeyType;
    }

    public String getSecondKeyType() {
        return secondKeyType;
    }

    public void setSecondKeyValue(String secondKeyValue) {
        this.secondKeyValue = secondKeyValue;
    }

    public String getSecondKeyValue() {
        return secondKeyValue;
    }

    public void setSecondEitSegment(String secondEitSegment) {
        this.secondEitSegment = secondEitSegment;
    }

    public String getSecondEitSegment() {
        return secondEitSegment;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getApprovalCode() {
        return approvalCode;
    }
}
