package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.RoleSetUpBean;
import com.appspro.fusionsshr.bean.ValidationBean;

import com.appspro.fusionsshr.bean.roleSetupValidationBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.Statement;

import java.util.ArrayList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import org.python.parser.ast.Str;

public class roleSetupValidationDAO extends AppsproConnection{
   
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    Statement stmt = null;
    RestHelper rh = new RestHelper();
    private static roleSetupValidationDAO instance = null;
    public static roleSetupValidationDAO getInstance(){
        if(instance == null)
            instance = new roleSetupValidationDAO();
        return instance;
        }
   
    public roleSetupValidationBean insertOrUpdateRoleSetupValidation(roleSetupValidationBean validationBean,
                                                   String transactionType) {
        
        
        try {
            connection = AppsproConnection.getConnection();


            if (transactionType.equals("ADD")) {
                
                String query = "INSERT INTO  " + " " + getSchema_Name() + ".XXX_ROLESETUPVALIDATION(ROLENAME,ROLEID,PERSONINFROMATION,OPERATION,STATICVALUE)VALUES(?,?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, validationBean.getRolename());
                ps.setString(2, validationBean.getRoleid());
                ps.setString(3, validationBean.getPersonInfromation());
                ps.setString(4, validationBean.getOperation());
                ps.setString(5, validationBean.getStaticValue());
             
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {


                String query = "UPDATE " + " " + getSchema_Name() + ".  XXX_ROLESETUPVALIDATION SET ROLENAME=?,PERSONINFROMATION=?,OPERATION=?,STATICVALUE=? , ROLEID =? WHERE ID=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, validationBean.getRolename());
                ps.setString(2, validationBean.getPersonInfromation());
                ps.setString(3, validationBean.getOperation());
                ps.setString(4, validationBean.getStaticValue());
                ps.setString(5,validationBean.getRoleid());
                ps.setString(6, validationBean.getId());
                ps.executeUpdate();


            }


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return validationBean;
    }
   
   
   
   
   
   
       public ArrayList<roleSetupValidationBean> getRoleSetupValidation() {

        ArrayList<roleSetupValidationBean> roleSetList =
            new ArrayList<roleSetupValidationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM "+" "+getSchema_Name()+". XXX_ROLESETUPVALIDATION RSV ,"+getSchema_Name()+".XXX_ROLESETUPEIT RSE\n" + 
                "WHERE RSV.ROLEID = RSE.ROLESETUP_ID";
            AppsproConnection.LOG.info(query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                roleSetupValidationBean roleSetupValidation = new roleSetupValidationBean();
                roleSetupValidation.setId(rs.getString("ID"));
                roleSetupValidation.setRolename(rs.getString("ROLENAME"));
                roleSetupValidation.setRoleid(rs.getString("ROLEID"));
                roleSetupValidation.setPersonInfromation(rs.getString("PERSONINFROMATION"));
                roleSetupValidation.setOperation(rs.getString("OPERATION"));
                roleSetupValidation.setStaticValue(rs.getString("STATICVALUE"));
                roleSetupValidation.setEitCode(rs.getString("EITCODE"));

                roleSetList.add(roleSetupValidation);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return roleSetList;

    }
    public ArrayList<roleSetupValidationBean> getAllRoleSetupValidation() {

     ArrayList<roleSetupValidationBean> roleSetList =
         new ArrayList<roleSetupValidationBean>();
     try {
         connection = AppsproConnection.getConnection();
         String query =
             "SELECT * FROM "+" "+getSchema_Name()+".XXX_ROLESETUPVALIDATION";
         ps = connection.prepareStatement(query);
         rs = ps.executeQuery();
         while (rs.next()) {
             roleSetupValidationBean roleSetupValidation = new roleSetupValidationBean();
             roleSetupValidation.setId(rs.getString("ID"));
             roleSetupValidation.setRolename(rs.getString("ROLENAME"));
             roleSetupValidation.setRoleid(rs.getString("ROLEID"));
             roleSetupValidation.setPersonInfromation(rs.getString("PERSONINFROMATION"));
             roleSetupValidation.setOperation(rs.getString("OPERATION"));
             roleSetupValidation.setStaticValue(rs.getString("STATICVALUE"));
      

             roleSetList.add(roleSetupValidation);

         }

     } catch (Exception e) {
         //("Error: ");
        e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
     } finally {
         closeResources(connection, ps, rs);
     }
     return roleSetList;

    }   
       
    public ArrayList<RoleSetUpBean> getAllowValidationForEmployee(String role_Id) {

     ArrayList<RoleSetUpBean> roleSetList =
         new ArrayList<RoleSetUpBean>();
     try {
         connection = AppsproConnection.getConnection();
         String query =
             "SELECT * FROM "+" "+getSchema_Name()+".XXX_ROLESETUPEIT WHERE ROLESETUP_ID =?";
         ps = connection.prepareStatement(query);
         ps.setString(1, role_Id);
         rs = ps.executeQuery();
         while (rs.next()) {
             RoleSetUpBean roleSetupValidation = new RoleSetUpBean();
             roleSetupValidation.setEitCodes(rs.getString("EITCODE"));
             roleSetList.add(roleSetupValidation);

         }

     } catch (Exception e) {
         //("Error: ");
        e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
     } finally {
         closeResources(connection, ps, rs);
     }
     return roleSetList;

    }    
    
    
    
    public ArrayList<RoleSetUpBean> getEitCode(String role_Id) {
       
        HashMap EitCodeMap = new HashMap();
        ArrayList<RoleSetUpBean> roleSetList =
            new ArrayList<RoleSetUpBean>();
        
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(role_Id);
                JSONObject jsonObjInput;
               String rolesIdStr= "";
               int roleId;
                for(int i = 0; i < arr.length(); i++){       
                    rolesIdStr+=","+arr.getJSONObject(i).getString("roleId").toString();
                }
                 rolesIdStr.substring(1);
       


     try {
        
         connection = AppsproConnection.getConnection();
         stmt = connection.createStatement();
         String query =
             "SELECT DISTINCT  EITCODE  FROM "+" "+getSchema_Name()+".XXX_ROLESETUPEIT  WHERE ROLESETUP_ID IN ("+rolesIdStr.substring(1)+")";

            rs= stmt.executeQuery(query);
  
           while (rs.next()) {
             
              RoleSetUpBean roleSetupValidation = new RoleSetUpBean();
              roleSetupValidation.setEitCodes(rs.getString("EITCODE"));
              roleSetList.add(roleSetupValidation);

          }
       
         

     } catch (Exception e) {
         //("Error: ");
        e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
     } finally {
         closeResources(connection, ps, rs);
     }
     return  roleSetList;

    } 
    public String deleteRoleSetUpValidation(String id) {

          connection = AppsproConnection.getConnection();
          String query;
          int checkResult=0;

          try {
              query = "delete  from "+" "+getSchema_Name()+".XXX_ROLESETUPVALIDATION WHERE ID=?";
              ps = connection.prepareStatement(query);
              ps.setInt(1, Integer.parseInt(id));
              checkResult = ps.executeUpdate();
              
          } catch (Exception e) {
              //("Error: ");
             e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
          } finally {
              closeResources(connection, ps, rs);
          }
          return Integer.toString(checkResult);
      }
    
    public ArrayList<roleSetupValidationBean> searchRoleSetup(roleSetupValidationBean bean) {
        ArrayList<roleSetupValidationBean> roleList =
            new ArrayList<roleSetupValidationBean>();
        roleSetupValidationBean roleSetupValidation = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT * FROM  " + " " + getSchema_Name() + ".XXX_ROLESETUPVALIDATION reports WHERE LOWER(ROLENAME) LIKE LOWER(?)";

            ps = connection.prepareStatement(query);
            //            ps.setString(1, "%" + eitCode + "%");
            ps.setString(1, "%" + bean.getRolename() + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                roleSetupValidation = new roleSetupValidationBean();
                roleSetupValidation.setId(rs.getString("ID"));
                roleSetupValidation.setRolename(rs.getString("ROLENAME"));
                roleSetupValidation.setRoleid(rs.getString("ROLEID"));
                roleSetupValidation.setPersonInfromation(rs.getString("PERSONINFROMATION"));
                roleSetupValidation.setOperation(rs.getString("OPERATION"));
                roleSetupValidation.setStaticValue(rs.getString("STATICVALUE"));
                roleList.add(roleSetupValidation);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

        } finally {
            closeResources(connection, ps, rs);
        }

        return roleList;
    } 
}
