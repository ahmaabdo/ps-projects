package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.AbsenceRecordBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import org.json.JSONObject;

public class AbsenceRecordDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs,rs2;
    RestHelper rh = new RestHelper();


    public ArrayList<AbsenceRecordBean> getAllAbsenceRecord(String startDate,
                                                            String endDate) {

        ArrayList<AbsenceRecordBean> absenceRecordList =new ArrayList<AbsenceRecordBean>();
          
        try {
            connection = AppsproConnection.getConnection();
         
            String query ="SELECT DISTINCT (person_Number),person_name FROM " + " " + rh.getSchema_Name() + ".XXX_OTL_EMAIL_NOTIFICATION where PUNCH_DATE>=? AND PUNCH_DATE<=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            rs = ps.executeQuery();
            while (rs.next()) {
                
                AbsenceRecordBean absenceRecordBean = new AbsenceRecordBean(); 
                
                ArrayList<AbsenceRecordBean> detailsList =new ArrayList<AbsenceRecordBean>();
                absenceRecordBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                absenceRecordBean.setPersonName(rs.getString("PERSON_NAME"));
                String query2 =
                    "SELECT * FROM " + " " + rh.getSchema_Name() + ".XXX_OTL_EMAIL_NOTIFICATION WHERE  PERSON_NUMBER=? AND PUNCH_DATE>=? AND PUNCH_DATE<=?";

                ps = connection.prepareStatement(query2);
                ps.setString(1, rs.getString("PERSON_NUMBER"));
                ps.setString(2, startDate);
                ps.setString(3, endDate);
                rs2 = ps.executeQuery();
                while (rs2.next()) {
                    AbsenceRecordBean absenceRecordBean2 = new AbsenceRecordBean(); 
                    absenceRecordBean2.setId(rs2.getString("ID"));
                    absenceRecordBean2.setPersonNumber(rs2.getString("PERSON_NUMBER"));
                    absenceRecordBean2.setPersonName(rs2.getString("PERSON_NAME"));
                    absenceRecordBean2.setPunchDate(rs2.getString("PUNCH_DATE"));
                    absenceRecordBean2.setDeviceInTime(rs2.getString("DEVICEINTIME"));
                    absenceRecordBean2.setCheckedInTime(rs2.getString("CHECKEDINTIME"));
                    absenceRecordBean2.setDeviceOutTime(rs2.getString("DEVICEOUTTIME"));
                    absenceRecordBean2.setCheckedOutTime(rs2.getString("CHECKEDOUTTIME"));
                    absenceRecordBean2.setLateMinutes(rs2.getString("LATEMINUTES"));
                    absenceRecordBean2.setDay(rs2.getString("DAY"));
                    absenceRecordBean2.setOfficialWorkingHours(rs2.getString("OFFICIAL_WORKING_HOURS"));
                    absenceRecordBean2.setActualWorkingHours(rs2.getString("ACTUAL_WORKING_HOURS"));
                    absenceRecordBean2.setIsAbsence(rs2.getString("ISABSENCE"));

                    detailsList.add(absenceRecordBean2);
                    
                }
                absenceRecordBean.setPersonData(detailsList);
                absenceRecordList.add(absenceRecordBean);
                

                }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return absenceRecordList;

    }

    public ArrayList<AbsenceRecordBean> getAllAbsenceRecordByManager(String mangerNum) {

        ArrayList<AbsenceRecordBean> absenceRecordList =new ArrayList<AbsenceRecordBean>();
          
        try {
            connection = AppsproConnection.getConnection();
         
            String query ="SELECT DISTINCT (person_Number),person_name FROM " + " " + rh.getSchema_Name() + ".XXX_OTL_EMAIL_NOTIFICATION where MANAGER_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, mangerNum);
            rs = ps.executeQuery();
            while (rs.next()) {
                
                AbsenceRecordBean absenceRecordBean = new AbsenceRecordBean(); 
                
                ArrayList<AbsenceRecordBean> detailsList =new ArrayList<AbsenceRecordBean>();
                absenceRecordBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                absenceRecordBean.setPersonName(rs.getString("PERSON_NAME"));
                
                // get all data related to this person number
                String query2 ="SELECT * FROM " + " " + rh.getSchema_Name() + ".XXX_OTL_EMAIL_NOTIFICATION where PERSON_NUMBER=?";
                ps = connection.prepareStatement(query2);
                ps.setString(1,rs.getString("PERSON_NUMBER"));
                rs2 = ps.executeQuery();
                while (rs2.next()) {
                    AbsenceRecordBean absenceRecordBean2 = new AbsenceRecordBean(); 
                    absenceRecordBean2.setId(rs2.getString("ID"));
                    absenceRecordBean2.setPersonNumber(rs2.getString("PERSON_NUMBER"));
                    absenceRecordBean2.setPersonName(rs2.getString("PERSON_NAME"));
                    absenceRecordBean2.setPunchDate(rs2.getString("PUNCH_DATE"));
                    absenceRecordBean2.setDeviceInTime(rs2.getString("DEVICEINTIME"));
                    absenceRecordBean2.setCheckedInTime(rs2.getString("CHECKEDINTIME"));
                    absenceRecordBean2.setDeviceOutTime(rs2.getString("DEVICEOUTTIME"));
                    absenceRecordBean2.setCheckedOutTime(rs2.getString("CHECKEDOUTTIME"));
                    absenceRecordBean2.setLateMinutes(rs2.getString("LATEMINUTES"));
                    absenceRecordBean2.setDay(rs2.getString("DAY"));
                    absenceRecordBean2.setOfficialWorkingHours(rs2.getString("OFFICIAL_WORKING_HOURS"));
                    absenceRecordBean2.setActualWorkingHours(rs2.getString("ACTUAL_WORKING_HOURS"));
                    absenceRecordBean2.setIsAbsence(rs2.getString("ISABSENCE"));

                    detailsList.add(absenceRecordBean2);
                    
                }
                absenceRecordBean.setPersonData(detailsList);
                absenceRecordList.add(absenceRecordBean);
          

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return absenceRecordList;

    }

    public ArrayList<AbsenceRecordBean> getAbsenceRecordByEmployee(String personNum) {

        ArrayList<AbsenceRecordBean> absenceRecordList =new ArrayList<AbsenceRecordBean>();
          
        try {
            connection = AppsproConnection.getConnection();
         
            String query ="SELECT DISTINCT (person_Number),person_name FROM " + " " + rh.getSchema_Name() + ".XXX_OTL_EMAIL_NOTIFICATION where PERSON_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, personNum);
            rs = ps.executeQuery();
            while (rs.next()) {
                
                AbsenceRecordBean absenceRecordBean = new AbsenceRecordBean(); 
                
                ArrayList<AbsenceRecordBean> detailsList =new ArrayList<AbsenceRecordBean>();
                absenceRecordBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                absenceRecordBean.setPersonName(rs.getString("PERSON_NAME"));
                
                // get all data related to this person number
                String query2 ="SELECT * FROM " + " " + rh.getSchema_Name() + ".XXX_OTL_EMAIL_NOTIFICATION where PERSON_NUMBER=?";
                ps = connection.prepareStatement(query2);
                ps.setString(1,rs.getString("PERSON_NUMBER"));
                rs2 = ps.executeQuery();
                while (rs2.next()) {
                    AbsenceRecordBean absenceRecordBean2 = new AbsenceRecordBean(); 
                    absenceRecordBean2.setId(rs2.getString("ID"));
                    absenceRecordBean2.setPersonNumber(rs2.getString("PERSON_NUMBER"));
                    absenceRecordBean2.setPersonName(rs2.getString("PERSON_NAME"));
                    absenceRecordBean2.setPunchDate(rs2.getString("PUNCH_DATE"));
                    absenceRecordBean2.setDeviceInTime(rs2.getString("DEVICEINTIME"));
                    absenceRecordBean2.setCheckedInTime(rs2.getString("CHECKEDINTIME"));
                    absenceRecordBean2.setDeviceOutTime(rs2.getString("DEVICEOUTTIME"));
                    absenceRecordBean2.setCheckedOutTime(rs2.getString("CHECKEDOUTTIME"));
                    absenceRecordBean2.setLateMinutes(rs2.getString("LATEMINUTES"));
                    absenceRecordBean2.setDay(rs2.getString("DAY"));
                    absenceRecordBean2.setOfficialWorkingHours(rs2.getString("OFFICIAL_WORKING_HOURS"));
                    absenceRecordBean2.setActualWorkingHours(rs2.getString("ACTUAL_WORKING_HOURS"));
                    absenceRecordBean2.setIsAbsence(rs2.getString("ISABSENCE"));

                    detailsList.add(absenceRecordBean2);
                    
                }
                absenceRecordBean.setPersonData(detailsList);
                absenceRecordList.add(absenceRecordBean);
          

            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return absenceRecordList;

    }

    public ArrayList<AbsenceRecordBean> SearchAbsenceRecord(AbsenceRecordBean body) {

        ArrayList<AbsenceRecordBean> absenceRecordList =
            new ArrayList<AbsenceRecordBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT DISTINCT (ID),PERSON_NUMBER,PERSON_NAME,PUNCH_DATE,DEVICEINTIME,CHECKEDINTIME,DEVICEOUTTIME" +
                ",CHECKEDOUTTIME,LATEMINUTES,DAY,OFFICIAL_WORKING_HOURS,ACTUAL_WORKING_HOURS," +
                "ISABSENCE ,MANAGER_NUMBER FROM " + " " + rh.getSchema_Name() +
                " .XXX_OTL_EMAIL_NOTIFICATION WHERE ( PERSON_NUMBER =NVL(?, PERSON_NUMBER )) AND ( PUNCH_DATE>=NVL( ? ,PUNCH_DATE) AND PUNCH_DATE<=NVL( ?,PUNCH_DATE))\n" +
                "  AND ( ISABSENCE =NVL(?,ISABSENCE)) AND ( ACTUAL_WORKING_HOURS=NVL(? ,ACTUAL_WORKING_HOURS))  ";
            ps = connection.prepareStatement(query);
            ps.setString(1, body.getPersonNumber());
            ps.setString(2, body.getStartDate());
            ps.setString(3, body.getEndDate());
            ps.setString(4, body.getIsAbsence());
            ps.setString(5, body.getActualWorkingHours());
            rs = ps.executeQuery();
            while (rs.next()) {
                AbsenceRecordBean absenceRecordBean = new AbsenceRecordBean();
                absenceRecordBean.setId(rs.getString("ID"));
                absenceRecordBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                absenceRecordBean.setPersonName(rs.getString("PERSON_NAME"));
                absenceRecordBean.setPunchDate(rs.getString("PUNCH_DATE"));
                absenceRecordBean.setDeviceInTime(rs.getString("DEVICEINTIME"));
                absenceRecordBean.setCheckedInTime(rs.getString("CHECKEDINTIME"));
                absenceRecordBean.setDeviceOutTime(rs.getString("DEVICEOUTTIME"));
                absenceRecordBean.setCheckedOutTime(rs.getString("CHECKEDOUTTIME"));
                absenceRecordBean.setLateMinutes(rs.getString("LATEMINUTES"));
                absenceRecordBean.setDay(rs.getString("DAY"));
                absenceRecordBean.setOfficialWorkingHours(rs.getString("OFFICIAL_WORKING_HOURS"));
                absenceRecordBean.setActualWorkingHours(rs.getString("ACTUAL_WORKING_HOURS"));
                absenceRecordBean.setIsAbsence(rs.getString("ISABSENCE"));
                absenceRecordBean.setManagerNumber(rs.getString("MANAGER_NUMBER"));

                absenceRecordList.add(absenceRecordBean);
            }
            System.out.println(absenceRecordList);
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return absenceRecordList;
    }

    public ArrayList<AbsenceRecordBean> getPunchInOrPunchOut(AbsenceRecordBean body) {
        ArrayList<AbsenceRecordBean> absenceRecordList =
            new ArrayList<AbsenceRecordBean>();
        String con="DISTINCT (person_Number),person_name";
        String query = "";
        String query2="";
        try {

            connection = AppsproConnection.getConnection();
            if (body.getPunch().equalsIgnoreCase("No punch out")) {
                String q=" from xxx_otl_email_notification where (CHECKEDOUTTIME  is  null and checkedintime  is not null) and ( PERSON_NUMBER =NVL(?, PERSON_NUMBER )) AND ( PUNCH_DATE>=NVL( ?,PUNCH_DATE) AND PUNCH_DATE<=NVL( ?,PUNCH_DATE))\n" +
                        "AND ( ISABSENCE =NVL(?,ISABSENCE)) AND ( MANAGER_NUMBER =NVL(?, MANAGER_NUMBER ))";
                query ="select  "+con+q;
                query2 = "select  * "+q;
            } else if (body.getPunch().equalsIgnoreCase("No punch in")) {
                String q=" from xxx_otl_email_notification where (checkedintime  is  null and CHECKEDOUTTIME  is not null)  and ( PERSON_NUMBER =NVL(?, PERSON_NUMBER )) AND ( PUNCH_DATE>=NVL( ?,PUNCH_DATE) AND PUNCH_DATE<=NVL( ?,PUNCH_DATE))\n" +
                        "AND ( ISABSENCE =NVL(?,ISABSENCE)) AND ( MANAGER_NUMBER =NVL(?, MANAGER_NUMBER ))";
                query =
                        "select  "+con+q;
                query2 ="select * "+q;
            } else if (body.getPunch().equalsIgnoreCase("No punch out or in")) {
                String q=" from xxx_otl_email_notification where (checkedintime  is  null and CHECKEDOUTTIME  is  null) and ( PERSON_NUMBER =NVL(?, PERSON_NUMBER )) AND ( PUNCH_DATE>=NVL( ?,PUNCH_DATE) AND PUNCH_DATE<=NVL( ?,PUNCH_DATE))\n" +
                        "AND ( ISABSENCE =NVL(?,ISABSENCE))AND ( MANAGER_NUMBER =NVL(?, MANAGER_NUMBER ))";
                query ="select  "+con+q;
                query2="select * "+q;

            } else if (body.getPunch().equalsIgnoreCase("No")) {
                String q=" FROM " + " " + rh.getSchema_Name() +
                        " .XXX_OTL_EMAIL_NOTIFICATION WHERE ( PERSON_NUMBER =NVL(?, PERSON_NUMBER )) AND ( PUNCH_DATE>=NVL( ? ,PUNCH_DATE) AND PUNCH_DATE<=NVL( ?,PUNCH_DATE))\n" +
                        "  AND ( ISABSENCE =NVL(?,ISABSENCE))  AND ( MANAGER_NUMBER =NVL(?, MANAGER_NUMBER ))";
                query =" SELECT  "+con+q;
                query2 ="select * "+q;
            }

            ps = connection.prepareStatement(query);
            ps.setString(1, body.getPersonNumber());
            ps.setString(2, body.getStartDate());
            ps.setString(3, body.getEndDate());
            ps.setString(4, body.getIsAbsence());
            ps.setString(5, body.getManagerNumber());

            rs = ps.executeQuery();
            while (rs.next()) {
                AbsenceRecordBean absenceRecordBean = new AbsenceRecordBean();
                ArrayList<AbsenceRecordBean> detailsList =new ArrayList<AbsenceRecordBean>();
                absenceRecordBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                absenceRecordBean.setPersonName(rs.getString("PERSON_NAME"));
                // get all data related to this person number
                
                ps = connection.prepareStatement(query2);
                ps.setString(1, rs.getString("PERSON_NUMBER"));
                ps.setString(2, body.getStartDate());
                ps.setString(3, body.getEndDate());
                ps.setString(4, body.getIsAbsence());
                ps.setString(5, body.getManagerNumber());
                rs2 = ps.executeQuery();
                while (rs2.next()) {
                    AbsenceRecordBean absenceRecordBean2 =
                        new AbsenceRecordBean();
                    absenceRecordBean2.setId(rs2.getString("ID"));
                    absenceRecordBean2.setPersonNumber(rs2.getString("PERSON_NUMBER"));
                    absenceRecordBean2.setPersonName(rs2.getString("PERSON_NAME"));
                    absenceRecordBean2.setPunchDate(rs2.getString("PUNCH_DATE"));
                    absenceRecordBean2.setDeviceInTime(rs2.getString("DEVICEINTIME"));
                    absenceRecordBean2.setCheckedInTime(rs2.getString("CHECKEDINTIME"));
                    absenceRecordBean2.setDeviceOutTime(rs2.getString("DEVICEOUTTIME"));
                    absenceRecordBean2.setCheckedOutTime(rs2.getString("CHECKEDOUTTIME"));
                    absenceRecordBean2.setLateMinutes(rs2.getString("LATEMINUTES"));
                    absenceRecordBean2.setDay(rs2.getString("DAY"));
                    absenceRecordBean2.setOfficialWorkingHours(rs2.getString("OFFICIAL_WORKING_HOURS"));
                    absenceRecordBean2.setActualWorkingHours(rs2.getString("ACTUAL_WORKING_HOURS"));
                    absenceRecordBean2.setIsAbsence(rs2.getString("ISABSENCE"));

                    detailsList.add(absenceRecordBean2);

                }
                absenceRecordBean.setPersonData(detailsList);
                absenceRecordList.add(absenceRecordBean);


            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return absenceRecordList;
    }

    public ArrayList<AbsenceRecordBean> getAllEmpByManag(String mangerNum) {

        ArrayList<AbsenceRecordBean> absenceRecordList =
            new ArrayList<AbsenceRecordBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT  DISTINCT(PERSON_NUMBER),PERSON_NAME FROM " + " " + rh.getSchema_Name() +
                ".XXX_OTL_EMAIL_NOTIFICATION WHERE MANAGER_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, mangerNum);
            rs = ps.executeQuery();
            while (rs.next()) {
                AbsenceRecordBean absenceRecordBean = new AbsenceRecordBean();
                absenceRecordBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                absenceRecordBean.setPersonName(rs.getString("PERSON_NAME"));

                absenceRecordList.add(absenceRecordBean);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return absenceRecordList;

    }
        public ArrayList<AbsenceRecordBean> getAllPersonNumber() {
    
            ArrayList<AbsenceRecordBean> absenceRecordList =
                new ArrayList<AbsenceRecordBean>();
            try {
                connection = AppsproConnection.getConnection();
                String query =
                    "SELECT  DISTINCT(PERSON_NUMBER) ,PERSON_NAME FROM " + " " + rh.getSchema_Name() + ".XXX_OTL_EMAIL_NOTIFICATION ";
                ps = connection.prepareStatement(query);
                
                rs = ps.executeQuery();
                while (rs.next()) {
                    AbsenceRecordBean absenceRecordBean = new AbsenceRecordBean();
                    absenceRecordBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                    absenceRecordBean.setPersonName(rs.getString("PERSON_NAME"));

                    absenceRecordList.add(absenceRecordBean);
                }
    
            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            } finally {
                closeResources(connection, ps, rs);
            }
            return absenceRecordList;
    
        }

    public ArrayList<AbsenceRecordBean> getAllTotal(String person) {

        ArrayList<AbsenceRecordBean> absenceRecordList =
            new ArrayList<AbsenceRecordBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT  OFFICIAL_WORKING_HOURS ,LATEMINUTES from " +
                " " + rh.getSchema_Name() +
                ".XXX_OTL_EMAIL_NOTIFICATION where PERSON_NUMBER=? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, person);
            rs = ps.executeQuery();
            while (rs.next()) {
                AbsenceRecordBean absenceRecordBean = new AbsenceRecordBean();
                absenceRecordBean.setOfficialWorkingHours(rs.getString("OFFICIAL_WORKING_HOURS"));
                absenceRecordBean.setLateMinutes(rs.getString("LATEMINUTES"));

                absenceRecordList.add(absenceRecordBean);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return absenceRecordList;

    }


}
