/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ValidGradeBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;


/**
 *
 * @author amro
 */
public class ValidGradeDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ValidGradeBean insertOrUpdateValidGrade(ValidGradeBean bean,
                                                   String transactionType) {

        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }
            if (!bean.getEffectiveStartDate().isEmpty()) {
                bean.setEffectiveStartDate(rh.convertToGregorian(bean.getEffectiveStartDate()));
            }
            if (transactionType.equals("ADD")) {

                String query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_VALID_GRADE (EFFECTIVE_START_DATE,GRADE,START_DATE,END_DATE,JOB_CODE , CREATEDBY , CREATEDDATE, FLAG,POSITION )\n" +
                    "                    VALUES (?,?,?,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);
                ps.setDate(1,
                           getSQLDateFromString(bean.getEffectiveStartDate()));
                ps.setString(2, bean.getGrade());
                ps.setDate(3, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(4, getSQLDateFromString(bean.getEndDate()));
                ps.setString(5, bean.getJobCode());
                ps.setString(6, bean.getCreatedBy());
                ps.setDate(7, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(8, bean.getFlag());
                ps.setString(9, bean.getPositionName());

                ps.executeUpdate();
                ArrayList<ValidGradeBean> validGradeBeansList = getMaxID();
                UpdateValidGradeCode(validGradeBeansList.get(validGradeBeansList.size() -
                                                             1));

            } else if (transactionType.equals("EDIT")) {
                String oldFlag = "N";
                String query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_VALID_GRADE \n" +
                    "SET  UPDATEDDATE =sysdate \n" +
                    ",END_DATE = sysdate " + ", UPDATEDBY = ? " + ",FLAG = ?" +
                    "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getUpdatedBy());
                ps.setString(2, oldFlag);
                ps.setInt(3, Integer.parseInt(bean.getId()));
                ps.executeUpdate();

                query =
                        "INSERT INTO  " + " " + getSchema_Name() + ".XXX_VALID_GRADE (EFFECTIVE_START_DATE,GRADE,START_DATE,END_DATE,JOB_CODE , CREATEDBY , CREATEDDATE, FLAG,POSITION )\n" +
                        "                    VALUES (?,?,?,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);
                ps.setDate(1,
                           getSQLDateFromString(bean.getEffectiveStartDate()));
                ps.setString(2, bean.getGrade());
                ps.setDate(3, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(4, getSQLDateFromString(bean.getEndDate()));
                ps.setString(5, bean.getJobCode());
                ps.setString(6, bean.getCreatedBy());
                ps.setDate(7, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(8, bean.getFlag());
                ps.setString(9, bean.getPositionName());
                ps.executeUpdate();

            }

            ps.close();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ValidGradeBean updateValidGrade(ValidGradeBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "UPDATE  " + " " + getSchema_Name() + ".XXX_VALID_GRADE \n" +
                "SET EFFECTIVE_START_DATE =? \n" +
                ", EFFECTIVE_END_DATE =? \n" +
                ", GRADE = ? \n" +
                ", START_DATE = ? \n" +
                ", END_DATE= ? \n" +
                ", JOB_CODE = ? \n" +
                "WHERE id = ?   ";

            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getEffectiveStartDate());
            ps.setString(2, bean.getEffectiveEndDate());
            ps.setString(3, bean.getGrade());
            ps.setString(4, bean.getStartDate());
            ps.setString(5, bean.getEndDate());
            ps.setString(6, bean.getJobCode());
            ps.setString(7, bean.getId());
            ps.executeUpdate();

            rs.close();
            ps.close();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ValidGradeBean deleteValidGrade(ValidGradeBean bean) {
        try {
            //   connection = ConnectionPoolNew.getConnection();

            String query = "BEGIN\n" +
                "  UPDATE  " + " " + getSchema_Name() + ".ABOUT_ME\n" +
                "                    SET ABOUT_ME = ?,\n" +
                "                    UPDATED_BY = ?,\n" +
                "                    UPDATED_DATE = sysdate\n" +
                "                     WHERE PERSON_ID= ?;\n" +
                "                     COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(query);
            //            ps.setString(1, aboutMeBean.getAboutMe());
            //            ps.setString(2, aboutMeBean.getPersonId());
            //            ps.setString(3, aboutMeBean.getPersonId());

            rs = ps.executeQuery();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<ValidGradeBean> getAllValidGradeByJobId(String jobId) {
        ArrayList<ValidGradeBean> gradeBeanList =
            new ArrayList<ValidGradeBean>();

        try {
            connection = AppsproConnection.getConnection();
            //(jobId);
            String query =
                " select * from  " + " " + getSchema_Name() + ".XXX_VALID_GRADE where JOB_CODE = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, jobId);
            rs = ps.executeQuery();
            while (rs.next()) {
                ValidGradeBean gradeBean = new ValidGradeBean();
                gradeBean.setJobCode(rs.getString("JOB_CODE"));
                gradeBean.setGrade(rs.getString("GRADE"));
                gradeBean.setCreatedBy(rs.getString("CREATEDBY"));
                gradeBean.setCreationDate(rs.getString("CREATEDDATE"));

                gradeBean.setFlag(rs.getString("FLAG"));
                gradeBean.setId(rs.getString("ID"));
                gradeBean.setUpdatedBy(rs.getString("UPDATEDBY"));
                gradeBean.setUpdatedDate(rs.getString("UPDATEDDATE"));

                gradeBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                gradeBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                gradeBean.setCode(rs.getString("CODE"));
                gradeBean.setPositionName(rs.getString("POSITION"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    gradeBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    gradeBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    gradeBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    gradeBean.setStartDate((rs.getString("START_DATE")));
                } //end
                gradeBeanList.add(gradeBean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return gradeBeanList;
    }

    public ArrayList<ValidGradeBean> searchValidGrades() {
        return null;
    }

    public ArrayList<ValidGradeBean> getAllValidGrades() {
        ArrayList<ValidGradeBean> gradeBeanList =
            new ArrayList<ValidGradeBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select  * from  " + " " + getSchema_Name() + ".XXX_VALID_GRADE";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ValidGradeBean gradeBean = new ValidGradeBean();
                gradeBean.setJobCode(rs.getString("JOB_CODE"));
                gradeBean.setGrade(rs.getString("GRADE"));
                gradeBean.setCreatedBy(rs.getString("CREATEDBY"));
                gradeBean.setCreationDate(rs.getString("CREATEDDATE"));

                gradeBean.setFlag(rs.getString("FLAG"));
                gradeBean.setId(rs.getString("ID"));
                gradeBean.setUpdatedBy(rs.getString("UPDATEDBY"));
                gradeBean.setUpdatedDate(rs.getString("UPDATEDDATE"));

                gradeBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                gradeBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                gradeBean.setCode(rs.getString("CODE"));
                gradeBean.setPositionName(rs.getString("POSITION"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    gradeBean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    gradeBean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    gradeBean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    gradeBean.setStartDate((rs.getString("START_DATE")));
                } //end
                gradeBeanList.add(gradeBean);
            }

            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return gradeBeanList;
    }

    public ArrayList<ValidGradeBean> getMaxID() {
        ArrayList<ValidGradeBean> gradeBeanList =
            new ArrayList<ValidGradeBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "    select  * from  " + " " + getSchema_Name() + ".XXX_VALID_GRADE  \n" +
                "                WHERE ID = (SELECT MAX(ID) from  " + " " +
                getSchema_Name() + ".XXX_VALID_GRADE)";

            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                ValidGradeBean gradeBean = new ValidGradeBean();
                gradeBean.setJobCode(rs.getString("JOB_CODE"));
                gradeBean.setGrade(rs.getString("GRADE"));
                gradeBean.setCreatedBy(rs.getString("CREATEDBY"));
                gradeBean.setCreationDate(rs.getString("CREATEDDATE"));

                gradeBean.setEndDate(rs.getString("END_DATE"));
                gradeBean.setFlag(rs.getString("FLAG"));
                gradeBean.setId(rs.getString("ID"));
                gradeBean.setStartDate(rs.getString("START_DATE"));
                gradeBean.setUpdatedBy(rs.getString("UPDATEDBY"));
                gradeBean.setUpdatedDate(rs.getString("UPDATEDDATE"));

                gradeBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                gradeBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                gradeBean.setCode(rs.getString("CODE"));
                gradeBean.setPositionName(rs.getString("POSITION"));
                gradeBeanList.add(gradeBean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return gradeBeanList;
    }

    public ValidGradeBean UpdateValidGradeCode(ValidGradeBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String code = bean.getId() + bean.getJobCode() + bean.getGrade();
            String query =
                "UPDATE  " + " " + getSchema_Name() + ".XXX_VALID_GRADE \n" +
                "SET CODE = ? " + "WHERE id = ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            ps.setInt(2, Integer.parseInt(bean.getId()));
            ps.executeUpdate();

            getMaxID();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<ValidGradeBean> getAllValidGradeByCode(String code) {
        ArrayList<ValidGradeBean> gradeBeanList =
            new ArrayList<ValidGradeBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "    select  * from  " + " " + getSchema_Name() + ".XXX_VALID_GRADE  \n" +
                "                    where CODE =?";

            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            rs = ps.executeQuery();
            while (rs.next()) {
                ValidGradeBean gradeBean = new ValidGradeBean();
                gradeBean.setJobCode(rs.getString("JOB_CODE"));
                gradeBean.setGrade(rs.getString("GRADE"));
                gradeBean.setCreatedBy(rs.getString("CREATEDBY"));
                gradeBean.setCreationDate(rs.getString("CREATEDDATE"));

                gradeBean.setEndDate(rs.getString("END_DATE"));
                gradeBean.setFlag(rs.getString("FLAG"));
                gradeBean.setId(rs.getString("ID"));
                gradeBean.setStartDate(rs.getString("START_DATE"));
                gradeBean.setUpdatedBy(rs.getString("UPDATEDBY"));
                gradeBean.setUpdatedDate(rs.getString("UPDATEDDATE"));

                gradeBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                gradeBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                gradeBean.setCode(rs.getString("CODE"));
                gradeBeanList.add(gradeBean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return gradeBeanList;

    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

}
