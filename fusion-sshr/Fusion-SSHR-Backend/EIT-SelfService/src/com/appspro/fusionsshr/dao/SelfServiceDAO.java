/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.DelegationBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.HandoverHeaderBean;
import com.appspro.fusionsshr.bean.MessageBeanSetup;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;

import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import com.appspro.mail.CredentialStoreClassMail;

import com.hha.appsproExceptionn.AppsProException;

import com.oracle.vmm.client.provider.ovm22.ws.sps.ArrayList;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;


import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import utilities.NotificationUtility;

public class SelfServiceDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    EmployeeDetails employeeDetails = new EmployeeDetails();
    MessageBeanSetup messageBean = new MessageBeanSetup();
    HashMap<String, String> extraInfoForEmail = new HashMap<String, String>();
    PeopleExtraInformationBean peib = new PeopleExtraInformationBean();

    public SelfServiceBean insertIntoSelfService(SelfServiceBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "insert into  " + " " + getSchema_Name() + ".XX_SELF_SERVICE (TYPE,TYPE_TABLE_NAME,TRANSACTION_ID,PERSON_ID,CREATED_BY,PERSON_NUMBER,CREATION_DATE) " +
                    "VALUES(?,?,?,?,?,?,sysdate)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getType());
            ps.setString(2, bean.getTypeTableName());
            ps.setString(3, bean.getTransactionId());
            ps.setString(4, bean.getPresonId());
            ps.setString(5, bean.getPresonId());
            // ps.setString(6, "sysdate");
            ps.setString(6, bean.getPersonNumber());
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public void updateSelfService(String RESPONSE_CODE, String ssType,
                                  String transactionId) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            //(transactionId);
            query =
                    "UPDATE   " + " " + getSchema_Name() + ".XX_SELF_SERVICE  " +
                    "SET STATUS = ?" + "WHERE TYPE = ? AND TRANSACTION_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, RESPONSE_CODE);
            ps.setString(2, ssType);
            ps.setString(3, transactionId);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }

    public String approvalpropation(SelfServiceBean bean) throws AppsProException {
        SelfServiceBean sfBean = new SelfServiceBean();

        java.util.ArrayList<ApprovalSetupBean> approvalSetupList =
            new java.util.ArrayList<ApprovalSetupBean>();


        ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
        ApprovalListBean alBean = new ApprovalListBean();
        ApprovalListDAO approvalListDao = new ApprovalListDAO();
        WorkflowNotificationDAO workflowNotificationDao =
            new WorkflowNotificationDAO();
        WorkFlowNotificationBean wfn = new WorkFlowNotificationBean();
        PeopleExtraInformationDAO peopleExtra =
            new PeopleExtraInformationDAO();
        ProbationaryPeriodEvaluationDAO probationperiodEvaluation =
            new ProbationaryPeriodEvaluationDAO();
        int requestID =
            Integer.parseInt(probationperiodEvaluation.getMaxprobationperiodEvaluation());

        sfBean.setType("XXX_HR_PROBATION_PERFORMANCE");
        sfBean.setTypeTableName("XXX_HR_PROBATION_PERFORMANCE");
        sfBean.setTransactionId(Integer.toString(requestID));
        sfBean.setPresonId(bean.getPresonId());
        sfBean.setCreated_by(bean.getCreated_by());
        sfBean.setPersonNumber(bean.getPersonNumber());
        insertIntoSelfService(sfBean);
        String s = "TEST";
        if (!s.equals("DRAFT")) {
            approvalSetupList =
                    approvalSetup.getApprovalByEITCode(bean.getType());


            alBean.setStepLeval("0");
            alBean.setServiceType(bean.getType());
            alBean.setTransActionId(Integer.toString(requestID));
            alBean.setWorkflowId("");
            alBean.setRolrType("EMP");
            alBean.setRoleId(bean.getPresonId());
            alBean.setPersonName(bean.getPersonName());
            alBean.setResponseCode("SUBMIT");
            alBean.setNotificationType("FYI");

            approvalListDao.insertIntoApprovalList(alBean);
            alBean = null;
            ApprovalSetupBean ItratorBean;
            Iterator<ApprovalSetupBean> approvalSetupListIterator =
                approvalSetupList.iterator();
            while (approvalSetupListIterator.hasNext()) {
                alBean = new ApprovalListBean();
                ItratorBean = approvalSetupListIterator.next();
                alBean.setStepLeval(ItratorBean.getApprovalOrder());
                alBean.setServiceType(bean.getType());
                alBean.setTransActionId(Integer.toString(requestID));
                alBean.setWorkflowId("");
                alBean.setRolrType(ItratorBean.getApprovalType());
                if (!alBean.getRolrType().equals("POSITION")) {
                    if (alBean.getRolrType().equals("EMP")) {
                        alBean.setRoleId(bean.getPresonId());
                    } else if (alBean.getRolrType().equals("LINE_MANAGER")) {
                        alBean.setRoleId(bean.getManagerId());
                        alBean.setLineManagerName(bean.getManagerName());
                    } else if (alBean.getRolrType().equals("LINE_MANAGER+1")) {
                        alBean.setRoleId(bean.getManagerOfManager());
                        alBean.setLineManagerName(bean.getManagerOfMnagerName());
                    } else if (alBean.getRolrType().equals("ROLES")) {
                        alBean.setRoleId(ItratorBean.getRoleName());
                    } else if (alBean.getRolrType().equals("Special_Case")) {

                        //Caall Dynamic Report Validation -- arg
                        BIReportDAO bIReportDAO = new BIReportDAO();
                        JSONObject obj = new JSONObject();
                        obj.put("reportName", "DynamicValidationReport");
                        obj.put("str", ItratorBean.getSpecialCase());

                        JSONObject jsonObj =
                            new JSONObject(bIReportDAO.getBiReport(obj));

                        //Value - return
                        AppsproConnection.LOG.info("VALUE ====== " +
                                                   jsonObj.get("value").toString());
                        alBean.setRoleId(jsonObj.get("value").toString());
                    }

                } else if (alBean.getRolrType().equals("POSITION")) {
                    alBean.setRoleId(ItratorBean.getRoleName());
                }

                alBean.setNotificationType(ItratorBean.getNotificationType());

                approvalListDao.insertIntoApprovalList(alBean);
                alBean = null;
            }
            if (!approvalSetupList.isEmpty()) {
                wfn.setMsgTitle(bean.getType() + "Request");
                wfn.setMsgBody(bean.getType() + "Request for" +
                               bean.getPresonId());
                wfn.setPersonName(bean.getPersonName());
                ApprovalListBean rolesBean =
                    approvalListDao.getRoles(Integer.toString(requestID),
                                             bean.getType());

                wfn.setReceiverType(rolesBean.getRolrType());
                wfn.setReceiverId(rolesBean.getRoleId());
                wfn.setType(rolesBean.getNotificationType());
                wfn.setRequestId(Integer.toString(requestID));
                wfn.setStatus("OPEN");
                wfn.setSelfType(bean.getType());
                workflowNotificationDao.insertIntoWorkflow(wfn);

            } else {
                peopleExtra.updateStatus("APPROVED", "", requestID);

                updateSelfService("APPROVED", sfBean.getType(),
                                  sfBean.getTransactionId());
            }

        }
        return "";
    }


    public String approvalCycleHandoverRequest(SelfServiceBean bean, String TransactionType,
                                               String approvalCondition) throws AppsProException {
        String approval1 = "";
        String empApproval = "";
        boolean isInsertApprovalList = false;
        int count = 0;
        SelfServiceBean sfBean = new SelfServiceBean();
        java.util.ArrayList<ApprovalSetupBean> approvalSetupList =
            new java.util.ArrayList<ApprovalSetupBean>();
        ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
        ApprovalListBean alBean = new ApprovalListBean();
        ApprovalListDAO approvalListDao = new ApprovalListDAO();
        WorkflowNotificationDAO workflowNotificationDao =
            new WorkflowNotificationDAO();
        WorkFlowNotificationBean wfn = new WorkFlowNotificationBean();
        handoverDAO handoverRequest = new handoverDAO();
        int requestID;
        if(TransactionType.equals("EDIT")){
            requestID = Integer.parseInt(bean.getTransactionId());
        }else{
        requestID =
            Integer.parseInt(handoverRequest.getMaxHandOverRequest());
        }
        HandoverHeaderBean handoverBean = new HandoverHeaderBean();
        handoverBean = handoverRequest.getHandoverByHeaderId(requestID);
        sfBean.setType(bean.getType());
        sfBean.setTypeTableName("XXX_HANDOVER_REQUEST");
        sfBean.setTransactionId(Integer.toString(requestID));
        sfBean.setPresonId(bean.getPresonId());
        sfBean.setCreated_by(bean.getCreated_by());
        sfBean.setPersonNumber(bean.getPersonNumber());
        insertIntoSelfService(sfBean);
        String s = "TEST";
        if (!s.equals("DRAFT")) {
            approvalSetupList =
                    approvalSetup.getApprovalByEITCodeAndConditionCode(bean.getType(),
                                                                       approvalCondition);

            alBean.setStepLeval("0");
            alBean.setServiceType(bean.getType());
            alBean.setTransActionId(Integer.toString(requestID));
            alBean.setWorkflowId("");
            alBean.setRolrType("EMP");
            alBean.setRoleId(bean.getPresonId());
            alBean.setPersonName(bean.getPersonName());
            alBean.setResponseCode("SUBMIT");
            alBean.setNotificationType("FYI");

            approvalListDao.insertIntoApprovalList(alBean);
            // String emailBody= "You Have "+ extraInformationBean.getEit_name()+ " Notification For your kind Approval" ;
            String body = "";
            EmployeeBean empObj = new EmployeeBean();
            String subject = "";
            try {
                empObj =
                        employeeDetails.getEmpNameAndEmail(bean.getPresonId(), "",
                                                           "");
//                body =
//                        rh.getEmailBody("FYI", empObj.getDisplayName(), "Handover Request",
//                empObj.getDisplayName());
                subject =
                        rh.getEmailSubject("FYI", "Handover Request", empObj.getDisplayName());
                extraInfoForEmail.put("Reciver",
                                      empObj.getDisplayName());
                extraInfoForEmail.put("EIT_NAME",
                                      "Handover Request");
                extraInfoForEmail.put("employeeName",
                                      empObj.getDisplayName());
                extraInfoForEmail.put("STATUS",
                                      "Submitted");
                peib.setEit_name("Handover Request");
                peib.setPersonName(bean.getPersonName());
                
                messageBean =
                        NotificationUtility.buildEmailBody(null,
                                                           extraInfoForEmail,
                                                           "XXX",
                                                           "FYI");
                CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                           subject,
                                                           messageBean.getEmailBody(),
                                                           messageBean.getAttachment(),
                                                           messageBean.getFileName(),
                                                           "FYI",
                                                           null,
                                                           "Handover Request",
                                                           messageBean.getHaveAtt() !=
                                                           null &&
                                                           "Y".equals(messageBean.getHaveAtt()) ?
                                                           true :
                                                           false,
                                                           peib);

                //mailObj.SEND_MAIL(empObj.getEmail(), emailSupject, emailBody);
            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
                throw new AppsProException(e);
            }

            alBean = null;
            ApprovalSetupBean ItratorBean;
            Iterator<ApprovalSetupBean> approvalSetupListIterator =
                approvalSetupList.iterator();
            while (approvalSetupListIterator.hasNext()) {
                count++;
                alBean = new ApprovalListBean();
                ItratorBean = approvalSetupListIterator.next();
                if (ItratorBean.getApprovalType().equals("JOB_LEVEL")) {
                    alBean.setServiceType(bean.getType());
                    alBean.setTransActionId(Integer.toString(requestID));
                    alBean.setWorkflowId("");
                    alBean.setRolrType(ItratorBean.getApprovalType());
                    alBean.setNotificationType(ItratorBean.getNotificationType());
                    /**************** call job level report *******************/
                    JSONObject dataDS = null;
                    JSONArray g1 = null;
                    BIReportDAO BIReportDAO = new BIReportDAO();
                    JSONObject obj = new JSONObject();
                    obj.put("reportName", "GET_JOB_LEVEL_REPORT");
                    obj.put("P_PERSON_NUMBER", bean.getPersonNumber());
                    JSONObject jsonObj =
                        new JSONObject(BIReportDAO.getBiReport(obj));
                    if (!jsonObj.getJSONObject("DATA_DS").isNull("G_1")) {
                        dataDS = jsonObj.getJSONObject("DATA_DS");
                        g1 = dataDS.getJSONArray("G_1");
                    }

                    for (int i = 0; i < g1.length(); i++) {
                        JSONObject data = (JSONObject)g1.get(i);
                        String stpLevel =
                            approvalListDao.getMaxStepLeval(String.valueOf(Integer.toString(requestID)),
                                                            String.valueOf(bean.getType()));
                        if (ItratorBean.getOperation().equals(">=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) >=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals(">")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) >
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("<")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) <
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("<=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) <=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                            }
                        } else if (ItratorBean.getOperation().equals("==") ||
                                   ItratorBean.getOperation().equals("===")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) ==
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        } else if (ItratorBean.getOperation().equals("!=")) {
                            if (Integer.parseInt(data.get("APPROVAL_AUTHORITY").toString()) !=
                                Integer.parseInt(ItratorBean.getManagerLeval())) {
                                alBean.setRoleId(data.get("MANAGER_ID").toString());
                                alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                                    data.get("APPROVAL_LEVEL").toString() :
                                                    stpLevel);
                                alBean.setLineManagerName(data.get("FULL_NAME").toString());
                            }
                        }
                        approvalListDao.insertIntoApprovalList(alBean);
                    }
                } else {
                    System.out.println("ID  : " + Integer.toString(requestID));
                    System.out.println("Code  : " + bean.getType());
                    String stpLevel =
                        approvalListDao.getMaxStepLeval(String.valueOf(Integer.toString(requestID)),
                                                        String.valueOf(bean.getType()));
                    System.out.println("stpLevel : " + stpLevel);
                    alBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?
                                        ItratorBean.getApprovalOrder() :
                                        stpLevel);
                    alBean.setServiceType(bean.getType());
                    alBean.setTransActionId(Integer.toString(requestID));
                    alBean.setWorkflowId("");
                    alBean.setRolrType(ItratorBean.getApprovalType());
                    java.util.ArrayList<DelegationBean> listArr =
                        new java.util.ArrayList<DelegationBean>();
                    DelegationDAO serviceDAO = new DelegationDAO();
                    listArr = serviceDAO.getAllSummary();
                    java.util.Date currentDate = new java.util.Date();
                    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");


                    if (!alBean.getRolrType().equals("POSITION")) {
                        alBean.setRoleId("");
                        for (int i = 0; i < listArr.size(); i++) {
                            //                                    if(!listArr.get(i).getPersonId().equals(extraInformationBean.getPerson_id())){

                            if (listArr.get(i).getCreatedByPersonId().equals(bean.getPresonId()) &&
                                ft.format(currentDate).compareTo(listArr.get(i).getEffectiveEndDate()) <=
                                0) {
                                alBean.setRoleId(listArr.get(i).getPersonId());
                            } else if (listArr.get(i).getCreatedByPersonId().equals(bean.getManagerId()) &&
                                       ft.format(currentDate).compareTo(listArr.get(i).getEffectiveEndDate()) <=
                                       0) {
                                alBean.setRoleId(listArr.get(i).getPersonId());
                            } else if (listArr.get(i).getCreatedByPersonId().equals(bean.getManagerOfManager()) &&
                                       ft.format(currentDate).compareTo(listArr.get(i).getEffectiveEndDate()) <=
                                       0) {
                                alBean.setRoleId(listArr.get(i).getPersonId());
                            }
                        }
                        //                                    }

                        if (alBean.getRoleId().isEmpty() ||
                            alBean.getRoleId().equals(null) ||
                            alBean.getRoleId() == "") {
                            if (alBean.getRolrType().equals("EMP")) {
                                if (count == 1) {
                                    approval1 = handoverBean.getHandoverTo1();
                                    empApproval = handoverBean.getHandoverTo1Name();
                                } else if (count == 2) {

                                    approval1 = handoverBean.getHandoverTo2();
                                    empApproval = handoverBean.getHandoverTo2Name();
                                } else if (count == 3) {

                                    approval1 = handoverBean.getHandoverTo3();
                                    empApproval = handoverBean.getHandoverTo3Name();
                                }
                                alBean.setRoleId(approval1);
                                alBean.setPersonName(empApproval);
                            } else if (alBean.getRolrType().equals("LINE_MANAGER") &&
                                       bean.getManagerId() != null &&
                                       !bean.getManagerId().isEmpty()) {
                                alBean.setRoleId(bean.getManagerId());
                                alBean.setPersonName(bean.getManagerName());

                            } else if (alBean.getRolrType().equals("LINE_MANAGER+1") &&
                                       bean.getManagerOfManager() != null &&
                                       !bean.getManagerOfManager().isEmpty()) {
                                alBean.setRoleId(bean.getManagerOfManager());
                                alBean.setPersonName(bean.getManagerOfMnagerName());
                                //  EmployeeBean obj = new EmployeeBean();


                            }
                        }


                        if (alBean.getRolrType().equals("Special_Case")) {

                            //Caall Dynamic Report Validation -- arg
                            BIReportDAO bIReportDAO = new BIReportDAO();
                            JSONObject obj = new JSONObject();
                            obj.put("reportName", "DynamicValidationReport");
                            obj.put("str", ItratorBean.getSpecialCase());

                            JSONObject jsonObj =
                                new JSONObject(bIReportDAO.getBiReport(obj));

                            alBean.setRoleId(jsonObj.get("value").toString());
                        } else if (alBean.getRolrType().equals("ROLES")) {
                            alBean.setRoleId(ItratorBean.getRoleName());
                            //("Role Name : ");
                            //(ItratorBean.getRoleName());
                        } else if (alBean.getRolrType().equals("AOR")) {
                            /**************** get AOR report  *******************/

                            BIReportDAO bIReportDAO = new BIReportDAO();
                            JSONObject obj = new JSONObject();
                            obj.put("reportName", "GET_AOR_REP");
                            obj.put("P_PERSON_NUMBER", bean.getPersonNumber());

                            JSONObject data =
                                new JSONObject(bIReportDAO.getBiReport(obj));
                            if (!data.getJSONObject("DATA_DS").isNull("G_1")) {
                                JSONObject dataDS =
                                    data.getJSONObject("DATA_DS");
                                JSONObject g1 = dataDS.getJSONObject("G_1");
                                alBean.setRoleId(g1.get("PERSON_ID").toString());
                                alBean.setLineManagerName(g1.get("FULL_NAME").toString());
                            }
                        }

                    } else if (alBean.getRolrType().equals("POSITION")) {
                        alBean.setRoleId(ItratorBean.getRoleName());
                    }
                    alBean.setNotificationType(ItratorBean.getNotificationType());
                    if (alBean.getRoleId() != null &&
                        !alBean.getRoleId().isEmpty()) {
                        if(alBean.getNotificationType().equals("FYI") && alBean.getRolrType().equals("EMP")){
                            alBean.setRoleId(bean.getPresonId());
                            alBean.setPersonName(bean.getPersonName());
                        }
                        approvalListDao.insertIntoApprovalList(alBean);
                        isInsertApprovalList = true;
                    }

                }
                alBean = null;
            }
            if (isInsertApprovalList) {
                wfn.setMsgTitle(bean.getType());
                wfn.setMsgBody(bean.getType() + ":" + bean.getPresonId());

                ApprovalListBean rolesBean =
                    approvalListDao.getRoles(Integer.toString(requestID),
                                             bean.getType());

                //1617 XXX_HR_MEDICAL_INSUR_FOR_HR


                //null LINE_MANAGER

                wfn.setReceiverType(rolesBean.getRolrType());
                wfn.setReceiverId(rolesBean.getRoleId());
                wfn.setType(rolesBean.getNotificationType());
                wfn.setRequestId(Integer.toString(requestID));
                wfn.setStatus("OPEN");
                wfn.setSelfType(bean.getType());
                wfn.setPersonName(bean.getPersonName());
                wfn.setNationalIdentity("");
                wfn.setResponsePersonId(rolesBean.getRoleId());
                workflowNotificationDao.insertIntoWorkflow(wfn);

                System.out.println("Role Type : " + rolesBean.getRolrType());
                System.out.println("Role Id : " + rolesBean.getRoleId());
                if (rolesBean.getRolrType().equals("EMP")) {
                    try {
                        empObj =
                                employeeDetails.getEmpNameAndEmail(rolesBean.getRoleId(),
                                                                        "",
                                                                        "");
                        
                        subject =
                                rh.getEmailSubject(rolesBean.getNotificationType(),
                                                   "Handover",
                                                   bean.getPersonName());
                        extraInfoForEmail.put("Reciver",
                                              empObj.getDisplayName());
                        extraInfoForEmail.put("EIT_NAME",
                                              "Handover");
                        extraInfoForEmail.put("employeeName",
                                              bean.getPersonName());
                        extraInfoForEmail.put("STATUS",
                                              "pending approval");
                        peib.setEit_name("Handover");
                        peib.setPersonName(bean.getPersonName());
                        
                        messageBean =
                                NotificationUtility.buildEmailBody(null,
                                                                   extraInfoForEmail,
                                                                   "XXX",
                                                                   rolesBean.getNotificationType());
                        CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                                   subject,
                                                                   messageBean.getEmailBody(),
                                                                   messageBean.getAttachment(),
                                                                   messageBean.getFileName(),
                                                                   rolesBean.getNotificationType(),
                                                                   null,
                                                                   "Handover",
                                                                   messageBean.getHaveAtt() !=
                                                                   null &&
                                                                   "Y".equals(messageBean.getHaveAtt()) ?
                                                                   true :
                                                                   false,
                                                                   peib);

                       
                        

                        //                                    mailObj.SEND_NOTIFICATION(empObj.getEmail(), subject, body,
                        //                                                              null, null, rolesBean.getNotificationType(),
                        //                                                              Integer.toString(extraInformationBean.getId()), extraInformationBean.getEit_name(),
                        //                                                              false, extraInformationBean);
                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                        AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                }
                if (rolesBean.getRolrType().equals("LINE_MANAGER")) {
                    try {
                        empObj =
                                employeeDetails.getEmpDetailsByPersonId(bean.getManagerId(),
                                                                        "",
                                                                        "");

                        body =
rh.getEmailBody(rolesBean.getNotificationType(), empObj.getDisplayName(),
                "Handover", bean.getPersonName());
                        subject =
                                rh.getEmailSubject(rolesBean.getNotificationType(),
                                                   "Handover",
                                                   bean.getPersonName());

                        //                                    mailObj.SEND_NOTIFICATION(empObj.getEmail(), subject, body,
                        //                                                              null, null, rolesBean.getNotificationType(),
                        //                                                              Integer.toString(extraInformationBean.getId()), extraInformationBean.getEit_name(),
                        //                                                              false, extraInformationBean);
                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                        AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("LINE_MANAGER+1")) {
                    try {
                        empObj =
                                employeeDetails.getEmpNameAndEmail(bean.getManagerOfManager(),
                                                                   "", "");
                        body =
rh.getEmailBody(rolesBean.getNotificationType(), empObj.getDisplayName(),
                "Handover", bean.getPersonName());
                        subject =
                                rh.getEmailSubject(rolesBean.getNotificationType(),
                                                   "Handover",
                                                   bean.getPersonName());

                        //                                    mailObj.SEND_NOTIFICATION(empObj.getEmail(), subject, body,
                        //                                                              null, null, rolesBean.getNotificationType(),
                        //                                                              Integer.toString(extraInformationBean.getId()), extraInformationBean.getEit_name(),
                        //                                                              false, extraInformationBean);
                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                        AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("ROLES")) {
                    try {
                        Map<String, String> paramMAp =
                            new HashMap<String, String>();
                        paramMAp.put("P_ROLEID", rolesBean.getRoleId());
                        JSONObject respone =
                            BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                                    paramMAp);

                        JSONObject dataDS = respone.getJSONObject("DATA_DS");
                        JSONArray g1 = dataDS.getJSONArray("G_1");
                        for (int i = 0; i < g1.length(); i++) {
                            JSONObject data = (JSONObject)g1.get(i);
                            body =
rh.getEmailBody(rolesBean.getNotificationType(), data.getString("FULL_NAME"),
                "Handover", bean.getPersonName());
                            subject =
                                    rh.getEmailSubject(rolesBean.getNotificationType(),
                                                       "Handover",
                                                       bean.getPersonName());

                            //                                        mailObj.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"), subject, body,
                            //                                                                  null, null, rolesBean.getNotificationType(),
                            //                                                                  Integer.toString(extraInformationBean.getId()), extraInformationBean.getEit_name(),
                            //                                                                  false, extraInformationBean);
                        }


                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                        AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("POSITION")) {
                    JSONObject dataDS = null;
                    try {

                        Map<String, String> paramMAp =
                            new HashMap<String, String>();
                        paramMAp.put("positionId", rolesBean.getRoleId());
                        JSONObject respone =
                            BIReportModel.runReport("EmailForPositionsReport",
                                                    paramMAp);

                        dataDS = respone.getJSONObject("DATA_DS");
                        JSONArray g1 = dataDS.getJSONArray("G_1");
                        for (int i = 0; i < g1.length(); i++) {
                            JSONObject data = (JSONObject)g1.get(i);

                            body =
rh.getEmailBody(rolesBean.getNotificationType(),
                data.getString("DISPLAY_NAME"), "Handover",
                bean.getPersonName());
                            subject =
                                    rh.getEmailSubject(rolesBean.getNotificationType(),
                                                       "Handover",
                                                       bean.getPersonName());

                            //                                                                                         mailObj.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"), subject, body,
                            //                                                                                                                   null, null, rolesBean.getNotificationType(),
                            //                                                                                                                   Integer.toString(extraInformationBean.getId()), extraInformationBean.getEit_name(),
                            //                                                                                                                   false, extraInformationBean);
                        }


                    } catch (Exception e) {
                        //("Error: ");
                        JSONObject g1 = dataDS.getJSONObject("G_1");
                        body =
rh.getEmailBody(rolesBean.getNotificationType(), g1.getString("DISPLAY_NAME"),
                "Handover", bean.getPersonName());
                        subject =
                                rh.getEmailSubject(rolesBean.getNotificationType(),
                                                   "Handover",
                                                   bean.getPersonName());

                        //                                                                mailObj.SEND_NOTIFICATION(g1.getString("EMAIL_ADDRESS"), subject, body,
                        //                                                                                          null, null, rolesBean.getNotificationType(),
                        //                                                                                          Integer.toString(extraInformationBean.getId()), extraInformationBean.getEit_name(),
                        //                                                                                          false, extraInformationBean);
                        // e.printStackTrace();
                        //AppsproConnection.LOG.error("ERROR", e);
                        // throw new AppsProException(e);
                    }
                }
            }
        }
        return "[]";
    }
    
}