/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.IconBean;
import com.appspro.fusionsshr.bean.xxx_pass_lookupBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author lenovo
 */
public class IconsDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    AppsproConnection appsObg = new AppsproConnection();

    public ArrayList<IconBean> getIcons() {

        ArrayList<IconBean> IconList = new ArrayList<IconBean>();

        try {
            connection = AppsproConnection.getConnection();
            //(getSchema_Name() );
            String query = "SElect * from  "+ " " + getSchema_Name() + ".XXX_ICONS";
            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                IconBean xxx_bean = new IconBean();
                xxx_bean.setId(rs.getString(1));
                xxx_bean.setEitCode(rs.getString(2));
                xxx_bean.setIcons(rs.getString(3));
                xxx_bean.setLeftIcons(rs.getString(4));

                IconList.add(xxx_bean);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return IconList;

    }

}
