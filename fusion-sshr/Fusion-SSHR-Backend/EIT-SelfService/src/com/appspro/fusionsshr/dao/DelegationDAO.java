package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.DelegationBean;

import static common.restHelper.RestHelper.getSchema_Name;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import com.appspro.fusionsshr.bean.roleSetupValidationBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

public class DelegationDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    ArrayList<DelegationBean> DelegationList =
        new ArrayList<DelegationBean>();
    public ArrayList<DelegationBean> getAllBypersonNumber(String personNumber) {

       
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + "  " + " " + getSchema_Name() + ".XXX_Delegation WHERE CREATED_BY_PERSONNUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, personNumber);
            rs = ps.executeQuery();
            while (rs.next()) {
                DelegationBean Delegation = new DelegationBean();
                Delegation.setId(rs.getString("ID"));
                Delegation.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                Delegation.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                Delegation.setPersonId(rs.getString("PERSONID"));
                Delegation.setPersonNumber(rs.getString("PERSONNUMBER"));
                Delegation.setCreatedByPersonId(rs.getString("CREATED_BY_PERSONID"));
                Delegation.setCreateByPersonNumber(rs.getString("CREATED_BY_PERSONNUMBER"));
                
                DelegationList.add(Delegation);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return DelegationList;

    }
    
    public ArrayList<DelegationBean> getAllSummary() {

       
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + "  " + " " + getSchema_Name() + ".XXX_Delegation";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                DelegationBean Delegation = new DelegationBean();
                Delegation.setId(rs.getString("ID"));
                Delegation.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                Delegation.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                Delegation.setPersonId(rs.getString("PERSONID"));
                Delegation.setPersonNumber(rs.getString("PERSONNUMBER"));
                Delegation.setCreatedByPersonId(rs.getString("CREATED_BY_PERSONID"));
                Delegation.setCreateByPersonNumber(rs.getString("CREATED_BY_PERSONNUMBER"));
                
                DelegationList.add(Delegation);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return DelegationList;

    }
    
    public ArrayList<DelegationBean> DelegationCountPeriod(String   personNumber) {
   
       String count=null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select EFFECTIVE_END_DATE from" + "  " + " " + getSchema_Name() + ".XXX_Delegation WHERE CREATED_BY_PERSONNUMBER=?";
            ps = connection.prepareStatement(query);
//            ps.setString(1, delefationBean.getPersonNumber());
            ps.setString(1,personNumber);
            rs = ps.executeQuery();
            while (rs.next()) {
                DelegationBean Delegation = new DelegationBean();
                Delegation.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                DelegationList.add(Delegation);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return DelegationList;

    }
    
    
    
    
    public ArrayList<DelegationBean> validationPeriod(DelegationBean  delegationBean) {
        ArrayList<DelegationBean> DelegationList =
            new ArrayList<DelegationBean>();
       
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT EFFECTIVE_END_DATE ,EFFECTIVE_START_DATE FROM(select * from " + " " + getSchema_Name() + ". XXX_DELEGATION WHERE ID !=?) WHERE  CREATED_BY_PERSONNUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, delegationBean.getId());
//            ps.setString(2, delefationBean.getPersonNumber());
            ps.setString(2, delegationBean.getCreateByPersonNumber());
            rs = ps.executeQuery();
            while (rs.next()) {
                DelegationBean delegation = new DelegationBean();
                delegation.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                delegation.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                DelegationList.add(delegation);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return DelegationList;

    }
    
    
    public DelegationBean insertOrUpdatedelegation(DelegationBean Delegation,
                                                   String transactionType) {
        
        
        try {
            connection = AppsproConnection.getConnection();


            if (transactionType.equals("ADD")) {
                
                String query = "INSERT INTO  " + " " + getSchema_Name() + ".XXX_Delegation(EFFECTIVE_START_DATE,EFFECTIVE_END_DATE,PERSONID,PERSONNUMBER,CREATED_BY_PERSONID,CREATED_BY_PERSONNUMBER)VALUES(?,?,?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, Delegation.getEffectiveStartDate());
                ps.setString(2, Delegation.getEffectiveEndDate());
                ps.setString(3, Delegation.getPersonId());
                ps.setString(4, Delegation.getPersonNumber());
                ps.setString(5, Delegation.getCreatedByPersonId());
                ps.setString(6, Delegation.getCreateByPersonNumber());
                
             
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {


                String query = "UPDATE " + " " + getSchema_Name() + ".XXX_Delegation set EFFECTIVE_START_DATE=? , EFFECTIVE_END_DATE=? , PERSONID=? ,PERSONNUMBER=? , CREATED_BY_PERSONID=?, CREATED_BY_PERSONNUMBER=? where ID=?";
                ps = connection.prepareStatement(query);

                ps.setString(1, Delegation.getEffectiveStartDate());
                ps.setString(2, Delegation.getEffectiveEndDate());
                ps.setString(3, Delegation.getPersonId());
                ps.setString(4, Delegation.getPersonNumber());
                ps.setString(5, Delegation.getCreatedByPersonId());
                ps.setString(6, Delegation.getCreateByPersonNumber());
                ps.setString(7, Delegation.getId());
                ps.executeUpdate();


            }


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Delegation;
    }
    public String deleteDelegationById(String id) {

         connection = AppsproConnection.getConnection();
         String query;
         int checkResult=0;

         try {
             query = "DELETE from XXX_Delegation where id=?";
             ps = connection.prepareStatement(query);
             ps.setInt(1, Integer.parseInt(id));
             checkResult = ps.executeUpdate();
             
         } catch (Exception e) {
             //("Error: ");
            e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
         } finally {
             closeResources(connection, ps, rs);
         }
         return Integer.toString(checkResult);
     } 
    
    
    
    
    
    
    
    
    
}
