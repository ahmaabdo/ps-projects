/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.PaaSDefultValueBean;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.json.JSONObject;


/**
 *
 * @author Lenovo
 */
public class PaaSDefultValueDao extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    // RestHelper rh = new RestHelper();

    public ArrayList<PaaSDefultValueBean> getAllPaaSDefultValue() {
        ArrayList<PaaSDefultValueBean> paaSDefultValueList =
            new ArrayList<PaaSDefultValueBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " select  * from  " + " " + getSchema_Name() + ".XXX_PAAS_DEFAULT_VALUE ";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                PaaSDefultValueBean paaSDefaultValueBean =
                    new PaaSDefultValueBean();
                paaSDefaultValueBean.setEitCode(rs.getString("EIT_CODE"));
                paaSDefaultValueBean.setSegmentName(rs.getString("SEGEMENT_NAME"));
                paaSDefaultValueBean.setQuery(rs.getString("QUERY"));
                paaSDefultValueList.add(paaSDefaultValueBean);
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);

        } finally {
            closeResources(connection, ps, rs);
        }

        return paaSDefultValueList;
    }

    public PaaSDefultValueBean addPaasDefaultValue(PaaSDefultValueBean paaSDefultValue) {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "INSERT INTO  " + " " + getSchema_Name() + ".XXX_PAAS_DEFAULT_VALUE (EIT_CODE,SEGEMENT_NAME,QUERY)VALUES(?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, paaSDefultValue.getEitCode());
            ps.setString(2, paaSDefultValue.getSegmentName());
            ps.setString(3, paaSDefultValue.getQuery());
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return paaSDefultValue;
    }


    public ArrayList<PaaSDefultValueBean> getPaaSDefultValueBySelfService(String selfService) {
        ArrayList<PaaSDefultValueBean> passDefaultValueList =
            new ArrayList<PaaSDefultValueBean>();

        try {
            connection = AppsproConnection.getConnection();

            String query =
                " select  * from  " + " " + getSchema_Name() + ".XXX_PAAS_DEFAULT_VALUE WHERE EIT_CODE=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, selfService);

            rs = ps.executeQuery();
            while (rs.next()) {

                PaaSDefultValueBean paaSDefultValueBean =
                    new PaaSDefultValueBean();
                paaSDefultValueBean.setSegmentName(rs.getString("SEGEMENT_NAME"));
                paaSDefultValueBean.setQuery(rs.getString("QUERY"));
                paaSDefultValueBean.setId(rs.getString("ID"));
                paaSDefultValueBean.setEitCode(rs.getString("EIT_CODE"));
                passDefaultValueList.add(paaSDefultValueBean);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return passDefaultValueList;
    }

    public PaaSDefultValueBean updatePaasDefaultValue(PaaSDefultValueBean paasDefaultBean,
                                                      String id) {
        try {

            connection = AppsproConnection.getConnection();

            String query =
                "UPDATE  XXX_PAAS_DEFAULT_VALUE SET EIT_CODE=?,SEGEMENT_NAME=?,QUERY=? WHERE ID =?";

            ps = connection.prepareStatement(query);
            ps.setString(1, paasDefaultBean.getEitCode());
            ps.setString(2, paasDefaultBean.getSegmentName());
            ps.setString(3, paasDefaultBean.getQuery());
            ps.setInt(4, Integer.parseInt(id));
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return paasDefaultBean;
    }

    public String deletePaasDefaultValue(String id) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult = 0;
        try {
            query = "DELETE FROM  XXX_PAAS_DEFAULT_VALUE  WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            checkResult = ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }


}
