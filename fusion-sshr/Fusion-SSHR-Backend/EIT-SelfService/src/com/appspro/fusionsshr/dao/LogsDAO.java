package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.db.CommonConfigReader;
import com.appspro.fusionsshr.bean.LogsBean;

import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;


public class LogsDAO extends AppsproConnection {


    private Connection connection;
    private PreparedStatement ps;
    private String logger_type = CommonConfigReader.getValue("LOGGER_TYPE");
    private String db_log_level = CommonConfigReader.getValue("DB_LOG_LEVEL");


    public void insertLogs(Object message, Throwable throwable, String level) {
        
        if (!checkLevel(level)) {
            return;
        }
        
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "insert into  " + " " + getSchema_Name() + ".XXX_LOGS (USER_ID,dated,logger,LEVEL_,message)" +
                " VALUES(?,SYSDATE,?,?,?)";
            ps = connection.prepareStatement(query);

            ps.setString(1, null);
            ps.setString(2, throwable != null ? throwable.getMessage() : null);
            ps.setString(3, level);
            ps.setObject(4, message);

            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace();
        } finally {
            closeResources(connection, ps);
        }

    }


    public void error(Object message, Throwable throwable) {
        if (logger_type.equals("DB")) {
            insertLogs(message, throwable, "ERROR");
        } else if (logger_type.equals("CONSOLE")) {
           System.out.println(message);
            System.out.println(throwable.getStackTrace());
        } else {
            insertLogs(message, throwable, "ERROR");
            System.out.println(message);
            System.out.println(throwable.getStackTrace());

        }

    }

    public void error(Object message) {
        if (logger_type.equals("DB")) {
            insertLogs(message, null, "ERROR");
        } else if (logger_type.equals("CONSOLE")) {
            System.out.println(message);

        } else {
            insertLogs(message, null, "ERROR");
            System.out.println(message);

        }
    }

    public void info(Object message, Throwable throwable) {
        if (logger_type.equals("DB")) {
            insertLogs(message, throwable, "INFO");
        } else if (logger_type.equals("CONSOLE")) {
            System.out.println(message);
            System.out.println(throwable.getStackTrace());
        } else {
            insertLogs(message, throwable, "INFO");
            System.out.println(message);
            System.out.println(throwable.getStackTrace());


        }
    }

    public void info(Object message) {
        if (logger_type.equals("DB")) {
            insertLogs(message, null, "INFO");
        } else if (logger_type.equals("CONSOLE")) {
            System.out.println(message);

        } else {
            insertLogs(message, null, "INFO");
			System.out.println(message);



        }
    }

    public void debug(Object message, Throwable throwable) {
        if (logger_type.equals("DB")) {
            insertLogs(message, throwable, "DEBUG");
        } else if (logger_type.equals("CONSOLE")) {
            System.out.println(message);
            System.out.println(throwable.getStackTrace());
        } else {
            insertLogs(message, throwable, "DEBUG");
            System.out.println(message);
            System.out.println(throwable.getStackTrace());

        }
    }

    public void debug(Object message) {
        if (logger_type.equals("DB")) {
            insertLogs(message, null, "DEBUG");
        } else if (logger_type.equals("CONSOLE")) {
            System.out.println(message);
        } else {
            insertLogs(message, null, "DEBUG");
            System.out.println(message);


        }

    }

    private boolean checkLevel(String level) {
        if (db_log_level == null) {
            return false;
        }

        if (level == null) {
            return false;
        }

        if (db_log_level.contains(level)) {
            return true;
        }

        return false;
    }
}


