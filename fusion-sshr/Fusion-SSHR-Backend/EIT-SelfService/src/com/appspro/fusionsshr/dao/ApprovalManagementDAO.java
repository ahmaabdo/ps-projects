/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalManagementBean;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.biPReports.BIPReports;
import common.biPReports.BIReportModel;

import java.io.IOException;

import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 *
 * @author Shadi Mansi-PC
 */


public class ApprovalManagementDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    private int max_transaction;
    BIReportModel objBIR=new BIPReports();
    //ApprovalManagementDAO obj = new ApprovalManagementDAO();

    //*****************Get Transaction Approval Number (SEQ)********************

    public int getTransactionApprovalManagementNumber() {
        connection = AppsproConnection.getConnection();
        String query =
            "SELECT NVL(MAX(TRANSACTION_NUM),0) AS max_transaction_approval FROM  " +
            " " + getSchema_Name() + ".XXX_APROVAL_MANAGEMENT";
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                max_transaction = rs.getInt("max_transaction_approval");
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        max_transaction++;

        return max_transaction;

    }

    //**********************INSERT ON Approval Management***********************

    public ApprovalManagementBean insertOrUpdateApprovalManagement(String body,
                                                                   String transactionType) throws IOException,
                                                                                                  JsonParseException,
                                                                                                  JsonMappingException {
        ApprovalManagementBean obj = new ApprovalManagementBean();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(body);
                JSONObject jsonObjInput;
                String body2 = "";
        
            try {
                if (transactionType.equals("ADD")) {
            int transactionNum = getTransactionApprovalManagementNumber();
                connection = AppsproConnection.getConnection();       
                for(int i = 0; i < arr.length(); i++){
                    jsonObjInput = arr.getJSONObject(i);
                    String query =
                        "INSERT INTO  " + " " + getSchema_Name() + ".XXX_APROVAL_MANAGEMENT(EIT_CODE,SEGMENT_NAME,TYPE,POSITION_NAME,ENABLE,HIDE,TRANSACTION_NUM,CREATED_BY,CREATED_DATE,REQUIRED)\n" +
                        "VALUES(?,?,?,?,?,?,?,?,TO_DATE(SYSDATE, 'DD-MM-YYYY'),?)";
                    ps = connection.prepareStatement(query);
                    ps.setString(1, jsonObjInput.get("eitCode").toString());
                    ps.setString(2, !jsonObjInput.isNull("segementName") ? jsonObjInput.get("segementName").toString():"");
                    ps.setString(3, !jsonObjInput.isNull("type") ? jsonObjInput.get("type").toString():"");
                    ps.setString(4, !jsonObjInput.isNull("positionName") ? jsonObjInput.get("positionName").toString():"");
                    ps.setString(5, !jsonObjInput.isNull("enable") ? jsonObjInput.get("enable").toString():"");
                    ps.setString(6, !jsonObjInput.isNull("hide") ? jsonObjInput.get("hide").toString():"");
                    ps.setInt(7, transactionNum);
                    ps.setString(8, !jsonObjInput.isNull("createdBy") ? jsonObjInput.get("createdBy").toString():"");
                    ps.setString(9, !jsonObjInput.isNull("required") ? jsonObjInput.get("required").toString():"");
                    ps.executeUpdate();

                }

            } else if (transactionType.equals("EDIT")) {
                connection = AppsproConnection.getConnection();       
                    for(int i = 0; i < arr.length(); i++){
                        jsonObjInput = arr.getJSONObject(i);
                    String query =
                        "UPDATE  " + " " + getSchema_Name() + ".XXX_APROVAL_MANAGEMENT AM SET am.enable = ?, am.hide = ? , am.REQUIRED = ? WHERE am.id = ? ";
                    ps = connection.prepareStatement(query);
                    ps.setString(1, !jsonObjInput.isNull("enable") ? jsonObjInput.get("enable").toString():"");
                    ps.setString(2, !jsonObjInput.isNull("hide") ? jsonObjInput.get("hide").toString():"");
                    ps.setString(3, !jsonObjInput.isNull("required") ? jsonObjInput.get("required").toString():"");    
                    ps.setInt(4, Integer.parseInt((String)jsonObjInput.get("id").toString()));

                    ps.executeUpdate();
                    }
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return obj;
    }

    public ArrayList<ApprovalManagementBean> getApprovalManagementByEITCode(String eitCode,
                                                                            String approvalType) {
        ApprovalManagementBean bean = null;
        ArrayList<ApprovalManagementBean> list =
            new ArrayList<ApprovalManagementBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT am.id ,am.eit_code,am.segment_name,am.type,am.enable,am.hide, am.REQUIRED,am.position_name FROM  " +
                    " " + getSchema_Name() +
                    ".XXX_APROVAL_MANAGEMENT AM WHERE am.eit_code = ? AND am.type = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, approvalType);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new ApprovalManagementBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setEitCode(rs.getString("eit_code"));
                bean.setSegementName(rs.getString("segment_name"));
                bean.setType(rs.getString("type"));
                bean.setEnable(rs.getString("enable"));
                bean.setHide(rs.getString("hide"));
                bean.setRequired(rs.getString("REQUIRED"));
                bean.setPositionName(rs.getString("position_name"));
                list.add(bean);

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public String getValidationApprovalManagementByEITCode(String eitCode,
                                                           String approvalType) {
        String count = "";
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT COUNT(am.id) AS eitCodeCount FROM  " + " " + getSchema_Name() +
                    ".XXX_APROVAL_MANAGEMENT AM WHERE am.eit_code = ? AND am.type = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, approvalType);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getString("eitCodeCount");

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
           closeResources(connection, ps, rs);
        }
        return count;
    }

    public ArrayList<ApprovalManagementBean> getApprovalManagementByPositionID(String eitCode,
                                                                               String approvalType,
                                                                               String positionId) {
        ApprovalManagementBean bean = null;
        ArrayList<ApprovalManagementBean> list =
            new ArrayList<ApprovalManagementBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT am.id ,am.eit_code,am.segment_name,am.type,am.enable,am.hide,am.REQUIRED,am.position_name FROM  " +
                    " " + getSchema_Name() +
                    ".XXX_APROVAL_MANAGEMENT AM WHERE am.eit_code = ? AND am.type = ? AND am.position_name = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, approvalType);
            ps.setString(3, positionId);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new ApprovalManagementBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setEitCode(rs.getString("eit_code"));
                bean.setSegementName(rs.getString("segment_name"));
                bean.setType(rs.getString("type"));
                bean.setEnable(rs.getString("enable"));
                bean.setHide(rs.getString("hide"));
                bean.setPositionName(rs.getString("position_name"));
                bean.setRequired(rs.getString("REQUIRED"));
                list.add(bean);

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
             closeResources(connection, ps, rs);
        }
        return list;
    }
    public String deleteApprovalManagement(String eitCode,String type,String id) {
       
        int status = 0;
        connection = AppsproConnection.getConnection();
        String query =
            "DELETE FROM  " + " " + getSchema_Name() + ".XXX_APROVAL_MANAGEMENT WHERE eit_code = ? and type=? and (position_name=? or position_name is null)";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, type);
            ps.setString(3, id);
           status= ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
            }
            return Integer.toString(status);
            }
    
    public String getValidationApprovalManagementByEITCodeANDPostion(String eitCode,
                                                           String approvalType, String positionId) {
        String count = "";
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT COUNT(am.id) AS eitCodeCount FROM  " + " " + getSchema_Name() +
                    ".XXX_APROVAL_MANAGEMENT AM WHERE am.eit_code = ? AND am.type = ? AND am.position_name = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, approvalType);
            ps.setString(3, positionId);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getString("eitCodeCount");

            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
           closeResources(connection, ps, rs);
        }
        return count;
    }
}
