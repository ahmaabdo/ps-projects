package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ManageSelfServiceBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static common.restHelper.RestHelper.getSchema_Name;


import java.util.ArrayList;

public class ManageSelfServiceDAO extends AppsproConnection  {
    
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    
    public ArrayList<ManageSelfServiceBean> getAllManageSelfService() {

     ArrayList<ManageSelfServiceBean> manageSelfSerivceList =
         new ArrayList<ManageSelfServiceBean>();
     
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM "+" "+ getSchema_Name()+".XXX_MANAGE_SS_MESSAGES";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                
                ManageSelfServiceBean manageSelfServiceBean = new ManageSelfServiceBean();
                manageSelfServiceBean.setId(rs.getString("ID"));
                manageSelfServiceBean.setSelfSerivce(rs.getString("SELF_SERVICE"));
                manageSelfServiceBean.setDescriptionAr(rs.getString("DESCRIPTION_AR"));
                manageSelfServiceBean.setDescriptionEn(rs.getString("DESCRIPTION_EN"));
                manageSelfServiceBean.setSelfServDraftMessageAr(rs.getString("SS_DRAFT_MESS_AR"));
                manageSelfServiceBean.setSelfServDraftMessageEn(rs.getString("SS_DRAFT_MESS_EN"));
                manageSelfServiceBean.setSelfServConfMessageAr(rs.getString("SS_CONF_MESS_AR"));
                manageSelfServiceBean.setSelfServConfMessageEn(rs.getString("SS_CONF_MESS_EN"));
                manageSelfSerivceList.add(manageSelfServiceBean);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return manageSelfSerivceList;
     
    }
    
    public ManageSelfServiceBean addManageSelfService(ManageSelfServiceBean manageSelfServiceBean) {
    
        try {
            
            connection = AppsproConnection.getConnection();
            String query = "INSERT INTO  " + " " + getSchema_Name() + ".XXX_MANAGE_SS_MESSAGES (SELF_SERVICE,DESCRIPTION_AR,DESCRIPTION_EN,SS_CONF_MESS_AR,SS_CONF_MESS_EN,SS_DRAFT_MESS_AR,SS_DRAFT_MESS_EN)VALUES(?,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);
                ps.setString(1, manageSelfServiceBean.getSelfSerivce());
                ps.setString(2, manageSelfServiceBean.getDescriptionAr());
                ps.setString(3, manageSelfServiceBean.getDescriptionEn());
                ps.setString(4, manageSelfServiceBean.getSelfServConfMessageAr());
                ps.setString(5, manageSelfServiceBean.getSelfServConfMessageEn());
                ps.setString(6, manageSelfServiceBean.getSelfServDraftMessageAr());
                ps.setString(7, manageSelfServiceBean.getSelfServDraftMessageEn());
               
                ps.executeUpdate();

            


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return manageSelfServiceBean;
    }
    
    public ManageSelfServiceBean updateManageSelfService(ManageSelfServiceBean manageSelfServiceBean) {
    
        try {
            connection = AppsproConnection.getConnection();
            String query = "update  " + " " + getSchema_Name() + ".XXX_MANAGE_SS_MESSAGES set SELF_SERVICE=? ,DESCRIPTION_AR=?,DESCRIPTION_EN=?,SS_CONF_MESS_AR=?,SS_CONF_MESS_EN=?,SS_DRAFT_MESS_AR=?,SS_DRAFT_MESS_EN=?\n" + 
            "where ID=?";
            
                ps = connection.prepareStatement(query);
                ps.setString(1, manageSelfServiceBean.getSelfSerivce());
                ps.setString(2, manageSelfServiceBean.getDescriptionAr());
                ps.setString(3, manageSelfServiceBean.getDescriptionEn());
                ps.setString(4, manageSelfServiceBean.getSelfServConfMessageAr());
                ps.setString(5, manageSelfServiceBean.getSelfServConfMessageEn());                
                ps.setString(6, manageSelfServiceBean.getSelfServDraftMessageAr());
                ps.setString(7, manageSelfServiceBean.getSelfServDraftMessageEn());
                ps.setString(8, manageSelfServiceBean.getId());

                ps.executeUpdate();

            


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return manageSelfServiceBean;
    }
    
    public String deleteManageSelfService(String id) {

           connection = AppsproConnection.getConnection();
           String query;
           int checkResult=0;

           try {
               query = "DELETE FROM "+" "+getSchema_Name()+".XXX_MANAGE_SS_MESSAGES WHERE ID=?";
               ps = connection.prepareStatement(query);
               ps.setInt(1, Integer.parseInt(id));
               checkResult = ps.executeUpdate();
               
           } catch (Exception e) {
               //("Error: ");
              e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
           } finally {
               closeResources(connection, ps, rs);
           }
           return Integer.toString(checkResult);
       }
    
    public ArrayList<ManageSelfServiceBean> getManageByESelfServ(String selfSerivce) {
          ArrayList<ManageSelfServiceBean> manageSelfSevList =
              new ArrayList<ManageSelfServiceBean>();
          
          ManageSelfServiceBean manageSelfServiceObject = null;
          connection = AppsproConnection.getConnection();

          String query =
              "select  * from  " + " " + getSchema_Name() + ".XXX_MANAGE_SS_MESSAGES  where SELF_SERVICE = ? ";

          try {
              ps = connection.prepareStatement(query);
              ps.setString(1, selfSerivce.toString());
              rs = ps.executeQuery();

              while (rs.next()) {
                  manageSelfServiceObject = new ManageSelfServiceBean();
                  manageSelfServiceObject.setId(rs.getString("ID"));
                  manageSelfServiceObject.setSelfSerivce(rs.getString("SELF_SERVICE"));
                  manageSelfServiceObject.setDescriptionAr(rs.getString("DESCRIPTION_AR"));
                  manageSelfServiceObject.setSelfServDraftMessageAr(rs.getString("SS_DRAFT_MESS_AR"));
                  manageSelfServiceObject.setSelfServDraftMessageEn(rs.getString("SS_DRAFT_MESS_EN"));
                  manageSelfServiceObject.setDescriptionEn(rs.getString("DESCRIPTION_EN"));
                  manageSelfServiceObject.setSelfServConfMessageAr(rs.getString("SS_CONF_MESS_AR"));
                  manageSelfServiceObject.setSelfServConfMessageEn(rs.getString("SS_CONF_MESS_EN"));
                  manageSelfServiceObject.setRejectConfirmMessageArVal(rs.getString("REJECT_MESSAGE_AR"));
                  manageSelfServiceObject.setRejectConfirmMessageVal(rs.getString("REJECT_MESSAGE_EN"));
                  manageSelfSevList.add(manageSelfServiceObject);
              }
          } catch (Exception e) {
              //("Error: ");
             e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
          } finally {
              closeResources(connection, ps, rs);
          }
          return manageSelfSevList;

      }
    
    
    
    public ManageSelfServiceBean insertOrUpdateManageSelfServ(ManageSelfServiceBean manageSelfServiceBean,
                                                         String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
          
            if (transactionType.equals("ADD")) {
                String query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_MANAGE_SS_MESSAGES (SELF_SERVICE,DESCRIPTION_AR,DESCRIPTION_EN,SS_CONF_MESS_AR,SS_CONF_MESS_EN,REJECT_MESSAGE_AR,REJECT_MESSAGE_EN,SS_DRAFT_MESS_AR,SS_DRAFT_MESS_EN)VALUES(?,?,?,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);
                ps.setString(1, manageSelfServiceBean.getSelfSerivce());
                ps.setString(2, manageSelfServiceBean.getDescriptionAr());
                ps.setString(3, manageSelfServiceBean.getDescriptionEn());
                ps.setString(4, manageSelfServiceBean.getSelfServConfMessageAr());
                ps.setString(5, manageSelfServiceBean.getSelfServConfMessageEn());
                ps.setString(6, manageSelfServiceBean.getRejectConfirmMessageArVal());
                ps.setString(7, manageSelfServiceBean.getRejectConfirmMessageVal());
                ps.setString(8, manageSelfServiceBean.getSelfServDraftMessageAr());
                ps.setString(9, manageSelfServiceBean.getSelfServDraftMessageEn());
               
                ps.executeUpdate();
                // insert into icons attachment
                String queryIconsInsert = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_ICONS(ICONS,NAME,EITCODE) VALUES(?,?,?)";
               
                ps = connection.prepareStatement(queryIconsInsert);
                ps.setString(1,manageSelfServiceBean.getIconData());
                ps.setString(2,manageSelfServiceBean.getIconName());  
                ps.setString(3,manageSelfServiceBean.getSelfSerivce());
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {
                String query =
                    "UPDATE " + getSchema_Name() + ".XXX_MANAGE_SS_MESSAGES set SELF_SERVICE=? ,DESCRIPTION_AR=?,DESCRIPTION_EN=?,SS_CONF_MESS_AR=?,SS_CONF_MESS_EN=?,REJECT_MESSAGE_AR=?,REJECT_MESSAGE_EN=?,SS_DRAFT_MESS_AR=?,SS_DRAFT_MESS_EN=? \n" + 
                    "where ID=?";
                ps = connection.prepareStatement(query);
                
                ps.setString(1, manageSelfServiceBean.getSelfSerivce());
                ps.setString(2, manageSelfServiceBean.getDescriptionAr());
                ps.setString(3, manageSelfServiceBean.getDescriptionEn());
                ps.setString(4, manageSelfServiceBean.getSelfServConfMessageAr());
                ps.setString(5, manageSelfServiceBean.getSelfServConfMessageEn());
                ps.setString(6, manageSelfServiceBean.getRejectConfirmMessageArVal());
                ps.setString(7, manageSelfServiceBean.getRejectConfirmMessageVal());
                ps.setString(8, manageSelfServiceBean.getSelfServDraftMessageAr());
                ps.setString(9, manageSelfServiceBean.getSelfServDraftMessageEn());
                ps.setString(10, manageSelfServiceBean.getId());
               
                ps.executeUpdate();
                // update icons attachment
                String queryIconUpdate = "UPDATE " + getSchema_Name() + ".XXX_ICONS set ICONS = ?,NAME=?,EITCODE=? where EITCODE =?";

                String queryIconsInsert = "INSERT INTO " + getSchema_Name() + ".XXX_ICONS(ICONS,NAME,EITCODE) VALUES(?,?,?)";
                ps = connection.prepareStatement(manageSelfServiceBean.getIconId() == null ?
                                                 queryIconsInsert : queryIconUpdate);
                
                ps.setString(1, manageSelfServiceBean.getIconData());
                ps.setString(2, manageSelfServiceBean.getIconName());
                ps.setString(3, manageSelfServiceBean.getSelfSerivce());
                if(manageSelfServiceBean.getIconId()!= null){
                    ps.setString(4, manageSelfServiceBean.getSelfSerivce());
                }
                ps.executeUpdate();
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return manageSelfServiceBean;
    }

    
}
