package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.HandoverHeaderBean;
import com.appspro.fusionsshr.bean.HandoverTasksBean;
import com.hha.appsproExceptionn.AppsProException;
import common.restHelper.RestHelper;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import static common.restHelper.RestHelper.getSchema_Name;

public class handoverDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    ResultSet rs2;
    RestHelper restHelper = new RestHelper();
    private String handoverRequestID;
    
    public ArrayList<HandoverHeaderBean> getAllHandoverByHeaderId(int headerId){
        
        ArrayList<HandoverHeaderBean> headerList=new ArrayList<HandoverHeaderBean>();
        connection = AppsproConnection.getConnection();
        try {
            StringBuilder query = new StringBuilder();;
            query.append("select hd.*," +
                "(select value_ar from xx_lookup_value where name = 'SS_STATUS' and code = hd.STATUS) as status_ar," +
                "(select value_en from xx_lookup_value where name = 'SS_STATUS' and code = hd.STATUS) as status_en," +
                "TO_CHAR(hd.CREATION_DATE,'DD/MM/YYYY HH24:MI:SS') as CREATION_DATEF from " + getSchema_Name() +
                ".XXX_HANDOVER_HEADER hd where hd.HEADER_ID=?  order by CREATION_DATE desc");
            ps = connection.prepareStatement(query.toString());
            ps.setInt(1, headerId);
            rs = ps.executeQuery();
            while (rs.next()) {
                HandoverHeaderBean headerBean=new HandoverHeaderBean();
                
                headerBean.setCreationDate(rs.getString("CREATION_DATEF"));
                headerBean.setHeaderId(rs.getInt("HEADER_ID"));
                headerBean.setEmployeeId(rs.getString("EMPLOYEE_ID"));
                headerBean.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));;
                headerBean.setStatus(rs.getString("STATUS"));;
                headerBean.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                headerBean.setEmployeeDepartment(rs.getString("EMPLOYEE_DEPARTMENT"));
                headerBean.setEmployeePosition(rs.getString("EMPLOYEE_POSITION"));
                headerBean.setEmployeeNationality(rs.getString("EMPLOYEE_NATIONALITY"));
                headerBean.setEmployeeManager(rs.getString("EMPLOYEE_MANAGER"));
                headerBean.setHandoverTo1(rs.getString("HANDOVER_TO1"));
                headerBean.setHandoverTo2(rs.getString("HANDOVER_TO2"));
                headerBean.setHandoverTo3(rs.getString("HANDOVER_TO3"));
                headerBean.setReason(rs.getString("REASON"));
                headerBean.setNotes(rs.getString("NOTES"));
                headerBean.setHandoverTo1Name(rs.getString("HANDOVER_TO1_NAME"));
                headerBean.setHandoverTo2Name(rs.getString("HANDOVER_TO2_NAME"));
                headerBean.setHandoverTo3Name(rs.getString("HANDOVER_TO3_NAME"));
                headerBean.setRequestCode(rs.getString("REQUEST_CODE"));
                headerBean.setStatusAr(rs.getString("status_ar"));
                headerBean.setStatusEn(rs.getString("status_en"));
                headerBean.setTasksList(getAllTasks(rs.getInt("HEADER_ID"),connection));
                
                headerList.add(headerBean);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
      return headerList;  
    }
    
    public ArrayList<HandoverHeaderBean> getAllHandover(String personId){
        
        ArrayList<HandoverHeaderBean> headerList=new ArrayList<HandoverHeaderBean>();
        connection = AppsproConnection.getConnection();
        try {
            StringBuilder query = new StringBuilder();;
            query.append("select hd.*," +
                "(select value_ar from xx_lookup_value where name = 'SS_STATUS' and code = hd.STATUS) as status_ar," +
                "(select value_en from xx_lookup_value where name = 'SS_STATUS' and code = hd.STATUS) as status_en," +
                "TO_CHAR(hd.CREATION_DATE,'DD/MM/YYYY HH24:MI:SS') as CREATION_DATEF from " + getSchema_Name() +
                ".XXX_HANDOVER_HEADER hd where hd.EMPLOYEE_ID=? order by CREATION_DATE desc");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, personId);
            rs = ps.executeQuery();
            while (rs.next()) {
                HandoverHeaderBean headerBean=new HandoverHeaderBean();
                
                headerBean.setCreationDate(rs.getString("CREATION_DATEF"));
                headerBean.setHeaderId(rs.getInt("HEADER_ID"));
                headerBean.setEmployeeId(rs.getString("EMPLOYEE_ID"));
                headerBean.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));;
                headerBean.setStatus(rs.getString("STATUS"));;
                headerBean.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                headerBean.setEmployeeDepartment(rs.getString("EMPLOYEE_DEPARTMENT"));
                headerBean.setEmployeePosition(rs.getString("EMPLOYEE_POSITION"));
                headerBean.setEmployeeNationality(rs.getString("EMPLOYEE_NATIONALITY"));
                headerBean.setEmployeeManager(rs.getString("EMPLOYEE_MANAGER"));
                headerBean.setHandoverTo1(rs.getString("HANDOVER_TO1"));
                headerBean.setHandoverTo2(rs.getString("HANDOVER_TO2"));
                headerBean.setHandoverTo3(rs.getString("HANDOVER_TO3"));
                headerBean.setReason(rs.getString("REASON"));
                headerBean.setNotes(rs.getString("NOTES"));
                headerBean.setHandoverTo1Name(rs.getString("HANDOVER_TO1_NAME"));
                headerBean.setHandoverTo2Name(rs.getString("HANDOVER_TO2_NAME"));
                headerBean.setHandoverTo3Name(rs.getString("HANDOVER_TO3_NAME"));
                headerBean.setRequestCode(rs.getString("REQUEST_CODE"));
                headerBean.setStatusAr(rs.getString("status_ar"));
                headerBean.setStatusEn(rs.getString("status_en"));
                headerBean.setTasksList(getAllTasks(rs.getInt("HEADER_ID"),connection));
                
                headerList.add(headerBean);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
      return headerList;  
    }
    
    public ArrayList<HandoverTasksBean> getAllTasks(int headerId,Connection connectionn) {
        ArrayList<HandoverTasksBean> taskList=new ArrayList<HandoverTasksBean>();
        try {
            StringBuilder query = new StringBuilder();
            query.append("SELECT * FROM  " + getSchema_Name() +
                    ".XXX_HANDOVER_TASKS where HEADER_ID = ? order by TASK_ID");
            AppsproConnection.LOG.info(query.toString());
            ps = connectionn.prepareStatement(query.toString());
            ps.setInt(1, headerId);
            rs2 = ps.executeQuery();
            while (rs2.next()) {
                HandoverTasksBean taskBean=new HandoverTasksBean();
                taskBean.setHeaderId(headerId);
                taskBean.setTaskId(rs2.getInt("TASK_ID"));
                taskBean.setTaskName(rs2.getString("TASK_NAME"));
                taskBean.setStatus(rs2.getString("STATUS"));
                taskBean.setDocumentType(rs2.getString("DOCUMENT_TYPE"));
                taskList.add(taskBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            new AppsProException(e);
        }
        finally {
                    closeResources(connection, ps, rs);
                }
        return taskList;
    }

    public HandoverHeaderBean insertOrUpdateXXX_handover(HandoverHeaderBean headerBean,
                                                                      String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();

            if (transactionType.equalsIgnoreCase("ADD")) {
                /**************** insert into header table *******************/
                query.append("insert into " + getSchema_Name() +".XXX_HANDOVER_HEADER ( " +
                    "HEADER_ID," +
                    "EMPLOYEE_ID," +
                    "EMPLOYEE_NUMBER," +
                    "STATUS," +
                    "CREATED_BY," +
                    "CREATION_DATE," +
                    "UPDATE_BY," +
                    "UPDATED_DATE," +
                    "EMPLOYEE_NAME," +
                    "EMPLOYEE_DEPARTMENT," +
                    "EMPLOYEE_POSITION," +
                    "EMPLOYEE_NATIONALITY," +
                    "EMPLOYEE_MANAGER," +
                    "HANDOVER_TO1," +
                    "HANDOVER_TO2," +
                    "HANDOVER_TO3," +
                    "REASON," +
                    "NOTES," +
                    "HANDOVER_TO1_NAME," +
                    "HANDOVER_TO2_NAME," +
                    "HANDOVER_TO3_NAME," +
                    "REQUEST_CODE )" +
                    " values (?,?,?,?,?,sysDate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                ps = connection.prepareStatement(query.toString(),new String[]{"HEADER_ID"});
                ps.setString(1, null);
                ps.setString(2, headerBean.getEmployeeId());
                ps.setString(3, headerBean.getEmployeeNumber());
                ps.setString(4, headerBean.getStatus());
                ps.setString(5, headerBean.getCreateBy());
                ps.setString(6, headerBean.getUpdateBy());
                ps.setString(7, headerBean.getUpdatedDate());
                ps.setString(8, headerBean.getEmployeeName());
                ps.setString(9, headerBean.getEmployeeDepartment());
                ps.setString(10, headerBean.getEmployeePosition());
                ps.setString(11, headerBean.getEmployeeNationality());
                ps.setString(12, headerBean.getEmployeeManager());
                ps.setString(13, headerBean.getHandoverTo1());
                ps.setString(14, headerBean.getHandoverTo2());
                ps.setString(15, headerBean.getHandoverTo3());
                ps.setString(16, headerBean.getReason());
                ps.setString(17, headerBean.getNotes());
                ps.setString(18, headerBean.getHandoverTo1Name());
                ps.setString(19, headerBean.getHandoverTo2Name());
                ps.setString(20, headerBean.getHandoverTo3Name());
                ps.setString(21, headerBean.getRequestCode());
                ps.executeUpdate();
                String headerId = "";
                                    ResultSet generatedKeys = ps.getGeneratedKeys();
                                    if (generatedKeys.next()) {
                                        headerId = generatedKeys.getString(1);
                                        headerBean.setHeaderId(Integer.parseInt(headerId));
                                    }
                                    System.out.println(headerId);
                /*************** insert into tasks table **********************/
                for (int i = 0; i < headerBean.getTasksList().size(); i++) {
                    HandoverTasksBean taskBean =headerBean.getTasksList().get(i);
                    //empty StringBuilder
                    query.setLength(0);
                    query.append("insert into " + getSchema_Name() + ".XXX_HANDOVER_TASKS ( " +
                        "HEADER_ID," +
                        "TASK_NAME," +
                        "STATUS," +
                        "DOCUMENT_TYPE )" +
                        " values (?,?,?,?)");
                    ps = connection.prepareStatement(query.toString());
                    ps.setInt(1, headerBean.getHeaderId());
                    ps.setString(2, taskBean.getTaskName());
                    ps.setString(3, taskBean.getStatus());
                    ps.setString(4, taskBean.getDocumentType());
                    ps.executeUpdate();
                }

            } else if (transactionType.equalsIgnoreCase("EDIT")) {
                /**************** update header table *******************/
                query.setLength(0);
                query.append("update " + getSchema_Name() + ".XXX_HANDOVER_HEADER set " +
                    "EMPLOYEE_ID=NVL(?,EMPLOYEE_ID)," +
                    "EMPLOYEE_NUMBER=NVL(?,EMPLOYEE_NUMBER)," +
                    "STATUS=NVL(?,STATUS)," +
                    "UPDATE_BY=NVL(?,UPDATE_BY)," +
                    "UPDATED_DATE=sysDate," +
                    "EMPLOYEE_NAME=NVL(?,EMPLOYEE_NAME)," +
                    "EMPLOYEE_DEPARTMENT=NVL(?,EMPLOYEE_DEPARTMENT)," +
                    "EMPLOYEE_POSITION=NVL(?,EMPLOYEE_POSITION)," +
                    "EMPLOYEE_NATIONALITY=NVL(?,EMPLOYEE_NATIONALITY)," +
                    "EMPLOYEE_MANAGER=NVL(?,EMPLOYEE_MANAGER)," +
                    "HANDOVER_TO1=NVL(?,HANDOVER_TO1)," +
                    "HANDOVER_TO2=NVL(?,HANDOVER_TO2)," +
                    "HANDOVER_TO3=NVL(?,HANDOVER_TO3)," +
                    "REASON=NVL(?,REASON)," +
                    "NOTES=NVL(?,NOTES)," +
                    "HANDOVER_TO1_NAME=NVL(?,HANDOVER_TO1_NAME)," +
                    "HANDOVER_TO2_NAME=NVL(?,HANDOVER_TO2_NAME)," +
                    "HANDOVER_TO3_NAME=NVL(?,HANDOVER_TO3_NAME)," +
                    "REQUEST_CODE=NVL(?,REQUEST_CODE) where HEADER_ID=?");
                ps = connection.prepareStatement(query.toString());
                ps.setString(1, headerBean.getEmployeeId());
                ps.setString(2, headerBean.getEmployeeNumber());
                ps.setString(3, headerBean.getStatus());
                ps.setString(4, headerBean.getUpdateBy());
                ps.setString(5, headerBean.getEmployeeName());
                ps.setString(6, headerBean.getEmployeeDepartment());
                ps.setString(7, headerBean.getEmployeePosition());
                ps.setString(8, headerBean.getEmployeeNationality());
                ps.setString(9, headerBean.getEmployeeManager());
                ps.setString(10, headerBean.getHandoverTo1());
                ps.setString(11, headerBean.getHandoverTo2());
                ps.setString(12, headerBean.getHandoverTo3());
                ps.setString(13, headerBean.getReason());
                ps.setString(14, headerBean.getNotes());
                ps.setString(15, headerBean.getHandoverTo1Name());
                ps.setString(16, headerBean.getHandoverTo2Name());
                ps.setString(17, headerBean.getHandoverTo3Name());
                ps.setString(18, headerBean.getRequestCode());
                ps.setInt(19, headerBean.getHeaderId());
                ps.executeUpdate();
                /*************** insert into tasks table **********************/
                
                /** remove all tasks related to thid header first**/
                query.setLength(0);
                query .append("delete from " + getSchema_Name() + ".XXX_HANDOVER_TASKS where HEADER_ID = ? ");
                ps = connection.prepareStatement(query.toString());
                ps.setInt(1, headerBean.getHeaderId());
                ps.executeUpdate();
                /************ insert into tasks table ****************/
                for (int i = 0; i < headerBean.getTasksList().size(); i++) {
                    HandoverTasksBean taskBean =headerBean.getTasksList().get(i);
                    query.setLength(0);
                    query.append("insert into " + getSchema_Name() + ".XXX_HANDOVER_TASKS ( " +
                        "HEADER_ID," +
                        "TASK_NAME," +
                        "STATUS," +
                        "DOCUMENT_TYPE )" +
                        " values (?,?,?,?)");
                    ps = connection.prepareStatement(query.toString());
                    ps.setInt(1, headerBean.getHeaderId());
                    ps.setString(2, taskBean.getTaskName());
                    ps.setString(3, taskBean.getStatus());
                    ps.setString(4, taskBean.getDocumentType());
                    ps.executeUpdate();
                } 

            }


        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        handoverDAO obj = new handoverDAO();
        obj.deleteSelfServiceRow(Integer.toString(headerBean.getHeaderId()));
        return headerBean;
    }


    public int getMaxId(Connection connection) {
        int id = 0;
        try {
            String query = null;
            query =
                    "SELECT NVL(MAX(HEADER_ID),0)+1 as NEXT_ID FROM  " + getSchema_Name() +
                    ".XXX_HANDOVER_HEADER";
            AppsproConnection.LOG.info(query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = Integer.parseInt(rs.getString("NEXT_ID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            new AppsProException(e);
        }
        finally {
                    closeResources(connection, ps, rs);
                }
        return id;
    }
    
    public String getMaxHandOverRequest() {
            try {
                connection = AppsproConnection.getConnection();
                String query = null;
                query = "SELECT  MAX(HEADER_ID) AS max_ID FROM  " + " " + getSchema_Name() + ".XXX_HANDOVER_HEADER";
                ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    handoverRequestID = rs.getString("max_ID");
                }
            } catch (Exception e) {
                System.out.println("Error: ");
                e.printStackTrace();
            } finally {
                 closeResources(connection, ps, rs);
            }
            return handoverRequestID;
        }

    public HandoverHeaderBean getHandoverByHeaderId(int headerId){
        
        HandoverHeaderBean headerBean=null;
        connection = AppsproConnection.getConnection();
        try {
            StringBuilder query = new StringBuilder();;
            query.append("select hd.*," +
                "(select value_ar from xx_lookup_value where name = 'SS_STATUS' and code = hd.STATUS) as status_ar," +
                "(select value_en from xx_lookup_value where name = 'SS_STATUS' and code = hd.STATUS) as status_en," +
                "TO_CHAR(hd.CREATION_DATE,'DD/MM/YYYY HH24:MI:SS') as CREATION_DATEF from " + getSchema_Name() +
                ".XXX_HANDOVER_HEADER hd where hd.HEADER_ID=?  order by CREATION_DATE desc");
            ps = connection.prepareStatement(query.toString());
            ps.setInt(1, headerId);
            rs = ps.executeQuery();
            while (rs.next()) {
                headerBean=new HandoverHeaderBean();
                
                headerBean.setCreationDate(rs.getString("CREATION_DATEF"));
                headerBean.setHeaderId(rs.getInt("HEADER_ID"));
                headerBean.setEmployeeId(rs.getString("EMPLOYEE_ID"));
                headerBean.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));;
                headerBean.setStatus(rs.getString("STATUS"));;
                headerBean.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                headerBean.setEmployeeDepartment(rs.getString("EMPLOYEE_DEPARTMENT"));
                headerBean.setEmployeePosition(rs.getString("EMPLOYEE_POSITION"));
                headerBean.setEmployeeNationality(rs.getString("EMPLOYEE_NATIONALITY"));
                headerBean.setEmployeeManager(rs.getString("EMPLOYEE_MANAGER"));
                headerBean.setHandoverTo1(rs.getString("HANDOVER_TO1"));
                headerBean.setHandoverTo2(rs.getString("HANDOVER_TO2"));
                headerBean.setHandoverTo3(rs.getString("HANDOVER_TO3"));
                headerBean.setReason(rs.getString("REASON"));
                headerBean.setNotes(rs.getString("NOTES"));
                headerBean.setHandoverTo1Name(rs.getString("HANDOVER_TO1_NAME"));
                headerBean.setHandoverTo2Name(rs.getString("HANDOVER_TO2_NAME"));
                headerBean.setHandoverTo3Name(rs.getString("HANDOVER_TO3_NAME"));
                headerBean.setRequestCode(rs.getString("REQUEST_CODE"));
                headerBean.setStatusAr(rs.getString("status_ar"));
                headerBean.setStatusEn(rs.getString("status_en"));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
      return headerBean;  
    }
    
    public String deleteSelfServiceRow(String id) {
        int status = 0;

        try {
            connection = AppsproConnection.getConnection();         

            // Delete From Self Service Table
            
            String queryDeleteFromSelfService = "BEGIN\n" +
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XX_SELF_SERVICE\n" +
                "WHERE TRANSACTION_ID = ?;\n" +
                "COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(queryDeleteFromSelfService);

            ps.setString(1, id);

            status = ps.executeUpdate();
            
            // Delete From Approval List Table
            
            String queryDeleteFromApprovalList = "BEGIN\n" +
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XX_APPROVAL_LIST\n" +
                "WHERE TRANSACTION_ID = ?;\n" +
                "COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(queryDeleteFromApprovalList);

            ps.setString(1, id);

            status = ps.executeUpdate();
            
            // Delete From Work Flow Notification Table
            
            String queryDeleteFromWorkFlowNotification = "BEGIN\n" +
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XX_WORKFLOW_NOTIFICATION\n" +
                "WHERE REQUEST_ID = ?;\n" +
                "COMMIT;\n" +
                "END;";

            ps = connection.prepareStatement(queryDeleteFromWorkFlowNotification);

            ps.setString(1, id);

            status = ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(status);

    }
    
    public HandoverHeaderBean updateHandoverStatus(HandoverHeaderBean headerBean) {
        try {
            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();

            
            
                /**************** update header table *******************/
                query.setLength(0);
                query.append("update " + getSchema_Name() + ".XXX_HANDOVER_HEADER set " +
                    "STATUS=NVL(?,STATUS)," +
                    "UPDATE_BY=NVL(?,UPDATE_BY)," +
                    "UPDATED_DATE=sysDate where HEADER_ID=?");
                ps = connection.prepareStatement(query.toString());
                ps.setString(1, headerBean.getStatus());
                ps.setString(2, headerBean.getUpdateBy());
                ps.setInt(3, headerBean.getHeaderId());
                ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return headerBean;
    }
    
    public ArrayList<HandoverHeaderBean> getAllHandoverByStatus(){
        
        ArrayList<HandoverHeaderBean> headerList=new ArrayList<HandoverHeaderBean>();
        connection = AppsproConnection.getConnection();
        try {
            StringBuilder query = new StringBuilder();;
            query.append("select hd.* from " + getSchema_Name() +
                ".XXX_HANDOVER_HEADER hd where hd.STATUS='PENDING_APPROVED' OR hd.STATUS='APPROVED' OR hd.STATUS='Withdraw' order by CREATION_DATE desc");
            ps = connection.prepareStatement(query.toString());
           // ps.setInt(1, headerId);
            rs = ps.executeQuery();
            while (rs.next()) {
                HandoverHeaderBean headerBean=new HandoverHeaderBean();
                
                headerBean.setCreationDate(rs.getString("CREATION_DATE"));
                headerBean.setHeaderId(rs.getInt("HEADER_ID"));
                headerBean.setEmployeeId(rs.getString("EMPLOYEE_ID"));
                headerBean.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));
                headerBean.setStatus(rs.getString("STATUS"));
                headerBean.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                headerBean.setRequestCode(rs.getString("REQUEST_CODE"));
                
                headerList.add(headerBean);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
      return headerList;  
    }
}