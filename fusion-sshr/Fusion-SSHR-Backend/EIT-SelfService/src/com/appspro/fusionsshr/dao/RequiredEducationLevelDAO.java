/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

//import com.appspro.db.ConnectionPoolNew;
import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.RequiredEducationLevelBean;
import com.appspro.fusionsshr.bean.RequiredQualificationBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author amro
 */
public class RequiredEducationLevelDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public RequiredEducationLevelBean insertRequiredEducationLevel(RequiredEducationLevelBean bean, String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }
            if (transactionType.equals("ADD")) {
                if (bean.getUpdatedDate() != null) {
                    bean.setUpdatedDate(rh.convertToGregorian(bean.getUpdatedDate()));
                } else {
                    bean.setUpdatedDate((bean.getUpdatedDate()));
                }
                String query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL (START_DATE,END_DATE,REQUIRED_EDUCATION_LEVEL,FLAG,JOB_NAME ,CREATEDBY,CREATEDDATE) \n"
                        + "                       VALUES (? ,? , ? ,?,? ,?,?)";

                ps = connection.prepareStatement(query);
//                ps.setString(1, bean.getId());
                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getRequiredEducationLevel());
                ps.setString(4, bean.getFlag());
                ps.setString(5, bean.getJobName());
                ps.setString(6, bean.getCreatedBy());
                ps.setDate(7, getSQLDateFromString(bean.getCreationDate()));
//                ps.setString(9, bean.getUpdatedBy());
//                ps.setDate(10, getSQLDateFromString(bean.getUpdatedDate()));
                ps.executeUpdate();
                ArrayList<RequiredEducationLevelBean> allowanceBeansList = getMaxID();
                UpdateEducationLevelCode(allowanceBeansList.get(allowanceBeansList.size() - 1));
            } else if (transactionType.equals("EDIT")) {
                String oldFlag = "N";
                String query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL \n"
                        + "SET  UPDATEDATE =sysdate \n"
                        + ",END_DATE = sysdate "
                        + ", UPDATEBY = ? "
                        + ",FLAG = ?"
                        + "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getUpdatedBy());
                ps.setString(2, oldFlag);
                ps.setInt(3, Integer.parseInt(bean.getId()));
                ps.executeUpdate();

                query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL (START_DATE,END_DATE,REQUIRED_EDUCATION_LEVEL,FLAG,JOB_NAME ,CREATEDBY,CREATEDDATE,UPDATEBY,UPDATEDATE,CODE) \n"
                        + "                       VALUES ( ?, ? ,? , ? ,?,? ,?,?,?,?)";

                ps = connection.prepareStatement(query);

                // ps.setString(1, bean.getId());
                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getRequiredEducationLevel());
                ps.setString(4, bean.getFlag());
                ps.setString(5, bean.getJobName());
                ps.setString(6, bean.getCreatedBy());
                ps.setDate(7, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(8, bean.getUpdatedBy());
                ps.setDate(9, getSQLDateFromString(bean.getUpdatedDate()));
                ps.setString(10, bean.getCode());
                ps.executeUpdate();

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<RequiredEducationLevelBean> getAllRequiredEducationLevel() {
        ArrayList<RequiredEducationLevelBean> educationLevelBeansList = new ArrayList<RequiredEducationLevelBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = " select  * from  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL ";

            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                RequiredEducationLevelBean bean = new RequiredEducationLevelBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setJobName(rs.getString("JOB_NAME"));
                bean.setRequiredEducationLevel(rs.getString("REQUIRED_EDUCATION_LEVEL"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    bean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    bean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    bean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    bean.setStartDate((rs.getString("START_DATE")));
                }//end 

                educationLevelBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return educationLevelBeansList;
    }

    public ArrayList<RequiredEducationLevelBean> getAllRequiredEducationLevelByJobName(String jobName) {
        ArrayList<RequiredEducationLevelBean> educationLevelBeansList = new ArrayList<RequiredEducationLevelBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = " select  * from  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL "
                    + "where JOB_NAME=? "
                    + "AND FLAG=?";

            ps = connection.prepareStatement(query);
            ps.setString(1, jobName);
            ps.setString(2, "Y");
            rs = ps.executeQuery();
            while (rs.next()) {
                RequiredEducationLevelBean bean = new RequiredEducationLevelBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setJobName(rs.getString("JOB_NAME"));
                bean.setRequiredEducationLevel(rs.getString("REQUIRED_EDUCATION_LEVEL"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                bean.setCode(rs.getString("CODE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    bean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    bean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    bean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    bean.setStartDate((rs.getString("START_DATE")));
                }//end 

                educationLevelBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return educationLevelBeansList;
    }

    public ArrayList<RequiredEducationLevelBean> getMaxID() {
        ArrayList<RequiredEducationLevelBean> educationLevelBeansList = new ArrayList<RequiredEducationLevelBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "    select  * from  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL  \n"
                    + "                WHERE ID = (SELECT MAX(ID) from  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL)";

            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                RequiredEducationLevelBean bean = new RequiredEducationLevelBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setEndDate(rs.getString("END_DATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setJobName(rs.getString("JOB_NAME"));
                bean.setStartDate(rs.getString("START_DATE"));
                bean.setRequiredEducationLevel(rs.getString("REQUIRED_EDUCATION_LEVEL"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                bean.setCode(rs.getString("CODE"));

                educationLevelBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return educationLevelBeansList;

    }

    public RequiredEducationLevelBean UpdateEducationLevelCode(RequiredEducationLevelBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String code = bean.getId() + bean.getJobName();
            String query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL \n"
                    + "SET CODE = ? "
                    + "WHERE id = ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            ps.setInt(2, Integer.parseInt(bean.getId()));
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

    public ArrayList<RequiredEducationLevelBean> getAllRequiredEducationLevelByCode(String code) {
        ArrayList<RequiredEducationLevelBean> educationLevelBeansList = new ArrayList<RequiredEducationLevelBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = " select  * from  "+ " " + getSchema_Name() + ".XXX_REQUIRED_EDUCATION_LEVEL "
                    + "where CODE=? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            rs = ps.executeQuery();
            while (rs.next()) {
                RequiredEducationLevelBean bean = new RequiredEducationLevelBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setEndDate(rs.getString("END_DATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setJobName(rs.getString("JOB_NAME"));
                bean.setStartDate(rs.getString("START_DATE"));
                bean.setRequiredEducationLevel(rs.getString("REQUIRED_EDUCATION_LEVEL"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                bean.setCode(rs.getString("CODE"));

                educationLevelBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }
        return educationLevelBeansList;
    }

}
