/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.LineSourceElementEntryBean;

import com.appspro.fusionsshr.bean.TrackServerRequestBean;
import static common.restHelper.RestHelper.getSchema_Name;
import common.restHelper.RestHelper;

import java.io.IOException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Date;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Shadi Mansi-PC
 */
public class ElementEntryHelper {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    private static String bulkElementEnteryHeader = "", bulkElementEnteryBody =
        "";
    private static ElementEntryHelper instance = null;

    public String createHelperBT(String path, JSONObject jsonObj,
                                 EmployeeBean emp) {
        String resp = "Service Failed.";
        String filename = "ElementEntry";
        List<LineSourceElementEntryBean> declareVariableList =
            new ArrayList<LineSourceElementEntryBean>();
        List<LineSourceElementEntryBean> declareLineDataList =
            new ArrayList<LineSourceElementEntryBean>();
        LineSourceElementEntryBean declareVariable = null;
        try {
            //            //(jsonObj);
            String sourceSystemOwner = "TBC";
            String sourceSystemId = jsonObj.getString("sourceSystemId");
            String EffectiveStartDate = "%EffectiveStartDate%";
            String StartDate = jsonObj.opt("effivtiveStartDate").toString();
            
            
            String EffectiveEndDate = "%EffectiveEndDate%";
            String ElementName = jsonObj.getString("elementName");
            String LegislativeDataGroupName =
                jsonObj.getString("legislativeDataGroupName");
            String AssignmentNumber = jsonObj.getString("assignmentNumber");
            String EntryType = jsonObj.getString("entryType");
            String CreatorType = jsonObj.getString("creatorType");
            String ssType = jsonObj.getString("SSType");
            String personNum = jsonObj.getString("personNumber");
            String eitCode = jsonObj.getString("eitCode");
            //
            String lineSourceSystemOwner = sourceSystemOwner;
            String lineElementEntryId = sourceSystemId;
            String lineEffectiveStartDate = EffectiveStartDate;
            String lineEffectiveEndDate = EffectiveEndDate;
            String lineElementName = ElementName;
            String lineLegislativeDataGroupName = LegislativeDataGroupName;
            String lineAssignmentNumber = AssignmentNumber;
            String trsId = jsonObj.get("trsId").toString();
            String inputValues = jsonObj.get("inputValues").toString();

            JSONArray jsonArr = new JSONArray(inputValues);
            int countOfInputValue = jsonArr.length();


            JSONObject jsonObjInput;
            for (int i = 0; i < jsonArr.length(); i++) {
                declareVariable = new LineSourceElementEntryBean();
                jsonObjInput = jsonArr.getJSONObject(i);
                declareVariable.setLineSourceSystemId(personNum + "_" +
                                                      eitCode + "_" + trsId +
                                                      "_" +
                                                      jsonObjInput.get("inputValueLbl").toString().replaceAll(" ",
                                                                                                              "_"));
                declareVariable.setLineInputValueName(jsonObjInput.get("inputValueLbl").toString());
                declareVariable.setLineScreenEntryValue((jsonObjInput.isNull("inputEitValue") ?
                                                         "" :
                                                         jsonObjInput.get("inputEitValue").toString()));
                declareVariableList.add(declareVariable);
            }
            for (LineSourceElementEntryBean lineSourceElementEntryBean :
                 declareVariableList) {
                declareVariable = new LineSourceElementEntryBean();
                //(lineSourceElementEntryBean.getLineScreenEntryValue());
                declareVariable.setLineData("MERGE|ElementEntryValue|" +
                                            lineSourceSystemOwner + "|" +
                                            lineSourceElementEntryBean.getLineSourceSystemId() +
                                            "|" + lineElementEntryId + "|" +
                                            lineEffectiveStartDate + "|" +
                                            lineEffectiveEndDate + "|" +
                                            lineElementName + "|" +
                                            lineLegislativeDataGroupName +
                                            "|" + lineAssignmentNumber + "|" +
                                            lineSourceElementEntryBean.getLineInputValueName() +
                                            "|" +
                                            lineSourceElementEntryBean.getLineScreenEntryValue());
                if (!lineSourceElementEntryBean.getLineScreenEntryValue().isEmpty()) {
                    declareLineDataList.add(declareVariable);
                }

                //System.out.write(lineSourceElementEntryBean.getLineScreenEntryValue());
            }

            //


            String headerdata =
                "MERGE|ElementEntry|" + sourceSystemOwner + "|" +
                sourceSystemId + "|" + EffectiveStartDate + "|" +
                EffectiveEndDate + "|" + ElementName + "|" +
                LegislativeDataGroupName + "|" + AssignmentNumber + "|" +
                EntryType + "|" + CreatorType;


            String toBytes = HCMElementEntryHelper.ELEMENT_HEADER + "\n" +
                headerdata + "\n" +
                HCMElementEntryHelper.ELEMENT_LINE + "\n";


            String fileContent =
                HCMElementEntryHelper.ELEMENT_HEADER + "\r\n" +
                headerdata + "\r\n" +
                HCMElementEntryHelper.ELEMENT_LINE + "\r\n";

            for (LineSourceElementEntryBean lineSourceElementEntryBean :
                 declareLineDataList) {
                toBytes += lineSourceElementEntryBean.getLineData() + "\n";
                fileContent += lineSourceElementEntryBean.getLineData() + "\n";

            }
            resp =
HCMElementEntryHelper.uploadAndLoadFile(path, fileContent, toBytes, trsId,
                                        ssType, personNum, filename, eitCode,
                                        emp);
            
            callUCM(trsId, StartDate, null, ssType, toBytes, filename);

        }  catch (IOException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            TrackServerRequestBean bean = new TrackServerRequestBean();
            bean.setRequestId(jsonObj.get("trsId").toString());
            bean.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
            bean.setElementResponseCode(500 + "");
            bean.setElementResponseDesc(e.getMessage());
            EFFDetails.getInstance().editElementResponseDetails(bean);
        }

        return resp;
    }
    
    
    
    //------- Submitted elements from PaaS -> Element Entry Observation Screen -------
    public String createElementFromPaaS(JSONObject jsonObj) {
        String resp = "Service Failed.";
        
        try {

            String startDate="%EffectiveStartDate%";
            String filePath=jsonObj.getString("filePath");
            String ucmId=jsonObj.getString("ucmId");
            String fileContent=jsonObj.getString("fileContent");
            String filename =jsonObj.getString("fileName");
            String ssType =jsonObj.getString("ssType");
            String ssId =jsonObj.getString("ssId");
            String personNum = jsonObj.getString("personNumber");
            String eitCode = jsonObj.getString("eitCode");
            
            resp = HCMElementEntryHelper.uploadAndLoadFile(filePath, fileContent, fileContent, ssId,
                                                            ssType, personNum, filename, eitCode, ucmId, null);
            
            callUCM(ssId, startDate, null, ssType, fileContent, filename);

        }  catch (IOException e) {
            e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            TrackServerRequestBean bean = new TrackServerRequestBean();
            bean.setRequestId(jsonObj.get("trsId").toString());
            bean.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
            bean.setElementResponseCode(500 + "");
            bean.setElementResponseDesc(e.getMessage());
            EFFDetails.getInstance().editElementResponseDetails(bean);
        }

        return resp;
    }
    
    
    //---------------------------------------Bulk Element --------------------------------
    public String createHelperBulkElement(String path, JSONObject jsonObj,
                                 EmployeeBean emp) {
        String resp = "Service Failed.";
        String filename = "ElementEntry";
        
        try {
            //            //(jsonObj);

            String StartDate = jsonObj.opt("effivtiveStartDate").toString();
            
            String fileContent=jsonObj.getString("DAT");
            String toBytes =jsonObj.getString("DAT");
            String ssType = "Attach Element";
            String personNum = jsonObj.getString("personNumber");
            String eitCode = jsonObj.getString("eitCode");

//            String toBytes = "";
//                HCMElementEntryHelper.ELEMENT_HEADER + "\n" +
//                headerdata + "\n" +
//                HCMElementEntryHelper.ELEMENT_LINE + "\n";


//            String fileContent ="";
//                HCMElementEntryHelper.ELEMENT_HEADER + "\r\n" +
//                headerdata + "\r\n" +
//                HCMElementEntryHelper.ELEMENT_LINE + "\r\n";


            resp =
    HCMElementEntryHelper.uploadAndLoadFile(path, fileContent, toBytes, "",
                                        ssType, personNum, filename, eitCode,
                                        emp);
            
            callUCM("", StartDate, null, ssType, toBytes, filename);

        }  catch (IOException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            TrackServerRequestBean bean = new TrackServerRequestBean();
            bean.setRequestId(jsonObj.get("trsId").toString());
            bean.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
            bean.setElementResponseCode(500 + "");
            bean.setElementResponseDesc(e.getMessage());
            EFFDetails.getInstance().editElementResponseDetails(bean);
        }

        return resp;
    }
    //----------------------------------------------End-------------------------------------

    protected String createHelperBankFile(String path, JSONObject jsonObj) {
        String resp = "Service Failed.";
        String filename = "ExternalBankAccount";

        try {
            String trsId = jsonObj.getString("trsId");


            String bankName = jsonObj.getString("bankName");
            String bankBranchName = jsonObj.getString("bankBranchName");
            String accountNumber = jsonObj.getString("accountNumber");
            String countryCode = "SA";
            String currencyCode = "SAR";
            String IBAN = jsonObj.getString("IBAN");
            String inactiveFlag = "N";
            String linedata1 =
                "MERGE|ExternalBankAccount|" + bankName + "|" + bankBranchName +
                "|" + accountNumber + "|" + countryCode + "|" + currencyCode +
                "|" + IBAN + "|" + inactiveFlag;
            //---------------New Bank -----------------------------------
            String personNumber = jsonObj.getString("personNumber");
            String newBankName = jsonObj.getString("newBankName");
            String newBankBranchName = jsonObj.getString("newBankBranchName");
            String newAccountNumber = jsonObj.getString("newAccountNumber");
            // String bankName = jsonObj.getString("sourceSystemId");
            //  String bankName = jsonObj.getString("sourceSystemId");
            String newPrimaryFlag = "Y";
            String linedata2 =
                "MERGE|ExternalBankAccountOwner|" + personNumber + "|" +
                newBankName + "|" + newBankBranchName + "|" +
                newAccountNumber + "|" + countryCode + "|" + currencyCode +
                "|" + newPrimaryFlag;
            //End


            String toBytes =
                HCMElementEntryHelper.ELEMENT_HEDER_EXTERNAL_BANK_ACCOUNT +
                "\n" +
                linedata1 + "\n" +
                HCMElementEntryHelper.ELEMENT_HEDER_External_Bank_Account_Owner +
                "\n" +
                linedata2;


            resp =
HCMElementEntryHelper.uploadAndLoadFile(path, toBytes, toBytes, trsId,
                                        "XXX_HR_PERSON_PAY_METHOD",
                                        personNumber, filename,
                                        "XXX_HR_PERSON_PAY_METHOD", null);
            
            callUCM(null, "2019-12-31", null, "XXX_HR_PERSON_PAY_METHOD",toBytes, filename);
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return resp;
    }

    protected String createHelperPersonalPaymentMethod(String path,
                                                       JSONObject jsonObj) {
        String resp = "Service Failed.";
        String filename = "PersonalPaymentMethod";

        try {
            String trsId = jsonObj.getString("trsId");
            String personNumber = jsonObj.getString("personNumber");

            String legislativeDataGroupName =
                jsonObj.getString("legislativeDataGroupName");
            String assignmentNumber = jsonObj.getString("assignmentNumber");
            String personalPaymentMethodCode =
                jsonObj.getString("personalPaymentMethodCode");
            String effectiveStartDate =
                jsonObj.getString("effectiveStartDate");
            String paymentAmountType = jsonObj.getString("paymentAmountType");
            String amount = "";
            String processingOrder = jsonObj.getString("processingOrder");
            String organizationPaymentMethodCode =
                jsonObj.getString("organizationPaymentMethodCode");
            String percentage = jsonObj.getString("percentage");
            String bankName = jsonObj.getString("bankName");
            String bankBranchName = jsonObj.getString("bankBranchName");
            String bankCountryCode = "SA";
            String bankAccountNumber = jsonObj.getString("bankAccountNumber");
            String linedata1 =
                "MERGE|PersonalPaymentMethod|" + legislativeDataGroupName +
                "|" + assignmentNumber + "|" + personalPaymentMethodCode +
                "|" + effectiveStartDate + "|" + paymentAmountType + "|" +
                amount + "|" + processingOrder + "|" +
                organizationPaymentMethodCode + "|" + percentage + "|" +
                bankName + "|" + bankBranchName + "|" + bankCountryCode + "|" +
                bankAccountNumber;


            String toBytes =
                HCMElementEntryHelper.ELEMENT_HEDER_PERSONAL_PAYMENT_METHOD +
                "\n" +
                linedata1;


            resp =
HCMElementEntryHelper.uploadAndLoadFile("  ", toBytes, toBytes, trsId,
                                        "XXX_PAYMENT", personNumber, filename,
                                        "XXX_HR_PERSON_PAY_METHOD", null);

            callUCM(null, "2019-12-31", null, "XXX_HR_PERSON_PAY_METHOD",
                    toBytes, filename);


        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
        return resp;
    }

    protected boolean callUCM(String hdlId, String startDate, String path,
                             String sstype, String file_bytes,
                             String file_name) {
        return HCMElementEntryHelper.CALL_UCM(hdlId, startDate, path, sstype,
                                              file_bytes, file_name);
    }

    private String updatePeopleGroup(String oldPG, String allowanceType,
                                     String allowanceAmount) {
        String[] ar = oldPG.split("[.]", -1);
        Map<String, String> PEOPLE_GROUP = new HashMap<String, String>();

        PEOPLE_GROUP.put("Housing Type", ar[0]);
        PEOPLE_GROUP.put("Override Housing Percentage", ar[1]);
        PEOPLE_GROUP.put("Transportation Type", ar[2]);
        PEOPLE_GROUP.put("Override Transportation Percentage", ar[3]);
        PEOPLE_GROUP.put("Override Transportation Amount", ar[4]);
        PEOPLE_GROUP.put("Food Allowance", ar[5]);
        PEOPLE_GROUP.put("Field Work Allowance", ar[6]);
        PEOPLE_GROUP.put("Work Nature Allowance", ar[7]);
        PEOPLE_GROUP.put("Override Work Nature Allowance Amount", ar[8]);
        PEOPLE_GROUP.put("Duty Allowance", ar[9]);
        PEOPLE_GROUP.put("Duty Allowance Amount", ar[10]);
        PEOPLE_GROUP.put("Mobile Allowance", ar[11]);
        PEOPLE_GROUP.put("Mobile Allowance Amount", ar[12]);
        PEOPLE_GROUP.put("Car Allowance", ar[13]);
        PEOPLE_GROUP.put("Car Allowance Amount", ar[14]);
        PEOPLE_GROUP.put("Pension Allowance", ar[15]);
        PEOPLE_GROUP.put("Hardship Allowance", ar[16]);
        PEOPLE_GROUP.put("Hardship Allowance Amount", ar[17]);
        PEOPLE_GROUP.put("Motivation Allowance Flag", ar[18]);
        PEOPLE_GROUP.put("Override Vacation Entitlement", ar[18]);


        if ("carAllowance".equalsIgnoreCase(allowanceType)) {
            PEOPLE_GROUP.put("Car Allowance", "Y");
            PEOPLE_GROUP.put("Car Allowance Amount", allowanceAmount);
        } else if ("mobileAllowance".equalsIgnoreCase(allowanceType)) {
            PEOPLE_GROUP.put("Mobile Allowance", "Y");
            PEOPLE_GROUP.put("Mobile Allowance Amount", allowanceAmount);
        } else if ("transportationAlowance".equalsIgnoreCase(allowanceType)) {
            PEOPLE_GROUP.put("Override Transportation Amount",
                             allowanceAmount);
        }

        return PEOPLE_GROUP.get("Housing Type") + "." +
            PEOPLE_GROUP.get("Override Housing Percentage") + "." +
            PEOPLE_GROUP.get("Transportation Type") + "." +
            PEOPLE_GROUP.get("Override Transportation Percentage") + "." +
            PEOPLE_GROUP.get("Override Transportation Amount") + "." +
            PEOPLE_GROUP.get("Food Allowance") + "." +
            PEOPLE_GROUP.get("Field Work Allowance") + "." +
            PEOPLE_GROUP.get("Work Nature Allowance") + "." +
            PEOPLE_GROUP.get("Override Work Nature Allowance Amount") + "." +
            PEOPLE_GROUP.get("Duty Allowance") + "." +
            PEOPLE_GROUP.get("Duty Allowance Amount") + "." +
            PEOPLE_GROUP.get("Mobile Allowance") + "." +
            PEOPLE_GROUP.get("Mobile Allowance Amount") + "." +
            PEOPLE_GROUP.get("Car Allowance") + "." +
            PEOPLE_GROUP.get("Car Allowance Amount") + "." +
            PEOPLE_GROUP.get("Pension Allowance") + "." +
            PEOPLE_GROUP.get("Hardship Allowance") + "." +
            PEOPLE_GROUP.get("Hardship Allowance Amount") + "." +
            PEOPLE_GROUP.get("Motivation Allowance Flag") + "." +
            PEOPLE_GROUP.get("Override Vacation Entitlement");
    }
    //update for STA

    private String updatePeopleGroupSTA(String oldPG, String allowanceType) {
        String[] ar = oldPG.split("[.]", -1);
        Map<String, String> PEOPLE_GROUP = new HashMap<String, String>();

        PEOPLE_GROUP.put("Housing Type", ar[0]);
        PEOPLE_GROUP.put("Override Housing Percentage", ar[1]);
        PEOPLE_GROUP.put("Transportation Type", ar[2]);
        PEOPLE_GROUP.put("Override Transportation Percentage", ar[3]);
        PEOPLE_GROUP.put("Override Transportation Amount", ar[4]);
        PEOPLE_GROUP.put("Food Allowance", ar[5]);
        PEOPLE_GROUP.put("Field Work Allowance", ar[6]);
        PEOPLE_GROUP.put("Work Nature Allowance", ar[7]);
        PEOPLE_GROUP.put("Override Work Nature Allowance Amount", ar[8]);
        PEOPLE_GROUP.put("Duty Allowance", ar[9]);
        PEOPLE_GROUP.put("Duty Allowance Amount", ar[10]);
        PEOPLE_GROUP.put("Mobile Allowance", ar[11]);
        PEOPLE_GROUP.put("Mobile Allowance Amount", ar[12]);
        PEOPLE_GROUP.put("Car Allowance", ar[13]);
        PEOPLE_GROUP.put("Car Allowance Amount", ar[14]);
        PEOPLE_GROUP.put("Pension Allowance", ar[15]);
        PEOPLE_GROUP.put("Hardship Allowance", ar[16]);
        PEOPLE_GROUP.put("Hardship Allowance Amount", ar[17]);
        PEOPLE_GROUP.put("Motivation Allowance Flag", ar[18]);
        PEOPLE_GROUP.put("Override Vacation Entitlement", ar[18]);


        if ("carAllowance".equalsIgnoreCase(allowanceType)) {
            PEOPLE_GROUP.put("Car Allowance", "");
            PEOPLE_GROUP.put("Car Allowance Amount", "");
        } else if ("mobileAllowance".equalsIgnoreCase(allowanceType)) {
            PEOPLE_GROUP.put("Mobile Allowance", "");
            PEOPLE_GROUP.put("Mobile Allowance Amount", "");
        } else if ("transportationAlowance".equalsIgnoreCase(allowanceType)) {
            PEOPLE_GROUP.put("Transportation Type", "");
            PEOPLE_GROUP.put("Override Transportation Amount", "");
        }

        return PEOPLE_GROUP.get("Housing Type") + "." +
            PEOPLE_GROUP.get("Override Housing Percentage") + "." +
            PEOPLE_GROUP.get("Transportation Type") + "." +
            PEOPLE_GROUP.get("Override Transportation Percentage") + "." +
            PEOPLE_GROUP.get("Override Transportation Amount") + "." +
            PEOPLE_GROUP.get("Food Allowance") + "." +
            PEOPLE_GROUP.get("Field Work Allowance") + "." +
            PEOPLE_GROUP.get("Work Nature Allowance") + "." +
            PEOPLE_GROUP.get("Override Work Nature Allowance Amount") + "." +
            PEOPLE_GROUP.get("Duty Allowance") + "." +
            PEOPLE_GROUP.get("Duty Allowance Amount") + "." +
            PEOPLE_GROUP.get("Mobile Allowance") + "." +
            PEOPLE_GROUP.get("Mobile Allowance Amount") + "." +
            PEOPLE_GROUP.get("Car Allowance") + "." +
            PEOPLE_GROUP.get("Car Allowance Amount") + "." +
            PEOPLE_GROUP.get("Pension Allowance") + "." +
            PEOPLE_GROUP.get("Hardship Allowance") + "." +
            PEOPLE_GROUP.get("Hardship Allowance Amount") + "." +
            PEOPLE_GROUP.get("Motivation Allowance Flag") + "." +
            PEOPLE_GROUP.get("Override Vacation Entitlement");
    }
    //update for change housing type

    private String updatePeopleGroupCHT(String oldPG, String housingType) {
        String[] ar = oldPG.split("[.]", -1);
        Map<String, String> PEOPLE_GROUP = new HashMap<String, String>();
        PEOPLE_GROUP.put("Housing Type", housingType);
        PEOPLE_GROUP.put("Override Housing Percentage", ar[1]);
        PEOPLE_GROUP.put("Transportation Type", ar[2]);
        PEOPLE_GROUP.put("Override Transportation Percentage", ar[3]);
        PEOPLE_GROUP.put("Override Transportation Amount", ar[4]);
        PEOPLE_GROUP.put("Food Allowance", ar[5]);
        PEOPLE_GROUP.put("Field Work Allowance", ar[6]);
        PEOPLE_GROUP.put("Work Nature Allowance", ar[7]);
        PEOPLE_GROUP.put("Override Work Nature Allowance Amount", ar[8]);
        PEOPLE_GROUP.put("Duty Allowance", ar[9]);
        PEOPLE_GROUP.put("Duty Allowance Amount", ar[10]);
        PEOPLE_GROUP.put("Mobile Allowance", ar[11]);
        PEOPLE_GROUP.put("Mobile Allowance Amount", ar[12]);
        PEOPLE_GROUP.put("Car Allowance", ar[13]);
        PEOPLE_GROUP.put("Car Allowance Amount", ar[14]);
        PEOPLE_GROUP.put("Pension Allowance", ar[15]);
        PEOPLE_GROUP.put("Hardship Allowance", ar[16]);
        PEOPLE_GROUP.put("Hardship Allowance Amount", ar[17]);
        PEOPLE_GROUP.put("Motivation Allowance Flag", ar[18]);
        PEOPLE_GROUP.put("Override Vacation Entitlement", ar[18]);

        return PEOPLE_GROUP.get("Housing Type") + "." +
            PEOPLE_GROUP.get("Override Housing Percentage") + "." +
            PEOPLE_GROUP.get("Transportation Type") + "." +
            PEOPLE_GROUP.get("Override Transportation Percentage") + "." +
            PEOPLE_GROUP.get("Override Transportation Amount") + "." +
            PEOPLE_GROUP.get("Food Allowance") + "." +
            PEOPLE_GROUP.get("Field Work Allowance") + "." +
            PEOPLE_GROUP.get("Work Nature Allowance") + "." +
            PEOPLE_GROUP.get("Override Work Nature Allowance Amount") + "." +
            PEOPLE_GROUP.get("Duty Allowance") + "." +
            PEOPLE_GROUP.get("Duty Allowance Amount") + "." +
            PEOPLE_GROUP.get("Mobile Allowance") + "." +
            PEOPLE_GROUP.get("Mobile Allowance Amount") + "." +
            PEOPLE_GROUP.get("Car Allowance") + "." +
            PEOPLE_GROUP.get("Car Allowance Amount") + "." +
            PEOPLE_GROUP.get("Pension Allowance") + "." +
            PEOPLE_GROUP.get("Hardship Allowance") + "." +
            PEOPLE_GROUP.get("Hardship Allowance Amount") + "." +
            PEOPLE_GROUP.get("Motivation Allowance Flag") + "." +
            PEOPLE_GROUP.get("Override Vacation Entitlement");
    }
    // ----------------------create Education Allowance element entry --------------------//

    protected String createEducationAllowanceFile(String path,
                                                  JSONObject jsonObj) {
        String resp = "Service Failed.";
        String filename = "ElementEntry";

        try {
            ExpenseDAO services = new ExpenseDAO();
            String multipleEntryCount = services.MultipleEntryCountSequence();
            String entryType = jsonObj.getString("entryType");
            String creatorType = jsonObj.getString("creatorType");
            String assignmentNumber = jsonObj.getString("assignmentNumber");
            String effectiveStartDate =
                jsonObj.getString("effectiveStartDate");
            String effectiveEndDate = jsonObj.getString("effectiveEndDate");
            String sourceSystemOwner = jsonObj.getString("sourceSystemOwner");
            String legislativeDataGroupName =
                jsonObj.getString("legislativeDataGroupName");
            int amount = jsonObj.getInt("amount");
            String childrenNumber = jsonObj.getString("childrenNumber");
            String sourceSystemId = jsonObj.getString("SourceSystemId");
            String personNumber = jsonObj.get("personNumber").toString();
            String requestDetailsId =
                jsonObj.get("requestDetailsId").toString();


            String linedata1 =
                "MERGE|ElementEntry|Education Allowance|" + entryType + "|" +
                creatorType + "|" + assignmentNumber + "|" +
                legislativeDataGroupName + "|" + effectiveStartDate + "|" +
                effectiveEndDate + "|" + sourceSystemOwner + "|" +
                multipleEntryCount + "_" + sourceSystemId;

            String linedata2 =
                "MERGE|ElementEntryValue|Education Allowance|" + multipleEntryCount +
                "_" + sourceSystemId + "|" + assignmentNumber + "|" +
                legislativeDataGroupName + "|" + "Amount" + "|" +
                effectiveStartDate + "|" + effectiveEndDate + "|" + amount +
                "|" + sourceSystemOwner + "|" + multipleEntryCount + "_" +
                sourceSystemId + "_INPUT1" + "\n" +
                "MERGE|ElementEntryValue|Education Allowance|" +
                multipleEntryCount + "_" + sourceSystemId + "|" +
                assignmentNumber + "|" + legislativeDataGroupName + "|" +
                "Number of Children" + "|" + effectiveStartDate + "|" +
                effectiveEndDate + "|" + childrenNumber + "|" +
                sourceSystemOwner + "|" + multipleEntryCount + "_" +
                sourceSystemId + "_INPUT2";


            String toBytes =
                HCMElementEntryHelper.ELEMENT_HEDER_Education_Allowance +
                "\n" +
                linedata1 + "\n" +
                HCMElementEntryHelper.ELEMENT_HEDER_Education_Allowance_VALUE +
                "\n" +
                linedata2;


            //            resp =
            HCMElementEntryHelper.uploadAndLoadFile("  ", toBytes, toBytes,
                                                    requestDetailsId,
                                                    "XXX_EXPENSE_REQUEST",
                                                    personNumber, filename,
                                                    "XXX_EXPENSE_REQUEST",
                                                    null);
            //
            callUCM(null, "2019-12-31", null, "XXX_EXPENSE_REQUEST", toBytes,
                    filename);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    public static ElementEntryHelper getInstance() {
        if (instance == null)
            instance = new ElementEntryHelper();
        bulkElementEnteryHeader = "";
        bulkElementEnteryBody = "";
        return instance;
    }
}
