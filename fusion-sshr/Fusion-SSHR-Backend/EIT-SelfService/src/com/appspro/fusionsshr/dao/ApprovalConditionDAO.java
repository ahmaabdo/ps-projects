package com.appspro.fusionsshr.dao;
import com.appspro.db.AppsproConnection;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalConditionBean;
import com.appspro.fusionsshr.bean.RoleSetUpBean;
import com.appspro.fusionsshr.bean.SummaryReportBean;

import com.appspro.fusionsshr.bean.roleSetupValidationBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
public class ApprovalConditionDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
   
   
    public ArrayList<ApprovalConditionBean> getAllApprovalCondition() {

     ArrayList<ApprovalConditionBean> approvalConditionList =
         new ArrayList<ApprovalConditionBean>();
     try {
         connection = AppsproConnection.getConnection();
         String query =
             "SELECT * FROM "+" "+getSchema_Name()+".XXX_APPROVAL_CONDITION";
         ps = connection.prepareStatement(query);
         rs = ps.executeQuery();
         while (rs.next()) {
             ApprovalConditionBean approvalConditionBean = new ApprovalConditionBean();
             approvalConditionBean.setId(rs.getString("ID"));
             approvalConditionBean.setEitCode(rs.getString("EIT_CODE"));
             approvalConditionBean.setFirstKeyType(rs.getString("FRIST_KEY_TYPE"));
             approvalConditionBean.setFirstKeyValue(rs.getString("FRIST_KEY_VALUE"));
             approvalConditionBean.setFirstEitSegment(rs.getString("FRIST_EIT_SEGMENT"));
             approvalConditionBean.setOperation(rs.getString("OPERATION"));
             approvalConditionBean.setSecondKeyType(rs.getString("SECOND_KEY_TYPE"));
             approvalConditionBean.setSecondKeyValue(rs.getString("SECOND_KEY_VALUE"));
             approvalConditionBean.setSecondEitSegment(rs.getString("SECOND_EIT_SEGMENT"));
             approvalConditionBean.setApprovalCode(rs.getString("APPROVAL_CODE"));
             
             
             approvalConditionList.add(approvalConditionBean);

         }

     } catch (Exception e) {
         //("Error: ");
        e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
     } finally {
         closeResources(connection, ps, rs);
     }
     return approvalConditionList;

    }
    
    
    public ApprovalConditionBean addApprovalCondition(ApprovalConditionBean approvalCodition) {
    
        try {
            connection = AppsproConnection.getConnection();
                
                String query = "INSERT INTO  " + " " + getSchema_Name() + ".XXX_APPROVAL_CONDITION (EIT_CODE,FRIST_KEY_TYPE,FRIST_KEY_VALUE,FRIST_EIT_SEGMENT,OPERATION,SECOND_KEY_TYPE,\n" + 
                "SECOND_KEY_VALUE,SECOND_EIT_SEGMENT,APPROVAL_CODE)VALUES(?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, approvalCodition.getEitCode());
                ps.setString(2, approvalCodition.getFirstKeyType());
                ps.setString(3, approvalCodition.getFirstKeyValue());
                ps.setString(4, approvalCodition.getFirstEitSegment());
                ps.setString(5, approvalCodition.getOperation());
                ps.setString(6, approvalCodition.getSecondKeyType());
                ps.setString(7, approvalCodition.getSecondKeyValue());
                ps.setString(8, approvalCodition.getSecondEitSegment());
                ps.setString(9, approvalCodition.getApprovalCode());
                ps.executeUpdate();

            


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalCodition;
    }
    
    public ApprovalConditionBean updateApprovalCondition(ApprovalConditionBean approvalCodition) {
    
        try {
            connection = AppsproConnection.getConnection();
                
                String query = "update  " + " " + getSchema_Name() + ".XXX_APPROVAL_CONDITION set EIT_CODE=? ,FRIST_KEY_TYPE=?,FRIST_KEY_VALUE=?,FRIST_EIT_SEGMENT=?,OPERATION=?,SECOND_KEY_TYPE=?\n" + 
                ",SECOND_KEY_VALUE=?,SECOND_EIT_SEGMENT=?,APPROVAL_CODE=? where ID=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, approvalCodition.getEitCode());
                ps.setString(2, approvalCodition.getFirstKeyType());
                ps.setString(3, approvalCodition.getFirstKeyValue());
                ps.setString(4, approvalCodition.getFirstEitSegment());
                ps.setString(5, approvalCodition.getOperation());
                ps.setString(6, approvalCodition.getSecondKeyType());
                ps.setString(7, approvalCodition.getSecondKeyValue());
                ps.setString(8, approvalCodition.getSecondEitSegment());
                ps.setString(9, approvalCodition.getApprovalCode());
               ps.setString(10, approvalCodition.getId());
                ps.executeUpdate();

            


        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalCodition;
    }
    
    
    public String getCountOfApprovalCondition(String approvalcode) {

     String approvalCodeCount=null;
     try {
         connection = AppsproConnection.getConnection();
         String query =
             "select count(*) FROM "+" "+getSchema_Name()+".XXX_APPROVAL_CONDITION WHERE APPROVAL_CODE =?";
         ps = connection.prepareStatement(query);
         ps.setString(1, approvalcode);
         rs = ps.executeQuery();
         while (rs.next()) {
             
             
             approvalCodeCount=rs.getString(1) ;

         }

     } catch (Exception e) {
         //("Error: ");
        e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
     } finally {
         closeResources(connection, ps, rs);
     }
     return approvalCodeCount;

    }
    
    public ArrayList<ApprovalConditionBean> getAllApprovalCode(String eitcode ) {

     ArrayList<ApprovalConditionBean> approvalConditionList =
         new ArrayList<ApprovalConditionBean>();
     try {
         connection = AppsproConnection.getConnection();
         String query =
             "SELECT APPROVAL_CODE FROM "+" "+getSchema_Name()+".XXX_APPROVAL_CONDITION where EIT_CODE=?";
         ps = connection.prepareStatement(query);
         ps.setString(1, eitcode);
         rs = ps.executeQuery();
         while (rs.next()) {
             ApprovalConditionBean approvalConditionBean = new ApprovalConditionBean();
             approvalConditionBean.setApprovalCode(rs.getString("APPROVAL_CODE"));
             
             
             approvalConditionList.add(approvalConditionBean);

         }

     } catch (Exception e) {
         //("Error: ");
        e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
     } finally {
         closeResources(connection, ps, rs);
     }
     return approvalConditionList;

    }
   
   
    public String deleteApprovalCondition(String id) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult=0;

        try {
            query = "DELETE FROM "+" "+getSchema_Name()+".XXX_APPROVAL_CONDITION WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            checkResult = ps.executeUpdate();
            
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }
    
    public ArrayList<ApprovalConditionBean> searchApprovalConditionSetup(ApprovalConditionBean bean) {
        ArrayList<ApprovalConditionBean> approvalList =
            new ArrayList<ApprovalConditionBean>();
        ApprovalConditionBean approvalBean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT * FROM  " + " " + getSchema_Name() + ".XXX_APPROVAL_CONDITION WHERE EIT_CODE = ?";

            ps = connection.prepareStatement(query);
            //            ps.setString(1, "%" + eitCode + "%");
            ps.setString(1, bean.getEitCode());
            rs = ps.executeQuery();
            while (rs.next()) {
                approvalBean = new ApprovalConditionBean();
                approvalBean.setId(rs.getString("ID"));
                approvalBean.setEitCode(rs.getString("EIT_CODE"));
                approvalBean.setFirstKeyType(rs.getString("FRIST_KEY_TYPE"));
                approvalBean.setFirstKeyValue(rs.getString("FRIST_KEY_VALUE"));
                approvalBean.setFirstEitSegment(rs.getString("FRIST_EIT_SEGMENT"));
                approvalBean.setOperation(rs.getString("OPERATION"));
                approvalBean.setSecondKeyType(rs.getString("SECOND_KEY_TYPE"));
                approvalBean.setSecondKeyValue(rs.getString("SECOND_KEY_VALUE"));
                approvalBean.setSecondEitSegment(rs.getString("SECOND_EIT_SEGMENT"));
                approvalBean.setApprovalCode(rs.getString("APPROVAL_CODE"));
                approvalList.add(approvalBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

        } finally {
            closeResources(connection, ps, rs);
        }

        return approvalList;
    }
}
