/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalManagementBean;
import com.appspro.fusionsshr.bean.AttachmentBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author CPBSLV
 */
public class AttachmentDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public AttachmentBean InsertXXX_Attachment(String body) {
        
        AttachmentBean obj = new AttachmentBean();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(body);
                JSONObject jsonObjInput;
                for(int i = 0; i < arr.length(); i++){
                    jsonObjInput = arr.getJSONObject(i);    
                    beanList.add(jsonObjInput);
                }

        try {
            connection = AppsproConnection.getConnection();

            String query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_ATTACHMENT(SS_ID,ATTACHMENT,NAME) VALUES(?,?,?)";

            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
            ps.setString(1, bean.get("SS_id").toString());
            ps.setString(2, bean.getString("attachment"));
            ps.setString(3,bean.get("name").toString());    
            ps.executeUpdate();
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return obj;
      
    }

    public AttachmentBean UpdateXXX_Attachment(String body) {
        
        AttachmentBean obj = new AttachmentBean();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        JSONArray arr = new JSONArray(body);
        if(arr == null || arr.length() < 1) {
            return null;
        }
                JSONObject jsonObjInput;
                for(int i = 0; i < arr.length(); i++){
                    jsonObjInput = arr.getJSONObject(i);    
                    beanList.add(jsonObjInput);
                } 
                
        if(beanList == null || beanList.isEmpty()) {
            return null;
        }

        try {
            connection = AppsproConnection.getConnection();

            String query="DELETE FROM  "+ " " + getSchema_Name() + ".XXX_ATTACHMENT WHERE SS_ID=?";
            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
              ps.setString(1, bean.get("SS_id").toString());
              ps.addBatch();
              ps.clearParameters();
            }
            
           
            ps.executeBatch();




         query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_ATTACHMENT(SS_ID,ATTACHMENT,NAME) VALUES(?,?,?)";

            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
              ps.setString(1, bean.get("SS_id").toString());
              ps.setString(2, bean.getString("attachment"));
              ps.setString(3,bean.get("name").toString()); 
                ps.addBatch();
                ps.clearParameters();
            }
            
            ps.executeBatch();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return obj;

    }

    public ArrayList<AttachmentBean> AllXXX_Attachment(String ss_id) {
        ArrayList<AttachmentBean> beanList = new ArrayList<AttachmentBean>();
    
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT * FROM  "+ " " + getSchema_Name() + ".XXX_ATTACHMENT WHERE SS_ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, ss_id);
            rs = ps.executeQuery();
            while (rs.next()) {
                AttachmentBean bean = new AttachmentBean();
                //bean.setId(rs.getString("NEXTVAL"));
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setAttachment(rs.getString("ATTACHMENT"));
                bean.setName(rs.getString("NAME"));
                beanList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
             closeResources(connection, ps, rs);
        }
        return beanList;

    }
}
