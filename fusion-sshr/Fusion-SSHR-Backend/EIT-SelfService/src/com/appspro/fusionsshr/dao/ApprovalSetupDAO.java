/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author user
 */
public class ApprovalSetupDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ApprovalSetupBean insertOrUpdateApprovalSetup(ApprovalSetupBean bean,
                                                         String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
          
            if (transactionType.equals("ADD")) {
           
                String query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP (EIT_CODE,APPROVAL_ORDER,APPROVAL_TYPE,ROLE_NAME,SPECIALCASE ,NOTIFICATION_TYPE,CREATEDBY,CREATEDDATE,APPROVAL_CODE,OPERATION,MANAGER_LEVAL )\n" +
                    "                 VALUES ( ?, ? ,? , ? ,?,? ,?,?,?,?,?)";

                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getEitCode());
                ps.setString(2, bean.getApprovalOrder());
                ps.setString(3, bean.getApprovalType());
                if( bean.getApprovalType().equals("ROLES")){
                        ps.setString(4, bean.getRoleName());
                }else if(bean.getApprovalType().equals("POSITION")){
                      ps.setString(4, bean.getPosition());
                }else{
                    ps.setString(4, "");
                }
                
                ps.setString(5, bean.getSpecialCase());
                ps.setString(6, bean.getNotificationType());
                ps.setString(7, bean.getCreatedBy());
                ps.setDate(8, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(9, bean.getApproval());
                if (bean.getApprovalType().equals("JOB_LEVEL")) {
                                   ps.setString(10, bean.getOperation());
                                   ps.setString(11, bean.getManagerLeval());
                               } else {
                                   ps.setString(10, "");
                                   ps.setString(11, "");
                               }

                ps.executeUpdate();
                // ArrayList<ClothesAllowanceBean> allowanceBeansList =getMaxID();
                // UpdateClothesAllowanceCode(allowanceBeansList.get(allowanceBeansList.size()-1));
            } else if (transactionType.equals("EDIT")) {
                String query =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP SET  EIT_CODE =? ,APPROVAL_ORDER = ? , APPROVAL_TYPE = ?, ROLE_NAME = ?, SPECIALCASE = ?, NOTIFICATION_TYPE= ? , APPROVAL_CODE=? , OPERATION=? , MANAGER_LEVAL =?  WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getEitCode());
                ps.setString(2, bean.getApprovalOrder());
                ps.setString(3, bean.getApprovalType());
                if( bean.getApprovalType().equals("ROLES")){
                        ps.setString(4, bean.getRoleName());
                }else if(bean.getApprovalType().equals("POSITION")){
                      ps.setString(4, bean.getPosition());
                }else{
                    ps.setString(4, "");
                }
               
                ps.setString(5, bean.getSpecialCase());
                ps.setString(6, bean.getNotificationType());
                ps.setString(7, bean.getApproval());
                if (bean.getApprovalType().equals("JOB_LEVEL")) {
                                    ps.setString(8, bean.getOperation());
                                    ps.setString(9, bean.getManagerLeval());
                                } else {
                                    ps.setString(8, "");
                                    ps.setString(9, "");
                                }
                ps.setString(10, bean.getId());
                ps.executeUpdate();
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

    public int getApprovalForPositionValidation(String EIT_Code,
                                                String Role_Name,
                                                String Approval_Type,
                                                String Notification_Type,
                                                String approval) {
        connection = AppsproConnection.getConnection();
        int countApproval = 0;
        try {
            //(!Role_Name.equals("") + "      " + !Role_Name.equals("null"));
            if ((!Role_Name.equals("")) && (!Role_Name.equals("null"))) {
                String query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP WHERE EIT_CODE = ? AND ROLE_NAME = ? AND APPROVAL_TYPE = ? AND NOTIFICATION_TYPE = ? AND APPROVAL_CODE=?";

                ps = connection.prepareStatement(query);
                ps.setString(1, EIT_Code.toString());
                ps.setString(2, Role_Name.toString());
                ps.setString(3, Approval_Type.toString());
                ps.setString(4, Notification_Type.toString());
                ps.setString(5, approval.toString());
                rs = ps.executeQuery();

                while (rs.next()) {
                    countApproval++;

                }
            } else {
                String query =
                    "SELECT * FROM  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP WHERE EIT_CODE = ? AND APPROVAL_TYPE = ? AND NOTIFICATION_TYPE = ? AND APPROVAL_CODE=?";

                ps = connection.prepareStatement(query);
                ps.setString(1, EIT_Code.toString());
                ps.setString(2, Approval_Type.toString());
                ps.setString(3, Notification_Type.toString());
                ps.setString(4,approval.toString());
                rs = ps.executeQuery();

                while (rs.next()) {
                    countApproval++;

                }
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return countApproval;
    }

    public ArrayList<ApprovalSetupBean> getApprovalByEITCode(String eitCode) {
        ArrayList<ApprovalSetupBean> approvalSetupList =
            new ArrayList<ApprovalSetupBean>();
        ApprovalSetupBean approvalSetupObject = null;
        connection = AppsproConnection.getConnection();

        String query =
            "select  * from  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP  where EIT_CODE = ? ORDER BY approval_order ASC";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                approvalSetupObject = new ApprovalSetupBean();
                approvalSetupObject.setId(rs.getString("ID"));
                approvalSetupObject.setEitCode(rs.getString("EIT_CODE"));
                approvalSetupObject.setApprovalOrder(rs.getString("APPROVAL_ORDER"));
                approvalSetupObject.setApprovalType(rs.getString("APPROVAL_TYPE"));
                approvalSetupObject.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupObject.setSpecialCase(rs.getString("SPECIALCASE"));
                approvalSetupObject.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                approvalSetupObject.setCreatedBy(rs.getString("CREATEDBY"));
                approvalSetupObject.setCreationDate(rs.getString("CREATEDDATE"));
                approvalSetupObject.setApproval(rs.getString("APPROVAL_CODE"));
                approvalSetupObject.setApprovalCode(rs.getString("APPROVAL_CODE"));
                approvalSetupObject.setOperation(rs.getString("OPERATION"));
                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                //                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                approvalSetupList.add(approvalSetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }
    
    
    public ArrayList<ApprovalSetupBean> getApprovalByEITCodeAndApprovalCode(String eitCode,String approvalCode) {
        ArrayList<ApprovalSetupBean> approvalSetupList =
            new ArrayList<ApprovalSetupBean>();
        ApprovalSetupBean approvalSetupObject = null;
        connection = AppsproConnection.getConnection();

        String query =
            "select  * from  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP  where EIT_CODE = ? AND APPROVAL_CODE =?  ORDER BY approval_order ASC";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode.toString());
            ps.setString(2, approvalCode.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                approvalSetupObject = new ApprovalSetupBean();
                approvalSetupObject.setId(rs.getString("ID"));
                approvalSetupObject.setEitCode(rs.getString("EIT_CODE"));
                approvalSetupObject.setApprovalOrder(rs.getString("APPROVAL_ORDER"));
                approvalSetupObject.setApprovalType(rs.getString("APPROVAL_TYPE"));
                approvalSetupObject.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupObject.setSpecialCase(rs.getString("SPECIALCASE"));
                approvalSetupObject.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                approvalSetupObject.setCreatedBy(rs.getString("CREATEDBY"));
                approvalSetupObject.setCreationDate(rs.getString("CREATEDDATE"));
                approvalSetupObject.setApproval(rs.getString("APPROVAL_CODE"));
                approvalSetupObject.setOperation(rs.getString("OPERATION"));
                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                //                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                approvalSetupList.add(approvalSetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }
    

    public int getMAxApprovalOrder(String eit_Code ,String approvalCode) {
        connection = AppsproConnection.getConnection();
        int max_Order = 0;
        String query =
            "SELECT MAX(APPROVAL_ORDER) AS Max_Approval_Order FROM  " + " " +
            getSchema_Name() + ".XXX_APROVALS_SETUP WHERE EIT_CODE = ? AND APPROVAL_CODE=?";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eit_Code);
            ps.setString(2, approvalCode);
            rs = ps.executeQuery();

            while (rs.next()) {
                max_Order = rs.getInt("Max_Approval_Order");
            }
            if (max_Order > 0) {
                max_Order = max_Order;
            } else {
                max_Order = 0;
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return max_Order;

    }

    public void deleteApprovalEitCode(int ID) {
        connection = AppsproConnection.getConnection();       
        
           String deleteQuery = "DELETE FROM  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP WHERE ID = ?";
           String selectQuery = "SELECT eit_code,APPROVAL_CODE FROM  " + " " + getSchema_Name() +
                ".xxx_aprovals_setup WHERE ID = ?";
        int store_ID = ID;
        int order = 1;
        String eit_code = "";
        String approval_code = "";
        try {
            ps = connection.prepareStatement(selectQuery);
            ps.setInt(1, ID);
            rs = ps.executeQuery();

            while (rs.next()) {
                eit_code = rs.getString("eit_code");
                approval_code = rs.getString("APPROVAL_CODE");
            }
            
            ps = connection.prepareStatement(deleteQuery);
            ps.setInt(1, ID);
            ps.executeUpdate();

            String select =
                "SELECT ID,APPROVAL_Order FROM  " + " " + getSchema_Name() +
                ".xxx_aprovals_setup WHERE eit_code = ? AND APPROVAL_CODE=? ORDER BY approval_order ASC";
            ps = connection.prepareStatement(select);
            ps.setString(1, eit_code);
            ps.setString(2,approval_code);
            rs = ps.executeQuery();

            while (rs.next()) {
                ID = rs.getInt("ID");
                String delete =
                    "UPDATE  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP SET APPROVAL_ORDER = ? WHERE ID = ?";

                ps = connection.prepareStatement(delete);
                ps.setString(1, String.valueOf(order));
                ps.setInt(2, ID);
                ps.executeUpdate();
                ++order;
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        //("Delete Success");

    }

    public String getLastNotificationType(String eitCode,String approvalCode) {
        connection = AppsproConnection.getConnection();
        String query =
            "select NOTIFICATION_TYPE from  " + " " + getSchema_Name() +
            ".XXX_APROVALS_SETUP where id=(select max(id) from  " + " " +
            getSchema_Name() + ".XXX_APROVALS_SETUP WHERE EIT_CODE =? And APPROVAL_CODE=?)";
        String notificationType = "";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2,approvalCode);
            rs = ps.executeQuery();

            while (rs.next()) {
                notificationType = rs.getString("NOTIFICATION_TYPE");
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return notificationType;
    }

    public ArrayList<ApprovalSetupBean> getApprovalTypeByEITCode(String eitCode) {
        ArrayList<ApprovalSetupBean> approvalSetupList =
            new ArrayList<ApprovalSetupBean>();
        ApprovalSetupBean approvalSetupObject = null;
        connection = AppsproConnection.getConnection();

        String query =
            "select distinct XAS.role_name,XAS.approval_type as valueName,XAS.notification_type,pl.value_en as labelName \n" +
            "from  " + " " + getSchema_Name() +
            ".XXX_APROVALS_SETUP XAS INNER JOIN  " + " " + getSchema_Name() +
            ".XXX_PAAS_LOOKUP PL ON xas.approval_type = pl.code  \n" +
            "where XAS.EIT_CODE = ? AND XAS.notification_type = 'FYA'";
        System.out.println(query);

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                approvalSetupObject = new ApprovalSetupBean();
              //  approvalSetupObject.setId(rs.getString("ID"));
                approvalSetupObject.setApprovalType(rs.getString("valueName"));
                approvalSetupObject.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupObject.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                approvalSetupObject.setSpecialCase(rs.getString("labelName"));

                approvalSetupList.add(approvalSetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
           // closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }
    public ArrayList<ApprovalSetupBean> getApprovalByEITCodeAndConditionCode(String eitCode,String conditionCode) {
        ArrayList<ApprovalSetupBean> approvalSetupList =
            new ArrayList<ApprovalSetupBean>();
        ApprovalSetupBean approvalSetupObject = null;
        connection = AppsproConnection.getConnection();

        String query =
            "select  * from  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP  where EIT_CODE = ? AND APPROVAL_CODE = ? ORDER BY approval_order ASC";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode.toString());
            ps.setString(2, conditionCode.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                approvalSetupObject = new ApprovalSetupBean();
                approvalSetupObject.setId(rs.getString("ID"));
                approvalSetupObject.setEitCode(rs.getString("EIT_CODE"));
                approvalSetupObject.setApprovalOrder(rs.getString("APPROVAL_ORDER"));
                approvalSetupObject.setApprovalType(rs.getString("APPROVAL_TYPE"));
                approvalSetupObject.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupObject.setSpecialCase(rs.getString("SPECIALCASE"));
                approvalSetupObject.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                approvalSetupObject.setCreatedBy(rs.getString("CREATEDBY"));
                approvalSetupObject.setCreationDate(rs.getString("CREATEDDATE"));
                approvalSetupObject.setApproval(rs.getString("APPROVAL_CODE"));
                approvalSetupObject.setOperation(rs.getString("OPERATION"));
                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                //                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                approvalSetupList.add(approvalSetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }
}
