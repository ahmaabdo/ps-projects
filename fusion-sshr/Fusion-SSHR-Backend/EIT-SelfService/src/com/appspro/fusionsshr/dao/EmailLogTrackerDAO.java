package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.EmailLogTrackerBean;

import com.appspro.mail.CredentialStoreClassMail;

import static common.restHelper.RestHelper.getSchema_Name;

import common.restHelper.RestHelper;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

public class EmailLogTrackerDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public void logMailInfo(String reciever, String personName, String subject,
                            String msgBody, boolean status) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;


                query = "INSERT INTO "+ getSchema_Name() + ".XXX_LOG_EMAIL_TRACKER " +
                        "(RECIEVER,PERSON_NAME,SUBJECT,MSG_BODY,STATUS,CREATION_DATE, UPDATED_DATE) " +
                        "VALUES(?,?,?,?,?, sysdate, sysdate)";

                ps = connection.prepareStatement(query);

                ps.setString(1, reciever);
                ps.setString(2, personName);
                ps.setString(3, subject);
                ps.setString(4, msgBody);
                ps.setString(5, status ? "SUCCESS" : "FAILED");

                ps.executeUpdate();
            

        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    
    public EmailLogTrackerBean resendEmailFromPaaS(EmailLogTrackerBean emailLogTrackerBean, String transactionType) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            if (transactionType.equals("ADD")) {

//                query = "INSERT INTO "+ getSchema_Name() + ".XXX_LOG_EMAIL_TRACKER " +
//                        "(RECIEVER,PERSON_NAME,SUBJECT,MSG_BODY,STATUS,CREATION_DATE, UPDATED_DATE) " +
//                        "VALUES(?,?,?,?,?, sysdate, sysdate)";
    //
    //                ps = connection.prepareStatement(query);
    //
    //                ps.setString(1, emailLogTrackerBean.getReciever());
                
            } else if (transactionType.equals("EDIT")) {
                query = "UPDATE "+ getSchema_Name() + ".XXX_LOG_EMAIL_TRACKER " +
                    "SET RECIEVER = ?, " +
                    "SET PERSON_NAME = ?, " +
                    "SET SUBJECT = ?, " +
                    "SET STATUS = 'EDITED', " +
                    "SET MSG_BODY = ?, " +
                    "SET UPDATED_DATE = sysdate " +
                    "WHERE ID = ?";
                ps = connection.prepareStatement(query);
                ps.setString(1, emailLogTrackerBean.getReciever());
                ps.setString(2, emailLogTrackerBean.getPersonName());
                ps.setString(3, emailLogTrackerBean.getSubject());
                ps.setString(4, emailLogTrackerBean.getMsgBody());
                ps.setInt(5, emailLogTrackerBean.getId());
                ps.executeUpdate();
            }
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        //Sending Email
        CredentialStoreClassMail.SEND_MAIL(emailLogTrackerBean.getReciever(),
                                           emailLogTrackerBean.getSubject(),
                                           emailLogTrackerBean.getMsgBody());
        return emailLogTrackerBean;
    }
    
    
    public String getEmailMsgBodyById(String id) {
        String msgBody = "";

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT MSG_BODY FROM "+ getSchema_Name() + ".XXX_LOG_EMAIL_TRACKER WHERE ID = ?";
            
            ps = connection.prepareStatement(query);
            ps.setString(1, id);            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                msgBody = rs.getString("MSG_BODY");
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return msgBody;
    }
    
    
    public ArrayList<EmailLogTrackerBean> getAllEmailsLogs() {

        ArrayList<EmailLogTrackerBean> beanArr = new ArrayList<EmailLogTrackerBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT ID,RECIEVER,PERSON_NAME,SUBJECT,STATUS,CREATION_DATE,UPDATED_DATE " +
                "FROM "+ getSchema_Name() + ".XXX_LOG_EMAIL_TRACKER WHERE CREATION_DATE > SYSDATE - 14 ORDER BY ID DESC";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                EmailLogTrackerBean bean = new EmailLogTrackerBean();
                
                bean.setId(rs.getInt("ID"));
                bean.setReciever(rs.getString("RECIEVER"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                bean.setSubject(rs.getString("SUBJECT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));

                beanArr.add(bean);
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return beanArr;
    }
    

    public ArrayList<EmailLogTrackerBean> getAllEmailsLogsSearchResult(EmailLogTrackerBean body) {
        
        ArrayList<EmailLogTrackerBean> beanArr = new ArrayList<EmailLogTrackerBean>();
        
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT ID,RECIEVER,PERSON_NAME,SUBJECT,STATUS,CREATION_DATE,UPDATED_DATE " +
                "FROM "+ getSchema_Name() + ".XXX_LOG_EMAIL_TRACKER \n" + 
            "WHERE (UPPER(RECIEVER) LIKE UPPER(NVL(?, RECIEVER))) \n" + 
            "AND (UPPER(PERSON_NAME) LIKE UPPER(NVL(?,PERSON_NAME))) \n" + 
            "AND (UPPER(SUBJECT) LIKE UPPER(NVL(?,SUBJECT))) \n" + 
            "AND (STATUS LIKE NVL(?,STATUS)) \n" +
            "AND CREATION_DATE > SYSDATE - 14 \n" +
            "ORDER BY ID DESC";
            
            ps = connection.prepareStatement(query);
            
            ps.setString(1, body.getReciever());
            ps.setString(2, body.getPersonName());
            ps.setString(3, body.getSubject());
            ps.setString(4, body.getStatus());
            rs = ps.executeQuery();

            while (rs.next()) {
                EmailLogTrackerBean bean = new EmailLogTrackerBean();
                
                bean.setId(rs.getInt("ID"));
                bean.setReciever(rs.getString("RECIEVER"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                bean.setSubject(rs.getString("SUBJECT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));

                beanArr.add(bean);
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return beanArr;
    }
    
    
}
