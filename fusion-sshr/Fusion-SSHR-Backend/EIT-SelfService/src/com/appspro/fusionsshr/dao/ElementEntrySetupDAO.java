package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ElementEntrySetupBean;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

public class ElementEntrySetupDAO extends AppsproConnection {

private static ElementEntrySetupDAO instance = null;
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ElementEntrySetupBean insertOrUpdateElementEntry(ElementEntrySetupBean bean,
                                                            String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            if (transactionType.equals("ADD")) {

                String query =
                    "INSERT INTO  " + " " + getSchema_Name() + ".XXX_ELEMENT_ENTRY_SETUP " +
                    "(EIT_CODE,CREATED_BY,CREATED_DATE,ABSENCE_STATUS,ABSENCE_TYPE,EMPLOYER,START_DATE,END_DATE," +
                    "SOURCE_SYSTEM_OWNER,PERSON_NUMBER,ELEMENT_NAME,RECURRING_ENTRY,SOURCE_SYSTEM_ID," +
                    "LEGISLATIVE_DATAGROUP_NAME,CREATOR_TYPE,TYPE_OF_ACTION,INPUT_VALUE,REASON,EFFIVTIVE_START_DATE,START_TIME,END_TIME)\n" +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getEitCode());
                ps.setString(2, bean.getCreatedBy());
                ps.setDate(3, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(4, bean.getAbsenceStatus());
                ps.setString(5, bean.getAbsenceType());
                ps.setString(6, bean.getEmployer());
                ps.setString(7, bean.getStartDate());
                ps.setString(8, bean.getEndDate());
                ps.setString(9, bean.getSourceSystemOwner());
                ps.setString(10, bean.getPersonNumber());
                ps.setString(11, bean.getElementName());
                ps.setString(12, bean.getRecurringEntry());
                ps.setString(13, bean.getSourceSystemId());
                ps.setString(14, bean.getLegislativeDatagroupName());
                ps.setString(15, bean.getCreatorType());
                ps.setString(16, bean.getTypeOfAction());
                ps.setString(17, bean.getInputValue());
                ps.setString(18, bean.getReason());
                ps.setString(19, bean.getEffivtiveStartDate());
                ps.setString(20, bean.getStartTime());
                ps.setString(21, bean.getEndTime());
                ps.executeUpdate();
                
                
                

            } else if (transactionType.equals("EDIT")) {
                String query =
                    "UPDATE " + getSchema_Name() + ".XXX_ELEMENT_ENTRY_SETUP SET " +
                    "EIT_CODE = ? , ABSENCE_STATUS = ? , ABSENCE_TYPE = ?, EMPLOYER = ?, START_DATE = ?, END_DATE= ?," +
                    "SOURCE_SYSTEM_OWNER = ? , PERSON_NUMBER = ? , ELEMENT_NAME = ?, RECURRING_ENTRY = ?, SOURCE_SYSTEM_ID = ?, LEGISLATIVE_DATAGROUP_NAME= ?," +
                    "CREATOR_TYPE = ? , TYPE_OF_ACTION = ? , INPUT_VALUE = ? ,REASON = ?,EFFIVTIVE_START_DATE = ? ,START_TIME = ? ,END_TIME= ? WHERE id = ? ";
                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getEitCode());
                ps.setString(2, bean.getAbsenceStatus());
                ps.setString(3, bean.getAbsenceType());
                ps.setString(4, bean.getEmployer());
                ps.setString(5, bean.getStartDate());
                ps.setString(6, bean.getEndDate());
                ps.setString(7, bean.getSourceSystemOwner());
                ps.setString(8, bean.getPersonNumber());
                ps.setString(9, bean.getElementName());
                ps.setString(10, bean.getRecurringEntry());
                ps.setString(11, bean.getSourceSystemId());
                ps.setString(12, bean.getLegislativeDatagroupName());
                ps.setString(13, bean.getCreatorType());
                ps.setString(14, bean.getTypeOfAction());
                ps.setString(15, bean.getInputValue());
                ps.setString(16, bean.getReason());
                ps.setString(17, bean.getEffivtiveStartDate());
                ps.setString(18, bean.getStartTime());
                ps.setString(19, bean.getEndTime());
                ps.setString(20, bean.getId());
                
                ps.executeUpdate();
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

    public ArrayList<ElementEntrySetupBean> getAllElementEntries() {
        ArrayList<ElementEntrySetupBean> elementEntrySetupList =
            new ArrayList<ElementEntrySetupBean>();
        ElementEntrySetupBean elementEntrySetupObject = null;
        connection = AppsproConnection.getConnection();

        String query =
            "select * from " + getSchema_Name() + ".XXX_ELEMENT_ENTRY_SETUP";

        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                elementEntrySetupObject = new ElementEntrySetupBean();
                elementEntrySetupObject.setId(rs.getString("ID"));
                elementEntrySetupObject.setEitCode(rs.getString("EIT_CODE"));
                elementEntrySetupObject.setAbsenceStatus(rs.getString("ABSENCE_STATUS"));
                elementEntrySetupObject.setAbsenceType(rs.getString("ABSENCE_TYPE"));
                elementEntrySetupObject.setEmployer(rs.getString("EMPLOYER"));
                elementEntrySetupObject.setStartDate(rs.getString("START_DATE"));
                elementEntrySetupObject.setEndDate(rs.getString("END_DATE"));
                elementEntrySetupObject.setSourceSystemOwner(rs.getString("SOURCE_SYSTEM_OWNER"));
                elementEntrySetupObject.setPersonNumber(rs.getString("PERSON_NUMBER"));
                elementEntrySetupObject.setElementName(rs.getString("ELEMENT_NAME"));
                elementEntrySetupObject.setRecurringEntry(rs.getString("RECURRING_ENTRY"));
                elementEntrySetupObject.setSourceSystemId(rs.getString("SOURCE_SYSTEM_ID"));
                elementEntrySetupObject.setLegislativeDatagroupName(rs.getString("LEGISLATIVE_DATAGROUP_NAME"));
                elementEntrySetupObject.setCreatorType(rs.getString("CREATOR_TYPE"));
                elementEntrySetupObject.setTypeOfAction(rs.getString("TYPE_OF_ACTION"));
                elementEntrySetupObject.setInputValue(rs.getString("INPUT_VALUE"));
                elementEntrySetupObject.setReason(rs.getString("REASON"));
                elementEntrySetupObject.setEffivtiveStartDate(rs.getString("EFFIVTIVE_START_DATE"));
                elementEntrySetupObject.setStartTime(rs.getString("START_TIME"));
                elementEntrySetupObject.setEndTime(rs.getString("END_TIME"));

                elementEntrySetupList.add(elementEntrySetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntrySetupList;
    }

    public ArrayList<ElementEntrySetupBean> getElementEntryByEIT(String eitCode) {
        ArrayList<ElementEntrySetupBean> elementEntrySetupList =
            new ArrayList<ElementEntrySetupBean>();
        ElementEntrySetupBean elementEntrySetupObject = null;
        connection = AppsproConnection.getConnection();

        String query =
            "select ID,EIT_CODE,CREATED_DATE,ABSENCE_STATUS,ABSENCE_TYPE," +
            "EMPLOYER,START_DATE,END_DATE,SOURCE_SYSTEM_OWNER,\n" +
            "PERSON_NUMBER,ELEMENT_NAME,RECURRING_ENTRY,SOURCE_SYSTEM_ID," +
            "LEGISLATIVE_DATAGROUP_NAME,CREATOR_TYPE,TYPE_OF_ACTION,INPUT_VALUE,REASON,EFFIVTIVE_START_DATE,START_TIME,END_TIME from " +
            getSchema_Name() + ". XXX_ELEMENT_ENTRY_SETUP WHERE EIT_CODE = ?";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            rs = ps.executeQuery();

            while (rs.next()) {
                elementEntrySetupObject = new ElementEntrySetupBean();
                elementEntrySetupObject.setId(rs.getString("ID"));
                elementEntrySetupObject.setEitCode(rs.getString("EIT_CODE"));
                elementEntrySetupObject.setCreationDate(rs.getString("CREATED_DATE"));
                elementEntrySetupObject.setAbsenceStatus(rs.getString("ABSENCE_STATUS"));
                elementEntrySetupObject.setAbsenceType(rs.getString("ABSENCE_TYPE"));
                elementEntrySetupObject.setEmployer(rs.getString("EMPLOYER"));
                elementEntrySetupObject.setStartDate(rs.getString("START_DATE"));
                elementEntrySetupObject.setEndDate(rs.getString("END_DATE"));
                elementEntrySetupObject.setSourceSystemOwner(rs.getString("SOURCE_SYSTEM_OWNER"));
                elementEntrySetupObject.setPersonNumber(rs.getString("PERSON_NUMBER"));
                elementEntrySetupObject.setElementName(rs.getString("ELEMENT_NAME"));
                elementEntrySetupObject.setRecurringEntry(rs.getString("RECURRING_ENTRY"));
                elementEntrySetupObject.setSourceSystemId(rs.getString("SOURCE_SYSTEM_ID"));
                elementEntrySetupObject.setLegislativeDatagroupName(rs.getString("LEGISLATIVE_DATAGROUP_NAME"));
                elementEntrySetupObject.setCreatorType(rs.getString("CREATOR_TYPE"));
                elementEntrySetupObject.setTypeOfAction(rs.getString("TYPE_OF_ACTION"));
                elementEntrySetupObject.setInputValue(rs.getString("INPUT_VALUE"));
                elementEntrySetupObject.setReason(rs.getString("REASON"));
                elementEntrySetupObject.setEffivtiveStartDate(rs.getString("EFFIVTIVE_START_DATE"));
                elementEntrySetupObject.setStartTime(rs.getString("START_TIME"));
                elementEntrySetupObject.setEndTime(rs.getString("END_TIME"));
                elementEntrySetupList.add(elementEntrySetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntrySetupList;

    }

    public String getDELETEEIT(String id) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult=0;

        try {
            query = "delete  from XXX_ELEMENT_ENTRY_SETUP WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            checkResult = ps.executeUpdate();
            
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }


    public static ElementEntrySetupDAO getInstance() {
        if(instance == null)
            instance = new ElementEntrySetupDAO();
        return instance;
    }
    
    public ArrayList<ElementEntrySetupBean> getElementEntryByEITForBT(String eitCode, String travelType) {
        ArrayList<ElementEntrySetupBean> elementEntrySetupList =
            new ArrayList<ElementEntrySetupBean>();
        ElementEntrySetupBean elementEntrySetupObject = null;
        connection = AppsproConnection.getConnection();
        String elementName = "";
        if("Working".equals(travelType)){
            elementName = "Business Trip Lumpsum Payment";
        }
        else{
            elementName = "Training Business Trip Lumpsum Payment";
        }

        String query =
            "select ID,EIT_CODE,CREATED_DATE,ABSENCE_STATUS,ABSENCE_TYPE," +
            "EMPLOYER,START_DATE,END_DATE,SOURCE_SYSTEM_OWNER,\n" +
            "PERSON_NUMBER,ELEMENT_NAME,RECURRING_ENTRY,SOURCE_SYSTEM_ID," +
            "LEGISLATIVE_DATAGROUP_NAME,CREATOR_TYPE,TYPE_OF_ACTION,INPUT_VALUE,REASON,EFFIVTIVE_START_DATE,START_TIME,END_TIME from " +
            getSchema_Name() + ". XXX_ELEMENT_ENTRY_SETUP WHERE EIT_CODE = ? AND ELEMENT_NAME = ?";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, elementName);
            rs = ps.executeQuery();

            while (rs.next()) {
                elementEntrySetupObject = new ElementEntrySetupBean();
                elementEntrySetupObject.setId(rs.getString("ID"));
                elementEntrySetupObject.setEitCode(rs.getString("EIT_CODE"));
                elementEntrySetupObject.setCreationDate(rs.getString("CREATED_DATE"));
                elementEntrySetupObject.setAbsenceStatus(rs.getString("ABSENCE_STATUS"));
                elementEntrySetupObject.setAbsenceType(rs.getString("ABSENCE_TYPE"));
                elementEntrySetupObject.setEmployer(rs.getString("EMPLOYER"));
                elementEntrySetupObject.setStartDate(rs.getString("START_DATE"));
                elementEntrySetupObject.setEndDate(rs.getString("END_DATE"));
                elementEntrySetupObject.setSourceSystemOwner(rs.getString("SOURCE_SYSTEM_OWNER"));
                elementEntrySetupObject.setPersonNumber(rs.getString("PERSON_NUMBER"));
                elementEntrySetupObject.setElementName(rs.getString("ELEMENT_NAME"));
                elementEntrySetupObject.setRecurringEntry(rs.getString("RECURRING_ENTRY"));
                elementEntrySetupObject.setSourceSystemId(rs.getString("SOURCE_SYSTEM_ID"));
                elementEntrySetupObject.setLegislativeDatagroupName(rs.getString("LEGISLATIVE_DATAGROUP_NAME"));
                elementEntrySetupObject.setCreatorType(rs.getString("CREATOR_TYPE"));
                elementEntrySetupObject.setTypeOfAction(rs.getString("TYPE_OF_ACTION"));
                elementEntrySetupObject.setInputValue(rs.getString("INPUT_VALUE"));
                elementEntrySetupObject.setReason(rs.getString("REASON"));
                elementEntrySetupObject.setEffivtiveStartDate(rs.getString("EFFIVTIVE_START_DATE"));
                elementEntrySetupObject.setStartTime(rs.getString("START_TIME"));
                elementEntrySetupObject.setEndTime(rs.getString("END_TIME"));
                elementEntrySetupList.add(elementEntrySetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntrySetupList;

    }
}
