package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import static common.restHelper.RestHelper.getSchema_Name;


import com.appspro.fusionsshr.bean.MessageNotificationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import org.json.JSONObject;

public class MessageNotificationDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    PreparedStatement ps1;
    CallableStatement cs;
    ResultSet rs;

    public ArrayList<MessageNotificationBean> getMessageNotificationBySelfService(String selfService) {
        ArrayList<MessageNotificationBean> messageNotificationList =
            new ArrayList<MessageNotificationBean>();

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "SELECT * FROM XXX_SETUP_MESSAGES_NOTI WHERE EIT_CODE=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, selfService.toString());

            rs = ps.executeQuery();
            while (rs.next()) {

                MessageNotificationBean messageNotificationBean =
                    new MessageNotificationBean();
                messageNotificationBean.setId(rs.getString("ID"));
                messageNotificationBean.setEitCode(rs.getString("EIT_CODE"));
                messageNotificationBean.setEmailBody(rs.getString("EMAIL_BODY"));
                messageNotificationBean.setNotificationMessage(rs.getString("NOTI_MESSAGE"));
                messageNotificationBean.setNotificationType(rs.getString("NOTI_TYPE"));
                messageNotificationBean.setHaveAttach(rs.getString("HAVE_ATT"));
                messageNotificationBean.setAttachSource(rs.getString("ATT_SOURCE"));
                messageNotificationList.add(messageNotificationBean);

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return messageNotificationList;
    }


    public MessageNotificationBean addMessageNotification(MessageNotificationBean messageNotificationBean) {
        String attachSource = messageNotificationBean.getAttachSource();
        int maxId = 0;

        try {

            connection = AppsproConnection.getConnection();
            String queryMaxId =
                "SELECT  NVL (MAX(id), 0)+1  AS MaxId  FROM XXX_SETUP_MESSAGES_NOTI";
            ps = connection.prepareStatement(queryMaxId);
            rs = ps.executeQuery();
            while (rs.next()) {
                maxId = rs.getInt("MaxId");
            }


            String query =
                "INSERT INTO XXX_SETUP_MESSAGES_NOTI (ID,EIT_CODE,EMAIL_BODY,NOTI_MESSAGE,NOTI_TYPE,HAVE_ATT,ATT_SOURCE)VALUES(?,?,?,?,?,?,?)";

            ps = connection.prepareStatement(query);
            ps.setInt(1, maxId);
            ps.setString(2, messageNotificationBean.getEitCode());
            ps.setString(3, messageNotificationBean.getEmailBody());
            ps.setString(4, messageNotificationBean.getNotificationMessage());
            ps.setString(5, messageNotificationBean.getNotificationType());
            ps.setString(6, messageNotificationBean.getHaveAttach());
            ps.setString(7, messageNotificationBean.getAttachSource());
            ps.executeUpdate();


            if (("PaaS").equals(attachSource) ||
                ("SaaS").equals(attachSource)) {
                String query2 =
                    "insert into XXX_ATTACMENT_MSG (MSG_ID,EIT_CODE,ATTACHMENT,FILE_NAME,FILE_TYPE) VALUES(?,?,?,?,? )";

                ps1 = connection.prepareStatement(query2);

                ps1.setString(1, String.valueOf(maxId));
                ps1.setString(2, messageNotificationBean.getEitCode());
                ps1.setString(3, messageNotificationBean.getAttachment());
                ps1.setString(4, messageNotificationBean.getFileName());
                ps1.setString(5, messageNotificationBean.getFileType());
                ps1.executeUpdate();
            }


        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            System.out.println("the erorr is " + e.getMessage());
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return messageNotificationBean;
    }

    public MessageNotificationBean updateMessageNotification(MessageNotificationBean messageNotificationBean,
                                                             String id) {
      
        String msgId = id;
        String attachSource = messageNotificationBean.getAttachSource();

        try {

            connection = AppsproConnection.getConnection();
            String query2 = " delete from  XXX_ATTACMENT_MSG WHERE MSG_ID=?";
            ps = connection.prepareStatement(query2);
            ps.setString(1, msgId);
            ps.executeUpdate();
            if (("PaaS").equals(attachSource) ||
                ("SaaS").equals(attachSource)) {
                String query3 =
                    "insert into XXX_ATTACMENT_MSG (MSG_ID,EIT_CODE,ATTACHMENT,FILE_NAME,FILE_TYPE) VALUES( ?,?,?,?,?)";
                ps = connection.prepareStatement(query3);
                ps.setString(1, msgId);
                ps.setString(2, messageNotificationBean.getEitCode());
                ps.setString(3, messageNotificationBean.getAttachment());
                ps.setString(4, messageNotificationBean.getFileName());
                ps.setString(5, messageNotificationBean.getFileType());
                ps.executeUpdate();

            }
            String query =
                "UPDATE  XXX_SETUP_MESSAGES_NOTI SET EIT_CODE=?,EMAIL_BODY=?,NOTI_MESSAGE=?,NOTI_TYPE=?,HAVE_ATT=?,ATT_SOURCE=? WHERE ID =?";

            ps = connection.prepareStatement(query);
            ps.setString(1, messageNotificationBean.getEitCode());
            ps.setString(2, messageNotificationBean.getEmailBody());
            ps.setString(3, messageNotificationBean.getNotificationMessage());
            ps.setString(4, messageNotificationBean.getNotificationType());
            ps.setString(5, messageNotificationBean.getHaveAttach());
            ps.setString(6, messageNotificationBean.getAttachSource());
            ps.setInt(7, Integer.parseInt(id));
            ps.executeUpdate();


        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return messageNotificationBean;
    }


    public String deleteNotificationMessage(String id, String attachSource) {

        connection = AppsproConnection.getConnection();
        String query;
        String query2;
        int checkResult = 0;

        try {
            query = "DELETE FROM XXX_SETUP_MESSAGES_NOTI WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            checkResult = ps.executeUpdate();
            if (attachSource.equals("PaaS")) {
                query2 = "DELETE FROM XXX_ATTACMENT_MSG WHERE MSG_ID=?";
                ps = connection.prepareStatement(query2);
                ps.setString(1, id);
                checkResult = ps.executeUpdate();
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }


    public ArrayList<MessageNotificationBean> getAttachmentByESelfServ(String id) {
        ArrayList<MessageNotificationBean> msgNotificationSelfSevList =
            new ArrayList<MessageNotificationBean>();

        MessageNotificationBean messageSelfServiceObject = null;
        connection = AppsproConnection.getConnection();

        String query =
            "select  * from  " + " " + getSchema_Name() + ".XXX_ATTACMENT_MSG  where MSG_ID = ? ";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                messageSelfServiceObject = new MessageNotificationBean();
                messageSelfServiceObject.setId(rs.getString("ID"));
                messageSelfServiceObject.setAttachment(rs.getString("ATTACHMENT"));
                messageSelfServiceObject.setFileName(rs.getString("FILE_NAME"));
                messageSelfServiceObject.setFileType(rs.getString("FILE_TYPE"));
                msgNotificationSelfSevList.add(messageSelfServiceObject);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return msgNotificationSelfSevList;
    }


    public String deleteAttachment(String id) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult = 0;

        try {
            query = "DELETE FROM  XXX_ATTACMENT_MSG  WHERE MSG_ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            checkResult = ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }
}


