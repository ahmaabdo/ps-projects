/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.HandoverHeaderBean;
import com.appspro.fusionsshr.bean.MessageBeanSetup;
import com.appspro.fusionsshr.bean.NotificationBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;
import com.appspro.mail.CredentialStoreClassMail;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import utilities.NotificationUtility;


/**
 *
 * @author user
 */
public class WorkflowNotificationDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    EmployeeDetails employeeDetails = new EmployeeDetails();
    CredentialStoreClassMail mailObj = new CredentialStoreClassMail();
    MessageBeanSetup messageBean = new MessageBeanSetup();
    HashMap<String, String> extraInfoForEmail = new HashMap<String, String>();
    PeopleExtraInformationBean peib = new PeopleExtraInformationBean();

    EmployeeBean emp = null;

    public WorkFlowNotificationBean insertIntoWorkflow(WorkFlowNotificationBean bean) {
        try {
            //("Work Flow Notification: ");
            //(new JSONObject (bean));
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    " insert into  " + " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION (MSG_TITLE,MSG_BODY,CREATION_DATE,RECEIVER_TYPE,RECEIVER_ID,RESPONSE_PERSON_ID,TYPE,RESPONSE_DATE,WORKFLOW_ID,REQUEST_ID,STATUS,SELF_TYPE,PERSON_NAME,NATIONALIDENTITY) " +
                    "VALUES(?,?,SYSDATE,?,?,?,?,?,?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getMsgTitle());
            ps.setString(2, bean.getMsgBody());
            ps.setString(3, bean.getReceiverType());
            ps.setString(4, bean.getReceiverId());
            ps.setString(5, bean.getResponsePersonId());
            ps.setString(6, bean.getType());
            ps.setString(7, bean.getResponseDate());
            ps.setString(8, bean.getWorkflowId());
            ps.setString(9, bean.getRequestId());
            ps.setString(10, bean.getStatus());
            ps.setString(11, bean.getSelfType());
            ps.setString(12, bean.getPersonName());
            ps.setString(13, bean.getNationalIdentity());
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }

    public ArrayList getAllNotification(String MANAGER_ID,
                                        String managerOfManager,
                                        String position, String emp,
                                        List<String> userRoles, String self_type,
                                                                     String personNumber,
                                                                     String requset_id,
                                                                     String type,
                                                                     String nationalIdentity,
                                                                     String status) {

        String rolesIdStr = "";
        int userRolesCount = userRoles.size();

        for (int i = 0; i < userRolesCount; i++) {
            rolesIdStr += ",?";
        }
        rolesIdStr = rolesIdStr.replaceFirst(",", "");
        if (userRolesCount > 0) {
            rolesIdStr += ")";
        } else {
            userRoles.add("");
            rolesIdStr = "?)";
        }
        userRolesCount = userRoles.size();
        ArrayList<NotificationBean> notificationList =
            new ArrayList<NotificationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT * FROM (\n" +
                    "SELECT NT.*,SS.PERSON_ID,SS.PERSON_NUMBER \n" +
                    "    FROM \n" +
                    "         " + " " + getSchema_Name() +
                    ".XX_WORKFLOW_NOTIFICATION NT,\n" +
                    "         " + " " + getSchema_Name() +
                    ".XX_SELF_SERVICE SS\n" +
                    "     WHERE \n" +
                    "            NT.STATUS = 'OPEN' \n" +
           " AND ((NT.SELF_TYPE =NVL(?,NT.SELF_TYPE)) AND (SS.PERSON_NUMBER =NVL(?,SS.PERSON_NUMBER)) AND (NT.TYPE =NVL(?,NT.TYPE)) and ( NT.NATIONALIDENTITY =? or ? is null) AND (SS.STATUS =? OR  ? is null) AND (NT.STATUS =? OR ? is null))\n" +
                    "            AND SS.TYPE = NT.SELF_TYPE\n" +
                    "            AND SS.TRANSACTION_ID= NT.REQUEST_ID\n" +
                    "            AND ((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = ?) OR \n" +
                    "                 (NT.RECEIVER_TYPE = 'LINE_MANAGER+1' AND NT.RECEIVER_ID = ?) OR \n" +
                    "(NT.RECEIVER_TYPE = 'LINE_MANAGER+' AND NT.RECEIVER_ID = ?) OR \n" +
                    "(NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = ?)OR \n" +
                    "(NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = ?) OR \n" +
                    "                 (NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = ?) OR                 \n" +
                    "                (NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = ?)OR" +
                    "(NT.RECEIVER_TYPE = 'Special_Case' AND NT.RECEIVER_ID = ?)OR" +
                    "(NT.RECEIVER_TYPE = 'ROLES' AND NT.RECEIVER_ID IN (" +
                    rolesIdStr + ")" + "                )\n" +
                    "			\n" +
                    "UNION\n" +
                    "    SELECT NT.*,SS.PERSON_ID,SS.PERSON_NUMBER \n" +
                    "    FROM \n" +
                    "         " + " " + getSchema_Name() +
                    ".XX_WORKFLOW_NOTIFICATION NT,\n" +
                    "         " + " " + getSchema_Name() +
                    ".XX_SELF_SERVICE SS \n" +
                    "     WHERE \n" +
                    "            NT.STATUS = 'CLOSED'\n" +
                    "            AND SS.TYPE = NT.SELF_TYPE\n" +
                    "            AND SS.TRANSACTION_ID= NT.REQUEST_ID\n" +
           " AND ((NT.SELF_TYPE =NVL(?,NT.SELF_TYPE)) AND (SS.PERSON_NUMBER =NVL(?,SS.PERSON_NUMBER)) AND (NT.TYPE =NVL(?,NT.TYPE)) and ( NT.NATIONALIDENTITY =? or ? is null) AND (SS.STATUS =? OR  ? is null) AND (NT.STATUS =? OR ? is null))\n" +
                    "            AND (NT.TYPE = 'FYI' OR( NT.TYPE = 'FYA' and NT.STATUS = 'OPEN')) \n" +
                    "            AND ((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = ?) OR \n" +
                    "                 (NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = ?) OR                 \n" +
                    "(NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = ?) OR \n" +
                    "(NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = ?) OR \n" +
                    "				 (NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = ?)\n" +
                    "                )\n" +
                    "			\n" +
                    "				) ORDER BY CREATION_DATE DESC";

            ps = connection.prepareStatement(query);
            ps.setString(1, self_type);
            ps.setString(2, personNumber);
            ps.setString(3, type);
            ps.setString(4, nationalIdentity);
            ps.setString(5, nationalIdentity);
            ps.setString(6, status);
            ps.setString(7, status);
            ps.setString(8, status);
            ps.setString(9, status);
            
            ps.setString(10, MANAGER_ID);
            ps.setString(11, MANAGER_ID);
            ps.setString(12, MANAGER_ID);
            ps.setString(13, emp);
            ps.setString(14, MANAGER_ID);
            ps.setString(15, position);
            ps.setString(16, emp);
            ps.setString(17, MANAGER_ID);
            int arrayIndex = 0;
            int psIndex = 18;
            for (int i = 18; i < 18 + userRolesCount; i++) {
                ps.setString(i, userRoles.get(arrayIndex).toString());
                arrayIndex++;
                psIndex++;
            }
            ps.setString(psIndex, self_type);
            psIndex++;
            ps.setString(psIndex, personNumber);
            psIndex++;
            ps.setString(psIndex, type);
            psIndex++;
            ps.setString(psIndex, nationalIdentity);
            psIndex++;
            ps.setString(psIndex, nationalIdentity);
            psIndex++;
            ps.setString(psIndex, status);
            psIndex++;
            ps.setString(psIndex, status);
            psIndex++;
            ps.setString(psIndex, status);
            psIndex++;
            ps.setString(psIndex, status);
            psIndex++;
            ps.setString(psIndex, MANAGER_ID);
            psIndex++;
            ps.setString(psIndex, position);
            psIndex++;
            ps.setString(psIndex, MANAGER_ID);
            psIndex++;
            ps.setString(psIndex, emp);
            psIndex++;
            ps.setString(psIndex, emp);
            //();
            rs = ps.executeQuery();
            NotificationBean obj = null;
            while (rs.next()) {
                obj = new NotificationBean();
                obj.setID(rs.getString("ID"));
                //  obj.put("ID", rs.getString("ID"));
                obj.setMSG_TITLE(rs.getString("MSG_TITLE"));
                obj.setMSG_BODY(rs.getString("MSG_BODY"));
                obj.setCREATION_DATE(rs.getString("CREATION_DATE"));
                obj.setRECEIVER_TYPE(rs.getString("RECEIVER_TYPE"));
                obj.setRECEIVER_ID(rs.getString("RECEIVER_ID"));
                obj.setRESPONSE_PERSON_ID(rs.getString("RESPONSE_PERSON_ID"));
                obj.setTYPE(rs.getString("TYPE"));
                obj.setRESPONSE_DATE(rs.getString("RESPONSE_DATE"));
                obj.setWORKFLOW_ID(rs.getString("WORKFLOW_ID"));
                obj.setREQUEST_ID(rs.getString("REQUEST_ID"));
                obj.setSTATUS(rs.getString("STATUS"));
                obj.setPERSON_NUMBER(rs.getString("PERSON_ID"));
                obj.setSELF_TYPE(rs.getString("SELF_TYPE"));
                obj.setRESPONSE_PERSON_ID(rs.getString("PERSON_NUMBER"));
                obj.setPERSON_NAME(rs.getString("PERSON_NAME"));
                ////(rs.getString("ID"));
                notificationList.add(obj);
                // arr.put(obj);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return notificationList;
    }

    public JSONArray getWorkFlowNotification(String MANAGER_ID,
                                             String managerOfManager,
                                             String position, String emp,
                                             String self_type,
                                             String requset_id, String type) {
        JSONArray arr = new JSONArray();

        if (self_type.equals("undefined")) {
            self_type = "";
        }
        if (requset_id.equals("undefined")) {
            requset_id = "";
        }
        if (type.equals("undefined")) {
            type = "";
        }
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT * FROM \n" +
                    "(SELECT NT.*,SS.PERSON_NUMBER FROM   " + " " +
                    getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION NT, " + " " +
                    getSchema_Name() + ".XX_SELF_SERVICE SS\n" +
                    //NT.STATUS = 'OPEN' AND
                    "WHERE  SS.TYPE = NT.SELF_TYPE AND SS.TRANSACTION_ID= NT.REQUEST_ID AND\n" +
                    "((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'LINE_MANAGER+1' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR \n" +
                    "(NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR \n" +
                    "(NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) AND\n" +
                    "(NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) AND\n" +
                    "(NT.SELF_TYPE = NVL(?, NT.SELF_TYPE) AND NT.REQUEST_ID = NVL(?, NT.REQUEST_ID) AND NT.TYPE = NVL(?, NT.TYPE)))\n AND NT.STATUS = NVL(?, NT.STATUS)))\n" +
                    "UNION\n" +
                    "SELECT NT.*,SS.PERSON_NUMBER FROM  " + " " +
                    getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION NT, " + " " +
                    getSchema_Name() + ".XX_SELF_SERVICE SS\n" +
                    "WHERE NT.STATUS = 'CLOSED' AND SS.TYPE = NT.SELF_TYPE AND SS.TRANSACTION_ID= NT.REQUEST_ID AND\n" +
                    "NT.TYPE = 'FYI' AND\n" +
                    "((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)) OR\n" +
                    "(NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = NVL(?, NT.RECEIVER_ID)))) \n" +
                    "ORDER BY CREATION_DATE DESC";
            ps = connection.prepareStatement(query);
            ps.setString(1, MANAGER_ID);
            ps.setString(2, MANAGER_ID);
            ps.setString(3, MANAGER_ID);
            ps.setString(4, position);
            ps.setString(5, emp);
            ps.setString(6, emp);
            ps.setString(7, self_type);
            ps.setString(8, requset_id);
            ps.setString(9, type);
            ps.setString(10, MANAGER_ID);
            ps.setString(11, position);
            ps.setString(12, MANAGER_ID);
            ps.setString(13, emp);
            ps.setString(14, emp);
            rs = ps.executeQuery();

            while (rs.next()) {
                JSONObject obj = new JSONObject();
                obj.put("ID", rs.getString("ID"));
                //  obj.put("ID", rs.getString("ID"));
                obj.put("MSG_TITLE", rs.getString("MSG_TITLE"));
                obj.put("MSG_BODY", rs.getString("MSG_BODY"));
                obj.put("CREATION_DATE", rs.getString("CREATION_DATE"));
                obj.put("RECEIVER_TYPE", rs.getString("RECEIVER_TYPE"));
                obj.put("RECEIVER_ID", rs.getString("RECEIVER_ID"));
                obj.put("RESPONSE_PERSON_ID",
                        rs.getString("RESPONSE_PERSON_ID"));
                obj.put("TYPE", rs.getString("TYPE"));
                obj.put("RESPONSE_DATE", rs.getString("RESPONSE_DATE"));
                obj.put("WORKFLOW_ID", rs.getString("WORKFLOW_ID"));
                obj.put("REQUEST_ID", rs.getString("REQUEST_ID"));
                obj.put("STATUS", rs.getString("STATUS"));
                obj.put("PERSON_NUMBER", rs.getString("PERSON_NUMBER"));
                obj.put("SELF_TYPE", rs.getString("SELF_TYPE"));
                ////(rs.getString("ID"));

                arr.put(obj);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return arr;
    }

    public void updateWorkflownotification(String PERSON_ID, String STATUS,
                                           String TRS_ID, String ssType,
                                           String workFlowId) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "UPDATE  " + " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION \n" +
                    "    SET\n" +
                    "        RESPONSE_PERSON_ID = ?,\n" +
                    "        RESPONSE_DATE = SYSDATE,\n" +
                    "        STATUS = ?\n" +
                    "     WHERE\n" +
                    "         REQUEST_ID = ?\n" +
                    "         AND SELF_TYPE = ?\n" +
                    "          AND ID= ?";

            ps = connection.prepareStatement(query);
            ps.setString(1, PERSON_ID);
            ps.setString(2, STATUS);
            ps.setString(3, TRS_ID);
            ps.setString(4, ssType);
            ps.setString(5, workFlowId);

            //(" query = " + query);

            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }

    public void workflowAction(String action) {
        PeopleExtraInformationDAO extraInformation =
            new PeopleExtraInformationDAO();
        JSONObject jsonObj = new JSONObject(action);
        String v_check = "YES";
        boolean v_check2 = true;
        String V_STEP_LEVEL;
        ApprovalListBean approvalListBean = new ApprovalListBean();
        ApprovalListDAO approvalDao = new ApprovalListDAO();
        SelfServiceDAO selfServiceDao = new SelfServiceDAO();

        try {
            V_STEP_LEVEL =
                    approvalDao.getMinStepLeval(jsonObj.getString("TRS_ID"),
                                                jsonObj.getString("ssType"));
            //("Min Step Leval : " + V_STEP_LEVEL);
            approvalListBean =
                    approvalDao.getNextApproval(jsonObj.getString("TRS_ID"),
                                                V_STEP_LEVEL,
                                                jsonObj.getString("ssType"));


            approvalDao.updateApprovalList(jsonObj.getString("RESPONSE_CODE"),
                                           jsonObj.getString("TRS_ID"),
                                           V_STEP_LEVEL,
                                           jsonObj.getString("ssType"),
                                           jsonObj.isNull("rejectReason") ?
                                           "" :
                                           jsonObj.getString("rejectReason"));
            this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                            "CLOSED",
                                            jsonObj.getString("TRS_ID"),
                                            jsonObj.getString("ssType"),
                                            jsonObj.getString("workflowId"));
            if (jsonObj.getString("RESPONSE_CODE").equals("REJECTED")) {
                if(jsonObj.getString("ssType").equals("XXX_PROBATION_PERIOD")){
                    String managerId = this.updateWorkflownotificationProbationPeriod(jsonObj.getString("TRS_ID"),
                                            jsonObj.getString("ssType"));
                    approvalDao.updateApprovalListPP(jsonObj.getString("RESPONSE_CODE"),
                                                   jsonObj.getString("TRS_ID"),
                                                   V_STEP_LEVEL,
                                                   jsonObj.getString("ssType"),
                                                   jsonObj.isNull("rejectReason") ?
                                                   "" :
                                                   jsonObj.getString("rejectReason"));
                    extraInformation.updateRejectReason(jsonObj.getString("rejectReason"),
                                                  Integer.parseInt(jsonObj.getString("TRS_ID")));
                    EmployeeBean empObj =
                        employeeDetails.getEmpNameAndEmail(managerId, "", "");
                    String body = "Dear " + empObj.getDisplayName() +",\n"+" Kindly note that the probation period request for " + jsonObj.opt("PERSON_NAME") + " has been rejected. Please go to notification on ERP Self-Services and re-submit.";
                    System.out.println(body);
                    
                    String subject = "Status of probation period for " + jsonObj.opt("PERSON_NAME");
                    System.out.println(subject);
                    mailObj.SEND_MAIL(empObj.getEmail(),
                                              subject, body);
                    
                }
                else if(jsonObj.getString("ssType").equals("XXX_HANDOVER_REQUEST")){
                    selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
                                                     jsonObj.getString("ssType"),
                                                     jsonObj.getString("TRS_ID"));
                    extraInformation.updateStatus("REJECTED",
                                                  jsonObj.getString("rejectReason"),
                                                  Integer.parseInt(jsonObj.getString("TRS_ID")));
                    PeopleExtraInformationBean reqBean =
                        extraInformation.getEITById(jsonObj.getString("TRS_ID"));
                    String selfServiceName = "handover request";
                    String persionId = jsonObj.opt("PERSON_EMP_FYI_ID").toString();
                    EmployeeBean empObj =
                        employeeDetails.getEmpDetailsByPersonId(persionId, "", "");
                    extraInfoForEmail.put("Reciver",
                                          jsonObj.opt("PERSON_NAME").toString());
                    extraInfoForEmail.put("EIT_NAME",
                                          selfServiceName);
                    extraInfoForEmail.put("employeeName",
                                          jsonObj.opt("PERSON_NAME").toString());
                    extraInfoForEmail.put("STATUS",
                                          "rejected");
                    peib.setEit_name(selfServiceName);
                    peib.setPersonName(jsonObj.opt("PERSON_NAME").toString());
                    String subject = "Status of handover for " + jsonObj.opt("PERSON_NAME");
                    System.out.println(subject);
                    messageBean =
                            NotificationUtility.buildEmailBody(null,
                                                               extraInfoForEmail,
                                                               "XXX",
                                                               "FYI");
                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                               subject,
                                                               messageBean.getEmailBody(),
                                                               messageBean.getAttachment(),
                                                               messageBean.getFileName(),
                                                               "FYI",
                                                               null,
                                                               selfServiceName,
                                                               messageBean.getHaveAtt() !=
                                                               null &&
                                                               "Y".equals(messageBean.getHaveAtt()) ?
                                                               true : false,
                                                               peib);
                } else if (jsonObj.getString("ssType").equals("XXX_EDU_EXPENSE_REQUEST")) {
                    selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
                                                     jsonObj.getString("ssType"),
                                                     jsonObj.getString("TRS_ID"));
                    extraInformation.updateStatus("REJECTED",
                                                  jsonObj.getString("rejectReason"),
                                                  Integer.parseInt(jsonObj.getString("TRS_ID")));
                    PeopleExtraInformationBean reqBean =
                        extraInformation.getEITById(jsonObj.getString("TRS_ID"));
                    String selfServiceName = "Education Expense request";
                    String persionId =
                        jsonObj.opt("PERSON_EMP_FYI_ID").toString();
                    EmployeeBean empObj =
                        employeeDetails.getEmpDetailsByPersonId(persionId, "",
                                                                "");
                    extraInfoForEmail.put("Reciver",
                                          jsonObj.opt("PERSON_NAME").toString());
                    extraInfoForEmail.put("EIT_NAME", selfServiceName);
                    extraInfoForEmail.put("employeeName",
                                          jsonObj.opt("PERSON_NAME").toString());
                    extraInfoForEmail.put("STATUS", "rejected");
                    peib.setEit_name(selfServiceName);
                    peib.setPersonName(jsonObj.opt("PERSON_NAME").toString());
                    String subject =
                        "Status of Education Expense for " + jsonObj.opt("PERSON_NAME");
                    System.out.println(subject);
                    messageBean =
                            NotificationUtility.buildEmailBody(null, extraInfoForEmail,
                                                               "XXX", "FYI");
                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                               subject,
                                                               messageBean.getEmailBody(),
                                                               messageBean.getAttachment(),
                                                               messageBean.getFileName(),
                                                               "FYI", null,
                                                               selfServiceName,
                                                               messageBean.getHaveAtt() !=
                                                               null &&
                                                               "Y".equals(messageBean.getHaveAtt()) ?
                                                               true : false,
                                                               peib);
                }
                else{
                selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
                                                 jsonObj.getString("ssType"),
                                                 jsonObj.getString("TRS_ID"));
                extraInformation.updateStatus("REJECTED",
                                              jsonObj.getString("rejectReason"),
                                              Integer.parseInt(jsonObj.getString("TRS_ID")));
                PeopleExtraInformationBean reqBean =
                    extraInformation.getEITById(jsonObj.getString("TRS_ID"));
                String selfServiceName = reqBean.getEit_name();
                String persionId = reqBean.getPerson_id();
                String body = rh.getEmailBodyForStatusRequest(jsonObj.getString("RESPONSE_CODE"), selfServiceName, jsonObj.get("PERSON_NAME").toString());
                                System.out.println(body);
                                
                                EmployeeBean empObj =
                                    employeeDetails.getEmpDetailsByPersonId(persionId, "", "");
                                String subject = rh.getEmailSubjectForStatusRequest(selfServiceName);
                                System.out.println(subject);
                mailObj.SEND_NOTIFICATION(empObj.getEmail(),
                                          subject, body, null, null,
                                          "FYI", jsonObj.getString("TRS_ID"),
                                         selfServiceName, false, reqBean);
            }
            }
            V_STEP_LEVEL =
                    approvalDao.getMinStepLeval(jsonObj.getString("TRS_ID"),
                                                jsonObj.getString("ssType"));
            //("Next Approval  Step Leval : " + V_STEP_LEVEL);
            approvalListBean =
                    approvalDao.getNextApproval(jsonObj.getString("TRS_ID"),
                                                V_STEP_LEVEL,
                                                jsonObj.getString("ssType"));

            if (approvalListBean.getId() == null) {
                //("Inside Null ");
                selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
                                                 jsonObj.getString("ssType"),
                                                 jsonObj.getString("TRS_ID"));
                v_check = "NO";
            }
            if (!v_check.equalsIgnoreCase("NO")) {
                if (jsonObj.getString("RESPONSE_CODE").equals("APPROVED")) {
                    if (approvalListBean.getNotificationType().equals("FYI")) {
                        while (v_check2) {
                            if (approvalListBean.getNotificationType().equals("FYI")) {
                                WorkFlowNotificationBean wfnBean =
                                    new WorkFlowNotificationBean();
                                wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                                wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                                wfnBean.setReceiverType(approvalListBean.getRolrType());
                                wfnBean.setReceiverId(approvalListBean.getRoleId());
                                wfnBean.setType(approvalListBean.getNotificationType());
                                wfnBean.setRequestId(approvalListBean.getTransActionId());
                                wfnBean.setStatus("OPEN");
                                wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                                wfnBean.setSelfType(approvalListBean.getServiceType());
                                //("W Notification : ");
                                //(new JSONObject(wfnBean));
                                this.insertIntoWorkflow(wfnBean);
                                if(jsonObj.getString("ssType").equals("XXX_HANDOVER_REQUEST")){
                                    sendEmailForHandover("Handover",approvalListBean.getRolrType(),
                                              approvalListBean.getNotificationType(),
                                              approvalListBean.getTransActionId(),
                                              jsonObj.get("PERSON_NAME").toString(),
                                              approvalListBean.getRoleId(),
                                              jsonObj.getString("RESPONSE_CODE"));
                                    handoverDAO handDao = new handoverDAO();
                                    HandoverHeaderBean headerBean = new HandoverHeaderBean();
                                    headerBean.setStatus(jsonObj.opt("RESPONSE_CODE").toString());
                                    headerBean.setUpdateBy(jsonObj.opt("PERSON_ID").toString());
                                    headerBean.setHeaderId(Integer.parseInt(jsonObj.opt("TRS_ID").toString()));
                                    handDao.updateHandoverStatus(headerBean);

                                }else if(jsonObj.getString("ssType").equals("XXX_EDU_EXPENSE_REQUEST")){
                                    sendEmailForHandover("Education",approvalListBean.getRolrType(),
                                              approvalListBean.getNotificationType(),
                                              approvalListBean.getTransActionId(),
                                              jsonObj.get("PERSON_NAME").toString(),
                                              approvalListBean.getRoleId(),
                                              jsonObj.getString("RESPONSE_CODE"));

                                }
                                else if(jsonObj.getString("ssType").equals("XXX_PROBATION_PERIOD")){
                                    selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
                                                                     jsonObj.getString("ssType"),
                                                                     jsonObj.getString("TRS_ID"));
                                    extraInformation.updateStatus(jsonObj.getString("RESPONSE_CODE"),
                                                                  "",
                                                                  Integer.parseInt(jsonObj.getString("TRS_ID")));
                                    sendEmailForHandover("Probation",approvalListBean.getRolrType(),
                                              approvalListBean.getNotificationType(),
                                              approvalListBean.getTransActionId(),
                                              jsonObj.get("PERSON_NAME").toString(),
                                              approvalListBean.getRoleId(),
                                              jsonObj.getString("RESPONSE_CODE"));
                                }
                                else {
                                sendEmail(approvalListBean.getRolrType(),
                                          approvalListBean.getNotificationType(),
                                          approvalListBean.getTransActionId(),
                                          jsonObj.get("PERSON_NAME").toString(),
                                          approvalListBean.getRoleId(),jsonObj.getString("RESPONSE_CODE"));
                                }
                                approvalDao.updateReqeustDate(approvalListBean.getId());
                                approvalDao.getMinStepLeval(approvalListBean.getTransActionId(),
                                                            approvalListBean.getServiceType());
                                approvalDao.updateApprovalList("Delivered",
                                                               approvalListBean.getTransActionId(),
                                                               approvalListBean.getStepLeval(),
                                                               approvalListBean.getServiceType(),
                                                               "");
                                this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                                                "CLOSED",
                                                                jsonObj.getString("TRS_ID"),
                                                                jsonObj.getString("ssType"),
                                                                jsonObj.getString("workflowId"));
                                V_STEP_LEVEL =
                                        approvalDao.getMinStepLeval(approvalListBean.getTransActionId(),
                                                                    approvalListBean.getServiceType());
                                approvalListBean =
                                        approvalDao.getNextApproval(approvalListBean.getTransActionId(),
                                                                    V_STEP_LEVEL,
                                                                    approvalListBean.getServiceType());

                                if (approvalListBean.getId() == null) {
                                    v_check2 = false;
                                }


                            } else {
                                v_check2 = false;
                                WorkFlowNotificationBean wfnBean =
                                    new WorkFlowNotificationBean();
                                wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                                wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                                wfnBean.setReceiverType(approvalListBean.getRolrType());
                                wfnBean.setReceiverId(approvalListBean.getRoleId());
                                wfnBean.setType(approvalListBean.getNotificationType());
                                wfnBean.setRequestId(approvalListBean.getTransActionId());
                                wfnBean.setStatus("OPEN");
                                wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                                wfnBean.setSelfType(approvalListBean.getServiceType());
                                this.insertIntoWorkflow(wfnBean);
                                sendEmail(approvalListBean.getRolrType(),
                                          approvalListBean.getNotificationType(),
                                          approvalListBean.getTransActionId(),
                                          jsonObj.get("PERSON_NAME").toString(),
                                          approvalListBean.getRoleId(),jsonObj.getString("RESPONSE_CODE"));
                                approvalDao.updateReqeustDate(approvalListBean.getId());
                                
                            }
                        }
                    } else {
                        WorkFlowNotificationBean wfnBean =
                            new WorkFlowNotificationBean();
                        wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                        wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                        wfnBean.setReceiverType(approvalListBean.getRolrType());
                        wfnBean.setReceiverId(approvalListBean.getRoleId());
                        wfnBean.setType(approvalListBean.getNotificationType());
                        wfnBean.setRequestId(approvalListBean.getTransActionId());
                        wfnBean.setStatus("OPEN");
                        wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                        wfnBean.setSelfType(approvalListBean.getServiceType());
                        this.insertIntoWorkflow(wfnBean);
                        if(jsonObj.getString("ssType").equals("XXX_HANDOVER_REQUEST")){
                            sendEmailForHandover("Handover",approvalListBean.getRolrType(),
                                      approvalListBean.getNotificationType(),
                                      approvalListBean.getTransActionId(),
                                      jsonObj.get("PERSON_NAME").toString(),
                                      approvalListBean.getRoleId(),
                                      jsonObj.getString("RESPONSE_CODE"));
                            
                            approvalDao.updateReqeustDate(approvalListBean.getId());
                        }else if (jsonObj.getString("ssType").equals("XXX_EDU_EXPENSE_REQUEST")) {
                            sendEmailForHandover("Education",
                                                 approvalListBean.getRolrType(),
                                                 approvalListBean.getNotificationType(),
                                                 approvalListBean.getTransActionId(),
                                                 jsonObj.get("PERSON_NAME").toString(),
                                                 approvalListBean.getRoleId(),
                                                 jsonObj.getString("RESPONSE_CODE"));

                            approvalDao.updateReqeustDate(approvalListBean.getId());
                        }else if (jsonObj.getString("ssType").equals("XXX_PROBATION_PERIOD")) {
                            sendEmailForHandover("Probation",
                                                 approvalListBean.getRolrType(),
                                                 approvalListBean.getNotificationType(),
                                                 approvalListBean.getTransActionId(),
                                                 jsonObj.get("PERSON_NAME").toString(),
                                                 approvalListBean.getRoleId(),
                                                 jsonObj.getString("RESPONSE_CODE"));

                            approvalDao.updateReqeustDate(approvalListBean.getId());
                        }

                        else{
                        sendEmail(approvalListBean.getRolrType(),
                                  approvalListBean.getNotificationType(),
                                  approvalListBean.getTransActionId(),
                                  jsonObj.get("PERSON_NAME").toString(),
                                  approvalListBean.getRoleId(),jsonObj.getString("RESPONSE_CODE"));


                        approvalDao.updateReqeustDate(approvalListBean.getId());
                        }

                    }
                }
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

    }

    public ArrayList<WorkFlowNotificationBean> getWorkFlowNotificationByFilter(String self_type,
                                                                               String request_id,
                                                                               String type,
                                                                               String receiver_id) {
        ArrayList<WorkFlowNotificationBean> list =
            new ArrayList<WorkFlowNotificationBean>();

        WorkFlowNotificationBean obj = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "select NT.*, ss.person_number from  " + " " + getSchema_Name() +
                    ".XX_WORKFLOW_NOTIFICATION NT INNER JOIN  " + " " +
                    getSchema_Name() + ".XX_SELF_SERVICE SS \n" +
                    "ON nt.self_type = ss.type AND nt.request_id = ss.transaction_id\n" +
                    "WHERE NT.SELF_TYPE LIKE ? AND NT.REQUEST_ID LIKE ? AND NT.TYPE LIKE ? AND nt.receiver_id = ? AND nt.status NOT IN ('CLOSED')";

            if (request_id.equals("undefined") || type.equals("undefined")) {
                request_id = "";
                type = "";
            }

            ps = connection.prepareStatement(query);
            ps.setString(1, "%" + self_type + "%");
            ps.setString(2, "%" + request_id + "%");
            ps.setString(3, "%" + type + "%");
            ps.setString(4, receiver_id);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj = new WorkFlowNotificationBean();
                obj.setID(rs.getString("ID"));
                obj.setMsgTitle(rs.getString("MSG_TITLE"));
                obj.setMsgBody(rs.getString("MSG_BODY"));
                obj.setCreationDate(rs.getString("CREATION_DATE"));
                obj.setReceiverType(rs.getString("RECEIVER_TYPE"));
                obj.setReceiverId(rs.getString("RECEIVER_ID"));
                obj.setResponsePersonId(rs.getString("RESPONSE_PERSON_ID"));
                obj.setType(rs.getString("TYPE"));
                obj.setResponseDate(rs.getString("RESPONSE_DATE"));
                obj.setWorkflowId(rs.getString("WORKFLOW_ID"));
                obj.setRequestId(rs.getString("REQUEST_ID"));
                obj.setStatus(rs.getString("STATUS"));
                obj.setPersonNumber(rs.getString("PERSON_NUMBER"));
                obj.setSelfType(rs.getString("SELF_TYPE"));
                ////(rs.getString("ID"));

                list.add(obj);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    //************************Search Notification*******************************

    public ArrayList<NotificationBean> getSearchWorkFlowNotification(String MANAGER_ID,
                                                                     String managerOfManager,
                                                                     String position,
                                                                     String emp,
                                                                     String self_type,
                                                                     String personNumber,
                                                                     String requset_id,
                                                                     String type,
                                                                     String nationalIdentity,
                                                                     String status) {


        String ssStatus = null;
        String ntStatus = null;
        ArrayList<NotificationBean> list = new ArrayList<NotificationBean>();
        NotificationBean obj = null;
        String operator = null;
        AppsproConnection.LOG.info(self_type);
        if (self_type.equals("")) {
            self_type = null;
        }
        if (requset_id.equals("")) {
            requset_id = null;
        }
        if (type.equals("")) {
            type = null;
        }
        if (nationalIdentity.isEmpty() || nationalIdentity.equals("") ||
            nationalIdentity.equals("")) {
            nationalIdentity = null;
            operator = "OR";
        } else {
            operator = "AND";
        }

        if (("APPROVED").equalsIgnoreCase(status)) {
            ssStatus = "APPROVED";
            ntStatus = "CLOSED";
        } else if (("REJECT").equalsIgnoreCase(status)) {
            ssStatus = "REJECTED";
            ntStatus = "CLOSED";
        } else if (("PENDDING APPROVED").equalsIgnoreCase(status)) {
            ssStatus = null;
            ntStatus = "OPEN";
        }
        else if(("SELECT_ALL_1").equalsIgnoreCase(status)){
            ssStatus = null;
            ntStatus = "OPEN";
        }
        else if(status.equals(null) || status.equals("")){
            ssStatus = null;
            ntStatus = "OPEN";
        }
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM (SELECT NT.*,SS.PERSON_ID,SS.PERSON_NUMBER FROM " +
                    " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION NT " +
                    " INNER JOIN \n" +
                    " " + getSchema_Name() + ".XX_SELF_SERVICE SS\n" +
                    "ON SS.TRANSACTION_ID= NT.REQUEST_ID \n" +
                    "WHERE     SS.TYPE = NT.SELF_TYPE  AND SS.TRANSACTION_ID= NT.REQUEST_ID AND ((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = ?) OR\n" +
                    "   (NT.RECEIVER_TYPE = 'LINE_MANAGER+1' AND NT.RECEIVER_ID = ?) OR\n" +
                    "    (NT.RECEIVER_TYPE = 'POSITION' AND NT.RECEIVER_ID = ?) OR\n" +
                    "  (NT.RECEIVER_TYPE = 'LINE_MANAGER+' AND NT.RECEIVER_ID = ?)OR\n" +
                    " (NT.RECEIVER_TYPE = 'JOB_LEVEL' AND NT.RECEIVER_ID = ?) OR\n" +
                    " (NT.RECEIVER_TYPE = 'AOR' AND NT.RECEIVER_ID = ?)OR\n" +
                    "(NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = ?))AND\n" +
                    "((NT.SELF_TYPE =? or ? is null) AND (SS.PERSON_NUMBER =? or ? is null) AND (NT.TYPE =? or ? is null) and " +
                    "( NT.NATIONALIDENTITY =? or ? is null) AND (SS.STATUS =? OR  ? is null) AND (NT.STATUS =? OR ? is null)))";
            System.out.println(query);
            ps = connection.prepareStatement(query);

            ps.setString(1, MANAGER_ID);
            ps.setString(2, MANAGER_ID);
            ps.setString(3, position);
            ps.setString(4, MANAGER_ID);
            ps.setString(5, MANAGER_ID);
            ps.setString(6, emp);
            ps.setString(7, emp);
            ps.setString(8, self_type);
            ps.setString(9, self_type);
            ps.setString(10, personNumber);
            ps.setString(11, personNumber);
            ps.setString(12, type);
            ps.setString(13, type);
            ps.setString(14, nationalIdentity);
            ps.setString(15, nationalIdentity);
            ps.setString(16, ssStatus);
            ps.setString(17, ssStatus);
            ps.setString(18, ntStatus);
            ps.setString(19, ntStatus);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj = new NotificationBean();
                obj.setID(rs.getString("ID"));
                //  obj.put("ID", rs.getString("ID"));
                obj.setMSG_TITLE(rs.getString("MSG_TITLE"));
                obj.setMSG_BODY(rs.getString("MSG_BODY"));
                obj.setCREATION_DATE(rs.getString("CREATION_DATE"));
                obj.setRECEIVER_TYPE(rs.getString("RECEIVER_TYPE"));
                obj.setRECEIVER_ID(rs.getString("RECEIVER_ID"));
                obj.setRESPONSE_PERSON_ID(rs.getString("RESPONSE_PERSON_ID"));
                obj.setTYPE(rs.getString("TYPE"));
                obj.setRESPONSE_DATE(rs.getString("RESPONSE_DATE"));
                obj.setWORKFLOW_ID(rs.getString("WORKFLOW_ID"));
                obj.setREQUEST_ID(rs.getString("REQUEST_ID"));
                obj.setSTATUS(rs.getString("STATUS"));
                obj.setPERSON_NUMBER(rs.getString("PERSON_ID"));
                obj.setSELF_TYPE(rs.getString("SELF_TYPE"));
                obj.setRESPONSE_PERSON_ID(rs.getString("PERSON_NUMBER"));
                obj.setPERSON_NAME(rs.getString("PERSON_NAME"));
                ////(rs.getString("ID"));

                list.add(obj);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }


    public void sendEmail(String approvalType, String notificationType,
                          String transactionId, String personName,
                          String roleId,String responsCode) throws AppsProException {
        PeopleExtraInformationDAO extraInformation =
            new PeopleExtraInformationDAO();
        PeopleExtraInformationBean reqBean =
            extraInformation.getEITById(transactionId);
        String selfServiceName = reqBean.getEit_name();
        EmployeeBean empObj = new EmployeeBean();
        String eitLBL =
            reqBean.getEitLbl().toString();
        eitLBL = eitLBL.replaceAll("\\\\", "");
        eitLBL = eitLBL.substring(1, eitLBL.length() - 1);
        //employeeId = mean person you went to send email to
        //roleid = mean the role id if approval type is role
        //employee id if line manager or line manager +1
        //position id if approval type is position
        if (approvalType.equals("LINE_MANAGER")) {
            try {

                empObj =
                        employeeDetails.getEmpDetailsByPersonId(roleId, "", "");
//                String body =
//                    rh.getEmailBody(notificationType, empObj.getDisplayName(),
//                                    selfServiceName, personName);
                String subject =
                    rh.getEmailSubject(notificationType, selfServiceName,
                                       personName);
                extraInfoForEmail.put("Reciver",
                                      empObj.getDisplayName());
                extraInfoForEmail.put("EIT_NAME",
                                      reqBean.getEit_name());
                extraInfoForEmail.put("employeeName",
                                      personName);
                extraInfoForEmail.put("STATUS",
                                      responsCode);
                messageBean =
                        NotificationUtility.buildEmailBody(new JSONObject(eitLBL),
                                                           extraInfoForEmail,
                                                           reqBean.getCode(),
                                                           notificationType);
                
                CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                           subject,
                                                           messageBean.getEmailBody(),
                                                           null,
                                                           null,
                                                           notificationType,
                                                           transactionId,
                                                           selfServiceName,
                                                           false,
                                                           reqBean);


//                mailObj.SEND_NOTIFICATION(empObj.getEmail(), subject, body,
//                                          null, null, notificationType,
//                                          transactionId, selfServiceName,
//                                          false, reqBean);
            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        } else if (approvalType.equals("LINE_MANAGER+1")) {
            try {
                empObj =
                        employeeDetails.getEmpDetailsByPersonId(roleId, "", "");
                String body =
                    rh.getEmailBody(notificationType, empObj.getDisplayName(),
                                    selfServiceName, personName);
                String subject =
                    rh.getEmailSubject(notificationType, selfServiceName,
                                       personName);
                mailObj.SEND_NOTIFICATION(empObj.getEmail(), subject, body,
                                          null, null, notificationType,
                                          transactionId, selfServiceName,
                                          false, reqBean);
            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        } else if (approvalType.equals("ROLES")) {
            try {
                Map<String, String> paramMAp = new HashMap<String, String>();
                paramMAp.put("P_ROLEID", roleId);
                JSONObject respone =
                    BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                            paramMAp);

                JSONObject dataDS = respone.getJSONObject("DATA_DS");
                JSONArray g1 = new JSONArray();
                Object jsonTokner =
                    new JSONTokener(dataDS.get("G_1").toString()).nextValue();
                if (jsonTokner instanceof JSONObject) {
                    JSONObject arrJson = dataDS.getJSONObject("G_1");

                    g1.put(arrJson);
                } else {
                    JSONArray arrJson = dataDS.getJSONArray("G_1");
                    g1 = arrJson;
                }
                for (int i = 0; i < g1.length(); i++) {
                    JSONObject data = (JSONObject)g1.get(i);
//                    String body =
//                        rh.getEmailBody(notificationType, data.getString("FULL_NAME"),
//                                        selfServiceName, personName);
                    String subject =
                        rh.getEmailSubject(notificationType, selfServiceName,
                                           personName);
                    extraInfoForEmail.put("Reciver",
                                          data.getString("FULL_NAME"));
                    extraInfoForEmail.put("EIT_NAME",
                                          reqBean.getEit_name());
                    extraInfoForEmail.put("employeeName",
                                          reqBean.getPersonName());
                    extraInfoForEmail.put("STATUS",
                                          responsCode);
                    messageBean =
                            NotificationUtility.buildEmailBody(new JSONObject(eitLBL),
                                                               extraInfoForEmail,
                                                               reqBean.getCode(),
                                                               notificationType);
                    
                    CredentialStoreClassMail.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
                                                               subject,
                                                               messageBean.getEmailBody(),
                                                               null,
                                                               null,
                                                               notificationType,
                                                               transactionId,
                                                               selfServiceName,
                                                               false,
                                                               reqBean);
//                    mailObj.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
//                                              subject, body, null, null,
//                                              notificationType, transactionId,
//                                              selfServiceName, false, reqBean);
                }

            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        } else if (approvalType.equals("POSITION")) {
            try {
                Map<String, String> paramMAp = new HashMap<String, String>();
                paramMAp.put("positionId", roleId);
                JSONObject respone =
                    BIReportModel.runReport("EmailForPositionsReport",
                                            paramMAp);
                JSONObject dataDS = respone.getJSONObject("DATA_DS");
                JSONArray g1 = new JSONArray();
                Object jsonTokner =
                    new JSONTokener(dataDS.get("G_1").toString()).nextValue();
                if (jsonTokner instanceof JSONObject) {
                    JSONObject arrJson = dataDS.getJSONObject("G_1");

                    g1.put(arrJson);
                } else {
                    JSONArray arrJson = dataDS.getJSONArray("G_1");
                    g1 = arrJson;
                }


                for (int i = 0; i < g1.length(); i++) {
                    JSONObject data = (JSONObject)g1.get(i);
                    if(notificationType.equals("FYI") && responsCode.equals("APPROVED")){
//                        String body =
//                            rh.getEmailBodyForStatusRequestFYIPosition(responsCode,
//                                            selfServiceName,data.getString("DISPLAY_NAME"),reqBean.getPersonName());
                        String subject =
                            rh.getEmailSubject(notificationType, selfServiceName,
                                               personName);
                        
                        extraInfoForEmail.put("Reciver",
                                              data.getString("DISPLAY_NAME"));
                        extraInfoForEmail.put("EIT_NAME",
                                              reqBean.getEit_name());
                        extraInfoForEmail.put("employeeName",
                                              personName);
                        extraInfoForEmail.put("STATUS",
                                              responsCode);
                        
                        messageBean =
                                NotificationUtility.buildEmailBody(new JSONObject(eitLBL),
                                                                   extraInfoForEmail,
                                                                   reqBean.getCode(),
                                                                   notificationType);
                        
                        CredentialStoreClassMail.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
                                                                   subject,
                                                                   messageBean.getEmailBody(),
                                                                   null,
                                                                   null,
                                                                   notificationType,
                                                                   transactionId,
                                                                   selfServiceName,
                                                                   false,
                                                                   reqBean);
//                        mailObj.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
//                                                  subject, body, null, null,
//                                                  notificationType, transactionId,
//                                                  selfServiceName, false, reqBean);
                    }
                    else{
//                    String body =
//                        rh.getEmailBody(notificationType, data.getString("DISPLAY_NAME"),
//                                        selfServiceName, personName);
                    String subject =
                        rh.getEmailSubject(notificationType, selfServiceName,
                                           personName);
                        extraInfoForEmail.put("Reciver",
                                              data.getString("DISPLAY_NAME"));
                        extraInfoForEmail.put("EIT_NAME",
                                              reqBean.getEit_name());
                        extraInfoForEmail.put("employeeName",
                                              reqBean.getPersonName());
                        extraInfoForEmail.put("STATUS",
                                              responsCode);
                        messageBean =
                                NotificationUtility.buildEmailBody(new JSONObject(eitLBL),
                                                                   extraInfoForEmail,
                                                                   reqBean.getCode(),
                                                                   notificationType);
                        
                        CredentialStoreClassMail.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
                                                                   subject,
                                                                   messageBean.getEmailBody(),
                                                                   null,
                                                                   null,
                                                                   notificationType,
                                                                   transactionId,
                                                                   selfServiceName,
                                                                   false,
                                                                   reqBean);
//                    mailObj.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
//                                              subject, body, null, null,
//                                              notificationType, transactionId,
//                                              selfServiceName, false, reqBean);
                    }
                }


            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        }
        else if (approvalType.equals("EMP")) {
            try {
                empObj =
                        employeeDetails.getEmpDetailsByPersonId(roleId, "", "");
                if(notificationType.equals("FYI") && responsCode.equals("APPROVED")){
//                    String body =
//                        rh.getEmailBodyForStatusRequest(responsCode,
//                                        selfServiceName,empObj.getDisplayName());
                    String subject =
                        rh.getEmailSubject(notificationType, selfServiceName,
                                           personName);
                    extraInfoForEmail.put("Reciver",
                                          personName);
                    extraInfoForEmail.put("EIT_NAME",
                                          reqBean.getEit_name());
                    extraInfoForEmail.put("employeeName",
                                          reqBean.getPersonName());
                    extraInfoForEmail.put("STATUS",
                                          responsCode);
                    messageBean =
                            NotificationUtility.buildEmailBody(new JSONObject(eitLBL),
                                                               extraInfoForEmail,
                                                               reqBean.getCode(),
                                                               notificationType);
                    
                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                               subject,
                                                               messageBean.getEmailBody(),
                                                               null,
                                                               null,
                                                               notificationType,
                                                               transactionId,
                                                               selfServiceName,
                                                               false,
                                                               reqBean);
//                    mailObj.SEND_NOTIFICATION(empObj.getEmail(),
//                                              subject, body, null, null,
//                                              notificationType, transactionId,
//                                              selfServiceName, false, reqBean);
                }

                else{
//                String body =
//                    rh.getEmailBody(notificationType, empObj.getDisplayName(),
//                                    selfServiceName, personName);
                String subject =
                    rh.getEmailSubject(notificationType, selfServiceName,
                                       personName);
                    extraInfoForEmail.put("Reciver",
                                          empObj.getDisplayName());
                    extraInfoForEmail.put("EIT_NAME",
                                          reqBean.getEit_name());
                    extraInfoForEmail.put("employeeName",
                                          reqBean.getPersonName());
                    extraInfoForEmail.put("STATUS",
                                          responsCode);
                    messageBean =
                            NotificationUtility.buildEmailBody(new JSONObject(eitLBL),
                                                               extraInfoForEmail,
                                                               reqBean.getCode(),
                                                               notificationType);
                    
                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                               subject,
                                                               messageBean.getEmailBody(),
                                                               null,
                                                               null,
                                                               notificationType,
                                                               transactionId,
                                                               selfServiceName,
                                                               false,
                                                               reqBean);


//                mailObj.SEND_NOTIFICATION(empObj.getEmail(), subject, body,
//                                          null, null, notificationType,
//                                          transactionId, selfServiceName,
//                                          false, reqBean);
                }
            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        }
    }

    public void workflowActionFYI(String action) {
        JSONObject jsonObj = new JSONObject(action);

        try {
            this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                            "SEEN",
                                            jsonObj.getString("TRS_ID"),
                                            jsonObj.getString("ssType"),
                                            jsonObj.getString("workflowId"));
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

    }
    
    public String updateWorkflownotificationProbationPeriod(String TRS_ID, String ssType) {
        String managerPersonId = "";
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "UPDATE  " + " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION \n" +
                    "    SET\n" +
                    "        RESPONSE_PERSON_ID = null,\n" +
                    "        RESPONSE_DATE = null,\n" +
                    "        STATUS = 'OPEN'\n" +
                    "     WHERE\n" +
                    "         REQUEST_ID = ?\n" +
                    "         AND SELF_TYPE = ?\n" +
                    "          AND RECEIVER_TYPE= 'LINE_MANAGER'";

            ps = connection.prepareStatement(query);

            ps.setString(1, TRS_ID);
            ps.setString(2, ssType);

            //(" query = " + query);

            ps.executeUpdate();
            
            query = "SELECT RECEIVER_ID FROM " + " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION WHERE REQUEST_ID = ? AND SELF_TYPE = ? AND RECEIVER_TYPE= 'LINE_MANAGER'";
            ps = connection.prepareStatement(query);

            ps.setString(1, TRS_ID);
            ps.setString(2, ssType);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                managerPersonId = rs.getString("RECEIVER_ID");

            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return managerPersonId;
    }
    
    public void sendEmailForHandover(String type,String approvalType, String notificationType,
                          String transactionId, String personName,
                          String roleId,String responsCode) throws AppsProException {
        PeopleExtraInformationBean reqBean = new PeopleExtraInformationBean();
        if(type.equals("Handover")){
        reqBean.setEit_name("Handover request");
        }else if(type.equals("Probation")){
                reqBean.setEit_name("Probation Period");
            }
        else{
        reqBean.setEit_name("Education Expense request");

            }
        reqBean.setPersonName(personName);
        String selfServiceName = reqBean.getEit_name();
        EmployeeBean empObj = new EmployeeBean();
        //employeeId = mean person you went to send email to
        //roleid = mean the role id if approval type is role
        //employee id if line manager or line manager +1
        //position id if approval type is position
        if (approvalType.equals("LINE_MANAGER")) {
            try {

                empObj =
                        employeeDetails.getEmpNameAndEmail(roleId, "", "");
                String subject =
                    rh.getEmailSubject(notificationType, selfServiceName,
                                       personName);
                extraInfoForEmail.put("Reciver",
                                      empObj.getDisplayName());
                extraInfoForEmail.put("EIT_NAME",
                                      reqBean.getEit_name());
                extraInfoForEmail.put("employeeName",
                                      personName);
                extraInfoForEmail.put("STATUS",
                                      responsCode);
                messageBean =
                        NotificationUtility.buildEmailBody(null,
                                                           extraInfoForEmail,
                                                           "XXX",
                                                           notificationType);
                
                CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                           subject,
                                                           messageBean.getEmailBody(),
                                                           null,
                                                           null,
                                                           notificationType,
                                                           transactionId,
                                                           selfServiceName,
                                                           false,
                                                           reqBean);
            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        } else if (approvalType.equals("LINE_MANAGER+1")) {
            try {
                empObj =
                        employeeDetails.getEmpDetailsByPersonId(roleId, "", "");
                String body =
                    rh.getEmailBody(notificationType, empObj.getDisplayName(),
                                    selfServiceName, personName);
                String subject =
                    rh.getEmailSubject(notificationType, selfServiceName,
                                       personName);
                System.out.println(body);
                System.out.println(subject);
                mailObj.SEND_NOTIFICATION(empObj.getEmail(), subject, body,
                                          null, null, notificationType,
                                          transactionId, selfServiceName,
                                          false, reqBean);
            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        } else if (approvalType.equals("ROLES")) {
            try {
                Map<String, String> paramMAp = new HashMap<String, String>();
                paramMAp.put("P_ROLEID", roleId);
                JSONObject respone =
                    BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                            paramMAp);

                JSONObject dataDS = respone.getJSONObject("DATA_DS");
                JSONArray g1 = new JSONArray();
                Object jsonTokner =
                    new JSONTokener(dataDS.get("G_1").toString()).nextValue();
                if (jsonTokner instanceof JSONObject) {
                    JSONObject arrJson = dataDS.getJSONObject("G_1");

                    g1.put(arrJson);
                } else {
                    JSONArray arrJson = dataDS.getJSONArray("G_1");
                    g1 = arrJson;
                }
                for (int i = 0; i < g1.length(); i++) {
                    JSONObject data = (JSONObject)g1.get(i);
                    String subject =
                        rh.getEmailSubject(notificationType, selfServiceName,
                                           personName);
                    extraInfoForEmail.put("Reciver",
                                          data.getString("FULL_NAME"));
                    extraInfoForEmail.put("EIT_NAME",
                                          reqBean.getEit_name());
                    extraInfoForEmail.put("employeeName",
                                          reqBean.getPersonName());
                    extraInfoForEmail.put("STATUS",
                                          responsCode);
                    messageBean =
                            NotificationUtility.buildEmailBody(null,
                                                               extraInfoForEmail,
                                                               "XXX",
                                                               notificationType);
                    
                    CredentialStoreClassMail.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
                                                               subject,
                                                               messageBean.getEmailBody(),
                                                               null,
                                                               null,
                                                               notificationType,
                                                               transactionId,
                                                               selfServiceName,
                                                               false,
                                                               reqBean);
                }

            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        } else if (approvalType.equals("POSITION")) {
            try {
                Map<String, String> paramMAp = new HashMap<String, String>();
                paramMAp.put("positionId", roleId);
                JSONObject respone =
                    BIReportModel.runReport("EmailForPositionsReport",
                                            paramMAp);
                JSONObject dataDS = respone.getJSONObject("DATA_DS");
                JSONArray g1 = new JSONArray();
                Object jsonTokner =
                    new JSONTokener(dataDS.get("G_1").toString()).nextValue();
                if (jsonTokner instanceof JSONObject) {
                    JSONObject arrJson = dataDS.getJSONObject("G_1");

                    g1.put(arrJson);
                } else {
                    JSONArray arrJson = dataDS.getJSONArray("G_1");
                    g1 = arrJson;
                }


                for (int i = 0; i < g1.length(); i++) {
                    JSONObject data = (JSONObject)g1.get(i);
                    if(notificationType.equals("FYI") && responsCode.equals("APPROVED")){
                        String subject =
                            rh.getEmailSubject(notificationType, selfServiceName,
                                               personName);
                        
                        extraInfoForEmail.put("Reciver",
                                              data.getString("DISPLAY_NAME"));
                        extraInfoForEmail.put("EIT_NAME",
                                              reqBean.getEit_name());
                        extraInfoForEmail.put("employeeName",
                                              personName);
                        extraInfoForEmail.put("STATUS",
                                              responsCode);
                        
                        messageBean =
                                NotificationUtility.buildEmailBody(null,
                                                                   extraInfoForEmail,
                                                                   "XXX",
                                                                   notificationType);
                        
                        CredentialStoreClassMail.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
                                                                   subject,
                                                                   messageBean.getEmailBody(),
                                                                   null,
                                                                   null,
                                                                   notificationType,
                                                                   transactionId,
                                                                   selfServiceName,
                                                                   false,
                                                                   reqBean);
                    }
                    else{
                    String subject =
                        rh.getEmailSubject(notificationType, selfServiceName,
                                           personName);
                        extraInfoForEmail.put("Reciver",
                                              data.getString("DISPLAY_NAME"));
                        extraInfoForEmail.put("EIT_NAME",
                                              reqBean.getEit_name());
                        extraInfoForEmail.put("employeeName",
                                              reqBean.getPersonName());
                        extraInfoForEmail.put("STATUS",
                                              responsCode);
                        messageBean =
                                NotificationUtility.buildEmailBody(null,
                                                                   extraInfoForEmail,
                                                                   "XXX",
                                                                   notificationType);
                        
                        CredentialStoreClassMail.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
                                                                   subject,
                                                                   messageBean.getEmailBody(),
                                                                   null,
                                                                   null,
                                                                   notificationType,
                                                                   transactionId,
                                                                   selfServiceName,
                                                                   false,
                                                                   reqBean);
                    }
                }


            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        }
        else if (approvalType.equals("EMP")) {
            try {
                empObj =
                        employeeDetails.getEmpNameAndEmail(roleId, "", "");
                if(notificationType.equals("FYI") && responsCode.equals("APPROVED")){
                    String subject =
                        rh.getEmailSubject(notificationType, selfServiceName,
                                           personName);
                    extraInfoForEmail.put("Reciver",
                                          personName);
                    extraInfoForEmail.put("EIT_NAME",
                                          reqBean.getEit_name());
                    extraInfoForEmail.put("employeeName",
                                          reqBean.getPersonName());
                    extraInfoForEmail.put("STATUS",
                                          responsCode);
                    messageBean =
                            NotificationUtility.buildEmailBody(null,
                                                               extraInfoForEmail,
                                                               "XXX",
                                                               notificationType);
                    
                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                               subject,
                                                               messageBean.getEmailBody(),
                                                               null,
                                                               null,
                                                               notificationType,
                                                               transactionId,
                                                               selfServiceName,
                                                               false,
                                                               reqBean);
    //                    mailObj.SEND_NOTIFICATION(empObj.getEmail(),
    //                                              subject, body, null, null,
    //                                              notificationType, transactionId,
    //                                              selfServiceName, false, reqBean);
                }

                else{
    //                String body =
    //                    rh.getEmailBody(notificationType, empObj.getDisplayName(),
    //                                    selfServiceName, personName);
                String subject =
                    rh.getEmailSubject(notificationType, selfServiceName,
                                       personName);
                    extraInfoForEmail.put("Reciver",
                                          empObj.getDisplayName());
                    extraInfoForEmail.put("EIT_NAME",
                                          reqBean.getEit_name());
                    extraInfoForEmail.put("employeeName",
                                          reqBean.getPersonName());
                    extraInfoForEmail.put("STATUS",
                                          responsCode);
                    messageBean =
                            NotificationUtility.buildEmailBody(null,
                                                               extraInfoForEmail,
                                                               "XXX",
                                                               notificationType);
                    
                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                               subject,
                                                               messageBean.getEmailBody(),
                                                               null,
                                                               null,
                                                               notificationType,
                                                               transactionId,
                                                               selfServiceName,
                                                               false,
                                                               reqBean);
                }
            } catch (Exception e) {
                //("Error: ");
                e.printStackTrace();
                AppsproConnection.LOG.error("ERROR", e);
            }
        }
    }
}

