package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.hha.appsproExceptionn.AppsProException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static common.restHelper.RestHelper.getSchema_Name;
import org.json.JSONObject;

public class ProbationReviewDAo extends AppsproConnection{
    
    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    
    public PeopleExtraInformationBean createInformationBean(String body){
            
            PeopleExtraInformationBean infoBean=new PeopleExtraInformationBean();
            
            JSONObject dataObj=new JSONObject(body);
            if(dataObj.has("employeeName")){
                infoBean.setPersonName(dataObj.get("employeeName").toString());
            }
            if(dataObj.has("employeeId")){
                infoBean.setPerson_id(dataObj.get("employeeId").toString());  
            }
            if(dataObj.has("employeeEmail")){
                infoBean.setPEI_INFORMATION1(dataObj.get("employeeEmail").toString());  
            }
            if(dataObj.has("employeeDepartment")){
                infoBean.setPEI_INFORMATION2(dataObj.get("employeeDepartment").toString());
            }
            if(dataObj.has("employeePosition")){
                infoBean.setPEI_INFORMATION3(dataObj.get("employeePosition").toString());
            }
            if(dataObj.has("employeeNationality")){
                infoBean.setPEI_INFORMATION4(dataObj.get("employeeNationality").toString());
            }
            if(dataObj.has("manager")){
                infoBean.setPEI_INFORMATION5(dataObj.get("manager").toString());
            }
            if(dataObj.has("hireDate")){
                infoBean.setPEI_INFORMATION6(dataObj.get("hireDate").toString());
            }
            if(dataObj.has("evaluationDate")){
                infoBean.setPEI_INFORMATION7(dataObj.get("evaluationDate").toString());
            }
            if(dataObj.has("firstGroupRadio")){
                infoBean.setPEI_INFORMATION8(dataObj.get("firstGroupRadio").toString());
            }
            if(dataObj.has("secondGroupRadio")){
                infoBean.setPEI_INFORMATION9(dataObj.get("secondGroupRadio").toString());
            }
            if(dataObj.has("overAllRate")){
                infoBean.setPEI_INFORMATION10(dataObj.get("overAllRate").toString());
            }
            if(dataObj.has("actionTypeSelected")){
                infoBean.setPEI_INFORMATION11(dataObj.get("actionTypeSelected").toString());
            }
            if(dataObj.has("status")){
                      infoBean.setStatus(dataObj.get("status").toString());
            }
            if(dataObj.has("eitCode")){
                      infoBean.setCode(dataObj.get("eitCode").toString());
            }
            if(dataObj.has("eitName")){
                      infoBean.setEit_name(dataObj.get("eitName").toString());
            }
            if(dataObj.has("createdBy")){
                      infoBean.setCreated_by(dataObj.get("createdBy").toString());
            }
            if(dataObj.has("id")){
                      infoBean.setId(Integer.parseInt(dataObj.get("id").toString()));
            }
            if(dataObj.has("personNumber")){
                      infoBean.setPerson_number(dataObj.get("personNumber").toString());
            }
            if(dataObj.has("lineManager")){
                      infoBean.setLine_manager(dataObj.get("lineManager").toString());
            }
            if(dataObj.has("managerOfManager")){
                      infoBean.setManage_of_manager(dataObj.get("managerOfManager").toString());
            }
            
            return infoBean;
            
        }
    
    
    public PeopleExtraInformationBean getProbationReviewById(String Id) throws AppsProException{
        PeopleExtraInformationBean bean = new PeopleExtraInformationBean();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO where ID=?";
            ps = connection.prepareStatement(query);
            ps.setLong(1, Long.parseLong(Id));
            rs = ps.executeQuery();
            while (rs.next()) {
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setPEI_INFORMATION1(rs.getString("PEI_INFORMATION1"));
                bean.setPEI_INFORMATION2(rs.getString("PEI_INFORMATION2"));
                bean.setPEI_INFORMATION3(rs.getString("PEI_INFORMATION3"));
                bean.setPEI_INFORMATION4(rs.getString("PEI_INFORMATION4"));
                bean.setPEI_INFORMATION5(rs.getString("PEI_INFORMATION5"));
                bean.setPEI_INFORMATION6(rs.getString("PEI_INFORMATION6"));
                bean.setPEI_INFORMATION7(rs.getString("PEI_INFORMATION7"));
                bean.setPEI_INFORMATION8(rs.getString("PEI_INFORMATION8"));
                bean.setPEI_INFORMATION9(rs.getString("PEI_INFORMATION9"));
                bean.setPEI_INFORMATION10(rs.getString("PEI_INFORMATION10"));
                bean.setPEI_INFORMATION11(rs.getString("PEI_INFORMATION11"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit_name(rs.getString("EIT_NAME"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setRejectReason(rs.getString("REJECT_REASON"));
            }
        } catch (Exception e) {
           
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
}
