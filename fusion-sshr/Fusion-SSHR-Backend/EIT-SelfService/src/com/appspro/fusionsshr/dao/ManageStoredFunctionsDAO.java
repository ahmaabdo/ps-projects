package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ManageStoredFunctionsBean;

import static common.restHelper.RestHelper.getSchema_Name;

import common.restHelper.RestHelper;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

public class ManageStoredFunctionsDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    
    public String createOrUpdateStoredFunction(ManageStoredFunctionsBean bean, String transactionType) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
            String storedFunctionQuery = null;

            if (transactionType.equals("ADD")) {

                storedFunctionQuery = bean.getBodyContent();    
                ps = connection.prepareStatement(storedFunctionQuery);
                ps.executeUpdate();

                query = "INSERT INTO "+ getSchema_Name() + ".XXX_STORED_FUNCTIONS_LOG " +
                        "(NAME,BODY,CREATION_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) " +
                        "VALUES(?,?,sysdate,sysdate,?,?)";
    
                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getName());
                ps.setString(2, bean.getBodyContent());
                ps.setString(3, bean.getCreatedBy());
                ps.setString(4, bean.getUpdatedBy());
                ps.executeUpdate();
                
            } else if (transactionType.equals("EDIT")) {
                
                storedFunctionQuery = bean.getBodyContent();    
                ps = connection.prepareStatement(storedFunctionQuery);
                ps.executeUpdate();
                
                query = "UPDATE "+ getSchema_Name() + ".XXX_STORED_FUNCTIONS_LOG " +
                    "SET NAME = ?, " +
                    "BODY = ?, " +
                    "UPDATED_BY = ?, " +
                    "UPDATED_DATE = sysdate " +
                    "WHERE ID = ?";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getName());
                ps.setString(2, bean.getBodyContent());
                ps.setString(3, bean.getUpdatedBy());
                ps.setInt(4, bean.getId());
                ps.executeUpdate();
                
            } else if (transactionType.equals("DELETE")) {

                storedFunctionQuery = "DROP FUNCTION " + getSchema_Name() + "." + bean.getName();
                ps = connection.prepareStatement(storedFunctionQuery);
                ps.executeUpdate();
                
                query = "DELETE FROM "+ getSchema_Name() + ".XXX_STORED_FUNCTIONS_LOG WHERE ID = ?";
                ps = connection.prepareStatement(query);
                ps.setInt(1, bean.getId());                
                ps.executeUpdate();
                
            }

        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
            return "error";
        } finally {
            closeResources(connection, ps, rs);
        }
        
        return "Success";
    }
    
    
    public String getBodyContentById(String id) {
        String bodyContent = "";

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT BODY FROM "+ getSchema_Name() + ".XXX_STORED_FUNCTIONS_LOG WHERE ID = ?";
            
            ps = connection.prepareStatement(query);
            ps.setString(1, id);            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                bodyContent = rs.getString("BODY");
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return bodyContent;
    }
    
    
    public ArrayList<ManageStoredFunctionsBean> getAllStoredFunctions() {

        ArrayList<ManageStoredFunctionsBean> beanArr = new ArrayList<ManageStoredFunctionsBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT ID,NAME,CREATION_DATE,UPDATED_DATE,STATUS " +
                "FROM "+ getSchema_Name() + ".XXX_STORED_FUNCTIONS_LOG " +
                "LEFT JOIN ALL_OBJECTS ON ALL_OBJECTS.OBJECT_NAME = "+ getSchema_Name() + ".XXX_STORED_FUNCTIONS_LOG.NAME " +
                "ORDER BY ID DESC";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                ManageStoredFunctionsBean bean = new ManageStoredFunctionsBean();
                
                bean.setId(rs.getInt("ID"));
                bean.setName(rs.getString("NAME"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));

                beanArr.add(bean);
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return beanArr;
    }
    
    public ArrayList<ManageStoredFunctionsBean> getAllStoredFunctionsSearchResult(ManageStoredFunctionsBean body) {
        
        ArrayList<ManageStoredFunctionsBean> beanArr = new ArrayList<ManageStoredFunctionsBean>();
        
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT ID,NAME,CREATION_DATE,UPDATED_DATE,STATUS " +
                "FROM "+ getSchema_Name() + ".XXX_STORED_FUNCTIONS_LOG \n" + 
                " LEFT JOIN ALL_OBJECTS ON ALL_OBJECTS.OBJECT_NAME = "+ getSchema_Name() + ".XXX_STORED_FUNCTIONS_LOG.NAME " +
            "WHERE (UPPER(NAME) LIKE UPPER(NVL(?, NAME))) \n" +
            "AND (STATUS = NVL(?, STATUS)) \n" + 
            "ORDER BY ID DESC";
            
            ps = connection.prepareStatement(query);
            
            ps.setString(1, body.getName());
            ps.setString(2, body.getStatus());
            rs = ps.executeQuery();

            while (rs.next()) {
                ManageStoredFunctionsBean bean = new ManageStoredFunctionsBean();
                
                bean.setId(rs.getInt("ID"));
                bean.setName(rs.getString("NAME"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));

                beanArr.add(bean);
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return beanArr;
    }
    
    
}
