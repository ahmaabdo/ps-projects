package com.appspro.fusionsshr.dao;

import common.restHelper.RestHelper;

import org.json.JSONObject;

public class MigrateValueSet {
    public MigrateValueSet() {
        super();
    }
    
    public static void GET_VAlUE_SET(String valueSetName) throws Exception {
        String urlValueSet = "https://eoay-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/valueSets?q=ValueSetCode='"+valueSetName+"'";
        
        String urlValue = "https://eoay-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/valueSets/";
        
//        JSONObject respone = new JSONObject(new RestHelper().callSaaS(urlValueSet, ""));
//        System.out.println(respone);
        
        JSONObject responeValue = new JSONObject(new RestHelper().callSaaS(urlValue+valueSetName+"/child/values?q=EnabledFlag='Y'", ""));
        if(responeValue == null){
            System.out.println("Value Set is not created");
            return;
        }
        String createValueURL= "https://eoay.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/valueSets/"+valueSetName+"/child/values";
        
        for(int i = 0; i < responeValue.getJSONArray("items").length();i++) {
            responeValue.getJSONArray("items").getJSONObject(i).remove("links");
            responeValue.getJSONArray("items").getJSONObject(i).remove("LastUpdateDate");
            responeValue.getJSONArray("items").getJSONObject(i).remove("LastUpdatedBy");
            responeValue.getJSONArray("items").getJSONObject(i).remove("ValueId");
            responeValue.getJSONArray("items").getJSONObject(i).remove("CreationDate");
//            System.out.println(responeValue.getJSONArray("items").getJSONObject(i));
            responeValue.getJSONArray("items").getJSONObject(i).remove("CreatedBy");
            
            System.out.println(responeValue.getJSONArray("items").getJSONObject(i).toString());
            
            System.out.println(new RestHelper().callPostRest(createValueURL,null,"application/json", responeValue.getJSONArray("items").getJSONObject(i).toString()));
            
        }
        System.out.println(createValueURL);
        
        
    }
    
    public static void main(String[] args) {
        try {
            GET_VAlUE_SET("XXX_PAAS_WITH_SALARY");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
