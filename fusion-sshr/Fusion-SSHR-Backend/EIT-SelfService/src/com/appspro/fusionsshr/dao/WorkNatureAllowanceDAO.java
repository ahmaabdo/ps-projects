/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.SecurityAllowanceBean;
import com.appspro.fusionsshr.bean.WorkNatureAllowanceBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author amro
 */
public class WorkNatureAllowanceDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public WorkNatureAllowanceBean insertOrUpdateWorkNatureAllowance(WorkNatureAllowanceBean bean, String transactionType) {

        try {
            connection = AppsproConnection.getConnection();
            if (!bean.getEndDate().isEmpty()) {
                bean.setEndDate(rh.convertToGregorian(bean.getEndDate()));
            }
            if (!bean.getStartDate().isEmpty()) {
                bean.setStartDate(rh.convertToGregorian(bean.getStartDate()));
            }

            if (transactionType.equals("ADD")) {

                String query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE (START_DATE,END_DATE,FLAG,PERCENTAGE ,POSITION_CODE,CREATEDBY,CREATEDDATE)\n"
                        + "                 VALUES (? , ? ,?,? ,?,?,?)";

                ps = connection.prepareStatement(query);
                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getFlag());
                ps.setString(4, bean.getPercentage());
                ps.setString(5, bean.getPositionCode());
                ps.setString(6, bean.getCreatedBy());
                ps.setDate(7, getSQLDateFromString(bean.getCreationDate()));
                ps.executeUpdate();
                ArrayList<WorkNatureAllowanceBean> workNatureAllowanceBeansList = getMaxID();
                UpdateWorkNatureAllowanceCode(workNatureAllowanceBeansList.get(workNatureAllowanceBeansList.size() - 1));

            } else if (transactionType.equals("EDIT")) {
                String oldFlag = "N";
                String query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE \n"
                        + "SET  UPDATEDATE =sysdate \n"
                        + ",END_DATE = sysdate "
                        + ", UPDATEBY = ? "
                        + ", FLAG = ? "
                        + "WHERE id = ? ";
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getUpdatedBy());
                ps.setString(2, oldFlag);
                ps.setInt(3, Integer.parseInt(bean.getId()));
                ps.executeUpdate();
                query = "INSERT INTO  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE (START_DATE,END_DATE,FLAG,PERCENTAGE ,POSITION_CODE,CREATEDBY,CREATEDDATE,CODE)\n"
                        + "                 VALUES (? , ? ,?,? ,?,?,?,?)";

//                 String sourceDate = bean.getStartDate();
//                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//                Date myDate = format.parse(sourceDate);
//                myDate = addDays(myDate, 2);
//                String dd = format.format(myDate);
//                 //(dd);
                ps = connection.prepareStatement(query);
                ps.setDate(1, getSQLDateFromString(bean.getStartDate()));
                ps.setDate(2, getSQLDateFromString(bean.getEndDate()));
                ps.setString(3, bean.getFlag());
                ps.setString(4, bean.getPercentage());
                ps.setString(5, bean.getPositionCode());
                ps.setString(6, bean.getCreatedBy());
                ps.setDate(7, getSQLDateFromString(bean.getCreationDate()));
                ps.setString(8, bean.getCode());
                ps.executeUpdate();

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<WorkNatureAllowanceBean> getAllWorkNatureAllowance() {
        ArrayList<WorkNatureAllowanceBean> workNatureAllowanceBeansList = new ArrayList<WorkNatureAllowanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = " select  * from  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE  ";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                WorkNatureAllowanceBean bean = new WorkNatureAllowanceBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setPercentage(rs.getString("PERCENTAGE"));
                bean.setPositionCode(rs.getString("POSITION_CODE"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                bean.setCode(rs.getString("CODE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    bean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    bean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    bean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    bean.setStartDate((rs.getString("START_DATE")));
                }//end

                workNatureAllowanceBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return workNatureAllowanceBeansList;
    }

    public ArrayList<WorkNatureAllowanceBean> getAllWorkNatureAllowanceByPositionCode(String positionCode) {
        ArrayList<WorkNatureAllowanceBean> workNatureAllowanceBeansList = new ArrayList<WorkNatureAllowanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "    select  * from  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE  \n"
                    + "                    where (POSITION_CODE =? OR CODE = ?)"
                    + "                    AND Flag =?";

            ps = connection.prepareStatement(query);
            ps.setString(1, positionCode);
            ps.setString(2, positionCode);
            ps.setString(3, "Y");
            rs = ps.executeQuery();
            while (rs.next()) {
                WorkNatureAllowanceBean bean = new WorkNatureAllowanceBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setPercentage(rs.getString("PERCENTAGE"));
                bean.setPositionCode(rs.getString("POSITION_CODE"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                bean.setCode(rs.getString("CODE"));
                //get hijri start date and end date
                if (rs.getString("END_DATE") != null) {
                    bean.setEndDate(rh.convertToHijri(rs.getString("END_DATE")));
                } else {
                    bean.setEndDate((rs.getString("END_DATE")));
                }
                if (rs.getString("START_DATE") != null) {
                    bean.setStartDate(rh.convertToHijri(rs.getString("START_DATE")));
                } else {
                    bean.setStartDate((rs.getString("START_DATE")));
                }//end 
                workNatureAllowanceBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return workNatureAllowanceBeansList;

    }

    public ArrayList<WorkNatureAllowanceBean> getMaxID() {
        ArrayList<WorkNatureAllowanceBean> workNatureAllowanceBeansList = new ArrayList<WorkNatureAllowanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "    select  * from  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE  \n"
                    + "                WHERE ID = (SELECT MAX(ID) from  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE)";

            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                WorkNatureAllowanceBean bean = new WorkNatureAllowanceBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setEndDate(rs.getString("END_DATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setPercentage(rs.getString("PERCENTAGE"));
                bean.setPositionCode(rs.getString("POSITION_CODE"));
                bean.setStartDate(rs.getString("START_DATE"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                //(bean);
                workNatureAllowanceBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return workNatureAllowanceBeansList;

    }

    public WorkNatureAllowanceBean UpdateWorkNatureAllowanceCode(WorkNatureAllowanceBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String code = bean.getId() + bean.getPositionCode() + bean.getPercentage();
            String query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE \n"
                    + "SET CODE = ? "
                    + "WHERE id = ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            ps.setInt(2, Integer.parseInt(bean.getId()));
            ps.executeUpdate();

            getMaxID();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<WorkNatureAllowanceBean> getAllWorkNatureAllowanceByCode(String code) {
        ArrayList<WorkNatureAllowanceBean> workNatureAllowanceBeansList = new ArrayList<WorkNatureAllowanceBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = "    select  * from  "+ " " + getSchema_Name() + ".XXX_WORK_NATURE_ALLOWANCE  \n"
                    + "                    where CODE =? OR POSITION_CODE = ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, code);
            ps.setString(2, code);
            rs = ps.executeQuery();
            while (rs.next()) {
                WorkNatureAllowanceBean bean = new WorkNatureAllowanceBean();
                bean.setCreatedBy(rs.getString("CREATEDBY"));
                bean.setCreationDate(rs.getString("CREATEDDATE"));
                bean.setEndDate(rs.getString("END_DATE"));
                bean.setFlag(rs.getString("FLAG"));
                bean.setId(rs.getString("ID"));
                bean.setPercentage(rs.getString("PERCENTAGE"));
                bean.setPositionCode(rs.getString("POSITION_CODE"));
                bean.setStartDate(rs.getString("START_DATE"));
                bean.setUpdatedBy(rs.getString("UPDATEBY"));
                bean.setUpdatedDate(rs.getString("UPDATEDATE"));
                bean.setCode(rs.getString("CODE"));

                workNatureAllowanceBeansList.add(bean);
            }

        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            //  JsonObject obj = new JsonObject();
            //   obj.addProperty("Error", "Internal Error: " + e.getMessage());
            //   array.add(obj);
        } finally {
            closeResources(connection, ps, rs);
        }

        return workNatureAllowanceBeansList;

    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
           ex.printStackTrace(); AppsproConnection.LOG.error("ERROR", ex);
        }
        return sqlDate;
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

}
