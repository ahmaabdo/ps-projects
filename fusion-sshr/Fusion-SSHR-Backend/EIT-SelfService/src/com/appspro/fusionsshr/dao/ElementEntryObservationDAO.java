package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ElementEntryObservationBean;
import static common.restHelper.RestHelper.getSchema_Name;

import common.restHelper.RestHelper;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

public class ElementEntryObservationDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    
    public String getElementEntryById(String id) {
        String fileContent = "";

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT FILE_CONTENT FROM "+ getSchema_Name() + ".XX_SS_HDL_FILE WHERE ID = ?";
            
            ps = connection.prepareStatement(query);
            ps.setString(1, id);            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                fileContent = rs.getString("FILE_CONTENT");
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return fileContent;
    }
    
    public ArrayList<ElementEntryObservationBean> getAllElementEntries() {

        ArrayList<ElementEntryObservationBean> elementEntryObservationBean = new ArrayList<ElementEntryObservationBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM "+ getSchema_Name() + ".XX_SS_HDL_FILE ORDER BY ID DESC";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                ElementEntryObservationBean bean = new ElementEntryObservationBean();
                
                bean.setId(rs.getInt("ID"));
                bean.setSsId(rs.getString("SS_ID"));
                bean.setSsType(rs.getString("SS_TYPE"));
                bean.setUcmId(rs.getString("UCM_ID"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setProcessId(rs.getString("PROCESS_ID"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));
                bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                bean.setFullName(rs.getString("FULL_NAME"));
                bean.setFilePath(rs.getString("FILE_PATH"));
                bean.setHireDate(rs.getString("HIRE_DATE"));
                bean.setPositionName(rs.getString("POSITION_NAME"));
                bean.setGradeName(rs.getString("GRADE_NAME"));
                bean.setAssignmentStatusType(rs.getString("ASSIGNMENT_STATUS_TYPE"));
                bean.setStartDate(rs.getString("START_DATE"));
                bean.setEndDate(rs.getString("END_DATE"));
                bean.setAdvancedLeave(rs.getString("ADVANCED_LEAVE"));
                bean.setFileName(rs.getString("FILE_NAME"));
                bean.setEitCode(rs.getString("EIT_CODE"));
                bean.setParentUcmId(rs.getString("PARENT_UCM_ID"));

                elementEntryObservationBean.add(bean);
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return elementEntryObservationBean;
    }
    

    public ArrayList<ElementEntryObservationBean> getElementEntrySearchResult(ElementEntryObservationBean body) {
        
        ArrayList<ElementEntryObservationBean> elementEntryObservationBean = new ArrayList<ElementEntryObservationBean>();
        
        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM "+ getSchema_Name() + ".XX_SS_HDL_FILE \n" + 
            "WHERE EIT_CODE = NVL(?,EIT_CODE)\n" + 
            "AND PERSON_NUMBER = NVL(?,PERSON_NUMBER)\n" + 
            "AND (UCM_ID LIKE NVL(?,UCM_ID) )\n" + 
            "AND (STATUS = NVL(?, STATUS)) \n" + 
            "ORDER BY CREATION_DATE DESC";
            if("ERROR".equals(body.getStatus())){
                body.setStatus("Import Status = SUCCESS, Load Status = ERROR");
            }
            if("SUCCESS".equals(body.getStatus())){
                body.setStatus("Import Status = SUCCESS, Load Status = SUCCESS");
            }
            
            System.out.println(query);
            ps = connection.prepareStatement(query);
            
            ps.setString(1, body.getEitCode());
            ps.setString(2, body.getPersonNumber());
            ps.setString(3, body.getUcmId().equals("") ? "%" : body.getUcmId());
            ps.setString(4, body.getStatus());
            rs = ps.executeQuery();

            while (rs.next()) {
                ElementEntryObservationBean bean = new ElementEntryObservationBean();
                
                bean.setId(rs.getInt("ID"));
                bean.setSsId(rs.getString("SS_ID"));
                bean.setSsType(rs.getString("SS_TYPE"));
                bean.setUcmId(rs.getString("UCM_ID"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setProcessId(rs.getString("PROCESS_ID"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));
                bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                bean.setFullName(rs.getString("FULL_NAME"));
                bean.setFilePath(rs.getString("FILE_PATH"));
                bean.setHireDate(rs.getString("HIRE_DATE"));
                bean.setPositionName(rs.getString("POSITION_NAME"));
                bean.setGradeName(rs.getString("GRADE_NAME"));
                bean.setAssignmentStatusType(rs.getString("ASSIGNMENT_STATUS_TYPE"));
                bean.setStartDate(rs.getString("START_DATE"));
                bean.setEndDate(rs.getString("END_DATE"));
                bean.setAdvancedLeave(rs.getString("ADVANCED_LEAVE"));
                bean.setFileName(rs.getString("FILE_NAME"));
                bean.setEitCode(rs.getString("EIT_CODE"));
                bean.setParentUcmId(rs.getString("PARENT_UCM_ID"));

                elementEntryObservationBean.add(bean);
            }
            
        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return elementEntryObservationBean;
    }
    
    
    public ElementEntryObservationBean updateElementEntry(ElementEntryObservationBean elementEntryObservationBean, String transactionType) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            if (transactionType.equals("ADD")) {

//                query = "INSERT INTO "+ getSchema_Name() + ".XX_SS_HDL_FILE (NAME) VALUES(?)";
//
//                ps = connection.prepareStatement(query);
//
//                ps.setString(1, elementEntryObservationBean.getFullName());
                
            } else if (transactionType.equals("EDIT")) {
                query = "UPDATE "+ getSchema_Name() + ".XX_SS_HDL_FILE SET STATUS = 'EDITED' WHERE ID = ?";
                ps = connection.prepareStatement(query);
                ps.setInt(1, elementEntryObservationBean.getId());
                ps.executeUpdate();
            }
            else if (transactionType.equals("DELETE")) {
                            query = "DELETE FROM "+ getSchema_Name() + ".XX_SS_HDL_FILE WHERE SS_ID = ?";
                            ps = connection.prepareStatement(query);
                            ps.setString(1, elementEntryObservationBean.getSsId());
                            ps.executeUpdate();
                        }

            
            

        } catch (Exception e) {
           e.printStackTrace(); 
           AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntryObservationBean;
    }
    
    
    
}
