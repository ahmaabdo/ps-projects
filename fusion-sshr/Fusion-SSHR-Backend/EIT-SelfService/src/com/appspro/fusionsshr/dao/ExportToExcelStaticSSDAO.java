package com.appspro.fusionsshr.dao;

import com.appspro.fusionsshr.bean.EducationRequestSoapBean;

import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExportToExcelStaticSSDAO {
    
    public Workbook getOtherStaticRequest(Workbook wb,PeopleExtraInformationBean body) {
        if("XXX_EDU_EXPENSE_REQUEST".equals(body.getCode()) || body.getCode().equals("")){
        wb = addSchoolAllowanceToExcel(wb,body);
        }
        return wb;
    }
    
    public Workbook addSchoolAllowanceToExcel(Workbook wb,PeopleExtraInformationBean body) {
        Sheet realSheet =
            wb.createSheet("Education Expense Request");
        ExpenseDAO dao =  new ExpenseDAO();
        ArrayList<EducationRequestSoapBean> eitlist = dao.GetAllEducationExpenseRequestForExcel(body);
        
        if(eitlist == null || eitlist.isEmpty()){
            return wb;
        }
        
        Row row = realSheet.createRow(0);
        
        row.createCell(0).setCellValue("Emp #");
        row.createCell(1).setCellValue("Status");
        row.createCell(2).setCellValue("Name");
        row.createCell(3).setCellValue("Department");
        row.createCell(4).setCellValue("Position");
        row.createCell(5).setCellValue("Child Name");
        row.createCell(6).setCellValue("Age");
        row.createCell(7).setCellValue("School");
        row.createCell(8).setCellValue("Semister");
        row.createCell(9).setCellValue("Year");
        row.createCell(10).setCellValue("Date");
        row.createCell(11).setCellValue("Req #");
        row.createCell(12).setCellValue("Amount");
        row.createCell(13).setCellValue("Payment Method");
         
        int i=1;
        for(EducationRequestSoapBean bean : eitlist){
            Row rowData = realSheet.createRow(i);
            
            rowData.createCell(0).setCellValue(bean.getEmployeeNumber());
            rowData.createCell(1).setCellValue(bean.getStatus());
            rowData.createCell(2).setCellValue(bean.getEmployeeName());
            rowData.createCell(3).setCellValue(bean.getEmployeeDepartment());
            rowData.createCell(4).setCellValue(bean.getEmployeePosition());
            rowData.createCell(5).setCellValue(bean.getStudentName());
            rowData.createCell(6).setCellValue(bean.getStudentAge());
            rowData.createCell(7).setCellValue(bean.getStudentSchool());
            rowData.createCell(8).setCellValue(bean.getSemister());
            rowData.createCell(9).setCellValue(bean.getAcademicYear());
            rowData.createCell(10).setCellValue(bean.getRequestDate());
            rowData.createCell(11).setCellValue(bean.getRequestNumber());
            rowData.createCell(12).setCellValue(bean.getInvoiceAmount());
            rowData.createCell(13).setCellValue(bean.getPaymentMethod());
            i++;
        }
        
        
        return wb;
    }
}
