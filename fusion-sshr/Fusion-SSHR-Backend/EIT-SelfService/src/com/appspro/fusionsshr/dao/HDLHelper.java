/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ElementEntryBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.io.InputStream;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Shadi Mansi-PC
 */

public class HDLHelper extends AppsproConnection{
    static ElementEntryDAO elementEntryDAO = new ElementEntryDAO();
    static ElementEntryBean obj = new ElementEntryBean();
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    public HDLHelper() {
        super();
    }

    public static String insertHDLFile(String processID, String ssId,
                                       String ssType, String status,
                                       String ucmId, String fileContent,
                                       String personNumber,String toBytes,
                                       String path,EmployeeBean employeeDetails,
                                       String latestAnnualLeave,
                                       String filename, String eitCode) throws JSONException, IOException {

        JSONObject body = new JSONObject();
        body.put("PROCESS_ID", processID);
        body.put("SS_ID", ssId);
        body.put("SS_TYPE", ssType);
        body.put("STATUS", status);
        body.put("UCM_ID", ucmId);
        body.put("FILE_CONTENT", fileContent);
        body.put("PERSON_NUMBER", personNumber);
        body.put("FILE_PATH", path);
        body.put("FILE_BYTES", toBytes);
        body.put("EIT_CODE", eitCode);
        body.put("FILE_NAME",filename);
        //Add Person Details Info.
        if(employeeDetails != null){
            body.put("FULL_NAME", employeeDetails.getDisplayName());
            body.put("HIRE_DATE", employeeDetails.getHireDate());
            body.put("POSITION_NAME", employeeDetails.getPositionName());
            body.put("GRADE_NAME", employeeDetails.getGrade());
            body.put("ASSIGNMENT_STATUS_TYPE", employeeDetails.getAssignmentStatusTypeId());
        }
        //Add latest Annual Leave
        JSONObject latestAnnualLeavebody;
        if(latestAnnualLeave != null) {
            latestAnnualLeavebody = new JSONObject(latestAnnualLeave);
            body.put("START_DATE", !latestAnnualLeavebody.isNull("START_DATE") ? latestAnnualLeavebody.getString("START_DATE") : "");
            body.put("END_DATE", latestAnnualLeavebody.isNull("END_DATE") ? latestAnnualLeavebody.getString("END_DATE") : "");
            body.put("ADVANCED_LEAVE", (latestAnnualLeavebody.getString("START_DATE") != null && !latestAnnualLeavebody.getString("START_DATE").isEmpty() ) ? "YES" : "NO" ); 
        }
        else
        {
            body.put("START_DATE", "");
            body.put("END_DATE", "");
            body.put("ADVANCED_LEAVE","NO" ); 
        }
        //(body.toString());
        
                    ObjectMapper mapper = new ObjectMapper();
            //ElementEntryBean elementEntryBean = mapper.readValue(body.toString(), ElementEntryBean.class);
      return elementEntryDAO.insertOrUpdateElementEntry(body.toString(),"ADD");       
            
    }

    public static String updateHDLFile(String processID, String id,
                                       String status,String UCM_ID) throws JSONException {

        JSONObject body = new JSONObject();
        body.put("P_PROCESS_ID", processID);
        if (UCM_ID != null) {
            body.put("UCM_ID", UCM_ID);    
        }
        
        body.put("P_STATUS", status);
        body.put("ID", id);

//        return RestHelper.callPutRest("https://erp.sami.com.sa/ords/sami/xx_selfService/HDL",
//                                      null, "application/json",
//                                      body.toString());
return "";
    }
    
    public String updateHDLFileTable(String processID, String SSID,
                                       String status,String UCM_ID) {
        try {

            connection = AppsproConnection.getConnection();
            String query = "";
                query = "update XX_SS_HDL_FILE set\n" +
                        "            STATUS          = NVL(?,STATUS),\n" +
                        "            PROCESS_ID      = ?,\n" +
                        "            UPDATED_DATE    = SYSDATE,\n" +
                        "            UCM_ID = NVL(?,UCM_ID)\n" +
                        "         where SS_ID = ?";
                ps = connection.prepareStatement(query);
                                ps.setString(1, status);
                                ps.setString(2, processID);
                                ps.setString(3, UCM_ID);
                                ps.setString(4, SSID);
                                ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    return "";
    }
    
    public String updateHDLFileTable(String processID, String SSID,
                                       String status) {
        try {

            connection = AppsproConnection.getConnection();
            String query = "";
                query = "update XX_SS_HDL_FILE set\n" +
                        "            STATUS          = NVL(?,STATUS),\n" +
                        "            PROCESS_ID      = ?,\n" +
                        "            UPDATED_DATE    = SYSDATE\n" +
                        "         where SS_ID = ?";
                ps = connection.prepareStatement(query);
                                ps.setString(1, status);
                                ps.setString(2, processID);
                                ps.setString(3, SSID);
                                ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    return "";
    }

    public static String getHDLFile(String id) throws JSONException {

//        return RestHelper.callGetRest("https://erp.sami.com.sa/ords/sami/xx_selfService/HDL",
//                                      id);
return "";
    }

   
        
    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large


        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length &&
               (numRead = is.read(bytes, offset, bytes.length - offset)) >=
               0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " +
                                  file.getName());
        }

        is.close();
        return bytes;
    }
    
    private static String objToString(Object var) {
        
        if(var == null || var.equals("null")) {
            return "";
        }
        
        return var.toString().equals("null") ? ""  : var.toString();
    }
}
