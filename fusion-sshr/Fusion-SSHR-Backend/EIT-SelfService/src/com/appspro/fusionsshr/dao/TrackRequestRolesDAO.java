package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.TrackRequestRolesBean;

import com.hha.appsproExceptionn.AppsProException;

import static common.restHelper.RestHelper.getSchema_Name;

import common.restHelper.RestHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

public class TrackRequestRolesDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    PreparedStatement ps1;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public TrackRequestRolesBean insertOrUpdateTrackRequestRoles(TrackRequestRolesBean trackRolesBean,

        String transactionType) {
        int maxFlag=0;

        try {
            connection = AppsproConnection.getConnection();
            String queryMaxId =
                "SELECT  NVL (MAX(flag), 0)+1  AS MaxFlag  FROM " + " " +getSchema_Name() + ". XXX_TRACK_REQUEST_ROLES";
            ps = connection.prepareStatement(queryMaxId);
            rs = ps.executeQuery();
            while (rs.next()) {
                maxFlag = rs.getInt("MaxFlag");
            }
            connection = AppsproConnection.getConnection();
            String query = null;
            if (transactionType.equals("ADD")) {
                ArrayList<String> arr = trackRolesBean.getSelfService();
                for (int i = 0; i < arr.size(); i++) {
                    query =
                            "INSERT INTO" + " " + getSchema_Name() + ".XXX_TRACK_REQUEST_ROLES (ROLE_ID,ROLE_NAME,ROLE_DESCRIPTION,SELF_SERVICE,FLAG) values(?,?,?,?,?)";
                    ps = connection.prepareStatement(query);
                    ps.setString(1, trackRolesBean.getRoleId());
                    ps.setString(2, trackRolesBean.getRoleName());
                    ps.setString(3, trackRolesBean.getRoleDescription());
                    ps.setString(4, arr.get(i));
                    ps.setInt(5, maxFlag);

                    ps.executeUpdate();
                }

            } else if (transactionType.equals("EDIT")) {
                ArrayList<String> arr = trackRolesBean.getSelfService();

                for (int i = 0; i < arr.size(); i++) {
                    query =
                            "UPDATE XXX_TRACK_REQUEST_ROLES SET ROLE_ID=?,ROLE_NAME=?,ROLE_DESCRIPTION=?,SELF_SERVICE=? WHERE ID=?";
                    ps = connection.prepareStatement(query);
                    ps.setString(1, trackRolesBean.getRoleId());
                    ps.setString(2, trackRolesBean.getRoleName());
                    ps.setString(3, trackRolesBean.getRoleDescription());
                    ps.setString(4, arr.get(i));
                    ps.setInt(5, trackRolesBean.getId());
                    ps.executeUpdate();

                }
            }

        } catch (Exception exc) {
            exc.printStackTrace();
            AppsproConnection.LOG.error("ERROR", exc);
            //            throw new AppsProException(exc);
        } finally {
            closeResources(connection, ps, rs);
        }
        return trackRolesBean;
    }


    public ArrayList<TrackRequestRolesBean> searchByRoles(TrackRequestRolesBean bean) {
        ArrayList<TrackRequestRolesBean> roleList =
            new ArrayList<TrackRequestRolesBean>();
        TrackRequestRolesBean roleBean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT * FROM  " + " " + getSchema_Name() + ".XXX_TRACK_REQUEST_ROLES  WHERE LOWER(ROLE_NAME) LIKE LOWER(?)";

            ps = connection.prepareStatement(query);
            ps.setString(1,bean.getRoleName());

            rs = ps.executeQuery();
            while (rs.next()) {
                roleBean = new TrackRequestRolesBean();
                roleBean.setRoleId(rs.getString("ROLE_ID"));
                roleBean.setRoleDescription(rs.getString("ROLE_DESCRIPTION"));
                roleBean.setRoleName(rs.getString("ROLE_NAME"));
                roleBean.setEitCodeOperation(rs.getString("SELF_SERVICE"));

                roleList.add(roleBean);
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);

        } finally {
            closeResources(connection, ps, rs);
        }

        return roleList;
    }


    public ArrayList<TrackRequestRolesBean> getAllTrackRoles() {

        ArrayList<TrackRequestRolesBean> trackRolesList =
            new ArrayList<TrackRequestRolesBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + " " + getSchema_Name() + ".XXX_TRACK_REQUEST_ROLES";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                TrackRequestRolesBean trackRolesBean =
                    new TrackRequestRolesBean();
                trackRolesBean.setId(rs.getInt("ID"));
                trackRolesBean.setRoleId(rs.getString("ROLE_ID"));
                trackRolesBean.setRoleName(rs.getString("ROLE_NAME"));
                trackRolesBean.setRoleDescription(rs.getString("ROLE_DESCRIPTION"));
                trackRolesBean.setEitCodeOperation(rs.getString("SELF_SERVICE"));
                trackRolesBean.setFlag(rs.getInt("FLAG"));
                trackRolesList.add(trackRolesBean);

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return trackRolesList;

    }

    public ArrayList<TrackRequestRolesBean> getAllByFlag(int flagNum) {

        ArrayList<TrackRequestRolesBean> trackRolesList =
            new ArrayList<TrackRequestRolesBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from" + " " + getSchema_Name() + ".XXX_TRACK_REQUEST_ROLES WHERE FLAG=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, flagNum);
            rs = ps.executeQuery();
            while (rs.next()) {
                TrackRequestRolesBean trackRolesBean =
                    new TrackRequestRolesBean();
                trackRolesBean.setId(rs.getInt("ID"));
                trackRolesBean.setRoleId(rs.getString("ROLE_ID"));
                trackRolesBean.setRoleName(rs.getString("ROLE_NAME"));
                trackRolesBean.setRoleDescription(rs.getString("ROLE_DESCRIPTION"));
                trackRolesBean.setEitCodeOperation(rs.getString("SELF_SERVICE"));
                trackRolesBean.setFlag(rs.getInt("FLAG"));
                trackRolesList.add(trackRolesBean);

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return trackRolesList;

    }

    public String deleteTrakRequestRoles(String id) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult = 0;

        try {
            query =
                    "DELETE from  " + " " + getSchema_Name() + ".XXX_TRACK_REQUEST_ROLES  WHERE ID=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            checkResult = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }


    public String deleteTrakRequestRolesByFlag(int flag) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult = 0;

        try {
            query =
                    "DELETE from  " + " " + getSchema_Name() + ".XXX_TRACK_REQUEST_ROLES  WHERE FLAG=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, flag);
            checkResult = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }

    public ArrayList<TrackRequestRolesBean> getAllEit(String ID
                                                     ) {

        ArrayList<TrackRequestRolesBean> roleSetList =
            new ArrayList<TrackRequestRolesBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM " + " " + getSchema_Name() + ".XXX_TRACK_REQUEST_ROLES WHERE ID =?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(ID));
            rs = ps.executeQuery();
            while (rs.next()) {
                TrackRequestRolesBean trackBean = new TrackRequestRolesBean();
                trackBean.setEitCodeOperation(rs.getString("SELF_SERVICE"));
                roleSetList.add(trackBean);

            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return roleSetList;

    }
}

