package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.AccessBean;



import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

public class AccessDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public AccessBean addAccessSetup(AccessBean access) {
    
        try {
            connection = AppsproConnection.getConnection();
                
                String query = "INSERT INTO  " + " " + getSchema_Name() + ".XXX_ACCESS_ROLES (ROLE_NAME,ACCESS_LIST,ACCESS_DATE,ROLE_ID,ICON_CLASS,SCREEN_NAME)VALUES(?,?,SYSDATE,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, access.getRoleName());
                ps.setString(2, access.getAccessList());
                ps.setString(3, access.getRoleId());
                ps.setString(4, access.getIconClass());
                ps.setString(5, access.getScreenName());

            ps.executeUpdate();   

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return access;
    }

    public AccessBean updateAccessSetup(AccessBean access) {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "UPDATE  " + " " + getSchema_Name() + ".XXX_ACCESS_ROLES set ROLE_NAME=? ,ACCESS_LIST=?,ACCESS_DATE=SYSDATE ,ROLE_ID=? ,ICON_CLASS=? ,SCREEN_NAME=? where ACCESS_ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, access.getRoleName());
            ps.setString(2, access.getAccessList());
            ps.setString(3, access.getRoleId());
            ps.setString(4, access.getIconClass());
            ps.setString(5, access.getScreenName());
            ps.setString(6, access.getAccessId());
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return access;
    }
    public String deleteAccessSetup(String id) {

        connection = AppsproConnection.getConnection();
        String query;
        int checkResult=0;

        try {
            query = "DELETE FROM "+" "+getSchema_Name()+".XXX_ACCESS_ROLES WHERE ACCESS_ID=?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            checkResult = ps.executeUpdate();
            
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(checkResult);
    }
    public ArrayList<AccessBean> getAllAccess() {

     ArrayList<AccessBean> accessList =
         new ArrayList<AccessBean>();
     try {
         connection = AppsproConnection.getConnection();
         String query =
             "SELECT * FROM "+" "+getSchema_Name()+".XXX_ACCESS_ROLES";
         ps = connection.prepareStatement(query);
         rs = ps.executeQuery();
         while (rs.next()) {
             AccessBean access = new AccessBean();
             access.setAccessId(rs.getString("access_id"));
             access.setRoleName(rs.getString("role_name"));
             access.setAccessList(rs.getString("access_list"));
             access.setAccessDate(rs.getString("access_date")); 
             access.setRoleId(rs.getString("role_id"));
             access.setIconClass(rs.getString("icon_class"));
             access.setScreenName(rs.getString("screen_name"));
             accessList.add(access);

         }

     } catch (Exception e) {
         //("Error: ");
        e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
     } finally {
         closeResources(connection, ps, rs);
     }
     return accessList;

    }
    
    public ArrayList<AccessBean> searchAccessSetup(AccessBean bean) {
        ArrayList<AccessBean> accessList =
            new ArrayList<AccessBean>();
        AccessBean accessBean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                " SELECT * FROM  " + " " + getSchema_Name() + ".XXX_ACCESS_ROLES WHERE access_list = ?";

            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getAccessList());
            rs = ps.executeQuery();
            while (rs.next()) {
                accessBean = new AccessBean();
                accessBean.setAccessId(rs.getString("access_id"));
                accessBean.setRoleName(rs.getString("role_name"));
                accessBean.setAccessList(rs.getString("access_list"));
                accessBean.setAccessDate(rs.getString("access_date"));   
                accessBean.setRoleId(rs.getString("role_id"));
                accessBean.setIconClass(rs.getString("icon_class"));
                accessBean.setScreenName(rs.getString("screen_name"));

                accessList.add(accessBean);

            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);

        } finally {
            closeResources(connection, ps, rs);
        }

        return accessList;
    }
}
