package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ElementEntrySetupBean;
import com.appspro.fusionsshr.bean.NotificationBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIPReports;
import common.biPReports.BIReportModel;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import java.io.InputStreamReader;

import java.lang.reflect.Array;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import static common.restHelper.RestHelper.getSchema_Name;

public class MailApprovalDAO extends AppsproConnection {
    
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    
    /*****************/
     String[] Keys;
     String[] PaaSKeys;
     String[] tempObject;
     String[] ArrKeys;
     String[] Typs={"date", "date", "number"};
     String[] model = {};
     String[] PaaSmodel = {};
     String[] Arrs = {};
     JSONArray elementEntryInputValue=new JSONArray();
     JSONObject elementEntryModel =new JSONObject();
     String url="";
     String reqeustedPersonNumber="";
    PeopleExtraInformationDAO service =
            new PeopleExtraInformationDAO();
    ElementEntrySetupDAO elementEntry = new ElementEntrySetupDAO();
    /*************************/
    public void NotificationSearch(String transactionId,String approveTypee) {
        NotificationBean list = new NotificationBean();
        NotificationBean obj = null;
        if (transactionId.equals("undefined")) {
            transactionId = "";
        }
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM (SELECT NT.*,SS.PERSON_ID,SS.PERSON_NUMBER FROM   " +
                    " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION NT, " +
                    " " + getSchema_Name() + ".XX_SELF_SERVICE SS\n" +
                    "WHERE NT.STATUS = 'OPEN' AND SS.TYPE = NT.SELF_TYPE AND SS.TRANSACTION_ID= NT.REQUEST_ID AND NT.REQUEST_ID=? AND NT.TYPE='FYA' )";
            ps = connection.prepareStatement(query);

            ps.setString(1, transactionId);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj = new NotificationBean();
                obj.setID(rs.getString("ID"));
                obj.setMSG_TITLE(rs.getString("MSG_TITLE"));
                obj.setMSG_BODY(rs.getString("MSG_BODY"));
                obj.setCREATION_DATE(rs.getString("CREATION_DATE"));
                obj.setRECEIVER_TYPE(rs.getString("RECEIVER_TYPE"));
                obj.setRECEIVER_ID(rs.getString("RECEIVER_ID"));
                obj.setRESPONSE_PERSON_ID(rs.getString("RESPONSE_PERSON_ID"));
                obj.setTYPE(rs.getString("TYPE"));
                obj.setRESPONSE_DATE(rs.getString("RESPONSE_DATE"));
                obj.setWORKFLOW_ID(rs.getString("WORKFLOW_ID"));
                obj.setREQUEST_ID(rs.getString("REQUEST_ID"));
                obj.setSTATUS(rs.getString("STATUS"));
                obj.setPERSON_NUMBER(rs.getString("PERSON_ID"));
                obj.setSELF_TYPE(rs.getString("SELF_TYPE"));
                obj.setRESPONSE_PERSON_ID(rs.getString("PERSON_NUMBER"));
                 obj.setPERSON_NAME(rs.getString("PERSON_NAME"));
            }
            WorkflowApproval(obj,approveTypee);
            
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    
    public void WorkflowApproval(NotificationBean bean,String ApproveType) throws AppsProException {
        WorkflowNotificationDAO det = new WorkflowNotificationDAO();
        JSONObject jo = new JSONObject();
        jo.put("MSG_TITLE",bean.getMSG_TITLE());
        jo.put("MSG_BODY", bean.getMSG_BODY());
        jo.put("TRS_ID", bean.getREQUEST_ID());
        jo.put("PERSON_ID", bean.getRECEIVER_ID());
        jo.put("ssType", bean.getSELF_TYPE());
        jo.put("PERSON_NAME", bean.getPERSON_NAME());
        
        if(ApproveType.equalsIgnoreCase("APPROVED")){
            jo.put("RESPONSE_CODE", "APPROVED");
            det.workflowAction(jo.toString()); 
            boolean lastStepApprover= lastStepApprover(bean);
            if(lastStepApprover){
                service.updateStatus("APPROVED","" ,Integer.parseInt(bean.getREQUEST_ID()));
                getPaaSEitesValues(bean); 
            }
        }else{
            jo.put("RESPONSE_CODE", "REJECTED");
            det.workflowAction(jo.toString()); 
        }
    }
   
    public boolean lastStepApprover(NotificationBean bean){
        boolean lastStep=false;
        ApprovalListDAO service = new ApprovalListDAO();
        ArrayList<ApprovalListBean> list = new ArrayList<ApprovalListBean>();

        list = service.getLastStepForApproval(bean.getREQUEST_ID(), bean.getSELF_TYPE());
        if(list.get(0).getRolrType().equalsIgnoreCase("POSITION") && list.get(0).getRoleId().equalsIgnoreCase(bean.getRECEIVER_ID())){
                lastStep= true;
            }
        else if(list.get(0).getRolrType().equalsIgnoreCase("LINE_MANAGER") && list.get(0).getRoleId().equalsIgnoreCase(bean.getRECEIVER_ID())){
                lastStep= true;  
            }
        else if(list.get(0).getRolrType().equalsIgnoreCase("LINE_MANAGER+1") && list.get(0).getRoleId().equalsIgnoreCase(bean.getRECEIVER_ID())){
                lastStep= true;  
            }
        else if(list.get(0).getRolrType().equalsIgnoreCase("LINE_MANAGER+") && list.get(0).getRoleId().equalsIgnoreCase(bean.getRECEIVER_ID())){
                lastStep= true;  
            }
        else if(list.get(0).getRolrType().equalsIgnoreCase("ROLES")){
                JSONObject dataDS=null;
                JSONArray g1=null;
                BIReportDAO BIReportDAO = new BIReportDAO();
                JSONObject obj = new JSONObject();
                obj.put("reportName",
                        "USER_ROLES");
                obj.put("personId", bean.getRECEIVER_ID()); 
                JSONObject jsonObj =
                    new JSONObject(BIReportDAO.getBiReport(obj));
                if (!jsonObj.getJSONObject("DATA_DS").isNull("G_1")) {
                     dataDS = jsonObj.getJSONObject("DATA_DS");
                     g1 = dataDS.getJSONArray("G_1");
                }
                
                for (int i = 0; i < g1.length(); i++) {
                JSONObject data = (JSONObject)g1.get(i);
                    if(list.get(0).getRoleId().equalsIgnoreCase(data.get("ROLE_ID").toString())){
                      lastStep= true;  
                    } 
                }
            }
        else if(list.get(0).getRolrType().equalsIgnoreCase("JOB_LEVEL") && list.get(0).getRoleId().equalsIgnoreCase(bean.getRECEIVER_ID())){
                lastStep= true; 
            }
        else if(list.get(0).getRolrType().equalsIgnoreCase("AOR") && list.get(0).getRoleId().equalsIgnoreCase(bean.getRECEIVER_ID())){
                lastStep= true; 
            }
        else{
            lastStep= false; 
        } 
        return lastStep;
    }
    
    public void getPaaSEitesValues(NotificationBean bean) throws AppsProException {
        PeopleExtraInformationBean paasEitValues = new PeopleExtraInformationBean();
        
        /************ getPaasEitsValues ***************/
        paasEitValues = service.getEITById(bean.getREQUEST_ID());
        
        
        url=paasEitValues.getUrl();
        reqeustedPersonNumber=paasEitValues.getPerson_number();
        EFFDetails det = new EFFDetails();
        JSONObject jsonObj=new JSONObject(paasEitValues);
        AppsproConnection.LOG.info("paas data");
        
        try {
            /************ insert eff file ************/
            PeopleExtraInformationBean o =
                new PeopleExtraInformationBean();
            o.setEit(jsonObj.toString());
            o.setUrl(  jsonObj.get("url").toString());
            o.setId(Integer.parseInt(bean.getRECEIVER_ID()));
            det.postEFF(o);
            
        } catch (Exception e) {
            Response.status(500).build();
        }
        
        ArrayList<ElementEntrySetupBean> list =
            new ArrayList<ElementEntrySetupBean>();
        try {
            list = elementEntry.getElementEntryByEIT(bean.getSELF_TYPE());
         
        } catch (Exception e) {
            Response.status(500).build();
        }
        
        
        if(list.size()>0){
            for(int a=0;a<list.size();a++){
                if(list.get(a).getTypeOfAction().toString().equalsIgnoreCase("Absence")){
                    
                }else{
                    JSONArray inputValueArray=new JSONArray(list.get(a).getInputValue());
                    AppsproConnection.LOG.info("input value");
                
                    for(int inputIndex=0;inputIndex<inputValueArray.length();inputIndex++){
                        JSONObject data = (JSONObject)inputValueArray.get(inputIndex);
                        JSONObject objJson = new JSONObject();
                        if (!data.get("inputEitValue").toString().equalsIgnoreCase("Transaction_ID")) {
                            if (data.get("inputEitValue").toString().equalsIgnoreCase("Transaction_ID")) {
                                objJson.put("inputEitValue",
                                            data.get("inputEitValue").toString());
                                objJson.put("inputValueLbl",
                                            data.get("inputValueLbl").toString());
                                elementEntryInputValue.put(objJson);
                            } else {
                                objJson.put("inputEitValue", "");
                                objJson.put("inputValueLbl",
                                            data.get("inputValueLbl").toString());
                                elementEntryInputValue.put(objJson);
                            }
                        } else {
                            objJson.put("inputEitValue",
                                        "P" + bean.getREQUEST_ID());
                            objJson.put("inputValueLbl",
                                        data.get("inputValueLbl").toString());
                            elementEntryInputValue.put(objJson);
                        }
                        
                        AppsproConnection.LOG.info("input value");
                        AppsproConnection.LOG.info(elementEntryInputValue.toString());  
                    }

                }
              
                elementEntryModel.put("personNumber",reqeustedPersonNumber );
                elementEntryModel.put("elementName",list.get(a).getElementName());
                elementEntryModel.put("legislativeDataGroupName",list.get(a).getLegislativeDatagroupName());
                elementEntryModel.put("assignmentNumber","E"+reqeustedPersonNumber );
                elementEntryModel.put("entryType", "E");
                elementEntryModel.put("creatorType",list.get(a).getCreatorType());
                elementEntryModel.put("sourceSystemId","PaaS_"+bean.getREQUEST_ID() );
                elementEntryModel.put("trsId",bean.getREQUEST_ID() );
                elementEntryModel.put("SSType",list.get(a).getRecurringEntry());
                elementEntryModel.put("eitCode",list.get(a).getEitCode());
                elementEntryModel.put("inputValues", elementEntryInputValue);
                elementEntryModel.put("effivtiveStartDate", jsonObj.get("creation_date").toString());
                elementEntryModel.put("reason",list.get(a).getReason()); 
            }  
            
           
            
            AppsproConnection.LOG.info("element model");
            AppsproConnection.LOG.info(elementEntryModel.toString());
            InputStream fis = new ByteArrayInputStream(elementEntryModel.toString().getBytes());
            SubmitElementEntry(fis); 
        }
       
        AppsproConnection.LOG.info(jsonObj.toString());
        
            
    }
    
    public void SubmitElementEntry(InputStream incomingData){
        ElementEntryHelper helper=new ElementEntryHelper();
        StringBuilder crunchifyBuilder = new StringBuilder();
        String respone = null;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
        } catch (Exception e) {
            //("Error Parsing: - ");
        }
        JSONObject jObject;
        JSONObject jObjectRespones = null;
        try {
            jObject = new JSONObject(crunchifyBuilder.toString());
//            HttpSession session = request.getSession();
//            ServletContext context = session.getServletContext();
//            String path = context.getRealPath(request.getContextPath());
//
//            if (null != path && path.contains("HHA-SSHR-BackEnd")) {
//                path = path.split("HHA-SSHR-BackEnd")[0] + "/images";
//            }
//            if (null != path && path.contains("FFusion-SSHR-Backend-Jdev")) {
//                path = path.split("Fusion-SSHR-Backend-Jdev")[0] + "/images";
//            }
//            else {
//                path = System.getProperty("java.scratch.dir");
//            }

            
                respone = helper.createHelperBT(System.getProperty("java.scratch.dir"), jObject,null);
           
            jObjectRespones = new JSONObject("{'status' : '" + respone + "'}");
        //            //(jObject.toString());
        } catch (JSONException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        }
    }
    public ArrayList<String> searchArrayContainValue(String nameKey,JSONArray searchArray) {
        ArrayList<String> data=new ArrayList<String>();
        for (int i = 0; i < searchArray.length(); i++) {
           JSONObject jsonObj= searchArray.getJSONObject(i);
            if ( jsonObj.has("LOOKUP_CODE")&&jsonObj.get("LOOKUP_CODE") == nameKey) {
                data.add(jsonObj.get("DESCRIPTION").toString());
                AppsproConnection.LOG.info(jsonObj);
            }
        }
        return data;
    }
}
