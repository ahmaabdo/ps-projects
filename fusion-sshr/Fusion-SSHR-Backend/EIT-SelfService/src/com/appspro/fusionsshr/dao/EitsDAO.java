/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.EitsBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;
import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONObject;


public class EitsDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    EFFDetails efd;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    SelfServiceDAO selfServiceDao = new SelfServiceDAO();
    SelfServiceBean sfBean = new SelfServiceBean();
    ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
    ArrayList<ApprovalSetupBean> approvalSetupList =
        new ArrayList<ApprovalSetupBean>();
    ApprovalListDAO approvalListDao = new ApprovalListDAO();
    ApprovalListBean alBean = new ApprovalListBean();
    WorkflowNotificationDAO workflowNotificationDao =
        new WorkflowNotificationDAO();
    WorkFlowNotificationBean wfn = new WorkFlowNotificationBean();

    public synchronized EitsBean insertOrUpdateXXX_EIT(EitsBean XXX_eitBean,
                                                       String transactionType) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
            BIReportModel obJBIR = new BIReportModel();
            if (transactionType.equals("ADD")) {

                XXX_eitBean.setId(getMaxId(connection));
                query =
                        "insert into   " + " " + getSchema_Name() + ".XXX_EITS(ID,EIT_CODE,EIT,STATUS,PERSON_NUMBER,PERSON_ID,CREATED_BY,CREATION_DATE,UPDATED_BY,UPDATED_DATE,LINE_MANAGER,MANAGER_OF_MANAGER,\n" +
                        "URL,EIT_NAME)" +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                // String X = XXX_eitBean.getEit().toString();
                ps = connection.prepareStatement(query);
                ps.setInt(1, XXX_eitBean.getId());
                ps.setString(2, XXX_eitBean.getCode());
                ps.setString(3, XXX_eitBean.getEit());
                ps.setString(4, XXX_eitBean.getStatus());
                ps.setString(5, XXX_eitBean.getPerson_number());
                ps.setString(6, XXX_eitBean.getPerson_id());
                ps.setString(7, XXX_eitBean.getCreated_by());
                ps.setString(8, XXX_eitBean.getCreation_date());
                ps.setString(9, XXX_eitBean.getUpdated_by());
                ps.setString(10, XXX_eitBean.getUpdated_date());
                ps.setString(11, XXX_eitBean.getLine_manager());
                ps.setString(12, XXX_eitBean.getManage_of_manager());
                ps.setString(13, XXX_eitBean.getUrl());
                ps.setString(14, XXX_eitBean.getEit_name());
                ps.executeUpdate();
                sfBean.setType(XXX_eitBean.getCode());
                sfBean.setTypeTableName("XXX_EITS");
                sfBean.setTransactionId(Integer.toString(XXX_eitBean.getId()));
                sfBean.setPresonId(XXX_eitBean.getPerson_id());
                sfBean.setCreated_by(XXX_eitBean.getCreated_by());
                sfBean.setPersonNumber(XXX_eitBean.getPerson_number());
                selfServiceDao.insertIntoSelfService(sfBean);
                String s = XXX_eitBean.getStatus();
                if (!s.equals("DRAFT")) {
                    approvalSetupList =
                            approvalSetup.getApprovalByEITCode(XXX_eitBean.getCode());
                    alBean.setStepLeval("0");
                    alBean.setServiceType(XXX_eitBean.getCode());
                    alBean.setTransActionId(Integer.toString(XXX_eitBean.getId()));
                    alBean.setWorkflowId("");
                    alBean.setRolrType("EMP");
                    alBean.setRoleId(XXX_eitBean.getPerson_id());
                    alBean.setResponseCode("SUBMIT");
                    alBean.setNotificationType("FYI");

                    approvalListDao.insertIntoApprovalList(alBean);
                    alBean = null;
                    ApprovalSetupBean ItratorBean;
                    Iterator<ApprovalSetupBean> approvalSetupListIterator =
                        approvalSetupList.iterator();
                    while (approvalSetupListIterator.hasNext()) {
                        alBean = new ApprovalListBean();
                        ItratorBean = approvalSetupListIterator.next();
                        alBean.setStepLeval(ItratorBean.getApprovalOrder());
                        alBean.setServiceType(XXX_eitBean.getCode());
                        alBean.setTransActionId(Integer.toString(XXX_eitBean.getId()));
                        alBean.setWorkflowId("");
                        alBean.setRolrType(ItratorBean.getApprovalType());
                        if (!alBean.getRolrType().equals("POSITION")) {
                            if (alBean.getRolrType().equals("EMP")) {
                                alBean.setRoleId(XXX_eitBean.getPerson_id());
                            } else if (alBean.getRolrType().equals("LINE_MANAGER") &&
                                       !XXX_eitBean.getLine_manager().isEmpty()) {
                                alBean.setRoleId(XXX_eitBean.getLine_manager());
                            } else if (alBean.getRolrType().equals("LINE_MANAGER+1") &&
                                       !XXX_eitBean.getManage_of_manager().isEmpty()) {
                                alBean.setRoleId(XXX_eitBean.getManage_of_manager());
                            } else if (alBean.getRolrType().equals("Special_Case")) {

                                //Caall Dynamic Report Validation -- arg
                                BIReportDAO bIReportDAO = new BIReportDAO();
                                JSONObject obj = new JSONObject();
                                obj.put("reportName",
                                        "DynamicValidationReport");
                                obj.put("str", ItratorBean.getSpecialCase());

                                JSONObject jsonObj =
                                    new JSONObject(bIReportDAO.getBiReport(obj));

                                //Value - return
                                alBean.setRoleId(jsonObj.get("value").toString());
                            }

                        } else if (alBean.getRolrType().equals("POSITION")) {
                            alBean.setRoleId(ItratorBean.getRoleName());
                        }

                        alBean.setNotificationType(ItratorBean.getNotificationType());

                        approvalListDao.insertIntoApprovalList(alBean);
                        alBean = null;
                    }
                    if (!approvalSetupList.isEmpty()) {
                        wfn.setMsgTitle(XXX_eitBean.getCode() + "Request");
                        wfn.setMsgBody(XXX_eitBean.getCode() + "Request for" +
                                       XXX_eitBean.getPerson_id());
                        ApprovalListBean rolesBean =
                            approvalListDao.getRoles(Integer.toString(XXX_eitBean.getId()),
                                                     XXX_eitBean.getCode());
                        wfn.setReceiverType(rolesBean.getRolrType());
                        wfn.setReceiverId(rolesBean.getRoleId());
                        wfn.setType(rolesBean.getNotificationType());
                        wfn.setRequestId(Integer.toString(XXX_eitBean.getId()));
                        wfn.setStatus("OPEN");
                        wfn.setSelfType(XXX_eitBean.getCode());
                        workflowNotificationDao.insertIntoWorkflow(wfn);

                    } else {
                        efd = new EFFDetails();


                        PeopleExtraInformationBean o =
                            new PeopleExtraInformationBean();
                        o.setEit(XXX_eitBean.getEit());
                        o.setUrl(XXX_eitBean.getUrl());
                        o.setId(XXX_eitBean.getId());
                        efd.postEFF(o);

                        this.updateStatus("APPROVED", XXX_eitBean.getId());

                        selfServiceDao.updateSelfService("APPROVED",
                                                         sfBean.getType(),
                                                         sfBean.getTransactionId());
                    }

                }
            } else if (transactionType.equals("EDIT")) {
                query =
                        "UPDATE  " + " " + getSchema_Name() + ".XXX_EITS SET  EIT_CODE=?,EIT=?,STATUS=?,PERSON_NUMBER=?,PERSON_ID=?,CREATED_BY=?,CREATION_DATE=?,UPDATED_BY=?,UPDATED_DATE=?,LINE_MANAGER=?,MANAGER_OF_MANAGER=?, URL=? WHERE ID=?";

                ps = connection.prepareStatement(query);

                ps.setString(1, XXX_eitBean.getCode());
                ps.setString(2, XXX_eitBean.getEit());
                ps.setString(3, XXX_eitBean.getStatus());
                ps.setString(4, XXX_eitBean.getPerson_number());
                ps.setString(5, XXX_eitBean.getPerson_id());
                ps.setString(6, XXX_eitBean.getCreated_by());
                ps.setString(7, XXX_eitBean.getCreation_date());
                ps.setString(8, XXX_eitBean.getCreated_by());
                ps.setString(9, obJBIR.getDateInGMT());
                ps.setString(10, XXX_eitBean.getLine_manager());
                ps.setString(11, XXX_eitBean.getManage_of_manager());
                ps.setString(12, XXX_eitBean.getUrl());
                ps.setInt(13, XXX_eitBean.getId());
                ps.executeUpdate();
            } else if (transactionType.equals("draft")) {
                query =
                        "UPDATE  " + " " + getSchema_Name() + ".XXX_EITS SET  EIT_CODE=?,EIT=?,STATUS=?,PERSON_NUMBER=?,PERSON_ID=?,CREATED_BY=?,CREATION_DATE=?,UPDATED_BY=?,UPDATED_DATE=?,LINE_MANAGER=?,MANAGER_OF_MANAGER=?, URL=? WHERE ID=?";

                ps = connection.prepareStatement(query);

                ps.setString(1, XXX_eitBean.getCode());
                ps.setString(2, XXX_eitBean.getEit());
                ps.setString(3, XXX_eitBean.getStatus());
                ps.setString(4, XXX_eitBean.getPerson_number());
                ps.setString(5, XXX_eitBean.getPerson_id());
                ps.setString(6, XXX_eitBean.getCreated_by());
                ps.setString(7, XXX_eitBean.getCreation_date());
                ps.setString(8, XXX_eitBean.getCreated_by());
                ps.setString(9, obJBIR.getDateInGMT());
                ps.setString(10, XXX_eitBean.getLine_manager());
                ps.setString(11, XXX_eitBean.getManage_of_manager());
                ps.setString(12, XXX_eitBean.getUrl());
                ps.setInt(13, XXX_eitBean.getId());
                ps.executeUpdate();

                approvalSetupList =
                        approvalSetup.getApprovalByEITCode(XXX_eitBean.getCode());
                alBean.setStepLeval("0");
                alBean.setServiceType(XXX_eitBean.getCode());
                alBean.setTransActionId(Integer.toString(XXX_eitBean.getId()));
                alBean.setWorkflowId("");
                alBean.setRolrType("EMP");
                alBean.setRoleId(XXX_eitBean.getPerson_id());
                alBean.setResponseCode("SUBMIT");
                alBean.setNotificationType("FYI");

                approvalListDao.insertIntoApprovalList(alBean);
                alBean = null;
                ApprovalSetupBean ItratorBean;
                Iterator<ApprovalSetupBean> approvalSetupListIterator =
                    approvalSetupList.iterator();
                while (approvalSetupListIterator.hasNext()) {
                    alBean = new ApprovalListBean();
                    ItratorBean = approvalSetupListIterator.next();
                    alBean.setStepLeval(ItratorBean.getApprovalOrder());
                    alBean.setServiceType(XXX_eitBean.getCode());
                    alBean.setTransActionId(Integer.toString(XXX_eitBean.getId()));
                    alBean.setWorkflowId("");
                    alBean.setRolrType(ItratorBean.getApprovalType());
                    if (!alBean.getRolrType().equals("POSITION")) {
                        if (alBean.getRolrType().equals("EMP")) {
                            alBean.setRoleId(XXX_eitBean.getPerson_id());
                        } else if (alBean.getRolrType().equals("LINE_MANAGER") &&
                                   !XXX_eitBean.getLine_manager().isEmpty()) {
                            alBean.setRoleId(XXX_eitBean.getLine_manager());
                        } else if (alBean.getRolrType().equals("LINE_MANAGER+1") &&
                                   !XXX_eitBean.getManage_of_manager().isEmpty()) {
                            alBean.setRoleId(XXX_eitBean.getManage_of_manager());
                        }

                    } else if (alBean.getRolrType().equals("POSITION")) {
                        alBean.setRoleId(ItratorBean.getRoleName());
                    } else if (alBean.getRolrType().equals("ROLES")) {
                        alBean.setRoleId(ItratorBean.getRoleName());
                        //("Role Name : ");
                        //(ItratorBean.getRoleName());
                    }
                    alBean.setNotificationType(ItratorBean.getNotificationType());

                    approvalListDao.insertIntoApprovalList(alBean);
                    alBean = null;
                }
                if (!approvalSetupList.isEmpty()) {
                    wfn.setMsgTitle(XXX_eitBean.getCode() + "Request");
                    wfn.setMsgBody(XXX_eitBean.getCode() + "Request for" +
                                   XXX_eitBean.getPerson_id());
                    ApprovalListBean rolesBean =
                        approvalListDao.getRoles(Integer.toString(XXX_eitBean.getId()),
                                                 XXX_eitBean.getCode());
                    wfn.setReceiverType(rolesBean.getRolrType());
                    wfn.setReceiverId(rolesBean.getRoleId());
                    wfn.setType("FYA");
                    wfn.setRequestId(Integer.toString(XXX_eitBean.getId()));
                    wfn.setStatus("OPEN");
                    wfn.setSelfType(XXX_eitBean.getCode());
                    workflowNotificationDao.insertIntoWorkflow(wfn);

                } else {
                    efd = new EFFDetails();
                    PeopleExtraInformationBean o =
                        new PeopleExtraInformationBean();
                    o.setEit(XXX_eitBean.getEit());
                    o.setUrl(XXX_eitBean.getUrl());
                    o.setId(XXX_eitBean.getId());
                    efd.postEFF(o);
                    this.updateStatus("APPROVED", XXX_eitBean.getId());

                    selfServiceDao.updateSelfService("APPROVED",
                                                     sfBean.getType(),
                                                     sfBean.getTransactionId());
                }

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return XXX_eitBean;
    }

    public void updateStatus(String RESPONSE_CODE, int ID) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query =
                    "UPDATE   " + " " + getSchema_Name() + ".XXX_EITS  " + "SET STATUS = ?" +
                    "WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, RESPONSE_CODE);
            ps.setInt(2, ID);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
    }

    public synchronized int getMaxId(Connection connection) {
        int id = 0;
        try {

            //  connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT  " + " " + getSchema_Name() + ".XXX_EITS_SEQ.NEXTVAL AS NEXT_ID FROM dual";
            ps = connection.prepareStatement(query);
            // ps.executeUpdate();
            rs = ps.executeQuery();
            while (rs.next()) {
                id = Integer.parseInt(rs.getString("NEXT_ID"));
            }
        } catch (Exception e) {
            //("Error: ");

           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return id;
    }

    public EitsBean getEITById(String Id) {
        EitsBean bean = new EitsBean();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_EITS where ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, Id);
            rs = ps.executeQuery();
            while (rs.next()) {
                //bean.setId(rs.getString("NEXTVAL"));
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<EitsBean> getEITsByPersonId(String eitCode,
                                                 String personId) {
        ArrayList<EitsBean> eitsBeanList = new ArrayList<EitsBean>();
        EitsBean bean = null;
        connection = AppsproConnection.getConnection();

        String query = null;

        try {
            connection = AppsproConnection.getConnection();
            query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_EITS where EIT_CODE = ? AND PERSON_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, personId);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new EitsBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                eitsBeanList.add(bean);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return eitsBeanList;

    }
}
