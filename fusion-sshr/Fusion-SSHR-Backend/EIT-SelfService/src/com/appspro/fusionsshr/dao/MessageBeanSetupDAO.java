package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.db.CommonConfigReader;

import com.appspro.fusionsshr.bean.MailTemplateBean;

import com.appspro.fusionsshr.bean.MessageBeanSetup;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageBeanSetupDAO extends AppsproConnection {
    

    private String logger_type = CommonConfigReader.getValue("LOGGER_TYPE");
    private String db_log_level = CommonConfigReader.getValue("DB_LOG_LEVEL");
    
    public MessageBeanSetup getByEITCode(String eitCode,String notiType) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select MSG.*,ATT.attachment,ATT.FILE_NAME,ATT.FILE_TYPE from  " + " " + getSchema_Name() + ".XXX_SETUP_MESSAGES_NOTI MSG, XXX_ATTACMENT_MSG ATT WHERE msg.id = ATT.MSG_ID(+) AND MSG.EIT_CODE = ? AND MSG.NOTI_TYPE = ?" +
                " UNION ALL " +
                " select MSG.*,ATT.attachment,ATT.FILE_NAME,ATT.FILE_TYPE from  " + " " + getSchema_Name() + ".XXX_SETUP_MESSAGES_NOTI MSG, XXX_ATTACMENT_MSG ATT WHERE msg.id = ATT.MSG_ID(+) AND MSG.EIT_CODE = 'Default' AND MSG.NOTI_TYPE = ?" +
                " AND 1 <> (SELECT  COUNT(1) from  " + " " + getSchema_Name() + ".XXX_SETUP_MESSAGES_NOTI MSG, XXX_ATTACMENT_MSG ATT WHERE msg.id = ATT.MSG_ID(+) AND MSG.EIT_CODE = ? AND MSG.NOTI_TYPE = ?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, notiType);
            ps.setString(3, notiType);
            ps.setString(4, eitCode);
            ps.setString(5, notiType);
            rs = ps.executeQuery();
            MessageBeanSetup bean = null;
            if (rs.next()) {
                bean = new MessageBeanSetup();
                bean.setId(rs.getString("ID"));
                bean.setEitCode(rs.getString("EIT_CODE"));
                bean.setEmailBody(rs.getString("EMAIL_BODY"));
                bean.setNotiMessage(rs.getString("NOTI_MESSAGE"));
                bean.setAttachment(rs.getString("attachment"));
                bean.setFileName(rs.getString("FILE_NAME"));
                bean.setFileType(rs.getString("FILE_TYPE"));
                bean.setHaveAtt(rs.getString("HAVE_ATT"));
               
            }
            
            return bean;
        } catch (SQLException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return null;
    }
    
    public MessageBeanSetup getNotiDescByEITCode(String eitCode,String notiType) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select MSG.* from  " + " " + getSchema_Name() + ".XXX_SETUP_MESSAGES_NOTI MSG WHERE MSG.EIT_CODE = ? AND MSG.NOTI_TYPE = ?" +
                " UNION ALL " +
                " select MSG.* from  " + " " + getSchema_Name() + ".XXX_SETUP_MESSAGES_NOTI MSG WHERE MSG.EIT_CODE = 'Default' AND MSG.NOTI_TYPE = ?" +
                " AND 1 <> (" +
                    " select MSG.* from  " + " " + getSchema_Name() + ".XXX_SETUP_MESSAGES_NOTI MSG WHERE MSG.EIT_CODE = ? AND MSG.NOTI_TYPE = ?" +  
                " )";
            ps = connection.prepareStatement(query);
            ps.setString(1, eitCode);
            ps.setString(2, notiType);
            ps.setString(3, notiType);
            ps.setString(4, eitCode);
            ps.setString(5, notiType);
            System.out.println(query);
            rs = ps.executeQuery();
            MessageBeanSetup bean = null;
            if (rs.next()) {
                bean = new MessageBeanSetup();
                bean.setId(rs.getString("ID"));
                bean.setEitCode(rs.getString("EIT_CODE"));
                bean.setEmailBody(rs.getString("EMAIL_BODY"));
                bean.setNotiMessage(rs.getString("NOTI_MESSAGE"));
               
            }
            
            return bean;
        } catch (SQLException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return null;
    }
    
    public Map<String,MessageBeanSetup> getAll() {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String,MessageBeanSetup> messageBeanSetupMap = new HashMap<String,MessageBeanSetup>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select MSG.*,ATT.attachment,ATT.FILE_NAME,ATT.FILE_TYPE from  " + " " + getSchema_Name() + ".XXX_SETUP_MESSAGES_NOTI MSG, XXX_ATTACMENT_MSG ATT WHERE msg.id = ATT.MSG_ID(+)";
               ps = connection.prepareStatement(query);

            System.out.println(query);
            rs = ps.executeQuery();
            MessageBeanSetup bean = null;
            while (rs.next()) {
                bean = new MessageBeanSetup();
                bean.setId(rs.getString("ID"));
                bean.setEitCode(rs.getString("EIT_CODE"));
                bean.setEmailBody(rs.getString("EMAIL_BODY"));
                bean.setNotiMessage(rs.getString("NOTI_MESSAGE"));
                bean.setAttachment(rs.getString("attachment"));
                bean.setFileName(rs.getString("FILE_NAME"));
                bean.setFileType(rs.getString("FILE_TYPE"));
                bean.setHaveAtt(rs.getString("HAVE_ATT"));
                bean.setNotiType(rs.getString("NOTI_TYPE"));
                messageBeanSetupMap.put(bean.getEitCode() + "/" + bean.getNotiType(),bean);
            }
            
            return messageBeanSetupMap;
        } catch (SQLException e) {
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return null;
    }
}
