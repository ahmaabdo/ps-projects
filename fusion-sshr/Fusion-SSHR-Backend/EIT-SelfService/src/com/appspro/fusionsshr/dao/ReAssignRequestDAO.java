package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ExpenseBean;
import com.appspro.fusionsshr.bean.HandoverHeaderBean;
import com.appspro.fusionsshr.bean.NotificationBean;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;

import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import common.biPReports.BIPReports;
import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;

import java.io.ByteArrayOutputStream;

import java.io.FileNotFoundException;

import java.io.FileOutputStream;
import java.io.IOException;

import java.io.OutputStream;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import java.util.Base64;

import java.util.HashMap;
import java.util.Iterator;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Path;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFColor;

import org.json.JSONArray;

import org.json.JSONObject;

import org.json.XML;

import static common.restHelper.RestHelper.getSchema_Name;

/**
 *
 * @author Shadi Mansi-PC
 */

public class ReAssignRequestDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ArrayList<PeopleExtraInformationBean> getPendingSelfServices() {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO where STATUS = 'PENDING_APPROVED' OR STATUS = 'APPROVED'OR STATUS = 'DRAFT' OR STATUS='Withdraw' OR STATUS='REJECTED' ORDER BY CREATION_DATE DESC";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setEit_name(rs.getString("EIT_NAME"));
                bean.setEitLbl(rs.getString("EITLBL"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                list.add(bean);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        handoverDAO handoverDAO = new handoverDAO();
        ArrayList<HandoverHeaderBean> handoverList =
            new ArrayList<HandoverHeaderBean>();
        handoverList = handoverDAO.getAllHandoverByStatus();
        for (int i = 0; i < handoverList.size(); i++) {
            bean = new PeopleExtraInformationBean();
            bean.setId(handoverList.get(i).getHeaderId());
            bean.setCode(handoverList.get(i).getRequestCode());
            bean.setStatus(handoverList.get(i).getStatus());
            bean.setPerson_number(handoverList.get(i).getEmployeeNumber());
            bean.setPerson_id(handoverList.get(i).getEmployeeId());
            bean.setCreation_date(handoverList.get(i).getCreationDate());
            bean.setEit_name("XXX_HANDOVER_REQUEST");

            bean.setPersonName(handoverList.get(i).getEmployeeName());
            list.add(bean);
        }

        ExpenseDAO expenseDAO = new ExpenseDAO();
        ArrayList<ExpenseBean> expenseList = new ArrayList<ExpenseBean>();
        expenseList = expenseDAO.GetAllEducationExpenseRequest();
        for (int y = 0; y < expenseList.size(); y++) {
            bean = new PeopleExtraInformationBean();
            bean.setId(Integer.parseInt(expenseList.get(y).getMasterId()));
            bean.setStatus(expenseList.get(y).getStatus());
            bean.setPerson_number(expenseList.get(y).getPersonNumber());
            bean.setPersonName(expenseList.get(y).getDisplayName());
            bean.setCreation_date(expenseList.get(y).getRequestDate());
            bean.setCode("XXX_EDU_EXPENSE_REQUEST");
            bean.setEit("Education Expense Request");
            bean.setEit_name("XXX_EDU_EXPENSE_REQUEST");
            list.add(bean);
        }
        return list;
    }


    public int updateStatusByWithdraw(String transactionId, String eitName) {
        int status = 0;
        connection = AppsproConnection.getConnection();
        String query = null;
        try {
            //----------update status of table  PER PEOPLE EXTRA INFO

            if (eitName.equals("XXX_EDU_EXPENSE_REQUEST")) {
                query =
                        "UPDATE " + getSchema_Name() + ".EDUCATION_EXPENSE_MASTER SET STATUS='Withdraw' WHERE MASTER_ID=? ";
            } else if (eitName.equals("XXX_HANDOVER_REQUEST")) {
                query =
                        "UPDATE " + getSchema_Name() + ".XXX_HANDOVER_HEADER set STATUS='Withdraw' where HEADER_ID=?";
            } else {
                query =
                        "UPDATE " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO set STATUS='Withdraw' where ID=?";
            }
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionId);
            status = ps.executeUpdate();
            //----------update approval list
            status = updateApprovalListWithdraw(transactionId);
            //----------update workflow notification
            status = updateWorkFlowNotificationWithdraw(transactionId);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return status;
    }


    public int updateWorkFlowNotificationWithdraw(String transactionId) {
        int status = 0;
        connection = AppsproConnection.getConnection();
        String query = null;
        try {
            query =
                    "UPDATE  " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION SET STATUS='CLOSED'" +
                    "WHERE REQUEST_ID=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionId);
            status = ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return status;
    }

    public int updateApprovalListWithdraw(String transactionId) {
        int status = 0;
        connection = AppsproConnection.getConnection();
        String query = null;
        try {
            query =
                    "UPDATE " + getSchema_Name() + ".XX_APPROVAL_LIST SET RESPONSE_CODE='Withdraw' " +
                    "WHERE TRANSACTION_ID=? AND STEP_LEVEL !='0'";
            ps = connection.prepareStatement(query);
            ps.setString(1, transactionId);
            status = ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);

        }
        return status;
    }


    public String deleteSelfServiceRow(String id, String eitName) {
        int status = 0;

        try {
            connection = AppsproConnection.getConnection();
            // Delete From People Extra Info Table
            String queryDeleteFromPeopleExtra = null;
            if (eitName.equals("XXX_EDU_EXPENSE_REQUEST")) {

                queryDeleteFromPeopleExtra =
                        "DELETE FROM " + getSchema_Name() +
                        ".EDUCATION_EXPENSE_MASTER\n" +
                        "WHERE MASTER_ID = ?;";


            } else if (eitName.equals("XXX_HANDOVER_REQUEST")) {

                queryDeleteFromPeopleExtra =
                        "DELETE from " + getSchema_Name() +
                        ".XXX_HANDOVER_HEADER WHERE HEADER_ID=?";

            } else {
                queryDeleteFromPeopleExtra =
                        "DELETE FROM " + getSchema_Name() +
                        ".XXX_PER_PEOPLE_EXTRA_INFO\n" +
                        "WHERE ID = ?";
            }
            ps = connection.prepareStatement(queryDeleteFromPeopleExtra);
            ps.setString(1, id);
            status = ps.executeUpdate();

            if (eitName.equals("XXX_HANDOVER_REQUEST")) {
                queryDeleteFromPeopleExtra =
                        "DELETE from " + getSchema_Name() +
                        ".XXX_HANDOVER_TASKS WHERE HEADER_ID=?";
                ps = connection.prepareStatement(queryDeleteFromPeopleExtra);
                ps.setString(1, id);
                status = ps.executeUpdate();
            }
            // Delete From Self Service Table

            String queryDeleteFromSelfService =
                "DELETE FROM " + getSchema_Name() + ".XX_SELF_SERVICE\n" +
                "WHERE TRANSACTION_ID = ?";

            ps = connection.prepareStatement(queryDeleteFromSelfService);

            ps.setString(1, id);

            status = ps.executeUpdate();

            // Delete From Approval List Table

            String queryDeleteFromApprovalList =
                "DELETE FROM " + getSchema_Name() + ".XX_APPROVAL_LIST\n" +
                "WHERE TRANSACTION_ID = ?";

            ps = connection.prepareStatement(queryDeleteFromApprovalList);

            ps.setString(1, id);

            status = ps.executeUpdate();

            // Delete From Work Flow Notification Table

            String queryDeleteFromWorkFlowNotification =
                "DELETE FROM  " + " " + getSchema_Name() +
                ".XX_WORKFLOW_NOTIFICATION\n" +
                "WHERE REQUEST_ID = ?";

            ps =
 connection.prepareStatement(queryDeleteFromWorkFlowNotification);

            ps.setString(1, id);

            status = ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(status);

    }

    public ArrayList<PeopleExtraInformationBean> getPendingSelfServicesByPersonNumber(PeopleExtraInformationBean body) {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "select * from  " + " " + getSchema_Name() + ".XXX_PER_PEOPLE_EXTRA_INFO where STATUS = 'PENDING_APPROVED'  And PERSON_NUMBER=NVL(?,PERSON_NUMBER) And EIT_CODE LIKE NVL(?,EIT_CODE) ORDER BY ID DESC";
            ps = connection.prepareStatement(query);
            ps.setString(1, body.getPerson_number());
            ps.setString(2, body.getCode());
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setEit_name(rs.getString("EIT_NAME"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                list.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public ArrayList<PeopleExtraInformationBean> getPendingAndApprovedSelfServicesByPersonNumber(PeopleExtraInformationBean body) {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        ArrayList<PeopleExtraInformationBean> listHandOver =
            new ArrayList<PeopleExtraInformationBean>();
        ArrayList<PeopleExtraInformationBean> listEducation =
            new ArrayList<PeopleExtraInformationBean>();
        ArrayList<String> arr = new ArrayList<String>();
        ArrayList<String> roleSelfService = new ArrayList<String>();
        arr = body.getRoleId();
        try {
            connection = AppsproConnection.getConnection();

            if (("null").equals(body.getStatus()) ||
                ("").equals(body.getStatus()) ||
                ("All").equals(body.getStatus())) {
                body.setStatus("");
            }

            if (body.getPerson_number() != null &&
                body.getPerson_number().equals("")) {
                body.setPerson_number(null);
            }


            if (body.getCode() != null && body.getCode().equals("")) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < arr.size(); i++) {
                    builder.append("?,");
                }
                String stmt =
                    "select * from  XXX_PER_PEOPLE_EXTRA_INFO WHERE STATUS=NVL(?,STATUS) And PERSON_NUMBER=NVL(?,PERSON_NUMBER) And  EIT_CODE IN (SELECT SELF_SERVICE FROM XXX_TRACK_REQUEST_ROLES WHERE ROLE_ID IN(" +
                    builder.deleteCharAt(builder.length() - 1).toString() +
                    ")" + ")";

                ps = connection.prepareStatement(stmt);
                //                System.out.println(stmt);
                ps.setString(1, body.getStatus());
                ps.setString(2, body.getPerson_number());
                int index = 3;
                for (Object o : arr) {
                    ps.setObject(index++, o); // or whatever it applies
                }
            } else {
                String query =
                    "select * from  XXX_PER_PEOPLE_EXTRA_INFO where STATUS=NVL(?,STATUS) And PERSON_NUMBER=NVL(?,PERSON_NUMBER) And EIT_CODE LIKE NVL(?,EIT_CODE) ORDER BY CREATION_DATE DESC";
                body.setCode(body.getCode());
                ps = connection.prepareStatement(query);
                ps.setString(1, body.getStatus());
                ps.setString(2, body.getPerson_number());
                ps.setString(3, body.getCode());
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(rs.getString("EIT_CODE"));
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setEit_name(rs.getString("EIT_NAME"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                list.add(bean);
            }
            if (body.getCode().equals(null) || body.getCode().equals("")) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < arr.size(); i++) {
                    builder.append("?,");
                }
                String stmt =
                    "SELECT SELF_SERVICE FROM XXX_TRACK_REQUEST_ROLES WHERE ROLE_ID IN(" +
                    builder.deleteCharAt(builder.length() - 1).toString() +
                    ")";

                ps = connection.prepareStatement(stmt);
                //                System.out.println(stmt);
                int index = 1;
                for (Object o : arr) {
                    ps.setObject(index++, o); // or whatever it applies
                }
                rs = ps.executeQuery();
                while (rs.next()) {
                    String serviceName = rs.getString("SELF_SERVICE");
                    roleSelfService.add(serviceName);
                }
                for (int x = 0; x < roleSelfService.size(); x++) {
                    if(roleSelfService.get(x).equals("XXX_HANDOVER_REQUEST")){
                        listHandOver = getHandOverByPersonNumberOrStatus(body);
                    }
                    else if(roleSelfService.get(x).equals("XXX_EDU_EXPENSE_REQUEST")){
                        listEducation = getEducationExpenseByPersonNumberOrStatus(body);
                    }
                    
                }
                
            }
            
                    list.addAll(listHandOver);
            list.addAll(listEducation);
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public ArrayList<PeopleExtraInformationBean> getEducationExpenseByPersonNumberOrStatus(PeopleExtraInformationBean body) {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        connection = AppsproConnection.getConnection();
        String status1 = null;
        String status2 = null;
        String query = null;
        try {

            if (("").equals(body.getStatus()) ||
                ("null").equals(body.getStatus()) ||
                ("All").equals(body.getStatus())) {
                status1 = "Pending Approval";
                status2 = "Approved";
            } else if (("PENDING_APPROVED").equals(body.getStatus())) {
                status1 = "Pending Approval";
            } else if (("APPROVED").equals(body.getStatus())) {
                status1 = "Approved";
            }
            query =
                    "select * from " + getSchema_Name() + ".EDUCATION_EXPENSE_MASTER where PERSON_NUMBER= NVL(?,PERSON_NUMBER) AND (STATUS=? OR STATUS=?) ORDER BY REQUEST_DATE DESC";
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, body.getPerson_number());
            ps.setString(2, status1);
            ps.setString(3, status2);

            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("MASTER_ID")));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPersonName(rs.getString("DISPLAY_NAME"));
                bean.setCreation_date(rs.getString("REQUEST_DATE"));
                bean.setCode("XXX_EDU_EXPENSE_REQUEST");
                bean.setEit("Education Expense Request");
                bean.setEit_name("XXX_EDU_EXPENSE_REQUEST");
                list.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }


    public ArrayList<PeopleExtraInformationBean> getHandOverByPersonNumberOrStatus(PeopleExtraInformationBean body) {
        PeopleExtraInformationBean bean = null;
        ArrayList<PeopleExtraInformationBean> list =
            new ArrayList<PeopleExtraInformationBean>();
        connection = AppsproConnection.getConnection();
        String status1 = null;
        String status2 = null;
        String query = null;
        try {

            if (("").equals(body.getStatus()) ||
                ("null").equals(body.getStatus()) ||
                ("All").equals(body.getStatus())) {
                status1 = "PENDING_APPROVED";
                status2 = "APPROVED";
            } else if (("PENDING_APPROVED").equals(body.getStatus())) {
                status1 = "PENDING_APPROVED";
            } else if (("APPROVED").equals(body.getStatus())) {
                status1 = "APPROVED";
            }
            query =
                    "select * from " + getSchema_Name() + ".XXX_HANDOVER_HEADER where EMPLOYEE_NUMBER=NVL(?,EMPLOYEE_NUMBER) AND (STATUS=? OR STATUS=?)ORDER BY CREATION_DATE DESC";
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, body.getPerson_number());
            ps.setString(2, status1);
            ps.setString(3, status2);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("HEADER_ID")));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("EMPLOYEE_NUMBER"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setCode("XXX_HANDOVER_REQUEST");
                bean.setEit("HandOver Request");
                bean.setEit_name("XXX_HANDOVER_REQUEST");
                list.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }


    public ArrayList<ApprovalListBean> getApprovalType(String serviceType,
                                                       String T_id) {
        ArrayList<ApprovalListBean> approvalSetupList =
            new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    " SELECT * FROM  " + " " + getSchema_Name() + ".XX_APPROVAL_LIST WHERE SERVICE_TYPE = ? AND TRANSACTION_ID = ? AND NOTIFICATION_TYPE = 'FYA'";
            ps = connection.prepareStatement(query);
            ps.setString(1, serviceType);
            ps.setString(2, T_id);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                bean.setReasonforReassign(rs.getString("REASON"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }

    public String updateApprovalList(ApprovalListBean bean) {
        int status = 0;
        WorkFlowNotificationBean objNotif = new WorkFlowNotificationBean();
        try {

            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "UPDATE " + getSchema_Name() + ".XX_APPROVAL_LIST SET ROLE_TYPE = ?, ROLE_ID = ?, REASON = ? WHERE ID = ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getRolrType());
            ps.setString(2, bean.getRoleId());
            ps.setString(3, bean.getReasonforReassign());
            ps.setString(4, bean.getId());
            status = ps.executeUpdate();


            query =
                    "SELECT ID,MSG_TITLE,MSG_BODY,CREATION_DATE,RECEIVER_TYPE,RECEIVER_ID,RESPONSE_PERSON_ID," +
                    "TYPE,RESPONSE_DATE,WORKFLOW_ID,REQUEST_ID,STATUS,SELF_TYPE," +
                    "PERSON_NAME,NATIONALIDENTITY FROM  " + getSchema_Name() +
                    ".XX_WORKFLOW_NOTIFICATION WHERE SELF_TYPE = ? AND STATUS = 'OPEN' AND REQUEST_ID = ? AND TYPE = 'FYA'";

            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getServiceType());
            ps.setString(2, bean.getResponseCode());

            rs = ps.executeQuery();

            while (rs.next()) {
                objNotif.setID(rs.getString("ID"));
                objNotif.setMsgTitle(rs.getString("MSG_TITLE"));
                objNotif.setMsgBody(rs.getString("MSG_BODY"));
                objNotif.setCreationDate(rs.getString("CREATION_DATE"));
                objNotif.setReceiverType(rs.getString("RECEIVER_TYPE"));
                objNotif.setReceiverId(rs.getString("RECEIVER_ID"));
                objNotif.setResponsePersonId(rs.getString("RESPONSE_PERSON_ID"));
                objNotif.setType(rs.getString("TYPE"));
                objNotif.setResponseDate(rs.getString("RESPONSE_DATE"));
                objNotif.setWorkflowId(rs.getString("WORKFLOW_ID"));
                objNotif.setRequestId(rs.getString("REQUEST_ID"));
                objNotif.setStatus(rs.getString("STATUS"));
                objNotif.setSelfType(rs.getString("SELF_TYPE"));
                objNotif.setNationalIdentity(rs.getString("NATIONALIDENTITY"));
                objNotif.setPersonName(rs.getString("PERSON_NAME"));
            }

            query =
                    "UPDATE " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION SET STATUS = 'CLOSED' WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, objNotif.getID());
            status = ps.executeUpdate();

            query =
                    " insert into  " + " " + getSchema_Name() + ".XX_WORKFLOW_NOTIFICATION (MSG_TITLE,MSG_BODY,CREATION_DATE,RECEIVER_TYPE,RECEIVER_ID,RESPONSE_PERSON_ID,TYPE,RESPONSE_DATE,WORKFLOW_ID,REQUEST_ID,STATUS,SELF_TYPE,PERSON_NAME,NATIONALIDENTITY) " +
                    "VALUES(?,?,SYSDATE,?,?,?,?,?,?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query);
            ps.setString(1, objNotif.getMsgTitle());
            ps.setString(2, objNotif.getMsgBody());
            ps.setString(3, bean.getRolrType());
            ps.setString(4, bean.getRoleId());
            ps.setString(5, objNotif.getResponsePersonId());
            ps.setString(6, objNotif.getType());
            ps.setString(7, objNotif.getResponseDate());
            ps.setString(8, objNotif.getWorkflowId());
            ps.setString(9, objNotif.getRequestId());
            ps.setString(10, objNotif.getStatus());
            ps.setString(11, objNotif.getSelfType());
            ps.setString(12, objNotif.getPersonName());
            ps.setString(13, objNotif.getNationalIdentity());
            status = ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return Integer.toString(status);
    }

    public static void main(String[] args) {
        try {
            String eitLBL =
                "\"{\\\"dummy\\\":1587977066622,\\\"referenceNumber\\\":\\\"SS_1001\\\",\\\"employeeLocation\\\":\\\"Head Office\\\",\\\"travelType\\\":\\\"Working\\\",\\\"tripDetails\\\":\\\"Collecting the MoU from MoH, and location inspection with FWA with Um Alqura\\\",\\\"grade\\\":14,\\\"businessTripRoute\\\":\\\"Local\\\",\\\"fromDestination\\\":\\\"Riyadh\\\",\\\"toDestinationCountry\\\":\\\"Saudi Arabia\\\",\\\"toDestinationCity\\\":\\\"Jeddah\\\",\\\"startDate\\\":\\\"2020-02-16\\\",\\\"endDate\\\":\\\"2020-02-17\\\",\\\"numberOfDays\\\":2,\\\"balanceDays\\\":40,\\\"employeeClass\\\":\\\"Business class\\\",\\\"travelBy\\\":\\\"Flight\\\",\\\"ticketClass\\\":\\\"Business class\\\",\\\"numberOfKilometers\\\":null,\\\"totalPerDiemAmount\\\":2400,\\\"expense\\\":0,\\\"totalAmount\\\":2400,\\\"dutyDelegatedTo\\\":\\\"000130 Al Fughom, Badar\\\",\\\"travelingOnBehalf\\\":null,\\\"attachment\\\":\\\"YES\\\",\\\"url\\\":\\\"https://eoay.fa.em2.oraclecloud.com:443/hcmRestApi/resources/11.13.18.05/emps/00020000000EACED00057708000110D9318F73200000004AACED00057372000D6A6176612E73716C2E4461746514FA46683F3566970200007872000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000171B8EEF40078/child/personExtraInformation/300000002143008/child/PersonExtraInformationContextXXX_5FBUSINESS_5FTRIP_5FREQUESTprivateVO\\\",\\\"travelTypeLbl\\\":\\\"Working\\\",\\\"businessTripRouteLbl\\\":\\\"Local\\\",\\\"fromDestinationLbl\\\":\\\"Riyadh\\\",\\\"toDestinationCountryLbl\\\":\\\"Saudi Arabia\\\",\\\"toDestinationCityLbl\\\":\\\"Jeddah\\\",\\\"travelByLbl\\\":\\\"Flight\\\"}\"";
            eitLBL = eitLBL.replaceAll("\\\\", "");
            eitLBL = eitLBL.substring(1, eitLBL.length() - 1);
            System.out.println(eitLBL);

            OutputStream fileOut;


            fileOut = new FileOutputStream("D:\\Geeks.xls");

            JSONObject jsonObject = new JSONObject(eitLBL);
            Iterator<String> keys = jsonObject.keys();

            Workbook wb = new HSSFWorkbook();
            Sheet realSheet = wb.createSheet("Sheet");

            CellStyle style = wb.createCellStyle();
            style.setFillBackgroundColor(IndexedColors.RED.getIndex());

            Row row = realSheet.createRow(0);
            int i = 0;
            while (keys.hasNext()) {


                String key = keys.next();
                System.out.println(key);

                row.createCell(i).setCellValue(key);
                i++;
            }

            for (int x = 0; x < i; x++) {
                realSheet.autoSizeColumn(x);
            }
            wb.write(fileOut);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public Map<String, ArrayList<PeopleExtraInformationBean>> getPendingSelfServicesExcel(PeopleExtraInformationBean body) {
        PeopleExtraInformationBean bean = null;
        Map<String, ArrayList<PeopleExtraInformationBean>> map =
            new HashMap<>();
        ArrayList<String> arr = new ArrayList<String>();
        arr = body.getRoleId();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < arr.size(); i++) {
                builder.append("?,");
            }
            String stmt =
                "select * from  XXX_PER_PEOPLE_EXTRA_INFO WHERE STATUS=NVL(?,STATUS) And PERSON_NUMBER=NVL(?,PERSON_NUMBER) And  EIT_CODE IN (SELECT SELF_SERVICE FROM XXX_TRACK_REQUEST_ROLES WHERE ROLE_ID IN(" +
                builder.deleteCharAt(builder.length() - 1).toString() +
                ")" + ")";

            ps = connection.prepareStatement(stmt);
            //                System.out.println(stmt);
            ps.setString(1, body.getStatus());
            ps.setString(2, body.getPerson_number());
            int index = 3;
            for (Object o : arr) {
                ps.setObject(index++, o); // or whatever it applies
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                String eitCode = rs.getString("EIT_CODE");
                if (map.get(eitCode) == null) {
                    map.put(eitCode,
                            new ArrayList<PeopleExtraInformationBean>());
                }
                bean = new PeopleExtraInformationBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setCode(eitCode);
                bean.setEit(rs.getString("EIT"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPerson_number(rs.getString("PERSON_NUMBER"));
                bean.setPerson_id(rs.getString("PERSON_ID"));
                bean.setCreated_by(rs.getString("CREATED_BY"));
                bean.setCreation_date(rs.getString("CREATION_DATE"));
                bean.setUpdated_by(rs.getString("UPDATED_BY"));
                bean.setUpdated_date(rs.getString("UPDATED_DATE"));
                bean.setLine_manager(rs.getString("LINE_MANAGER"));
                bean.setManage_of_manager(rs.getString("MANAGER_OF_MANAGER"));
                bean.setUrl(rs.getString("URL"));
                bean.setEit_name(rs.getString("EIT_NAME"));
                bean.setEitLbl(rs.getString("EITLBL"));
                bean.setPersonName(rs.getString("PERSON_NAME"));
                map.get(eitCode).add(bean);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
            AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return map;
    }

    public Workbook generateGeneral(Map<String, ArrayList<PeopleExtraInformationBean>> map,
                                    PeopleExtraInformationBean body) throws FileNotFoundException,
                                                                            IOException {

        BIPReports biPReports = new BIPReports();
        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

        biPReports.setReportAbsolutePath("EIT_SEGMENT_REPORT");
        String fresponse = biPReports.executeReports();
        JSONObject xmlJSONObj = XML.toJSONObject(fresponse.toString());
        String jsonString =
            xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();

        JSONArray segmentJSON = new JSONArray(jsonString);
        System.out.println(jsonString);
        Map<String, Map<String, Integer>> segment =
            new HashMap<String, Map<String, Integer>>();
        Map<String, String> eitNames = new HashMap<String, String>();
        Map<String, String> segmentNames = new HashMap<String, String>();

        int index = 0;

        for (int inJ = 0; inJ < segmentJSON.length(); inJ++) {
            String desc =
                segmentJSON.getJSONObject(inJ).getString("DESCRIPTIVE_FLEX_CONTEXT_CODE");
            String eitName =
                segmentJSON.getJSONObject(inJ).getString("EIT_NAME");
            String segmentName =
                segmentJSON.getJSONObject(inJ).getString("NAME");
            String description =
                segmentJSON.getJSONObject(inJ).getString("DESCRIPTION");
            if (segment.get(desc) == null) {
                segment.put(desc, new HashMap<String, Integer>());
            }

            if (eitNames.get(desc) == null) {
                eitNames.put(desc, eitName);
            }

            if (segmentNames.get(description) == null) {
                segmentNames.put(description, segmentName);
            }
            if (inJ > 0) {
                if (!segmentJSON.getJSONObject(inJ -
                                               1).getString("DESCRIPTIVE_FLEX_CONTEXT_CODE").equals(segmentJSON.getJSONObject(inJ).getString("DESCRIPTIVE_FLEX_CONTEXT_CODE"))) {

                    index = 0;
                }
            }
            if (index == 0) {

                segment.get(desc).put("Employee Number", index);
                segmentNames.put("Employee Number", "Employee Number");
                index += 1;
                segment.get(desc).put("Status", index);
                index += 1;
                segmentNames.put("Status", "Status");
            }
            segment.get(desc).put(segmentJSON.getJSONObject(inJ).getString("DESCRIPTION"),
                                  index);
            index++;

        }

        try {
            Workbook wb = new HSSFWorkbook();
            for (Map.Entry<String, ArrayList<PeopleExtraInformationBean>> entry :
                 map.entrySet()) {
                if (eitNames.get(entry.getKey().toString()) == null) {
                    continue;
                }
                Sheet realSheet =
                    wb.createSheet(eitNames.get(entry.getKey().toString()));
                ArrayList<PeopleExtraInformationBean> ssBeanList =
                    entry.getValue();
                for (int in = 0; in < ssBeanList.size(); in++) {
                    PeopleExtraInformationBean bean = ssBeanList.get(in);

                    String eitLBL = bean.getEitLbl();

                    eitLBL = eitLBL.replaceAll("\\\\", "");
                    eitLBL = eitLBL.substring(1, eitLBL.length() - 1);


                    JSONObject jsonObject = new JSONObject(eitLBL);
                    jsonObject.put("Status", bean.getStatus());
                    jsonObject.put("Employee Number", bean.getPerson_number());


                    //
                    CellStyle style = wb.createCellStyle();
                    style.setFillBackgroundColor(IndexedColors.RED.getIndex());


                    if (in == 0) {
                        CellStyle backgroundStyle = wb.createCellStyle();
                        backgroundStyle.setFillBackgroundColor(IndexedColors.RED.getIndex());
                        Row row = realSheet.createRow(in);
                        Iterator<Map.Entry<String, Integer>> it =
                            segment.get(bean.getCode()).entrySet().iterator();
                        int i = 0;
                        while (it.hasNext()) {
                            Map.Entry<String, Integer> pair = it.next();

                            String key = pair.getKey();
                            Integer value = pair.getValue();
                            Cell cell = row.createCell(value);
                            cell.setCellValue(segmentNames.get(key));
                            cell.setCellStyle(backgroundStyle);
                            i++;
                        }
                    }
                    Row row = realSheet.createRow(in + 1);
                    Iterator<String> keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        Integer j = 0;
                        if (segment.get(bean.getCode()).get(key) != null) {
                            j = segment.get(bean.getCode()).get(key);
                        } else {
                            j =
  segment.get(bean.getCode()).get(key.replaceAll("Lbl", ""));
                        }
                        if (j == null)
                            continue;

                        if (!jsonObject.isNull(key))
                            row.createCell(j).setCellValue(jsonObject.optString(key));
                    }
                    for (int x = 0; x < in; x++) {
                        realSheet.autoSizeColumn(x);
                    }
                }


            }


            return wb;
        } catch (Exception e) {
            e.printStackTrace();
        }

        //        for (int x = 0; x <= arr.length(); x++) {
        //            realSheet.autoSizeColumn(x);
        //        }

        return null;

    }


}
