package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.net.URLConnection;

import java.sql.SQLException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;


public class TimeCardDao extends RestHelper {
    private static TimeCardDao instance = null;
    public static boolean IS_DELETING = false;

    public static TimeCardDao getInstance() {
        if (instance == null)
            instance = new TimeCardDao();
        return instance;
    }


    public JSONArray getLOV() throws IOException {

        JSONObject json =
            BIReportModel.getInstance().getReportAttributes("Incomplete And In Error Time Cards Report");

        if (json.has("soapenv:Envelope") &&
            json.getJSONObject("soapenv:Envelope").has("soapenv:Body") &&
            json.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").has("getReportParametersResponse") &&
            json.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").getJSONObject("getReportParametersResponse").has("getReportParametersReturn") &&
            json.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").getJSONObject("getReportParametersResponse").getJSONObject("getReportParametersReturn").has("listOfParamNameValues") &&
            json.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").getJSONObject("getReportParametersResponse").getJSONObject("getReportParametersReturn").getJSONObject("listOfParamNameValues").has("item")) {

            return json.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").getJSONObject("getReportParametersResponse").getJSONObject("getReportParametersReturn").getJSONObject("listOfParamNameValues").getJSONArray("item");
        }
        return null;
    }

    public static Map<String, String> toMap(JSONObject object) throws JSONException {
        Map<String, String> map = new HashMap<String, String>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            String value = object.get(key).toString();
            map.put(key, value);
        }
        return map;
    }

    public JSONObject deleteCards(JSONObject ids) {
        JSONObject response = new JSONObject();
        response.put("status", "Done");
        response.put("message", "");

        DateFormat format;
        format = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
//        String localUrl = "http://127.0.0.1:7101/HHA-OTL/EmployeeTimeAnalysisService";
        String url = "https://hhajcs-a56241rrrr9.java.em2.oraclecloudapps.com/HHA-OTL/EmployeeTimeAnalysisService";
        System.out.println("DELETE CARDS URL:"+url);
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        HttpURLConnection http = null;
        try {

            String report = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:rest=\"http://rest.appspro.com/\">\n" + 
            "   <soapenv:Header/>\n" + 
            "   <soapenv:Body>\n" + 
            "      <rest:deleteTimeCardReport>\n" + 
            "         <!--Optional:-->\n" + 
            "         <arg0>\n" + 
            "            <!--Optional:-->\n";
            
            if(!ids.isNull("P_ADDRESS_LINE")) {
            report += 
            "            <p_ADDRESS_LINE>"+ids.opt("P_ADDRESS_LINE").toString()+"</p_ADDRESS_LINE>\n" + 
            "            <!--Optional:-->\n";
            }
            if(!ids.isNull("P_BU_ID")) {
            report += 
            "            <p_BU_ID>"+ids.opt("P_BU_ID").toString()+"</p_BU_ID>\n" + 
            "            <!--Optional:-->\n";
            }
            
            if(!ids.isNull("P_DEPART_ID")) {
            report +=
            "            <p_DEPART_ID>"+ids.opt("P_DEPART_ID").toString()+"</p_DEPART_ID>\n" + 
            "            <!--Optional:-->\n" ;
            }
            if(!ids.isNull("P_FROM_DATE")) {
            report +=
            "            <p_FROM_DATE>"+ids.opt("P_FROM_DATE").toString()+"</p_FROM_DATE>\n" + 
            "            <!--Optional:-->\n";
            }
            
            if(!ids.isNull("p_JOB_ID")) {
            report +=
            "            <p_JOB_ID>"+ids.opt("P_JOB_ID").toString()+"</p_JOB_ID>\n" + 
            "            <!--Optional:-->\n";
            }
            
            if(!ids.isNull("p_LOCATION_ID")) {
            report +=
            "            <p_LOCATION_ID>"+ids.opt("P_LOCATION_ID").toString()+"</p_LOCATION_ID>\n" + 
            "            <!--Optional:-->\n";
            }
            
            if(!ids.isNull("P_POSITION_ID")) {
            report +=
            "            <p_POSITION_ID>"+ids.opt("P_POSITION_ID").toString()+"</p_POSITION_ID>\n" + 
            "            <!--Optional:-->\n";
            }
            
            if(!ids.isNull("P_TO_DATE")) {
            report +=
            "            <p_TO_DATE>"+ids.opt("P_TO_DATE").toString()+"</p_TO_DATE>\n";
            }
            report +=
            "         </arg0>\n" + 
            "      </rest:deleteTimeCardReport>\n" + 
            "   </soapenv:Body>\n" + 
            "</soapenv:Envelope>";
            

            System.out.println("DELETE CARDS SOAP\n"+report);
            byte[] buffer = new byte[report.length()];
            buffer = report.getBytes();
            bout.write(buffer);
            byte[] b = bout.toByteArray();


//            URL Exturl = new URL(url);
            java.net.URL Exturl =
                        new URL(null, url, new sun.net.www.protocol.https.Handler());
            if (Exturl.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                http = (HttpsURLConnection)Exturl.openConnection();
            } else {
                http = (HttpURLConnection)Exturl.openConnection();
            }

            String SOAPAction = "";
            System.setProperty("DUseSunHttpHandler", "true");
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml");//; charset=utf-8
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            //Write the content of the request to the outputstream of the HTTP Connection.
            out.write(b);
            out.close();
            int st = http.getResponseCode();
            System.out.println("DELETE CARDS RESPONSE STATUS:"+st);
            if (st == 200) {
                
                response.put("status", "Done");
                response.put("message", "Failed");
            }else{
                response.put("status", "Error");
                response.put("message", "ERROR, Code:"+st);
            }
        } catch (Exception e) {
            TimeCardDao.IS_DELETING = false;
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
            response.put("status", "Error");
            response.put("message", e.getMessage());
        } finally {
            TimeCardDao.IS_DELETING = false;
            if (http != null)
                http.disconnect();
        }
        return response;
    }
}
