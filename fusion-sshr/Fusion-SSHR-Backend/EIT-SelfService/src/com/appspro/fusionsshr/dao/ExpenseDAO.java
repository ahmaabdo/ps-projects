package com.appspro.fusionsshr.dao;
import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.ApprovalListBean;
import com.appspro.fusionsshr.bean.ApprovalSetupBean;
import com.appspro.fusionsshr.bean.EducationRequestSoapBean;
import com.appspro.fusionsshr.bean.EitsBean;
import com.appspro.fusionsshr.bean.EmployeeBean;
import com.appspro.fusionsshr.bean.ExpenseAttachmentDetails;
import com.appspro.fusionsshr.bean.ExpenseBean;
import com.appspro.fusionsshr.bean.ExpenseStudentBean;

import com.appspro.fusionsshr.bean.HandoverHeaderBean;
import com.appspro.fusionsshr.bean.MessageBeanSetup;
import com.appspro.fusionsshr.bean.PeopleExtraInformationBean;
import com.appspro.fusionsshr.bean.PositionRolesBean;
import com.appspro.fusionsshr.bean.SelfServiceBean;

import com.appspro.fusionsshr.bean.WorkFlowNotificationBean;

import com.appspro.mail.CredentialStoreClassMail;

import com.hha.appsproExceptionn.AppsProException;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import org.json.JSONTokener;

import utilities.NotificationUtility;

public class ExpenseDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    String expenseMaxId;
    EmployeeDetails employeeDetails = new EmployeeDetails();
    CredentialStoreClassMail mailObj = new  CredentialStoreClassMail();
    HashMap<String, String> extraInfoForEmail = new HashMap<String, String>();
    private static SelfServiceBean EXTRA_INFO_BEAN_THREAD;
    MessageBeanSetup messageBean = new MessageBeanSetup();
    private static String ROLE_NOTI_TYPE;
    private static String ROLE_NOTI_ID;
    PositionRolesBean positionRoles = new PositionRolesBean();
    EmployeeBean empObj = new EmployeeBean();

    public ExpenseBean insertExpense(ExpenseBean expensebean) {
        try {
            // insert into master
            connection = AppsproConnection.getConnection();
            String query =
                "insert into " + " " + getSchema_Name() + ".EDUCATION_EXPENSE_MASTER(REQUEST_DATE,DISPLAY_NAME,EMAIL,POSITION_CLASSIFICATION,ACADEMIC_YEAR,SEMESTER,PERSON_NUMBER,STATUS,REQUEST_NUMBER,GRADE,JOINING_DATE,DEPARTMENT,NATIONALITY) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps=connection.prepareStatement(query);
            ps.setString(1,expensebean.getRequestDate());
            ps.setString(2,expensebean.getDisplayName());
            ps.setString(3,expensebean.getEmail());
            ps.setString(4,expensebean.getPosition());
            ps.setString(5,expensebean.getAcademicYear());
            ps.setString(6,expensebean.getSemister());
            ps.setString(7,expensebean.getPersonNumber());
            ps.setString(8,expensebean.getStatus());
            ps.setString(9,expensebean.getRequestNumber());
            ps.setString(10,expensebean.getGrade());
            ps.setString(11,expensebean.getJoiningDate());
            ps.setString(12, expensebean.getDepartment());
            ps.setString(13, expensebean.getNationality());
            ps.executeUpdate();  
            
            // get master id 
            int masterID=0;
            query="SELECT MAX(MASTER_ID) from EDUCATION_EXPENSE_MASTER";
            ps = connection.prepareStatement(query);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                masterID = result.getInt("MAX(MASTER_ID)");
                expensebean.setMasterId(String.valueOf(masterID));
                //("roleSetupId " + roleSetupId);
            } 
            for(int i=0;i<expensebean.getStudentDetails().size();i++){
                
                query="insert into"+" " + getSchema_Name() +".EDUCATION_EXPENSE_DETAILS(STUDENT_NAME,STUDENT_AGE,STUDENT_GRADE,STUDENT_SCHOOL_NAME,MASTER_ID,STUDENT_INVOICE_NO,STUDENT_INVOICE_DATE,NOTE,INVOICE_AMOUNT,INVOICE_PAYMENT,STUDENT_ID,BANK_NAME,IBAN,PAYMENT_METHOD) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1,expensebean.getStudentDetails().get(i).getStudentName());
                ps.setString(2,expensebean.getStudentDetails().get(i).getStudentAge());
                ps.setString(3,expensebean.getStudentDetails().get(i).getStudentGrade());
                ps.setString(4,expensebean.getStudentDetails().get(i).getStudentSchool());
                ps.setInt(5,masterID);
                ps.setString(6,expensebean.getStudentDetails().get(i).getStudentInvoiceNo());
                ps.setString(7,expensebean.getStudentDetails().get(i).getStudentInvoiceDate());
                ps.setString(8,expensebean.getStudentDetails().get(i).getNote());
                ps.setString(9, expensebean.getStudentDetails().get(i).getInvoiceAmount());
                ps.setString(10, expensebean.getStudentDetails().get(i).getInvoicePayment());
                ps.setString(11,expensebean.getStudentDetails().get(i).getStudentId());
                ps.setString(12, expensebean.getStudentDetails().get(i).getBankName());
                ps.setString(13, expensebean.getStudentDetails().get(i).getIban());
                ps.setString(14, expensebean.getStudentDetails().get(i).getPaymentMethod());
                ps.executeUpdate();
                
                int DetailsId=0;
                query="SELECT MAX(DETAILS_ID) from EDUCATION_EXPENSE_DETAILS";
                ps = connection.prepareStatement(query);
                 rs = ps.executeQuery();
                if (rs.next()) {
                    DetailsId = rs.getInt("MAX(DETAILS_ID)");
                    //("roleSetupId " + roleSetupId);
                } 
                for(int x=0; x<expensebean.getStudentDetails().get(i).getAttachment().size();x++){
                   query="insert into"+" " + getSchema_Name() +".EDUCATIONEXPENSEATTACHMENT(ATTACHMENT,DETAILSID,NAME) values(?,?,?)";
                   ps = connection.prepareStatement(query);
                   ps.setString(1,expensebean.getStudentDetails().get(i).getAttachment().get(x).getData());
                   ps.setInt(2, DetailsId); 
                   ps.setString(3,expensebean.getStudentDetails().get(i).getAttachment().get(x).getName()); 
                   //ps.setString(9,expensebean.getStudentDetails().get(i).getAttachment());
                   ps.executeUpdate();  
                    
               }
                
            }
                

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return expensebean;
    }
    public ExpenseBean updateExpense(ExpenseBean expensebean,int masterId) {
        try {
            // update master
            connection = AppsproConnection.getConnection();
            String query =
                "update " + " " + getSchema_Name() + ".EDUCATION_EXPENSE_MASTER set REQUEST_DATE=?,DISPLAY_NAME=?,EMAIL=?,POSITION_CLASSIFICATION=?,ACADEMIC_YEAR=?,SEMESTER=?,REQUEST_NUMBER=?,GRADE=?,JOINING_DATE=?,DEPARTMENT=?,NATIONALITY=? WHERE MASTER_ID=?";
            ps=connection.prepareStatement(query);
            ps.setString(1,expensebean.getRequestDate());
            ps.setString(2,expensebean.getDisplayName());
            ps.setString(3,expensebean.getEmail());
            ps.setString(4,expensebean.getPosition());
            ps.setString(5,expensebean.getAcademicYear());
            ps.setString(6,expensebean.getSemister());
            ps.setString(7,expensebean.getRequestNumber());
            ps.setString(8,expensebean.getGrade());
            ps.setString(9,expensebean.getJoiningDate());
            ps.setString(10, expensebean.getDepartment());
            ps.setString(11, expensebean.getNationality());
            ps.setInt(12,masterId);
            ps.executeUpdate();  
            
            //update details
            ArrayList<ExpenseStudentBean> studentBean=new ArrayList<ExpenseStudentBean>();
            studentBean=expensebean.getStudentDetails();
            for(int i=0;i<studentBean.size();i++){
                //delete attachment of student 
                
                query="delete from "+" "+getSchema_Name()+".EDUCATIONEXPENSEATTACHMENT WHERE DETAILSID=?";
                ps=connection.prepareStatement(query);
                ps.setInt(1,Integer.parseInt(studentBean.get(i).getDetailID()));
                ps.executeUpdate();
                 // delete student itself
                query="delete from "+" "+getSchema_Name()+".EDUCATION_EXPENSE_DETAILS WHERE MASTER_ID=?";
                ps=connection.prepareStatement(query);
                ps.setInt(1,masterId);
                ps.executeUpdate();
            }
            // insert updated students
            for(int i=0;i<studentBean.size();i++){
                
                query="insert into"+" " + getSchema_Name() +".EDUCATION_EXPENSE_DETAILS(STUDENT_NAME,STUDENT_AGE,STUDENT_GRADE,STUDENT_SCHOOL_NAME,MASTER_ID,STUDENT_INVOICE_NO,STUDENT_INVOICE_DATE,NOTE,INVOICE_AMOUNT,INVOICE_PAYMENT,STUDENT_ID,BANK_NAME,IBAN,PAYMENT_METHOD) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1,studentBean.get(i).getStudentName());
                ps.setString(2,studentBean.get(i).getStudentAge());
                ps.setString(3,studentBean.get(i).getStudentGrade());
                ps.setString(4,studentBean.get(i).getStudentSchool());
                ps.setInt(5,Integer.parseInt(studentBean.get(i).getMasterId()));
                ps.setString(6,studentBean.get(i).getStudentInvoiceNo());
                ps.setString(7,studentBean.get(i).getStudentInvoiceDate());
                ps.setString(8,studentBean.get(i).getNote());
                ps.setString(9, expensebean.getStudentDetails().get(i).getInvoiceAmount());
                ps.setString(10, expensebean.getStudentDetails().get(i).getInvoicePayment());
                ps.setString(11,expensebean.getStudentDetails().get(i).getStudentId());
                ps.setString(12,expensebean.getStudentDetails().get(i).getBankName());
                ps.setString(13,expensebean.getStudentDetails().get(i).getIban());
                ps.setString(14, expensebean.getStudentDetails().get(i).getPaymentMethod());
                ps.executeUpdate();
                // get new detail id 
                int DetailsId=0;
                query="SELECT MAX(DETAILS_ID) from EDUCATION_EXPENSE_DETAILS";
                ps = connection.prepareStatement(query);
                 rs = ps.executeQuery();
                if (rs.next()) {
                    DetailsId = rs.getInt("MAX(DETAILS_ID)");
                    //("roleSetupId " + roleSetupId);
                } 
                // insert updated attachments

                for(int x=0; x<studentBean.get(i).getAttachment().size();x++){
                   query="insert into"+" " + getSchema_Name() +".EDUCATIONEXPENSEATTACHMENT(ATTACHMENT,DETAILSID,NAME) values(?,?,?)";
                   ps = connection.prepareStatement(query);
                   ps.setString(1,expensebean.getStudentDetails().get(i).getAttachment().get(x).getData());
                   ps.setInt(2,DetailsId); 
                   ps.setString(3,expensebean.getStudentDetails().get(i).getAttachment().get(x).getName()); 
                   //ps.setString(9,expensebean.getStudentDetails().get(i).getAttachment());
                   ps.executeUpdate();  
                    
               }
                
            }
                

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return expensebean;
    }
    public ArrayList<ExpenseBean> getMasterData(String personNumber) {
        ArrayList<ExpenseBean> masterList=new ArrayList<ExpenseBean>();
        try {   
           connection = AppsproConnection.getConnection();
           String query="select * from "+" "+getSchema_Name()+".EDUCATION_EXPENSE_MASTER WHERE PERSON_NUMBER=? order by MASTER_ID DESC";
           ps=connection.prepareStatement(query);
           ps.setString(1, personNumber); 
            rs = ps.executeQuery();
            while(rs.next()){
                ExpenseBean expenseBean=new ExpenseBean();
                expenseBean.setMasterId(Integer.toString(rs.getInt("MASTER_ID")));
                expenseBean.setRequestDate(rs.getString("REQUEST_DATE"));
                expenseBean.setDisplayName(rs.getString("DISPLAY_NAME"));
                expenseBean.setEmail(rs.getString("EMAIL"));
                expenseBean.setPosition(rs.getString("POSITION_CLASSIFICATION"));
                expenseBean.setAcademicYear(rs.getString("ACADEMIC_YEAR"));
                expenseBean.setSemister(rs.getString("SEMESTER"));
                expenseBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                expenseBean.setStatus(rs.getString("STATUS"));
                expenseBean.setRequestNumber(rs.getString("REQUEST_NUMBER"));
                expenseBean.setGrade(rs.getString("GRADE"));
                expenseBean.setJoiningDate(rs.getString("JOINING_DATE"));
                expenseBean.setDepartment(rs.getString("DEPARTMENT"));
                expenseBean.setNationality(rs.getString("NATIONALITY"));
                masterList.add(expenseBean);
                }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return masterList;
    }
    public ExpenseBean getMasterDataById(String requestId) {
        ExpenseBean expenseBean=new ExpenseBean();
        try {   
           connection = AppsproConnection.getConnection();
           String query="select * from "+" "+getSchema_Name()+".EDUCATION_EXPENSE_MASTER WHERE MASTER_ID=? ";
           ps=connection.prepareStatement(query);
           ps.setInt(1, Integer.parseInt(requestId)); 
            rs = ps.executeQuery();
            while(rs.next()){
                expenseBean.setMasterId(Integer.toString(rs.getInt("MASTER_ID")));
                expenseBean.setRequestDate(rs.getString("REQUEST_DATE"));
                expenseBean.setDisplayName(rs.getString("DISPLAY_NAME"));
                expenseBean.setEmail(rs.getString("EMAIL"));
                expenseBean.setPosition(rs.getString("POSITION_CLASSIFICATION"));
                expenseBean.setAcademicYear(rs.getString("ACADEMIC_YEAR"));
                expenseBean.setSemister(rs.getString("SEMESTER"));
                expenseBean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                expenseBean.setStatus(rs.getString("STATUS"));
                expenseBean.setRequestNumber(rs.getString("REQUEST_NUMBER"));
                expenseBean.setGrade(rs.getString("GRADE"));
                expenseBean.setJoiningDate(rs.getString("JOINING_DATE"));  
                expenseBean.setDepartment(rs.getString("DEPARTMENT"));
                expenseBean.setNationality(rs.getString("NATIONALITY"));
                }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return expenseBean;
    }
    public ArrayList<ExpenseStudentBean> getDetailsData(int masterId) {
        ArrayList<ExpenseStudentBean> detailsList=new ArrayList<ExpenseStudentBean>();
        try {   
           connection = AppsproConnection.getConnection();
           String query="select * from "+" "+getSchema_Name()+".EDUCATION_EXPENSE_DETAILS WHERE MASTER_ID=? order by DETAILS_ID DESC";
           ps=connection.prepareStatement(query);
           ps.setInt(1, masterId); 
            rs = ps.executeQuery();
            while(rs.next()){
                ExpenseStudentBean expenseStudentBean=new ExpenseStudentBean();
                expenseStudentBean.setDetailID(Integer.toString(rs.getInt("DETAILS_ID")));
                expenseStudentBean.setStudentName(rs.getString("STUDENT_NAME"));
                expenseStudentBean.setStudentAge(rs.getString("STUDENT_AGE"));
                expenseStudentBean.setStudentGrade(rs.getString("STUDENT_GRADE"));
                expenseStudentBean.setStudentSchool(rs.getString("STUDENT_SCHOOL_NAME"));
                expenseStudentBean.setMasterId(rs.getString("MASTER_ID"));
                expenseStudentBean.setStudentInvoiceNo(rs.getString("STUDENT_INVOICE_NO"));
                expenseStudentBean. setStudentInvoiceDate(rs.getString("STUDENT_INVOICE_DATE"));
                expenseStudentBean.setInvoiceAmount(rs.getString("INVOICE_AMOUNT"));
                expenseStudentBean.setInvoicePayment(rs.getString("INVOICE_PAYMENT"));
                expenseStudentBean.setNote(rs.getString("NOTE"));
                expenseStudentBean.setStudentId(rs.getString("STUDENT_ID"));
                expenseStudentBean.setBankName(rs.getString("BANK_NAME"));
                expenseStudentBean.setIban(rs.getString("IBAN"));
                expenseStudentBean.setPaymentMethod(rs.getString("PAYMENT_METHOD"));
                String query2="select * from "+" "+getSchema_Name()+".EDUCATIONEXPENSEATTACHMENT WHERE DETAILSID=?";
                PreparedStatement ps2=connection.prepareStatement(query2);
                ps2.setInt(1,rs.getInt("DETAILS_ID"));
                ResultSet rs2=ps2.executeQuery();
                ArrayList<ExpenseAttachmentDetails> attachList=new ArrayList<ExpenseAttachmentDetails>();
                while(rs2.next()){
                    ExpenseAttachmentDetails attachbean=new ExpenseAttachmentDetails();
                        attachbean.setData(rs2.getString("ATTACHMENT"));
                        attachbean.setName(rs2.getString("NAME"));
                        attachbean.setId(rs2.getString("DETAILSID"));
                        attachList.add(attachbean);
                    }
                expenseStudentBean.setAttachment(attachList);
                detailsList.add(expenseStudentBean);
                }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            closeResources(connection, ps, rs);
        }
        return detailsList;
    }
    public ArrayList<ExpenseAttachmentDetails> getDetailsAttachments(int detailId) {
        ArrayList<ExpenseAttachmentDetails> attchmentList=new ArrayList<ExpenseAttachmentDetails>();
        try{
            connection=AppsproConnection.getConnection();
            String query="select * from "+" "+getSchema_Name()+".EDUCATIONEXPENSEATTACHMENT where DETAILSID=?";
            ps=connection.prepareStatement(query);
            ps.setInt(1,detailId );
            rs=ps.executeQuery();
            while(rs.next()){
               ExpenseAttachmentDetails attachbean=new ExpenseAttachmentDetails();
               attachbean.setData(rs.getString("ATTACHMENT"));
               attachbean.setName(rs.getString("NAME"));
               attachbean.setId(rs.getString("DETAILSID"));
               attchmentList.add(attachbean);               
                }
        }catch(Exception e){
             e.printStackTrace();;
            }
        finally{
                closeResources(connection, ps, rs);
            }
          return attchmentList;
        }
    public String removeDetailByDetailId(int detailId){
        try{
            connection=AppsproConnection.getConnection();
            String query="delete from "+" "+getSchema_Name()+".EDUCATION_EXPENSE_DETAILS WHERE DETAILs_ID=?";
            ps=connection.prepareStatement(query);
            ps.setInt(1,detailId);
            ps.executeUpdate();
            //delete all attach of this studentr
            query="delete from "+" " + getSchema_Name() +".EDUCATIONEXPENSEATTACHMENT  WHERE DETAILSID=? ";
            ps=connection.prepareStatement(query);
            ps.setInt(1, detailId);
            ps.executeUpdate();
        }catch(Exception e){
            e.printStackTrace();
            }
        finally{
            closeResources(connection, ps, rs);
            }
        return "removed";
        }
    
    public SelfServiceBean insertIntoSelfService(SelfServiceBean bean) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "insert into  "+ " " + getSchema_Name() + ".XX_SELF_SERVICE (TYPE,TYPE_TABLE_NAME,TRANSACTION_ID,PERSON_ID,CREATED_BY,PERSON_NUMBER,CREATION_DATE) "
                    + "VALUES(?,?,?,?,?,?,sysdate)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getType());
            ps.setString(2, bean.getTypeTableName());
            ps.setString(3, bean.getTransactionId());
            ps.setString(4, bean.getPresonId());
            ps.setString(5, bean.getPresonId());
            // ps.setString(6, "sysdate");
            ps.setString(6, bean.getPersonNumber());
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }
    
    public void updateSelfService(String RESPONSE_CODE, String ssType, String transactionId) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            //(transactionId);
            query = "UPDATE   "+ " " + getSchema_Name() + ".XX_SELF_SERVICE  "
                    + "SET STATUS = ?"
                    + "WHERE TYPE = ? AND TRANSACTION_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, RESPONSE_CODE);
            ps.setString(2, ssType);
            ps.setString(3, transactionId);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    public String getMaxExpense() {
            try {
                connection = AppsproConnection.getConnection();
                String query = null;
                query = "SELECT  MAX(MASTER_ID) AS max_ID FROM EDUCATION_EXPENSE_MASTER";
                ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    expenseMaxId = rs.getString("max_ID");
                }
            } catch (Exception e) {
                System.out.println("Error: ");
                e.printStackTrace();
            } finally {
                 closeResources(connection, ps, rs);
            }
            return expenseMaxId;
        }
    public String approvalExpense(SelfServiceBean bean)throws AppsProException {
                SelfServiceBean sfBean = new SelfServiceBean();

                java.util.ArrayList<ApprovalSetupBean> approvalSetupList = new java.util.ArrayList<ApprovalSetupBean>();
                boolean isInsertApprovalList=false;
                
                ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
                ApprovalListBean alBean = new ApprovalListBean();
                ApprovalListDAO approvalListDao = new ApprovalListDAO();
                WorkflowNotificationDAO workflowNotificationDao =
                    new WorkflowNotificationDAO();
                WorkFlowNotificationBean wfn = new WorkFlowNotificationBean();
                PeopleExtraInformationDAO peopleExtra = new PeopleExtraInformationDAO();
                int requestID = Integer.parseInt(getMaxExpense());
                
                sfBean.setType(bean.getType());
                sfBean.setTypeTableName("XXX_EDU_EXPENSE_REQUEST");
                sfBean.setTransactionId(Integer.toString(requestID));
                sfBean.setPresonId(bean.getPresonId());
                sfBean.setCreated_by(bean.getCreated_by());
                sfBean.setPersonNumber(bean.getPersonNumber());
                insertIntoSelfService(sfBean);
                String s = "TEST";
                if (!s.equals("DRAFT")) {
                    approvalSetupList =
                            approvalSetup.getApprovalByEITCode(bean.getType());
                 
                    
                    alBean.setStepLeval("0");
                    alBean.setServiceType(bean.getType());
                    alBean.setTransActionId(Integer.toString(requestID));
                    alBean.setWorkflowId("");
                    alBean.setRolrType("EMP");
                    alBean.setRoleId(bean.getPresonId());
                    alBean.setPersonName(bean.getPersonName());
                    alBean.setResponseCode("SUBMIT");
                    alBean.setNotificationType("FYI");
                    approvalListDao.insertIntoApprovalList(alBean);
                    String emailBody  = "";
                    EmployeeBean empObj = new EmployeeBean();
                    String subject = "";                   
                    try {
                        EXTRA_INFO_BEAN_THREAD = bean;
                        Thread t1 = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    EmployeeBean empObj =
                                        employeeDetails.getEmpNameAndEmail(EXTRA_INFO_BEAN_THREAD.getPresonId(),
                                                                           "", "");
                                    extraInfoForEmail.put("Reciver",
                                                          empObj.getDisplayName());
                                    extraInfoForEmail.put("EIT_NAME",
                                                          "EDUCATION EXPENSE REQUEST");
                                    extraInfoForEmail.put("employeeName",
                                                          EXTRA_INFO_BEAN_THREAD.getPersonName());

                                    //String eitLBL =null;
                                    JSONObject eitLBL = new JSONObject();
                                        eitLBL.put("EIT_NAME","Education Expense Request");
                                    messageBean =
                                            NotificationUtility.buildEmailBody(eitLBL,
                                                                               extraInfoForEmail,
                                                                               EXTRA_INFO_BEAN_THREAD.getType(),
                                                                               "FYIR");
                                    //                body =
                                    //                        rh.getEmailBody("FYI", empObj.getDisplayName(), extraInformationBean.getEit_name(),
                                    //                                        empObj.getDisplayName());
                                    String subject =
                                        rh.getEmailSubject("FYI","Education Expense Request",
                                                           empObj.getDisplayName());

                                    PeopleExtraInformationBean peopleeit=new PeopleExtraInformationBean();
                                    peopleeit.setEit_name("Education Expense Request");
                                    peopleeit.setPersonName(EXTRA_INFO_BEAN_THREAD.getPersonName());
                                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                                               subject,
                                                                               messageBean.getEmailBody(),
                                                                               messageBean.getAttachment(),
                                                                               messageBean.getFileName(),
                                                                               "FYI",
                                                                               EXTRA_INFO_BEAN_THREAD.getTransactionId(),
                                                                               "Education Expense Request",
                                                                               messageBean.getHaveAtt() !=
                                                                               null &&
                                                                               "Y".equals(messageBean.getHaveAtt()) ?
                                                                               true :
                                                                               false,
                                                                               peopleeit);
                                    //                AppsproConnection.LOG.info("Thread start");

                                    //                AppsproConnection.LOG.info("Thread end");
                                }
                            });
                        t1.start();

                    } catch (Exception e) {
                        AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                    alBean = null;
                    ApprovalSetupBean ItratorBean;
                    Iterator<ApprovalSetupBean> approvalSetupListIterator =
                        approvalSetupList.iterator();
                    while (approvalSetupListIterator.hasNext()) {
                        alBean = new ApprovalListBean();
                        ItratorBean = approvalSetupListIterator.next();
                        alBean.setStepLeval(ItratorBean.getApprovalOrder());
                        alBean.setServiceType(bean.getType());
                        alBean.setTransActionId(Integer.toString(requestID));
                        alBean.setWorkflowId("");
                        alBean.setRolrType(ItratorBean.getApprovalType());
                        if (!alBean.getRolrType().equals("POSITION")) {
                            if (alBean.getRolrType().equals("EMP")) {
                                alBean.setRoleId(bean.getPresonId());
                                isInsertApprovalList=true;
                            } else if (alBean.getRolrType().equals("LINE_MANAGER")  && bean.getManagerName()!=null && !bean.getManagerName().isEmpty()) {
                                alBean.setRoleId(bean.getManagerId());
                                alBean.setLineManagerName(bean.getManagerName());
                                isInsertApprovalList=true;
                            } else if (alBean.getRolrType().equals("LINE_MANAGER+1") ) {
                                alBean.setRoleId(bean.getManagerOfManager());
                                alBean.setLineManagerName(bean.getManagerOfMnagerName());
                                isInsertApprovalList=true;
                            } else if (alBean.getRolrType().equals("ROLES")) {
                                 alBean.setRoleId(ItratorBean.getRoleName());
                                 isInsertApprovalList=true;
                             }else if (alBean.getRolrType().equals("Special_Case")) {
     
                                //Caall Dynamic Report Validation -- arg
                                BIReportDAO bIReportDAO = new BIReportDAO();
                                JSONObject obj = new JSONObject();
                                obj.put("reportName", "DynamicValidationReport");
                                obj.put("str", ItratorBean.getSpecialCase());

                                JSONObject jsonObj =
                                    new JSONObject(bIReportDAO.getBiReport(obj));

                                //Value - return
                                System.out.println("VALUE ====== " +
                                                   jsonObj.get("value").toString());
                                alBean.setRoleId(jsonObj.get("value").toString());
                                isInsertApprovalList=true;
                            }

                        } else if (alBean.getRolrType().equals("POSITION")) {
                            alBean.setRoleId(ItratorBean.getRoleName());
                            isInsertApprovalList=true;
                        }

                        alBean.setNotificationType(ItratorBean.getNotificationType());
                        if(isInsertApprovalList==true){  
                        approvalListDao.insertIntoApprovalList(alBean);
                        }else if(isInsertApprovalList==false){  
                         ExpenseBean beanofexpense=new ExpenseBean();
                         beanofexpense.setStatus("Approved");
                         beanofexpense.setMasterId(String.valueOf(requestID));
                         UpdateStatus(beanofexpense);
                        }
                         alBean = null;
                    }
                    if (!approvalSetupList.isEmpty()) {
                        wfn.setMsgTitle("Education Expense Request");
                        wfn.setMsgBody("Education Expense Request for" +
                                       bean.getPresonId());
                        wfn.setPersonName(bean.getPersonName());
                        ApprovalListBean rolesBean =
                            approvalListDao.getRoles(Integer.toString(requestID),
                                                     bean.getType());
                        
                        wfn.setReceiverType(rolesBean.getRolrType());
                        wfn.setReceiverId(rolesBean.getRoleId());
                        wfn.setType(rolesBean.getNotificationType());
                        wfn.setRequestId(Integer.toString(requestID));
                        wfn.setStatus("OPEN");
                        wfn.setSelfType(bean.getType());
                        workflowNotificationDao.insertIntoWorkflow(wfn);
                if (rolesBean.getRolrType().equals("LINE_MANAGER")) {
                    try {
                        EXTRA_INFO_BEAN_THREAD = bean;
                        ROLE_NOTI_TYPE = rolesBean.getNotificationType();
                        Thread t2 = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    EmployeeBean empObj =
                                        employeeDetails.getEmpNameAndEmail(EXTRA_INFO_BEAN_THREAD.getManagerId(),
                                                                           "",
                                                                           "");
                                    extraInfoForEmail.put("Reciver",
                                                          empObj.getDisplayName());
                                    extraInfoForEmail.put("EIT_NAME",
                                                          "Education Expense Request");
                                    extraInfoForEmail.put("employeeName",
                                                          EXTRA_INFO_BEAN_THREAD.getPersonName());
                                    JSONObject eitLBL = new JSONObject();
                                        eitLBL.put("EIT_NAME","Education Expense Request");
                                    messageBean =
                                            NotificationUtility.buildEmailBody(eitLBL,
                                                                               extraInfoForEmail,
                                                                               EXTRA_INFO_BEAN_THREAD.getType(),
                                                                               ROLE_NOTI_TYPE);

                                    //                        body =
                                    //                        rh.getEmailBody(rolesBean.getNotificationType(),
                                    //                                        empObj.getDisplayName(),
                                    //                                        extraInformationBean.getEit_name(),
                                    //                                        extraInformationBean.getPersonName());
                                    String subject =
                                        rh.getEmailSubject(ROLE_NOTI_TYPE,
                                                           "Education Expense Request",
                                                           EXTRA_INFO_BEAN_THREAD.getPersonName());
                                    PeopleExtraInformationBean peopleeit=new PeopleExtraInformationBean();
                                    peopleeit.setEit_name("Education Expense Request");
                                    peopleeit.setPersonName(EXTRA_INFO_BEAN_THREAD.getPersonName());
                            

                                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                                               subject,
                                                                               messageBean.getEmailBody(),
                                                                               null,
                                                                               null,
                                                                               ROLE_NOTI_TYPE,
                                                                               EXTRA_INFO_BEAN_THREAD.getTransactionId(),
                                                                               "Education Expense Request",
                                                                               false,
                                                                               peopleeit);
                                }
                            });
                        t2.start();

                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                        AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("LINE_MANAGER+1")) {
                    try {
                        EXTRA_INFO_BEAN_THREAD = bean;
                        ROLE_NOTI_TYPE = rolesBean.getNotificationType();
                        Thread t3 = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    EmployeeBean empObj =
                                        employeeDetails.getEmpNameAndEmail(EXTRA_INFO_BEAN_THREAD.getManagerOfManager(),
                                                                           "",
                                                                           "");
                                    extraInfoForEmail.put("Reciver",
                                                          empObj.getDisplayName());
                                    extraInfoForEmail.put("EIT_NAME",
                                                          "Education Expense Request");
                                    extraInfoForEmail.put("employeeName",
                                                          EXTRA_INFO_BEAN_THREAD.getPersonName());

                                    JSONObject eitLBL = new JSONObject();
                                        eitLBL.put("EIT_NAME","Education Expense Request");

                                    messageBean =
                                            NotificationUtility.buildEmailBody(eitLBL,
                                                                               extraInfoForEmail,
                                                                               EXTRA_INFO_BEAN_THREAD.getType(),
                                                                               ROLE_NOTI_TYPE);
                                    //                        body =
                                    //                        rh.getEmailBody(rolesBean.getNotificationType(),
                                    //                                        empObj.getDisplayName(),
                                    //                                        extraInformationBean.getEit_name(),
                                    //                                        extraInformationBean.getPersonName());
                                    String subject =
                                        rh.getEmailSubject(ROLE_NOTI_TYPE,
                                                           "Education Expense Request",
                                                           EXTRA_INFO_BEAN_THREAD.getPersonName());
                                    PeopleExtraInformationBean peopleeit=new PeopleExtraInformationBean();
                                    peopleeit.setEit_name("Education Expense Request");
                                    peopleeit.setPersonName(EXTRA_INFO_BEAN_THREAD.getPersonName());

                                    CredentialStoreClassMail.SEND_NOTIFICATION(empObj.getEmail(),
                                                                               subject,
                                                                               messageBean.getEmailBody(),
                                                                               null,
                                                                               null,
                                                                               ROLE_NOTI_TYPE,
                                                                               EXTRA_INFO_BEAN_THREAD.getTransactionId(),
                                                                               "Education Expense Request",
                                                                               false,
                                                                               peopleeit);
                                }
                            });
                        t3.start();

                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                        AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("ROLES")) {
                    try {
                        EXTRA_INFO_BEAN_THREAD = bean;
                        ROLE_NOTI_TYPE = rolesBean.getNotificationType();
                        ROLE_NOTI_ID = rolesBean.getRoleId();
                        Thread t4 = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Map<String, String> paramMAp =
                                        new HashMap<String, String>();
                                    paramMAp.put("P_ROLEID", ROLE_NOTI_ID);
                                    JSONObject respone =
                                        BIReportModel.runReport("EmployeeRoleForCustomRoleReport",
                                                                paramMAp);

                                    JSONObject dataDS =
                                        respone.getJSONObject("DATA_DS");
                                    JSONArray g1 = dataDS.getJSONArray("G_1");
                                    for (int i = 0; i < g1.length(); i++) {
                                        JSONObject data =
                                            (JSONObject)g1.get(i);
                                        extraInfoForEmail.put("Reciver",
                                                              data.getString("FULL_NAME"));
                                        extraInfoForEmail.put("EIT_NAME",
                                                              "Education Expense Request");
                                        extraInfoForEmail.put("employeeName",
                                                              EXTRA_INFO_BEAN_THREAD.getPersonName());
                                        JSONObject eitLBL = new JSONObject();
                                            eitLBL.put("EIT_NAME","Education Expense Request");


                                        messageBean =
                                                NotificationUtility.buildEmailBody(eitLBL,
                                                                                   extraInfoForEmail,
                                                                                   EXTRA_INFO_BEAN_THREAD.getType(),
                                                                                   ROLE_NOTI_TYPE);
                                        PeopleExtraInformationBean peopleeit=new PeopleExtraInformationBean();
                                        peopleeit.setEit_name("Education Expense Request");
                                        peopleeit.setPersonName(EXTRA_INFO_BEAN_THREAD.getPersonName());
                                        //                            body =
                                        //                            rh.getEmailBody(rolesBean.getNotificationType(),
                                        //                                            data.getString("FULL_NAME"),
                                        //                                            extraInformationBean.getEit_name(),
                                        //                                            extraInformationBean.getPersonName());
                                        String subject =
                                            rh.getEmailSubject(ROLE_NOTI_TYPE,
                                                               "Education Expense Request",
                                                               EXTRA_INFO_BEAN_THREAD.getPersonName());

                                        CredentialStoreClassMail.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
                                                                                   subject,
                                                                                   messageBean.getEmailBody(),
                                                                                   null,
                                                                                   null,
                                                                                   ROLE_NOTI_TYPE,
                                                                                   EXTRA_INFO_BEAN_THREAD.getTransactionId(),
                                                                                   "Education Expense Request",
                                                                                   false,
                                                                                   peopleeit);
                                    }

                                }
                            });
                        t4.start();


                    } catch (Exception e) {
                        //("Error: ");
                        e.printStackTrace();
                        AppsproConnection.LOG.error("ERROR", e);
                        throw new AppsProException(e);
                    }
                } else if (rolesBean.getRolrType().equals("POSITION")) {
                    JSONObject dataDS = null;
                    try {

                        EXTRA_INFO_BEAN_THREAD = bean;
                        ROLE_NOTI_TYPE = rolesBean.getNotificationType();
                        ROLE_NOTI_ID = rolesBean.getRoleId();
                        Thread t5 = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Map<String, String> paramMAp =
                                        new HashMap<String, String>();
                                    paramMAp.put("positionId", ROLE_NOTI_ID);
                                    JSONObject respone =
                                        BIReportModel.runReport("EmailForPositionsReport",
                                                                paramMAp);
                                    JSONObject dataDS =
                                        respone.getJSONObject("DATA_DS");
                                    JSONArray g1 = new JSONArray();
                                    Object jsonTokner =
                                        new JSONTokener(dataDS.get("G_1").toString()).nextValue();
                                    if (jsonTokner instanceof JSONObject) {
                                        JSONObject arrJson =
                                            dataDS.getJSONObject("G_1");

                                        g1.put(arrJson);
                                    } else {
                                        JSONArray arrJson =
                                            dataDS.getJSONArray("G_1");
                                        g1 = arrJson;
                                    }
                                    for (int i = 0; i < g1.length(); i++) {
                                        JSONObject data =
                                            (JSONObject)g1.get(i);
                                        extraInfoForEmail.put("Reciver",
                                                              data.getString("DISPLAY_NAME"));
                                        extraInfoForEmail.put("EIT_NAME",
                                                              "Education Expense Request");
                                        extraInfoForEmail.put("employeeName",
                                                              EXTRA_INFO_BEAN_THREAD.getPersonName());
                                        JSONObject eitLBL = new JSONObject();
                                            eitLBL.put("EIT_NAME","Education Expense Request");

                                        messageBean =
                                                NotificationUtility.buildEmailBody(eitLBL,
                                                                                   extraInfoForEmail,
                                                                                   EXTRA_INFO_BEAN_THREAD.getType(),
                                                                                   ROLE_NOTI_TYPE);


                                        //                            body =
                                        //                                    rh.getEmailBody(rolesBean.getNotificationType(),
                                        //                                    data.getString("DISPLAY_NAME"),
                                        //                                    extraInformationBean.getEit_name(),
                                        //                                    extraInformationBean.getPersonName());
                                        String subject =
                                            rh.getEmailSubject(ROLE_NOTI_TYPE,
                                                               "Education Expense Request",
                                                               EXTRA_INFO_BEAN_THREAD.getPersonName());
                                        PeopleExtraInformationBean peopleeit=new PeopleExtraInformationBean();
                                        peopleeit.setEit_name("Education Expense Request");
                                        peopleeit.setPersonName(EXTRA_INFO_BEAN_THREAD.getPersonName());

                                        CredentialStoreClassMail.SEND_NOTIFICATION(data.getString("EMAIL_ADDRESS"),
                                                                                   subject,
                                                                                   messageBean.getEmailBody(),
                                                                                   null,
                                                                                   null,
                                                                                   ROLE_NOTI_TYPE,
                                                                                   EXTRA_INFO_BEAN_THREAD.getTransactionId(),
                                                                                   "Education Expense Request",
                                                                                   false,
                                                                                   peopleeit);
                                    }
                                }
                            });
                        t5.start();


                    } catch (Exception e) {

                        // e.printStackTrace();
                        AppsproConnection.LOG.error("ERROR", e);
                        // throw new AppsProException(e);
                    }
                }
                   } else {

                try {
                    peopleExtra.updateStatus("APPROVED","", requestID);
                    ExpenseBean expenseBean=new ExpenseBean();
                    expenseBean.setStatus("Approved");
                    expenseBean.setMasterId(Integer.toString(requestID));
                    UpdateStatus(expenseBean);
                } catch (AppsProException e) {
                }
                updateSelfService("APPROVED", sfBean.getType(),
                                          sfBean.getTransactionId());
                    }

                }
                return "done";
            }
    public ExpenseBean UpdateStatus(ExpenseBean expenseBean) {

        try {
            connection = AppsproConnection.getConnection();
        
                String query =
                    "update " + " " + getSchema_Name() + ".EDUCATION_EXPENSE_MASTER set STATUS=?  WHERE MASTER_ID=? ";
                ps = connection.prepareStatement(query);

                ps.setString(1, expenseBean.getStatus()); 
                ps.setString(2, expenseBean.getMasterId());
                ps.executeUpdate();
           

        } catch (Exception e) {
            System.out.println("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return expenseBean;
    }
    public String getExpenseApprovalSetup() {
        int countApproval = 0;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select * from  " + " " + getSchema_Name() + ".XXX_APROVALS_SETUP  WHERE EIT_CODE='XXX_EDU_EXPENSE_REQUEST' and NOTIFICATION_TYPE='FYA'";

            ps = connection.prepareStatement(query);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                countApproval++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return String.valueOf(countApproval);
    }

    public ArrayList<String> getAllExpenseAmountPerEmp(String personNumber) {
        ArrayList<Integer> masters=new ArrayList<Integer>();
        ArrayList<String> result=new ArrayList<String>();
        int totalAmount=0;
        Date today=new Date();
        SimpleDateFormat ft=new SimpleDateFormat("yyyy");
        String year=ft.format(today);
        String startdate=year+"-01-01";
        String endDate=year+"-12-31";
        try {
            connection = AppsproConnection.getConnection();
            String query="select SUM(INVOICE_AMOUNT) AS MASTERPAY FROM"+" "+getSchema_Name()+".EDUCATION_EXPENSE_DETAILS  WHERE MASTER_ID IN(SELECT MASTER_ID FROM EDUCATION_EXPENSE_MASTER WHERE PERSON_NUMBER=? And Status<>'REJECTED' AND (REQUEST_DATE BETWEEN ? AND ? ))";
            ps=connection.prepareStatement(query);
            ps.setString(1, personNumber);
            ps.setString(2, startdate);
            ps.setString(3, endDate);
            rs=ps.executeQuery();
            if(rs.next()){
                result.add(Integer.toString(rs.getInt("MASTERPAY")));
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
      return result;
    }
    public boolean checkStudentRequestPerSemister(String personNumber,String semister,String student){
        boolean studentExist=false;
            try {
                connection = AppsproConnection.getConnection();
                String query =
                    "select COUNT(*) COUNT FROM " + " " + getSchema_Name() +
                    ".EDUCATION_EXPENSE_DETAILS  WHERE MASTER_ID IN(SELECT MASTER_ID FROM EDUCATION_EXPENSE_MASTER WHERE PERSON_NUMBER=? And Status<>'REJECTED' AND SEMESTER=?) And STUDENT_NAME=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, personNumber);
                ps.setString(2, semister);
                ps.setString(3, student);
                rs = ps.executeQuery();
                if (rs.next()) {
                    int count = rs.getInt("COUNT");
                    if (count > 0) {
                        studentExist = true;
                    } else {
                        studentExist = false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                closeResources(connection, ps, rs);
            }

            return studentExist;
        }
    public boolean checkStudentRequestPerSemisterEdit(String masterId,String personNumber,String semister,String student){
        boolean studentExist=false;
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "select COUNT(*) COUNT FROM " + " " + getSchema_Name() +
                ".EDUCATION_EXPENSE_DETAILS  WHERE MASTER_ID IN(SELECT MASTER_ID FROM EDUCATION_EXPENSE_MASTER WHERE PERSON_NUMBER=? And Status<>'REJECTED' AND SEMESTER=? AND MASTER_ID<>?) And STUDENT_NAME=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, personNumber);
            ps.setString(2, semister);
            ps.setInt(3, Integer.parseInt(masterId));
            ps.setString(4, student);
            rs = ps.executeQuery();
            if (rs.next()) {
                int count = rs.getInt("COUNT");
                if (count > 0) {
                    studentExist = true;
                } else {
                    studentExist = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            closeResources(connection, ps, rs);
        }

        return studentExist;
    }
    public String getEducationSequence(){
        String requestNumber="";
                          try {   
                             connection = AppsproConnection.getConnection();
                             String query="select EDUCATION_EXPENSE_NUMBER_SEQ.nextval as requestNumber from dual";
                             ps=connection.prepareStatement(query);
                            rs = ps.executeQuery();
                              if(rs.next()){
                                requestNumber="EDU_00"+rs.getString("requestNumber");
                                  }
                          }catch(Exception e){
                              e.printStackTrace();
                              }finally{
                                  closeResources(connection, ps, rs);
                              }
                      return requestNumber;
                      }

    public ArrayList<ExpenseBean> GetAllEducationExpenseRequest() {
        ArrayList<ExpenseBean> expenseList = new ArrayList<ExpenseBean>();
        connection = AppsproConnection.getConnection();
        String query = "";
        try {
            query =
                    "select * from "+getSchema_Name()+".EDUCATION_EXPENSE_MASTER where STATUS= 'Withdraw'  OR   STATUS = 'Pending Approval' OR STATUS = 'Approved'OR STATUS = 'DRAFT'ORDER BY REQUEST_DATE DESC";
            ps = connection.prepareStatement(query.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                ExpenseBean bean = new ExpenseBean();
                bean.setRequestDate(rs.getString("REQUEST_DATE"));
                bean.setDisplayName(rs.getString("DISPLAY_NAME"));
                bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setMasterId(rs.getString("MASTER_ID"));
                expenseList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return expenseList;
    }
    
    public ArrayList<EducationRequestSoapBean> GetAllEducationExpenseRequestForWS() {
        ArrayList<EducationRequestSoapBean> expenseList = new ArrayList<EducationRequestSoapBean>();
        EducationRequestSoapBean bean = null;
        connection = AppsproConnection.getConnection();
        String query = "";
        try {
            query =
                    "SELECT\n" + 
                    "    eem.master_id,\n" + 
                    "    eem.person_number,\n" + 
                    "    eem.display_name,\n" + 
                    "    eem.department,\n" + 
                    "    eem.position_classification,\n" + 
                    "    eem.semester,\n" + 
                    "    eem.request_date,\n" + 
                    "    eem.request_number,\n" + 
                    "    eem.academic_year,\n" + 
                    "    eem.status,\n" + 
                    "    eed.student_name,\n" + 
                    "    eed.student_age,\n" + 
                    "    eed.student_school_name,\n" + 
                    "    eed.invoice_amount,\n" + 
                    "    eed.payment_method,\n" + 
                    "    wfn.receiver_type,\n" + 
                    "    wfn.receiver_id,\n" + 
                    "    wfn.status status_notification,\n" + 
                    "    wfn.type\n" + 
                    "FROM\n" + 
                    "    education_expense_master    eem,\n" + 
                    "    education_expense_details   eed,\n" + 
                    "    xx_workflow_notification    wfn\n" + 
                    "WHERE\n" + 
                    "    eem.master_id = eed.master_id\n" + 
                    "    AND eem.master_id = wfn.request_id\n" + 
                    "    AND wfn.id = (\n" + 
                    "        SELECT\n" + 
                    "            MAX(wfn1.id)\n" + 
                    "        FROM\n" + 
                    "            xx_workflow_notification wfn1\n" + 
                    "        WHERE\n" + 
                    "            wfn1.request_id = wfn.request_id\n" + 
                    "    )\n" + 
                    "ORDER BY\n" + 
                    "    wfn.creation_date";
            ps = connection.prepareStatement(query.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new EducationRequestSoapBean();
                bean.setMasterId(rs.getString("MASTER_ID"));
                bean.setEmployeeNumber(rs.getString("PERSON_NUMBER"));
                bean.setEmployeeName(rs.getString("DISPLAY_NAME"));
                bean.setEmployeeDepartment(rs.getString("DEPARTMENT"));
                bean.setEmployeePosition(rs.getString("POSITION_CLASSIFICATION"));
                if(rs.getString("SEMESTER").equals("semisterOne")){
                    bean.setSemister("First");
                }
                else if(rs.getString("SEMESTER").equals("semisterTwo")){
                    bean.setSemister("Second");
                }
                else{
                    bean.setSemister("Full Year"); 
                }
                bean.setRequestDate(rs.getString("REQUEST_DATE"));
                bean.setRequestNumber(rs.getString("REQUEST_NUMBER"));
                bean.setAcademicYear(rs.getString("ACADEMIC_YEAR"));
                if(rs.getString("STATUS").equals("Pending Approval")){
                    bean.setStatus("Under Approval");
                }
                else if(rs.getString("STATUS").equals("Approved")){
                    bean.setStatus("Workflow Completed");
                }
                else{
                    bean.setStatus("Rejected"); 
                }
                bean.setStudentName(rs.getString("STUDENT_NAME"));
                bean.setStudentAge(rs.getString("STUDENT_AGE"));
                bean.setStudentSchool(rs.getString("STUDENT_SCHOOL_NAME"));
                bean.setInvoiceAmount(rs.getString("INVOICE_AMOUNT"));
                if(rs.getString("PAYMENT_METHOD").equals("Schoolaccount")){
                    bean.setPaymentMethod("School Account");
                }
                else if(rs.getString("PAYMENT_METHOD").equals("EmployeeAccount")){
                    bean.setPaymentMethod("Employee Account");
                }
                if(rs.getString("STATUS").equals("Pending Approval") &&  rs.getString("STATUS_NOTIFICATION").equals("OPEN")){
                    positionRoles = new PositionRolesBean();
                    if(rs.getString("RECEIVER_TYPE").equals("POSITION")){
                        positionRoles = positionRoles.getPositionReport(rs.getString("RECEIVER_ID"));
                        bean.setPendingWith(positionRoles.getEmployeeName()); 
                    }
                    else if(rs.getString("RECEIVER_TYPE").equals("ROLES")){
                        positionRoles = positionRoles.getRolesReport(rs.getString("RECEIVER_ID"));
                        bean.setPendingWith(positionRoles.getEmployeeRoleName()); 
                    }
                    else if(rs.getString("RECEIVER_TYPE").equals("LINE_MANAGER")){
                        empObj =
                                employeeDetails.getEmpNameAndEmail(rs.getString("RECEIVER_ID"), "", "");
                        bean.setPendingWith(empObj.getDisplayName()); 
                    }
                    else if(rs.getString("RECEIVER_TYPE").equals("EMP")){
                        empObj =
                                employeeDetails.getEmpNameAndEmail(rs.getString("RECEIVER_ID"), "", "");
                        bean.setPendingWith(empObj.getDisplayName());
                    }
                }
                else{
                    bean.setPendingWith("-");
                }
                expenseList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return expenseList;
    }
    
    
    

    //TO do add filteration SHADI on 30/08/2020
    public ArrayList<EducationRequestSoapBean> GetAllEducationExpenseRequestForExcel(PeopleExtraInformationBean body) {
        ArrayList<EducationRequestSoapBean> expenseList = new ArrayList<EducationRequestSoapBean>();
        EducationRequestSoapBean bean = null;
        String status = null;
        String personNumber = null;
        if("PENDING_APPROVED".equals(body.getStatus())){    
            status = "Pending Approval";
        }
        if("APPROVED".equals(body.getStatus())){
            status = "Approved";
        }
        if("Withdraw".equals(body.getStatus())){
            status = "Withdraw";
        }
        if("All".equals(body.getStatus())){
            status = null;
        }
        
        if("".equals(body.getPerson_number())){
            personNumber = null;
        }
        else{
            personNumber = body.getPerson_number();
        }
        connection = AppsproConnection.getConnection();
        String query = "";
        try {
            query =
                    "SELECT eem.master_id,\n" + 
                    "       eem.person_number,\n" + 
                    "       eem.display_name,\n" + 
                    "       eem.department,\n" + 
                    "       eem.position_classification,\n" + 
                    "       eem.semester,\n" + 
                    "       eem.request_date,\n" + 
                    "       eem.request_number,\n" + 
                    "       eem.academic_year,\n" + 
                    "       eem.status,\n" + 
                    "       eed.student_name,\n" + 
                    "       eed.student_age,\n" + 
                    "       eed.student_school_name,\n" + 
                    "       eed.invoice_amount,\n" + 
                    "       eed.payment_method\n" + 
                    "  FROM education_expense_master eem, education_expense_details eed\n" + 
                    " WHERE eem.master_id = eed.master_id AND eem.person_number = NVL(?,eem.person_number) AND eem.status = NVL(?,eem.status)";
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, personNumber);
            ps.setString(2, status);
            rs = ps.executeQuery();
            System.out.println(query);
            while (rs.next()) {
                bean = new EducationRequestSoapBean();
                bean.setMasterId(rs.getString("MASTER_ID"));
                bean.setEmployeeNumber(rs.getString("PERSON_NUMBER"));
                bean.setEmployeeName(rs.getString("DISPLAY_NAME"));
                bean.setEmployeeDepartment(rs.getString("DEPARTMENT"));
                bean.setEmployeePosition(rs.getString("POSITION_CLASSIFICATION"));
                if(rs.getString("SEMESTER").equals("semisterOne")){
                    bean.setSemister("First");
                }
                else if(rs.getString("SEMESTER").equals("semisterTwo")){
                    bean.setSemister("Second");
                }
                else{
                    bean.setSemister("Full Year"); 
                }
                bean.setRequestDate(rs.getString("REQUEST_DATE"));
                bean.setRequestNumber(rs.getString("REQUEST_NUMBER"));
                bean.setAcademicYear(rs.getString("ACADEMIC_YEAR"));
                if(rs.getString("STATUS").equals("Pending Approval")){
                    bean.setStatus("Under Approval");
                }
                else if(rs.getString("STATUS").equals("Approved")){
                    bean.setStatus("Workflow Completed");
                }
                else{
                    bean.setStatus("Rejected"); 
                }
                bean.setStudentName(rs.getString("STUDENT_NAME"));
                bean.setStudentAge(rs.getString("STUDENT_AGE"));
                bean.setStudentSchool(rs.getString("STUDENT_SCHOOL_NAME"));
                bean.setInvoiceAmount(rs.getString("INVOICE_AMOUNT"));
                if(rs.getString("PAYMENT_METHOD").equals("Schoolaccount")){
                    bean.setPaymentMethod("School Account");
                }
                else if(rs.getString("PAYMENT_METHOD").equals("EmployeeAccount")){
                    bean.setPaymentMethod("Employee Account");
                }
                expenseList.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return expenseList;
    }
    
    public String MultipleEntryCountSequence() {
        String result = null;
        try {

            connection = AppsproConnection.getConnection();
            String query =
                "SELECT  " + " " + getSchema_Name() + ".EDUCATION_ALLOWANCE_SEQ.NEXTVAL  from dual";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                result = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
                    closeResources(connection, ps, rs);
                }
        return result;
    }
}
