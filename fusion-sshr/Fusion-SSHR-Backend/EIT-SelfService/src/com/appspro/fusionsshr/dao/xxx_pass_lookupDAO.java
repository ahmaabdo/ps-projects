/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.xxx_pass_lookupBean;
import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author lenovo
 */
public class xxx_pass_lookupDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public ArrayList<xxx_pass_lookupBean> getXXX_pass_lookup(String searchKey) {

        ArrayList<xxx_pass_lookupBean> xxx_pass_lookupList = new ArrayList<xxx_pass_lookupBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = "SElect * from  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP where NAME=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, searchKey);
            rs = ps.executeQuery();
            while (rs.next()) {
                xxx_pass_lookupBean xxx_bean = new xxx_pass_lookupBean();
                xxx_bean.setId(rs.getString("ID"));
                xxx_bean.setName(rs.getString("NAME"));
                xxx_bean.setCode(rs.getString("CODE"));
                xxx_bean.setValueEnglish(rs.getString("VALUE_EN"));
                xxx_bean.setValueArabic(rs.getString("VALUE_AR"));
                xxx_pass_lookupList.add(xxx_bean);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return xxx_pass_lookupList;

    }

    public ArrayList<xxx_pass_lookupBean> getAllPassLookup() {

        ArrayList<xxx_pass_lookupBean> xxx_pass_lookupList = new ArrayList<xxx_pass_lookupBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = "SElect * from  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP ";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                xxx_pass_lookupBean xxx_bean = new xxx_pass_lookupBean();
                xxx_bean.setId(rs.getString("ID"));
                xxx_bean.setName(rs.getString("NAME"));
                xxx_bean.setCode(rs.getString("CODE"));
                xxx_bean.setValueEnglish(rs.getString("VALUE_EN"));
                xxx_bean.setValueArabic(rs.getString("VALUE_AR"));

                xxx_pass_lookupList.add(xxx_bean);

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return xxx_pass_lookupList;

    }

    public xxx_pass_lookupBean insertOrUpdateXXX_PassLookup(xxx_pass_lookupBean XXX_Passlookup, String transactionType) {
        try {

            connection = AppsproConnection.getConnection();
            String query = null;

            if (transactionType.equals("ADD")) {

                query = "insert into  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP (NAME,CODE,VALUE_AR,VALUE_EN) VALUES(?,?,?,?)";

                ps = connection.prepareStatement(query);

                ps.setString(1, XXX_Passlookup.getName());
                ps.setString(2, XXX_Passlookup.getCode());
                ps.setString(3, XXX_Passlookup.getValueArabic());
                ps.setString(4, XXX_Passlookup.getValueEnglish());
            } else if (transactionType.equals("EDIT")) {
                query = "UPDATE  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP set NAME=?,CODE=?,VALUE_AR=?,VALUE_EN=? WHERE ID=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, XXX_Passlookup.getName());
                ps.setString(2, XXX_Passlookup.getCode());
                ps.setString(3, XXX_Passlookup.getValueArabic());
                ps.setString(4, XXX_Passlookup.getValueEnglish());
                ps.setString(5, XXX_Passlookup.getId());
            }

            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return XXX_Passlookup;
    }

    public String getCodeLookup(String code) {

        String result = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = "select code FROM  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP WHERE code=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, code);

            rs = ps.executeQuery();
            while (rs.next()) {
                result = rs.getString(1);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return result;
    }

    public String getnameLookup(String name) {

        String result = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = "select name FROM  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP WHERE name=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, name);

            rs = ps.executeQuery();
            while (rs.next()) {
                result = rs.getString(1);
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);

        }

        return result;
    }

//     public String getCodeLookupCheckUpdate(String codeExit,String codeChange) {
//
//        String result = null;
//        try {
//            connection = AppsproConnection.getConnection();
//            String query = "select * from (select code from  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP where code !=?) where code=?";
//            ps = connection.prepareStatement(query);
//            ps.setString(1, codeExit);
//            ps.setString(2, codeChange);
//
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                result = rs.getString(1);
//            }
//
//        } catch (Exception e) {
//            //("Error: ");
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//        } finally {
//            closeResources(connection, ps, rs);
//        }
//
//        return result;
//    }
//     public String getNameLookupCheckUpdate(String nameExit,String nameChange) {
//
//        String result = null;
//        try {
//            connection = AppsproConnection.getConnection();
//            String query = "select * from (select name from  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP where name !=?) where name=?";
//            ps = connection.prepareStatement(query);
//            ps.setString(1, nameExit);
//            ps.setString(2, nameChange);
//
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                result = rs.getString(1);
//            }
//
//        } catch (Exception e) {
//            //("Error: ");
//           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
//        } finally {
//            closeResources(connection, ps, rs);
//        }
//
//        return result;
//    }
//    
    public String getLookupCheckUpdate(String name, String code, String id) {

        String result = null;
        try {
            connection = AppsproConnection.getConnection();
            String query = " select *from(  select ID ,code ,name from  "+ " " + getSchema_Name() + ".XXX_PAAS_LOOKUP WHERE ID !=?)where name=?and code=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            ps.setString(2, name);
            ps.setString(3, code);

            rs = ps.executeQuery();
            while (rs.next()) {
                result = rs.getString("name");

            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }

        return result;
    }
    
    
    public String deleteLookupRecord(String id) {

           connection = AppsproConnection.getConnection();
           String query;
           int checkResult=0;

           try {
               query = "DELETE FROM "+" "+getSchema_Name()+".XXX_PAAS_LOOKUP WHERE ID=?";
               ps = connection.prepareStatement(query);
               ps.setInt(1, Integer.parseInt(id));
               checkResult = ps.executeUpdate();
               
           } catch (Exception e) {
               //("Error: ");
              e.printStackTrace(); AppsproConnection.LOG.error("ERROR", e);
           } finally {
               closeResources(connection, ps, rs);
           }
           return Integer.toString(checkResult);
       }
    

}
