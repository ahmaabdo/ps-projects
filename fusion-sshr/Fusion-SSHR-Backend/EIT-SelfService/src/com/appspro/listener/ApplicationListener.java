package com.appspro.listener;

import com.appspro.fusionsshr.dao.SaaSPositionDetails;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import utilities.NotificationUtility;

public class ApplicationListener implements ServletContextListener {
    public ApplicationListener() {
        super();
    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("//////////////////////////////////////////////////");
        NotificationUtility.getAll();
        new SaaSPositionDetails().getOrgDetails();
        System.out.println("///////////////////  Cached ///////////////////////");
        System.out.println("///////////////////  Context Init /////////////////");
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
