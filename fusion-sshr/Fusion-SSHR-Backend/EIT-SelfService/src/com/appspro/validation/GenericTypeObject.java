package com.appspro.validation;

public class GenericTypeObject<T> {
    private T t;

       public void setValue(T t) { this.t = t; }
       public T get() { return t; }
    public GenericTypeObject() {
        super();
    }
}
