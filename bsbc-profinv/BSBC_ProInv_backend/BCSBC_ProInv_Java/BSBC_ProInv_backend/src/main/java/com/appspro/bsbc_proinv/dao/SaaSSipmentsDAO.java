/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.dao;

import com.appspro.helper.Helper;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 *
 * @author Anas Alghawi
 */
public class SaaSSipmentsDAO {
      private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

        public static String jsonGetRequestNonParam(String endPoint) {
        String json = null;
        String pathParameter = endPoint+"?onlyData=true";
        try {
            
            URL url = new URL(Helper.saasURL+"fscmRestApi/resources/11.13.18.05/shipments/"+pathParameter);
            System.out.println(url);
            
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic U0NNLmFkbWluOmFzZGFzZDEyMw==");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();
            InputStream inStream = connection.getInputStream();
            json = streamToString(inStream); // input stream to string
        } catch (IOException ex) {
            ex.printStackTrace();
        }
         System.out.println(json);
        return json;
    }
}
