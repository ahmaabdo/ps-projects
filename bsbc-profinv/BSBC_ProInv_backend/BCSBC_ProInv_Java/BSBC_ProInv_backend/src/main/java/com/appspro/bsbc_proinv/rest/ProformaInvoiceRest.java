/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.rest;


//import javax.ws.rs.GET;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.appspro.bsbc_proinv.dao.ProformaInvoiceDAO;
import com.appspro.bsbc_proinv.dao.AcknowledgeDAO;
import com.appspro.helper.Helper;

/**
 *
 * @author Anas Alghawi
 */
@Path("/ProformaInvoiceRest")
public class ProformaInvoiceRest {
   ProformaInvoiceDAO serviceProformaInvoice = new ProformaInvoiceDAO();

    @POST   
    @Produces({"application/json"})
    @Consumes(MediaType.APPLICATION_JSON)
    public String  addNewProformaInvoice(String bean) throws Exception {
//     serviceProformaInvoice.sendPost(bean);
//     return bean ;
        String url = Helper.paasURL + "shipments";
        String returnValue = Helper.callPostRest(url, bean);
        String returnValue1 = "{\"result\":\"sucess\"}";
        if (returnValue != null && !returnValue.isEmpty()) {
            System.out.println("return+" + returnValue + "retun");
            return returnValue;
        }
        return returnValue1;

    }
    
    @GET   
    @Produces({"application/json"})
    @Consumes(MediaType.APPLICATION_JSON)
    public String  getProformaInvoice ( ) throws Exception {
    
     return  serviceProformaInvoice.jsonGetRequestNonParam(); 
        
    }
    @GET  
    @Path("/custrx/{piNumber}")
    @Produces({"application/json"})
    @Consumes(MediaType.APPLICATION_JSON)
    public String  getProformaInvoiceTRX (@PathParam("piNumber") String piNumber ) throws Exception {
    
     return  serviceProformaInvoice.jsonGetRequestTRX(piNumber); 
        
    }
    
    @GET  
    @Path("/{pinumber}")
    @Produces({"application/json"})
    @Consumes(MediaType.APPLICATION_JSON)
    public String  getProformaInvoiceByPi (@PathParam("pinumber") String piNumber ) throws Exception {
    
     return  serviceProformaInvoice.jsonGetRequestParam(piNumber); 
        
    }


    
}

