/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.appspro.login.Login;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
/**
 *
 * @author Anas Alghawi
 */
@Path("/login")
public class LogInRest {

    @POST
    @Consumes("application/json")
//    @Path("/login2")
    public String getEmpDetails(String body,
            @Context HttpServletRequest request) {
        String jsonInString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(body);
            String userName = actualObj.get("userName").asText();
            String password = actualObj.get("password").asText();
            Login login = new Login();
            boolean result = login.validateLogin(userName, password);
            jsonInString = "{'result':" + result + "}";
            System.out.println(jsonInString);
            return jsonInString ;
        }
catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonInString;
    }

    public LogInRest() {
        super();
    }
    
}

