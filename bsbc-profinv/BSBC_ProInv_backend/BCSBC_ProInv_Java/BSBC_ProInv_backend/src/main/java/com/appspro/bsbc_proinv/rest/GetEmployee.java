/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.rest;

import com.appspro.bsbc_proinv.dao.EmployeeDetails;
import com.appspro.bean.EmployeeBean;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Anas Alghawi
 */
@Path("/getemployee")
public class GetEmployee {
    
    @GET
    @Path("/{username}")
    @Produces({"application/json"})
    public EmployeeBean getDataFromLookup(@PathParam("username") String userName) {

        
                EmployeeBean obj = new EmployeeBean();
                EmployeeDetails det = new EmployeeDetails();
                    if(userName != null &&
                       !userName.isEmpty() ) {
                        obj = det.getEmpDetails(userName,null,null);
                    }
                    
                    return obj;
    }
    
}
