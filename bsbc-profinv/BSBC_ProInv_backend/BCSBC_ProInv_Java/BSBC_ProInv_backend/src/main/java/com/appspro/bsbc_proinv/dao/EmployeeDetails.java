/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.dao;

import com.appspro.bean.EmployeeBean;
import com.appspro.login.MainSaasSrevices;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.CertificateException;
import javax.imageio.ImageIO;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class EmployeeDetails extends MainSaasSrevices{
    
    public EmployeeDetails() {
        super();
    }
    
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public EmployeeBean getEmpDetails(String username, String jwt,
                                      String host) {
        String finalresponse = "";
        //        String jwttoken = jwt.trim();
        EmployeeBean emp = new EmployeeBean();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl =
            this.getEmployeeUrl()+"?q=UserName=" +
            username +
            "&expand=assignments,photo,assignments.peopleGroupKeyFlexfield";

        String jsonResponse = "";
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction =
                this.getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                                          "Basic QXNocmFmLmFidS1zYWFkOldlbGNvbWVAMTIz");

            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
           
            emp = getEmpAssignment(null, host, arr, emp);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return emp;
    }


    public EmployeeBean getEmpDetailsByPersonId(String personId, String jwt,
                                                String host) {
        String finalresponse = "";
        //        String jwttoken = jwt.trim();
        EmployeeBean emp = new EmployeeBean();

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl =
             this.getEmployeeUrl()+"?q=PersonId=" +
            personId +"&expand=personExtraInformation,assignments,photo,assignments.peopleGroupKeyFlexfield";
        
        String jsonResponse = "";
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction =
                this.getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                                          "Basic QXNocmFmLmFidS1zYWFkOldlbGNvbWVAMTIz");
            connection.setRequestProperty("REST-Framework-Version", "2");

            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
            emp = getEmpAssignment(null, host, arr, emp);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }

        return emp;
    }

   
    
      public EmployeeBean getEmpAssignment(String jwttoken, String host,
                                         JSONArray arr, EmployeeBean emp) throws MalformedURLException, ProtocolException, IOException {

        try {

            JSONArray assignmets = new JSONArray();
            JSONArray photo = new JSONArray();
            for (int i = 0; i < arr.length(); i++) {
                if (!arr.getJSONObject(i).isNull("DisplayName")) {
                    emp.setDisplayName(arr.getJSONObject(i).get("DisplayName").toString());
                }

                if (!arr.getJSONObject(i).isNull("HireDate")) {
                    emp.setHireDate(arr.getJSONObject(i).get("HireDate").toString());
                }
                if (!arr.getJSONObject(i).isNull("DateOfBirth")) {
                    emp.setDateOfBirth(arr.getJSONObject(i).get("DateOfBirth").toString());
                }

                if (!arr.getJSONObject(i).isNull("CitizenshipLegislationCode")) {
                    emp.setCitizenshipLegislationCode(arr.getJSONObject(i).get("CitizenshipLegislationCode").toString());
                }

                if (!arr.getJSONObject(i).isNull("PersonId")) {
                    emp.setPersonId(arr.getJSONObject(i).get("PersonId").toString());
                }

                if (!arr.getJSONObject(i).isNull("PersonNumber")) {
                    emp.setPersonNumber(arr.getJSONObject(i).get("PersonNumber").toString());
                }

                if (!arr.getJSONObject(i).isNull("Country")) {
                    emp.setCountry(arr.getJSONObject(i).get("Country").toString());
                }

                if (!arr.getJSONObject(i).isNull("City")) {
                    emp.setCity(arr.getJSONObject(i).get("City").toString());
                }

                if (!arr.getJSONObject(i).isNull("Region")) {
                    emp.setRegion(arr.getJSONObject(i).get("Region").toString());
                }

                if (!arr.getJSONObject(i).isNull("NationalId")) {
                    emp.setNationalId(arr.getJSONObject(i).get("NationalId").toString());
                }

               assignmets = arr.getJSONObject(i).getJSONArray("assignments");
                if (arr.getJSONObject(0).has("photo")) {
                 //   photo = arr.getJSONObject(0).getJSONArray("photo");
                }
                if (!arr.getJSONObject(i).isNull("MaritalStatus")) {
                    emp.setMaritalStatus(arr.getJSONObject(i).get("MaritalStatus").toString());
                }
                if (!arr.getJSONObject(i).isNull("WorkEmail")) {
                    emp.setEmail(arr.getJSONObject(i).get("WorkEmail").toString());
                }
                if (!arr.getJSONObject(i).isNull("WorkPhoneNumber")) {
                    emp.setWorkPhone(arr.getJSONObject(i).get("WorkPhoneNumber").toString());
                }
                if (!arr.getJSONObject(i).isNull("WorkPhoneExtension")) {
                    emp.setWorkPhoneExt(arr.getJSONObject(i).get("WorkPhoneExtension").toString());
                }
                if (!arr.getJSONObject(i).isNull("WorkMobilePhoneNumber")) {
                    emp.setMobileNumber(arr.getJSONObject(i).get("WorkMobilePhoneNumber").toString());
                }
                if (!arr.getJSONObject(i).isNull("WorkFaxNumber")) {
                    emp.setFax(arr.getJSONObject(i).get("WorkFaxNumber").toString());
                }
                if (!arr.getJSONObject(i).isNull("WorkFaxExtension")) {
                    emp.setFaxExt(arr.getJSONObject(i).get("WorkFaxExtension").toString());
                }
                if (!arr.getJSONObject(i).isNull("LegalEntityId")) {
                    emp.setLegalEntityId(arr.getJSONObject(i).get("LegalEntityId").toString());
                }
            }

           
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            if (photo.length() > 0) {
                JSONArray data = photo;
                JSONArray links = new JSONArray();

                links = data.getJSONObject(0).getJSONArray("links");


                String serverUrl =
                    links.getJSONObject(0).getString("href") + "/enclosure/Image";
                URL url =
                    new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
                if (url.getProtocol().toLowerCase().equals("https")) {
                    trustAllHosts();
                    https = (HttpsURLConnection)url.openConnection();
                    https.setHostnameVerifier(DO_NOT_VERIFY);
                    connection = https;
                } else {
                    connection = (HttpURLConnection)url.openConnection();
                }
                String SOAPAction =
                    this.getInstanceUrl();
                //connection = (HttpsURLConnection)url.openConnection();
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestProperty("SOAPAction", SOAPAction);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type",
                                              "application/json; Charset=UTF-8");
                //  connection.addRequestProperty("Authorization","Bearer " + jwttoken);
                connection.addRequestProperty("Authorization",
                                              "Basic QXNocmFmLmFidS1zYWFkOldlbGNvbWVAMTIz");
                BufferedImage bi = ImageIO.read(connection.getInputStream());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(bi, "jpg", baos);
                baos.flush();
                byte[] imageInByte = baos.toByteArray();
                emp.setPicBase64(javax.xml.bind.DatatypeConverter.printBase64Binary(imageInByte));
                baos.close();
            }
      

            return emp;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return emp;

    }
    public static void main(String[] args) {
        EmployeeDetails d = new EmployeeDetails();
        System.out.println(d.getEmpDetails("IO", null, null)); 
    }
    

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.1");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}