/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc_proinv.rest;

/**
 *
 * @author Anas Alghawi
 */
import biPReports.BIPReports;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Path("/report")
public class BIReportService {

    @POST
    @Path("/commonbireport")
    @Consumes(MediaType.APPLICATION_JSON)
    public String getBiReport(InputStream incomingData, @Context HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        HttpSession session = request.getSession();
        System.out.println("Inside service");
        try {
            BufferedReader in
                    = new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
        } catch (Exception e) {
            System.out.println("Error Parsing: - ");
        }
        JSONObject jObject; // json
        String fresponse = null;
        try {
            jObject = new JSONObject(crunchifyBuilder.toString());
            String reportName = jObject.getString("reportName");
            BIPReports biPReports = new BIPReports();
            biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
            biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

            if (reportName != null) {

                if (reportName.equals(BIPReports.REPORT_NAME.GET_PROFORMA_INVOICE_SHIPMENT.getValue())) {
                    System.out.println("Inside Report");
                    String paramName = null;
                    System.out.println("get_Query_For_PROFORMA_INVOICE_SHIPMENT");
                    for (int i = 0;
                            i < BIPReports.GET_PROFORMA_INVOICE_SHIPMENT_PARAM.values().length;
                            i++) {
                        paramName
                                = BIPReports.GET_PROFORMA_INVOICE_SHIPMENT_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                jObject.getString(paramName));
                    }

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.GET_PROFORMA_INVOICE_SHIPMENT.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj
                            = XML.toJSONObject(fresponse.toString());
                    System.out.println( fresponse.toString());
                    System.out.println(xmlJSONObj);
                    
                   System.out.println("DATA"+xmlJSONObj.getJSONObject("DATA_DS"));
                   System.out.println("lengtttttttttttth"+xmlJSONObj.getJSONObject("DATA_DS").length());
                    String jsonString;
                   if(xmlJSONObj.getJSONObject("DATA_DS").length() >=5){
                       jsonString  = xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                    "value").replaceAll("LABEL",
                                            "label");
                   }
                   else{
                          jsonString  = xmlJSONObj.getJSONObject("DATA_DS").toString();
                           }
                   
                          
                    
                    return jsonString;

                }
                else if (reportName.equals(BIPReports.REPORT_NAME.GET_PROFORMA_INVOICE_NUMBER_REPORT.getValue())) {
                                    System.out.println("Inside Report");
                                    String paramName = null;
                                    System.out.println("get_Query_For_PROFORMA_INVOICE_SHIPMENT");
                                    for (int i = 0;
                                            i < BIPReports.GET_PROFORMA_INVOICE_NUMBER_REPORT_PARAM.values().length;
                                            i++) {
                                        paramName
                                                = BIPReports.GET_PROFORMA_INVOICE_NUMBER_REPORT_PARAM.values()[i].getValue();
                                        biPReports.getParamMap().put(paramName,
                                                jObject.getString(paramName));
                                    }

                                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.GET_PROFORMA_INVOICE_NUMBER_REPORT.getValue());
                                    fresponse = biPReports.executeReports();
                                    JSONObject xmlJSONObj
                                            = XML.toJSONObject(fresponse.toString());
                                    System.out.println( fresponse.toString());
                                    System.out.println(xmlJSONObj);
                                    
                                   System.out.println("DATA"+xmlJSONObj.getJSONObject("DATA_DS"));
                                   System.out.println("lengtttttttttttth"+xmlJSONObj.getJSONObject("DATA_DS").length());
                                    String jsonString;
                                   if(xmlJSONObj.getJSONObject("DATA_DS").length() >=5){
                                       jsonString  = xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                    "value").replaceAll("LABEL",
                                                            "label");
                                   }
                                   else{
                                          jsonString  = xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                    "value").replaceAll("LABEL",
                                                            "label");
                                           }
                                   
                                          
                                    
                                    return jsonString;

                                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // return HTTP response 200 in case of success
        return fresponse;
    }
    
    
    @POST
    @Path("/pdfreport")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getBiPdfReport(InputStream incomingData, @Context HttpServletRequest request) {
        StringBuilder crunchifyBuilder = new StringBuilder();
        HttpSession session = request.getSession();
        System.out.println("Inside service");
        
        try {
            BufferedReader in
                    = new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
        } catch (Exception e) {
            System.out.println("Error Parsing: - ");
        }
        System.out.println(crunchifyBuilder.toString());
        JSONObject jObject; // json
        String fresponse = null;
        try {
            jObject = new JSONObject(crunchifyBuilder.toString());
            String reportName = jObject.getString("reportName");
            BIPReports biPReports = new BIPReports();
            biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
            biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

            if (reportName != null) {
                           
                if (reportName.equals(BIPReports.REPORT_NAME.GET_PROFORMA_INVOICE_PDF_REPORT.getValue())) {
    System.out.println("test to pdf file ");
                    String paramName = null;

                    for (int i = 0;
                            i < BIPReports.GET_PROFORMA_INVOICE_PDF_REPORT_PARAM.values().length;
                            i++) {
                        paramName
                                = BIPReports.GET_PROFORMA_INVOICE_PDF_REPORT_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                jObject.getString(paramName));
                    }

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.GET_PROFORMA_INVOICE_PDF_REPORT.getValue());
                    biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                    biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                    byte[] reportBytes = biPReports.executeReportsPDF();

                    fresponse = javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                    System.out.println(reportBytes);
                    System.out.println("this value return is " + fresponse);
                    return Response.ok("{" + "\"REPORT\":" + "\"" + fresponse + "\"" + "}", MediaType.APPLICATION_JSON).build();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // return HTTP response 200 in case of success
        return Response.ok("", MediaType.APPLICATION_JSON).build();
    }

    public static void doSomething(Node node) {
        // do something with the current node instead of System.out
        System.out.println(node.getNodeName() + "++++++++   "
                + node.getTextContent());
        String xx = node.getNodeName();
        if (node.getNodeName().equals("G_1")) {
            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                Node currentNode = node.getChildNodes().item(i);

            }
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                doSomething(currentNode);
            }
        }
    }
    

}
