/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['config/serviceconfig'], function (serviceConfig) {

    /**
     * The view model for managing all services
     */
    function services() {

        var self = this;
        var servicesHost = "";
        var paasServiceHost = "";
        var appServiceHost = "";
        var biReportServletPath = "";

//        var restPath = "http://localhost:8080/BSBC_PROINV/webapi/";
//        var restPath= "http://localhost:7101/BSBC-ProInv-context-root/webapi/";
        var restPath= "https://bsbcjcssx-a527636.java.us2.oraclecloudapps.com/BSBC-ProInv-context-root/webapi/";
//        var restPath= "https://bsbcjcssx-a527636.java.us2.oraclecloudapps.com/BSBC-ProInv-context-root-test/webapi/";



        self.authenticate = function (payload) {
            var serviceURL = restPath + "login";
            var headers = {
                "Authorization": "Basic bmlkYWxAc3VsdGFuLmNvbS5rdzpCc2JjT3JhQDE5"
            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.getGeneric = function (serviceName, header) {
            var serviceURL = restPath + serviceName;
            console.log(serviceURL);
            var headers = {
                "Authorization": "Basic bmlkYWxAc3VsdGFuLmNvbS5rdzpCc2JjT3JhQDE5"
            };

            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.editGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            console.log(serviceURL);
            console.log("payload == " + payload);
            var headers = {
                "Authorization": "Basic bmlkYWxAc3VsdGFuLmNvbS5rdzpCc2JjT3JhQDE5"
            };
//             headers.Authorization = "Basic bmlkYWxAc3VsdGFuLmNvbS5rdwo6QnNiY"
            return serviceConfig.callPutService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.addGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
//             headers.Authorization = "Basic bmlkYWxAc3VsdGFuLmNvbS5rdwo6QnNiY"
            var headers = {
                "Authorization": "Basic bmlkYWxAc3VsdGFuLmNvbS5rdzpCc2JjT3JhQDE5"
            };
            console.log("Inside Services");
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.addGenericSaas = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
//             headers.Authorization = "Basic bmlkYWxAc3VsdGFuLmNvbS5rdwo6QnNiY"
            var headers = {
                "Authorization": "Basic bmlkYWxAc3VsdGFuLmNvbS5rdzpCc2JjT3JhQDE5"
            };
            console.log("Inside Services");
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };



        self.deleteGeneric = function (serviceName, headers) {
            var serviceURL = restPath + serviceName;
            console.log(serviceURL);
            var headers = {
               "Authorization": "Basic bmlkYWxAc3VsdGFuLmNvbS5rdzpCc2JjT3JhQDE5"
            };
//           headers.Authorization = "Basic bmlkYWxAc3VsdGFuLmNvbS5rdwo6QnNiY"
            return serviceConfig.callDeleteService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };



    }
    ;

    return new services();
});