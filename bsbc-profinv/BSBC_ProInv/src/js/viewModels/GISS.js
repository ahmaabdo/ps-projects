/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'notify', 'ojs/ojknockout',
    'promise', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojarraydataprovider', 'ojs/ojbutton',
    'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojdatetimepicker',
    'ojs/ojselectcombobox', 'ojs/ojtimezonedata', 'ojs/ojformlayout', 'ojs/ojvalidationgroup',
    'ojs/ojarraytabledatasource', 'ojs/ojcheckboxset', 'ojs/ojfilepicker', 'ojs/ojselectcombobox',
    'ojs/ojcollapsible', 'ojs/ojdialog'],
        function (oj, ko, $, services) {

            function searchProformaInvoiceViewModel() {
                var self = this;

                self.localesearch = ko.observable();

                self.headerRowNumber = ko.observable();
                self.Warrantydata = ko.observableArray([]);
                self.columnArray = ko.observableArray([]);
                self.searchTableArray = ko.observableArray([]);
                self.searchTableColumnArray = ko.observableArray([]);
                self.filteredArray = ko.observableArray([]);
                self.resData = ko.observableArray([]);
                //SaaS Data
                self.saaSShipmentsData = ko.observableArray([]);
                //PaaS XXX_PROFORMA_INVOICE Data
                self.paaSShipmentsData = ko.observableArray([]);
                //ProForma Invoice  Search Filelds Labels
                self.locaclSalesOrderNumber = ko.observable();
                self.localCutomerPO = ko.observable();
                self.localTender = ko.observable();
                //ProForma Invoice  Create Button Label
                self.locaclCreate = ko.observable();
                //ProForma Invoice  Search Filelds Inputes
                self.salesOrderNumber = ko.observable();
                self.cutomerPO = ko.observable();
                self.tender = ko.observable();
                //Search Table Headers Text
                self.shipmentNumber = ko.observable();
                self.shipmentDate = ko.observable();
                self.shipmentTotalAmount = ko.observable();
                self.curancey = ko.observable();
                self.referenceNumber = ko.observable();
                //Popup Table inout Text
                self.pINumberLabel = ko.observable();
                self.pITotalAmountLabel = ko.observable();
                self.CuranceyLabel = ko.observable();
                self.pIDateLabel = ko.observable();
                self.pIStatusLabel = ko.observable();
                //Popup Table Headers Text
                self.pINumber = ko.observable();
                self.pITotalAmount = ko.observable();
                self.Curancey = ko.observable();
                self.pIDate = ko.observable();
                self.pIStatus = ko.observable();
                //Popup Table button Text
                self.localCreatePI = ko.observable();
                self.localCancelPI = ko.observable();
                //Vlidation search inputes
                self.searchValidation = ko.observable(false);
                //upload row shipment number
                self.rowShipmentNumber = ko.observable();
                //files array
                self.filesArray = ko.observableArray([]);
                //PI Attachment
                self.pIAttachment = ko.observable();
                self.poNumber = ko.observable();
                self.saleOrder = ko.observable();
                self.tender = ko.observable();
                self.reference = ko.observable();
                //
                self.pIInfo = ko.observable();
                // Sequance PI
                self.PISeqNumber = ko.observable();
                //Selected Index Array
                self.selectedData = ko.observableArray([]);

                self.createdShipment = ko.observable();
                self.pdfReponseData = ko.observable();
                self.printDisable = ko.observable(false);
                
                self.postToArArray = ko.observableArray([]);


                //popupTableColumnArray Array
                self.popupTableColumnArray = ko.observableArray();

                self.createdPIArray = ko.observableArray([]);
                self.createdPIColumnArray = ko.observableArray();
                //Search Table DataSource
                self.datasource = new oj.ArrayTableDataSource(self.saaSShipmentsData, {idAttribute: 'shipmentNumber'});
                self.createdPIDatasource = new oj.ArrayTableDataSource(self.createdPIArray, {idAttribute: 'shipmentNumber'});

                self.dataprovider1 = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.saaSShipmentsData, {idAttribute: 'shipmentNumber'}));
                self.dataprovider2 = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.createdPIArray, {idAttribute: 'shipmentNumber'}));
                //Search Table Cloumn Array 
//                self.searchTableColumnArray([{"renderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_tmpl", true),
//                        "headerRenderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_hdr_tmpl", true)},
//                    {"headerText": self.shipmentNumber(),
//                        "field": "shipmentNumber"},
//                    {"headerText": self.shipmentDate(),
//                        "field": "shipmentDate"},
//                    {"headerText": self.shipmentTotalAmount(),
//                        "field": "shipmentTotalAmount"},
//                    {"headerText": self.curancey(),
//                        "field": "curancey"},
//                    {"headerText": self.referenceNumber(),
//                        "field": "referenceNumber"}
////                    {"headerText": "upload",
////                        "renderer": oj.KnockoutTemplateUtils.getRenderer("upload", true)}
//                ]);
                //popup Table DataSource
                self.popupDatasource = new oj.ArrayTableDataSource(self.selectedData, {idAttribute: 'startIndex'});
                //Search Table Cloumn Array 
                self.popupTableColumnArray([
                    {"headerText": self.shipmentNumber(),
                        "field": "startIndex"},
                    {"headerText": self.shipmentNumber(),
                        "field": "shipmentNumber"},
                    {"headerText": self.shipmentDate(),
                        "field": "shipmentDate"},
                    {"headerText": self.shipmentTotalAmount(),
                        "field": "shipmentTotalAmount"},
                    {"headerText": self.curancey(),
                        "field": "curancey"},
                    {"headerText": self.referenceNumber(),
                        "field": "referenceNumber"}

                ]);
                //status LOV
                self.piStatusLOV = [
                    {value: 'IE', label: 'Internet Explorer'},
                    {value: 'FF', label: 'Firefox'},
                    {value: 'CH', label: 'Chrome'},
                    {value: 'OP', label: 'Opera'},
                    {value: 'SA', label: 'Safari'}
                ];



//                window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
//                    alert("Error occured: " + errorMsg);//or any message
//                    return false;
//                };


                self.selectionListener = function (event)
                {
                
                    if (event.target.currentRow) {

                        self.rowShipmentNumber(event.target.currentRow.rowKey);
                    }
                    var data = event.detail;
                    if (data != null)
                    {
                        var selectionObj = data.value;
                        if (selectionObj && selectionObj.length > 0)
                        {
                            for (j = 0; j < selectionObj.length; j++)
                            {
                                var range = selectionObj[j];
                                var startIndex = range.startIndex;
                                var endIndex = range.endIndex;

                                if (startIndex != null &&
                                        startIndex.column != null &&
                                        endIndex.column == startIndex.column &&
                                        startIndex.column == 0)
                                {
                                    if ($('#table_checkboxset_hdr')[0].value != null &&
                                            $('#table_checkboxset_hdr')[0].value.length > 0 &&
                                            $('#table_checkboxset_hdr')[0].value[0] == 'checked')
                                    {
                                        $('#table_checkboxset_hdr')[0].value = [];
                                        return;
                                    } else
                                    {
                                        $('#table_checkboxset_hdr')[0].value = ['checked'];
                                        return;
                                    }
                                }
                            }
                        }
                        var totalSize = self.datasource.totalSize();
                        var i, j;
                        for (i = 0; i < totalSize; i++)
                        {
                            self.datasource.at(i).then(function (row)
                            {
                                var foundInSelection = false;
                                if (selectionObj)
                                {
                                    for (j = 0; j < selectionObj.length; j++)
                                    {
                                        var range = selectionObj[j];
                                        var startIndex = range.startIndex;
                                        var endIndex = range.endIndex;

                                        if (startIndex != null && startIndex.row != null)
                                        {
                                            if (row.index >= startIndex.row && row.index <= endIndex.row)
                                            {
                                                row.data.Selected(['checked']);
                                                foundInSelection = true;
                                            }
                                        }
                                    }
                                    if (!foundInSelection)
                                    {
                                        row.data.Selected([]);
                                    }
                                }
                            });
                        }
                    }
                    self.currentSelection();
                };
                self.selectAllListener = function (event)
                {
                    if (self._clearCheckboxHdr)
                    {
                        return;
                    }
                    var data = event.detail;
                    if (data != null)
                    {
                        var table = document.getElementById('table');
                        if (data['value'].length > 0)
                        {
                            var totalSize = self.datasource.totalSize();
                            table.selection = [{startIndex: {"row": 0}, endIndex: {"row": totalSize - 1}}];
                        } else
                        {
                            table.selection = [];
                        }
                    }
                };
                self.currentSelection = function ()
                {
                    var element = document.getElementById('table');
                    var selectionObj = element.selection;
                    var selectionTxt = "";
                    self.selectedData([]);
                    var len = selectionObj ? selectionObj.length : 0;

                    var i = 0;
                    for (i = 0; i < len; i++)
                    {
                        var range = selectionObj[i];
                        var startIndex = range.startIndex;
                        var endIndex = range.endIndex;
                        var startKey = range.startKey;
                        var endKey = range.endKey;

                        if (startIndex != null && startIndex.row != null)
                        {
                            var selectedIndex = startIndex.row;
                            var stringShipmentNumber = self.saaSShipmentsData()[selectedIndex].shipmentNumber.toString();
                            var stringShipmentTotalAmount = self.saaSShipmentsData()[selectedIndex].shipmentTotalAmount.toString();
                            var stringSaleOrder = self.saaSShipmentsData()[selectedIndex].saleOrder.toString();
                            var StringARInvoiceNumber = self.saaSShipmentsData()[selectedIndex].arInvoiceNumber.toString();
                            var stringARInvStatus = self.saaSShipmentsData()[selectedIndex].arInvStatus.toString();
                            self.selectedData().push({
//                                "startIndex": selectedIndex,
                                "PISeqNumber": "",
                                "shipmentNumber": stringShipmentNumber,
                                "shipmentDate": self.saaSShipmentsData()[selectedIndex].shipmentDate,
                                "curancey": self.saaSShipmentsData()[selectedIndex].curancey,
                                "shipmentTotalAmount": stringShipmentTotalAmount,
                                "referenceNumber": self.saaSShipmentsData()[selectedIndex].referenceNumber,
                                "attachment": "",
                                "poNumber": self.saaSShipmentsData()[selectedIndex].poNumber,
                                "saleOrder": stringSaleOrder,
                                "headerReference": self.saaSShipmentsData()[selectedIndex].headerReference,
                                "lineReference": self.saaSShipmentsData()[selectedIndex].lineReference,
                                "tender": self.saaSShipmentsData()[selectedIndex].tender,
                                "amount": self.saaSShipmentsData()[selectedIndex].amount,
                                "arInvoiceNumber": StringARInvoiceNumber,
                                "arInvStatus": stringARInvStatus,
                                "arInvCusTRXNumber": self.saaSShipmentsData()[selectedIndex].arInvCusTRXNumber





                            });

                        }
                        if (startKey != null && startKey.row != null)
                        {
                            selectionTxt = selectionTxt + "start row key: " + startKey.row + ", end row key: " + endKey.row + "\n";
                        }

                        if (startIndex != null && startIndex.column != null)
                        {
                            //column selection
                            selectionTxt = selectionTxt + "Column Selection\n";
                            selectionTxt = selectionTxt + "start column index: " + startIndex.column + ", end column index: " + endIndex.column + "\n";
                        }
                        if (startKey != null && startKey.column != null)
                        {
                            selectionTxt = selectionTxt + "start column key: " + startKey.column + ", end column key: " + endKey.column + "\n";
                        }
                    }
                    $('#selectionCurrent').val(selectionTxt);
//                   
                };
                self.syncCheckboxes = function (event)
                {
                    event.stopPropagation();
                    if (event.currentTarget.id != 'table_checkboxset_hdr')
                    {
                        self._clearCheckboxHdr = true;
                        $('#table_checkboxset_hdr')[0].value = [];
                        self._clearCheckboxHdr = false;
                    }
                    setTimeout(function ()
                    {
                        // sync the checkboxes with selection obj
                        var selectionObj = [];
                        var totalSize = self.datasource.totalSize();
                        var i;
                        for (i = 0; i < totalSize; i++)
                        {
                            self.datasource.at(i).then(function (row) {
                                if (row.data.Selected().length > 0 &&
                                        row.data.Selected()[0] == 'checked')
                                {
                                    selectionObj.push({startIndex: {row: row.index}, endIndex: {row: row.index}});
                                }

                                if (row.index == totalSize - 1)
                                {
                                    table.selection = selectionObj;
                                }
                            });
                        }
                    }, 0);
                };


                //Search input validation on click
                self.inputsValidation = ko.computed(function () {
                    if (self.salesOrderNumber() || self.tender() || self.cutomerPO()) {
                        return true;
                    } else {
                        return false;
                    }
                }, self);
//                self.searchValidation(self.inputsValidation())





                self.find = function () {

                    //                    $("#table").ojTable("refresh");
                    document.getElementById('table').refresh();
                    self.saaSShipmentsData([]);
                    self.createdPIArray([]);
                    if (self.inputsValidation())
                    {


                        if (!self.salesOrderNumber()) {
                            self.salesOrderNumber("");
                        }
                        if (!self.tender()) {
                            self.tender("");
                        }
                        if (!self.cutomerPO()) {
                            self.cutomerPO("");
                        }
                        var paraPayload = {
                            "reportName": "GET_PROFORMA_INVOICE_SHIPMENT",
                            "BU_NAME":"GISS",
                            "SALES_ORDER": self.salesOrderNumber(),
                            "CUSTOMER_PO": self.cutomerPO(),
                            "TENDER": self.tender(),
                            "AR_INVOICE_NUMBER": ""

                        };
                        self.getSaasShipments(paraPayload);



                    } else {
                        $.notify(
                                "Please Insert Data", "error");
                    }
                };




                self.CreatePIButton = function () {
                    $('#loader-overlay').show();
                    function show() {
                        if (self.selectedData().length > 0) {

                            var amount = 0;
                            for (i = 0; i < self.selectedData().length; i++) {
                                amount = amount + Number(self.selectedData()[i].amount);
                                self.selectedData()[i].PISeqNumber = self.PISeqNumber();

                            }

                            self.pINumber(self.PISeqNumber());
                            self.pITotalAmount(amount.toFixed(3).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                            self.Curancey(self.selectedData()[0].curancey);
                            self.pIDate(self.selectedData()[0].shipmentDate);
                            self.poNumber(self.selectedData()[0].poNumber);
                            self.saleOrder(self.selectedData()[0].saleOrder);
                            self.tender(self.selectedData()[0].tender);
                            self.reference(self.selectedData()[0].referenceNumber);



                            document.querySelector('#popupUndorelease').open();


                            self.pIInfo({
                                "pINumber": self.pINumber(),
                                "pITotalAmount": amount.toString(),
                                "Curancey": self.Curancey(),
                                "poNumber": self.poNumber(),
                                "saleOrder": self.saleOrder().toString(),
                                "tender": self.tender(),
                                "referenceNumber": self.reference(),
                                "attachment": ""
                            });


                            $('#loader-overlay').hide();

                        } else {
                            $.notify(
                                    "Please Select Shipment Number", "error");
                            $('#loader-overlay').hide();
                        }

                    }

                    self.getPICurrentNumber('math', show);


                };

self.assigData = ko.observableArray([]);
                self.createPI = function () {

                    var mapFiles = function () {
                        if (self.filesArray().length > 0) {
                            for (i = 0; i < self.selectedData().length; i++) {
                                for (j = 0; j < self.selectedData().length; j++) {
                                    if (self.selectedData()[i].shipmentNumber == self.filesArray()[j].rowKey) {
                                        self.selectedData()[i].attachment = self.filesArray()[j].fileData;
                                    }
                                }
                            }
                        }

                    };
                    self.assigData(self.selectedData());
                    $.when(mapFiles()).then(self.postPaasShipments());


                };

                self.cancelPI = function () {
                    alert("Calnceld");
                };

                self.acceptStr = ko.observableArray(["*"]);
                self.uploadFile = function (event) {


                    var file = event.detail.files[0];
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        self.filesArray.push({
                            "rowKey": self.rowShipmentNumber(),
                            "fileData": reader.result
                        });
                    };
                    reader.onerror = function (error) {
                        console.log('Error: ', error);
                    };

                };
                self.uploadPIFile = function (event) {

                    var file = event.detail.files[0];
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        self.pIAttachment(reader.result);

                        self.pIInfo({
                            "pINumber": self.pINumber(),
                            "pITotalAmount": self.pITotalAmount(),
                            "Curancey": self.Curancey(),
                            "poNumber": self.poNumber(),
                            "saleOrder": self.saleOrder().toString(),
                            "tender": self.tender(),
                            "referenceNumber": self.reference(),
                            "attachment": self.pIAttachment()
                        });
                    
                    };
                    reader.onerror = function (error) {
                        console.log('Error: ', error);
                    };






                };



                var formatDate = function (date) {
                    var monthNames = [
                        "January", "February", "March",
                        "April", "May", "June", "July",
                        "August", "September", "October",
                        "November", "December"
                    ];

                    var d = new Date(date);
                    var day = d.getDate();
                    var monthIndex = d.getMonth() + 1;
                    var year = d.getFullYear();

//                    return day + '- ' + monthNames[monthIndex] + '-' + year;
                    return day + '-' + monthIndex + '-' + year;
                };

                self.difference = function (resData, searchTableArray) {

                   if (resData.constructor !== Array) {
                        resData = [resData];
                    }

                    var indexsesArray = [];
                    for (i = 0; i < resData.length; i++) {
                
                        for (j = 0; j < searchTableArray.length; j++) {
                
                            if (searchTableArray[j].shipmentNumber == resData[i].DELIVERY_ID
                                    && searchTableArray[j].saleOrder == resData[i].SALES_ORDER_NUMBER) {
                                self.createdPIArray.push(searchTableArray[j]);

                                indexsesArray.push(i);
                break;

                            }

                        }
                    }

                    for (var i = indexsesArray.length - 1; i >= 0; i--) {
                        resData.splice(indexsesArray[i], 1);
                    }

                    return resData;


                }

                var bUName;
                self.getSaasShipments = function (searchParam) {
                    $('#loader-overlay').show();
                    self.saaSShipmentsData([]);
                    var getSaasShipmentsCbFn = function (data) {
                        self.saaSShipmentsData([]);
                        self.createdPIArray([]);

                        if (data.toString().includes("version"))
                        {
                            $('#loader-overlay').hide();
                            $.notify(
                                    "There is no data", "error");
                        }
                        resData = JSON.parse(data);
                        console.log();

                        var filteredArray = [];
                        var amount;
                        var fromatAmount;
                        var invStatus;
                        var refNumberHeader = "";
                        var refNumberLine = "";

                        if (resData.hasOwnProperty("PO_HREF")) {
                            refNumberHeader = resData.PO_HREF;
                        }
                        if (resData.hasOwnProperty("PO_LREF")) {
                            refNumberLine = resData.PO_LREF;
                        }
                        if (resData.hasOwnProperty("BUNAME")) {
                            bUName = resData.BUNAME;
                        }
                        if (resData.hasOwnProperty('CUSTOMER_PO')) {
                            $.notify("No Data Found", {globalPosition: "top center", className: "error"});
                            $('#loader-overlay').hide();
                        }
                        if (resData.constructor !== Array) {
                            resData = [resData];
                        }
                        if (self.searchTableArray().length > 0) {
                            filteredArray = self.difference(resData, self.searchTableArray());

                            for (j = 0; j < filteredArray.length; j++) {
                                if (filteredArray[j].hasOwnProperty("PO_HREF")) {
                                    refNumberHeader = filteredArray[j].PO_HREF;
                                }
                                if (filteredArray[j].hasOwnProperty("PO_LREF")) {
                                    refNumberLine = filteredArray[j].PO_LREF;
                                }

                                amount = filteredArray[j].PICKED_QUANTITY * filteredArray[j].UNIT_PRICE;
                                fromatAmount = amount.toFixed(3).replace(/\d(?=(\d{3})+\.)/g, '$&,');

                                if (filteredArray[j].INV_STATUS === "Y") {
                                    invStatus = "Complete";
                                } else if (filteredArray[j].INV_STATUS === "N") {
                                    invStatus = invStatus = "Incomplete";
                                }

//                                        if (self.searchTableArray()[i].shipmentNumber !== resData[j].DELIVERY_ID &&  self.searchTableArray()[i].saleOrder == resData[j].SALES_ORDER_NUMBER) {
                                self.saaSShipmentsData.push({
                                    Selected: ko.observable([]),
                                    shipmentNumber: filteredArray[j].DELIVERY_ID,
                                    shipmentDate: formatDate(filteredArray[j].DATE_SCHEDULED),
                                    shipmentTotalAmount: fromatAmount,
                                    curancey: filteredArray[j].CURRENCY_CODE,
                                    referenceNumber: refNumberHeader,
                                    attachment: "",
                                    poNumber: filteredArray[j].CUST_PO_NUMBER,
                                    saleOrder: filteredArray[j].SALES_ORDER_NUMBER,
                                    tender: filteredArray.ATTRIBUTE_CHAR1,
                                    headerReference: refNumberHeader,
                                    lineReference: refNumberLine,
                                    amount: amount,
                                    cusTRXID: filteredArray[j].CUS_TRX_ID,
                                    arInvoiceNumber: filteredArray[j].AR_INV_NUMBER,
                                    arInvStatus: invStatus,
                                    arInvCusTRXNumber: filteredArray[j].CUS_TRX_ID,
                                    dvShipmentsDate: filteredArray[j].dvShipmentsDate,
                                    piShipmentsDate: filteredArray[j].piShipmentsDate
                                });

                            }
                            ;

                            if (self.saaSShipmentsData().length < 1) {
                                $.notify("No Shipments Found", {globalPosition: "top center", className: "error"});
                            }
                            ;

                        } else {

                            $.each(resData, function (j) {
                                if (resData[j].hasOwnProperty("PO_HREF")) {
                                    refNumberHeader = resData[j].PO_HREF;
                                }
                                if (resData[j].hasOwnProperty("PO_LREF")) {
                                    refNumberLine = resData[j].PO_LREF;
                                }
                                amount = resData[j].PICKED_QUANTITY * resData[j].UNIT_PRICE;
                                fromatAmount = amount.toFixed(3).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                if (resData[j].INV_STATUS === "Y") {
                                    invStatus = "Complete";
                                } else if (resData[j].INV_STATUS === "N") {
                                    invStatus = invStatus = "Incomplete";
                                }

                                self.saaSShipmentsData.push({
                                    Selected: ko.observable([]),
                                    shipmentNumber: resData[j].DELIVERY_ID,
                                    shipmentDate: formatDate(resData[j].DATE_SCHEDULED),
                                    shipmentTotalAmount: fromatAmount,
                                    curancey: resData[j].CURRENCY_CODE,
                                    referenceNumber: refNumberHeader,
                                    attachment: "",
                                    poNumber: resData[j].CUST_PO_NUMBER,
                                    saleOrder: resData[j].SALES_ORDER_NUMBER,
                                    tender: resData.ATTRIBUTE_CHAR1,
                                    headerReference: refNumberHeader,
                                    lineReference: refNumberLine,
                                    amount: amount,
                                    cusTRXID: resData[j].CUS_TRX_ID,
                                    arInvoiceNumber: resData[j].AR_INV_NUMBER,
                                    arInvStatus: invStatus,
                                    arInvCusTRXNumber: resData[j].CUS_TRX_ID,
                                    dvShipmentsDate: resData[j].dvShipmentsDate,
                                    piShipmentsDate: resData[j].piShipmentsDate
                                });

                            });

                            if (self.saaSShipmentsData().length < 1) {
                                $.notify("No Shipments Found", {globalPosition: "top center", className: "error"});
                            }


                        }

                        self.getPaasShipments();

                        $('#loader-overlay').hide();
                    };


                    var getSaasShipmentsfailCbFn = function () {
                        console.log("Failed");
                    };


                    var UrlPath = "report/commonbireport";
                    services.addGeneric(UrlPath, searchParam).then(getSaasShipmentsCbFn, getSaasShipmentsfailCbFn);
                };


                self.getPaasShipments = function () {
                    var getPaasShipmentsCbFn = function (data) {
                        self.searchTableArray([]);
                        $.each(data.items, function (i) {
                            self.searchTableArray.push({
                                Selected: ko.observable([]),
                                id: data.items[i].id,
                                shipmentNumber: data.items[i].shipment_number,
                                shipmentDate: data.items[i].char_shipment_date,
                                shipmentTotalAmount: data.items[i].shipment_total_amount,
                                curancey: data.items[i].currency,
                                referenceNumber: data.items[i].reference_number,
                                attachment: data.items[i].attachment,
                                poNumber: data.items[i].customer_po,
                                saleOrder: data.items[i].sales_order,
                                tender: data.items[i].tender,
                                headerReference: data.items[i].header_ref,
                                lineReference: data.items[i].line_ref,
                                piNumber: data.items[i].pi_number,
                                dvNumber: data.items[i].acknowledged_pi_dv,
                                arInvNumber: data.items[i].ar_number,
                                arInvStatus: data.items[i].ar_status,
                                piShipmentsDate: data.items[i].char_pi_date,
                                dvShipmentsDate: data.items[i].char_dv_date


                            });
                        });

                    };
                    var getPaasShipmentsfailCbFn = function () {
                        console.log("Failed");
                    };
                    var UrlPath = "ProformaInvoiceRest";
                    services.getGeneric(UrlPath).then(getPaasShipmentsCbFn, getPaasShipmentsfailCbFn);
//                    console.clear();
                };

                function isEmpty(val) {
                    return (val === undefined || val == null || val.length <= 0) ? true : false;
                }
                //get Seq from database
                self.getPINumber = function () {
                    var getPINumberCbFn = function (data) {

                    };
                    var getPINumberfailCbFn = function () {
                        console.log("Failed");
                    };
                    if(isEmpty(bUName)){
                        bUName = "anyBUN";
                    }
                     console.log(bUName)
                    var UrlPath = "PINumber/PINextNumber/GISS";
                    services.getGeneric(UrlPath).then(getPINumberCbFn, getPINumberfailCbFn);
                };
                self.getPICurrentNumber = function (param, callback) {
                     var getPICurrentNumberCbFn = function (data) {
                        self.PISeqNumber(data.items[0].pinumber);
                        console.log(data);
                        self.getPINumber();
                        callback();
                    };
                    var getPICurrentNumberfailCbFn = function () {
                        console.log("Failed");
                    };
                     if(isEmpty(bUName)){
                        bUName = "anyBUN";
                    }
                     console.log(bUName)

                    var UrlPath = "PINumber/PICurrNumber/GISS";
                    services.getGeneric(UrlPath).then(getPICurrentNumberCbFn, getPICurrentNumberfailCbFn);
                };




                self.postShipments = function () {

                };
                
                self.postSasShipments = function (pINumber) {

                    var postSaasShipmentsCbFn = function (data) {
                        self.closePopup();
                        self.getPINumber();
                        self.resetAfterCall();
                        $('#loader-overlay').hide();
                        $.notify("PI Created", {globalPosition: "top center", className: "success"});
////                
                    }
                    var postSaasShipmentsfailCbFn = function (pINumber) {
                        self.getPINumber();
                        self.resetAfterCall();
                        $('#loader-overlay').hide();
                        $.notify("Error: Failed To Post Data To SaaS Service", "error");

                    }

                    var UrlPath = "AcknowledgeRest/saasarinv/";

                    $.each(self.assigData(), function (j) {


                                self.postToArArray({
                                    "piNumber": pINumber
                                });
                            
                            services.addGenericSaas(UrlPath + self.assigData()[j].arInvCusTRXNumber, self.postToArArray()).then(postSaasShipmentsCbFn, postSaasShipmentsfailCbFn);

                    });
                

                };


                self.postPaasShipments = function () {
                    $('#loader-overlay').show();
                    var j=1;
                    var jj=1;
                    var counter = 0;
                    var postPaasShipmentsCbFnp = function (data) {
                        self.postSasShipments(self.pIInfo().pINumber);
                    };
                    var postPaasShipmentsCbFns = function (data) {
                        jj++;
                        self.getPINumber();
                        self.resetAfterCall();
                        $('#loader-overlay').hide();
                        var popup = document.querySelector('#popupUndorelease');
                    popup.close();
                    };
                    
                    var postPaasShipmentsfailCbFn = function () {
                        self.getPINumber();
                        self.resetAfterCall();
                        $('#loader-overlay').hide();
                    };
                    var postPaasShipmentsfailCbFnp = function () {
                        self.getPINumber();
                        self.resetAfterCall();
                        $('#loader-overlay').hide();
                    };
                    var UrlPath1 = "ProformaInvoiceRest";
                    var UrlPath2 = "AcknowledgeRest";

                    services.addGeneric(UrlPath2, self.pIInfo()).then(postPaasShipmentsCbFnp, postPaasShipmentsfailCbFnp);

                    for (i = 0; i < self.assigData().length; i++) {
                        services.addGeneric(UrlPath1, JSON.stringify(self.assigData()[i])).then(postPaasShipmentsCbFns, postPaasShipmentsfailCbFn);
                        counter++;
                    }

                };

                self.closePopup = function () {
                    var popup = document.querySelector('#popupUndorelease');
                    popup.close();
                };



                self.createdShipmentsTableListner = function (event) {
                    self.createdShipment();
                    self.printDisable(true)
                    if (self.createdPIArray().length > 0) {
                        var index = event.target.currentRow.rowIndex

                        if (self.createdPIArray()[index].dvNumber) {
                            self.createdShipment({
                                "reportName": "Pro-Forma Invoice Template Report",
                                "P_SALES_ORDER_NUMBER": self.createdPIArray()[index].saleOrder,
                                "PL_NUMBER": self.createdPIArray()[index].piNumber
                            });
                        } else {
                            self.createdShipment({
                                "reportName": "Pro-Forma Invoice Template Report",
                                "P_SALES_ORDER_NUMBER": self.createdPIArray()[index].saleOrder,
                                "PL_NUMBER": ""

                            });
                        }


                    }

                };

                self.callPDFService = function () {


                    var getPDFReportCbFn = function (data) {
                        $('#loader-overlay').hide();

                        var pdfBase64Data = data.REPORT;
                        self.pdfReponseData('data:application/pdf;base64,' + data.REPORT);

                        var  pdfWindow = window.open("pdfreport");
                        pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + encodeURI(pdfBase64Data) + "'></iframe>");



                    }
                    var getPDFReportfailCbFn = function (data) {
                        $('#loader-overlay').hide();
                        $.notify(
                                "Error While Excuting report", "error");
                    }

                    if (self.printDisable()) {
                        $('#loader-overlay').show();
                        var UrlPath = "report/pdfreport";
                        services.addGeneric(UrlPath, self.createdShipment()).then(getPDFReportCbFn, getPDFReportfailCbFn);

                    } else {
                        $('#loader-overlay').hide();
                        $.notify(
                                "Please Select PI Shipment", "error");
                    }

                };



                self.resetAfterCall = function () {
                    self.salesOrderNumber("");
//                    self.saaSShipmentsData("");
                    self.cutomerPO("");
                    self.tender("");
                    self.saaSShipmentsData([]);
                    self.searchTableArray([]);
                    self.createdPIArray([]);
                    self.printDisable(false)
                    self.getPaasShipments();

                };

                self.reloadlanguage = function ()
                {

                    var newlang = localStorage.getItem('setLang');
                    if (!newlang)
                    {
                        localStorage.setItem('lang', newlang);
                    }

                    switch (newlang) {
                        case 'arabic':
                            newLang = 'ar-sa';
                            break;
                        default:
                            newLang = 'en-US';
                    }
                    oj.Config.setLocale(newLang,
                            function () {

                                $('html').attr('lang', newLang);
                                if (newLang === 'ar-sa') {
                                    $('html').attr('dir', 'rtl');

                                    //ProForma Invoice  Search Filelds Labels Translation
                                    self.locaclSalesOrderNumber(oj.Translations.getTranslatedString('Sales Order Number'));
                                    self.localCutomerPO(oj.Translations.getTranslatedString('Cutomer PO'));
                                    self.localTender(oj.Translations.getTranslatedString('Tender'));
                                    //Table Headers Text
                                    self.shipmentNumber(oj.Translations.getTranslatedString('Shipment Number'));
                                    self.shipmentDate(oj.Translations.getTranslatedString('Shipment Date'));
                                    self.shipmentTotalAmount(oj.Translations.getTranslatedString('Shipment Total Amount'));
                                    self.curancey(oj.Translations.getTranslatedString('Curancey'));
                                    self.referenceNumber(oj.Translations.getTranslatedString('Reference Number'));
                                    //Create button label
                                    self.locaclCreate(oj.Translations.getTranslatedString('Create'));
                                    //popup Table Headers
                                    self.pINumberLabel(oj.Translations.getTranslatedString('PI Number'));
                                    self.pITotalAmountLabel(oj.Translations.getTranslatedString('PI Total amount'));
                                    self.CuranceyLabel(oj.Translations.getTranslatedString('Curancey'));
                                    self.pIDateLabel(oj.Translations.getTranslatedString('PI Date '));
                                    self.pIStatusLabel(oj.Translations.getTranslatedString('PI Status '));
                                    //PI Button
                                    self.localCreatePI(oj.Translations.getTranslatedString('Create PI'));
                                    self.localCancelPI(oj.Translations.getTranslatedString('Cancel PI'));


                                    //Search Table Cloumn Array 
                                    self.searchTableColumnArray([{"renderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_tmpl", true),
                                            "headerRenderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_hdr_tmpl", true)},
                                        {"headerText": self.shipmentNumber(),
                                            "field": "shipmentNumber"},
                                        {"headerText": self.shipmentDate(),
                                            "field": "shipmentDate"},
                                        {"headerText": self.shipmentTotalAmount(),
                                            "field": "shipmentTotalAmount"},
                                        {"headerText": self.curancey(),
                                            "field": "curancey"},
                                        {"headerText": self.referenceNumber(),
                                            "field": "referenceNumber"},
                                        {"headerText": "AR Invoice Number",
                                            "field": ""},
                                        {"headerText": "AR Invoice Status",
                                            "field": ""},
                                        {"headerText": "PI Number",
                                            "field": ""},
                                        {"headerText": "Acknowledge DV Number",
                                            "field": ""},
                                        {"headerText": "Acknowledge DV Number",
                                            "field": ""}
//                    {"headerText": "upload",
//                        "renderer": oj.KnockoutTemplateUtils.getRenderer("upload", true)}
                                    ]);

                                } else {
                                    $('html').attr('dir', 'ltr');


                                }
                                //ProForma Invoice  Search Filelds Labels Translation
                                self.locaclSalesOrderNumber(oj.Translations.getTranslatedString('Sales Order Number'));
                                self.localCutomerPO(oj.Translations.getTranslatedString('Cutomer PO'));
                                self.localTender(oj.Translations.getTranslatedString('Tender'));
                                //Table Headers Text
                                self.shipmentNumber(oj.Translations.getTranslatedString('Shipment Number'));
                                self.shipmentDate(oj.Translations.getTranslatedString('Shipment Date'));
                                self.shipmentTotalAmount(oj.Translations.getTranslatedString('Shipment Total Amount'));
                                self.curancey(oj.Translations.getTranslatedString('Curancey'));
                                self.referenceNumber(oj.Translations.getTranslatedString('Reference Number'));
                                //Create button label
                                self.locaclCreate(oj.Translations.getTranslatedString('Create'));
                                //popup Table Headers
                                self.pINumberLabel(oj.Translations.getTranslatedString('PI Number'));
                                self.pITotalAmountLabel(oj.Translations.getTranslatedString('PI Total amount'));
                                self.CuranceyLabel(oj.Translations.getTranslatedString('Curancey'));
                                self.pIDateLabel(oj.Translations.getTranslatedString('PI Date '));
                                self.pIStatusLabel(oj.Translations.getTranslatedString('PI Status '));

                                //Search Table Cloumn Array 
                                self.searchTableColumnArray([
                                    {"renderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_tmpl", true),
                                        "headerRenderer": oj.KnockoutTemplateUtils.getRenderer("checkbox_hdr_tmpl", true)},
                                    {"headerText": self.shipmentNumber(),
                                        "field": "shipmentNumber"},
                                    {"headerText": self.shipmentDate(),
                                        "field": "shipmentDate"},
                                    {"headerText": self.shipmentTotalAmount(),
                                        "field": "shipmentTotalAmount"},
                                    {"headerText": self.curancey(),
                                        "field": "curancey"},
                                    {"headerText": "AR Invoice Number",
                                        "field": "arInvoiceNumber"},
                                    {"headerText": "AR Invoice Status",
                                        "field": "invStatus"},
                                    {"headerText": self.referenceNumber(),
                                        "field": "referenceNumber"}


                                ]);

                                self.createdPIColumnArray([
                                    {"headerText": self.shipmentNumber(),
                                        "field": "shipmentNumber"},
                                    {"headerText": self.shipmentDate(),
                                        "field": "shipmentDate"},
                                    {"headerText": self.shipmentTotalAmount(),
                                        "field": "shipmentTotalAmount"},
                                    {"headerText": self.curancey(),
                                        "field": "curancey"},
                                    {"headerText": "AR Invoice Number",
                                        "field": "arInvNumber"},
                                    {"headerText": "AR Invoice Status",
                                        "field": "arInvStatus"},
                                    {"headerText": "PI Number",
                                        "field": "piNumber"},
                                    {"headerText": "PI Date",
                                        "field": "piShipmentsDate"},
                                    {"headerText": "Acknowledge DV Number",
                                        "field": "dvNumber"},
                                    {"headerText": "DV Date",
                                        "field": "dvShipmentsDate"}
                                ]);


                            });
                };


                self.connected = function () {
                    // Implement if needed
                    self.getPaasShipments();

                };

                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                self.disconnected = function () {
                    // Implement if needed
//                    self.btnRest();
                    self.resetAfterCall();

                };

                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                self.transitionCompleted = function () {
                    // Implement if needed
                    var table = document.getElementById('table');
                    table.addEventListener('selectionChanged', self.selectionListener);
                    $('#table').on('click', '.oj-checkboxset', self.syncCheckboxes);
                };
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new searchProformaInvoiceViewModel();
        }
);
