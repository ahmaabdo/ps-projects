/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'notify', 'ojs/ojknockout',
    'promise', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojarraydataprovider', 'ojs/ojbutton',
    'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojdatetimepicker',
    'ojs/ojselectcombobox', 'ojs/ojtimezonedata', 'ojs/ojformlayout', 'ojs/ojvalidationgroup',
    'ojs/ojarraytabledatasource', 'ojs/ojcheckboxset', 'ojs/ojfilepicker', 'ojs/ojselectcombobox',
    'ojs/ojcollapsible', 'ojs/ojdialog'],
        function (oj, ko, $, services) {

            function searchProformaInvoiceViewModel() {
                var self = this;

                self.localesearch = ko.observable();

                self.headerRowNumber = ko.observable();
                self.Warrantydata = ko.observableArray([]);
                self.columnArray = ko.observableArray([]);
                self.searchTableArray = ko.observableArray([]);
                self.searchTableColumnArray = ko.observableArray([]);
                self.filteredArray = ko.observableArray([]);
                self.resData = ko.observableArray([]);
                //SaaS Data
                self.saaSShipmentsData = ko.observableArray([]);
                //PaaS XXX_PROFORMA_INVOICE Data
                self.paaSShipmentsData = ko.observableArray([]);
                //ProForma Invoice  Search Filelds Labels
                self.locaclSalesOrderNumber = ko.observable();
                self.localCutomerPO = ko.observable();
                self.localTender = ko.observable();
                //ProForma Invoice  Create Button Label
                self.locaclCreate = ko.observable();
                //ProForma Invoice  Search Filelds Inputes
                self.salesOrderNumber = ko.observable();
                self.cutomerPO = ko.observable();
                self.tender = ko.observable();
                //Search Table Headers Text
                self.shipmentNumber = ko.observable();
                self.shipmentDate = ko.observable();
                self.shipmentTotalAmount = ko.observable();
                self.curancey = ko.observable();
                self.referenceNumber = ko.observable();
                //Popup Table inout Text
                self.pINumberLabel = ko.observable();
                self.pITotalAmountLabel = ko.observable();
                self.CuranceyLabel = ko.observable();
                self.pIDateLabel = ko.observable();
                self.pIStatusLabel = ko.observable();
                //Popup Table Headers Text
                self.pINumber = ko.observable();
                self.pITotalAmount = ko.observable();
                self.Curancey = ko.observable();
                self.pIDate = ko.observable();
                self.pIStatus = ko.observable();
                //Popup Table button Text
                self.localCreatePI = ko.observable();
                self.localCancelPI = ko.observable();
                //Vlidation search inputes
                self.searchValidation = ko.observable(false);
                //upload row shipment number
                self.rowShipmentNumber = ko.observable();
                //files array
                self.filesArray = ko.observableArray([]);
                //PI Attachment
                self.pIAttachment = ko.observable();
                self.poNumber = ko.observable();
                self.saleOrder = ko.observable();
                self.tender = ko.observable();
                self.reference = ko.observable();
                //
                self.pIInfo = ko.observable();
                // Sequance PI
                self.PISeqNumber = ko.observable();
                //Selected Index Array
                self.selectedData = ko.observableArray([]);

                self.createdShipment = ko.observable();
                self.pdfReponseData = ko.observable();
                self.printDisable = ko.observable(false);
                
                self.postToArArray = ko.observableArray([]);
                
                self.arInvoiceNumber = ko.observable();


                //popupTableColumnArray Array
                self.popupTableColumnArray = ko.observableArray();

                self.createdPIArray = ko.observableArray([]);
                self.createdPIColumnArray = ko.observableArray();
                //Search Table DataSource
                self.datasource = new oj.ArrayTableDataSource(self.saaSShipmentsData, {idAttribute: 'shipmentNumber'});
                self.createdPIDatasource = new oj.ArrayTableDataSource(self.saaSShipmentsData, {idAttribute: 'shipmentNumber'});

                self.dataprovider1 = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.saaSShipmentsData, {idAttribute: 'shipmentNumber'}));
                self.dataprovider2 = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.saaSShipmentsData, {idAttribute: 'shipmentNumber'}));

//                ]);

                   self.searchTableColumnArray([
                                    {"headerText": self.shipmentNumber(),
                                        "field": "shipmentNumber"},
                                    {"headerText": self.shipmentDate(),
                                        "field": "shipmentDate"},
                                    {"headerText": self.shipmentTotalAmount(),
                                        "field": "shipmentTotalAmount"},
                                    {"headerText": self.curancey(),
                                        "field": "curancey"},
                                    {"headerText": "AR Invoice Number",
                                        "field": "arInvoiceNumber"},
                                    {"headerText": "AR Invoice Status",
                                        "field": "arInvStatus"},
                                    {"headerText": "PI Number",
                                        "field": "piNumber"},
//                                    {"headerText": "PI Date",
//                                        "field": "piShipmentsDate"},
                                    {"headerText": "Acknowledge DV Number",
                                        "field": "dvNumber"},
//                                    {"headerText": "DV Date",
//                                        "field": "dvShipmentsDate"}
                                ]);
        


                $(document).ready(function () {
                    $("#findd").on("click", function () {
                        $(this).attr("disabled", "disabled");
                        doWork(); //this method contains your logic
                    });
                });

                function doWork() {
                  //  alert("doing work");
                    //actually this function will do something and when processing is done the button is enabled by removing the 'disabled' attribute
                    //I use setTimeout so you can see the button can only be clicked once, and can't be clicked again while work is being done
                    setTimeout('$("#findd").removeAttr("disabled")', 6000);
                }


                self.find = function () {

                    //                    $("#table").ojTable("refresh");


                    self.saaSShipmentsData([]);
                    self.createdPIArray([]);


                    if (self.arInvoiceNumber() && self.arInvoiceNumber() !=" ")
                    {


                        var paraPayload = {
                            "reportName": "GET_PROFORMA_INVOICE_NUMBER_REPORT",
                            "AR_INVOICE_NUMBER": self.arInvoiceNumber()

                        };
                        self.getSaasShipments(paraPayload);



                    } else {
                        $.notify(
                                "Please Insert Data", "error");
                    }


                };






                var formatDate = function (date) {
                    var monthNames = [
                        "January", "February", "March",
                        "April", "May", "June", "July",
                        "August", "September", "October",
                        "November", "December"
                    ];

                    var d = new Date(date);
                    var day = d.getDate();
                    var monthIndex = d.getMonth() + 1;
                    var year = d.getFullYear();

//                    return day + '- ' + monthNames[monthIndex] + '-' + year;
                    return day + '-' + monthIndex + '-' + year;
                };



                self.getSaasShipments = function (searchParam) {
                    $('#loader-overlay').show();
                    self.saaSShipmentsData([]);
                    var getSaasShipmentsCbFn = function (data) {                      
                        if (data.toString().includes("version"))
                        {                           
                            $('#loader-overlay').hide();
                            $.notify(
                                "There is no data", "error");
                        }
                        if(data) {
                            
                        
                            self.saaSShipmentsData([]);



                             resData = JSON.parse(data);

                            var filteredArray = [];
                            var amount;
                            var fromatAmount;
                            var invStatus;
                            var refNumberHeader = "";
                            var refNumberLine = "";

                            if (resData.hasOwnProperty("PO_HREF")) {
                                refNumberHeader = resData.PO_HREF;
                            }
                            if (resData.hasOwnProperty("PO_LREF")) {
                                refNumberLine = resData.PO_LREF;
                            }

                            if (resData.hasOwnProperty('CUSTOMER_PO')) {
                                $.notify("No Data Found", {globalPosition: "top center", className: "error"});
                                $('#loader-overlay').hide();
                            }else{
                                         if (resData.constructor !== Array) {
                                resData = [resData];
                            }
    //                            var filteredData = [];


                                $.each(resData, function (j) {
                                    if (resData[j].hasOwnProperty("PO_HREF")) {
                                        refNumberHeader = resData[j].PO_HREF;
                                    }
                                    if (resData[j].hasOwnProperty("PO_LREF")) {
                                        refNumberLine = resData[j].PO_LREF;
                                    }
                                    amount = resData[j].PICKED_QUANTITY * resData[j].UNIT_PRICE;
                                    fromatAmount = amount.toFixed(3).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                    if (resData[j].INV_STATUS === "Y") {
                                        invStatus = "Complete";
                                    } else if (resData[j].INV_STATUS === "N") {
                                        invStatus = invStatus = "Incomplete";
                                    }

                                    self.saaSShipmentsData.push({
                                        Selected: ko.observable([]),
                                        shipmentNumber: resData[j].DELIVERY_ID,
                                        shipmentDate: formatDate(resData[j].DATE_SCHEDULED),
                                        shipmentTotalAmount: fromatAmount,
                                        curancey: resData[j].CURRENCY_CODE,
                                        referenceNumber: refNumberHeader,
                                        attachment: "",
                                        poNumber: resData[j].CUST_PO_NUMBER,
                                        saleOrder: resData[j].SALES_ORDER_NUMBER,
                                        tender: resData.ATTRIBUTE_CHAR1,
                                        headerReference: refNumberHeader,
                                        lineReference: refNumberLine,
                                        amount: amount,
                                        cusTRXID: resData[j].CUS_TRX_ID,
                                        arInvoiceNumber: resData[j].AR_INV_NUMBER,
                                        arInvStatus: invStatus,
                                        arInvCusTRXNumber: resData[j].CUS_TRX_ID,
                                        dvShipmentsDate: resData[j].dvShipmentsDate,
                                        piShipmentsDate: resData[j].piShipmentsDate,
                                        piNumber: resData[j].PI_NUMBER,
                                        dvNumber : resData[j].DV_NUMBER
                                    });

                                });
                                if (self.saaSShipmentsData().length < 1) {
                                    $.notify("No Shipments Found", {globalPosition: "top center", className: "error"});
                                }
                            }
               
                        }
                        $('#loader-overlay').hide();
                    };


                    var getSaasShipmentsfailCbFn = function () {
                        console.log("Failed");
                    };


                    var UrlPath = "report/commonbireport";
                    services.addGeneric(UrlPath, searchParam).then(getSaasShipmentsCbFn, getSaasShipmentsfailCbFn);

                };



                

                self.resetAfterCall = function () {

                    self.saaSShipmentsData([]);
                    self.arInvoiceNumber();

     
                };




















                self.reloadlanguage = function ()
                {

                    var newlang = localStorage.getItem('setLang');
                    if (!newlang)
                    {
                        localStorage.setItem('lang', newlang);
                    }



                    switch (newlang) {
                        case 'arabic':
                            newLang = 'ar-sa';
                            break;
                        default:
                            newLang = 'en-US';
                    }
                    oj.Config.setLocale(newLang,
                            function () {

                                $('html').attr('lang', newLang);
                                if (newLang === 'ar-sa') {
                                    $('html').attr('dir', 'rtl');

                                    //ProForma Invoice  Search Filelds Labels Translation
                                    self.locaclSalesOrderNumber(oj.Translations.getTranslatedString('Sales Order Number'));
                                    self.localCutomerPO(oj.Translations.getTranslatedString('Cutomer PO'));
                                    self.localTender(oj.Translations.getTranslatedString('Tender'));
                                    //Table Headers Text
                                    self.shipmentNumber(oj.Translations.getTranslatedString('Shipment Number'));
                                    self.shipmentDate(oj.Translations.getTranslatedString('Shipment Date'));
                                    self.shipmentTotalAmount(oj.Translations.getTranslatedString('Shipment Total Amount'));
                                    self.curancey(oj.Translations.getTranslatedString('Curancey'));
                                    self.referenceNumber(oj.Translations.getTranslatedString('Reference Number'));
                                    //Create button label
                                    self.locaclCreate(oj.Translations.getTranslatedString('Create'));
                                    //popup Table Headers
                                    self.pINumberLabel(oj.Translations.getTranslatedString('PI Number'));
                                    self.pITotalAmountLabel(oj.Translations.getTranslatedString('PI Total amount'));
                                    self.CuranceyLabel(oj.Translations.getTranslatedString('Curancey'));
                                    self.pIDateLabel(oj.Translations.getTranslatedString('PI Date '));
                                    self.pIStatusLabel(oj.Translations.getTranslatedString('PI Status '));
                                    //PI Button
                                    self.localCreatePI(oj.Translations.getTranslatedString('Create PI'));
                                    self.localCancelPI(oj.Translations.getTranslatedString('Cancel PI'));


                                    //Search Table Cloumn Array 
                                   
                                       self.searchTableColumnArray([
                                    {"headerText": self.shipmentNumber(),
                                        "field": "shipmentNumber"},
                                    {"headerText": self.shipmentDate(),
                                        "field": "shipmentDate"},
                                    {"headerText": self.shipmentTotalAmount(),
                                        "field": "shipmentTotalAmount"},
                                    {"headerText": self.curancey(),
                                        "field": "curancey"},
                                    {"headerText": "AR Invoice Number",
                                        "field": "arInvoiceNumber"},
                                    {"headerText": "AR Invoice Status",
                                        "field": "arInvStatus"},
                                    {"headerText": "PI Number",
                                        "field": "piNumber"},
//                                    {"headerText": "PI Date",
//                                        "field": "piShipmentsDate"},
                                    {"headerText": "Acknowledge DV Number",
                                        "field": "dvNumber"},
//                                    {"headerText": "DV Date",
//                                        "field": "dvShipmentsDate"}
                                ]);

                                } else {
                                    $('html').attr('dir', 'ltr');


                                }
                                //ProForma Invoice  Search Filelds Labels Translation
                                self.locaclSalesOrderNumber(oj.Translations.getTranslatedString('Sales Order Number'));
                                self.localCutomerPO(oj.Translations.getTranslatedString('Cutomer PO'));
                                self.localTender(oj.Translations.getTranslatedString('Tender'));
                                //Table Headers Text
                                self.shipmentNumber(oj.Translations.getTranslatedString('Shipment Number'));
                                self.shipmentDate(oj.Translations.getTranslatedString('Shipment Date'));
                                self.shipmentTotalAmount(oj.Translations.getTranslatedString('Shipment Total Amount'));
                                self.curancey(oj.Translations.getTranslatedString('Curancey'));
                                self.referenceNumber(oj.Translations.getTranslatedString('Reference Number'));
                                //Create button label
                                self.locaclCreate(oj.Translations.getTranslatedString('Create'));
                                //popup Table Headers
                                self.pINumberLabel(oj.Translations.getTranslatedString('PI Number'));
                                self.pITotalAmountLabel(oj.Translations.getTranslatedString('PI Total amount'));
                                self.CuranceyLabel(oj.Translations.getTranslatedString('Curancey'));
                                self.pIDateLabel(oj.Translations.getTranslatedString('PI Date '));
                                self.pIStatusLabel(oj.Translations.getTranslatedString('PI Status '));

                                //Search Table Cloumn Array 
                   

                                                 self.searchTableColumnArray([
                                    {"headerText": "Sales Order",
                                        "field": "saleOrder"},
                                    {"headerText": self.shipmentNumber(),
                                        "field": "shipmentNumber"},
                                    {"headerText": self.shipmentDate(),
                                        "field": "shipmentDate"},
                                    {"headerText": self.shipmentTotalAmount(),
                                        "field": "shipmentTotalAmount"},
                                    {"headerText": self.curancey(),
                                        "field": "curancey"},
                                    {"headerText": "AR Invoice Number",
                                        "field": "arInvoiceNumber"},
                                    {"headerText": "AR Invoice Status",
                                        "field": "arInvStatus"},
                                    {"headerText": "PI Number",
                                        "field": "piNumber"},
//                                    {"headerText": "PI Date",
//                                        "field": "piShipmentsDate"},
                                    {"headerText": "Acknowledge DV Number",
                                        "field": "dvNumber"},
//                                    {"headerText": "DV Date",
//                                        "field": "dvShipmentsDate"}
                                ]);


                            });
                };


                self.connected = function () {
                    // Implement if needed
//                     self.find();

                };

                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                self.disconnected = function () {
                    // Implement if needed
//                    self.btnRest();
                    self.resetAfterCall();

                };

                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                self.transitionCompleted = function () {
                    // Implement if needed
                
                };
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new searchProformaInvoiceViewModel();
        }
);
