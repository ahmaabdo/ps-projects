/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'notify', 'ojs/ojknockout', 'promise', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojarraydataprovider', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojarraytabledatasource', 'ojs/ojdatetimepicker',
    'ojs/ojselectcombobox', 'ojs/ojtimezonedata', 'ojs/ojformlayout', 'ojs/ojvalidationgroup', 'ojs/ojarraytabledatasource', 'ojs/ojcheckboxset', 'ojs/ojfilepicker', 'ojs/ojselectcombobox'],
        function (oj, ko, $, services) {

            function proFormaInvAcknViewModel() {
                var self = this;
                self.ackTableArray = ko.observableArray([]);
                self.ackTableColumnArray = ko.observableArray([]);
                //ProForma Invoice  Search Filelds Labels
                self.locaclSalesOrderNumber = ko.observable();
                self.localCutomerPO = ko.observable();
                self.localTender = ko.observable();
                self.pINumberInput = ko.observable();
                self.saleOrderInput = ko.observable();
                self.poNumberInput = ko.observable();
                self.tenderInput = ko.observable();
                //ProForma Invoice  Create Button Label
                self.locaclCreate = ko.observable();
                self.locaclAcknowledge = ko.observable();
                self.localesearch = ko.observable();
                self.localeCancel = ko.observable();
                //ProForma Invoice  Search Filelds Inputes
                self.salesOrderNumber = ko.observable();
                self.cutomerPO = ko.observable();
                self.tender = ko.observable();
                //Search Table Headers Text
                self.shipmentNumber = ko.observable();
                self.shipmentDate = ko.observable();
                self.shipmentTotalAmount = ko.observable();
                self.curancey = ko.observable();
                self.referenceNumber = ko.observable();
                //Popup Table Headers Text
                self.pINumberLabel = ko.observable();
                self.pITotalAmountLabel = ko.observable();
                self.CuranceyLabel = ko.observable();
                self.pIDateLabel = ko.observable();
                self.pIStatusLabel = ko.observable();
                //Popup Table Headers Text
                self.pINumber = ko.observable();
                self.pITotalAmount = ko.observable();
                self.Curancey = ko.observable();
                self.pIDate = ko.observable();
                self.pIStatus = ko.observable();
                self.dvNote = ko.observable();
                //Put Data
                self.putData = ko.observableArray();
                self.saasPayloadData = ko.observableArray();
                self.pIAttachment = ko.observable();
                //Selected Index Array
                self.selectedData = ko.observableArray([]);
                self.CusTRXArray = ko.observableArray([]);
                self.postToArArray = ko.observableArray([]);
                self.successFlag = ko.observable(true);




                //popupTableColumnArray Array
                self.popupTableColumnArray = ko.observableArray();
                //Search Table Data Array
//                self.ackTableArray([{pINumber: 1001, pIDate: 'ADFPM 1001 neverending', pITotalAmount: 200, pIcurancey: 300, referenceNumber: 300},
//                    {pINumber: 556, pIDate: 'BB', pITotalAmount: 200, pIcurancey: 300, referenceNumber: 300},
//                    { pINumber: 10, pIDate: 'Administration', pITotalAmount: 200, pIcurancey: 300, referenceNumber: 300},
//                    { pINumber: 20, pIDate: 'Marketing', pITotalAmount: 200, pIcurancey: 300, referenceNumber: 300}]);
                //Search Table DataSource
                self.datasource = new oj.ArrayTableDataSource(self.ackTableArray, {idAttribute: 'pINumber'});
//                self.dataprovider = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.ackTableArray, {idAttribute: 'pINumber'}));
                //Search Table Cloumn Array 
                self.ackTableColumnArray([
                    {"headerText": "PI Number",
                        "field": "pINumber"},
                    {"headerText": "PI Date",
                        "field": "pIDate"},
                    {"headerText": "PI Amount Amount",
                        "field": "pITotalAmount"},
                    {"headerText": "Curancey",
                        "field": "pIcurancey"},
                    {"headerText": "Reference Number",
                        "field": "referenceNumber"}
                ]);


                //popup Table DataSource
                self.popupDatasource = new oj.ArrayTableDataSource(self.selectedData, {idAttribute: 'startIndex'});
                //Search Table Cloumn Array 
                self.popupTableColumnArray([
                    {"headerText": self.shipmentNumber(),
                        "field": "startIndex"},
                    {"headerText": self.shipmentNumber(),
                        "field": "shipmentNumber"},
                    {"headerText": self.shipmentDate(),
                        "field": "shipmentDate"},
                    {"headerText": self.shipmentTotalAmount(),
                        "field": "shipmentTotalAmount"},
                    {"headerText": self.curancey(),
                        "field": "curancey"},
                    {"headerText": self.referenceNumber(),
                        "field": "referenceNumber"}]);





                self.selectionListener = function (event)
                {
                    self.currentSelection();
                };

                self.currentSelection = function ()
                {
                    var element = document.getElementById('table');
                    var selectionObj = element.selection;
                    var selectionTxt = "";
                    self.selectedData([]);
                    var len = selectionObj ? selectionObj.length : 0;

                    var i = 0;
                    for (i = 0; i < len; i++)
                    {
                        var range = selectionObj[i];
                        var startIndex = range.startIndex;
                        var endIndex = range.endIndex;
                        var startKey = range.startKey;
                        var endKey = range.endKey;

                        if (startIndex != null && startIndex.row != null)
                        {
                            //row selection
//                            selectionTxt = selectionTxt + "Row Selection\n";
//                            selectionTxt = selectionTxt + "start row index: " + startIndex.row + ", end row index: " + endIndex.row + "\n";
                            var selectedIndex = startIndex.row;
                            if (self.ackTableArray().length > 0) {
                                self.selectedData().push({
                                    "startIndex": selectedIndex,
                                    "pINumber": self.ackTableArray()[selectedIndex].pINumber,
                                    "pIDate": self.ackTableArray()[selectedIndex].pIDate,
                                    "pIcurancey": self.ackTableArray()[selectedIndex].pIcurancey,
                                    "pITotalAmount": self.ackTableArray()[selectedIndex].pITotalAmount,
                                    "referenceNumber": self.ackTableArray()[selectedIndex].referenceNumber


                                });
                            }
                            ;

                        }
                        if (startKey != null && startKey.row != null)
                        {
                            selectionTxt = selectionTxt + "start row key: " + startKey.row + ", end row key: " + endKey.row + "\n";
                        }

                        if (startIndex != null && startIndex.column != null)
                        {
                            //column selection
                            selectionTxt = selectionTxt + "Column Selection\n";
                            selectionTxt = selectionTxt + "start column index: " + startIndex.column + ", end column index: " + endIndex.column + "\n";
                        }
                        if (startKey != null && startKey.column != null)
                        {
                            selectionTxt = selectionTxt + "start column key: " + startKey.column + ", end column key: " + endKey.column + "\n";
                        }
                    }
                    $('#selectionCurrent').val(selectionTxt);
//                   
                };

                self.acceptStr = ko.observableArray(["*"]);

                self.uploadPIFile = function (event) {
                    var file = event.detail.files[0];
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        self.pIAttachment(reader.result)

                    };
                    reader.onerror = function (error) {
                        console.log('Error: ', error);
                    };

                }


                self.ackButton = function () {
//                    self.popupDatasource = new oj.ArrayTableDataSource(self.selectedData, {idAttribute: 'startIndex'});
                    if (self.selectedData().length > 0) {
                        document.querySelector('#popupUndorelease').open();
                        self.pINumber(self.selectedData()[0].pINumber);
                    } else {
                        $.notify("Please Select PI", "error")
                    }

//                    document.querySelector('#popupUndorelease').open();
                }
                self.cancelButton = function () {
//                    self.popupDatasource = new oj.ArrayTableDataSource(self.selectedData, {idAttribute: 'startIndex'});
                    if (self.selectedData().length > 0) {
                        self.putData({
                            "process": "3",
                            "piNumber": self.selectedData()[0].pINumber,
                            "attachment": ""
                        })
                        $('#loader-overlay').show();
                        self.pINumber(self.selectedData()[0].pINumber);
                        self.postPaasShipments('cancel');
                    } else {
                        $.notify("Please Select PI", "error")
                    }

//                    document.querySelector('#popupUndorelease').open();
                }
                function isEmpty(val) {
                    return (val === undefined || val == null || val.length <= 0) ? true : false;
                }
                self.confirm = function () {
//                    
                    if (self.dvNote()) {
                        $('#loader-overlay').show();
                        var attached = "";
                        if (self.pIAttachment()) {
                            attached = self.pIAttachment();
                        }
                         var piNum = self.selectedData()[0].pINumber;
                        if (!isEmpty(self.pIAttachment())) {             
                            self.putData({
                                "process": "1",
                                "piNumber": piNum.toString(),
                                "dvNote": self.dvNote(),
                                "attachment": attached
                            });
                            self.saasPayloadData({
                                "shipments": "1",
                                "piNumber": piNum.toString(),
                                "attachment": attached
                            });
                       
                        self.pINumber(self.selectedData()[0].pINumber);
                        self.getCusTRX();
                         } else {
                            $.notify("You Should Attached Something!", "error");
                        }
                    } else {
                        $.notify("Please Insert DV Number", "error");
                    }

                }

                self.getCusTRX = function () {
                    var getCusTRXCbFn = function (data) {
                        $.each(data.items, function (i) {
                            self.CusTRXArray.push(
                                    data.items[i].cus_trx_number
                                    )
                        })
                        self.postSasShipments();
                    }
                    var getCusTRXfailCbFn = function () {
                        $.notify("Error: Failed To Post Data To SaaS", "error")
                    }
                    var UrlPath = "ProformaInvoiceRest/custrx/" + self.putData().piNumber;
                    services.getGeneric(UrlPath).then(getCusTRXCbFn, getCusTRXfailCbFn);

                }


                self.postSasShipments = function () {

                    var postSaasShipmentsCbFn = function (data) {
                        //                        console.log(data);
                        if (data.ReturnStatus != "SUCCES") {
                            self.closePopup();
                            $.notify("Error: This Invoice Was Posted To GL, You Can't Acknowledge", "error");
                        } else {
                            self.successFlag(true);
                        }
//                        self.closePopup();

                    };
                    var postSaasShipmentsfailCbFn = function (data) {
                        self.successFlag(false);
//                        $.notify("Error: Failed To Post Data To SaaS Service Because This invoice Was Posted To General Ledger", "error");

                    };

                    var UrlPath = "AcknowledgeRest/saasarinv/";

                    $.each(self.CusTRXArray(), function (j) {

//                        for (i = 0; i < 2; i++) {
//
//                            if (i == 0) {
//                                self.postToArArray({
//                                    "piNumber": self.pINumber()
////                                    "piNumber": "FIN1901402"
//
//                                });
//                            } else if (i == 1) {
//                                self.postToArArray({
//                                    "aknowledge": self.dvNote()
////                                    "aknowledge": "20.03.2019"
//                                });
//                            }
//                            console.log("Loopi " + i + "=" + JSON.stringify(self.postToArArray()));
//                            services.addGenericSaas(UrlPath + self.CusTRXArray()[j], self.postToArArray()).then(postSaasShipmentsCbFn, postSaasShipmentsfailCbFn);
//                        }


                        self.postToArArray({
//                                    "aknowledge": self.dvNote()
                            "aknowledge": self.dvNote()
                        });

//                            console.log("Loopi " + i + "=" + JSON.stringify(self.postToArArray()));
                        services.addGenericSaas(UrlPath + self.CusTRXArray()[j], self.postToArArray()).then(postSaasShipmentsCbFn, postSaasShipmentsfailCbFn);

                    });
                    if (self.successFlag) {
//                                                self.postPaasShipments('ack');

                    } else {
                        $('#loader-overlay').hide();
                        $.notify("Error: Failed To Post Data To SaaS Service", "error");
                    }

                };

                self.test = function () {
                    self.CusTRXArray([212368]);
                    self.postSasShipments()

                };

                self.postPaasShipments = function (process) {

                    var postPaasShipmentsCbFn = function (data) {
                        self.closePopup();
                        self.resetAll();

                        if (process == 'ack') {
                            $.notify("PI Acknowledged", {globalPosition: "top center", className: "success"});
                        } else {
                            $.notify("PI Canceled", {globalPosition: "top center", className: "success"});
                        }

                        $('#loader-overlay').hide();
                    };

                    var postPaasShipmentsfailCbFn = function () {
                        console.log("Failed");
                        self.closePopup;
                        self.resetAll();
                        $('#loader-overlay').hide();
                        $.notify("Process Failed", {globalPosition: "top center", className: "error"});
                    };

                    var UrlPath = "AcknowledgeRest/ack";

                    services.addGeneric(UrlPath, self.putData()).then(postPaasShipmentsCbFn, postPaasShipmentsfailCbFn);
//                    self.resetAfterCall()
                };

                self.search = function () {
                    if (self.inputsValidation())
                    {
                        $('#loader-overlay').show();
                        self.getPaasAck();

                    } else {
                        $.notify(
                                "Please Insert Data", "error")
                    }

                };
                self.getPaasAck = function () {
                    var getPaasShipmentsCbFn = function (data) {
                        self.ackTableArray([]);
                        var amount;
                        var fromatAmount;
                        if (data.items.length) {
                            $.each(data.items, function (i) {
                                amount = data.items[i].pi_amount
                                fromatAmount = amount.toFixed(3).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                self.ackTableArray.push({

                                    pINumber: data.items[i].pi_number,
                                    pIDate: data.items[i].pi_date,
                                    pITotalAmount: fromatAmount,
                                    pIcurancey: data.items[i].currency,

                                });
                            });

                        } else {
                            $.notify("No PI Found", {globalPosition: "top center", className: "error"})
                        }
                        $('#loader-overlay').hide();
                    };
                    var getPaasShipmentsfailCbFn = function () {
                        $('#loader-overlay').hide();
                        console.log("Failed");
                    };

                    var UrlPath;
                    if (self.pINumberInput()) {
                        UrlPath = "AcknowledgeRest/" + self.pINumberInput();
                    } else if (self.saleOrderInput()) {
                        UrlPath = "AcknowledgeRest/saleorder/" + self.saleOrderInput();
                    } else if (self.poNumberInput()) {
                        UrlPath = "AcknowledgeRest/POnumber/" + self.poNumberInput();
                    } else if (self.tenderInput()) {
                        UrlPath = "AcknowledgeRest/tender/" + self.tenderInput();
                    }


                    services.getGeneric(UrlPath).then(getPaasShipmentsCbFn, getPaasShipmentsfailCbFn);

                };
                self.closePopup = function () {
                    var popup = document.querySelector('#popupUndorelease');
                    popup.close();
                };

                self.resetAll = function () {
                    self.ackTableArray([]);
                    self.salesOrderNumber("");
                    self.salesOrderNumber("");
                    self.cutomerPO("");
                    self.tender("");
                    self.pINumber("");
                    self.pITotalAmount("");
                    self.Curancey("");
                    self.pIDate("");
                    self.pIStatus("");
                    self.dvNote("");
                    self.putData([]);
                    self.pIAttachment = ko.observable("");

                    self.saleOrderInput("");
                    self.poNumberInput("");
                    self.saleOrderInput("");
                    self.tenderInput("");
                };

                //Search input validation on click
                self.inputsValidation = ko.computed(function () {
                    if (self.saleOrderInput() || self.poNumberInput() || self.pINumberInput() || self.tenderInput()) {
                        return true;
                    } else {
                        return false
                    }
                }, self);























                self.reloadlanguage = function ()
                {

                    var newlang = localStorage.getItem('setLang');
                    if (!newlang)
                    {
                        localStorage.setItem('lang', newlang);
                    }



                    switch (newlang) {
                        case 'arabic':
                            newLang = 'ar-sa';
                            break;
                        default:
                            newLang = 'en-US';
                    }
                    oj.Config.setLocale(newLang,
                            function () {

                                $('html').attr('lang', newLang);
                                if (newLang === 'ar-sa') {
                                    $('html').attr('dir', 'rtl');

                                    //ProForma Invoice  Search Filelds Labels Translation
                                    self.locaclSalesOrderNumber(oj.Translations.getTranslatedString('Sales Order Number'));
                                    self.localCutomerPO(oj.Translations.getTranslatedString('Cutomer PO'));
                                    self.localTender(oj.Translations.getTranslatedString('Tender'));
                                    //Table Headers Text
                                    self.shipmentNumber(oj.Translations.getTranslatedString('Shipment Number'));
                                    self.shipmentDate(oj.Translations.getTranslatedString('Shipment Date'));
                                    self.shipmentTotalAmount(oj.Translations.getTranslatedString('Shipment Total Amount'));
                                    self.curancey(oj.Translations.getTranslatedString('Curancey'));
                                    self.referenceNumber(oj.Translations.getTranslatedString('Reference Number'));
                                    //Create button label
                                    self.locaclCreate(oj.Translations.getTranslatedString('Create'));
                                    self.locaclAcknowledge(oj.Translations.getTranslatedString('Acknowledge'));
                                    self.localesearch(oj.Translations.getTranslatedString('Search'));
                                    self.localeCancel(oj.Translations.getTranslatedString('Cancel'));
                                    //popup Table Headers
                                    self.pINumberLabel(oj.Translations.getTranslatedString('PI Number'));
                                    self.pITotalAmountLabel(oj.Translations.getTranslatedString('PI Total amount'));
                                    self.CuranceyLabel(oj.Translations.getTranslatedString('Curancey'));
                                    self.pIDateLabel(oj.Translations.getTranslatedString('PI Date '));
                                    self.pIStatusLabel(oj.Translations.getTranslatedString('PI Status '));

                                    //Search Table Column Array
                                    //Search Table Cloumn Array 
                                    self.ackTableColumnArray([
                                        {"headerText": "PI Number",
                                            "field": "pINumber"},
                                        {"headerText": "PI Date",
                                            "field": "pIDate"},
                                        {"headerText": "PI Amount Amount",
                                            "field": "pITotalAmount"},
                                        {"headerText": "Curancey",
                                            "field": "pIcurancey"},
                                        {"headerText": "Reference Number",
                                            "field": "referenceNumber"}
                                    ]);

                                } else {
                                    $('html').attr('dir', 'ltr');


                                }
                                //ProForma Invoice  Search Filelds Labels Translation
                                self.locaclSalesOrderNumber(oj.Translations.getTranslatedString('Sales Order Number'));
                                self.localCutomerPO(oj.Translations.getTranslatedString('Cutomer PO'));
                                self.localTender(oj.Translations.getTranslatedString('Tender'));
                                //Table Headers Text
                                self.shipmentNumber(oj.Translations.getTranslatedString('Shipment Number'));
                                self.shipmentDate(oj.Translations.getTranslatedString('Shipment Date'));
                                self.shipmentTotalAmount(oj.Translations.getTranslatedString('Shipment Total Amount'));
                                self.curancey(oj.Translations.getTranslatedString('Curancey'));
                                self.referenceNumber(oj.Translations.getTranslatedString('Reference Number'));
                                //Create button label
                                self.locaclCreate(oj.Translations.getTranslatedString('Create'));
                                self.locaclAcknowledge(oj.Translations.getTranslatedString('Acknowledge'));
                                self.localesearch(oj.Translations.getTranslatedString('Search'));
                                self.localeCancel(oj.Translations.getTranslatedString('Cancel'));
                                //popup Table Headers
                                self.pINumberLabel(oj.Translations.getTranslatedString('PI Number'));
                                self.pITotalAmountLabel(oj.Translations.getTranslatedString('PI Total amount'));
                                self.CuranceyLabel(oj.Translations.getTranslatedString('Curancey'));
                                self.pIDateLabel(oj.Translations.getTranslatedString('PI Date '));
                                self.pIStatusLabel(oj.Translations.getTranslatedString('PI Status '));

                                //Search Table Column Array
                                //Search Table Cloumn Array 
                                self.ackTableColumnArray([
                                    {"headerText": "PI Number",
                                        "field": "pINumber"},
                                    {"headerText": "PI Date",
                                        "field": "pIDate"},
                                    {"headerText": "PI Amount Amount",
                                        "field": "pITotalAmount"},
                                    {"headerText": "Curancey",
                                        "field": "pIcurancey"},
                                    {"headerText": "Reference Number",
                                        "field": "referenceNumber"}
                                ]);
                            });
                };





                self.connected = function () {
                    // Implement if needed
//                    $(document).ready(function(){
//                      
//                    }
                    //)
//                    self.getPaasAck();


                };

                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                self.disconnected = function () {
                    // Implement if needed
                    self.resetAll();


                };

                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                self.transitionCompleted = function () {
                    // Implement if needed
                    var table = document.getElementById('table');
                    table.addEventListener('selectionChanged', self.selectionListener);

                };
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new proFormaInvAcknViewModel();
        }
);
