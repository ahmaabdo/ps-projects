
define([], function () {
 function Screen() {
 var self = this;
self.buildScreen =  function buildScreen (EIT,formName ,model){
    var varName ;
    var req;
    for (var i =0;i<EIT.length;i++){
            var div = document.createElement("div");
           div.setAttribute('class', 'oj-flex-item');
           var parent = document.getElementById("xx");
           
            parent.appendChild(div); 
            
            if(EIT[i].REQUIRED_FLAG =="Y"){
                req= true;
            }else{
                req =false;
            }
             
            
        //----------------End Div -------------------
       // drawLabel(EIT[i].END_USER_COLUMN_NAME);
       varName='model.'+EIT[i].APPLICATION_COLUMN_NAME;
        if(EIT[i].FLEX_value_SET_NAME=="100 Character No Validation"){                       
            drawInputText (varName,EIT[i].END_USER_COLUMN_NAME ,req);
        }else if(EIT[i].FLEX_value_SET_NAME=="10 Number"){
            drawInputNumber(varName,EIT[i].END_USER_COLUMN_NAME,req);    
        }else if(EIT[i].FLEX_value_SET_NAME=="HRC_STANDARD_DATE"){
           drawInputDate(varName,EIT[i].END_USER_COLUMN_NAME ,req);
        }
       
    }
    
    
};
function drawInputText (varName,lblValue ,required){
    drawLabel(lblValue);
    var input = document.createElement("oj-input-text");
    input.setAttribute('disabled', 'false');
    input.setAttribute('value','{{'+varName+'}}');
    input.setAttribute('required',required); 
    var parent = document.getElementById("xx");
     // return input ; 
   parent.appendChild(input);
}
function drawInputNumber(varName,lblValue ,required){
   drawLabel(lblValue);
    var input = document.createElement("oj-input-number");
    input.setAttribute('disabled', 'false');
    input.setAttribute('value','{{'+varName+'}}'); 
    input.setAttribute('required',required); 
    var parent = document.getElementById("xx");
    //return input ; 
    parent.appendChild(input);
}
function drawLabel(lblValue){
    var label = document.createElement("label");
    label.innerHTML = lblValue;    
    var parent = document.getElementById("xx");    
    parent.appendChild(label);
}
function drawInputDate(varName,lblValue ,required){
    drawLabel(lblValue);
    var input = document.createElement("oj-input-date");
    input.setAttribute('disabled', 'false');
    input.setAttribute('value','{{'+varName+'}}'); 
    input.setAttribute('required',required); 
    var parent = document.getElementById("xx");
    //  return input ; 
    parent.appendChild(input);
}

 }
 return new Screen();
});
