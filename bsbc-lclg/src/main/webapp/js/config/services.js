/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['config/serviceconfig'], function (serviceConfig) {

    /**
     * The view model for managing all services
     */
    function services() {

        var self = this;

        var servicesHost = ""
        var paasServiceHost = ""
        var appServiceHost = ""
        var biReportServletPath = ""
        var restPathSearch ="http://localhost:8080/LC_LG_Extension/webapi/myresource";
        var restPath ="http://localhost:8080/LC_LG_Extension/webapi/workbenchservices";

        self.authenticate = function (userName, password) {
            var serviceURL = restPath + "login/login2";
            var payload = {
                "userName": userName, "password": password
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.getGeneric = function (serviceName,headers) {
            var serviceURL = restPathSearch + serviceName;
            console.log(serviceURL);
//          headers.Authorization = "Basic YW1yby5hbGZhcmVzQGFwcHNwcm8tbWUuY29tOkFwcHNwcm9AMTIzNA=="
             return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers,true);
        };
        self.editGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
            };
            return serviceConfig.callPutApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.addGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
            };
            console.log("Inside Services")
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.searcheGeneric = function (serviceName,payload) {
            var serviceURL =restPath+serviceName;
            var headers = {
                 "content-type": "application/x-www-form-urlencoded"
            };

            return serviceConfig.callPostServiceUncodeed(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
        };
        
        
        
        

    };

    return new services();
});