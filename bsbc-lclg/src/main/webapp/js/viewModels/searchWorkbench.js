/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'ojs/ojknockout', 'promise', 'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojarraydataprovider', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojarraytabledatasource', 'ojs/ojdatetimepicker',
    'ojs/ojselectcombobox', 'ojs/ojtimezonedata'],
        function (oj, ko, $, services) {

            function AboutViewModel() {
                var self = this;
                // Below are a set of the ViewModel methods invoked by the oj-module component.
                // Please reference the oj-module jsDoc for additional information.
                /**
                 * Optional ViewModel method invoked after the View is inserted into the
                 * document DOM.  The application can put logic that requires the DOM being
                 * attached here. 
                 * This method might be called multiple times - after the View is created 
                 * and inserted into the DOM and after the View is reconnected 
                 * after being disconnected.
                 */

                self.searchTerm = ko.observable();
                self.TransactionNumbervalue = ko.observable();
                self.LCNumbervalue = ko.observable();
                self.selectValLCType = ko.observable();
                self.selectValBusinessUnit = ko.observable();
                self.selectValRateType = ko.observable();
                self.selectValStatus = ko.observable();
                self.BusinessUnitVal = ko.observable();
                self.LCTypeVal = ko.observable();
                self.RateTypeVal = ko.observable();
                self.StatusVal = ko.observable();
                self.workbenchdata = ko.observableArray([]);
                self.ExchangeDatevalue = ko.observable();
                self.LCOpeningDatevalue = ko.observable();
                self.LCExpiryDatevalue = ko.observable();
                self.isRequired = ko.observable(true);
                self.selectValLCType({'value': 'please select value', 'label': 'please select value'});
                self.selectValBusinessUnit({'value': 'please select value', 'label': 'please select value'});
                self.selectValRateType({'value': 'please select value', 'label': 'please select value'});
                self.selectValStatus({'value': 'please select value', 'label': 'please select value'});

                self.search = function (event) {
              
                   self.workbenchdata([]);
                     self.globalSearch();
                     
                };

                self.btnRest = function (event) {
                    self.TransactionNumbervalue("");
                    self.LCNumbervalue("");
                    self.selectValLCType({'value': 'please select value', 'label': 'please select value'});
                    self.selectValBusinessUnit({'value': 'please select value', 'label': 'please select value'});
                    self.selectValRateType({'value': 'please select value', 'label': 'please select value'});
                    self.selectValStatus({'value': 'please select value', 'label': 'please select value'});
                    self.ExchangeDatevalue("");
                    self.LCOpeningDatevalue("");
                    self.LCExpiryDatevalue("");

                };
      
                self.globalSearch = function () {

                    var searhCbFn = function (data) {

                        $.each(data, function (index) {

                            self.workbenchdata.push({
                                id:[data[index].id],
                               TransactionNumber: [data[index].transaction_number],
                               LCNumber: [data[index].lc_number],
                               status_date:[data[index].status_date],
                               BusinessUnit: [data[index].business_unit],
                               functional_currency_amount:[data[index].functional_currency_amount],
                               currency:[data[index].currency],
                               LCType: [data[index].lc_type],
                               Amount: [data[index].amount],
                               RateType: [data[index].rate_type],
                               ExchangeDate: [data[index].exchange_rate],
                               ExchangeRate: [data[index].exchange_date],
                               LCOpeningDate: [data[index].lc_opening_date],
                               LCExpiryDate: [data[index].lc_expiry_date],
                               Status: [data[index].status]});
                        });

                    };
                    var searhfailCbFn = function () {
                        console.log("Failed");
                    };
                    var service = "/search/" + self.TransactionNumbervalue()+"/"+ self.BusinessUnitVal()+"/"+ self.LCNumbervalue()+"/"+ self.LCTypeVal()+"/"+ self.RateTypeVal()+"/"+ self.StatusVal()+"/"+  self.ExchangeDatevalue()+"/"+  self.LCOpeningDatevalue()+"/"+  self.LCExpiryDatevalue();
                    services.getGeneric(service).then(searhCbFn, searhfailCbFn);
                };
                self.dataprovider = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.workbenchdata, {idAttribute: 'TransactionNumber'}));

                self.columnArray = [{"headerText": " Transaction Number ",
                        "field": "TransactionNumber"},
                    {"headerText": "LC Number",
                        "field": "LCNumber"},
                    {"headerText": "Business Unit",
                        "field": "BusinessUnit"},
                    {"headerText": "LC Type",
                        "field": "LCType"},
                    {"headerText": "Amount",
                        "field": "Amount"},
                    {"headerText": "Rate Type",
                        "field": "RateType"},
                    {"headerText": "Exchange Date",
                        "field": "ExchangeDate"},
                    {"headerText": "Exchange Rate",
                        "field": "ExchangeRate"},
                    {"headerText": "LC Opening Date",
                        "field": "LCOpeningDate"},
                    {"headerText": "LC Expiry Date",
                        "field": "LCExpiryDate"},
                    {"headerText": "Status",
                        "field": "Status"}];

                self.BusinessUnit = [
                    {value: 'please select value', label: ' please select value'},
                    {value: 'LC1 BU', label: 'LC1 BU'},
                    {value: 'BusinessUnit3', label: 'BusinessUnit3'},
                    {value: 'BusinessUnit4', label: 'BusinessUnit4'},
                    {value: 'BusinessUnit5', label: 'BusinessUnit5'}
                ];

                self.LCType = [
                    {value: 'please select value', label: ' please select value'},
                    {value: 'TYPE', label: 'TYPE'},
                    {value: 'BusinessUnit3', label: 'BusinessUnit3'},
                    {value: 'BusinessUnit4', label: 'BusinessUnit4'},
                    {value: 'BusinessUnit5', label: 'BusinessUnit5'}
                ];

                self.RateType = [
                    {value: 'please select value', label: ' please select value'},
                    {value: 'RATE TYPE', label: 'RATE TYPE'},
                    {value: 'BusinessUnit3', label: 'BusinessUnit3'},
                    {value: 'BusinessUnit4', label: 'BusinessUnit4'},
                    {value: 'BusinessUnit5', label: 'BusinessUnit5'}
                ];

                self.Status = [
                    {value: 'please select value', label: ' please select value'},
                    {value: 'STATUS', label: 'STATUS'},
                    {value: 'BusinessUnit3', label: 'BusinessUnit3'},
                    {value: 'BusinessUnit4', label: 'BusinessUnit4'},
                    {value: 'BusinessUnit5', label: 'BusinessUnit5'}
                ];

                self.buttonClick = function (event) {
                    var element = document.getElementById('table');
                    var currentRow = element.currentRow;
                    console.log(currentRow['rowIndex']);
                    console.log(self.workbenchdata()[currentRow['rowIndex']].LCNumber[0]);
                };

                self.add = function () {
                    oj.Router.rootInstance.go('workBench');
                };
                self.update = function () {
                    
                    
                       var element = document.getElementById('table');
                    var currentRow = element.currentRow;

                    if (currentRow !== null) {
                        // var guaranteeStatus 
                        oj.Router.rootInstance.store(self.workbenchdata()[currentRow['rowIndex']]);
                        oj.Router.rootInstance.go('EditworkBench');
                        console.log(self.workbenchdata()[currentRow['rowIndex']]);
                        oj.Router.rootInstance.go('EditworkBench');
                    } else {
                       console.log("Not Selected");
                    }
//                    
//                    
//                     console.log("test update");
//                    var element = document.getElementById('table');
//                    var currentRow = element.currentRow;
//                    console.log( self.workbenchdata()[currentRow['rowIndex']]); 
                   // oj.Router.rootInstance.go('EditworkBench');
                };
                self.view = function () {
                    
                                var element = document.getElementById('table');
                    var currentRow = element.currentRow;

                    if (currentRow !== null) {
                        // var guaranteeStatus 
                        oj.Router.rootInstance.store(self.workbenchdata()[currentRow['rowIndex']]);
                        console.log(self.workbenchdata()[currentRow['rowIndex']]);
                        oj.Router.rootInstance.go('ViewworkBench');
                    } else {
                       console.log("Not Selected");
                    }
                  
                    
                };



                self.connected = function () {
                    // Implement if needed
                };
                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                self.disconnected = function () {
                    // Implement if needed
                };
                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                self.transitionCompleted = function () {
                    // Implement if needed

                };
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new AboutViewModel();
        }
);
