/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojkeyset', 'config/services', 'ojs/ojknockout', 'promise', 'ojs/ojlistview', 'ojs/ojlabel', 'ojs/ojinputtext', 'ojs/ojselectcombobox',
    'ojs/ojdatetimepicker', 'ojs/ojcheckboxset', 'ojs/ojbutton', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojnavigationlist',
    'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojtrain', 'ojs/ojdialog',
    'ojs/ojpopup'],
        function (oj, ko, $, keySet, services) {

            function workBenchViewModel() {
                var self = this;

// Inputs Search Start

//Business Unit Input search Start 
                self.tags = [
                    {value: "Value 1", label: "Business Unit 1"},
                    {value: "Value 2", label: "Business Unit 2"},
                    {value: "Value 3", label: "Business Unit 3"},
                    {value: "Value 4", label: "Business Unit 4"},
                    {value: "Value 5", label: "Business Unit 5"},
                    {value: "Value 6", label: "Business Unit 6"},
                    {value: "Value 7", label: "Business Unit 7"}
                ];

                self.tagsDataProvider = new oj.ArrayDataProvider(this.tags, {keyAttributes: 'value'});

                self.search = function (event) {

                    var trigger = event.type;
                    var term;

                    if (trigger === "ojValueUpdated") {
                        // search triggered from input field
                        // getting the search term from the ojValueUpdated event
                        term = event['detail']['value'];
                        trigger += " event";
                    } else {
                        // search triggered from end slot
                        // getting the value from the element to use as the search term.
                        term = document.getElementById("search").value;
                        trigger = "click on search button";
                    }

                };

                //Business Unit Input search END




                //Currency Input search Start 
                self.currencyValues = [
                    {value: "Value 1", label: "Currency 1"},
                    {value: "Value 2", label: "Currency 2"},
                    {value: "Value 3", label: "Currency 3"},
                    {value: "Value 4", label: "Currency 4"},
                    {value: "Value 5", label: "Currency 5"},
                    {value: "Value 6", label: "Currency 6"},
                    {value: "Value 7", label: "Currency 7"}
                ];

                self.tagsDataProvider2 = new oj.ArrayDataProvider(this.currencyValues, {keyAttributes: 'value'});

                self.getCurrency = function (event) {

                    var trigger = event.type;
                    var term;

                    if (trigger === "ojValueUpdated") {
                        // search triggered from input field
                        // getting the search term from the ojValueUpdated event
                        term = event['detail']['value'];
                        trigger += " event";
                    } else {
                        // search triggered from end slot
                        // getting the value from the element to use as the search term.
                        term = document.getElementById("search").value;
                        trigger = "click on search button";
                    }

                };

                //Currency Input search END



                //Banck Account Input search Start 
                self.banckAccountValues = [
                    {value: "Value 1", label: "banck Account 1"},
                    {value: "Value 2", label: "banck Account 2"},
                    {value: "Value 3", label: "banck Account 3"},
                    {value: "Value 4", label: "banck Account 4"},
                    {value: "Value 5", label: "banck Account 5"},
                    {value: "Value 6", label: "banck Account 6"},
                    {value: "Value 7", label: "banck Account 7"}
                ];

                self.tagsDataProvider3 = new oj.ArrayDataProvider(this.banckAccountValues, {keyAttributes: 'value'});

                self.getBanckAccount = function (event) {

                    var trigger = event.type;
                    var term;

                    if (trigger === "ojValueUpdated") {
                        // search triggered from input field
                        // getting the search term from the ojValueUpdated event
                        term = event['detail']['value'];
                        trigger += " event";
                    } else {
                        // search triggered from end slot
                        // getting the value from the element to use as the search term.
                        term = document.getElementById("search").value;
                        trigger = "click on search button";
                    }

                };

                //Banck Account Input search END

// INPUTS Search End 

                this.itemOnly = function (context)
                {
                    return context['leaf'];
                };

                this.expanded = new keySet.ExpandedKeySet(['a', 'b', 'c']);

                this.isRequired = ko.observable(true);



                self.transactionNumber = ko.observable();
                self.businessUnit = ko.observable();
                self.LCNumber = ko.observable();
                self.amount = ko.observable();
                self.currency = ko.observable();
                self.LCType = ko.observable();
                self.LCOpeningData = ko.observable();
                self.LCExpiryData = ko.observable();
                self.rateType = ko.observable();
                self.exchangeDate = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));
                self.exchangeRate = ko.observable();
                self.eunctionalCurrencyAmount = ko.observable();
                self.status = ko.observable();
                self.statusDate = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));
                self.banckAccount = ko.observable();
                self.incoTerms = ko.observable();
                self.paymentTerms = ko.observable();
                self.lastShipmentDate = ko.observable();
                self.partialShipment = ko.observable(false);
                //self.partialShipmentValue = ko.observable(self.partialShipment()[0]);
                self.otherTerms = ko.observable();
                self.applicantcommission = ko.observable();
                self.benificiaryCommission = ko.observable();
                self.transshipment = ko.observable(false);
                //self.transshipmenttValue = ko.observable(self.transshipment()[0]);
                self.shipmentPort = ko.observable();
                self.shipmentMode = ko.observable();
                self.description = ko.observable();
                self.tolerance = ko.observable();
                self.LCMargin = ko.observable();
                self.tolerancePercent = ko.observable();
                self.LCMarginPercent = ko.observable();

// Message Start

//
//                self.createMessage = function (severity, summary, detail) {
//                    //  var initCapSeverity = severity.charAt(0).toUpperCase() + severity.slice(1);
//                    return {
//                        severity: severity,
//                        summary: summary,
//                        detail: detail,
//                        autoTimeout: 5000
//                    };
//                };


                // Initially display error and warning messages
               // self.applicationMessages = ko.observableArray([]);


// Message End
// 
// Reset Function Start
                self.resetInputsText = function () {

                    self.transactionNumber("");
                    self.businessUnit("");
                    self.LCNumber("");
                    self.amount("");
                    self.currency("");
                    self.LCType("");
                    self.LCOpeningData("");
                    self.LCExpiryData("");
                    self.rateType("");
                    self.exchangeRate("");
                    self.eunctionalCurrencyAmount("");
                    self.status("");
                    self.banckAccount("");
                    self.incoTerms("");
                    self.paymentTerms("");
                    self.lastShipmentDate("");
                    self.partialShipment(false);
                    self.otherTerms("");
                    self.applicantcommission("");
                    self.benificiaryCommission("");
                    self.transshipment(false);
                    self.shipmentPort("");
                    self.shipmentMode("");
                    self.description("");
                    self.tolerance("");
                    self.LCMargin("");
                    self.tolerancePercent("");
                    self.LCMarginPercent("");
                };

// Reset Function End

                // Function which Call POST Web Services for Lc Workbenck and LC Details

//                self.callPostLCWorkBenck = function () {
//
//                    var payload = {
//                        "transactionNumber": self.transactionNumber(),
//                        "businessUnit": self.businessUnit(),
//                        "lcnumber": self.LCNumber(),
//                        "amount": self.amount(),
//                        "currency": self.currency(),
//                        "lctype": self.LCType(),
//                        "lcopeningData": self.LCOpeningData(),
//                        "lcexpiryData": self.LCExpiryData(),
//                        "rateType": self.rateType(),
//                        "exchangeDate": self.exchangeDate(),
//                        "exchangeRate": self.exchangeRate(),
//                        "eunctionalCurrencyAmount": self.eunctionalCurrencyAmount(),
//                        "status": self.status(),
//                        "statusDate": self.statusDate(),
//                        "banckAccount": self.banckAccount(),
//                        "incoTerms": self.incoTerms(),
//                        "paymentTerms": self.paymentTerms(),
//                        "lastShipmentDate": self.lastShipmentDate(),
//                        "partialShipment": self.partialShipment(),
//                        "otherTerms": self.otherTerms(),
//                        "applicantcommission": self.applicantcommission(),
//                        "benificiaryCommission": self.benificiaryCommission(),
//                        "transshipment": self.transshipment(),
//                        "shipmentPort": self.shipmentPort(),
//                        "shipmentMode": self.shipmentMode(),
//                        "description": self.description(),
//                        "tolerance": self.tolerance(),
//                        "tolerancePercent": self.tolerancePercent(),
//                        "lcmargin": self.LCMargin(),
//                        "lcmarginPercent": self.LCMarginPercent()
//                    };
//                    var createWorkBenchCbFn = function (data) {
//
//                        self.applicationMessages.push(self.createMessage('confirmation', '', 'operation accomplished successfully'));
//                        self.restSuccessFunctionality();
//
//                    };
//                    var failCbFn = function () {
//
//                        self.applicationMessages.push(self.createMessage('error', '', 'Error message detail'));
//                        self.restErrorFunctionality();
//                        console.log("Failed");
//                    };
//                    var service = "/workbench";
//                    services.addGeneric(service, JSON.stringify(payload)).then(createWorkBenchCbFn, failCbFn);
//                };

                // Below are a set of the ViewModel methods invoked by the oj-module component.
                // Please reference the oj-module jsDoc for additional information.




// New Add By Mohamed Yasser Start

//                this.selectedStepValue = ko.observable('stp1');
//                this.selectedStepLabel = ko.observable('Step One');
//
//                this.stepArray = ko.observableArray(
//                        [{label: 'Create', id: 'stp1'},
//
//                            {label: 'Review', id: 'stp3'}
//                        ]);
//
//                this.updateLabelText = function (event) {
//                    var train = document.getElementById("train");
//                    self.selectedStepLabel(train.getStep(event.detail.value).label);
//                };
//
//                self.nextStep = function () {
//                    var train = document.getElementById("train");
//                    var next = train.getNextSelectableStep();
//                    if (next !== null) {
//                        self.selectedStepValue(next);
//                        self.selectedStepLabel(train.getStep(next).label);
//                    }
//                };
//
//                self.previousStep = function () {
//                    var train = document.getElementById("train");
//                    var prev = train.getPreviousSelectableStep();
//                    if (prev !== null) {
//                        self.selectedStepValue(prev);
//                        self.selectedStepLabel(train.getStep(prev).label);
//                    }
//                };

                self.lCWorkbenckHide = ko.observable(true);
                self.lCDetailsHide = ko.observable(false);
                //self.hideNext = ko.observable(false);
                //self.showSubmit = ko.observable(false);
                self.hidePervious = ko.observable(false);
                self.hidelcDetails = ko.observable(true);
                self.hideCancel = ko.observable(true);
                
                 self.cancleAction = function () {
                     oj.Router.rootInstance.go('searchWorkbench');
                 };

                self.showLCDetails = function () {

//                    if (!self.transactionNumber() || !self.businessUnit() || !self.LCNumber() || !self.amount() || !self.currency()
//                            || !self.LCType() || !self.LCOpeningData() || !self.LCExpiryData() || !self.rateType() || !self.exchangeRate() ||
//                            !self.eunctionalCurrencyAmount() || !self.status()) {
//                        self.applicationMessages.push(self.createMessage('warning', '', 'Please complete The Data'));
//                    } else {

                        self.lCWorkbenckHide(false);
                        self.lCDetailsHide(true);
                        self.hidelcDetails(false);
                        self.hideCancel(false);
                        //self.hideNext(true);
                        self.hidePervious(true);
                   // }
                };

                self.showLCWorkbench = function () {
                    self.lCWorkbenckHide(true);
                    self.lCDetailsHide(false);
                    $(".disable-element").prop('disabled', false);
                    self.hidelcDetails(true);
                    self.hideCancel(true);
                    self.hidePervious(false);
                  //  self.hideNext(false);
                  //  self.showSubmit(false);
                   // self.previousStep();
                };

//                self.disableInputText = function () {
//
//                    if (!self.banckAccount() || !self.incoTerms() || !self.paymentTerms() || !self.lastShipmentDate() ||
//                            !self.otherTerms() || !self.applicantcommission() || !self.benificiaryCommission() || !self.shipmentPort() || !self.shipmentMode() || !self.description() ||
//                            !self.tolerance() || !self.LCMargin() || !self.tolerancePercent() || !self.LCMarginPercent()
//                            ) {
//                        self.applicationMessages.push(self.createMessage('warning', '', 'Please complete The Data'));
//                    } else {
//
//                        self.hideNext(false);
//                        self.showSubmit(true);
//                        $(".disable-element").prop('disabled', true);
//                        $(".checkmark").css("background-color", "#ededee");
//                        self.nextStep();
//                    }
//                };

//                self.openPopup = function () {
//                    document.querySelector('#modalDialog1').open();
//                };
//
//                self.submit = function () {
//                    self.callPostLCWorkBenck();
//                    document.querySelector('#modalDialog1').close();
//                };

//                self.closePopup = function () {
//                    document.querySelector('#modalDialog1').close();
//                    self.hideNext(true);
//                    self.showSubmit(false);
//                    $(".disable-element").prop('disabled', false);
//                    self.previousStep();
//                };


                self.restSuccessFunctionality = function () {
                    $(".disable-element").prop('disabled', false);
                    self.resetInputsText();
                    self.hidelcDetails(true);
                    self.hideCancel(true);
                    self.hidePervious(false);
                    self.hideNext(false);
                    self.showSubmit(false);
                    self.lCWorkbenckHide(true);
                    self.lCDetailsHide(false);
                    self.previousStep();
                    oj.Router.rootInstance.go('searchWorkbench');
                };

                self.restErrorFunctionality = function () {

                    $(".disable-element").prop('disabled', false);
                    self.hideNext(true);
                    self.showSubmit(false);
                    ;
                };

                startAnimationListener = function (data, event) {
                    var ui = event.detail;
                    if (!$(event.target).is("#popup1"))
                        return;

                    if ("open" === ui.action)
                    {
                        event.preventDefault();
                        var options = {"direction": "top"};
                        oj.AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
                    } else if ("close" === ui.action)
                    {
                        event.preventDefault();
                        ui.endCallback();
                    }
                };
// New Add By Mohamed Yasser End

                /**
                 * Optional ViewModel method invoked after the View is inserted into the
                 * document DOM.  The application can put logic that requires the DOM being
                 * attached here. 
                 * This method might be called multiple times - after the View is created 
                 * and inserted into the DOM and after the View is reconnected 
                 * after being disconnected.
                 */
                self.connected = function () {
                    // Implement if needed
                    //self.applicationMessages([]);
                    oj.Router.rootInstance.retrieve();
                    console.log(oj.Router.rootInstance.retrieve());
                    self.amount(oj.Router.rootInstance.retrieve().Amount[0]);
                    self.businessUnit(oj.Router.rootInstance.retrieve().BusinessUnit[0]);
                    self.exchangeDate(oj.Router.rootInstance.retrieve().ExchangeDate[0]);
                    self.exchangeRate(oj.Router.rootInstance.retrieve().ExchangeRate[0]);
                    self.LCExpiryData(oj.Router.rootInstance.retrieve().LCExpiryDate[0]);
                    self.LCNumber(oj.Router.rootInstance.retrieve().LCNumber[0]);
                    self.LCOpeningData(oj.Router.rootInstance.retrieve().LCOpeningDate[0]);
                    self.LCType(oj.Router.rootInstance.retrieve().LCType[0]);
                    self.rateType(oj.Router.rootInstance.retrieve().RateType[0]);
                    self.status(oj.Router.rootInstance.retrieve().Status[0]);
                    self.transactionNumber(oj.Router.rootInstance.retrieve().TransactionNumber[0]);
                };

                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                self.disconnected = function () {
                    // Implement if needed
                };

                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                self.transitionCompleted = function () {
                    // Implement if needed
                };
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new workBenchViewModel();
        }
);
