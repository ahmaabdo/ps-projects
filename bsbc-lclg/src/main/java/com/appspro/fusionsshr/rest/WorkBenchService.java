/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;


import com.appspro.fusionsshr.bean.WorkBenchBean;
import com.appspro.fusionsshr.bean.WorkbenchSearch;
import com.appspro.fusionsshr.dao.WorlBench;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import static org.glassfish.jersey.internal.inject.Bindings.service;

/**
 *
 * @author amromyresource
 */
@Path("workbenchservices")
public class WorkBenchService {

    WorlBench worlBench = new WorlBench();

    @POST
    @Path("/workbench/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public WorkBenchBean createWorkBench(WorkBenchBean workBenchBean) {
        return worlBench.createWorkBench(workBenchBean);
    }
    
    @PUT
    @Path("/update/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public WorkBenchBean worknbenchUpdate(WorkBenchBean workBenchBean) {

        return worlBench.updateWorkbrench(workBenchBean);

    }

}
