/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.fusionsshr.bean.WorkbenchSearch;
import com.appspro.fusionsshr.dao.WorkbenchSearchDAO;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author lenovo
 */
@Path("myresource")
public class workbenchSearchService {

    WorkbenchSearchDAO service = new WorkbenchSearchDAO();

    @GET
    @Path("/test/")
    @Produces(MediaType.TEXT_PLAIN)
    public String getdata() {
        return "test sucess dfweqfre" ;
    }

    @GET
    @Path("/search/{transaction_number}/{BUSINESS_UNIT}/{LC_NUMBER}/{LC_TYPE}/{RATE_TYPE}/{STATUS}/{EXCHANGE_DATE}/{LC_OPENINIG_DATE}/{LC_EXPIRY_DATE}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<WorkbenchSearch> worknbenchSearch(@PathParam("transaction_number") String transaction_number,@PathParam("BUSINESS_UNIT") String BUSINESS_UNIT,@PathParam("LC_NUMBER") String LC_NUMBER,@PathParam("LC_TYPE") String LC_TYPE,@PathParam("RATE_TYPE") String RATE_TYPE,@PathParam("STATUS") String STATUS,@PathParam("EXCHANGE_DATE") String EXCHANGE_DATE,@PathParam("LC_OPENINIG_DATE") String LC_OPENINIG_DATE,@PathParam("LC_EXPIRY_DATE") String LC_EXPIRY_DATE) {
        ArrayList<WorkbenchSearch> WorkbenchList = new ArrayList<WorkbenchSearch>();
        WorkbenchList = service.search(transaction_number,BUSINESS_UNIT,LC_NUMBER,LC_TYPE,RATE_TYPE,STATUS,EXCHANGE_DATE,LC_OPENINIG_DATE,LC_EXPIRY_DATE);
        return WorkbenchList;
    }

}
