
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.fusionsshr.bean.WorkbenchSearch;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author lenovo
 */
public class WorkbenchSearchDAO {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;

    
       public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sqlDate;
    }
    

        public ArrayList<WorkbenchSearch> search(String transaction_number, String BUSINESS_UNIT, String LC_NUMBER, String LC_TYPE, String RATE_TYPE, String STATUS, String EXCHANGE_DATE, String LC_OPENINIG_DATE, String LC_EXPIRY_DATE) {
        
        ArrayList<WorkbenchSearch> workbenchList = new ArrayList<WorkbenchSearch>();
        
        try {
            connection = AppsproConnection.getConnection();
            
            String query = "SELECT ID,TRANSACTION_NUMBER ,BUSINESS_UNIT,LC_NUMBER, AMOUNT, CURRENCY,LC_TYPE, TO_CHAR(LC_OPENINIG_DATE,'DD-MM-YYYY'), \n"
                    + "TO_CHAR(LC_EXPIRY_DATE,'DD-MM-YYYY'), RATE_TYPE,TO_CHAR(EXCHANGE_DATE,'DD-MM-YYYY'),EXCHANGE_RATE,FUNCTIONAL_CURRENCY_AMOUNT\n"
                    + ",STATUS,TO_CHAR(STATUS_DATE,'DD-MM-YYYY') from xx_moe.XX_LC_WORKBENCH where  TRANSACTION_NUMBER = ? OR BUSINESS_UNIT = ? OR LC_NUMBER=? OR LC_TYPE=? OR RATE_TYPE=? OR STATUS=? "
                    + "OR EXCHANGE_DATE=? OR LC_OPENINIG_DATE=? OR LC_EXPIRY_DATE=?\n";
            
            ps = connection.prepareStatement(query);
            ps.setString(1, transaction_number);
            ps.setString(2, BUSINESS_UNIT);
            ps.setString(3, LC_NUMBER);
            ps.setString(4, LC_TYPE);
            ps.setString(5, RATE_TYPE);
            ps.setString(6, STATUS);
            ps.setDate(7, getSQLDateFromString(EXCHANGE_DATE));
            ps.setDate(8, getSQLDateFromString(LC_OPENINIG_DATE));
            ps.setDate(9, getSQLDateFromString(LC_EXPIRY_DATE));
            
            rs = ps.executeQuery();
            while (rs.next()) {
                WorkbenchSearch workbenchBean = new WorkbenchSearch();
                workbenchBean.setId(rs.getString(1));
                workbenchBean.setTransaction_number(rs.getString(2));
                workbenchBean.setBusiness_unit(rs.getString(3));   
                workbenchBean.setLc_number(rs.getString(4));
                workbenchBean.setAmount(rs.getString(5));
                workbenchBean.setCurrency(rs.getString(6));
                workbenchBean.setLc_type(rs.getString(7));
                workbenchBean.setLc_opening_date(rs.getString(8));
                workbenchBean.setLc_expiry_date(rs.getString(9));
                workbenchBean.setRate_type(rs.getString(10));                
                workbenchBean.setExchange_date(rs.getString(11));
                workbenchBean.setExchange_rate(rs.getString(12));
                workbenchBean.setFunctional_currency_amount(rs.getString(13));
                workbenchBean.setStatus(rs.getString(14));
                workbenchBean.setStatus_date(rs.getString(15));
                
                workbenchList.add(workbenchBean);
                
            }
        } catch (Exception e) {
            System.out.println("Error: ");
            e.printStackTrace();
        } finally {
            //closeResources(connection, ps, rs);
        }
        return workbenchList;
        
    }
    
    
    
  


}
