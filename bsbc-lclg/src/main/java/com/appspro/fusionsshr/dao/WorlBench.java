/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.fusionsshr.bean.WorkBenchBean;
import com.appspro.fusionsshr.bean.WorkbenchSearch;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author amro
 */
public class WorlBench {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;


    public WorkBenchBean createWorkBench(WorkBenchBean workBenchBean) {

        try {
            connection = AppsproConnection.getConnection();

            String query = "DECLARE \n"
                    + "   LC_ID  NUMBER:= seq_id.nextval; \n"
                    + "   LC_DETAILS_ID  NUMBER:= seq_id.nextval;\n"
                    + "BEGIN \n"
                    + "insert into xx_moe.XX_LC_WORKBENCH (id, transaction_number, business_unit, lc_number,\n"
                    + "  amount, currency, lc_type, lc_openinig_date, lc_expiry_date, rate_type, exchange_date, exchange_rate, functional_currency_amount, status, status_date) VALUES (LC_ID, ?, ?, ?, ?, ?, ?,\n"
                    + "      ?, ?, ?, ?, ?, ?, ?, ?);\n"
                    + "      \n"
                    + "      \n"
                    + "      insert INTO xx_moe.XX_LC_DETAILS (id, WORKBENCH_ID, INCO_TERMS, BANK_ACCOUNT,\n"
                    + "PAYMENT_TERMS, LAST_SHIPMENT_DATE, PARTIAL_SHIPMENT,\n"
                    + "OTHER_TERMS, APPLICANT_COMMISSION,\n"
                    + "BENIFICIARY_COMMESION, TRANSSHIPMENT, SHIPMENT_PORT,\n"
                    + "SHIPMENT_MODE, DESCRIPTION, TOLERANCE, TOLERANCE_PERCENT,\n"
                    + "LC_MARGIN, LC_MARGIN_PERCENT ) VALUES (LC_DETAILS_ID , LC_ID, ?, ?, ?, \n"
                    + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? );\n"
                    + "      \n"
                    + "  COMMIT;\n"
                    + "END;";

            ps = connection.prepareStatement(query);
            ps.setString(1, workBenchBean.getTransactionNumber());
            ps.setString(2, workBenchBean.getBusinessUnit());
            ps.setString(3, workBenchBean.getLcnumber());
            ps.setString(4, workBenchBean.getAmount());
            ps.setString(5, workBenchBean.getCurrency());
            ps.setString(6, workBenchBean.getLctype());
            ps.setDate(7, getSQLDateFromString(workBenchBean.getLcopeningData()));
            ps.setDate(8, getSQLDateFromString(workBenchBean.getLcexpiryData()));
            ps.setString(9, workBenchBean.getRateType());
            ps.setDate(10, getSQLDateFromString(workBenchBean.getExchangeDate()));
            ps.setString(11, workBenchBean.getExchangeRate());
            ps.setString(12, workBenchBean.getEunctionalCurrencyAmount());
            ps.setString(13, workBenchBean.getStatus());
            ps.setDate(14, getSQLDateFromString(workBenchBean.getStatusDate()));
            ps.setString(15, workBenchBean.getIncoTerms());
            ps.setString(16, workBenchBean.getBanckAccount());
            ps.setString(17, workBenchBean.getPaymentTerms());
            ps.setDate(18, getSQLDateFromString(workBenchBean.getLastShipmentDate()));
            ps.setString(19, workBenchBean.getPartialShipment());
            ps.setString(20, workBenchBean.getOtherTerms());
            ps.setString(21, workBenchBean.getApplicantcommission());
            ps.setString(22, workBenchBean.getBenificiaryCommission());
            ps.setString(23, workBenchBean.getTransshipment());
            ps.setString(24, workBenchBean.getShipmentPort());
            ps.setString(25, workBenchBean.getShipmentMode());
            ps.setString(26, workBenchBean.getDescription());
            ps.setString(27, workBenchBean.getTolerance());
            ps.setString(28, workBenchBean.getTolerancePercent());
            ps.setString(29, workBenchBean.getLcmargin());
            ps.setString(30, workBenchBean.getLcmarginPercent());

            rs = ps.executeQuery();

            rs.close();
            ps.close();
        } catch (Exception e) {
            System.out.println("Error: ");
            e.printStackTrace();
        }
        return workBenchBean;
    }
    
    
public WorkBenchBean updateWorkbrench(WorkBenchBean workBenchBean) {

        try {
            connection = AppsproConnection.getConnection();

            String query = "DECLARE \n"
                    + "   LC_ID  NUMBER:= ?; \n"
                    + "BEGIN \n"
                    + "UPDATE xx_moe.XX_LC_WORKBENCH SET \n"
                    + "transaction_number = ?,\n"
                    + "business_unit= ?,\n"
                    + "lc_number= ?,\n"
                    + "amount= ?,\n"
                    + "currency= ?,\n"
                    + "lc_type = ?,\n"
                    + "lc_openinig_date = ?,\n"
                    + "lc_expiry_date = ?,\n"
                    + "rate_type = ?,\n"
                    + "exchange_date = ?,\n"
                    + "exchange_rate= ?,\n"
                    + "functional_currency_amount = ?,\n"
                    + "status= ?,\n"
                    + "status_date = ?\n"
                    + "WHERE id= LC_ID;\n"
                    + "      \n"
                    + "\n"
                    + "UPDATE xx_moe.XX_LC_DETAILS SET \n"
                    + "INCO_TERMS = ?,\n"
                    + "BANK_ACCOUNT= ?,\n"
                    + "PAYMENT_TERMS= ?,\n"
                    + "LAST_SHIPMENT_DATE= ?,\n"
                    + "PARTIAL_SHIPMENT= ?,\n"
                    + "OTHER_TERMS = ?,\n"
                    + "APPLICANT_COMMISSION = ?,\n"
                    + "BENIFICIARY_COMMESION = ?,\n"
                    + "TRANSSHIPMENT = ?,\n"
                    + "SHIPMENT_PORT = ?,\n"
                    + "SHIPMENT_MODE= ?,\n"
                    + "DESCRIPTION = ?,\n"
                    + "TOLERANCE= ?,\n"
                    + "TOLERANCE_PERCENT = ?,\n"
                    + "LC_MARGIN = ?,\n"
                    + "LC_MARGIN_PERCENT = ?\n"
                    + "WHERE id= LC_ID;     \n"
                    + "  COMMIT;\n"
                    + "END; \n";

            ps = connection.prepareStatement(query);
            ps.setString(1, workBenchBean.getId());
            ps.setString(2, workBenchBean.getTransactionNumber());
            ps.setString(3, workBenchBean.getBusinessUnit());
            ps.setString(4, workBenchBean.getLcnumber());
            ps.setString(5, workBenchBean.getAmount());
            ps.setString(6, workBenchBean.getCurrency());
            ps.setString(7, workBenchBean.getLctype());
            ps.setDate(8, getSQLDateFromString(workBenchBean.getLcopeningData()));
            ps.setDate(9, getSQLDateFromString(workBenchBean.getLcexpiryData()));
            ps.setString(10, workBenchBean.getRateType());
            ps.setDate(11, getSQLDateFromString(workBenchBean.getExchangeDate()));
            ps.setString(12, workBenchBean.getExchangeRate());
            ps.setString(13, workBenchBean.getEunctionalCurrencyAmount());
            ps.setString(14, workBenchBean.getStatus());
            ps.setDate(15, getSQLDateFromString(workBenchBean.getStatusDate()));
            ps.setString(16, workBenchBean.getIncoTerms());
            ps.setString(17, workBenchBean.getBanckAccount());
            ps.setString(18, workBenchBean.getPaymentTerms());
            ps.setDate(19, getSQLDateFromString(workBenchBean.getLastShipmentDate()));
            ps.setString(20, workBenchBean.getPartialShipment());
            ps.setString(21, workBenchBean.getOtherTerms());
            ps.setString(22, workBenchBean.getApplicantcommission());
            ps.setString(23, workBenchBean.getBenificiaryCommission());
            ps.setString(24, workBenchBean.getTransshipment());
            ps.setString(25, workBenchBean.getShipmentPort());
            ps.setString(26, workBenchBean.getShipmentMode());
            ps.setString(27, workBenchBean.getDescription());
            ps.setString(28, workBenchBean.getTolerance());
            ps.setString(29, workBenchBean.getTolerancePercent());
            ps.setString(30, workBenchBean.getLcmargin());
            ps.setString(31, workBenchBean.getLcmarginPercent());

            rs = ps.executeQuery();

            rs.close();
            ps.close();
        } catch (Exception e) {
            System.out.println("Error: ");
            e.printStackTrace();
        }
        return workBenchBean;
    }

    public java.sql.Date getSQLDateFromString(String date) {
        java.sql.Date sqlDate = null;
        try {
            Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sqlDate;
    }
}
