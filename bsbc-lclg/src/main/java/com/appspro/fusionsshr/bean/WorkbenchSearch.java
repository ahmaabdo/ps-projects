/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class WorkbenchSearch {
    private String    id;
    private String    transaction_number;
    private String    business_unit;  
    private String    lc_number;
    private String    lc_type;
    private String    rate_type;
    private String    exchange_date;
    private String    lc_opening_date;
    private String    lc_expiry_date;
    private String    status;
    private String    amount;
    private String    currency;
    private String    exchange_rate;
    private String    functional_currency_amount;
    private String    status_date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransaction_number() {
        return transaction_number;
    }

    public void setTransaction_number(String transaction_number) {
        this.transaction_number = transaction_number;
    }

    public String getBusiness_unit() {
        return business_unit;
    }

    public void setBusiness_unit(String business_unit) {
        this.business_unit = business_unit;
    }

    public String getLc_number() {
        return lc_number;
    }

    public void setLc_number(String lc_number) {
        this.lc_number = lc_number;
    }

    public String getLc_type() {
        return lc_type;
    }

    public void setLc_type(String lc_type) {
        this.lc_type = lc_type;
    }

    public String getRate_type() {
        return rate_type;
    }

    public void setRate_type(String rate_type) {
        this.rate_type = rate_type;
    }

    public String getExchange_date() {
        return exchange_date;
    }

    public void setExchange_date(String exchange_date) {
        this.exchange_date = exchange_date;
    }

    public String getLc_opening_date() {
        return lc_opening_date;
    }

    public void setLc_opening_date(String lc_opening_date) {
        this.lc_opening_date = lc_opening_date;
    }

    public String getLc_expiry_date() {
        return lc_expiry_date;
    }

    public void setLc_expiry_date(String lc_expiry_date) {
        this.lc_expiry_date = lc_expiry_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchange_rate() {
        return exchange_rate;
    }

    public void setExchange_rate(String exchange_rate) {
        this.exchange_rate = exchange_rate;
    }

    public String getFunctional_currency_amount() {
        return functional_currency_amount;
    }

    public void setFunctional_currency_amount(String functional_currency_amount) {
        this.functional_currency_amount = functional_currency_amount;
    }

    public String getStatus_date() {
        return status_date;
    }

    public void setStatus_date(String status_date) {
        this.status_date = status_date;
    }

    @Override
    public String toString() {
        return "WorkbenchSearch{" + "id=" + id + ", transaction_number=" + transaction_number + ", business_unit=" + business_unit + ", lc_number=" + lc_number + ", lc_type=" + lc_type + ", rate_type=" + rate_type + ", exchange_date=" + exchange_date + ", lc_opening_date=" + lc_opening_date + ", lc_expiry_date=" + lc_expiry_date + ", status=" + status + ", amount=" + amount + ", currency=" + currency + ", exchange_rate=" + exchange_rate + ", functional_currency_amount=" + functional_currency_amount + ", status_date=" + status_date + '}';
    }

}