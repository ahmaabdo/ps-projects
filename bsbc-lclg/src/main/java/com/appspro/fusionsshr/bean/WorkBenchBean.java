/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.bean;

/**
 *
 * @author lenovo
 */
public class WorkBenchBean {

    private String id;
    private String transactionNumber;
    private String businessUnit;
    private String lcnumber;
    private String amount;
    private String currency;
    private String lctype;
    private String lcopeningData;
    private String lcexpiryData;
    private String rateType;
    private String exchangeDate;
    private String exchangeRate;
    private String eunctionalCurrencyAmount;
    private String status;
    private String statusDate;
    private String banckAccount;
    private String incoTerms;
    private String paymentTerms;
    private String lastShipmentDate;
    private String partialShipment;
    private String otherTerms;
    private String applicantcommission;
    private String benificiaryCommission;
    private String transshipment;
    private String shipmentPort;
    private String shipmentMode;
    private String description;
    private String tolerance;
    private String lcmargin;
    private String tolerancePercent;
    private String lcmarginPercent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the transactionNumber
     */
    public String getTransactionNumber() {
        return transactionNumber;
    }

    /**
     * @param transactionNumber the transactionNumber to set
     */
    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    /**
     * @return the businessUnit
     */
    public String getBusinessUnit() {
        return businessUnit;
    }

    /**
     * @param businessUnit the businessUnit to set
     */
    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    /**
     * @return the lcnumber
     */
    public String getLcnumber() {
        return lcnumber;
    }

    /**
     * @param lcnumber the lcnumber to set
     */
    public void setLcnumber(String lcnumber) {
        this.lcnumber = lcnumber;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the lctype
     */
    public String getLctype() {
        return lctype;
    }

    /**
     * @param lctype the lctype to set
     */
    public void setLctype(String lctype) {
        this.lctype = lctype;
    }

    /**
     * @return the lcopeningData
     */
    public String getLcopeningData() {
        return lcopeningData;
    }

    /**
     * @param lcopeningData the lcopeningData to set
     */
    public void setLcopeningData(String lcopeningData) {
        this.lcopeningData = lcopeningData;
    }

    /**
     * @return the lcexpiryData
     */
    public String getLcexpiryData() {
        return lcexpiryData;
    }

    /**
     * @param lcexpiryData the lcexpiryData to set
     */
    public void setLcexpiryData(String lcexpiryData) {
        this.lcexpiryData = lcexpiryData;
    }

    /**
     * @return the rateType
     */
    public String getRateType() {
        return rateType;
    }

    /**
     * @param rateType the rateType to set
     */
    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    /**
     * @return the exchangeDate
     */
    public String getExchangeDate() {
        return exchangeDate;
    }

    /**
     * @param exchangeDate the exchangeDate to set
     */
    public void setExchangeDate(String exchangeDate) {
        this.exchangeDate = exchangeDate;
    }

    /**
     * @return the exchangeRate
     */
    public String getExchangeRate() {
        return exchangeRate;
    }

    /**
     * @param exchangeRate the exchangeRate to set
     */
    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    /**
     * @return the eunctionalCurrencyAmount
     */
    public String getEunctionalCurrencyAmount() {
        return eunctionalCurrencyAmount;
    }

    /**
     * @param eunctionalCurrencyAmount the eunctionalCurrencyAmount to set
     */
    public void setEunctionalCurrencyAmount(String eunctionalCurrencyAmount) {
        this.eunctionalCurrencyAmount = eunctionalCurrencyAmount;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the statusDate
     */
    public String getStatusDate() {
        return statusDate;
    }

    /**
     * @param statusDate the statusDate to set
     */
    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * @return the banckAccount
     */
    public String getBanckAccount() {
        return banckAccount;
    }

    /**
     * @param banckAccount the banckAccount to set
     */
    public void setBanckAccount(String banckAccount) {
        this.banckAccount = banckAccount;
    }

    /**
     * @return the incoTerms
     */
    public String getIncoTerms() {
        return incoTerms;
    }

    /**
     * @param incoTerms the incoTerms to set
     */
    public void setIncoTerms(String incoTerms) {
        this.incoTerms = incoTerms;
    }

    /**
     * @return the paymentTerms
     */
    public String getPaymentTerms() {
        return paymentTerms;
    }

    /**
     * @param paymentTerms the paymentTerms to set
     */
    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    /**
     * @return the lastShipmentDate
     */
    public String getLastShipmentDate() {
        return lastShipmentDate;
    }

    /**
     * @param lastShipmentDate the lastShipmentDate to set
     */
    public void setLastShipmentDate(String lastShipmentDate) {
        this.lastShipmentDate = lastShipmentDate;
    }

    /**
     * @return the partialShipment
     */
    public String getPartialShipment() {
        return partialShipment;
    }

    /**
     * @param partialShipment the partialShipment to set
     */
    public void setPartialShipment(String partialShipment) {
        this.partialShipment = partialShipment;
    }

    /**
     * @return the otherTerms
     */
    public String getOtherTerms() {
        return otherTerms;
    }

    /**
     * @param otherTerms the otherTerms to set
     */
    public void setOtherTerms(String otherTerms) {
        this.otherTerms = otherTerms;
    }

    /**
     * @return the applicantcommission
     */
    public String getApplicantcommission() {
        return applicantcommission;
    }

    /**
     * @param applicantcommission the applicantcommission to set
     */
    public void setApplicantcommission(String applicantcommission) {
        this.applicantcommission = applicantcommission;
    }

    /**
     * @return the benificiaryCommission
     */
    public String getBenificiaryCommission() {
        return benificiaryCommission;
    }

    /**
     * @param benificiaryCommission the benificiaryCommission to set
     */
    public void setBenificiaryCommission(String benificiaryCommission) {
        this.benificiaryCommission = benificiaryCommission;
    }

    /**
     * @return the transshipment
     */
    public String getTransshipment() {
        return transshipment;
    }

    /**
     * @param transshipment the transshipment to set
     */
    public void setTransshipment(String transshipment) {
        this.transshipment = transshipment;
    }

    /**
     * @return the shipmentPort
     */
    public String getShipmentPort() {
        return shipmentPort;
    }

    /**
     * @param shipmentPort the shipmentPort to set
     */
    public void setShipmentPort(String shipmentPort) {
        this.shipmentPort = shipmentPort;
    }

    /**
     * @return the shipmentMode
     */
    public String getShipmentMode() {
        return shipmentMode;
    }

    /**
     * @param shipmentMode the shipmentMode to set
     */
    public void setShipmentMode(String shipmentMode) {
        this.shipmentMode = shipmentMode;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the tolerance
     */
    public String getTolerance() {
        return tolerance;
    }

    /**
     * @param tolerance the tolerance to set
     */
    public void setTolerance(String tolerance) {
        this.tolerance = tolerance;
    }

    /**
     * @return the lcmargin
     */
    public String getLcmargin() {
        return lcmargin;
    }

    /**
     * @param lcmargin the lcmargin to set
     */
    public void setLcmargin(String lcmargin) {
        this.lcmargin = lcmargin;
    }

    /**
     * @return the tolerancePercent
     */
    public String getTolerancePercent() {
        return tolerancePercent;
    }

    /**
     * @param tolerancePercent the tolerancePercent to set
     */
    public void setTolerancePercent(String tolerancePercent) {
        this.tolerancePercent = tolerancePercent;
    }

    /**
     * @return the lcmarginPercent
     */
    public String getLcmarginPercent() {
        return lcmarginPercent;
    }

    /**
     * @param lcmarginPercent the lcmarginPercent to set
     */
    public void setLcmarginPercent(String lcmarginPercent) {
        this.lcmarginPercent = lcmarginPercent;
    }

    @Override
    public String toString() {
        return "WorkBenchBean{" + "id=" + id + ", transactionNumber=" + transactionNumber + ", businessUnit=" + businessUnit + ", lcnumber=" + lcnumber + ", amount=" + amount + ", currency=" + currency + ", lctype=" + lctype + ", lcopeningData=" + lcopeningData + ", lcexpiryData=" + lcexpiryData + ", rateType=" + rateType + ", exchangeDate=" + exchangeDate + ", exchangeRate=" + exchangeRate + ", eunctionalCurrencyAmount=" + eunctionalCurrencyAmount + ", status=" + status + ", statusDate=" + statusDate + ", banckAccount=" + banckAccount + ", incoTerms=" + incoTerms + ", paymentTerms=" + paymentTerms + ", lastShipmentDate=" + lastShipmentDate + ", partialShipment=" + partialShipment + ", otherTerms=" + otherTerms + ", applicantcommission=" + applicantcommission + ", benificiaryCommission=" + benificiaryCommission + ", transshipment=" + transshipment + ", shipmentPort=" + shipmentPort + ", shipmentMode=" + shipmentMode + ", description=" + description + ", tolerance=" + tolerance + ", lcmargin=" + lcmargin + ", tolerancePercent=" + tolerancePercent + ", lcmarginPercent=" + lcmarginPercent + '}';
    }

}
