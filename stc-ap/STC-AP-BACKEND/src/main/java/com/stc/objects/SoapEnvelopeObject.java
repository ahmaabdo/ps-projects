package com.stc.objects;

import java.util.HashMap;
import java.util.Map;

public class SoapEnvelopeObject {
    String header;
    String username;
    String password;
    String soapurl;
    Map<String,String> parameter = new HashMap<String,String>();
    String payload;

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSoapurl() {
        return soapurl;
    }

    public void setSoapurl(String soapurl) {
        this.soapurl = soapurl;
    }

    public Map<String, String> getParameter() {
        return parameter;
    }

    public void setParameter(Map<String, String> parameter) {
        this.parameter = parameter;
    }
}




