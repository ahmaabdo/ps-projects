package com.stc.objects;

/**
 *
 * @author Anas Alghawi
 */
public class UserOrig {
    public String userOrigSystem;
    public String userOrigSystemId;

    public UserOrig(String userOrigSystem, String userOrigSystemId) {
        this.userOrigSystem = userOrigSystem;
        this.userOrigSystemId = userOrigSystemId;
    }

    public String getUserOrigSystem() {
        return userOrigSystem;
    }

    public void setUserOrigSystem(String userOrigSystem) {
        this.userOrigSystem = userOrigSystem;
    }

    public String getUserOrigSystemId() {
        return userOrigSystemId;
    }

    public void setUserOrigSystemId(String userOrigSystemId) {
        this.userOrigSystemId = userOrigSystemId;
    }
}
