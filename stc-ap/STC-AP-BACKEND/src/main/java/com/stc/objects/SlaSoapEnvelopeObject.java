/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.objects;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Anas Alghawi
 */
public class SlaSoapEnvelopeObject {

    String header;
    String username;
    String password;
    String soapurl;
    Map<String, String> congifItem1param = new HashMap<String, String>();
    Map<String, String> congifItem2param = new HashMap<String, String>();

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSoapurl() {
        return soapurl;
    }

    public void setSoapurl(String soapurl) {
        this.soapurl = soapurl;
    }

    public Map<String, String> getCongifItem1param() {
        return congifItem1param;
    }

    public void setCongifItem1param(Map<String, String> congifItem1param) {
        this.congifItem1param = congifItem1param;
    }

    public Map<String, String> getCongifItem2param() {
        return congifItem2param;
    }

    public void setCongifItem2param(Map<String, String> congifItem2param) {
        this.congifItem2param = congifItem2param;
    }

}
