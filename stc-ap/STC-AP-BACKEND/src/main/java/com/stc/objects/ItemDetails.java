package com.stc.objects;

/**
 *
 * @author Anas Alghawi
 */
public class ItemDetails {
    String itemType;
    String itemKey;
    int actId;

    public ItemDetails(String itemType, String itemKey, int actId) {
        this.itemType = itemType;
        this.itemKey = itemKey;
        this.actId = actId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemKey() {
        return itemKey;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public int getActId() {
        return actId;
    }

    public void setActId(int actId) {
        this.actId = actId;
    }
}
