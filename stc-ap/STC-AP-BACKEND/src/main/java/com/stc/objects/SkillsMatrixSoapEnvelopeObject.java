/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.objects;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class SkillsMatrixSoapEnvelopeObject {
    
    String header;
    String username;
    String password;
    String soapurl;
    Map<String, String> masterParmeters = new HashMap<String, String>();
    Map<String, String> detailsParmeters = new HashMap<String, String>();
    JSONObject jsonPayload = new JSONObject();
    String requestType;
    String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
  

   

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSoapurl() {
        return soapurl;
    }

    public void setSoapurl(String soapurl) {
        this.soapurl = soapurl;
    }

     public Map<String, String> getMasterParmeters() {
        return masterParmeters;
    }

    public void setMasterParmeters(Map<String, String> masterParmeters) {
        this.masterParmeters = masterParmeters;
    }

    public Map<String, String> getDetailsParmeters() {
        return detailsParmeters;
    }

    public void setDetailsParmeters(Map<String, String> detailsParmeters) {
        this.detailsParmeters = detailsParmeters;
    }

    public JSONObject getJsonPayload() {
        return jsonPayload;
    }

    public void setJsonPayload(JSONObject jsonPayload) {
        this.jsonPayload = jsonPayload;
    }
    
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
