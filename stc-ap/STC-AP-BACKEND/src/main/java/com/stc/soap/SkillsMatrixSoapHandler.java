/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.soap;

import com.google.gson.JsonObject;
import com.stc.objects.SkillsMatrixSoapEnvelopeObject;
import com.stc.objects.SlaSoapEnvelopeObject;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author Anas Alghawi
 */
public class SkillsMatrixSoapHandler {
    
        private void createSoapEnvelope(SOAPMessage soapMessage, SkillsMatrixSoapEnvelopeObject seo) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "wsu";
        String myNamespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        // SOAP Header
        SOAPHeader soapHeader = envelope.getHeader();
        soapHeader.addNamespaceDeclaration("ns1", seo.getHeader());

        // SOAP Security
        SOAPElement soapSecurity = soapHeader.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
        SOAPElement userToken = soapSecurity.addChildElement("UsernameToken", "wsse");

        userToken.addChildElement("Username", "wsse").addTextNode(seo.getUsername());
        userToken.addChildElement("Password", "wsse").addTextNode(seo.getPassword());

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        soapBody.addNamespaceDeclaration("ns2", seo.getSoapurl());
        SOAPElement inputParameters = soapBody.addChildElement("inputParameters", "ns2");
        SOAPElement masterParameters = inputParameters.addChildElement("P_MASTER","ns2");
        SOAPElement masterItemParameters = masterParameters.addChildElement("P_MASTER_ITEM","ns2");
        SOAPElement detailsParameters = inputParameters.addChildElement("P_DETAILS","ns2");
       
        
        JSONObject masterJson = seo.getJsonPayload();
        JSONArray detailsJson = masterJson.getJSONArray("SKILLS");
        
        if(seo.getRequestType() == "update"){
            masterItemParameters.addChildElement("SKM_ID","ns2").addTextNode("21"); 
        }
        masterItemParameters.addChildElement("PERSON_ID","ns2").addTextNode(seo.getUserId());
        masterItemParameters.addChildElement("PO_MATCHED","ns2").addTextNode("N");
        
         
        for(int i=0; i<detailsJson.length(); i++){             
             for(int j=0; j<detailsJson.getJSONObject(i).getJSONArray("SECTIONS").length(); j++){
             JSONObject json = detailsJson.getJSONObject(i);
             JSONArray jsonArray = json.getJSONArray("SECTIONS");
             SOAPElement detailsItemParameters = detailsParameters.addChildElement("P_DETAILS_ITEM","ns2");
             detailsItemParameters.addChildElement("SECTION_TYPE", "ns2").addTextNode(json.getString("SECTION_TYPE"));
             detailsItemParameters.addChildElement("SECTION_NAME", "ns2").addTextNode(jsonArray.getJSONObject(j).getString("SECTION_NAME"));
             detailsItemParameters.addChildElement("SECTION_VALUE", "ns2").addTextNode(jsonArray.getJSONObject(j).getString("SECTION_VALUE"));
             }
        }
        
        inputParameters.addChildElement("P_LANGUAGE","ns2").addTextNode("US");


    }

    public String callSoapWebService(String soapEndpointUrl, String soapAction, SkillsMatrixSoapEnvelopeObject seo) {
        String result = "";
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, seo), soapEndpointUrl);
            if (soapResponse != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);
                String finalString = new String(baos.toByteArray());

                JSONObject xmlJSONObj = XML.toJSONObject(finalString);

                String jsonPrettyPrintString = xmlJSONObj.toString(4);
                result = jsonPrettyPrintString;
                baos.close();
            } else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "complete");
                result = jsonObject.toString();
            }
            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return result;
    }

    private SOAPMessage createSOAPRequest(String soapAction, SkillsMatrixSoapEnvelopeObject seo) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, seo);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }
    
}
