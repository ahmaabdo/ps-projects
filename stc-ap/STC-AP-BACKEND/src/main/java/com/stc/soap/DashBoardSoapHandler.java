/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.soap;

import com.google.gson.JsonObject;
import com.stc.objects.SoapEnvelopeObject;
import java.io.ByteArrayOutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author Anas Alghawi
 */
public class DashBoardSoapHandler {

    private void createSoapEnvelope(SOAPMessage soapMessage, SoapEnvelopeObject seo) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "wsu";
        String myNamespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        // SOAP Header
        SOAPHeader soapHeader = envelope.getHeader();
        soapHeader.addNamespaceDeclaration("ns1", seo.getHeader());

        // SOAP Security
        SOAPElement soapSecurity = soapHeader.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
        SOAPElement userToken = soapSecurity.addChildElement("UsernameToken", "wsse");

        userToken.addChildElement("Username", "wsse").addTextNode(seo.getUsername());
        userToken.addChildElement("Password", "wsse").addTextNode(seo.getPassword());

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        soapBody.addNamespaceDeclaration("ns2", seo.getSoapurl());
        SOAPElement inputParameters = soapBody.addChildElement("inputParameters", "ns2");
        SOAPElement actionDetails = inputParameters.addChildElement("P_ACTION_DETAILS", "ns2");

        JSONArray payloadArray = new JSONArray(seo.getPayload());
        System.out.println("payloadArray :\n" + payloadArray);
        for (int i = 0; i < payloadArray.length(); i++) {
            SOAPElement actionDetailsItem = actionDetails.addChildElement("P_ACTION_DETAILS_ITEM", "ns2");
            JSONObject json = payloadArray.getJSONObject(i);

//            actionDetailsItem.addChildElement("PERSON_ID", "ns2").addTextNode(json.getString("PERSON_ID"));
             if (json.has("PERSON_ID")) {
                actionDetailsItem.addChildElement("PERSON_ID", "ns2").addTextNode(json.getString("PERSON_ID"));
            } else {
                actionDetailsItem.addChildElement("PERSON_ID", "ns2").addTextNode("1");
            }
            actionDetailsItem.addChildElement("INVOICE_ID", "ns2").addTextNode(json.getString("INVOICE_ID"));

            if (json.has("ACCOUNTANT_INVOICE_ID")) {
                if (json.isNull("ACCOUNTANT_INVOICE_ID")) {
                    actionDetailsItem.addChildElement("ACCOUNTANT_INVOICE_ID", "ns2").addTextNode("1");
                } else {
                    actionDetailsItem.addChildElement("ACCOUNTANT_INVOICE_ID", "ns2").addTextNode(json.getString("ACCOUNTANT_INVOICE_ID"));
                }
            } else {
                actionDetailsItem.addChildElement("ACCOUNTANT_INVOICE_ID", "ns2").addTextNode("1");
            }
            if (json.has("ITEM_KEY")) {
                actionDetailsItem.addChildElement("ITEM_KEY", "ns2").addTextNode(json.getString("ITEM_KEY"));
            } else {
                 if (json.has("ItemKey")) {
                actionDetailsItem.addChildElement("ITEM_KEY", "ns2").addTextNode(json.getString("ItemKey"));
            }else{
                actionDetailsItem.addChildElement("ITEM_KEY", "ns2").addTextNode("1");
                 }
            }
            if (json.has("NOTE")) {
                if (json.isNull("NOTE")) {
                    actionDetailsItem.addChildElement("NOTE", "ns2").addTextNode("null");
                }else{
                    actionDetailsItem.addChildElement("NOTE", "ns2").addTextNode(json.getString("NOTE"));
                }       
            } else {
                actionDetailsItem.addChildElement("NOTE", "ns2").addTextNode("1");
            }

        }

    }

    public String callSoapWebService(String soapEndpointUrl, String soapAction, SoapEnvelopeObject seo) {
        String result = "";
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, seo), soapEndpointUrl);
            if (soapResponse != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);
                String finalString = new String(baos.toByteArray());

                JSONObject xmlJSONObj = XML.toJSONObject(finalString);

                String jsonPrettyPrintString = xmlJSONObj.toString(4);
                result = jsonPrettyPrintString;
                baos.close();
            } else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "complete");
                result = jsonObject.toString();
            }
            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        System.out.println("result : \n" + result);
        return result;
    }

    public SOAPMessage createSOAPRequest(String soapAction, SoapEnvelopeObject seo) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, seo);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }

    public String callSoapWebServiceJSON(String soapEndpointUrl, String soapAction, SoapEnvelopeObject seo, String objectName) {

        String result = "";

        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, seo), soapEndpointUrl);

            if (soapResponse != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);

                String finalString = new String(baos.toByteArray(), "UTF-8");
                JSONObject xmlJSONObj = XML.toJSONObject(finalString);

//                System.out.println(xmlJSONObj);
                JSONObject outputParam = xmlJSONObj.getJSONObject("env:Envelope")
                        .getJSONObject("env:Body").getJSONObject("OutputParameters");

                JSONArray jsonArray = new JSONArray();

                if (outputParam.toString().contains("Row")) {
                    Object rowObj = outputParam.getJSONObject("EXECUTE_QUERY").get("Row");

                    if (rowObj instanceof JSONArray) {
                        JSONArray row = outputParam.getJSONObject("EXECUTE_QUERY").getJSONArray("Row");
                        for (int i = 0; i < row.length(); i++) {
                            JSONObject jsonObject = new JSONObject();
                            JSONArray column = row.getJSONObject(i).getJSONArray("Column");
                            for (int j = 0; j < column.length(); j++) {
                                String name = column.getJSONObject(j).getString("name");
                                String content = null;
                                if (column.getJSONObject(j).has("content")) {
                                    content = column.getJSONObject(j).get("content").toString();
                                    jsonObject.put(name, content);
                                } else {
                                    jsonObject.put(name, JSONObject.NULL);
                                }
                            }
                            jsonArray.put(jsonObject);
                        }
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(objectName, jsonArray);

                        String jsonPrettyPrintString = jsonObject.toString(4);
                        result = jsonPrettyPrintString;
                    } else if (rowObj instanceof JSONObject) {
                        JSONObject row = outputParam.getJSONObject("EXECUTE_QUERY").getJSONObject("Row");

                        JSONObject jsonObject = new JSONObject();

//                        
                        Object colojb = row.get("Column");
                        if (colojb instanceof JSONObject) {
                            JSONObject column = row.getJSONObject("Column");
                            String name = column.getString("name");
                            String content = null;
                            if (column.has("content")) {
                                content = column.get("content").toString();
                                jsonObject.put(name, content);
                            } else {
                                jsonObject.put(name, JSONObject.NULL);
                            }

                            jsonArray.put(jsonObject);

                            JSONObject jsonResult = new JSONObject();
                            jsonResult.put(objectName, jsonArray);

                            String jsonPrettyPrintString = jsonResult.toString(4);
                            result = jsonPrettyPrintString;

                        } else {

                            JSONArray column = row.getJSONArray("Column");
                            for (int j = 0; j < column.length(); j++) {
                                String name = column.getJSONObject(j).getString("name");
                                String content = null;
                                if (column.getJSONObject(j).has("content")) {
                                    content = column.getJSONObject(j).get("content").toString();
                                    jsonObject.put(name, content);
                                } else {
                                    jsonObject.put(name, JSONObject.NULL);
                                }
                            }
                            jsonArray.put(jsonObject);

                            JSONObject jsonResult = new JSONObject();
                            jsonResult.put(objectName, jsonArray);

                            String jsonPrettyPrintString = jsonResult.toString(4);
                            result = jsonPrettyPrintString;
                        }

                    }

                } //for remove material service
                else if (outputParam.toString().contains("P_STATUS")) {
//                    JSONObject status = outputParam.getJSONObject("P_STATUS");
                    JSONObject jsonObject = new JSONObject();
                    String content = null;

                    if (outputParam.getString("P_STATUS").isEmpty()) {
                        jsonObject.put("P_STATUS", JSONObject.NULL);
                    } else {
                        jsonObject.put("P_STATUS", outputParam.getString("P_STATUS"));
                    }

                    jsonArray.put(jsonObject);

                    JSONObject jsonResult = new JSONObject();
                    jsonResult.put(objectName, jsonArray);

                    String jsonPrettyPrintString = jsonResult.toString(4);
                    result = jsonPrettyPrintString;

                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(objectName, jsonArray);
                    result = jsonObject.toString();
                }
                baos.close();
            } else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "complete");
                result = jsonObject.toString();
            }
//            System.out.println(result);
            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return result;
    }

}
