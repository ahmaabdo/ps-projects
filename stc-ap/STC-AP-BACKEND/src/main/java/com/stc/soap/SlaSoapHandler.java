/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.soap;

import com.google.gson.JsonObject;
import com.stc.objects.SlaSoapEnvelopeObject;
import com.stc.objects.SoapEnvelopeObject;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author Anas Alghawi
 */
public class SlaSoapHandler {

    private void createSoapEnvelope(SOAPMessage soapMessage, SlaSoapEnvelopeObject seo) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "wsu";
        String myNamespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        // SOAP Header
        SOAPHeader soapHeader = envelope.getHeader();
        soapHeader.addNamespaceDeclaration("ns1", seo.getHeader());

        // SOAP Security
        SOAPElement soapSecurity = soapHeader.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
        SOAPElement userToken = soapSecurity.addChildElement("UsernameToken", "wsse");

        userToken.addChildElement("Username", "wsse").addTextNode(seo.getUsername());
        userToken.addChildElement("Password", "wsse").addTextNode(seo.getPassword());

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        soapBody.addNamespaceDeclaration("ns2", seo.getSoapurl());
        SOAPElement inputParameters = soapBody.addChildElement("inputParameters", "ns2");
        SOAPElement p_config = inputParameters.addChildElement("P_CONFIG", "ns2");

        if (seo.getCongifItem1param().size() > 0) {
            SOAPElement p_config1 = inputParameters.addChildElement("P_CONFIG_ITEM", "ns2");
            p_config1.addChildElement("CONF_TYPE", "ns2").addTextNode(seo.getCongifItem1param().get("CONF_TYPE").toString());
            p_config1.addChildElement("CONF_NAME", "ns2").addTextNode(seo.getCongifItem1param().get("CONF_NAME").toString());
            p_config1.addChildElement("CONF_VALUE", "ns2").addTextNode(seo.getCongifItem1param().get("CONF_VALUE").toString());
            p_config1.addChildElement("CONF_LANG", "ns2").addTextNode(seo.getCongifItem1param().get("CONF_LANG").toString());
            p_config1.addChildElement("CONF_PROMPT", "ns2").addTextNode(seo.getCongifItem1param().get("CONF_PROMPT").toString());
            p_config.addChildElement(p_config1);
        }      
        if (seo.getCongifItem2param().size() > 0) {
            SOAPElement p_config2 = inputParameters.addChildElement("P_CONFIG_ITEM", "ns2");
            p_config2.addChildElement("CONF_TYPE", "ns2").addTextNode(seo.getCongifItem2param().get("CONF_TYPE").toString());
            p_config2.addChildElement("CONF_NAME", "ns2").addTextNode(seo.getCongifItem2param().get("CONF_NAME").toString());
            p_config2.addChildElement("CONF_VALUE", "ns2").addTextNode(seo.getCongifItem2param().get("CONF_VALUE").toString());
            p_config2.addChildElement("CONF_LANG", "ns2").addTextNode(seo.getCongifItem2param().get("CONF_LANG").toString());
            p_config2.addChildElement("CONF_PROMPT", "ns2").addTextNode(seo.getCongifItem2param().get("CONF_PROMPT").toString());
            p_config.addChildElement(p_config2);
        }
    }

    public String callSoapWebService(String soapEndpointUrl, String soapAction, SlaSoapEnvelopeObject seo) {
        String result = "";
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, seo), soapEndpointUrl);
            if (soapResponse != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);
                String finalString = new String(baos.toByteArray());

                JSONObject xmlJSONObj = XML.toJSONObject(finalString);

                String jsonPrettyPrintString = xmlJSONObj.toString(4);
                result = jsonPrettyPrintString;
                baos.close();
            } else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "complete");
                result = jsonObject.toString();
            }
            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return result;
    }

    private SOAPMessage createSOAPRequest(String soapAction, SlaSoapEnvelopeObject seo) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, seo);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }

}
