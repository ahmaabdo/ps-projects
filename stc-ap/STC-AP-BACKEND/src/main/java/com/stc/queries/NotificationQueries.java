/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.queries;

/**
 *
 * @author Anas Alghawi
 */
public class NotificationQueries {
    
    public static final String GET_NOTIFICATION_HEADER(String personId) {

        return "SELECT NOTIFICATION_ID ,SUBJECT FROM XXAPRG_NOTIFICATIONS WHERE EMPLOYEE_ID = "+ personId +" ORDER BY BEGIN_DATE";
    }
    public static final String GET_NOTIFICATION_DETAILS(String notificationId) {

        return "SELECT * FROM XXAPRG_NOTIFICATIONS WHERE NOTIFICATION_ID = "+ notificationId + " ORDER BY BEGIN_DATE";
    }
    
}
