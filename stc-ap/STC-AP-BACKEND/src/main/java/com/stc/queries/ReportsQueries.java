/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.queries;

/**
 *
 * @author Anas Alghawi
 */
public class ReportsQueries {
    
    public static final String GET_TOP_INVOICES_VENDOR(String fromDate, String toDate) {

        return "SELECT VENDOR_NUM ,VENDOR_NAME ,COUNT(1) NUMBER_INVOICES FROM XXAPRG_INVOICES_V INV WHERE INV.INVOICE_DATE BETWEEN '"+ fromDate +"' AND '"+ toDate +"' GROUP BY VENDOR_NUM,VENDOR_NAME ORDER BY 3 DESC";
    }
    public static final String GET_TOTAL_INVOICE_SPENT_BY_EXPESNE_TYPE(String fromDate, String toDate) {

        return "SELECT nvl(EXPENSE_TYPE,'No Expense Type')EXPENSE_TYPE ,COUNT(1) RECIVERD,SUM(DUE) DUE, SUM(DELAYED)DELAYED,SUM(OVERDUE)OVERDUE FROM ( SELECT EXPENSE_TYPE_DESC EXPENSE_TYPE, NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'APPROVED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID and trunc(END_DATE) <= (select trunc(due_date) from xxaprg_invoices_v invin where invin.invoice_id = inv.INVOICE_ID)), 0 ) due, NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'APPROVED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID and trunc(END_DATE) > (select trunc(due_date) from xxaprg_invoices_v invin where invin.invoice_id = inv.INVOICE_ID)), 0 ) delayed , NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'REJECTED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID ), 0 ) OVERDUE FROM XXAPRG_ACCOUNTANT_INVOICES inv, XXAPRG_INVOICES_V invo WHERE inv.INVOICE_ID = invo.INVOICE_ID and ACCOUNTANT_INVOICE_ID IN (( SELECT MAX (ACCOUNTANT_INVOICE_ID) FROM XXAPRG_ACCOUNTANT_INVOICES WHERE TRUNC(START_DATE) BETWEEN '"+fromDate+ "' AND '"+ toDate +"' AND EXISTS (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACCOUNTANT_INVOICE_ID = ACC_INVOICE_ACTION_ID) GROUP BY INVOICE_ID)) ) GROUP BY EXPENSE_TYPE ORDER BY 1,2";
    }
    public static final String GET_TOTAL_INVOICE_SPENT_BY_MONTH(String fromDate, String toDate) {

        return "SELECT YEAR,MONTH,COUNT(1) RECIVERD,SUM(DUE) DUE, SUM(DELAYED)DELAYED,SUM(OVERDUE)OVERDUE FROM ( SELECT TO_CHAR (END_DATE, 'RRRR') YEAR, TO_CHAR (END_DATE, 'MONTH') MONTH, NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'APPROVED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID and trunc(END_DATE) <= (select trunc(due_date) from xxaprg_invoices_v invin where invin.invoice_id = inv.INVOICE_ID)), 0 ) due, NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'APPROVED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID and trunc(END_DATE) > (select trunc(due_date) from xxaprg_invoices_v invin where invin.invoice_id = inv.INVOICE_ID)), 0 ) delayed , NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'REJECTED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID ), 0 ) OVERDUE FROM XXAPRG_ACCOUNTANT_INVOICES inv WHERE ACCOUNTANT_INVOICE_ID IN (( SELECT MAX (ACCOUNTANT_INVOICE_ID) FROM XXAPRG_ACCOUNTANT_INVOICES WHERE TRUNC(START_DATE) BETWEEN '"+ fromDate +"' AND '"+ toDate +"' AND EXISTS (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACCOUNTANT_INVOICE_ID = ACC_INVOICE_ACTION_ID) GROUP BY INVOICE_ID)) ) GROUP BY YEAR,MONTH ORDER BY 1,2";
    }
    public static final String GET_TOTAL_INVOICE_SPENT_BY_SOURCE(String fromDate, String toDate) {

        return "SELECT SOURCE ,COUNT(1) RECIVERD,SUM(DUE) DUE, SUM(DELAYED)DELAYED,SUM(OVERDUE)OVERDUE FROM ( SELECT SOURCE, NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'APPROVED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID and trunc(END_DATE) <= (select trunc(due_date) from xxaprg_invoices_v invin where invin.invoice_id = inv.INVOICE_ID)), 0 ) due, NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'APPROVED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID and trunc(END_DATE) > (select trunc(due_date) from xxaprg_invoices_v invin where invin.invoice_id = inv.INVOICE_ID)), 0 ) delayed , NVL ( (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACTION_CODE = 'REJECTED' AND ACC_INVOICE_ACTION_ID = ACCOUNTANT_INVOICE_ID ), 0 ) OVERDUE FROM XXAPRG_ACCOUNTANT_INVOICES inv, XXAPRG_INVOICES_V invo WHERE inv.INVOICE_ID = invo.INVOICE_ID and ACCOUNTANT_INVOICE_ID IN (( SELECT MAX (ACCOUNTANT_INVOICE_ID) FROM XXAPRG_ACCOUNTANT_INVOICES WHERE TRUNC(START_DATE) BETWEEN '"+fromDate+"' AND '"+toDate+"' AND EXISTS (SELECT 1 FROM XXAPRG_ACC_INVOICE_ACTIONS_V WHERE ACCOUNTANT_INVOICE_ID = ACC_INVOICE_ACTION_ID) GROUP BY INVOICE_ID)) ) GROUP BY SOURCE ORDER BY 1,2;";
    }
}
