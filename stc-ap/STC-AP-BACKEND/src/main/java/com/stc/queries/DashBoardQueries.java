/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.queries;

/**
 *
 * @author Anas Alghawi
 */
public class DashBoardQueries {

    public static final String GET_ACT_DASHBOARD(String personId, String lang) {

        return "SELECT  inv.invoice_id as \"InvoiceId\", inv.invoice_num as \"InvoiceName\", to_char(inv.invoice_date, 'DD-MON-YYYY') as \"InvoiceDate\", inv.invoice_amount as \"Amount\", inv.batch_name as \"BatchName\", inv.vendor_id as \"VendorNo\", inv.vendor_name as \"VendorName\", inv.invoice_currency_code as \"Currency\", inv.source as \"Source\", inv.po_header_id as \"PONumber\", inv.vendor_type_lookup_code as \"SupplierType\", inv.attribute2 as \"TechnicalSector\", inv.EXPENSE_TYPE_DESC as \"ExpenseType\", inv.accountant_invoice_id as \"accountant\", CASE WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED' ELSE 'PENDING' END AS \"STATUS\", inv.accountant_invoice_id as \"accountantId\", NULL as \"supervisorId\", inv.priority as \"Priority\", inv.DAY_REMAIN \"remainingDays\", inv.SLA_DAY_REMAIN \"SlaRemainingDays\", inv.item_key AS \"itemKey\",inv.person_id AS \"pesronId\",  INV.EMPLOYEE_WORK_LOAD AS \"WORKLOAD\", NVL (inv.EXCHANGE_RATE, 1) as \"EXCHANGE_RATE\", inv.DOC_SEQUENCE_VALUE as \"SEQUANCE\", inv.DISTRICT_DESC AS \"DISTRICT\", inv.ATTRIBUTE3 AS \"INITIALINVOICE\", inv.ATTRIBUTE4 AS \"SERVICETYPE\", inv.ATTRIBUTE5 AS \"SERVICEPERIOD\", inv.ATTRIBUTE6 AS \"CONVERSIONRATE\", inv.ATTRIBUTE7 AS \"RATEDATE\", inv.ATTRIBUTE9 AS \"DEDUCT_TYPE\", inv.ATTRIBUTE10 AS \"PORELEASE\", to_char(inv.DUE_DATE, 'DD-MON-YYYY') AS \"DUE_DATE\", inv.VENDOR_NUM AS \"VENDOR_NUM\" FROM XXAPRG_Account_invoice_details inv WHERE inv.person_id = " + personId;
    }

    public static final String GET_FOCAL_DASHBOARD(String lang) {

        return "SELECT inv.invoice_id AS \"InvoiceId\", inv.invoice_num AS \"InvoiceName\", to_char(inv.invoice_date, 'DD-MON-YYYY') AS \"InvoiceDate\", inv.invoice_amount AS \"Amount\", inv.batch_name AS \"BatchName\", inv.vendor_id AS \"VendorNo\", inv.vendor_name AS \"VendorName\", inv.invoice_currency_code AS \"Currency\", inv.source AS \"Source\", inv.po_header_id AS \"PONumber\", inv.vendor_type_lookup_code AS \"SupplierType\", inv.attribute2 AS \"TechnicalSector\", inv.EXPENSE_TYPE_DESC AS \"ExpenseType\", inv.accountant_invoice_id AS \"accountant\", CASE WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED' ELSE 'PENDING' END AS \"STATUS\", inv.accountant_invoice_id AS \"accountantId\", NULL AS \"supervisorId\", inv.priority AS \"Priority\", inv.day_remain \"remainingDays\", inv.item_key AS \"itemKey\", inv.person_id AS \"pesronId\", inv.person_name AS \"personName\",  INV.EMPLOYEE_WORK_LOAD AS \"WORKLOAD\", NVL (inv.EXCHANGE_RATE, 1) as \"EXCHANGE_RATE\", inv.DOC_SEQUENCE_VALUE as \"SEQUANCE\", inv.DISTRICT_DESC AS \"DISTRICT\", inv.ATTRIBUTE3 AS \"INITIALINVOICE\", inv.ATTRIBUTE4 AS \"SERVICETYPE\", inv.ATTRIBUTE5 AS \"SERVICEPERIOD\", inv.ATTRIBUTE6 AS \"CONVERSIONRATE\", inv.ATTRIBUTE7 AS \"RATEDATE\", inv.ATTRIBUTE9 AS \"DEDUCT_TYPE\", inv.ATTRIBUTE10 AS \"PORELEASE\", to_char(inv.DUE_DATE, 'DD-MON-YYYY') AS \"DUE_DATE\", inv.VENDOR_NUM AS \"VENDOR_NUM\" FROM xxaprg_dashboard_invoices_v inv";
    }

    public static final String GET_COMPLETED_DASHBOARD(String userId) {

        return "SELECT\n"
                + "    inv.invoice_id                AS \"InvoiceId\",\n"
                + "    inv.invoice_num               AS \"InvoiceName\",\n"
                + "    to_char(inv.invoice_date, 'DD-MON-YYYY')              AS \"InvoiceDate\",\n"
                + "    inv.invoice_amount            AS \"Amount\",\n"
                + "    inv.batch_name                 AS \"BatchName\",\n"
                + "    inv.vendor_id                 AS \"VendorNo\",\n"
                + "    inv.vendor_name               AS \"VendorName\",\n"
                + "    inv.invoice_currency_code     AS \"Currency\",\n"
                + "    inv.source                    AS \"Source\",\n"
                + "    inv.po_header_id              AS \"PONumber\",\n"
                + "    inv.vendor_type_lookup_code   AS \"SupplierType\",\n"
                + "    inv.attribute2                AS \"TechnicalSector\",\n"
                + "    inv.EXPENSE_TYPE_DESC               AS \"ExpenseType\",\n"
                + "    inv.accountant_invoice_id     AS \"accountant\",\n"
                + "    CASE\n"
                + "        WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED'\n"
                + "        WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED'\n"
                + "        WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED'\n"
                + "        WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED'\n"
                + "        WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED'\n"
                + "        WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED'\n"
                + "        ELSE 'PENDING'\n"
                + "    END AS \"STATUS\",\n"
                + "    inv.accountant_invoice_id     AS \"accountantId\",\n"
                + "    NULL AS \"supervisorId\",\n"
                + "    inv.item_key                  AS \"itemKey\",\n"
                + "    inv.person_id                 AS \"pesronId\",\n"
                + "    inv.action_code               AS \"ACTION\",\n"
                + "    inv.comments                  AS \"COMMENTS\",\n"
+"       inv.item_key                   AS \"itemKey\",\n" +
"       inv.person_id                  AS \"pesronId\",\n" +
"       NVL (inv.EXCHANGE_RATE, 1)     as \"EXCHANGE_RATE\", \n" +
"       inv.DOC_SEQUENCE_VALUE         as \"SEQUANCE\",\n" +
"       inv.DISTRICT_DESC               AS \"DISTRICT\",\n" +
"       inv.ATTRIBUTE3                 AS \"INITIALINVOICE\",\n" +
"       inv.ATTRIBUTE4                 AS \"SERVICETYPE\",\n" +
"       inv.ATTRIBUTE5                 AS \"SERVICEPERIOD\",\n" +
"       inv.ATTRIBUTE6                 AS \"CONVERSIONRATE\",\n" +
"       inv.ATTRIBUTE7                 AS \"RATEDATE\", \n" +
"       inv.ATTRIBUTE9                 AS \"DEDUCT_TYPE\",\n" +
"       inv.ATTRIBUTE10                AS \"PORELEASE\",\n" +
"       inv.DUE_DATE                   AS \"DUE_DATE\",                  \n" +
"       inv.VENDOR_NUM                 AS \"VENDOR_NUM\""
                + "FROM\n"
                + "    xxaprg_complete_invoices_v inv\n"
                + "WHERE\n"
                + "    trunc(action_end_date) BETWEEN trunc(SYSDATE - 7) AND trunc(SYSDATE)\n"
                + "    AND person_id = '" + userId + "'\n"
                + "ORDER BY\n"
                + "    action_end_date";
    }

    public static final String GET_REJECTION_HEADER_LOV = "SELECT ffv.flex_value code, ffv.description FROM fnd_flex_values_vl ffv, fnd_flex_value_sets fvs WHERE fvs.flex_value_set_name = 'AP_RE_ENG_INVOICE_H' AND ffv.flex_value_set_id = fvs.flex_value_set_id";
    public static final String GET_REJECTION_DETAILS_LOV (String parentCode){ 
             return "SELECT FFV.PARENT_FLEX_VALUE_LOW PARINT_CODE,FFV.FLEX_VALUE CODE,FFV.DESCRIPTION \n"
            + "   FROM FND_FLEX_VALUES_VL FFV, FND_FLEX_VALUE_SETS FVS\n"
            + "  WHERE FVS.FLEX_VALUE_SET_NAME =\n"
            + "        (SELECT FLEX_VALUE_SET_NAME\n"
            + "           FROM FND_FLEX_VALUE_SETS\n"
            + "          WHERE PARENT_FLEX_VALUE_SET_ID =\n"
            + "                (SELECT FLEX_VALUE_SET_ID\n"
            + "                   FROM FND_FLEX_VALUE_SETS\n"
            + "                  WHERE FLEX_VALUE_SET_NAME = 'AP_RE_ENG_INVOICE_H'))\n"
            + "    AND FFV.FLEX_VALUE_SET_ID = FVS.FLEX_VALUE_SET_ID\n"
            + "    and  FFV.PARENT_FLEX_VALUE_LOW = '" + parentCode + "'";
    }
}
