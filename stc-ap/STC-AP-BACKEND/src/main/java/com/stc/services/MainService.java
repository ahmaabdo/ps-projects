package com.stc.services;

import com.stc.controller.MainController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */

//localhost:8080/ap_integration/mainservice/testSysdate
@Path("/mainservice")
public class MainService {

    @GET
    @Path("testSysdate")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response testSysdate() {


       
            String result;
            MainController mainController = new MainController();
            result = "";

            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            else {
                return Response
                        .status(200)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        
    }
    
    @POST
    @Path("validate-login")
    @Consumes("application/json")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response validateLogin(String payload) {

        String result;
        MainController mainController = new MainController();
        result = mainController.validateLogin(payload);

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }

    }
    
    @GET
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    @Consumes("application/json")
    @Path("get-users")
    public Response getEmployees() {
        String result;

            MainController SkillsMatrixController = new MainController();
            result = SkillsMatrixController.getEmployees();
            if (result.isEmpty()) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                return Response
                        .status(Response.Status.OK)
                        .entity(result)
                        .type(MediaType.APPLICATION_JSON)
                        .build();
            }
        
    }
  
}
