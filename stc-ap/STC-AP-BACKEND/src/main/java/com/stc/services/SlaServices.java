/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.google.gson.JsonObject;
import com.stc.bean.SlaBean;
import com.stc.controller.SlaController;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Anas Alghawi
 */
@Path("/sla")
public class SlaServices {

    SlaController slaController = new SlaController();

    @GET
    @Path("get-sla")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getSla() {
        String result;
        Status statusCode;

        result = "{\n"
                + "    \"invoiceReview\": \"15-Sep-2019\",\n"
                + "    \"supervisorReview\": \"15-Sep-2019\"\n"
                + "}";

        if (StringUtils.isEmpty(result)) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            statusCode = Response.Status.OK;
        }

        return Response
                .status(statusCode)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    @POST
    @Path("update-sla")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    @Consumes("application/json")
    public Response updateSla(SlaBean jsonPayload) {
        String result;
        Status statusCode;
        SlaBean jsonData = new SlaBean();
        jsonData = jsonPayload;

        if (StringUtils.isEmpty(jsonData.getInvoiceReview()) && StringUtils.isEmpty(jsonData.getSupervisorReview())) {

            result = "Empty Mandatory Value";
            statusCode = Response.Status.BAD_REQUEST;
            return Response
                    .status(statusCode)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        } else {

//            result = slaController.updateSla(jsonData.getInvoiceReview(), jsonData.getSupervisorReview());
            result = "S";

            if (StringUtils.isEmpty(result)) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

            } else {

                statusCode = Response.Status.OK;
            }
        }
        return Response
                .status(statusCode)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
