/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.MainController;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author Anas Alghawi
 */
@Path("/reports")
public class ReportsService {

    MainController mainController = new MainController();

    @GET
    @Path("{reportName}")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getTotalInvoiceReport(@PathParam("reportName") String reportName,
            @QueryParam("fromDate") String fromDate,
            @QueryParam("toDate") String toDate) {

        String result;

            if (fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()) {

                
                result = "  [\n"
                        + "  	{\n"
                        + "  		\"id\": 0,\n"
                        + "  		\"company\": \"Apple\",\n"
                        + "  		\"year\": \"2012\",\n"
                        + "  		\"value\": 42\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 1,\n"
                        + "  		\"company\": \"Apple\",\n"
                        + "  		\"year\": \"2013\",\n"
                        + "  		\"value\": 34\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 2,\n"
                        + "  		\"company\": \"Apple\",\n"
                        + "  		\"year\": \"2014\",\n"
                        + "  		\"value\": 42\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 3,\n"
                        + "  		\"company\": \"Apple\",\n"
                        + "  		\"year\": \"2015\",\n"
                        + "  		\"value\": 34\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 4,\n"
                        + "  		\"company\": \"Google\",\n"
                        + "  		\"year\": \"2012\",\n"
                        + "  		\"value\": 55\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 5,\n"
                        + "  		\"company\": \"Google\",\n"
                        + "  		\"year\": \"2013\",\n"
                        + "  		\"value\": 30\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 6,\n"
                        + "  		\"company\": \"Google\",\n"
                        + "  		\"year\": \"2014\",\n"
                        + "  		\"value\": 55\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 7,\n"
                        + "  		\"company\": \"Google\",\n"
                        + "  		\"year\": \"2015\",\n"
                        + "  		\"value\": 30\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 8,\n"
                        + "  		\"company\": \"Microsoft\",\n"
                        + "  		\"year\": \"2012\",\n"
                        + "  		\"value\": 36\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 9,\n"
                        + "  		\"company\": \"Microsoft\",\n"
                        + "  		\"year\": \"2013\",\n"
                        + "  		\"value\": 50\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 10,\n"
                        + "  		\"company\": \"Microsoft\",\n"
                        + "  		\"year\": \"2014\",\n"
                        + "  		\"value\": 36\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 11,\n"
                        + "  		\"company\": \"Microsoft\",\n"
                        + "  		\"year\": \"2015\",\n"
                        + "  		\"value\": 50\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 12,\n"
                        + "  		\"company\": \"Yahoo\",\n"
                        + "  		\"year\": \"2012\",\n"
                        + "  		\"value\": 22\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 13,\n"
                        + "  		\"company\": \"Yahoo\",\n"
                        + "  		\"year\": \"2013\",\n"
                        + "  		\"value\": 22\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 14,\n"
                        + "  		\"company\": \"Yahoo\",\n"
                        + "  		\"year\": \"2014\",\n"
                        + "  		\"value\": 22\n"
                        + "  	},\n"
                        + "  	{\n"
                        + "  		\"id\": 15,\n"
                        + "  		\"company\": \"Yahoo\",\n"
                        + "  		\"year\": \"2015\",\n"
                        + "  		\"value\": 22\n"
                        + "  	}\n"
                        + "  ]";

                if (result.isEmpty()) {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                } else {
                    return Response
                            .status(200)
                            .entity(result)
                            .type(MediaType.APPLICATION_JSON)
                            .build();
                }
            } else {

                result = "Please enter fromDate, toDate values";
            }
     
        return Response
                .status(400)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
