/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.AccountsController;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Anas Alghawi
 */
@Path("/accounts")
public class AccountsService {

    @GET
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    @Consumes("application/json")
    @Path("defined")
    public Response getDefinedAccounts() {
        String result;

        AccountsController accountsController = new AccountsController();
        result = accountsController.getDefinedAccounts();
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }

    }
    
    @GET
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    @Consumes("application/json")
    @Path("undefined")
    public Response getUndefinedAccounts() {
        String result;

        AccountsController accountsController = new AccountsController();
        result = accountsController.getUndefinedAccounts();
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }

    }
    
    @GET
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    @Consumes("application/json")
    @Path("account-roles")
    public Response getUserRoleType() {
        String result;

        AccountsController accountsController = new AccountsController();
        result = accountsController.getUserRoleType();
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }

    }

}
