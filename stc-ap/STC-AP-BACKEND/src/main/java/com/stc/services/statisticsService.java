/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Anas Alghawi
 */
@Path("/statistics")
public class statisticsService {

    @GET
    @Path("achievers_of_the_week")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getAchieversOfTheWweek() {
        String result;
        result = "[\n"
                + "                  {  \n"
                + "                      \"EMPLOYEE_ID\":\"40\",\n"
                + "                      \"FIRST_NAME\":\"Bob\",\n"
                + "                      \"LAST_NAME\":\"Jones\",\n"
                + "                      \"EFFICIENCY\":\"80\",\n"
                + "                      \"COMPLETION_RATE\":\"70\",\n"
                + "                      \"PROCESSING_IN_TIME\":\"30\",\n"
                + "                      \"IMG\":\"\"\n"
                + "				\n"
                + "                   },\n"
                + "                   {  \n"
                + "                      \"EMPLOYEE_ID\":\"50\",\n"
                + "                      \"FIRST_NAME\":\"Bart\",\n"
                + "                      \"LAST_NAME\":\"Buckler\",\n"
                + "                      \"EFFICIENCY\":\"98\",\n"
                + "                      \"COMPLETION_RATE\":\"90\",\n"
                + "                      \"PROCESSING_IN_TIME\":\"10\",\n"
                + "			 \"IMG\":\"\"\n"
                + "                   },\n"
                + "                   {  \n"
                + "                      \"EMPLOYEE_ID\":\"60\",\n"
                + "                      \"FIRST_NAME\":\"Bobby\",\n"
                + "                      \"LAST_NAME\":\"Fisher\",\n"
                + "                      \"EFFICIENCY\":\"90\",\n"
                + "                      \"COMPLETION_RATE\":\"80\",\n"
                + "                      \"PROCESSING_IN_TIME\":\"20\",\n"
                + "                      \"IMG\":\"\"\n"
                + "                   }\n"
                + "                ]";

        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("leaderboard")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getLeaderboard() {
        String result;
        result = "[\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 5,\n"
                + "        \"FIRST_NAME\": \"Amy\",\n"
                + "        \"LAST_NAME\": \"Bartlet\",\n"
                + "        \"EFFICIENCY\": 40,\n"
                + "        \"COMPLETION_RATE\": 20,\n"
                + "        \"PROCESSING_IN_TIME\": 80,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 10,\n"
                + "        \"FIRST_NAME\": \"Andy\",\n"
                + "        \"LAST_NAME\": \"Jones\",\n"
                + "        \"EFFICIENCY\": 60,\n"
                + "        \"COMPLETION_RATE\": 40,\n"
                + "        \"PROCESSING_IN_TIME\": 60,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 20,\n"
                + "        \"FIRST_NAME\": \"Andrew\",\n"
                + "        \"LAST_NAME\": \"Bugsy\",\n"
                + "        \"EFFICIENCY\": 20,\n"
                + "        \"COMPLETION_RATE\": 10,\n"
                + "        \"PROCESSING_IN_TIME\": 90,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 30,\n"
                + "        \"FIRST_NAME\": \"Annette\",\n"
                + "        \"LAST_NAME\": \"Barnes\",\n"
                + "        \"EFFICIENCY\": 50,\n"
                + "        \"COMPLETION_RATE\": 30,\n"
                + "        \"PROCESSING_IN_TIME\": 70,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 40,\n"
                + "        \"FIRST_NAME\": \"Tom\",\n"
                + "        \"LAST_NAME\": \"Rogger\",\n"
                + "        \"EFFICIENCY\": 50,\n"
                + "        \"COMPLETION_RATE\": 47,\n"
                + "        \"PROCESSING_IN_TIME\": 35,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 50,\n"
                + "        \"FIRST_NAME\": \"Bart\",\n"
                + "        \"LAST_NAME\": \"Buckler\",\n"
                + "        \"EFFICIENCY\": 98,\n"
                + "        \"COMPLETION_RATE\": 90,\n"
                + "        \"PROCESSING_IN_TIME\": 10,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 30,\n"
                + "        \"FIRST_NAME\": \"Annette\",\n"
                + "        \"LAST_NAME\": \"Barnes\",\n"
                + "        \"EFFICIENCY\": 50,\n"
                + "        \"COMPLETION_RATE\": 30,\n"
                + "        \"PROCESSING_IN_TIME\": 70,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 40,\n"
                + "        \"FIRST_NAME\": \"Bob\",\n"
                + "        \"LAST_NAME\": \"Jones\",\n"
                + "        \"EFFICIENCY\": 80,\n"
                + "        \"COMPLETION_RATE\": 70,\n"
                + "        \"PROCESSING_IN_TIME\": 30,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 50,\n"
                + "        \"FIRST_NAME\": \"Michel\",\n"
                + "        \"LAST_NAME\": \"Bob\",\n"
                + "        \"EFFICIENCY\": 38,\n"
                + "        \"COMPLETION_RATE\": 70,\n"
                + "        \"PROCESSING_IN_TIME\": 30,\n"
                + "        \"IMG\": \"\"\n"
                + "    },\n"
                + "    {\n"
                + "        \"EMPLOYEE_ID\": 60,\n"
                + "        \"FIRST_NAME\": \"Bobby\",\n"
                + "        \"LAST_NAME\": \"Fisher\",\n"
                + "        \"EFFICIENCY\": 90,\n"
                + "        \"COMPLETION_RATE\": 80,\n"
                + "        \"PROCESSING_IN_TIME\": 20,\n"
                + "        \"IMG\": \"\"\n"
                + "    }\n"
                + "]";
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GET
    @Path("invoices_overview")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response InvoicesOverview() {
        String result;
        result = "{  \n"
                + "      \"TOTAL_PROCESSED_INVOICES\":22670000,\n"
                + "      \"REMAINEING_INVOICES\":13580000,\n"
                + "      \"IN_PROGRESS_INVOICES\":130,\n"
                + "      \"PENDING_INVOICES\":120,\n"
                + "      \"TATAL_DOMISTIC_INVOICES\":70\n"
                + "   }";
        if (result.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    
    public static void main(String[] args) {
        String httpMethod  = "GET";
        String resourceURIString = "http://172.21.14.92:4001/igate/v1.0/en/public/users/fhatlani@stc.com.sa/images/profile";
        String language  = "";
        String dateAndTime = "Wed, 18 Sep 2019 12:01:38 GMT";
        String UserAgent = "ERP-SCM-AP";
        String APIVersion = "v1.0";
        String format =  httpMethod + resourceURIString + language + dateAndTime + UserAgent + APIVersion + httpMethod;
        
        System.out.println(format);


        
    }

}
