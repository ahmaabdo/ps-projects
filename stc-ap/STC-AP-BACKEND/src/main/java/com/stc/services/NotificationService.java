/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.NotificationController;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Anas Alghawi
 */
@Path("/notification")
public class NotificationService {
    
    @GET
    @Path("header/{pesronId}")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getNotificationHeader(@PathParam("pesronId") String pesronId) {

        NotificationController notificationController = new NotificationController();
        String result;

        result = notificationController.getNotificationHeader(pesronId);

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    @GET
    @Path("details/{notificationId}")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getNotificationDetails(@PathParam("notificationId") String notificationId) {

        NotificationController notificationController = new NotificationController();
        String result;

        result = notificationController.getNotificationDetails(notificationId);

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
}
