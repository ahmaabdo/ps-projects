/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.services;

import com.stc.controller.DashboardController;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Anas Alghawi
 */
@Path("/dashboard")
public class DashboardService {

    DashboardController dashboardController = new DashboardController();

    @GET
    @Path("get-invoices/{pesronId}")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getDashboard(@PathParam("pesronId") String pesronId) {

        DashboardController dashboardController = new DashboardController();
        String result;

        result = dashboardController.getDashboard(pesronId, "AR");

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        }

    }

    @GET
    @Path("get-invoices/focal")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getFocalDashboard() {

        DashboardController dashboardController = new DashboardController();
        String result;

        result = dashboardController.getFocalDashboard();

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        }
    }
    @GET
    @Path("completed-invoices/{personID}")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getCompletedInvoices(@PathParam("personID") String personId) {

        DashboardController dashboardController = new DashboardController();
        String result;

        result = dashboardController.getCompletedInvoices(personId);

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        }
    }

    @GET
    @Path("header-rejection-reason")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getRejectionHeaderLov() {

        DashboardController dashboardController = new DashboardController();
        String result;

        result = dashboardController.getRejectionHeaderLov();

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }
    
    @GET
    @Path("details-rejection-reason/{parentCode}")
    @Consumes({"application/xml", "application/json", "text/plain", "text/html"})
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response getRejectionDetailsLov(@PathParam("parentCode") String parentCode) {

        DashboardController dashboardController = new DashboardController();
        String result;

        result = dashboardController.getRejectionDetailsLov(parentCode);

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @POST
    @Path("invoice-action")
    @Consumes("application/json")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response invoiceAction(String payload) {

        DashboardController dashboardController = new DashboardController();

        String result;
        result = dashboardController.invoiceAction(payload);
//        result = "S";

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        }

    }

    @POST
    @Path("assign-action")
    @Consumes("application/json")
    @Produces({"application/xml", "application/json", "text/plain", "text/html"})
    public Response assigneeAction(String payload) {

        DashboardController dashboardController = new DashboardController();

        String result;
        result = dashboardController.assigneeAction(payload);

        if (result.isEmpty()) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        } else {
            return Response
                    .status(200)
                    .entity(result)
                    .type(MediaType.APPLICATION_JSON)
                    .build();

        }

    }
}
