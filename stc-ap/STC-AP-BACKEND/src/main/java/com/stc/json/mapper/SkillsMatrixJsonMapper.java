/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.json.mapper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Hp
 */
public class SkillsMatrixJsonMapper {

    public String get(String jsonObject) {

        JSONArray skillsMatrix =
            new JSONObject(jsonObject).getJSONArray("SKILLS_MATRIX");
        System.out.println("skillsMatrix\n"+skillsMatrix);

        JSONObject skill = null;
        Map<String, JSONObject> mappedJson = new HashMap<String, JSONObject>();


        for (int i = 0; i < skillsMatrix.length(); i++) {
            skill = skillsMatrix.getJSONObject(i);

            String user = skill.opt("PERSON_NAME").toString();
            String personId = skill.opt("PERSON_ID").toString();
            String workLoad = skill.opt("WORKLOAD").toString();
            String sectionName = skill.opt("SECTION_NAME").toString();
            String sectionCode = skill.opt("SECTION_CODE").toString();
            String sectionType = skill.opt("SECTION_TYPE").toString();
            String sectionValue = skill.opt("SECTION_VALUE").toString();
            String skmdId = skill.opt("SKMD_ID").toString();

            if (mappedJson.get(personId) == null) {
                JSONObject header = new JSONObject();

                header.put("USER", user);
                header.put("PERSON_ID", personId);
                header.put("WORKLOAD", workLoad);

                JSONArray skillObjectResult = new JSONArray();

                JSONObject vendorObjectResult = new JSONObject();
                JSONArray sectionVendorObjectResult = new JSONArray();

                vendorObjectResult.put("SECTIONS", sectionVendorObjectResult);

                header.put("SKILLS", skillObjectResult);
                header.put("VENDORS", vendorObjectResult);

                mappedJson.put(personId, header);
            }

            JSONObject skillMatrixResult = mappedJson.get(personId);

            JSONObject sections = new JSONObject();

            sections.put("SKMD_ID", skmdId);
            sections.put("SECTION_CODE", sectionCode);
            sections.put("SECTION_NAME", sectionName);
            sections.put("SECTION_VALUE", sectionValue);


            if (sectionType.equals("VENDOR")) {

                JSONArray vendorSection =
                    skillMatrixResult.getJSONObject("VENDORS").getJSONArray("SECTIONS");
                vendorSection.put(sections);

            } else {
                JSONArray skills = skillMatrixResult.getJSONArray("SKILLS");
                if (skills.length() == 0) {

                    JSONObject skillChild = new JSONObject();

                    skillChild.put("SECTION_TYPE", sectionType);
                    skillChild.put("SECTIONS", new JSONArray());
                    skillChild.getJSONArray("SECTIONS").put(sections);

                    skills.put(skillChild);
                } else {
                    boolean isFound = false;
                    for (int j = 0; j < skills.length(); j++) {
                        if (isFound) {
                            break;
                        }
                        JSONObject skillChild = skills.getJSONObject(j);

                        if (sectionType.equals(skillChild.getString("SECTION_TYPE"))) {
                            skillChild.getJSONArray("SECTIONS").put(sections);
                            isFound = true;
                            break;

                        }

                    }

                    if (!isFound) {
                        JSONObject skillChild2 = new JSONObject();

                        skillChild2.put("SECTION_TYPE", sectionType);
                        skillChild2.put("SECTIONS", new JSONArray());
                        skillChild2.getJSONArray("SECTIONS").put(sections);

                        skills.put(skillChild2);
                    }
                }
            }


        }

        Iterator it = mappedJson.entrySet().iterator();
        JSONArray result = new JSONArray();
        while (it.hasNext()) {
            Map.Entry parameter = (Map.Entry)it.next();
            result.put(parameter.getValue());
        }

        return result.toString();
    }

}
