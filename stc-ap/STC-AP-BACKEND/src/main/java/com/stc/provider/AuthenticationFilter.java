package com.stc.provider;

/**
 *
 * @author Anas Alghawi
 */


import org.glassfish.jersey.internal.util.Base64;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

@Provider
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter
{

    @Context
    private ResourceInfo resourceInfo;

    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Basic";
    private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED)
            .build();
    private static final Response ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN)
            .build();

    @Override
    public void filter(ContainerRequestContext requestContext)
    {
        Method method = resourceInfo.getResourceMethod();
        //Access allowed for all
        if( ! method.isAnnotationPresent(PermitAll.class))
        {
            //Access denied for all
            if(method.isAnnotationPresent(DenyAll.class))
            {
                requestContext.abortWith(ACCESS_FORBIDDEN);
                return;
            }

            //Get request headers
            final MultivaluedMap<String, String> headers = requestContext.getHeaders();

            //Fetch authorization header
            final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

            if (requestContext.getMethod().equals("OPTIONS")) {
                return;
            }

            //If no authorization information present; block access
            if(authorization == null || authorization.isEmpty())
            {
                requestContext.abortWith(ACCESS_DENIED);
                return;
            }

            //Get encoded username and password
            final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");

            //System.out.println(encodedUserPassword);

            //Decode username and password
            String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));;

            //Split username and password tokens
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();

            //System.out.println(username);
            //System.out.println(password);

            //RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
            //Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));
            //rolesSet.add("ADMIN");
            if(!isUserAllowed(username, password))
            {
                requestContext.abortWith(ACCESS_DENIED);
                return;
            }

            //Verify user access
            /*if(method.isAnnotationPresent(RolesAllowed.class))
            {
                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
                Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

                //Is user valid?
                if(!isUserAllowed(username, password, rolesSet))
                {
                    requestContext.abortWith(ACCESS_DENIED);
                    return;
                }
            }*/
        }
    }

    private boolean isUserAllowed(final String username, final String password)
    {
        boolean isAllowed = false;
        Properties properties = getProperties();
        //Step 1. Fetch password from database and match with password in argument
        //If both match then get the defined role for user from database and continue; else return isAllowed [false]
        //Access the database and do this part yourself
        //String userRole = userMgr.getUserRole(username);

        //System.out.println("auth" + properties.getProperty("username"));
        //System.out.println("auth" + properties.getProperty("password"));

        if(username.equals(properties.getProperty("username")) && password.equals(properties.getProperty("password")))
        {
            String userRole = "ADMIN";

            //Step 2. Verify user role
            /*if(rolesSet.contains(userRole))
            {
                isAllowed = true;
            }*/
            isAllowed = true;
        }
        else {
            isAllowed = false;
        }
        return isAllowed;
    }


    public Properties getProperties(){
        Properties properties = new Properties();
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            properties.load(new FileInputStream(new File(classLoader.getResource("authentication.properties").getFile())));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}
