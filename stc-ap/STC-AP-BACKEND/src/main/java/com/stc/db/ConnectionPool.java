/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.db;

/**
 *
 * @author Anas Alghawi
 */
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;


public class ConnectionPool {
    private Connection connection;
    private String dbUrl;
    private static String dbDriver;
    private static HikariDataSource ds;

    //static final int MAX_POOL_SIZE  = 10;
    //static final int MIN_IDLE       = 5;
    static final long MAX_LIFE_TIME = 60000;
    static final long IDLE_TIME = 15000;
    static final long CONN_TIME = 120000;

    final static Logger logger = Logger.getLogger(ConnectionPool.class);

    public ConnectionPool(){

        HikariConfig config = new HikariConfig("/conn.properties");
        Properties properties = getProperties();

//        dbDriver = config.getDataSourceClassName();
//        config.setMaximumPoolSize(Integer.parseInt(properties.getProperty("maxPoolSize")));
//        config.setMinimumIdle(Integer.parseInt(properties.getProperty("minimumIdle")));
//        config.setMaxLifetime(MAX_LIFE_TIME);
//        config.setIdleTimeout(IDLE_TIME);
//        config.setConnectionTimeout(CONN_TIME);
//
//        config.setConnectionTestQuery("SELECT 1 FROM Dual");
//
//        dbDriver = config.getDataSourceClassName();
//
//        ds = new HikariDataSource(config);
    }

    public Properties getProperties(){
        Properties properties = new Properties();
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            properties.load(new FileInputStream(new File(classLoader.getResource("config.properties").getFile())));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    private Properties readProperty()
    {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = this.getClass().getResourceAsStream("/conn.properties");


            // load a properties file
            prop.load(input);


        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return prop;
    }

    public static ConnectionPool getInstance(){
        return new ConnectionPool();
    }

    public String getDbUrl(){
        return dbUrl;
    }

    public String getDbDriver(){
        return dbDriver;
    }

    public void setDbUrl(String dbUrl){
        this.dbUrl = dbUrl;
    }

    public void setDbDriver(String dbDriver){
        this.dbDriver = dbDriver;
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}




