/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import static com.stc.controller.MainController.logger;
import com.stc.db.ConnectionPool;
import com.stc.json.mapper.SkillsMatrixJsonMapper;
import com.stc.objects.SkillsMatrixSoapEnvelopeObject;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.queries.SkillsMatrixQueries;
import com.stc.soap.SkillsMatrixSoapHandler;
import com.stc.soap.SoapHandler;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class SkillsMatrixController {

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public ResultSet resultSet;
    final static Logger logger = Logger.getLogger(SkillsMatrixController.class);

//    public String createSkillsMatrix(String payload) {
//        String result = null;
//
//        try {
//            JSONObject jsonObject = new JSONObject(payload);
//            ArrayList<String> list = new ArrayList<String>();
//
//            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_skill_matrix_pkg/?wsdl";
//            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/skill_matrix_insert_row/";
//
//            SoapEnvelopeObject seo = new SoapEnvelopeObject();
//            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/");
//            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
//            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
//            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/skill_matrix_insert_row/");
//
//            Map<String, String> param = new HashMap<String, String>();
//            param.put("P_PERSON_ID", jsonObject.getString("P_PERSON_ID"));
//            param.put("P_PERSON_JOB_ID", jsonObject.getString("P_PERSON_JOB_ID"));
//            param.put("P_SOURCE_ISUPPLY", jsonObject.getString("P_SOURCE_ISUPPLY"));
//            param.put("P_SOURCE_PROJECT_MANAGERS", jsonObject.getString("P_SOURCE_PROJECT_MANAGERS"));
//            param.put("P_SOURCE_NFTS", jsonObject.getString("P_SOURCE_NFTS"));
//            param.put("P_SECTOR_ISUPPLY", jsonObject.getString("P_SECTOR_ISUPPLY"));
//            param.put("P_REQUEST_BY_DATE", jsonObject.getString("P_REQUEST_BY_DATE"));
//            param.put("P_SECTOR_PROJECT_MANAGERS", jsonObject.getString("P_SECTOR_PROJECT_MANAGERS"));
//            param.put("P_SUPPLY_ISUPPLY", jsonObject.getString("P_SUPPLY_ISUPPLY"));
//            param.put("P_SUPPLY_PROJECT_MANAGERS", jsonObject.getString("P_SUPPLY_PROJECT_MANAGERS"));
//            param.put("P_PROCESS_ISUPPLY", jsonObject.getString("P_PROCESS_ISUPPLY"));
//            param.put("P_PROCESS_PROJECT_MANAGERS", jsonObject.getString("P_PROCESS_PROJECT_MANAGERS"));
//
//            seo.setParameter(param);
//
//            SoapHandler handler = new SoapHandler();
//            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
//            String createSkillsSoapResult = new JSONObject(jsonData).getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters").getString("P_RESULT");
//            System.out.println(createSkillsSoapResult);
//            if (createSkillsSoapResult.equalsIgnoreCase("COMPLET")) {
//                
//                JSONArray vendorsArray = jsonObject.getJSONArray("vendors");
//                String personId = jsonObject.getString("P_PERSON_ID");
//                System.out.println("Skill created");
//                
//                if (vendorsArray.length() > 0) {
//                    
//                    for (int i = 0; i < vendorsArray.length(); i++) {
//                        
//                        String createVendorsSoapResult = createSkillsMatrixVendors(personId, vendorsArray.getJSONObject(i).getString("vendor_id"), vendorsArray.getJSONObject(i).getString("vendor_name"));
//                        System.out.println("add vendpr id "+ vendorsArray.getJSONObject(i).getString("vendor_id")+ " "+createVendorsSoapResult);
//                    }
//                }
//            }
//            result = createSkillsSoapResult;
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Create Work Request Error", e);
//        }
//        return result;
//
//    }
//
//    public String updateSkillsMatrix() {
//        String result = null;
//
//        try {
//
////            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
//            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_skill_matrix_pkg/?wsdl";
//            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/skill_matrix_update_row/";
//
//            SoapEnvelopeObject seo = new SoapEnvelopeObject();
//            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/");
//            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
//            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
//            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/skill_matrix_update_row/");
//
//            Map<String, String> param = new HashMap<String, String>();
//            param.put("P_PERSON_ID", "3");
//            param.put("P_PERSON_JOB_ID", "4");
//            param.put("P_SOURCE_ISUPPLY", "5");
//            param.put("P_SOURCE_PROJECT_MANAGERS", "1");
//            param.put("P_SOURCE_NFTS", "1");
//            param.put("P_SECTOR_ISUPPLY", "1");
//            param.put("P_REQUEST_BY_DATE", "7");
//            param.put("P_SECTOR_PROJECT_MANAGERS", "1");
//            param.put("P_SUPPLY_ISUPPLY", "1");
//            param.put("P_SUPPLY_PROJECT_MANAGERS", "1");
//            param.put("P_PROCESS_ISUPPLY", "1");
//            param.put("P_PROCESS_PROJECT_MANAGERS", "P_PROCESS_PROJECT_MANAGERS");
//
//            seo.setParameter(param);
//
//            SoapHandler handler = new SoapHandler();
//            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
//            JSONObject obj = new JSONObject(jsonData).getJSONObject("env:Envelope");
//            System.out.println(obj.toString());
//            result = jsonData;
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Create Work Request Error", e);
//        }
//        return result;
//
//    }
//
//    public String createSkillsMatrixVendors(String PersonId, String vendorId, String vendorName) {
//        String result = null;
//
//        try {
//
//            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_skill_matrix_pkg/?wsdl";
//            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/vendor_insert_row/";
//
//            SoapEnvelopeObject seo = new SoapEnvelopeObject();
//            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/");
//            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
//            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
//            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxstc_skill_matrix_pkg/vendor_insert_row/");
//
//            Map<String, String> param = new HashMap<String, String>();
//            param.put("P_PERSON_ID", PersonId);
//            param.put("P_VENDOR_ID", vendorId);
//            param.put("P_VENDOR_NAME", vendorName);
//
//            seo.setParameter(param);
//
//            SoapHandler handler = new SoapHandler();
//            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
//                        System.out.println(jsonData);
//            String createVendorsSoapResult = new JSONObject(jsonData).getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters").getString("P_RESULT");
//            System.out.println(createVendorsSoapResult);
//            result = createVendorsSoapResult;
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error("Create Work Request Error", e);
//        }
//        return result;
//
//    }
    public String getSkillsMatrix(String skmId, String lang) {
        SkillsMatrixQueries skillsMatrixQueries = new SkillsMatrixQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", skillsMatrixQueries.GET_SKILLS_MATRIX(skmId, lang));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "SKILLS_MATRIX");

            JSONObject jsonObject = new JSONObject(result);
            System.out.println("jsonObject");
            SkillsMatrixJsonMapper skillsMatrixJsonMapper = new SkillsMatrixJsonMapper();
            result = skillsMatrixJsonMapper.get(jsonObject.toString());

            System.out.println(result);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Work Request Error", e);
        }
        return result;

    }

    public String createSkillsMatrix(JSONObject payload, String userId) {
        String result = null;
        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/create_skill_matrix/";

            SkillsMatrixSoapEnvelopeObject seo = new SkillsMatrixSoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/create_skill_matrix/");

            seo.setJsonPayload(payload);
            seo.setRequestType("create");

            SkillsMatrixSoapHandler handler = new SkillsMatrixSoapHandler();
            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
            JSONObject obj = new JSONObject(jsonData);
            System.out.println("Response SOAP Message:\n" + obj.getJSONObject("env:Envelope"));

            JSONObject output = obj.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters");
            output.remove("xmlns");
            output.remove("xmlns:xsi");
            output.remove("X_MESSAGE");

            System.out.println("\nResult:\n" + output.toString() + "\n\n********************************************************\n");
            result = output.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Sla Controller", e);
        }

        return result;
    }

    public String updateSkillsMatrix(JSONObject payload, String userId) {
        String result = null;
        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/update_skill_matrix/";

            SkillsMatrixSoapEnvelopeObject seo = new SkillsMatrixSoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/update_skill_matrix/");

            seo.setJsonPayload(payload);
            seo.setRequestType("update");
            seo.setUserId(userId);

            SkillsMatrixSoapHandler handler = new SkillsMatrixSoapHandler();
            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
            JSONObject obj = new JSONObject(jsonData);
            System.out.println("Response SOAP Message:\n" + obj.getJSONObject("env:Envelope"));

            JSONObject output = obj.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters");
            output.remove("xmlns");
            output.remove("xmlns:xsi");
            output.remove("X_MESSAGE");

            System.out.println("\nResult:\n" + output.toString() + "\n\n********************************************************\n");
            result = output.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Sla Controller", e);
        }

        return result;
    }

    public String getSectionsTypeLov() {
        SkillsMatrixQueries skillsMatrixQueries = new SkillsMatrixQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", skillsMatrixQueries.XXARRG_SECTIONS_TYPE_LOV_V());
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject SoapesultJsonObject = new JSONObject(result);

            System.out.println(SoapesultJsonObject);
            JSONArray jsonArray = SoapesultJsonObject.getJSONArray("ResponseJsonArray");
            JSONArray sectorsJsonArray = new JSONArray();
            JSONArray vendorsJsonArray = new JSONArray();
            JSONArray supplyersJsonArray = new JSONArray();
            JSONArray processJsonArray = new JSONArray();
            JSONArray sourceJsonArray = new JSONArray();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                if (jsonArray.getJSONObject(i).getString("SECTION_TYPE").equalsIgnoreCase("SECTOR")) {
                    jsonObject.put("SECTION_CODE", jsonArray.getJSONObject(i).get("SECTION_NAME"));
                    jsonObject.put("SECTION_NAME", jsonArray.getJSONObject(i).get("SECTION_NAME_MEANING"));
                    jsonObject.put("SECTION_TYPE", jsonArray.getJSONObject(i).get("SECTION_TYPE"));
                    jsonObject.put("SECTION_VALUE", jsonArray.getJSONObject(i).get("SECTION_VALUE"));
                    sectorsJsonArray.put(jsonObject);
                } else if (jsonArray.getJSONObject(i).getString("SECTION_TYPE").equalsIgnoreCase("VENDOR")) {
                    jsonObject.put("SECTION_CODE", jsonArray.getJSONObject(i).get("SECTION_NAME"));
                    jsonObject.put("SECTION_NAME", jsonArray.getJSONObject(i).get("SECTION_NAME_MEANING"));
                    jsonObject.put("SECTION_TYPE", jsonArray.getJSONObject(i).get("SECTION_TYPE"));
                    jsonObject.put("SECTION_VALUE", jsonArray.getJSONObject(i).get("SECTION_VALUE"));
                    vendorsJsonArray.put(jsonObject);
                } else if (jsonArray.getJSONObject(i).getString("SECTION_TYPE").equalsIgnoreCase("SUPPLY")) {
                    jsonObject.put("SECTION_CODE", jsonArray.getJSONObject(i).get("SECTION_NAME"));
                    jsonObject.put("SECTION_NAME", jsonArray.getJSONObject(i).get("SECTION_NAME_MEANING"));
                    jsonObject.put("SECTION_TYPE", jsonArray.getJSONObject(i).get("SECTION_TYPE"));
                    jsonObject.put("SECTION_VALUE", jsonArray.getJSONObject(i).get("SECTION_VALUE"));
                    supplyersJsonArray.put(jsonObject);
                } else if (jsonArray.getJSONObject(i).getString("SECTION_TYPE").equalsIgnoreCase("PROCESS")) {
                    jsonObject.put("SECTION_CODE", jsonArray.getJSONObject(i).get("SECTION_NAME"));
                    jsonObject.put("SECTION_NAME", jsonArray.getJSONObject(i).get("SECTION_NAME_MEANING"));
                    jsonObject.put("SECTION_TYPE", jsonArray.getJSONObject(i).get("SECTION_TYPE"));
                    jsonObject.put("SECTION_VALUE", jsonArray.getJSONObject(i).get("SECTION_VALUE"));
                    processJsonArray.put(jsonObject);
                } else if (jsonArray.getJSONObject(i).getString("SECTION_TYPE").equalsIgnoreCase("SOURCE")) {
                    jsonObject.put("SECTION_CODE", jsonArray.getJSONObject(i).get("SECTION_NAME"));
                    jsonObject.put("SECTION_NAME", jsonArray.getJSONObject(i).get("SECTION_NAME_MEANING"));
                    jsonObject.put("SECTION_TYPE", jsonArray.getJSONObject(i).get("SECTION_TYPE"));
                    jsonObject.put("SECTION_VALUE", jsonArray.getJSONObject(i).get("SECTION_VALUE"));
                    sourceJsonArray.put(jsonObject);
                }

            }

            JSONObject resultJson = new JSONObject();
            resultJson.put("SECTOR", sectorsJsonArray);
            resultJson.put("VENDOR", vendorsJsonArray);
            resultJson.put("SUPPLY", supplyersJsonArray);
            resultJson.put("PROCESS", processJsonArray);
            resultJson.put("SOURCE", sourceJsonArray);

            System.out.print("getSectionsTypeLov: \n" + resultJson.toString() + "\n");

            result = resultJson.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getSectionsTypeLov Error", e);
        }
        return result;
    }

    public String getNoSkillsEmployees() {
        SkillsMatrixQueries skillsMatrixQueries = new SkillsMatrixQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", skillsMatrixQueries.GET_NO_SKILL_EMPLOYEES);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject SoapesultJsonObject = new JSONObject(result);

            System.out.println(SoapesultJsonObject);
            JSONArray jsonArray = SoapesultJsonObject.getJSONArray("ResponseJsonArray");

            System.out.print("getNoSkillsEmployees: \n" + jsonArray.toString() + "\n");

            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getNoSkillsEmployees Error", e);
        }
        return result;
    }

    public static void main(String[] args) {
        SkillsMatrixController a = new SkillsMatrixController();

//        a.createSkillsMatrix();
//        a.updateSkillsMatrix();
//        a.createSkillsMatrixVendors();
        String sample = " {  \n"
                + "      \"USER\":\"AHMAD\",\n"
                + "      \"PERSON_ID\":\"1\",\n"
                + "      \"SKILLS\":[  \n"
                + "         {  \n"
                + "            \"SECTION_TYPE\":\"SOURCE\",\n"
                + "            \"SECTIONS\":[  \n"
                + "               {  \n"
                + "                  \"SKMD_ID\":\"1\",\n"
                + "                  \"SECTION_NAME\":\"Isupply\",\n"
                + "                  \"SECTION_VALUE\":\"Y\"\n"
                + "               },\n"
                + "               {  \n"
                + "                  \"SKMD_ID\":\"2\",\n"
                + "                  \"SECTION_NAME\":\"Project Managers\",\n"
                + "                  \"SECTION_VALUE\":\"Y\"\n"
                + "               },\n"
                + "               {  \n"
                + "                  \"SKMD_ID\":\"3\",\n"
                + "                  \"SECTION_NAME\":\"NFTS\",\n"
                + "                  \"SECTION_VALUE\":\"Y\"\n"
                + "               }\n"
                + "            ]\n"
                + "         },\n"
                + "         {  \n"
                + "            \"SECTION_TYPE\":\"SECTOR\",\n"
                + "            \"SECTIONS\":[  \n"
                + "               {  \n"
                + "                  \"SKMD_ID\":\"4\",\n"
                + "                  \"SECTION_NAME\":\"Network\",\n"
                + "                  \"SECTION_VALUE\":\"Y\"\n"
                + "               },\n"
                + "               {  \n"
                + "                  \"SKMD_ID\":\"5\",\n"
                + "                  \"SECTION_NAME\":\"Applications\",\n"
                + "                  \"SECTION_VALUE\":\"N\"\n"
                + "               },\n"
                + "               {  \n"
                + "                  \"SKMD_ID\":\"6\",\n"
                + "                  \"SECTION_NAME\":\"CPU\",\n"
                + "                  \"SECTION_VALUE\":\"Y\"\n"
                + "               }\n"
                + "            ]\n"
                + "         }\n"
                + "      ],\n"
                + "      \"VENDORS\":{  \n"
                + "         \"SECTIONS\":[  \n"
                + "            {  \n"
                + "               \"SKMD_ID\":\"1\",\n"
                + "               \"SECTION_NAME\":\"Vendor 1\",\n"
                + "               \"SECTION_VALUE\":\"Y\"\n"
                + "            },\n"
                + "            {  \n"
                + "               \"SKMD_ID\":\"2\",\n"
                + "               \"SECTION_NAME\":\"Vendor 2\",\n"
                + "               \"SECTION_VALUE\":\"N\"\n"
                + "            },\n"
                + "            {  \n"
                + "               \"SKMD_ID\":\"3\",\n"
                + "               \"SECTION_NAME\":\"Vendor 3\",\n"
                + "               \"SECTION_VALUE\":\"Y\"\n"
                + "            }\n"
                + "         ]\n"
                + "      }\n"
                + "   }";
//        System.out.println(new JSONObject(sample));

//          a.getSkillsMatrix("21", "ar");
//SkillsMatrixJsonMapper skillsMatrixJsonMapper= new SkillsMatrixJsonMapper();
//        System.out.println(skillsMatrixJsonMapper.get(a.getSkillsMatrix("21", "ar")));
//          a.createSkillsMatrix(new JSONObject(sample));
//        a.updateSkillsMatrix(new JSONObject(sample));
//        a.getSectionsTypeLov();
        a.getNoSkillsEmployees();
    }

}
