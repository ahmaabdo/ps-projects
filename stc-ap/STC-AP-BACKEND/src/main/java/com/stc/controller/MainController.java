package com.stc.controller;

import static com.stc.controller.SkillsMatrixController.pool;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.queries.SkillsMatrixQueries;
import com.stc.soap.SoapHandler;
import org.apache.log4j.Logger;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class MainController {

    final static Logger logger = Logger.getLogger(MainController.class);
    SkillsMatrixQueries skillsMatrixQueries = new SkillsMatrixQueries();

    public String getEmployeeType() {
        String result = null;

        try {

//            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", skillsMatrixQueries.XXAPRG_EMPLOYEE_TYPE_V);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("ResponseJsonArray");

            System.out.print("getEmployeeType: \n" + jsonArray);

            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getEmployeeType Error", e);
        }
        return result;
    }

    public String getPoMatchLov(String lang) {
        String result = null;

        try {

//            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxstc_eam_custom_api2/?wsdl";
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", skillsMatrixQueries.XXAPRG_PO_MATCH(lang));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("ResponseJsonArray");

            System.out.print("getPoMatchLov: \n" + jsonArray);

            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getPoMatchLov Error", e);
        }
        return result;
    }

    public String getEmployees() {
        SkillsMatrixQueries skillsMatrixQueries = new SkillsMatrixQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", skillsMatrixQueries.GET_EMPLOYEES);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject SoapesultJsonObject = new JSONObject(result);

            System.out.println(SoapesultJsonObject);
            JSONArray jsonArray = SoapesultJsonObject.getJSONArray("ResponseJsonArray");

            System.out.print("getEmployees: \n" + jsonArray.toString() + "\n");

            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getEmployees Error", e);
        }
        return result;
    }

    public String validateLogin(String payload) {
        String result = null;
        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/validate_login/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/");
            seo.setSoapurl("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/validate_login/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));

            JSONObject jsonPaylod = new JSONObject(payload);
            Map<String, String> param = new HashMap<String, String>();
            param.put("P_USER_NAME", jsonPaylod.getString("username"));
            param.put("P_PASSWORD", jsonPaylod.getString("password"));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            String soapResponse = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
            JSONObject jsonResponse = new JSONObject(soapResponse);

            String jsonResultStatus = jsonResponse.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters").getString("X_RESULT");

            if (jsonResultStatus.equalsIgnoreCase("S")) {
                jsonResponse = jsonResponse.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters");
                jsonResponse.remove("xmlns");
                jsonResponse.remove("xmlns:xsi");
                jsonResponse.remove("X_MESSAGE");

            } else {
                jsonResponse = jsonResponse.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters");
                jsonResponse.remove("xmlns");
                jsonResponse.remove("xmlns:xsi");
                jsonResponse.remove("P_PERSON_NAME");
                jsonResponse.remove("P_ACCOUNT_ID");
                jsonResponse.remove("P_PERSON_ID");
                jsonResponse.remove("P_PERSON_TYPE_CODE");
            }

            System.out.println("\n validate_login :\n" + jsonResponse);
            result = jsonResponse.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getPoMatchLov Error", e);
        }

        return result;
    }

    public static void main(String args[]) {

        MainController a = new MainController();
        String payload = "{\"username\":\"ASWDEED\", \"password\":\"19YPMxyyb\"}";
//        a.getEmployeeType();
//        a.getPoMatchLov("us");
        a.validateLogin(payload);

    }
}
