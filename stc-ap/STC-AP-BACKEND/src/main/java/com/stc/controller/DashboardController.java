/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import static com.stc.controller.SkillsMatrixController.pool;
import com.stc.db.ConnectionPool;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.queries.DashBoardQueries;
import com.stc.soap.DashBoardSoapHandler;
import com.stc.soap.SoapHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class DashboardController {

    final static Logger logger = Logger.getLogger(DashboardController.class);

    public String invoiceAction(String payload) {
        String result = null;

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            SoapEnvelopeObject seo = new SoapEnvelopeObject();

            JSONObject requestPayload = new JSONObject(payload);
            System.out.println("invoiceAction invoiceAction = \n" + requestPayload);

            String soapAction = null;
            if (requestPayload.getString("ACTION").equalsIgnoreCase("ACT-APPROVE")) {
                soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/approve_card";
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/approve_card/");
            }
            if (requestPayload.getString("ACTION").equalsIgnoreCase("ACT-REJECT")) {
                soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/reject_card";
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/reject_card/");
            }
            if (requestPayload.getString("ACTION").equalsIgnoreCase("SUP-APPROVE")) {
                soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/approve_card";
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/approve_card/");
            }
            if (requestPayload.getString("ACTION").equalsIgnoreCase("SUP-REJECT")) {
                soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/reject_card";
                seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/reject_card/");
            }

            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));

//            Map<String, String> param = new HashMap<String, String>();
//            param.put("PERSON_ID", requestPayload.getString("PERSON_ID"));
//            param.put("INVOICE_ID", requestPayload.getString("INVOICE_ID"));
//            param.put("ACCOUNTANT_INVOICE_ID", requestPayload.getString("ACCOUNTANT_INVOICE_ID"));
//            param.put("ITEM_KEY", requestPayload.getString("ITEM_KEY"));
//            param.put("NOTE", invoiceAction);
//            seo.setParameter(param);
            seo.setPayload("[" + payload + "]");

            DashBoardSoapHandler handler = new DashBoardSoapHandler();
//            String jsonData = handler.createSOAPRequest(soapAction, seo).toString();
            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);

            result = jsonData.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("invoiceAction Error", e);
        }
        return result;
    }

    public String getDashboard(String personId, String lang) {
        DashBoardQueries dashBoardQueries = new DashBoardQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", dashBoardQueries.GET_ACT_DASHBOARD(personId, lang));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ACT_DASHBOARD");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = new JSONArray();
            jsonArray = jsonObject.getJSONArray("ACT_DASHBOARD");

            SlaController slaController = new SlaController();
            JSONObject slaJsonObject = new JSONObject(slaController.getSla());
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonArray.getJSONObject(i).put("SLA", slaJsonObject);
            }
            System.out.println("\njsonArray :\n" + jsonArray);
            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Work Request Error", e);
        }
        return result;

    }

    public String getFocalDashboard() {
        DashBoardQueries dashBoardQueries = new DashBoardQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", dashBoardQueries.GET_FOCAL_DASHBOARD("AR"));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ACT_DASHBOARD");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = new JSONArray();
            jsonArray = jsonObject.getJSONArray("ACT_DASHBOARD");

            SlaController slaController = new SlaController();
            JSONObject slaJsonObject = new JSONObject(slaController.getSla());
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonArray.getJSONObject(i).put("SLA", slaJsonObject);
            }
            System.out.println("\njsonArray :\n" + jsonArray);
            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Create Work Request Error", e);
        }
        return result;

    }

    public String getCompletedInvoices(String personId) {
        DashBoardQueries dashBoardQueries = new DashBoardQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", dashBoardQueries.GET_COMPLETED_DASHBOARD(personId));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Response");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = new JSONArray();
            jsonArray = jsonObject.getJSONArray("Response");

            System.out.println("\n getCompletedInvoices :\n" + jsonArray);
            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getCompletedInvoices Error", e);
        }
        return result;
    }

    public String getRejectionHeaderLov() {
        DashBoardQueries dashBoardQueries = new DashBoardQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", dashBoardQueries.GET_REJECTION_HEADER_LOV);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Response");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = new JSONArray();
            jsonArray = jsonObject.getJSONArray("Response");

            System.out.println("\n getRejectionHeaderLov :\n" + jsonArray);
            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getRejectionHeaderLov Error", e);
        }
        return result;
    }

    public String getRejectionDetailsLov(String parentCode) {
        DashBoardQueries dashBoardQueries = new DashBoardQueries();
        String result = null;

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", dashBoardQueries.GET_REJECTION_DETAILS_LOV(parentCode));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "Response");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = new JSONArray();
            jsonArray = jsonObject.getJSONArray("Response");

            System.out.println("\n getRejectionHeaderLov :\n" + jsonArray);
            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getRejectionHeaderLov Error", e);
        }
        return result;
    }

    public String assignInvoice(String payload) {
        String result = null;
//        JSONObject requestPayload = new JSONObject(payload);

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/assign_invoice/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/assign_invoice/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("P_INVOICE_ID", "106810");
            param.put("P_PERSON_ID", "62");
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);

            System.out.print("assignInvoice: \n" + jsonData);

            result = jsonData;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("assignInvoice Error", e);
        }
        return result;
    }

    public String deAssignInvoice(String payload) {
        String result = null;
//        JSONObject requestPayload = new JSONObject(payload);

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/deassign_invoice/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/deassign_invoice/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("P_ACC_INV_ID", "230");
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);

            System.out.print("assignInvoice: \n" + jsonData);

            result = jsonData;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("assignInvoice Error", e);
        }
        return result;
    }

    public String assigneeAction(String payload) {
        String result = null;
        JSONArray requestPayloadArray = new JSONArray(payload);
        JSONObject requestPayload = requestPayloadArray.getJSONObject(0);
        String action = requestPayload.getString("ACTION");

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/assign_cards/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            if (action.equalsIgnoreCase("ASSIGN")) {
                seo.setHeader("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/");
                seo.setSoapurl("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/assign_cards/");
            } else if (action.equalsIgnoreCase("REASSIGN")) {
                seo.setHeader("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/reassign_cards/");
                seo.setSoapurl("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/reassign_cards/");
            }

            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
//            seo.setPayload(payload);
            seo.setPayload(payload);

            DashBoardSoapHandler handler = new DashBoardSoapHandler();
            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
//            String jsonData = handler.createSOAPRequest(soapAction, seo).toString();

            result = jsonData.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("invoiceAction Error", e);
        }
        return result;
    }

    public static void main(String[] args) {

        DashboardController a = new DashboardController();
        String sample = "{\n"
                + "          \"ACTION\" : \"ACT-APPROVE\",\n"
                + "          \"PERSON_ID\": \"258042\",\n"
                + "          \"INVOICE_ID\": \"2587257\",\n"
                + "          \"ACCOUNTANT_INVOICE_ID\": \"247\",\n"
                + "          \"ITEM_KEY\": \"589-590\",\n"
                + "          \"NOTE\": \"Tayseer\"\n"
                + "}";

        String sample1 = "[{\n"
                + "        \"ACTION\" : \"ASSIGN\",\n"
                + "        \"PERSON_ID\": \"37025\",\n"
                + "        \"INVOICE_ID\": \"2587265\",\n"
                                + "        \"ACCOUNTANT_INVOICE_ID\": \"263\",\n"
                                + "        \"NOTE\": \"REJECT\",\n"
                                + "          \"ITEM_KEY\": \"257-258\",\n"
                + "    }"
                //                + ", {\n"
                //                + "          \"ACTION\" : \"ASSIGN\",\n"
                //                + "        \"PERSON_ID\": \"62\",\n"
                //                + "        \"INVOICE_ID\": \"106813\",\n"
                //                + "        \"ACCOUNTANT_INVOICE_ID\": \"259\",\n"
                //                + "        \"NOTE\": \"REJECT\",\n"
                //                + "          \"ITEM_KEY\": \"259-260\",\n"
                //                + "    }\n"
                + "]";
//        System.out.println(sample1);
//        a.invoiceAction(sample);
//        a.getDashboard("61", "AR");
//        a.getFocalDashboard();
//        a.assignInvoice("");
//        a.deAssignInvoice("");
        a.assigneeAction(sample1);
//        a.getRejectionHeaderLov();
//        a.getRejectionDetailsLov("AWT ERROR");
//        a.getCompletedInvoices();

    }

}
