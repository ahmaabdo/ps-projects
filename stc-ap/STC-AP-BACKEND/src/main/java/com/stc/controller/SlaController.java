/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import static com.stc.controller.SkillsMatrixController.pool;
import com.stc.objects.SlaSoapEnvelopeObject;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.queries.SlaQueries;
import com.stc.soap.SlaSoapHandler;
import com.stc.soap.SoapHandler;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class SlaController {

    final static Logger logger = Logger.getLogger(SlaController.class);

    public String updateSla(String invoiceReview, String supervisorReview) {
        String result = null;
        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/update_configration/";

            SlaSoapEnvelopeObject seo = new SlaSoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/update_configration/");

            Map<String, String> congifItem1param = new HashMap<String, String>();
            Map<String, String> congifItem2param = new HashMap<String, String>();

            if (StringUtils.isNoneEmpty(supervisorReview)) {          
                congifItem1param.put("CONF_PROMPT", "Supervisor Review");
                congifItem1param.put("CONF_LANG", "US");
                congifItem1param.put("CONF_VALUE", supervisorReview);
                congifItem1param.put("CONF_NAME", "SUPERVISOR REVIEW");
                congifItem1param.put("CONF_TYPE", "SLA");
            }
            
            System.out.println("congifItem1param: \n"+ congifItem1param);
            if (StringUtils.isNoneEmpty(invoiceReview)) {
                congifItem2param.put("CONF_TYPE", "SLA");
                congifItem2param.put("CONF_NAME", "INVOICE REVIEW");
                congifItem2param.put("CONF_VALUE", invoiceReview);
                congifItem2param.put("CONF_LANG", "US");
                congifItem2param.put("CONF_PROMPT", "Invoice Review");
            }

            seo.setCongifItem1param(congifItem1param);
            seo.setCongifItem2param(congifItem2param);

            SlaSoapHandler handler = new SlaSoapHandler();
            String jsonData = handler.callSoapWebService(soapEndpointUrl, soapAction, seo);
            JSONObject obj = new JSONObject(jsonData);
            System.out.println("Response SOAP Message:\n"+ obj.getJSONObject("env:Envelope"));
            
            JSONObject output =  obj.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters");
            output.remove("xmlns");
            output.remove("xmlns:xsi");
            output.remove("X_MESSAGE");
            
            System.out.println("\nResult:\n"+output.toString()+"\n\n********************************************************\n");
            result = output.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Sla Controller", e);
        }

        return result;
    }
    
    public String getSla() {
        SlaQueries slaQueries = new SlaQueries();
        String result = null;
        

        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", slaQueries.XXAPRG_CONFIGRATION_V);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("ResponseJsonArray");
            
            JSONObject resutlJson = new JSONObject();
            for(int i=0; i<jsonArray.length(); i++){
                
                if(jsonArray.getJSONObject(i).getString("CONF_TYPE").equalsIgnoreCase("SLA") && jsonArray.getJSONObject(i).getString("CONF_NAME").equalsIgnoreCase("INVOICE REVIEW")){
                    resutlJson.put("accountant", jsonArray.getJSONObject(i).getString("CONF_VALUE"));
                }else{
                    resutlJson.put("supervisor", jsonArray.getJSONObject(i).getString("CONF_VALUE"));
                }
            }
            
            
            System.out.print("getSla: \n" + resutlJson);
            
            result = resutlJson.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getSla Error", e);
        }
        return result;
    }

    public static void main(String[] args) {
        
        SlaController a = new SlaController();
//        a.updateSla("1","1");
//        a.getSla();
    }
}
