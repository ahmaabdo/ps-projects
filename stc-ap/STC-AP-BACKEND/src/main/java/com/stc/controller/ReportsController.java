/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import com.stc.db.ConnectionPool;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.queries.NotificationQueries;
import com.stc.queries.ReportsQueries;
import com.stc.soap.SoapHandler;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class ReportsController {
    
    final static Logger logger = Logger.getLogger(ReportsController.class);
    public static ConnectionPool pool = ConnectionPool.getInstance();

    public String topInvoiceVendor(String fromDate, String toDate) {
        ReportsQueries reportsQueries = new ReportsQueries();
        String result = null;
        try {

            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY",reportsQueries.GET_TOP_INVOICES_VENDOR(fromDate, toDate));
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "RESPONSE");

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = new JSONArray();
            jsonArray = jsonObject.getJSONArray("RESPONSE");

            System.out.println("\njsonArray :\n" + jsonArray);
            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("topInvoiceVendor Error", e);
        }
        return result;
    }
}
