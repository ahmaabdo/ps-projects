/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.controller;

import com.stc.db.ConnectionPool;
import com.stc.objects.SoapEnvelopeObject;
import com.stc.queries.AccountsQueries;
import com.stc.soap.SoapHandler;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class AccountsController {

    public static ConnectionPool pool = ConnectionPool.getInstance();
    final static Logger logger = Logger.getLogger(AccountsController.class);

    public String getDefinedAccounts() {
        String result = null;

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            AccountsQueries accountsQueries = new AccountsQueries();
            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", accountsQueries.GET_ALL_DEFINED_ACCOUNTS);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject SoapesultJsonObject = new JSONObject(result);

            System.out.println(SoapesultJsonObject);
            JSONArray jsonArray = SoapesultJsonObject.getJSONArray("ResponseJsonArray");

            System.out.print("\n getDefinedAccounts: \n" + jsonArray.toString() + "\n");

            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getDefinedAccounts Error", e);
        }
        return result;
    }

    public String getUndefinedAccounts() {
        String result = null;

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            AccountsQueries accountsQueries = new AccountsQueries();
            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", accountsQueries.GET_NOT_DEFINED_ACCOUNTS);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject SoapesultJsonObject = new JSONObject(result);

            System.out.println(SoapesultJsonObject);
            JSONArray jsonArray = SoapesultJsonObject.getJSONArray("ResponseJsonArray");

            System.out.print("\n getUndefinedAccounts: \n" + jsonArray.toString() + "\n");

            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getUndefinedAccounts Error", e);
        }
        return result;
    }

    public String getUserRoleType() {
        String result = null;

        try {
            String soapEndpointUrl = pool.getProperties().getProperty("soagatewayurl") + "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl";
            String soapAction = "http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/";

            SoapEnvelopeObject seo = new SoapEnvelopeObject();
            seo.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
            seo.setUsername(pool.getProperties().getProperty("grantUserId"));
            seo.setPassword(pool.getProperties().getProperty("grantPassword"));
            seo.setSoapurl("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/execute_query/");

            AccountsQueries accountsQueries = new AccountsQueries();
            Map<String, String> param = new HashMap<String, String>();
            param.put("SQL_QUERY", accountsQueries.GET_EMPLOYEE_ROLE);
            seo.setParameter(param);

            SoapHandler handler = new SoapHandler();
            result = handler.callSoapWebServiceJSON(soapEndpointUrl, soapAction, seo, "ResponseJsonArray");

            JSONObject SoapesultJsonObject = new JSONObject(result);

            System.out.println(SoapesultJsonObject);
            JSONArray jsonArray = SoapesultJsonObject.getJSONArray("ResponseJsonArray");

            System.out.print("\n getUserRoleType: \n" + jsonArray.toString() + "\n");

            result = jsonArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getUserRoleType Error", e);
        }
        return result;
    }

    public static void main(String[] args) {
        AccountsController a = new AccountsController();
//        a.getDefinedAccounts();
        a.getUndefinedAccounts();
//        a.getUserRoleType();
    }

}
