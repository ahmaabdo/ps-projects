/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stc.bean;

/**
 *
 * @author Anas Alghawi
 */
public class SlaBean {

    String invoiceReview;
    String supervisorReview;


    public String getInvoiceReview() {
        return invoiceReview;
    }

    public void setInvoiceReview(String invoiceReview) {
        this.invoiceReview = invoiceReview;
    }

    public String getSupervisorReview() {
        return supervisorReview;
    }

    public void setSupervisorReview(String supervisorReview) {
        this.supervisorReview = supervisorReview;
    }

  

}
