package com;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class PortalService {

	private static PortalService instance = null;

	public static PortalService getInstance() {
		if (instance == null)
			instance = new PortalService();
		return instance;
	}

	public String mapUsers(JSONArray ids_emails) {
		List<User> user_list;
		JSONArray arr = new JSONArray();

		try {
			user_list = UserLocalServiceUtil.getUsers(QueryUtil.ALL_POS,
					QueryUtil.ALL_POS);

			
			for (int y = 0; y < ids_emails.length(); y++) {

				for (int x = 0; x < user_list.size(); x++) {
					if (ids_emails
							.getJSONObject(y)
							.get("EMAIL_ADDRESS")
							.toString()
							.equalsIgnoreCase(
									user_list.get(x).getEmailAddress())) {
						
						JSONObject obj = new JSONObject();
						obj.put("email", user_list.get(x).getEmailAddress());
						obj.put("img", user_list.get(x).getPortraitId());
						obj.put("id",
								ids_emails.getJSONObject(y).get("PERSON_ID"));
						arr.put(obj);

//						break;
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}

		return arr.toString();
	}

}
