package com;

import com.integration.stc.AppsProLogger;
import com.integration.stc.soap.AppsproSoupCaller;
import com.integration.util.AccountUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.List;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 * @author Hussein
 */
public class AppController extends MVCPortlet {

	class Test {
		public String name;
	}

	com.integration.serv.ApiServices apiServices = new com.integration.serv.ApiServices();

	// @ProcessAction(name="testController")
	// @JSONWebService(value = "testService", method = "GET")

	public void testController(ActionRequest request, ActionResponse response)
			throws Exception {

		// FileLogger.log("test line 1\ntest line 2");
		// PrintWriter pw =
		// PortalUtil.getHttpServletResponse(response).getWriter();
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		// List<User> user_list =
		// UserLocalServiceUtil.getUsers(QueryUtil.ALL_POS,
		// QueryUtil.ALL_POS);
		//
		// JSONArray arr = new JSONArray();
		// for (int x = 0; x < user_list.size(); x++) {
		// JSONObject obj = new JSONObject();
		// obj.put("email", user_list.get(x).getEmailAddress());
		// obj.put("img", user_list.get(x).getPortraitId());
		// arr.put(obj);
		// }
		//
		// AppsproSoupCaller.buildQuery("test").run(false);
		//
		ThemeDisplay td = new ThemeDisplay();

		AppsProLogger.info(td.getURLPortal());

		InetAddress inetAddress = InetAddress.getLocalHost();
		System.out.println("IP Address:- " + inetAddress.getHostAddress());
		System.out.println("Host Name:- " + inetAddress.getHostName());

		pw.write(inetAddress.getHostAddress() + " -- "
				+ inetAddress.getHostName());
		pw.flush();
		pw.close();
	}

	public void loadUsersImageData(ActionRequest request,
			ActionResponse response) throws Exception {
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();

		pw.write(PortalService.getInstance().mapUsers(
				new JSONArray(apiServices.getUsersIdEmails())));
		pw.flush();
		pw.close();
	}

	// get eps data by igate email
	public void getLoggedUserInfo(ActionRequest request, ActionResponse response)
			throws Exception {

		AppsProLogger.info("LOGGER:HELLO");

		User user = PortalUtil.getUser(request);
		JSONObject userData = new JSONObject();
		// plz set on localeStorage.mail if you are locally to avoid crash
		String type = request.getParameter("mail");
		System.out.println("email:" + type);
		userData.put("username", type != null && !"undefined".equals(type)
				&& type.length() > 3 ? type : user.getEmailAddress());
		// asalfawzan@stc.com.sa focal
		// "yalsaeed@stc.com.sa" supervisor
		// adowish@stc.com.sa Audtior
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.validateLogin(userData.toString());
		JSONObject updatedUserData = new JSONObject(res);
		updatedUserData.put("USERS", "[]");
		if (updatedUserData.get("STATUS").toString().equalsIgnoreCase("OK")) {
			updatedUserData.getJSONObject("DATA").put("img_id",
					user.getPortraitId());
			updatedUserData.put("TOKEN", AccountUtil
					.generateToken(updatedUserData.getJSONObject("DATA")));
			updatedUserData.put(
					"USERS",
					PortalService.getInstance().mapUsers(
							new JSONArray(apiServices.getUsersIdEmails())));
		}

		pw.write(updatedUserData.toString());
		pw.close();
	}

	// return all invoices to dashboard screen
	public void getFocal(ActionRequest request, ActionResponse response)
			throws Exception {
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getFocal();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get configration sla values
	public void getSla(ActionRequest request, ActionResponse response)
			throws Exception {
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getSla();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// ?
	public void notificationDetails(ActionRequest request,
			ActionResponse response) throws Exception {

		int id = Integer.valueOf(request.getParameter("id"));
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getNotificationDetails(id);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// ?
	public void notificationHeader(ActionRequest request,
			ActionResponse response) throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();

		String id = AccountUtil.getId(request);
		String res;
		if (id == null) {
			res = AccountUtil.createAccessDeniedResponse();
		} else {
			// int id = Integer.valueOf(request.getParameter("id"));
			res = apiServices.getNotificationHeader(Integer.valueOf(id));
		}
		pw.write(res);
		pw.flush();
		pw.close();
	}

	public void closeNotification(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		JSONObject payload = new JSONObject();

		payload.put("notificationId", request.getParameter("notificationId"));
		payload.put("userName", request.getParameter("userName"));

		String res = apiServices.closeNotificationACT(payload.toString());
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get my assigned invoices
	public void dashboardGetInvoices(ActionRequest request,
			ActionResponse response) throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();

		// int id = Integer.valueOf(request.getParameter("id"));
		String id = AccountUtil.getId(request);
		String res;
		if (id == null) {
			res = AccountUtil.createAccessDeniedResponse();
		} else {
			res = apiServices.dashboardGetInvoices(Integer.valueOf(id));
		}
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get my completed invloices
	public void dashboardCompletedInvoices(ActionRequest request,
			ActionResponse response) throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		// int id = Integer.valueOf(request.getParameter("id"));

		String id = AccountUtil.getId(request);
		String res;
		if (id == null) {
			res = AccountUtil.createAccessDeniedResponse();
		} else {

			String from = request.getParameter("from");
			String to = request.getParameter("to");
			res = apiServices.dashboardCompletedInvoices(id,from,to);
		}
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get users
	public void getUsers(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getUsers();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get invoice rejected reason
	public void rejectReason(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
//		String lang = request.getParameter("lang");
		String lang = AccountUtil.getLang(request);
		String res = apiServices.getRejectReason(lang);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	//
	public void rejectDetails(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String id = request.getParameter("id");

		String res = apiServices.getRejectDetails(id);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	public void getInvoiceHistory(ActionRequest request, ActionResponse response)
			throws Exception {
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String invoiceId = request.getParameter("id");
		String item_key = request.getParameter("item_key");

//		String lang = request.getParameter("lang");
		String lang = AccountUtil.getLang(request);
		String res = apiServices.getInvoiceHistory(invoiceId,item_key,lang);

		pw.write(res);
		pw.flush();
		pw.close();

	}

	public void getPersonInvoices(ActionRequest request, ActionResponse response)
			throws Exception {
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String personId = request.getParameter("id");

		String res = apiServices.getPersonInvoices(personId);
		System.out.println(res);

		pw.write(res);
		pw.flush();
		pw.close();

	}

	public void statistics(ActionRequest request, ActionResponse response)
			throws Exception {
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String startDate = request.getParameter("start") != null
				&& request.getParameter("start") != "undefined" ? request
				.getParameter("start") : "1/11/2019";
		String endDate = request.getParameter("end") != null
				&& request.getParameter("end") != "undefined" ? request
				.getParameter("end") : "30/11/2019";

//		String lang = request.getParameter("lang") != null
//				&& request.getParameter("lang") != "undefined" ? request
//				.getParameter("lang") : "US";
				String lang = AccountUtil.getLang(request);

		String res = apiServices.statistics(startDate, endDate, lang);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	public void weekAchievers(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String startDate = request.getParameter("start") != null
				&& request.getParameter("start") != "undefined" ? request
				.getParameter("start") : "2019-11-10";
		String endDate = request.getParameter("end") != null
				&& request.getParameter("end") != "undefined" ? request
				.getParameter("end") : "2019-11-10";

		String res = apiServices.getWeekAchievers(startDate, endDate);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	public void leaderboard(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getLeaderboard();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get emps with no skills yet(into skill matrix screen)
	public void noSkills(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getNoSkills();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get LOV skills
	public void getSkills(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getSkills();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get user skills
	public void getAllSkills(ActionRequest request, ActionResponse response)
			throws Exception {
		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getAllSkills();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// UpdateSla data (4 vals)
	public void updateSla(ActionRequest request, ActionResponse response)
			throws Exception {

		// ---get user id by token
		String id = AccountUtil.getId(request);
		String res;
		if (id == null) {
			res = AccountUtil.createAccessDeniedResponse();
		} else {
			// -----------------------
			JSONObject payload = new JSONObject();

			payload.put("invoiceReview", request.getParameter("invoiceReview"));
			payload.put("supervisorReview",
					request.getParameter("supervisorReview"));
			payload.put("taxAccountantReview",
					request.getParameter("taxAccountantReview"));
			payload.put("taxSupervisorReview",
					request.getParameter("taxSupervisorReview"));

			res = apiServices.updateSla(payload.toString());
		}

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// assign/reassign card to emp
	public void dashboardAssignAction(ActionRequest request,
			ActionResponse response) throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String payload = request.getParameter("payload");

		String res = apiServices.dashboardAssignAction(payload.trim());
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// approve / reject /send back
	public void dashboardInvoiceAction(ActionRequest request,
			ActionResponse response) throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String id = AccountUtil.getId(request);
		String res;
		if (id == null) {
			res = AccountUtil.createAccessDeniedResponse();
		} else {
			String _payload = request.getParameter("payload");
			JSONArray payloadArr = new JSONArray(_payload);
			for (int x = 0; x < payloadArr.length(); x++) {
				payloadArr.getJSONObject(x).put("PERSON_ID", id);
			}
			res = apiServices.dashboardInvoiceAction(payloadArr.toString());
		}
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// create/update skills
	public void updateSkills(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String payload = request.getParameter("payload");
		String type = request.getParameter("type");

		String res = apiServices.updateSkills(payload,
				"update".equalsIgnoreCase(type) ? "u" : "c");
		pw.write(res);
		pw.flush();
		pw.close();
	}

	public void getUsersHasNoType(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String key = request.getParameter("key");
		String res = apiServices.getUsersHasNoType(key == null ? "" : key);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// get user types
	public void getUserType(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String res = apiServices.getUserType();
		pw.write(res);
		pw.flush();
		pw.close();
	}

	public void getMyAdutior(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String pid = AccountUtil.getId(request);
//		String lang = request.getParameter("lang");
		String lang = AccountUtil.getLang(request);
		String res = apiServices.getMyAudtiorIvoices(pid,lang);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// set user type to link with igate
	public void setUserType(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();
		String payload = request.getParameter("payload");
		String method = request.getParameter("method");

		String res = apiServices.setUserType(payload, method);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	// ---------------------------
	// --------REPORTS------------
	public void getReportData(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();

		String startDate = request.getParameter("start") != null
				&& request.getParameter("start") != "undefined" ? request
				.getParameter("start") : "1/11/2019";
		String endDate = request.getParameter("end") != null
				&& request.getParameter("end") != "undefined" ? request
				.getParameter("end") : "30/11/2019";

		String res = apiServices.getReportData(startDate, endDate);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	public void getInvoiceDetails(ActionRequest request, ActionResponse response)
			throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();

		String id = request.getParameter("id");

		String res = apiServices.getInvoiceDetails(id);
		pw.write(res);
		pw.flush();
		pw.close();
	}

	public void getInvoiceAttachments(ActionRequest request,
			ActionResponse response) throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();

		String id = request.getParameter("id");

		String res = apiServices.getInvoiceAttachments(id);
		pw.write(res);
		pw.flush();
		pw.close();
	}
	
	public void getAttachmentBlob(ActionRequest request,
			ActionResponse response) throws Exception {

		PrintWriter pw = PortalUtil.getHttpServletResponse(response)
				.getWriter();

		String P_INVOICE_ID = request.getParameter("P_INVOICE_ID");
		String P_DOCUMENT_ID = request.getParameter("P_DOCUMENT_ID");

		String res = apiServices.getAttachmentBlob(P_INVOICE_ID,P_DOCUMENT_ID);
		pw.write(res);
		pw.flush();
		pw.close();
	}
}
