package com.integration.util;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.util.PortalUtil;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import javax.portlet.ActionRequest;
import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;

import org.json.JSONObject;

public class AccountUtil {

	// private static HashMap users = new HashMap();
	private static Map<String, Object> users = new HashMap<String, Object>();

	public static String generateToken() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	public static String generateToken(String prefex) {
		return prefex + UUID.randomUUID().toString().replace("-", "");
	}

	public static boolean isExist(String token) {
		return users.containsKey(token);
	}

	public static JSONObject get(String token) {
		return ((JSONObject) users.get(token));
	}

	public static String save(String token, JSONObject user) {
		users.put(token, user);
		System.out.println(token + "----" + user.toString());
		return token;
	}

	public static boolean isIdExist(String id) {
		Iterator<Object> iterator = users.values().iterator();
		while (iterator.hasNext()) {
			JSONObject obj = (JSONObject) iterator.next();
			if (obj.has("P_PERSON_ID")
					&& obj.get("P_PERSON_ID").toString().equalsIgnoreCase(id)) {
				return true;
			}
		}
		return false;
	}

	public static String getTokenById(String id) {
		Iterator<String> iterator = users.keySet().iterator();

		while (iterator.hasNext()) {
			String token = (String) iterator.next();
			if (users.get(token) != null
					&& ((JSONObject) users.get(token)).get("P_PERSON_ID")
							.toString().equalsIgnoreCase(id)) {
				return token;
			}
		}
		return null;
	}

	public static String generateToken(JSONObject user) {
		String token = getTokenById(user.getString("P_PERSON_ID"));// check if
																	// already
																	// has token
		if (token == null) {
			String type = user.getString("P_PERSON_TYPE_CODE");
			String pref = "FF";
			
			if(type.equals("FOCALPOINT"))
				pref = "FF";
			else if(type.equals("SUPERVISOR"))
				pref = "SS";
			else if(type.equals("AUDITOR"))
				pref = "AA";
			else if(type.equals("TAXAUDITOR"))
				pref = "TA";
			else if(type.equals("TAXSUPERVISOR"))
				pref = "TS";
			else if(type.equals("VP"))
				pref = "VP";
			
			token = save(generateToken(pref), user);
		}
		return token;
	}

	public static String getIdByToken(String token) {
		return get(token) != null ? get(token).getString("P_PERSON_ID") : null;
	}

	public static String getId(ActionRequest request) {
		HttpServletRequest httpsr = PortalUtil.getHttpServletRequest(request);
		String token = httpsr.getHeader("token");
		return AccountUtil.getIdByToken(token);
	}

	public static String createAccessDeniedResponse() {
		JSONObject res = new JSONObject();
		res.put("STATUS", "ERROR");
		res.put("DATA", "ACCESS DENIED");
		res.put("CODE", "403");
		return res.toString();
	}
	
	public static String getLang(ActionRequest request){
		return LanguageUtil.getLanguageId(request).contains("en")?"US":"AR";
	}

}
