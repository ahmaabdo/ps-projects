package com.integration.stc.json.mapper;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONObject;

public class NotificationMapper {

	public static String mapDetailsResponse(JSONObject res) {
		return res.toString();
	}
	
	public static Map<String, Object> mapRequest (JSONObject res)
	{
		System.out.println(res.get("notificationId"));
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		Map<String, Object> inputParameters = new LinkedHashMap<String, Object>();
		inputParameters.put("P_NOTIFICATION_ID",res.get("notificationId").toString());
		inputParameters.put("P_USERNAME",res.get("userName").toString());
		params.put("inputParameters", inputParameters);
		
		return params;		
	}

	public static String mapHeaderResponse(JSONObject res) {
		return res.toString();
	}

}
