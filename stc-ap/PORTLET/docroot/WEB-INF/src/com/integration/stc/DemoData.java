package com.integration.stc;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import org.json.JSONObject;

import sun.misc.IOUtils;
import sun.nio.cs.StandardCharsets;

/**
*
* @author Hussein
*/
public class DemoData {

	private static JSONObject file = null;
	private static DemoData instance = null;
	public static DemoData getInstance(){
		if(instance == null)
			instance = new DemoData();
		return instance;
	}
	public static String getDemo(String key){
		String res = "";
		if(file == null)
		{
			ClassLoader classLoader = getInstance().getClass().getClassLoader();
			try {
                Reader reader = new FileReader(new File(classLoader.getResource("resources/demo.json").getFile()));
                JsonElement elem = new JsonParser().parse(reader);
			    Gson gson  = new GsonBuilder().create();
			    String sss = gson.toJson(elem);
						
				file = new JSONObject(sss);
				System.out.println(file);
				res =  file.has(key)? file.get(key).toString():"{}";
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		
		}else{
			res =  file.has(key)? file.get(key).toString():"{}";
		}
		return res;
	}
	
	public static String getFileContent( InputStream fis ) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    Reader r = new InputStreamReader(fis, "UTF-8");  //or whatever encoding
	    int ch = r.read();
	    while(ch >= 0) {
	        sb.append((char)ch);
	        ch = r.read();
	    }
	    return sb.toString();
	}
}
