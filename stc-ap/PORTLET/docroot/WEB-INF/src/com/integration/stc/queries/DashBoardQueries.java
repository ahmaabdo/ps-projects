/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.integration.stc.queries;

/**
 *
 * @author Anas Alghawi
 */
public class DashBoardQueries {

    public static final String GET_INVOICE_ATTCHMENTS(String invoice_id) {
        return "SELECT  INVOICE_ID, DOCUMENT_ID, CATEGORY, FILE_NAME, TITLE  FROM XXAPRG_INVOICE_ATTACH_DOCUMENT WHERE INVOICE_ID = " + invoice_id;
    }
    
    public static final String GET_ACT_DASHBOARD(String personId, String lang) {
    	return "SELECT inv.invoice_type_lookup_code AS \"InvoiceType\", inv.INVOICE_NET_AMOUNT AS \"InvoiceNet\", inv.INVOICE_VAT AS \"InvoiceVat\",inv.APPROVAL_STATUS_DISPLAY as \"invoicStatus\", inv.reciept_num as \"recieptNo\", inv.urgent_flag_meaning AS \"UrgentFlagMeaning\", inv.urgent_flag AS \"UrgentFlag\" , inv.invoice_id as \"InvoiceId\", inv.invoice_num as \"InvoiceName\",inv.bank_name AS \"bankName\", inv.bank_account_num AS \"bankNum\" , inv.invoice_status as \"InvoiceStatus\"," +
    			"inv.Sla_value as \"Sla_value\", inv.SLA_DAY_REMAIN \"SlaRemainingDays\",(inv.SLA_DAY_REMAIN/inv.Sla_value * 100) AS \"LeftPercent\"  ," +
    			"inv.VENDOR_TYPE_DESC as \"vendorDesc\", inv.description as \"invoiceDescription\",inv.PREVIOUS_PERSON_NAME as \"previousName\", to_char(inv.invoice_date, 'DD-MON-YYYY') as \"InvoiceDate\", inv.invoice_amount as \"Amount\", inv.batch_name as \"BatchName\", inv.vendor_id as \"VendorNo\", inv.vendor_name as \"VendorName\", inv.invoice_currency_code as \"Currency\", inv.source as \"Source\", inv.source_desc as \"SourceDesc\", inv.po_number \"PONumber\", inv.vendor_type_lookup_code as \"SupplierType\", inv.SECTOR_DESC as \"TechnicalSector\", inv.EXPENSE_TYPE_DESC as \"ExpenseType\", CASE WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED' ELSE 'PENDING' END AS \"STATUS\", inv.accountant_invoice_id as \"accountantId\", NULL as \"supervisorId\", inv.priority as \"Priority\", inv.DAY_REMAIN \"remainingDays\"," +
    			"inv.item_key AS \"itemKey\",inv.person_id AS \"pesronId\",  INV.EMPLOYEE_WORK_LOAD AS \"WORKLOAD\", NVL (inv.EXCHANGE_RATE, 1) as \"EXCHANGE_RATE\", inv.DOC_SEQUENCE_VALUE as \"SEQUANCE\", inv.DISTRICT_DESC AS \"DISTRICT\", inv.ATTRIBUTE3 AS \"INITIALINVOICE\", inv.ATTRIBUTE4 AS \"SERVICETYPE\", inv.ATTRIBUTE5 AS \"SERVICEPERIOD\", inv.EXCHANGE_RATE AS \"CONVERSIONRATE\", to_char(inv.EXCHANGE_DATE, 'DD-MON-YYYY') AS \"RATEDATE\", inv.ATTRIBUTE9 AS \"DEDUCT_TYPE\", inv.ATTRIBUTE10 AS \"PORELEASE\", to_char(inv.DUE_DATE, 'DD-MON-YYYY') AS \"DUE_DATE\", inv.VENDOR_NUM AS \"VENDOR_NUM\" FROM XXAPRG_Account_invoice_details inv WHERE inv.person_id = " + personId + " order by \"LeftPercent\"  asc";
    }

//    inv.Sla_value as \"Sla_value\",
    public static final String GET_FOCAL_DASHBOARD(String lang) {
        return "SELECT " +
        		" (SELECT trunc(sysdate) - trunc(start_date) remaining_days FROM XXAPRG_ACCOUNTANT_INVOICES WHERE INVOICE_ID = inv.invoice_id and PERSON_ID = inv.person_id and item_key= inv.item_key and rownum = 1) AS \"AssignedSince\", " +
        		"inv.invoice_type_lookup_code AS \"InvoiceType\", inv.INVOICE_NET_AMOUNT AS \"InvoiceNet\", inv.INVOICE_VAT AS \"InvoiceVat\",inv.APPROVAL_STATUS_DISPLAY as \"invoicStatus\", inv.reciept_num as \"recieptNo\", inv.invoice_id AS \"InvoiceId\" ,inv.invoice_num AS \"InvoiceName\",inv.description as \"invoiceDescription\" , inv.VENDOR_TYPE_DESC as \"vendorDesc\",inv.bank_name AS \"bankName\", inv.bank_account_num AS \"bankNum\" ,to_char(inv.invoice_date, 'DD-MON-YYYY') AS \"InvoiceDate\", inv.invoice_amount AS \"Amount\", inv.urgent_flag AS \"UrgentFlag\" , inv.urgent_flag_meaning AS \"UrgentFlagMeaning\",inv.batch_name AS \"BatchName\", inv.vendor_id AS \"VendorNo\", inv.vendor_name AS \"VendorName\", inv.invoice_currency_code AS \"Currency\", inv.source AS \"Source\", inv.source_desc AS \"SourceDesc\", inv.po_number AS \"PONumber\", inv.vendor_type_lookup_code AS \"SupplierType\", inv.SECTOR_DESC AS \"TechnicalSector\", inv.EXPENSE_TYPE_DESC AS \"ExpenseType\"," +
//        		" CASE " +
//        		"WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED' " +
//        		"WHEN inv.invoice_status = 'TAXAUD-ASSIGNED' THEN 'ASSIGNED' " +
//        		"WHEN inv.invoice_status = 'TAXSUP-ASSIGNED' THEN 'ASSIGNED' " +
//        		"WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED' " +
//        		
//        		"WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED' " +
//        		"WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED' " +
//        		"WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED' " +
//        		"WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED' " +
//        		"ELSE 'PENDING' END" +
        		"inv.invoice_status AS \"STATUS\", inv.accountant_invoice_id AS \"accountantId\", NULL AS \"supervisorId\", inv.priority AS \"Priority\", inv.day_remain \"remainingDays\", inv.item_key AS \"itemKey\", inv.person_id AS \"pesronId\", inv.person_name AS \"personName\",  INV.EMPLOYEE_WORK_LOAD AS \"WORKLOAD\", NVL (inv.EXCHANGE_RATE, 1) as \"EXCHANGE_RATE\", inv.DOC_SEQUENCE_VALUE as \"SEQUANCE\", inv.DISTRICT_DESC AS \"DISTRICT\", inv.ATTRIBUTE3 AS \"INITIALINVOICE\", inv.ATTRIBUTE4 AS \"SERVICETYPE\", inv.ATTRIBUTE5 AS \"SERVICEPERIOD\", inv.EXCHANGE_RATE AS \"CONVERSIONRATE\", to_char(inv.EXCHANGE_DATE, 'DD-MON-YYYY') AS \"RATEDATE\", inv.ATTRIBUTE9 AS \"DEDUCT_TYPE\", inv.ATTRIBUTE10 AS \"PORELEASE\", to_char(inv.DUE_DATE, 'DD-MON-YYYY') AS \"DUE_DATE\", inv.VENDOR_NUM AS \"VENDOR_NUM\" FROM xxaprg_dashboard_invoices_v inv " +
        		"WHERE inv.invoice_status like 'PENDING' OR inv.invoice_status LIKE '%ASSIGNED%' order by  (case when PRIORITY = 'High' then 1 when PRIORITY = 'Medium' then 2 when PRIORITY = 'Low' then 3 else 4 end)";
    }

    public static final String GET_COMPLETED_DASHBOARD(String userId ,String startDate,String endDate) {

        return "SELECT\n"
        		+ "    inv.invoice_type_lookup_code AS \"InvoiceType\",\n"
        		+ "    inv.APPROVAL_STATUS_DISPLAY as \"invoicStatus\",\n"
        		+ "    inv.INVOICE_VAT               AS \"InvoiceVat\",\n"
        		+ "    inv.INVOICE_NET_AMOUNT        AS \"InvoiceNet\",\n"
        		+ "    inv.reciept_num               AS \"recieptNo\",\n"
                + "    inv.invoice_id                AS \"InvoiceId\",\n"
                + "    inv.invoice_num               AS \"InvoiceName\",\n"
                + "    to_char(inv.invoice_date, 'DD-MON-YYYY')              AS \"InvoiceDate\",\n"
                + "    inv.invoice_amount            AS \"Amount\",\n"
                + "    inv.batch_name                 AS \"BatchName\",\n"
                + "    inv.vendor_id                 AS \"VendorNo\",\n"
                + "    inv.vendor_name               AS \"VendorName\",\n"
                + "    inv.VENDOR_TYPE_DESC          AS \"vendorDesc\",\n"
                + "    inv.invoice_currency_code     AS \"Currency\",\n"
                + "    inv.source                    AS \"Source\",\n"
                + "    inv.po_NUMBER	             AS \"PONumber\",\n"
                + "	   inv.bank_name 				 AS \"bankName\",\n"
                + "    inv.bank_account_num			 AS \"bankNum\" ,\n"
                + "    inv.vendor_type_lookup_code   AS \"SupplierType\",\n"
                + "    inv.SECTOR_DESC               AS \"TechnicalSector\",\n"
                + "    inv.EXPENSE_TYPE_DESC               AS \"ExpenseType\",\n"
                + "    inv.accountant_invoice_id     AS \"accountant\",\n"
                + "    CASE\n"
                + "        WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED'\n"
                + "        WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED'\n"
                + "        WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED'\n"
                + "        WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED'\n"
                + "        WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED'\n"
                + "        WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED'\n"
                + "        ELSE 'PENDING'\n"
                + "    END AS \"STATUS\",\n"
                + "    inv.accountant_invoice_id     AS \"accountantId\",\n"
                + "    NULL AS \"supervisorId\",\n"
                + "    inv.item_key                  AS \"itemKey\",\n"
                + "    inv.person_id                 AS \"pesronId\",\n"
                + "    inv.action_code               AS \"ACTION\",\n"
                + "    inv.comments                  AS \"COMMENTS\",\n"
+"       inv.item_key                   AS \"itemKey\",\n" +
"       inv.person_id                  AS \"pesronId\",\n" +
"       NVL (inv.EXCHANGE_RATE, 1)     as \"EXCHANGE_RATE\", \n" +
"       inv.DOC_SEQUENCE_VALUE         as \"SEQUANCE\",\n" +
"       inv.DISTRICT_DESC               AS \"DISTRICT\",\n" +
"       inv.ATTRIBUTE3                 AS \"INITIALINVOICE\",\n" +
"       inv.ATTRIBUTE4                 AS \"SERVICETYPE\",\n" +
"       inv.ATTRIBUTE5                 AS \"SERVICEPERIOD\",\n" +
"       inv.EXCHANGE_RATE                 AS \"CONVERSIONRATE\",\n" +
"       to_char(inv.EXCHANGE_DATE, 'DD-MON-YYYY')                 AS \"RATEDATE\", \n" +
"       inv.ATTRIBUTE9                 AS \"DEDUCT_TYPE\",\n" +
"       inv.ATTRIBUTE10                AS \"PORELEASE\",\n" +
"       inv.DUE_DATE                   AS \"DUE_DATE\",                  \n" +
"       inv.VENDOR_NUM                 AS \"VENDOR_NUM\""
                + "FROM\n"
                + "    xxaprg_complete_invoices_v inv\n"
                + "WHERE\n"
                + "    trunc(action_end_date) BETWEEN TO_DATE('" + startDate + "','DD/MM/YYYY') AND TO_DATE('" + endDate + "','DD/MM/YYYY')\n"
                + "    AND person_id = " + userId + "\n"
                + "ORDER BY\n"
                + "    action_end_date";
    }

    public static final String GET_REJECTION_HEADER_LOV = "SELECT ffv.flex_value code, ffv.description FROM fnd_flex_values_vl ffv, fnd_flex_value_sets fvs WHERE fvs.flex_value_set_name = 'AP_RE_ENG_INVOICE_H' AND ffv.flex_value_set_id = fvs.flex_value_set_id";
//    public static final String GET_REJECTION_DATA = "SELECT h.MEANING header_meaning, h.DESCRIPTION header_reson, d.DESCRIPTION details_reason FROM FND_LOOKUP_VALUES_VL d, FND_LOOKUP_VALUES_VL h WHERE     (   NVL ('', d.territory_code) = d.territory_code OR d.territory_code IS NULL) AND d.lookup_type = 'XXSTC_AP_INVOICE_SECREJ_LKP' AND (d.LOOKUP_TYPE LIKE 'XXSTC_AP_INVOICE_SECREJ_LKP') AND (d.VIEW_APPLICATION_ID = 0) AND (d.SECURITY_GROUP_ID = 0) AND (   NVL ('', h.territory_code) = h.territory_code OR h.territory_code IS NULL) AND h.lookup_type = 'XXSTC_AP_INVOICE_PRIMEREJ_LKP' AND (h.LOOKUP_TYPE LIKE 'XXSTC_AP_INVOICE_PRIMEREJ_LKP') AND (h.VIEW_APPLICATION_ID = 0) AND (h.SECURITY_GROUP_ID = 0) AND d.TAG = h.LOOKUP_CODE";
    public static final String GET_REJECTION_DATA(String lang){ return  "SELECT * FROM XXAPRG_REJECTION_REASON_ALL WHERE LANGUAGE = '"+lang+"'";};
    public static final String GET_REJECTION_DETAILS_LOV (String parentCode){ 
             return "SELECT FFV.PARENT_FLEX_VALUE_LOW PARINT_CODE,FFV.FLEX_VALUE CODE,FFV.DESCRIPTION \n"
            + "   FROM FND_FLEX_VALUES_VL FFV, FND_FLEX_VALUE_SETS FVS\n"
            + "  WHERE FVS.FLEX_VALUE_SET_NAME =\n"
            + "        (SELECT FLEX_VALUE_SET_NAME\n"
            + "           FROM FND_FLEX_VALUE_SETS\n"
            + "          WHERE PARENT_FLEX_VALUE_SET_ID =\n"
            + "                (SELECT FLEX_VALUE_SET_ID\n"
            + "                   FROM FND_FLEX_VALUE_SETS\n"
            + "                  WHERE FLEX_VALUE_SET_NAME = 'AP_RE_ENG_INVOICE_H'))\n"
            + "    AND FFV.FLEX_VALUE_SET_ID = FVS.FLEX_VALUE_SET_ID\n"
            + "    and  FFV.PARENT_FLEX_VALUE_LOW = '" + parentCode + "'";
    }
    
    
//    HISTORY
    public static final String HIRERARCHICAL_INVOICE_ACTION (String invoiceId,String item_key,String lang)
    {
    	return "SELECT PERSON_ID,ACTION_MEANING,COMMENTS NOTES, to_char(ACTION_DATE, 'DD-MON-YYYY') ACTION_DATE, DECODE('"+lang+"','AR',PERSON_NAME,PERSON_NAME_E)PERSON_NAME FROM XXAPRG_INVOICE_ACTION_TREE_V WHERE INVOICE_ID = "+invoiceId+" AND LOG_WF_ITEM_KEY = nvl('"+item_key+"',LOG_WF_ITEM_KEY) ORDER BY ACTION_DATE DESC";
    }
    
    
    public static final String PERSON_INVOICES (String personId)
    {
//    	return "SELECT * FROM XXAPRG_ACCOUNTANT_INVOICES_V WHERE END_DATE IS NULL AND  PERSON_ID = "+personId+"";
    	return "SELECT PERSON_ID,PERSON_NAME,INVOICE_ID,ACCOUNTANT_INVOICE_ID,INVOICE_NUM,DESCRIPTION,INVOICE_DATE FROM XXAPRG_ACCOUNT_INVOICE_DETAILS WHERE END_DATE IS NULL AND PERSON_ID = "+personId+"";
    }
    
    public static final String GET_INVOICE_DETAILS(String id) {
        return "SELECT  inv.invoice_type_lookup_code AS \"InvoiceType\", inv.INVOICE_NET_AMOUNT AS \"InvoiceNet\", inv.INVOICE_VAT AS \"InvoiceVat\",inv.invoice_id as \"InvoiceId\", inv.invoice_num as \"InvoiceName\", inv.invoice_status as \"InvoiceStatus\", inv.Sla_value as \"Sla_value\", inv.VENDOR_TYPE_DESC as \"vendorDesc\", inv.description as \"invoiceDescription\",inv.PREVIOUS_PERSON_NAME as \"previousName\", to_char(inv.invoice_date, 'DD-MON-YYYY') as \"InvoiceDate\", inv.invoice_amount as \"Amount\", inv.batch_name as \"BatchName\", inv.vendor_id as \"VendorNo\", inv.vendor_name as \"VendorName\", inv.invoice_currency_code as \"Currency\", inv.source as \"Source\", inv.source_desc as \"SourceDesc\", inv.po_number \"PONumber\", inv.vendor_type_lookup_code as \"SupplierType\", inv.SECTOR_DESC as \"TechnicalSector\", inv.EXPENSE_TYPE_DESC as \"ExpenseType\", inv.accountant_invoice_id as \"accountant\", CASE WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED' ELSE 'PENDING' END AS \"STATUS\", inv.accountant_invoice_id as \"accountantId\", NULL as \"supervisorId\", inv.priority as \"Priority\", inv.DAY_REMAIN \"remainingDays\", inv.SLA_DAY_REMAIN \"SlaRemainingDays\", inv.item_key AS \"itemKey\",inv.person_id AS \"pesronId\",  INV.EMPLOYEE_WORK_LOAD AS \"WORKLOAD\", NVL (inv.EXCHANGE_RATE, 1) as \"EXCHANGE_RATE\", inv.DOC_SEQUENCE_VALUE as \"SEQUANCE\", inv.DISTRICT_DESC AS \"DISTRICT\", inv.ATTRIBUTE3 AS \"INITIALINVOICE\", inv.ATTRIBUTE4 AS \"SERVICETYPE\", inv.ATTRIBUTE5 AS \"SERVICEPERIOD\", inv.EXCHANGE_RATE AS \"CONVERSIONRATE\", to_char(inv.EXCHANGE_DATE, 'DD-MON-YYYY') AS \"RATEDATE\", inv.ATTRIBUTE9 AS \"DEDUCT_TYPE\", inv.ATTRIBUTE10 AS \"PORELEASE\", to_char(inv.DUE_DATE, 'DD-MON-YYYY') AS \"DUE_DATE\", inv.VENDOR_NUM AS \"VENDOR_NUM\" FROM xxaprg_invoices_v inv WHERE inv.INVOICE_ID = " + id;
    }
    
    public static final String GET_MY_ADTUIOR_INVOICES (String supervisorId,String lang){
    	return "SELECT inv.invoice_type_lookup_code AS \"InvoiceType\", inv.INVOICE_NET_AMOUNT AS \"InvoiceNet\", inv.INVOICE_VAT AS \"InvoiceVat\",inv.invoice_id as \"InvoiceId\", inv.invoice_num as \"InvoiceName\", inv.invoice_status as \"InvoiceStatus\", inv.Sla_value as \"Sla_value\", inv.VENDOR_TYPE_DESC as \"vendorDesc\", inv.description as \"invoiceDescription\",inv.PREVIOUS_PERSON_NAME as \"previousName\", to_char(inv.invoice_date, 'DD-MON-YYYY') as \"InvoiceDate\", inv.invoice_amount as \"Amount\", inv.batch_name as \"BatchName\", inv.vendor_id as \"VendorNo\", inv.vendor_name as \"VendorName\", inv.invoice_currency_code as \"Currency\", inv.source as \"Source\", inv.source_desc as \"SourceDesc\", inv.po_number \"PONumber\", inv.vendor_type_lookup_code as \"SupplierType\", inv.SECTOR_DESC as \"TechnicalSector\", inv.EXPENSE_TYPE_DESC as \"ExpenseType\", inv.accountant_invoice_id as \"accountant\", CASE WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED' ELSE 'PENDING' END AS \"STATUS\", inv.accountant_invoice_id as \"accountantId\", NULL as \"supervisorId\", inv.priority as \"Priority\", inv.DAY_REMAIN \"remainingDays\", inv.SLA_DAY_REMAIN \"SlaRemainingDays\", inv.item_key AS \"itemKey\",inv.person_id AS \"pesronId\",  INV.EMPLOYEE_WORK_LOAD AS \"WORKLOAD\", NVL (inv.EXCHANGE_RATE, 1) as \"EXCHANGE_RATE\", inv.DOC_SEQUENCE_VALUE as \"SEQUANCE\", inv.DISTRICT_DESC AS \"DISTRICT\", inv.ATTRIBUTE3 AS \"INITIALINVOICE\", inv.ATTRIBUTE4 AS \"SERVICETYPE\", inv.ATTRIBUTE5 AS \"SERVICEPERIOD\", inv.EXCHANGE_RATE AS \"CONVERSIONRATE\", to_char(inv.EXCHANGE_DATE, 'DD-MON-YYYY') AS \"RATEDATE\", inv.ATTRIBUTE9 AS \"DEDUCT_TYPE\", inv.ATTRIBUTE10 AS \"PORELEASE\", to_char(inv.DUE_DATE, 'DD-MON-YYYY') AS \"DUE_DATE\", inv.VENDOR_NUM AS \"VENDOR_NUM\",DECODE('"+lang+"','AR',person_name,person_name_e) personName FROM XXAPRG_INVOICES_SLA_HIGH_V inv WHERE  inv.SUPERVISOR_ID = '" + supervisorId + "'";
    }
    
}
