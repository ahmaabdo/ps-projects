package com.integration.stc.json.mapper;

import com.integration.stc.DemoData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class SkillsMatrixMapper {

	public static String mapResponse(JSONObject res) {
		return res.toString();
	}

	public static String mapNoSkillsResponse(JSONObject res) {
		return res.toString();
	}

	public static String mapSkillsResponse(JSONObject res) {
		
		if(res.get("STATUS").toString().equalsIgnoreCase("OK")){
		JSONArray jsonArray = res.getJSONArray("DATA");
		
        JSONObject resultJson = new JSONObject();
        JSONObject result = new JSONObject();
        result.put("STATUS", "OK");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("SECTION_CODE", jsonArray.getJSONObject(i).get("SECTION_NAME"));
            jsonObject.put("SECTION_NAME", jsonArray.getJSONObject(i).get("SECTION_NAME_MEANING"));
            jsonObject.put("SECTION_TYPE", jsonArray.getJSONObject(i).get("SECTION_TYPE"));
            jsonObject.put("SECTION_VALUE", jsonArray.getJSONObject(i).get("SECTION_VALUE"));
            
            String type = jsonArray.getJSONObject(i).getString("SECTION_TYPE");
            if(resultJson.has(type))
            	resultJson.getJSONArray(type).put(jsonObject);
            else
            {
            	JSONArray arr = new JSONArray();
            	arr.put(jsonObject);
            	resultJson.put(type, arr);
            }
        }
        result.put("DATA", resultJson);
        return result.toString();
		}else{
			return res.toString();
		}
	}

	public static String mapAllSkillsResponse(JSONObject res) {

		if(!res.get("STATUS").toString().equalsIgnoreCase("OK")){
			return res.toString();
		}
//		JSONArray skillsMatrix = new JSONArray(DemoData.getDemo("test"));
		JSONArray skillsMatrix = res.getJSONArray("DATA");

//		JSONArray result = new JSONArray();
            JSONObject skill = null;
            Map<String, JSONObject> mappedJson = new LinkedHashMap<String, JSONObject>();

            for (int i = 0; i < skillsMatrix.length(); i++) {
                skill = skillsMatrix.getJSONObject(i);
                
                String user = skill.has("PERSON_NAME")? skill.opt("PERSON_NAME").toString():"";
                String personId = skill.has("PERSON_ID")?skill.opt("PERSON_ID").toString():"";
                String workLoad = skill.has("WORKLOAD")?skill.opt("WORKLOAD").toString():"0";
                String sectionName = skill.has("SECTION_NAME")?skill.opt("SECTION_NAME").toString():"";
                String sectionNameMeaning = skill.has("SECTION_NAME_MEANING")?skill.opt("SECTION_NAME_MEANING").toString():sectionName;
//                String sectionCode = skill.has("SECTION_CODE")?skill.opt("SECTION_CODE").toString():"";
                String sectionType = skill.has("SECTION_TYPE")?skill.opt("SECTION_TYPE").toString():"";
                String sectionValue = skill.has("SECTION_VALUE")?skill.opt("SECTION_VALUE").toString():"";
                String skmdId = skill.has("SKMD_ID")?skill.opt("SKMD_ID").toString():"";
                String SKM_ID = skill.has("SKM_ID")?skill.opt("SKM_ID").toString():"";

                if(personId == null || personId == "" || personId.length() == 0 || personId == "null")
                	continue;
                
                
                JSONObject empData;
//                -----------------------------------------
                JSONObject userSkill = new JSONObject();
                userSkill.put("SKMD_ID", skmdId);
                userSkill.put("SECTION_CODE", sectionName);
                userSkill.put("SECTION_NAME", sectionNameMeaning);
                userSkill.put("SECTION_VALUE", sectionValue);
                userSkill.put("SECTION_TYPE", sectionType);
                
//                ------------------------------------------
                if(!mappedJson.containsKey(personId)){
                	empData = new JSONObject();
                	empData.put("USER", user);
                	empData.put("PERSON_ID", personId);
                	empData.put("WORKLOAD", workLoad);
                	empData.put("SKM_ID", SKM_ID);
                	empData.put("SKILLS", new JSONArray());
                    mappedJson.put(personId, empData);
                }
            	mappedJson.get(personId).getJSONArray("SKILLS").put(userSkill);
            }
            
            res.put("DATA", mappedJson.values());
            return  res.toString();
	}

	public static Map<String, Object> mapRequest(JSONObject obj) {
		
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		Map<String, Object> inputParameters = new LinkedHashMap<String, Object>();
		Map<String, Object> P_MASTER = new LinkedHashMap<String, Object>();
		Map<String, Object> P_MASTER_ITEM = new LinkedHashMap<String, Object>();

		if(obj.has("SKM_ID"))
			P_MASTER_ITEM.put("SKM_ID", obj.get("SKM_ID").toString()); 
		P_MASTER_ITEM.put("PERSON_ID", obj.get("PERSON_ID").toString()); 
		P_MASTER_ITEM.put("PO_MATCHED", "N"); 

		
		P_MASTER.put("P_MASTER_ITEM", P_MASTER_ITEM); 
		inputParameters.put("P_MASTER", P_MASTER); 
		
		inputParameters.put("P_LANGUAGE", "AR"); 

		Map<String, Object> P_DETAILS = new LinkedHashMap<String, Object>();
//		JSONArray P_DETAILS_ITEMS = new JSONArray();
		List<LinkedHashMap> P_DETAILS_ITEMS = new ArrayList<LinkedHashMap>();
		JSONArray items = obj.getJSONArray("SKILLS");

		for(int x=0;x<items.length();x++){ //{SECTION_TYPE:'',SECTIONS:[]}
//			for(int y=0;y<items.getJSONObject(x).getJSONArray("SECTIONS").length();y++){
				LinkedHashMap<String, Object> P_DETAILS_ITEM = new LinkedHashMap<String, Object>();
//				boolean isVendor = items.getJSONObject(x).get("SECTION_TYPE").toString().equalsIgnoreCase("VENDORS");

				P_DETAILS_ITEM.put("SECTION_TYPE", items.getJSONObject(x).get("SECTION_TYPE").toString());
				P_DETAILS_ITEM.put("SECTION_NAME", items.getJSONObject(x).get("SECTION_CODE").toString());
				P_DETAILS_ITEM.put("SECTION_VALUE", "Y");//items.getJSONObject(x).getJSONArray("SECTIONS").getJSONObject(y).get("SECTION_VALUE").toString()
				P_DETAILS_ITEMS.add(P_DETAILS_ITEM);
//			}
		}
		
		P_DETAILS.put("P_DETAILS_ITEM", P_DETAILS_ITEMS); 
		inputParameters.put("P_DETAILS", P_DETAILS); 

		
		
		params.put("InputParameters", inputParameters);
		return params;
	}

	public static String mapUpdateCreateResponse(JSONObject res) {
		return res.toString();
	}

}
