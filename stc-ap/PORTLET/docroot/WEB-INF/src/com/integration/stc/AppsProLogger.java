package com.integration.stc;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

public class AppsProLogger {
	
	private static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	final static Logger logger = Logger.getLogger(AppsProLogger.class);

	
	public static void log(String data){
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("logger.txt", true)));
		    String currentDateTime = formatter.format(new Date());
		    for(int x=0;x<data.split("\n").length;x++)
		    	out.println(currentDateTime+data.split("\n")[x]);
		    out.println("\n\n");
		    out.close();
		} catch (IOException e) {
			Log(e);
		}
	}
	
	public static void error(String message){
		logger.error(message+"\n\n");
	}
	public static void info(String message){
		logger.info(message+"\n\n");
	}
	
	public static void Log(Exception e){
		logger.error("APPSPRO", e);
	}
	
	public static String getMessage(Throwable e) {
		StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	e.printStackTrace(pw);
    	return sw.toString(); // stack trace as a string	
	}
	
	public static String stackTraceToString(Throwable e) {
	    StringBuilder sb = new StringBuilder();
	    for (StackTraceElement element : e.getStackTrace()) {
	        sb.append(element.toString());
	        sb.append("\n");
	    }
	    sb.append("\n\n");
	    return sb.toString();
	}

	public static String writeToString(SOAPMessage soapResponse) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			soapResponse.writeTo(baos);
			String finalString = new String(baos.toByteArray(), "UTF-8");
			baos.close();
			return finalString;
		} catch (SOAPException e) {
//			e.printStackTrace();
		} catch (IOException e) {
//			e.printStackTrace();
		}	
		return null;
	}
	
	public static String writeToString(InputStream is) {
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
	}
}
