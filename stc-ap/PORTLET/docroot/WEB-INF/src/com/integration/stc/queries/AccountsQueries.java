/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.integration.stc.queries;

/**
 *
 * @author Anas Alghawi
 */
public class AccountsQueries {

    public static final String GET_ALL_DEFINED_ACCOUNTS = "SELECT ACCOUNT_ID, PERSON_ID, PERSON_NAME, PERSON_TYPE_CODE, TO_CHAR(START_DATE,'dd-mm-yyyy') START_DATE, TO_CHAR(END_DATE,'dd-mm-yyyy') END_DATE FROM XXAPRG_ACCOUNTANTS_V";
    public static final String GET_NOT_DEFINED_ACCOUNTS = "SELECT PERSON_ID ,FULL_NAME FROM XXAPRG_NEW_ACCOUNT_V WHERE ROWNUM < 300";
    public static final String GET_EMPLOYEE_ROLE = "SELECT FLEX_VALUE CODE, DESCRIPTION FROM XXAPRG_EMPLOYEE_TYPE_V";
    public static final String GET_EMPLOYEE_EPSID_IGATEEMAIL = " SELECT * FROM XXAPRG_USER_EMAILS";
    public static final String GET_EMPLOYEE_HAS_NO_TYPE = " SELECT * FROM XXAPRG_ACCOUNT_NAME";

    public static String GET_EMPS_HAS_NO_TYPE_SEARCH(String EMP_NAME_OR_ID){
    	return "SELECT * FROM XXAPRG_ACCOUNT_NAME WHERE (PERSON_NAME LIKE '%"+EMP_NAME_OR_ID+"%' OR PERSON_NAME_E LIKE '%"+EMP_NAME_OR_ID+"%' OR PERSOIN_ID LIKE '%"+EMP_NAME_OR_ID+"%' OR EMAIL_ADDRESS LIKE '%"+EMP_NAME_OR_ID+"%') AND ROWNUM < 50";
    }

}
