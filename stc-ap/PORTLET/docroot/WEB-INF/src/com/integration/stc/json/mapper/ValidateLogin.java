package com.integration.stc.json.mapper;

import com.integration.util.AccountUtil;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

public class ValidateLogin {

	public static Map<String, Object> mapRequest(JSONObject obj){
		
		System.out.println(obj.get("username"));
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> inputParameters = new HashMap<String, Object>();
		inputParameters.put("P_IGATE_NAME",obj.get("username").toString());
		params.put("inputParameters", inputParameters);
		return params;
	}

	
	public static String mapResponse(JSONObject jsonResponse) {
		JSONObject result = new JSONObject();

		JSONObject res = new JSONObject();
		if (jsonResponse.get("STATUS").toString().equalsIgnoreCase("OK") && jsonResponse.getJSONObject("DATA").getString("X_RESULT").equalsIgnoreCase("S")) {
			jsonResponse = jsonResponse.getJSONObject("DATA");
			result.put("P_PERSON_NAME", jsonResponse.get("P_PERSON_NAME").toString());
			result.put("P_ACCOUNT_ID", jsonResponse.get("P_ACCOUNT_ID").toString());
			result.put("P_PERSON_ID", jsonResponse.get("P_PERSON_ID").toString());
			result.put("P_PERSON_TYPE_CODE", jsonResponse.get("P_PERSON_TYPE_CODE").toString());
			res.put("STATUS", "OK");
			res.put("DATA", result);
		} else {
			res.put("STATUS", "ERROR");
			res.put("DATA", "INVALID USERNAME");
        }
		
		return res.toString();
		
	}
	
	
}
