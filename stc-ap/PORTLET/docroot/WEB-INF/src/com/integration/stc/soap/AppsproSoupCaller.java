package com.integration.stc.soap;

import com.integration.stc.AppsProLogger;
import com.liferay.portal.theme.ThemeDisplay;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import sun.security.jca.GetInstance;

import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;

public class AppsproSoupCaller {

	private String actionUrl;
	private String endPoint;
	private String header;
	private String username;
	private String password;
	private String namespaceURI;
	private String baseActionUrl;
	private Map<String, Object> params;

	public AppsproSoupCaller(){
		this.baseActionUrl = "http://xmlns.oracle.com/apps/pos/soaprovider/plsql/xxaprg_pkg/";
	}
	public static String getBaseUrl(String ins) {
		if ("dev".equalsIgnoreCase(ins))
			return "divsoagatewayurl";
		else if ("bat".equalsIgnoreCase(ins))
			return "batoagatewayurl";
		else {
			String url = "divsoagatewayurl";
			try {
				if (InetAddress.getLocalHost().getHostName().contains("-bat"))
					url = "batoagatewayurl";
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			System.out.println("CALLING INSTANCE ?:" + url);
			return url;
		}
	}

	public static AppsproSoupCaller build(String soapAction,
			Map<String, Object> params) {
		AppsproSoupCaller soupCaller = new AppsproSoupCaller();

		// set header data
		soupCaller.setActionUrl(soupCaller.getBaseActionUrl() + soapAction+"/");
		soupCaller.setEndPoint(soupCaller.getProperties().getProperty(
				getBaseUrl("bat"))
				+ "/webservices/SOAProvider/plsql/xxaprg_pkg/?wsdl");
		soupCaller
				.setHeader("http://xmlns.oracle.com/apps/eam/soaprovider/plsql/xxaprg_pkg/");
		soupCaller.setUsername(soupCaller.getProperties().getProperty(
				"grantUserId"));
		soupCaller.setPassword(soupCaller.getProperties().getProperty(
				"grantPassword"));
		soupCaller
				.setNamespaceURI("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
		soupCaller.setParams(params);

		return soupCaller;
	}

	public static AppsproSoupCaller buildQuery(String query) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		Map<String, Object> inputParameters = new LinkedHashMap<String, Object>();
		inputParameters.put("SQL_QUERY", query);
		params.put("inputParameters", inputParameters);
		return build("execute_query",params);
	}

	//
	public JSONObject run(boolean run) {
		JSONObject response = new JSONObject();
		response.put("STATUS", "OK");

		// test config
		// System.out.println("hussein:"+getProperties().getProperty("myhost"));
		// -----------
		SOAPMessage body = null;
		try {
			body = createSOAPRequest(getActionUrl(), getParams());
		} catch (SOAPException e1) {
			response.put("STATUS", "ERROR");
			response.put("DATA", "SOAPException(CANT CREATE BODY):"
					+ AppsProLogger.getMessage(e1));
			e1.printStackTrace();
		} catch (IOException e1) {
			response.put("STATUS", "ERROR");
			response.put("DATA", "IOException(CANT CREATE BODY):"
					+ AppsProLogger.getMessage(e1));
			e1.printStackTrace();
		}

		if (body != null) {
			try {

				try {
					AppsProLogger.info("CALL=>\n"
							+ AppsProLogger.writeToString(body));
				} catch (Exception e) {
				}
				if (run) {
					JSONObject xmlJSONObj = XML
							.toJSONObject(callSoap(AppsProLogger
									.writeToString(body)));
					response.put("DATA", xmlJSONObj);
				} else {
					response.put("DATA", "REQUEST BUILT");
				}

			} catch (UnsupportedOperationException e) {
				response.put("STATUS", "ERROR");
				response.put("DATA", "UnsupportedOperationException:"
						+ AppsProLogger.getMessage(e));
				e.printStackTrace();
			} catch (Exception e) {
				response.put("STATUS", "ERROR");
				response.put("DATA", "Exception:" + AppsProLogger.getMessage(e));
				e.printStackTrace();
			}
		}

		return getResponse(response);
	}

	public String callSoap(SOAPMessage body)
			throws UnsupportedOperationException, SOAPException, IOException {
		// Only ever need one URL
		URL _endpoint = new URL(null, getEndPoint(), new URLStreamHandler() {

			@Override
			protected URLConnection openConnection(URL url) throws IOException {
				URL clone = new URL(url.toString());
				URLConnection connection = clone.openConnection();
				// If you cast to HttpURLConnection, you can set redirects
				// connection.setInstanceFollowRedirects (false); // no redirs
				connection.setConnectTimeout(60 * 1000); // 1 m
				connection.setReadTimeout(2 * 60 * 1000); // 2 m
				// Custom header
				connection.addRequestProperty("Developer", "Hussein");
				return connection;
			}
		});

		SOAPConnection soapConnection = SOAPConnectionFactory.newInstance()
				.createConnection();
		SOAPMessage soapResponse = soapConnection.call(body, _endpoint);
		return AppsProLogger.writeToString(soapResponse);
	}

	public String callSoap(String body) throws UnsupportedOperationException,
			SOAPException, IOException {
		System.setProperty("DUseSunHttpHandler", "true");
		byte[] buffer = new byte[body.length()];
		buffer = body.getBytes();
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		bout.write(buffer);
		byte[] b = bout.toByteArray();
		// java.net.URL url = new URL(null,ss , new
		// sun.net.www.protocol.https.Handler());
		java.net.URL url = new URL(getEndPoint());
		java.net.HttpURLConnection http;
		// if (url.getProtocol().toLowerCase().equals("https")) {
		// java.net.HttpURLConnection https =
		// (HttpsURLConnection)url.openConnection();
		// System.setProperty("DUseSunHttpHandler", "true");
		// http = https;
		// } else {
		
		http = (HttpURLConnection) url.openConnection();
		// }

		String SOAPAction = "";
		// http.setRequestProperty("Content-Length", String.valueOf(b.length));
		http.setRequestProperty("Content-Length", String.valueOf(b.length));
		http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		http.setRequestProperty("SOAPAction", SOAPAction);
		http.setRequestMethod("POST");
		http.setDoOutput(true);
		http.setDoInput(true);
		OutputStream out = http.getOutputStream();
		
		out.write(b);
		InputStream is=null;
		if(http.getResponseCode() >= 400)
			is = http.getErrorStream();
		else
		 is = http.getInputStream();

		return AppsProLogger.writeToString(is);
	}

	public void setActionUrl(String url) {
		this.actionUrl = url;
	}

	public String getActionUrl() {
		return this.actionUrl;
	}

	public void setEndPoint(String endpoint) {
		this.endPoint = endpoint;
	}

	public String getEndPoint() {
		return this.endPoint;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNamespaceURI() {
		return namespaceURI;
	}

	public void setNamespaceURI(String namespaceURI) {
		this.namespaceURI = namespaceURI;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	private Properties getProperties() {
		Properties properties = new Properties();
		ClassLoader classLoader = getClass().getClassLoader();
		try {
			properties.load(new FileInputStream(new File(classLoader
					.getResource("resources/config.properties").getFile())));
			// properties.load(new
			// FileInputStream("C:\\Users\\hp\\Desktop\\config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return properties;
	}

	private SOAPMessage createSOAPRequest(String soapAction,
			Map<String, Object> params) throws SOAPException, IOException {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		createSoapEnvelope(soapMessage, params);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);

		soapMessage.saveChanges();

		AppsProLogger.log("Request SOAP Message:=>"
				+ AppsProLogger.writeToString(soapMessage));

		return soapMessage;
	}

	private void createSoapEnvelope(SOAPMessage soapMessage,
			Map<String, Object> map) throws SOAPException {
		SOAPPart soapPart = soapMessage.getSOAPPart();

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("x1", getNamespaceURI());

		// SOAP Header
		SOAPHeader soapHeader = envelope.getHeader();
		soapHeader.addNamespaceDeclaration("x2", getHeader());

		// SOAP Security
		SOAPElement soapSecurity = soapHeader
				.addChildElement(
						"Security",
						"wsse",
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
		SOAPElement userToken = soapSecurity.addChildElement("UsernameToken",
				"wsse");

		userToken.addChildElement("Username", "wsse")
				.addTextNode(getUsername());
		userToken.addChildElement("Password", "wsse")
				.addTextNode(getPassword());

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		soapBody.addNamespaceDeclaration("ns", getActionUrl());

		addTo(soapBody, params);
	}

	private void addTo(SOAPElement element, Map<String, Object> map)
			throws SOAPException {
		// System.out.println(map.toString());
		try {
			for (Map.Entry m : map.entrySet()) {
				if (m.getValue() != null && m.getValue() != "null") {
					if (m.getValue() instanceof Map) {// nested
						addTo(element.addChildElement(m.getKey().toString(),
								"ns"), (Map) m.getValue());
					} else if (m.getValue() instanceof List) {
						List<LinkedHashMap> items = (ArrayList<LinkedHashMap>) m
								.getValue();
						for (int x = 0; x < items.size(); x++) {
							addTo(element.addChildElement(
									m.getKey().toString(), "ns"),
									(LinkedHashMap<String, Object>) items
											.get(x));// todo
														// linkedhashmap
						}
					} else {// number, string, ...
						element.addChildElement(m.getKey().toString(), "ns")
								.addTextNode(m.getValue().toString());
					}
				}
			}
		} catch (SOAPException se) {
			throw new SOAPException("error in addTo " + map.toString(), se);
		}
	}

	public static JSONObject getResponse(JSONObject res) {

		AppsProLogger.info("getResponse=>\n" + res.toString());
		JSONObject response = new JSONObject();

		// if status ok no error happened
		if (res.get("STATUS").toString().equalsIgnoreCase("OK")) {
			JSONObject DATA = res.getJSONObject("DATA");

			// check if error response
			if (DATA.has("env:Envelope")
					&& DATA.getJSONObject("env:Envelope").has("env:Body")
					&& DATA.getJSONObject("env:Envelope")
							.getJSONObject("env:Body").has("env:Fault")) {

				response.put("STATUS", "ERROR");
				// get error message
				response.put("DATA", DATA.getJSONObject("env:Envelope")
						.getJSONObject("env:Body").getJSONObject("env:Fault")
						.getJSONObject("faultstring").get("content").toString());

			} else if (DATA.has("env:Envelope")
					&& DATA.getJSONObject("env:Envelope").has("env:Body")
					&& DATA.getJSONObject("env:Envelope")
							.getJSONObject("env:Body").has("OutputParameters")) {

				JSONObject output = DATA.getJSONObject("env:Envelope")
						.getJSONObject("env:Body")
						.getJSONObject("OutputParameters");
				response.put("STATUS", "OK");

				if (output.has("EXECUTE_QUERY")) {
					JSONArray rows = new JSONArray();
					if (output.get("EXECUTE_QUERY").toString().contains("{")
							&& output.getJSONObject("EXECUTE_QUERY").has("Row")) {
						JSONArray _rows;
						try {
							_rows = output.getJSONObject("EXECUTE_QUERY")
									.getJSONArray("Row");
						} catch (Exception e) {
							_rows = new JSONArray();
							_rows.put(output.getJSONObject("EXECUTE_QUERY")
									.getJSONObject("Row"));
						}
						for (int x = 0; x < _rows.length(); x++) {
							JSONObject record = new JSONObject();
							JSONArray col = _rows.getJSONObject(x)
									.getJSONArray("Column");

							for (int y = 0; y < col.length(); y++) {
								record.put(
										col.getJSONObject(y).getString("name"),
										col.getJSONObject(y).has("content") ? col
												.getJSONObject(y)
												.get("content").toString()
												: JSONObject.NULL);
							}
							rows.put(record);
						}
					}
					response.put("DATA", rows);
				} else
					response.put("DATA", output);
			} else {
				response.put("STATUS", "ERROR");
				// ["env:Envelope"]["env:Body"]["env:Fault"]["faultstring"]["content"]
				response.put("DATA", "Internal Server Error:"
						+ res.get("DATA").toString());
			}
			return response;
		} else
			// error happened so forward to front
			return res;
	}

	public String getBaseActionUrl() {
		return baseActionUrl;
	}

	public void setBaseActionUrl(String baseActionUrl) {
		this.baseActionUrl = baseActionUrl;
	}
}
