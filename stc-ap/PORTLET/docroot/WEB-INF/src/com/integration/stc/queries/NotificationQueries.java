/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.integration.stc.queries;

/**
 *
 * @author Anas Alghawi
 */
public class NotificationQueries {
    
    public static final String GET_NOTIFICATION_HEADER(String personId) {

        return "SELECT INVOICE_ID, NOTIFICATION_ID, SUBJECT FROM XXAPRG_NOTIFICATIONS WHERE EMPLOYEE_ID = "+ personId +" ORDER BY BEGIN_DATE";
    }
    public static final String GET_NOTIFICATION_DETAILS(String notificationId) {
//        return "SELECT * FROM XXAPRG_NOTIFICATIONS WHERE NOTIFICATION_ID = "+ notificationId + " ORDER BY BEGIN_DATE";
    	return "SELECT inv.invoice_id as \"InvoiceId\", inv.invoice_num as \"InvoiceName\", inv.invoice_status as \"InvoiceStatus\", inv.VENDOR_TYPE_DESC as \"vendorDesc\", inv.description as \"invoiceDescription\", to_char(inv.invoice_date, 'DD-MON-YYYY') as \"InvoiceDate\", inv.invoice_amount as \"Amount\", inv.batch_name as \"BatchName\", inv.vendor_id as \"VendorNo\", inv.vendor_name as \"VendorName\", inv.invoice_currency_code as \"Currency\", inv.source as \"Source\", inv.source_desc as \"SourceDesc\", inv.po_number \"PONumber\", inv.vendor_type_lookup_code as \"SupplierType\", inv.attribute2 as \"TechnicalSector\", inv.EXPENSE_TYPE_DESC as \"ExpenseType\", CASE WHEN inv.invoice_status = 'AUD-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'AUD-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'AUD-REJECTED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-ASSIGNED' THEN 'ASSIGNED' WHEN inv.invoice_status = 'SUP-APPROVED' THEN 'COMPLETED' WHEN inv.invoice_status = 'SUP-REJECTED' THEN 'COMPLETED' ELSE 'PENDING' END AS \"STATUS\", NULL as \"supervisorId\",inv.person_id AS \"pesronId\", NVL (inv.EXCHANGE_RATE, 1) as \"EXCHANGE_RATE\", inv.DOC_SEQUENCE_VALUE as \"SEQUANCE\", inv.DISTRICT_DESC AS \"DISTRICT\", inv.ATTRIBUTE3 AS \"INITIALINVOICE\", inv.ATTRIBUTE4 AS \"SERVICETYPE\", inv.ATTRIBUTE5 AS \"SERVICEPERIOD\", inv.ATTRIBUTE6 AS \"CONVERSIONRATE\", inv.ATTRIBUTE7 AS \"RATEDATE\", inv.ATTRIBUTE9 AS \"DEDUCT_TYPE\", inv.ATTRIBUTE10 AS \"PORELEASE\", to_char(inv.DUE_DATE, 'DD-MON-YYYY') AS \"DUE_DATE\", inv.VENDOR_NUM AS \"VENDOR_NUM\" FROM  XXAPRG_NOTIFICATIONS inv WHERE inv.NOTIFICATION_ID = "+ notificationId;
    }
    
}
