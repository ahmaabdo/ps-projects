package com.integration.stc.json.mapper;

import org.json.JSONArray;
import org.json.JSONObject;

public class ReportsMapper {

	public static String mapResponse(JSONObject response) {
		JSONObject obj = new JSONObject();
		JSONObject element;
		obj.put("STATUS", "OK");
		
		JSONArray arr = new JSONArray();
		
		element = new JSONObject();
		element.put("title", "Top invoices vendors");
		element.put("DATA", response.get("TOP_INVOICES_VENDOR"));
		arr.put(element);
		
		element = new JSONObject();
		element.put("title", "Top invoices Spent By Source");
		element.put("DATA", response.get("GET_TOTAL_INVOICE_SPENT_BY_SOURCE"));
		arr.put(element);
		
		element = new JSONObject();
		element.put("title", "Top invoices Spent By Months");
		element.put("DATA", response.get("GET_TOTAL_INVOICE_SPENT_BY_MONTH"));
		arr.put(element);
		
		element = new JSONObject();
		element.put("title", "Top invoices Spent By Expense Type");
		element.put("DATA", response.get("GET_TOTAL_INVOICE_SPENT_BY_EXPESNE_TYPE"));
		arr.put(element);
		
		element = new JSONObject();
		element.put("title", "Top invoices Spent By Sector");
		element.put("DATA", response.get("GET_TOTAL_INVOICE_SPENT_BY_SECTOR"));
		arr.put(element);
		
		
		obj.put("DATA", arr);
		return obj.toString();
	}

	public static JSONObject mapOverviewResponse(JSONObject response) {

		JSONObject obj = new JSONObject();
		JSONObject element;
		obj.put("STATUS", "OK");
		
		JSONArray arr = new JSONArray();
		
		element = new JSONObject();
		element.put("title", "Top Approved and Pending invoices  Per  Sector");
		element.put("CODE", "TOTAL_APPROVED_PINDING_INVOICES_PER_SECTOR");
		element.put("DATA", response.get("TOTAL_APPROVED_PINDING_INVOICES_PER_SECTOR"));
		arr.put(element);
		
		element = new JSONObject();
		element.put("title", "Top Assigned and Approved invoices Per Employee");
		element.put("CODE", "TOTAL_ASSIGN_APPROVED_INVOICE_PER_EMPLOYEE");
		element.put("DATA", response.get("TOTAL_ASSIGN_APPROVED_INVOICE_PER_EMPLOYEE"));
		arr.put(element);
		
		element = new JSONObject();
		element.put("title", "Top Rejected invoices By Sector");
		element.put("CODE", "TOTAL_REJECTED_INVOICES_PER_SECTOR");
		element.put("DATA", response.get("TOTAL_REJECTED_INVOICES_PER_SECTOR"));
		arr.put(element);
		
		obj.put("DATA", arr);
		return obj;
	}

}
