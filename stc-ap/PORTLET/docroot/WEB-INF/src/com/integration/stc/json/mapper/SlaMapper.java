package com.integration.stc.json.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class SlaMapper {

	public static String mapResponse(JSONObject res) {
		if (res.get("STATUS").toString().equalsIgnoreCase("OK")) {
			if (res.getJSONArray("DATA").length() > 0) {

				JSONObject response = new JSONObject();
				
				for (int i = 0; i < res.getJSONArray("DATA").length(); i++) {
					if (res.getJSONArray("DATA").getJSONObject(i)
							.getString("CONF_NAME")
							.equalsIgnoreCase("SUPERVISOR REVIEW")) {
						response.put("supervisorReview",
								res.getJSONArray("DATA").getJSONObject(i)
										.getString("CONF_VALUE"));
					}
					if (res.getJSONArray("DATA").getJSONObject(i)
							.getString("CONF_NAME")
							.equalsIgnoreCase("ACCOUNTANT REVIEW")) {
						response.put("invoiceReview", res.getJSONArray("DATA")
								.getJSONObject(i).getString("CONF_VALUE"));
					}
					if (res.getJSONArray("DATA").getJSONObject(i)
							.getString("CONF_NAME")
							.equalsIgnoreCase("TAX ACCOUNTANT REVIEW")) {
						response.put("taxAccountantReview",
								res.getJSONArray("DATA").getJSONObject(i)
										.getString("CONF_VALUE"));
					}
					if (res.getJSONArray("DATA").getJSONObject(i)
							.getString("CONF_NAME")
							.equalsIgnoreCase("TAX SUPERVISOR REVIEW")) {
						response.put("taxSupervisorReview",
								res.getJSONArray("DATA").getJSONObject(i)
										.getString("CONF_VALUE"));
					}
				}
				res.put("DATA", response);
			}
		}
		return res.toString();
	}

	public static Map<String, Object> mapRequest(JSONObject payload) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		Map<String, Object> inputParameters = new LinkedHashMap<String, Object>();
		Map<String, Object> P_CONFIG = new LinkedHashMap<String, Object>();
		List<LinkedHashMap> P_CONFIG_ITEMS = new ArrayList<LinkedHashMap>();

		if (payload.has("invoiceReview")) {
			LinkedHashMap<String, Object> P_CONFIG_ITEM = new LinkedHashMap<String, Object>();
			P_CONFIG_ITEM.put("CONF_TYPE", "SLA");
			P_CONFIG_ITEM.put("CONF_NAME", "ACCOUNTANT REVIEW");
			P_CONFIG_ITEM.put("CONF_VALUE", payload.get("invoiceReview")
					.toString());
			P_CONFIG_ITEM.put("CONF_LANG", "US");
			P_CONFIG_ITEM.put("CONF_PROMPT", "Accountant Review");
			P_CONFIG_ITEMS.add(P_CONFIG_ITEM);
		}
		if (payload.has("supervisorReview")) {
			LinkedHashMap<String, Object> P_CONFIG_ITEM = new LinkedHashMap<String, Object>();
			P_CONFIG_ITEM.put("CONF_TYPE", "SLA");
			P_CONFIG_ITEM.put("CONF_NAME", "SUPERVISOR REVIEW");
			P_CONFIG_ITEM.put("CONF_VALUE", payload.get("supervisorReview")
					.toString());
			P_CONFIG_ITEM.put("CONF_LANG", "US");
			P_CONFIG_ITEM.put("CONF_PROMPT", "Supervisor Review");
			P_CONFIG_ITEMS.add(P_CONFIG_ITEM);
		}
		if (payload.has("taxAccountantReview")) {
			LinkedHashMap<String, Object> P_CONFIG_ITEM = new LinkedHashMap<String, Object>();
			P_CONFIG_ITEM.put("CONF_TYPE", "SLA");
			P_CONFIG_ITEM.put("CONF_NAME", "TAX ACCOUNTANT REVIEW");
			P_CONFIG_ITEM.put("CONF_VALUE", payload.get("taxAccountantReview")
					.toString());
			P_CONFIG_ITEM.put("CONF_LANG", "US");
			P_CONFIG_ITEM.put("CONF_PROMPT", "Tax Accountant Review");
			P_CONFIG_ITEMS.add(P_CONFIG_ITEM);
		}
		if (payload.has("taxSupervisorReview")) {
			LinkedHashMap<String, Object> P_CONFIG_ITEM = new LinkedHashMap<String, Object>();
			P_CONFIG_ITEM.put("CONF_TYPE", "SLA");
			P_CONFIG_ITEM.put("CONF_NAME", "TAX SUPERVISOR REVIEW");
			P_CONFIG_ITEM.put("CONF_VALUE", payload.get("taxSupervisorReview")
					.toString());
			P_CONFIG_ITEM.put("CONF_LANG", "US");
			P_CONFIG_ITEM.put("CONF_PROMPT", "Tax Supervisor Review");

			P_CONFIG_ITEMS.add(P_CONFIG_ITEM);
		}

		P_CONFIG.put("P_CONFIG_ITEM", P_CONFIG_ITEMS);
		inputParameters.put("P_CONFIG", P_CONFIG);
		params.put("inputParameters", inputParameters);
		return params;
	}

	public static String mapUpdateResponse(JSONObject res) {
		return res.toString();
	}

	public static String mapInvoiceActionResponse(JSONObject res) {
		return res.toString();
	}

}
