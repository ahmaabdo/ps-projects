package com.integration.stc.json.mapper;

import com.integration.serv.ApiServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;


import org.json.JSONArray;
import org.json.JSONObject;

public class DashboardMapper {

	public static Map<String, Object> mapRequest(JSONArray payload) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		Map<String, Object> inputParameters = new LinkedHashMap<String, Object>();
		Map<String, Object> P_ACTION_DETAILS = new LinkedHashMap<String, Object>();
		List<LinkedHashMap> P_ACTION_DETAILS_ITEMS = new ArrayList<LinkedHashMap>();

		for(int x=0;x<payload.length();x++) 
		{
			LinkedHashMap<String, Object> P_ACTION_DETAILS_ITEM = new LinkedHashMap<String, Object>();
			if(payload.getJSONObject(x).has("PERSON_ID"))
				P_ACTION_DETAILS_ITEM.put("PERSON_ID", payload.getJSONObject(x).get("PERSON_ID").toString());
			if(payload.getJSONObject(x).has("INVOICE_ID"))
				P_ACTION_DETAILS_ITEM.put("INVOICE_ID", payload.getJSONObject(x).get("INVOICE_ID").toString());
			if(payload.getJSONObject(x).has("ACCOUNTANT_INVOICE_ID"))
				P_ACTION_DETAILS_ITEM.put("ACCOUNTANT_INVOICE_ID", payload.getJSONObject(x).get("ACCOUNTANT_INVOICE_ID").toString());
//			if(payload.getJSONObject(x).has("ITEM_KEY"))
//				P_ACTION_DETAILS_ITEM.put("ITEM_KEY", payload.getJSONObject(x).get("ITEM_KEY").toString());
			if(payload.getJSONObject(x).has("NOTE"))
				P_ACTION_DETAILS_ITEM.put("NOTE", payload.getJSONObject(x).get("NOTE").toString());
			
			P_ACTION_DETAILS_ITEMS.add(P_ACTION_DETAILS_ITEM);
		}
		P_ACTION_DETAILS.put("P_ACTION_DETAILS_ITEM", P_ACTION_DETAILS_ITEMS); 
		inputParameters.put("P_ACTION_DETAILS", P_ACTION_DETAILS); 
		if(payload.getJSONObject(0).has("URGENT_FLAG"))
			inputParameters.put("P_URGENT_FLAG", payload.getJSONObject(0).get("URGENT_FLAG").toString()); 
		params.put("InputParameters", inputParameters);
		return params;
	}

	public static String mapResponse(JSONObject res) {
		return res.toString();
	}

	public static String mapFocalResponse(JSONObject res) {
		if(res.get("STATUS").toString().equalsIgnoreCase("ERROR")){
			return res.toString();
		}
		JSONArray jsonArray= res.getJSONArray("DATA");
        JSONObject slaJsonObject = new JSONObject(ApiServices.getInstance().getSla());
        for (int i = 0; i < jsonArray.length(); i++) {
            jsonArray.getJSONObject(i).put("SLA", slaJsonObject);
        }
        res.put("DATA", jsonArray);
		return res.toString();
	}

	public static String mapInvicesResponse(JSONObject res) {
		return mapFocalResponse(res);//same code
	}

	public static String mapCompletedResponse(JSONObject res) {
		return res.toString();//
	}

	public static String mapRejectReasonResponse(JSONObject res) {
		JSONArray data = res.getJSONArray("DATA");
		JSONArray response = new JSONArray();
		System.out.println(data);
		for(int x=0;x<data.length();x++){
			JSONObject obj = data.getJSONObject(x);
			String HEADER_CODE = obj.get("HEADER_CODE").toString();
			String HEAD_DESC = obj.get("HEAD_DESC").toString();
			String DETAIL_CODE = obj.get("DETAIL_CODE").toString();
			String DETAIL_DESC = obj.get("DETAIL_DESC").toString();
			
			JSONObject subObj = new JSONObject();
			subObj.put("DETAIL_CODE", DETAIL_CODE);
			subObj.put("DETAIL_DESC", DETAIL_DESC);
			
			boolean isExist = false;
			for(int y=0;y<response.length();y++){
				if(HEADER_CODE.equalsIgnoreCase(response.getJSONObject(y).get("HEADER_CODE").toString())){
					response.getJSONObject(y).getJSONArray("details").put(subObj);
					isExist = true;
					break;
				}
			}
			
			if(!isExist){
				JSONObject resObj = new JSONObject();
				resObj.put("HEADER_CODE", HEADER_CODE);
				resObj.put("HEAD_DESC", HEAD_DESC);
				
				JSONArray newa = new JSONArray();
				newa.put(subObj);
				resObj.put("details",newa);

				response.put(resObj);
			}
		}
		res.put("DATA", response);
		return res.toString();
	}

	public static String mapRejectDeatailsResponse(JSONObject res) {
		return res.toString();
	}
	
	public static String getInvoiceHistoryresponse(JSONObject res) {
		return res.toString();
	}

	public static Map<String, Object> mapInvoiceAtionRequest(JSONArray payload) {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> inputParameters = new HashMap<String, Object>();
		Map<String, Object> P_ACTION_DETAILS = new HashMap<String, Object>();
		List<LinkedHashMap> P_ACTION_DETAILS_ITEMS = new ArrayList<LinkedHashMap>();

		for(int x=0;x<payload.length();x++) 
		{
			LinkedHashMap<String, Object> P_ACTION_DETAILS_ITEM = new LinkedHashMap<String, Object>();
			if(payload.getJSONObject(x).has("PERSON_ID"))
				P_ACTION_DETAILS_ITEM.put("PERSON_ID", payload.getJSONObject(x).get("PERSON_ID").toString());
			if(payload.getJSONObject(x).has("INVOICE_ID"))
				P_ACTION_DETAILS_ITEM.put("INVOICE_ID", payload.getJSONObject(x).get("INVOICE_ID").toString());
			if(payload.getJSONObject(x).has("ACCOUNTANT_INVOICE_ID"))
				P_ACTION_DETAILS_ITEM.put("ACCOUNTANT_INVOICE_ID", payload.getJSONObject(x).get("ACCOUNTANT_INVOICE_ID").toString());
			if(payload.getJSONObject(x).has("ItemKey"))
				P_ACTION_DETAILS_ITEM.put("ITEM_KEY", payload.getJSONObject(x).get("ItemKey").toString());
			if(payload.getJSONObject(x).has("ITEM_KEY"))
				P_ACTION_DETAILS_ITEM.put("ITEM_KEY", payload.getJSONObject(x).get("ITEM_KEY").toString());
			if(payload.getJSONObject(x).has("NOTE"))
				P_ACTION_DETAILS_ITEM.put("NOTE", payload.getJSONObject(x).get("NOTE").toString());
			
			P_ACTION_DETAILS_ITEMS.add(P_ACTION_DETAILS_ITEM);
		}
		P_ACTION_DETAILS.put("P_ACTION_DETAILS_ITEM", P_ACTION_DETAILS_ITEMS); 
		inputParameters.put("P_ACTION_DETAILS", P_ACTION_DETAILS); 
		params.put("InputParameters", inputParameters);
		return params;
	}

	public static Map<String, Object> mapAttchmentBlobRequest(
			String p_INVOICE_ID, String p_DOCUMENT_ID) {

		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> inputParameters = new HashMap<String, Object>();
	
		inputParameters.put("P_INVOICE_ID", p_INVOICE_ID);
		inputParameters.put("P_DOCUMENT_ID", p_DOCUMENT_ID);
		params.put("InputParameters", inputParameters);
		return params;
	}

}
