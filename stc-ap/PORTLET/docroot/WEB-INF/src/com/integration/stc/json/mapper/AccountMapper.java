package com.integration.stc.json.mapper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class AccountMapper {
	public static Map<String, Object> mapRequest(JSONArray payload) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		Map<String, Object> inputParameters = new LinkedHashMap<String, Object>();
		Map<String, Object> P_ACCOUNT = new LinkedHashMap<String, Object>();
		List<LinkedHashMap<String, Object>> P_ACCOUNT_ITEMS = new ArrayList<LinkedHashMap<String, Object>>();

		for (int x = 0; x < payload.length(); x++) {
			LinkedHashMap<String, Object> P_ACCOUNT_ITEM = new LinkedHashMap<String, Object>();
			if (payload.getJSONObject(x).has("ACCOUNT_ID"))
				P_ACCOUNT_ITEM.put("ACCOUNT_ID",
						payload.getJSONObject(x).get("ACCOUNT_ID").toString());
			P_ACCOUNT_ITEM.put("PERSON_ID",
					payload.getJSONObject(x).get("PERSON_ID").toString());
			String code = payload.getJSONObject(x).get("PERSON_TYPE_CODE")
					.toString();
			if (code.equalsIgnoreCase("-1")) {
				P_ACCOUNT_ITEM.put("END_DATE", "1993-11-10");
			} else if(code.equalsIgnoreCase("ACTIVE")) {
				
				P_ACCOUNT_ITEM.put("END_DATE", "4712-12-31");
			}else
				P_ACCOUNT_ITEM.put("PERSON_TYPE_CODE", code);

			P_ACCOUNT_ITEMS.add(P_ACCOUNT_ITEM);
		}
		P_ACCOUNT.put("P_ACCOUNT_ITEM", P_ACCOUNT_ITEMS);
		inputParameters.put("P_ACCOUNT", P_ACCOUNT);
		params.put("InputParameters", inputParameters);
		return params;
	}

	public static String mapResponse(JSONObject res) {
		JSONObject response = new JSONObject();
		if (res.getJSONObject("DATA").has("X_RESULT")
				&& res.getJSONObject("DATA").get("X_RESULT").toString()
						.equalsIgnoreCase("S")) {
			response.put("STATUS", "OK");
		} else {
			response.put("STATUS", "ERROR");
			if (res.getJSONObject("DATA").has("X_MESSAGE"))
				response.put("DATA", res.getJSONObject("DATA").get("X_MESSAGE")
						.toString());
		}
		return response.toString();
	}
}
