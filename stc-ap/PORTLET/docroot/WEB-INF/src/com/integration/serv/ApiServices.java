package com.integration.serv;

import com.integration.stc.BackendHelper;
import com.integration.stc.DemoData;
import com.integration.stc.json.mapper.AccountMapper;
import com.integration.stc.json.mapper.DashboardMapper;
import com.integration.stc.json.mapper.NotificationMapper;
import com.integration.stc.json.mapper.ReportsMapper;
import com.integration.stc.json.mapper.SkillsMatrixMapper;
import com.integration.stc.json.mapper.SlaMapper;
import com.integration.stc.json.mapper.ValidateLogin;
import com.integration.stc.queries.AccountsQueries;
import com.integration.stc.queries.DashBoardQueries;
import com.integration.stc.queries.NotificationQueries;
import com.integration.stc.queries.ReportsQueries;
import com.integration.stc.queries.SkillsMatrixQueries;
import com.integration.stc.queries.SlaQueries;
import com.integration.stc.soap.AppsproSoupCaller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

/**
*/
public class ApiServices {
	private static ApiServices instance = null;

	// private static Gson gson = new Gson();

	public static ApiServices getInstance() {
		if (instance == null)
			instance = new ApiServices();
		return instance;
	}

	private static String baseUrl = "http://localhost:8081/ap_integration/";
	private static final String username = "stc";
	private static final String password = "password";

	public String getFocal() {
		if (!BackendHelper.isCallingLocally)
			return call("dashboard/get-invoices/focal");
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getFocal");

			JSONObject res = AppsproSoupCaller.buildQuery(
					DashBoardQueries.GET_FOCAL_DASHBOARD("AR")).run(true);
			return DashboardMapper.mapFocalResponse(res);
		}
	}

	public String getSla() {
		if (!BackendHelper.isCallingLocally)
			return DemoData.getDemo("getSla");
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getSla");

			JSONObject res = AppsproSoupCaller.buildQuery(
					SlaQueries.XXAPRG_CONFIGRATION_V).run(true);
			return SlaMapper.mapResponse(res);

		}
	}

	public String getUsersIdEmails() {
		if (BackendHelper.IS_STATIC)
			return "[]";

		JSONObject res = AppsproSoupCaller.buildQuery(
				AccountsQueries.GET_EMPLOYEE_EPSID_IGATEEMAIL).run(true);
		return res.get("DATA").toString();
	}

	public String getNotificationDetails(int param1) {
		if (!BackendHelper.isCallingLocally)
			return call("notification/details/" + param1);
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getNotificationDetails");

			JSONObject res = AppsproSoupCaller.buildQuery(
					NotificationQueries.GET_NOTIFICATION_DETAILS(param1 + ""))
					.run(true);
			return NotificationMapper.mapHeaderResponse(res);
		}
	}

	public String getNotificationHeader(int param1) {

		if (!BackendHelper.isCallingLocally)
			return call("notification/header/" + param1);
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getNotificationHeader");

			JSONObject res = AppsproSoupCaller.buildQuery(
					NotificationQueries.GET_NOTIFICATION_HEADER(param1 + ""))
					.run(true);
			return NotificationMapper.mapHeaderResponse(res);
		}
	}

	public String closeNotificationACT(String payload) {
		if (!BackendHelper.isCallingLocally)
			return call("notification/header/");

		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getNotificationHeader");

			JSONObject jsonResponse = AppsproSoupCaller
					.build("wf_close_fyi_notification",
							NotificationMapper.mapRequest(new JSONObject(
									payload))).run(true);
			return NotificationMapper.mapHeaderResponse(jsonResponse);
		}
	}

	public String dashboardGetInvoices(int param1) {
		if (!BackendHelper.isCallingLocally)
			return call("dashboard/get-invoices/" + param1);
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("dashboardGetInvoices");

			JSONObject res = AppsproSoupCaller.buildQuery(
					DashBoardQueries.GET_ACT_DASHBOARD(param1 + "", "AR")).run(
					true);
			return DashboardMapper.mapInvicesResponse(res);
		}
	}

	public String dashboardCompletedInvoices(String id,String from,String to) {

		if (!BackendHelper.isCallingLocally)
			return DemoData.getDemo("dashboardCompletedInvoices");
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("dashboardCompletedInvoices");


			JSONObject res = AppsproSoupCaller.buildQuery(
					DashBoardQueries.GET_COMPLETED_DASHBOARD(id ,from,to)).run(true);
			return DashboardMapper.mapCompletedResponse(res);
		}
	}

	public String getUsers() {
		if (!BackendHelper.isCallingLocally)
			return call("mainservice/get-users", "GET", null);
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getUsers");

			JSONObject res = AppsproSoupCaller.buildQuery(
					SkillsMatrixQueries.GET_EMPLOYEES).run(true);
			return SkillsMatrixMapper.mapResponse(res);
		}
	}

	public String getRejectReason(String lang) {
		if (!BackendHelper.isCallingLocally)
			return call("dashboard/header-rejection-reason");
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getRejectReason");

			JSONObject res = AppsproSoupCaller.buildQuery(
					DashBoardQueries.GET_REJECTION_DATA(lang)).run(true);
			return DashboardMapper.mapRejectReasonResponse(res);
		}
	}

	public String getRejectDetails(String param1) {
		if (!BackendHelper.isCallingLocally)
			return call("dashboard/details-rejection-reason/" + param1);
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getRejectDetails");

			JSONObject res = AppsproSoupCaller.buildQuery(
					DashBoardQueries.GET_REJECTION_DETAILS_LOV(param1 + ""))
					.run(true);
			return DashboardMapper.mapRejectDeatailsResponse(res);
		}
	}

	public String getInvoiceHistory(String invoiceId,String item_key, String lang) {
		if (!BackendHelper.isCallingLocally)
			return call("getInvoiceHistory");
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getRejectDetails");

			JSONObject res = AppsproSoupCaller.buildQuery(
					DashBoardQueries.HIRERARCHICAL_INVOICE_ACTION(invoiceId,item_key,lang)).run(true);
			return DashboardMapper.getInvoiceHistoryresponse(res);
		}
	}

	public String statistics(String startDate, String endDate,String lang) {
		JSONObject response = new JSONObject();
		response.put("STATUS", "OK");
		JSONObject data = new JSONObject();

		JSONObject leaders = BackendHelper.IS_STATIC ? new JSONObject(
				DemoData.getDemo("getLeaderboard")) : AppsproSoupCaller
				.buildQuery(ReportsQueries.GET_TOP_TEN(startDate, endDate))
				.run(true);
//		JSONObject achieverPerMonth = BackendHelper.IS_STATIC ? new JSONObject(
//				DemoData.getDemo("getWeekAchievers")) : AppsproSoupCaller
//				.buildQuery(
//						ReportsQueries.GET_TOP_ACHIEVERS_PER_MONTH(startDate,
//								endDate)).run(true);
		JSONObject achieverPerWeek = BackendHelper.IS_STATIC ? new JSONObject(
				DemoData.getDemo("getWeekAchievers")) : AppsproSoupCaller
				.buildQuery(
						ReportsQueries.GET_TOP_ACHIEVERS_PER_WEEK(startDate,
								endDate)).run(true);

		data.put("leaders", leaders);
//		data.put("achieverPerMonth", achieverPerMonth);
		data.put("achieverPerWeek", achieverPerWeek);

		JSONObject overview = new JSONObject();

		if (!BackendHelper.IS_STATIC) {
			JSONObject TOTAL_APPROVED_PINDING_INVOICES_PER_SECTOR = AppsproSoupCaller
					.buildQuery(
							ReportsQueries
									.TOTAL_APPROVED_PINDING_INVOICES_PER_SECTOR(
											startDate, endDate)).run(true);
			overview.put("TOTAL_APPROVED_PINDING_INVOICES_PER_SECTOR",
					TOTAL_APPROVED_PINDING_INVOICES_PER_SECTOR);

			JSONObject TOTAL_ASSIGN_APPROVED_INVOICE_PER_EMPLOYEE = AppsproSoupCaller
					.buildQuery(
							ReportsQueries
									.TOTAL_ASSIGN_APPROVED_INVOICE_PER_EMPLOYEE(
											startDate, endDate, lang))
					.run(true);
			overview.put("TOTAL_ASSIGN_APPROVED_INVOICE_PER_EMPLOYEE",
					TOTAL_ASSIGN_APPROVED_INVOICE_PER_EMPLOYEE);

			JSONObject TOTAL_REJECTED_INVOICES_PER_SECTOR = AppsproSoupCaller
					.buildQuery(
							ReportsQueries.TOTAL_REJECTED_INVOICES_PER_SECTOR(
									startDate, endDate)).run(true);
			overview.put("TOTAL_REJECTED_INVOICES_PER_SECTOR",
					TOTAL_REJECTED_INVOICES_PER_SECTOR);
			overview = ReportsMapper.mapOverviewResponse(overview);
		} else {
			overview = new JSONObject(DemoData.getDemo("overview"));
		}
		data.put("overview", overview);

		return data.toString();
	}

	public String getPersonInvoices(String personId) {
		if (!BackendHelper.isCallingLocally)
			return call("getInvoiceHistory");
		else {

			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getPersonInvoices");

			JSONObject res = AppsproSoupCaller.buildQuery(
					DashBoardQueries.PERSON_INVOICES(personId + "")).run(true);
			return DashboardMapper.getInvoiceHistoryresponse(res);

		}
	}

	public String getMyAudtiorIvoices(String personId,String lang) {

		if (BackendHelper.IS_STATIC)
			return DemoData.getDemo("getMyAdutiorInvoices");

		JSONObject res = AppsproSoupCaller.buildQuery(
				DashBoardQueries.GET_MY_ADTUIOR_INVOICES(personId,lang)).run(true);
		return DashboardMapper.getInvoiceHistoryresponse(res);
	}

	public String getWeekAchievers(String startDate, String endDate) {
		if (!BackendHelper.isCallingLocally)
			return call("statistics/achievers_of_the_week");
		else {
			// if(BackendHelper.IS_STATIC)
			return DemoData.getDemo("getWeekAchievers");
		}
	}

	public String getLeaderboard() {

		if (!BackendHelper.isCallingLocally)
			return call("statistics/leaderboard");
		else {
			// if(BackendHelper.IS_STATIC)
			return DemoData.getDemo("getLeaderboard");

			// JSONObject res =
			// ApsproSoupCaller.buildQuery(ReportsQueries.GET_TOP_TEN("1/11/2019",
			// "30/11/2019")).run(true);
			// return SkillsMatrixMapper.mapNoSkillsResponse(res);
		}
	}

	public String getNoSkills() {

		if (!BackendHelper.isCallingLocally)
			return call("skills-matrix/no-skills-users");
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("noSkills");

			JSONObject res = AppsproSoupCaller.buildQuery(
					SkillsMatrixQueries.GET_NO_SKILL_EMPLOYEES).run(true);
			return SkillsMatrixMapper.mapNoSkillsResponse(res);
		}
	}

	public String getSkills() {
		if (!BackendHelper.isCallingLocally)
			return call("skills-matrix/skills");
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getSkills");

			JSONObject res = AppsproSoupCaller.buildQuery(
					SkillsMatrixQueries.XXARRG_SECTIONS_TYPE_LOV_V()).run(true);
			return SkillsMatrixMapper.mapSkillsResponse(res);
		}
	}

	public String getAllSkills() {

		if (!BackendHelper.isCallingLocally)
			return call("skills-matrix/all");
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("getAllSkills");

			JSONObject res = AppsproSoupCaller.buildQuery(
					SkillsMatrixQueries.GET_SKILLS_MATRIX("AR")).run(true);

			return SkillsMatrixMapper.mapAllSkillsResponse(res);
		}
	}

	public String getUserType() {

		if (BackendHelper.IS_STATIC)
			return DemoData.getDemo("getUserType");

		JSONObject res = AppsproSoupCaller.buildQuery(
				AccountsQueries.GET_EMPLOYEE_ROLE).run(true);

		return res.toString();

	}

	public String updateSla(String map) {

		if (!BackendHelper.isCallingLocally)
			return call("sla/update-sla", "POST", map);
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("updateSla");

			JSONObject payload = new JSONObject(map);
			if (!payload.has("invoiceReview")
					|| !payload.has("supervisorReview")) {
				return "Empty Mandatory Value";
			} else {
				JSONObject res = AppsproSoupCaller
						.build("update_configration",SlaMapper.mapRequest(new JSONObject(map))).run(
								!BackendHelper.IS_STATIC);
				return SlaMapper.mapUpdateResponse(res);
			}
		}
	}

	public String validateLogin(String payload) {

		if (!BackendHelper.isCallingLocally)
			return call("mainservice/validate-login", "POST", payload);

		if (BackendHelper.IS_STATIC) {
			JSONObject o = new JSONObject(payload);
			if (o.has("username")) {
				if (o.get("username").toString()
						.equalsIgnoreCase("yalsaeed@stc.com.sa"))
					return DemoData.getDemo("getSUPERVISOR");
				else if (o.get("username").toString()
						.equalsIgnoreCase("afaljuraid@stc.com.sa"))
					return DemoData.getDemo("getTAXSUPERVISOR");
				else if (o.get("username").toString()
						.equalsIgnoreCase("adowish@stc.com.sa"))
					return DemoData.getDemo("getAUDITOR");
				else if (o.get("username").toString()
						.equalsIgnoreCase("wdosari@stc.com.sa"))
					return DemoData.getDemo("getTAXAUDITOR");
				else
					return DemoData.getDemo("getFOCALPOINT");
			} else
				return DemoData.getDemo("getFOCALPOINT");
		} else {
			JSONObject jsonResponse = AppsproSoupCaller
					.build("VALIDATE_IGATE_LOGIN",
							ValidateLogin.mapRequest(new JSONObject(payload)))
					.run(true);
			return ValidateLogin.mapResponse(jsonResponse);
		}
	}

	public String dashboardAssignAction(String payload) {

		if (!BackendHelper.isCallingLocally)
			return call("dashboard/assign-action", "POST", payload);
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("dashboardAssignAction");

			JSONArray arr = new JSONArray(payload);
			JSONObject res = AppsproSoupCaller.build(("ASSIGN".equalsIgnoreCase(arr.getJSONObject(0)
									.getString("ACTION")) ? "assign_cards"
									: "reassign_cards"),
					DashboardMapper.mapRequest(arr)).run(true);
			return DashboardMapper.mapResponse(res);
		}
	}

	public String dashboardInvoiceAction(String map) {

		if (!BackendHelper.isCallingLocally)
			return call("dashboard/invoice-action", "POST", map);
		else {

			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("dashboardInvoiceAction");

			JSONArray payload = new JSONArray(map);
			String methoud = "";
			if (("ACT-APPROVE".equalsIgnoreCase(payload.getJSONObject(0)
					.getString("ACTION")))
					|| ("SUP-APPROVE".equalsIgnoreCase(payload.getJSONObject(0)
							.getString("ACTION"))))
				methoud = "approve_card";

			if (("ACT-REJECT".equalsIgnoreCase(payload.getJSONObject(0)
					.getString("ACTION")))
					|| ("SUP-REJECT".equalsIgnoreCase(payload.getJSONObject(0)
							.getString("ACTION"))))
				methoud = "reject_card";

			if (("SEND_BACK".equalsIgnoreCase(payload.getJSONObject(0)
					.getString("ACTION"))))
				methoud = "send_card_back";
			
			if (("SEND_TO_AUDITOR".equalsIgnoreCase(payload.getJSONObject(0)
					.getString("ACTION"))))
				methoud = "SENDBACK_CARD_TO_AUDITOR";

			if (("CANCEL".equalsIgnoreCase(payload.getJSONObject(0).getString(
					"ACTION"))))
				methoud = "CANCEL_CARD";

			JSONObject res = AppsproSoupCaller.build(methoud,
					DashboardMapper.mapInvoiceAtionRequest(payload)).run(true);
			return SlaMapper.mapInvoiceActionResponse(res);
		}
	}

	// update/create
	public String updateSkills(String _data, String type) {

		JSONObject data = new JSONObject(_data);

		if (!BackendHelper.isCallingLocally)
			return call("skills-matrix/update/" + data.getString("PERSON_ID"),
					"PATCH", _data);
		else {
			if (BackendHelper.IS_STATIC)
				return DemoData.getDemo("updateSkills");

			JSONObject res = AppsproSoupCaller.build((type == "c" ? "create_skill_matrix"
									: "update_skill_matrix"),
					SkillsMatrixMapper.mapRequest(data)).run(true);
			return SkillsMatrixMapper.mapUpdateCreateResponse(res);
		}
	}

	public String getUsersHasNoType() {
		if (BackendHelper.IS_STATIC)
			return DemoData.getDemo("getUsersHasNoType");

		JSONObject res = AppsproSoupCaller.buildQuery(
				AccountsQueries.GET_EMPLOYEE_HAS_NO_TYPE).run(true);

		return res.toString();
	}

	public String getUsersHasNoType(String key) {
		if (BackendHelper.IS_STATIC)
			return DemoData.getDemo("getUsersHasNoType");

		JSONObject res = AppsproSoupCaller.buildQuery(
				AccountsQueries.GET_EMPS_HAS_NO_TYPE_SEARCH(key)).run(true);

		return res.toString();
	}

	public String setUserType(String payload, String method/*
															 * c->create,
															 * u->update
															 */) {
		if (BackendHelper.IS_STATIC) return "{\"STATUS\":\"OK\",\"DATA\":\"S\"}";

		JSONObject res = AppsproSoupCaller.build((!"u".equalsIgnoreCase(method) ? "create_account"
								: "update_account"),
				AccountMapper.mapRequest(new JSONArray(payload))).run(true);
		return AccountMapper.mapResponse(res);
	}

	public String getInvoiceDetails(String id) {
		if (BackendHelper.IS_STATIC)
			return "{}";

		JSONObject res = AppsproSoupCaller.buildQuery(
				DashBoardQueries.GET_INVOICE_DETAILS(id)).run(true);
		return res.toString();
	}

	public String getInvoiceAttachments(String id) {
		if (BackendHelper.IS_STATIC)
			return DemoData.getDemo("getInvoiceAttachments");

		JSONObject res = AppsproSoupCaller.buildQuery(
				DashBoardQueries.GET_INVOICE_ATTCHMENTS(id)).run(true);
		return res.toString();
	}
	public String getAttachmentBlob(String P_INVOICE_ID,String P_DOCUMENT_ID) {
		if (BackendHelper.IS_STATIC)
			return DemoData.getDemo("{}");
		
		JSONObject res = AppsproSoupCaller
				.build("get_attachment_clob",DashboardMapper.mapAttchmentBlobRequest(P_INVOICE_ID,P_DOCUMENT_ID)).run(true);
		return res.toString();
	}

	
	public String call(String path) {// GET no data passed
		return call(path, "GET", null);
	}

	public String call(String path, String methoud, String payload) {

		System.out.println("Calling url:" + (baseUrl + path) + ", methoud:"
				+ methoud + ",data:" + payload);
		String res = "";
		try {
			URL url = new URL(baseUrl + path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			byte[] message = (username + ":" + password).getBytes("UTF-8");
			String encoded = javax.xml.bind.DatatypeConverter
					.printBase64Binary(message);
			conn.setRequestProperty("Authorization", "Basic " + encoded);

			conn.setRequestMethod(methoud);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");

			conn.setDoOutput(true);
			if (payload != null) {
				conn.setDoInput(true);
				DataOutputStream wr = new DataOutputStream(
						conn.getOutputStream());
				wr.writeBytes(payload);
				wr.flush();
				wr.close();
			}
			String output;

			if (conn.getResponseCode() != 200) {
				BufferedReader ebr = new BufferedReader(new InputStreamReader(
						conn.getErrorStream(), "UTF-8"));

				while ((output = ebr.readLine()) != null) {
					res += output;
				}
				ebr.close();
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode() + "\n" + output);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				res += output;
			}

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return res;
	}

	// REPORTS
	public String getReportData(String fromDate, String toDate) {

		// GET_TOP_INVOICES_VENDOR
		//
		if (BackendHelper.IS_STATIC)
			return DemoData.getDemo("reportData");

		JSONObject response = new JSONObject();

		JSONObject TOP_INVOICES_VENDOR = AppsproSoupCaller.buildQuery(
				ReportsQueries.GET_TOP_INVOICES_VENDOR(fromDate, toDate)).run(
				true);
		response.put("TOP_INVOICES_VENDOR", TOP_INVOICES_VENDOR);

		JSONObject GET_TOTAL_INVOICE_SPENT_BY_EXPESNE_TYPE = AppsproSoupCaller
				.buildQuery(
						ReportsQueries.GET_TOTAL_INVOICE_SPENT_BY_EXPESNE_TYPE(
								fromDate, toDate)).run(true);
		response.put("GET_TOTAL_INVOICE_SPENT_BY_EXPESNE_TYPE",
				GET_TOTAL_INVOICE_SPENT_BY_EXPESNE_TYPE);

		JSONObject GET_TOTAL_INVOICE_SPENT_BY_MONTH = AppsproSoupCaller
				.buildQuery(
						ReportsQueries.GET_TOTAL_INVOICE_SPENT_BY_MONTH(
								fromDate, toDate)).run(true);
		response.put("GET_TOTAL_INVOICE_SPENT_BY_MONTH",
				GET_TOTAL_INVOICE_SPENT_BY_MONTH);

		JSONObject GET_TOTAL_INVOICE_SPENT_BY_SOURCE = AppsproSoupCaller
				.buildQuery(
						ReportsQueries.GET_TOTAL_INVOICE_SPENT_BY_SOURCE(
								fromDate, toDate)).run(true);
		response.put("GET_TOTAL_INVOICE_SPENT_BY_SOURCE",
				GET_TOTAL_INVOICE_SPENT_BY_SOURCE);
		
		JSONObject GET_TOTAL_INVOICE_SPENT_BY_SECTOR = AppsproSoupCaller
				.buildQuery(
						ReportsQueries.GET_TOTAL_INVOICE_SPENT_BY_SECTOR(
								fromDate, toDate)).run(true);
		response.put("GET_TOTAL_INVOICE_SPENT_BY_SECTOR",
				GET_TOTAL_INVOICE_SPENT_BY_SECTOR);

		return ReportsMapper.mapResponse(response);
	}

}
