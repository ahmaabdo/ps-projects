import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { StcReportsComponent } from './pages/stc-reports/stc-reports.component';
import { StcSlaComponent } from './pages/stc-sla/stc-sla.component';
import { StcStatisticsComponent } from './pages/stc-statistics/stc-statistics.component';
import { from } from 'rxjs';
import { StcDashboardComponent } from './pages/stc-dashboard/stc-dashboared.component';
import { StcMyAccountComponent } from './pages/stc-my-account/stc-my-account.component';
import { StcFocalDashboardComponent } from './pages/stc-focal-dashboard/stc-focal-dashboard.component';
import { StcPageNotFoundComponent } from './pages/stc-page-not-found/stc-page-not-found.component';

import { StcSkillsMatrixComponent } from './pages/stc-skills-matrix/stc-skills-matrix.component';
import { StcLoginComponent } from './pages/stc-login/stc-login.component';
import { LoginAuthService } from './guard/login-auth.service'

import { StcMappingComponent } from './pages/stc-mapping/stc-mapping.component'

import { AuthGuardService } from './guard/auth-guard.service'
import { TestComponent } from './pages/test/test.component';
import { StcUnauthorizedComponent } from './pages/stc-unauthorized/stc-unauthorized.component';
import { VpDashBoardComponent } from './pages/vp-dash-board/vp-dash-board.component';

const routes: Routes = [
  { path: '', redirectTo: 'loginPage', pathMatch: 'full' },
  // { path: 'home', component: StcHomeComponent },
  { path: 'loginPage', component: StcLoginComponent, canActivate: [LoginAuthService] },
  { path: 'SkillsMatrix', component: StcSkillsMatrixComponent, canActivate: [AuthGuardService] },
  { path: "StcReports", component: StcReportsComponent, canActivate: [AuthGuardService] },
  { path: "StcSla", component: StcSlaComponent, canActivate: [AuthGuardService] },
  { path: "StcStatistics", component: StcStatisticsComponent},
  { path: "test", component: TestComponent },
  { path: "Unauthorized", component: StcUnauthorizedComponent },
  { path: "StcMapping", component: StcMappingComponent, canActivate: [AuthGuardService] },
  { path: "VpDashBoard", component: VpDashBoardComponent, canActivate: [AuthGuardService] },
  {
    path: 'DashBoard', component: StcDashboardComponent, children: [
      { path: '', redirectTo: 'MyAccount', pathMatch: 'full' },
      { path: 'MyAccount', component: StcMyAccountComponent, canActivate: [AuthGuardService] },
      { path: 'FocalDashboard', component: StcFocalDashboardComponent, canActivate: [AuthGuardService] }
    ]
  },
  { path: '**', component: StcPageNotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
