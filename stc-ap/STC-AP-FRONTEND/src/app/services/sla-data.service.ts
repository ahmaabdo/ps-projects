import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
@Injectable({
  providedIn: 'root'
})
export class SlaDataService {

  CONFIG;
  constructor(private service: CommonService) {

  }
  reportData(data: any): Promise<any> {
    let url = localStorage.updateSla;
    return this.service.postGeneric(url, data, { 'responseType': 'plain/text' });
  }

  getData(): Promise<any> {
    return this.service.getGeneric(localStorage.getSla);
  }
}
