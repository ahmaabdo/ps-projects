import { Injectable } from '@angular/core';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class MappingServiceService {

  setType(payload): Promise<any> {
    return this.common.postGeneric(localStorage.setUserType, payload);
  }
  getUserTypes(): Promise<any> {
    return new Promise((res, rej) => {
      this.common.getGeneric(localStorage.getUserType, null, 1).then(resp => {
        if (resp.STATUS == 'OK' && !resp.DATA.find(e => e.CODE == '-1')) {
          resp.DATA.push({ 'DESCRIPTION': 'DISABLED', 'CODE': '-1' });
        }
        res(resp);
      })
    })
    //         


  }

  constructor(private common: CommonService) { }
  getUsers(key: string): Promise<any> {
    return this.common.getGeneric(localStorage.getUsersHasNoType, { params: { 'key': key } })
  }
}
