import { Injectable } from '@angular/core';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class LoginPageService {

  constructor(public common: CommonService) { }

  getUserDetails(username, password): Promise<any> {

    let data = new FormData();
    data.append("username", username);
    data.append("password", password);
    let url = localStorage.validateLogin;
    return this.common.postGeneric(url, data);
  }
}
