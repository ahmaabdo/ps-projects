import { Injectable } from '@angular/core';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  notificationArray = [];
  service: any;

  constructor(public common: CommonService) {
  }
  getNotificationHeader(): Promise<any> {
    let url = localStorage.notificationHeader;
    return this.common.getGeneric(url)
  }
  getNotificationData(notifId): Promise<any> {
    return this.common.getGeneric(localStorage.notificationDetails, { params: { 'id': notifId } })
  }
  closeNotification(data: any): Promise<any> {
    let url = localStorage.closeNotification;
    return this.common.postGeneric(url, data, { 'responseType': 'plain/text' })
  }

  setDetails(notification) {
    this.notificationArray = notification;
    console.log(this.notificationArray)
  }
}
