import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  data: any = {};
  currentLang: string = '';
  authenticating: boolean = false;


  constructor(
    private http: HttpClient,
    public common: CommonService
  ) { }

  use(lang: string): Promise<{}> {
    this.currentLang = lang;
    localStorage.lang = lang;
    document.body.setAttribute("dir", this.direction());
    localStorage.dir = this.direction();


    return new Promise<{}>((resolve, reject) => {

      const langPath = this.common.getAssetsPath() + `i18n/i18n.json`;

      this.http.get<{}>(langPath).toPromise().then((res: any) => {

        this.data = res;//Object.assign({}, translation || {});
        try { this.data = JSON.parse(this.data) } catch (ignored) { }
        resolve(this.data);
      }, err => {
        console.log(err);
        this.data = {};
        resolve(this.data);
      })
    });
  }

  trans(key) {
    return this.data[key] ? this.data[key][this.currentLang] : key;
  }
  direction() {
    return this.currentLang && this.currentLang === "ar" ? "rtl" : "ltr"
  }
}
