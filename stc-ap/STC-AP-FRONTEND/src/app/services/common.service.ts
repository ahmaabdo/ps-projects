import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { ConfirmMessageComponent } from '../modals/confirm-message/confirm-message.component';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  public IS_AUTENTICATING: boolean = false;
  public usersIdsTypes: any[];

  isInvoiceMatches(invoice, key) {
    let values = Object.values(invoice);
    let matches = false;
    for (let x = 0; x < values.length; x++) {
      if (values[x] && (values[x] + "").toLowerCase().includes(String(key).toLowerCase())) {
        matches = true;
        break;
      }
    }
    return matches;
  }

  getSearchLookup() {
    return [{
      make: 'Last Week',
      make_ar: 'أخر أسبوع',
      days: 7,
    }, {
      make: 'Last Month',
      make_ar: 'أخر شهر',
      days: 30,
    }, {
      make: 'Last Quarter',
      make_ar: 'أخر ربع سنوي',
      days: 90,
    },
    {
      make: 'Last Year',
      make_ar: 'أخر عام',
      days: 365,
    },
    {
      make: 'Fixed Date',
      make_ar: 'أكتب التاريخ',
      days: -1,
    }];
  }
  getUserTypeById(id) {
    return this.usersIdsTypes && this.usersIdsTypes.find(u => u.id == id) ? this.usersIdsTypes.find(u => u.id == id).type : "No Role";
  }
  getUserAccountById(id) {
    return this.usersIdsTypes && this.usersIdsTypes.find(u => u.id == id) ? this.usersIdsTypes.find(u => u.id == id).ACCOUNT_ID || 0 : 0;
  }

  subDate(startDate: string, searchVal: number): Date {

    var dt = new Date(startDate);
    dt.setDate(dt.getDate() - searchVal);
    return dt;
  }

  /**
   * 
   * @param args ["d", "m", "y", "/"] day month year delemnator -> return date in passed format
   * @param _date "optional" ['11-10-1993', 'm', 'd', 'y', '-'] if exist this will be the date to be returned else current date will be the date
   */
  getDate(args: string[], _date?: any) {
    if (_date && _date.length == 5 && _date instanceof Array) {
      let arr = _date[0].split(_date[4]);
      return arr[args.indexOf(_date[1])] + args[3] + arr[args.indexOf(_date[2])] + args[3] + arr[args.indexOf(_date[3])];
    }


    let date: any = _date instanceof Date ? _date : new Date();
    let dd: any = date.getDate();
    let mm: any = date.getMonth() + 1;

    let yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    return (args[0] == 'd' ? dd : args[0] == 'm' ? mm : yyyy) + args[3] + (args[1] == 'd' ? dd : args[1] == 'm' ? mm : yyyy) + args[3] + (args[2] == 'd' ? dd : args[2] == 'm' ? mm : yyyy);
  }

  filterDays: any = 7;
  completedCollection = [];
  allBatchs = new Set();
  myAccountData: any[] = [];
  private baseUrl = '';//'http://localhost:8080/ap_integration/';
  cacheObject = {};

  constructor(private http: HttpClient, private _snackBar: MatSnackBar, public matDialog: MatDialog) {
  }

  getFilterDays() {
    return this.filterDays
  }
  setFilterDays(days) {
    this.filterDays = days
  }
  /****************************** */

  getUserImage() {
    return this.getUserImageByImageId(JSON.parse(localStorage.user).img_id);
  }

  getUserType() {
    let t = localStorage.token.charAt(0) + localStorage.token.charAt(1);
    switch (t) {
      case "FF": return "FOCALPOINT";
      case "AA": return "AUDITOR";//AUDITOR
      case "SS": return "SUPERVISOR";//SUPERVISOR
      case "TA": return "TAXAUDITOR";//AUDITOR
      case "TS": return "TAXSUPERVISOR";//SUPERVISOR
      case "VP": return "VP";//SUPERVISOR
      default: return "FOCALPOINT";
    }
  }

  getUserImageById(id) {
    if (localStorage.imagesData) {
      let data: any[] = JSON.parse(localStorage.imagesData);
      let userData = data.find(e => e.id == id);
      if (userData)
        return location.origin + '/image/user_male_portrait?img_id=' + userData.img;
    }
    return this.getAssetsPath() + "img/avatar.jpg";
  }
  getUserImageByImageId(img_id) {
    if (img_id == '0')
      return this.getAssetsPath() + "img/avatar.jpg";
    return location.origin + '/image/user_male_portrait?img_id=' + img_id;
  }


  getAssetsPath() {
    return location.host === 'localhost:4200' ? '/../../assets/' : '/../../newigate/static/assets/';
  }
  getCurrentUser() {
    return localStorage.user ? JSON.parse(localStorage.user) : null;
  }
  setUser(user) {
    localStorage.user = JSON.stringify(user);
  }

  deleteUser() {
    localStorage.removeItem('user');
  }
  /**
   * @param path service url
   * @param options http option like responseType 
   * @param cacheFlag   null,1,2
   *    //null-no cache
   *    //1-read from cache if exist,if not get new data and cache it
        //2-dont read from cache but save new data to cache
   */
  getGeneric(path: string, options?: any, cacheFlag?: number): Promise<any> {
    this.loading(true);
    let option: any = this.getOptions();
    if (options)
      option = { ...option, ...options };
    return new Promise((resolve, reject) => {
      if (cacheFlag && this.cacheObject[path]) {
        resolve(this.cacheObject[path]);
      } else {
        // console.log("calling",path)
        this.http.get(this.baseUrl + path, option).subscribe((res: any) => {
          this.loading(false);
          try { res = JSON.parse(res) } catch (ignored) { }
          if (res && res.CODE == 403) {
            console.log("Access Denied")
            this.openSnackError("Session Expired", "please reload the page");
          } else {
            if (cacheFlag)
              this.cacheObject[path] = res;
            setTimeout(() => {
              resolve(res);
            }, localStorage.DELAY_RESPONSE == 'true' ? 5000 : 0);
          }
        })
      }
    })
  }

  setMyAccountData(arr: any[]) {
    this.myAccountData = arr;

    for (let i = 0; i < this.myAccountData.length; i++) {
      if (!this.completedCollection.includes(this.myAccountData[i].BatchName)) {

        this.completedCollection.push(this.myAccountData[i].BatchName)
      }
    }
    for (let i = 0; i < this.myAccountData.length; i++) {
      this.allBatchs.add(this.myAccountData[i].BatchName)
    }

  }


  getNoSkillsUser(path: string): Promise<any> {
    return new Promise((resolve, rej) => {
      this.http.get(this.baseUrl + path, this.getOptions()).subscribe(res => {
        this.loading(false);
        resolve(res);
      })
    })
  }

  putGeneric(path: string, payload: Object): Promise<any> {
    return new Promise((resolve, reject) => {
      let options = this.getOptions();
      options['responseType'] = 'text';
      this.loading(true);
      this.http.put(this.baseUrl + path, payload, options).subscribe((res: any) => {
        this.loading(false);
        try { res = JSON.parse(res) } catch (ignored) { }
        if (res && res.CODE == 403) {
          console.log("Access Denied")
          this.openSnackError("Session Expired", "please reload the page");
        } else {
          resolve(res);
        }
      }, error => {
        this.loading(false);
        console.log(error);
      })
    })
  }

  toFormData(json) {
    try { json = JSON.parse(json) } catch (ignored) { }

    return Object.keys(json).map(function (key) {
      let val = json[key];
      if (typeof val == 'object' || Array.isArray(val))
        val = JSON.stringify(val);
      return encodeURIComponent(key) + '=' + encodeURIComponent(val);
    }).join('&');
  }

  postGeneric(path: string, payload: any, options?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.loading(true);
      let option: any = this.getOptions();
      if (options)
        option = { ...option, ...options };

      if (!option.headers)
        option.headers = {};
      option.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      // option.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';//'multipart/form-data';
      // console.log(this.http);
      let p = this.toFormData({ ...payload });//clone to prevent change source
      console.log(payload, p, option);
      this.http.post(this.baseUrl + path, p, option).subscribe((res: any) => {

        this.loading(false);
        try { res = JSON.parse(res) } catch (ignored) { }
        if (res && res.CODE == 403) {
          console.log("Access Denied")
          this.openSnackError("Session Expired", "please reload the page");
        } else {
          setTimeout(() => {
            resolve(res);
          }, localStorage.DELAY_RESPONSE == 'true' ? 5000 : 0);
        }
      })
    })

  }



  getOptions() {
    return {
      headers: {
        // 'Authorization': 'Basic c3RjOnBhc3N3b3Jk'
        token: localStorage.token || "NOTOKEN"
      }
    };
  }

  loading(isLoading) {

    if (isLoading) {
      // (<any>document.getElementById("favIcon")).href = "https://alligator.io/assets/img/favicon-32x32.png";
    } else {
      // (<any>document.getElementById("favIcon")).href = "https://www.w3schools.com/tags/demo_icon.gif";
    }
  }



  // slaRemingDays(remingSla, sla) {
  //   return Number(remingSla) <= Math.floor(Number(sla) / 3) ? 'animationRed' : Number(remingSla) <= (Math.floor(Number(sla) / 3) + Math.ceil(Number(sla) / 3)) ? 'animationYellow' : 'restAnimation';
  // }


  openSnackError(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      panelClass: ['background-snackbar-red']
    });
  }
  openSnackSuccess(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      panelClass: ['background-snackbar-green']
    });
  }
  openSnackBar(message: string, action: string, classValue: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      panelClass: [classValue]
    });
  }



  confirm(title, message, ok?, cancel?): Promise<any> {
    return this.infoConfirm(title, message, 'confirm', ok, cancel);
  }

  info(title, message, ok?): Promise<any> {
    return this.infoConfirm(title, message, 'info', ok);
  }

  private infoConfirm(title, message, flag, ok?, cancel?): Promise<any> {
    return new Promise((res, rej) => {
      const dialogRef = this.matDialog.open(ConfirmMessageComponent, { disableClose: true, data: { 'title': title, 'message': message, 'flag': flag, 'ok': ok, 'cancel': cancel } });
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        res(result ? true : false);
      });
    })
  }

  numberWithCommas = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  log = data => console.log(data);
  round = (no, num) => Math.round(no * Math.pow(10, num)) / Math.pow(10, num);
  private format(fmt, ...args) {
    if (!fmt.match(/^(?:(?:(?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{[0-9]+\}))+$/)) {
      throw new Error('invalid format string.');
    }
    return fmt.replace(/((?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{([0-9]+)\})/g, (m, str, index) => {
      if (str) {
        return str.replace(/(?:{{)|(?:}})/g, m => m[0]);
      } else {
        if (index >= args.length) {
          throw new Error('argument index is out of range in format');
        }
        return args[index];
      }
    });
  }

  // using
  //print("hello {0}, did you go to {1}",'hussein','Egypt') => hello hussein, did you go to Egypt
  public print(fmt, ...args) {
    console.log(this.format(fmt, ...args));
  }
}
