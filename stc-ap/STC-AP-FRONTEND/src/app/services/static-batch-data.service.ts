import { Injectable } from '@angular/core';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class StaticBatchDataService {


  currentUser: any;
  NotificationArray: any[];
  load: boolean = true;
  userLogin: any


  constructor(public service: CommonService) {
    this.currentUser = service.getCurrentUser()
    this.NotificationArray = []
  }

  /*****************Services********************* */
  getMyAccountInvoice(): Promise<any> {
    return this.service.getGeneric(localStorage.dashboardGetInvoices)
  }

  getMyAccountCompletedInvoice(to, from): Promise<any> {
    let url = localStorage.dashboardCompletedInvoices;
    return new Promise((res, rej) => {
      this.service.getGeneric(url, { params: { "from": from, "to": to } }).then(resp => {
        if (resp.STATUS == 'OK')
          res(resp.DATA);
        else {
          res([]);
          console.log("ERROR", resp.DATA);
        }
      })
    });
  }

  invoiceAction(data): Promise<any> {
    let url = localStorage.dashboardInvoiceAction;
    return this.service.postGeneric(url, data, { 'responseType': 'text' })
  }

  getFocalInvoices(): Promise<any> {
    let url = localStorage.getFocal;
    return this.service.getGeneric(url, { 'responseType': 'text' })
  }

  assignAction(data): Promise<any> {
    let url = localStorage.dashboardAssignAction;
    return this.service.postGeneric(url, data, { 'responseType': 'text' });
  }


  getAllUsers(): Promise<any> {
    return new Promise((res, rej) => {
      this.service.getGeneric(localStorage.getUsers).then(resp => {
        if (resp.STATUS == 'OK') {
          this.service.usersIdsTypes = resp.DATA.map(e => ({ id: e.PERSON_ID, type: e.PERSON_TYPE_CODE, ACCOUNT_ID: e.ACCOUNT_ID }));
        }
        res(resp);
      });
    })
  }

  getRejectionData(): Promise<any> {
    let url = localStorage.rejectReason;
    //
    return this.service.getGeneric(url, { params: { 'lang': localStorage.lang == 'ar' ? 'AR' : 'US' } });
  }
  getDetailsReject(id): Promise<any> {
    let url = localStorage.rejectDetails;
    return this.service.getGeneric(url, { params: { 'id': id }, 'responseType': 'text' });
  }

  getinvoiceHistory(id, item_key): Promise<any> {
    let url = localStorage.getInvoiceHistory;
    return this.service.getGeneric(url, { params: { 'id': id, 'item_key': item_key}, 'responseType': 'text' });

  }

  getPersonInvoices(id): Promise<any> {
    let url = localStorage.getPersonInvoices;
    return this.service.getGeneric(url, { params: { 'id': id }, 'responseType': 'text' });
  }

  getMyAudtiorInvoices(): Promise<any> {
    let url = localStorage.getMyAdutior;
    //, { params: { 'start': start, 'end': end, 'lang': localStorage.lang == 'ar' ? 'AR' : 'US' } }
    return this.service.getGeneric(url, { 'responseType': 'text', params: { 'lang': localStorage.lang == 'ar' ? 'AR' : 'US' } });
  }
}
