import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonService } from './common.service';
​
@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  constructor(private service:CommonService) { 
​
  }
  reportData():Promise<any>{
    let url= localStorage.weekAchievers 
    return this.service.getGeneric(url);
  }
  reportDataOfLeaderboard():Promise<any>{
    let url = localStorage.leaderboard ; 
    return this.service.getGeneric(url)
  }
}