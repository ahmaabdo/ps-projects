import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-placeholder',
  templateUrl: './dashboard-placeholder.component.html',
  styleUrls: ['./dashboard-placeholder.component.css']
})
export class DashboardPlaceholderComponent implements OnInit {

  constructor() { }

  placeHolderArr: any[] = [1, 2, 3]
  ngOnInit() {
  }

}
