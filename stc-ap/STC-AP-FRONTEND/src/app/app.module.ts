import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MatMenuModule } from '@angular/material/menu';
import { ChartsModule } from 'ng2-charts';
import { ReportServiceService } from './services/report-service.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgSelect2Module } from 'ng-select2';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MatFormFieldModule, MatSelectModule, MatInputModule, MatButtonModule, MatIconModule, MAT_CHECKBOX_CLICK_ACTION, MatSlideToggleModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatSnackBarModule } from '@angular/material';

import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchPipe } from './pipe/search.pipe';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
// import { MaterialModule } from './material.module/material.module.component';

import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatRadioModule } from '@angular/material';



const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'left',
      distance: 12
    },
    vertical: {
      position: 'bottom',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};



import { TranslationPipe } from './pipe/translation.pipe';
import { TranslationService } from './services/translation.service';
import { SwitchLanguageComponent } from './components/switch-language/switch-language.component';
import { APP_BASE_HREF } from '@angular/common';



import { SortCheckPipe } from './pipe/sort-check.pipe';
import { EmployeeNamePipe } from './pipe/employee-name.pipe';
import { ArrayPriorityPipe } from './pipe/array-priority.pipe';
import { TestComponent } from './pages/test/test.component';
// import { HttpClient } from 'selenium-webdriver/http';
import { NgxMasonryModule } from 'ngx-masonry';
import { CompletedPipe } from './pipe/completed.pipe';
import { OrderModule } from 'ngx-order-pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SortHiddenPipe } from './pipe/sort-hidden.pipe';
import { InvoiceCardComponent } from './components/invoice-card/invoice-card.component';
import { ViewInvoiceComponent } from './components/view-invoice/view-invoice.component';
import { StcNavbarComponent } from './pages/stc-navbar/stc-navbar.component';
import { StcSkillsMatrixComponent } from './pages/stc-skills-matrix/stc-skills-matrix.component';
import { StcReportsComponent } from './pages/stc-reports/stc-reports.component';
import { StcSlaComponent } from './pages/stc-sla/stc-sla.component';
import { StcStatisticsComponent } from './pages/stc-statistics/stc-statistics.component';
import { StcDashboardComponent } from './pages/stc-dashboard/stc-dashboared.component';
import { StcMyAccountComponent } from './pages/stc-my-account/stc-my-account.component';
import { StcFocalDashboardComponent } from './pages/stc-focal-dashboard/stc-focal-dashboard.component';
import { StcPageNotFoundComponent } from './pages/stc-page-not-found/stc-page-not-found.component';
import { StcLoginComponent } from './pages/stc-login/stc-login.component';
import { StcMappingComponent } from './pages/stc-mapping/stc-mapping.component';
import { StcUnauthorizedComponent } from './pages/stc-unauthorized/stc-unauthorized.component';
import { ConfirmMessageComponent } from './modals/confirm-message/confirm-message.component';
import { DashboardPlaceholderComponent } from './placeholders/dashboard-placeholder/dashboard-placeholder.component';
import { VpDashBoardComponent } from './pages/vp-dash-board/vp-dash-board.component';
import { ReversePipe } from './pipe/reverse.pipe';
import { InvoiceDetailsComponent } from './components/invoice-details/invoice-details.component';
import { AssignComponent } from './components/assign/assign.component';
import { HistoryComponent } from './components/history/history.component';
import { RejectReasonsComponent } from './components/reject-reasons/reject-reasons.component';





let liferay = (<any>window).Liferay;



// on init
export function setupTranslateFactory(
  service: TranslationService): Function {
  let lang = "en";
  if (liferay)
    lang = liferay.ThemeDisplay.getLanguageId().includes("ar") ? "ar" : "en";
  return () => service.use(lang);
}

@NgModule({
  declarations: [
    AppComponent,
    StcNavbarComponent,
    StcSkillsMatrixComponent,
    StcReportsComponent,
    StcSlaComponent,
    StcStatisticsComponent,
    StcReportsComponent,
    StcDashboardComponent,
    SearchPipe,
    ArrayPriorityPipe,
    StcMyAccountComponent,
    StcFocalDashboardComponent,
    StcPageNotFoundComponent,
    TranslationPipe,
    EmployeeNamePipe,
    SwitchLanguageComponent,
    StcLoginComponent,
    SortCheckPipe,
    TestComponent,
    StcMappingComponent,
    StcMappingComponent,
    CompletedPipe,
    SortHiddenPipe,
    StcUnauthorizedComponent,
    InvoiceCardComponent,
    ViewInvoiceComponent,
    ConfirmMessageComponent,
    DashboardPlaceholderComponent,
    VpDashBoardComponent,
    ReversePipe,
    InvoiceDetailsComponent,
    AssignComponent,
    HistoryComponent,
    RejectReasonsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DragDropModule,
    MatDialogModule,
    BrowserAnimationsModule,
    NgbModule,
    AngularFontAwesomeModule,
    MatMenuModule,
    ChartsModule,
    HttpClientModule,
    Ng2GoogleChartsModule,
    NgxEchartsModule,
    // saveAs

    FormsModule,
    ReactiveFormsModule,

    AngularFontAwesomeModule,
    MatMenuModule,
    NgSelect2Module,
    HttpClientModule,
    MatSelectModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    Ng2SearchPipeModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxMasonryModule,
    ScrollToModule.forRoot(),
    NgScrollbarModule,
    ScrollingModule,
    ScrollToModule.forRoot(),
    MatCheckboxModule,
    MatSlideToggleModule,
    NgxMasonryModule,
    OrderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule
  ],
  exports: [
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
  schemas: [],
  providers: [ReportServiceService, {
    provide: APP_INITIALIZER,
    useFactory: setupTranslateFactory,
    deps: [TranslationService],
    multi: true
  }
    , { provide: APP_BASE_HREF, useValue: liferay ? liferay.currentURL : '/' },
    { provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check' },
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmMessageComponent,
    InvoiceDetailsComponent,
    AssignComponent,
    HistoryComponent,
    RejectReasonsComponent
  ]
})

export class AppModule { }
