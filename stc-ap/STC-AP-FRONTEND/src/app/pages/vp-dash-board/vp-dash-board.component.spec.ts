import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VpDashBoardComponent } from './vp-dash-board.component';

describe('VpDashBoardComponent', () => {
  let component: VpDashBoardComponent;
  let fixture: ComponentFixture<VpDashBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VpDashBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VpDashBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
