import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlaDataService } from '../../services/sla-data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { TranslationService } from 'src/app/services/translation.service';
@Component({
  selector: 'app-stc-sla',
  templateUrl: './stc-sla.component.html',
  styleUrls: ['./stc-sla.component.css']
})
export class StcSlaComponent implements OnInit {
  currentPage: string = "sla"
  config: {
    invoiceReview: number,
    supervisorReview: number,
    taxAccountantReview: number,
    taxSupervisorReview: number,
  } | any = {};
  showPlacholder: boolean = false;

  constructor(private trans:TranslationService,public common: CommonService, config: NgbModalConfig, private modalService: NgbModal, private sla: SlaDataService, private _snackBar: MatSnackBar, private _report: SlaDataService, private router: Router) { }

  ngOnInit() {
    if (this.common.getUserType() != 'FOCALPOINT')
      this.router.navigateByUrl("**", { skipLocationChange: true })
      this.showPlacholder = true
    this._report.getData().then(data => {
      
      if (data.STATUS != 'OK') {
        console.log("error", data.DATA);
        return;
      }
      data = data.DATA;
      try { data = JSON.parse(data) } catch (ignored) { };
      this.showPlacholder = false;
      this.config = data;
      this._report.CONFIG = {...this.config};
    });
  }
  checkSla() {
    if (!this.config.invoiceReview || !this.config.supervisorReview || !this.config.taxAccountantReview || !this.config.taxSupervisorReview) {
      this.common.openSnackBar,(this.trans.trans('no_change_msg'),this.trans.trans('close_label'), 'background-snackbar-red')
    }
    else {
      this.common.confirm('slaConfig_label', 'sla_confirmation_label', 'edit', 'cancel').then(res => {
        if (res)
          this.changeSla();
      })
    }
  }

  changeSla() {
    this._report.reportData(this.config).then(res => {
      this._report.CONFIG = {...this.config};
      this.common.openSnackBar(this.trans.trans('sla_update_msg'), this.trans.trans('close_label'), 'background-snackbar-green')
    })
  }

  /********************open modal ****************/
  open(approveContent) {
    this.modalService.open(approveContent, { centered: true });
  }

}
