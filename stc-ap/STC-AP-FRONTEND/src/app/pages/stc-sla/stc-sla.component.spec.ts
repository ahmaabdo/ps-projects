import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcSlaComponent } from './stc-sla.component';

describe('StcSlaComponent', () => {
  let component: StcSlaComponent;
  let fixture: ComponentFixture<StcSlaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcSlaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcSlaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
