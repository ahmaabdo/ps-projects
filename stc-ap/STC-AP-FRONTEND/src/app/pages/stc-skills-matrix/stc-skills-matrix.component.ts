import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { SkillsMatrixDataService } from '../../services/skills-matrix-data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommonService } from '../../services/common.service';
import { Router } from '@angular/router';
import { TranslationService } from '../../services/translation.service';
import { MappingServiceService } from '../../services/mapping-service.service';
import { StaticBatchDataService } from '../../services/static-batch-data.service';


@Component({
  selector: 'stc-skills-matrix',
  templateUrl: './stc-skills-matrix.component.html',
  styleUrls: ['./stc-skills-matrix.component.css']
})

export class StcSkillsMatrixComponent implements OnInit {

  employeeWithoutSkills: any[] = [];
  selectedUserId: any;
  searchSkillsEmps;
  @ViewChild("content", { static: true }) content: ElementRef;
  // dropdownListVendors;
  dropdownSettings;
  myForm
  currentPage: string = "skillsmatrix";
  allSkillsData: any[] = [];
  // skillsDataLenght: boolean = false;
  load: boolean = true;
  // currentUser: Object;
  empToBeUpdated: any;
  skillTemp: any;
  showmorebuttonCount = 5
  UpdateTemp: any;
  SkillsTypes: any[];
  selectedCategory;
  loopIndex = 0;
  placeHolderArr: any[] = [1, 2, 3, 4]
  isSummaryMode: boolean = false;
  isLazyLoadEnabled: boolean = false;
  types: any = [];
  constructor(public trans: TranslationService, public common: CommonService, private modalService: NgbModal, private router: Router, private mappingData: MappingServiceService, private batch: StaticBatchDataService) {
    // this.currentUser = this.common.getCurrentUser();
    this.isLazyLoadEnabled = true;//localStorage.isLazyLoadEnabled ? true : false;
    //get LOV

    // if (!this.skillsData.skills)
    this.common.getGeneric(localStorage.getSkills, null, 1).then(res => {
      this.handleResponse(res);
    })
    // else
    //   this.handleResponse(this.skillsData.skills);

    /************************************************************************************** */

    /*****************multiselect  data***************/

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'SECTION_CODE',
      textField: 'SECTION_NAME',
      valueFiels: 'SECTION_VALUE',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };



    this.mappingData.getUserTypes().then(res => {
      if (res.STATUS == 'OK') {
        this.types = res.DATA;
      }
      else
        console.log("ERROR", res.DATA);
    });

  }

  handleResponse(res) {
    if (res.STATUS == 'OK') {
      res = res.DATA;
      try { res = JSON.parse(res); } catch (ignored) { }
      this.skillTemp = res;
      this.SkillsTypes = Object.keys(this.skillTemp);
      this.selectedCategory = this.SkillsTypes[0];
      //all emp skills
      this.getAll(_ => {
        this.load = false;
        this.setupPaging();
      });
    } else {
      this.load = false;
      console.log(res.DATA);
    }
  }
  setupPaging() {
    let l: number = this.allSkillsData.filter(e => !e.hide).length;
    this.pagesCount = Math.ceil(l / this.length);
    this.pageIndex = 1;

  }
  ngOnInit() {
    if (this.common.getUserType() != 'FOCALPOINT') {
      this.router.navigateByUrl("**", { skipLocationChange: true })
    }
  }
  openLg(mode, empId) {//edit or add
    if (mode != 'add' && mode != 'edit')
      return;

    this.UpdateTemp = JSON.parse(JSON.stringify(this.skillTemp))

    if (mode == 'edit') {
      const empString = JSON.stringify(this.allSkillsData.find(e => e.PERSON_ID == empId));
      this.empToBeUpdated = JSON.parse(empString);
      // set user selections
      Object.keys(this.UpdateTemp).forEach(type => {
        this.UpdateTemp[type].forEach(i => {
          if (this.empToBeUpdated.SKILLS.find(s => s.SECTION_CODE == i.SECTION_CODE)) {
            i.SECTION_VALUE = 'Y';
          }
        })
      })
    } else {//add
      this.empToBeUpdated = {};
      this.empToBeUpdated.PERSON_ID = empId;
      this.empToBeUpdated.WORKLOAD = 0;
      this.empToBeUpdated.USER = this.employeeWithoutSkills.find(e => e.PERSON_ID == empId).PERSON_NAME;
      this.empToBeUpdated.SKILLS = [];
      this.empToBeUpdated.TYPE = this.employeeWithoutSkills.find(e => e.PERSON_ID == empId).PERSON_TYPE_CODE;
    }

    console.log(this.empToBeUpdated)
    console.log(this.UpdateTemp);

    this.modalService.dismissAll();
    this.modalService.open(this.content, { size: 'lg', centered: true, backdrop: 'static' });
  }

  save(modal) {

    let skills: any[] = this.getSelectedSkills(this.UpdateTemp);

    for (let x = 0; x < this.SkillsTypes.length; x++) {
      if (this.SkillsTypes[x] != 'VENDOR' && !skills.find(e => e.SECTION_TYPE == this.SkillsTypes[x])) {
        this.common.openSnackBar(this.trans.trans('at_least_cate_msg').replace('#cate#',this.SkillsTypes[x]), this.trans.trans('close_label'), "background-snackbar-red");
        return;
      }
    }

    let isChanged = this.isSkillsChanged();

    this.empToBeUpdated.SKILLS = skills;
    let payload = {
      payload: JSON.stringify(this.empToBeUpdated),
      type: this.empToBeUpdated.SKM_ID ? 'update' : 'create'
    }
    console.log("start saving");
    this.doSave(payload, isChanged).then(_ => {
      if (isChanged)
        modal.dismiss('Cross click');
      console.log("end saving");
      //update account
      if (this.empToBeUpdated.TYPE && this.empToBeUpdated.TYPE != this.common.getUserTypeById(this.empToBeUpdated.PERSON_ID)) {
        let acc_payload = {
          payload: JSON.stringify([{
            'ACCOUNT_ID': this.common.getUserAccountById(this.empToBeUpdated.PERSON_ID) || null,
            'PERSON_ID': this.empToBeUpdated.PERSON_ID,
            'PERSON_TYPE_CODE': this.empToBeUpdated.TYPE
          }]),
          method: this.common.getUserAccountById(this.empToBeUpdated.PERSON_ID) > 0 ? 'u' : 'c'
        }
        this.mappingData.setType(acc_payload).then(res => {
          if (res.STATUS == 'OK') {
            this.common.openSnackSuccess(this.trans.trans('done_label'), this.trans.trans('user_type_update_msg'));
            this.batch.getAllUsers().then(_ => {
              this.allSkillsData.forEach(e => e.TYPE = this.common.getUserTypeById(e.PERSON_ID));
            });
          }
          else
            this.common.openSnackError(this.trans.trans('error_label'), res.DATA);
        })
      }
    });
  }

  getSelectedSkills(allSkils): any[] {
    let arr = [];
    this.SkillsTypes.forEach(e => {
      arr.push(...allSkils[e].filter(e => e.SECTION_VALUE == 'Y'));
    })
    return arr;
  }

  opensm(content) {
    this.modalService.open(content, { size: 'lg', centered: true, backdrop: 'static' });
  }

  doSave(payload, isChanged): Promise<any> {
    return new Promise(resolve => {
      if (!isChanged) {
        this.common.openSnackError(this.trans.trans('error_label'), this.trans.trans('no_change_msg'));
        resolve(false);
      }
      else
        this.common.postGeneric(localStorage.updateSkills, payload, { 'responseType': 'text' }).then(res => {
          this.common.openSnackBar(payload.type == 'update' ? this.trans.trans('update_successfully') : this.trans.trans('skills_create_msg'), this.trans.trans('close_label'), "background-snackbar-green");

          if (payload.type == 'update') {
            let index = this.allSkillsData.findIndex(e => e.PERSON_ID == this.empToBeUpdated.PERSON_ID);
            this.allSkillsData[index] = this.empToBeUpdated;
          } else {
            this.getAll();
          }
          resolve(true);
        })
    });
  }

  refresh() {
    this.load = true;
    this.getAll(_ => {
      this.load = false;
    });
  }
  getAll(done?) {
    this.common.getGeneric(localStorage.getAllSkills, null, this.isLazyLoadEnabled ? null : 1).then(e => {
      try { e = JSON.parse(e) } catch (ignored) { }
      if (e.STATUS == 'OK') {
        this.allSkillsData = e.DATA;
        this.allSkillsData.forEach(e => e.TYPE = this.common.getUserTypeById(e.PERSON_ID));
      }
      else {
        this.allSkillsData = [];
        console.log("error", e.DATA);
        this.common.info("ERROR", e.DATA);
      }
      if (done) done()
    });
  }

  isSkillsChanged(): Boolean {
    for (let x = 0; x < Object.keys(this.UpdateTemp).length; x++) {
      var type = Object.keys(this.UpdateTemp)[x];
      var arr = this.UpdateTemp[type];
      for (let y = 0; y < arr.length; y++) {
        var empSkill = this.empToBeUpdated.SKILLS.find(e => e.SECTION_CODE == arr[y].SECTION_CODE);
        if ((!empSkill && arr[y].SECTION_VALUE == 'Y') || (empSkill && empSkill.SECTION_VALUE != arr[y].SECTION_VALUE)) {
          return true;
        }
      }
    }
    return false;
  }

  onItemSelect(myForm) {

  }
  onSelectAll(myForm) {

  }
  onItemDeSelect(myForm) {

  }

  getEmpSkillSections(emp, type) {
    return emp.SKILLS.filter(e => e.SECTION_TYPE == type);
  }

  getCount() {
    return this.UpdateTemp[this.selectedCategory].filter(e => e.SECTION_VALUE == 'Y').length;
  }

  selectAll(isSelectAll, type) {
    this.UpdateTemp[type].filter(e => !e.hide).forEach(e => {
      e.SECTION_VALUE = isSelectAll ? 'Y' : 'N';
    });
  }

  searchKey: string = "";

  ischeckboxmode: boolean = true;
  search(event, type) {
    let key: string = event.target.value ? event.target.value : "";
    key = key.trim().toLowerCase();

    //reset all hide to false
    this.SkillsTypes.forEach(e => {
      this.UpdateTemp[e].forEach(n => n.hide = false);
    })

    this.UpdateTemp[type]
      .filter(n => !n.SECTION_NAME.toLowerCase().includes(key))
      .forEach(e => e.hide = true);
  }
  isAllChecked(Cattype) {
    return this.UpdateTemp[Cattype].every(e => e.SECTION_VALUE == 'Y')
  }

  reset() {
    // this.clearAllSelections();
    this.SkillsTypes.forEach(type => {

      this.UpdateTemp[type].forEach(i => {
        if (this.empToBeUpdated.SKILLS.find(s => s.SECTION_CODE == i.SECTION_CODE)) {
          i.SECTION_VALUE = 'Y';
        } else {
          i.SECTION_VALUE = 'N';
        }
      })
    })
  }

  openSelect(model, loadTemp) {
    this.modalService.open(loadTemp)
    this.common.getGeneric(localStorage.noSkills).then(res => {
      if (res.STATUS == 'OK') {
        console.log(res.DATA)
        this.employeeWithoutSkills = res.DATA.filter(e=>e.PERSON_TYPE_CODE == 'AUDITOR');
      }
      else
        console.log("error", res.DATA);
      this.selectedUserId = " "
      this.modalService.dismissAll();
      this.modalService.open(model, { centered: true, backdrop: 'static' });
    })
  }

  currentCategoryViewed = null;
  view(emp, type) {
    this.currentCategoryViewed = {
      type: type,
      PERSON_ID: emp.PERSON_ID,
      items: emp.SKILLS.filter(e => e.SECTION_TYPE == type)
    }
  }

  openAddingModel(UserId) {
    // this.openLg('add',UserId);
    if (this.employeeWithoutSkills.find(e => e.PERSON_ID == UserId)) {
      this.openLg('add', UserId);
    }
    else {
      this.common.openSnackBar(this.trans.trans('no_user'), this.trans.trans('close_label'), "background-snackbar-red");
    }
  }

  onSearchSkillsEmps(event) {
    let key: string = event.target.value ? event.target.value : "";
    key = key.trim().toLowerCase();

    //reset all hide to false
    this.allSkillsData.forEach(e => {
      e.hide = false;
    })

    this.allSkillsData
      .filter(n => !n.USER.toLowerCase().includes(key) && !(n.PERSON_ID + '').toLowerCase().includes(key))
      .forEach(e => e.hide = true);
    this.setupPaging();
  }
  getEmpCount() {
    return this.allSkillsData.filter(e => !e.hide).slice(0, this.length * this.pageIndex).length;
  }

  keydown(event) {

    if (event.target.value.length == 1 && event.code == "Backspace") {
      // if (event.srcElement.min && Number(event.srcElement.min) < event.srcElement.value) {
      this.length = 1;//Number(event.srcElement.min);
      // }
      return false;
    }

    if ((event.target.value.length == 1 && (event.code == "Backspace" || event.code == "Delete"))) {
      return false;
    }
    return true;
  }
  pagesCount = 1;
  pageIndex = 1;
  length = 6;//items per page

  // onFilterTypeChanged() {
  //   setTimeout(() => {
  //     this.employeeWithoutSkills.map(e => { e.show = false; return e; })
  //     .filter(em => this.types.find(e => e.CODE == em.PERSON_TYPE_CODE && e.isChecked))
  //     .forEach(e => e.show = true)
  //   }, 10);
  // }
}