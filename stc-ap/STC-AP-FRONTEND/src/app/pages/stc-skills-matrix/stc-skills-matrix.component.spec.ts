import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {NgForm} from '@angular/forms';
import { StcSkillsMatrixComponent } from './stc-skills-matrix.component';

describe('StcSkillMatrixComponent', () => {
  let component: StcSkillsMatrixComponent;
  let fixture: ComponentFixture<StcSkillsMatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcSkillsMatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcSkillsMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
