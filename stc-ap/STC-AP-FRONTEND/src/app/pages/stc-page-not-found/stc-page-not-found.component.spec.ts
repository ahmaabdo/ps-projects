import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcPageNotFoundComponent } from './stc-page-not-found.component';

describe('StcPageNotFoundComponent', () => {
  let component: StcPageNotFoundComponent;
  let fixture: ComponentFixture<StcPageNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcPageNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcPageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
