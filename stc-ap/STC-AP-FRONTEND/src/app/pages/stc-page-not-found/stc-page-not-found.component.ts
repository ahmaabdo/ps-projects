import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-stc-page-not-found',
  templateUrl: './stc-page-not-found.component.html',
  styleUrls: ['./stc-page-not-found.component.css']
})
export class StcPageNotFoundComponent implements OnInit {

  constructor(public common:CommonService) { }

  ngOnInit() {
  }

}
