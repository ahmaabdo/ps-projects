import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcMappingComponent } from './stc-mapping.component';

describe('StcMappingComponent', () => {
  let component: StcMappingComponent;
  let fixture: ComponentFixture<StcMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
