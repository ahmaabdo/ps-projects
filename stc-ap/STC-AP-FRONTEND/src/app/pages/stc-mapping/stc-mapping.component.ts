import { Component, OnInit } from '@angular/core';
import { MappingServiceService } from '../../services/mapping-service.service';
import { TranslationService } from '../../services/translation.service';
import { CommonService } from '../../services/common.service';
import { StaticBatchDataService } from 'src/app/services/static-batch-data.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-stc-mapping',
  templateUrl: './stc-mapping.component.html',
  styleUrls: ['./stc-mapping.component.css']
})
export class StcMappingComponent implements OnInit {
  currentPage: string = "mapping"
  empData: any[] = [];
  empId; empType;
  types = [];
  empSearch: string = "";
  allEmployees: any[];
  editMode: boolean = true;
  editableEmp: any;
  editableEmpType: any;
  activeTap = true;
  // empSearch ;
  constructor(private modalService: NgbModal, private batchData: StaticBatchDataService, private mappingData: MappingServiceService, public trans: TranslationService, public common: CommonService) { }

  ngOnInit() {

    this.mappingData.getUserTypes()
      .then(res => {
        if (res.STATUS == 'OK') {
          this.types = res.DATA;
          console.log("types", this.types);
          this.getUsers();
        }
        else
          console.log("ERROR", res.DATA);
      })
  }

  getUsers() {
    this.batchData.getAllUsers().then(res => {
      this.editMode = true
      if (res.STATUS == 'OK') {
        this.allEmployees = res.DATA;
        this.types.forEach(e => e.emps = this.allEmployees.filter(empFilter => empFilter.PERSON_TYPE_CODE == e.CODE))
      }
      else
        console.log("ERROR", res.DATA);
    })
  }

  searchForEmp() {
    this.mappingData.getUsers(this.empSearch).then(res => {
      if (res.STATUS == 'OK') {
        let isEn = this.trans.currentLang == 'en';
        let d = res.DATA.map(e => ({
          PERSON_ID: e.PERSOIN_ID,
          PERSON_NAME: isEn ? e.PERSON_NAME_E : e.PERSON_NAME
        }));
        this.empData = d;
        console.log(this.empData)
      }
      else
        console.log("ERROR", res.DATA);
    })
  }

  save(action, personID?, type?) {
    let payload;
    if (personID && type) {
      payload = {
        payload: JSON.stringify(
          [{
            'ACCOUNT_ID': this.common.getUserAccountById(personID) || null,
            'PERSON_ID': personID,
            'PERSON_TYPE_CODE': type
          }]
        ),
        method: action //create c  //update u
      }

    }
    else {
      let arr = this.empData.filter(e => e.type && e.type != 'none');
      if (arr.length == 0) {
        this.common.openSnackError(this.trans.trans('no_user'),this.trans.trans('at_least'));
        return;
      }
      payload = {
        payload: JSON.stringify(
          arr.map(emp => ({
            'PERSON_ID': emp.PERSON_ID,
            'PERSON_TYPE_CODE': emp.type
          }))
        ),
        method: action //create c  //update u
      }
    }
    this.mappingData.setType(payload).then(res => {
      console.log(res);
      this.common.openSnackSuccess(action=='c'?this.trans.trans('mapping_done_msg'):this.trans.trans('mapping_edit_msg'), this.trans.trans('page_refresh_msg'));
      // this.searchForEmp();
      this.getUsers();
    })
  }

  onDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {

      this.common.confirm('Confirm_label',this.activeTap ? this.trans.trans("deactive_msg").replace("#name#",event.item.element.nativeElement.innerText)+"?": this.trans.trans("change_type_msg").replace("#name#",event.item.element.nativeElement.innerText).replace("#from#",this.trans.trans(event.item.element.nativeElement.dataset['target'])).replace("#to#",this.trans.trans(event.container.element.nativeElement.id))).then(e => {
        console.log(e);
        if (e) {
          this.save("u", event.item.element.nativeElement.id, event.container.element.nativeElement.id)
          transferArrayItem(event.previousContainer.data,
            event.container.data,
            event.previousIndex,
            event.currentIndex);
          console.log(event.item.element.nativeElement.dataset['target'])
        }

      })
    }
  }

  openEditModal(modal, emp) {
    this.editableEmp = emp;
    console.log(emp);
    this.editableEmpType = emp.PERSON_TYPE_CODE
    this.modalService.open(modal, { centered: true, backdrop: 'static' })
  }
  saveEditMode() {
    if (this.editableEmp.PERSON_TYPE_CODE != this.editableEmpType) {
      this.modalService.dismissAll();
      this.common.confirm('Confirm_label',this.activeTap ? this.trans.trans("deactive_msg").replace("#name#",this.editableEmp.PERSON_NAME)+"?": this.trans.trans("change_type_msg").replace("#name#",this.editableEmp.PERSON_NAME).replace("#from#",this.trans.trans(this.editableEmp.PERSON_TYPE_CODE)).replace("#to#",this.trans.trans(this.editableEmpType))).then(e => {
        if (e) {
          this.save("u", this.editableEmp.PERSON_ID, this.editableEmpType)
        }
      });
    }
    else {
      this.common.openSnackError(this.trans.trans('no_type_change'), this.trans.trans('close_label'));
    }
  }

  activeUser(user) {
    this.common.confirm('Confirm_label', 'active_msg').then(yes => {
      if (yes) {
        this.save("u", user.PERSON_ID, 'ACTIVE');
      }
    })
  }

}
