import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcUnauthorizedComponent } from './stc-unauthorized.component';

describe('StcUnauthorizedComponent', () => {
  let component: StcUnauthorizedComponent;
  let fixture: ComponentFixture<StcUnauthorizedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcUnauthorizedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcUnauthorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
