import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-stc-unauthorized',
  templateUrl: './stc-unauthorized.component.html',
  styleUrls: ['./stc-unauthorized.component.css']
})
export class StcUnauthorizedComponent implements OnInit {

  constructor(
    private router: Router,
    public common: CommonService,
    private _location: Location
  ) { }

  ngOnInit() {
    if (localStorage.user) {
      console.log("hello")
      this.router.navigateByUrl(this.common.getUserType() == "FOCALPOINT" ? '/DashBoard/FocalDashboard' : '/DashBoard/MyAccount', { skipLocationChange: true });
    }
    this._location.replaceState('/');
  }
}
