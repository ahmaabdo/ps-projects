import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor() {
    $(() => {

      const box = $('.box'),
        ph = $('.box-placeholder');

      let toggleEffect = () => {
        box.hide();
        ph.show();

        setTimeout(() => {
          ph.hide();
          box.show();
        }, 2e3);
      };

      toggleEffect();
      setInterval(toggleEffect, 4e3);
    });
  }

  cards = [200, 200, 100, 200, 100];

  masonryItems = [
    { title: 'item 1', height: 100 },
    { title: 'item 2', height: 100 },
    { title: 'item 3', height: 200 },
    { title: 'item 4', height: 100 },
    { title: 'item 5', height: 300 },
    { title: 'item 6', height: 100 }
  ];

  items = [{ color: 'red', text: 'text text' }, { color: 'green', text: 'text text' }, { color: 'blue', text: 'text text' }, { color: 'red', text: 'text text text text' }]

  resons: Array<{ main: string, sub: string, isOpen: boolean }>;
  main: any[] = ['main1', 'main2', 'main3', 'other'];
  sub: any[] = ['sub1', 'sub2', 'sub3'];
  ngOnInit() {
  }
}
