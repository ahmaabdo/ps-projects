import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcNavbarComponent } from './stc-navbar.component';

describe('StcNavbarComponent', () => {
  let component: StcNavbarComponent;
  let fixture: ComponentFixture<StcNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
