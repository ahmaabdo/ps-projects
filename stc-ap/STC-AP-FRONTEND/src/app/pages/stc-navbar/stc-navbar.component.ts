import { Component, OnInit, Input, NgModule } from '@angular/core';
import * as $ from 'jquery';
import { MatMenuModule } from '@angular/material/menu';
import { StaticBatchDataService } from '../../services/static-batch-data.service';
import { CommonService } from '../../services/common.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { NotificationsService } from '../../services/notifications.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslationService } from '../../services/translation.service';



@NgModule({

  imports: [MatMenuModule]

})

@Component({
  selector: 'stc-navbar',
  templateUrl: './stc-navbar.component.html',
  styleUrls: ['./stc-navbar.component.css']
})
export class StcNavbarComponent implements OnInit {

  data;

  pages = [
  ]
  @Input('currentPage') currentPage: string = "dashboard";
  userName: string;
  notificationsHeader: any[] = [];
  openedNotificaton: any[] = [];
  constructor(private language: TranslationService, private modalService: NgbModal, public notifications: NotificationsService, public common: CommonService, public invoiceData: StaticBatchDataService, private router: Router, private _snackBar: MatSnackBar) {
    this.pages = [
      {
        name: 'dashboard',
        link: this.common.getUserType() == 'FOCALPOINT' ? '/DashBoard/FocalDashboard' : (this.common.getUserType() == 'VP' ? '/VpDashBoard' : '/DashBoard/MyAccount'),
        label: this.language.trans('Dashboard'),
        visible: true
      },
      {
        name: 'skillsmatrix',
        link: '/SkillsMatrix',
        label: this.language.trans('skillsMatrix_label'),
        visible: this.common.getUserType() == 'FOCALPOINT'
      },
      {
        name: 'sla',
        link: '/StcSla',
        label: this.language.trans('sla_label'),
        visible: this.common.getUserType() == 'FOCALPOINT'
      },
      {
        name: 'reports',
        link: '/StcReports',
        label: this.language.trans('reports_label'),
        visible: this.common.getUserType() == 'FOCALPOINT'
      },
      {
        name: 'statistics',
        link: '/StcStatistics',
        label: this.language.trans('statistics_label'),
        visible: this.common.getUserType() == 'FOCALPOINT' || this.common.getUserType() == 'SUPERVISOR' || this.common.getUserType() == 'VP'
      },
      {
        name: 'mapping',
        link: '/StcMapping',
        label: this.language.trans('mapping'),
        visible: this.common.getUserType() == 'FOCALPOINT'
      },
    ]
  }

  ngOnInit() {
    // console.log(this.userName);

    this.invoiceData.userLogin = "MyAccount";
    /********************jquery*****************/
    $(document).ready(function () {

      /********************navbar links border************** */
      $("#Stc-Navbar .links li").mouseenter(function () {
        $(this).addClass("activeLink");
        $(this).siblings().removeClass("activeLink");
      });
      $("#Stc-Navbar .links li").mouseleave(function () {
        $(this).removeClass("activeLink");
      });

      /************************left Navbar******************** */

      /********hide in small screen ****** */
      if ($(window).width() > 767) {
        $("#Stc-LeftNavbar").hide();
      }
      $(window).resize(function () {
        if ($(window).width() > 767) {
          $("#Stc-LeftNavbar").hide();
        }
      });
      /********************************* */
      /*************open left navbar***** */
      $("#Stc-Navbar .fa-bars").click(function () {
        $("#Stc-LeftNavbar").show();
      });
      /**********close left Navbar***** */
      $("#Stc-LeftNavbar .fa-times").click(function () {
        $("#Stc-LeftNavbar").hide();
      });
    });
  }

  // ShowDetails(content, id) {
  //   this.data = {
  //     notificationId: id,
  //     userName: this.common.getCurrentUser().P_PERSON_NAME
  //   }


  // this.notifications.getNotificationData(id).then(res => {
  //   if (res.STATUS == 'OK' && res.DATA.length > 0) {
  //     this.openedNotificaton = res.DATA[0];
  //     this.modalService.open(content, { size: "lg", centered: true, backdrop: 'static' });
  //   }
  //   else {
  //     console.log("error", res.DATA);
  //     this.common.openSnackError("Error", res.DATA);
  //   }
  //   console.log(this.openedNotificaton)
  // })
  // }

  // closeNotificationFun() {
  //   this.modalService.dismissAll();
  //   console.log("payload" + this.data)
  //   this.notifications.closeNotification(this.data).then(res => {
  //     this.notificationsHeader = this.notificationsHeader.filter(el => el.NOTIFICATION_ID != this.data.notificationId)
  //     this.common.openSnackBar('Notification removed from notification', 'close', 'background-snackbar-green')
  //   })
  // }
}
