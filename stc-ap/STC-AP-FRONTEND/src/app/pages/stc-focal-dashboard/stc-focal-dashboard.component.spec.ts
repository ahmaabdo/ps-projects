import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcFocalDashboardComponent } from './stc-focal-dashboard.component';

describe('StcFocalDashboardComponent', () => {
  let component: StcFocalDashboardComponent;
  let fixture: ComponentFixture<StcFocalDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcFocalDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcFocalDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
