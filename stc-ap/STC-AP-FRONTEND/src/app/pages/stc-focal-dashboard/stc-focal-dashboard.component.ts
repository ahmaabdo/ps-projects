import { EventEmitter, Component, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StaticBatchDataService } from '../../services/static-batch-data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';
import { CommonService } from '../../services/common.service';
import { NgxMasonryOptions } from 'ngx-masonry';
import { TranslationService } from '../../services/translation.service'
import { MappingServiceService } from '../../services/mapping-service.service';
import { SlaDataService } from '../../services/sla-data.service';
@Component({
  selector: 'app-stc-focal-dashboard',
  templateUrl: './stc-focal-dashboard.component.html',
  styleUrls: ['./stc-focal-dashboard.component.css']
})
export class StcFocalDashboardComponent implements OnInit {
  public isCollapsed: boolean = false;
  @Output() searchItem: EventEmitter<any> = new EventEmitter();
  searchText;
  public myOptions: NgxMasonryOptions = {
    transitionDuration: '0',
    originLeft: this.trans.direction() !== 'rtl',
    columnWidth: 300,
    // horizontalOrder: true
  };
  ShowPending: boolean = true; //show and hide pending and assigned section
  openedInvoice: any; //popup invoice 
  // apiInvocie: any[] = [];
  showLoadImage: boolean = false;
  urgent: string = 'N';
  getAllUs: any;
  empName: any;
  empType: any;
  theme: string = "three";
  peronsInvoices: any[] = [];
  types: any[] = [];
  filterObj: any = {
    selectedCode: 'all'
  }

  isMultiAssignVisable: boolean = false;
  constructor(private sla: SlaDataService, private mappingData: MappingServiceService, public route: ActivatedRoute, public trans: TranslationService, public batch: StaticBatchDataService, private modalService: NgbModal, private invoicesData: StaticBatchDataService, private router: Router, public common: CommonService) {
    this.getUsersForPending();
  }

  ngOnInit() {
    this.getInvoices();
    if (this.common.getUserType() != 'FOCALPOINT') {
      this.router.navigateByUrl("**", { skipLocationChange: true })
    }

    this.mappingData.getUserTypes()
      .then(res => {
        if (res.STATUS == 'OK') {
          this.types = res.DATA.filter(e => e.CODE != -1);
          this.types.unshift({ DESCRIPTION: 'All', CODE: 'all' });
        }
        else
          console.log("ERROR", res.DATA);
      })

  }
  getUsersForPending() {
    this.batch.getAllUsers().then(res => {
      try { res = JSON.parse(res) } catch (i) { }
      this.getAllUs = res.DATA.filter(e => e.PERSON_TYPE_CODE == 'AUDITOR' && e.STATUS == 'ACTIVE');
    });
  }

  getInvoices() {
    this.showLoadImage = true;
    this.invoicesData.getFocalInvoices().then(resp => {
      try {
        resp = JSON.parse(resp)

      }
      catch (igore) { };
      if (resp.STATUS == 'OK') {
        this.createMasterObject(resp.DATA);
      }
      else {
        // this.apiInvocie = [];
        // console.log("ERROR", resp.DATA)
        this.common.info("ERROR", resp.DATA);
      }
      this.showLoadImage = false;
    })
  }

  open(content, personName) {
    this.empName = personName
    console.log(personName)

    var id = this.getAllUs.find(el => el.PERSON_NAME == personName).PERSON_ID;
    var empType = this.getAllUs.find(el => el.PERSON_NAME == personName).PERSON_TYPE_CODE;

    this.empType = empType;

    console.log(id)
    console.log(this.empType)

    this.batch.getPersonInvoices(id).then(res => {
      try { res = JSON.parse(res); } catch (ignored) { }
      if (res.STATUS == 'OK') {
        this.peronsInvoices = res.DATA;
        console.log(this.peronsInvoices);
      }
      else
        console.log("error", res.DATA);

      this.modalService.open(content, { size: 'lg', centered: true, backdrop: 'static' });
    });
  }
  // assignedEmpBaches: any = {};

  drop(event: CdkDragDrop<{}[]>) {

    if (event.previousContainer == event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    }

    else {
      let toUser = event.container.element.nativeElement['id'];
      event.previousContainer.element.nativeElement.childNodes.forEach(e => {
        if (e['classList']) {
          e['classList'].forEach(h => {
            if (h == 'cdk-drag-dragging') {
              e['classList'].add("disabled");
            }
          })
        }
      });
      let payload = {
        payload: JSON.stringify([{
          "ACTION": "REASSIGN",
          "PERSON_ID": toUser,
          "INVOICE_ID": event.item.data["InvoiceId"],
          "ACCOUNTANT_INVOICE_ID": event.item.data["accountantId"],
          "ITEM_KEY": "1",
        }])
      };
      this.invoicesData.assignAction(payload)
        .then(res => {
          this.getInvoices();
          // this.apiInvocie.find(e => e.InvoiceId == event.item.data.InvoiceId).pesronId = toUser;
        })
    }
  }

  // check duplication
  dropAssign(event: CdkDragDrop<{}[]>) {

    if (event.previousContainer == event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    }

    else {
      let toUser = event.container.element.nativeElement['id'];
      event.previousContainer.element.nativeElement.childNodes.forEach(e => {
        if (e['classList']) {
          e['classList'].forEach(h => {
            if (h == 'cdk-drag-dragging') {
              e['classList'].add("disabled");
            }
          })
        }
      });

      let payload = {
        payload: JSON.stringify([{
          "ACTION": "ASSIGN",
          "PERSON_ID": toUser,
          "INVOICE_ID": event.item.data["InvoiceId"],
          "ACCOUNTANT_INVOICE_ID": event.item.data["accountantId"],
          "ITEM_KEY": "1",
        }])
      };
      this.invoicesData.assignAction(payload)
        .then(res => {
          // this.apiInvocie.find(e => e.InvoiceId == event.item.data.InvoiceId).STATUS = "ASSIGNED";
          this.getInvoices();
          this.getUsersForPending();
          this.common.openSnackBar(this.trans.trans('invoice_assign_msg'), this.trans.trans('close_label'), 'background-snackbar-green')
        })
    }
  }

  calcLeftPercent(i) {
    //set sla values based on assigned user type
    let type = i.STATUS.split("-")[0];
    let CONFIG = i.SLA && i.SLA.STATUS == 'OK' ? i.SLA.DATA : null;
    let Sla_value = 1;
    if (type && CONFIG) {
      switch (type) {
        case 'AUD':
          Sla_value = CONFIG['invoiceReview'];
          break;
        case 'TAXAUD':
          Sla_value = CONFIG['taxAccountantReview'];
          break;
        case 'SUP':
          Sla_value = CONFIG['supervisorReview'];
          break;
        case 'TAXSUP':
          Sla_value = CONFIG['taxSupervisorReview'];
          break;
      }
    }
    i.SlaRemainingDays = Sla_value - i.AssignedSince;
    let remingSlaPercent = i.SlaRemainingDays && Sla_value ? Math.floor((i.SlaRemainingDays / Sla_value) * 100) : 100;
    i.remingSlaPercent = remingSlaPercent > 0 ? remingSlaPercent : 0;
    i.LeftPercent = this.common.round(i.AssignedSince * 100 / Sla_value, 2);
  }
  masterObject = {
    pending: [],//[{name,loaded,show,invoices}]
    assigned: [],//[{name,id,workload,batches[{name,loaded,show,invoices}]}]
    MAX_LOADED: 5,
    PENDING_LENGTH: 0,
    ASSIGNED_LENGTH: 0,
    IS_SEARCHING: false
  }
  createMasterObject(allInvoices) {
    this.resetMaster();
    // sort
    allInvoices.filter(e => e.STATUS.includes("ASSIGNED")).forEach(e => {
      this.calcLeftPercent(e);
    })
    allInvoices.sort((a, b) => a.LeftPercent > b.LeftPercent ? -1 : 1);
    //-----
    allInvoices.forEach(i => {
      if (i.STATUS == "PENDING") {
        if (this.masterObject.pending.find(e => e.name == i.BatchName)) {
          this.masterObject.pending.find(e => e.name == i.BatchName).invoices.push(i);
        } else {
          this.masterObject.pending.push({ name: i.BatchName, loaded: this.masterObject.MAX_LOADED, invoices: [i] });
        }
        this.masterObject.PENDING_LENGTH += 1;
      }
      else if (i.STATUS.includes("ASSIGNED")) {
        // console.log(i.LeftPercent);
        if (this.masterObject.assigned.find(e => e.id == i.pesronId)) {

          if (this.masterObject.assigned.find(e => e.id == i.pesronId).batches.find(e => e.name == i.BatchName))
            this.masterObject.assigned.find(e => e.id == i.pesronId).batches.find(e => e.name == i.BatchName).invoices.push(i);
          else
            this.masterObject.assigned.find(e => e.id == i.pesronId).batches.push({ name: i.BatchName, loaded: this.masterObject.MAX_LOADED, invoices: [i] })
        } else {
          this.masterObject.assigned.push({ name: i.personName, id: i.pesronId, TYPE: this.common.getUserTypeById(i.pesronId), workload: i.WORKLOAD, batches: [{ name: i.BatchName, loaded: this.masterObject.MAX_LOADED, invoices: [i] }] });
        }
        this.masterObject.ASSIGNED_LENGTH += 1;
      }
    });
  }

  resetMaster() {

    this.masterObject = {
      pending: [],
      assigned: [],
      MAX_LOADED: 5,
      PENDING_LENGTH: 0,
      ASSIGNED_LENGTH: 0,
      IS_SEARCHING: false
    }
  }
  //this methoud called from datshboard.ts
  onSearch(key: string) {

    //pending
    if (this.ShowPending)
      this.masterObject.pending.forEach(bache => {
        bache.loaded = this.masterObject.MAX_LOADED
        bache.invoices.forEach(invoice => {
          if (key && key.length > 0) {
            this.masterObject.IS_SEARCHING = true;
            if (!this.common.isInvoiceMatches(invoice, key))
              invoice.hide = true;
            else
              invoice.hide = false;
          }
          else {
            this.masterObject.IS_SEARCHING = false;
            invoice.hide = false;
          }
        })
        bache.invisable = bache.invoices.every(e => e.hide);
      });

    else
      this.masterObject.assigned.forEach(emp => {      //assigned
        emp.batches.forEach(bache => {
          bache.loaded = this.masterObject.MAX_LOADED
          bache.invoices.forEach(invoice => {
            if (key && key.length > 0) {
              this.masterObject.IS_SEARCHING = true;
              if (!this.common.isInvoiceMatches(invoice, key))
                invoice.hide = true;
              else
                invoice.hide = false;
            }
            else {
              this.masterObject.IS_SEARCHING = false;
              invoice.hide = false;
            }

          })
          bache.invisable = bache.invoices.every(e => e.hide);
        });

        emp.invisable = emp.batches.every(e => e.invisable);
      })

  }
  checkHidden(batch) {
    return batch['invoices'].filter(e => !e.hide).length
  }
  newWidth() {
    if (this.theme == 'one')
      return (8 / 10) * window.innerWidth < 300 ? 300 : (8 / 10) * window.innerWidth;
    else if (this.theme == 'two')
      return (3.9 / 10) * window.innerWidth < 300 ? 300 : (3.9 / 10) * window.innerWidth;
    else
      return (2.2 / 10) * window.innerWidth < 300 ? 300 : (2.2 / 10) * window.innerWidth;
  }
  isSearchEmpBox: boolean = false;

  onRefresh() {
    if (this.route.snapshot.routeConfig.path == 'FocalDashboard')
      this.getInvoices();
    console.log("StcFocalDashboardComponent", "onRefresh", this.route.snapshot.routeConfig.path);
  }

  // getCountOfEmpsAndInvoicesByType(type) {

  //   return '0 emps (3 invoices)';
  // }

  log(i){
    // console.log(i.LeftPercent)
  }
}