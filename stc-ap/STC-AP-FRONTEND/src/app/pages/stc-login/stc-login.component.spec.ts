import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcLoginComponent } from './stc-login.component';

describe('StcLoginComponent', () => {
  let component: StcLoginComponent;
  let fixture: ComponentFixture<StcLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
