import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../../services/statistics.service'
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { TranslationService } from '../../services/translation.service';

@Component({
  selector: 'app-stc-statistics',
  templateUrl: './stc-statistics.component.html',
  styleUrls: ['./stc-statistics.component.css']
})
export class StcStatisticsComponent implements OnInit {
  currentPage: string = "statistics"
  public chartType: string = 'bar';
  months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sept', 'oct', 'nov', 'dec'];
  masterReport: any = {};//{data:[overview array{title,no,years,months,weeks}],years,months,weeks}
  colors = ['#94327f', '#da3f7b', '#efac2a', '#64cc9c', '#999'];
  public chartColors: any[] = [];
  placeHolderArr: any[] = [1, 2, 3]
  public chartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };
  chartData: any;
  chartLabels: any;
  load: boolean = false;
  content: any = {};
  public emps: any[] = []
  public leaders: any[] = [];
  days: any

  duration = 7;
  dateFrom;
  dateTo;
  userTypes = [];


  selectedOverview;
  bestEmployees: any[];

  constructor(public trans: TranslationService, private language: TranslationService, private _report: StatisticsService, private modalService: NgbModal, private router: Router, public common: CommonService) {
    this.userTypes = common.getSearchLookup();
  }

  saveChart(can) {
    can.toBlob(function (blob) {
      saveAs(blob, "Statistics-chart.png")
    })
  }

  change(type) {
    return;
    if (type == 'Year') {
      this.chartData = this.masterReport.year;
      this.chartLabels = this.masterReport.yearLabels

    }
    else if (type == 'Month') {
      this.chartData = this.masterReport.Months;
      this.chartLabels = this.masterReport.monthLabels;
    }
    else {
      this.chartData = this.masterReport.week;
      this.chartLabels = this.masterReport.weekLabels;
    }

  }

  ngOnInit() {
    this.search();
  }


  doSearch(end, start) {
    this.load = true
    this.common.getGeneric(localStorage.statistics, { params: { 'start': start, 'end': end, 'lang': localStorage.lang == 'ar' ? 'AR' : 'US' } })
      .then(res => {
        console.log(res);
        this.load = false;
        if (res.leaders.STATUS == 'OK') {
          this.leaders = res.leaders.DATA.sort((a, b) => (a.COMPLATION_RATE + (a.EFFICIENCY || 0) + (a.PERCENT_OF_TIME || 0)) < (b.COMPLATION_RATE + (b.EFFICIENCY || 0) + (b.PERCENT_OF_TIME || 0)) ? 1 : -1);
          this.emps = this.leaders.slice(0, 3);
          this.emps.forEach(e => e.TYPE = this.common.getUserTypeById(e.PERSON_ID));
          this.bestEmployees = this.emps.reverse()
        }
        // if (res.achieverPerWeek.STATUS == 'OK') {
        //   this.emps = res.achieverPerWeek.DATA.splice(0, 3);
        //   this.emps.forEach(e => e.TYPE = this.common.getUserTypeById(e.PERSON_ID));
        //   this.bestEmployees = this.emps.reverse()
        // }
        this.initMasterReport(res.overview);
      })
    console.log(this.common.getUserType())
    if (this.common.getUserType() != 'FOCALPOINT' && this.common.getUserType() != 'SUPERVISOR' && this.common.getUserType() != 'VP') {
      this.router.navigateByUrl("**", { skipLocationChange: true })
    }
  }

  search() {
    if (!this.duration)
      return;

    let _endDate: any;
    let _startDate: any;

    if (this.duration != -1) {
      _endDate = this.common.getDate(["y", "m", "d", "-"]);
      _startDate = this.common.subDate(_endDate, this.duration);

      _endDate = this.common.getDate(["d", "m", "y", "/"], [_endDate, 'y', 'm', 'd', '-']);
      _startDate = this.common.getDate(["d", "m", "y", "/"], _startDate);
    } else {
      if (!this.dateFrom || !this.dateTo)
        return;
      _endDate = this.common.getDate(["d", "m", "y", "/"], this.dateTo);
      _startDate = this.common.getDate(["d", "m", "y", "/"], this.dateFrom);
    }
    this.doSearch(_endDate, _startDate);
  }

  formatNumber(n) {
    if (n > 1000000)
      return (n / 1000000) + ' M';
    else if (n > 1000)
      return (n / 1000) + ' K';
    else
      return n;
  }
  open(approveContent) {
    this.modalService.open(approveContent);
  }

  initMasterReport(overview) {
    if (!overview || !overview.DATA)
      return
    this.masterReport.overview = overview.DATA;
    this.masterReport.overview.forEach(item => {
      let data: any[] = [], labels: any[] = [];

      if (item.DATA.STATUS == 'OK') {
        if (item.CODE == 'TOTAL_APPROVED_PINDING_INVOICES_PER_SECTOR') {
          labels = item.DATA.DATA.map(e => e.SOURCE);
          data = [{ data: item.DATA.DATA.map(e => Number(e['TATAL APPROVED INVOICE'])), label: 'TATAL APPROVED INVOICE' }, { data: item.DATA.DATA.map(e => Number(e['TATAL PINDING INVOICE'])), label: 'TATAL PINDING INVOICE' }]
        }
        else if (item.CODE == 'TOTAL_ASSIGN_APPROVED_INVOICE_PER_EMPLOYEE') {
          labels = item.DATA.DATA.map(e => e.EMPLOYEE_NAME);
          data = [{ data: item.DATA.DATA.map(e => Number(e['APPROVED INVOICE'])), label: 'APPROVED INVOICE' }, { data: item.DATA.DATA.map(e => Number(e['ASSIGNED INVOICE'])), label: 'ASSIGNED INVOICE' }]
        }

        else if (item.CODE == 'TOTAL_REJECTED_INVOICES_PER_SECTOR') {
          labels = item.DATA.DATA.map(e => e.SOURCE);
          data = [{ data: item.DATA.DATA.map(e => Number(e['REJECTED_INVOICE'])), label: 'REJECTED_INVOICE' }]
        }
      }

      item.chartLabels = labels;
      item.total = 0;
      try {
        item.total = (<any>data.map(e => e.data)).flat(1).reduce((p, c) => (p || 0) + c);
      } catch (e) { };
      item.chartData = data;
    });

    this.selectedOverview = this.masterReport.overview[0];

    // console.log(this.masterReport.overview)
    // this.view(this.masterReport.overview[0])

    // this.chartColors = overview.data.map((e, index) => {
    //   return {
    //     backgroundColor: this.colors[index % this.colors.length],
    //     borderColor: this.colors[index % this.colors.length],
    //     borderWidth: 2,
    //   };
    // });

    // this.masterReport.yearLabels = overview.years;
    // this.masterReport.monthLabels = overview.months.map(e => this.trans.trans(this.months[e - 1]));
    // this.masterReport.weekLabels = overview.weeks;

    // this.masterReport.year = overview.data.map(e => ({ data: e.years, label: e.title }));
    // this.masterReport.Months = overview.data.map(e => ({ data: e.months, label: e.title }));
    // this.masterReport.week = overview.data.map(e => ({ data: e.weeks, label: e.title }));

    // console.log(this.masterReport.year, this.masterReport.yearLabels)
    // this.chartData = [{ "label": "خالد محمد عبدالرحمن ال دبل", "data": [0, 1] }, { "label": "فيصل محمد ابراهيم بن صليهم", "data": [0, 7] }, { "label": "محمود حسن محمد عوض", "data": [0, 1] }, { "label": "وقيان حمد وقيان الدوسري", "data": [0, 1] }];//this.masterReport.year;
    // this.chartLabels = ['APPROVED INVOICE', 'ASSIGNED INVOICE'];//this.masterReport.yearLabels;
  }

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }


  view(item) {
    console.log(item)
    if (item.DATA.STATUS == 'OK') {
      let data: any[] = [], labels: any[] = [];
      if (item.CODE == 'TOTAL_APPROVED_PINDING_INVOICES_PER_SECTOR') {
        labels = item.DATA.DATA.map(e => e.SOURCE);
        data = [{ data: item.DATA.DATA.map(e => Number(e['TATAL APPROVED INVOICE'])), label: 'TATAL APPROVED INVOICE' }, { data: item.DATA.DATA.map(e => Number(e['TATAL PINDING INVOICE'])), label: 'TATAL PINDING INVOICE' }]
      }
      else if (item.CODE == 'TOTAL_ASSIGN_APPROVED_INVOICE_PER_EMPLOYEE') {
        labels = item.DATA.DATA.map(e => e.EMPLOYEE_NAME);
        data = [{ data: item.DATA.DATA.map(e => Number(e['APPROVED INVOICE'])), label: 'APPROVED INVOICE' }, { data: item.DATA.DATA.map(e => Number(e['ASSIGNED INVOICE'])), label: 'ASSIGNED INVOICE' }]
      }

      else if (item.CODE == 'TOTAL_REJECTED_INVOICES_PER_SECTOR') {
        labels = item.DATA.DATA.map(e => e.SOURCE);
        data = [{ data: item.DATA.DATA.map(e => Number(e['REJECTED_INVOICE'])), label: 'REJECTED_INVOICE' }]
      }

      this.chartLabels = labels;
      this.chartData = data;
    }
  }
}

