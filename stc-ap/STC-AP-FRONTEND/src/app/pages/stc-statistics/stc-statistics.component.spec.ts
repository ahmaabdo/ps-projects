import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcStatisticsComponent } from './stc-statistics.component';

describe('StcStatisticsComponent', () => {
  let component: StcStatisticsComponent;
  let fixture: ComponentFixture<StcStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
