import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StaticBatchDataService } from '../../services/static-batch-data.service';
import { CommonService } from '../../services/common.service';
import { OrderPipe } from 'ngx-order-pipe';
import { TranslationService } from '../../services/translation.service';
import { NgxMasonryOptions } from 'ngx-masonry';

@Component({
  selector: 'app-stc-my-account',
  templateUrl: './stc-my-account.component.html',
  styleUrls: ['./stc-my-account.component.css']
})
export class StcMyAccountComponent implements OnInit {
  showLoadImage: boolean = false; //show loading image 
  theme: string = "three";
  batchChange: boolean = true;
  placeHolderArr: any[] = [1, 2, 3]
  constructor(public route: ActivatedRoute, public trans: TranslationService, private router: Router, public batch: StaticBatchDataService, public common: CommonService, public orderPipe: OrderPipe) {
  }
  public myOptions: NgxMasonryOptions = {
    transitionDuration: '0',
    originLeft: this.trans.direction() !== 'rtl',
    columnWidth: 300,
  };
  ngOnInit() {
    if (this.common.getUserType() != "FOCALPOINT") {
      this.getInvoices();
      this.searchDuration(7);
      if (this.common.getUserType() == "SUPERVISOR")
        this.getMyAdutiiorInvoives();
    } else {
    }
    console.log(localStorage.user)
    /***********ensure login  *********/
    if (this.common.getUserType() == "FOCALPOINT") {
      this.router.navigateByUrl("**", { skipLocationChange: true })
    }
  }


  getInvoices(isSilent?: boolean) {
    this.showLoadImage = !isSilent;
    this.batch.getMyAccountInvoice().then(res => {
      if (res.STATUS != 'OK') {
        console.log("error", res.DATA);
        this.showLoadImage = false;
        return;
      }
      this.CustomBacklog(res.DATA, 'backlog')
      this.showLoadImage = false;
    })
  }

  getMyAdutiiorInvoives() {
    this.batch.getMyAudtiorInvoices().then(res => {
      console.log(res);
      if (res.STATUS == 'OK') {

        let arr = [];
        res.DATA.forEach(i => {

          if (arr.find(e => e.id == i.pesronId)) {
            if (arr.find(e => e.id == i.pesronId).batches.find(e => e.name == i.BatchName))
              arr.find(e => e.id == i.pesronId).batches.find(e => e.name == i.BatchName).invoices.push(i);
            else
              arr.find(e => e.id == i.pesronId).batches.push({ name: i.BatchName, loaded: this.masterObject.MAX_LOADED, invoices: [i] })
          } else {
            arr.push({ name: i.personName, id: i.pesronId, batches: [{ name: i.BatchName, loaded: this.masterObject.MAX_LOADED, invoices: [i] }] });
          }
        });
        this.masterObject.MYAUDTIOR = arr;
      }
      else
        console.log("error", res.DATA);
    })
  }

  CustomBacklog(invoices, type: string) {
    if (type == 'backlog') {
      this.masterObject.BACKLOG = [];
      this.masterObject.BACKLOG_length = 0;
      invoices.forEach(i => {
        //add remingSlaPercent
        // i.remingSlaPercent = i.SlaRemainingDays && i.Sla_value ? Math.floor((i.SlaRemainingDays / i.Sla_value) * 100) : 100;
        // console.log(i.remingSlaPercent,i.LeftPercent);

        if (this.masterObject.BACKLOG.find(e => e.name == i.BatchName))
          this.masterObject.BACKLOG.find(e => e.name == i.BatchName).invoices.push(i)
        else
          this.masterObject.BACKLOG.push({ name: i.BatchName, loaded: this.masterObject.MAX_LOADED, invoices: [i] })
        this.masterObject.BACKLOG_length += 1;
        // this.masterObject.BACKLOG.forEach(batch => {
        //   batch.invoices.sort((a, b) => a.remingSlaPercent < b.remingSlaPercent ? -1 : 1)
        // })
      })
      // order by number of red invoices
      // this.masterObject.BACKLOG.sort((a, b) => (a.invoices.slice(0, 3).filter(e => e.remingSlaPercent < 33)) < (b.invoices.slice(0, 3).filter(e => e.remingSlaPercent < 33)) ? 1 : -1);
    }
    else {
      this.masterObject.COMPLETED = [];
      this.masterObject.COMPLETED_length = 0;
      invoices.forEach(i => {
        if (this.masterObject.COMPLETED.find(e => e.name == i.BatchName))
          this.masterObject.COMPLETED.find(e => e.name == i.BatchName).invoices.push(i)
        else
          this.masterObject.COMPLETED.push({ name: i.BatchName, loaded: this.masterObject.MAX_LOADED, invoices: [i] })
        this.masterObject.COMPLETED_length += 1;
      })
    }
    console.log(this.masterObject)
  }

  masterObject = {
    MAX_LOADED: 3,
    BACKLOG: [],//[{name:"A",invoices:[],loaded:MAX_LOADED}]
    COMPLETED: [],//[{name:"A",invoices:[],loaded:MAX_LOADED}]
    COMPLETED_length: 0,
    BACKLOG_length: 0,
    IS_SEARCHING: false,
    ASSIGNED: [],//[{name,id,workload,batches[{name,loaded,invoices:[]}]}]
    ASSIGNED_LENGTH: 0,
    MYAUDTIOR: [],//[{name,id,workload,batches[{name,loaded,invoices:[]}]}]
  }

  //this methoud called from datshboard.ts
  onSearch(key: string) {
    //backlog
    this.masterObject.BACKLOG.forEach(bache => {
      bache.loaded = this.masterObject.MAX_LOADED
      bache.invoices.forEach(invoice => {
        if (key && key.length > 0) {
          this.masterObject.IS_SEARCHING = true;
          if (!this.common.isInvoiceMatches(invoice, key))
            invoice.hide = true;
          else
            invoice.hide = false;
        }
        else {
          this.masterObject.IS_SEARCHING = false;
          invoice.hide = false;
        }
      })
      bache.invisable = bache.invoices.every(e => e.hide);
    });

    //completed
    this.masterObject.COMPLETED.forEach(bache => {
      bache.loaded = this.masterObject.MAX_LOADED
      bache.invoices.forEach(invoice => {
        if (key && key.length > 0) {
          this.masterObject.IS_SEARCHING = true;
          if (!this.common.isInvoiceMatches(invoice, key))
            invoice.hide = true;
          else
            invoice.hide = false;
        }
        else {
          this.masterObject.IS_SEARCHING = false;
          invoice.hide = false;
        }
      })
      bache.invisable = bache.invoices.every(e => e.hide);
    });
  }


  checkHidden(batch) {
    return batch['invoices'].filter(e => !e.hide).length
  }

  onRefresh() {
    if (this.route.snapshot.routeConfig.path == 'MyAccount') {
      this.getInvoices(true);
      this.searchDuration(7);
    }
    console.log("StcMyAccountComponent", "onRefresh", this.route.snapshot.routeConfig.path);
  }

  searchDuration(duration?, dateFrom?, dateTo?) {
    if (!duration)
      return;

    let _endDate: any;
    let _startDate: any;

    if (duration != -1) {
      _endDate = this.common.getDate(["y", "m", "d", "-"]);
      _startDate = this.common.subDate(_endDate, duration);

      _endDate = this.common.getDate(["d", "m", "y", "/"], [_endDate, 'y', 'm', 'd', '-']);
      _startDate = this.common.getDate(["d", "m", "y", "/"], _startDate);
    } else {
      if (!dateFrom || !dateTo)
        return;
      _endDate = this.common.getDate(["d", "m", "y", "/"], dateTo);
      _startDate = this.common.getDate(["d", "m", "y", "/"], dateFrom);
    }


    this.batch.getMyAccountCompletedInvoice(_endDate, _startDate).then(res => {
      this.CustomBacklog(res, 'COMPLETED')
    });
  }
}