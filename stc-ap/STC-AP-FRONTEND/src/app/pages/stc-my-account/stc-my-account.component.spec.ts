import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcMyAccountComponent } from './stc-my-account.component';

describe('StcMyAccountComponent', () => {
  let component: StcMyAccountComponent;
  let fixture: ComponentFixture<StcMyAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcMyAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcMyAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
