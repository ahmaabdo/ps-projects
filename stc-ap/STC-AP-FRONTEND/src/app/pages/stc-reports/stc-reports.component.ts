import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { TranslationService } from '../../services/translation.service';
import { ChartOptions } from 'chart.js';

@Component({
  selector: 'stc-reports',
  templateUrl: './stc-reports.component.html',
  styleUrls: ['./stc-reports.component.css'],
  providers: [NgbModalConfig, NgbModal],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class StcReportsComponent implements OnInit {
  isDataLoaded: boolean = true;
  duration = 7;
  dateFrom;
  dateTo;
  userTypes = [];
  public barChartOptions: ChartOptions | any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }

  public barChartType = 'bar';
  public barChartLegend = true;
  selectedContent;

  chart: [];
  public contents = []
  content: any = {}
  placeHolderArr: any[] = [1, 2, 3, 4, 5, 6]

  currentPage: string = "reports";
  constructor(public trans: TranslationService, public common: CommonService, private router: Router) {
    this.userTypes = common.getSearchLookup();
  }

  save(can) {
    can.toBlob(function (blob) {
      saveAs(blob, "report.jpg")
    })
  }

  ngOnInit() {
    this.search();
  }

  doSearch(end, start) {
    this.common.getGeneric(localStorage.getReportData, { params: { 'start': start, 'end': end } })
      .then(resp => {
        if (resp.STATUS == 'OK') {
          this.contents = resp.DATA//.filter(e => e.DATA.STATUS == 'OK')
            .map(e => {
              if (e.title == 'Top invoices vendors')
                e.chartData = e.DATA.STATUS != "OK" ? [] : ['NUMBER_INVOICES'].map(_label => ({ label: _label, data: e.DATA.DATA.map(e => Number(e[_label] || 0)) }));
              else
                e.chartData = e.DATA.STATUS != "OK" ? [] : ['DELAYED', 'DUE', 'OVERDU', 'RECIVERD'].map(_label => ({ label: _label, data: e.DATA.DATA.map(e => Number(e[_label] || 0)) }));
              e.barChartLabels = e.DATA.STATUS != "OK" ? [] : e.DATA.DATA.map(e => e.TITLE);
              console.log(e.title, '=>', e.DATA.DATA);
              delete e.DATA;
              return e;
            });
          this.selectedContent = this.contents[0];
          this.isDataLoaded = true;
        }
      })

    if (this.common.getUserType() != 'FOCALPOINT') {
      this.router.navigateByUrl("**", { skipLocationChange: true })
    }
  }

  search() {
    if (!this.duration)
      return;

    let _endDate: any;
    let _startDate: any;

    if (this.duration != -1) {
      _endDate = this.common.getDate(["y", "m", "d", "-"]);
      _startDate = this.common.subDate(_endDate, this.duration);

      _endDate = this.common.getDate(["d", "m", "y", "/"], [_endDate, 'y', 'm', 'd', '-']);
      _startDate = this.common.getDate(["d", "m", "y", "/"], _startDate);
    } else {
      if (!this.dateFrom || !this.dateTo)
        return;
      _endDate = this.common.getDate(["d", "m", "y", "/"], this.dateTo);
      _startDate = this.common.getDate(["d", "m", "y", "/"], this.dateFrom);
    }
    this.doSearch(_endDate, _startDate);
  }
}
