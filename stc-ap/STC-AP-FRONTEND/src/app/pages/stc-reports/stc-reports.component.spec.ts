import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcReportsComponent } from './stc-reports.component';

describe('StcReportsComponent', () => {
  let component: StcReportsComponent;
  let fixture: ComponentFixture<StcReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
