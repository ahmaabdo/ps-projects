import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { StaticBatchDataService } from '../../services/static-batch-data.service';
import { CommonService } from '../../services/common.service';
import { CdkVirtualScrollViewport } from "@angular/cdk/scrolling";
import { MatSelect } from "@angular/material/select";
import { MatOptionSelectionChange } from "@angular/material/core";
import { FormControl } from "@angular/forms";
import { TranslationService } from '../../services/translation.service';





@Component({
  selector: 'stc-dashboared',
  templateUrl: './stc-dashboared.component.html',
  styleUrls: ['./stc-dashboared.component.css'],

})
export class StcDashboardComponent {
  @ViewChild(CdkVirtualScrollViewport, { static: false })
  cdkVirtualScrollViewPort: CdkVirtualScrollViewport;
  @ViewChild(MatSelect, { static: true }) matSelect: MatSelect;

  currentPage = "dashboard"
  title: any = " "
  type: string;
  employeeData;
  activePage;
  searchText: string;
  days: any;
  dateFrom; dateTo; duration = 7;

  itemSelect = new FormControl();
  selectedItem: any;
  _itemTrigger: any;
  userTypes: { make: string; make_ar: string; days: number; }[];
  get itemTrigger(): string {
    return this._itemTrigger;
  }
  set itemTrigger(v: string) {
    this._itemTrigger = v;
  }

  constructor(public trans: TranslationService, public common: CommonService, private batch: StaticBatchDataService) {
    this.title = this.common.getUserType();
    this.userTypes = common.getSearchLookup();
    console.log(this.title);

    this.itemSelect.valueChanges.forEach(v => {
      console.log("value changes", v);
      this.selectedItem = v;
      this.itemTrigger = v;
    });

  }

  search(keyword) {
    if (this.activePage && this.activePage.onSearch)
      this.activePage.onSearch(keyword);
  }
  showOption() {
    // console.log(typeof this.activePage.newWidth)
    if (typeof this.activePage.newWidth != 'undefined') {
      this.activePage.myOptions.columnWidth = this.activePage.newWidth();
      this.activePage.showLoadImage = true;
      setTimeout(() => {
        this.activePage.showLoadImage = false;
      }, 100);
      console.log(this.activePage.myOptions.columnWidth)
    }
  }

  public onRouterOutletActivate(event: any) {
    this.activePage = event; //facolpoint /myaccount
    if (event.searchItem)
      event.searchItem.subscribe((data) => {
        // Will receive the data from child here
        this.searchText = "";
        this.search("");
      })
  }
}
