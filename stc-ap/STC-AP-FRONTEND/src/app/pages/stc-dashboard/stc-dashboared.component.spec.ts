import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StcDashboardComponent } from './stc-dashboared.component';



describe('StcDashboaredComponent', () => {
  let component: StcDashboardComponent;
  let fixture: ComponentFixture<StcDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StcDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
