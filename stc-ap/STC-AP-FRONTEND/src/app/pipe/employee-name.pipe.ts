import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'employeeName'
})
export class EmployeeNamePipe implements PipeTransform {

  transform(value: string, ...args: any[]): any {
    if (!value)
      return "";
    let arrayName = value.split(" ")
    return arrayName && arrayName.length > 3 ? arrayName.slice(0, 3).join(" ") : value
  }

}
