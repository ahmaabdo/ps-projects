import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(arr: any, ...args: any[]): any {
    var keyWord = args[0];
    console.log(keyWord && keyWord.length > 0 ? arr.filter(e => Object.values(e).find((value: string) => ((value + "").toLowerCase().includes(keyWord.toLowerCase())))) : arr);
    return keyWord && keyWord.length > 0 ? arr.filter(e => Object.values(e).find((value: string) => ((value + "").toLowerCase().includes(keyWord.toLowerCase())))) : arr;
  } 

}
