import { Pipe, PipeTransform } from '@angular/core';
import { TranslationService } from '../services/translation.service';

@Pipe({
  name: 'translation',
  pure: false
})
export class TranslationPipe implements PipeTransform {

  constructor(private translate: TranslationService) {}

  transform(key: any): any {
    return this.translate.trans(key);
  }

}
