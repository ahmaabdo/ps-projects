import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortHidden'
})
export class SortHiddenPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if(value && value.length > 0){
      let ordered =  [...value.filter(e=>!e.hide),...value.filter(e=>e.hide)];
      // console.log(value,ordered)
      return ordered;
    }
    return value;
  }

}
