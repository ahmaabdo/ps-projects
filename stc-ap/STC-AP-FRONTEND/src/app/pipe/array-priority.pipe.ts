import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayPriority'
})
export class ArrayPriorityPipe implements PipeTransform {

  transform(value: any[], ...args: any[]): any {
    if (!value || value.find(e => !e.Priority))
      return value;
    // value.sort((a,b) => a.Priority.localeCompare(b.Priority));
    value.sort((a, b) => {
      if (a.Priority == 'High') {
        return a.Priority.localeCompare(b.Priority)
      }
      else {
        return b.Priority.localeCompare(a.Priority)
      }
    })
    return value
  }

}
