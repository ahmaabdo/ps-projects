import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortCheck'
})
export class SortCheckPipe implements PipeTransform {

  transform(value: any[], ...args: any[]): any {
    if(args.length == 0 && value.length > 0){
      return [...value.filter(s=>s.SECTION_VALUE == 'Y'), ...value.filter(s=>s.SECTION_VALUE != 'Y')];
    }
    console.log(value,args);
    let key = [...args]
    // console.log(key[1],"before",value)

    for(let i = 0 ; i<key[0].length ; i++)
    {
      for(let y = 0 ; y<value.length ; y++)
      {
        if(key[0][i].SECTION_NAME == value[y].SECTION_NAME)
        {
          value.splice(y,1);
          value.unshift(key[0][i])
          
          break 
        }
      }
    }
    // console.log(key[1],"after",value)
    return value
  //     a.SECTION_VALUE.localeCompare(b.SECTION_VALUE)
  //   );
  }

}
