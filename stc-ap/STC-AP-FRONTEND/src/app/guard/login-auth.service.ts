import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { CommonService } from '../services/common.service';
import { StaticBatchDataService } from '../services/static-batch-data.service';


@Injectable({
  providedIn: 'root'
})
export class LoginAuthService {

  constructor(private _location: Location, private route: Router, public common: CommonService, private batch: StaticBatchDataService) {
    console.log("LoginAuthService")
  }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log("autenticating ...")
    this.common.IS_AUTENTICATING = true;
    let options = localStorage.mail ? { params: { "mail": localStorage.mail.trim() } } : null;
    this.common.getGeneric(localStorage.getLoggedUserInfo, options).then(userData => {
      console.log("autenticated")
      this.common.IS_AUTENTICATING = false;
      if (userData.STATUS == "OK") {

        localStorage.user = JSON.stringify(userData.DATA);//save user info to local stotage

        localStorage.token = userData.TOKEN;
        if (userData.USERS) {
          let us = userData.USERS;
          try { us = JSON.parse(us) } catch (ignored) { }
          localStorage.imagesData = JSON.stringify(us);

          //load some caches data
          this.batch.getAllUsers();
        }

        if (this.common.getUserType() == "FOCALPOINT") {
          this.route.navigateByUrl('/DashBoard/FocalDashboard', { skipLocationChange: true });
        }
        else if (this.common.getUserType() == "VP") {
          this.route.navigateByUrl('/VpDashBoard', { skipLocationChange: true });
        }
        else {
          this.route.navigateByUrl('/DashBoard/MyAccount', { skipLocationChange: true });
        }
      } else {
        localStorage.removeItem("user");
        this.route.navigateByUrl('/Unauthorized', { skipLocationChange: true });
        this.common.info("ERROR", userData.DATA);
      }
      this._location.replaceState('/');
    })

  }
}
