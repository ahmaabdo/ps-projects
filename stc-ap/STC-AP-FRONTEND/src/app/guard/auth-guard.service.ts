
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonService } from '../services/common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
  user ;
  constructor(private _router: Router ,private services:CommonService ,private route:ActivatedRoute) {
    this.user = this.services.getCurrentUser()
   }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(localStorage.user)
    {
      return true ; 
    }
    else
    {
      this._router.navigateByUrl('/loginPage', { skipLocationChange: true })
    }
  }
}
