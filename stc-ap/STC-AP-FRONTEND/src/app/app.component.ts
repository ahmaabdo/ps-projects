import { Component } from '@angular/core';
import { TranslationService } from './services/translation.service';
import { NotifierService } from "angular-notifier";
import { CommonService } from './services/common.service';
import { NotificationsService } from './services/notifications.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  setDir: string = '';
  private readonly notifier: NotifierService;
  notfication;
  notificationids = [];
  notificationArray = []
  constructor(private notificationData: NotificationsService, public common: CommonService, public translateService: TranslationService, notifierService: NotifierService) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    const getDir = this.translateService.direction();
    this.setDir = getDir;
    // console.log(this.common.getCurrentUser())
  }


  title = 'AP-STC';
}
