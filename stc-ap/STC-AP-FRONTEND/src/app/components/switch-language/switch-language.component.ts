import { Component, OnInit } from '@angular/core';

import { from } from 'rxjs';
import { Router, Route } from '@angular/router';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'switch-language',
  templateUrl: './switch-language.component.html',
  styleUrls: ['./switch-language.component.css']
})
export class SwitchLanguageComponent implements OnInit {

  setDir:any;
  constructor(private router:Router,public common:CommonService) { }

  ngOnInit() {
  }

  logoutAction(){
    // this.common.deleteUser();
    this.router.navigateByUrl("/loginPage", { skipLocationChange: true }).then(e=>{
      // this.common.deleteUser()
    })
  }
}
