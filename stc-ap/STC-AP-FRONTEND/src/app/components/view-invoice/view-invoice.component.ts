import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-invoice-body',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.css']
})
export class ViewInvoiceComponent implements OnInit {

  @Input('invoiceData') invoice: any = {};
  @Input('invoiceType') type: string;

  attachments = [];
  gettingAttachments: boolean = false;
  IS_DOWNLOADING: boolean = false;
  constructor(
    public common: CommonService
  ) { }

  ngOnInit() {
    //get attachments
    this.gettingAttachments = true;
    this.common.getGeneric(localStorage.getInvoiceAttachments, { params: { 'id': this.invoice.InvoiceId } })
      .then(res => {
        this.gettingAttachments = false;
        if (res.STATUS == 'OK') {
          this.attachments = res.DATA;
        }
      })
  }

  dataURItoBlob(att) {
    if (this.IS_DOWNLOADING)
      return;
      
    this.IS_DOWNLOADING = true;
    this.common.getGeneric(localStorage.getAttachmentBlob, { params: { 'P_INVOICE_ID': att.INVOICE_ID, 'P_DOCUMENT_ID': att.DOCUMENT_ID } }).then(res => {
      this.IS_DOWNLOADING = false;
      if (res.STATUS == 'OK') {
        let base64 = res.DATA.GET_ATTACHMENT_CLOB;
        this.save(att.FILE_NAME, base64, 'application/octet-stream;base64');
      } else
        this.common.info("Error", res.DATA);
    })
  }

  save(title, dataURI, content_type) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: content_type });
    saveAs(blob, title)
    return blob;
  }
}
