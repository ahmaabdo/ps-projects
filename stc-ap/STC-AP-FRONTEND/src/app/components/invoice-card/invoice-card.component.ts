import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import { StaticBatchDataService } from 'src/app/services/static-batch-data.service';
import { TranslationService } from 'src/app/services/translation.service';
import { InvoiceDetailsComponent } from '../invoice-details/invoice-details.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-invoice-card',
  templateUrl: './invoice-card.component.html',
  styleUrls: ['./invoice-card.component.css']
})
export class InvoiceCardComponent implements OnInit {

  @Input('theme') theme: string = "three"
  @Input('invoiceData') invoice: any = {};
  @Input('invoiceType') type: string;
  @Input('isMultiAssignVisable') isMultiAssignVisable: boolean;
  @Output() onRefresh: EventEmitter<any> = new EventEmitter();
  @Output() onAssignChanged: EventEmitter<any> = new EventEmitter();

  historyArr: any[] = [];
  comment: string;
  OtherCheckBox: boolean = false;
  OthersReasons: string = "";
  MeanReasons: any[] = [];
  @Input('empsList') allEmployeesName: any[];

  assignName: string;
  urgent: string = "N";
  loadBtn: boolean;
  clikedBtn;
  modalData: any = {};
  attachments: any[] = [];
  is_request_sending: boolean;
  constructor(
    private modalService: NgbModal,
    public common: CommonService,
    public batch: StaticBatchDataService,
    public trans: TranslationService,
    private matDialog: MatDialog
  ) {
  }


  ngOnInit() {
  }

  openLg(modal, template?: string) {
    this.loadBtn = false;

    if (template == 'assign' || template == 'reassign') {
      this.loadBtn = true
      this.batch.getAllUsers().then(res => {
        try { res = JSON.parse(res) } catch (i) { }
        if (res.STATUS != 'OK') {
          console.log("ERROR", res.DATA);
          return;
        }

        if (this.type == 'pending') {
          this.allEmployeesName = res.DATA.filter(e => e.PERSON_TYPE_CODE == 'AUDITOR' && e.STATUS == 'ACTIVE');
        }
        else {
          this.allEmployeesName = res.DATA.filter(e => e.PERSON_TYPE_CODE == 'AUDITOR' && e.STATUS == 'ACTIVE' && e.PERSON_ID != this.invoice['pesronId']);
        }
        this.loadBtn = false;
        this.modalService.dismissAll();
        this.modalData.type = template;
        this.modalService.open(modal, { centered: true, backdrop: 'static' });
      })
    } else {//view moew
      if (localStorage.newPopupDesign == 'true')
        this.matDialog.open(InvoiceDetailsComponent, { disableClose: true, data: { invoice: this.invoice, type: this.type, allEmployeesName: this.allEmployeesName } })
          .afterClosed().subscribe(isRefreshNeeded => {
            if (isRefreshNeeded)
              this.onRefresh.emit();
            else
              console.log("No Refresh Needed");
          })
      else
        this.modalService.open(modal, { centered: true, backdrop: 'static' });
    }
  }


  getHistory(history) {
    this.loadBtn = true
    this.batch.getinvoiceHistory(this.invoice.InvoiceId, this.invoice.itemKey)
      .then(res => {
        this.loadBtn = false
        this.modalService.dismissAll();
        try { res = JSON.parse(res); } catch (ignored) { }
        if (res.STATUS == 'OK') {
          this.historyArr = res.DATA
          this.historyArr.forEach(e => {
            e.COMMENTS = e.NOTES ? e.NOTES.split("||").map(e => "-" + (e.split(";").length > 2 ? e.split(";")[2] : e)) : []
            return e;
          });

          this.modalService.open(history, { size: 'lg', centered: true, backdrop: 'static' });
        }
        else
          this.common.info("error", res.DATA);
      });
  }



  sendAction(_action: string, note: string) {
    let action: string = '';
    this.is_request_sending = true

    if (_action == 'APPROVE')
      action = this.common.getUserType() == 'SUPERVISOR' || this.common.getUserType() == 'TAXSUPERVISOR' ? `SUP-APPROVE` : "ACT-APPROVE";
    else if (_action == 'REJECT')
      action = this.common.getUserType() == 'SUPERVISOR' || this.common.getUserType() == 'TAXSUPERVISOR' ? `SUP-REJECT` : "ACT-REJECT"

    let payload = {
      payload: JSON.stringify([{
        "ACTION": action,
        "INVOICE_ID": this.invoice.InvoiceId,
        "ACCOUNTANT_INVOICE_ID": this.invoice.accountant,
        "NOTE": note,
        "ItemKey": this.invoice.itemKey
      }])
    };

    this.loadBtn = true;
    this.batch.invoiceAction(payload).then(res => {
      this.modalService.dismissAll();
      this.loadBtn = false;
      this.is_request_sending = false;
      if (res.DATA && res.DATA.X_RESULT == 'E') {
        this.common.info("Error", res.DATA.X_MESSAGE);
      } else
        this.onRefresh.emit();
    });
    this.comment = ""
  }



  rejectAction(action) {

    let note = "";

    if (!this.OtherCheckBox) {

      let ress = [...this.MeanReasons];
      ress.forEach(e => e.details = e.details.filter(ee => ee.checked));

      ress = ress.filter(e => e.details.length > 0);

      if (ress.length < 1) {
        this.common.openSnackError('please choose your reasons ', 'close');
        return;
      }

      let p = []
      ress.forEach(e => {
        e.details.forEach(o => {
          p.push(`${e.HEADER_CODE};${o.DETAIL_CODE};${o.DETAIL_DESC}`);
        })
      })
      note = p.join("||");
    }
    else {
      if (!this.OthersReasons) {
        this.common.openSnackError('please write your reason ', 'close');
        return;
      }
      note = 'OTHERS;43;' + this.OthersReasons;
    }

    this.sendAction(action, note);
  }

  // assign/reassgin
  invoiceAction(action) {
    this.is_request_sending = true
    let assignObj = this.allEmployeesName.find(e => e.PERSON_NAME == this.assignName);
    if (!assignObj) return

    let payload = {
      payload: JSON.stringify([{
        "ACTION": action,
        "PERSON_ID": assignObj.PERSON_ID,
        "INVOICE_ID": this.invoice["InvoiceId"],
        "ACCOUNTANT_INVOICE_ID": this.invoice["accountantId"],
        "ITEM_KEY": this.invoice["itemKey"] || 1,
        "URGENT_FLAG": this.urgent
      }])
    }

    this.batch.assignAction(payload)
      .then(res => {
        console.log(res.STATUS, res.DATA);
        this.onRefresh.emit();
        this.is_request_sending = false;
        this.modalService.dismissAll();
      }, error => {
        console.log(error);
        this.is_request_sending = false;
        this.modalService.dismissAll();
      })
  }

  // REJECT SEND_BACK SEND_TO_AUDITOR CANCEL 
  openRejectPopup(reject, action) {
    this.modalData.type = action;
    this.loadBtn = true;

    this.batch.getRejectionData().then(res => {
      this.loadBtn = false;
      if (res.STATUS == 'OK') {
        this.MeanReasons = res.DATA;
        console.log(this.MeanReasons)
      }
      else
        console.log("error", res.DATA);

      this.OthersReasons = "";
      this.OtherCheckBox = false;
      this.modalService.dismissAll();
      this.modalService.open(reject, { size: 'lg', centered: true, backdrop: 'static' });
    })
  }
  log(d) {
    console.log(d);
  }
}
