import { Component, OnInit, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { StaticBatchDataService } from '../../services/static-batch-data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from '../../services/common.service';
import { AssignComponent } from '../assign/assign.component';
import { HistoryComponent } from '../history/history.component';
import { RejectReasonsComponent } from '../reject-reasons/reject-reasons.component';
import { TranslationService } from 'src/app/services/translation.service';

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.css']
})
export class InvoiceDetailsComponent implements OnInit {

  invoice: any = {}
  type: any;
  loadBtn: boolean = false;
  allEmployeesName: any[];
  // modalData: any = {};
  historyArr: any[] = [];
  is_request_sending: boolean = false;
  comment: string;
  clikedBtn;
  // OtherCheckBox: boolean;
  // MeanReasons: any[] = [];
  // OthersReasons: string = "";
  isSubDialogOpened: boolean = false;
  constructor(
    public trans:TranslationService , 
    @Inject(MAT_DIALOG_DATA) public data: any,
    private batch: StaticBatchDataService,
    private modalService: NgbModal,
    public common: CommonService,
    public dialogRef: MatDialogRef<InvoiceDetailsComponent>,
    private matDialog: MatDialog
  ) { }


  ngOnInit() {
    this.invoice = this.data.invoice;
    this.type = this.data.type;
    this.allEmployeesName = this.data.allEmployeesName;
  }


  openLg(template?: string) {
    this.loadBtn = false;

    if (template == 'assign' || template == 'reassign') {
      this.loadBtn = true
      this.batch.getAllUsers().then(res => {
        this.loadBtn = false;

        try { res = JSON.parse(res) } catch (i) { }
        if (res.STATUS != 'OK') {
          console.log("ERROR", res.DATA);
          return;
        }

        if (this.type == 'pending') {
          this.allEmployeesName = res.DATA.filter(e => e.PERSON_TYPE_CODE == 'AUDITOR' && e.STATUS == 'ACTIVE');
        }
        else {
          this.allEmployeesName = res.DATA.filter(e => e.PERSON_TYPE_CODE == 'AUDITOR' && e.STATUS == 'ACTIVE' && e.PERSON_ID != this.invoice['pesronId']);
        }
        this.isSubDialogOpened = true;
        this.matDialog.open(AssignComponent, { disableClose: true, data: { type: template, allEmployeesName: this.allEmployeesName, invoice: this.invoice } })
          .afterClosed().subscribe(s => {
            this.isSubDialogOpened = false;
            if (s) {
              this.dialogRef.close(true);
            }
          });
      })
    }
  }


  getHistory() {
    this.loadBtn = true
    this.batch.getinvoiceHistory(this.invoice.InvoiceId, this.invoice.itemKey)
      .then(res => {
        console.log(res);
        this.loadBtn = false
        this.modalService.dismissAll();
        try { res = JSON.parse(res); } catch (ignored) { }
        if (res.STATUS == 'OK') {
          this.historyArr = res.DATA
          this.historyArr.forEach(e => {
            e.COMMENTS = e.NOTES ? e.NOTES.split("||").map(e => "-" + (e.split(";").length > 2 ? e.split(";")[2] : e)) : []
            return e;
          });
          this.isSubDialogOpened = true;
          this.matDialog.open(HistoryComponent, { disableClose: true, data: { historyArr: this.historyArr, invoice: this.invoice } })
            .afterClosed().subscribe(_ => {
              this.isSubDialogOpened = false;
              // this.dialogRef.close();
            });
        }
        else
          this.common.info("error", res.DATA);
      });
  }

  sendAction(_action: string, note: string) {
    let action: string = '';
    this.is_request_sending = true

    if (_action == 'APPROVE')
      action = this.common.getUserType() == 'SUPERVISOR' || this.common.getUserType() == 'TAXSUPERVISOR' ? `SUP-APPROVE` : "ACT-APPROVE";
    else if (_action == 'REJECT')
      action = this.common.getUserType() == 'SUPERVISOR' || this.common.getUserType() == 'TAXSUPERVISOR' ? `SUP-REJECT` : "ACT-REJECT"

    let payload = {
      payload: JSON.stringify([{
        "ACTION": action,
        "INVOICE_ID": this.invoice.InvoiceId,
        "ACCOUNTANT_INVOICE_ID": this.invoice.accountant,
        "NOTE": note,
        "ItemKey": this.invoice.itemKey
      }])
    };

    this.loadBtn = true;
    this.batch.invoiceAction(payload).then(res => {
      this.dialogRef.close(true);
      this.loadBtn = false;
      this.is_request_sending = false;
      if (res.DATA && res.DATA.X_RESULT == 'E') {
        this.common.info("Error", res.DATA.X_MESSAGE);
      }
    });
    this.comment = ""
  }

  // REJECT SEND_BACK SEND_TO_AUDITOR CANCEL 
  openRejectPopup(action) {
    // this.modalData.type = action;
    this.loadBtn = true;

    this.batch.getRejectionData().then(res => {
      this.loadBtn = false;
      let MeanReasons = [];
      if (res.STATUS == 'OK') {
        MeanReasons = res.DATA;
      }
      else
        console.log("error", res.DATA);

      this.isSubDialogOpened = true;
      this.matDialog.open(RejectReasonsComponent, { disableClose: true, data: MeanReasons })
        .afterClosed().subscribe(note => {
          this.isSubDialogOpened = false;
          if (note && note.length > 0)
            this.sendAction(action, note);
          // else
          //   this.dialogRef.close();
        });
    })
  }
}
