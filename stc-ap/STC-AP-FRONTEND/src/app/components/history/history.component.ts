import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CommonService } from '../../services/common.service';
import { StaticBatchDataService } from '../../services/static-batch-data.service';
import { TranslationService } from 'src/app/services/translation.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  historyArr: any[] = [];
  invoice: any = {};

  constructor(
    public trans:TranslationService , 
    @Inject(MAT_DIALOG_DATA) public data: any,
    public common: CommonService,
    public dialogRef: MatDialogRef<HistoryComponent>,
    private batch: StaticBatchDataService,
  ) { }

  ngOnInit() {
    this.invoice = this.data.invoice;
    this.historyArr = this.data.historyArr;
  }

}
