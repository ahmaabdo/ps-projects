import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { StaticBatchDataService } from '../../services/static-batch-data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from '../../services/common.service';
import { TranslationService } from '../../services/translation.service';

@Component({
  selector: 'app-reject-reasons',
  templateUrl: './reject-reasons.component.html',
  styleUrls: ['./reject-reasons.component.css']
})
export class RejectReasonsComponent implements OnInit {

  clikedBtn;
  MeanReasons: any[] = [];
  OtherCheckBox: boolean = false;
  OthersReasons;
  is_request_sending: boolean = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private batch: StaticBatchDataService,
    private modalService: NgbModal,
    public common: CommonService,
    public dialogRef: MatDialogRef<RejectReasonsComponent>,
    private matDialog: MatDialog,
    public trans: TranslationService
  ) { }

  ngOnInit() {
    this.MeanReasons = this.data;
  }

  reject() {
    let note = "";

    if (!this.OtherCheckBox) {

      let ress = [...this.MeanReasons];
      ress.forEach(e => e.details = e.details.filter(ee => ee.checked));

      ress = ress.filter(e => e.details.length > 0);

      if (ress.length < 1) {
        this.common.openSnackError('please choose your reasons ', 'close');
        return;
      }

      let p = []
      ress.forEach(e => {
        e.details.forEach(o => {
          p.push(`${e.HEADER_CODE};${o.DETAIL_CODE};${o.DETAIL_DESC}`);
        })
      })
      note = p.join("||");
    }
    else {
      if (!this.OthersReasons) {
        this.common.openSnackError('please write your reason ', 'close');
        return;
      }
      note = 'OTHERS;43;' + this.OthersReasons;
    }
    this.dialogRef.close(note);
  }

}
