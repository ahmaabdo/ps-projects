import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CommonService } from '../../services/common.service';
import { StaticBatchDataService } from '../../services/static-batch-data.service';
import { TranslationService } from 'src/app/services/translation.service';

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.css']
})
export class AssignComponent implements OnInit {

  type;
  allEmployeesName: any[] = [];
  is_request_sending: boolean = false;
  assignName: string;
  invoice: any = {}


  constructor(
    public trans:TranslationService ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public common: CommonService,
    public dialogRef: MatDialogRef<AssignComponent>,
    private batch: StaticBatchDataService,
  ) { }

  ngOnInit() {
    this.type = this.data.type;
    this.allEmployeesName = this.data.allEmployeesName;
    this.invoice = this.data.invoice;
  }


  // assign/reassgin
  invoiceAction(action) {
    this.is_request_sending = true
    let assignObj = this.allEmployeesName.find(e => e.PERSON_NAME == this.assignName);
    if (!assignObj) return

    let payload = {
      payload: JSON.stringify([{
        "ACTION": action,
        "PERSON_ID": assignObj.PERSON_ID,
        "INVOICE_ID": this.invoice["InvoiceId"],
        "ACCOUNTANT_INVOICE_ID": this.invoice["accountantId"],
        "ITEM_KEY": this.invoice["itemKey"] || 1,
        "URGENT_FLAG": this.invoice.UrgentFlag
      }])
    }

    this.batch.assignAction(payload)
      .then(res => {
        console.log(res.STATUS, res.DATA);
        this.is_request_sending = false;
        this.dialogRef.close(true);
      }, error => {
        console.log(error);
        this.is_request_sending = false;
        this.dialogRef.close(false);
      })
  }

}
