/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your application specific code will go here
 */
define(['knockout', 'config/services', 'ojs/ojmodule-element-utils', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils',
  'ojs/ojrouter', 'ojs/ojarraydataprovider', 'ojs/ojknockouttemplateutils', 'ojs/ojmodule-element', 'ojs/ojknockout', 'ojs/ojnavigationlist',
  'ojs/ojconveyorbelt', 'ojs/ojavatar'],
  function (ko, services, moduleUtils, ResponsiveUtils, ResponsiveKnockoutUtils, Router, ArrayDataProvider, KnockoutTemplateUtils) {
    function ControllerViewModel() {
      var self = this;


      self.avatarSize = ko.observable("xxs");

      self.KnockoutTemplateUtils = KnockoutTemplateUtils;
      self.isLogin = ko.observable(false);
      // start avatar

      // end avatar
      //
      // Handle announcements sent when pages change, for Accessibility.
      self.manner = ko.observable('polite');
      self.message = ko.observable();
      self.personDetails = ko.observableArray([]);
      document.getElementById('globalBody').addEventListener('announce', announcementHandler, false);

      function announcementHandler(event) {
        setTimeout(function () {
          self.message(event.detail.message);
          self.manner(event.detail.manner);
        }, 200);
      };

      // Media queries for repsonsive layouts
      var smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
      self.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);

      // Router setup
      self.router = Router.rootInstance;
      self.router.dispose();
      self.router.configure({//processPages
        'login': { label: 'Login', value: '', title: 'Login', isDefault: true },
        'homePage': { label: 'Home', value: 'home', title: 'Home' },
        'payrollResultPages': { label: 'payroll Result Pages', value: 'payrollResult-pages/payrollResultScreen', title: 'payroll Result Pages' },
        'configrationPages': { label: 'Manage Entries', value: 'configration-pages/configrationScreen', title: 'Manage Entries' },
        'processPages': { label: 'Process Pages', value: 'process-pages/processScreen', title: 'Process Pages' },
        'homeSetup': { label: 'Setup', value: 'homeSetup/home-setup', title: 'Setup' },
        'indexElement': { label: 'Element', value: 'element/indexElement', title: 'Element' },
        'indexObjectGroup': { label: 'Object Group', value: 'objectGroup/indexObjectGroup' },
        'elementEntries': { label: 'Element Entries', value: 'element/elementEntries', title: 'Manage Element Entries' },
        'createObjectGroup': { label: 'Create Object Group', value: 'objectGroup/createObjectGroup' },
        'editObjectGroup': { label: 'Edit Object Group', value: 'objectGroup/editObjectGroup' },
        'indexBatchElement': { label: 'Batch Element', value: 'batchElement/indexBatchElement' },
        'createBatchElement': { label: 'Create Batch Element', value: 'batchElement/createBatchElement' },
        'indexViewProcessResult': { label: 'indexViewProcessResult', value: 'viewProcessResult/indexViewProcessResult' },
        'indexFastFormula': { label: 'Fast Formula', value: 'fastFormula/indexFastFormula' },
        'createFastFormula': { label: 'Create Fast Formula', value: 'fastFormula/createFastFormula' },
        'editFastFormula': { label: 'Edit Fast Formula', value: 'fastFormula/editFastFormula' },
        'indexReport': { label: 'Report', value: 'reports/indexReport' },
        'createReport': { label: 'Report', value: 'reports/createReport' },
        'editReport': { label: 'Report', value: 'reports/editReport' },
        'process': { label: 'Report', value: 'process' },
        'retroCalculation': { label: 'Retro Calculation', value: 'retroCalculation/retroCalculation', title: 'Retro Calculation' },
        'payrollRun': { label: 'payroll Run', value: 'payrollRun/payrollRun', title: 'Payroll Run' },
        'quickPay': { label: 'quick Pay', value: 'quickPay/quickPay', title: 'Quick Pay' },
        'calculatePayment': { label: 'calculate Payment', value: 'calculatePayment/calculatePayment', title: 'Calculate Payment' },
        'costingProcess': { label: 'costing Process', value: 'costingProcess/costingProcess', title: 'Costing Process' },
        'transferGL': { label: 'transfer GL', value: 'transferGL/transferGL', title: 'Transfer GL' },
        'rollBack': { label: 'roll Back', value: 'rollBack/rollBack', title: 'RollBack' },
        'defineFormulaResult': { label: 'Define Formula Result', value: 'fastFormula/defineFormulaResult' },
      });
      Router.defaults['urlAdapter'] = new Router.urlParamAdapter();

      self.loadModule = function () {
        self.moduleConfig = ko.pureComputed(function () {
          var name = self.router.moduleConfig.name();
          var viewPath = 'views/' + name + '.html';
          var modelPath = 'viewModels/' + name;
          return moduleUtils.createConfig({
            viewPath: viewPath,
            viewModelPath: modelPath, params: { parentRouter: self.router }
          });
        });
      };

      //////////////////// Navigation setup ////////////////////////////
      self.homePage = function () {
        oj.Router.rootInstance.go('homePage')
      }

      self.homeSetup = function () {
        oj.Router.rootInstance.go('homeSetup')
      }

      self.configrationPages = function () {
        oj.Router.rootInstance.go('configrationPages')
      }

      self.payrollResultPages = function () {
        oj.Router.rootInstance.go('payrollResultPages')
      }
      self.processPages = function () {
        oj.Router.rootInstance.go('processPages')
      }

      self.toggle = ko.observable(false);
      self.collapseSideMenu = function () {
        $("#btn-sidemenu").click(function () {

          if (!self.toggle()) {
            $(".sidemenu").animate(
              {
                left: "-999px"
              }
            );
            $(".main-content").animate(
              {
                margin: "25px 0px 0px 35px"
              }
            );
            self.toggle(true);
          } else {
            $(".sidemenu").animate(
              {
                left: "25px"
              }
            );
            $(".main-content").animate(
              {
                margin: "25px 0px 0px 150px"
              }
            );
            self.toggle(false);
          }
          console.log(self.toggle());
        });
      }
      // Header
      // Application Name used in Branding Area
      self.appName = ko.observable("AppsPay");
      // User Info used in Global Navigation area
      self.userLogin = ko.observable("john.hancock@appspro-me.com");

      // header dropdown options
      self.headerDropdown = async function (event) {
        var action = event.target.value;
        if (action == "logout") {
          self.auth();
        }
      }

      // Authentication
      self.username = ko.observable("user@appspro-me.com");
      self.password = ko.observable("password");

      self.auth = async function () {
        var valid = checkValidationLoginForm();
        if (valid) {
          if (self.isLogin()) {
            self.isLogin(false);
            $('#login').show();
            $('#dashboard-header').hide();
            oj.Router.rootInstance.go("/");
          } else {
            self.isLogin(true);
            $('#login').hide();
            $('#dashboard-header').show();
            oj.Router.rootInstance.go("homePage");
          }
        } else {
          $(".error-message").html("Invalid username or password!");
        }

      }

      function checkValidationLoginForm() {
        var error = 0;
        if (self.username().length <= 0 || self.password().length <= 0) error++;

        return (error) ? false : true;
      }

      this.urlCheck = ko.computed(async function () {
        var path = self.router.moduleConfig.name();

        if (path != "login") {
          self.isLogin(true);
          $('#login').hide();
          $('#dashboard-header').show();
        } else {
          self.isLogin(false);
          $('#login').show();
          $('#dashboard-header').hide();
        }

      }, this);

      //Appspro
      console.log('%c ', 'padding: 10% 0% 0% 100%; background: url(https://www.appspro-me.com/wp-content/uploads/2020/03/appspro-logo-02-1.png) no-repeat center / 40%;');
      
      self.getEmployeesDetails = async function () {
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetEmployee";
        await services.getGeneric(url).then(result => {
          if (result)
            self.personDetails(result);
        }, fail);
      }

      self.getEmployeesDetails();
    }

    return new ControllerViewModel();
  }
);
