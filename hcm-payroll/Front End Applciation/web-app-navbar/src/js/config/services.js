/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['config/serviceConfig'], function (serviceConfig) {

    /**
     * The view model for managing all services
     */
    function services() {

        var self = this;
        var servicesHost = "";
        var paasServiceHost = "";
        var appServiceHost = "";
        var biReportServletPath = "";
        
        var restPath= "http://payrollext.appspro-me.com:7003/";

        self.authenticate = function (userName, password) {
            var serviceURL = restPath + "login/login2";
            var payload = {
                "userName": userName, "password": password
            };
            var headers = {

            };
                                                       

            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        
        self.getGeneric = function (serviceName,headers) {
            var serviceURL = restPath + serviceName;
                                                      

             return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers,true);
        };
        
        self.getGenericFile = function (serviceName,headers) {
            var serviceURL = restPath + serviceName;
             return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationXWWw, headers,true);
        };
        
  
        self.editGeneric = function (serviceName, payload) {
              var serviceURL = restPath + serviceName;
              var headers = {
            };
            return serviceConfig.callPutService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.addGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.searcheGeneric = function (serviceName,payload) {
            var serviceURL =restPath+serviceName;
            var headers = {
                 "content-type": "application/x-www-form-urlencoded"
            };

            return serviceConfig.callPostServiceUncodeed(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
        };
 
         self.deleteGeneric = function (serviceName,headers) {
            var serviceURL = restPath + serviceName;
             return serviceConfig.callDeleteService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers,true);
        };
        
         self.addOrEditGeneric = function (serviceName, payload, type) {
            var serviceURL = restPath + serviceName+ type;
            
            var headers = {
            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        
        self.getBusinessUnitsReport = function () {
            var serviceURL =  restPath + "report/commonbireport";
            var payload = {
                "reportName": "HRW_BUSINESS_UNITS"
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        
        self.getCurrencyReport = function () {
            var serviceURL =  restPath + "report/commonbireport";
            var payload = {
                "reportName": "GET_ALL_CURRENCIES_REPORT"
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        
        self.getCurrencyRateReport = function () {
            var serviceURL =  restPath + "report/commonbireport";
            var payload = {
                "reportName": "GET_CURRENCY_RATE_REPORT"
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        
        self.getCustomerAccountReport = function () {
            var serviceURL =  restPath + "report/commonbireport";
            var payload = {
                "reportName": "CUSTOMER_ACCOUNT_REPORT"
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        
        self.getBankAccountReport = function (document) {
            var serviceURL =  restPath + "report/commonbireport";
            var payload = {
                "reportName": "CE_BANK_ACCOUNTS_REPORT",
                "documentType": document
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        
        self.getCurrencyRateReport = function (to_currency) {
            var serviceURL =  restPath + "report/commonbireport";
            var payload = {
                "reportName": "GET_CURRENCY_RATE_REPORT",
                "TO_CURRENCY": to_currency
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
         self.getGenericReport = function (payload) {
                    var serviceURL = restPath + "report/commonbireport";
                    
                    var headers = {

                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
        

        self.getRestPath = function () {
            
            return restPath;
        };
        
    };

    return new services();
});
