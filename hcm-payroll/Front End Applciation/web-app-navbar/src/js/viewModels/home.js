/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your customer ViewModel code goes here
 */
define(['ojs/ojcore', 'appController', 'text!data/showData.json', 'text!data/element.json', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojrouter', 'ojs/ojlistview'
],
  function (oj, app, processResultValuesData, elementData, services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils) {

    function HomePageViewModel() {
      var self = this;

      self.setupPage = function () {
        app.router.go('homeSetup')
      };
      self.configrationPages = function () {
        app.router.go('configrationPages')
      };
      self.payrollPages = function () {
        app.router.go('payrollResultPages')
      };
      self.processPages = function(){
        app.router.go('processPages')
      }
      this.controlDetails = function () {

      };



      self.connected = function () {

        // Implement further logic if needed
        //$(".oj-navigationlist-item-element").removeClass("oj-selected").addClass("oj-default");
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      self.transitionCompleted = function () {
        // Implement if needed
      };
    }


    return HomePageViewModel;
  }
);