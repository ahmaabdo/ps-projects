define(['accUtils', 'config/services', 'appController', 'knockout', 'jquery', 'ojs/ojarraydataprovider',
  'ojs/ojanimation', 'ojs/ojconverterutils-i18n', 'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber',
  'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup',
  'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojtimezonedata'
],
  function (accUtils, services, app, ko, $, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {
    function IndexElementViewModel() {
      var self = this;
      var rowDataElement, rowDataPerson;

      self.notifMessage = ko.observableArray();
      self.columnArray = ko.observableArray();
      self.columnArrayPerson = ko.observableArray();
      self.elementNameOptionsArr = ko.observableArray([]);
      self.elementsArr = ko.observableArray([]);
      self.assignmentOptionsArr = ko.observableArray();
      self.labels = ko.observable([]);
      self.operationType = ko.observable("ADD");
      self.elementEntryDetails = ko.observable();
      self.elementNameVal = ko.observable();
      self.assignmentVal = ko.observable();
      self.empStartDate = ko.observable("");
      self.empEndDate = ko.observable("");
      self.editEntryDisabled = ko.observable(false);
      self.recurringElementEntryEndDate = ko.observable("");
      self.saveChangesDisabled = ko.observable(false);
      self.inputValuesLoaded = ko.observable(true);
      self.searchElementStatus = ko.observable(false);
      self.searchInputStatus = ko.observable(true);
      self.elementNameArrayStatus = ko.observable(true);
      self.empNumberSearchVal = ko.observable("");
      self.empNameSearchVal = ko.observable("");
      self.elementEffictiveDate = ko.observable("");
      self.employeeEffictiveDate = ko.observable("");
      self.createElementProcessType = ko.observable();
      self.createElementEffectiveStartDate = ko.observable();
      self.createElementEffectiveEndDate = ko.observable();
      self.createElementPriority = ko.observable();
      self.createElementCurrency = ko.observable();
      self.empNameVal = ko.observable();
      self.elementNameLbl = ko.observable();
      self.processingTypeVal = ko.observable();
      self.personDetailsArr = ko.observableArray();
      self.tableDataElement = ko.observableArray();
      self.globalDataElement = ko.observableArray();
      self.dataProviderElement = ko.observable(new ArrayDataProvider([], {}));
      self.dataProviderPerson = ko.observable(new ArrayDataProvider([], {}));
      //Dynamic section arrays
      self.numberOrMoneyInputValArr = ko.observableArray([]);
      self.characterInputValArr = ko.observableArray([]);
      self.dateInputValArr = ko.observableArray([]);
      self.messagesDataprovider = ko.observableArray();
      self.notificationMessage = function (type, messages) {
        self.notifMessage([{
          severity: type,
          summary: messages[0],
          detail: '',
          autoTimeout: 5000
        }]);
      }

      self.back = function () {
        app.router.go('configrationPages')
      };
      self.elementEntries = function () {
        app.router.go('elementEntries')
      };
      self.indexBatchElement = function () {
        app.router.go('indexBatchElement')
      };
      /////////////////
      self.messagesDataprovider = new ArrayDataProvider(self.notifMessage);

      self.getElementData = async function (isSearch) {
        self.searchElementStatus(true);
        self.tableDataElement([]);

        self.getElementEntryDetails();
        var success = function (result) {
          if (result) {
            $.each(result, function (index, element) {
              element.effectiveStartDate = dateConverter(element.effectiveStartDate);
              element.effectiveEndDate = dateConverter(element.effectiveEndDate);
            });
            self.globalDataElement(result);

            for (var elementData in self.globalDataElement()) {
              var mappedArr = self.assignmentOptionsArr().find(obj => obj.personId == self.globalDataElement()[elementData].personId);
              if (mappedArr) {
                self.globalDataElement()[elementData].empName = mappedArr.displayName;
                self.globalDataElement()[elementData].empNumber = mappedArr.value;
              }
            }
            self.filterElementProvider();
          }
        }

        var fail = function () {
          console.log("fail");
        }
        var url = isSearch ? "payroll/rest/AppsProRest/GetElementEntryByDate/" + dateConverterForCreation(self.elementEffictiveDate()) : "payroll/rest/AppsProRest/GetElementEntry";
        await services.getGeneric(url).then(success, fail);
        self.searchElementStatus(false);
      };

      self.getElementForm = async function () {
        await services.getGeneric("payroll/rest/AppsProRest/GetElementForm")
          .then(result => {
            if (result)
              self.elementsArr(result);
          }, err => {
            console.log("fail");
          });
      };

      self.getElementsForEmp = async function (id) {
        self.elementNameArrayStatus(true);
        self.elementNameOptionsArr([]);

        var url = "payroll/rest/AppsProRest/GetElementEntryEligible/" + id;
        await services.getGeneric(url).then(result => {
          if (result) {
            $.each(result, function (index, val) {
              var elementForEmployee = self.elementsArr().find(obj => obj.elementTypeId == val.elementTypeId);
              self.elementNameOptionsArr.push({ label: elementForEmployee.elementName, value: elementForEmployee.elementTypeId, processingType: elementForEmployee.processingType });
            });
          }
        }, err => {
          console.log("fail")
        });

        self.elementNameArrayStatus(false);
      };


      self.startAnimationListenerCreateElement = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-element-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };

      self.openListenerCreateElement = function () {
        var popup = document.getElementById('create-element-modal');
        popup.open('#btnGo');

      };

      self.resetSearchBtnAction = function () {
        self.empNumberSearchVal("");
        self.empNameSearchVal("");
        self.employeeEffictiveDate("");
        self.dataProviderPerson(new ArrayDataProvider([], {}));
      };

      self.resetSearchElementBtnAction = function () {
        self.elementEffictiveDate("");
        self.getElementData(false);
      };

      self.clearValues = function () {
        self.elementNameVal("");
        self.createElementEffectiveStartDate("");
      };

      self.cancelListenerCreateElement = function () {
        var popup = document.getElementById('create-element-modal');
        popup.close();

        var popup2 = document.getElementById('remove-element-modal');
        popup2.close('#btnGo');
      };

      self.searchBtnElementAction = function () {
        if (!self.elementEffictiveDate()) {
          self.notificationMessage('error', ['Please enter a value']);
          return;
        }
        self.getElementData(true);
      };

      self.searchBtnAction = function () {
        if (!self.empNumberSearchVal() && !self.empNameSearchVal() && !self.employeeEffictiveDate()) {
          self.notificationMessage('error', ['Please enter a value']);
          return;
        }

        let empNameRegex = new RegExp(self.empNameSearchVal().toUpperCase() + ".*")

        var searchByNumber = [];
        for (var index in self.personDetailsArr()) {
          if (self.empNumberSearchVal()) {
            if (self.personDetailsArr()[index].PERSON_NUMBER == self.empNumberSearchVal()
              && (self.personDetailsArr()[index].DISPLAY_NAME).toUpperCase().match(empNameRegex)
              && (validateBetween(self.employeeEffictiveDate(), self.personDetailsArr()[index].EFFECTIVE_START_DATE, self.personDetailsArr()[index].EFFECTIVE_END_DATE))) {
              searchByNumber.push(self.personDetailsArr()[index]);
            }
          } else {
            if ((self.personDetailsArr()[index].DISPLAY_NAME).toUpperCase().match(empNameRegex)
              && (validateBetween(self.employeeEffictiveDate(), self.personDetailsArr()[index].EFFECTIVE_START_DATE, self.personDetailsArr()[index].EFFECTIVE_END_DATE))) {
              searchByNumber.push(self.personDetailsArr()[index]);
            }
          }
        }

        if (searchByNumber.length) {
          self.dataProviderPerson(new ArrayDataProvider(searchByNumber, { keyAttributes: 'PERSON_NUMBER', implicitSort: [{ attribute: 'priority', direction: 'ascending' }] }));
        } else {
          self.dataProviderPerson(new ArrayDataProvider(searchByNumber, { keyAttributes: 'PERSON_NUMBER', implicitSort: [{ attribute: 'priority', direction: 'ascending' }] }));
          self.notificationMessage('error', ['No result found']);
        };
      };

      self.createElement = function () {
        var valid = _checkValidationGroupCreateElement();

        if (valid) {
          self.operationType("ADD");
          self.getInputValues(self.elementNameVal());
          $("#search-emp-page").hide();
          $("#summary-page").hide();
          $("#add-edit-page").fadeIn();
          self.cancelListenerCreateElement();
        }
      };

      self.removeRecurringElement = async function () {
        var valid = _checkValidationGroupRemoveElement();
        if (valid)
          self.removeElement(rowDataElement.elementEntryId, dateConverterForCreation(self.recurringElementEntryEndDate()));
        var popup = document.getElementById('remove-element-modal');
        popup.close('#btnGo');

      };

      self.startDateValueChanged = function (e) {
        self.createElementEffectiveEndDate(dateConverter(getLastDayOfMonth(self.createElementEffectiveStartDate())));
      };

      var _checkValidationGroupCreateElement = function () {

        if (!validateBetween(self.createElementEffectiveStartDate(), self.empStartDate(), self.empEndDate())) {
          self.notificationMessage('error', ['Element Entry Date must between Employee Start Date and End Date']);
          return false;
        }

        var tracker = document.getElementById("create-element-tracker");
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      var _checkValidationGroupRemoveElement = function () {
        var tracker = document.getElementById("remove-element-tracker");
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.tableSelectionListenerPerson = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if (currentRow)
          rowDataPerson = self.personDetailsArr().find(obj => obj.PERSON_NUMBER == currentRow['rowKey']);
        else
          self.notificationMessage('error', ['Something went wrong.']);
      };

      self.newElementAction = function (event) {
        self.elementEffictiveDate("");
        self.getElementData(false);
        for (var elementData in self.globalDataElement()) {
          var mappedArr = self.assignmentOptionsArr().find(obj => obj.personId == self.globalDataElement()[elementData].personId);
          if (mappedArr) {
            self.globalDataElement()[elementData].empName = mappedArr.displayName;
            self.globalDataElement()[elementData].empNumber = mappedArr.value;
          }
        }

        self.getElementsForEmp(rowDataPerson.PERSON_ID);
        self.tableDataElement([]);
        self.assignmentVal(rowDataPerson.PERSON_NUMBER);
        self.empStartDate(rowDataPerson.EFFECTIVE_START_DATE);
        self.empEndDate(rowDataPerson.EFFECTIVE_END_DATE);
        self.empNameVal(rowDataPerson.DISPLAY_NAME);
        self.filterElementProvider();

        $("#search-emp-page").hide();
        $("#summary-page").fadeIn();
        $("#add-edit-page").hide();

      }.bind(this);

      self.filterElementProvider = function () {
        var searchByNumber = [];
        for (var index in self.globalDataElement()) {
          if (self.assignmentVal())
            if (self.globalDataElement()[index].empNumber == self.assignmentVal())
              searchByNumber.push(self.globalDataElement()[index]);
        }

        self.tableDataElement(searchByNumber);
        self.dataProviderElement(new ArrayDataProvider(self.tableDataElement, { keyAttributes: 'num', implicitSort: [{ attribute: 'priority', direction: 'ascending' }] }));
      };

      self.tableSelectionListenerElement = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if (currentRow) {
          var thisRow = currentRow['rowIndex'];
          rowDataElement = self.tableDataElement()[thisRow];
          rowDataElement.ProccessingType == 'Nonrecurring' ? self.editEntryDisabled(true) : self.editEntryDisabled(false);
        }
      };

      self.elementAction = function (event) {
        var action = event.target.value;
        if (action == "edit") {
          self.operationType("EDIT");

          self.createElementEffectiveStartDate(dateConverter(rowDataElement.effectiveStartDate));
          self.processingTypeVal(rowDataElement.ProccessingType);
          self.elementNameVal(self.elementsArr().find(obj => obj.elementTypeId == rowDataElement.elementTypeId).elementTypeId)

          self.getInputValues(self.elementNameVal());

          $("#search-emp-page").hide();
          $("#summary-page").hide();
          $("#add-edit-page").fadeIn();
        } else if (action == 'correction') {

          self.operationType("CORRECTION");

          self.createElementEffectiveStartDate(dateConverter(rowDataElement.effectiveStartDate));
          self.processingTypeVal(rowDataElement.ProccessingType);
          self.elementNameVal(self.elementsArr().find(obj => obj.elementTypeId == rowDataElement.elementTypeId).elementTypeId)

          self.getInputValues(self.elementNameVal());

          $("#search-emp-page").hide();
          $("#summary-page").hide();
          $("#add-edit-page").fadeIn();

        } else if (action == "remove") {
          if (rowDataElement.ProccessingType == 'Nonrecurring') {
            self.removeElement(rowDataElement.elementEntryId, dateConverterForCreation(new Date()));
          } else {
            var popup = document.getElementById('remove-element-modal');
            popup.open('#btnGo');
          }
        }
      }.bind(this);

      self.removeElement = async function (id, endDate) {
        var success = function (result) {
          self.notificationMessage('confirmation', ['Element Entry is removed.']);
          self.getElementData(false);
        }
        var fail = function () {
          self.notificationMessage('error', ['Something went wrong.']);
        }
        var payload = {
          'endDate': endDate
        }
        var url = "payroll/rest/AppsProRest/DeleteElementEntry/" + id;
        await services.addGeneric(url, payload).then(success, fail);

      }

      function dateConverter(date, iso = false) {
        if (date) {
          var d = new Date(date);
          var new_date;

          if (iso) {
            new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
          } else {
            new_date = d.getFullYear() + '-' + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + ('0' + d.getDate()).slice(-2);
          }
          return new_date;
        }
      }


      function validateBetween(check, from, to) {
        if (!check || !from || !to)
          return true;
        return (new Date(check) >= new Date(from) && new Date(check) <= new Date(to));
      }

      function dateConverterForCreation(date, iso = false) {
        if (date) {
          var d = new Date(date);
          var new_date;

          if (iso) {
            new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
          } else {
            new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
          }
          return new_date;
        }
      }

      function getLastDayOfMonth(date) {
        var d = new Date(date);
        var new_date = new Date(d.getFullYear(), d.getMonth() + 1, 0);
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      self.createOrUpdateElement = async function () {

        if (!validateBetween(self.createElementEffectiveStartDate(), self.empStartDate(), self.empEndDate())) {
          self.notificationMessage('error', ['Element Entry Date must between Employee Start Date and End Date']);
          return false;
        }

        var valid = _checkValidationGroupElement();
        if (valid) {
          self.saveChangesDisabled(true);

          var elementEntryId = self.operationType() == "ADD" ? 0 : rowDataElement.elementEntryId;

          var inputValues = [];
          for (var i in self.dateInputValArr())
            inputValues.push({ "id": self.dateInputValArr()[i].inputValueId, "Value": self.dateInputValArr()[i].defaultValue, "elementEntryId": elementEntryId })
          for (var i in self.numberOrMoneyInputValArr())
            inputValues.push({ "id": self.numberOrMoneyInputValArr()[i].inputValueId, "Value": self.numberOrMoneyInputValArr()[i].defaultValue, "elementEntryId": elementEntryId })
          for (var i in self.characterInputValArr())
            inputValues.push({ "id": self.characterInputValArr()[i].inputValueId, "Value": self.characterInputValArr()[i].defaultValue, "elementEntryId": elementEntryId })

          var payload = {
            "effectiveStartDate": dateConverterForCreation(self.createElementEffectiveStartDate()),
            "effectiveEndDate": self.processingTypeVal() == 'Recurring' ? "" : dateConverterForCreation(getLastDayOfMonth(self.createElementEffectiveStartDate())),
            "elementTypeId": self.elementNameVal(),
            "personId": self.assignmentOptionsArr().find(obj => obj.value == self.assignmentVal()).personId,
            "inputValues": inputValues
          };

          var success = function (result) {
            self.notificationMessage('confirmation', ['Element Entry has been added']);
            self.tableDataElement([]);
          }

          var fail = function () {
            self.notificationMessage('error', ['Something went wrong.']);
          }

          if (self.operationType() == "EDIT") {
            // payload.effectiveEndDate = "";
            await services.editGeneric("payroll/rest/AppsProRest/UpdateElementEntry/" + rowDataElement.elementEntryId, payload).then(success, fail);
          } else if (self.operationType() == "CORRECTION") {
            await services.editGeneric("payroll/rest/AppsProRest/CorrectElementEntry", payload).then(success, fail);
          } else if (self.operationType() == "ADD") {
            await services.addGeneric("payroll/rest/AppsProRest/ElementEntry", payload).then(success, fail);
          }
        }
        self.saveChangesDisabled(false);
        self.getElementData(false);
        self.goToSummaryPage();
      }.bind(this);

      var _checkValidationGroupElement = function () {
        var tracker = document.getElementById("edit-element-tracker");
        var tracker2 = document.getElementById("edit-element-tracker-2");
        if (tracker.valid === "valid" && tracker2.valid === "valid") {
          return true;
        }
        else {
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          tracker2.showMessages();
          tracker2.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.getInputValues = async function (id) {
        self.inputValuesLoaded(false);
        self.numberOrMoneyInputValArr([]);
        self.dateInputValArr([]);
        self.characterInputValArr([]);
        var success = function (result) {
          for (var index in result) {
            if (result[index].uom == 'number' || result[index].uom == 'money') {
              result[index].defaultValue = parseInt(result[index].defaultValue) ? parseInt(result[index].defaultValue) : 0;

              if (self.operationType() == "EDIT" || self.operationType() == "CORRECTION")
                for (var element in self.elementEntryDetails())
                  if (self.elementEntryDetails()[element].elementEntryId == rowDataElement.elementEntryId)
                    if (self.elementEntryDetails()[element].inputValueId == result[index].inputValueId)
                      if (parseInt(self.elementEntryDetails()[element].screenEntryValue))
                        result[index].defaultValue = parseInt(self.elementEntryDetails()[element].screenEntryValue);

              // result[index].maxValue = parseInt(result[index].maxValue);
              // result[index].minValue = parseInt(result[index].minValue);
              self.numberOrMoneyInputValArr.push(result[index]);

            } else if (result[index].uom == 'date') {
              result[index].defaultValue = dateConverter(result[index].defaultValue) ? dateConverter(result[index].defaultValue) : "";

              if (self.operationType() == "EDIT" || self.operationType() == "CORRECTION")
                for (var element in self.elementEntryDetails())
                  if (self.elementEntryDetails()[element].elementEntryId == rowDataElement.elementEntryId)
                    if (self.elementEntryDetails()[element].inputValueId == result[index].inputValueId)
                      if (dateConverter(self.elementEntryDetails()[element].screenEntryValue))
                        result[index].defaultValue = dateConverter(self.elementEntryDetails()[element].screenEntryValue);

              self.dateInputValArr.push(result[index]);

            } else {
              result[index].defaultValue = result[index].defaultValue ? result[index].defaultValue : "";

              if (self.operationType() == "EDIT" || self.operationType() == "CORRECTION")
                for (var element in self.elementEntryDetails())
                  if (self.elementEntryDetails()[element].elementEntryId == rowDataElement.elementEntryId)
                    if (self.elementEntryDetails()[element].inputValueId == result[index].inputValueId)
                      if (self.elementEntryDetails()[element].screenEntryValue)
                        result[index].defaultValue = self.elementEntryDetails()[element].screenEntryValue;

              self.characterInputValArr.push(result[index]);
            }
          }
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetInputValue/" + id;
        await services.getGeneric(url).then(success, fail);

        self.inputValuesLoaded(true);
      }

      self.getAllEmployees = ko.computed(function () {
        if (app.personDetails().length) {
          self.searchInputStatus(false);
          self.personDetailsArr(app.personDetails());
          self.assignmentOptionsArr([]);
          $.each(app.personDetails(), function (index, val) {
            self.assignmentOptionsArr.push({ label: val.PERSON_NUMBER, value: val.PERSON_NUMBER, personId: val.PERSON_ID, displayName: val.DISPLAY_NAME });
          });

        } else {
          self.searchInputStatus(true);
        }
      });

      self.getElementEntryDetails = async function () {
        var success = function (result) {
          if (result) {
            self.elementEntryDetails(result);
          }
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementEntryDetails";
        await services.getGeneric(url).then(success, fail);
      }

      self.elementChangeListener = function (e) {

        if (self.elementNameVal()) {
          var currentSelectedElement = self.elementNameOptionsArr().find(obj => obj.value == self.elementNameVal());
          if (currentSelectedElement) {
            self.elementNameLbl(currentSelectedElement.label);
            self.processingTypeVal(currentSelectedElement.processingType);
          } else {
            currentSelectedElement = self.elementsArr().find(obj => obj.elementTypeId == self.elementNameVal());
            self.elementNameLbl(currentSelectedElement.elementName);
            self.processingTypeVal(currentSelectedElement.processingType);
          }

        } else {
          self.elementNameLbl("")
          self.processingTypeVal("")
        }

      };

      self.goToSummaryPage = function () {
        $("#search-emp-page").hide();
        $("#add-edit-page").hide();
        $("#summary-page").fadeIn();
        self.clearValues();
      }.bind(this);

      self.goToSearchPage = function () {
        $("#search-emp-page").fadeIn();
        $("#add-edit-page").hide();
        $("#summary-page").hide();
        self.clearValues();
      }.bind(this);

      self.connected = () => {
        app.collapseSideMenu();
        self.getElementForm();
        self.getAllEmployees();
        self.getElementData(false);
        accUtils.announce('Dashboard page loaded.', 'assertive');
        $("li#ui-id-6").removeClass("oj-selected").addClass("oj-default");
      };


      self.columnArray([
        // {
        //   "headerText": "Employee Name", "field": "empName"
        // },
        // {
        //   "headerText": "Employee Number", "field": "empNumber"
        // },
        {
          "headerText": "Element Name", "field": "elementName"
        },
        {
          "headerText": "Effective As-of Date", "field": "effectiveStartDate"
        },
        {
          "headerText": "Effective End Date", "field": "effectiveEndDate"
        },
        {
          "headerText": "Element Type", "field": "elementType"
        },
        {
          "headerText": "Processing Type", "field": "ProccessingType"
        },
        {
          "headerText": "Action", "field": "action", "template": "actionTemplateEnties"
        }
      ]);

      self.columnArrayPerson([
        {
          "headerText": "Name", "field": "DISPLAY_NAME"
        },
        {
          "headerText": "Person Number", "field": "PERSON_NUMBER"
        },
        {
          "headerText": "Position", "field": "POSITION_NAME"
        },
        {
          "headerText": "Start Date", "field": "EFFECTIVE_START_DATE"
        },
        {
          "headerText": "End Date", "field": "EFFECTIVE_END_DATE"
        },
        {
          "headerText": "Job Name", "field": "JOB_NAME"
        },
        {
          "headerText": "Manager Name", "field": "MANAGER_NAME"
        },
        {
          "headerText": "Location", "field": "LOCATION"
        },
        {
          "headerText": "Manager Number", "field": "MANAGER_NUMBER"
        },
        {
          "headerText": "Organization Name", "field": "MANAGER_NUMBER"
        },
        {
          "headerText": "Action", "field": "action", "template": "actionTemplate"
        }
      ]);

    }

    return IndexElementViewModel;
  }
);

