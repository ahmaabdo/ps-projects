/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['accUtils', 'config/services', 'knockout','appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojtimezonedata'
],
  function (accUtils, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {
    function IndexElementViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      self.notifMessage = ko.observableArray();
      self.messagesDataprovider = ko.observableArray();
      self.notificationMessage = function (type, messages) {
        var msg = [];
        switch (type) {
          case 'error':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'error',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'warning':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'warning',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'success':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
          default:
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
        }

        self.notifMessage(msg);
      }
      self.messagesDataprovider = new ArrayDataProvider(self.notifMessage);


    ////navigator
    
            self.back = function(){
              app.router.go('homeSetup')
            };
            self.manageElement = function(){
              app.router.go('indexElement')
            };
            self.manageObjectGroup = function(){
              app.router.go('indexObjectGroup')
            };
            self.fastFormula = function(){
              app.router.go('indexFastFormula')
            };
            self.report = function(){
              app.router.go('indexReport')
            };
    /////////////////
            
      /**
       * Date Format
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      this.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      function currentDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
  
        return yyyy+'-'+mm+'-'+dd;
      }

      self.currentDate = ko.observable(currentDate());

      /**
       * Call Element APIs 
       */

      self.getElementData = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm";
        services.getGeneric(url).then(success, fail);
      };

      self.getElementDataById = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm/" + id;
        services.getGeneric(url).then(success, fail);
      }

      self.getClassificationElement = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }

        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/ClassificationElement";
        services.getGeneric(url).then(success, fail);
      }

      self.getClassificationElementById = async function (id) {
        self.getClassificationElement((result) => {
          var value = [];
          $.each(result, function (index, classficationElement) {
            if (classficationElement.classificationId == id) {
              value = classficationElement;
              return false;
            }
          });
          return id;
        });
      }

      self.getCurrency = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetCurrency";
        services.getGeneric(url).then(success, fail);
      }

      self.getLookUp = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/Lookup";
        services.getGeneric(url).then(success, fail);
      }

      /**
       * Element
       */
      self.advanceDisable = ko.observable();

      // element search keyword
      self.keyword = ko.observable();

      self.keywordSearch = function () {
        console.log(self.keyword());
      }.bind(this);

      // element search advance
      self.advanceElementName = ko.observable();
      self.advanceElementType = ko.observable();
      self.advanceElementTypeOptions = ko.observableArray();

      self.advanceElementProcessingType = ko.observable();
      self.advanceElementEffectiveDate = ko.observable();
      self.advanceElementEffectiveEndDate = ko.observable();
      self.advanceElementInputValue = ko.observable("test");
      self.advanceElementPriority = ko.observable();
      self.advanceElementCurrency = ko.observable();


      self.advanceSearchElement = function () {
        var data = {
          "elementName": self.advanceElementName(),
          "elementType": self.advanceElementType(),
          "processingType": self.advanceElementProcessingType(),
          "effictiveDate": self.advanceElementEffectiveDate(),
          "inputValue": self.advanceElementInputValue(),
          "priority": self.advanceElementPriority(),
          "currency": self.advanceElementCurrency(),
        }
        console.log(data);
      }

      // element options
      self.elementTypeOptions = ko.observableArray();
      self.currencyOptions = ko.observableArray();
      self.processingTypeOptions = ko.observableArray();

      function setElementDropdowns() {
        // element classification option
        self.getClassificationElement((result) => {
          var options = [];
          $.each(result, function (index, classficationElement) {
            options.push({ value: classficationElement.classificationId, label: classficationElement.classificationName });
          });
          self.elementTypeOptions(options);
        });

        // currency option
        self.getCurrency((result) => {
          var options = [];
          $.each(result, function (index, currency) {
            options.push({ value: currency.CURRENCY_CODE, label: currency.DESCRIPTION });
          });
          self.currencyOptions(options);
        });

        // processing type option
        self.getLookUp((result) => {
          var options = [];
          $.each(result, function (index, processingType) {
            if (processingType.name == "Processing Type") options.push({ value: processingType.code, label: processingType.valueEn });
          });
          self.processingTypeOptions(options);
        });
      }

      // CREATE ELEMENT
      self.createElementName = ko.observable();
      self.createElementType = ko.observable();
      self.createElementProcessType = ko.observable();
      self.createElementEffectiveStartDate = ko.observable();
      self.createElementEffectiveEndDate = ko.observable();
      self.createElementPriority = ko.observable();
      self.createElementCurrency = ko.observable();

      // create element modal
      self.startAnimationListenerCreateElement = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-element-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerCreateElement = function () {
        var popup = document.getElementById('create-element-modal');
        popup.open('#btnGo');

      };
      self.cancelListenerCreateElement = function () {
        var popup = document.getElementById('create-element-modal');
        popup.close();
      };

      // User presses the submit button
      // No need to revalidate each field since
      // field level validation occurs on blur and Enter.
      self.createElement = function () {
        var valid = _checkValidationGroupCreateElement();

        if (valid) {
          // submit the form would go here
          // eslint-disable-next-line no-alert
          var payload = {
            "elementName": self.createElementName(),
            "elementType": self.createElementType(),
            "processingType": self.createElementProcessType(),
            "effectiveStartDate": dateConverter(self.createElementEffectiveStartDate()),
            "effectiveEndDate": (self.createElementEffectiveEndDate())? dateConverter(self.createElementEffectiveEndDate()) : '',
            "priority": self.createElementPriority(),
            "currency": self.createElementCurrency()
          }

          var success = function (result) {  
            if(result[0].success){
              self.notificationMessage('error', ['Element Name already exist.']);
            }else{
              var element = result[0];
              self.cancelListenerCreateElement();
              self.notificationMessage('success', ['Element is created.']);
              self.setTableElement();

              // create Pay Value if Earning or Deduction
              if(element.elementType == '1' || element.elementType == '2') self.createPayValue(element); 
            }
            
          }

          var fail = function (result) {
            console.log("fail");
            self.cancelListenerCreateElement();
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/defineElementForm";
          services.addGeneric(url, payload).then(success, fail);
        }
      }.bind(this);

      // creating Pay Value
      self.createPayValue = function(element){
        var payload = {
          "name": "Pay Value",
          "displaySequance": 1,
          "uom": "money",
          "effectiveStartDate": dateConverter(element.effectiveStartDate),
          "effectiveEndDate": "",
          "elementTypeId": element.elementTypeId,
          "userDisplayFlag": "true",
          "mandatoryFlag": "true",
          "defaultValue": 0,
          "lookupType": "Test",
        }
        console.log(payload);
        var success = function (result) {
          console.log("pay value created");
        }
        var fail = function () {
          console.log("something went wrong.");
        }
        var url = "payroll/rest/AppsProRest/InputValue";
        services.addGeneric(url, payload).then(success, fail);        
      }

      var _checkValidationGroupCreateElement = function () {
        var tracker = document.getElementById("create-element-tracker");
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      // ELEMENT TABLE
      self.arrayElement = ko.observableArray();
      self.dataProviderElement = ko.observableArray();

      self.setTableElement = function () {
        self.getElementData((result) => {
          var temp = [];
          $.each(result, function (index, element) {

            temp.push({
              num: ++index,
              elementName: element.elementName,
              elementType: element.elementType,
              processingType: element.processingType,
              priority: element.priority,
              effectiveStartDate: dateConverter(element.effectiveStartDate),
              effectiveEndDate: dateConverter(element.effectiveEndDate),
              currency: element.currency,
              status: "1",
              action: element.elementTypeId,
            });
          });
          
          self.tableDataElement(result);
          self.arrayElement(temp);
        });
      }
      
      self.dataProviderElement = new ArrayDataProvider(self.arrayElement, { keyAttributes: 'num', implicitSort: [{ attribute: 'priority', direction: 'ascending' }] });

      // element table row listener
      var rowDataElement;
      self.tableDataElement = ko.observableArray();

      self.selectedRowKeyElement = ko.observable();
      self.selectedIndexElement = ko.observable();
      self.tableSelectionListenerElement = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if (currentRow) {
          self.selectedRowKeyElement(currentRow['rowKey']);
          self.selectedIndexElement(currentRow['rowIndex'])
          var thisRow = self.selectedIndexElement();
          rowDataElement = self.tableDataElement()[thisRow]
          console.log(thisRow);
          console.log(rowDataElement);
        }
      };

      self.elementAction = function (event) {
        var action = event.target.value;
        if (action == "edit") {
          self.editElement(rowDataElement.elementTypeId);
          self.setTableInputValues(rowDataElement.elementTypeId);
          self.setTableEligibility(rowDataElement.elementTypeId);
          self.setTableCosting(rowDataElement.elementTypeId);
          self.setTableDFR(rowDataElement.elementTypeId);
          setCostingInputValueDropdown(rowDataElement.elementTypeId);

          $("#page-1").hide();
          $("#page-2").fadeIn();
        }
        if (action == "remove") {
          self.removeElement(rowDataElement.elementTypeId);
        }
      }.bind(this);

      self.removeElement = function (id) {
        var success = function (result) {
          if(result[0].success){
            self.notificationMessage('error', ['Element is being used. It cannot be removed.']);
          }else{
            self.notificationMessage('success', ['Element is removed.']);
            self.setTableElement();
          }
        }
        var fail = function () {
          self.notificationMessage('error', ['Something went wrong.']);
        }
        var url = "payroll/rest/AppsProRest/DeleteElementForm/" + id;
        services.deleteGeneric(url).then(success, fail);
      }


      /**
       * PAGE 2
       */

      // EDIT ELEMENT
      self.editElementID = ko.observable();
      self.editElementName = ko.observable();
      self.editElementType = ko.observable();
      self.editElementProcessingType = ko.observable();
      self.editElementPriority = ko.observable();
      self.editElementEffectiveDate = ko.observable();
      self.editElementEffectiveEndDate = ko.observable();
      self.editElementCurrency = ko.observable();
      self.editElementStatus = ko.observable();

      self.editElement = function (id) {
        self.getElementDataById((result) => {
          var element = result[0];

          self.elementTypeOptions().forEach(function (option, index) {
            if (option.label == element.elementType) self.editElementType(parseInt(option.value));
          });
          self.editElementID(element.elementTypeId);
          self.editElementName(element.elementName);
          self.editElementProcessingType(element.processingType);
          self.editElementPriority(parseInt(element.priority));
          self.editElementEffectiveDate(dateConverter(element.effectiveStartDate, true));
          self.editElementEffectiveEndDate(dateConverter(element.effectiveEndDate, true));
          self.editElementCurrency(element.currency);
          self.editElementStatus("1");
        }, id);

      }

      self.updateElement = function () {
        var valid = _checkValidationGroupEditElement();
        if (valid) {
          // submit the form would go here
          // eslint-disable-next-line no-alert
          var payload = {
            "elementName": self.editElementName(),
            "elementType": self.editElementType(),
            "processingType": self.editElementProcessingType(),
            "priority": self.editElementPriority(),
            "effectiveStartDate": dateConverter(self.editElementEffectiveDate()),
            "effectiveEndDate": dateConverter(self.editElementEffectiveEndDate()),
            "currency": self.editElementCurrency()
          };

          var success = function (result) {
            console.log(result);
            var element = result[0];
            self.editElement(element.elementTypeId);
            self.notificationMessage('success', ['Element is updated.']);
            self.setTableElement();
          }

          var fail = function () {
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/updateElementForm/" + self.editElementID();
          services.editGeneric(url, payload).then(success, fail);
        }
      }.bind(this);

      var _checkValidationGroupEditElement = function () {
        var tracker = document.getElementById("edit-element-tracker");
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };


      /**
       * ELEMENT: INPUT-VALUES
      */

      // Call Input Value APIs
      self.getInputValues = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetInputValue/" + id;
        services.getGeneric(url).then(success, fail);
      }

      self.arrayInputValue = ko.observableArray();
      self.dataProviderInputValues = ko.observableArray();

      // INPUT VALUE TABLE
      self.setTableInputValues = function (id) {
        self.getInputValues((result) => {
          var temp = [];
          $.each(result, function (index, inputValue) {
            temp.push({
              num: ++index,
              name: inputValue.name,
              action: inputValue.inputValueId,
            });
          });
          self.tableDataInputValue(result);
          self.arrayInputValue(temp);
        }, id);
      };

      self.dataProviderInputValues = new ArrayDataProvider(self.arrayInputValue, { keyAttributes: 'action', implicitSort: [{ attribute: 'priority', direction: 'ascending' }] });

      // input values row table listener
      var rowDataInputValue;
      self.tableDataInputValue = ko.observableArray();

      self.selectedRowKeyInputValue = ko.observable();
      self.selectedIndexInputValue = ko.observable();
      self.tableSelectionListenerInputValue = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if (currentRow) {
          self.selectedRowKeyInputValue(currentRow['rowKey']);
          self.selectedIndexInputValue(currentRow['rowIndex'])
          var thisRow = self.selectedIndexInputValue();
          rowDataInputValue = self.tableDataInputValue()[thisRow];
          console.log(thisRow);
          console.log(rowDataInputValue);
        }
      };

      self.inputValueAction = function (event) {
        var action = event.target.value;
        if (action == "edit") {
          self.editInputValue(rowDataInputValue.inputValueId);
          self.modalEditInputValue();
        }
        if (action == "remove") {
          self.removeInputValue(rowDataInputValue.inputValueId)
        }  
      }

      // REMOVE INPUT-VALUES
      self.removeInputValue = function (id) {
        var success = function (result) {
          self.notificationMessage('success', ['Input Value is removed.']);
          self.setTableInputValues(self.editElementID());
        }
        var fail = function () {
          self.notificationMessage('error', ['Something went wrong.']);
        }
        var url = "payroll/rest/AppsProRest/DeleteInputValue/" + id;
        services.deleteGeneric(url).then(success, fail);
      }

      // CREATE INPUT-VALUES
      self.inputValueID = ko.observable("");
      self.inputValueName = ko.observable("");
      self.inputValueDisplaySequence = ko.observable(null);
      self.inputValueUnitOfMeasure = ko.observable("");
      self.inputValueEffectiveStartDate = ko.observable("");
      self.inputValueEffectiveEndDate = ko.observable("");
      self.inputValueDisplay = ko.observable("false");
      self.inputValueRequired = ko.observable("false");
      self.inputValueDefaultValue = ko.observable();
      self.inputValueTypeName = ko.observable("");
      // self.inputValueMin = ko.observable(0);
      // self.inputValueMax = ko.observable(0);
      self.inputValueWarningError = ko.observable("");

      self.inputValueCheckbox = ko.observableArray([
        self.inputValueDisplay(),
        self.inputValueRequired(),
      ]);

      function clearInputValues() {
        self.inputValueID("");
        self.inputValueName("");
        self.inputValueDisplaySequence(null);
        self.inputValueUnitOfMeasure("");
        self.inputValueEffectiveStartDate("");
        self.inputValueEffectiveEndDate("");
        self.inputValueDisplay("false");
        self.inputValueRequired("false");
        self.inputValueDefaultValue(null);
        self.inputValueTypeName("");
        // self.inputValueMin(0);
        // self.inputValueMax(0);
        // self.inputValueWarningError("");

        self.inputValueCheckbox([]);
      }

      

      self.createInputValue = function(){
        var valid = _checkValidationGroupInputValue("create-inputValue-tracker");

        if(valid){
          self.inputValueDisplay("false");
          self.inputValueRequired("false");
          $.each(self.inputValueCheckbox(), function (index, value) {
            if(value == "display") self.inputValueDisplay("true");
            if(value == "required") self.inputValueRequired("true");
          });

          var payload = {
            "name": self.inputValueName(),
            "displaySequance": self.inputValueDisplaySequence(),
            "uom": self.inputValueUnitOfMeasure(),
            "effectiveStartDate": dateConverter(self.inputValueEffectiveStartDate()),
            "effectiveEndDate": (self.inputValueEffectiveEndDate())? dateConverter(self.inputValueEffectiveEndDate()) : "",
            "elementTypeId": self.editElementID(),
            "userDisplayFlag": self.inputValueDisplay(),
            "mandatoryFlag": self.inputValueRequired(),
            "defaultValue": self.inputValueDefaultValue(),
            "lookupType": "Test",
            // "minValue": self.inputValueMin(),
            // "maxValue": self.inputValueMax(),
            // "warningOfErorr": self.inputValueWarningError()
          }

          var success = function (result) {
            self.cancelModalInputValue();
            self.notificationMessage('success', ['Input Value is created.']);
            self.setTableInputValues(self.editElementID());
            clearInputValues();
          }
          var fail = function () {
            self.cancelModalInputValue();
            self.notificationMessage('error', ['Something went wrong.'])
          }

          var url = "payroll/rest/AppsProRest/InputValue";
          services.addGeneric(url, payload).then(success, fail);
        }
        
      }

      // create input value modal
      self.startAnimationListenerCreateInputValue = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-inputValue-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.modalInputValue = function () {
        clearInputValues();
        var popup = document.getElementById('create-inputValue-modal');
        popup.open('#btnGo');
      };
      self.cancelModalInputValue = function () {
        var popup = document.getElementById('create-inputValue-modal');
        popup.close();
      };

      self.editInputValue = function(){
        clearInputValues();
        self.inputValueID(rowDataInputValue.inputValueId);
        self.inputValueName(rowDataInputValue.name);
        self.inputValueDisplaySequence(parseInt(rowDataInputValue.displaySequance));
        self.inputValueUnitOfMeasure(rowDataInputValue.uom);
        self.inputValueEffectiveStartDate(dateConverter(rowDataInputValue.effectiveStartDate,true));
        self.inputValueEffectiveEndDate(dateConverter(rowDataInputValue.effectiveEndDate,true));
        self.inputValueDisplay(rowDataInputValue.userDisplayFlag);
        self.inputValueRequired(rowDataInputValue.mandatoryFlag);

        let defualtValue = null;
        if(typeof rowDataInputValue.defaultValue != 'undefined'){
          if(rowDataInputValue.uom == 'Number' || rowDataInputValue.uom == 'Money') defualtValue = parseInt(rowDataInputValue.defaultValue);
          if(rowDataInputValue.uom == 'Character' || rowDataInputValue.uom == '') defualtValue = rowDataInputValue.defaultValue;
          if(rowDataInputValue.uom == 'Date') defualtValue = dateConverter(rowDataInputValue.defaultValue,true);
        }
        console.log(defualtValue);
        self.inputValueDefaultValue(defualtValue);
        // self.inputValueTypeName("test");
        // self.inputValueMin(parseInt(rowDataInputValue.minValue));
        // self.inputValueMax(parseInt(rowDataInputValue.maxValue));
        // self.inputValueWarningError(rowDataInputValue.warningOfErorr);

        self.inputValueCheckbox([
          (self.inputValueDisplay() == "true")? "display" : "",
          (self.inputValueRequired() == "true")? "required" : "",
        ]);
      }

      self.inputListenerUOM = function(){
        var optionValue = event.detail;
        var uom = optionValue.value;
        if(uom != self.inputValueUnitOfMeasure()) self.inputValueDefaultValue(null);
      }

      self.updateInputValue = function(){
        var valid = _checkValidationGroupInputValue("edit-inputValue-tracker");

        if(valid){
          self.inputValueDisplay("false");
          self.inputValueRequired("false");
          $.each(self.inputValueCheckbox(), function (index, value) {
            if(value == "display") self.inputValueDisplay("true");
            if(value == "required") self.inputValueRequired("true");
          });

          var payload = {
            "name": self.inputValueName(),
            "displaySequance": self.inputValueDisplaySequence(),
            "uom": self.inputValueUnitOfMeasure(),
            "effectiveStartDate": dateConverter(self.inputValueEffectiveStartDate()),
            "effectiveEndDate": (self.inputValueEffectiveEndDate())? dateConverter(self.inputValueEffectiveEndDate()) : "",
            "elementTypeId": self.editElementID(),
            "userDisplayFlag": self.inputValueDisplay(),
            "mandatoryFlag": self.inputValueRequired(),
            "defaultValue": self.inputValueDefaultValue(),
            "lookupType": "none",
            // "minValue": self.inputValueMin(),
            // "maxValue": self.inputValueMax(),
            // "warningOfErorr": self.inputValueWarningError()
          }

          var success = function (result) {
            self.cancelModalEditInputValue();
            self.notificationMessage('success', ['Input Value is updated.']);
            self.setTableInputValues(self.editElementID());
            clearInputValues();
          }

          var fail = function () {
            self.cancelModalEditInputValue();
            self.notificationMessage('error', ['Something went wrong.']);
          }
          
          var url = "payroll/rest/AppsProRest/UpdateInputValue/"+self.inputValueID();
          services.editGeneric(url, payload).then(success,fail);
        }

      }

      // Edit input value modal
      self.startAnimationListenerEditInputValue = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'edit-inputValue-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.modalEditInputValue = function () {
        var popup = document.getElementById('edit-inputValue-modal');
        popup.open('#btnGo');
      };
      self.cancelModalEditInputValue = function () {
        var popup = document.getElementById('edit-inputValue-modal');
        popup.close();
      };

      // Input Value validation
      var _checkValidationGroupInputValue = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      /**
       * ELEMENT: ELIGIBILITY
      */

      // Call Eligibility APIs
      self.getEligibility = async function(callback, id) {
        var success = function(result){
          if(callback) callback(result);
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetAllElementEligibility/"+id;
        services.getGeneric(url).then(success,fail);
      }

      self.getEligibilityById = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementEligibility/" + id;
        services.getGeneric(url).then(success, fail);
      }

      self.getDynamicRes = async function (callback, lov) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/dynsmicRes/" + lov;
        services.getGeneric(url).then(success, fail);
      }

      self.getPeopleGroup = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetPeopleGroupLOV";
        services.getGeneric(url).then(success, fail);
      }

      // Set eligibility LOVs dropdown
      self.optionEligibitliyGrade = ko.observableArray();
      self.optionEligibilityJob = ko.observableArray();
      self.optionEligibilityLocation = ko.observableArray();
      self.optionEligibilityOrganization = ko.observableArray();
      self.optionEligibilityPayroll = ko.observableArray();
      self.optionEligibilityPosition = ko.observableArray();
      self.optionEligibilitySalary = ko.observableArray();
      self.optionEligibilityPeopleGroup = ko.observableArray();

      function setEligibiliyDropdowns() {
        self.getDynamicRes((result) => {
          let temp = [];
          $.each(result, function (index, lov) {
            temp.push({
              value: lov.GradeId.toString(),
              label: lov.GradeName,
            });
          });
          self.optionEligibitliyGrade(temp);
        }, "grades");

        self.getDynamicRes((result) => {
          let temp = [];
          $.each(result, function (index, lov) {
            temp.push({
              value: lov.JobId.toString(),
              label: lov.Name,
            });
          });
          self.optionEligibilityJob(temp);
        }, "jobs");

        self.getDynamicRes((result) => {
          let temp = [];
          $.each(result, function (index, lov) {
            temp.push({
              value: lov.LocationId.toString(),
              label: lov.LocationName,
            });
          });
          self.optionEligibilityLocation(temp);
        }, "locations");

        self.getDynamicRes((result) => {
          let temp = [];
          $.each(result, function (index, lov) {
            temp.push({
              value: lov.OrganizationId.toString(),
              label: lov.Name,
            });
          });
          self.optionEligibilityOrganization(temp);
        }, "organizations");

        self.getDynamicRes((result) => {
          let temp = [];
          $.each(result, function (index, lov) {
            temp.push({
              value: lov.PayrollId.toString(),
              label: lov.PayrollName,
            });
          });
          self.optionEligibilityPayroll(temp);
        }, "payrollDefinitionsLOV");

        self.getDynamicRes((result) => {
          let temp = [];
          $.each(result, function (index, lov) {
            temp.push({
              value: lov.PositionId.toString(),
              label: lov.Name,
            });
          });
          self.optionEligibilityPosition(temp);
        }, "positions");

        self.getDynamicRes((result) => {
          let temp = [];
          $.each(result, function (index, lov) {
            temp.push({
              value: lov.SalaryBasisId.toString(),
              label: lov.SalaryBasisName,
            });
          });
          self.optionEligibilitySalary(temp);
        }, "salaryBasisLov");

        self.getPeopleGroup((result) => {
          let temp = [];
          $.each(result, function (index, lov) {
            temp.push({
              value: lov.PEOPLE_GROUP_ID.toString(),
              label: lov.GROUP_NAME,
            });
          });
          self.optionEligibilityPeopleGroup(temp);
        });
      }

      // ELIGIBILITY TABLE
      self.arrayEligibility = ko.observableArray();
      self.dataProviderEligibility = ko.observableArray();

      self.setTableEligibility = function (id) {
        self.arrayEligibility([]);
        self.getEligibility((result) => {
          var temp = [];
          $.each(result, function (index, eligibility) {
            if (eligibility.elementTypeId == self.editElementID()) {
              temp.push({
                num: ++index,
                name: eligibility.elementLinkName,
                action: eligibility.elementLinkId,
              });
            }
          });
          self.tableDataEligibility(temp);
          self.arrayEligibility(temp);
        }, id);
      };

      self.dataProviderEligibility = new ArrayDataProvider(self.arrayEligibility, { keyAttributes: 'num', implicitSort: [{ attribute: 'num', direction: 'ascending' }] });

      // eligibility row table listener
      var rowDataEligibility;
      self.tableDataEligibility = ko.observableArray();

      self.selectedRowKeyEligibility = ko.observable();
      self.selectedIndexEligibility = ko.observable();
      self.tableSelectionListenerEligibility = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if (currentRow) {
          self.selectedRowKeyEligibility(currentRow['rowKey']);
          self.selectedIndexEligibility(currentRow['rowIndex'])
          var thisRow = self.selectedIndexEligibility();
          rowDataEligibility = self.tableDataEligibility()[thisRow];
          console.log(thisRow);
          console.log(rowDataEligibility);
        }
      };

      self.eligibilityAction = function (event) {
        var action = event.target.value;
        if (action == "edit") {
          self.editEligibility(rowDataEligibility.action);
          self.modalEditEligibility();
        }
        if (action == "remove") {
          self.removeEligibility(rowDataEligibility.action)
        }
      }.bind(this);

      // REMOVE ELIGIBILITY
      self.removeEligibility = function (id) {
        var success = function (result) {
          self.notificationMessage('success', ['Eligibility is removed.']);
          self.setTableEligibility(self.editElementID());
        }
        var fail = function () {
          self.notificationMessage('error', ['Something went wrong.']);
          self.notificationMessage('success', ['Eligibility is removed.']);
          self.setTableEligibility(self.editElementID());
        }

        var url = "payroll/rest/AppsProRest/DeleteElementEligibility/" + id;
        services.deleteGeneric(url).then(success, fail);
      }

      // CREATE ELIGIBILITY
      self.eligibilityID = ko.observable();
      self.eligibilityName = ko.observable("");
      self.eligibilityEffectiveStartDate = ko.observable("");
      self.eligibilityEffectiveEndDate = ko.observable("");
      //self.eligibilityAutomaticEntry = ko.observableArray([]);
      self.eligibilityGradeId = ko.observable("");
      self.eligibilityJobId = ko.observable("");
      self.eligibilityLocationId = ko.observable("");
      self.eligibilityOrganizationId = ko.observable("");
      self.eligibilityPayrollId = ko.observable("");
      self.eligibilityPeopleGroupId = ko.observable("");
      self.eligibilityPositionId = ko.observable("");
      self.eligibilitySalaryId = ko.observable("");

      function clearEligibility() {
        self.eligibilityName("");
        self.eligibilityEffectiveStartDate("");
        self.eligibilityEffectiveEndDate("");
        //self.eligibilityAutomaticEntryArray([]);
        self.eligibilityGradeId("");
        self.eligibilityJobId("");
        self.eligibilityLocationId("");
        self.eligibilityOrganizationId("");
        self.eligibilityPayrollId("");
        self.eligibilityPeopleGroupId("");
        self.eligibilityPositionId("");
        self.eligibilitySalaryId("");
      }

      self.createEligibility = function(){
        var valid = _checkValidationGroupEligibility("create-eligibility-tracker");

        if (valid) {
          var payload = {
            "elementLinkName": self.eligibilityName(),
            "effectiveStartDate": dateConverter(self.eligibilityEffectiveStartDate()),
            "effectiveEndDate": (self.eligibilityEffectiveEndDate())? dateConverter(self.eligibilityEffectiveEndDate()) : "",
            "gradeId": self.eligibilityGradeId(),
            "jobId": self.eligibilityJobId(),
            "locationId": self.eligibilityLocationId(),
            "organizationId": self.eligibilityOrganizationId(),
            "payrollId": self.eligibilityPayrollId(),
            "positionId": self.eligibilityPositionId(),
            "businessGroupId": "0",
            "peopleGroupId": self.eligibilityPeopleGroupId(),
            "standardLinkFlag": "",
            "elementTypeId": self.editElementID()
          }

          var success = function (result) {
            self.cancelModalEligibility();
            self.notificationMessage('success', ['Eligibility is created.']);
            self.setTableEligibility(self.editElementID());

            clearEligibility();
          }
          var fail = function () {
            self.cancelModalEligibility();
            self.notificationMessage('error', ['Something went wrong.'])
          }

          var url = "payroll/rest/AppsProRest/insertElementEligibility";
          services.addGeneric(url, payload).then(success, fail);
        }

      }

      // create input value modal
      self.startAnimationListenerCreateEligibility = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-eligibility-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.modalEligibility = function () {
        clearEligibility();
        var popup = document.getElementById('create-eligibility-modal');
        popup.open('#btnGo');
      };
      self.cancelModalEligibility = function () {
        var popup = document.getElementById('create-eligibility-modal');
        popup.close();
      };

      // EDIT EDIT ELIGIBILITY

      self.editEligibility = function(id){
        clearEligibility()
        self.getEligibilityById((result)=>{
          let eligibility = result[0];
          console.log(eligibility);
          self.eligibilityName(eligibility.elementLinkName);
          self.eligibilityID(eligibility.elementLinkId);
          self.eligibilityEffectiveStartDate(dateConverter(eligibility.effectiveStartDate,true));
          self.eligibilityEffectiveEndDate(dateConverter(eligibility.effectiveEndDate,true));
          //self.eligibilityAutomaticEntryArray([]);
          self.eligibilityGradeId(eligibility.gradeId);
          self.eligibilityJobId(eligibility.jobId);
          self.eligibilityLocationId(eligibility.locationId);
          self.eligibilityOrganizationId(eligibility.organizationId);
          self.eligibilityPayrollId(eligibility.payrollId);
          self.eligibilityPeopleGroupId(eligibility.peopleGroupId);
          self.eligibilityPositionId(eligibility.positionId);
        },id);

      }

      self.updateEligibility = function(id){
        var valid = _checkValidationGroupEligibility("edit-eligibility-tracker");

        if (valid) {
          var payload = {
            "elementLinkName": self.eligibilityName(),
            "elementLinkId": self.eligibilityID(),
            "effectiveStartDate": dateConverter(self.eligibilityEffectiveStartDate()),
            "effectiveEndDate": (self.eligibilityEffectiveEndDate())? dateConverter(self.eligibilityEffectiveEndDate()) : "",
            "gradeId": self.eligibilityGradeId(),
            "jobId": self.eligibilityJobId(),
            "locationId": self.eligibilityLocationId(),
            "organizationId": self.eligibilityOrganizationId(),
            "payrollId": self.eligibilityPayrollId(),
            "positionId": self.eligibilityPositionId(),
            "businessGroupId": "0",
            "peopleGroupId": self.eligibilityPeopleGroupId(),
            "standardLinkFlag": "true",
            "elementTypeId": self.editElementID()
          }

          var success = function (result) {
            self.cancelModalEditEligibility();
            self.notificationMessage('success', ['Eligibility is updated.']);
            self.setTableEligibility(self.editElementID());
          }

          var fail = function () {
            self.cancelModalEditEligibility();
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/updateElementEligibility";
          services.editGeneric(url, payload).then(success, fail);
        }

      }

      // Edit input value modal
      self.startAnimationListenerEditEligibility = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'edit-eligibility-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.modalEditEligibility = function () {
        var popup = document.getElementById('edit-eligibility-modal');
        popup.open('#btnGo');
      };
      self.cancelModalEditEligibility = function () {
        var popup = document.getElementById('edit-eligibility-modal');
        popup.close();
      };

      // eligibility form validation
      var _checkValidationGroupEligibility = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      /**
       * ELEMENT: COSTING
       */

      // Call Costing APIs

      self.getCostingData = async function(callback){
        var success = function(result){
          if(callback){
            var temp = [];
            $.each(result, function (index, costing) {
              if(costing.elementTypeId == self.editElementID()){
                temp.push(costing);
              }
            });

            callback(temp);
          } 
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetAllElementCosting";
        services.getGeneric(url).then(success, fail);
      }

      self.getCostingById = async function(callback, id){
        var success = function(result){
          if(callback){
            var temp = [];
            $.each(result, function (index, costing) {
              if(costing.elementTypeId == self.editElementID()){
                temp.push(costing);
              }
            });

            callback(temp);
          } 
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementCosting/"+id;
        services.getGeneric(url).then(success,fail);
      }

      self.getCostingSegments = async function(callback){
        var success = function(result){
          if(callback) callback(result);
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/getElementCostingSegments";
        services.getGeneric(url).then(success,fail);
      }

      // Set Costing dropdowns
      self.optionCostingType = ko.observableArray();
      self.optionCostingInputValue = ko.observableArray();

      self.requiredSegment = ko.observable(false);
      self.optionSegmentCompany = ko.observableArray(); // cost segment 1
      self.optionSegmentFunctions = ko.observableArray(); // cost segment 2
      self.optionSegmentNaturalAccount = ko.observableArray(); // cost segment 3
      self.optionSegmentGeographicLocations = ko.observableArray(); // cost segment 4
      self.optionSegmentTowers = ko.observableArray(); // cost segment 5
      self.optionSegmentProjects = ko.observableArray(); // cost segment 6
      self.optionSegmentProducts = ko.observableArray(); // cost segment 7
      self.optionSegmentFuture1 = ko.observableArray(); // cost segment 8
      self.optionSegmentFuture2 = ko.observableArray(); // cost segment 9

      function unique(arr){
        let new_arr = [...new Set(arr)]; 
        return new_arr;
      }

      function optionFormat(arr){
        let temp = [];
        $.each(arr, function (index, segment1) {
          temp.push({ value: segment1, label: segment1});
        });

        return temp;
      }

      function setCostingDropdowns(){
        self.getCostingSegments((result)=>{
          var temp1 = [];
          var temp2 = [];
          var temp3 = [];
          var temp4 = [];
          var temp5 = [];
          var temp6 = [];
          var temp7 = [];
          var temp8 = [];
          var temp9 = [];
          $.each(result, function (index, segment) {
            temp1.push(segment.COMPANY);
            temp2.push(segment.FUNCTION);
            temp3.push(segment.ACCOUNT);
            temp4.push(segment.GEO_LOCATION);
            temp5.push(segment.TOWER);
            temp6.push(segment.PROJECT);
            temp7.push(segment.PRODUCT);
            temp8.push(segment.FUTURE1);
            temp9.push(segment.FUTURE2);
          });

          self.optionSegmentCompany(optionFormat(unique(temp1)));
          self.optionSegmentFunctions(optionFormat(unique(temp2)));
          self.optionSegmentNaturalAccount(optionFormat(unique(temp3)));
          self.optionSegmentGeographicLocations(optionFormat(unique(temp4)));
          self.optionSegmentTowers(optionFormat(unique(temp5)));
          self.optionSegmentProjects(optionFormat(unique(temp6)));
          self.optionSegmentProducts(optionFormat(unique(temp7)));
          self.optionSegmentFuture1(optionFormat(unique(temp8)));
          self.optionSegmentFuture2(optionFormat(unique(temp9)));

        });

        self.getLookUp((result) => {
          var options = [];
          $.each(result, function (index, costType) {
            if (costType.name === "Cost Type") options.push({ value: costType.code, label: costType.valueEn });
          });
          self.optionCostingType(options);
        });
      }

      function setCostingInputValueDropdown(id){
        self.getInputValues((result) => {
          var options = [];
          console.log(">>>");
          console.log(result);
          $.each(result, function (index, inputValue) {
            if(inputValue.uom === "Money") options.push({ value: inputValue.inputValueId, label: inputValue.name });
          });

          self.optionCostingInputValue(options);
        }, id);
      }

      self.changeListenerCostType = function(event, current){
        var optionValue = event.detail;
        var id = optionValue.value;

        if(id === 'Costed') self.requiredSegment(true);
        else self.requiredSegment(false);
      }

      // Create Costing Modal
      self.startAnimationListenerCreateCosting = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-costing-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.modalCreateCosting = function () {
        clearCosting();
        var popup = document.getElementById('create-costing-modal');
        popup.open('#btnGo');
      };
      self.cancelModalCreateCosting = function () {
        var popup = document.getElementById('create-costing-modal');
        popup.close();
      };

      // CREATE COSTING
      self.costingID = ko.observable("");
      self.costingSegment = ko.observable("");
      self.costingInputValue = ko.observable("");
      self.costingEffectiveStartDate = ko.observable("");
      self.costingEffectiveEndDate = ko.observable("");
      self.costingGeneralLedger = ko.observableArray("");
      self.costingCostType = ko.observable("");

      self.costingCompany = ko.observable(""); // cost segment 1
      self.costingFunctions = ko.observable(""); // cost segment 2
      self.costingNaturalAccount = ko.observable(""); // cost segment 3
      self.costingGeographicLocations = ko.observable(""); // cost segment 4
      self.costingTowers = ko.observable(""); // cost segment 5
      self.costingProjects = ko.observable(""); // cost segment 6
      self.costingProducts = ko.observable(""); // cost segment 7
      self.costingFuture1 = ko.observable(""); // cost segment 8
      self.costingFuture2 = ko.observable(""); // cost segment 9

      self.costingOffsetCompany = ko.observable(""); // balance segment 1
      self.costingOffsetFunctions = ko.observable(""); // balance segment 2
      self.costingOffsetNaturalAccount = ko.observable(""); // balance segment 3
      self.costingOffsetGeographicLocations = ko.observable(""); // balance segment 4
      self.costingOffsetTowers = ko.observable(""); // balance segment 5
      self.costingOffsetProjects = ko.observable(""); // balance segment 6
      self.costingOffsetProducts = ko.observable(""); // balance segment 7
      self.costingOffsetFuture1 = ko.observable(""); // balance segment 8
      self.costingOffsetFuture2 = ko.observable(""); // balance segment 9

      function clearCosting() {
        self.costingID("");
        self.costingEffectiveStartDate("");
        self.costingEffectiveEndDate("");
        self.costingGeneralLedger([]);
        self.costingCostType("");

        self.costingCompany(""); // cost segment 1
        self.costingFunctions(""); // cost segment 2
        self.costingNaturalAccount(""); // cost segment 3
        self.costingGeographicLocations(""); // cost segment 4
        self.costingTowers(""); // cost segment 5
        self.costingProjects(""); // cost segment 6
        self.costingProducts(""); // cost segment 7
        self.costingFuture1(""); // cost segment 8
        self.costingFuture2(""); // cost segment 9

        self.costingOffsetCompany(""); // balance segment 1
        self.costingOffsetFunctions(""); // balance segment 2
        self.costingOffsetNaturalAccount(""); // balance segment 3
        self.costingOffsetGeographicLocations(""); // balance segment 4
        self.costingOffsetTowers(""); // balance segment 5
        self.costingOffsetProjects(""); // balance segment 6
        self.costingOffsetProducts(""); // balance segment 7
        self.costingOffsetFuture1(""); // balance segment 8
        self.costingOffsetFuture2(""); // balance segment 9
      }

      // Set costing table
      self.arrayCosting = ko.observableArray();
      self.dataProviderCosting = ko.observableArray();
      
      self.setTableCosting = function(){
        self.getCostingData((result)=>{
          var temp = [];
          $.each(result, function (index, costing) {
            temp.push({
              num: ++index,
              name: "Costing "+costing.elementCostId,
              action: costing.elementCostId,
            });
          });
          self.tableDataCosting(temp);
          self.arrayCosting(temp); 
        });    
      }

      self.dataProviderCosting = new ArrayDataProvider(self.arrayCosting, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });
      
      // Costing Listener 
      var rowDataCosting;
      self.tableDataCosting = ko.observableArray();

      self.selectedRowKeyCosting = ko.observable();
      self.selectedIndexCosting = ko.observable();
      self.tableSelectionListenerCosting = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if (currentRow) {
          self.selectedRowKeyCosting(currentRow['rowKey']);
          self.selectedIndexCosting(currentRow['rowIndex'])
          var thisRow = self.selectedIndexCosting();
          rowDataCosting = self.tableDataCosting()[thisRow];
          console.log(thisRow);
          console.log(rowDataCosting);
        }
      };

      // row action
      self.costingAction = function(event){
        var action = event.target.value;
        if(action == "edit"){
          self.editCosting(rowDataCosting.action);
          self.modalEditCosting();
        }
        if(action == "remove"){
          self.removeCosting(rowDataCosting.action)
        }  
      }.bind(this);

      // create costing
      self.createCosting = function () {
        var valid = _checkValidationGroupCosting("create-costing-tracker");

        if (valid) {
          var payload = {
            "effectiveStartDate": dateConverter(self.costingEffectiveStartDate()),
            "effectiveEndDate": (self.costingEffectiveEndDate())? dateConverter(self.costingEffectiveEndDate()) : "",
            "transferToGLFlag": self.costingGeneralLedger()[0],
            "costableType": self.costingCostType(),
            "gradeId": "0",
            "jobId": "0",
            "locationId": "0",
            "organizationId": "0",
            "payrollId": "0",
            "peopleGroupId": "0",
            "positionId": "0",
            "businessGroupId": "0",
            "elementTypeId": self.editElementID(),
            "costSegment1": self.costingCompany(),
            "costSegment2": self.costingFunctions(),
            "costSegment3": self.costingNaturalAccount(),
            "costSegment4": self.costingGeographicLocations(),
            "costSegment5": self.costingTowers(),
            "costSegment6": self.costingProjects(),
            "costSegment7": self.costingProducts(),
            "costSegment8": self.costingFuture1(),
            "costSegment9": self.costingFuture2(),
            "costSegment10": "0",
            "balanceSegment1": self.costingOffsetCompany(),
            "balanceSegment2": self.costingOffsetFunctions(),
            "balanceSegment3": self.costingOffsetNaturalAccount(),
            "balanceSegment4": self.costingOffsetGeographicLocations(),
            "balanceSegment5": self.costingOffsetTowers(),
            "balanceSegment6": self.costingOffsetProjects(),
            "balanceSegment7": self.costingOffsetProducts(),
            "balanceSegment8": self.costingOffsetFuture1(),
            "balanceSegment9": self.costingOffsetFuture2(),
            "balanceSegment10": "0",
          }

          var success= function(data){
            clearCosting();
            self.cancelModalCreateCosting();
            self.notificationMessage('success',['Costing is created.']);
            self.setTableCosting();
          }
  
          var fail= function(){
            self.cancelModalCreateCosting();
            self.notificationMessage('error',['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/insertElementCosting";
          services.addGeneric(url, payload).then(success, fail);
        }
      }

      // EDIT COSTING

      // modal
      self.startAnimationListenerEditCosting = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'edit-costing-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.modalEditCosting = function () {
        var popup = document.getElementById('edit-costing-modal');
        popup.open('#btnGo');
      };
      self.cancelModalEditCosting = function () {
        var popup = document.getElementById('edit-costing-modal');
        popup.close();
      };

      self.editCosting = function(id){
        self.getCostingById((result)=>{
          var costing = result[0];
          self.costingID(costing.elementCostId);
          self.costingEffectiveStartDate(dateConverter(costing.effectiveStartDate,true));
          self.costingEffectiveEndDate(dateConverter(costing.effectiveEndDate,true));
          self.costingGeneralLedger([costing.transferToGLFlag]);
          self.costingCostType(costing.costableType);

          self.costingCompany(costing.costSegment1); // cost segment 1
          self.costingFunctions(costing.costSegment2); // cost segment 2
          self.costingNaturalAccount(costing.costSegment3); // cost segment 3
          self.costingGeographicLocations(costing.costSegment4); // cost segment 4
          self.costingTowers(costing.costSegment5); // cost segment 5
          self.costingProjects(costing.costSegment6); // cost segment 6
          self.costingProducts(costing.costSegment7); // cost segment 7
          self.costingFuture1(costing.costSegment8); // cost segment 8
          self.costingFuture2(costing.costSegment9); // cost segment 9
    
          self.costingOffsetCompany(costing.balanceSegment1); // balance segment 1
          self.costingOffsetFunctions(costing.balanceSegment2); // balance segment 2
          self.costingOffsetNaturalAccount(costing.balanceSegment3); // balance segment 3
          self.costingOffsetGeographicLocations(costing.balanceSegment4); // balance segment 4
          self.costingOffsetTowers(costing.balanceSegment5); // balance segment 5
          self.costingOffsetProjects(costing.balanceSegment6); // balance segment 6
          self.costingOffsetProducts(costing.balanceSegment7); // balance segment 7
          self.costingOffsetFuture1(costing.balanceSegment8); // balance segment 8
          self.costingOffsetFuture2(costing.balanceSegment9); // balance segment 9
        },id);
      }

      self.updateCosting = function(id){
        var valid = _checkValidationGroupCosting("edit-costing-tracker");

        if (valid) {
          var payload = {
            "effectiveStartDate": dateConverter(self.costingEffectiveStartDate()),
            "effectiveEndDate": (self.costingEffectiveEndDate())? dateConverter(self.costingEffectiveEndDate()) : "",
            "transferToGLFlag": self.costingGeneralLedger()[0],
            "costableType": self.costingCostType(),
            "gradeId": "0",
            "jobId": "0",
            "locationId": "0",
            "organizationId": "0",
            "payrollId": "0",
            "peopleGroupId": "0",
            "positionId": "0",
            "businessGroupId": "0",
            "elementTypeId": self.editElementID(),
            "costSegment1": self.costingCompany(),
            "costSegment2": self.costingFunctions(),
            "costSegment3": self.costingNaturalAccount(),
            "costSegment4": self.costingGeographicLocations(),
            "costSegment5": self.costingTowers(),
            "costSegment6": self.costingProjects(),
            "costSegment7": self.costingProducts(),
            "costSegment8": self.costingFuture1(),
            "costSegment9": self.costingFuture2(),
            "costSegment10": "0",
            "balanceSegment1": self.costingOffsetCompany(),
            "balanceSegment2": self.costingOffsetFunctions(),
            "balanceSegment3": self.costingOffsetNaturalAccount(),
            "balanceSegment4": self.costingOffsetGeographicLocations(),
            "balanceSegment5": self.costingOffsetTowers(),
            "balanceSegment6": self.costingOffsetProjects(),
            "balanceSegment7": self.costingOffsetProducts(),
            "balanceSegment8": self.costingOffsetFuture1(),
            "balanceSegment9": self.costingOffsetFuture2(),
            "balanceSegment10": "0",
          }

          var success= function(data){
            clearCosting();
            self.cancelModalEditCosting();
            self.notificationMessage('success',['Costing is updated.']);
            self.setTableCosting();
          }

          var fail = function () {
            self.cancelModalEditCosting();
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/updateElementCosting/"+self.costingID();
          services.editGeneric(url,payload).then(success,fail);
        }
      }

      // REMOVE COSTING

      self.removeCosting = async function(id){
        var success= function(data){
          self.notificationMessage('success',['Costing is removed.']);
          self.setTableCosting();
        }

        var fail = function(){
          self.notificationMessage('success',['Costing is removed.']);
          self.setTableCosting();
          // self.notificationMessage('error',['Something went wrong.']);
        }

        var url = "payroll/rest/AppsProRest/DeleteElementCosting/"+id;
        services.deleteGeneric(url).then(success,fail);
      }

      // form validation
      var _checkValidationGroupCosting = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      }         

      /**
       * 
       * ELEMENT: DEFINE FORMULA RESULT
       */

      self.createPage = ko.observable(true);

      // Call API Define Fast Formula

      self.getDFRData = async function (callback, id) {

        var success = function(result){
          if(callback) callback(result);
          console.log("success");
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetAllFormulaResultsByElementId/"+id;
        services.getGeneric(url).then(success,fail);
      }

      self.getDFRDataById = async function (callback, id) {

        var success = function(result){
          if(callback) callback(result);
          console.log("success");
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetFormulaResultById/"+id;
        services.getGeneric(url).then(success,fail);
      }

      self.getFastFormula = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
          console.log("success");
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/FastFormulaRest/GetFastFormula";
        services.getGeneric(url).then(success, fail);
      }

      // Set Define Fast Formula Dropdown 
      self.optionDefineFormulaResult = ko.observableArray();
      self.optionElementDefineFormulaResult = ko.observableArray();
      self.optionElementDefineFormulaResult2 = ko.observableArray();
      self.optionInputValueDefineFormulaResult = ko.observableArray(); 
      self.optionInputValueDefineFormulaResult2 = ko.observableArray(); 
      self.optionLinkType = ko.observableArray([
        { value: 'direct result', label: 'Direct Result ' },
        { value: 'indirect result', label: 'Indirect Result' },
        { value: 'stop', label: 'Stop' },
        { value: 'message', label: 'Message' },
      ]);

      function setDefineFormulaResultDropdowns(){
        self.getFastFormula((result) => {
          var options = [];
          $.each(result, function (index, fastFormula) {
             options.push({ value: fastFormula.formula_id, label: fastFormula.formula_name });
          });
          self.optionDefineFormulaResult(options);
        });
      }

      self.changeListenerLinkType = function(event, current){
        var optionValue = event.detail;
        var id = optionValue.value;

        self.optionElementDefineFormulaResult(null);
        self.optionInputValueDefineFormulaResult(null);

        clearFormulaRules();
        self.getElementData((result) => {
          var options = [];
          var options2 = [];
          $.each(result, function (index, elementDFR) {
            if(id == 'direct result'){
              if(elementDFR.elementTypeId == self.editElementID()){
                options.push({ value: elementDFR.elementTypeId, label: elementDFR.elementName });
                options2.push({ value: elementDFR.elementTypeId+'-'+elementDFR.elementName, label: elementDFR.elementName });
              } 
            }
            if(id == 'indirect result'){
              if(elementDFR.elementTypeId != self.editElementID()){
                options.push({ value: elementDFR.elementTypeId, label: elementDFR.elementName });
                options2.push({ value: elementDFR.elementTypeId+'-'+elementDFR.elementName, label: elementDFR.elementName });
              } 
            }
          });
          self.optionElementDefineFormulaResult(options);
          self.optionElementDefineFormulaResult2(options2);
        });

      }

      self.changeListenerElementDFR = function(event, current){
        var optionValue = event.detail;
        var id = optionValue.value;

        if(typeof id == "string" && id.indexOf("-") >= 0) id = id.split("-")[0];

        self.getInputValues((result) => {
          var options = [];
          var options2 = [];
          $.each(result, function (index, inputValue) {
            options.push({ value: inputValue.inputValueId, label: inputValue.name });
            options2.push({ value: inputValue.inputValueId+'-'+inputValue.name, label: inputValue.name });  
          });
          self.optionInputValueDefineFormulaResult(options);
          self.optionInputValueDefineFormulaResult2(options2);
        },id);
      }

      // Index page

      // set table Define Formula Result
      self.arrayDFR = ko.observableArray([]);
      self.dataProviderDFR = ko.observableArray([]);

      self.setTableDFR = function (id) {
        self.arrayDFR([]);
        self.getDFRData((result) => {
          var temp = [];
          
          $.each(result, function (index, defineFormulaResult) {
            temp.push({
              num: ++index,
              description: defineFormulaResult.description,
              formulaName: defineFormulaResult.formulaName,
              startDate: dateConverter(defineFormulaResult.effectiveStartDate),
              endDate: dateConverter(defineFormulaResult.effectiveEndDate),
              action: defineFormulaResult.elementSetId,
            });
          });

          self.tableDataDFR(result);
          self.arrayDFR(temp);
        },id);
      }

      self.dataProviderDFR = new ArrayDataProvider(self.arrayDFR, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });


      // table index listener
      var rowDataDefineFormulaResult;
      self.tableDataDFR = ko.observableArray([]);

      self.selectedRowKey = ko.observable("");
      self.selectedIndex = ko.observable("");
      self.tableSelectionListenerDFR = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow;
        if (currentRow) {
          self.selectedRowKey(currentRow['rowKey']);
          self.selectedIndex(currentRow['rowIndex'])
          var thisRow = self.selectedIndex();
          rowDataDefineFormulaResult = self.tableDataDFR()[thisRow];
          console.log(thisRow);
          console.log(rowDataDefineFormulaResult);
        }
      };

      // DFR CREATE MODAl
      self.startAnimationListenerCreateDFR = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };

      self.openListenerCreateDFR = function () {
        clearDFR();
        var popup = document.getElementById('create-dfr-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerCreateDFR = function () {
        var popup = document.getElementById('create-dfr-modal');
        popup.close();
        
      };

      // CREATE DEFINE FORMULA
      self.DFRDescription = ko.observable("");
      self.DFRformulaName = ko.observable("");
      self.DFRFastFormula = ko.observable("");
      self.DFRStartDate = ko.observable("");
      self.DFREndDate = ko.observable("");

      function clearDFR(){
        self.DFRDescription("");
        self.DFRformulaName("");
        self.DFRFastFormula("");
        self.DFRStartDate("");
        self.DFREndDate("");
        self.arrayLinkType([]);
      };

      function clearFormulaRules(){
        self.inputValueDefineFormulaResult("");
        self.elementNameDefineFormulaResult("");
      };

      self.createDFR = function () {
        var valid = _checkValidationGroupDFR("create-dfr-tracker");
          if (valid) {
            self.DFRformulaName(self.optionDefineFormulaResult().find(x => x.value === self.DFRFastFormula()).label);

            self.setTableLinkType();
            self.cancelListenerCreateDFR();
            self.createPage(true);
            $("#page-1-dfr").hide();
            $("#page-2-dfr").show();
          }
      }

      self.removeRowDefineFormulaResult = function (id) {
        var success = function (result) {
          self.notificationMessage('success', ['Formula Result is removed.']);
          self.setTableDFR(self.editElementID());
          console.log("success");
        }
        var fail = function () {
          //self.notificationMessage('error', ['Something went wrong.']);
          self.notificationMessage('success', ['Formula Result is removed.']);
          self.setTableDFR(self.editElementID());
          console.log("fail");
        }
        var url = "payroll/rest/AppsProRest/DeleteFormulaResult/" + id;
        services.deleteGeneric(url).then(success, fail);
      }

      self.DFRAction = function (event) {
        var action = event.target.value;
        if (action == "edit") {
          self.createPage(false);
          $("#page-2-dfr").show();
          $("#page-1-dfr").hide();
          clearDFR();

          self.editDFR(rowDataDefineFormulaResult.formulaUsageId);
        }
        if (action == "remove") {
          self.removeRowDefineFormulaResult(rowDataDefineFormulaResult.formulaUsageId);
        }
      };

      //CREATE LINK TYPE

      self.submitDFR = function () {
        var payload = {
          "effectiveStartDate": dateConverter(self.DFRStartDate()),
          "effectiveEndDate": (self.DFREndDate())? dateConverter(self.DFREndDate()) : "",
          "formulaId": self.DFRFastFormula(),
          "elementTypeId": self.editElementID(),
          "businessGroupId": "",
          "description": self.DFRDescription(),
          "formulaResultRules": self.arrayLinkType()
        }

        console.log(payload);
        var success = function (data) {
          self.notificationMessage('success', ['Define Formula Result is created.']);
          $("#page-1-dfr").show();
          $("#page-2-dfr").hide();
          self.setTableDFR(self.editElementID());        
        }
  
        var fail = function () {
          self.notificationMessage('error', ['Something went wrong.']);
        }

        var url = "payroll/rest/AppsProRest/insertFormulaResult";
        services.addGeneric(url, payload).then(success, fail);
      }
        

      /**
       * Create Link Type
       */

      self.linkType = ko.observable("");
      self.elementNameDefineFormulaResult = ko.observable("");
      self.inputValueDefineFormulaResult = ko.observable("");

      // LINK TYPE table values
      self.arrayLinkType = ko.observableArray();
      self.newArrayLinkType = ko.observableArray();
      self.dataProviderLinkType = ko.observableArray();

      self.setTableLinkType = function () {
        self.newArrayLinkType(self.arrayLinkType());
        self.tableDataLinkType(self.arrayLinkType());
      }

      self.dataProviderLinkType = new ArrayDataProvider(self.newArrayLinkType, { keyAttributes: 'name', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // Link Type table row listener
      var rowDataLinkType;
      var thisRowLinkType;
      self.tableDataLinkType = ko.observableArray();

      self.selectedRowKeyLinkType = ko.observable("");
      self.selectedIndexLinkType = ko.observable("");
      self.tableSelectionListenerLinkType = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if (currentRow) {
          self.selectedRowKeyLinkType(currentRow['rowKey']);
          self.selectedIndexLinkType(currentRow['rowIndex'])
          thisRowLinkType = self.selectedIndexLinkType();
          rowDataLinkType = self.tableDataLinkType()[thisRowLinkType];
          console.log(thisRowLinkType);
          console.log(rowDataLinkType);
        }
      }


      // remove Link type table row
      self.removeLinkType = function () {
        var temp = self.arrayLinkType();
        temp.splice(thisRowLinkType, 1);
        self.arrayLinkType(temp);
        self.setTableLinkType();
      }

      // insert Link type table row
      self.insertDefineFormulaResult = function () {
        var valid = _checkValidationGroupDFR("insert-defineFormulaResult-tracker")
        if(valid){
          var elementArr = self.elementNameDefineFormulaResult().split("-");
          var inputValueArr = self.inputValueDefineFormulaResult().split("-");
          var json_data = {
            resultRuleType: self.linkType(),
            elementTypeId: elementArr[0],
            elementName: elementArr[1],
            inputValueId: inputValueArr[0],
            inputValueName: inputValueArr[1],
            messageType: "",
            businessGroupId: "",
            resultName: "",
            effectiveStartDate: dateConverter(self.DFRStartDate()),
            effectiveEndDate: (self.DFREndDate())? dateConverter(self.DFREndDate()) : "",
          }

          
          self.arrayLinkType().push(json_data);
          self.setTableLinkType();
  
          console.log("insert");
          console.log(self.arrayLinkType());
          self.cancelListenerLinkType();
        }
      }      


      // modal
      self.startAnimationListenerLinkType = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'insert-defineFormulaResult-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerLinkType = function () {
        self.linkType(null);
        clearFormulaRules();
        var popup = document.getElementById('insert-defineFormulaResult-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerLinkType = function () {
        var popup = document.getElementById('insert-defineFormulaResult-modal');
        popup.close();
      };

      self.gotoIndexCancel = function () {
        clearDFR();
        $("#page-2-dfr").hide();
        $("#page-1-dfr").fadeIn();
      };


      /**
       * Update Define Formula Result
       */

      self.formulaUsageId = ko.observable();
      self.editDescriptionDFR = ko.observable("asdasdasd");
      self.editFastFormulaDFR = ko.observable();
      self.editEffectiveStartDateDFR = ko.observable();
      self.editEffectiveEndDateDFR = ko.observable();
      self.formulaResultRules = ko.observableArray();

      function clearEditDFR(){
        self.formulaUsageId = ko.observable("");
        self.editDescriptionDFR = ko.observable("");
        self.editFastFormulaDFR = ko.observable("");
        self.editEffectiveStartDateDFR = ko.observable("");
        self.editEffectiveEndDateDFR = ko.observable("");
        self.formulaResultRules = ko.observableArray([]);
      }

      function formatFormulaResultRules(arr){
        var new_array = [];
        $.each(arr, function (index, formulaResultRule) { 
          formulaResultRule.effectiveStartDate = dateConverter(self.editEffectiveStartDateDFR());
          formulaResultRule.effectiveEndDate = dateConverter(self.editEffectiveEndDateDFR());
          new_array.push(formulaResultRule);
        });
        
        return new_array;
      }

      self.editDFR = function(id){
        self.getDFRDataById((result)=>{
          self.formulaUsageId(result.formulaUsageId);
          self.editDescriptionDFR(result.description);
          self.editFastFormulaDFR(result.formulaId);
          self.editEffectiveStartDateDFR(dateConverter(result.effectiveStartDate,true));
          self.editEffectiveEndDateDFR(dateConverter(result.effectiveEndDate,true));
          self.formulaResultRules(formatFormulaResultRules(result.formulaResultRulesBeanList));
          self.tableDataLinkTypeEdit(result.formulaResultRulesBeanList);
        },id);
      }      

      self.updateDFR = function () {
        var valid = _checkValidationGroupDFR("edit-tracker");
        if (valid) {

          var payload = {
            "formulaUsageId": self.formulaUsageId(),
            "effectiveStartDate": dateConverter(self.editEffectiveStartDateDFR()),
            "effectiveEndDate": (self.editEffectiveEndDateDFR())? dateConverter(self.editEffectiveEndDateDFR()) : "",
            "formulaId": self.editFastFormulaDFR(),
            "elementTypeId": self.editElementID(),
            "businessGroupId": "",
            "description": self.editDescriptionDFR(),
            "formulaResultRules": formatFormulaResultRules(self.formulaResultRules())
          }

          var success = function (result) {
            self.notificationMessage('success', ['Define Formula is updated.']);
            console.log("succees");
            $("#page-1-dfr").show();
            $("#page-2-dfr").hide();
            
            clearDFR();
            self.setTableDFR(self.editElementID());

          }

          var fail = function () {
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/UpdateFormulaResult";
          services.editGeneric(url, payload).then(success, fail);
        }

      }

      self.insertLinkType = function(){
        var valid = _checkValidationGroupDFR("edit-defineFormulaResult-tracker");
        if (valid) {
          var payload = {
            "effectiveStartDate": dateConverter(self.editEffectiveStartDateDFR()),
            "effectiveEndDate": (self.editEffectiveEndDateDFR())? dateConverter(self.editEffectiveEndDateDFR()) : "",
            "formulaUsageId": self.formulaUsageId(),
            "elementTypeId": self.editElementLinkType(),
            "businessGroupId": "",
            "resultName": "",
            "resultRuleType": self.editLinkType(),
            "messageType": "",
            "inputValueId": self.editInputValueLinkType()
          }
  
          var success = function (result) {
            self.cancelListenerLinkTypeEdit();
            self.resetLinkTypeTable(self.formulaUsageId());
          }

          var fail = function () {
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/insertFormulaResultRule";
          services.addGeneric(url, payload).then(success, fail);
        }
      }

      self.deleteLinkType = function(){
        var success = function (result) {
         console.log('success');
        }
        var fail = function () {
          self.resetLinkTypeTable(self.formulaUsageId());
        }
        var url = "payroll/rest/AppsProRest/DeleteFormulaResultRule/"+rowDataLinkTypeEdit.formulaResultRuleId;
        services.deleteGeneric(url).then(success, fail);
      }

      // edit table linktype
      self.resetLinkTypeTable = function(id){
        self.formulaResultRules([]);
        self.getDFRDataById((result)=>{
          self.formulaResultRules(formatFormulaResultRules(result.formulaResultRulesBeanList));
          self.tableDataLinkTypeEdit(result.formulaResultRulesBeanList);
        },id);
      } 

      self.dataProviderLinkTypeEdit = ko.observableArray();
      self.dataProviderLinkTypeEdit = new ArrayDataProvider(self.formulaResultRules, { keyAttributes: 'name', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // edit table - row listener
      var rowDataLinkTypeEdit;
      self.tableDataLinkTypeEdit = ko.observableArray();

      self.selectedRowKeyLinkTypeEdit = ko.observable("");
      self.selectedIndexLinkTypeEdit = ko.observable("");
      self.tableSelectionListenerLinkTypeEdit = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow;
        if (currentRow) {
          self.selectedRowKeyLinkTypeEdit(currentRow['rowKey']);
          self.selectedIndexLinkTypeEdit(currentRow['rowIndex'])
          var thisRowLinkTypeEdit = self.selectedIndexLinkTypeEdit();
          rowDataLinkTypeEdit = self.tableDataLinkTypeEdit()[thisRowLinkTypeEdit];
          console.log(thisRowLinkTypeEdit);
          console.log(rowDataLinkTypeEdit);
        }
      }

      // edit modal linktype
      self.editLinkType = ko.observable();
      self.editElementLinkType = ko.observable();
      self.editInputValueLinkType = ko.observable();

      function clearLinkTypeEdit(){
        self.editLinkType("");
        self.editElementLinkType("");
        self.editInputValueLinkType("");
      }

      
      self.startAnimationListenerLinkTypeEdit = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'edit-defineFormulaResult-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerLinkTypeEdit = function () {
        clearLinkTypeEdit();
        var popup = document.getElementById('edit-defineFormulaResult-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerLinkTypeEdit = function () {
        var popup = document.getElementById('edit-defineFormulaResult-modal');
        popup.close();
      }; 

      //END DFR

      var _checkValidationGroupDFR = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };


      //--- OTHER
      self.gotoPage1 = function () {
        $("#page-2").hide();
        $("#page-1").fadeIn();
        self.selectedItem("");
      }.bind(this);


      //--- SWITCHER
      self.selectedItem = ko.observable();
      self.currentEdge = ko.observable('top');
      self.valueChangedHandler = function (event) {
        var value = event.detail.value;
        var previousValue = event.detail.previousValue;
        var demoContianer = document.getElementById('element-tabs-container');
        demoContianer.className = demoContianer.className.replace('demo-edge-' + previousValue, 'demo-edge-' + value);
      };

      self.connected = () => {
        app.collapseSideMenu();
        accUtils.announce('Dashboard page loaded.', 'assertive');
        document.title = "Dashboard";
        // Implement further logic if needed
        //$(".oj-navigationlist-item-element").removeClass("oj-selected").addClass("oj-default");

        self.setTableElement();
	      self.setTableCosting();

        setElementDropdowns();
        setEligibiliyDropdowns();
        setCostingDropdowns();
        setDefineFormulaResultDropdowns();
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = () => {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = () => {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return IndexElementViewModel;
  }
);

