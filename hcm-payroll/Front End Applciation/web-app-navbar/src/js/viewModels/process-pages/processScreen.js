/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your customer ViewModel code goes here
 */
define(['ojs/ojcore', 'appController', 'text!data/showData.json', 'text!data/element.json', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation',
    'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
    'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojrouter', 'ojs/ojlistview'
],
    function (oj, app, processResultValuesData, elementData, services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils) {

        function processScreenViewModel() {
            var self = this;

            self.retroCalculation = function () {
                app.router.go('retroCalculation')
            };
            self.payrollRun = function () {
                app.router.go('payrollRun');
            };
            self.quickPay = function () {
                app.router.go('quickPay');
            };
            self.calculatePayment = function () {
                app.router.go('calculatePayment');
            };
            self.costingProcess = function () {
                app.router.go('costingProcess');
            };
            self.transferGL = function () {
                app.router.go('transferGL');
            };
            self.rollBack = function () {
              app.router.go('rollBack');
            };
            ///////////////////////////////////
            this.controlDetails = function () {

            };



            self.connected = function () {

                // Implement further logic if needed
                //$(".oj-navigationlist-item-element").removeClass("oj-selected").addClass("oj-default");
            };

            /**
             * Optional ViewModel method invoked after the View is disconnected from the DOM.
             */
            self.disconnected = function () {
                // Implement if needed
            };

            self.transitionCompleted = function () {
                // Implement if needed
            };
        }


        return processScreenViewModel;
    }
);