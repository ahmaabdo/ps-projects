/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['accUtils', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojtimezonedata', 'ojs/ojconverterutils-i18n', 'ojs/ojknockout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox'
],
  function (accUtils, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {
    function payrollRunViewModel() {
      var self = this;


      /**Call API**/

      self.getPeriodName = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/PeriodlName";
        await services.getGeneric(url).then(success, fail);
      }

      self.getPayrollName = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/PeyrollName/AWA Pyroll";
        await services.getGeneric(url).then(success, fail);
      }

      self.getObjectGroupData = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetObjectGroup";
        await services.getGeneric(url).then(success, fail);
      }

      self.getAssignementGroup = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
          console.log("<<<");
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/AssignmentGroup";
        await services.getGeneric(url).then(success, fail);
      }



      /**
       * Date Formatting
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      self.back = function () {
        app.router.go('processPages')
      };

      self.payrollFlow = ko.observable();
      self.overridingDate = ko.observable();
      self.payrollValue = ko.observable();
      self.payrollPeriod = ko.observable();
      self.processDate = ko.observable();
      self.dateEarned = ko.observable();
      self.consolidationGroup = ko.observable();
      self.runType = ko.observable();
      self.processConfigurationGroup = ko.observable();
      self.elementGroup = ko.observable();
      self.optionPayrollPeriod = ko.observableArray();
      self.optionPayrollValue = ko.observableArray();
      self.assignementGroup = ko.observable();
      self.groups = ko.observableArray();
      self.optionAssignementGroup = ko.observableArray();
      self.loadingPayrollValue = ko.observable(false);
      self.loadingPayrollPeriod = ko.observable(false);
      self.loadingGroups = ko.observable(false);

      function setDropdown(){
        self.loadingPayrollValue(true);
        self.loadingPayrollPeriod(true);
        self.loadingGroups(true);

        //payroll dropdown//
        self.getPayrollName((result) => {
          var options = [];
          $.each(result, function (index, payrollName) {
            options.push({ value: payrollName.PAYROLL_NAME, label: payrollName.PAYROLL_NAME });
          });
          self.optionPayrollValue(options);
          self.loadingPayrollValue(false);
        });

        //period name dropdown//
        self.getPeriodName((result) => {
          var options = [];
          $.each(result, function (index, periodName) {
            options.push({ value: periodName.TIME_PERIOD_ID, label: periodName.PERIOD_NAME });
          });
          self.optionPayrollPeriod(options);
          self.loadingPayrollPeriod(false);
        });
  
        self.getObjectGroupData((result) => {
          var options = [];
          $.each(result, function (index, groups) {
            options.push({ value: groups.elementSetName, label: groups.elementSetName });
          });
          self.groups(options);
          self.loadingGroups(false);
        });
        
      }

      self.getAssignementGroup((result) => {
        var options = [];
        $.each(result, function (index, assignementGroup) {
          options.push({ value: assignementGroup.OBJECT_GROUP_ID, label: assignementGroup.BASE_OBJECT_GROUP_NAME });
        });
        self.optionAssignementGroup(options);
      });
          
        self.gotoIndex = function(){
          oj.Router.rootInstance.go("process");
        }

        var _checkValidationGroupPayrollRun = function (id) {
          var tracker = document.getElementById(id);
          if (tracker.valid === "valid") {
            return true;
          }
          else {
            // show messages on all the components
            // that have messages hidden.
            tracker.showMessages();
            tracker.focusOn("@firstInvalidShown");
            return false;
          }
        };
  
        self.createPayrollRun = function(){
          // var valid = _checkValidationGroupPayrollRun("create-tracker");
          var valid = _checkValidationGroupPayrollRun("create-tracker-2");
          if(valid){
            var jsondata = {
              "processDate": self.processDate(),
              "payrollValue": self.payrollValue(),
              "payrollPeriod": self.payrollPeriod(),
              "dateEarned": self.dateEarned(),
              "processConfigurationGroup": self.processConfigurationGroup(),
              "consolidationGroup": self.consolidationGroup(),
              "processConfigurationGroup": self.processConfigurationGroup(),
              "payrollFlow": self.payrollFlow(),
            }
            console.log(jsondata);
          } 
        }


      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = () => {
        accUtils.announce('Dashboard page loaded.', 'assertive');
        document.title = "Payroll Run";

        setDropdown();
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = () => {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = () => {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return payrollRunViewModel;
  }
);

