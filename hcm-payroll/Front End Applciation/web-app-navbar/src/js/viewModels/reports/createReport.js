/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your about ViewModel code goes here
 */
define(['accUtils', 'ojs/ojcore', 'ojs/ojconverter-datetime', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojrouter'],
  function (accUtils, oj, DateTimeConverter, services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils) {

    function CreateReportViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      self.notifMessage = ko.observableArray();
      self.messagesDataprovider = ko.observableArray();
      self.notificationMessage = function (type, messages) {
        var msg = [];
        switch (type) {
          case 'error':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'error',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'warning':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'warning',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'success':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
          default:
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
        }

        self.notifMessage(msg);
      }
      self.messagesDataprovider = new ArrayDataProvider(self.notifMessage);

      /**
       * Call APIs
       */

      self.getoptionType = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
          console.log("success");
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/Lookup";
        services.getGeneric(url).then(success, fail);
      }

      /**
       * Date Formatting
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      self.advanceDisable = ko.observable();

      self.gotoIndex = function (json_data = {}) {
        oj.Router.rootInstance.store(json_data);
        oj.Router.rootInstance.go("indexReport");
      }

      /**
       * Create Report
       */
      self.reportName = ko.observable();
      self.reportType = ko.observable();
      self.parameterCounter = ko.observable(5);
      self.reportParameter1 = ko.observable("");
      self.reportParameter2 = ko.observable("");
      self.reportParameter3 = ko.observable("");
      self.reportParameter4 = ko.observable("");
      self.reportParameter5 = ko.observable("");
      self.reportParameter6 = ko.observable("");
      self.reportParameter7 = ko.observable("");
      self.reportParameter8 = ko.observable("");
      self.reportParameter9 = ko.observable("");
      self.reportParameter10 = ko.observable("");
      self.reportParameter11 = ko.observable("");
      self.reportParameter12 = ko.observable("");
      self.reportParameter13 = ko.observable("");
      self.reportParameter14 = ko.observable("");
      self.reportParameter15 = ko.observable("");
      self.reportParameter16 = ko.observable("");
      self.reportParameter17 = ko.observable("");
      self.reportParameter18 = ko.observable("");
      self.reportParameter19 = ko.observable("");
      self.reportParameter20 = ko.observable("");
      self.optionType = ko.observableArray();

      /**
       * Set dropdown
       */
      function setReportDropdowns() {

        self.getoptionType((result) => {
          var options = [];
          $.each(result, function (index, direction) {
            if (direction.name == "Output Formats") options.push({ value: direction.code, label: direction.valueEn });
          });
          self.optionType(options);
        });
      }


      var _checkValidation = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }

      };

      var userIdCount = 2;
      self.users = ko.observableArray([{ label: "Parameter 1", value: ''}]);
      self.removeUser = function(event, current, bindingContext) {
        self.users.remove(current.data);
        var temp = [];
        $.each(self.users(), function (index, user) {
          user.label = "Parameter "+(++index);
          temp.push(user);
        });
        self.users([]);
        console.log(self.users());
        self.users(temp);
        userIdCount--;

      };
      
      self.addUser = function () {
        var temp = self.users();
        temp.push({ label: "Parameter " + userIdCount++, value: ''});
        self.users(temp);
      };

      self.updateList = ko.computed(function() {
        return self.users();
      });

      self.createReport = function () {
        var valid = _checkValidation("create-tracker");

        if (valid) {
          var temp =self.users();
          var payload = {
            "reportName": self.reportName(),
            "reportTypeVal": self.reportType(),
            "parameter1": (temp[0])? temp[0].value : "",
            "parameter2": (temp[1])? temp[1].value : "",
            "parameter3": (temp[2])? temp[2].value : "",
            "parameter4": (temp[3])? temp[3].value : "",
            "parameter5": (temp[4])? temp[4].value : "",
            "parameter6": (temp[5])? temp[5].value : "",
            "parameter7": (temp[6])? temp[6].value : "",
            "parameter8": (temp[7])? temp[7].value : "",
            "parameter9": (temp[8])? temp[8].value : "",
            "parameter10": (temp[9])? temp[9].value : "",
            "parameter11": (temp[10])? temp[10].value : "",
            "parameter12": (temp[11])? temp[11].value : "",
            "parameter13": (temp[12])? temp[12].value : "",
            "parameter14": (temp[13])? temp[13].value : "",
            "parameter15": (temp[14])? temp[14].value : "",
            "parameter16": (temp[15])? temp[15].value : "",
            "parameter17": (temp[16])? temp[16].value : "",
            "parameter18": (temp[17])? temp[17].value : "",
            "parameter19": (temp[18])? temp[18].value : "",
            "parameter20": (temp[19])? temp[19].value : "",
          }

          var success = function (data) {
            self.gotoIndex({ action: 'create', success: true, message: 'Report is created.' });
            console.log(payload);

          }
          var fail = function () {
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/report/insertBIReportInfo";
          services.addGeneric(url, payload).then(success, fail);

        }
      }


      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {
        accUtils.announce('Report page loaded.');
        document.title = "Report";
        // Implement further logic if needed
        setReportDropdowns();


      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return CreateReportViewModel;
  }
);
