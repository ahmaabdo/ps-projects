/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['accUtils', 'text!data/objectGroup.json', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
'ojs/ojconverter-datetime',
'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher'
],
 function(accUtils,  objectGroupData, services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {

    function EditReportViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      this.notifMessage = ko.observableArray();
      this.messagesDataprovider = ko.observableArray();
      this.notificationMessage = function(type, messages){
        var msg = [];
        switch (type) {
          case 'error':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'error',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;

          case 'warning':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'warning',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
              
          case 'success':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
          default:
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
        }

        this.notifMessage(msg);
      }
      this.messagesDataprovider = new ArrayDataProvider(self.notifMessage);

      /**
       * Call API
       */

      self.getoptionType = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
          console.log("success");
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/Lookup";
        services.getGeneric(url).then(success, fail);
      }


      /**
       * Date Formatting
       */

      function dateConverter(date, iso=false){
        var d = new Date(date);
        var new_date;

        if(iso){
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        }else{
          new_date = ('0' + d.getDate()).slice(-2) +"-"+ (('0' + (d.getMonth()+1)).slice(-2) ) + '-'+ d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
      {
        pattern: "dd-MM-yyyy"
      }));
      
      self.advanceDisable = ko.observable();

      self.gotoIndex = function(json_data={}){
        oj.Router.rootInstance.store(json_data);
        oj.Router.rootInstance.go("indexReport");
      }.bind(this);

      /**
       * Update Define Formula Result
       */
      
      self.editID = ko.observable();
      self.editReportName = ko.observable();
      self.editReportType = ko.observable();
      self.Parameter1 = ko.observable();
      self.Parameter2 = ko.observable();
      self.Parameter3 = ko.observable();
      self.Parameter4 = ko.observable();
      self.Parameter5 = ko.observable();
      self.Parameter6 = ko.observable();
      self.Parameter7 = ko.observable();
      self.Parameter8 = ko.observable();
      self.Parameter9 = ko.observable();
      self.Parameter10 = ko.observable();
      self.Parameter11 = ko.observable();
      self.Parameter12 = ko.observable();
      self.Parameter13 = ko.observable();
      self.Parameter14 = ko.observable();
      self.Parameter15 = ko.observable();
      self.Parameter16 = ko.observable();
      self.Parameter17 = ko.observable();
      self.Parameter18 = ko.observable();
      self.Parameter19 = ko.observable();
      self.Parameter20 = ko.observable();
      self.optionType = ko.observableArray();
      /**
       * Set dropdown
       */
      function setReportDropdowns(){

        self.getoptionType((result) => {
          var options = [];
          $.each(result, function (index, direction) {
            if(direction.name == "Output Formats") options.push({ value: direction.code, label: direction.valueEn});
          });
          self.optionType(options);
        });
      }
   
      self.setDataReport = function(updateList){
        var temp = [];
        console.log(updateList);
        self.editID(updateList.id);
        self.editReportName(updateList.reportName);
        self.editReportType(updateList.reportTypeVal);
        if(updateList.parameter1) temp.push ({ label: "Parameter 1", value: updateList.parameter1});
        if(updateList.parameter2) temp.push ({ label: "Parameter 2", value: updateList.parameter2});
        if(updateList.parameter3) temp.push ({ label: "Parameter 3", value: updateList.parameter3});
        if(updateList.parameter4) temp.push ({ label: "Parameter 4", value: updateList.parameter4});
        if(updateList.parameter5) temp.push ({ label: "Parameter 5", value: updateList.parameter5});
        if(updateList.parameter6) temp.push ({ label: "Parameter 6", value: updateList.parameter6});
        if(updateList.parameter7) temp.push ({ label: "Parameter 7", value: updateList.parameter7});
        if(updateList.parameter8) temp.push ({ label: "Parameter 8", value: updateList.parameter8});
        if(updateList.parameter9) temp.push ({ label: "Parameter 9", value: updateList.parameter9});
        if(updateList.parameter10) temp.push ({ label: "Parameter 10", value: updateList.parameter10});
        if(updateList.parameter11) temp.push ({ label: "Parameter 11", value: updateList.parameter11});
        if(updateList.parameter12) temp.push ({ label: "Parameter 12", value: updateList.parameter12});
        if(updateList.parameter13) temp.push ({ label: "Parameter 13", value: updateList.parameter13});
        if(updateList.parameter14) temp.push ({ label: "Parameter 14", value: updateList.parameter14});
        if(updateList.parameter15) temp.push ({ label: "Parameter 15", value: updateList.parameter15});
        if(updateList.parameter16) temp.push ({ label: "Parameter 16", value: updateList.parameter16});
        if(updateList.parameter17) temp.push ({ label: "Parameter 17", value: updateList.parameter17});
        if(updateList.parameter18) temp.push ({ label: "Parameter 18", value: updateList.parameter18});
        if(updateList.parameter19) temp.push ({ label: "Parameter 19", value: updateList.parameter19});
        if(updateList.parameter20) temp.push ({ label: "Parameter 20", value: updateList.parameter20});

        self.users(temp);
        userIdCount = self.users().length;
        
        console.log(userIdCount);
      }
      
      self.updateReport = function(){
        var valid = _checkValidationGroupCreateReport();
        var temp =self.users();
        if(valid){
          var payload = {
            "id": self.editID(),
            "reportName": self.editReportName(),
            "reportTypeVal": self.editReportType(),
            "parameter1": (temp[0])? temp[0].value : "",
            "parameter2": (temp[1])? temp[1].value : "",
            "parameter3": (temp[2])? temp[2].value : "",
            "parameter4": (temp[3])? temp[3].value : "",
            "parameter5": (temp[4])? temp[4].value : "",
            "parameter6": (temp[5])? temp[5].value : "",
            "parameter7": (temp[6])? temp[6].value : "",
            "parameter8": (temp[7])? temp[7].value : "",
            "parameter9": (temp[8])? temp[8].value : "",
            "parameter10": (temp[9])? temp[9].value : "",
            "parameter11": (temp[10])? temp[10].value : "",
            "parameter12": (temp[11])? temp[11].value : "",
            "parameter13": (temp[12])? temp[12].value : "",
            "parameter14": (temp[13])? temp[13].value : "",
            "parameter15": (temp[14])? temp[14].value : "",
            "parameter16": (temp[15])? temp[15].value : "",
            "parameter17": (temp[16])? temp[16].value : "",
            "parameter18": (temp[17])? temp[17].value : "",
            "parameter19": (temp[18])? temp[18].value : "",
            "parameter20": (temp[19])? temp[19].value : "",

          }
          var success = function (data) {
            self.gotoIndex({ action: 'create', success: true, message: 'Report is updated.' });
          }
  
          var fail = function(){
            console.log("fail");
            self.notificationMessage('error',['Something went wrong.']);
          }
  
          var url = "payroll/rest/report/UpdateBIReportInfo";
          services.editGeneric(url, payload).then(success,fail);
        }
      }

      var _checkValidationGroupCreateReport = function () {
        var tracker = document.getElementById("edit-tracker");
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.users = ko.observableArray([{ label: "Parameter 1", value: ''}]);
      var userIdCount = 2;
      self.removeUser = function(event, current, bindingContext) {
        self.users.remove(current.data);
        var temp = [];
        $.each(self.users(), function (index, user) {
          user.label = "Parameter "+(++index);
          temp.push(user);
        });
        self.users([]);
        self.users(temp);
        userIdCount--;

      };
 
      self.addUser = function () {
        var temp = self.users();
        userIdCount = self.users().length;
        if(self.users().length == userIdCount)
        temp.push({ label: "Parameter " + ++userIdCount, value: ''});
        self.users(temp);
      };

      self.updateList = ko.computed(function() {
        return self.users();
      });


      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function() {

        accUtils.announce('Report page loaded.');
        document.title = "Report";
        // Implement further logic if needed
        setReportDropdowns();     
        updateList = oj.Router.rootInstance.retrieve();
        self.setDataReport(updateList);

      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return EditReportViewModel;
  }
);
