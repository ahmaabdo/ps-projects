/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your about ViewModel code goes here
 */
define(['accUtils','ojs/ojcore', 'ojs/ojconverter-datetime', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation',
'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojrouter'],
 function(accUtils, oj,DateTimeConverter, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils) {

    function IndexReportViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      self.notifMessage = ko.observableArray();
      self.messagesDataprovider = ko.observableArray();
      self.notificationMessage = function (type, messages) {
        var msg = [];
        switch (type) {
          case 'error':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'error',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'warning':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'warning',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'success':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
          default:
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
        }

        self.notifMessage(msg);
      }
      self.messagesDataprovider = new ArrayDataProvider(self.notifMessage);
       ////navigator
       self.back = function(){
        app.router.go('homeSetup')
      };
      self.manageElement = function(){
        app.router.go('indexElement')
      };
      self.manageObjectGroup = function(){
        app.router.go('indexObjectGroup')
      };
      self.fastFormula = function(){
        app.router.go('indexFastFormula')
      };
      self.report = function(){
        app.router.go('indexReport')
      };
      /////////////////
      /**
       * API Call
       */

      self.getReportData = function(callback){
        var success = function(result){
          if(callback) callback(result);
          console.log("success");
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/report/GetAllBIReportsInfo";
        services.getGeneric(url).then(success,fail);
      }

      self.removeRowReport = function(id){

        var success = function(result){
          self.notificationMessage('success',['FastFormula is removed.']);
          console.log("Success");
          
        }
        var fail = function(){
          self.notificationMessage('success', ['Report is removed.']);
          console.log("Success");
          self.setTableReport();
        }

        var url = "payroll/rest/report/DeleteBIReportInfoById/" +id;
        services.deleteGeneric(url).then(success,fail);
      }

      /**
       * Date Formatting
       */

      function dateConverter(date, iso=false){
        var d = new Date(date);
        var new_date;

        if(iso){
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        }else{
          new_date = ('0' + d.getDate()).slice(-2) +"-"+ (('0' + (d.getMonth()+1)).slice(-2) ) + '-'+ d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
      {
        pattern: "dd-MM-yyyy"
      }));

      /**
       * Search
       */
      self.advanceDisable = ko.observable();
      
      // keyword search
      self.keyword = ko.observable();
      self.searchKeyword = function(){
        alert("search keyword statement");
      }.bind(this);

      // advance search

      self.searchReportName = ko.observable();
      self.searchReportType = ko.observable();
      self.searchReportParameter1 = ko.observable();
      self.searchReportParameter2 = ko.observable();
      self.searchReportParameter3 = ko.observable();
      self.searchReportParameter4 = ko.observable();

      self.searchAdvance = function(){
        alert("search advance statement");
      }.bind(this);


      self.openListenerReport = function () {
        oj.Router.rootInstance.go("createReport");
      };
      self.cancelListenerReport = function () {
        var popup = document.getElementById('createReport');
        popup.close();
      };
   
      /**
       * table
       */

      self.gotoEditReport = function(json_data){
        oj.Router.rootInstance.store(json_data);
        oj.Router.rootInstance.go("editReport");
      }

      // table listener
      var rowDataReport;
      self.tableDataReport = ko.observableArray(); 
      self.selectedRowKey = ko.observable();
      self.selectedIndex = ko.observable();
      self.tableSelectionListenerReport = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow;
        if(currentRow){
          self.selectedRowKey(currentRow['rowKey']);
          self.selectedIndex(currentRow['rowIndex'])
          var thisRow = self.selectedIndex();
          rowDataReport = self.tableDataReport()[thisRow]
          console.log(thisRow);
          console.log(rowDataReport);
        }
      };

      // set table values
      self.arrayReport = ko.observableArray();
      self.dataProviderReport = ko.observableArray();
      
      self.setTableReport = function(){
        self.getReportData((result)=>{
          var temp = [];
          $.each(result, function (index, report) {
            temp.push({
              num: ++index,
              name: report.reportName,
              type: report.reportTypeVal,
              action: report.id,
            });
          });
          self.tableDataReport(result);
          self.arrayReport(temp); 
        });    
      }

      self.dataProviderReport = new ArrayDataProvider(self.arrayReport, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });
      
      self.elementAction = function(){
        var action = event.target.value;
        if(action == "edit") self.gotoEditReport(rowDataReport);
        if(action == "remove") {
          self.removeRowReport(rowDataReport.id);
          self.setTableReport();
        };
      }

      self.connected = function() {
        app.collapseSideMenu();
        accUtils.announce('Report page loaded.');
        document.title = "Report";
        // Implement further logic if needed

        updateList = oj.Router.rootInstance.retrieve();
        if(typeof updateList != "undefined" && updateList.success){
          
          self.notificationMessage('success',[updateList.message]);
          oj.Router.rootInstance.store({});
        }

        self.setTableReport();
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return IndexReportViewModel;
  }
);
