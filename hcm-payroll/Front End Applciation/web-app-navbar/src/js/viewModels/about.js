/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation',
'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojrouter'],
 function( oj,services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils) {

    function AboutViewModel() {
      var self = this;

      // this.createElement = function () {
      //   var valid = _checkValidationGroupCreateElement();
      //   if (valid) {
      //     // submit the form would go here
      //     // eslint-disable-next-line no-alert
      //     var data = {
      //       "elementName": this.createElementName(),
      //       "elementType": this.createElementType(),
      //       "processingType": this.createProcessType(),
      //       "priority": this.createEffectiveStartDate(),
      //       "effectiveStartDate": this.createEffectiveEndDate(),
      //       "effectiveDate": this.createPriority(),
      //       "currency": this.createCurrency()
      //     }
      //     console.log(data);
      //     alert('everything is valid; submit the form');
      //   }
      // }.bind(this);

      // var _checkValidationGroupCreateElement = function () {
      //   var tracker = document.getElementById("tracker");
      //   if (tracker.valid === "valid") {
      //     return true;
      //   }
      //   else {
      //     // show messages on all the components
      //     // that have messages hidden.
      //     tracker.showMessages();
      //     tracker.focusOn("@firstInvalidShown");
      //     return false;
      //   }
      // };

      // this.createElementName = ko.observable();
      // this.createElementType = ko.observable();
      // this.createProcessType = ko.observable();
      // this.createEffectiveStartDate = ko.observable();
      // this.createEffectiveEndDate = ko.observable();
      // this.createPriority = ko.observable();
      // this.createCurrency = ko.observable();

      // this.startAnimationListener = function (event) {
      //   var ui = event.detail;
      //   if (event.target.id !== 'popup1') { return; }

      //   if (ui.action === 'open') {
      //     event.preventDefault();
      //     var options = { direction: 'top' };
      //     AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
      //   } else if (ui.action === 'close') {
      //     event.preventDefault();
      //     ui.endCallback();
      //   }
      // };
      // this.openListener = function () {
      //   var popup = document.getElementById('popup1');
      //   popup.open('#btnGo');
      // };
      // this.cancelListener = function () {
      //   var popup = document.getElementById('popup1');
      //   popup.close();
      // };

      // //*--- MODAL

      // // User presses the submit button
      // // No need to revalidate each field since
      // // field level validation occurs on blur and Enter.
      // this.createElement = function () {
      //   var valid = _checkValidationGroupCreateElement();
      //   if (valid) {
      //     // submit the form would go here
      //     // eslint-disable-next-line no-alert
      //     var data = {
      //       "elementName": this.createElementName(),
      //       "elementType": this.createElementType(),
      //       "processingType": this.createProcessType(),
      //       "priority": this.createEffectiveStartDate(),
      //       "inputValue": this.createEffectiveEndDate(),
      //       "effectiveDate": this.createPriority(),
      //       "currency": this.createCurrency()
      //     }
      //     console.log(data);
      //     alert('everything is valid; submit the form');
      //   }
      // }.bind(this);

      // var _checkValidationGroupCreateElement = function () {
      //   var tracker = document.getElementById("tracker");
      //   if (tracker.valid === "valid") {
      //     return true;
      //   }
      //   else {
      //     // show messages on all the components
      //     // that have messages hidden.
      //     tracker.showMessages();
      //     tracker.focusOn("@firstInvalidShown");
      //     return false;
      //   }
      // };

      self.elementName = ko.observable();
      self.elementType = ko.observable();
      self.processingType = ko.observable();
      self.effectiveDate = ko.observable();
      self.inputValue = ko.observable();
      self.priority = ko.observable();
      self.currency = ko.observable();
    




      self.submit = function() {
        
        var paylod={

          "elementName": self.elementName(),
            "elementType": self.elementType(),
            "processingType": self.processingType(),
            "priority": self.effectiveDate(),
            "inputValue": self.inputValue(),
            "effectiveDate": self.priority(),
            "currency": self.currency()




        }
        
        var success= function(){
    
          console.log(data);
          console.log("success")
        }

        var fail= function(){
          alert("data repeated");

        }
        
        var url="defineElementForm"
        services.addGeneric(url,paylod).then(success,fail)
      }

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function() {
        
        // Implement further logic if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return AboutViewModel;
  }
);
