/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['accUtils', 'text!data/objectGroup.json', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages'
],
  function (accUtils, objectGroupData, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {

    function IndexObjectGroupViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      self.notifMessage = ko.observableArray();
      self.messagesDataprovider = ko.observableArray();
      self.notificationMessage = function (type, messages) {
        var msg = [];
        switch (type) {
          case 'error':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'error',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'warning':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'warning',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'success':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
          default:
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
        }

        self.notifMessage(msg);
      }
      self.messagesDataprovider = new ArrayDataProvider(self.notifMessage);

      ////navigator

      self.back = function () {
        app.router.go('homeSetup')
      };
      self.manageElement = function () {
        app.router.go('indexElement')
      };
      self.manageObjectGroup = function () {
        app.router.go('indexObjectGroup')
      };
      self.fastFormula = function () {
        app.router.go('indexFastFormula')
      };
      self.report = function(){
        app.router.go('indexReport')
      };
      /////////////////

      /**
       * API Call
       */

      self.getObjectGroupData = function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetObjectGroup";
        services.getGeneric(url).then(success, fail);
      }

      /**
       * Date Formatting
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      /**
       * Search
       */
      self.advanceDisable = ko.observable();

      // keyword search
      self.keyword = ko.observable();
      self.searchKeyword = function () {
        alert("search keyword statement");
      }.bind(this);

      // advance search
      self.searchName = ko.observable();
      self.searchType = ko.observable();
      self.searchStartDate = ko.observable();
      self.searchEndDate = ko.observable();

      self.searchAdvance = function () {
        alert("search advance statement");
      }.bind(this);


      // create object group pop-up
      self.createName = ko.observable();
      self.createType = ko.observable("element group");
      self.createStartDate = ko.observable();
      self.createEndDate = ko.observable();

      var _checkValidationGroupCreateObjectGroup = function () {
        var tracker = document.getElementById("create-tracker");
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.createObjectGroup = function () {
        var valid = _checkValidationGroupCreateObjectGroup();
        if (valid) {
          var json_data = {
            name: self.createName(),
            type: self.createType(),
            startDate: self.createStartDate(),
            endDate: (self.createEndDate()) ? self.createEndDate() : ""
          };
          console.log(json_data);

          oj.Router.rootInstance.store(json_data);
          oj.Router.rootInstance.go("createObjectGroup");
        }
      }.bind(this);

      // create object group modal
      self.startAnimationListenerCreateObjectGroup = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerObjectGroup = function () {
        var popup = document.getElementById('create-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerObjectGroup = function () {
        var popup = document.getElementById('create-modal');
        popup.close();
      };


      /**
       * table
       */

      self.gotoEditObjectGroup = function (json_data) {
        oj.Router.rootInstance.store(json_data);
        oj.Router.rootInstance.go("editObjectGroup");
      }

      self.removeRowObjectGroup = function (id) {

        var success = function (result) {
          self.notificationMessage('success', ['Object Group is removed.']);
          self.setTableObjectGroup();
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/DeleteObjectGroup/" + id;
        services.deleteGeneric(url).then(success, fail);
      }

      // table listener
      var rowDataObjectGroup;
      self.tableDataObjectGroup = ko.observableArray();

      self.selectedRowKey = ko.observable();
      self.selectedIndex = ko.observable();
      self.tableSelectionListenerObjectGroup = function (event) {
        console.log("listner");
        var data = event.detail;
        var currentRow = data.currentRow;
        if (currentRow) {
          self.selectedRowKey(currentRow['rowKey']);
          self.selectedIndex(currentRow['rowIndex'])
          var thisRow = self.selectedIndex();
          rowDataObjectGroup = self.tableDataObjectGroup()[thisRow]
          console.log(thisRow);
          console.log(rowDataObjectGroup);
        }
      };

      // set table values
      self.arrayObjectGroup = ko.observableArray();
      self.dataProviderObjectGroup = ko.observableArray();

      self.setTableObjectGroup = function () {
        self.getObjectGroupData((result) => {
          var temp = [];
          $.each(result, function (index, objectGroup) {
            temp.push({
              num: ++index,
              name: objectGroup.elementSetName,
              type: objectGroup.elementSetType,
              startDate: dateConverter(objectGroup.effectiveStartDate),
              endDate: dateConverter(objectGroup.effectiveEndDate),
              action: objectGroup.elementSetId,
            });
          });

          self.tableDataObjectGroup(result);
          self.arrayObjectGroup(temp);
        });
      }

      self.dataProviderObjectGroup = new ArrayDataProvider(self.arrayObjectGroup, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      self.elementAction = function () {
        var action = event.target.value;
        if (action == "edit") self.gotoEditObjectGroup(rowDataObjectGroup);
        if (action == "remove") self.removeRowObjectGroup(rowDataObjectGroup.elementSetId);
      }.bind(this);

      self.connected = function () {
        app.collapseSideMenu();
        accUtils.announce('Object group form page loaded.');
        document.title = "Object Group Form";
        // Implement further logic if needed
        //$(".oj-navigationlist-item-element").removeClass("oj-selected").addClass("oj-default");

        route_data = oj.Router.rootInstance.retrieve();

        if (typeof route_data != "undefined" && route_data.success) {

          self.notificationMessage('success', [route_data.message]);
          oj.Router.rootInstance.store({});
        }

        self.setTableObjectGroup();
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return IndexObjectGroupViewModel;
  }
);
