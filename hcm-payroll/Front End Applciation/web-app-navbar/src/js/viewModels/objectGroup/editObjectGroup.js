/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['accUtils', 'text!data/objectGroup.json', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
'ojs/ojconverter-datetime',
'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher'
],
 function(accUtils, objectGroupData, services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {

    function CreateObjectGroupViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      this.notifMessage = ko.observableArray();
      this.messagesDataprovider = ko.observableArray();
      this.notificationMessage = function(type, messages){
        var msg = [];
        switch (type) {
          case 'error':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'error',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;

          case 'warning':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'warning',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
              
          case 'success':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
          default:
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
        }

        this.notifMessage(msg);
      }
      this.messagesDataprovider = new ArrayDataProvider(this.notifMessage);

      /**
       * Call API
       */

      self.getElementData = function(callback){
        var success = function(result){
          if(callback) callback(result);
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm";
        services.getGeneric(url).then(success,fail);
      };

      self.getObjectGroupDetails = function(callback, id){
        var success = function(result){
          if(callback) callback(result);
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetObjectGroupDetails/"+id;
        services.getGeneric(url).then(success,fail);
      }

      self.getObjectGroupClassiDetails = function(callback, id){
        var success = function(result){
          if(callback) callback(result);
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetObjectGroupClassiDetails/"+id;
        services.getGeneric(url).then(success,fail);
      }

      self.getClassificationElement = function(callback){
        var success = function(result){
          if(callback) callback(result);
        }

        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/ClassificationElement";
        services.getGeneric(url).then(success,fail);
      }

      /**
       * Date Formatting
       */

      function dateConverter(date, iso=false){
        var d = new Date(date);
        var new_date;

        if(iso){
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        }else{
          new_date = ('0' + d.getDate()).slice(-2) +"-"+ (('0' + (d.getMonth()+1)).slice(-2) ) + '-'+ d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
      {
        pattern: "dd-MM-yyyy"
      }));
      
      self.advanceDisable = ko.observable();

      self.gotoIndex = function(json_data={}){
        oj.Router.rootInstance.store(json_data);
        oj.Router.rootInstance.go("indexObjectGroup");
      }.bind(this);

      /**
       * Update Object Group
       */
      self.editID = ko.observable();
      self.editName = ko.observable();
      self.editType = ko.observable();
      self.editStartDate = ko.observable();
      self.editEndDate = ko.observable();

      self.setDataObjectGroup = function(route_data){
        console.log(route_data);
        self.editID(route_data.elementSetId);
        self.editName(route_data.elementSetName);
        self.editType(route_data.elementSetType);
        self.editStartDate(dateConverter(route_data.effectiveStartDate,true));
        self.editEndDate(dateConverter(route_data.effectiveEndDate,true));
      }

      self.updateObjectGroup = function(){
        var valid = _checkValidationGroupCreateObjectGroup();
        if(valid){
          var payload = {
            "elementSetName": self.editName(),
            "elementSetType": self.editType(),
            "effectiveStartDate": dateConverter(self.editStartDate()),
            "effectiveEndDate": dateConverter(self.editEndDate()),
            "element": self.arrayElementClassification().concat(self.arrayElement())
          }
          console.log(payload);
          var success = function(data){
            self.gotoIndex({action:'update', success:true, message:'Object Group is updated.'});
            // self.notificationMessage('success',['Object Group is created.']);
          }
  
          var fail = function(){
            console.log("fail");
            self.notificationMessage('error',['Something went wrong.']);
          }
  
          var url = "payroll/rest/AppsProRest/UpdateObjectGroup/"+self.editID();
          services.editGeneric(url, payload).then(success,fail);
        }
      }

      var _checkValidationGroupCreateObjectGroup = function () {
        var tracker = document.getElementById("edit-tracker");
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      /**
       * Element Classification Inclusion and Exclusion
       */

      // set element classification table values
      self.arrayElementClassification = ko.observableArray([]);
      self.newArrayElementClassification = ko.observableArray();
      self.dataProviderElementClassification = ko.observableArray();

      self.setArrayElementClassification = function(id) {
        self.getObjectGroupClassiDetails((result)=>{
          var temp = self.arrayElementClassification();
          $.each(result, function(index, elementClassification){
            temp.push({
              "classificationId": elementClassification.classificationId,
              "inclusionStatus": "include"
            })
          });
          self.arrayElementClassification(temp);
        },id);
      }

      self.setTableElementClassification = function(data){
        self.getClassificationElement((result)=>{
          var temp = [];
          $.each(self.arrayElementClassification(), function(indexArr, ElementClassficationArr){
            $.each(result, function(index, elementClassfication){
              if(ElementClassficationArr.classificationId == elementClassfication.classificationId){
                temp.push({
                  num: ++indexArr,
                  name: elementClassfication.classificationName,
                  inclusionStatus: String(ElementClassficationArr.inclusionStatus),
                  action: ElementClassficationArr.classificationId,
                });
              }
            });
          });
          self.newArrayElementClassification(temp); 
        });
      }

      self.dataProviderElementClassification = new ArrayDataProvider(self.newArrayElementClassification, { keyAttributes: 'name', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // remove element classfication table row
      self.removeElementClassification = function(){
        self.arrayElementClassification().splice(thisRowElementClassification, 1);
        self.setTableElementClassification();
      }

      // element table row listener
      var rowDataElementClassification;
      var thisRowElementClassification;
      self.tableDataElement = ko.observableArray();
      
      self.selectedRowKeyElement = ko.observable();
      self.selectedIndexElement = ko.observable();
      self.tableSelectionListenerElementClassification  = function(event){
        var data = event.detail;
        var currentRow = data.currentRow
        if(currentRow){
          self.selectedRowKeyElement(currentRow['rowKey']);
          self.selectedIndexElement(currentRow['rowIndex'])
          thisRowElementClassification = self.selectedIndexElement();
          rowDataElementClassification = self.tableDataElement()[thisRowElementClassification];
          console.log(thisRowElementClassification);
          // console.log(rowDataElement);
        }
      }

      // MODAL
      self.startAnimationListenerElementClassification = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'add-elementClassification-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerElementClassification = function () {
        self.setTableInsertElementClassification();
        var popup = document.getElementById('add-elementClassification-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerElementClassification = function () {
        var popup = document.getElementById('add-elementClassification-modal');
        popup.close();
      };

      // modal - table row listener
      var rowDataInsertElementClassification;
      self.tableDataInsertElementClassification = ko.observableArray();
      
      self.selectedRowKeyInsertElementClassification = ko.observable();
      self.selectedIndexInsertElementClassification = ko.observable();
      self.tableSelectionListenerInsertElementClassification = function(event){
        var data = event.detail;
        var currentRow = data.currentRow
        if(currentRow){
          self.selectedRowKeyInsertElementClassification(currentRow['rowKey']);
          self.selectedIndexInsertElementClassification(currentRow['rowIndex'])
          var thisRow = self.selectedIndexInsertElementClassification();
          rowDataInsertElementClassification = self.tableDataInsertElementClassification()[thisRow];
          console.log(thisRow);
          console.log(rowDataInsertElementClassification);
        }
      }

      // modal - set element classification table values            
      self.arrayInsertElementClassification = ko.observableArray();
      self.dataProviderInsertElementClassification = ko.observableArray();

      self.setTableInsertElementClassification = function(){
        self.getClassificationElement((result)=>{
          var temp = [];
          $.each(result, function(index, classficationElement){
            temp.push({
              num: ++index,
              name: classficationElement.classificationName,
              action: classficationElement.classificationId
            });
          });
          self.arrayInsertElementClassification(temp);
          self.tableDataInsertElementClassification(result);
        });
      }

      self.dataProviderInsertElementClassification = new ArrayDataProvider(self.arrayInsertElementClassification, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      self.insertElementClassification = function(){
        self.arrayElementClassification().push({
          "classificationId": rowDataInsertElementClassification.classificationId,
          "inclusionStatus": "include"
        });
        self.setTableElementClassification();
        self.cancelListenerElementClassification();
      }

      /**
       * Element Inclusion and Exclusion
       */

      // set element table values
      self.arrayElement = ko.observableArray();
      self.newArrayElement = ko.observableArray();
      self.dataProviderElement = ko.observableArray();

      self.setArrayElement = function(id) {
        self.getObjectGroupDetails((result)=>{
          console.log(result);
          $.each(result, function(index, element){
            self.arrayElement().push({
              "elementId": element.elementTypeId,
              "inclusionStatus": "include"
            })
          });
        },id);
      }

      self.setTableElement = function(){
        self.getElementData((result)=>{
          var temp = [];
          if(self.arrayElement().length){
            $.each(self.arrayElement(), function(indexArr, elementArr){
              $.each(result, function(index, element){
                if(elementArr.elementId == element.elementTypeId){
                  temp.push({
                    num: ++indexArr,
                    name: element.elementName,
                    // reportingName: element.reportingName,
                    // elementClassificationName: element.elementClassificationName,
                    inclusionStatus: elementArr.inclusionStatus,
                    action: elementArr.elementId,
                  });
                }
              });
              
            });
          }
          self.tableDataElement(temp);
          self.newArrayElement(temp); 
        });
        
      }

      self.dataProviderElement = new ArrayDataProvider(self.newArrayElement, { keyAttributes: 'num', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // remove element table row
      self.removeElement = function(){
        self.arrayElement().splice(thisRow, 1);
        self.setTableElement();
      }

      // element table row listener
      var rowDataElement;
      var thisRow;
      self.tableDataElement = ko.observableArray();
      
      self.selectedRowKeyElement = ko.observable();
      self.selectedIndexElement = ko.observable();
      self.tableSelectionListenerElement = function(event){
        var data = event.detail;
        var currentRow = data.currentRow
        if(currentRow){
          self.selectedRowKeyElement(currentRow['rowKey']);
          self.selectedIndexElement(currentRow['rowIndex'])
          thisRow = self.selectedIndexElement();
          rowDataElement = self.tableDataElement()[thisRow];
          console.log(thisRow);
          console.log(rowDataElement);
        }
      }

      // MODAL
      self.startAnimationListenerCreateObjectGroup = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'add-element-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerElement = function () {
        self.setTableInsertElement();
        var popup = document.getElementById('add-element-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerElement = function () {
        var popup = document.getElementById('add-element-modal');
        popup.close();
      };

      // modal - table row listener
      var rowDataInsertElement;
      self.tableDataInsertElement = ko.observableArray();
      
      self.selectedRowKeyInsertElement = ko.observable();
      self.selectedIndexInsertElement = ko.observable();
      self.tableSelectionListenerInsertElement = function(event){
        var data = event.detail;
        var currentRow = data.currentRow
        if(currentRow){
          self.selectedRowKeyInsertElement(currentRow['rowKey']);
          self.selectedIndexInsertElement(currentRow['rowIndex'])
          var thisRow = self.selectedIndexInsertElement();
          rowDataInsertElement = self.tableDataInsertElement()[thisRow];
          console.log(thisRow);
          console.log(rowDataInsertElement);
        }
      }

      // modal - set element classification table values
      self.arrayInsertElement = ko.observableArray();
      self.dataProviderInsertElement = ko.observableArray();

      self.setTableInsertElement = function(data){
        self.getElementData((result)=>{
          var temp = [];
          $.each(result, function(index, element){
            temp.push({
              num: ++index,
              name: element.elementName,
              action: element.elementTypeId
            });
          });
          self.tableDataInsertElement(result);
          self.arrayInsertElement(temp); 
        });
        
      }

      self.dataProviderInsertElement = new ArrayDataProvider(self.arrayInsertElement, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      self.insertElement = function(){
        self.arrayElement().push({
          "elementId": rowDataInsertElement.elementTypeId,
          "inclusionStatus": "include"
        });
        // console.log(self.arrayElement());
        self.setTableElement();
        self.cancelListenerElement();
      }

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function() {
        accUtils.announce('Object group form page loaded.');
        document.title = "Object Group Form";
        // Implement further logic if needed
        $("li#ui-id-6").removeClass("oj-default").addClass("oj-selected");

        route_data = oj.Router.rootInstance.retrieve();
        self.setDataObjectGroup(route_data);

        self.setArrayElement(route_data.elementSetId);
        self.setArrayElementClassification(route_data.elementSetId);

        self.setTableElementClassification();
        self.setTableElement();
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return CreateObjectGroupViewModel;
  }
);
