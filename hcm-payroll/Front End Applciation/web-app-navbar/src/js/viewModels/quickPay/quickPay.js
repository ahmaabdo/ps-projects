/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['accUtils', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojtimezonedata', 'ojs/ojconverterutils-i18n', 'ojs/ojknockout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox'
],
  function (accUtils, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {
    function quickPayViewModel() {
      var self = this;


      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));



      self.payrollFlow = ko.observable();
      self.overridingDate = ko.observable();
      self.payrollValue = ko.observable();
      self.payrollPeriod = ko.observable();
      self.processDate = ko.observable();
      self.dateEarned = ko.observable();
      self.consolidationGroup = ko.observable();
      self.runType = ko.observable();
      self.processConfigurationGroup = ko.observable();
      self.elementGroup = ko.observable();

        self.browsers = ko.observableArray([
          { value: 'Internet Explorer', label: 'type1' },
          { value: 'Firefox', label: 'type2' },
          { value: 'Chrome', label: 'type3' },
          { value: 'Opera', label: 'type4' }]);
        self.gotoIndex = function(){
          oj.Router.rootInstance.go("process");
        }

      self.back = function () {
        app.router.go('processPages')
      };

      var _checkValidationGroupQuickPay = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.createQuickPay = function(){
        var valid = _checkValidationGroupQuickPay("create-tracker");
        var valid = _checkValidationGroupQuickPay("create-tracker-2");
        if(valid){
          var jsondata = {
            "payrollFlow": self.payrollFlow(),
            "overridingDate": self.overridingDate(),
            "payrollValue": self.payrollValue(),
            "payrollPeriod": self.payrollPeriod(),
            "processDeatails": self.processDeatails(),
            "dateEarned": self.dateEarned(),
            "consolidationGroup": self.consolidationGroup(),
            "runType": self.runType(),
            "processConfigurationGroup": self.processConfigurationGroup(),
            "elementGroup": self.elementGroup(),
          }
          console.log(jsondata);
          self.back();
        } 
      }

      self.connected = () => {
        accUtils.announce('Dashboard page loaded.', 'assertive');
        document.title = "Quick Pay";

      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = () => {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = () => {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return quickPayViewModel;
  }
);

