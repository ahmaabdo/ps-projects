/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your customer ViewModel code goes here
 */
define(['accUtils', 'text!data/fastFormula.json', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
'ojs/ojconverter-datetime',
'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages'],
 function(accUtils, fastFormulaData, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {

    function IndexFastFormulaViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      self.notifMessage = ko.observableArray();
      self.messagesDataprovider = ko.observableArray();
      self.notificationMessage = function(type, messages){
        var msg = [];
        switch (type) {
          case 'error':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'error',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;

          case 'warning':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'warning',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
              
          case 'success':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
          default:
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
        }

        self.notifMessage(msg);
      }
      self.messagesDataprovider = new ArrayDataProvider(self.notifMessage);


            ////navigator
                  self.back = function(){
                    app.router.go('homeSetup')
                  };
                  self.manageElement = function(){
                    app.router.go('indexElement')
                  };
                  self.manageObjectGroup = function(){
                    app.router.go('indexObjectGroup')
                  };
                  self.fastFormula = function(){
                    app.router.go('indexFastFormula')
                  };
                  self.report = function(){
                    app.router.go('indexReport')
                  };
                  /////////////////

      /**
       * API Call
       */

      self.getFastFormulaData = async function(callback){
        var success = function(result){
          if(callback) callback(result);
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/FastFormulaRest/GetFastFormula";
        services.getGeneric(url).then(success,fail);
      }

      /**
       * Date Formatting
       */

      function dateConverter(date, iso = false,api=false) {
        if(api){
          var d = new Date(date);
        }else{
          var arr = date.split("-");
          var d = new Date(arr[2], (arr[1]-1), arr[0]);
        }
        var new_date;
        
        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d);
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }

        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
      {
        pattern: "dd-MM-yyyy"
      }));

      /**
       * Search
       */
      self.advanceDisable = ko.observable();
      
      // keyword search
      self.keyword = ko.observable();
      self.searchKeyword = function(){
        alert("search keyword statement");
      }.bind(this);

      // advance search
      self.searchName = ko.observable();
      self.searchType = ko.observable();
      self.searchStartDate = ko.observable();
      self.searchEndDate = ko.observable();

      self.searchAdvance = function(){
        alert("search advance statement");
      }.bind(this);

      // create Fast formula pop-up
      self.createFastFormulaName = ko.observable();
      self.createFastFormulaType = ko.observable();
      self.createFastFormulaStartDate = ko.observable();
      self.createFastFormulaEndDate = ko.observable();

      var _checkValidationGroupFastFormula = function () {
        var tracker = document.getElementById("create-tracker");
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.createFastFormula = function(){
        var valid = _checkValidationGroupFastFormula();
        if(valid){
          var json_data = {
            name: self.createFastFormulaName(),
            type: self.createFastFormulaType(),
            startDate: self.createFastFormulaStartDate(),
            endDate: self.createFastFormulaEndDate()
          };
          console.log(json_data);

          oj.Router.rootInstance.store(json_data);
          oj.Router.rootInstance.go("createFastFormula");
        }
      }.bind(this);

      // create object group modal
      self.startAnimationListenerCreateFastFormula = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerFastFormula = function () {
        var popup = document.getElementById('create-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerFastFormula = function () {
        var popup = document.getElementById('create-modal');
        popup.close();
      };

      /**
       * table
       */

      self.gotoEditFastFormula = function(json_data){
        // oj.Router.rootInstance.store(json_data);
        sessionStorage.setItem("fastFormula",JSON.stringify(json_data));
        oj.Router.rootInstance.go("editFastFormula");
      }

      self.gotoDefineFormulaResult = function(json_data){
        // oj.Router.rootInstance.store(json_data);
        sessionStorage.setItem("fastFormula",JSON.stringify(json_data));
        oj.Router.rootInstance.go("defineFormulaResult");
      }

      self.removeRowFastFormula = function(id){
        var payload = {
          "formula_id": id
        }
        var success = function(result){
          self.notificationMessage('success',['FastFormula is removed.']);
          self.setTableFastFormula();
        }
        var fail = function(){
          self.notificationMessage('error',['Something went wrong.']);
        }

        var url = "payroll/rest/FastFormulaRest/delete";
        services.addGeneric(url,payload).then(success,fail);
      }

      // table listener
      var rowDataFastFormula;
      self.tableDataFastFormula = ko.observableArray();
      
      self.selectedRowKey = ko.observable();
      self.selectedIndex = ko.observable();
      self.tableSelectionListenerFastFormula = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow;
        if(currentRow){
          self.selectedRowKey(currentRow['rowKey']);
          self.selectedIndex(currentRow['rowIndex'])
          var thisRow = self.selectedIndex();
          rowDataFastFormula = self.tableDataFastFormula()[thisRow]
          console.log(thisRow);
          console.log(rowDataFastFormula);
        }
      };

      // set table values
      self.arrayFastFormula = ko.observableArray();
      self.dataProviderFastFormula = ko.observableArray();
      
      self.setTableFastFormula = function(){
        
        self.getFastFormulaData((result)=>{
          var temp = [];
          $.each(result, function (index, fastFormula) {
            temp.push({
              
              id: ++index,
              name: fastFormula.formula_name,
              type: fastFormula.formula_type_id,
              startDate: dateConverter(fastFormula.effective_start_date),
              endDate: dateConverter(fastFormula.effective_end_date),
              action: fastFormula.formula_id,
            });
          });
          self.tableDataFastFormula(result);
          self.arrayFastFormula(temp); 
        });    
      }
      
      self.dataProviderFastFormula = new ArrayDataProvider(self.arrayFastFormula, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });
      
      self.elementAction = function(){
        var action = event.target.value;
        if(action == "edit") self.gotoEditFastFormula(rowDataFastFormula);
        if(action == "defineFormulaResult") self.gotoDefineFormulaResult(rowDataFastFormula);
        if(action == "remove") self.removeRowFastFormula(rowDataFastFormula.formula_id);
      }.bind(this);

      self.connected = function() {
        app.collapseSideMenu();
        accUtils.announce('Fast Formula page loaded.');
        document.title = "Fast Formula";
        // Implement further logic if needed

        route_data = oj.Router.rootInstance.retrieve();
        if(typeof route_data != "undefined" && route_data.success){
          
          self.notificationMessage('success',[route_data.message]);
          oj.Router.rootInstance.store({});
        } 

        self.setTableFastFormula();
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return IndexFastFormulaViewModel;
  }
);
