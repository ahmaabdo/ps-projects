/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your about ViewModel code goes here
 */
define(['accUtils', 'text!data/fastFormula.json', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
'ojs/ojconverter-datetime',
'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages'],
 function(accUtils, fastFormulaData, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter){

    function DefineFormulaResultViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      self.notifMessage = ko.observableArray();
      self.messagesDataprovider = ko.observableArray();
      self.notificationMessage = function(type, messages){
        var msg = [];
        switch (type) {
          case 'error':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'error',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;

          case 'warning':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'warning',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
              
          case 'success':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
          default:
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
        }

        self.notifMessage(msg);
      }
      self.messagesDataprovider = new ArrayDataProvider(self.notifMessage);


            ////navigator
                  self.back = function(){
                    app.router.go('homeSetup')
                  };
                  self.manageElement = function(){
                    app.router.go('indexElement')
                  };
                  self.manageObjectGroup = function(){
                    app.router.go('indexObjectGroup')
                  };
                  self.fastFormula = function(){
                    app.router.go('indexFastFormula')
                  };
                  /////////////////

      /**
       * API Call
       */
      self.getElementDataDFR = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm";
        services.getGeneric(url).then(success, fail);
      };

      self.getElementData = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm";
        services.getGeneric(url).then(success, fail);
      };

      self.getElementDataById = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm/" + id;
        services.getGeneric(url).then(success, fail);
      }

      self.getFastFormulaData = async function(callback){
        var success = function(result){
          if(callback) callback(result);
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/FastFormulaRest/GetFastFormula";
        services.getGeneric(url).then(success,fail);
      }
      
      self.getDFRData = async function (callback, id) {

        var success = function(result){
          if(callback){
            var temp = [];
            self.DFRFastFormula()
            $.each(result, function (i, dfr) { 
              if(dfr.formulaId == self.DFRFastFormula()) temp.push(dfr);
            });

            callback(temp);
          } 
          console.log("success");
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetAllFormulaResultsByElementId/"+id;
        services.getGeneric(url).then(success,fail);
      }

      self.getDFRDataById = async function (callback, id) {

        var success = function(result){
          if(callback) callback(result);
          console.log("success");
        }
        var fail = function(){
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetFormulaResultById/"+id;
        services.getGeneric(url).then(success,fail);
      }

      self.getFastFormula = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
          console.log("success");
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/FastFormulaRest/GetFastFormula";
        services.getGeneric(url).then(success, fail);
      }

      self.getInputValues = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetInputValue/" + id;
        services.getGeneric(url).then(success, fail);
      }

      self.getLookup = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/Lookup";
        await services.getGeneric(url).then(success, fail);
      }

      /**
       * Date Formatting
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      this.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      function currentDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
  
        return yyyy+'-'+mm+'-'+dd;
      }

      self.currentDate = ko.observable(currentDate());


      /**
       * Search
       */
      self.advanceDisable = ko.observable();
      
      // keyword search
      self.keyword = ko.observable();
      self.searchKeyword = function(){
        alert("search keyword statement");
      }.bind(this);

      // advance search
      self.searchName = ko.observable();
      self.searchType = ko.observable();
      self.searchStartDate = ko.observable();
      self.searchEndDate = ko.observable();

      self.searchAdvance = function(){
        alert("search advance statement");
      }.bind(this);

      /**
       * table
       */

      self.createPage = ko.observable(true);

      // FastFormula variable
      self.fastFormula = ko.observable();

      // Set Define Fast Formula Dropdown 
      self.optionDefineFormulaResult = ko.observableArray();
      self.optionElementDefineFormulaResult = ko.observableArray();
      self.optionElementDefineFormulaResult2 = ko.observableArray();
      self.optionInputValueDefineFormulaResult = ko.observableArray(); 
      self.optionInputValueDefineFormulaResult2 = ko.observableArray();
      self.optionFormulaResult = ko.observableArray();
      self.optionFormulaResult2 = ko.observableArray();
      self.optionMessageDefineElement = ko.observableArray(); 
      self.optionLinkType = ko.observableArray();

      function setDefineFormulaResultDropdowns(){
        self.getFastFormula((result) => {
          var options = [];
          $.each(result, function (index, fastFormula) {
             options.push({ value: fastFormula.formula_id, label: fastFormula.formula_name });
          });
          self.optionDefineFormulaResult(options);
        });

        // Fast Formula out parameter options
        var options1 = [];
        var options2 = [];
        var fastFormula = self.fastFormula();
        $.each(fastFormula.parameteList, function (i, parameter) { 
          if(parameter.directions === "OUT"){
            options1.push({ value: parameter.id, label: parameter.name });
            options2.push({ value: parameter.id+"-"+parameter.name, label: parameter.name });
          } 
        });
        self.optionFormulaResult(options1);
        self.optionFormulaResult2(options2);

        
        //Lookup
        self.getLookup((result)=>{
          var options1 = [];
          var options2 = [];
          $.each(result, function (i, lookup) { 
            if(lookup.name === "LINK_TYPE") options1.push({ value: lookup.code, label: lookup.valueEn });
            if(lookup.name === "MESSAGE") options2.push({ value: lookup.code, label: lookup.valueEn });
          });

          self.optionLinkType(options1);
          self.optionMessageDefineElement(options2);
        });
      }

      self.changeListenerFormulaResult = function(event, current){
        var optionValue = event.detail;
        var id = optionValue.value;
        
        if(self.createPage()){
          self.linkType(null);
          self.elementNameDefineFormulaResult(null);
          self.inputValueDefineFormulaResult(null);
        }else{
          self.editLinkType(null);
          self.editElementLinkType(null);
          self.editInputValueLinkType(null);
        }
        
      }

      self.changeListenerLinkType = function(event, current){
        var optionValue = event.detail;
        var id = optionValue.value;

        self.optionElementDefineFormulaResult(null);
        self.optionInputValueDefineFormulaResult(null);
        self.optionElementDefineFormulaResult2(null);
        self.optionInputValueDefineFormulaResult2(null);
        clearFormulaRules();

        self.getElementData((result) => {
          var options = [];
          var options2 = [];
          $.each(result, function (index, elementDFR) {
            if(id == 'Direct Result'){
              if(elementDFR.elementTypeId == self.DFRElement() || elementDFR.elementTypeId == self.editDFRElement()){
                options.push({ value: elementDFR.elementTypeId, label: elementDFR.elementName });
                options2.push({ value: elementDFR.elementTypeId+'-'+elementDFR.elementName, label: elementDFR.elementName });
              } 
            }
            if(id == 'Indirect Result'){
              if(elementDFR.elementTypeId != self.DFRElement() && elementDFR.elementTypeId != self.editDFRElement()){
                options.push({ value: elementDFR.elementTypeId, label: elementDFR.elementName });
                options2.push({ value: elementDFR.elementTypeId+'-'+elementDFR.elementName, label: elementDFR.elementName });
              } 
            }
          });
          self.optionElementDefineFormulaResult(options);
          self.optionElementDefineFormulaResult2(options2);
        });

      }

      self.changeListenerElementDFR = function(event, current){
        var optionValue = event.detail;
        var id = optionValue.value;

        if(id){
          if(typeof id == "string" && id.indexOf("-") >= 0) id = id.split("-")[0];
          self.getInputValues((result) => {
            var fastFormula = self.fastFormula();
            var formulaRuleId = (self.createPage())? self.formulaResult().split("-")[0] : self.editFormulaResult();
            var parameterType = "";
            $.each(fastFormula.parameteList, function (i, parameter) { 
              if(parameter.id === formulaRuleId) parameterType = parameter.parameter_type; 
            });

            var options = [];
            var options2 = [];
            $.each(result, function (index, inputValue) {
              // if((inputValue.uom == parameterType)){
              //   options.push({ value: inputValue.inputValueId, label: inputValue.name });
              //   options2.push({ value: inputValue.inputValueId+'-'+inputValue.name, label: inputValue.name });  
              // }
              if((inputValue.uom == "money" || inputValue.uom == "number") && (parameterType == "INT" || parameterType == "FLOAT")){
                options.push({ value: inputValue.inputValueId, label: inputValue.name });
                options2.push({ value: inputValue.inputValueId+'-'+inputValue.name, label: inputValue.name });  
              }
              if(inputValue.uom == "date" && parameterType == "DATE"){
                options.push({ value: inputValue.inputValueId, label: inputValue.name });
                options2.push({ value: inputValue.inputValueId+'-'+inputValue.name, label: inputValue.name });  
              }
              if(inputValue.uom == "character" && parameterType == "VARCHAR"){
                options.push({ value: inputValue.inputValueId, label: inputValue.name });
                options2.push({ value: inputValue.inputValueId+'-'+inputValue.name, label: inputValue.name });  
              }
            });
            self.optionInputValueDefineFormulaResult(options);
            self.optionInputValueDefineFormulaResult2(options2);
          },id);
        }
      }

      self.changeListenerElement = function(event, current){
        var optionValue = event.detail;
        var id = optionValue.value;

        self.setTableDFR(id);
      }

      // set table Define Formula Result
      self.selectedElement = ko.observable();

      self.arrayDFR = ko.observableArray([]);
      self.dataProviderDFR = ko.observableArray([]);

      self.setTableDFR = function (id) {
        self.arrayDFR([]);
        self.getDFRData((result) => {
          var temp = [];

          $.each(result, function (index, defineFormulaResult) {
            temp.push({
              num: ++index,
              description: defineFormulaResult.description,
              formulaName: defineFormulaResult.formulaName,
              startDate: dateConverter(defineFormulaResult.effectiveStartDate),
              endDate: dateConverter(defineFormulaResult.effectiveEndDate),
              action: defineFormulaResult.elementSetId,
            });
          });

          self.tableDataDFR(result);
          self.arrayDFR(temp);
        },id);
      }

      self.dataProviderDFR = new ArrayDataProvider(self.arrayDFR, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // table index listener
      var rowDataDefineFormulaResult;
      self.tableDataDFR = ko.observableArray([]);

      self.selectedRowKey = ko.observable("");
      self.selectedIndex = ko.observable("");
      self.tableSelectionListenerDFR = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow;
        if (currentRow) {
          self.selectedRowKey(currentRow['rowKey']);
          self.selectedIndex(currentRow['rowIndex'])
          var thisRow = self.selectedIndex();
          rowDataDefineFormulaResult = self.tableDataDFR()[thisRow];
          console.log(thisRow);
          console.log(rowDataDefineFormulaResult);
        }
      };

      // DFR CREATE MODAl
      self.startAnimationListenerCreateDFR = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };

      self.openListenerCreateDFR = function () {
        clearDFR();
        var popup = document.getElementById('create-dfr-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerCreateDFR = function () {
        var popup = document.getElementById('create-dfr-modal');
        popup.close();
        
      };

      // CREATE DEFINE FORMULA
      self.DFRDescription = ko.observable("");
      self.DFRformulaName = ko.observable("");
      self.DFRFastFormula = ko.observable("");
      self.DFRStartDate = ko.observable("");
      self.DFREndDate = ko.observable("");
      self.DFRElement = ko.observable("");
      self.optionDFRElement = ko.observableArray();

      // currency option
      self.getElementDataDFR((result) => {
        var options = [];
        $.each(result, function (index, elementDFR) {
          options.push({ value: String(elementDFR.elementTypeId), label: elementDFR.elementName });
        });
        self.optionDFRElement(options);
      });

      function clearDFR(){
        self.DFRDescription("");
        self.DFRformulaName("");
        self.DFRElement("");
        // self.DFRFastFormula("");
        self.DFRStartDate("");
        self.DFREndDate("");
        self.arrayLinkType([]);
      };

      function clearFormulaRules(){
        self.elementNameDefineFormulaResult("");
        self.inputValueDefineFormulaResult("");
        
        self.editElementLinkType("");
        self.editInputValueLinkType("");
      };
      function clearFormulaResult(){
        self.formulaResult("");
        
      };


      self.createDFR = function () {
        var valid = _checkValidationGroupDFR("create-dfr-tracker");
          if (valid) {
            self.DFRformulaName(self.optionDefineFormulaResult().find(x => x.value == self.DFRFastFormula()).label);
            
            self.editDFRElement(null);
            self.setTableLinkType();
            self.cancelListenerCreateDFR();
            self.createPage(true);
            $("#page-1-dfr").hide();
            $("#page-2-dfr").show();
          }
      }

      self.removeRowDefineFormulaResult = function (id) {
        var success = function (result) {
          self.notificationMessage('success', ['Formula Result is removed.']);
          self.setTableDFR(self.selectedElement());
          console.log("success");
        }
        var fail = function () {
          //self.notificationMessage('error', ['Something went wrong.']);
          self.notificationMessage('success', ['Formula Result is removed.']);
          self.setTableDFR(self.selectedElement());
          console.log("fail");
        }
        var url = "payroll/rest/AppsProRest/DeleteFormulaResult/" + id;
        services.deleteGeneric(url).then(success, fail);
      }

      self.DFRAction = function (event) {
        var action = event.target.value;
        if (action == "edit") {
          self.createPage(false);
          $("#page-2-dfr").show();
          $("#page-1-dfr").hide();
          clearDFR();

          self.editDFR(rowDataDefineFormulaResult.formulaUsageId);
        }
        if (action == "remove") {
          self.removeRowDefineFormulaResult(rowDataDefineFormulaResult.formulaUsageId);
        }
      };

      //CREATE LINK TYPE

      self.submitDFR = function () {
        var payload = {
          "effectiveStartDate": dateConverter(self.DFRStartDate()),
          "effectiveEndDate": (self.DFREndDate())? dateConverter(self.DFREndDate()) : "",
          "formulaId": self.DFRFastFormula(),
          "elementTypeId": self.DFRElement(),
          "businessGroupId": "",
          "description": self.DFRDescription(),
          "formulaResultRules": self.arrayLinkType()
        }

        var success = function (data) {
          self.notificationMessage('success', ['Define Formula Result is created.']);
          $("#page-1-dfr").show();
          $("#page-2-dfr").hide();
          self.setTableDFR(self.DFRElement());        
        }
  
        var fail = function () {
          self.notificationMessage('error', ['Something went wrong.']);
        }

        var url = "payroll/rest/AppsProRest/insertFormulaResult";
        services.addGeneric(url, payload).then(success, fail);
      }
        

      /**
       * Create Link Type
       */

      self.linkType = ko.observable("");
      self.elementNameDefineFormulaResult = ko.observable("");
      self.inputValueDefineFormulaResult = ko.observable("");
      self.messageDefineElement = ko.observable("");
      self.formulaResult = ko.observable("");

      // LINK TYPE table values
      self.arrayLinkType = ko.observableArray();
      self.newArrayLinkType = ko.observableArray();
      self.dataProviderLinkType = ko.observableArray();

      self.setTableLinkType = function () {
        self.newArrayLinkType(self.arrayLinkType());
        self.tableDataLinkType(self.arrayLinkType());
      }

      self.dataProviderLinkType = new ArrayDataProvider(self.newArrayLinkType, { keyAttributes: 'name', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // Link Type table row listener
      var rowDataLinkType;
      var thisRowLinkType;
      self.tableDataLinkType = ko.observableArray();

      self.selectedRowKeyLinkType = ko.observable("");
      self.selectedIndexLinkType = ko.observable("");
      self.tableSelectionListenerLinkType = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if (currentRow) {
          self.selectedRowKeyLinkType(currentRow['rowKey']);
          self.selectedIndexLinkType(currentRow['rowIndex'])
          thisRowLinkType = self.selectedIndexLinkType();
          rowDataLinkType = self.tableDataLinkType()[thisRowLinkType];
          console.log(thisRowLinkType);
          console.log(rowDataLinkType);
        }
      }


      // remove Link type table row
      self.removeLinkType = function () {
        var temp = self.arrayLinkType();
        temp.splice(thisRowLinkType, 1);
        self.arrayLinkType(temp);
        self.setTableLinkType();
      }

      // insert Link type table row
      self.insertDefineFormulaResult = function () {
        var valid = _checkValidationGroupDFR("insert-defineFormulaResult-tracker")
        if(valid){
          var outParamterArr = self.formulaResult().split("-");
          var elementArr = self.elementNameDefineFormulaResult().split("-");
          var inputValueArr = self.inputValueDefineFormulaResult().split("-");
          var json_data = {
            parameterId: outParamterArr[0],
            parameterName : outParamterArr[1],
            resultRuleType: self.linkType(),
            elementTypeId: elementArr[0],
            elementName: elementArr[1],
            inputValueId: inputValueArr[0],
            inputValueName: inputValueArr[1],
            messageType: self.messageDefineElement(),
            businessGroupId: "",
            resultName: "",
            effectiveStartDate: dateConverter(self.DFRStartDate()),
            effectiveEndDate: (self.DFREndDate())? dateConverter(self.DFREndDate()) : "",
          }

          
          self.arrayLinkType().push(json_data);
          self.setTableLinkType();
          self.cancelListenerLinkType();
        }
      }      


      // modal
      self.startAnimationListenerLinkType = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'insert-defineFormulaResult-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerLinkType = function () {
        self.linkType(null);
        clearFormulaRules();
        clearFormulaResult();
        var popup = document.getElementById('insert-defineFormulaResult-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerLinkType = function () {
        var popup = document.getElementById('insert-defineFormulaResult-modal');
        popup.close();

      };

      self.gotoIndexCancel = function () {
        clearDFR();
        $("#page-2-dfr").hide();
        $("#page-1-dfr").fadeIn();
      };
      self.gotoIndexFastFormula = function () {
        clearDFR();
        oj.Router.rootInstance.go("indexFastFormula");
      };


      /**
       * Update Define Formula Result
       */

      self.formulaUsageId = ko.observable();
      self.editDFRElement = ko.observable();
      self.editDescriptionDFR = ko.observable("asdasdasd");
      self.editFastFormulaDFR = ko.observable();
      self.editEffectiveStartDateDFR = ko.observable();
      self.editEffectiveEndDateDFR = ko.observable();
      self.formulaResultRules = ko.observableArray();

      function clearEditDFR(){
        self.formulaUsageId = ko.observable("");
        self.editDescriptionDFR = ko.observable("");
        self.editFastFormulaDFR = ko.observable("");
        self.editEffectiveStartDateDFR = ko.observable("");
        self.editEffectiveEndDateDFR = ko.observable("");
        self.formulaResultRules = ko.observableArray([]);
      }

      function formatFormulaResultRules(arr){
        var new_array = [];
        $.each(arr, function (index, formulaResultRule) { 
          formulaResultRule.effectiveStartDate = dateConverter(self.editEffectiveStartDateDFR());
          formulaResultRule.effectiveEndDate = dateConverter(self.editEffectiveEndDateDFR());
          new_array.push(formulaResultRule);
        });
        
        return new_array;
      }

      self.editDFR = function(id){
        self.getDFRDataById((result)=>{
          self.formulaUsageId(result.formulaUsageId);
          self.editDFRElement(result.elementTypeId);
          self.editDescriptionDFR(result.description);
          self.editFastFormulaDFR(result.formulaId);
          self.editEffectiveStartDateDFR(dateConverter(result.effectiveStartDate,true));
          self.editEffectiveEndDateDFR(dateConverter(result.effectiveEndDate,true));
          self.formulaResultRules(formatFormulaResultRules(result.formulaResultRulesBeanList));
          self.tableDataLinkTypeEdit(result.formulaResultRulesBeanList);
        },id);
      }      

      self.updateDFR = function () {
        var valid = _checkValidationGroupDFR("edit-tracker");
        if (valid) {

          var payload = {
            "formulaUsageId": self.formulaUsageId(),
            "effectiveStartDate": dateConverter(self.editEffectiveStartDateDFR()),
            "effectiveEndDate": (self.editEffectiveEndDateDFR())? dateConverter(self.editEffectiveEndDateDFR()) : "",
            "formulaId": self.editFastFormulaDFR(),
            "elementTypeId": self.DFRElement(),
            "businessGroupId": "",
            "description": self.editDescriptionDFR(),
            "formulaResultRules": formatFormulaResultRules(self.formulaResultRules())
          }

          var success = function (result) {
            self.notificationMessage('success', ['Define Formula is updated.']);

            $("#page-1-dfr").show();
            $("#page-2-dfr").hide();
            
            clearDFR();
            self.setTableDFR(self.editDFRElement());

          }

          var fail = function () {
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/UpdateFormulaResult";
          services.editGeneric(url, payload).then(success, fail);
        }

      }

      self.insertLinkType = function(){
        var valid = _checkValidationGroupDFR("edit-defineFormulaResult-tracker");
        if (valid) {
          var payload = {
            "effectiveStartDate": dateConverter(self.editEffectiveStartDateDFR()),
            "effectiveEndDate": (self.editEffectiveEndDateDFR())? dateConverter(self.editEffectiveEndDateDFR()) : "",
            "formulaUsageId": self.formulaUsageId(),
            "elementTypeId": self.editElementLinkType(),
            "businessGroupId": "",
            "resultName": "",
            "resultRuleType": self.editLinkType(),
            "messageType": self.editMessageDefineElement(),
            "inputValueId": self.editInputValueLinkType(),
            "parameterId": self.editFormulaResult()
          }
  
          var success = function (result) {
            self.cancelListenerLinkTypeEdit();
            self.resetLinkTypeTable(self.formulaUsageId());
          }

          var fail = function () {
            self.notificationMessage('error', ['Something went wrong.']);
          }

          var url = "payroll/rest/AppsProRest/insertFormulaResultRule";
          services.addGeneric(url, payload).then(success, fail);
        }
      }

      self.deleteLinkType = function(){
        var success = function (result) {
         console.log('success');
        }
        var fail = function () {
          self.resetLinkTypeTable(self.formulaUsageId());
        }
        var url = "payroll/rest/AppsProRest/DeleteFormulaResultRule/"+rowDataLinkTypeEdit.formulaResultRuleId;
        services.deleteGeneric(url).then(success, fail);
      }

      // edit table linktype
      self.resetLinkTypeTable = function(id){
        self.formulaResultRules([]);
        self.getDFRDataById((result)=>{
          self.formulaResultRules(formatFormulaResultRules(result.formulaResultRulesBeanList));
          self.tableDataLinkTypeEdit(result.formulaResultRulesBeanList);
        },id);
      } 

      self.dataProviderLinkTypeEdit = ko.observableArray();
      self.dataProviderLinkTypeEdit = new ArrayDataProvider(self.formulaResultRules, { keyAttributes: 'name', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // edit table - row listener
      var rowDataLinkTypeEdit;
      self.tableDataLinkTypeEdit = ko.observableArray();

      self.selectedRowKeyLinkTypeEdit = ko.observable("");
      self.selectedIndexLinkTypeEdit = ko.observable("");
      self.tableSelectionListenerLinkTypeEdit = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow;
        if (currentRow) {
          self.selectedRowKeyLinkTypeEdit(currentRow['rowKey']);
          self.selectedIndexLinkTypeEdit(currentRow['rowIndex'])
          var thisRowLinkTypeEdit = self.selectedIndexLinkTypeEdit();
          rowDataLinkTypeEdit = self.tableDataLinkTypeEdit()[thisRowLinkTypeEdit];
          console.log(thisRowLinkTypeEdit);
          console.log(rowDataLinkTypeEdit);
        }
      }

      // edit modal linktype
      self.editFormulaResult = ko.observable();
      self.editLinkType = ko.observable();
      self.editElementLinkType = ko.observable();
      self.editInputValueLinkType = ko.observable();
      self.editMessageDefineElement = ko.observable();

      function clearLinkTypeEdit(){
        self.editFormulaResult("");
        self.editLinkType("");
        self.editElementLinkType("");
        self.editInputValueLinkType("");
        self.editMessageDefineElement("");
      }

      
      self.startAnimationListenerLinkTypeEdit = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'edit-defineFormulaResult-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerLinkTypeEdit = function () {
        clearLinkTypeEdit();
        var popup = document.getElementById('edit-defineFormulaResult-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerLinkTypeEdit = function () {
        var popup = document.getElementById('edit-defineFormulaResult-modal');
        popup.close();
      }; 

      //END DFR

      var _checkValidationGroupDFR = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.connected = function() {
        app.collapseSideMenu();
        accUtils.announce('Define Formula Result page loaded.');
        document.title = "Define Formula Result";
        // Implement further logic if needed

        // route_data = oj.Router.rootInstance.retrieve();
        route_data = JSON.parse(sessionStorage.getItem("fastFormula"));
        if(typeof route_data != "undefined" && route_data.success){
          
          self.notificationMessage('success',[route_data.message]);
          oj.Router.rootInstance.store({});
        } 
        else{
          self.DFRFastFormula(route_data.formula_id);
          self.fastFormula(route_data);
        }
        
        self.tableDataDFR();
        setDefineFormulaResultDropdowns();
        self.getElementDataDFR();    
      };


      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return DefineFormulaResultViewModel;
  }
);
