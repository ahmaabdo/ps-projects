/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['accUtils', 'text!data/objectGroup.json', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojselectcombobox', 'ojs/ojformlayout',
],
  function (accUtils, objectGroupData, services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {

    function CreateFastFormulaViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      this.notifMessage = ko.observableArray();
      this.messagesDataprovider = ko.observableArray();
      this.notificationMessage = function (type, messages) {
        var msg = [];
        switch (type) {
          case 'error':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'error',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'warning':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'warning',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'success':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
          default:
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
        }

        this.notifMessage(msg);
      }
      this.messagesDataprovider = new ArrayDataProvider(this.notifMessage);

      /**
       * Call APIs
       */

      self.getElementData = function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm";
        services.getGeneric(url).then(success, fail);
      };

      self.getLookUp = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/Lookup";
        services.getGeneric(url).then(success, fail);
      }

      self.getInputValues = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetInputValue/" + id;
        services.getGeneric(url).then(success, fail);
      }

      self.getReport = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/report/GetAllBIReportsInfo";
        services.getGeneric(url).then(success, fail);
      }

      // Set dropdown
      self.optionDirection = ko.observableArray();
      self.optionCategory = ko.observableArray();
      self.optionCategory2 = ko.observableArray();
      self.element = ko.observableArray();
      self.optionElement = ko.observableArray();
      self.optionInputValue = ko.observableArray();
      self.optionPersonDetails = ko.observableArray();
      self.optionReportName = ko.observableArray();
      self.optionParameterType = ko.observableArray();
      self.optionParameter = ko.observableArray([
        { value: '1', label: 'Parameter 1' },
        { value: '2', label: 'Parameter 2' }
      ]);

      function setFastFormulaDropdowns() {
        // insert parameter dropdown
        self.getLookUp((result) => {
          var option1 = [];
          var option2 = [];
          var option3 = [];
          // var option4 = [];
          var option5 = [];
          $.each(result, function (indexInArray, direction) {
            if (direction.name == "PARAMETER_TYPE") option1.push({ value: direction.code, label: direction.valueEn });
            if (direction.name == "PARAMETER_CATEGORY" && direction.code != "Static") option2.push({ value: direction.code, label: direction.valueEn });
            if (direction.name == "PERSON_DETAILS") option3.push({ value: direction.code, label: direction.valueEn });
            // if (direction.name == "UNIT_OF_MEASURE") option4.push({ value: direction.code, label: direction.valueEn });
            if (direction.name == "PARAMETER_CATEGORY" && direction.code != "Static" && direction.code != "REPORT") option5.push({ value: direction.code, label: direction.valueEn });
          });

          self.optionDirection(option1);
          self.optionCategory(option2);
          self.optionPersonDetails(option3);
          self.optionParameterType([
            { value: 'INT', label: "INT" },
            { value: 'FLOAT', label: "FLOAT" },
            { value: 'VARCHAR', label: "VARCHAR" },
            { value: 'DATE', label: "DATE" },
          ]);
          self.optionCategory2(option5);
        });

        self.getElementData((result) => {
          var option = [];
          $.each(result, function (indexInArray, element) {
            option.push({ value: element.elementTypeId, label: element.elementName });
          });

          self.element(result);
          self.optionElement(option);
        });

        self.getReport((result) => {
          var option = [];
          $.each(result, function (indexInArray, report) {
            option.push({ value: report.reportName, label: report.reportName });
          });

          self.optionReportName(option);
        });

        
      }

      self.changeListenerElementDropdown = function (event, current) {
        var optionValue = event.detail;
        var id = optionValue.value;
        self.getInputValues((result) => {
          var option = [];
          $.each(result, function (indexInArray, inputValue) {
            option.push({ value: inputValue.inputValueId, label: inputValue.name });
          });

          self.optionInputValue(option);
        }, id);
      }

      self.reportParameters = ko.observableArray();
      self.changeListenerReport = function (event, current) {
        var optionValue = event.detail;
        var name = optionValue.value;

        self.getReport((result) => {
          var parameters = [];
          $.each(result, function (indexInArray, report) {
            if (name == report.reportName) {
              if(report.parameter1) parameters.push({parameter: 1, name: report.parameter1, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter2) parameters.push({parameter: 2, name: report.parameter2, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter3) parameters.push({parameter: 3, name: report.parameter3, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter4) parameters.push({parameter: 4, name: report.parameter4, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter5) parameters.push({parameter: 5, name: report.parameter5, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter6) parameters.push({parameter: 6, name: report.parameter6, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter7) parameters.push({parameter: 7, name: report.parameter7, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter8) parameters.push({parameter: 8, name: report.parameter8, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter9) parameters.push({parameter: 9, name: report.parameter9, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter10) parameters.push({parameter: 10, name: report.parameter10, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter11) parameters.push({parameter: 11, name: report.parameter11, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter12) parameters.push({parameter: 12, name: report.parameter12, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter13) parameters.push({parameter: 13, name: report.parameter13, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter14) parameters.push({parameter: 14, name: report.parameter14, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter15) parameters.push({parameter: 15, name: report.parameter15, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter16) parameters.push({parameter: 16, name: report.parameter16, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter17) parameters.push({parameter: 17, name: report.parameter17, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter18) parameters.push({parameter: 18, name: report.parameter18, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter19) parameters.push({parameter: 19, name: report.parameter19, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
              if(report.parameter20) parameters.push({parameter: 20, name: report.parameter20, category: '', personDetails: '', elementId: '', inputValueId: '', inputValueOption: [], static: ''});
            }
          });

          self.reportParameters(parameters);
        });
      }

      self.changeListenerDirection = function () {
        self.createParameterCategory("");
        self.createPersonDetails("");
        self.createElement("");
        self.createInputValue("");
        self.createReportName("");
        self.createParameter1("");
        self.createParameter2("");
        self.createParameter3("");
        self.createParameter4("");
        self.createParameter5("");
      }

      self.changeListenerCategory = function () {
        self.createPersonDetails("");
        self.createElement("");
        self.createInputValue("");
        self.createReportName("");
        self.createParameter1("");
        self.createParameter2("");
        self.createParameter3("");
        self.createParameter4("");
        self.createParameter5("");
      }

      self.changeListenerReportCategory = function (event, current) {
        var optionValue = event.detail.value
        // console.log(current.data);
        // console.log(self.reportParameters()[current.index]);
        var temp = [];
        $.each(self.reportParameters(), function (i, newParameter) { 
           if(i == current.index) newParameter.category = optionValue;
           temp.push(newParameter);
        });
        self.reportParameters([]);
        self.reportParameters(temp);
      }

      self.changeListenerReportElement = function (event, current) {
        var optionValue = event.detail.value
        self.currentIndex = ko.observable(current.index);
        self.getInputValues((result) => {
          var option = [];
          $.each(result, function (indexInArray, inputValue) {
            option.push({ value: inputValue.name, label: inputValue.name });
          });

          var temp = [];
          $.each(self.reportParameters(), function (i, newParameter) { 
            if(i == self.currentIndex()) newParameter.inputValueOption = option;
            temp.push(newParameter);
          });
          self.reportParameters([]);
          self.reportParameters(temp);
        }, optionValue);
      }

      self.setParameterList = ko.pureComputed(function() {
        return self.reportParameters();
      }, this);

      /**
       * Date Formatting
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      function currentDate() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        return mm + '/' + dd + '/' + yyyy;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      self.advanceDisable = ko.observable();

      self.gotoIndex = function (json_data = {}) {
        oj.Router.rootInstance.store(json_data);
        oj.Router.rootInstance.go("indexFastFormula");
      }.bind(this);

      /**
       * Create Fast Formula
       */
      self.createName = ko.observable();
      self.createType = ko.observable();
      self.createStartDate = ko.observable();
      self.createEndDate = ko.observable();
      self.fastFormulaText = ko.observable();

      // parameter
      self.createParameterName = ko.observable();
      self.createParameterDirection = ko.observable();
      self.createParameterType = ko.observable();
      self.createParameterCategory = ko.observable();

      self.createPersonDetails = ko.observable();
      self.createElement = ko.observable();
      self.createInputValue = ko.observable();
      self.createReportName = ko.observable();
      self.createParameter1 = ko.observable();
      self.createParameter2 = ko.observable();
      self.createParameter3 = ko.observable();
      self.createParameter4 = ko.observable();
      self.createParameter5 = ko.observable();

      function clearParameter() {
        // parameter
        self.createParameterName("");
        self.createParameterDirection("");
        self.createParameterType("");
        self.createParameterCategory("");

        self.createPersonDetails("");
        self.createElement("");
        self.createInputValue("");
        self.createReportName("");
        self.createParameter1("");
        self.createParameter2("");
        self.createParameter3("");
        self.createParameter4("");
        self.createParameter5("");
      }

      clearParameter();

      self.setDataFastFormula = function (route_data) {
        self.createName(route_data.name);
        self.createType(route_data.type);
        self.createStartDate(dateConverter(route_data.startDate));
        self.createEndDate((route_data.endDate) ? dateConverter(route_data.endDate) : '');

        self.setFomulaHeader();
      }

      self.createFastFormula = function () {
        if(self.compileStatus()){
          var payload = {
            "effective_start_date": self.createStartDate(),
            "effective_end_date": (self.createEndDate()) ? self.createEndDate() : "",
            "formula_type_id": self.createType(),
            "formula_name": self.createName(),
            "formula_text": self.fastFormulaHeader()+" "+self.fastFormulaText(),
            "compile_flag": "",
            "enterprise_id": "",
            "legislative_data_group_id": "",
            "legislation_code": "",
            "object_version_number": "",
            "last_update_date": "",
            "last_updated_by": "",
            "last_update_login": "",
            "created_by": "user",
            "creation_date": currentDate(),
            "module_id": "",
            "parameteList": self.arrayParameter()
          }
          console.log(payload);
          var success = function (data) {
            self.gotoIndex({ action: 'create', success: true, message: 'Fast Formula is created.' });
            // self.notificationMessage('success',['Object Group is created.']);
          }
  
          var fail = function () {
            console.log("fail");
            self.notificationMessage('error', ['Something went wrong.']);
          }
  
          var url = "payroll/rest/FastFormulaRest/addFastFormula";
          services.addGeneric(url, payload).then(success, fail);
        }else{
          self.notificationMessage('error',['Have your formula successfully compiled first.']);
        }
      }

      //switcher

      this.selectedItem = ko.observable('home');
      this.currentEdge = ko.observable('top');
      this.valueChangedHandler = function (event) {
        var value = event.detail.value;
        var previousValue = event.detail.previousValue;
        var demoContianer = document.getElementById('demo-container');
        demoContianer.className = demoContianer.className.replace('demo-edge-' + previousValue, 'demo-edge-' + value);
      };
      // Populate some content
      var tabContentNodes = document.querySelectorAll('.demo-tab-content');
      var textNode = document.createElement('p');
      textNode.appendChild(document.createTextNode('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pharetra, risus ac interdum sollicitudin, sem erat ultrices ipsum, eget vehicula nibh augue sollicitudin ligula. Sed ullamcorper cursus feugiat. Mauris tristique aliquam dictum. Nulla facilisi. Nulla ut sapien sapien. Phasellus tristique arcu id ipsum mattis id aliquam risus sollicitudin.'));
      Array.prototype.forEach.call(tabContentNodes, function (tabContent) {
        for (var i = 0; i < 7; i++) {
          tabContent.appendChild(textNode.cloneNode(true));
        }
      });


      /**
       * PARAMETER
       */

      // set table values
      self.arrayParameter = ko.observableArray([]);
      self.dataProviderCreateParameter = ko.observableArray();

      // self.setTableCreateParameter = function(){
      //   self.getCreateParameterData((result)=>{
      //     var temp = [];
      //     $.each(self.arrayCreateParameter(), function (index, parameter) {
      //       temp.push({
      //         num: ++index,
      //         name: parameter.name,
      //         direction: parameter.direction,
      //         type: parameter.type,
      //         length: parameter.length,
      //         option: parameter.option,
      //         source: parameter.source,
      //         action: parameter.elementSetId,
      //       });
      //     });

      //     self.tableDataCreateParameter(result);
      //     self.newArrayCreateParameter(temp); 
      //   });    
      // }

      self.dataProviderCreateParameter = new ArrayDataProvider(self.arrayParameter, { keyAttributes: 'action', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      var _checkValidationGroupParameter = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };


      self.createParameter = function () {
        var valid = _checkValidationGroupParameter("create-tracker");
        if (valid) {
          var parameters = self.reportParameters();
          var pram_source = [];
          var pram_element = [];
          var pram = [];
          // $.each(self.reportParameters(), function (i, parameter) { 
          for (i = 0; i <= 5; i++) {  
            if(typeof parameters[i] != 'undefined'){
              if(parameters[i].category == 'PERSON_DETAILS'){
                pram_source.push(parameters[i].category);
                pram_element.push('');
                pram.push(parameters[i].personDetails);
              }
              else if(parameters[i].category == 'ELEMENT'){
                pram_source.push(parameters[i].category);
                $.each(self.element(), function (ii, element) { 
                  if(parameters[i].elementId == element.elementTypeId) pram_element.push(element.elementName);
                });
                pram.push(parameters[i].inputValueId);
              }
              else if(parameters[i].category == 'STATIC'){
                pram_source.push(parameters[i].category);
                pram_element.push('');
                pram.push(parameters[i].static);
              }
            }else{
              pram_source.push('');
              pram_element.push('');
              pram.push('');
            }
          }
          // });

          var tempInsert = self.arrayParameter();
          tempInsert.push({
            "name": self.createParameterName(),
            "parameter_type": self.createParameterType(),
            "length": "9999",
            "directions": self.createParameterDirection(),
            "parameter_category": self.createParameterCategory(),
            "reportName": self.createReportName(),
            "pram1": pram[0],
            "pram2": pram[1],
            "pram3": pram[2],
            "pram4": pram[3],
            "pram5": pram[4],
            "PRAM1_SOURCE": pram_source[0],
            "PRAM1_ELEMENT": pram_element[0],
            "PRAM2_SOURCE": pram_source[1],
            "PRAM2_ELEMENT": pram_element[1],
            "PRAM3_SOURCE": pram_source[2],
            "PRAM3_ELEMENT": pram_element[2],
            "PRAM4_SOURCE": pram_source[3],
            "PRAM4_ELEMENT": pram_element[3],
            "PRAM5_SOURCE": pram_source[4],
            "PRAM5_ELEMENT": pram_element[4],
            "source": "",
            "elementName": self.createElement(),
            "inputValue": self.createInputValue(),
            "formula_id": ""
          });

          clearParameter();
          self.arrayParameter(tempInsert);
          self.setFomulaHeader();
          // self.cancelListenerCreateParameter();
          self.gotoMain();
        }
      }

      self.removeCreateParameter = function () {
        var temp = self.arrayParameter();
        temp.splice(rowIndexParameter, 1);
        self.arrayParameter(temp);
        self.setFomulaHeader();
      }

      // create parameter modal
      self.startAnimationListenerCreateParameter = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerCreateParameter = function () {
        var popup = document.getElementById('create-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerCreateParameter = function () {
        var popup = document.getElementById('create-modal');
        popup.close();
      };


      // table listener
      var rowIndexParameter;
      var rowDataCreateParameter;
      self.tableDataCreateParameter = ko.observableArray(self.arrayParameter());

      self.selectedRowKey = ko.observable();
      self.selectedIndex = ko.observable();
      self.tableSelectionListenerCreateParameter = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow;
        if (currentRow) {
          self.selectedRowKey(currentRow['rowKey']);
          self.selectedIndex(currentRow['rowIndex'])
          rowIndexParameter = self.selectedIndex();
          rowDataCreateParameter = self.tableDataCreateParameter()[rowIndexParameter];
          console.log(rowIndexParameter);
          console.log(rowDataCreateParameter);
        }
      };

      self.gotoInsertParameter = function(){
        $("#body-wrap").hide();
        $("#insert-parameter").show();
      }

      self.gotoMain = function(){
        $("#body-wrap").show();
        $("#insert-parameter").hide();
      }


      /**
       * Formula
       */
      self.fastFormulaHeader = ko.observable("");

      self.setFomulaHeader = function(){
        var parameters = "";
        var parameterList = self.arrayParameter();
        $.each(parameterList, function (i, parameter) { 
          var formatParameter = parameter.name.trim().replace(/\s/g,'')+" "+parameter.directions+" "+parameter.parameter_type;
          var new_line = (parameterList.length != (i+1))? ",\n" : "\n";
          parameters = parameters+formatParameter+new_line;
        });

        var temp = self.fastFormulaHeader();
        temp = "create or replace PROCEDURE "+self.createName().trim().replace(/\s/g,'')+"(\n"+parameters+")";
        self.fastFormulaHeader(temp);
      }      

      // Compile
      self.compileProccess = ko.observable(false);
      self.compileStatus = ko.observable(false);
      self.compileFastFormula = async function () {
        self.compileProccess(true);

        var payload = {
          "formula_text": self.fastFormulaHeader()+" "+self.fastFormulaText()
        }

        var success = function (data) {
          console.log(JSON.parse(data));
          var res = JSON.parse(data);

          if(res.Status == "ERROR"){
            self.compileStatus(false);
            self.notificationMessage('error',[res.ErrorCode]);
          }else{
            self.compileStatus(true);
            self.notificationMessage('success',['Compile successful!']);
          }
          
        }

        var fail = function () {
          console.log("fail");
          self.notificationMessage('error', ['Something went wrong.']);
        }

        var url = "payroll/rest/FastFormulaRest/compile/";
        await services.addGeneric(url, payload).then(success, fail);

        self.compileProccess(false);
      }


      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {
        accUtils.announce('Fast Formula form page loaded.');
        document.title = "Fast Formula Form";
        // Implement further logic if needed
        //$("li#ui-id-14").removeClass("oj-default").addClass("oj-selected");

        route_data = oj.Router.rootInstance.retrieve();
        self.setDataFastFormula(route_data);
        // self.setTableCreateParameter();
        setFastFormulaDropdowns();

      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return CreateFastFormulaViewModel;
  }
);
