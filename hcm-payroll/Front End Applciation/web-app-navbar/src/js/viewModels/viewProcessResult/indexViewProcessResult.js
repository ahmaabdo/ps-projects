define(['ojs/ojcore', 'appController', 'config/services',
  'knockout', 'jquery', 'ojs/ojconverter-datetime',
  'ojs/ojmessaging', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojdialog',
  'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset',
  'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojrouter', 'ojs/ojlistview'
],
  function (oj, app, services, ko, $, DateTimeConverter) {

    function IndexViewProcessResultViewModel() {
      var self = this;

      self.notifMessage = ko.observableArray();
      self.columnArrayInputs = ko.observableArray();
      self.columnArrayProcess = ko.observableArray();
      self.columnArrayEmployees = ko.observableArray();
      self.columnArrayEntries = ko.observableArray();
      self.advanceDisable = ko.observable();
      self.empNameSearchValue = ko.observable("");
      self.empNumberSearchValue = ko.observable("");
      self.nameSearchValue = ko.observable("");
      self.selectedProcessValue = ko.observable("");
      self.selectedEmployeeValue = ko.observable("");
      self.selectedEntriesValue = ko.observable("");
      self.periodSearchValue = ko.observable("");
      self.typeSearchValue = ko.observable("");
      self.processDataLoaded = ko.observable(false);
      self.searchInputStatus = ko.observable(true);
      self.searchEmployeeStatus = ko.observable(false);
      self.processResultValuesArray = ko.observable([]);
      self.processResultDetails = ko.observableArray([]);
      self.employeeDetailsArr = ko.observableArray([]);
      self.entriesDetailsArr = ko.observableArray([]);
      self.processResultDetailsProvider = ko.observable(new oj.ArrayDataProvider([], {}));
      self.employeeResultDetailsProvider = ko.observable(new oj.ArrayDataProvider([], {}));
      self.employeeEntriesProvider = ko.observable(new oj.ArrayDataProvider([], {}));
      self.inputsDataProvider = ko.observable(new oj.ArrayDataProvider([], {}));

      self.typeSearchOptions = ko.observableArray([
        { label: 'Run', value: 'Run' },
        { label: 'Costing', value: 'Costing' },
        { label: 'Prepayment', value: 'Prepayment' },
        { label: 'Transfer to GL', value: 'Transfer to GL' },
      ]);


      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));


      self.messagesDataprovider = ko.observableArray();
      self.notificationMessage = function (type, messages) {
        self.notifMessage([{
          severity: type,
          summary: messages[0],
          detail: '',
          autoTimeout: 5000
        }]);
      }
      self.messagesDataprovider = new oj.ArrayDataProvider(self.notifMessage);

      self.dataproviderProcessResultValues = ko.observable(new oj.ArrayDataProvider(self.processResultValuesArray(), {}));


      self.getPersonDetails = ko.computed(() => {
        if (app.personDetails().length && self.processDataLoaded()) {

          for (var i in self.employeeDetailsArr()) {
            var personDetails = app.personDetails().find(obj => obj.PERSON_ID == self.employeeDetailsArr()[i].assignmentId);
            if (personDetails) {
              self.employeeDetailsArr()[i].personName = personDetails.DISPLAY_NAME;
              self.employeeDetailsArr()[i].personNumber = personDetails.PERSON_NUMBER;
            }
          }
        }
      });


      self.tableSelectionListenerProcess = function (event) {
        var currentRow = event.detail.currentRow
        if (currentRow)
          self.selectedProcessValue(self.processResultDetails().find(obj => obj.flowName == currentRow['rowKey']));
        else
          self.notificationMessage('error', ['Something went wrong.']);

        console.log(self.selectedProcessValue())
      };

      self.tableSelectionListenerEmployee = function (event) {
        var currentRow = event.detail.currentRow
        if (currentRow)
          self.selectedEmployeeValue(self.employeeDetailsArr().find(obj => obj.personNumber == currentRow['rowKey']));
        else
          self.notificationMessage('error', ['Something went wrong.']);


        console.log(self.selectedEmployeeValue())
      };

      self.tableSelectionListenerEntries = function (event) {
        var currentRow = event.detail.currentRow
        if (currentRow)
          self.selectedEntriesValue(self.entriesDetailsArr().find(obj => obj.elementName == currentRow['rowKey']));
        else
          self.notificationMessage('error', ['Something went wrong.']);
      };

      self.getProcessResult = async function () {
        var success = function (result) {
          if (result) {
            var index = 0;
            for (var i in result)
              result[i].action = ++index;

            self.processResultDetails.push(result);
            self.employeeDetailsArr(result.employeeDetails);

            //TODO: remove this line
            self.processResultDetailsProvider(new oj.ArrayDataProvider(self.processResultDetails, {}));
          }
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetProcessResult";
        await services.getGeneric(url).then(success, fail);
        self.processDataLoaded(true);
        self.searchInputStatus(false);
      }

      self.viewDetails = function (event) {
        $("#page-1").hide();
        $("#page-3").hide();
        $("#page-2").fadeIn();

        self.employeeResultDetailsProvider(new oj.ArrayDataProvider(self.employeeDetailsArr, { keyAttributes: 'personNumber' }));
      };

      self.viewEmployeeDetails = function (event) {

        for (var i in self.employeeDetailsArr()) {
          if (self.employeeDetailsArr()[i].assignmentId == self.selectedEmployeeValue().assignmentId) {
            self.entriesDetailsArr(self.employeeDetailsArr()[i].elementDetails);
          }
        }

        for (var i in self.entriesDetailsArr())
          self.entriesDetailsArr()[i].payValue = self.entriesDetailsArr()[i].inputs.find(obj => obj.inputValueName == 'Pay Value').inputValue;


        self.employeeEntriesProvider(new oj.ArrayDataProvider(self.entriesDetailsArr, { keyAttributes: 'elementName' }));
        var action = event.target.value;
        if (action == "edit") {
          $("#page-1").hide();
          $("#page-3").fadeIn();
          $("#page-2").hide();
        } else {
          console.log("remove")
        }
      };

      self.closeDialog = function (event) {
        document.getElementById('modalDialog1').close();
      };

      self.viewEntries = function (event) {
        document.getElementById('modalDialog1').open();
        self.inputsDataProvider(new oj.ArrayDataProvider(self.selectedEntriesValue().inputs, {}));
      };

      self.resetSearchBtnAction = function () {
        self.nameSearchValue("");
        self.periodSearchValue("");
        self.typeSearchValue("");
        self.processResultDetailsProvider(new oj.ArrayDataProvider([], {}));
      };

      self.resetEmpSearchBtnAction = function () {
        self.empNameSearchValue("");
        self.empNumberSearchValue("");
        self.employeeResultDetailsProvider(new oj.ArrayDataProvider(self.employeeDetailsArr, { keyAttributes: 'personNumber' }));
      };


      self.searchBtnAction = function () {
        if (!self.nameSearchValue() && !self.periodSearchValue() && !self.typeSearchValue()) {
          self.notificationMessage('error', ['Please enter a value']);
          return;
        }

        let nameSearchValue = new RegExp(self.nameSearchValue().toUpperCase() + ".*")
        let periodSearchValue = new RegExp(self.periodSearchValue().toUpperCase() + ".*")
        let typeSearchValue = new RegExp(self.typeSearchValue().toUpperCase() + ".*")

        var searchByNumber = [];
        for (var index in self.processResultDetails()) {

          if ((self.processResultDetails()[index].flowName).toUpperCase().match(nameSearchValue) &&
            (self.processResultDetails()[index].timePeriodName).toUpperCase().match(periodSearchValue) &&
            (self.processResultDetails()[index].actionType).toUpperCase().match(typeSearchValue)) {
            searchByNumber.push(self.processResultDetails()[index]);
          }
        }

        self.processResultDetailsProvider(new oj.ArrayDataProvider(searchByNumber, { keyAttributes: 'flowName' }));
        if (searchByNumber.length < 1)
          self.notificationMessage('error', ['No result found']);
      };



      self.empSearchBtnAction = function () {
        if (!self.empNameSearchValue() && !self.empNumberSearchValue()) {
          self.notificationMessage('error', ['Please enter a value']);
          return;
        }

        let empNameSearchValue = new RegExp(self.empNameSearchValue().toUpperCase() + ".*")

        var searchByNumber = [];
        for (var index in self.employeeDetailsArr()) {
          if (self.employeeDetailsArr()[index].personName) {
            if (self.empNumberSearchValue()) {
              if ((self.employeeDetailsArr()[index].personName).toUpperCase().match(empNameSearchValue) &&
                self.employeeDetailsArr()[index].personNumber == self.empNumberSearchValue())
                searchByNumber.push(self.employeeDetailsArr()[index]);

            }
            else {
              if ((self.employeeDetailsArr()[index].personName).toUpperCase().match(empNameSearchValue))
                searchByNumber.push(self.employeeDetailsArr()[index]);

            }
          }
        }

        self.employeeResultDetailsProvider(new oj.ArrayDataProvider(searchByNumber, { keyAttributes: 'personNumber' }));
        if (searchByNumber.length < 1)
          self.notificationMessage('error', ['No result found']);
      };

      //Process Data Back button
      self.gotoPage1 = function () {
        $("#page-2").hide();
        $("#page-1").fadeIn();
        $("#page-3").hide();
      }.bind(this);

      self.gotoPage2 = function () {
        $("#page-3").hide();
        $("#page-2").fadeIn();
        $("#page-1").hide();
      }.bind(this);

      self.back = function () {
        app.router.go('payrollResultPages')
      };

      self.indexViewProcessResult = function () {
        app.router.go('indexViewProcessResult');
      };

      self.indexReport = function () {
        app.router.go('indexReport');
      };

      self.connected = function () {
        app.collapseSideMenu();
        self.getProcessResult();
      };

      self.columnArrayEntries([
        {
          "headerText": "Balance Name", "field": "elementName"
        },
        {
          "headerText": "Pay Value", "field": "payValue"
        },
        {
          "headerText": "Action", "field": "action", "template": "actionTemplate"
        }
      ]);

      self.columnArrayEmployees([
        {
          "headerText": "Employee Name", "field": "personName"
        },
        {
          "headerText": "Employee Number", "field": "personNumber"
        },
        {
          "headerText": "Status", "field": "lineStatus", "template": "status"
        },
        {
          "headerText": "Action", "field": "action", "template": "actionTemplate"
        }
      ]);

      self.columnArrayProcess([
        {
          "headerText": "Payroll Name", "field": "flowName"
        },
        {
          "headerText": "Type", "field": "actionType"
        },
        {
          "headerText": "Period", "field": "timePeriodName"
        },
        {
          "headerText": "Status", "field": "actionStatus", "template": "status"
        },
        {
          "headerText": "Action", "field": "action", "template": "actionTemplate"
        }
      ]);

      self.columnArrayInputs([
        {
          "headerText": "Input Name", "field": "inputValueName"
        },
        {
          "headerText": "Value", "field": "inputValue"
        },
        {
          "headerText": "Unit of Measure", "field": "uom"
        }
      ]);

    }

    return IndexViewProcessResultViewModel;
  }
);