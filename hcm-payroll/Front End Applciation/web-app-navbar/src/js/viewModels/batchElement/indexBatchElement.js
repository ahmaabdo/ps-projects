/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['accUtils', 'appController', 'text!data/objectGroup.json', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
'ojs/ojconverter-datetime',
'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher'
],
 function(accUtils,app, objectGroupData, services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {

    function IndexBatchElementViewModel() {
      var self = this;
      /**
       * Notification Messages
       */
      this.notifMessage = ko.observableArray();
      this.messagesDataprovider = ko.observableArray();
      this.notificationMessage = function(type, messages){
        var msg = [];
        switch (type) {
          case 'error':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'error',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;

          case 'warning':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'warning',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
              
          case 'success':
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
          default:
              $.each(messages, function(index, message){
                msg.push({
                  severity: 'confirmation',
                  summary: message,
                  detail: '',
                  autoTimeout: parseInt(0, 10)
                })
              });
              break;
        }

        this.notifMessage(msg);
      }




      self.back = function(){
        app.router.go('configrationPages')
      };
      self.elementEntries = function(){
        app.router.go('elementEntries')
      };
      self.indexBatchElement = function(){
        app.router.go('indexBatchElement')
      };
      /////////////////

      
      this.messagesDataprovider = new ArrayDataProvider(this.notifMessage);

      /**
       * Search
       */
      self.advanceDisable = ko.observable();
      
      // keyword search
      self.keyword = ko.observable();
      self.searchKeyword = function(){
        alert("search keyword statement");
      }.bind(this);

      // advance search
      self.searchName = ko.observable();
      self.searchPersonNumber = ko.observable();
      self.searchStartDate = ko.observable();
      self.searchEndDate = ko.observable();

      self.searchAdvance = function(){
        alert("search advance statement");
      }.bind(this);


      /**
       * Date Format
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      
      function currentDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        return yyyy+'-'+mm+'-'+dd;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      self.currentDate = ko.observable(currentDate());

      /**
       * Call API
       */

      // Element list
      self.getElementData = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm";
        services.getGeneric(url).then(success, fail);
      };

      // Batch Element List
      self.getBatchElementData = async function (callback){
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/elementBatch/GetElementBatch";
        services.getGeneric(url).then(success, fail);
      }

      // Input value list related to Element selected
      self.getInputValues = function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetInputValue/" + id;
        services.getGeneric(url).then(success, fail);
      }


      // Set batch element option
      self.elementOptions = ko.observableArray();
      self.employeeOptions = ko.observableArray();

      function setBatchElementDropdowns() {
        self.getElementData((result) => {
          var options = [];
          $.each(result, function (index, element) {
            options.push({ value: element.elementTypeId, label: element.elementName });
          });
          self.elementOptions(options);
        });
      }

      // create object group pop-up
      self.createName = ko.observable();
      self.createElement = ko.observable();
      self.createStartDate = ko.observable();
      self.createEndDate = ko.observable();

      self.createBatchElement = function(){
        var valid = _checkValidationGroupCreateBatchElement("create-tracker");
        if(valid){
          var json_data = {
            name: self.createName(),
            element: self.createElement(),
            startDate: self.createStartDate(),
            endDate: self.createEndDate()
          };
          console.log(json_data);

          oj.Router.rootInstance.store(json_data);
          oj.Router.rootInstance.go("createBatchElement");
        }
      }

      // create object group modal
      self.startAnimationListenerCreateBatchElement = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'create-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenerBatchElement = function () {
        var popup = document.getElementById('create-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerBatchElement = function () {
        var popup = document.getElementById('create-modal');
        popup.close();
      };

    
      /**
       * table
       */
      self.gotoEditBatchElement = function(){
        oj.Router.rootInstance.store(rowData);
        oj.Router.rootInstance.go("editBatchElement");
      }

      self.removeRowBatchElement = function(){
        self.arrayBatchElement().splice(thisRow,1);
        self.setTableBatchElement();
      }

      // set table values
      self.arrayBatchElement = ko.observableArray();
      self.dataProviderBatchElement = ko.observableArray();
      
      self.setTableBatchElement = function(){
        self.getBatchElementData((result)=>{
          var temp = [];
          $.each(result, function (index, batchElement) {
            temp.push({
              num: ++index,
              name: batchElement.DESCRIPTION,
              element: batchElement.ELEMENT_NAME,
              startDate: dateConverter(batchElement.CREATION_DATE),
              endDate: batchElement.endDate,
              action: batchElement.ID,
            });
          });

          self.arrayBatchElement(temp);    
        });
      }

      self.dataProviderBatchElement = new ArrayDataProvider(self.arrayBatchElement, { keyAttributes: 'num', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });
      
      self.elementAction = function(){
        var action = event.target.value;
        if(action == "edit") self.gotoEditBatchElement(rowDataBatchElement);
        if(action == "remove") self.removeRowBatchElement();
      }.bind(this);

      // table listener
      var thisRow;
      var rowDataBatchElement;
      self.tableDataBatchElement = ko.observableArray(self.arrayBatchElement());
      
      self.selectedRowKey = ko.observable();
      self.selectedIndex = ko.observable();
      self.tableSelectionListenerBatchElement = function (event) {
        
          var data = event.detail;
          var currentRow = data.currentRow
          self.selectedRowKey(currentRow['rowKey']);
          self.selectedIndex(currentRow['rowIndex'])
          thisRow = self.selectedIndex();
          rowDataBatchElement = self.tableDataBatchElement()[thisRow]
          console.log(thisRow);
          console.log(rowDataBatchElement);
      };

      // validation
      var _checkValidationGroupCreateBatchElement = function (id) { 
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };
      

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function() {
        app.collapseSideMenu();
        accUtils.announce('Batch Element page loaded.');
        document.title = "Batch Element";
        // Implement further logic if needed
        //$(".oj-navigationlist-item-element").removeClass("oj-selected").addClass("oj-default");

        route_data = oj.Router.rootInstance.retrieve();
        if(typeof route_data != "undefined" && route_data.success){
          self.notificationMessage('success',[route_data.message]);
          oj.Router.rootInstance.store({});
        }
        
        setBatchElementDropdowns();
        self.setTableBatchElement();
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return IndexBatchElementViewModel;
  }
);


