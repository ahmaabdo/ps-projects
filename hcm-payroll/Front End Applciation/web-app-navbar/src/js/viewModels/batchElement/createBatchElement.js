/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['accUtils', 'config/services', 'knockout', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojtimezonedata'
],
  function (accUtils, services, ko, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {

    function CreateBatchElementViewModel() {
      var self = this;

      /**
       * Notification Messages
       */
      this.notifMessage = ko.observableArray();
      this.messagesDataprovider = ko.observableArray();
      this.notificationMessage = function (type, messages) {
        var msg = [];
        switch (type) {
          case 'error':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'error',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'warning':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'warning',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;

          case 'success':
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
          default:
            $.each(messages, function (index, message) {
              msg.push({
                severity: 'confirmation',
                summary: message,
                detail: '',
                autoTimeout: parseInt(0, 10)
              })
            });
            break;
        }
        this.notifMessage(msg);
      }
      this.messagesDataprovider = new ArrayDataProvider(this.notifMessage);

      self.advanceDisable = ko.observable();

      self.gotoIndex = function (json_data={}) {
        oj.Router.rootInstance.store(json_data);
        oj.Router.rootInstance.go("indexBatchElement");
      };

      /**
       * Date Format
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }


      function currentDate(dformat) {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        var d = ""; 
        if(dformat === "dd-MM-yyyy") d = dd + '-' + mm + '-' + yyyy;
        else d= yyyy + '-' + mm + '-' + dd;
        
        return d;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));

      self.currentDate = ko.observable(currentDate());

      /**
       * Call API
       */

      // Element list
      self.getElementData = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm";
        services.getGeneric(url).then(success, fail);
      };

      self.getElementDataById = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetElementForm/" + id;
        services.getGeneric(url).then(success, fail);
      }

      self.getEmployee = async function (callback) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetEmployee";
        services.getGeneric(url).then(success, fail);
      };

      // Input value list related to Element selected
      self.getInputValues = async function (callback, id) {
        var success = function (result) {
          if (callback) callback(result);
        }
        var fail = function () {
          console.log("fail");
        }

        var url = "payroll/rest/AppsProRest/GetInputValue/" + id;
        services.getGeneric(url).then(success, fail);
      }


      // Set batch element option
      self.elementOptions = ko.observableArray();
      self.employeeList = ko.observableArray();
      self.employeeOptions = ko.observableArray();
      self.employeeLoading = ko.observable(false);

      function setBatchElementDropdowns() {
        self.employeeLoading(true);

        self.getElementData((result) => {
          var options = [];
          $.each(result, function (index, element) {
            options.push({ value: element.elementTypeId, label: element.elementName });
          });
          self.elementOptions(options);
        });

        self.getEmployee((result) => {
          var options = [];
          $.each(result, function (index, employee) {
            // options.push({ value: employee.PERSON_NUMBER+'-'+employee.DISPLAY_NAME, label: employee.PERSON_NUMBER });
            options.push({ value: employee.PERSON_NUMBER, label: employee.DISPLAY_NAME.trim()+" ( "+employee.PERSON_NUMBER+" )" });
          });
          self.employeeOptions(options);
          self.employeeList(result);
          self.employeeLoading(false);
        });
      }

      // self.employeeSearch = function(){
      //   var employeeList =  self.employeeList();
      //   var val = ($("#oj-combobox-input-2").val()).toLowerCase();
      //   var newOption = [];
        
      //   $.each(employeeList, function (i, employee) {
      //     var flag = 0;
      //     var personNumber = (typeof employee.PERSON_NUMBER == "string")? employee.PERSON_NUMBER.toLowerCase() : employee.PERSON_NUMBER;
      //     var personName = (employee.DISPLAY_NAME).toLowerCase(); 

      //     if(personNumber.search(val) >= 0) flag++;
      //     if(personName.search(val) >= 0) flag++;

      //     if(flag) newOption.push({ value: employee.PERSON_NUMBER, label: employee.DISPLAY_NAME+" ("+employee.PERSON_NUMBER+")" });
      //   });
      //   self.employeeOptions([]);
      //   self.employeeOptions(newOption);
      // }

      // Define Input Values
      self.inputValueDate = ko.observableArray([]);
      self.inputValueCharacter = ko.observableArray([]);
      self.inputValueNumberOrMoney = ko.observableArray([]);

      function defineInputValues(id) {
        self.getInputValues((result) => {
          var date = [];
          var character = [];
          var numberOrMoney = [];

          $.each(result, function (i, inputValue) {
            if (inputValue.uom === 'date') {
              inputValue.defaultValue = (inputValue.defaultValue) ? dateConverter(inputValue.defaultValue, true) : "";
              date.push(inputValue);
            }
            if (inputValue.uom === 'character') {

              character.push(inputValue);
            }
            if (inputValue.uom === 'money' || inputValue.uom === 'number') {
              inputValue.defaultValue = (inputValue.defaultValue) ? parseInt(inputValue.defaultValue) : 0;
              numberOrMoney.push(inputValue);
            }
          });

          self.inputValueDate(date);
          self.inputValueCharacter(character);
          self.inputValueNumberOrMoney(numberOrMoney);
        }, id);
      }

      function formatElementArray(arr){
        var new_array = [];
        $.each(arr, function (i, employee) { 
          var temp = [];
          $.each(employee.inputValues, function (ii, inputValue) {  
            inputValue = {
              "id": inputValue.inputValueId,
              "Value": (inputValue.uom === 'date')? dateConverter(inputValue.defaultValue) : inputValue.defaultValue
            }
            temp.push(inputValue);
          });
          employee.inputValues = temp;
          new_array.push(employee);
        });

        return new_array;
      }

      /**
       * Create Batch Element
       */

      self.batchElementName = ko.observable();
      self.employeeNumber = ko.observable();
      self.effectiveStartDate = ko.observable();
      self.effectiveEndDate = ko.observable();
      self.element = ko.observable();
      self.elementDetail = ko.observable();
      self.elementName = ko.observable();
      self.inputValues = ko.observableArray();

      self.setDataBatchElement = function (route_data) {
        self.batchElementName(route_data.name);
        self.element(route_data.element);
        self.effectiveStartDate(self.currentDate());
        self.effectiveEndDate("");

        self.getElementDataById((result)=>{
          var element = result[0];
          self.elementDetail(element);
          self.elementName(self.elementDetail().elementName);
        },route_data.element);
      }

      self.createBatchElement = function () {
        var valid = _checkValidationGroupCreateBatchElement("create-tracker");
        var elementArray = formatElementArray(self.arrayEmployee());

        if (valid && elementArray.length > 0) {
          var payload = {
            "CREATION_DATE" : self.currentDate(),
            "elementName": self.elementDetail().elementName,
            "CREATION_BY": "0",
            "DESCRIPTION": self.batchElementName(),
            "elementArray": elementArray
          }

          var success = function (data) {
            self.gotoIndex({action:'create', success:true, message:'Batch Element is created.'});
            // self.notificationMessage('success',['Object Group is created.']);
          }
  
          var fail = function () {
            console.log("fail");
            self.notificationMessage('error', ['Something went wrong.']);
          }
  
          var url = "payroll/rest/elementBatch/";
          services.addGeneric(url, payload).then(success, fail);
          
        }else{
          self.notificationMessage('error',['Required to insert Employees for this Batch Element.']);
        }
      }

      self.test = ko.observableArray();

      // EMPLOYEE TABLE
      self.arrayEmployee = ko.observableArray([]);
      self.newArrayEmployee = ko.observableArray([]);
      self.dataProviderEmployee = ko.observableArray();

      self.setBatchElementTable = function () {
        var temp = [];
        $.each(self.arrayEmployee(), function (index, employee) {
          temp.push({
            personNumber: employee.personNumber,
            personName: employee.personName,
            action: ""
          });
        });
        self.newArrayEmployee(temp);
      };

      self.dataProviderEmployee = new ArrayDataProvider(self.newArrayEmployee, { keyAttributes: 'personId', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // Employee Table listener
      var rowDataEmployee;
      self.tableDataEmployee = ko.observableArray(self.arrayEmployee());

      self.selectedRowKeyEmployee = ko.observable();
      self.selectedIndexEmployee = ko.observable();
      self.tableSelectionListenerEmployee = function (event) {
        var data = event.detail;
        var currentRow = data.currentRow
        if(currentRow){
          self.selectedRowKeyEmployee(currentRow['rowKey']);
          self.selectedIndexEmployee(currentRow['rowIndex'])
          var thisRow = self.selectedIndexEmployee();
          rowDataEmployee = self.tableDataEmployee()[thisRow];
          console.log(thisRow);
          console.log(rowDataEmployee);
        }
      };

      /**
       * Modal for insert
       */

      self.startAnimationListenerCreateBatchElement = function (event) {
        var ui = event.detail;
        if (event.target.id !== 'add-batchElement-modal') { return; }

        if (ui.action === 'open') {
          event.preventDefault();
          var options = { direction: 'top' };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        } else if (ui.action === 'close') {
          event.preventDefault();
          ui.endCallback();
        }
      };
      self.openListenertCreateBatchElement = function () {
        self.employeeNumber("");
        defineInputValues(self.element());
        var popup = document.getElementById('add-batchElement-modal');
        popup.open('#btnGo');
      };
      self.cancelListenerCreateBatchElement = function () {
        var popup = document.getElementById('add-batchElement-modal');
        popup.close();
      };

      // Insert Employee
      self.insertEmployee = function () {
        var valid = _checkValidationGroupCreateBatchElement("insert-tracker");

        if (valid) {
          var mergeArr = self.inputValueDate().concat(self.inputValueCharacter().concat(self.inputValueNumberOrMoney()));
          // var personArr = self.employeeNumber().split("-");
          var personArr = [];

          $.each(self.employeeList(), function (indexInArray, employee) { 
            if(employee.PERSON_NUMBER == self.employeeNumber()) personArr = employee;
          });
          console.log(personArr);
          self.arrayEmployee().push({
            "effectiveStartDate": currentDate("dd-MM-yyyy"),
            "effectiveEndDate": "",
            "elementTypeId": self.element(),
            "personId": personArr.PERSON_ID,
            "personNumber": personArr.PERSON_NUMBER,
            "personName": personArr.DISPLAY_NAME,
            "inputValues": mergeArr
          });

          self.setBatchElementTable();
          self.cancelListenerCreateBatchElement();
        }
      }

      self.removeEmployee = function(){
        var temp = self.arrayEmployee();
        temp.splice(self.selectedIndexEmployee(), 1);
        self.arrayEmployee(temp);

        self.setBatchElementTable();
      }

      // employee - input values table
      // self.arrayInputValues = ko.observableArray();
      // self.dataProviderInputValues = ko.observableArray();

      // self.setTableEmployees = ko.computed(function () {
      //   if(self.element()){
      //     self.getInputValues((result) => {
      //       var temp = [];
      //       $.each(result, function (index, inputValue) {
      //         temp.push({ 
      //           num: ++index, 
      //           name: inputValue.name,
      //           action: inputValue.inputValueId
      //          });
      //       });
      //       self.arrayInputValues(temp);
      //       self.tableData(temp);
      //     },self.element());
      //   }
      // });

      // self.dataProviderInputValues = new ArrayDataProvider(self.arrayInputValues, { keyAttributes: 'name', implicitSort: [{ attribute: 'id', direction: 'ascending' }] });

      // table listener
      // var rowData;
      // self.tableData = ko.observableArray();

      // self.selectedRowKey = ko.observable();
      // self.selectedIndex = ko.observable();
      // self.tableSelectionListenerInputValues = function (event) {
      //     var data = event.detail;
      //     var currentRow = data.currentRow
      //     self.selectedRowKey(currentRow['rowKey']);
      //     self.selectedIndex(currentRow['rowIndex'])
      //     var thisRow = self.selectedIndex();
      //     rowData = self.tableData()[thisRow];
      //     console.log(thisRow);
      //     console.log(rowData);
      // };

      // form validation
      var _checkValidationGroupCreateBatchElement = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      


      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {
        accUtils.announce('Batch Element form page loaded.');
        document.title = "Batch Element Form";
        // Implement further logic if needed
        setBatchElementDropdowns();
        self.setBatchElementTable();

        route_data = oj.Router.rootInstance.retrieve();
        self.setDataBatchElement(route_data);
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return CreateBatchElementViewModel;
  }
);



