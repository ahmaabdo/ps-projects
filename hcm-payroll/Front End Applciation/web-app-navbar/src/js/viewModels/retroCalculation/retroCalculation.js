/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['accUtils', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojtimezonedata', 'ojs/ojconverterutils-i18n', 'ojs/ojknockout', 'ojs/ojdatetimepicker'
],
  function (accUtils, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {
    function retroCalculationViewModel() {
      var self = this;

      /**
       * Date Formatting
       */

      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));
      ////////////////////
      self.back = function () {
        app.router.go('processPages')
      };
      ///////////////////////////////////


      self.processDate = ko.observable();
      self.payrollValue = ko.observable();
      self.payrollSearch = ko.observable();
      self.overridingDate = ko.observable();
      self.processConfigurationGroup = ko.observable();
      self.payrollRelationshipGroup = ko.observable();

      self.payrollFlow = ko.observable();
      self.gotoIndex = function () {
        oj.Router.rootInstance.go("process");
      }

      var _checkValidationGroupRetroCalculation = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.createRetroCalculation = function(){
        var valid = _checkValidationGroupRetroCalculation("create-tracker");
        var valid = _checkValidationGroupRetroCalculation("create-tracker-2");
        if(valid){
          var jsondata = {
            "payrollFlow": self.payrollFlow(),
            "processDate": self.processDate(),
            "payrollSearch": self.payrollSearch(),
            "overridingDate": self.overridingDate(),
            "processConfigurationGroup": self.processConfigurationGroup(),
            "payrollRelationshipGroup": self.payrollRelationshipGroup(),
          }
          console.log(jsondata);
          self.back();
        } 
      }

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = () => {
        accUtils.announce('Dashboard page loaded.', 'assertive');
        document.title = "Retro Calculation";

      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = () => {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = () => {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return retroCalculationViewModel;
  }
);

