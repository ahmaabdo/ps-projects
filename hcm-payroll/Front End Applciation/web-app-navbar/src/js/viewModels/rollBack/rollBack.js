/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['accUtils', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojtimezonedata', 'ojs/ojconverterutils-i18n', 'ojs/ojknockout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox'
],
  function (accUtils, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {
    function rollBackViewModel() {
      var self = this;


      this.selectVal1 = ko.observable();
      self.payrollFlow = ko.observable();
      self.payrollProcess = ko.observable();
      self.payrollRelationGroup = ko.observable();
      self.reason = ko.observable();
      self.gotoIndex = function () {
        oj.Router.rootInstance.go("process");
      }


      self.back = function () {
        app.router.go('processPages')
      };

      var _checkValidationGroupRollBack = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.createRollBack = function(){
        var valid = _checkValidationGroupRollBack("create-tracker");
        if(valid){
          var jsondata = {
            "PayrollFlow": self.payrollFlow(),
            "payrollProcess": self.payrollProcess(),
            "payrollRelationGroup": self.payrollRelationGroup(),
            "reason": self.reason(),
          }
          
          console.log(jsondata);
          self.back();
        }
        
      }
      self.connected = () => {
        accUtils.announce('Dashboard page loaded.', 'assertive');
        document.title = "RollBack";

      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = () => {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = () => {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return rollBackViewModel;
  }
);

