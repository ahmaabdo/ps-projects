/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['accUtils', 'config/services', 'knockout', 'appController', 'ojs/ojbootstrap', 'jquery', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojmessaging', 'ojs/ojarraydataprovider', 'ojs/ojanimation', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime',
  'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojformlayout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
  'ojs/ojtable', 'ojs/ojmenu', 'ojs/ojpopup', 'ojs/ojvalidationgroup', 'ojs/ojcheckboxset', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojmessages', 'ojs/ojtimezonedata', 'ojs/ojconverterutils-i18n', 'ojs/ojknockout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox'
],
  function (accUtils, services, ko, app, Bootstrap, $, ResponsiveUtils, ResponsiveKnockoutUtils, Message, ArrayDataProvider, AnimationUtils, ConverterUtilsI18n, DateTimeConverter) {
    function costingProcessViewModel() {
      var self = this;


      this.selectVal1 = ko.observable();

      var self = this;


      function dateConverter(date, iso = false) {
        var d = new Date(date);
        var new_date;

        if (iso) {
          new_date = ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(d)
        } else {
          new_date = ('0' + d.getDate()).slice(-2) + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + '-' + d.getFullYear();
        }
        return new_date;
      }

      self.dateFormat = ko.observable(new DateTimeConverter.IntlDateTimeConverter(
        {
          pattern: "dd-MM-yyyy"
        }));


      self.payrollFlow = ko.observable();
      self.scope = ko.observable();
      self.processStartDate = ko.observable();
      self.processEndDate = ko.observable();
      self.processValue = ko.observable();
      self.consolidationGroup = ko.observable();
      self.payrollValue = ko.observable();
      self.processConsolidationGroup = ko.observable();
      self.payrollStatutoryUnit = ko.observable();
      self.taxReportingUnit = ko.observable();
      self.department = ko.observable();
      self.location = ko.observable();
      self.payrollPeriod = ko.observable();
      self.optionScope = ko.observableArray([
        { value: 'Internet Explorer', label: 'Scope1' },
        { value: 'Firefox', label: 'Scope2' },
        { value: 'Chrome', label: 'Scope3' },
        { value: 'Opera', label: 'Scope4' }]);
      self.gotoIndex = function(){
        oj.Router.rootInstance.go("process");
      }
      self.gotoIndex = function () {
        oj.Router.rootInstance.go("process");
      }


      self.back = function () {
        app.router.go('processPages')
      };

      var _checkValidationGroupCostingProcess = function (id) {
        var tracker = document.getElementById(id);
        if (tracker.valid === "valid") {
          return true;
        }
        else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };

      self.createCostingProcess = function(){
        var valid = _checkValidationGroupCostingProcess("create-tracker");
        var valid = _checkValidationGroupCostingProcess("create-tracker-2");
        if(valid){
          var jsondata = {
            "payrollFlow": self.payrollFlow(),
            "scope": self.scope(),
            "processStartDate": self.processStartDate(),
            "processEndDate": self.processEndDate(),
            "processValue": self.processValue(),
            "consolidationGroup": self.consolidationGroup(),
            "payrollValue": self.payrollValue(),
            "processConsolidationGroup": self.processConsolidationGroup(),
            "payrollStatutoryUnit": self.payrollStatutoryUnit(),
            "taxReportingUnit": self.taxReportingUnit(),
            "department": self.department(),
            "location": self.location(),
            "payrollPeriod": self.payrollPeriod(),
            "scope": self.scope(),
          }
          console.log(jsondata);
          self.back();
        } 
      }

      self.connected = () => {
        accUtils.announce('Dashboard page loaded.', 'assertive');
        document.title = "Costing Process";

      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = () => {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = () => {
        // Implement if needed
      };
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return costingProcessViewModel;
  }
);

