/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;


import com.appspro.db.AppsproConnection;
//import com.appspro.receipt.bean.TrackServerRequestBean;
//import com.appspro.receipt.dao.EFFDetails;

import com.oracle.xmlns.apps.hcm.common.dataloader.core.dataloaderintegrationservice.HCMDataLoaderSoapHttpPortClient;

import common.restHelper.RestHelper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.MalformedURLException;
import java.net.URL;


import java.text.SimpleDateFormat;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.apache.commons.codec.binary.Base64;

import org.joda.time.LocalDate;

import java.util.Date;
//import org.eclipse.persistence.internal.oxm.conversion.Base64;

/**
 *
 * @author Shadi Mansi-PC
 */
public class EncodeBased64Binary {
    RestHelper restHelper = new RestHelper();

    public EncodeBased64Binary() {
        super();
    }

    //    public static void main(String[] args) {
    //        EncodeBased64Binary inst = new EncodeBased64Binary();
    //        try {
    //            String attachment_string =
    //                inst.encodeFileToBase64Binary("D:/fromDesktop/AppsPro/hcm/dataHBL/PayrollInterfaceInboundRecord_-_All_Contexts.zip");
    //        } catch (IOException e) {
    //        }
    //    }

    //return document name (e.g UCMFA00064714) or null in case of error

    public String callUCMService(String data,
                                 String filename) throws MalformedURLException,
                                                         IOException {
        String xml1 =
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ucm=\"http://www.oracle.com/UCM\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <ucm:GenericRequest webKey=\"cs\">\n" +
            "         <ucm:Service IdcService=\"CHECKIN_UNIVERSAL\">\n" +
            "            <ucm:Document>\n" +
            "               <!--Zero or more repetitions:-->\n" +
            "               <ucm:Field name=\"dDocTitle\">";
        String part2 = "</ucm:Field>\n" +
            "              <ucm:Field name=\"dDocType\">Document</ucm:Field>\n" +
            "               <ucm:Field name=\"dSecurityGroup\">FAFusionImportExport</ucm:Field>\n" +
            "               <ucm:Field name=\"dDocAuthor\">" +
            RestHelper.USER_NAME + "</ucm:Field>\n" +
            "               <ucm:Field name=\"dDocAccount\">hcm$/dataloader$/export$</ucm:Field>\n" +
            "               <ucm:File name=\"primaryFile\" href=\"PayrollInterfaceInboundRecordAllContexts.zip\">\n" +
            "                  <ucm:Contents>";
        String xml2 = "</ucm:Contents>\n" +
            "               </ucm:File>\n" +
            "            </ucm:Document>\n" +
            "         </ucm:Service>\n" +
            "      </ucm:GenericRequest>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";

        String input = xml1 + filename + part2 + data + xml2;
        //=====================================================
        String url1 =
            RestHelper.getInstance().getInstanceUrl() + "/idcws/GenericSoapPort";
        java.net.URL wsURL =
            new URL(null, url1, new sun.net.www.protocol.https.Handler());

        URL url = new URL(wsURL, url1);

        //URLConnection connection = url.openConnection();
        HttpsURLConnection httpConn = (HttpsURLConnection)url.openConnection();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        byte[] buffer = new byte[input.length()];
        buffer = input.getBytes();
        bout.write(buffer);
        byte[] b = bout.toByteArray();

        String SOAPAction = RestHelper.SAAS_URL + ":443";
        // httpConn.setHostnameVerifier(new AllowAllHostnameVerifier());
        // Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        httpConn.setRequestProperty("SOAPAction", SOAPAction);
        httpConn.setRequestMethod("POST");

        HostnameVerifier defaultVerifier = new HostnameVerifier() {

            public boolean verify(String hostname, SSLSession session) {
                // TODO: add verify logic here
                if (hostname == "oracledemos.com:443") {
                    return true;
                } else
                    return true;

            }

        };
        HostnameVerifier hv = defaultVerifier;
        httpConn.setDefaultHostnameVerifier(hv);
        //String userPassword = "HCMUser" + ":" + "Appspro@123";

        //byte encoding[] = Base64.encodeBase64(userPassword.getBytes());
        // String encodedPwd = new String(encoding);
        httpConn.setRequestProperty("Authorization",
                                    "Basic " + RestHelper.getAuth());
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        httpConn.setConnectTimeout(6000000);

        OutputStream out = httpConn.getOutputStream();
        out.write(b);
        out.close();
        InputStream is = httpConn.getInputStream();
        String responseXML = getStringFromInputStream(is);
        int start = responseXML.indexOf("dDocName:");
        return start > -1 ? responseXML.substring(start + 9, start + 22) :
               null;
    }

    public String callLoaderService(String ucmref, String path, String hdlId) {
        HCMDataLoaderSoapHttpPortClient c =
            new HCMDataLoaderSoapHttpPortClient();
        String response = null;
        try {
            String id = c.loadDataService(ucmref, path, hdlId);
            if (id != null) {
                Long processId = Long.valueOf(id);
                response = processId.toString();
            }
        } catch (Exception e) {
           e.printStackTrace(); 
//          // AppsproConnection.LOG.error("ERROR", e);
        } finally {
        }

        return response;
    }

    public String encodeFileToBase64Binary(String fileName) throws IOException {

        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        byte[] encoded = Base64.encodeBase64(bytes);
        String encodedString = new String(encoded);
        return encodedString;
    }

    public String encodeFileToBase64Binary2(byte[] bits) throws IOException {

        // File file = new File(fileName);
        byte[] bytes = bits;
        byte[] encoded = Base64.encodeBase64(bytes);
        String encodedString = new String(encoded);
        return encodedString;
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large


        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length &&
               (numRead = is.read(bytes, offset, bytes.length - offset)) >=
               0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " +
                                  file.getName());
        }

        is.close();
        return bytes;
    }

    private String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
           e.printStackTrace(); 
//          // AppsproConnection.LOG.error("ERROR", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                   e.printStackTrace();
//                  // AppsproConnection.LOG.error("ERROR", e);
                }
            }
        }

        return sb.toString();

    }
}

