package com.appspro.payroll.bean;

import java.util.List;

public class FastFormulaBean {
private String formula_id,effective_start_date, effective_end_date, formula_type_id, formula_name, formula_text, compile_flag, enterprise_id, 
legislative_data_group_id, legislation_code, object_version_number, last_update_date, last_updated_by, last_update_login, 
created_by, creation_date, module_id ;
    
    List<FormulaParameterBean> parameteList ; 

    public void setEffective_start_date(String effective_start_date) {
        this.effective_start_date = effective_start_date;
    }

    public String getEffective_start_date() {
        return effective_start_date;
    }

    public void setEffective_end_date(String effective_end_date) {
        this.effective_end_date = effective_end_date;
    }

    public String getEffective_end_date() {
        return effective_end_date;
    }

    public void setFormula_type_id(String formula_type_id) {
        this.formula_type_id = formula_type_id;
    }

    public String getFormula_type_id() {
        return formula_type_id;
    }

    public void setFormula_name(String formula_name) {
        this.formula_name = formula_name;
    }

    public String getFormula_name() {
        return formula_name;
    }

    public void setFormula_text(String formula_text) {
        this.formula_text = formula_text;
    }

    public String getFormula_text() {
        return formula_text;
    }

    public void setCompile_flag(String compile_flag) {
        this.compile_flag = compile_flag;
    }

    public String getCompile_flag() {
        return compile_flag;
    }

    public void setEnterprise_id(String enterprise_id) {
        this.enterprise_id = enterprise_id;
    }

    public String getEnterprise_id() {
        return enterprise_id;
    }

    public void setLegislative_data_group_id(String legislative_data_group_id) {
        this.legislative_data_group_id = legislative_data_group_id;
    }

    public String getLegislative_data_group_id() {
        return legislative_data_group_id;
    }

    public void setLegislation_code(String legislation_code) {
        this.legislation_code = legislation_code;
    }

    public String getLegislation_code() {
        return legislation_code;
    }

    public void setObject_version_number(String object_version_number) {
        this.object_version_number = object_version_number;
    }

    public String getObject_version_number() {
        return object_version_number;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public void setLast_update_login(String last_update_login) {
        this.last_update_login = last_update_login;
    }

    public String getLast_update_login() {
        return last_update_login;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setFormula_id(String formula_id) {
        this.formula_id = formula_id;
    }

    public String getFormula_id() {
        return formula_id;
    }

    public void setParameteList(List<FormulaParameterBean> parameteList) {
        this.parameteList = parameteList;
    }

    public List<FormulaParameterBean> getParameteList() {
        return parameteList;
    }
}
