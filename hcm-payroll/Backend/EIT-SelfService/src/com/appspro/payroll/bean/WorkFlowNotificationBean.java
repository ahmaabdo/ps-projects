/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.bean;

/**
 *
 * @author user
 */
public class WorkFlowNotificationBean {

    private String ID;
    private String msgTitle;
    private String msgBody;
    private String creationDate;
    private String receiverType;
    private String receiverId;
    private String responsePersonId;
    private String type;
    private String responseDate;
    private String workflowId;
    private String requestId;
    private String status;
    private String selfType;
    private String personNumber;
    private String personName;
    private String nationalIdentity;
    
    
    
    


    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setMsgTitle(String msgTitle) {
        this.msgTitle = msgTitle;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public void setResponsePersonId(String responsePersonId) {
        this.responsePersonId = responsePersonId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSelfType(String selfType) {
        this.selfType = selfType;
    }

    public String getID() {
        return ID;
    }

    public String getMsgTitle() {
        return msgTitle;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public String getResponsePersonId() {
        return responsePersonId;
    }

    public String getType() {
        return type;
    }

    public String getResponseDate() {
        return responseDate;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getStatus() {
        return status;
    }

    public String getSelfType() {
        return selfType;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setNationalIdentity(String nationalIdentity) {
        this.nationalIdentity = nationalIdentity;
    }

    public String getNationalIdentity() {
        return nationalIdentity;
    }
}
