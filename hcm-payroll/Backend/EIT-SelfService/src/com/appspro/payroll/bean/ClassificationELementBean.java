package com.appspro.payroll.bean;

import java.util.Date;

public class ClassificationELementBean {

    private int classificationId;
    private String classificationName;
    private Date creationDate;


    public void setClassificationId(int classificationId) {
        this.classificationId = classificationId;
    }

    public int getClassificationId() {
        return classificationId;
    }

    public void setClassificationName(String classificationName) {
        this.classificationName = classificationName;
    }

    public String getClassificationName() {
        return classificationName;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }
}
