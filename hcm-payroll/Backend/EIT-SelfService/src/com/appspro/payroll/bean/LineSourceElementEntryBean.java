/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.bean;

/**
 *
 * @author Shadi Mansi-PC
 */
public class LineSourceElementEntryBean {
    
    private String lineSourceSystemId;
    private String lineInputValueName;
    private String lineScreenEntryValue;
    private String lineData;

    public String getLineSourceSystemId() {
        return lineSourceSystemId;
    }

    public void setLineSourceSystemId(String lineSourceSystemId) {
        this.lineSourceSystemId = lineSourceSystemId;
    }

    public String getLineInputValueName() {
        return lineInputValueName;
    }

    public void setLineInputValueName(String lineInputValueName) {
        this.lineInputValueName = lineInputValueName;
    }

    public String getLineScreenEntryValue() {
        return lineScreenEntryValue;
    }

    public void setLineScreenEntryValue(String lineScreenEntryValue) {
        this.lineScreenEntryValue = lineScreenEntryValue;
    }

    public String getLineData() {
        return lineData;
    }

    public void setLineData(String lineData) {
        this.lineData = lineData;
    }
    
    
    
    
    
}
