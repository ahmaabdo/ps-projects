package com.appspro.payroll.bean;

public class ElementEntrySetupBean {

    private String id;
    private String eitCode;
    private String createdBy;
    private String creationDate;
    private String absenceStatus;
    private String absenceType;
    private String employer;
    private String startDate;
    private String endDate;
    private String sourceSystemOwner;
    private String personNumber;
    private String elementName;
    private String recurringEntry;
    private String sourceSystemId;
    private String legislativeDatagroupName;
    private String creatorType;
    private String typeOfAction;
    private String inputValue;
    private String reason ;
    private String effivtiveStartDate ;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getAbsenceStatus() {
        return absenceStatus;
    }

    public void setAbsenceStatus(String absenceStatus) {
        this.absenceStatus = absenceStatus;
    }

    public String getAbsenceType() {
        return absenceType;
    }

    public void setAbsenceType(String absenceType) {
        this.absenceType = absenceType;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSourceSystemOwner() {
        return sourceSystemOwner;
    }

    public void setSourceSystemOwner(String sourceSystemOwner) {
        this.sourceSystemOwner = sourceSystemOwner;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getRecurringEntry() {
        return recurringEntry;
    }

    public void setRecurringEntry(String recurringEntry) {
        this.recurringEntry = recurringEntry;
    }

    public String getSourceSystemId() {
        return sourceSystemId;
    }

    public void setSourceSystemId(String sourceSystemId) {
        this.sourceSystemId = sourceSystemId;
    }

    public String getLegislativeDatagroupName() {
        return legislativeDatagroupName;
    }

    public void setLegislativeDatagroupName(String legislativeDatagroupName) {
        this.legislativeDatagroupName = legislativeDatagroupName;
    }

    public String getCreatorType() {
        return creatorType;
    }

    public void setCreatorType(String creatorType) {
        this.creatorType = creatorType;
    }

    public String getTypeOfAction() {
        return typeOfAction;
    }

    public void setTypeOfAction(String typeOfAction) {
        this.typeOfAction = typeOfAction;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setEffivtiveStartDate(String effivtiveStartDate) {
        this.effivtiveStartDate = effivtiveStartDate;
    }

    public String getEffivtiveStartDate() {
        return effivtiveStartDate;
    }
}
