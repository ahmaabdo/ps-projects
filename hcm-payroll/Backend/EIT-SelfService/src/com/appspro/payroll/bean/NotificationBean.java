/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.bean;

/**
 *
 * @author user
 */
public class NotificationBean {

    private String ID;
    private String MSG_TITLE;
    private String MSG_BODY;
    private String CREATION_DATE;
    private String RECEIVER_TYPE;
    private String RECEIVER_ID;
    private String RESPONSE_PERSON_ID;
    private String TYPE;
    private String RESPONSE_DATE;
    private String WORKFLOW_ID;
    private String REQUEST_ID;
    private String STATUS;
    private String PERSON_NUMBER;
    private String SELF_TYPE;
    private String PERSON_NAME;
    
    
    

    public String getID() {
        return ID;
    }

    public String getMSG_TITLE() {
        return MSG_TITLE;
    }

    public String getMSG_BODY() {
        return MSG_BODY;
    }

    public String getCREATION_DATE() {
        return CREATION_DATE;
    }

    public String getRECEIVER_TYPE() {
        return RECEIVER_TYPE;
    }

    public String getRECEIVER_ID() {
        return RECEIVER_ID;
    }

    public String getRESPONSE_PERSON_ID() {
        return RESPONSE_PERSON_ID;
    }

    public String getTYPE() {
        return TYPE;
    }

    public String getRESPONSE_DATE() {
        return RESPONSE_DATE;
    }

    public String getWORKFLOW_ID() {
        return WORKFLOW_ID;
    }

    public String getREQUEST_ID() {
        return REQUEST_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public String getPERSON_NUMBER() {
        return PERSON_NUMBER;
    }

    public String getSELF_TYPE() {
        return SELF_TYPE;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setMSG_TITLE(String MSG_TITLE) {
        this.MSG_TITLE = MSG_TITLE;
    }

    public void setMSG_BODY(String MSG_BODY) {
        this.MSG_BODY = MSG_BODY;
    }

    public void setCREATION_DATE(String CREATION_DATE) {
        this.CREATION_DATE = CREATION_DATE;
    }

    public void setRECEIVER_TYPE(String RECEIVER_TYPE) {
        this.RECEIVER_TYPE = RECEIVER_TYPE;
    }

    public void setRECEIVER_ID(String RECEIVER_ID) {
        this.RECEIVER_ID = RECEIVER_ID;
    }

    public void setRESPONSE_PERSON_ID(String RESPONSE_PERSON_ID) {
        this.RESPONSE_PERSON_ID = RESPONSE_PERSON_ID;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public void setRESPONSE_DATE(String RESPONSE_DATE) {
        this.RESPONSE_DATE = RESPONSE_DATE;
    }

    public void setWORKFLOW_ID(String WORKFLOW_ID) {
        this.WORKFLOW_ID = WORKFLOW_ID;
    }

    public void setREQUEST_ID(String REQUEST_ID) {
        this.REQUEST_ID = REQUEST_ID;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public void setPERSON_NUMBER(String PERSON_NUMBER) {
        this.PERSON_NUMBER = PERSON_NUMBER;
    }

    public void setSELF_TYPE(String SELF_TYPE) {
        this.SELF_TYPE = SELF_TYPE;
    }

    public void setPERSON_NAME(String PERSON_NAME) {
        this.PERSON_NAME = PERSON_NAME;
    }

    public String getPERSON_NAME() {
        return PERSON_NAME;
    }
}
