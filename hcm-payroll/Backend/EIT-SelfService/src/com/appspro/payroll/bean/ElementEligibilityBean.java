package com.appspro.payroll.bean;

public class ElementEligibilityBean {

    private int elementLinkId;
    private String effectiveStartDate;
    private String effectiveEndDate;
    private String businessGroupId;
    private String elementTypeId;
    private String payrollId;
    private String jobId;
    private String positionId;
    private String peopleGroupId;
    private String organizationId;
    private String locationId;
    private String gradeId;
    private String standardLinkFlag;
    private String elementLinkName;


    public ElementEligibilityBean() {
        super();
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setBusinessGroupId(String businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    public String getBusinessGroupId() {
        return businessGroupId;
    }


    public void setElementTypeId(String elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public String getElementTypeId() {
        return elementTypeId;
    }

    public void setPayrollId(String payrollId) {
        this.payrollId = payrollId;
    }

    public String getPayrollId() {
        return payrollId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPeopleGroupId(String peopleGroupId) {
        this.peopleGroupId = peopleGroupId;
    }

    public String getPeopleGroupId() {
        return peopleGroupId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setStandardLinkFlag(String standardLinkFlag) {
        this.standardLinkFlag = standardLinkFlag;
    }

    public String getStandardLinkFlag() {
        return standardLinkFlag;
    }

    public void setElementLinkId(int elementLinkId) {
        this.elementLinkId = elementLinkId;
    }

    public int getElementLinkId() {
        return elementLinkId;
    }

    public void setElementLinkName(String elementLinkName) {
        this.elementLinkName = elementLinkName;
    }

    public String getElementLinkName() {
        return elementLinkName;
    }
}
