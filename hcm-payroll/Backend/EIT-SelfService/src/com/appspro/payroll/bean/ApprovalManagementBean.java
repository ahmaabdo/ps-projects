/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.bean;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Shadi Mansi-PC
 */
public class ApprovalManagementBean {

    private int id;
    private String eitCode;
    private String segementName;
    private String type;
    private String positionName;
    private String enable;
    private String hide;
    private int transactionNumber;
    private String createdBy;
    private String createdDate;
    private String updatedDate;
    private String required;
    
    @XmlElement(name = "FORM_LEFT_PROMPT")
    private String fromLeft;

    public String getFromLeft() {
        return fromLeft;
    }

    public void setFromLeft(String fromLeft) {
        this.fromLeft = fromLeft;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public String getSegementName() {
        return segementName;
    }

    public void setSegementName(String segementName) {
        this.segementName = segementName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getHide() {
        return hide;
    }

    public void setHide(String hide) {
        this.hide = hide;
    }

    public int getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(int transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getRequired() {
        return required;
    }
}
