package com.appspro.payroll.bean;

public class FormulaResultRulesBean {


    private int formulaResultRuleId;
    private String effectiveStartDate;
    private String effectiveEndDate;
    private int formulaUsageId;
    private String elementTypeId;
    private String businessGroupId;
    private String resultName;
    private String resultRuleType;
    private String messageType;
    private int inputValueId;
    private String elementName;
    private String inputValueName;
    private int parameterId;
    private String parameterName;

    public FormulaResultRulesBean() {
        super();
    }

    public void setFormulaResultRuleId(int formulaResultRuleId) {
        this.formulaResultRuleId = formulaResultRuleId;
    }

    public int getFormulaResultRuleId() {
        return formulaResultRuleId;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }


    public void setElementTypeId(String elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public String getElementTypeId() {
        return elementTypeId;
    }

    public void setBusinessGroupId(String businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    public String getBusinessGroupId() {
        return businessGroupId;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultRuleType(String resultRuleType) {
        this.resultRuleType = resultRuleType;
    }

    public String getResultRuleType() {
        return resultRuleType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setFormulaUsageId(int formulaUsageId) {
        this.formulaUsageId = formulaUsageId;
    }

    public int getFormulaUsageId() {
        return formulaUsageId;
    }

    public void setInputValueId(int inputValueId) {
        this.inputValueId = inputValueId;
    }

    public int getInputValueId() {
        return inputValueId;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setInputValueName(String inputValueName) {
        this.inputValueName = inputValueName;
    }

    public String getInputValueName() {
        return inputValueName;
    }

    public void setParameterId(int parameterId) {
        this.parameterId = parameterId;
    }

    public int getParameterId() {
        return parameterId;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterName() {
        return parameterName;
    }
}
