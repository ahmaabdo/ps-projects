package com.appspro.payroll.bean;

public class ElementInputValueBean {
    
    private int inputValueId;
    private String effectiveStartDate;
    private String effectiveEndDate;
    private int businessGroupId;
    private String name;
    private String uom;
    private String lookupType;
    private String elementTypeId;
    private String displaySequance;
    private String userDisplayFlag;
    private String defaultValue;
    private String minValue;
    private String maxValue;
    private String mandatoryFlag;
    private String warningOfErorr;
    private String version;
    private String check;


    public void setInputValueId(int inputValueId) {
        this.inputValueId = inputValueId;
    }

    public int getInputValueId() {
        return inputValueId;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setBusinessGroupId(int businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    public int getBusinessGroupId() {
        return businessGroupId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getLookupType() {
        return lookupType;
    }

    public void setElementTypeId(String elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public String getElementTypeId() {
        return elementTypeId;
    }

    public void setDisplaySequance(String displaySequance) {
        this.displaySequance = displaySequance;
    }

    public String getDisplaySequance() {
        return displaySequance;
    }

    public void setUserDisplayFlag(String userDisplayFlag) {
        this.userDisplayFlag = userDisplayFlag;
    }

    public String getUserDisplayFlag() {
        return userDisplayFlag;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMandatoryFlag(String mandatoryFlag) {
        this.mandatoryFlag = mandatoryFlag;
    }

    public String getMandatoryFlag() {
        return mandatoryFlag;
    }


    public void setWarningOfErorr(String warningOfErorr) {
        this.warningOfErorr = warningOfErorr;
    }

    public String getWarningOfErorr() {
        return warningOfErorr;
    }


    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }


    public void setCheck(String check) {
        this.check = check;
    }

    public String getCheck() {
        return check;
    }
}
