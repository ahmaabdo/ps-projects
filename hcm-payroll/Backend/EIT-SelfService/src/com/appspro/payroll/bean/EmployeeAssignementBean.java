package com.appspro.payroll.bean;

public class EmployeeAssignementBean {
    private int id;
    private String empName;
    private Long empNum;
    private Long empId;
    private Long payrollRelationShipNum;
    private String assignmentNum;
    private String payrollName;
    private String elementName;
    private String processingpriority;
    private String processingType;
    private String currencyCode;
    private String classificationName;
    private String inputValueName;
    private String uom;
    private String lookupType;
    private String displaySequnce;
    private String useDisplayFlag;
    private String defualtValue;
    private String MandetoryFlag;
    private String formulaResultName;
    private String formulaName;
    private String formulaText;
    private String personId;
    private String inputValue;
    private String personName;
    private String personNum;
    

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpNum(Long empNum) {
        this.empNum = empNum;
    }

    public Long getEmpNum() {
        return empNum;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public Long getEmpId() {
        return empId;
    }

    public void setPayrollRelationShipNum(Long payrollRelationShipNum) {
        this.payrollRelationShipNum = payrollRelationShipNum;
    }

    public Long getPayrollRelationShipNum() {
        return payrollRelationShipNum;
    }

    public void setAssignmentNum(String assignmentNum) {
        this.assignmentNum = assignmentNum;
    }

    public String getAssignmentNum() {
        return assignmentNum;
    }

    public void setPayrollName(String payrollName) {
        this.payrollName = payrollName;
    }

    public String getPayrollName() {
        return payrollName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setProcessingpriority(String processingpriority) {
        this.processingpriority = processingpriority;
    }

    public String getProcessingpriority() {
        return processingpriority;
    }

    public void setProcessingType(String processingType) {
        this.processingType = processingType;
    }

    public String getProcessingType() {
        return processingType;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setClassificationName(String classificationName) {
        this.classificationName = classificationName;
    }

    public String getClassificationName() {
        return classificationName;
    }

    public void setInputValueName(String inputValueName) {
        this.inputValueName = inputValueName;
    }

    public String getInputValueName() {
        return inputValueName;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getLookupType() {
        return lookupType;
    }

    public void setDisplaySequnce(String displaySequnce) {
        this.displaySequnce = displaySequnce;
    }

    public String getDisplaySequnce() {
        return displaySequnce;
    }

    public void setUseDisplayFlag(String useDisplayFlag) {
        this.useDisplayFlag = useDisplayFlag;
    }

    public String getUseDisplayFlag() {
        return useDisplayFlag;
    }

    public void setDefualtValue(String defualtValue) {
        this.defualtValue = defualtValue;
    }

    public String getDefualtValue() {
        return defualtValue;
    }

    public void setMandetoryFlag(String MandetoryFlag) {
        this.MandetoryFlag = MandetoryFlag;
    }

    public String getMandetoryFlag() {
        return MandetoryFlag;
    }

    public void setFormulaResultName(String formulaResultName) {
        this.formulaResultName = formulaResultName;
    }

    public String getFormulaResultName() {
        return formulaResultName;
    }

    public void setFormulaName(String formulaName) {
        this.formulaName = formulaName;
    }

    public String getFormulaName() {
        return formulaName;
    }

    public void setFormulaText(String formulaText) {
        this.formulaText = formulaText;
    }

    public String getFormulaText() {
        return formulaText;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonNum(String personNum) {
        this.personNum = personNum;
    }

    public String getPersonNum() {
        return personNum;
    }
}
