package com.appspro.payroll.bean;

import java.util.ArrayList;
import java.util.List;

public class ListResponse {
    private List list = new ArrayList();
    
    public ListResponse() {
        super();
    }

    public void setList(List list) {
        this.list = list;
    }

    public List getList() {
        return list;
    }
}
