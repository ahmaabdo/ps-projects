package com.appspro.payroll.bean;

import java.util.ArrayList;
import java.util.List;

public class FormulaResultBean {
    
    private int formulaUsageId;
    private String effectiveStartDate;
    private String effectiveEndDate;
    private String formulaId;
    private String formulaName;
    private String elementTypeId;
    private String elementName;
    private String businessGroupId;
    private String description;
    List<FormulaResultRulesBean> formulaResultRulesBeanList =
        new ArrayList<FormulaResultRulesBean>();
   
    
    
    public FormulaResultBean() {
        super();
    }

    public void setFormulaUsageId(int formulaUsageId) {
        this.formulaUsageId = formulaUsageId;
    }

    public int getFormulaUsageId() {
        return formulaUsageId;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setFormulaId(String formulaId) {
        this.formulaId = formulaId;
    }

    public String getFormulaId() {
        return formulaId;
    }

    public void setElementTypeId(String elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public String getElementTypeId() {
        return elementTypeId;
    }

    public void setBusinessGroupId(String businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    public String getBusinessGroupId() {
        return businessGroupId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setFormulaResultRulesBeanList(List<FormulaResultRulesBean> formulaResultRulesBeanList) {
        this.formulaResultRulesBeanList = formulaResultRulesBeanList;
    }

    public List<FormulaResultRulesBean> getFormulaResultRulesBeanList() {
        return formulaResultRulesBeanList;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setFormulaName(String formulaName) {
        this.formulaName = formulaName;
    }

    public String getFormulaName() {
        return formulaName;
    }
}
