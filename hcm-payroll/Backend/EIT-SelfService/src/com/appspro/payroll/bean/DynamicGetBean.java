package com.appspro.payroll.bean;

public class DynamicGetBean {
    private String ID;
    private String GET_CODE;
    private String GET_PATH;
    private String RESPONSE_MAIN;
    private String RESONSE_SUP_1;
    private String RESONSE_SUP_2;


    public void setID(String ID) {
        this.ID = ID;
    }

    public String getID() {
        return ID;
    }

    public void setGET_CODE(String GET_CODE) {
        this.GET_CODE = GET_CODE;
    }

    public String getGET_CODE() {
        return GET_CODE;
    }

    public void setGET_PATH(String GET_PATH) {
        this.GET_PATH = GET_PATH;
    }

    public String getGET_PATH() {
        return GET_PATH;
    }

    public void setRESPONSE_MAIN(String RESPONSE_MAIN) {
        this.RESPONSE_MAIN = RESPONSE_MAIN;
    }

    public String getRESPONSE_MAIN() {
        return RESPONSE_MAIN;
    }

    public void setRESONSE_SUP_1(String RESONSE_SUP_1) {
        this.RESONSE_SUP_1 = RESONSE_SUP_1;
    }

    public String getRESONSE_SUP_1() {
        return RESONSE_SUP_1;
    }

    public void setRESONSE_SUP_2(String RESONSE_SUP_2) {
        this.RESONSE_SUP_2 = RESONSE_SUP_2;
    }

    public String getRESONSE_SUP_2() {
        return RESONSE_SUP_2;
    }
}
