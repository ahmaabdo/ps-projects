package com.appspro.payroll.bean;

import java.util.List;

public class ObjectGroupFormBean {

    private int elementSetId;
    private String elementSetName;
    private String elementSetType;
    private String effectiveStartDate;
    private String effectiveEndDate;
    private String elementTypeId;
    private String inclusionStatus;
    private String elementName;
    private String elementType;
    private List<ElementFormBean> elementFormBeanList;
    private String check;
    private String classificationId;

    public void setElementSetId(int elementSetId) {
        this.elementSetId = elementSetId;
    }

    public int getElementSetId() {
        return elementSetId;
    }

    public void setElementSetName(String elementSetName) {
        this.elementSetName = elementSetName;
    }

    public String getElementSetName() {
        return elementSetName;
    }

    public void setElementSetType(String elementSetType) {
        this.elementSetType = elementSetType;
    }

    public String getElementSetType() {
        return elementSetType;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setElementTypeId(String elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public String getElementTypeId() {
        return elementTypeId;
    }

    public void setInclusionStatus(String inclusionStatus) {
        this.inclusionStatus = inclusionStatus;
    }

    public String getInclusionStatus() {
        return inclusionStatus;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementFormBeanList(List<ElementFormBean> elementFormBeanList) {
        this.elementFormBeanList = elementFormBeanList;
    }

    public List<ElementFormBean> getElementFormBeanList() {
        return elementFormBeanList;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public String getCheck() {
        return check;
    }

    public void setClassificationId(String classificationId) {
        this.classificationId = classificationId;
    }

    public String getClassificationId() {
        return classificationId;
    }
}
