/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.bean;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Shadi Mansi-PC
 */
public class ElementEntryBean {
    
    private int id;
    @XmlElement(name = "SS_ID")
    private int ssId;
    @XmlElement(name = "SS_TYPE")
    private String ssType;
    @XmlElement(name = "UCM_ID")
    private String ucmId;
    @XmlElement(name = "STATUS")
    private String status;
    @XmlElement(name = "PROCESS_ID")
    private String prossesId;
    private String creationDate;
    private String updateDate;
    @XmlElement(name = "FILE_CONTENT")
    private String fileContent;
    @XmlElement(name = "PERSON_NUMBER")
    private String personNumber;
    @XmlElement(name = "FILE_BYTES")
    private String fileBytes;
    @XmlElement(name = "FILE_PATH")
    private String filePath;
    @XmlElement(name = "FULL_NAME")
    private String fullName;
    @XmlElement(name = "HIRE_DATE")
    private String hireDate;
    @XmlElement(name = "POSITION_NAME")
    private String positionName;
    @XmlElement(name = "GRADE_NAME")
    private String gradeName;
    @XmlElement(name = "ASSIGNMENT_STATUS_TYPE")
    private String assignmentStatusType;
    @XmlElement(name = "START_DATE")
    private String startDate;
    @XmlElement(name = "END_DATE")
    private String endDate;
    @XmlElement(name = "ADVANCED_LEAVE")
    private String advancedLeave;
    @XmlElement(name = "FILE_NAME")
    private String fileName;
    private String ssAr;
    private String ssEn;
    private String astAr;
    private String astEn;
    @XmlElement(name = "EIT_CODE")
    private String eitCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSsId() {
        return ssId;
    }

    public void setSsId(int ssId) {
        this.ssId = ssId;
    }

    public String getSsType() {
        return ssType;
    }

    public void setSsType(String ssType) {
        this.ssType = ssType;
    }

    public String getUcmId() {
        return ucmId;
    }

    public void setUcmId(String ucmId) {
        this.ucmId = ucmId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProssesId() {
        return prossesId;
    }

    public void setProssesId(String prossesId) {
        this.prossesId = prossesId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(String fileBytes) {
        this.fileBytes = fileBytes;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getHireDate() {
        return hireDate;
    }

    public void setHireDate(String hireDate) {
        this.hireDate = hireDate;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getAssignmentStatusType() {
        return assignmentStatusType;
    }

    public void setAssignmentStatusType(String assignmentStatusType) {
        this.assignmentStatusType = assignmentStatusType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAdvancedLeave() {
        return advancedLeave;
    }

    public void setAdvancedLeave(String advancedLeave) {
        this.advancedLeave = advancedLeave;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSsAr() {
        return ssAr;
    }

    public void setSsAr(String ssAr) {
        this.ssAr = ssAr;
    }

    public String getSsEn() {
        return ssEn;
    }

    public void setSsEn(String ssEn) {
        this.ssEn = ssEn;
    }

    public String getAstAr() {
        return astAr;
    }

    public void setAstAr(String astAr) {
        this.astAr = astAr;
    }

    public String getAstEn() {
        return astEn;
    }

    public void setAstEn(String astEn) {
        this.astEn = astEn;
    }

    public String getEitCode() {
        return eitCode;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }
    
    
    
    
    
    
    
}
