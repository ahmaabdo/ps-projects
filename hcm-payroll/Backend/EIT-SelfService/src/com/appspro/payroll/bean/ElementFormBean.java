package com.appspro.payroll.bean;

import java.util.Date;


public class ElementFormBean {

    public ElementFormBean() {
    }

    private String elementName;
    private String elementType;
    private String processingType;
    private String effectiveStartDate;
    private String effectiveEndDate;
    private String priority;
    private String currency;
    private int elementTypeId;
    private String businessGroupId;
    private String classificationId;
    private String reportingName;
    private String description;
    private String success;


    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementType() {
        return elementType;
    }

    public void setProcessingType(String processingType) {
        this.processingType = processingType;
    }

    public String getProcessingType() {
        return processingType;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }


    public void setElementTypeId(int elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public int getElementTypeId() {
        return elementTypeId;
    }

    public void setBusinessGroupId(String businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    public String getBusinessGroupId() {
        return businessGroupId;
    }

    public void setClassificationId(String classificationId) {
        this.classificationId = classificationId;
    }

    public String getClassificationId() {
        return classificationId;
    }

    public void setReportingName(String reportingName) {
        this.reportingName = reportingName;
    }

    public String getReportingName() {
        return reportingName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


    public void setCheck(String success) {
        this.success = success;
    }

    public String getCheck() {
        return success;
    }
}
