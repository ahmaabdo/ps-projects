package com.appspro.payroll.bean;

public class BusinessUnitsBean {
    private String BUId;
    private String BUname;
    
    public BusinessUnitsBean() {
       
        super();
    }

    public void setBUId(String BUId) {
        this.BUId = BUId;
    }

    public String getBUId() {
        return BUId;
    }

    public void setBUname(String BUname) {
        this.BUname = BUname;
    }

    public String getBUname() {
        return BUname;
    }
}
