package com.appspro.payroll.bean;

public class ViewProcessResultBean {
    private String flowName;
    private String timePeriodName;
    private String actionType;
    private String actionStatus;
    private String assignmentId;
    private String lineStatus;
    private String elementName;
    private String defualtValue;
    private String uom;
    private String inputValueName;
    private String inputValue;

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setTimePeriodName(String timePeriodName) {
        this.timePeriodName = timePeriodName;
    }

    public String getTimePeriodName() {
        return timePeriodName;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    public String getActionStatus() {
        return actionStatus;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setLineStatus(String lineStatus) {
        this.lineStatus = lineStatus;
    }

    public String getLineStatus() {
        return lineStatus;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setDefualtValue(String defualtValue) {
        this.defualtValue = defualtValue;
    }

    public String getDefualtValue() {
        return defualtValue;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setInputValueName(String inputValueName) {
        this.inputValueName = inputValueName;
    }

    public String getInputValueName() {
        return inputValueName;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public String getInputValue() {
        return inputValue;
    }
}
