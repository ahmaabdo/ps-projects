/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.bean;

/**
 *
 * @author user
 */
public class ApprovalSetupBean {

    private String id;
    private String eitCode;
    private String approvalOrder;
    private String approvalType;
    private String roleName;
    private String specialCase;
    private String notificationType;
    private String createdBy;
    private String creationDate;
    private String updatedBy;
    private String updatedDate;
    private String managerLeval ;
    private String approval;
    private String position;
    private String approvalCode;
    private String operation;
    

    public String getManagerLeval() {
        return managerLeval;
    }

    public void setManagerLeval(String managerLeval) {
        this.managerLeval = managerLeval;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public String getEitCode() {
        return eitCode;
    }

    public String getApprovalOrder() {
        return approvalOrder;
    }

    public String getApprovalType() {
        return approvalType;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getSpecialCase() {
        return specialCase;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEitCode(String eitCode) {
        this.eitCode = eitCode;
    }

    public void setApprovalOrder(String approvalOrder) {
        this.approvalOrder = approvalOrder;
    }

    public void setApprovalType(String approvalType) {
        this.approvalType = approvalType;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public void setSpecialCase(String specialCase) {
        this.specialCase = specialCase;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }



    public void setApproval(String approval) {
        this.approval = approval;
    }

    public String getApproval() {
        return approval;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getApprovalCode() {
        return approvalCode;
    }
    public void setOperation(String operation) {
           this.operation = operation;
       }

       public String getOperation() {
           return operation;
       }
}
