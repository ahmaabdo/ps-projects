package com.appspro.payroll.bean;

public class ElementBatchBean {
  private String ID,ELEMENT_NAME ,CREATION_BY,CREATION_DATE , DESCRIPTION;

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getID() {
        return ID;
    }

    public void setELEMENT_NAME(String ELEMENT_NAME) {
        this.ELEMENT_NAME = ELEMENT_NAME;
    }

    public String getELEMENT_NAME() {
        return ELEMENT_NAME;
    }


    public void setCREATION_BY(String CREATION_BY) {
        this.CREATION_BY = CREATION_BY;
    }

    public String getCREATION_BY() {
        return CREATION_BY;
    }

    public void setCREATION_DATE(String CREATION_DATE) {
        this.CREATION_DATE = CREATION_DATE;
    }

    public String getCREATION_DATE() {
        return CREATION_DATE;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }
}
