package com.appspro.payroll.bean;

public class FormulaParameterBean {
    private String id, name, parameter_type, parameter_category, source, length, directions, elementName, inputValue, reportName, pram1, pram2, pram3, pram4, pram5, formula_id;
   private String PRAM1_SOURCE,PRAM1_ELEMENT,PRAM2_SOURCE,PRAM2_ELEMENT,PRAM3_SOURCE,PRAM3_ELEMENT,PRAM4_SOURCE,PRAM4_ELEMENT,PRAM5_SOURCE,PRAM5_ELEMENT;
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setParameter_type(String parameter_type) {
        this.parameter_type = parameter_type;
    }

    public String getParameter_type() {
        return parameter_type;
    }

    public void setParameter_category(String parameter_category) {
        this.parameter_category = parameter_category;
    }

    public String getParameter_category() {
        return parameter_category;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getLength() {
        return length;
    }

    public void setFormula_id(String formula_id) {
        this.formula_id = formula_id;
    }

    public String getFormula_id() {
        return formula_id;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public String getDirections() {
        return directions;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportName() {
        return reportName;
    }

    public void setPram1(String pram1) {
        this.pram1 = pram1;
    }

    public String getPram1() {
        return pram1;
    }

    public void setPram2(String pram2) {
        this.pram2 = pram2;
    }

    public String getPram2() {
        return pram2;
    }

    public void setPram3(String pram3) {
        this.pram3 = pram3;
    }

    public String getPram3() {
        return pram3;
    }

    public void setPram4(String pram4) {
        this.pram4 = pram4;
    }

    public String getPram4() {
        return pram4;
    }

    public void setPram5(String pram5) {
        this.pram5 = pram5;
    }

    public String getPram5() {
        return pram5;
    }

    public void setPRAM1_SOURCE(String PRAM1_SOURCE) {
        this.PRAM1_SOURCE = PRAM1_SOURCE;
    }

    public String getPRAM1_SOURCE() {
        return PRAM1_SOURCE;
    }

    public void setPRAM1_ELEMENT(String PRAM1_ELEMENT) {
        this.PRAM1_ELEMENT = PRAM1_ELEMENT;
    }

    public String getPRAM1_ELEMENT() {
        return PRAM1_ELEMENT;
    }

    public void setPRAM2_SOURCE(String PRAM2_SOURCE) {
        this.PRAM2_SOURCE = PRAM2_SOURCE;
    }

    public String getPRAM2_SOURCE() {
        return PRAM2_SOURCE;
    }

    public void setPRAM2_ELEMENT(String PRAM2_ELEMENT) {
        this.PRAM2_ELEMENT = PRAM2_ELEMENT;
    }

    public String getPRAM2_ELEMENT() {
        return PRAM2_ELEMENT;
    }

    public void setPRAM3_SOURCE(String PRAM3_SOURCE) {
        this.PRAM3_SOURCE = PRAM3_SOURCE;
    }

    public String getPRAM3_SOURCE() {
        return PRAM3_SOURCE;
    }

    public void setPRAM3_ELEMENT(String PRAM3_ELEMENT) {
        this.PRAM3_ELEMENT = PRAM3_ELEMENT;
    }

    public String getPRAM3_ELEMENT() {
        return PRAM3_ELEMENT;
    }

    public void setPRAM4_SOURCE(String PRAM4_SOURCE) {
        this.PRAM4_SOURCE = PRAM4_SOURCE;
    }

    public String getPRAM4_SOURCE() {
        return PRAM4_SOURCE;
    }

    public void setPRAM4_ELEMENT(String PRAM4_ELEMENT) {
        this.PRAM4_ELEMENT = PRAM4_ELEMENT;
    }

    public String getPRAM4_ELEMENT() {
        return PRAM4_ELEMENT;
    }

    public void setPRAM5_SOURCE(String PRAM5_SOURCE) {
        this.PRAM5_SOURCE = PRAM5_SOURCE;
    }

    public String getPRAM5_SOURCE() {
        return PRAM5_SOURCE;
    }

    public void setPRAM5_ELEMENT(String PRAM5_ELEMENT) {
        this.PRAM5_ELEMENT = PRAM5_ELEMENT;
    }

    public String getPRAM5_ELEMENT() {
        return PRAM5_ELEMENT;
    }
}
