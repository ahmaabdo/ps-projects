package com.appspro.payroll.bean;

public class RunPayrollBean {
    private String elementName;
    private String elementTypeId;
    private String formulaName;
    private String formulaText;
    private String formulaId;
    private String payrollName;
    private String empName;
    private String inputValue;
    private String personId;
    private String personNum;


    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementTypeId(String elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public String getElementTypeId() {
        return elementTypeId;
    }

    public void setFormulaName(String formulaName) {
        this.formulaName = formulaName;
    }

    public String getFormulaName() {
        return formulaName;
    }

    public void setFormulaText(String formulaText) {
        this.formulaText = formulaText;
    }

    public String getFormulaText() {
        return formulaText;
    }

    public void setFormulaId(String formulaId) {
        this.formulaId = formulaId;
    }

    public String getFormulaId() {
        return formulaId;
    }

    public void setPayrollName(String payrollName) {
        this.payrollName = payrollName;
    }

    public String getPayrollName() {
        return payrollName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonNum(String personNum) {
        this.personNum = personNum;
    }

    public String getPersonNum() {
        return personNum;
    }
}
