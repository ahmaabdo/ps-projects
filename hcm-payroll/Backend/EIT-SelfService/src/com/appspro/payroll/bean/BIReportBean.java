package com.appspro.payroll.bean;

public class BIReportBean {
    
    private int id;
    private String parameter1;
    private String parameter2;
    private String parameter3;
    private String parameter4;
    private String parameter5;
    private String reportName;
    private String reportTypeVal;
    private String parameter6;
    private String parameter7;
    private String parameter8;
    private String parameter9;
    private String parameter10;
    private String parameter11;
    private String parameter12;
    private String parameter13;
    private String parameter14;
    private String parameter15;
    private String parameter16;
    private String parameter17;
    private String parameter18;
    private String parameter19;
    private String parameter20;
    
    public BIReportBean() {
        super();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter2(String parameter2) {
        this.parameter2 = parameter2;
    }

    public String getParameter2() {
        return parameter2;
    }

    public void setParameter3(String parameter3) {
        this.parameter3 = parameter3;
    }

    public String getParameter3() {
        return parameter3;
    }

    public void setParameter4(String parameter4) {
        this.parameter4 = parameter4;
    }

    public String getParameter4() {
        return parameter4;
    }

    public void setParameter5(String parameter5) {
        this.parameter5 = parameter5;
    }

    public String getParameter5() {
        return parameter5;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportTypeVal(String reportTypeVal) {
        this.reportTypeVal = reportTypeVal;
    }

    public String getReportTypeVal() {
        return reportTypeVal;
    }

    public void setParameter6(String parameter6) {
        this.parameter6 = parameter6;
    }

    public String getParameter6() {
        return parameter6;
    }

    public void setParameter7(String parameter7) {
        this.parameter7 = parameter7;
    }

    public String getParameter7() {
        return parameter7;
    }

    public void setParameter8(String parameter8) {
        this.parameter8 = parameter8;
    }

    public String getParameter8() {
        return parameter8;
    }

    public void setParameter9(String parameter9) {
        this.parameter9 = parameter9;
    }

    public String getParameter9() {
        return parameter9;
    }

    public void setParameter10(String parameter10) {
        this.parameter10 = parameter10;
    }

    public String getParameter10() {
        return parameter10;
    }

    public void setParameter11(String parameter11) {
        this.parameter11 = parameter11;
    }

    public String getParameter11() {
        return parameter11;
    }

    public void setParameter12(String parameter12) {
        this.parameter12 = parameter12;
    }

    public String getParameter12() {
        return parameter12;
    }

    public void setParameter13(String parameter13) {
        this.parameter13 = parameter13;
    }

    public String getParameter13() {
        return parameter13;
    }

    public void setParameter14(String parameter14) {
        this.parameter14 = parameter14;
    }

    public String getParameter14() {
        return parameter14;
    }

    public void setParameter15(String parameter15) {
        this.parameter15 = parameter15;
    }

    public String getParameter15() {
        return parameter15;
    }

    public void setParameter16(String parameter16) {
        this.parameter16 = parameter16;
    }

    public String getParameter16() {
        return parameter16;
    }

    public void setParameter17(String parameter17) {
        this.parameter17 = parameter17;
    }

    public String getParameter17() {
        return parameter17;
    }

    public void setParameter18(String parameter18) {
        this.parameter18 = parameter18;
    }

    public String getParameter18() {
        return parameter18;
    }

    public void setParameter19(String parameter19) {
        this.parameter19 = parameter19;
    }

    public String getParameter19() {
        return parameter19;
    }

    public void setParameter20(String parameter20) {
        this.parameter20 = parameter20;
    }

    public String getParameter20() {
        return parameter20;
    }
}
