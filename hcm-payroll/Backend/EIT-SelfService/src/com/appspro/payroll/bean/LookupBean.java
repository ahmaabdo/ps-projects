package com.appspro.payroll.bean;

public class LookupBean {

    private int id;
    private String name;
    private String code;
    private String valueEn;
    private String valueAr;


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setValueEn(String valueEn) {
        this.valueEn = valueEn;
    }

    public String getValueEn() {
        return valueEn;
    }

    public void setValueAr(String valueAr) {
        this.valueAr = valueAr;
    }

    public String getValueAr() {
        return valueAr;
    }
}
