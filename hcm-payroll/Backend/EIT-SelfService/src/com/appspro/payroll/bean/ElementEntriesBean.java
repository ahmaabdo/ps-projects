package com.appspro.payroll.bean;

public class ElementEntriesBean {
  
  private int elementEntryId;
  private String personId;
  private String elementTypeId;
  private String effectiveStartDate;
  private String effectiveEndDate;
  private String entryType;
  private String inputValueId;
  private String screenEntryValue;
  private int elementEntryValueId;
  private String elementType;
  private String elementName;
  private String ProccessingType;
  private String maxValue;
  private String minValue;
  private String inputeValueName;
  private String check;
  private String uom;
  private String currencyCode;


    public void setElementEntryId(int elementEntryId) {
        this.elementEntryId = elementEntryId;
    }

    public int getElementEntryId() {
        return elementEntryId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setElementTypeId(String elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public String getElementTypeId() {
        return elementTypeId;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setInputValueId(String inputValueId) {
        this.inputValueId = inputValueId;
    }

    public String getInputValueId() {
        return inputValueId;
    }

    public void setScreenEntryValue(String screenEntryValue) {
        this.screenEntryValue = screenEntryValue;
    }

    public String getScreenEntryValue() {
        return screenEntryValue;
    }

    public void setElementEntryValueId(int elementEntryValueId) {
        this.elementEntryValueId = elementEntryValueId;
    }

    public int getElementEntryValueId() {
        return elementEntryValueId;
    }


    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementType() {
        return elementType;
    }


    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }


    public void setProccessingType(String ProccessingType) {
        this.ProccessingType = ProccessingType;
    }

    public String getProccessingType() {
        return ProccessingType;
    }


    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setInputeValueName(String inputeValueName) {
        this.inputeValueName = inputeValueName;
    }

    public String getInputeValueName() {
        return inputeValueName;
    }


    public void setCheck(String check) {
        this.check = check;
    }

    public String getCheck() {
        return check;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
}
