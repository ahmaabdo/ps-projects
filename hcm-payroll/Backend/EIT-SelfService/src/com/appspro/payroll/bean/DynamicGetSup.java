package com.appspro.payroll.bean;

public class DynamicGetSup {
  private String id;
  private String GET_ID;
  private String SUP_NAME;
  private String GET_CODE;
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }
    public void setGET_ID(String GET_ID) {
        this.GET_ID = GET_ID;
    }
    public String getGET_ID() {
        return GET_ID;
    }
    public void setSUP_NAME(String SUP_NAME) {
        this.SUP_NAME = SUP_NAME;
    }
    public String getSUP_NAME() {
        return SUP_NAME;
    }
    public void setGET_CODE(String GET_CODE) {
        this.GET_CODE = GET_CODE;
    }
    public String getGET_CODE() {
        return GET_CODE;
    }
}
