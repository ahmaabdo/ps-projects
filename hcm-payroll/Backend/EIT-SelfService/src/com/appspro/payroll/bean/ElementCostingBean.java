package com.appspro.payroll.bean;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ElementCostingBean {
    
    
    private int elementCostId;
    private String effectiveStartDate;
    private String effectiveEndDate;
    private String businessGroupId;
    private String elementTypeId;
    private String payrollId;
    private String jobId;
    private String positionId;
    private String peopleGroupId;
    private String organizationId;
    private String locationId;
    private String gradeId;
    private String transferToGLFlag;
    private String costableType;
    private String costSegment1;
    private String costSegment2;
    private String costSegment3;
    private String costSegment4;
    private String costSegment5;
    private String costSegment6;
    private String costSegment7;
    private String costSegment8;
    private String costSegment9;
    private String costSegment10;
    private String balanceSegment1;
    private String balanceSegment2;
    private String balanceSegment3;
    private String balanceSegment4;
    private String balanceSegment5;
    private String balanceSegment6;
    private String balanceSegment7;
    private String balanceSegment8;
    private String balanceSegment9;
    private String balanceSegment10;


    //to string reflection implementation. Used to print out the class variables in DAOs if needed
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public ElementCostingBean() {
        super();
    }

    public void setElementCostId(int elementCostId) {
        this.elementCostId = elementCostId;
    }

    public int getElementCostId() {
        return elementCostId;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setBusinessGroupId(String businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    public String getBusinessGroupId() {
        return businessGroupId;
    }

    public void setElementTypeId(String elementTypeId) {
        this.elementTypeId = elementTypeId;
    }

    public String getElementTypeId() {
        return elementTypeId;
    }

    public void setPayrollId(String payrollId) {
        this.payrollId = payrollId;
    }

    public String getPayrollId() {
        return payrollId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPeopleGroupId(String peopleGroupId) {
        this.peopleGroupId = peopleGroupId;
    }

    public String getPeopleGroupId() {
        return peopleGroupId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setTransferToGLFlag(String transferToGLFlag) {
        this.transferToGLFlag = transferToGLFlag;
    }

    public String getTransferToGLFlag() {
        return transferToGLFlag;
    }

    public void setCostableType(String costableType) {
        this.costableType = costableType;
    }

    public String getCostableType() {
        return costableType;
    }

    public void setCostSegment1(String costSegment1) {
        this.costSegment1 = costSegment1;
    }

    public String getCostSegment1() {
        return costSegment1;
    }

    public void setCostSegment2(String costSegment2) {
        this.costSegment2 = costSegment2;
    }

    public String getCostSegment2() {
        return costSegment2;
    }

    public void setCostSegment3(String costSegment3) {
        this.costSegment3 = costSegment3;
    }

    public String getCostSegment3() {
        return costSegment3;
    }

    public void setCostSegment4(String costSegment4) {
        this.costSegment4 = costSegment4;
    }

    public String getCostSegment4() {
        return costSegment4;
    }

    public void setCostSegment5(String costSegment5) {
        this.costSegment5 = costSegment5;
    }

    public String getCostSegment5() {
        return costSegment5;
    }

    public void setCostSegment6(String costSegment6) {
        this.costSegment6 = costSegment6;
    }

    public String getCostSegment6() {
        return costSegment6;
    }

    public void setCostSegment7(String costSegment7) {
        this.costSegment7 = costSegment7;
    }

    public String getCostSegment7() {
        return costSegment7;
    }

    public void setCostSegment8(String costSegment8) {
        this.costSegment8 = costSegment8;
    }

    public String getCostSegment8() {
        return costSegment8;
    }

    public void setCostSegment9(String costSegment9) {
        this.costSegment9 = costSegment9;
    }

    public String getCostSegment9() {
        return costSegment9;
    }

    public void setCostSegment10(String costSegment10) {
        this.costSegment10 = costSegment10;
    }

    public String getCostSegment10() {
        return costSegment10;
    }

    public void setBalanceSegment1(String balanceSegment1) {
        this.balanceSegment1 = balanceSegment1;
    }

    public String getBalanceSegment1() {
        return balanceSegment1;
    }

    public void setBalanceSegment2(String balanceSegment2) {
        this.balanceSegment2 = balanceSegment2;
    }

    public String getBalanceSegment2() {
        return balanceSegment2;
    }

    public void setBalanceSegment3(String balanceSegment3) {
        this.balanceSegment3 = balanceSegment3;
    }

    public String getBalanceSegment3() {
        return balanceSegment3;
    }

    public void setBalanceSegment4(String balanceSegment4) {
        this.balanceSegment4 = balanceSegment4;
    }

    public String getBalanceSegment4() {
        return balanceSegment4;
    }

    public void setBalanceSegment5(String balanceSegment5) {
        this.balanceSegment5 = balanceSegment5;
    }

    public String getBalanceSegment5() {
        return balanceSegment5;
    }

    public void setBalanceSegment6(String balanceSegment6) {
        this.balanceSegment6 = balanceSegment6;
    }

    public String getBalanceSegment6() {
        return balanceSegment6;
    }

    public void setBalanceSegment7(String balanceSegment7) {
        this.balanceSegment7 = balanceSegment7;
    }

    public String getBalanceSegment7() {
        return balanceSegment7;
    }

    public void setBalanceSegment8(String balanceSegment8) {
        this.balanceSegment8 = balanceSegment8;
    }

    public String getBalanceSegment8() {
        return balanceSegment8;
    }

    public void setBalanceSegment9(String balanceSegment9) {
        this.balanceSegment9 = balanceSegment9;
    }

    public String getBalanceSegment9() {
        return balanceSegment9;
    }

    public void setBalanceSegment10(String balanceSegment10) {
        this.balanceSegment10 = balanceSegment10;
    }

    public String getBalanceSegment10() {
        return balanceSegment10;
    }

}
