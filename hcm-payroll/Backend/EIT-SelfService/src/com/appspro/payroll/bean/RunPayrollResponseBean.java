package com.appspro.payroll.bean;

import java.util.ArrayList;
import java.util.List;


public class RunPayrollResponseBean {

private String elementName;
private String elementId;
private FastFormulaBean formula;
private List<EmployeeAssignementBean> employee;


    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setFormula(FastFormulaBean formula) {
        this.formula = formula;
    }

    public FastFormulaBean getFormula() {
        return formula;
    }

    public void setEmployee(List<EmployeeAssignementBean> employee) {
        this.employee = employee;
    }

    public List<EmployeeAssignementBean> getEmployee() {
        return employee;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public String getElementId() {
        return elementId;
    }
}
