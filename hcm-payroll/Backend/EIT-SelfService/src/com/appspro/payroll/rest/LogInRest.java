/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.rest;


import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.EmployeeBean;
import com.appspro.payroll.dao.EmployeeDetails;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.login.Login;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.xml.bind.DatatypeConverter;


@Path("/login")
public class LogInRest {

    @POST
    @Consumes("application/json")
    @Path("/login2")
    public Response getEmpDetails(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        String jsonInString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(body);
            String userName = actualObj.get("userName").asText();
            String password = actualObj.get("password").asText();
            boolean mainData = actualObj.get("mainData").asBoolean();
            Login login = new Login();
            boolean result = login.validateLogin(userName, password);
            jsonInString = "{\"result\":" + result + "}";
            if (result) {
                String encoding =
                    DatatypeConverter.printBase64Binary((userName + ":" +
                                                         password).getBytes("UTF-8"));

                HttpSession session = request.getSession();
                session.setAttribute("authorization", "Basic " + encoding);
                EmployeeBean obj = new EmployeeBean();
                EmployeeDetails det = new EmployeeDetails();


//                if (session.getAttribute("EmpDetails") != null) {
                //                    obj = (EmployeeBean)session.getAttribute("EmpDetails");
                //
                //                } else {
                ////                    if (userName != null && !userName.isEmpty()) {
                ////                        obj =
                ////det.getEmpDetails(userName, null, null, !mainData);
                ////                        if (!mainData) {
                ////                            session.setAttribute("EmpDetails", obj);
                ////                        }
                //
                //                    } else {
                //
                //                        return Response.ok("",
                //                                           MediaType.APPLICATION_JSON).build();
                //                    }
                //
                //                }
            }

            return Response.ok(jsonInString,
                               MediaType.APPLICATION_JSON).build();

        } catch (JsonProcessingException e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
            return Response.status(500).entity(e.getMessage()).build();
        } catch (IOException e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }
        return Response.ok(jsonInString, MediaType.APPLICATION_JSON).build();

    }

    public LogInRest() {
        super();
    }
}
