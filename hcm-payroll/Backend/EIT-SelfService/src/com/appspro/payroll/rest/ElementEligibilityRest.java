package com.appspro.payroll.rest;


import com.appspro.payroll.bean.ElementEligibilityBean;


import com.appspro.payroll.dao.ElementEligibilityDAO;



import com.google.gson.Gson;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;


@Path("/AppsProRest")

public class ElementEligibilityRest {
    
    ElementEligibilityDAO elmentEligibilityDAO=new ElementEligibilityDAO();
    @POST
        @Path("/insertElementEligibility")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public Response insertElementEligibility(String eligibilityBean) {
            List<ElementEligibilityBean> elementEligibilityList =
                new ArrayList<ElementEligibilityBean>();
            try {
                elementEligibilityList =
                    elmentEligibilityDAO.insertElementEligibility(eligibilityBean);
                Gson jsonConverter = new GsonBuilder().create();
                return Response.status(201).entity(jsonConverter.toJson(elementEligibilityList)).build();
               
            } catch (Exception e) {
                e.printStackTrace();
                return Response.status(201).entity("Something Wrong Please Check").build();
            }
        }
    

        @PUT
        @Path("/updateElementEligibility")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public Response updateElementEligibility(String eligibilityBean) {
            List<ElementEligibilityBean> elementEligibilityList =
                new ArrayList<ElementEligibilityBean>();
            
            try {
                elementEligibilityList =
                    elmentEligibilityDAO.updateElementEligibility(eligibilityBean.toString());
                Gson jsonConverter = new GsonBuilder().create();
                return Response.status(201).entity(jsonConverter.toJson(elementEligibilityList)).build();
            } catch (Exception e) {
                e.printStackTrace();
                            return Response.status(500).entity("Something Wrong Please Check").build();
            }
        }
        
        @GET
        @Path("/GetAllElementEligibility/{ELEMENT_TYPE_ID}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public String GetAllElementEligibility(@PathParam("ELEMENT_TYPE_ID")int elementTypeId) {
            
            List<ElementEligibilityBean> elementEligibilityBeanList =
                new ArrayList<ElementEligibilityBean>();
            try {
                elementEligibilityBeanList = elmentEligibilityDAO.getAllElementEligibility(elementTypeId);

                return new JSONArray(elementEligibilityBeanList).toString();

            } catch (Exception e) {
                return e.toString();
            }
        }

        @GET
        @Path("GetElementEligibility/{id}")
        @Produces(MediaType.APPLICATION_JSON)
        @Consumes(MediaType.APPLICATION_JSON)
        public String getElementEligibilityById(@PathParam("id")
            int id) {
            
            List<ElementEligibilityBean> elementEligibilityBeanList =
                new ArrayList<ElementEligibilityBean>();
            try {
                elementEligibilityBeanList = elmentEligibilityDAO.getEligibilityById(id);
                return new JSONArray(elementEligibilityBeanList).toString();
            } catch (Exception e) {
                return e.toString();
            }
        }
        
        @DELETE
        @Path("DeleteElementEligibility/{id}")
        @Produces(MediaType.APPLICATION_JSON)
        @Consumes(MediaType.APPLICATION_JSON)
        public String deleteElementForm(@PathParam("id")
            int id) {
            
            
            try {
             return   elmentEligibilityDAO.deleteEligibilityById(id);
                
            } catch (Exception e) {
                return e.toString();
            }
            
        }
        
    @GET
    @Path("GetPeopleGroupLOV")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPeopleGroupLOV()
        {
        
        
        try {
            String assetData = elmentEligibilityDAO.getPeopleGroupLOVData();
            return Response.ok().entity(assetData).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                                    "    \"errorCode\": \"500\",\n" +
                                    "    \"errorMessage\": \"" + e.getMessage() + "\"\n" +
                                    "}").build();
        }
    }
    
    
}
