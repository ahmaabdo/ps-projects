package com.appspro.payroll.rest;


import com.appspro.payroll.bean.ElementFormBean;
import com.appspro.payroll.dao.CallProcedure;

import com.appspro.payroll.dao.ElementEnrtyFormDAO;


import com.google.gson.Gson;

import com.google.gson.GsonBuilder;

import common.restHelper.RestHelper;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/AppsProRest")
public class DefineElementRest {

    ElementEnrtyFormDAO elementEnrtyFormDAO = new ElementEnrtyFormDAO();

    @POST
    @Path("/defineElementForm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response defineElementForm(String defineElementBean) throws ParseException {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            elementFormBeanList =
                    elementEnrtyFormDAO.insertProcedure(defineElementBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementFormBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
    }

    @PUT
    @Path("/updateElementForm/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateElementForm(@PathParam("id")
        int id, String updateElementBean) throws ParseException {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            elementFormBeanList =
                    elementEnrtyFormDAO.updateProcedure(id, updateElementBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementFormBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @GET
    @Path("/GetElementForm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllElementForm() {

        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            elementFormBeanList = elementEnrtyFormDAO.getAllElement();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementFormBeanList)).build();

        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetElementForm/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getElementById(@PathParam("id")
        Integer id) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            elementFormBeanList = elementEnrtyFormDAO.getElementById(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementFormBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("/SearchElementFormKeyword/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response searchElementFormByKeyword(@PathParam("param")
        String param) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            elementFormBeanList =
                    elementEnrtyFormDAO.getElementByKeyword(param);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementFormBeanList)).build();

        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    //    @GET
    //    @Path("/SearchElementF")
    //    @Produces(MediaType.APPLICATION_JSON)
    //    @Consumes(MediaType.APPLICATION_JSON)
    //    public Response searchElementForm(@QueryParam("elementName")
    //        String elementName, @QueryParam("processType")
    //        String processType, @QueryParam("effectiveStartDate")
    //        String effectiveStartDate, @QueryParam("priority")
    //        String priority, @QueryParam("currency")
    //        String currency) {
    //        List<ElementFormBean> elementFormBeanList =
    //            new ArrayList<ElementFormBean>();
    //        try {
    //            elementFormBeanList =
    //                    elementEnrtyFormDAO.searchElement(elementName, processType,
    //                                                      effectiveStartDate,
    //                                                      priority, currency);
    //            Gson jsonConverter = new GsonBuilder().create();
    //            return Response.status(200).entity(jsonConverter.toJson(elementFormBeanList)).build();
    //
    //        } catch (Exception e) {
    //            return Response.status(500).entity("Something wrong please check!").build();
    //        }
    //    }

    @POST
    @Path("/SearchElementForm")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response searchElementForm2(String payload) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            elementFormBeanList = elementEnrtyFormDAO.searchElement(payload);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementFormBeanList)).build();

        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @DELETE
    @Path("DeleteElementForm/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteElementForm(@PathParam("id")
        Integer id) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            elementFormBeanList = elementEnrtyFormDAO.deleteElementForm(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementFormBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetCurrency")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCurrency() {
        String elementFormBeanList = null;
        try {
            elementFormBeanList = elementEnrtyFormDAO.getCurrency();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(elementFormBeanList).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

}
