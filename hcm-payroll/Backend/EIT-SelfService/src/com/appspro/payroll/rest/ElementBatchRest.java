package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementBatchBean;
import com.appspro.payroll.dao.ElementBatchDAO;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
//import org.json.simple.parser.JSONParser;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/elementBatch")
public class ElementBatchRest {
    ElementBatchDAO service = new ElementBatchDAO();
    @GET
    @Path("/GetElementBatch")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllObjectGroup() {
        List<ElementBatchBean> list =
            new ArrayList<ElementBatchBean>();
        try {
            list = service.getElementBatch();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(list)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public String createElemetBatch(String jsonRequest) throws JSONException, IOException,
                                                            SQLException {
        String list="";          
        JSONObject obj = new JSONObject(jsonRequest);
     list =service.addelEmentBatchV2(obj);
    return list;
    }

}
