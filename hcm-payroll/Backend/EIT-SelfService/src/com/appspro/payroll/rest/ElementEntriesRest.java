package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementEntriesBean;
import com.appspro.payroll.bean.ObjectGroupFormBean;
import com.appspro.payroll.dao.ElementEntriesDAO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

@Path("/AppsProRest")
public class ElementEntriesRest {
    ElementEntriesDAO elementEntriesDAO = new ElementEntriesDAO();

    @POST
    @Path("/ElementEntry")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response elementEntry(String elementEntryBean) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList =
                    elementEntriesDAO.insertProcedure(elementEntryBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
    }

    @PUT
    @Path("/UpdateElementEntry/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateElementEntry(@PathParam("id")
        int id, String elementEntryBean) throws ParseException {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList =
                    elementEntriesDAO.updateProcedure(id, elementEntryBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @GET
    @Path("/GetElementEntry")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllElementEntry() {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList = elementEntriesDAO.getAllElementEntry();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetElementEntry/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getElementEntryById(@PathParam("id")
        Integer id) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList =
                    elementEntriesDAO.getAllElementEntryById(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetElementEntryByPersonNum/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getElementEntryByPersonNum(@PathParam("id")
        String personNum) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList =
                    elementEntriesDAO.getElementEntryByPersonNum(personNum);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetElementEntryDetails")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getElementEntryDetailsById() {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList =
                    elementEntriesDAO.getAllElementEntryDetails();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetEmployee")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getEmployee() {
        String elementEntriesBeanList = null;
        try {
            elementEntriesBeanList = elementEntriesDAO.getEmployees();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(elementEntriesBeanList).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @POST
    @Path("DeleteElementEntry/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteElementEntry(@PathParam("id") Integer id, String body) {
        List<ElementEntriesBean> elementEntriesBeanList = new ArrayList<ElementEntriesBean>();
        JSONObject obj = new JSONObject(body);
        try {
            elementEntriesBeanList = elementEntriesDAO.deleteElementEntry(id,obj.get("endDate").toString());
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }
    
    @GET
    @Path("/GetElementEntryEligible/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllElementEntryEligible(@PathParam("id")
        String id) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList =
                    elementEntriesDAO.getAllElementEligable(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetElementEntryByEffectiveDate/{effectiveDate}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getElementEntryByEffectiveDate(@PathParam("effectiveDate")
        String effectiveDate) {
        String elementEntriesBeanList = null;
        try {
            elementEntriesBeanList =
                    elementEntriesDAO.getEmployeesByEffectiveDate(effectiveDate);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(elementEntriesDAO.getEmployeesByEffectiveDate(effectiveDate)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @PUT
    @Path("/CorrectElementEntry")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response correctElementEntry(String elementEntryBean) throws ParseException {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList =
                    elementEntriesDAO.correctElementEntryValue(elementEntryBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @GET
    @Path("/GetElementEntryByDate/{date}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllElementEntryByDate(@PathParam("date")
        String date) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            elementEntriesBeanList = elementEntriesDAO.getAllElementEntryByEffectiveDate(date);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementEntriesBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }
}
