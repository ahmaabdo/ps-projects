package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementFormBean;

import com.google.gson.Gson;
import com.appspro.payroll.dao.FastFormulaDAO;
import com.appspro.payroll.bean.FastFormulaBean;

import com.google.gson.GsonBuilder;

import java.sql.SQLException;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/FastFormulaRest")
public class FastFormulaRest {
    FastFormulaDAO services=new FastFormulaDAO();
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/addFastFormula")
    public Response addFastFormula(String jsonRequest) throws ParseException {
        Gson gson = new Gson();
        FastFormulaBean bean = gson.fromJson(jsonRequest, FastFormulaBean.class);
        Gson jsonConverter = new GsonBuilder().create();
        String list;
        try {
            list = services.addFastFormula(bean);
            return Response.status(201).entity(jsonConverter.toJson(list)).build();
        } catch (SQLException e) {
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
       
    }
    @GET
    @Path("/GetFastFormula")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetfastFourmla() {
        List<FastFormulaBean> list =
            new ArrayList<FastFormulaBean>();
        try {
            list = services.getFastFormula();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(list)).build();

        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/updateFastFormula")
    public Response updateFastFourmla(String jsonRequest) throws ParseException {
        Gson gson = new Gson();
        FastFormulaBean bean = gson.fromJson(jsonRequest, FastFormulaBean.class);
        Gson jsonConverter = new GsonBuilder().create();
        String list;
        try {
            list = services.updateFastFormula(bean);
            return Response.status(201).entity(jsonConverter.toJson(list)).build();
        } catch (SQLException e) {
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
       
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/delete")
    public Response deleteFastFormula(String jsonRequest)   {
        Gson gson = new Gson();
        FastFormulaBean bean = gson.fromJson(jsonRequest, FastFormulaBean.class);
        Gson jsonConverter = new GsonBuilder().create();
        String list;
        try {
            list = services.deleteFastFormula(bean);
            return Response.status(201).entity(jsonConverter.toJson(list)).build();
        } catch (SQLException e) {
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
       
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/compile")
    public Response compileFastFormula(String jsonRequest) throws ParseException {
        Gson gson = new Gson();
        FastFormulaBean bean =
            gson.fromJson(jsonRequest, FastFormulaBean.class);
        Gson jsonConverter = new GsonBuilder().create();
        String list;
        try {
            list = services.compileFastFormula(bean);
            return Response.status(201).entity(jsonConverter.toJson(list)).build();
        } catch (SQLException e) {
            return Response.status(201).entity("Something Wrong Please Check").build();
        }

    }
    
}
