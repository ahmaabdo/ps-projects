package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementFormBean;

import com.appspro.payroll.bean.ObjectGroupFormBean;

import com.appspro.payroll.dao.ObjectGroupFormDAO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/AppsProRest")
public class ObjectGroupFormRest {

    ObjectGroupFormDAO objectGroupFormDAO = new ObjectGroupFormDAO();

    @POST
    @Path("/objectGroupForm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response objectGroupForm(String objectGroupFormBean) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            objectGroupFormBeanList =
                    objectGroupFormDAO.insertProcedure(objectGroupFormBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(objectGroupFormBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
    }

    @GET
    @Path("/GetObjectGroup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllObjectGroup() {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            objectGroupFormBeanList = objectGroupFormDAO.getAllObjectGroup();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(objectGroupFormBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetObjectGroupHeader/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getObjectGroupHeaderId(@PathParam("id")
        Integer id) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            objectGroupFormBeanList =
                    objectGroupFormDAO.getObjectGroupHeaderById(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(objectGroupFormBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetObjectGroupDetails/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getObjectGroupDetailsById(@PathParam("id")
        Integer id) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            objectGroupFormBeanList =
                    objectGroupFormDAO.getObjectGroupDetailsById(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(objectGroupFormBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("GetObjectGroupClassiDetails/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getObjectGroupClassiDetailsById(@PathParam("id")
        Integer id) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            objectGroupFormBeanList =
                    objectGroupFormDAO.getObjectGroupDetailsClassiById(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(objectGroupFormBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("/SearchObjectGroupKeyword/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response searchObjectGroupByKeyword(@PathParam("param")
        String param) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            objectGroupFormBeanList =
                    objectGroupFormDAO.getObjectGroupByKeyword(param);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(objectGroupFormBeanList)).build();

        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @PUT
    @Path("/UpdateObjectGroup/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateObjectGroup(@PathParam("id")
        int id, String updateElementBean) throws ParseException {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            objectGroupFormBeanList =
                    objectGroupFormDAO.updateProcedure(id, updateElementBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(objectGroupFormBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @DELETE
    @Path("DeleteObjectGroup/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteObjectGroup(@PathParam("id")
        Integer id) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            objectGroupFormBeanList = objectGroupFormDAO.deleteObjectGroup(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(objectGroupFormBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

}
