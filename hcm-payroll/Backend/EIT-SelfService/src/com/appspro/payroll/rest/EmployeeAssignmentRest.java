package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementEntriesBean;
import com.appspro.payroll.bean.EmployeeAssignementBean;
import com.appspro.payroll.dao.EmployeeAssignmentDAO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/AppsProRest")
public class EmployeeAssignmentRest {

    EmployeeAssignmentDAO employeeAssignmentDAO = new EmployeeAssignmentDAO();

    @GET
    @Path("/GetEmpDetails")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllEmpDetails() {
        List<EmployeeAssignementBean> employeeAssignementBeanList =
            new ArrayList<EmployeeAssignementBean>();
        try {
            employeeAssignementBeanList =
                    employeeAssignmentDAO.getAllEmpDetails();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(employeeAssignementBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

}
