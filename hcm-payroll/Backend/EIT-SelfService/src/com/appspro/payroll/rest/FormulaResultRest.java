package com.appspro.payroll.rest;


import com.appspro.payroll.bean.FormulaResultBean;


import com.appspro.payroll.bean.FormulaResultRulesBean;
import com.appspro.payroll.dao.FormulaResultDAO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/AppsProRest")
public class FormulaResultRest {
    FormulaResultDAO formulaResultDAO = new FormulaResultDAO();

    @POST
    @Path("/insertFormulaResult")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertFormulaResult(String formulaResultBean) {
        List<FormulaResultBean> formulaResultBeanList =
            new ArrayList<FormulaResultBean>();
        try {
            formulaResultBeanList =
                    formulaResultDAO.insertFormulaResult(formulaResultBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(formulaResultBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
    }

    @PUT
    @Path("/UpdateFormulaResult")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateFormulaResult(String formulaResultBean) {
        List<FormulaResultBean> formulaResultBeanList =
            new ArrayList<FormulaResultBean>();
        try {
            formulaResultBeanList =
                    formulaResultDAO.updateFormulaResult(formulaResultBean);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(formulaResultBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @GET
    @Path("GetAllFormulaResultsByElementId/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllFormulaResultsByElementId(@PathParam("id")
        Integer id) {
        List<FormulaResultBean> formulaResultBeanList =
            new ArrayList<FormulaResultBean>();
        try {
            formulaResultBeanList =
                    formulaResultDAO.getAllFormulaResultsByElementId(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(formulaResultBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }


    @GET
    @Path("GetFormulaResultById/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFormulaResultById(@PathParam("id")
        Integer id) {
        FormulaResultBean formulaResultBean = new FormulaResultBean();
        try {
            formulaResultBean = formulaResultDAO.getFormulaResultById(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(formulaResultBean)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @DELETE
    @Path("DeleteFormulaResult/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String deleteFormulaResult(@PathParam("id")
        int id) {


        try {
            return formulaResultDAO.deleteFormulaResultById(id);

        } catch (Exception e) {
            return e.toString();
        }

    }
    
    
    @POST
    @Path("/insertFormulaResultRule")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertFormulaResultRule(String formulaResultRule) {
        FormulaResultRulesBean formulaResultRuleBean =
            new FormulaResultRulesBean();
        try {
            formulaResultRuleBean =
                    formulaResultDAO.insertFormulaResultRule(formulaResultRule);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(formulaResultRuleBean)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
    }

    @PUT
    @Path("/UpdateFormulaResultRule")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateFormulaResultRule(String formulaResultRule) {
        FormulaResultRulesBean formulaResultRuleBean =
            new FormulaResultRulesBean();
        try {
            formulaResultRuleBean =
                    formulaResultDAO.updateFormulaResultRule(formulaResultRule);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(formulaResultRuleBean)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    
    @DELETE
    @Path("DeleteFormulaResultRule/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String deleteFormulaResultRule(@PathParam("id")
        int id) {


        try {
            return formulaResultDAO.deleteFormulaResultRuleById(id);

        } catch (Exception e) {
            return e.toString();
        }

    }


}
