package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementCostingBean;

import com.appspro.payroll.dao.ElementCostingDAO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("/AppsProRest")

public class ElementCostingRest {
    ElementCostingDAO elmentCostingDAO = new ElementCostingDAO();

    @POST
    @Path("/insertElementCosting")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertElementCosting(String costingBean) {
        List<ElementCostingBean> elementCostingList =
            new ArrayList<ElementCostingBean>();
        try {
            elementCostingList =
                    elmentCostingDAO.insertElementCosting(costingBean.toString());
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementCostingList)).build();

        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(201).entity("Something Wrong Please Check").build();
        }
    }


    @PUT
    @Path("/updateElementCosting/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateElementCosting(String costingBean, @PathParam("id") int id) {
        List<ElementCostingBean> elementCostingList =
            new ArrayList<ElementCostingBean>();

        try {
            elementCostingList =
                    elmentCostingDAO.updateProcedure(costingBean.toString(), id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementCostingList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }
    
    @GET
    @Path("getElementCostingSegments")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getElementCostingSegments()
        {
        try {
            String assetData = elmentCostingDAO.getElementCostingSegmentsFromSaaS();
            return Response.ok().entity(assetData).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                                    "    \"errorCode\": \"500\",\n" +
                                    "    \"errorMessage\": \"" + e.getMessage() + "\"\n" +
                                    "}").build();
        }
    }

    @GET
    @Path("/GetAllElementCosting")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String GetAllElementCosting() {

        List<ElementCostingBean> elementCostingBeanList =
            new ArrayList<ElementCostingBean>();
        try {
            elementCostingBeanList = elmentCostingDAO.getAllElementCosting();

            return new JSONArray(elementCostingBeanList).toString();

        } catch (Exception e) {
            return e.toString();
        }
    }

    @GET
    @Path("GetElementCosting/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getElementCostingById(@PathParam("id") int id) {

        List<ElementCostingBean> elementCostingBeanList =
            new ArrayList<ElementCostingBean>();
        try {
            elementCostingBeanList =
                    elmentCostingDAO.getElementCostingById(id);
            return new JSONArray(elementCostingBeanList).toString();
        } catch (Exception e) {
            return e.toString();
        }
    }

    @DELETE
    @Path("DeleteElementCosting/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String deleteElementCostingForm(@PathParam("id") int id) {
        try {
            return elmentCostingDAO.deleteElementCostingById(id);

        } catch (Exception e) {
            return e.toString();
        }

    }


}
