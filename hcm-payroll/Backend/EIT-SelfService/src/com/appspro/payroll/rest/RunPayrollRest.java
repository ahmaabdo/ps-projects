package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementEntriesBean;

import com.appspro.payroll.dao.RunPeyrollDAO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/AppsProRest")
public class RunPayrollRest {
    RunPeyrollDAO runPeyrollDAO = new RunPeyrollDAO();

    @GET
    @Path("/PeyrollName/{payrollName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPayrollName(@PathParam("payrollName")
        String payrollName) throws ParseException {
        String payrollNameList = null;
        try {
            payrollNameList = runPeyrollDAO.getPayroll(payrollName);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(runPeyrollDAO.getPayroll(payrollName)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @GET
    @Path("/PeriodlName")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPeriodlName() throws ParseException {
        String payrollNameList = null;
        try {
            payrollNameList = runPeyrollDAO.getPeriod();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(runPeyrollDAO.getPeriod()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @GET
    @Path("/AssignmentGroup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAssignmentGroup() throws ParseException {
        String payrollNameList = null;
        try {
            payrollNameList = runPeyrollDAO.getAssignmentGroup();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(runPeyrollDAO.getAssignmentGroup()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }
}
