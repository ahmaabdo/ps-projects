package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementFormBean;
import com.appspro.payroll.bean.ElementInputValueBean;
import com.appspro.payroll.dao.ElementEnrtyFormDAO;

import com.appspro.payroll.dao.ElementInputValueDAO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/AppsProRest")
public class ElementInputValueRest {

    ElementInputValueDAO elementInputValueDAO = new ElementInputValueDAO();

    @POST
    @Path("/InputValue")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response elementInputValue(String elementInputValue) throws ParseException {
        List<ElementInputValueBean> elementInputValueBean =
            new ArrayList<ElementInputValueBean>();
        try {
            elementInputValueBean =
                    elementInputValueDAO.insertProcedure(elementInputValue);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementInputValueBean)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @PUT
    @Path("/UpdateInputValue/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateInputValue(@PathParam("id")
        int id, String updateInputValue) throws ParseException {
        List<ElementInputValueBean> elementInputValueBeanList =
            new ArrayList<ElementInputValueBean>();
        try {
            elementInputValueBeanList =
                    elementInputValueDAO.updateProcedure(id, updateInputValue);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(201).entity(jsonConverter.toJson(elementInputValueBeanList)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity("Something Wrong Please Check").build();
        }
    }

    @GET
    @Path("/GetInputValue/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllInputValue(@PathParam("id")
        Integer id) {

        List<ElementInputValueBean> elementInputValueBeanList =
            new ArrayList<ElementInputValueBean>();
        try {
            elementInputValueBeanList =
                    elementInputValueDAO.getElementInputValueById(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementInputValueBeanList)).build();

        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @GET
    @Path("/GetInputValueById/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetInputValueById(@PathParam("id")
        Integer id) {

        List<ElementInputValueBean> elementInputValueBeanList =
            new ArrayList<ElementInputValueBean>();
        try {
            elementInputValueBeanList =
                    elementInputValueDAO.getInputValueByInputId(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementInputValueBeanList)).build();

        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

    @DELETE
    @Path("/DeleteInputValue/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteInputValue(@PathParam("id")
        Integer id) {
        List<ElementInputValueBean> elementInputValueBeanList =
            new ArrayList<ElementInputValueBean>();
        try {
            elementInputValueBeanList =
                    elementInputValueDAO.DeleteInputValue(id);
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(jsonConverter.toJson(elementInputValueBeanList)).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }

}
