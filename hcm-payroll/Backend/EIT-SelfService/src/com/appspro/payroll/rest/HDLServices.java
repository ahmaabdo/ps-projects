/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementEntryBean;
import com.appspro.payroll.dao.ElementEntryDAO;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Shadi Mansi-PC
 */
@Path("HDL/")
public class HDLServices {
    ElementEntryDAO services = new ElementEntryDAO();
    
@POST
@Path("/searchHDL")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
   public String getData(String body,@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
       List<ElementEntryBean> list = new ArrayList<ElementEntryBean>();
       try {
           ObjectMapper mapper = new ObjectMapper();
           ElementEntryBean guaranteeBean = mapper.readValue(body, ElementEntryBean.class);
           list = services.getElementEntryPayrollValidator(guaranteeBean);
           return  new  JSONArray(list).toString();
       } catch (Exception e) {
           return new  JSONArray(list).toString();
       }
        
   }
   
    @POST
    @Path("/editHDL")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
       public String editHDLElementEntry(String body,@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
           List<ElementEntryBean> list = new ArrayList<ElementEntryBean>();
           String str = "";
           try {
               str = services.editHDL(body);
               return str;
           } catch (Exception e) {
               //
           }
             
           return str;
            
       }
    
}
