package com.appspro.payroll.rest;

import com.appspro.payroll.dao.LookupDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("/AppsProRest")
public class PaasLookUpRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/Lookup")
    public Response getAll(@HeaderParam("authorization")
        String authString) {
        LookupDAO lookupDAO = new LookupDAO();
        try {
//            if (authString.equals("QXBwc1BybzpBcHBzUFJvQDMxMg==")) {
                JSONArray json = new JSONArray(lookupDAO.getLookup());
                return Response.status(200).entity(json.toString()).build();
//            } else {
//                return Response.status(Response.Status.UNAUTHORIZED).build();
//            }
        } catch (Exception ex) {
            return Response.status(500).entity(lookupDAO.getLookup()).build();
        }
    }

}
