package com.appspro.payroll.rest;

import com.appspro.payroll.bean.ElementEntriesBean;
import com.appspro.payroll.bean.ViewProcessResultBean;
import com.appspro.payroll.dao.ViewProcessResultDAO;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/AppsProRest")
public class ViewProcessResultRest {
    ViewProcessResultDAO viewProcessResultDAO = new ViewProcessResultDAO();

    @GET
    @Path("/GetProcessResult")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllProcessResult() {
        List<ViewProcessResultBean> viewProcessResultBeanList =
            new ArrayList<ViewProcessResultBean>();
        try {
            JSONObject  obj=
                    viewProcessResultDAO.getAllProcessResult();
            Gson jsonConverter = new GsonBuilder().create();
            return Response.status(200).entity(obj.toString()).build();
        } catch (Exception e) {
            return Response.status(500).entity("Something wrong please check!").build();
        }
    }
}
