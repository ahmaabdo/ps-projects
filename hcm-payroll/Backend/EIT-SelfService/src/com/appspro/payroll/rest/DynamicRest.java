package com.appspro.payroll.rest;

import com.appspro.payroll.bean.MailTemplateBean;
import com.appspro.payroll.dao.DynamicGetApiDAO;
import com.appspro.payroll.dao.MailTemplateDao;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/dynsmicRes")
public class DynamicRest {
    @GET
    @Path("/{getCode}")
    public Response get(@PathParam("getCode") String getCode) {
        try {
            DynamicGetApiDAO det = new DynamicGetApiDAO();
            List<JSONObject> lst = det.getDynamic(getCode);
            return Response.ok(new JSONArray(lst).toString(),
                               MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

}
