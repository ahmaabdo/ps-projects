/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.ElementEntryBean;

import common.biPReports.BIReportModel;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import static common.restHelper.RestHelper.getSchema_Name;


/**
 *
 * @author Shadi Mansi-PC
 */
public class ElementEntryDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    BIReportModel objBIR=new BIReportModel();
    public String insertOrUpdateElementEntry(String bean,
                                             String transactionType) {
        try {
            JSONObject jsonObj = new JSONObject(bean);

            connection = AppsproConnection.getConnection();
            String query = "";
            if (transactionType.equals("ADD")) {
                query =
                        "insert INTO " + getSchema_Name() + ".XX_SS_HDL_FILE (SS_ID,SS_TYPE,UCM_ID,STATUS,PROCESS_ID,CREATION_DATE,UPDATED_DATE,FILE_CONTENT,PERSON_NUMBER,FILE_BYTES,FILE_PATH,\n" +
                        "         FULL_NAME,HIRE_DATE,POSITION_NAME,GRADE_NAME,ASSIGNMENT_STATUS_TYPE,END_DATE,START_DATE,ADVANCED_LEAVE,FILE_NAME,EIT_CODE) values (?,?,?,?,?,SYSDATE,?,?,\n" +
                        "		 ?,?,?,?,?,?,?,?,?,?,?,?,?)";

                ps = connection.prepareStatement(query);
                ps.setInt(1, Integer.parseInt(jsonObj.getString("SS_ID")));
                ps.setString(2, jsonObj.getString("SS_TYPE"));
                ps.setString(3, "");
                ps.setString(4, jsonObj.getString("STATUS"));
                ps.setString(5, "");
                ps.setString(6, "");
                ps.setString(7, jsonObj.getString("FILE_CONTENT"));
                ps.setString(8, jsonObj.getString("PERSON_NUMBER"));
                ps.setString(9, jsonObj.getString("FILE_BYTES"));
                ps.setString(10, " x");
                ps.setString(11," x");
                ps.setString(12," x");
                ps.setString(13, " x");
                ps.setString(14, " x");
                ps.setString(15," x");
                ps.setString(16, jsonObj.getString("END_DATE"));
                ps.setString(17, jsonObj.getString("START_DATE"));
                ps.setString(18, jsonObj.getString("ADVANCED_LEAVE"));
                ps.setString(19, jsonObj.getString("FILE_NAME"));
                ps.setString(20, jsonObj.getString("EIT_CODE"));
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {

                query = "BEGIN\n" +
                        "update XX_SS_HDL_FILE set\n" +
                        "            STATUS          = NVL(?,STATUS),\n" +
                        "            PROCESS_ID      = ?,\n" +
                        "            UPDATED_DATE    = SYSDATE,\n" +
                        "            UCM_ID = NVL(?,UCM_ID)\n" +
                        "         where ID = ?;\n" +
                        "         \n" +
                        "         UPDATE XX_SELF_SERVICE SET STATUS = ? WHERE TRANSACTION_ID = (SELECT SS_ID FROM XX_SS_HDL_FILE WHERE ID  = ?) AND TYPE = (SELECT SS_TYPE FROM XX_SS_HDL_FILE WHERE ID  = ?);\n" +
                        "         \n" +
                        "         END;";
                ps = connection.prepareStatement(query);
                //                ps.setString(1, jsonObj.getString("STATUS"));
                //                ps.setString(2, "");
                //                ps.setString(3, "");
                //                ps.setString(4, jsonObj.getString("STATUS"));
                //                ps.setInt(5, jsonObj.getString("ID"));
                //                ps.setInt(6, jsonObj.getString("ID"));
                //                ps.executeUpdate();
            }

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return "";
    }

    public List<ElementEntryBean> getElementEntryPayrollValidator(ElementEntryBean bean) {
        List<ElementEntryBean> list = new ArrayList<ElementEntryBean>();
        if (bean.getStatus().equals(null) || bean.getStatus().equals("")) {
            bean.setStatus(null);
        }
        if (bean.getEitCode().equals(null) || bean.getEitCode().equals("")) {
                        bean.setEitCode(null);
                    }
        if (bean.getPersonNumber().equals(null) ||
            bean.getPersonNumber().equals("")) {
            bean.setPersonNumber(null);
        }
        if (bean.getAssignmentStatusType().equals(null) ||
            bean.getAssignmentStatusType().equals("")) {
            bean.setAssignmentStatusType(null);
        }
        try {

            ElementEntryBean obj = null;
            connection = AppsproConnection.getConnection();
            String query = " SELECT \n"
                                + "    id,UCM_ID,EIT_CODE,SS_TYPE,SS_ID,PROCESS_ID,CREATION_DATE,FILE_BYTES,FILE_CONTENT,FILE_PATH,PERSON_NUMBER,FULL_NAME,HIRE_DATE,POSITION_NAME,GRADE_NAME,\n"
                                + "    (SELECT value_ar from XXX_PAAS_LOOKUP where name = 'SELF_TYPE' and code = SS_TYPE) ss_ar,\n"
                                + "    (SELECT value_en from XXX_PAAS_LOOKUP where name = 'SELF_TYPE' and code = SS_TYPE) ss_en,\n"
                                + "    (SELECT value_ar from XXX_PAAS_LOOKUP where name = 'ASSIGNMENT_STATUS_TYPE' and code = ASSIGNMENT_STATUS_TYPE) ast_ar,\n"
                                + "    (SELECT value_en from XXX_PAAS_LOOKUP where name = 'ASSIGNMENT_STATUS_TYPE' and code = ASSIGNMENT_STATUS_TYPE) ast_en,\n"
                                + "    decode(status,'Upload Element File','NO','YES') status,\n"
                                + "    FILE_NAME,\n"
                                + "    START_DATE,\n"
                                + "    END_DATE,\n"
                                + "    ADVANCED_LEAVE\n"
                                + "    FROM \n"
                                + "        XX_SS_HDL_FILE \n"
                                + "    WHERE EIT_CODE = NVL(?,EIT_CODE)\n"
                                + "          AND  STATUS = decode(?,null,STATUS,'YES','COMPLETED','Upload Element File')\n"
                                + "          AND PERSON_NUMBER = NVL(?,PERSON_NUMBER)\n"
                                + "          AND ASSIGNMENT_STATUS_TYPE = decode(?,null,ASSIGNMENT_STATUS_TYPE,?)\n"
                                + "    order by id desc";

                        ps = connection.prepareStatement(query);
                        ps.setString(1, bean.getEitCode());
                        ps.setString(2, bean.getStatus());
                        ps.setString(3, bean.getPersonNumber());
                        ps.setString(4, bean.getAssignmentStatusType());
                        ps.setString(5, bean.getAssignmentStatusType());
                        rs = ps.executeQuery();
                        while (rs.next()) {
                            obj = new ElementEntryBean();
                            obj.setId(Integer.parseInt(rs.getString("ID")));
                            obj.setUcmId(rs.getString("UCM_ID"));
                            obj.setSsType(rs.getString("SS_TYPE"));
                            obj.setSsId(Integer.parseInt(rs.getString("SS_ID")));
                            obj.setProssesId(rs.getString("PROCESS_ID"));
                            obj.setEitCode(rs.getString("EIT_CODE"));
                            obj.setCreationDate(rs.getString("CREATION_DATE"));
                            obj.setFileBytes(rs.getString("FILE_BYTES"));
                            obj.setFileContent(rs.getString("FILE_CONTENT"));

                            obj.setFilePath(rs.getString("FILE_PATH"));
                            obj.setPersonNumber(rs.getString("PERSON_NUMBER"));
                            obj.setFullName(rs.getString("FULL_NAME"));

                            obj.setHireDate(rs.getString("HIRE_DATE"));
                            obj.setPositionName(rs.getString("POSITION_NAME"));
                            obj.setGradeName(rs.getString("GRADE_NAME"));

                            obj.setSsAr(rs.getString("ss_ar"));
                            obj.setSsEn(rs.getString("ss_en"));
                            obj.setAstAr(rs.getString("ast_ar"));
                            obj.setAstEn(rs.getString("ast_en"));

                            obj.setStatus(rs.getString("status"));
                            obj.setFileName(rs.getString("FILE_NAME"));
                            obj.setStartDate(rs.getString("START_DATE"));
                            obj.setEndDate(rs.getString("END_DATE"));
                            obj.setAdvancedLeave(rs.getString("ADVANCED_LEAVE"));
                            list.add(obj);
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (Exception e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);

        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public String editHDL(String body) {
        
        JSONObject jsonObj = new JSONObject(body);
        
        try {
            connection = AppsproConnection.getConnection();
            String query = "";
            query = "update XX_SS_HDL_FILE set STATUS = ?,\n" +
                    "                     UPDATED_DATE = ?\n" +
                    "                     where id = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, jsonObj.getString("STATUS"));
            ps.setString(2, objBIR.getDateInGMT());
            ps.setInt(3, Integer.parseInt(jsonObj.get("id").toString()));
            ps.executeUpdate();
            //            query = "UPDATE XX_SELF_SERVICE SET STATUS = ? WHERE TRANSACTION_ID = (SELECT SS_ID FROM XX_SS_HDL_FILE WHERE ID  = ?) AND TYPE = (SELECT SS_TYPE FROM XX_SS_HDL_FILE WHERE ID  = ?)";
            //            ps = connection.prepareStatement(query);
            //            ps.setString(1, bean.getStatus());
            //            ps.setInt(2, bean.getId());
            //            ps.setInt(3, bean.getId());
            //            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return "";
    }

    public static void main(String[] args) {
        ElementEntryBean obj = new ElementEntryBean();
        ElementEntryDAO o = new ElementEntryDAO();
        obj.setAdvancedLeave("ff");
        obj.setSsId(2);
        obj.setSsType("dgss");
        //o.insertOrUpdateElementEntry(obj, "ADD");

    }
}
