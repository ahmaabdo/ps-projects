package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.ClassificationELementBean;
import com.appspro.payroll.bean.LookupBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

public class classificationELementDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public List<ClassificationELementBean> getClassificationElement() {
        List<ClassificationELementBean> classificationELementList = new ArrayList<ClassificationELementBean>();
        connection = AppsproConnection.getConnection();

        String query = null;
        query = "SELECT * FROM PAY_ELE_CLASSIFICATIONS";
        System.out.println("\nExecuting query: " + query);
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                ClassificationELementBean classificationELementBean = new ClassificationELementBean();
                classificationELementBean.setClassificationId(rs.getInt("CLASSIFICATION_ID"));
                classificationELementBean.setClassificationName(rs.getString("BASE_CLASSIFICATION_NAME"));
                classificationELementBean.setCreationDate(rs.getDate("CREATION_DATE"));
                classificationELementList.add(classificationELementBean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return classificationELementList;
    }

}

