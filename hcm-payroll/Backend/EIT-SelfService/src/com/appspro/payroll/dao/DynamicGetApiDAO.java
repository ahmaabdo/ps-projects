package com.appspro.payroll.dao;
import com.appspro.payroll.bean.DynamicGetBean;

import com.appspro.payroll.bean.DynamicGetSup;
import com.appspro.payroll.bean.SaaSPositionBean;
import common.restHelper.RestHelper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import org.json.JSONArray;
import org.json.JSONObject;
public class DynamicGetApiDAO extends RestHelper  {
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
//    SaaSPositionDetails det = new SaaSPositionDetails();
//    ArrayList<SaaSPositionBean> organizationList = det.getOrgDetails();
        public ArrayList<JSONObject> getDynamic(String GET_CODE) {
        DynamicApi det = new DynamicApi();
        ArrayList<DynamicGetBean> dynamicApi = det.getDymamic(GET_CODE);
        ArrayList<DynamicGetSup> dynamicsupApi = det.getDymamicSup(GET_CODE);
        //getDymamicSup
        DynamicGetBean apiBean = dynamicApi.get(0);
        ArrayList<JSONObject> resList = new ArrayList<JSONObject>();
        String finalresponse = "";
        //        String jwttoken = jwt.trim();
        SaaSPositionBean org = new SaaSPositionBean();
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        System.out.println(apiBean.getGET_PATH());
        String serverUrl = getInstanceUrl() + apiBean.getGET_PATH()+"?limit=500";
        String jsonResponse = "";
        System.out.println(serverUrl);
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction
                    = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.se
            //equestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                    "Basic " + getAuth());
            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray(apiBean.getRESPONSE_MAIN());
            System.out.println(arr.length());
            System.out.println(dynamicsupApi.size());
            if(dynamicsupApi.size()==0){
                for (int i = 0; i < arr.length(); i++) {
                    resList.add((JSONObject)arr.get(i));
                }
            }else{
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject jsonRes = new JSONObject();
                    for(int x=0;x<dynamicsupApi.size();x++){
//                        dynamicsupApi.get(x).getSUP_NAME()
                        JSONObject items = (JSONObject)arr.get(i);
                        jsonRes.put(dynamicsupApi.get(x).getSUP_NAME(),
                                    items.get(dynamicsupApi.get(x).getSUP_NAME()))    ;
                    }
                    resList.add(jsonRes);
                }
            }
        } catch (Exception e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }
        if (finalresponse.length() > 1) {
        } else {
            finalresponse = jsonResponse;
        }
        return resList;
    }
}
