/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.EmployeeBean;

//import com.appspro.receipt.bean.TrackServerRequestBean;
import com.appspro.payroll.rest.BIReportService;

import common.biPReports.BIPReports;

import common.biPReports.BIReportModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

import org.joda.time.LocalDate;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import utilities.EncodeBased64Binary;

import java.util.Date;

/**
 *
 * @author Shadi Mansi-PC
 */
public class HCMElementEntryHelper {

    public static final String ELEMENT_HEADER =
        "METADATA|ElementEntry|SourceSystemOwner|SourceSystemId|EffectiveStartDate|EffectiveEndDate|ElementName|LegislativeDataGroupName|AssignmentNumber|EntryType|CreatorType";
    public static final String ELEMENT_LINE =
        "METADATA|ElementEntryValue|SourceSystemOwner|SourceSystemId|ElementEntryId(SourceSystemId)|EffectiveStartDate|EffectiveEndDate|ElementName|LegislativeDataGroupName|AssignmentNumber|InputValueName|ScreenEntryValue";
    //
    public static final String ELEMENT_HEADER_ALLOWANCE =
        "METADATA|WorkTerms|SourceSystemOwner|SourceSystemId|PeriodOfServiceId(SourceSystemId)|ActionCode|AssignmentNumber|AssignmentStatusTypeCode|AssignmentType|EffectiveStartDate|EffectiveEndDate|EffectiveSequence|EffectiveLatestChange|SystemPersonType|BusinessUnitShortCode|LegalEmployerName|PersonTypeCode|PersonNumber";
    public static final String ELEMENT_LINE_ALLOWANCE =
        "METADATA|Assignment|SourceSystemOwner|SourceSystemId|WorkTermsAssignmentId(SourceSystemId)|PeriodOfServiceId(SourceSystemId)|ActionCode|AssignmentNumber|WorkTermsNumber|AssignmentStatusTypeCode|AssignmentType|EffectiveStartDate|EffectiveEndDate|BusinessUnitShortCode|SystemPersonType|PersonTypeCode|LegalEmployerName|EffectiveSequence|EffectiveLatestChange|ReasonCode|PersonNumber|WorkAtHomeFlag|ManagerFlag|HourlySalariedCode|NormalHours|Frequency|AssignmentCategory|PeopleGroup";

    public static final String ELEMENT_HEADER_WORKER_NO_PG =
        "METADATA|WorkTerms|SourceSystemOwner|SourceSystemId|PeriodOfServiceId(SourceSystemId)|ActionCode|AssignmentNumber|AssignmentStatusTypeCode|AssignmentType|EffectiveStartDate|EffectiveEndDate|EffectiveSequence|EffectiveLatestChange|SystemPersonType|BusinessUnitShortCode|LegalEmployerName|PersonTypeCode|PersonNumber";
    public static final String ELEMENT_LINE_WORKER_NO_PG =
        "METADATA|Assignment|SourceSystemOwner|SourceSystemId|WorkTermsAssignmentId(SourceSystemId)|PeriodOfServiceId(SourceSystemId)|ActionCode|AssignmentNumber|WorkTermsNumber|AssignmentStatusTypeCode|AssignmentType|EffectiveStartDate|EffectiveEndDate|BusinessUnitShortCode|SystemPersonType|PersonTypeCode|LegalEmployerName|EffectiveSequence|EffectiveLatestChange|ReasonCode|PersonNumber|WorkAtHomeFlag|ManagerFlag|HourlySalariedCode|NormalHours|Frequency";

    public static final String ELEMENT_HEADER_WORKER_PERSON_ABSENCE =
        "METADATA|PersonAbsenceEntry|Employer|PersonNumber|AbsenceType|AbsenceStatus|ApprovalStatus|StartDate|EndDate|SourceSystemOwner|SourceSystemId";
    public static final String ELEMENT_HEDER_EXTERNAL_BANK_ACCOUNT =
        "METADATA|ExternalBankAccount|BankName|BankBranchName|AccountNumber|CountryCode|CurrencyCode|IBAN|InactiveFlag";
    public static final String ELEMENT_HEDER_External_Bank_Account_Owner =
        "METADATA|ExternalBankAccountOwner|PersonNumber|BankName|BankBranchName|AccountNumber|CountryCode|CurrencyCode|PrimaryFlag";
    public static final String ELEMENT_HEDER_PERSONAL_PAYMENT_METHOD =
        "METADATA|PersonalPaymentMethod|LegislativeDataGroupName|AssignmentNumber|PersonalPaymentMethodCode|EffectiveStartDate|PaymentAmountType|Amount|ProcessingOrder|OrganizationPaymentMethodCode|Percentage|BankName|BankBranchName|BankCountryCode|BankAccountNumber";

    public HCMElementEntryHelper() {
        super();
    }

    public static String uploadAndLoadFile(String path, String fileContent,
                                           String toBytes,
                                           EmployeeBean emp) throws IOException {
        return uploadAndLoadFile(path, fileContent, toBytes, "NA", "NA", null,
                                 null, null, emp);
    }

    public static String uploadAndLoadFile(String path, String fileContent,
                                           String toBytes, String trsId,
                                           String ssType, String personNumber,
                                           String filename, String eitCode,
                                           EmployeeBean emp) throws IOException {
        String resp = "";
        String annualLeave = getLatestAnnualLeave(personNumber);

        try {

            return HDLHelper.insertHDLFile(null, trsId, ssType,
                                           "Upload Element File", null,
                                           fileContent, personNumber, toBytes,
                                           path,
                                           emp == null ? getPersonDetails(personNumber) :
                                           emp, annualLeave, //
                    filename, eitCode);


        } catch (JSONException e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }

        return resp;
    }

    public static boolean CALL_UCM(String hdlId, String startDate, String path,
                                   String sstype, String file_bytes,
                                   String file_name) {

        boolean resp = false;
        //(file_bytes);
        //(file_name);
        String filename = file_name;
        String fileContent = file_bytes;
        String toBytes = file_bytes;

        if (sstype.equals("N")) {
            toBytes =
                    toBytes.replaceAll("%EffectiveEndDate%", HCMElementEntryHelper.getlastDayofCuurentMonth());
            fileContent =
                    fileContent.replaceAll("%EffectiveEndDate%", HCMElementEntryHelper.getlastDayofCuurentMonth());


        } else {
            toBytes = toBytes.replaceAll("%EffectiveEndDate%", "");
            fileContent = fileContent.replaceAll("%EffectiveEndDate%", "");

        }
//        AppsproConnection.LOG.info(startDate);
        if (startDate.equals("%EffectiveStartDate%")) {
            toBytes =
                    toBytes.replaceAll("%EffectiveStartDate%", HCMElementEntryHelper.getFirstDayofCuurentMonth());

            fileContent =
                    fileContent.replaceAll("%EffectiveStartDate%", HCMElementEntryHelper.getFirstDayofCuurentMonth());
        } else {
            //this Case If Effivtive Start Date Of Elelement Dependent of EIT Segment

            if (startDate.contains("/") || startDate.contains("-")) {
                //this Case If Element value is Date
                // startDate.replaceAll("-", "/");
                toBytes =
                        toBytes.replaceAll("%EffectiveStartDate%", HCMElementEntryHelper.GET_FIRST_DATE_OF_SEPECIFIC_MONTH(startDate).replaceAll("-",
                                                                                                                                                 "/"));

                fileContent =
                        fileContent.replaceAll("%EffectiveStartDate%", HCMElementEntryHelper.GET_FIRST_DATE_OF_SEPECIFIC_MONTH(startDate).replaceAll("-",
                                                                                                                                                     "/"));
            } else {
                BIReportModel biModel = new BIReportModel();
                Map<String, String> reportMaping =
                    new HashMap<String, String>();
                reportMaping.put("TIME_PERIOD_ID", startDate);
                String PERIOD_NAME = "";
                JSONObject obj =
                    biModel.runReport("GET_TIME_PERIOD_REPORT", reportMaping).getJSONObject("DATA_DS").getJSONObject("G_1");
                PERIOD_NAME = obj.get("PERIOD_NAME").toString();


                toBytes =
                        toBytes.replaceAll("%EffectiveStartDate%", HCMElementEntryHelper.PERIOD_NAME_Date_Format(PERIOD_NAME));

                fileContent =
                        fileContent.replaceAll("%EffectiveStartDate%", HCMElementEntryHelper.PERIOD_NAME_Date_Format(PERIOD_NAME));


                //                JSONObject obj = new JSONObject();
            }
        }


//        AppsproConnection.LOG.info("********************************");
//        AppsproConnection.LOG.info(fileContent);
//        AppsproConnection.LOG.info("********************************");

        //            //(toBytes);

        //            try {
        //
        //                PrintWriter outp =
        //                    new PrintWriter(new FileWriter(path + "/"+filename+".dat"));
        //                outp.println(fileContent);
        //                outp.close();
        //            } catch (Exception e) {
        //               e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        //            }


        //=========to create a zip file and encrypt
        resp = createAndSendZipFile(filename, toBytes, hdlId);

        return resp;

    }

    public static boolean createAndSendZipFile(String filename, String toBytes,
                                               String hdlId) {
        boolean resp = false;

        try {
            File file = File.createTempFile(filename, ".zip");
            String zipFileName = filename + ".dat";

            ZipOutputStream out =
                new ZipOutputStream(new FileOutputStream(file));
            ZipEntry e = new ZipEntry(zipFileName);
            out.putNextEntry(e);

            byte[] data = toBytes.toString().getBytes();
            out.write(data, 0, data.length);
            out.closeEntry();
            //                file.deleteOnExit();
            out.close();
            byte[] bytes = loadFile(file);


            EncodeBased64Binary k = new EncodeBased64Binary();

            String docName =
                k.callUCMService(Base64.encodeBase64String(bytes), filename +
                                 ".zip");

            String response = null;
            if (docName != null)
                response = k.callLoaderService(docName, "", hdlId);
            if (response == null)
//               AppsproConnection.LOG.info("error in  loader");
            resp = docName != null && response != null;
        } catch (IOException e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
            if (hdlId != null) {
//                TrackServerRequestBean bean = new TrackServerRequestBean();
//                bean.setRequestId(hdlId);
//                bean.setElementResponseDate(TrackServerRequestBean.getCurrentDateTime());
//                bean.setElementResponseCode(200 + "");
//                EFFDetails.getInstance().editElementResponseDetails(bean);
            }
        }
        return resp;
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large


        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length &&
               (numRead = is.read(bytes, offset, bytes.length - offset)) >=
               0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " +
                                  file.getName());
        }

        is.close();
        return bytes;
    }

    public static String getFirstDayofCuurentMonth() {
        Calendar aCalendar = Calendar.getInstance();

        // set DATE to 1, so first date of previous month
        aCalendar.set(Calendar.DATE, 1);

        Date firstDateOfPreviousMonth = aCalendar.getTime();

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        return format.format(firstDateOfPreviousMonth);

    }

    public static String getlastDayofCuurentMonth() {
        Calendar aCalendar = Calendar.getInstance();
        // set actual maximum date of previous month
        aCalendar.set(Calendar.DATE,
                      aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        //read it
        Date lastDateOfPreviousMonth = aCalendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        return format.format(lastDateOfPreviousMonth);

    }

    private static EmployeeBean getPersonDetails(String personNumber) throws JSONException {
        EmployeeDetails empDetails = new EmployeeDetails();
        //        //(personNumber);
        EmployeeBean bean =
            empDetails.getEmpDetailsByPersonNum(personNumber, null, null);

        return bean;
    }

    public static String FORMAT_DATE(String date) {
        //read it
        String startDateString = date;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate;
        DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
        try {
            startDate = df.parse(startDateString);
            return df1.format(startDate);
        } catch (ParseException e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }

        return null;
    }

    public static String PERIOD_NAME_Date_Format(String PERIOD_NAME) {
        // String x =PERIOD_NAME;
        String PERIOD_Date = "";
        try {
            // String x =PERIOD_NAME;
            SimpleDateFormat formatter = new SimpleDateFormat("MM yyyy");
            SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd");

            PERIOD_Date = formatter2.format(formatter.parse(PERIOD_NAME));
//            AppsproConnection.LOG.info(formatter2.format(formatter.parse(PERIOD_NAME)));
        } catch (ParseException e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }
        return PERIOD_Date;
    }

    public static String GET_LAST_DATE_OF_SEPECIFIC_MONTH(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(date);
            //    //(c);
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            //            //(convertedDate);
            c.set(Calendar.DAY_OF_MONTH,
                  c.getActualMaximum(Calendar.DAY_OF_MONTH));

            return df1.format(c.getTime());
        } catch (ParseException e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }


        return null;
    }

    public static String GET_FIRST_DATE_OF_SEPECIFIC_MONTH(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(date);
            //    //(c);
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            //            //(convertedDate);
            c.set(Calendar.DAY_OF_MONTH,
                  c.getActualMinimum(Calendar.DAY_OF_MONTH));

            return df1.format(c.getTime());
        } catch (ParseException e) {
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }


        return null;
    }

    private static String getLatestAnnualLeave(String personNumber) throws JSONException {
        BIPReports biPReports = new BIPReports();
        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
        biPReports.getParamMap().put("PERSON_ID", personNumber);
        biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.GET_LATEST_ANNUAL_LEAVE.getValue());
        String fresponse = biPReports.executeReports();
        JSONObject xmlJSONObj = XML.toJSONObject(fresponse.toString());
        String jsonString = null;
        if (!xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1")) {
            jsonString =
                    xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString();
        }


        return jsonString;
    }

    private static final SimpleDateFormat[] FORMATTERS =
        new SimpleDateFormat[] { new SimpleDateFormat("MM/dd/yy") };

    public static String FROMAT_ANY_DATE(String dateInString) {
        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formatedDate = null;
        for (int i = 0; i < FORMATTERS.length; i++) {
            try {
                date = FORMATTERS[i].parse(dateInString);
                formatedDate = formatter.format(date);
                break;
            } catch (ParseException e) {
                // Nothing to do, try the next one
            }
        }
        return formatedDate;
    }

    public static void main(String[] args) {
        //        try {
        //        String x= "METADATA|ElementEntry|SourceSystemOwner|SourceSystemId|EffectiveStartDate|EffectiveEndDate|ElementName|LegislativeDataGroupName|AssignmentNumber|EntryType|CreatorType\n" +
        //        "MERGE|Assignment|NADEC|23125_WORKTERMS|23125_WORKREL|23125_WORKREL|HIRE|ET23125|ET23125|ACTIVE_PROCESS|ET|%EffectiveStartDate%|%EffectiveEndDate%|KSA AGRICULTURE BU|EMP|Employee|NADEC KSA LE|1|Y|Employee|23125|Employee|N|N|N|S|KSA_AGRICULTURE|PROVIDED.......Y.1835...Y.200.......\n" +
        //        "METADATA|ElementEntryValue|SourceSystemOwner|SourceSystemId|ElementEntryId(SourceSystemId)|EffectiveStartDate|EffectiveEndDate|ElementName|LegislativeDataGroupName|AssignmentNumber|InputValueName|ScreenEntryValue\n";
        //        x = x.replaceAll(Pattern.quote("%EffectiveStartDate%"),"X");
        //        x= x.replaceAll("%EffectiveEndDate%",
        //                               HCMElementEntryHelper.FORMAT_DATE("2050-04-08"));


        //(HCMElementEntryHelper.FORMAT_DATE("2050-04-08"));
        //        } catch (JSONException e) {
        //        }
        ;

    }

}

