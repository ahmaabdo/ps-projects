package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.payroll.bean.ElementFormBean;

import com.appspro.payroll.bean.EmployeeAssignementBean;
import com.appspro.payroll.bean.FastFormulaBean;

import com.appspro.payroll.bean.RunPayrollBean;

import com.appspro.payroll.bean.RunPayrollResponseBean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import common.biPReports.BIReportModel;

import common.biPReports.BIReportModel.Payroll_Name_PARAM;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class RunPeyrollDAO extends AppsproConnection {


    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;
    CallableStatement cstmt = null;

    public String getPeriod() {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            BIReportModel biModel = new BIReportModel();
            JSONObject json =
                biModel.runReport(BIReportModel.REPORT_NAME.PERIOD_PAYROLL.getValue(),
                                  paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getPayroll(String payrollName) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if (payrollName != null) {

                paramMap.put(BIReportModel.Payroll_Name_PARAM.P_PAYROLL_NAME.getValue(),
                             payrollName);
            }
            BIReportModel biModel = new BIReportModel();
            JSONObject json =
                biModel.runReport(BIReportModel.REPORT_NAME.Payroll_Name.getValue(),
                                  paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getAssignmentGroup() {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            BIReportModel biModel = new BIReportModel();
            JSONObject json =
                biModel.runReport(BIReportModel.REPORT_NAME.Assignment_Group.getValue(),
                                  paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ElementFormBean> getAllElement() {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT  PETF.ELEMENT_TYPE_ID, PETF.ELEMENT_NAME\n" +
                    "FROM PAY_ELEMENT_TYPES_F PETF\n" +
                    "WHERE to_date(PETF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy')";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementFormBean elementFormBean = new ElementFormBean();
                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                elementFormBeanList.add(elementFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public List<FastFormulaBean> getFastFormulaByElementId(String ElementTypeId) {
        List<FastFormulaBean> list = new ArrayList<FastFormulaBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT FF.FORMULA_ID, FF.FORMULA_NAME, FF.FORMULA_TEXT\n" +
                "FROM PAY_ELEMENT_TYPES_F PETF,\n" +
                "FF_FORMULAS_F FF,\n" +
                "PAY_FORMULA_USAGE_F PFUF\n" +
                "WHERE PETF.ELEMENT_TYPE_ID = PFUF.ELEMENT_TYPE_ID\n" +
                "AND FF.FORMULA_ID = PFUF.FORMULA_ID\n" +
                "AND to_date(PETF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy') \n" +
                "AND to_date(FF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy') \n" +
                "AND to_date(PFUF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy')\n" +
                "AND PFUF.ELEMENT_TYPE_ID = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setString(1, ElementTypeId);
            rs = ps.executeQuery();
            while (rs.next()) {
                FastFormulaBean obj = new FastFormulaBean();
                obj.setFormula_id(rs.getString("FORMULA_ID"));
                obj.setFormula_name(rs.getString("FORMULA_NAME"));
                obj.setFormula_text(rs.getString("FORMULA_TEXT"));
                list.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public List<EmployeeAssignementBean> getAllEmpDetailsByElementId(String ElementTypeId) {
        List<EmployeeAssignementBean> EmployeeAssignementBeanList =
            new ArrayList<EmployeeAssignementBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT  EAMP.PERSON_ID, EAMP.EMPLOYEE_NAME, EAMP.PERSON_NUMBER, EAMP.PAYROLL_NAME\n" +
                    "FROM PAY_ELEMENT_TYPES_F PETF,\n" +
                    "PAY_ELEMENT_ENTRIES_F PEEF,\n" +
                    "EMPLOYEE_ASSIGNMENT EAMP\n" +
                    "WHERE PETF.ELEMENT_TYPE_ID = PEEF.ELEMENT_TYPE_ID\n" +
                    "AND PEEF.ASSIGNMENT_ID = EAMP.PERSON_ID\n" +
                    "AND to_date(PETF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy')  \n" +
                    "AND to_date(PEEF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy')\n" +
                    "AND PEEF.ELEMENT_TYPE_ID = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setString(1, ElementTypeId);
            rs = ps.executeQuery();
            while (rs.next()) {
                EmployeeAssignementBean employeeAssignementBean =
                    new EmployeeAssignementBean();
                employeeAssignementBean.setPersonId(rs.getString("PERSON_ID"));
                //                employeeAssignementBean.setInputValue(rs.getString("INPUT_VALUE"));
                employeeAssignementBean.setEmpName(rs.getString("EMPLOYEE_NAME"));
                employeeAssignementBean.setPayrollName(rs.getString("PAYROLL_NAME"));
                employeeAssignementBean.setPersonNum(rs.getString("PERSON_NUMBER"));
                EmployeeAssignementBeanList.add(employeeAssignementBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return EmployeeAssignementBeanList;
    }

    public ArrayList<RunPayrollResponseBean> buildJson() {
        ArrayList<RunPayrollResponseBean> runPayrollResponseBeanList =
            new ArrayList<>();
        JSONArray result = new JSONArray();
        JSONArray element = new JSONArray(getAllElement());
        for (int ind = 0; ind < element.length(); ind++) {
            RunPayrollBean runPayrollBean = new RunPayrollBean();
            RunPayrollResponseBean runPayrollResponseBean =
                new RunPayrollResponseBean();
            JSONObject elementObj = (JSONObject)element.get(ind);
            System.out.println(elementObj);
            runPayrollResponseBean.setElementName(elementObj.optString("elementName"));
            runPayrollResponseBean.setElementId(elementObj.optString("elementTypeId"));
            JSONArray fastFormula =
                new JSONArray(getFastFormulaByElementId(runPayrollResponseBean.getElementId()));
            System.out.println(fastFormula);
            JSONObject formulaObj = (JSONObject)fastFormula.optJSONObject(0);
            if (fastFormula.length() > 0) {
                FastFormulaBean fastFormulaBean = new FastFormulaBean();
                fastFormulaBean.setFormula_name(formulaObj.optString("formula_name"));
                fastFormulaBean.setFormula_text(formulaObj.optString("formula_text").replaceAll("\n", ""));
                fastFormulaBean.setFormula_id(formulaObj.optString("formula_id"));
                runPayrollResponseBean.setFormula(fastFormulaBean);
                //                JSONObject jsonHeader = new JSONObject();
                //                jsonHeader.put("elementName", runPayrollBean.getElementName());
                //                JSONObject jsonFormula = new JSONObject();
                //                jsonFormula.put("formula_id", runPayrollBean.getFormulaId());
                //                jsonFormula.put("formula_name",
                //                                runPayrollBean.getFormulaName());
                //                jsonFormula.put("formula_text",
                //                                runPayrollBean.getFormulaText());
                //                jsonHeader.put("formula", jsonFormula);
                runPayrollResponseBean.setEmployee(getAllEmpDetailsByElementId(runPayrollResponseBean.getElementId()));
                if (runPayrollResponseBean.getEmployee().size() > 0) {
                    runPayrollResponseBeanList.add(runPayrollResponseBean);
                }
                //                JSONArray employee =
                //                    new JSONArray(getAllEmpDetailsByElementId(runPayrollBean.getElementTypeId()));
                //                jsonHeader.put("Employees", employee);
                //
                //                result.put(jsonHeader);
            }
        }
        System.out.println(new JSONArray(runPayrollResponseBeanList));
        return runPayrollResponseBeanList;
    }

    public static void main(String[] args) {
        RunPeyrollDAO runPeyrollDAO = new RunPeyrollDAO();
        // runPeyrollDAO.getFastFormulaByElementId("486");
        //        JSONArray element =
        //            new JSONArray(runPeyrollDAO.getFastFormulaByElementId("486"));
        runPeyrollDAO.buildJson();
        //       JSONArray element = new JSONArray(runPeyrollDAO.getAllElement());
        //       System.out.println(element);

    }

}
