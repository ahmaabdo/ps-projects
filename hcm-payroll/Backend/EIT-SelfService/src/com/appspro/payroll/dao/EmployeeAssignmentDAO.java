package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.payroll.bean.ElementEntriesBean;
import com.appspro.payroll.bean.EmployeeAssignementBean;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class EmployeeAssignmentDAO extends AppsproConnection {


    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;
    CallableStatement cstmt = null;

    public List<EmployeeAssignementBean> insertEmp(EmployeeAssignementBean employeeAssignementBean) {

        List<EmployeeAssignementBean> employeeAssignementBeanList =
            new ArrayList<EmployeeAssignementBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "INSERT INTO EMPLOYEE_ASSIGNMENT (EMPLOYEE_NAME, PERSON_NUMBER, PERSON_ID, PAYROLL_RELATIONSHIP_NUMBER, ASSIGNMENT_NUMBER, PAYROLL_NAME)" +
                    "VALUES(?,?,?,?,?,?)";
            System.out.println("\nExecuting query: " + query);
            cstmt = connection.prepareCall(query);
            cstmt.setString(1, employeeAssignementBean.getEmpName());
            cstmt.setLong(2, employeeAssignementBean.getEmpNum());
            cstmt.setLong(3, employeeAssignementBean.getEmpId());
            cstmt.setLong(4,
                          employeeAssignementBean.getPayrollRelationShipNum());
            cstmt.setString(5, employeeAssignementBean.getAssignmentNum());
            cstmt.setString(6, employeeAssignementBean.getPayrollName());
            cstmt.executeUpdate();
            employeeAssignementBeanList.add(employeeAssignementBean);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return employeeAssignementBeanList;
    }


    public String getEmployeesAssignment(String payrollName) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if (payrollName != null) {

                paramMap.put(BIReportModel.EMPLOYEE_WITH_PEYROLL_PARAM.P_PAYROLL_NAME.getValue(),
                             payrollName);
            }
            BIReportModel biModel = new BIReportModel();
            JSONObject json =
                biModel.runReport(BIReportModel.REPORT_NAME.EMPLOYEE_WITH_PEYROLL.getValue(),
                                  paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<EmployeeAssignementBean> getEmp() {
        List<EmployeeAssignementBean> empList = new ArrayList<>();
        JSONArray jsonReport =
            new JSONArray(getEmployeesAssignment("AWA Pyroll"));
        System.out.println(jsonReport);
        JSONObject data = new JSONObject();
        JSONArray g1 = new JSONArray();
        for (int i = 0; i < jsonReport.length(); i++) {
            if (jsonReport.length() > 0) {
                JSONObject json = jsonReport.getJSONObject(i);
                EmployeeAssignementBean employeeAssignementBean =
                    new EmployeeAssignementBean();
                employeeAssignementBean.setEmpName(json.optString("EMPLOYEE_NAME"));
                employeeAssignementBean.setAssignmentNum(json.optString("ASSIGNMENT_NUMBER"));
                employeeAssignementBean.setEmpId(json.optLong("PERSON_ID"));
                employeeAssignementBean.setEmpNum(json.optLong("PERSON_NUMBER"));
                employeeAssignementBean.setPayrollName(json.optString("PAYROLL_NAME"));
                employeeAssignementBean.setPayrollRelationShipNum(json.optLong("PAYROLL_RELATIONSHIP_NUMBER"));
                insertEmp(employeeAssignementBean);
                empList.add(employeeAssignementBean);
            }
        }
        return empList;
    }

    public List<EmployeeAssignementBean> getAllEmpDetails() {
        List<EmployeeAssignementBean> EmployeeAssignementBeanList =
            new ArrayList<EmployeeAssignementBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT PETF.ELEMENT_NAME, PETF.PROCESSING_PRIORITY, PETF.PROCESSING_TYPE, PETF.CURRENCY_CODE,\n" +
                    "PEC.BASE_CLASSIFICATION_NAME,\n" +
                    "PIVF.BASE_NAME As INPUT_VALUE_NAME, PIVF.UOM, PIVF.LOOKUP_TYPE, PIVF.DISPLAY_SEQUENCE, PIVF.USER_DISPLAY_FLAG, PIVF.DEFAULT_VALUE, PIVF.MANDATORY_FLAG,\n" +
                    "PFUF.DESCRIPTION AS FORMULA_RESULT_NAME,\n" +
                    "FFF.FORMULA_NAME, FFF.FORMULA_TEXT,\n" +
                    "EMPA.PERSON_ID,\n" +
                    "PEEVF.SCREEN_ENTRY_VALUE AS INPUT_VALUE,\n" +
                    "EMPA.EMPLOYEE_NAME, EMPA.PAYROLL_NAME,EMPA.PERSON_NUMBER\n" +
                    "FROM PAY_ELEMENT_TYPES_F PETF,\n" +
                    "PAY_ELE_CLASSIFICATIONS PEC,\n" +
                    "PAY_INPUT_VALUES_F PIVF,\n" +
                    "PAY_FORMULA_USAGE_F PFUF,\n" +
                    "FF_FORMULAS_F FFF,\n" +
                    "EMPLOYEE_ASSIGNMENT EMPA,\n" +
                    "PAY_ELEMENT_ENTRIES_F PEEF,\n" +
                    "PAY_ELEMENT_ENTRY_VALUES_F PEEVF\n" +
                    "WHERE PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "AND PETF.ELEMENT_TYPE_ID = PIVF.ELEMENT_TYPE_ID\n" +
                    "AND PETF.ELEMENT_TYPE_ID = PFUF.ELEMENT_TYPE_ID\n" +
                    "AND PIVF.ELEMENT_TYPE_ID = PFUF.ELEMENT_TYPE_ID\n" +
                    "AND PEEF.ELEMENT_TYPE_ID = PFUF.ELEMENT_TYPE_ID\n" +
                    "AND PETF.ELEMENT_TYPE_ID = PEEF.ELEMENT_TYPE_ID\n" +
                    "AND PIVF.ELEMENT_TYPE_ID = PEEF.ELEMENT_TYPE_ID\n" +
                    "AND PEEF.ELEMENT_ENTRY_ID = PEEVF.ELEMENT_ENTRY_ID\n" +
                    "AND PIVF.INPUT_VALUE_ID = PEEVF.INPUT_VALUE_ID\n" +
                    "AND FFF.FORMULA_ID = PFUF.FORMULA_ID\n" +
                    "AND EMPA.PERSON_ID = PEEF.ASSIGNMENT_ID\n" +
                    "AND to_date(PETF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy') \n" +
                    "AND to_date(PETF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy') \n" +
                    "AND to_date(PIVF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy') \n" +
                    "AND to_date(FFF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy') \n" +
                    "AND to_date(PFUF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy') \n" +
                    "AND to_date(PEEVF.EFFECTIVE_END_DATE,'dd-mm-yyyy') > to_date(sysdate,'dd-mm-yyyy') \n";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                EmployeeAssignementBean employeeAssignementBean =
                    new EmployeeAssignementBean();
                employeeAssignementBean.setElementName(rs.getString("ELEMENT_NAME"));
                employeeAssignementBean.setProcessingpriority(rs.getString("PROCESSING_PRIORITY"));
                employeeAssignementBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                employeeAssignementBean.setCurrencyCode(rs.getString("CURRENCY_CODE"));
                employeeAssignementBean.setClassificationName(rs.getString("BASE_CLASSIFICATION_NAME"));
                employeeAssignementBean.setInputValueName(rs.getString("INPUT_VALUE_NAME"));
                employeeAssignementBean.setUom(rs.getString("UOM"));
                employeeAssignementBean.setLookupType(rs.getString("LOOKUP_TYPE"));
                employeeAssignementBean.setDisplaySequnce(rs.getString("DISPLAY_SEQUENCE"));
                employeeAssignementBean.setUseDisplayFlag(rs.getString("USER_DISPLAY_FLAG"));
                employeeAssignementBean.setDefualtValue(rs.getString("DEFAULT_VALUE"));
                employeeAssignementBean.setMandetoryFlag(rs.getString("MANDATORY_FLAG"));
                employeeAssignementBean.setFormulaResultName(rs.getString("FORMULA_RESULT_NAME"));
                employeeAssignementBean.setFormulaName(rs.getString("FORMULA_NAME"));
                employeeAssignementBean.setFormulaText(rs.getString("FORMULA_TEXT"));
                employeeAssignementBean.setPersonId(rs.getString("PERSON_ID"));
                employeeAssignementBean.setInputValue(rs.getString("INPUT_VALUE"));
                employeeAssignementBean.setEmpName(rs.getString("EMPLOYEE_NAME"));
                employeeAssignementBean.setPayrollName(rs.getString("PAYROLL_NAME"));
                employeeAssignementBean.setPersonNum(rs.getString("PERSON_NUMBER"));
                EmployeeAssignementBeanList.add(employeeAssignementBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return EmployeeAssignementBeanList;
    }

    public static void main(String[] args) {
        EmployeeAssignmentDAO employeeAssignmentDAO =
            new EmployeeAssignmentDAO();
        employeeAssignmentDAO.getEmp();
    }
}
