package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;
import org.json.JSONArray;
import com.appspro.payroll.bean.ClassificationELementBean;
import com.appspro.payroll.bean.ElementBatchBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public class ElementBatchDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public List<ElementBatchBean> getElementBatch() {
        List<ElementBatchBean> list = new ArrayList<ElementBatchBean>();
        connection = AppsproConnection.getConnection();
        String query = "SELECT * FROM XX_BATCH_ELEMENT";
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                ElementBatchBean obj = new ElementBatchBean();
                obj.setID(rs.getString("ID"));
                obj.setELEMENT_NAME(rs.getString("ELEMENT_NAME"));
                obj.setCREATION_BY(rs.getString("CREATION_BY"));
                obj.setCREATION_DATE(rs.getString("CREATION_DATE"));
                obj.setDESCRIPTION(rs.getString("DESCRIPTION"));
                list.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
        
    }
    
    public String addelEmentBatchV2(JSONObject obj) throws JSONException,
                                                                        SQLException {
       String result = "";
       JSONObject jObject = new JSONObject();
       JSONArray arr = obj.getJSONArray("elementArray");
       connection = AppsproConnection.getConnection();
       ElementEntriesDAO elementService = new ElementEntriesDAO();
       String sql_select = "{call BATCH_ELEMENT_PRO(?,?,?,?)}";
       
       try  {
               ps = connection.prepareStatement(sql_select);
               ps.setString(1, obj.getString("elementName"));
               ps.setString(2, obj.getString("CREATION_BY"));
               ps.setString(3,obj.getString("CREATION_DATE"));
               ps.setString(4,obj.getString("DESCRIPTION"));
               ps.executeUpdate();
               jObject.put("Status", "Inserted");        
           for(int i =0;i<arr.length();i++){
               JSONObject elmentObj = (JSONObject)arr.get(i);          
               elementService.insertProcedure(elmentObj.toString());
           }
           
           } catch (SQLException e) {
           e.printStackTrace();
           System.out.println(e.getMessage());
           jObject.put("Status", "ERROR");
           jObject.put("ErrorCode", e.getMessage());
           result = jObject.toString();
       } finally {
           closeResources(connection, ps, rs);
       }
       return jObject.toString();
        
   }
    
//    public String addelEmentBatch(List<ElementBatchBean> elementBatchList) throws JSONException,
//                                                                        SQLException {
//       JSONObject jObject = new JSONObject();
//       String result = "";
//        connection = AppsproConnection.getConnection();
//       String sql_select = "{call BATCH_ELEMENT_PRO(?,?,?)}";
//       try  {
//           int count = 0;
//           ps = connection.prepareStatement(sql_select);
//     for (ElementBatchBean elementBatch : elementBatchList) {
//               ps.setString(1, elementBatch.getELEMENT_NAME());
//               ps.setString(2, elementBatch.getPERSON_NUMBER());
//               ps.setString(3, elementBatch.getEFFECTIVE_DATE());
//               ps.addBatch();
//               count++;
//               // execute every 100 rows or less
//               if (count % 100 == 0 || count == elementBatchList.size()) {
//                  ps.executeBatch();
//               }
//           }
//    //            System.out.println(i + " records inserted");
//
//           jObject.put("Status", "Inserted");
//
//    //            System.out.println(JSONOutput);
//       } catch (SQLException e) {
//           e.printStackTrace();
//           System.out.println("Noor Eror");
//           System.out.println(e.getMessage());
//           jObject.put("Status", "ERROR");
//           jObject.put("ErrorCode", e.getMessage());
//           result = jObject.toString();
//       } finally {
//           closeResources(connection, ps, rs);
//       }
//
//       return jObject.toString();
//    }
}
