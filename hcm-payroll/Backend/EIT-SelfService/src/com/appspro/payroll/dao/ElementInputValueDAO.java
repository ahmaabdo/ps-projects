package com.appspro.payroll.dao;
import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.ElementFormBean;
import com.appspro.payroll.bean.ElementInputValueBean;
import common.restHelper.RestHelper;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import oracle.jdbc.OracleTypes;
import org.json.JSONArray;
import org.json.JSONObject;
public class ElementInputValueDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;
    CallableStatement cstmt = null;

    public List<ElementInputValueBean> insertProcedure(String insertElementInputValue) {

        List<ElementInputValueBean> elementInputValueList =
            new ArrayList<ElementInputValueBean>();
        try {
            ElementInputValueBean elementInputValueBean =
                new ElementInputValueBean();
            JSONObject json = new JSONObject(insertElementInputValue);
            elementInputValueBean.setName(json.optString("name"));
            elementInputValueBean.setDisplaySequance(json.optString("displaySequance"));
            elementInputValueBean.setUom(json.optString("uom"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            elementInputValueBean.setEffectiveStartDate(startDate);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                elementInputValueBean.setEffectiveEndDate(endDate);
            } else {
                elementInputValueBean.setEffectiveEndDate("31-Dec-4721");
            }

            elementInputValueBean.setUserDisplayFlag(json.optString("userDisplayFlag"));
            elementInputValueBean.setMandatoryFlag(json.optString("mandatoryFlag"));
            elementInputValueBean.setDefaultValue(json.optString("defaultValue"));
            elementInputValueBean.setLookupType(json.optString("lookupType"));
            elementInputValueBean.setMinValue(json.optString("minValue"));
            elementInputValueBean.setMaxValue(json.optString("maxValue"));
            elementInputValueBean.setWarningOfErorr(json.optString("warningOfErorr"));
            elementInputValueBean.setElementTypeId(json.optString("elementTypeId"));

            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "{call INSERT_ELEMENT_INPUT_VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            System.out.println("\nExecuting query: " + query);

            cstmt = connection.prepareCall(query);
            cstmt.setString(1, elementInputValueBean.getName());
            cstmt.setString(2, elementInputValueBean.getDisplaySequance());
            cstmt.setString(3, elementInputValueBean.getUom());
            cstmt.setString(4, elementInputValueBean.getEffectiveStartDate());
            cstmt.setString(5, elementInputValueBean.getEffectiveEndDate());
            cstmt.setString(6, elementInputValueBean.getElementTypeId());
            cstmt.setString(7, elementInputValueBean.getUserDisplayFlag());
            cstmt.setString(8, elementInputValueBean.getDefaultValue());
            cstmt.setString(9, elementInputValueBean.getLookupType());
            cstmt.setString(10, elementInputValueBean.getMinValue());
            cstmt.setString(11, elementInputValueBean.getMaxValue());
            cstmt.setString(12, elementInputValueBean.getWarningOfErorr());
            cstmt.setString(13, elementInputValueBean.getMandatoryFlag());
            cstmt.registerOutParameter(14, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            elementInputValueBean.setInputValueId(cstmt.getInt(14));
            elementInputValueBean.getInputValueId();
            elementInputValueList.add(elementInputValueBean);

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementInputValueList;
    }

    public List<ElementInputValueBean> updateProcedure(int id,
                                                       String updateElementInputValue) {

        List<ElementInputValueBean> elementInputValueBeanList =
            new ArrayList<ElementInputValueBean>();
        try {
            ElementInputValueBean elementInputValueBean =
                new ElementInputValueBean();
            JSONObject json = new JSONObject(updateElementInputValue);
            elementInputValueBean.setName(json.optString("name"));
            elementInputValueBean.setDisplaySequance(json.optString("displaySequance"));
            elementInputValueBean.setUom(json.optString("uom"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            elementInputValueBean.setEffectiveStartDate(startDate);
            
            
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                elementInputValueBean.setEffectiveEndDate(endDate);
            } else {
                elementInputValueBean.setEffectiveEndDate("");
            }

            elementInputValueBean.setUserDisplayFlag(json.optString("userDisplayFlag"));
            elementInputValueBean.setMandatoryFlag(json.optString("mandatoryFlag"));
            elementInputValueBean.setDefaultValue(json.optString("defaultValue"));
            elementInputValueBean.setLookupType(json.optString("lookupType"));
            elementInputValueBean.setMinValue(json.optString("minValue"));
            elementInputValueBean.setMaxValue(json.optString("maxValue"));
            elementInputValueBean.setWarningOfErorr(json.optString("warningOfErorr"));
            elementInputValueBean.setElementTypeId(json.optString("elementTypeId"));

            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call UPDATE_ELEMENT_INPUT_VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            System.out.println("\nExecuting query: " + query);

            cstmt = connection.prepareCall(query);
            cstmt.setInt(1, id);
            cstmt.setString(2, elementInputValueBean.getName());
            cstmt.setString(3, elementInputValueBean.getDisplaySequance());
            cstmt.setString(4, elementInputValueBean.getUom());
            cstmt.setString(5, elementInputValueBean.getEffectiveStartDate());
            cstmt.setString(6, elementInputValueBean.getEffectiveEndDate());
            cstmt.setString(7, elementInputValueBean.getElementTypeId());
            cstmt.setString(8, elementInputValueBean.getUserDisplayFlag());
            cstmt.setString(9, elementInputValueBean.getDefaultValue());
            cstmt.setString(10, elementInputValueBean.getLookupType());
            cstmt.setString(11, elementInputValueBean.getMinValue());
            cstmt.setString(12, elementInputValueBean.getMaxValue());
            cstmt.setString(13, elementInputValueBean.getWarningOfErorr());
            cstmt.setString(14, elementInputValueBean.getMandatoryFlag());
            cstmt.registerOutParameter(15, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            elementInputValueBean.setInputValueId(cstmt.getInt(15));
            elementInputValueBean.getInputValueId();
            elementInputValueBeanList.add(elementInputValueBean);

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementInputValueBeanList;
    }


    public List<ElementInputValueBean> getElementInputValueById(int id) {
        List<ElementInputValueBean> elementInputValueBeanList =
            new ArrayList<ElementInputValueBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT * FROM \"PAYROLL_TEST\".\"PAY_INPUT_VALUES_F\" WHERE \"ELEMENT_TYPE_ID\" = ? AND to_date(\"EFFECTIVE_END_DATE\",'dd-MM-yyyy') > to_date(SYSDATE, 'dd-MM-yyyy')";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementInputValueBean elementInputValueBean = new ElementInputValueBean();
                elementInputValueBean.setInputValueId(rs.getInt("INPUT_VALUE_ID"));
                elementInputValueBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementInputValueBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                //                elementFormBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementInputValueBean.setName(rs.getString("BASE_NAME"));
                elementInputValueBean.setUom(rs.getString("UOM"));
                elementInputValueBean.setLookupType(rs.getString("LOOKUP_TYPE"));
                elementInputValueBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementInputValueBean.setDisplaySequance(rs.getString("DISPLAY_SEQUENCE"));
                elementInputValueBean.setUserDisplayFlag(rs.getString("USER_DISPLAY_FLAG"));
                elementInputValueBean.setDefaultValue(rs.getString("DEFAULT_VALUE"));
                elementInputValueBean.setMinValue(rs.getString("MIN_VALUE"));
                elementInputValueBean.setMaxValue(rs.getString("MAX_VALUE"));
                elementInputValueBean.setWarningOfErorr(rs.getString("WARNING_OR_ERROR"));
                elementInputValueBean.setMandatoryFlag(rs.getString("MANDATORY_FLAG"));
                elementInputValueBean.setVersion(rs.getString("OBJECT_VERSION_NUMBER"));
                elementInputValueBeanList.add(elementInputValueBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementInputValueBeanList;
    }

    public List<ElementInputValueBean> getInputValueByInputId(int id) {
        List<ElementInputValueBean> elementInputValueBeanList =
            new ArrayList<ElementInputValueBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM \"PAYROLL_TEST\".\"PAY_INPUT_VALUES_F\" WHERE \"INPUT_VALUE_ID\" = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementInputValueBean elementInputValueBean =
                    new ElementInputValueBean();
                elementInputValueBean.setInputValueId(rs.getInt("INPUT_VALUE_ID"));
                elementInputValueBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementInputValueBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                //                elementFormBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementInputValueBean.setName(rs.getString("BASE_NAME"));
                elementInputValueBean.setUom(rs.getString("UOM"));
                elementInputValueBean.setLookupType(rs.getString("LOOKUP_TYPE"));
                elementInputValueBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementInputValueBean.setDisplaySequance(rs.getString("DISPLAY_SEQUENCE"));
                elementInputValueBean.setUserDisplayFlag(rs.getString("USER_DISPLAY_FLAG"));
                elementInputValueBean.setDefaultValue(rs.getString("DEFAULT_VALUE"));
                elementInputValueBean.setMinValue(rs.getString("MIN_VALUE"));
                elementInputValueBean.setMaxValue(rs.getString("MAX_VALUE"));
                elementInputValueBean.setWarningOfErorr(rs.getString("WARNING_OR_ERROR"));
                elementInputValueBean.setMandatoryFlag(rs.getString("MANDATORY_FLAG"));
                elementInputValueBean.setVersion(rs.getString("OBJECT_VERSION_NUMBER"));
                elementInputValueBeanList.add(elementInputValueBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementInputValueBeanList;
    }

    public List<ElementInputValueBean> DeleteInputValue(Integer id) {
        List<ElementInputValueBean> elementInputValueBeanList =
            new ArrayList<ElementInputValueBean>();

        elementInputValueBeanList = getInputValueByInputId(id);
        String check = new JSONArray(elementInputValueBeanList).toString();
        if (!check.equals("[]")) {

            try {

                connection = AppsproConnection.getConnection();
                String query = null;
                query =
                        "UPDATE PAYROLL_TEST.PAY_INPUT_VALUES_F SET EFFECTIVE_END_DATE =sysdate WHERE INPUT_VALUE_ID = ?";
                System.out.println("\nExecuting query: " + query);
                ps = connection.prepareStatement(query);
                ps.setInt(1, id);
                ps.executeUpdate();
                ElementInputValueBean elementInputValueBean =
                    new ElementInputValueBean();
                elementInputValueBean.setCheck("Success Deleted");
                elementInputValueBeanList.add(elementInputValueBean);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
        } else {
            ElementInputValueBean elementInputValueBean = new ElementInputValueBean();
            elementInputValueBean.setCheck("There is no id in DB");
            elementInputValueBeanList.add(elementInputValueBean);
        }
        return elementInputValueBeanList;
    }


}
