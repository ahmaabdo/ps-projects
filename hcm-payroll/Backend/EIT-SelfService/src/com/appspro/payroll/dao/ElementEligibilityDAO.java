package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;


import com.appspro.payroll.bean.ElementEligibilityBean;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;


import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ElementEligibilityDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;


    public List<ElementEligibilityBean> callInsertUpdateEligibilityProcedure(ElementEligibilityBean elementEligibilityBean,
                                                                             String procedureType) {
        List<ElementEligibilityBean> elementEligibilityList =
            new ArrayList<ElementEligibilityBean>();
        try {

            connection = AppsproConnection.getConnection();


            if (procedureType.equalsIgnoreCase("insert")) {
                CallableStatement stmtInsert =
                    connection.prepareCall("{call PAYROLL_TEST.INSERT_ELEMENT_ELIGIBILITY(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

                stmtInsert.setString(1,
                                     elementEligibilityBean.getEffectiveStartDate());
                stmtInsert.setString(2,
                                     elementEligibilityBean.getEffectiveEndDate());
                stmtInsert.setString(3,
                                     elementEligibilityBean.getBusinessGroupId());
                stmtInsert.setString(4,
                                     elementEligibilityBean.getElementTypeId());
                stmtInsert.setString(5, elementEligibilityBean.getPayrollId());
                stmtInsert.setString(6, elementEligibilityBean.getJobId());
                stmtInsert.setString(7,
                                     elementEligibilityBean.getPositionId());
                stmtInsert.setString(8,
                                     elementEligibilityBean.getPeopleGroupId());
                stmtInsert.setString(9,
                                     elementEligibilityBean.getOrganizationId());
                stmtInsert.setString(10,
                                     elementEligibilityBean.getLocationId());
                stmtInsert.setString(11, elementEligibilityBean.getGradeId());
                stmtInsert.setString(12,
                                     elementEligibilityBean.getStandardLinkFlag());
                stmtInsert.setString(13,
                                     elementEligibilityBean.getElementLinkName());
                stmtInsert.registerOutParameter(14, java.sql.Types.INTEGER);

                stmtInsert.executeUpdate();


                elementEligibilityBean.setElementLinkId(stmtInsert.getInt(14));
                elementEligibilityList.add(elementEligibilityBean);
                return elementEligibilityList;
            }

            else if (procedureType.equalsIgnoreCase("update")) {

                CallableStatement stmtUpdate =
                    connection.prepareCall("{call PAYROLL_TEST.UPDATE_ELEMENT_ELIGIBILITY(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

                stmtUpdate.setInt(1,
                                  elementEligibilityBean.getElementLinkId());
                stmtUpdate.setString(2,
                                     elementEligibilityBean.getEffectiveStartDate());
                stmtUpdate.setString(3,
                                     elementEligibilityBean.getEffectiveEndDate());
                stmtUpdate.setString(4,
                                     elementEligibilityBean.getBusinessGroupId());
                stmtUpdate.setString(5,
                                     elementEligibilityBean.getElementTypeId());
                stmtUpdate.setString(6, elementEligibilityBean.getPayrollId());
                stmtUpdate.setString(7, elementEligibilityBean.getJobId());
                stmtUpdate.setString(8,
                                     elementEligibilityBean.getPositionId());
                stmtUpdate.setString(9,
                                     elementEligibilityBean.getPeopleGroupId());
                stmtUpdate.setString(10,
                                     elementEligibilityBean.getOrganizationId());
                stmtUpdate.setString(11,
                                     elementEligibilityBean.getLocationId());
                stmtUpdate.setString(12, elementEligibilityBean.getGradeId());
                stmtUpdate.setString(13,
                                     elementEligibilityBean.getStandardLinkFlag());
                stmtUpdate.setString(14,
                                     elementEligibilityBean.getElementLinkName());
                stmtUpdate.registerOutParameter(15, java.sql.Types.INTEGER);

                stmtUpdate.executeUpdate();

                elementEligibilityBean.setElementLinkId(stmtUpdate.getInt(15));

                elementEligibilityList.add(elementEligibilityBean);
                return elementEligibilityList;
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEligibilityList;
    }

    public List<ElementEligibilityBean> insertElementEligibility(String elementEligibilityData) {

        List<ElementEligibilityBean> elementEligibilityList =
            new ArrayList<ElementEligibilityBean>();

        try {

            ElementEligibilityBean elementEligibilityBean =
                new ElementEligibilityBean();
            JSONObject json = new JSONObject(elementEligibilityData);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                elementEligibilityBean.setEffectiveEndDate(endDate);
                System.err.println(endDate);
            } else {
                elementEligibilityBean.setEffectiveEndDate("31-Dec-4721");
            }
            elementEligibilityBean.setEffectiveStartDate(startDate);
            System.err.println(startDate);
            elementEligibilityBean.setBusinessGroupId(json.optString("businessGroupId"));
            elementEligibilityBean.setElementTypeId(json.optString("elementTypeId"));
            elementEligibilityBean.setPayrollId(json.optString("payrollId"));
            elementEligibilityBean.setJobId(json.optString("jobId"));
            elementEligibilityBean.setPositionId(json.optString("positionId"));
            elementEligibilityBean.setPeopleGroupId(json.optString("peopleGroupId"));
            elementEligibilityBean.setOrganizationId(json.optString("organizationId"));
            elementEligibilityBean.setLocationId(json.optString("locationId"));
            elementEligibilityBean.setGradeId(json.optString("gradeId"));
            elementEligibilityBean.setStandardLinkFlag(json.optString("standardLinkFlag"));
            elementEligibilityBean.setElementLinkName(json.optString("elementLinkName"));

            elementEligibilityList =
                    callInsertUpdateEligibilityProcedure(elementEligibilityBean,
                                                         "insert");

            return elementEligibilityList;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return elementEligibilityList;
    }


    public List<ElementEligibilityBean> updateElementEligibility(String elementEligibilityData) {
        List<ElementEligibilityBean> elementEligibilityList =
            new ArrayList<ElementEligibilityBean>();

        try {
            ElementEligibilityBean elementEligibilityBean =
                new ElementEligibilityBean();
            JSONObject json = new JSONObject(elementEligibilityData);

            elementEligibilityBean.setElementLinkId(json.optInt("elementLinkId"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                elementEligibilityBean.setEffectiveEndDate(endDate);
            } else {
                elementEligibilityBean.setEffectiveEndDate("");
            }
            elementEligibilityBean.setEffectiveStartDate(startDate);
            elementEligibilityBean.setBusinessGroupId(json.optString("businessGroupId"));
            elementEligibilityBean.setElementTypeId(json.optString("elementTypeId"));
            elementEligibilityBean.setPayrollId(json.optString("payrollId"));
            elementEligibilityBean.setJobId(json.optString("jobId"));
            elementEligibilityBean.setPositionId(json.optString("positionId"));
            elementEligibilityBean.setPeopleGroupId(json.optString("peopleGroupId"));
            elementEligibilityBean.setOrganizationId(json.optString("organizationId"));
            elementEligibilityBean.setLocationId(json.optString("locationId"));
            elementEligibilityBean.setGradeId(json.optString("gradeId"));
            elementEligibilityBean.setStandardLinkFlag(json.optString("standardLinkFlag"));
            elementEligibilityBean.setElementLinkName(json.optString("elementLinkName"));


            elementEligibilityList =
                    callInsertUpdateEligibilityProcedure(elementEligibilityBean,
                                                         "update");

            return elementEligibilityList;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return elementEligibilityList;
    }


    public List<ElementEligibilityBean> getAllElementEligibility(int id) {

        List<ElementEligibilityBean> elementEligibilityBeanList =
            new ArrayList<ElementEligibilityBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_LINKS_F\" WHERE \"ELEMENT_TYPE_ID\" = ? AND to_date(\"EFFECTIVE_END_DATE\",'dd-MM-yyyy') > to_date(SYSDATE, 'dd-MM-yyyy')";
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                ElementEligibilityBean elementEligibilityBean =
                    new ElementEligibilityBean();

                elementEligibilityBean.setElementLinkId(rs.getInt("ELEMENT_LINK_ID"));
                elementEligibilityBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementEligibilityBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementEligibilityBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementEligibilityBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementEligibilityBean.setPayrollId(rs.getString("PAYROLL_ID"));
                elementEligibilityBean.setJobId(rs.getString("JOB_ID"));
                elementEligibilityBean.setPositionId(rs.getString("POSITION_ID"));
                elementEligibilityBean.setPeopleGroupId(rs.getString("PEOPLE_GROUP_ID"));
                elementEligibilityBean.setOrganizationId(rs.getString("ORGANIZATION_ID"));
                elementEligibilityBean.setLocationId(rs.getString("LOCATION_ID"));
                elementEligibilityBean.setGradeId(rs.getString("GRADE_ID"));
                elementEligibilityBean.setStandardLinkFlag(rs.getString("STANDARD_LINK_FLAG"));
                elementEligibilityBean.setElementLinkName(rs.getString("ELEMENT_LINK_NAME"));
                System.err.println(rs.getString("ELEMENT_LINK_NAME"));
                elementEligibilityBeanList.add(elementEligibilityBean);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEligibilityBeanList;
    }

    public List<ElementEligibilityBean> getEligibilityById(int id) {

        List<ElementEligibilityBean> elementEligibilityBeanList =
            new ArrayList<ElementEligibilityBean>();
        ElementEligibilityBean elementEligibilityBean =
            new ElementEligibilityBean();

        try {
            connection = AppsproConnection.getConnection();


            String query = null;
            query =
                    "SELECT * FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_LINKS_F\" WHERE \"ELEMENT_LINK_ID\" = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {


                elementEligibilityBean.setElementLinkId(rs.getInt("ELEMENT_LINK_ID"));
                elementEligibilityBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementEligibilityBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementEligibilityBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementEligibilityBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementEligibilityBean.setPayrollId(rs.getString("PAYROLL_ID"));
                elementEligibilityBean.setJobId(rs.getString("JOB_ID"));
                elementEligibilityBean.setPositionId(rs.getString("POSITION_ID"));
                elementEligibilityBean.setPeopleGroupId(rs.getString("PEOPLE_GROUP_ID"));
                elementEligibilityBean.setOrganizationId(rs.getString("ORGANIZATION_ID"));
                elementEligibilityBean.setLocationId(rs.getString("LOCATION_ID"));
                elementEligibilityBean.setGradeId(rs.getString("GRADE_ID"));
                elementEligibilityBean.setStandardLinkFlag(rs.getString("STANDARD_LINK_FLAG"));
                elementEligibilityBean.setElementLinkName(rs.getString("ELEMENT_LINK_NAME"));
                elementEligibilityBeanList.add(elementEligibilityBean);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEligibilityBeanList;
    }

    public String deleteEligibilityById(int id) {


        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call PAYROLL_TEST.DELETE_ELIGIBILITY_BY_ID(?)}";
            System.out.println("\nExecuting query: " + query);

            ps = connection.prepareStatement(query);
            ps.setInt(1, id);

            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return "Successful removed data";
    }

    public String getPeopleGroupLOVData() {
        BIReportModel biRreportModel = new BIReportModel();
        try {
            //                Map<String, String> paramMap = new HashMap<String, String>();
            //                System.err.println(BIReportModel.REPORT_NAME.PeopleGroupReport.getValue());
            JSONObject json =
                biRreportModel.runReportWithoutParams(BIReportModel.REPORT_NAME.PeopleGroupReport.getValue());
            //                System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    //    public static void main(String[] args) {
    //
    //        ElementEligibilityDAO eD=new ElementEligibilityDAO();
    //        System.out.println(eD.getAssetData());
    //
    //    }
}
