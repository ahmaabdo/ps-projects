package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.payroll.bean.ElementEntriesBean;

import common.biPReports.BIPReports;

import common.biPReports.BIReportModel;

import common.biPReports.BIReportModel.Employee_By_Effective_Date;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ElementEntriesDAO extends AppsproConnection {


    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;
    CallableStatement cstmt = null;

    public List<ElementEntriesBean> insertProcedure(String insertElementEntry) {

        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            ElementEntriesBean elementEntriesBean = new ElementEntriesBean();
            JSONObject json = new JSONObject(insertElementEntry);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                elementEntriesBean.setEffectiveEndDate(endDate);
            } else {
                elementEntriesBean.setEffectiveEndDate("31-Dec-4721");
            }
            elementEntriesBean.setEffectiveStartDate(startDate);
            elementEntriesBean.setElementTypeId(json.optString("elementTypeId"));
            elementEntriesBean.setPersonId(json.optString("personId"));

            JSONArray innerJson = new JSONArray();
            innerJson = json.optJSONArray("inputValues");
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call INSERT_ELEMENT_ENTRIES(?,?,?,?,?)}";
            System.out.println("\nExecuting query: " + query);
            cstmt = connection.prepareCall(query);
            cstmt.setString(1, elementEntriesBean.getEffectiveStartDate());
            cstmt.setString(2, elementEntriesBean.getEffectiveEndDate());
            cstmt.setString(3, elementEntriesBean.getElementTypeId());
            cstmt.setString(4, elementEntriesBean.getPersonId());
            cstmt.registerOutParameter(5, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            int generatedId = cstmt.getInt(5);
            elementEntriesBean.setElementEntryId(generatedId);
            for (int i = 0; i < innerJson.length(); i++) {
                query = "{call INSERT_ELEMENT_ENTRIES_DETAILS(?,?,?,?,?,?)}";
                System.out.println("\nExecuting query: " + query);
                cstmt = connection.prepareCall(query);
                cstmt.setInt(1, elementEntriesBean.getElementEntryId());
                String elementId = innerJson.optJSONObject(i).optString("id");
                elementEntriesBean.setElementTypeId(elementId);

                cstmt.setString(2, elementEntriesBean.getElementTypeId());

                String classificationId =
                    innerJson.optJSONObject(i).optString("Value");
                elementEntriesBean.setScreenEntryValue(classificationId);

                cstmt.setString(3, elementEntriesBean.getScreenEntryValue());
                cstmt.setString(4, elementEntriesBean.getEffectiveStartDate());
                cstmt.setString(5, elementEntriesBean.getEffectiveEndDate());
                cstmt.registerOutParameter(6, OracleTypes.NUMBER);
                cstmt.executeUpdate();
            }
            elementEntriesBeanList.add(elementEntriesBean);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return elementEntriesBeanList;
    }


    public List<ElementEntriesBean> updateProcedure(int id,
                                                    String updateElementEntry) {

        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            ElementEntriesBean elementEntriesBean = new ElementEntriesBean();
            JSONObject json = new JSONObject(updateElementEntry);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                elementEntriesBean.setEffectiveEndDate(endDate);
            } else {
                elementEntriesBean.setEffectiveEndDate("31-Dec-4721");
            }
            elementEntriesBean.setEffectiveStartDate(startDate);
            elementEntriesBean.setElementTypeId(json.optString("elementTypeId"));
            elementEntriesBean.setPersonId(json.optString("personId"));

            JSONArray innerJson = new JSONArray();
            innerJson = json.optJSONArray("inputValues");
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call UPDATE_ELEMENT_ENTRIES(?,?,?,?,?,?)}";
            System.out.println("\nExecuting query: " + query);

            cstmt = connection.prepareCall(query);
            cstmt.setInt(1, id);
            cstmt.setString(2, elementEntriesBean.getEffectiveStartDate());
            cstmt.setString(3, elementEntriesBean.getEffectiveEndDate());
            cstmt.setString(4, elementEntriesBean.getElementTypeId());
            cstmt.setString(5, elementEntriesBean.getPersonId());
            cstmt.registerOutParameter(6, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            int generatedId = cstmt.getInt(6);
            elementEntriesBean.setElementEntryId(generatedId);
            for (int i = 0; i < innerJson.length(); i++) {
                query = "{call INSERT_ELEMENT_ENTRIES_DETAILS(?,?,?,?,?,?)}";
                System.out.println("\nExecuting query: " + query);
                cstmt = connection.prepareCall(query);
                cstmt.setInt(1, elementEntriesBean.getElementEntryId());
                String elementId = innerJson.optJSONObject(i).optString("id");
                elementEntriesBean.setElementTypeId(elementId);

                cstmt.setString(2, elementEntriesBean.getElementTypeId());

                String classificationId =
                    innerJson.optJSONObject(i).optString("Value");
                elementEntriesBean.setScreenEntryValue(classificationId);

                cstmt.setString(3, elementEntriesBean.getScreenEntryValue());
                cstmt.setString(4, elementEntriesBean.getEffectiveStartDate());
                cstmt.setString(5, elementEntriesBean.getEffectiveEndDate());
                cstmt.registerOutParameter(6, OracleTypes.NUMBER);
                cstmt.executeUpdate();
            }
            elementEntriesBeanList.add(elementEntriesBean);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return elementEntriesBeanList;
    }

    public List<ElementEntriesBean> getAllElementEntry() {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT DISTINCT PEEF.ELEMENT_ENTRY_ID, PEEF.EFFECTIVE_START_DATE, PEEF.EFFECTIVE_END_DATE, PEEF.ASSIGNMENT_ID, PEEF.ELEMENT_TYPE_ID, PETF.ELEMENT_NAME, PETF.PROCESSING_TYPE, PEC.BASE_CLASSIFICATION_NAME\n" +
                    "                    FROM PAY_ELEMENT_ENTRIES_F PEEF,PAY_ELEMENT_ENTRY_VALUES_F PEEVF, PAY_ELEMENT_TYPES_F PETF, PAY_ELE_CLASSIFICATIONS PEC, PAY_INPUT_VALUES_F PIVF\n" +
                    "                    WHERE PEEF.ELEMENT_ENTRY_ID = PEEVF.ELEMENT_ENTRY_ID\n" +
                    "                    AND PEEF.ELEMENT_TYPE_ID = PETF.ELEMENT_TYPE_ID\n" +
                    "                    AND PEEVF.INPUT_VALUE_ID = PIVF.INPUT_VALUE_ID\n" +
                    "                    AND PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "                    AND TO_DATE(PEEF.EFFECTIVE_END_DATE,'DD-MM-YYYY') > TO_DATE(SYSDATE, 'DD-MM-YYYY')";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementEntriesBean elementEntriesBean =
                    new ElementEntriesBean();
                elementEntriesBean.setElementEntryId(rs.getInt("ELEMENT_ENTRY_ID"));
                elementEntriesBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementEntriesBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementEntriesBean.setPersonId(rs.getString("ASSIGNMENT_ID"));
                elementEntriesBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementEntriesBean.setElementName(rs.getString("ELEMENT_NAME"));
                elementEntriesBean.setProccessingType(rs.getString("PROCESSING_TYPE"));
                elementEntriesBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementEntriesBeanList.add(elementEntriesBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntriesBeanList;
    }

    public List<ElementEntriesBean> getElementEntryByPersonNum(String presonNum) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT DISTINCT PEEF.ELEMENT_ENTRY_ID, PEEF.EFFECTIVE_START_DATE, PEEF.EFFECTIVE_END_DATE, PEEF.ASSIGNMENT_ID, PEEF.ELEMENT_TYPE_ID, PETF.ELEMENT_NAME, PETF.PROCESSING_TYPE, PEC.BASE_CLASSIFICATION_NAME\n" +
                    "                                        FROM PAY_ELEMENT_ENTRIES_F PEEF,PAY_ELEMENT_ENTRY_VALUES_F PEEVF, PAY_ELEMENT_TYPES_F PETF, PAY_ELE_CLASSIFICATIONS PEC, PAY_INPUT_VALUES_F PIVF\n" +
                    "                                        WHERE PEEF.ELEMENT_ENTRY_ID = PEEVF.ELEMENT_ENTRY_ID\n" +
                    "                                        AND PEEF.ELEMENT_TYPE_ID = PETF.ELEMENT_TYPE_ID\n" +
                    "                                        AND PEEVF.INPUT_VALUE_ID = PIVF.INPUT_VALUE_ID\n" +
                    "                                        AND PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "                                        AND TO_DATE(PEEF.EFFECTIVE_END_DATE,'DD-MM-YYYY') > TO_DATE(SYSDATE, 'DD-MM-YYYY')\n" +
                    "                                        AND ASSIGNMENT_ID = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setString(1, presonNum);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementEntriesBean elementEntriesBean =
                    new ElementEntriesBean();
                elementEntriesBean.setElementEntryId(rs.getInt("ELEMENT_ENTRY_ID"));
                elementEntriesBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementEntriesBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementEntriesBean.setPersonId(rs.getString("ASSIGNMENT_ID"));
                elementEntriesBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementEntriesBean.setElementName(rs.getString("ELEMENT_NAME"));
                elementEntriesBean.setProccessingType(rs.getString("PROCESSING_TYPE"));
                elementEntriesBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementEntriesBeanList.add(elementEntriesBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntriesBeanList;
    }

    public List<ElementEntriesBean> getAllElementEntryById(int id) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT DISTINCT PEEF.ELEMENT_ENTRY_ID, PEEF.EFFECTIVE_START_DATE, PEEF.EFFECTIVE_END_DATE, PEEF.ASSIGNMENT_ID, PEEF.ELEMENT_TYPE_ID, PETF.ELEMENT_NAME, PETF.PROCESSING_TYPE, PEC.BASE_CLASSIFICATION_NAME\n" +
                    "                    FROM PAY_ELEMENT_ENTRIES_F PEEF,PAY_ELEMENT_ENTRY_VALUES_F PEEVF, PAY_ELEMENT_TYPES_F PETF, PAY_ELE_CLASSIFICATIONS PEC, PAY_INPUT_VALUES_F PIVF\n" +
                    "                    WHERE PEEF.ELEMENT_ENTRY_ID = PEEVF.ELEMENT_ENTRY_ID\n" +
                    "                    AND PEEF.ELEMENT_TYPE_ID = PETF.ELEMENT_TYPE_ID\n" +
                    "                    AND PEEVF.INPUT_VALUE_ID = PIVF.INPUT_VALUE_ID\n" +
                    "                    AND PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "                    AND TO_DATE(PEEF.EFFECTIVE_END_DATE,'DD-MM-YYYY') > TO_DATE(SYSDATE, 'DD-MM-YYYY')" +
                    "AND PEEF.ELEMENT_ENTRY_ID = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementEntriesBean elementEntriesBean =
                    new ElementEntriesBean();
                elementEntriesBean.setElementEntryId(rs.getInt("ELEMENT_ENTRY_ID"));
                elementEntriesBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementEntriesBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementEntriesBean.setPersonId(rs.getString("ASSIGNMENT_ID"));
                elementEntriesBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementEntriesBean.setElementName(rs.getString("ELEMENT_NAME"));
                elementEntriesBean.setProccessingType(rs.getString("PROCESSING_TYPE"));
                elementEntriesBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementEntriesBeanList.add(elementEntriesBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntriesBeanList;
    }

    public List<ElementEntriesBean> getAllElementEntryDetails() {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT \n" +
                    "PEEVF.ELEMENT_ENTRY_ID, PEEVF.INPUT_VALUE_ID, PEEVF.SCREEN_ENTRY_VALUE, PIVF.UOM, PIVF.BASE_NAME, PIVF.MIN_VALUE, PIVF.MAX_VALUE\n" +
                    "FROM PAY_ELEMENT_ENTRIES_F PEEF,PAY_ELEMENT_ENTRY_VALUES_F PEEVF, PAY_ELEMENT_TYPES_F PETF, PAY_ELE_CLASSIFICATIONS PEC, PAY_INPUT_VALUES_F PIVF\n" +
                    "WHERE PEEF.ELEMENT_ENTRY_ID = PEEVF.ELEMENT_ENTRY_ID\n" +
                    "AND PEEF.ELEMENT_TYPE_ID = PETF.ELEMENT_TYPE_ID\n" +
                    "AND PEEVF.INPUT_VALUE_ID = PIVF.INPUT_VALUE_ID\n" +
                    "AND PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "AND TO_DATE(PEEF.EFFECTIVE_END_DATE,'DD-MM-YYYY') > TO_DATE(SYSDATE, 'DD-MM-YYYY')";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementEntriesBean elementEntriesBean =
                    new ElementEntriesBean();
                elementEntriesBean.setElementEntryId(rs.getInt("ELEMENT_ENTRY_ID"));
                elementEntriesBean.setInputValueId(rs.getString("INPUT_VALUE_ID"));
                elementEntriesBean.setScreenEntryValue(rs.getString("SCREEN_ENTRY_VALUE"));
                elementEntriesBean.setElementEntryValueId(rs.getInt("ELEMENT_ENTRY_ID"));
                elementEntriesBean.setMinValue(rs.getString("MIN_VALUE"));
                elementEntriesBean.setMaxValue(rs.getString("MAX_VALUE"));
                elementEntriesBean.setInputeValueName(rs.getString("BASE_NAME"));
                elementEntriesBean.setInputeValueName(rs.getString("UOM"));
                elementEntriesBeanList.add(elementEntriesBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntriesBeanList;
    }
    
    public List<ElementEntriesBean> deleteElementEntry(Integer id, String endDate) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();

        elementEntriesBeanList = getAllElementEntryById(id);
        String check = new JSONArray(elementEntriesBeanList).toString();
        if (!check.equals("[]")) {

            try {
                
                connection = AppsproConnection.getConnection();
                String query = null;
                query = "UPDATE PAYROLL_TEST.PAY_ELEMENT_ENTRIES_F SET EFFECTIVE_END_DATE =? WHERE ELEMENT_ENTRY_ID = ?";
                ps = connection.prepareStatement(query);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date date = sdf.parse(endDate);
                sdf = new SimpleDateFormat("dd-MMM-yyyy");
                ps.setString(1, sdf.format(date));
                ps.setInt(2, id);
                ps.executeUpdate();

                return elementEntriesBeanList;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
        } else {
            ElementEntriesBean elementEntriesBean = new ElementEntriesBean();
            elementEntriesBean.setCheck("There is no id in DB");
            elementEntriesBeanList.add(elementEntriesBean);
            return elementEntriesBeanList;
        }
        return elementEntriesBeanList;
    }

    public String getEmployees() {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            BIReportModel biModel = new BIReportModel();
            JSONObject json =
                biModel.runReport(BIReportModel.REPORT_NAME.EMPLOYEE_Report.getValue(),
                                  paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getEmployeesById(String personId) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if (personId != null) {

                paramMap.put(BIReportModel.EMPLOYEE_BY_ID_PARAM.PERSON_ID.getValue(),
                             personId);
            }
            BIReportModel biModel = new BIReportModel();
            JSONObject json =
                biModel.runReport(BIReportModel.REPORT_NAME.EMPLOYEE_BY_ID.getValue(),
                                  paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ElementEntriesBean> getAllElementEligable(String personId) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            JSONArray empJson = new JSONArray(getEmployeesById(personId));
            JSONObject dataG1 = empJson.optJSONObject(0);
            String jobId = dataG1.optString("JOB_ID");
            String organizationId = dataG1.optString("ORGANIZATION_ID");
            String positionId = dataG1.optString("POSITION_ID");
            String gradeId = dataG1.optString("GRADE_ID");
            String locationId = dataG1.optString("LOCATION_ID");
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT DISTINCT PETF.ELEMENT_TYPE_ID\n" +
                    "FROM PAY_ELEMENT_TYPES_F PETF,\n" +
                    "PAY_ELE_CLASSIFICATIONS PEC,\n" +
                    "PAY_ELEMENT_LINKS_F PELF\n" +
                    "WHERE PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "AND PETF.ELEMENT_TYPE_ID = PELF.ELEMENT_TYPE_ID\n" +
                    "AND (PELF.JOB_ID = NVL('', ?) OR PELF.JOB_ID is null)\n" +
                    "AND (PELF.POSITION_ID = NVL('', ?) OR PELF.POSITION_ID is null)\n" +
                    "AND (PELF.ORGANIZATION_ID = NVL('', ?) OR PELF.ORGANIZATION_ID is null) \n" +
                    "AND (PELF.LOCATION_ID = NVL('', ?) OR PELF.LOCATION_ID is null)\n" +
                    "AND (PELF.GRADE_ID = NVL('', ?) OR PELF.GRADE_ID is null)\n" +
                    "AND to_date(PETF.EFFECTIVE_END_DATE,'dd-MM-yyyy') > to_date(SYSDATE, 'dd-MM-yyyy')\n" +
                    "AND to_date(PELF.EFFECTIVE_END_DATE,'dd-MM-yyyy') > to_date(SYSDATE, 'dd-MM-yyyy')";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setString(1, jobId);
            ps.setString(2, positionId);
            ps.setString(3, organizationId);
            ps.setString(4, locationId);
            ps.setString(5, gradeId);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementEntriesBean elementEntriesBean =
                    new ElementEntriesBean();
                elementEntriesBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                System.out.println(elementEntriesBean.getElementTypeId());
                elementEntriesBeanList.add(elementEntriesBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntriesBeanList;
    }

    public String getEmployeesByEffectiveDate(String effectiveDate) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if (effectiveDate != null) {

                paramMap.put(BIReportModel.Employee_By_Effective_Date.EFFECTIVE_DATE.getValue(),
                             effectiveDate);
            }
            BIReportModel biModel = new BIReportModel();
            JSONObject json =
                biModel.runReport(BIReportModel.REPORT_NAME.Employee_By_Effective_Date.getValue(),
                                  paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ElementEntriesBean> correctElementEntryValue(String correctElementEntry) {

        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            ElementEntriesBean elementEntriesBean = new ElementEntriesBean();
            JSONObject json = new JSONObject(correctElementEntry);
            JSONArray innerJson = new JSONArray();
            innerJson = json.optJSONArray("inputValues");
            connection = AppsproConnection.getConnection();
            String query = null;
            for (int i = 0; i < innerJson.length(); i++) {
                query = "UPDATE PAYROLL_TEST.PAY_ELEMENT_ENTRY_VALUES_F SET SCREEN_ENTRY_VALUE = ? WHERE INPUT_VALUE_ID = ? AND ELEMENT_ENTRY_ID = ?";
                System.out.println("\nExecuting query: " + query);
                cstmt = connection.prepareCall(query);

                String value = innerJson.optJSONObject(i).optString("Value");
                elementEntriesBean.setScreenEntryValue(value);
                cstmt.setString(1, elementEntriesBean.getScreenEntryValue());
                
                String inputValueId = innerJson.optJSONObject(i).optString("id");
                elementEntriesBean.setInputValueId(inputValueId);
                cstmt.setString(2, elementEntriesBean.getInputValueId());
                
                int elementEntryId =
                    Integer.valueOf(innerJson.optJSONObject(i).optString("elementEntryId"));
                elementEntriesBean.setElementEntryId(elementEntryId);
                cstmt.setInt(3, elementEntriesBean.getElementEntryId());
                cstmt.executeUpdate();
            }
            elementEntriesBeanList.add(elementEntriesBean);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return elementEntriesBeanList;
    }

    public List<ElementEntriesBean> getAllElementEntryByEffectiveDate(String effectiveDate) {
        List<ElementEntriesBean> elementEntriesBeanList =
            new ArrayList<ElementEntriesBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT PEEF.ELEMENT_ENTRY_ID, PEEF.EFFECTIVE_START_DATE, PEEF.EFFECTIVE_END_DATE, PEEF.ASSIGNMENT_ID, PEEF.ELEMENT_TYPE_ID, PETF.ELEMENT_NAME, PETF.PROCESSING_TYPE, PEC.BASE_CLASSIFICATION_NAME\n" +
                    "FROM PAY_ELEMENT_ENTRIES_F PEEF,PAY_ELEMENT_ENTRY_VALUES_F PEEVF, PAY_ELEMENT_TYPES_F PETF, PAY_ELE_CLASSIFICATIONS PEC, PAY_INPUT_VALUES_F PIVF\n" +
                    "WHERE PEEF.ELEMENT_ENTRY_ID = PEEVF.ELEMENT_ENTRY_ID\n" +
                    "AND PEEF.ELEMENT_TYPE_ID = PETF.ELEMENT_TYPE_ID\n" +
                    "AND PEEVF.INPUT_VALUE_ID = PIVF.INPUT_VALUE_ID\n" +
                    "AND PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "AND TO_DATE(?,'DD-MM-YYYY') between TO_CHAR(TO_DATE(PEEF.EFFECTIVE_START_DATE,'DD-MM-YYYY')) and TO_CHAR(TO_DATE(PEEF.EFFECTIVE_END_DATE,'DD-MM-YYYY'))";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            System.out.println(effectiveDate);
            ps.setString(1, effectiveDate);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementEntriesBean elementEntriesBean =
                    new ElementEntriesBean();
                elementEntriesBean.setElementEntryId(rs.getInt("ELEMENT_ENTRY_ID"));
                elementEntriesBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementEntriesBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementEntriesBean.setPersonId(rs.getString("ASSIGNMENT_ID"));
                elementEntriesBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementEntriesBean.setElementName(rs.getString("ELEMENT_NAME"));
                elementEntriesBean.setProccessingType(rs.getString("PROCESSING_TYPE"));
                elementEntriesBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementEntriesBeanList.add(elementEntriesBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementEntriesBeanList;
    }

    public static void main(String[] args) {
        ElementEntriesDAO elementEntriesDAO = new ElementEntriesDAO();
        elementEntriesDAO.getAllElementEligable("300000002029557");
    }

}
