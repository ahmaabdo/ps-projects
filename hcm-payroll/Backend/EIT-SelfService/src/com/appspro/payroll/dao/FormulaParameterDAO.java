package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.payroll.bean.FastFormulaBean;

import com.appspro.payroll.bean.FormulaParameterBean;

import common.restHelper.RestHelper;
import java.util.ArrayList;
import java.util.List;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

public class FormulaParameterDAO extends AppsproConnection{

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public String addelFourmlaPram(List<FormulaParameterBean> formulaParameterList , String fourmlaId) throws JSONException,
                                                                             SQLException {
           JSONObject jObject = new JSONObject();
           System.out.println(formulaParameterList.size());
           String result = "";
            connection = AppsproConnection.getConnection();
            String sql_select = "{call FastFourmlaPram( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)}";
           try  {
               int count = 0;
               ps = connection.prepareStatement(sql_select);
               //(   
               // , PRAM1, PRAM2, PRAM3, PRAM4, PRAM5)
         for (FormulaParameterBean elementPram : formulaParameterList) {
             elementPram.setFormula_id(fourmlaId);
             System.out.println(elementPram.getName());
                   ps.setString(1, elementPram.getName());
                   ps.setString(2, elementPram.getParameter_type());
                   ps.setString(3, elementPram.getParameter_category());
                   ps.setString(4, elementPram.getSource());
                   ps.setString(5, elementPram.getLength());
                   ps.setString(6, elementPram.getFormula_id());
                   ps.setString(7, elementPram.getDirections());
                   ps.setString(8, elementPram.getElementName());
                   ps.setString(9, elementPram.getInputValue());
                   ps.setString(10, elementPram.getReportName());
                   ps.setString(11, elementPram.getPram1());
                   ps.setString(12, elementPram.getPram2());
                   ps.setString(13, elementPram.getPram3());
                   ps.setString(14, elementPram.getPram4());
                   ps.setString(15, elementPram.getPram5());
                   ps.setString(16, elementPram.getPRAM1_SOURCE());
                   ps.setString(17, elementPram.getPRAM1_ELEMENT());
                   ps.setString(18, elementPram.getPRAM2_SOURCE());
                   ps.setString(19, elementPram.getPRAM2_ELEMENT());
                   ps.setString(20, elementPram.getPRAM3_SOURCE());
                   ps.setString(21, elementPram.getPRAM3_ELEMENT());
                   ps.setString(22, elementPram.getPRAM4_SOURCE());
                   ps.setString(23, elementPram.getPRAM4_ELEMENT());
                   ps.setString(24, elementPram.getPRAM5_SOURCE());
                   ps.setString(25, elementPram.getPRAM5_ELEMENT());
                                                                            
                   ps.addBatch();
                   count++;
                   // execute every 100 rows or less
                   if (count % 100 == 0 || count == formulaParameterList.size()) {
                      ps.executeBatch();
                   }
               }
        //            System.out.println(i + " records inserted");

     

               jObject.put("Status", "Inserted");

     

        //            System.out.println(JSONOutput);
           } catch (SQLException e) {
               e.printStackTrace();
               System.out.println("Noor Eror");
               System.out.println(e.getMessage());
               jObject.put("Status", "ERROR");
               jObject.put("ErrorCode", e.getMessage());
               result = jObject.toString();
           } finally {
               closeResources(connection, ps, rs);
           }

     

           return jObject.toString();
        }
    public String deleteFastFormulaPram(FastFormulaBean bean) throws JSONException,
                                                                        SQLException {
       JSONObject jObject = new JSONObject();
       String result = "";
        connection = AppsproConnection.getConnection();
       String sql_select = "{call FF_FORMULAS_F_PRO_Delete_PRAM(?)}";
       try  {
           ps = connection.prepareStatement(sql_select);
               ps.setString(1, bean.getFormula_id());
           
               ps.executeUpdate();
           jObject.put("Status", "Deleted");
       } catch (SQLException e) {
           e.printStackTrace();
           System.out.println("Noor Eror");
           System.out.println(e.getMessage());
           jObject.put("Status", "ERROR");
           jObject.put("ErrorCode", e.getMessage());
           result = jObject.toString();
       } finally {
           closeResources(connection, ps, rs);
       }

       return jObject.toString();
    }
    public List<FormulaParameterBean> getFastFormulaPram(String fourmlaId) {
        List<FormulaParameterBean> list = new ArrayList<FormulaParameterBean>();
        connection = AppsproConnection.getConnection();
        String query = "select * from FORMULA_PARAMETER \n" + 
        "where formula_id = ? ";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, fourmlaId);
            rs = ps.executeQuery();
     //(   ,
     // , PRAM1, PRAM2, PRAM3, PRAM4, PRAM5)
            while (rs.next()) {
                FormulaParameterBean obj = new FormulaParameterBean();
                obj.setId(rs.getString("ID"));
                obj.setName(rs.getString("NAME"));
                obj.setParameter_type(rs.getString("PARAMETER_TYPE"));
                obj.setParameter_category(rs.getString("PARAMETER_CATEGORY"));
                obj.setSource(rs.getString("SOURCE"));
                obj.setLength(rs.getString("LENGTH"));
                obj.setFormula_id(rs.getString("FORMULA_ID"));
                obj.setDirections(rs.getString("DIRECTIONS"));
                obj.setElementName(rs.getString("ELEMENTNAME"));
                obj.setInputValue(rs.getString("INPUTVALUE"));
                obj.setReportName(rs.getString("REPORTNAME"));
                obj.setPram1(rs.getString("PRAM1"));
                obj.setPram2(rs.getString("PRAM2"));
                obj.setPram3(rs.getString("PRAM3"));
                obj.setPram4(rs.getString("PRAM4"));
                obj.setPram5(rs.getString("PRAM5"));
                obj.setPRAM1_SOURCE(rs.getString("PRAM1_SOURCE"));
                obj.setPRAM1_ELEMENT(rs.getString("PRAM1_ELEMENT"));
                obj.setPRAM2_SOURCE(rs.getString("PRAM2_SOURCE"));
                obj.setPRAM2_ELEMENT(rs.getString("PRAM2_ELEMENT"));
                obj.setPRAM3_SOURCE(rs.getString("PRAM3_SOURCE"));
                obj.setPRAM3_ELEMENT(rs.getString("PRAM3_ELEMENT"));
                obj.setPRAM4_SOURCE(rs.getString("PRAM4_SOURCE"));
                obj.setPRAM4_ELEMENT(rs.getString("PRAM4_ELEMENT"));
                obj.setPRAM5_SOURCE(rs.getString("PRAM5_SOURCE"));
                obj.setPRAM5_ELEMENT(rs.getString("PRAM5_ELEMENT"));
                
                
                list.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
        
    }
    public String editFourmlaPram(List<FormulaParameterBean> formulaParameterList) throws JSONException,
                                                                             SQLException {
           JSONObject jObject = new JSONObject();
           System.out.println(formulaParameterList.size());
           String result = "";
            connection = AppsproConnection.getConnection();
            String sql_select = "{call FastFourmlaPramUpdate( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)}";
           try  {
               int count = 0;
               ps = connection.prepareStatement(sql_select);
               //(   
               // , PRAM1, PRAM2, PRAM3, PRAM4, PRAM5)
         for (FormulaParameterBean elementPram : formulaParameterList) {
             System.out.println(elementPram.getId());
                   ps.setString(1, elementPram.getName());
                   ps.setString(2, elementPram.getParameter_type());
                   ps.setString(3, elementPram.getParameter_category());
                   ps.setString(4, elementPram.getSource());
                   ps.setString(5, elementPram.getLength());
                   ps.setString(6, elementPram.getFormula_id());
                   ps.setString(7, elementPram.getDirections());
                   ps.setString(8, elementPram.getElementName());
                   ps.setString(9, elementPram.getInputValue());
                   ps.setString(10, elementPram.getReportName());
                   ps.setString(11, elementPram.getPram1());
                   ps.setString(12, elementPram.getPram2());
                   ps.setString(13, elementPram.getPram3());
                   ps.setString(14, elementPram.getPram4());
                   ps.setString(15, elementPram.getPram5());
                   ps.setString(16, elementPram.getId());
                                                                            
                   ps.addBatch();
                   count++;
                   // execute every 100 rows or less
                   if (count % 100 == 0 || count == formulaParameterList.size()) {
                      ps.executeBatch();
                   }
               }
        //            System.out.println(i + " records inserted");

     

               jObject.put("Status", "Inserted");

     

        //            System.out.println(JSONOutput);
           } catch (SQLException e) {
               e.printStackTrace();
               System.out.println("Noor Eror");
               System.out.println(e.getMessage());
               jObject.put("Status", "ERROR");
               jObject.put("ErrorCode", e.getMessage());
               result = jObject.toString();
           } finally {
               closeResources(connection, ps, rs);
           }

     

           return jObject.toString();
        }
}
