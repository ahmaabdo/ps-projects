package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;


import com.appspro.payroll.bean.FormulaResultBean;

import com.appspro.payroll.bean.FormulaResultRulesBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;


import oracle.jdbc.OracleTypes;

import org.json.JSONArray;

import org.json.JSONObject;


public class FormulaResultDAO extends AppsproConnection {
    Connection connection;
    Connection connection2;
    PreparedStatement ps;
    PreparedStatement ps2;
    PreparedStatement innerPS;
    ResultSet rs;
    ResultSet rs2;
    ResultSet innerRS;
    RestHelper rh = new RestHelper();
    static String date;
    CallableStatement cstmt = null;
    CallableStatement innerCStmt = null;

    public List<FormulaResultBean> insertFormulaResult(String insertFormulaResult) {

        List<FormulaResultBean> formulaResultBeanList =
            new ArrayList<FormulaResultBean>();
        List<FormulaResultRulesBean> formulaResultRulesBeanList =
            new ArrayList<FormulaResultRulesBean>();
        try {
            FormulaResultBean formulaResultBean = new FormulaResultBean();
            JSONObject json = new JSONObject(insertFormulaResult);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                formulaResultBean.setEffectiveEndDate(endDate);
            } else {
                formulaResultBean.setEffectiveEndDate("31-Dec-4721");
            }
            formulaResultBean.setEffectiveStartDate(startDate);

            formulaResultBean.setFormulaId(json.optString("formulaId"));
            formulaResultBean.setElementTypeId(json.optString("elementTypeId"));
            formulaResultBean.setBusinessGroupId(json.optString("businessGroupId"));
            formulaResultBean.setDescription(json.optString("description"));

            JSONArray innerJson = new JSONArray();
            innerJson = json.optJSONArray("formulaResultRules");
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call INSERT_PAY_FORMULA_USAGE_F(?,?,?,?,?,?,?)}";

            cstmt = connection.prepareCall(query);
            cstmt.setString(1, formulaResultBean.getEffectiveStartDate());
            cstmt.setString(2, formulaResultBean.getEffectiveEndDate());
            cstmt.setString(3, formulaResultBean.getFormulaId());
            cstmt.setString(4, formulaResultBean.getElementTypeId());
            cstmt.setString(5, formulaResultBean.getBusinessGroupId());
            cstmt.setString(6, formulaResultBean.getDescription());

            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.executeUpdate();
            int generatedId = cstmt.getInt(7);
            formulaResultBean.setFormulaUsageId(generatedId);

            for (int i = 0; i < innerJson.length(); i++) {


                FormulaResultRulesBean formulaResultRulesBean =
                    new FormulaResultRulesBean();


                SimpleDateFormat sdfInner = new SimpleDateFormat("dd-MM-yyyy");
                Date dateInner =
                    sdfInner.parse(innerJson.optJSONObject(i).optString("effectiveStartDate"));
                sdfInner = new SimpleDateFormat("dd-MMM-yyyy");
                String startDateInner = sdfInner.format(dateInner);
                SimpleDateFormat sdf2Inner =
                    new SimpleDateFormat("dd-MM-yyyy");
                String effectiveEndDateInner =
                    innerJson.optJSONObject(i).optString("effectiveEndDate");
                if (!effectiveEndDateInner.isEmpty()) {
                    Date endDateEffInner =
                        sdf2Inner.parse(effectiveEndDateInner);
                    sdf2Inner = new SimpleDateFormat("dd-MMM-yyyy");
                    String endDateInner = sdf2Inner.format(endDateEffInner);
                    formulaResultRulesBean.setEffectiveEndDate(endDateInner);
                } else {
                    formulaResultRulesBean.setEffectiveEndDate("31-Dec-4721");
                }

                formulaResultRulesBean.setEffectiveStartDate(startDateInner);
                formulaResultRulesBean.setFormulaUsageId(formulaResultBean.getFormulaUsageId());
                formulaResultRulesBean.setElementTypeId(innerJson.optJSONObject(i).optString("elementTypeId"));
                formulaResultRulesBean.setBusinessGroupId(innerJson.optJSONObject(i).optString("businessGroupId"));
                formulaResultRulesBean.setResultName(innerJson.optJSONObject(i).optString("resultName"));
                formulaResultRulesBean.setResultRuleType(innerJson.optJSONObject(i).optString("resultRuleType"));
                formulaResultRulesBean.setMessageType(innerJson.optJSONObject(i).optString("messageType"));
                formulaResultRulesBean.setInputValueId(innerJson.optJSONObject(i).optInt("inputValueId"));
                formulaResultRulesBean.setParameterId(innerJson.optJSONObject(i).optInt("parameterId"));

                query =
                        "{call INSERT_PAY_FORMULA_RESULT_RULES(?,?,?,?,?,?,?,?,?,?,?)}";

                cstmt = connection.prepareCall(query);
                cstmt.setString(1,
                                formulaResultRulesBean.getEffectiveStartDate());
                cstmt.setString(2,
                                formulaResultRulesBean.getEffectiveEndDate());
                cstmt.setInt(3, generatedId);
                cstmt.setString(4, formulaResultRulesBean.getElementTypeId());
                cstmt.setString(5,
                                formulaResultRulesBean.getBusinessGroupId());
                cstmt.setString(6, formulaResultRulesBean.getResultName());
                cstmt.setString(7, formulaResultRulesBean.getResultRuleType());
                cstmt.setString(8, formulaResultRulesBean.getMessageType());
                cstmt.setInt(9, formulaResultRulesBean.getInputValueId());
                cstmt.setInt(10, formulaResultRulesBean.getParameterId());

                cstmt.registerOutParameter(11, java.sql.Types.INTEGER);

                cstmt.executeUpdate();
                int generatedIdInner = cstmt.getInt(11);
                formulaResultRulesBean.setFormulaResultRuleId(generatedIdInner);
                formulaResultRulesBeanList.add(formulaResultRulesBean);
            }
            formulaResultBean.setFormulaResultRulesBeanList(formulaResultRulesBeanList);
            formulaResultBeanList.add(formulaResultBean);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return formulaResultBeanList;
    }


    public List<FormulaResultBean> updateFormulaResult(String updateFormulaResult) {

        List<FormulaResultBean> formulaResultBeanList =
            new ArrayList<FormulaResultBean>();
        List<FormulaResultRulesBean> formulaResultRulesBeanList =
            new ArrayList<FormulaResultRulesBean>();
        try {
            FormulaResultBean formulaResultBean = new FormulaResultBean();
            JSONObject json = new JSONObject(updateFormulaResult);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                formulaResultBean.setEffectiveEndDate(endDate);
            } else {
                formulaResultBean.setEffectiveEndDate("31-Dec-4721");
            }
            formulaResultBean.setEffectiveStartDate(startDate);

            formulaResultBean.setFormulaId(json.optString("formulaId"));
            formulaResultBean.setElementTypeId(json.optString("elementTypeId"));
            formulaResultBean.setBusinessGroupId(json.optString("businessGroupId"));
            formulaResultBean.setDescription(json.optString("description"));

            JSONArray innerJson = new JSONArray();
            innerJson = json.optJSONArray("formulaResultRules");

            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call UPDATE_PAY_FORMULA_USAGE_F(?,?,?,?,?,?,?,?)}";

            cstmt = connection.prepareCall(query);

            cstmt.setInt(1, json.optInt("formulaUsageId"));
            cstmt.setString(2, formulaResultBean.getEffectiveStartDate());
            cstmt.setString(3, formulaResultBean.getEffectiveEndDate());
            cstmt.setString(4, formulaResultBean.getFormulaId());
            cstmt.setString(5, formulaResultBean.getElementTypeId());
            cstmt.setString(6, formulaResultBean.getBusinessGroupId());
            cstmt.setString(7, formulaResultBean.getDescription());

            cstmt.registerOutParameter(8, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            int generatedId = cstmt.getInt(8);
            formulaResultBean.setFormulaUsageId(generatedId);

            for (int i = 0; i < innerJson.length(); i++) {


                FormulaResultRulesBean formulaResultRulesBean =
                    new FormulaResultRulesBean();


                SimpleDateFormat sdfInner = new SimpleDateFormat("dd-MM-yyyy");
                Date dateInner =
                    sdfInner.parse(innerJson.optJSONObject(i).optString("effectiveStartDate"));
                sdfInner = new SimpleDateFormat("dd-MMM-yyyy");
                String startDateInner = sdfInner.format(dateInner);
                SimpleDateFormat sdf2Inner =
                    new SimpleDateFormat("dd-MM-yyyy");
                String effectiveEndDateInner =
                    innerJson.optJSONObject(i).optString("effectiveEndDate");
                if (!effectiveEndDateInner.isEmpty()) {
                    Date endDateEffInner =
                        sdf2Inner.parse(effectiveEndDateInner);
                    sdf2Inner = new SimpleDateFormat("dd-MMM-yyyy");
                    String endDateInner = sdf2Inner.format(endDateEffInner);
                    formulaResultRulesBean.setEffectiveEndDate(endDateInner);
                } else {
                    formulaResultRulesBean.setEffectiveEndDate("31-Dec-4721");
                }

                formulaResultRulesBean.setEffectiveStartDate(startDateInner);
                formulaResultRulesBean.setFormulaUsageId(formulaResultBean.getFormulaUsageId());
                formulaResultRulesBean.setElementTypeId(innerJson.optJSONObject(i).optString("elementTypeId"));
                formulaResultRulesBean.setBusinessGroupId(innerJson.optJSONObject(i).optString("businessGroupId"));
                formulaResultRulesBean.setResultName(innerJson.optJSONObject(i).optString("resultName"));
                formulaResultRulesBean.setResultRuleType(innerJson.optJSONObject(i).optString("resultRuleType"));
                formulaResultRulesBean.setMessageType(innerJson.optJSONObject(i).optString("messageType"));

                formulaResultRulesBean.setInputValueId(innerJson.optJSONObject(i).optInt("inputValueId"));
                formulaResultRulesBean.setParameterId(innerJson.optJSONObject(i).optInt("parameterId"));

                query =
                        "{call UPDATE_PAY_FORMULA_RESULT_RULES(?,?,?,?,?,?,?,?,?,?,?,?)}";

                cstmt = connection.prepareCall(query);

                cstmt.setInt(1,
                             innerJson.optJSONObject(i).optInt("formulaResultRuleId"));
                cstmt.setString(2,
                                formulaResultRulesBean.getEffectiveStartDate());
                cstmt.setString(3,
                                formulaResultRulesBean.getEffectiveEndDate());
                cstmt.setInt(4, generatedId);
                cstmt.setString(5, formulaResultRulesBean.getElementTypeId());
                cstmt.setString(6,
                                formulaResultRulesBean.getBusinessGroupId());
                cstmt.setString(7, formulaResultRulesBean.getResultName());
                cstmt.setString(8, formulaResultRulesBean.getResultRuleType());
                cstmt.setString(9, formulaResultRulesBean.getMessageType());
                cstmt.setInt(10, formulaResultRulesBean.getInputValueId());
                cstmt.setInt(11, formulaResultRulesBean.getParameterId());

                cstmt.registerOutParameter(12, OracleTypes.NUMBER);

                cstmt.executeUpdate();

                int generatedIdInner = cstmt.getInt(12);
                formulaResultRulesBean.setFormulaResultRuleId(generatedIdInner);
                formulaResultRulesBeanList.add(formulaResultRulesBean);
            }
            formulaResultBean.setFormulaResultRulesBeanList(formulaResultRulesBeanList);
            formulaResultBeanList.add(formulaResultBean);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return formulaResultBeanList;
    }

    public List<FormulaResultBean> getAllFormulaResultsByElementId(int id) {
        List<FormulaResultBean> formulaResultBeanList =
            new ArrayList<FormulaResultBean>();

        List<FormulaResultRulesBean> formulaResultRulesBeanList;

        FormulaResultBean formulaResultBean;
        FormulaResultRulesBean formulaResultRulesBean;

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            String innerQuery = null;
            query = "SELECT\n" +
                    "    \"FU\".\"FORMULA_USAGE_ID\"       \"FORMULA_USAGE_ID\",\n" +
                    "    \"FU\".\"EFFECTIVE_START_DATE\"   \"EFFECTIVE_START_DATE\",\n" +
                    "    \"FU\".\"EFFECTIVE_END_DATE\"     \"EFFECTIVE_END_DATE\",\n" +
                    "    \"FU\".\"FORMULA_ID\"             \"FORMULA_ID\",\n" +
                    "    \"FU\".\"ELEMENT_TYPE_ID\"        \"ELEMENT_TYPE_ID\",\n" +
                    "    \"FU\".\"BUSINESS_GROUP_ID\"      \"BUSINESS_GROUP_ID\",\n" +
                    "    \"FU\".\"DESCRIPTION\"            \"DESCRIPTION\"\n" +
                    "FROM\n" +
                    "    \"PAYROLL_TEST\".\"PAY_FORMULA_USAGE_F\" \"FU\"\n" +
                    "    WHERE \"FU\".ELEMENT_TYPE_ID = ?\n" +
                    "    AND to_date(\"FU\".\"EFFECTIVE_END_DATE\", 'DD-MM-YYYY') > to_date(sysdate, 'DD-MM-YYYY')";


            innerQuery = "SELECT\n" +
                    "    \"FRR\".\"FORMULA_RESULT_RULE_ID\"       \"FORMULA_RESULT_RULE_ID\",\n" +
                    "    \"FRR\".\"EFFECTIVE_START_DATE\"   \"EFFECTIVE_START_DATE\",\n" +
                    "    \"FRR\".\"EFFECTIVE_END_DATE\"     \"EFFECTIVE_END_DATE\",\n" +
                    "    \"FRR\".\"FORMULA_USAGE_ID\"             \"FORMULA_USAGE_ID\",\n" +
                    "    \"FRR\".\"ELEMENT_TYPE_ID\"        \"ELEMENT_TYPE_ID\",\n" +
                    "    \"FRR\".\"BUSINESS_GROUP_ID\"      \"BUSINESS_GROUP_ID\",\n" +
                    "    \"FRR\".\"RESULT_NAME\"            \"RESULT_NAME\",\n" +
                    "    \"FRR\".\"RESULT_RULE_TYPE\"        \"RESULT_RULE_TYPE\",\n" +
                    "    \"FRR\".\"MESSAGE_TYPE\"      \"MESSAGE_TYPE\",\n" +
                    "    \"FRR\".\"INPUT_VALUE_ID\"      \"INPUT_VALUE_ID\",\n" +
                    "    \"FRR\".\"PARAMETER_ID\"      \"PARAMETER_ID\"\n" +
                    "FROM \n" +
                    "    pay_formula_result_rules_f FRR\n" +
                    "WHERE\n" +
                    "   \"FRR\".\"FORMULA_USAGE_ID\"= ?\n" +
                    "AND\n" +
                    "  to_date(\"FRR\".\"EFFECTIVE_END_DATE\", 'DD-MM-YYYY') > to_date(sysdate, 'DD-MM-YYYY')";


            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                formulaResultBean = new FormulaResultBean();
                formulaResultBean.setFormulaUsageId(rs.getInt("FORMULA_USAGE_ID"));
                int formulaUsageId = rs.getInt("FORMULA_USAGE_ID");
                formulaResultBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                formulaResultBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                formulaResultBean.setFormulaId(rs.getString("FORMULA_ID"));

                String formulaId = rs.getString("FORMULA_ID");
                if (formulaId != null) {
                    int formulaIdInt = Integer.parseInt(formulaId);
                    String formulaName = getFormulaNameById(formulaIdInt);
                    formulaResultBean.setFormulaName(formulaName);
                }
                formulaResultBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));

                String elementTypeIdOuter = rs.getString("ELEMENT_TYPE_ID");
                if (elementTypeIdOuter != null) {
                    int elementTypeIdIntOuter =
                        Integer.parseInt(elementTypeIdOuter);
                    String elementTypeNameOuter =
                        getElmentNameById(elementTypeIdIntOuter);
                    formulaResultBean.setElementName(elementTypeNameOuter);
                }
                formulaResultBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                formulaResultBean.setDescription(rs.getString("DESCRIPTION"));


                innerPS = connection.prepareStatement(innerQuery);
                innerPS.setInt(1, formulaUsageId);
                innerRS = innerPS.executeQuery();
                formulaResultRulesBeanList =
                        new ArrayList<FormulaResultRulesBean>();
                while (innerRS.next()) {
                    formulaResultRulesBean = new FormulaResultRulesBean();

                    formulaResultRulesBean.setFormulaResultRuleId(innerRS.getInt("FORMULA_RESULT_RULE_ID"));
                    formulaResultRulesBean.setEffectiveStartDate(innerRS.getString("EFFECTIVE_START_DATE"));
                    formulaResultRulesBean.setEffectiveEndDate(innerRS.getString("EFFECTIVE_END_DATE"));
                    formulaResultRulesBean.setFormulaUsageId(innerRS.getInt("FORMULA_USAGE_ID"));
                    formulaResultRulesBean.setElementTypeId(innerRS.getString("ELEMENT_TYPE_ID"));

                    String elementTypeIdInner =
                        innerRS.getString("ELEMENT_TYPE_ID");
                    if (elementTypeIdInner != null) {
                        int elementTypeIdInt =
                            Integer.parseInt(elementTypeIdInner);
                        String elementTypeNameInner =
                            getElmentNameById(elementTypeIdInt);
                        formulaResultRulesBean.setElementName(elementTypeNameInner);
                    }
                    formulaResultRulesBean.setBusinessGroupId(innerRS.getString("BUSINESS_GROUP_ID"));
                    formulaResultRulesBean.setResultName(innerRS.getString("RESULT_NAME"));
                    formulaResultRulesBean.setResultRuleType(innerRS.getString("RESULT_RULE_TYPE"));
                    formulaResultRulesBean.setMessageType(innerRS.getString("MESSAGE_TYPE"));
                    formulaResultRulesBean.setInputValueId(innerRS.getInt("INPUT_VALUE_ID"));

                    Integer inputValueId = innerRS.getInt("INPUT_VALUE_ID");

                    if (inputValueId != null) {
                        String inputValueNameInner =
                            getInputValueNameById(inputValueId);

                        formulaResultRulesBean.setInputValueName(inputValueNameInner);
                    }

                    formulaResultRulesBean.setParameterId(innerRS.getInt("PARAMETER_ID"));
                    Integer paramId = innerRS.getInt("PARAMETER_ID");

                    if (paramId != null) {
                        String parameterNameInner =
                            getParameterNameById(paramId);
                        formulaResultRulesBean.setParameterName(parameterNameInner);
                    }

                    formulaResultRulesBeanList.add(formulaResultRulesBean);

                }
                formulaResultBean.setFormulaResultRulesBeanList(formulaResultRulesBeanList);
                formulaResultBeanList.add(formulaResultBean);

            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
            closeResources(connection, innerPS, innerRS);
        }
        return formulaResultBeanList;
    }

    public FormulaResultBean getFormulaResultById(int id) {

        FormulaResultBean formulaResultBean = new FormulaResultBean();

        List<FormulaResultRulesBean> formulaResultRulesBeanList =
            new ArrayList<FormulaResultRulesBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            String innerQuery = null;

            query = "SELECT\n" +
                    "    \"FU\".\"FORMULA_USAGE_ID\"       \"FORMULA_USAGE_ID\",\n" +
                    "    \"FU\".\"EFFECTIVE_START_DATE\"   \"EFFECTIVE_START_DATE\",\n" +
                    "    \"FU\".\"EFFECTIVE_END_DATE\"     \"EFFECTIVE_END_DATE\",\n" +
                    "    \"FU\".\"FORMULA_ID\"             \"FORMULA_ID\",\n" +
                    "    \"FU\".\"ELEMENT_TYPE_ID\"        \"ELEMENT_TYPE_ID\",\n" +
                    "    \"FU\".\"BUSINESS_GROUP_ID\"      \"BUSINESS_GROUP_ID\",\n" +
                    "    \"FU\".\"DESCRIPTION\"            \"DESCRIPTION\"\n" +
                    "FROM\n" +
                    "    \"PAYROLL_TEST\".\"PAY_FORMULA_USAGE_F\" \"FU\"\n" +
                    "WHERE \n" +
                    "    \"FU\".\"FORMULA_USAGE_ID\" = ? ";


            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            rs.next();
            formulaResultBean.setFormulaUsageId(rs.getInt("FORMULA_USAGE_ID"));
            formulaResultBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
            formulaResultBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
            formulaResultBean.setFormulaId(rs.getString("FORMULA_ID"));

            String formulaId = rs.getString("FORMULA_ID");
            if(formulaId!=null){
                
            int formulaIdInt = Integer.parseInt(formulaId);
            String formulaName = getFormulaNameById(formulaIdInt);
            formulaResultBean.setFormulaName(formulaName);
            }

            formulaResultBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));

            String elementTypeIdOuter = rs.getString("ELEMENT_TYPE_ID");
            if(elementTypeIdOuter!=null){
            int elementTypeIdIntOuter = Integer.parseInt(elementTypeIdOuter);
            String elementTypeNameOuter =
                getElmentNameById(elementTypeIdIntOuter);
            formulaResultBean.setElementName(elementTypeNameOuter);
            }

            formulaResultBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
            formulaResultBean.setDescription(rs.getString("DESCRIPTION"));


            innerQuery = "SELECT\n" +
                    "    \"FRR\".\"FORMULA_RESULT_RULE_ID\"       \"FORMULA_RESULT_RULE_ID\",\n" +
                    "    \"FRR\".\"EFFECTIVE_START_DATE\"   \"EFFECTIVE_START_DATE\",\n" +
                    "    \"FRR\".\"EFFECTIVE_END_DATE\"     \"EFFECTIVE_END_DATE\",\n" +
                    "    \"FRR\".\"FORMULA_USAGE_ID\"             \"FORMULA_USAGE_ID\",\n" +
                    "    \"FRR\".\"ELEMENT_TYPE_ID\"        \"ELEMENT_TYPE_ID\",\n" +
                    "    \"FRR\".\"BUSINESS_GROUP_ID\"      \"BUSINESS_GROUP_ID\",\n" +
                    "    \"FRR\".\"RESULT_NAME\"            \"RESULT_NAME\",\n" +
                    "    \"FRR\".\"RESULT_RULE_TYPE\"        \"RESULT_RULE_TYPE\",\n" +
                    "    \"FRR\".\"MESSAGE_TYPE\"      \"MESSAGE_TYPE\",\n" +
                    "    \"FRR\".\"INPUT_VALUE_ID\"      \"INPUT_VALUE_ID\",\n" +
                    "    \"FRR\".\"PARAMETER_ID\"      \"PARAMETER_ID\"\n" +
                    "FROM \n" +
                    "    pay_formula_result_rules_f FRR\n" +
                    "WHERE\n" +
                    "   \"FRR\".\"FORMULA_USAGE_ID\"= ?\n" +
                    "AND\n" +
                    "  to_date(\"FRR\".\"EFFECTIVE_END_DATE\", 'DD-MM-YYYY') > to_date(sysdate, 'DD-MM-YYYY')";


            innerPS = connection.prepareStatement(innerQuery);
            innerPS.setInt(1, rs.getInt("FORMULA_USAGE_ID"));
            innerRS = innerPS.executeQuery();
            while (innerRS.next()) {
                FormulaResultRulesBean formulaResultRulesBean =
                    new FormulaResultRulesBean();

                formulaResultRulesBean.setFormulaResultRuleId(innerRS.getInt("FORMULA_RESULT_RULE_ID"));
                formulaResultRulesBean.setEffectiveStartDate(innerRS.getString("EFFECTIVE_START_DATE"));
                formulaResultRulesBean.setEffectiveEndDate(innerRS.getString("EFFECTIVE_END_DATE"));
                formulaResultRulesBean.setFormulaUsageId(innerRS.getInt("FORMULA_USAGE_ID"));
                formulaResultRulesBean.setElementTypeId(innerRS.getString("ELEMENT_TYPE_ID"));


                String elementTypeIdInner =
                    innerRS.getString("ELEMENT_TYPE_ID");
                if(elementTypeIdInner!=null){
                int elementTypeIdIntInner =
                    Integer.parseInt(elementTypeIdInner);
                String elementTypeNameInner =
                    getElmentNameById(elementTypeIdIntInner);
                formulaResultRulesBean.setElementName(elementTypeNameInner);
                }

                formulaResultRulesBean.setBusinessGroupId(innerRS.getString("BUSINESS_GROUP_ID"));
                formulaResultRulesBean.setResultName(innerRS.getString("RESULT_NAME"));
                formulaResultRulesBean.setResultRuleType(innerRS.getString("RESULT_RULE_TYPE"));
                formulaResultRulesBean.setMessageType(innerRS.getString("MESSAGE_TYPE"));
                formulaResultRulesBean.setInputValueId(innerRS.getInt("INPUT_VALUE_ID"));

                Integer inputValueId = innerRS.getInt("INPUT_VALUE_ID");
                if(inputValueId!=null){
                String inputValueNameInner =
                    getInputValueNameById(inputValueId);
                formulaResultRulesBean.setInputValueName(inputValueNameInner);
                }

                formulaResultRulesBean.setParameterId(innerRS.getInt("PARAMETER_ID"));

                Integer paramId = innerRS.getInt("PARAMETER_ID");
                if(paramId!=null){
                String parameterNameInner =
                    getParameterNameById(paramId);
                formulaResultRulesBean.setParameterName(parameterNameInner);
                }

                formulaResultRulesBeanList.add(formulaResultRulesBean);

            }
            formulaResultBean.setFormulaResultRulesBeanList(formulaResultRulesBeanList);

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
            closeResources(connection, innerPS, innerRS);
        }
        return formulaResultBean;
    }


    public String deleteFormulaResultById(int id) {
        String innerQuery = null;

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            String updateProcedure = null;
            query = "{call PAYROLL_TEST.DELETE_FORMULA_USAGE_BY_ID(?)}";


            cstmt = connection.prepareCall(query);
            cstmt.setInt(1, id);

            cstmt.executeQuery();


            innerQuery = "SELECT\n" +
                    "    \"FRR\".\"FORMULA_RESULT_RULE_ID\"       \"FORMULA_RESULT_RULE_ID\",\n" +
                    "    \"FRR\".\"EFFECTIVE_START_DATE\"   \"EFFECTIVE_START_DATE\",\n" +
                    "    \"FRR\".\"EFFECTIVE_END_DATE\"     \"EFFECTIVE_END_DATE\",\n" +
                    "    \"FRR\".\"FORMULA_USAGE_ID\"             \"FORMULA_USAGE_ID\",\n" +
                    "    \"FRR\".\"ELEMENT_TYPE_ID\"        \"ELEMENT_TYPE_ID\",\n" +
                    "    \"FRR\".\"BUSINESS_GROUP_ID\"      \"BUSINESS_GROUP_ID\",\n" +
                    "    \"FRR\".\"RESULT_NAME\"            \"RESULT_NAME\",\n" +
                    "    \"FRR\".\"RESULT_RULE_TYPE\"        \"RESULT_RULE_TYPE\",\n" +
                    "    \"FRR\".\"MESSAGE_TYPE\"      \"MESSAGE_TYPE\",\n" +
                    "    \"FRR\".\"INPUT_VALUE_ID\"      \"INPUT_VALUE_ID\",\n" +
                    "    \"FRR\".\"PARAMETER_ID\"      \"PARAMETER_ID\"\n" +
                    "FROM \n" +
                    "    pay_formula_result_rules_f FRR\n" +
                    "WHERE\n" +
                    "   \"FRR\".\"FORMULA_USAGE_ID\"= ?\n" +
                    "AND\n" +
                    "  to_date(\"FRR\".\"EFFECTIVE_END_DATE\", 'DD-MM-YYYY') > to_date(sysdate, 'DD-MM-YYYY')";


            innerPS = connection.prepareStatement(innerQuery);
            innerPS.setInt(1, id);
            innerRS = innerPS.executeQuery();

            updateProcedure =
                    "{call PAYROLL_TEST.DELETE_PAY_FORMULA_RESULT_RULES(?)}";
            while (innerRS.next()) {


                innerCStmt = connection.prepareCall(updateProcedure);
                innerCStmt.setInt(1, innerRS.getInt("FORMULA_RESULT_RULE_ID"));
                innerCStmt.executeQuery();

            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
            closeResources(connection, innerPS, innerRS);
        }
        return "Successful removed data";
    }

    public String deleteFormulaResultRuleById(int id) {

        try {
            connection = AppsproConnection.getConnection();
            String query = null;

            query = "{call PAYROLL_TEST.DELETE_PAY_FORMULA_RESULT_RULES(?)}";


            cstmt = connection.prepareCall(query);
            cstmt.setInt(1, id);

            cstmt.executeQuery();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return "Successful Removed data";
    }


    public FormulaResultRulesBean insertFormulaResultRule(String insertFormulaResultRule) {


        FormulaResultRulesBean formulaResultRulesBean =
            new FormulaResultRulesBean();
        try {
            connection = AppsproConnection.getConnection();
            JSONObject json = new JSONObject(insertFormulaResultRule);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                formulaResultRulesBean.setEffectiveEndDate(endDate);
            } else {
                formulaResultRulesBean.setEffectiveEndDate("31-Dec-4721");
            }
            formulaResultRulesBean.setEffectiveStartDate(startDate);
            formulaResultRulesBean.setFormulaUsageId(json.optInt("formulaUsageId"));
            formulaResultRulesBean.setElementTypeId(json.optString("elementTypeId"));
            formulaResultRulesBean.setBusinessGroupId(json.optString("businessGroupId"));
            formulaResultRulesBean.setResultName(json.optString("resultName"));
            formulaResultRulesBean.setResultRuleType(json.optString("resultRuleType"));
            formulaResultRulesBean.setMessageType(json.optString("messageType"));
            formulaResultRulesBean.setInputValueId(json.optInt("inputValueId"));
            formulaResultRulesBean.setParameterId(json.optInt("parameterId"));


            String query =
                "{call INSERT_PAY_FORMULA_RESULT_RULES(?,?,?,?,?,?,?,?,?,?,?)}";

            cstmt = connection.prepareCall(query);
            cstmt.setString(1, formulaResultRulesBean.getEffectiveStartDate());
            cstmt.setString(2, formulaResultRulesBean.getEffectiveEndDate());
            cstmt.setInt(3, formulaResultRulesBean.getFormulaUsageId());
            cstmt.setString(4, formulaResultRulesBean.getElementTypeId());
            cstmt.setString(5, formulaResultRulesBean.getBusinessGroupId());
            cstmt.setString(6, formulaResultRulesBean.getResultName());
            cstmt.setString(7, formulaResultRulesBean.getResultRuleType());
            cstmt.setString(8, formulaResultRulesBean.getMessageType());
            cstmt.setInt(9, formulaResultRulesBean.getInputValueId());
            cstmt.setInt(10, formulaResultRulesBean.getParameterId());

            cstmt.registerOutParameter(11, java.sql.Types.INTEGER);

            cstmt.executeUpdate();
            int generatedId = cstmt.getInt(11);
            formulaResultRulesBean.setFormulaResultRuleId(generatedId);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return formulaResultRulesBean;
    }

    public FormulaResultRulesBean updateFormulaResultRule(String updateFormulaResultRule) {


        FormulaResultRulesBean formulaResultRulesBean =
            new FormulaResultRulesBean();
        try {
            connection = AppsproConnection.getConnection();
            JSONObject json = new JSONObject(updateFormulaResultRule);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                formulaResultRulesBean.setEffectiveEndDate(endDate);
            } else {
                formulaResultRulesBean.setEffectiveEndDate("31-Dec-4721");
            }
            formulaResultRulesBean.setEffectiveStartDate(startDate);
            formulaResultRulesBean.setFormulaUsageId(json.optInt("formulaUsageId"));
            formulaResultRulesBean.setElementTypeId(json.optString("elementTypeId"));
            formulaResultRulesBean.setBusinessGroupId(json.optString("businessGroupId"));
            formulaResultRulesBean.setResultName(json.optString("resultName"));
            formulaResultRulesBean.setResultRuleType(json.optString("resultRuleType"));
            formulaResultRulesBean.setMessageType(json.optString("messageType"));
            formulaResultRulesBean.setInputValueId(json.optInt("inputValueId"));
            formulaResultRulesBean.setParameterId(json.optInt("parameterId"));


            String query =
                "{call UPDATE_PAY_FORMULA_RESULT_RULES(?,?,?,?,?,?,?,?,?,?,?,?)}";

            cstmt = connection.prepareCall(query);

            cstmt.setInt(1, json.optInt("formulaResultRuleId"));
            cstmt.setString(2, formulaResultRulesBean.getEffectiveStartDate());
            cstmt.setString(3, formulaResultRulesBean.getEffectiveEndDate());
            cstmt.setInt(4, formulaResultRulesBean.getFormulaUsageId());
            cstmt.setString(5, formulaResultRulesBean.getElementTypeId());
            cstmt.setString(6, formulaResultRulesBean.getBusinessGroupId());
            cstmt.setString(7, formulaResultRulesBean.getResultName());
            cstmt.setString(8, formulaResultRulesBean.getResultRuleType());
            cstmt.setString(9, formulaResultRulesBean.getMessageType());
            cstmt.setInt(10, formulaResultRulesBean.getInputValueId());
            cstmt.setInt(11, formulaResultRulesBean.getParameterId());

            cstmt.registerOutParameter(12, OracleTypes.NUMBER);

            cstmt.executeUpdate();

            int generatedId = cstmt.getInt(12);
            formulaResultRulesBean.setFormulaResultRuleId(generatedId);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return formulaResultRulesBean;
    }

    public String getElmentNameById(int id) {
        try {
            connection2 = AppsproConnection.getConnection();
            String query = null;

            query = "SELECT\n" +
                    "    \n" +
                    "    \"A1\".\"ELEMENT_NAME\"      \"ELEMENT_NAME\"\n" +
                    "FROM\n" +
                    "    \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"A1\"\n" +
                    "WHERE\n" +
                    "    \"A1\".\"ELEMENT_TYPE_ID\" = ?";

            ps2 = connection2.prepareStatement(query);
            ps2.setInt(1, id);
            rs2 = ps2.executeQuery();
            String elementName = null;
            while (rs2.next()) {
                elementName = rs2.getString("ELEMENT_NAME");
                return elementName;
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection2, ps2, rs2);
        }
        return null;
    }

    public String getElmentTypeIdByElmentName(String elmentName) {
        try {

            connection = AppsproConnection.getConnection();
            CallableStatement stmtGetElementTypeId =
                connection.prepareCall("{call PAYROLL_TEST.GET_ELEMENT_TYPE_ID_BY_NAME(?,?)}");

            stmtGetElementTypeId.setString(1, elmentName);
            stmtGetElementTypeId.registerOutParameter(2,
                                                      java.sql.Types.INTEGER);
            stmtGetElementTypeId.executeUpdate();
            Integer elementTypeId = stmtGetElementTypeId.getInt(2);
            return elementTypeId.toString();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getInputValueNameById(int id) {
        try {
            connection2 = AppsproConnection.getConnection();
            String query = null;

            query = "SELECT\n" +
                    "    \n" +
                    "    \"A1\".\"BASE_NAME\"   \"BASE_NAME\"\n" +
                    "FROM\n" +
                    "    \"PAYROLL_TEST\".\"PAY_INPUT_VALUES_F\" \"A1\"\n" +
                    "WHERE\n" +
                    "    \"A1\".\"INPUT_VALUE_ID\" = ?";

            ps2 = connection2.prepareStatement(query);
            ps2.setInt(1, id);
            rs2 = ps2.executeQuery();
            String InputValueName = null;
            while (rs2.next()) {
                InputValueName = rs2.getString("BASE_NAME");
                return InputValueName;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection2, ps2, rs2);
        }
        return null;
    }

    public Integer getInputValueIdByName(String inputValueName) {
        try {

            connection = AppsproConnection.getConnection();
            CallableStatement stmtGetInputValueName =
                connection.prepareCall("{call PAYROLL_TEST.GET_INPUT_VALUE_ID_BY_NAME(?,?)}");

            stmtGetInputValueName.setString(1, inputValueName);
            stmtGetInputValueName.registerOutParameter(2,
                                                       java.sql.Types.INTEGER);
            stmtGetInputValueName.executeUpdate();
            int inputValueId = stmtGetInputValueName.getInt(2);
            return inputValueId;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFormulaNameById(int id) {
        try {
            connection2 = AppsproConnection.getConnection();
            String query = null;

            query = "SELECT\n" +
                    "    \"A1\".\"FORMULA_NAME\" \"FORMULA_NAME\"\n" +
                    "FROM\n" +
                    "    \"PAYROLL_TEST\".\"FF_FORMULAS_F\" \"A1\"\n" +
                    "WHERE\n" +
                    "    \"A1\".\"FORMULA_ID\" = ?";

            ps2 = connection2.prepareStatement(query);
            ps2.setInt(1, id);
            rs2 = ps2.executeQuery();
            String formulaName = null;
            while (rs2.next()) {
                formulaName = rs2.getString("FORMULA_NAME");
                return formulaName;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection2, ps2, rs2);
        }
        return null;
    }

    public String getParameterNameById(int id) {
        try {
            connection2 = AppsproConnection.getConnection();
            String query = null;

            query = "SELECT\n" +
                    "    \"FP\".\"ID\"                   \"ID\",\n" +
                    "    \"FP\".\"NAME\"                 \"NAME\"\n" +
                    "\n" +
                    "FROM\n" +
                    "    \"PAYROLL_TEST\".\"FORMULA_PARAMETER\" \"FP\"\n" +
                    "    WHERE \"FP\".ID = ?";

            ps2 = connection2.prepareStatement(query);
            ps2.setInt(1, id);
            rs2 = ps2.executeQuery();
            String parameterName = null;
            while (rs2.next()) {
                parameterName = rs2.getString("NAME");
                return parameterName;
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection2, ps2, rs2);
        }
        return null;
    }

    //    public static void main(String[] args) {
    //
    //        FormulaResultDAO fd = new FormulaResultDAO();
    //        System.err.println(fd.getParameterNameById(32));
    //
    //    }

}

