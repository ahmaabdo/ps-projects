/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.payroll.dao;


import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.BIReportBean;

import com.appspro.payroll.bean.SummaryReportBean;

import common.biPReports.BIPReports;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;


import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;



import org.apache.commons.codec.binary.Base64;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Lenovo
 */
public class BIReportDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;
    CallableStatement cstmt = null;

    public String getBiReport(JSONObject jObject) {
        StringBuilder crunchifyBuilder = new StringBuilder();

        // JSONObject jObject; // json
        String fresponse = null;
        try {
            //    jObject = new JSONObject(incomingData);
            String reportName = jObject.get("reportName").toString();
            if (!BIPReports.REPORT_NAME.EIT_Name.getValue().equals(reportName)) {
                //                if(!Authentication.restAuth(authString, authorization)) {
                //                    return Response.status(Response.Status.UNAUTHORIZED).build();
                //                }
            }

            BIPReports biPReports = new BIPReports();
            biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
            biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

            if (reportName != null) {

                if (reportName.equals(BIPReports.REPORT_NAME.FUSE_REPORT.getValue())) {

                    String paramName = null;

                    //                    for (int i = 0;
                    //                         i < BIPReports.FUSE_REPORT_PARAM.values().length;
                    //                         i++) {
                    //                        paramName =
                    //                                BIPReports.FUSE_REPORT_PARAM.values()[i].getValue();
                    //                        biPReports.getParamMap().put(paramName,
                    //                                                     jObject.getString(paramName));
                    //                    }
                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.FUSE_REPORT.getValue());
                    fresponse = biPReports.executeReports();

                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.GET_ALL_POSITIONS.getValue())) {

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.GET_ALL_POSITIONS.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                             "value").replaceAll("LABEL",
                                                                                                                 "label");

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.EIT_REPORT.getValue())) {
                    String paramName = null;
                    for (int i = 0;
                         i < BIPReports.EIT_REPORT_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.EIT_REPORT_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.EIT_REPORT.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                             "value").replaceAll("LABEL",
                                                                                                                 "label");

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue())) {
                    String paramName = null;
                    //("Dynamic Report ");
                    for (int i = 0;
                         i < BIPReports.Dynamic_Rrport_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.Dynamic_Rrport_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString().replaceAll(">",
                                                                                                  "GT").replaceAll("<",
                                                                                                                   "LT"));
                    }
                    //(paramName);
                    //(jObject.get(paramName));
                    //(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    //(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("ROWSET").get("ROW").toString().replaceAll("VALUE",
                                                                                            "value").replaceAll("LABEL",
                                                                                                                "label");

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.Dynamic_Validation_Rrport.getValue())) {
                    String paramName = null;
                    //("Dynamic Validation  Report ");
                    for (int i = 0;
                         i < BIPReports.Dynamic_Rrport_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.Dynamic_Rrport_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString().replaceAll(">",
                                                                                                  "GT").replaceAll("<",
                                                                                                                   "LT"));
                    }
                    //(paramName);
                    //(jObject.get(paramName));
                    //(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.Dynamic_Rrport.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    //(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("ROWSET").get("ROW").toString().replaceAll("VALUE",
                                                                                            "value");

                    //                    return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
                    JSONObject jsonObj = new JSONObject(jsonString);
                    return jsonString;

                } else if (reportName.contains(BIPReports.REPORT_NAME.ABSENCE_REPORT.getValue())) {
                    String paramName = null;
                    //String subReportName = jObject.getString("reportSubName");
                    //                    //(subReportName);
                    for (int i = 0;
                         i < BIPReports.ABSENCE_REPORT_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.ABSENCE_REPORT_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.ABSENCE_REPORT.getValue());
                    biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                    biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                    byte[] reportBytes = biPReports.executeReportsPDF();

                    fresponse =
                            javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                    //  //(reportBytes);
                } else if (reportName.equals(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue())) {
                    String paramName = null;
                    //("get_Query_For_List_Report Report ");
                    for (int i = 0;
                         i < BIPReports.get_Query_For_List_Report_PARAM.values().length;
                         i++) {
                        paramName =
                                BIPReports.get_Query_For_List_Report_PARAM.values()[i].getValue();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                        //(BIPReports.get_Query_For_List_Report_PARAM.values()[i].getValue());
                    }

                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue());
                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                             "value").replaceAll("LABEL",
                                                                                                                 "label");

                    return jsonString;

                } else if (reportName.equals(BIPReports.REPORT_NAME.EIT_Name.getValue())) {

                    String paramName = null;

                    //                    for (int i = 0;
                    //                         i < BIPReports.FUSE_REPORT_PARAM.values().length;
                    //                         i++) {
                    //                        paramName =
                    //                                BIPReports.FUSE_REPORT_PARAM.values()[i].getValue();
                    //                        biPReports.getParamMap().put(paramName,
                    //                                                     jObject.getString(paramName));
                    //                    }
                    biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.EIT_Name.getValue());
                    fresponse = biPReports.executeReports();

                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());
                    String jsonString =
                        xmlJSONObj.getJSONObject("DATA_DS").getJSONArray("G_1").toString();

                    return jsonString;

                } else {
                    String paramName;
                    List<String> paramValues = new ArrayList<String>();
                    List<String> paramNameList =
                        Arrays.asList("id", "report_Name", "parameter1",
                                      "parameter2", "parameter3", "parameter4",
                                      "parameter5");

                    SummaryReportDAO summary = new SummaryReportDAO();
                    ArrayList arr = summary.getReportParamSummary(reportName);
                    SummaryReportBean reportBeans =
                        (SummaryReportBean)arr.get(0);

                    //(arr.size());

                    if (reportBeans.getParameter1() != null) {
                        paramName = reportBeans.getParameter1();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }
                    if (reportBeans.getParameter2() != null) {
                        paramName = reportBeans.getParameter2();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }
                    if (reportBeans.getParameter3() != null) {
                        paramName = reportBeans.getParameter3();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }
                    if (reportBeans.getParameter4() != null) {
                        paramName = reportBeans.getParameter4();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }
                    if (reportBeans.getParameter5() != null) {
                        paramName = reportBeans.getParameter5();
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    }

                    biPReports.setReportAbsolutePath(reportBeans.getReport_Name());

                    //                    for (SummaryReportBean reportBeans : arr) {
                    //
                    ////                        paramValues.add(reportBeans.getId());
                    ////                        paramValues.add(reportBeans.getReport_Name());
                    ////                        paramValues.add(reportBeans.getParameter1());
                    ////                        paramValues.add(reportBeans.getParameter2());
                    ////                        paramValues.add(reportBeans.getParameter3());
                    ////                        paramValues.add(reportBeans.getParameter4());
                    ////                        paramValues.add(reportBeans.getParameter5());
                    //
                    ////                        for (int i = 0; i < paramValues.size(); i++) {
                    ////                            paramName = paramNameList.get(i);
                    ////                            biPReports.getParamMap().put(paramName, jObject.getString(paramName));
                    ////                            biPReports.setReportAbsolutePath(BIPReports.REPORT_NAME.get_Query_For_List_Report.getValue());
                    ////                        }
                    //
                    //                     }
                    biPReports.setReportAbsolutePath(reportBeans.getReport_Name());

                    fresponse = biPReports.executeReports();
                    JSONObject xmlJSONObj =
                        XML.toJSONObject(fresponse.toString());

                    return xmlJSONObj.toString();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace(); // AppsproConnection.LOG.error("ERROR", e);
            //return Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode(),e.getMessage()).build();
        }

        // return HTTP response 200 in case of success
        return fresponse;
    }

    public static void doSomething(Node node) {
        // do something with the current node instead of System.out
        String xx = node.getNodeName();
        if (node.getNodeName().equals("G_1")) {
            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                Node currentNode = node.getChildNodes().item(i);

            }
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                doSomething(currentNode);
            }
        }
    }

    public BIReportBean insertReportInfo(String insertReportInfo) {
        BIReportBean biReportBean = new BIReportBean();

        try {

            JSONObject json = new JSONObject(insertReportInfo);


            biReportBean.setParameter1(json.optString("parameter1"));
            biReportBean.setParameter2(json.optString("parameter2"));
            biReportBean.setParameter3(json.optString("parameter3"));
            biReportBean.setParameter4(json.optString("parameter4"));
            biReportBean.setParameter5(json.optString("parameter5"));
            biReportBean.setReportName(json.optString("reportName"));
            biReportBean.setReportTypeVal(json.optString("reportTypeVal"));
            biReportBean.setParameter6(json.optString("parameter6"));
            biReportBean.setParameter7(json.optString("parameter7"));
            biReportBean.setParameter8(json.optString("parameter8"));
            biReportBean.setParameter9(json.optString("parameter9"));
            biReportBean.setParameter10(json.optString("parameter10"));
            biReportBean.setParameter11(json.optString("parameter11"));
            biReportBean.setParameter12(json.optString("parameter12"));
            biReportBean.setParameter13(json.optString("parameter13"));
            biReportBean.setParameter14(json.optString("parameter14"));
            biReportBean.setParameter15(json.optString("parameter15"));
            biReportBean.setParameter16(json.optString("parameter16"));
            biReportBean.setParameter17(json.optString("parameter17"));
            biReportBean.setParameter18(json.optString("parameter18"));
            biReportBean.setParameter19(json.optString("parameter19"));
            biReportBean.setParameter20(json.optString("parameter20"));


            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call INSERT_REPORT_INFO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

            cstmt = connection.prepareCall(query);
            cstmt.setString(1, biReportBean.getParameter1());
            cstmt.setString(2, biReportBean.getParameter2());
            cstmt.setString(3, biReportBean.getParameter3());
            cstmt.setString(4, biReportBean.getParameter4());
            cstmt.setString(5, biReportBean.getParameter5());
            cstmt.setString(6, biReportBean.getReportName());
            cstmt.setString(7, biReportBean.getReportTypeVal());
            cstmt.setString(8, biReportBean.getParameter6());
            cstmt.setString(9, biReportBean.getParameter7());
            cstmt.setString(10, biReportBean.getParameter8());
            cstmt.setString(11, biReportBean.getParameter9());
            cstmt.setString(12, biReportBean.getParameter10());
            cstmt.setString(13, biReportBean.getParameter11());
            cstmt.setString(14, biReportBean.getParameter12());
            cstmt.setString(15, biReportBean.getParameter13());
            cstmt.setString(16, biReportBean.getParameter14());
            cstmt.setString(17, biReportBean.getParameter15());
            cstmt.setString(18, biReportBean.getParameter16());
            cstmt.setString(19, biReportBean.getParameter17());
            cstmt.setString(20, biReportBean.getParameter18());
            cstmt.setString(21, biReportBean.getParameter19());
            cstmt.setString(22, biReportBean.getParameter20());

            cstmt.registerOutParameter(23, java.sql.Types.INTEGER);
            cstmt.executeUpdate();
            int generatedId = cstmt.getInt(23);
            biReportBean.setId(generatedId);

            
            return biReportBean;
           
            
        } catch (Exception  e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return biReportBean;
    }


    public BIReportBean updateReportInfo(String updateReportInfo) {

        BIReportBean biReportBean = new BIReportBean();

        try {

            JSONObject json = new JSONObject(updateReportInfo);

            biReportBean.setId(json.optInt("id"));
            biReportBean.setParameter1(json.optString("parameter1"));
            biReportBean.setParameter2(json.optString("parameter2"));
            biReportBean.setParameter3(json.optString("parameter3"));
            biReportBean.setParameter4(json.optString("parameter4"));
            biReportBean.setParameter5(json.optString("parameter5"));
            biReportBean.setReportName(json.optString("reportName"));
            biReportBean.setReportTypeVal(json.optString("reportTypeVal"));
            biReportBean.setParameter6(json.optString("parameter6"));
            biReportBean.setParameter7(json.optString("parameter7"));
            biReportBean.setParameter8(json.optString("parameter8"));
            biReportBean.setParameter9(json.optString("parameter9"));
            biReportBean.setParameter10(json.optString("parameter10"));
            biReportBean.setParameter11(json.optString("parameter11"));
            biReportBean.setParameter12(json.optString("parameter12"));
            biReportBean.setParameter13(json.optString("parameter13"));
            biReportBean.setParameter14(json.optString("parameter14"));
            biReportBean.setParameter15(json.optString("parameter15"));
            biReportBean.setParameter16(json.optString("parameter16"));
            biReportBean.setParameter17(json.optString("parameter17"));
            biReportBean.setParameter18(json.optString("parameter18"));
            biReportBean.setParameter19(json.optString("parameter19"));
            biReportBean.setParameter20(json.optString("parameter20"));


            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call UPDATE_REPORT_INFO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

            cstmt = connection.prepareCall(query);
            cstmt.setInt(1, biReportBean.getId());
            cstmt.setString(2, biReportBean.getParameter1());
            cstmt.setString(3, biReportBean.getParameter2());
            cstmt.setString(4, biReportBean.getParameter3());
            cstmt.setString(5, biReportBean.getParameter4());
            cstmt.setString(6, biReportBean.getParameter5());
            cstmt.setString(7, biReportBean.getReportName());
            cstmt.setString(8, biReportBean.getReportTypeVal());
            cstmt.setString(9, biReportBean.getParameter6());
            cstmt.setString(10, biReportBean.getParameter7());
            cstmt.setString(11, biReportBean.getParameter8());
            cstmt.setString(12, biReportBean.getParameter9());
            cstmt.setString(13, biReportBean.getParameter10());
            cstmt.setString(14, biReportBean.getParameter11());
            cstmt.setString(15, biReportBean.getParameter12());
            cstmt.setString(16, biReportBean.getParameter13());
            cstmt.setString(17, biReportBean.getParameter14());
            cstmt.setString(18, biReportBean.getParameter15());
            cstmt.setString(19, biReportBean.getParameter16());
            cstmt.setString(20, biReportBean.getParameter17());
            cstmt.setString(21, biReportBean.getParameter18());
            cstmt.setString(22, biReportBean.getParameter19());
            cstmt.setString(23, biReportBean.getParameter20());

            
            cstmt.executeUpdate();
            

            
            return biReportBean;
           
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return biReportBean;
    }
    
    public List<BIReportBean> getAllBIReportsInfo() {

        List<BIReportBean> biReportBeanList =
            new ArrayList<BIReportBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT\n" + 
                    "    \"R\".\"ID\"              \"ID\",\n" + 
                    "    \"R\".\"PARAMETER1\"      \"PARAMETER1\",\n" + 
                    "    \"R\".\"PARAMETER2\"      \"PARAMETER2\",\n" + 
                    "    \"R\".\"PARAMETER3\"      \"PARAMETER3\",\n" + 
                    "    \"R\".\"PARAMETER4\"      \"PARAMETER4\",\n" + 
                    "    \"R\".\"PARAMETER5\"      \"PARAMETER5\",\n" + 
                    "    \"R\".\"REPORT_NAME\"     \"REPORT_NAME\",\n" + 
                    "    \"R\".\"REPORTTYPEVAL\"   \"REPORTTYPEVAL\",\n" + 
                    "    \"R\".\"PARAMETER6\"      \"PARAMETER6\",\n" + 
                    "    \"R\".\"PARAMETER7\"      \"PARAMETER7\",\n" + 
                    "    \"R\".\"PARAMETER8\"      \"PARAMETER8\",\n" + 
                    "    \"R\".\"PARAMETER9\"      \"PARAMETER9\",\n" + 
                    "    \"R\".\"PARAMETER10\"     \"PARAMETER10\",\n" + 
                    "    \"R\".\"PARAMETER11\"     \"PARAMETER11\",\n" + 
                    "    \"R\".\"PARAMETER12\"     \"PARAMETER12\",\n" + 
                    "    \"R\".\"PARAMETER13\"     \"PARAMETER13\",\n" + 
                    "    \"R\".\"PARAMETER14\"     \"PARAMETER14\",\n" + 
                    "    \"R\".\"PARAMETER15\"     \"PARAMETER15\",\n" + 
                    "    \"R\".\"PARAMETER16\"     \"PARAMETER16\",\n" + 
                    "    \"R\".\"PARAMETER17\"     \"PARAMETER17\",\n" + 
                    "    \"R\".\"PARAMETER18\"     \"PARAMETER18\",\n" + 
                    "    \"R\".\"PARAMETER19\"     \"PARAMETER19\",\n" + 
                    "    \"R\".\"PARAMETER20\"     \"PARAMETER20\"\n" + 
                    "FROM\n" + 
                    "    \"PAYROLL_TEST\".\"XXX_REPORT\" \"R\"";
            
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                BIReportBean biReportBean =
                    new BIReportBean();

                biReportBean.setId(rs.getInt("ID"));
                biReportBean.setParameter1(rs.getString("PARAMETER1"));
                biReportBean.setParameter2(rs.getString("PARAMETER2"));
                biReportBean.setParameter3(rs.getString("PARAMETER3"));
                biReportBean.setParameter4(rs.getString("PARAMETER4"));
                biReportBean.setParameter5(rs.getString("PARAMETER5"));
                biReportBean.setReportName(rs.getString("REPORT_NAME"));
                biReportBean.setReportTypeVal(rs.getString("REPORTTYPEVAL"));
                biReportBean.setParameter6(rs.getString("PARAMETER6"));
                biReportBean.setParameter7(rs.getString("PARAMETER7"));
                biReportBean.setParameter8(rs.getString("PARAMETER8"));
                biReportBean.setParameter9(rs.getString("PARAMETER9"));
                biReportBean.setParameter10(rs.getString("PARAMETER10"));
                biReportBean.setParameter11(rs.getString("PARAMETER11"));
                biReportBean.setParameter12(rs.getString("PARAMETER12"));
                biReportBean.setParameter13(rs.getString("PARAMETER13"));
                biReportBean.setParameter14(rs.getString("PARAMETER14"));
                biReportBean.setParameter15(rs.getString("PARAMETER15"));
                biReportBean.setParameter16(rs.getString("PARAMETER16"));
                biReportBean.setParameter17(rs.getString("PARAMETER17"));
                biReportBean.setParameter18(rs.getString("PARAMETER18"));
                biReportBean.setParameter19(rs.getString("PARAMETER19"));
                biReportBean.setParameter20(rs.getString("PARAMETER20"));
          
                
                biReportBeanList.add(biReportBean);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return biReportBeanList;
    }
    
    public BIReportBean getBIReportInfoById(int id) {

        BIReportBean biReportBean =
            new BIReportBean();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT\n" + 
                    "    \"R\".\"ID\"              \"ID\",\n" + 
                    "    \"R\".\"PARAMETER1\"      \"PARAMETER1\",\n" + 
                    "    \"R\".\"PARAMETER2\"      \"PARAMETER2\",\n" + 
                    "    \"R\".\"PARAMETER3\"      \"PARAMETER3\",\n" + 
                    "    \"R\".\"PARAMETER4\"      \"PARAMETER4\",\n" + 
                    "    \"R\".\"PARAMETER5\"      \"PARAMETER5\",\n" + 
                    "    \"R\".\"REPORT_NAME\"     \"REPORT_NAME\",\n" + 
                    "    \"R\".\"REPORTTYPEVAL\"   \"REPORTTYPEVAL\",\n" + 
                    "    \"R\".\"PARAMETER6\"      \"PARAMETER6\",\n" + 
                    "    \"R\".\"PARAMETER7\"      \"PARAMETER7\",\n" + 
                    "    \"R\".\"PARAMETER8\"      \"PARAMETER8\",\n" + 
                    "    \"R\".\"PARAMETER9\"      \"PARAMETER9\",\n" + 
                    "    \"R\".\"PARAMETER10\"     \"PARAMETER10\",\n" + 
                    "    \"R\".\"PARAMETER11\"     \"PARAMETER11\",\n" + 
                    "    \"R\".\"PARAMETER12\"     \"PARAMETER12\",\n" + 
                    "    \"R\".\"PARAMETER13\"     \"PARAMETER13\",\n" + 
                    "    \"R\".\"PARAMETER14\"     \"PARAMETER14\",\n" + 
                    "    \"R\".\"PARAMETER15\"     \"PARAMETER15\",\n" + 
                    "    \"R\".\"PARAMETER16\"     \"PARAMETER16\",\n" + 
                    "    \"R\".\"PARAMETER17\"     \"PARAMETER17\",\n" + 
                    "    \"R\".\"PARAMETER18\"     \"PARAMETER18\",\n" + 
                    "    \"R\".\"PARAMETER19\"     \"PARAMETER19\",\n" + 
                    "    \"R\".\"PARAMETER20\"     \"PARAMETER20\"\n" + 
                    "FROM\n" + 
                    "    \"PAYROLL_TEST\".\"XXX_REPORT\" \"R\"\n" + 
                    "WHERE \"R\".ID=?";
            
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            rs.next();
                

                biReportBean.setId(rs.getInt("ID"));
                biReportBean.setParameter1(rs.getString("PARAMETER1"));
                biReportBean.setParameter2(rs.getString("PARAMETER2"));
                biReportBean.setParameter3(rs.getString("PARAMETER3"));
                biReportBean.setParameter4(rs.getString("PARAMETER4"));
                biReportBean.setParameter5(rs.getString("PARAMETER5"));
                biReportBean.setReportName(rs.getString("REPORT_NAME"));
                biReportBean.setReportTypeVal(rs.getString("REPORTTYPEVAL"));
                biReportBean.setParameter6(rs.getString("PARAMETER6"));
                biReportBean.setParameter7(rs.getString("PARAMETER7"));
                biReportBean.setParameter8(rs.getString("PARAMETER8"));
                biReportBean.setParameter9(rs.getString("PARAMETER9"));
                biReportBean.setParameter10(rs.getString("PARAMETER10"));
                biReportBean.setParameter11(rs.getString("PARAMETER11"));
                biReportBean.setParameter12(rs.getString("PARAMETER12"));
                biReportBean.setParameter13(rs.getString("PARAMETER13"));
                biReportBean.setParameter14(rs.getString("PARAMETER14"));
                biReportBean.setParameter15(rs.getString("PARAMETER15"));
                biReportBean.setParameter16(rs.getString("PARAMETER16"));
                biReportBean.setParameter17(rs.getString("PARAMETER17"));
                biReportBean.setParameter18(rs.getString("PARAMETER18"));
                biReportBean.setParameter19(rs.getString("PARAMETER19"));
                biReportBean.setParameter20(rs.getString("PARAMETER20"));
          
 

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return biReportBean;
    }

    public String deleteBIReportInfoById(int id) {

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "DELETE FROM payroll_test.XXX_REPORT \n" + 
            "WHERE id=?\n";
            
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ps.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return "BI Report Info Record Deleted Successfully";
    }

}
