package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.ElementEntriesBean;

import com.appspro.payroll.bean.ViewProcessResultBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class ViewProcessResultDAO extends AppsproConnection {


    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;
    CallableStatement cstmt = null;


    public  JSONObject getAllProcessResult() {
        JSONObject obj = new JSONObject();
        List<ViewProcessResultBean> viewProcessResultBeanList =
            new ArrayList<ViewProcessResultBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT distinct PPA.FLOW_NAME, PPA.TIME_PERIOD_NAME, PPA.ACTION_TYPE, PPA.ACTION_STATUS,\n" +
                    "PAA.ASSIGNMENT_ID, PAA.ACTION_STATUS AS \"LINE_STATUS\",\n" +
                    "PETF.ELEMENT_NAME,\n" +
                    "PIVF.DEFAULT_VALUE,\n" +
                    "PIVF.UOM,\n" +
                    "PIVF.BASE_NAME AS \"INPUT_VALUE_NAME\",\n" +
                    "PEEVF.SCREEN_ENTRY_VALUE AS \"INPUT_VALUE\"\n" +
                    "FROM PAY_PAYROLL_ACTIONS PPA,\n" +
                    "PAY_ASSIGNMENT_ACTIONS PAA,\n" +
                    "PAY_RUN_RESULTS PRR,\n" +
                    "PAY_ELEMENT_TYPES_F PETF,\n" +
                    "PAY_INPUT_VALUES_F PIVF,\n" +
                    "PAY_ELEMENT_ENTRIES_F PEEF,\n" +
                    "PAY_ELEMENT_ENTRY_VALUES_F PEEVF\n" +
                    "WHERE PPA.PAYROLL_ACTION_ID = PAA.PAYROLL_ACTION_ID\n" +
                    "AND PAA.ASSIGNMENT_ACTION_ID = PRR.ASSIGNMENT_ACTION_ID\n" +
                    "AND PRR.ELEMENT_TYPE_ID = PETF.ELEMENT_TYPE_ID\n" +
                    "AND PETF.ELEMENT_TYPE_ID = PIVF.ELEMENT_TYPE_ID\n" +
                    "AND PRR.ELEMENT_TYPE_ID = PIVF.ELEMENT_TYPE_ID\n" +
                    "AND PETF.ELEMENT_TYPE_ID = PEEF.ELEMENT_TYPE_ID\n" +
                    "AND PIVF.ELEMENT_TYPE_ID = PEEF.ELEMENT_TYPE_ID\n" +
                    "AND PRR.ELEMENT_TYPE_ID = PEEF.ELEMENT_TYPE_ID\n" +
                    "AND PEEF.ASSIGNMENT_ID = PAA.ASSIGNMENT_ID\n" +
                    "AND PEEVF.ELEMENT_ENTRY_ID = PEEF.ELEMENT_ENTRY_ID";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
           
            JSONArray employeeDetails = new JSONArray();
            
            while (rs.next()) {
                
                ViewProcessResultBean viewProcessResultBean =
                    new ViewProcessResultBean();
                viewProcessResultBean.setFlowName(rs.getString("FLOW_NAME"));
                viewProcessResultBean.setTimePeriodName(rs.getString("TIME_PERIOD_NAME"));
                viewProcessResultBean.setActionType(rs.getString("ACTION_TYPE"));
                viewProcessResultBean.setActionStatus(rs.getString("ACTION_STATUS"));
                viewProcessResultBean.setAssignmentId(rs.getString("ASSIGNMENT_ID"));
                viewProcessResultBean.setLineStatus(rs.getString("LINE_STATUS"));
                viewProcessResultBean.setElementName(rs.getString("ELEMENT_NAME"));
                viewProcessResultBean.setDefualtValue(rs.getString("DEFAULT_VALUE"));
                viewProcessResultBean.setUom(rs.getString("UOM"));
                viewProcessResultBean.setInputValueName(rs.getString("INPUT_VALUE_NAME"));
                viewProcessResultBean.setInputValue(rs.getString("INPUT_VALUE"));
                viewProcessResultBeanList.add(viewProcessResultBean);
            }
            obj.put("flowName", viewProcessResultBeanList.get(0).getFlowName());
            obj.put("timePeriodName", viewProcessResultBeanList.get(0).getTimePeriodName());
            obj.put("actionType", viewProcessResultBeanList.get(0).getActionType());
            obj.put("actionStatus", viewProcessResultBeanList.get(0).getActionStatus());
           
            String assigmentID = viewProcessResultBeanList.get(0).getAssignmentId(); 
            List<String> empList =  new ArrayList<String>();
            empList.add(assigmentID);
            for (int x=0;x<viewProcessResultBeanList.size();x++){
                if(!viewProcessResultBeanList.get(x).getAssignmentId().equalsIgnoreCase(assigmentID)&&
               empList.indexOf(viewProcessResultBeanList.get(x).getAssignmentId())==-1 ){
//                    JSONObject tempEmp = new JSONObject();
//                    tempEmp.put("assignmentId",viewProcessResultBeanList.get(x).getAssignmentId());
//                    tempEmp.put("lineStatus",viewProcessResultBeanList.get(x).getLineStatus());
//                    employeeDetails.put(tempEmp);
//                   
                    assigmentID = viewProcessResultBeanList.get(x).getAssignmentId();
                    empList.add(assigmentID);
                }
            }
            assigmentID=empList.get(0);
            JSONObject emp = new JSONObject();
            emp.put("assignmentId",viewProcessResultBeanList.get(0).getAssignmentId());
            emp.put("lineStatus",viewProcessResultBeanList.get(0).getLineStatus());
            JSONArray elementDetails = new JSONArray();
          //  employeeDetails.put(emp);
            for(int i = 0;i<empList.size();i++){
                if(!viewProcessResultBeanList.get(i).getAssignmentId().equalsIgnoreCase(assigmentID)){ 
                    emp=new JSONObject();
                    emp.put("assignmentId",viewProcessResultBeanList.get(i).getAssignmentId());
                    emp.put("lineStatus",viewProcessResultBeanList.get(i).getLineStatus());
                    elementDetails =  new JSONArray();
                    assigmentID =viewProcessResultBeanList.get(i).getAssignmentId();
                }


                List<String> elementList =  new ArrayList<String>();
//                 elementList.add(firstElement);
                for(int y =0 ; y<viewProcessResultBeanList.size();y++){
                    if(viewProcessResultBeanList.get(y).getAssignmentId().equalsIgnoreCase(assigmentID)
                    ){ 
                        if(elementList.indexOf(viewProcessResultBeanList.get(y).getElementName())==-1){
                            JSONObject elementObject = new JSONObject();
                            elementObject.put("elementName",viewProcessResultBeanList.get(y).getElementName() );
                          
                            elementList.add(viewProcessResultBeanList.get(y).getElementName());
                            JSONArray inputs = new JSONArray();
                            List<String> inputList =  new ArrayList<String>();
                            for(int z = 0 ;z<viewProcessResultBeanList.size();z++){
                                if(viewProcessResultBeanList.get(z).getAssignmentId().equalsIgnoreCase(assigmentID)){
                                           if(viewProcessResultBeanList.get(y).getElementName().equalsIgnoreCase(
                                           viewProcessResultBeanList.get(z).getElementName())){
                                               if(inputList.indexOf(viewProcessResultBeanList.get(z).getInputValueName())==-1){
                                               inputList.add(viewProcessResultBeanList.get(z).getInputValueName());
                                               JSONObject input = new JSONObject();
                                               input.put("defualtValue",viewProcessResultBeanList.get(z).getDefualtValue());
                                               input.put("uom",viewProcessResultBeanList.get(z).getUom());
                                               input.put("inputValueName",viewProcessResultBeanList.get(z).getInputValueName());
                                               input.put("inputValue",viewProcessResultBeanList.get(z).getInputValue());
                                               inputs.put(input);
                                               
                                           }
                                } 
                                           
                                       }
                            }
                            elementObject.put("inputs",inputs);
                            elementDetails.put(elementObject);                                                    
                        }                     
                                                                                                            
                    }
                }
                emp.put("elementDetails", elementDetails);
                employeeDetails.put(emp);
                
            }

            obj.put("employeeDetails",employeeDetails);
            System.out.println(obj);
            

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return obj;
    }
    public static void main(String[] args) {
        ViewProcessResultDAO ser = new ViewProcessResultDAO();
        ser.getAllProcessResult();
    }
}
