package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.payroll.bean.LookupBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class LookupDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public List<LookupBean> getLookup() {
        List<LookupBean> lookup = new ArrayList<LookupBean>();
        connection = AppsproConnection.getConnection();

        String query = null;
        query = "SELECT * FROM XXX_PAAS_LOOKUP";
        System.out.println("\nExecuting query: " + query);
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                LookupBean lookupBean = new LookupBean();
                lookupBean.setId(rs.getInt("ID"));
                lookupBean.setName(rs.getString("NAME"));
                lookupBean.setCode(rs.getString("CODE"));
                lookupBean.setValueEn(rs.getString("VALUE_EN"));
                lookupBean.setValueAr(rs.getString("VALUE_AR"));
                lookup.add(lookupBean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return lookup;
    }

}
