package com.appspro.payroll.dao;
import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.FastFormulaBean;
import common.restHelper.RestHelper;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class FastFormulaDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    FormulaParameterDAO pramService = new FormulaParameterDAO();
    public List<FastFormulaBean> getFastFormula() {
        List<FastFormulaBean> list = new ArrayList<FastFormulaBean>();
        connection = AppsproConnection.getConnection();
        String query = "SELECT         \n" + 
        "        FF_FORMULAS_F.FORMULA_ID FORMULA_ID,        \n" + 
        "        FF_FORMULAS_F.EFFECTIVE_START_DATE EFFECTIVE_START_DATE,   \n" + 
        "        FF_FORMULAS_F.EFFECTIVE_END_DATE EFFECTIVE_END_DATE,  \n" + 
        "        FF_FORMULAS_F.FORMULA_TYPE_ID FORMULA_TYPE_ID, \n" + 
        "        FF_FORMULAS_F.FORMULA_NAME FORMULA_NAME, \n" + 
        "        FF_FORMULAS_F.FORMULA_TEXT FORMULA_TEXT,         \n" + 
        "        FF_FORMULAS_F.COMPILE_FLAG COMPILE_FLAG,         \n" + 
        "        FF_FORMULAS_F.ENTERPRISE_ID ENTERPRISE_ID,         \n" + 
        "        FF_FORMULAS_F.LEGISLATIVE_DATA_GROUP_ID LEGISLATIVE_DATA_GROUP_ID,       \n" + 
        "        FF_FORMULAS_F.LEGISLATION_CODE LEGISLATION_CODE,       \n" + 
        "        FF_FORMULAS_F.OBJECT_VERSION_NUMBER OBJECT_VERSION_NUMBER,     \n" + 
        "        FF_FORMULAS_F.LAST_UPDATE_DATE LAST_UPDATE_DATE,    \n" + 
        "        FF_FORMULAS_F.LAST_UPDATED_BY LAST_UPDATED_BY,   \n" + 
        "        FF_FORMULAS_F.LAST_UPDATE_LOGIN LAST_UPDATE_LOGIN,        \n" + 
        "        FF_FORMULAS_F.CREATED_BY CREATED_BY,      \n" + 
        "        FF_FORMULAS_F.CREATION_DATE CREATION_DATE,       \n" + 
        "        FF_FORMULAS_F.MODULE_ID MODULE_ID         \n" + 
        "        FROM         \n" + 
        "        FF_FORMULAS_F \n" + 
        "		where to_date(to_char(sysdate,'dd-mm-yyyy') ,'dd-MM-yyyy') between to_date( EFFECTIVE_START_DATE, 'dd-MM-yyyy' )\n" + 
        "and to_date( EFFECTIVE_END_DATE, 'dd-MM-yyyy' ) ";
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                FastFormulaBean obj = new FastFormulaBean();
                obj.setFormula_id(rs.getString("FORMULA_ID"));
                obj.setEffective_start_date(rs.getString("EFFECTIVE_START_DATE"));
                obj.setEffective_end_date(rs.getString("EFFECTIVE_END_DATE"));
                obj.setFormula_type_id(rs.getString("FORMULA_TYPE_ID"));
                obj.setFormula_name(rs.getString("FORMULA_NAME"));
                obj.setFormula_text(rs.getString("FORMULA_TEXT"));
                obj.setCompile_flag(rs.getString("COMPILE_FLAG"));
                obj.setEnterprise_id(rs.getString("ENTERPRISE_ID"));
                obj.setLegislative_data_group_id(rs.getString("LEGISLATIVE_DATA_GROUP_ID"));
                obj.setLegislation_code(rs.getString("LEGISLATION_CODE"));
                obj.setObject_version_number(rs.getString("OBJECT_VERSION_NUMBER"));
                obj.setLast_update_date(rs.getString("LAST_UPDATE_DATE"));
                obj.setLast_updated_by(rs.getString("LAST_UPDATED_BY"));
                obj.setLast_update_login(rs.getString("LAST_UPDATE_LOGIN"));
                obj.setCreated_by(rs.getString("CREATED_BY"));
                obj.setCreation_date(rs.getString("CREATION_DATE"));
                obj.setModule_id(rs.getString("MODULE_ID"));
                obj.setParameteList(pramService.getFastFormulaPram(rs.getString("FORMULA_ID")));
                list.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
        
    }
    public String addFastFormula(FastFormulaBean bean) throws JSONException,
                                                                        SQLException,
                                                              ParseException {
       JSONObject jObject = new JSONObject();
       String result = "";
        connection = AppsproConnection.getConnection();
       String sql_select = "{call FF_FORMULAS_F_PRO_V2(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)}";
       try  {
           SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
           Date date ; 
           Date enddate;
           String startDate = "";
           String endDate = "";
            if(bean.getEffective_start_date() != null && ! bean.getEffective_start_date().isEmpty()){
                date = sdf.parse(bean.getEffective_start_date());
                sdf = new SimpleDateFormat("dd-MM-yyyy");
                startDate = sdf.format(date);
           } 
           if(bean.getEffective_end_date() != null && ! bean.getEffective_end_date().isEmpty()){
               sdf = new SimpleDateFormat("dd-MM-yyyy");
                enddate = sdf.parse(bean.getEffective_end_date());
               sdf = new SimpleDateFormat("dd-MM-yyyy");
                endDate = sdf.format(enddate);
           }else{
               endDate = "31-12-4721";
           }          
           CallableStatement callBleSt = connection.prepareCall(sql_select);
               callBleSt.setString(1, startDate);
               callBleSt.setString(2,endDate);
               callBleSt.setString(3, bean.getFormula_type_id());
               callBleSt.setString(4, bean.getFormula_name());
               callBleSt.setString(5, bean.getFormula_text());
               callBleSt.setString(6, bean.getCompile_flag());
               callBleSt.setString(7, bean.getEnterprise_id());
               callBleSt.setString(8, bean.getLegislative_data_group_id());
               callBleSt.setString(9, bean.getLegislation_code());
               callBleSt.setString(10, bean.getObject_version_number());
               callBleSt.setString(11, bean.getLast_update_date());
               callBleSt.setString(12, bean.getLast_updated_by());
               callBleSt.setString(13, bean.getLast_update_login());
               callBleSt.setString(14, bean.getCreated_by());
               callBleSt.setString(15, bean.getCreation_date());
               callBleSt.setString(16, bean.getModule_id());
              callBleSt.registerOutParameter(17,Types.INTEGER);
          

           
                 callBleSt.executeUpdate();         
           String fourmlaId=  Integer.toString(callBleSt.getInt(17)) ;
           
              
              pramService.addelFourmlaPram(bean.getParameteList(),fourmlaId);
             
               // execute every 100 rows or less

           
    //            System.out.println(i + " records inserted");

           jObject.put("Status", "Inserted");

    //            System.out.println(JSONOutput);
       } catch (SQLException e) {
           e.printStackTrace();
           System.out.println("Noor Eror");
           System.out.println(e.getMessage());
           jObject.put("Status", "ERROR");
           jObject.put("ErrorCode", e.getMessage());
           result = jObject.toString();
       } finally {
           closeResources(connection, ps, rs);
       }

       return jObject.toString();
    }
    public String updateFastFormula(FastFormulaBean bean) throws JSONException,
                                                                        SQLException,
                                                                 ParseException {
       JSONObject jObject = new JSONObject();
       String result = "";
        connection = AppsproConnection.getConnection();
       String sql_select = "{call FF_FORMULAS_F_PRO_UPDATE(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)}";
       try  {
           SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
           Date date ; 
           Date enddate;
           String startDate = "";
           String endDate = "";
            if(bean.getEffective_start_date() != null && ! bean.getEffective_start_date().isEmpty()){
                date = sdf.parse(bean.getEffective_start_date());
                sdf = new SimpleDateFormat("dd-MM-yyyy");
                startDate = sdf.format(date);
           } 
           if(bean.getEffective_end_date() != null && ! bean.getEffective_end_date().isEmpty()){
               sdf = new SimpleDateFormat("dd-MM-yyyy");
                enddate = sdf.parse(bean.getEffective_end_date());
               sdf = new SimpleDateFormat("dd-MM-yyyy");
                endDate = sdf.format(enddate);
           }else{
               endDate = "31-12-4721";
           }
           CallableStatement callBleSt = connection.prepareCall(sql_select);
               callBleSt.setString(1,startDate);
               callBleSt.setString(2, endDate);
               callBleSt.setString(3, bean.getFormula_type_id());
               callBleSt.setString(4, bean.getFormula_name());
               callBleSt.setString(5, bean.getFormula_text());
               callBleSt.setString(6, bean.getCompile_flag());
               callBleSt.setString(7, bean.getEnterprise_id());
               callBleSt.setString(8, bean.getLegislative_data_group_id());
               callBleSt.setString(9, bean.getLegislation_code());
               callBleSt.setString(10, bean.getObject_version_number());
               callBleSt.setString(11, bean.getLast_update_date());
               callBleSt.setString(12, bean.getLast_updated_by());
               callBleSt.setString(13, bean.getLast_update_login());
               callBleSt.setString(14, bean.getCreated_by());
               callBleSt.setString(15, bean.getCreation_date());
               callBleSt.setString(16, bean.getModule_id());
               callBleSt.setString(17, bean.getFormula_id());
              callBleSt.registerOutParameter(18,Types.INTEGER);
           
               callBleSt.executeUpdate();
           String fourmlaId=  Integer.toString(callBleSt.getInt(18)) ;
              pramService.deleteFastFormulaPram(bean);
             pramService.addelFourmlaPram(bean.getParameteList(),fourmlaId);

           jObject.put("Status", "Inserted");

    //            System.out.println(JSONOutput);
       } catch (SQLException e) {
           e.printStackTrace();
           System.out.println("Noor Eror");
           System.out.println(e.getMessage());
           jObject.put("Status", "ERROR");
           jObject.put("ErrorCode", e.getMessage());
           result = jObject.toString();
       } finally {
           closeResources(connection, ps, rs);
       }

       return jObject.toString();
    }
    public String deleteFastFormula(FastFormulaBean bean) throws JSONException,
                                                                        SQLException {
       JSONObject jObject = new JSONObject();
       String result = "";
        connection = AppsproConnection.getConnection();
       String sql_select = "{call FF_FORMULAS_F_PRO_Delete(?)}";
       try  {
           ps = connection.prepareStatement(sql_select);
               ps.setString(1, bean.getFormula_id());
           
               ps.executeUpdate();
             
               // execute every 100 rows or less

           
    //            System.out.println(i + " records inserted");

           jObject.put("Status", "Deleted");

    //            System.out.println(JSONOutput);
       } catch (SQLException e) {
           e.printStackTrace();
           System.out.println("Noor Eror");
           System.out.println(e.getMessage());
           jObject.put("Status", "ERROR");
           jObject.put("ErrorCode", e.getMessage());
           result = jObject.toString();
       } finally {
           closeResources(connection, ps, rs);
       }

       return jObject.toString();
    }

    public String compileFastFormula(FastFormulaBean bean) throws JSONException,
                                                                 SQLException {
        JSONObject jObject = new JSONObject();
        connection = AppsproConnection.getConnection();
        String sql_select = bean.getFormula_text();
        try  {
            ps = connection.prepareStatement(sql_select);
            int i =     ps.executeUpdate();
            System.out.println(i);
            jObject.put("Status", "COMPILED");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Noor Eror");
            System.out.println(e.getMessage());
            jObject.put("Status", "ERROR");
            jObject.put("ErrorCode", e.getMessage());
        } finally {
            closeResources(connection, ps, rs);
        }
        
      return jObject.toString();
    }
    
}
