package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;



import common.restHelper.RestHelper;
import static common.restHelper.RestHelper.getSchema_Name;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.appspro.payroll.bean.DynamicGetBean;
import com.appspro.payroll.bean.DynamicGetSup;

public class DynamicApi extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    //SELECT * FROM DYNAMIC_GET_API
    // where GET_CODE = 'finBusinessUnitsLOV'
    public ArrayList<DynamicGetBean> getDymamic(String GET_CODE) {
        ArrayList<DynamicGetBean> approvalSetupList = new ArrayList<DynamicGetBean>();
        DynamicGetBean bean = null;
        connection = AppsproConnection.getConnection();
        String query = null;
        query = "SELECT * FROM  "+ " " + getSchema_Name() + ".DYNAMIC_GET_API\n"
                + " WHERE GET_CODE = ? ";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, GET_CODE.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new DynamicGetBean();
                bean.setID(rs.getString("ID"));
                bean.setGET_CODE(rs.getString("GET_CODE"));
                bean.setGET_PATH(rs.getString("GET_PATH"));
                bean.setRESPONSE_MAIN(rs.getString("RESPONSE_MAIN_1"));
                bean.setRESONSE_SUP_1(rs.getString("RESONSE_SUP_1"));
                bean.setRESONSE_SUP_2(rs.getString("RESONSE_SUP_2"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }
    public ArrayList<DynamicGetSup> getDymamicSup(String GET_CODE) {
        ArrayList<DynamicGetSup> approvalSetupList = new ArrayList<DynamicGetSup>();
        DynamicGetSup bean = null;
        connection = AppsproConnection.getConnection();
        String query = null;
        query = "SELECT * FROM  "+ " " + getSchema_Name() + ".DYNAMIC_GET_API_SUP\n"
                + " WHERE GET_CODE = ? ";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, GET_CODE.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new DynamicGetSup();
                bean.setId(rs.getString("ID"));
                bean.setGET_CODE(rs.getString("GET_CODE"));
                bean.setGET_ID(rs.getString("GET_ID"));
                bean.setSUP_NAME(rs.getString("SUP_NAME"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }
}