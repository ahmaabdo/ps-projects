package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.ElementCostingBean;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;

import static common.restHelper.RestHelper.getSchema_Name;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ElementCostingDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;

    public Integer getElmentTypeIdByElmentName(String elmentName) {
        try {

            connection = AppsproConnection.getConnection();
            CallableStatement stmtGetElementTypeId =
                connection.prepareCall("{call " + getSchema_Name() +
                                       ".GET_ELEMENT_TYPE_ID_BY_NAME(?,?)}");

            stmtGetElementTypeId.setString(1, elmentName);
            stmtGetElementTypeId.registerOutParameter(2,
                                                      java.sql.Types.INTEGER);
            stmtGetElementTypeId.executeUpdate();
            return stmtGetElementTypeId.getInt(2);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ElementCostingBean> callInsertCostingProcedure(ElementCostingBean elementCostingBean) {
        List<ElementCostingBean> elementCostingList =
            new ArrayList<ElementCostingBean>();
        try {

            connection = AppsproConnection.getConnection();

            CallableStatement stmtInsert =
                connection.prepareCall("{call " + getSchema_Name() +
                                       ".INSERT_ELEMENT_COSTING(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            stmtInsert.setString(1,
                                 elementCostingBean.getEffectiveStartDate());
            stmtInsert.setString(2, elementCostingBean.getEffectiveEndDate());
            stmtInsert.setString(3, elementCostingBean.getBusinessGroupId());
            stmtInsert.setString(4, elementCostingBean.getElementTypeId());
            stmtInsert.setString(5, elementCostingBean.getPayrollId());
            stmtInsert.setString(6, elementCostingBean.getJobId());
            stmtInsert.setString(7, elementCostingBean.getPositionId());
            stmtInsert.setString(8, elementCostingBean.getPeopleGroupId());
            stmtInsert.setString(9, elementCostingBean.getOrganizationId());
            stmtInsert.setString(10, elementCostingBean.getLocationId());
            stmtInsert.setString(11, elementCostingBean.getGradeId());
            stmtInsert.setString(12, elementCostingBean.getTransferToGLFlag());
            stmtInsert.setString(13, elementCostingBean.getCostableType());
            stmtInsert.setString(14, elementCostingBean.getCostSegment1());
            stmtInsert.setString(15, elementCostingBean.getCostSegment2());
            stmtInsert.setString(16, elementCostingBean.getCostSegment3());
            stmtInsert.setString(17, elementCostingBean.getCostSegment4());
            stmtInsert.setString(18, elementCostingBean.getCostSegment5());
            stmtInsert.setString(19, elementCostingBean.getCostSegment6());
            stmtInsert.setString(20, elementCostingBean.getCostSegment7());
            stmtInsert.setString(21, elementCostingBean.getCostSegment8());
            stmtInsert.setString(22, elementCostingBean.getCostSegment9());
            stmtInsert.setString(23, elementCostingBean.getCostSegment10());
            stmtInsert.setString(24, elementCostingBean.getBalanceSegment1());
            stmtInsert.setString(25, elementCostingBean.getBalanceSegment2());
            stmtInsert.setString(26, elementCostingBean.getBalanceSegment3());
            stmtInsert.setString(27, elementCostingBean.getBalanceSegment4());
            stmtInsert.setString(28, elementCostingBean.getBalanceSegment5());
            stmtInsert.setString(29, elementCostingBean.getBalanceSegment6());
            stmtInsert.setString(30, elementCostingBean.getBalanceSegment7());
            stmtInsert.setString(31, elementCostingBean.getBalanceSegment8());
            stmtInsert.setString(32, elementCostingBean.getBalanceSegment9());
            stmtInsert.setString(33, elementCostingBean.getBalanceSegment10());
            stmtInsert.registerOutParameter(34, java.sql.Types.INTEGER);

            stmtInsert.executeUpdate();


            elementCostingBean.setElementCostId(stmtInsert.getInt(34));
            elementCostingList.add(elementCostingBean);
            return elementCostingList;


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementCostingList;
    }

    public List<ElementCostingBean> insertElementCosting(String elementCostingData) {

        List<ElementCostingBean> elementCostingList =
            new ArrayList<ElementCostingBean>();

        try {

            ElementCostingBean elementCostingBean = new ElementCostingBean();
            JSONObject json = new JSONObject(elementCostingData);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                elementCostingBean.setEffectiveEndDate(endDate);
            } else {
                elementCostingBean.setEffectiveEndDate("31-Dec-4721");
            }
            elementCostingBean.setEffectiveStartDate(startDate);
            elementCostingBean.setBusinessGroupId(json.optString("businessGroupId"));
            elementCostingBean.setElementTypeId(json.optString("elementTypeId"));
            elementCostingBean.setPayrollId(json.optString("payrollId"));
            elementCostingBean.setJobId(json.optString("jobId"));
            elementCostingBean.setPositionId(json.optString("positionId"));
            elementCostingBean.setPeopleGroupId(json.optString("peopleGroupId"));
            elementCostingBean.setOrganizationId(json.optString("organizationId"));
            elementCostingBean.setLocationId(json.optString("locationId"));
            elementCostingBean.setGradeId(json.optString("gradeId"));
            elementCostingBean.setTransferToGLFlag(json.optString("transferToGLFlag"));
            elementCostingBean.setCostableType(json.optString("costableType"));
            elementCostingBean.setCostSegment1(json.optString("costSegment1"));
            elementCostingBean.setCostSegment2(json.optString("costSegment2"));
            elementCostingBean.setCostSegment3(json.optString("costSegment3"));
            elementCostingBean.setCostSegment4(json.optString("costSegment4"));
            elementCostingBean.setCostSegment5(json.optString("costSegment5"));
            elementCostingBean.setCostSegment6(json.optString("costSegment6"));
            elementCostingBean.setCostSegment7(json.optString("costSegment7"));
            elementCostingBean.setCostSegment8(json.optString("costSegment8"));
            elementCostingBean.setCostSegment9(json.optString("costSegment9"));
            elementCostingBean.setCostSegment10(json.optString("costSegment10"));
            elementCostingBean.setBalanceSegment1(json.optString("balanceSegment1"));
            elementCostingBean.setBalanceSegment2(json.optString("balanceSegment2"));
            elementCostingBean.setBalanceSegment3(json.optString("balanceSegment3"));
            elementCostingBean.setBalanceSegment4(json.optString("balanceSegment4"));
            elementCostingBean.setBalanceSegment5(json.optString("balanceSegment5"));
            elementCostingBean.setBalanceSegment6(json.optString("balanceSegment6"));
            elementCostingBean.setBalanceSegment7(json.optString("balanceSegment7"));
            elementCostingBean.setBalanceSegment8(json.optString("balanceSegment8"));
            elementCostingBean.setBalanceSegment9(json.optString("balanceSegment9"));
            elementCostingBean.setBalanceSegment10(json.optString("balanceSegment10"));

            elementCostingList =
                    callInsertCostingProcedure(elementCostingBean);

            return elementCostingList;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return elementCostingList;
    }


    public List<ElementCostingBean> updateProcedure(String elementEligibilityData, int id) {

        List<ElementCostingBean> ElementCostingBeanList =
            new ArrayList<ElementCostingBean>();
        try {
            ElementCostingBean elementCostingBean = new ElementCostingBean();
            JSONObject json = new JSONObject(elementEligibilityData);

            elementCostingBean.setElementCostId(json.optInt("elementCostId"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                elementCostingBean.setEffectiveEndDate(endDate);
            } else {
                elementCostingBean.setEffectiveEndDate("");
            }

            elementCostingBean.setEffectiveStartDate(startDate);
            elementCostingBean.setBusinessGroupId(json.optString("businessGroupId"));
            elementCostingBean.setElementTypeId(json.optString("elementTypeId"));
            elementCostingBean.setPayrollId(json.optString("payrollId"));
            elementCostingBean.setJobId(json.optString("jobId"));
            elementCostingBean.setPositionId(json.optString("positionId"));
            elementCostingBean.setPeopleGroupId(json.optString("peopleGroupId"));
            elementCostingBean.setOrganizationId(json.optString("organizationId"));
            elementCostingBean.setLocationId(json.optString("locationId"));
            elementCostingBean.setGradeId(json.optString("gradeId"));
            elementCostingBean.setTransferToGLFlag(json.optString("transferToGLFlag"));
            elementCostingBean.setCostableType(json.optString("costableType"));
            elementCostingBean.setCostSegment1(json.optString("costSegment1"));
            elementCostingBean.setCostSegment2(json.optString("costSegment2"));
            elementCostingBean.setCostSegment3(json.optString("costSegment3"));
            elementCostingBean.setCostSegment4(json.optString("costSegment4"));
            elementCostingBean.setCostSegment5(json.optString("costSegment5"));
            elementCostingBean.setCostSegment6(json.optString("costSegment6"));
            elementCostingBean.setCostSegment7(json.optString("costSegment7"));
            elementCostingBean.setCostSegment8(json.optString("costSegment8"));
            elementCostingBean.setCostSegment9(json.optString("costSegment9"));
            elementCostingBean.setCostSegment10(json.optString("costSegment10"));
            elementCostingBean.setBalanceSegment1(json.optString("balanceSegment1"));
            elementCostingBean.setBalanceSegment2(json.optString("balanceSegment2"));
            elementCostingBean.setBalanceSegment3(json.optString("balanceSegment3"));
            elementCostingBean.setBalanceSegment4(json.optString("balanceSegment4"));
            elementCostingBean.setBalanceSegment5(json.optString("balanceSegment5"));
            elementCostingBean.setBalanceSegment6(json.optString("balanceSegment6"));
            elementCostingBean.setBalanceSegment7(json.optString("balanceSegment7"));
            elementCostingBean.setBalanceSegment8(json.optString("balanceSegment8"));
            elementCostingBean.setBalanceSegment9(json.optString("balanceSegment9"));
            elementCostingBean.setBalanceSegment10(json.optString("balanceSegment10"));

            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call UPDATE_ELEMENT_COSTING(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

            CallableStatement cstmt = null;
            cstmt = connection.prepareCall(query);
            cstmt.setInt(1,id);
            cstmt.setString(2, elementCostingBean.getEffectiveStartDate());
            cstmt.setString(3, elementCostingBean.getEffectiveEndDate());
            cstmt.setString(4, elementCostingBean.getBusinessGroupId());
            cstmt.setString(5, elementCostingBean.getElementTypeId());
            cstmt.setString(6, elementCostingBean.getPayrollId());
            cstmt.setString(7, elementCostingBean.getJobId());
            cstmt.setString(8, elementCostingBean.getPositionId());
            cstmt.setString(9, elementCostingBean.getPeopleGroupId());
            cstmt.setString(10, elementCostingBean.getOrganizationId());
            cstmt.setString(11, elementCostingBean.getLocationId());
            cstmt.setString(12, elementCostingBean.getGradeId());
            cstmt.setString(13, elementCostingBean.getTransferToGLFlag());
            cstmt.setString(14, elementCostingBean.getCostableType());

            cstmt.setString(15, elementCostingBean.getCostSegment1());
            cstmt.setString(16, elementCostingBean.getCostSegment2());
            cstmt.setString(17, elementCostingBean.getCostSegment3());
            cstmt.setString(18, elementCostingBean.getCostSegment4());
            cstmt.setString(19, elementCostingBean.getCostSegment5());
            cstmt.setString(20, elementCostingBean.getCostSegment6());
            cstmt.setString(21, elementCostingBean.getCostSegment7());
            cstmt.setString(22, elementCostingBean.getCostSegment8());
            cstmt.setString(23, elementCostingBean.getCostSegment9());
            cstmt.setString(24, elementCostingBean.getCostSegment10());
            cstmt.setString(25, elementCostingBean.getBalanceSegment1());
            cstmt.setString(26, elementCostingBean.getBalanceSegment2());
            cstmt.setString(27, elementCostingBean.getBalanceSegment3());
            cstmt.setString(28, elementCostingBean.getBalanceSegment4());
            cstmt.setString(29, elementCostingBean.getBalanceSegment5());
            cstmt.setString(30, elementCostingBean.getBalanceSegment6());
            cstmt.setString(31, elementCostingBean.getBalanceSegment7());
            cstmt.setString(32, elementCostingBean.getBalanceSegment8());
            cstmt.setString(33, elementCostingBean.getBalanceSegment9());
            cstmt.setString(34, elementCostingBean.getBalanceSegment10());

            cstmt.registerOutParameter(35, oracle.jdbc.OracleTypes.NUMBER);
            cstmt.executeUpdate();

            elementCostingBean.setElementCostId(cstmt.getInt(35));
            elementCostingBean.getElementTypeId();
            ElementCostingBeanList.add(elementCostingBean);

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return ElementCostingBeanList;
    }


    public List<ElementCostingBean> getAllElementCosting() {

        List<ElementCostingBean> elementCostingBeanList =
            new ArrayList<ElementCostingBean>();

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "SELECT * FROM " + getSchema_Name() + ".PAY_ELEMENT_COSTING WHERE to_date(\"EFFECTIVE_END_DATE\",'dd-MM-yyyy') > to_date(SYSDATE, 'dd-MM-yyyy')";
            
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                ElementCostingBean elementCostingBean =
                    new ElementCostingBean();

                elementCostingBean.setElementCostId(rs.getInt("ELEMENT_COST_ID"));
                elementCostingBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementCostingBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementCostingBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementCostingBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementCostingBean.setPayrollId(rs.getString("PAYROLL_ID"));
                elementCostingBean.setJobId(rs.getString("JOB_ID"));
                elementCostingBean.setPositionId(rs.getString("POSITION_ID"));
                elementCostingBean.setPeopleGroupId(rs.getString("PEOPLE_GROUP_ID"));
                elementCostingBean.setOrganizationId(rs.getString("ORGANIZATION_ID"));
                elementCostingBean.setLocationId(rs.getString("LOCATION_ID"));
                elementCostingBean.setGradeId(rs.getString("GRADE_ID"));
                elementCostingBean.setTransferToGLFlag(rs.getString("TRANSFER_TO_GL_FLAG"));
                elementCostingBean.setCostableType(rs.getString("COSTABLE_TYPE"));
                elementCostingBean.setCostSegment1(rs.getString("COST_SEGMENT1"));
                elementCostingBean.setCostSegment2(rs.getString("COST_SEGMENT2"));
                elementCostingBean.setCostSegment3(rs.getString("COST_SEGMENT3"));
                elementCostingBean.setCostSegment4(rs.getString("COST_SEGMENT4"));
                elementCostingBean.setCostSegment5(rs.getString("COST_SEGMENT5"));
                elementCostingBean.setCostSegment6(rs.getString("COST_SEGMENT6"));
                elementCostingBean.setCostSegment7(rs.getString("COST_SEGMENT7"));
                elementCostingBean.setCostSegment8(rs.getString("COST_SEGMENT8"));
                elementCostingBean.setCostSegment9(rs.getString("COST_SEGMENT9"));
                elementCostingBean.setCostSegment10(rs.getString("COST_SEGMENT10"));
                elementCostingBean.setBalanceSegment1(rs.getString("BALANCE_SEGMENT1"));
                elementCostingBean.setBalanceSegment2(rs.getString("BALANCE_SEGMENT2"));
                elementCostingBean.setBalanceSegment3(rs.getString("BALANCE_SEGMENT3"));
                elementCostingBean.setBalanceSegment4(rs.getString("BALANCE_SEGMENT4"));
                elementCostingBean.setBalanceSegment5(rs.getString("BALANCE_SEGMENT5"));
                elementCostingBean.setBalanceSegment6(rs.getString("BALANCE_SEGMENT6"));
                elementCostingBean.setBalanceSegment7(rs.getString("BALANCE_SEGMENT7"));
                elementCostingBean.setBalanceSegment8(rs.getString("BALANCE_SEGMENT8"));
                elementCostingBean.setBalanceSegment9(rs.getString("BALANCE_SEGMENT9"));
                elementCostingBean.setBalanceSegment10(rs.getString("BALANCE_SEGMENT10"));

                elementCostingBeanList.add(elementCostingBean);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementCostingBeanList;
    }

    public List<ElementCostingBean> getElementCostingById(int id) {

        List<ElementCostingBean> elementCostingBeanList =
            new ArrayList<ElementCostingBean>();

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "SELECT * FROM " + getSchema_Name() + ".PAY_ELEMENT_COSTING WHERE ELEMENT_COST_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                ElementCostingBean elementCostingBean = new ElementCostingBean();
                
                elementCostingBean.setElementCostId(rs.getInt("ELEMENT_COST_ID"));
                elementCostingBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementCostingBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementCostingBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementCostingBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                elementCostingBean.setPayrollId(rs.getString("PAYROLL_ID"));
                elementCostingBean.setJobId(rs.getString("JOB_ID"));
                elementCostingBean.setPositionId(rs.getString("POSITION_ID"));
                elementCostingBean.setPeopleGroupId(rs.getString("PEOPLE_GROUP_ID"));
                elementCostingBean.setOrganizationId(rs.getString("ORGANIZATION_ID"));
                elementCostingBean.setLocationId(rs.getString("LOCATION_ID"));
                elementCostingBean.setGradeId(rs.getString("GRADE_ID"));
                elementCostingBean.setTransferToGLFlag(rs.getString("TRANSFER_TO_GL_FLAG"));
                elementCostingBean.setCostableType(rs.getString("COSTABLE_TYPE"));
                elementCostingBean.setCostSegment1(rs.getString("COST_SEGMENT1"));
                elementCostingBean.setCostSegment2(rs.getString("COST_SEGMENT2"));
                elementCostingBean.setCostSegment3(rs.getString("COST_SEGMENT3"));
                elementCostingBean.setCostSegment4(rs.getString("COST_SEGMENT4"));
                elementCostingBean.setCostSegment5(rs.getString("COST_SEGMENT5"));
                elementCostingBean.setCostSegment6(rs.getString("COST_SEGMENT6"));
                elementCostingBean.setCostSegment7(rs.getString("COST_SEGMENT7"));
                elementCostingBean.setCostSegment8(rs.getString("COST_SEGMENT8"));
                elementCostingBean.setCostSegment9(rs.getString("COST_SEGMENT9"));
                elementCostingBean.setCostSegment10(rs.getString("COST_SEGMENT10"));
                elementCostingBean.setBalanceSegment1(rs.getString("BALANCE_SEGMENT1"));
                elementCostingBean.setBalanceSegment2(rs.getString("BALANCE_SEGMENT2"));
                elementCostingBean.setBalanceSegment3(rs.getString("BALANCE_SEGMENT3"));
                elementCostingBean.setBalanceSegment4(rs.getString("BALANCE_SEGMENT4"));
                elementCostingBean.setBalanceSegment5(rs.getString("BALANCE_SEGMENT5"));
                elementCostingBean.setBalanceSegment6(rs.getString("BALANCE_SEGMENT6"));
                elementCostingBean.setBalanceSegment7(rs.getString("BALANCE_SEGMENT7"));
                elementCostingBean.setBalanceSegment8(rs.getString("BALANCE_SEGMENT8"));
                elementCostingBean.setBalanceSegment9(rs.getString("BALANCE_SEGMENT9"));
                elementCostingBean.setBalanceSegment10(rs.getString("BALANCE_SEGMENT10"));

                elementCostingBeanList.add(elementCostingBean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementCostingBeanList;
    }

    public String deleteElementCostingById(int id) {


        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call " + getSchema_Name() + ".DELETE_ELEMENT_COSTING_BY_ID(?)}";

            ps = connection.prepareStatement(query);
            ps.setInt(1, id);

            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return "Successful removed data";
    }

    
    public String getElementCostingSegmentsFromSaaS() {
        BIReportModel biRreportModel = new BIReportModel();
        try {
            JSONObject json =
                biRreportModel.runReportWithoutParams(BIReportModel.REPORT_NAME.ChartsOfAccsSegments.getValue());
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
