package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.ElementFormBean;

import common.biPReports.BIReportModel;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Types;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.json.JSONTokener;

import org.springframework.dao.DataIntegrityViolationException;

public class ElementEnrtyFormDAO extends AppsproConnection {


    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;
    CallableStatement cstmt = null;

    public List<ElementFormBean> insertProcedure(String insertElement) {

        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        List<ElementFormBean> elementFormBeanListValidate =
            new ArrayList<ElementFormBean>();
        try {
            ElementFormBean insertElementBean = new ElementFormBean();
            JSONObject json = new JSONObject(insertElement);
            insertElementBean.setElementName(json.optString("elementName"));
            insertElementBean.setElementType(json.optString("elementType"));
            insertElementBean.setProcessingType(json.optString("processingType"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                insertElementBean.setEffectiveEndDate(endDate);
            } else {
                insertElementBean.setEffectiveEndDate("31-Dec-4721");
            }
            insertElementBean.setEffectiveStartDate(startDate);

            insertElementBean.setPriority(json.optString("priority"));
            insertElementBean.setCurrency(json.optString("currency"));

            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call INSERT_ELEMENT(?,?,?,?,?,?,?,?)}";
            System.out.println("\nExecuting query: " + query);

            cstmt = connection.prepareCall(query);
            cstmt.setString(1, insertElementBean.getElementName());
            cstmt.setString(2, insertElementBean.getElementType());
            cstmt.setString(3, insertElementBean.getProcessingType());
            cstmt.setString(4, insertElementBean.getEffectiveStartDate());
            cstmt.setString(5, insertElementBean.getEffectiveEndDate());
            cstmt.setString(6, insertElementBean.getPriority());
            cstmt.setString(7, insertElementBean.getCurrency());
            cstmt.registerOutParameter(8, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            insertElementBean.setElementTypeId(cstmt.getInt(8));
            insertElementBean.getElementTypeId();
            elementFormBeanList.add(insertElementBean);
        } catch (SQLIntegrityConstraintViolationException e) {
            ElementFormBean insertElementBean = new ElementFormBean();
            insertElementBean.setCheck("The Element Name Already Exist");
            elementFormBeanListValidate.add(insertElementBean);
            return elementFormBeanListValidate;

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public List<ElementFormBean> updateProcedure(int id,
                                                 String updateElement) {

        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            ElementFormBean updateElementBean = new ElementFormBean();
            JSONObject json = new JSONObject(updateElement);
            updateElementBean.setElementName(json.optString("elementName"));
            updateElementBean.setElementType(json.optString("elementType"));
            updateElementBean.setProcessingType(json.optString("processingType"));
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveStartDate = json.optString("effectiveStartDate");
            if (!effectiveStartDate.isEmpty()) {
                Date startDateEff = sdf.parse(effectiveStartDate);
                sdf = new SimpleDateFormat("dd-MMM-yyyy");
                String startDate = sdf.format(startDateEff);
                updateElementBean.setEffectiveStartDate(startDate);
            } else {
                updateElementBean.setEffectiveEndDate("");
            }
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                updateElementBean.setEffectiveEndDate(endDate);
            } else {
                updateElementBean.setEffectiveEndDate("");
            }

            updateElementBean.setPriority(json.optString("priority"));
            updateElementBean.setCurrency(json.optString("currency"));

            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call UPDATE_ELEMENT_FORM(?,?,?,?,?,?,?,?,?)}";
            System.out.println("\nExecuting query: " + query);

            cstmt = connection.prepareCall(query);
            cstmt.setInt(1, id);
            cstmt.setString(2, updateElementBean.getElementName());
            cstmt.setString(3, updateElementBean.getElementType());
            cstmt.setString(4, updateElementBean.getProcessingType());
            cstmt.setString(5, updateElementBean.getEffectiveStartDate());

            cstmt.setString(6, updateElementBean.getEffectiveEndDate());
            cstmt.setString(7, updateElementBean.getPriority());
            cstmt.setString(8, updateElementBean.getCurrency());
            cstmt.registerOutParameter(9, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            updateElementBean.setElementTypeId(cstmt.getInt(9));
            //            updateElementBean.setElementTypeId(rs.getInt("OUT_SEQ"));
            updateElementBean.getElementTypeId();
            elementFormBeanList.add(updateElementBean);

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public List<ElementFormBean> getAllElement() {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();

        try {
            // Load SQL Server JDBC driver and establish connection.
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT\n" +
                    "\"PE\".\"ELEMENT_TYPE_ID\",\n" +
                    "\"PE\".\"CURRENCY_CODE\",\n" +
                    "\"PE\".\"PROCESSING_TYPE\",\n" +
                    "\"PE\".\"ELEMENT_NAME\",\n" +
                    "\"PE\".\"EFFECTIVE_START_DATE\",\n" +
                    "\"PE\".\"EFFECTIVE_END_DATE\",\n" +
                    "\"PE\".\"PROCESSING_PRIORITY\",\n" +
                    "\"PE\".\"OBJECT_VERSION_NUMBER\",\n" +
                    "\"CL\".\"BASE_CLASSIFICATION_NAME\"\n" +
                    "FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"PE\",\n" +
                    "\"PAYROLL_TEST\".\"PAY_ELE_CLASSIFICATIONS\" \"CL\"\n" +
                    "WHERE \"PE\".\"CLASSIFICATION_ID\" = \"CL\".\"CLASSIFICATION_ID\"\n" +
                    "AND  to_date(\"PE\".\"EFFECTIVE_END_DATE\",'dd-MM-yyyy') > to_date(SYSDATE, 'dd-MM-yyyy')";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementFormBean elementFormBean = new ElementFormBean();
                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                //                elementFormBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementFormBean.setCurrency(rs.getString("CURRENCY_CODE"));
                elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                //                elementFormBean.setReportingName(rs.getString("REPORTING_NAME"));
                //                elementFormBean.setDescription(rs.getString("DESCRIPTION"));
                elementFormBean.setPriority(rs.getString("PROCESSING_PRIORITY"));
                elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                elementFormBeanList.add(elementFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public List<ElementFormBean> getElementById(int id) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT \n" +
                    "\"PE\".\"ELEMENT_TYPE_ID\",\n" +
                    "\"PE\".\"CURRENCY_CODE\",\n" +
                    "\"PE\".\"PROCESSING_TYPE\",\n" +
                    "\"PE\".\"ELEMENT_NAME\",\n" +
                    "\"PE\".\"EFFECTIVE_START_DATE\",\n" +
                    "\"PE\".\"EFFECTIVE_END_DATE\",\n" +
                    "\"PE\".\"PROCESSING_PRIORITY\",\n" +
                    "\"PE\".\"OBJECT_VERSION_NUMBER\",\n" +
                    "\"CL\".\"BASE_CLASSIFICATION_NAME\"\n" +
                    "\n" +
                    "FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"PE\",\n" +
                    "\"PAYROLL_TEST\".\"PAY_ELE_CLASSIFICATIONS\" \"CL\"\n" +
                    "    \n" +
                    "    \n" +
                    "    WHERE \"PE\".\"CLASSIFICATION_ID\" = \"CL\".\"CLASSIFICATION_ID\"\n" +
                    "    AND \"PE\".\"ELEMENT_TYPE_ID\" = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementFormBean elementFormBean = new ElementFormBean();
                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                //                elementFormBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementFormBean.setCurrency(rs.getString("CURRENCY_CODE"));
                elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                //                elementFormBean.setReportingName(rs.getString("REPORTING_NAME"));
                //                elementFormBean.setDescription(rs.getString("DESCRIPTION"));
                elementFormBean.setPriority(rs.getString("PROCESSING_PRIORITY"));
                elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                elementFormBeanList.add(elementFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    //    public List<ElementFormBean> DeleteElementForm(Integer id) {
    //        List<ElementFormBean> elementFormBeanList =
    //            new ArrayList<ElementFormBean>();
    //
    //        elementFormBeanList = getElementById(id);
    //        String check = new JSONArray(elementFormBeanList).toString();
    //        if (!check.equals("[]")) {
    //
    //            try {
    //
    //                connection = AppsproConnection.getConnection();
    //                String query = null;
    //                query =
    //                        "DELETE FROM PAYROLL_TEST.PAY_ELEMENT_TYPES_F WHERE ELEMENT_TYPE_ID = ?";
    //                System.out.println("\nExecuting query: " + query);
    //                ps = connection.prepareStatement(query);
    //                ps.setInt(1, id);
    //                ps.executeUpdate();
    //
    //                return elementFormBeanList;
    //            } catch (Exception e) {
    //                e.printStackTrace();
    //            } finally {
    //                closeResources(connection, ps, rs);
    //            }
    //        } else {
    //            ElementFormBean elementFormBean = new ElementFormBean();
    //            elementFormBean.setCheck("There is no id in DB");
    //            elementFormBeanList.add(elementFormBean);
    //            return elementFormBeanList;
    //        }
    //        return null;
    //    }

    public List<ElementFormBean> validateDeleteElementByIdForObjectGroup(int id) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT \n" +
                    "\"PE\".\"ELEMENT_TYPE_ID\",\n" +
                    "\"PE\".\"CURRENCY_CODE\",\n" +
                    "\"PE\".\"PROCESSING_TYPE\",\n" +
                    "\"PE\".\"ELEMENT_NAME\",\n" +
                    "\"PE\".\"EFFECTIVE_START_DATE\",\n" +
                    "\"PE\".\"EFFECTIVE_END_DATE\",\n" +
                    "\"PE\".\"PROCESSING_PRIORITY\",\n" +
                    "\"PE\".\"OBJECT_VERSION_NUMBER\",\n" +
                    "\"CL\".\"BASE_CLASSIFICATION_NAME\",\n" +
                    "\"PES\".\"EFFECTIVE_END_DATE\" AS \"END_DATE_OBJECT_GROUP\"\n" +
                    "FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"PE\",\n" +
                    "\"PAYROLL_TEST\".\"PAY_ELE_CLASSIFICATIONS\" \"CL\",\n" +
                    "\"PAYROLL_TEST\".\"PAY_ELEMENT_SET_MEMBERS\" \"PESM\",\n" +
                    "\"PAYROLL_TEST\".\"PAY_ELEMENT_SETS\" \"PES\"\n" +
                    "WHERE \"PE\".\"CLASSIFICATION_ID\" = \"CL\".\"CLASSIFICATION_ID\"\n" +
                    "AND \"PE\".\"ELEMENT_TYPE_ID\" = \"PESM\".\"ELEMENT_TYPE_ID\"\n" +
                    "AND \"PES\".\"ELEMENT_SET_ID\" = \"PESM\".\"ELEMENT_SET_ID\"\n" +
                    "AND TO_DATE(\"PES\".\"EFFECTIVE_END_DATE\", 'DD-MM-YYYY') > TO_DATE(SYSDATE, 'DD-MM-YYYY')\n" +
                    "AND \"PE\".\"ELEMENT_TYPE_ID\" = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementFormBean elementFormBean = new ElementFormBean();
                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementFormBean.setCurrency(rs.getString("CURRENCY_CODE"));
                elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                elementFormBean.setPriority(rs.getString("PROCESSING_PRIORITY"));
                elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                elementFormBeanList.add(elementFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public List<ElementFormBean> validateDeleteElementByIdForInputValue(int id) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT \n" +
                    "\"PE\".\"ELEMENT_TYPE_ID\",\n" +
                    "\"PE\".\"CURRENCY_CODE\",\n" +
                    "\"PE\".\"PROCESSING_TYPE\",\n" +
                    "\"PE\".\"ELEMENT_NAME\",\n" +
                    "\"PE\".\"EFFECTIVE_START_DATE\",\n" +
                    "\"PE\".\"EFFECTIVE_END_DATE\",\n" +
                    "\"PE\".\"PROCESSING_PRIORITY\",\n" +
                    "\"PE\".\"OBJECT_VERSION_NUMBER\",\n" +
                    "\"CL\".\"BASE_CLASSIFICATION_NAME\",\n" +
                    "\"PIVF\".\"EFFECTIVE_END_DATE\" AS \"END_DATE_INPUT_VALUE\"\n" +
                    "FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"PE\",\n" +
                    "\"PAYROLL_TEST\".\"PAY_ELE_CLASSIFICATIONS\" \"CL\",\n" +
                    "\"PAYROLL_TEST\".\"PAY_INPUT_VALUES_F\" \"PIVF\"\n" +
                    "WHERE \"PE\".\"CLASSIFICATION_ID\" = \"CL\".\"CLASSIFICATION_ID\"\n" +
                    "AND \"PE\".\"ELEMENT_TYPE_ID\" = \"PIVF\".\"ELEMENT_TYPE_ID\"\n" +
                    "AND TO_DATE(\"PIVF\".\"EFFECTIVE_END_DATE\",'DD-MM-YYYY') > TO_DATE(SYSDATE, 'DD-MM-YYYY')\n" +
                    "AND \"PE\".\"ELEMENT_TYPE_ID\" = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementFormBean elementFormBean = new ElementFormBean();
                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementFormBean.setCurrency(rs.getString("CURRENCY_CODE"));
                elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                elementFormBean.setPriority(rs.getString("PROCESSING_PRIORITY"));
                elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                elementFormBeanList.add(elementFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public List<ElementFormBean> validateDeleteElementByIdForElementEntry(int id) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT DISTINCT PEEF.ELEMENT_ENTRY_ID, PEEF.EFFECTIVE_START_DATE, PEEF.EFFECTIVE_END_DATE, PEEF.ASSIGNMENT_ID, PEEF.ELEMENT_TYPE_ID, PETF.ELEMENT_NAME, PETF.PROCESSING_TYPE, PEC.BASE_CLASSIFICATION_NAME\n" +
                    "                                        FROM PAY_ELEMENT_ENTRIES_F PEEF,PAY_ELEMENT_ENTRY_VALUES_F PEEVF, PAY_ELEMENT_TYPES_F PETF, PAY_ELE_CLASSIFICATIONS PEC, PAY_INPUT_VALUES_F PIVF\n" +
                    "                                        WHERE PEEF.ELEMENT_ENTRY_ID = PEEVF.ELEMENT_ENTRY_ID\n" +
                    "                                        AND PEEF.ELEMENT_TYPE_ID = PETF.ELEMENT_TYPE_ID\n" +
                    "                                        AND PEEVF.INPUT_VALUE_ID = PIVF.INPUT_VALUE_ID\n" +
                    "                                        AND PETF.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "                                        AND TO_DATE(PEEF.EFFECTIVE_END_DATE,'DD-MM-YYYY') > TO_DATE(SYSDATE, 'DD-MM-YYYY') \n" +
                    "                    AND PEEF.ELEMENT_TYPE_ID = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementFormBean elementFormBean = new ElementFormBean();
                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                elementFormBeanList.add(elementFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public List<ElementFormBean> deleteElementForm(Integer id) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        List<ElementFormBean> elementFormBeanListInputValue =
            new ArrayList<ElementFormBean>();
        List<ElementFormBean> elementFormBeanListElementEntry =
            new ArrayList<ElementFormBean>();
        List<ElementFormBean> elementFormBeanListExist =
            new ArrayList<ElementFormBean>();

        elementFormBeanList = validateDeleteElementByIdForObjectGroup(id);
        String check = new JSONArray(elementFormBeanList).toString();

        elementFormBeanListInputValue =
                validateDeleteElementByIdForInputValue(id);
        String checkInputValue =
            new JSONArray(elementFormBeanListInputValue).toString();
        elementFormBeanListElementEntry =
                validateDeleteElementByIdForElementEntry(id);
        String checkElementEnrty =
            new JSONArray(elementFormBeanListElementEntry).toString();
        if (check.equals("[]") && checkInputValue.equals("[]") &&
            checkElementEnrty.equals("[]")) {

            try {

                connection = AppsproConnection.getConnection();
                String query = null;
                query =
                        "UPDATE PAYROLL_TEST.PAY_ELEMENT_TYPES_F SET EFFECTIVE_END_DATE =sysdate WHERE ELEMENT_TYPE_ID = ?";
                System.out.println("\nExecuting query: " + query);
                ps = connection.prepareStatement(query);
                ps.setInt(1, id);
                ps.executeUpdate();

                ElementFormBean elementFormBean = new ElementFormBean();
                elementFormBean.setCheck("True");
                elementFormBeanList.add(elementFormBean);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
        } else {
            ElementFormBean elementFormBean = new ElementFormBean();
            elementFormBean.setCheck("False");
            elementFormBeanListExist.add(elementFormBean);
            return elementFormBeanListExist;
        }
        return elementFormBeanList;
    }

    public static boolean isDateValid(String date) {
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public List<ElementFormBean> getElementByKeyword(String param) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();
        try {
            if (!isDateValid(param)) {
                System.out.println("Not Date");
            } else {
                SimpleDateFormat inputFormat =
                    new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat outputFormat =
                    new SimpleDateFormat("dd-MMM-yyyy");
                Date startDate = inputFormat.parse(param);
                date = outputFormat.format(startDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            if (date != null) {
                query = " SELECT \"PE\".\"ELEMENT_TYPE_ID\",\n" +
                        "\"PE\".\"CURRENCY_CODE\",\n" +
                        "\"PE\".\"PROCESSING_TYPE\",\n" +
                        "\"PE\".\"ELEMENT_NAME\",\n" +
                        "\"PE\".\"EFFECTIVE_START_DATE\",\n" +
                        "\"PE\".\"EFFECTIVE_END_DATE\",\n" +
                        "\"PE\".\"PROCESSING_PRIORITY\",\n" +
                        "\"PE\".\"OBJECT_VERSION_NUMBER\",\n" +
                        "\"CL\".\"BASE_CLASSIFICATION_NAME\"\n" +
                        "FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"PE\",\n" +
                        "\"PAYROLL_TEST\".\"PAY_ELE_CLASSIFICATIONS\" \"CL\" WHERE '" +
                        date +
                        "' IN  (\"PE\".\"ELEMENT_NAME\" , \"PE\".\"PROCESSING_PRIORITY\" ,\"PE\".\"EFFECTIVE_START_DATE\",\"PE\".\"EFFECTIVE_END_DATE\", \"PE\".\"CURRENCY_CODE\", \"PE\".\"PROCESSING_TYPE\")";
            } else {
                query = "  SELECT \"PE\".\"ELEMENT_TYPE_ID\",\n" +
                        "\"PE\".\"CURRENCY_CODE\",\n" +
                        "\"PE\".\"PROCESSING_TYPE\",\n" +
                        "\"PE\".\"ELEMENT_NAME\",\n" +
                        "\"PE\".\"EFFECTIVE_START_DATE\",\n" +
                        "\"PE\".\"EFFECTIVE_END_DATE\",\n" +
                        "\"PE\".\"PROCESSING_PRIORITY\",\n" +
                        "\"PE\".\"OBJECT_VERSION_NUMBER\",\n" +
                        "\"CL\".\"BASE_CLASSIFICATION_NAME\"\n" +
                        "FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"PE\",\n" +
                        "\"PAYROLL_TEST\".\"PAY_ELE_CLASSIFICATIONS\" \"CL\" WHERE '" +
                        param +
                        "' IN  (\"PE\".\"ELEMENT_NAME\" , \"PE\".\"PROCESSING_PRIORITY\" , \"PE\".\"CURRENCY_CODE\", \"PE\".\"PROCESSING_TYPE\", \"PE\".\"CLASSIFICATION_ID\")";
            }
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ElementFormBean elementFormBean = new ElementFormBean();
                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                //                String startTime = rs.getString("EFFECTIVE_START_DATE");
                //                SimpleDateFormat inputFormat =
                //                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
                //                SimpleDateFormat outputFormat =
                //                    new SimpleDateFormat("yyyy-MM-dd");
                //                Date startDate = inputFormat.parse(startTime);
                //                elementFormBean.setEffectiveStartDate(outputFormat.format(startDate));
                //
                //                String endTime = rs.getString("EFFECTIVE_END_DATE");
                //                SimpleDateFormat inputFormatEnd =
                //                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
                //                SimpleDateFormat outputFormatEnd =
                //                    new SimpleDateFormat("yyyy-MM-dd");
                //                Date endDate = inputFormatEnd.parse(endTime);
                //                elementFormBean.setEffectiveEndDate(outputFormatEnd.format(endDate));
                elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                //                elementFormBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                elementFormBean.setCurrency(rs.getString("CURRENCY_CODE"));
                elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                //                elementFormBean.setReportingName(rs.getString("REPORTING_NAME"));
                //                elementFormBean.setDescription(rs.getString("DESCRIPTION"));
                elementFormBean.setPriority(rs.getString("PROCESSING_PRIORITY"));
                elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                elementFormBeanList.add(elementFormBean);
            }
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public List<ElementFormBean> searchElement(String payload) {
        List<ElementFormBean> elementFormBeanList =
            new ArrayList<ElementFormBean>();

        JSONObject json = new JSONObject(payload);
        String elementName = json.optString("elementName");
        String proccessType = json.optString("processingType");
        String effectiveStartDate = json.optString("effectiveStartDate");
        String effectiveEndDate = json.optString("effectiveEndDate");
        String priority = json.optString("priority");
        String currency = json.optString("currency");
        String classificationId = json.optString("classificationId");
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            if (!effectiveEndDate.isEmpty()) {

                SimpleDateFormat inputFormat =
                    new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat outputFormat =
                    new SimpleDateFormat("dd-MMM-yyyy");
                Date startDate = inputFormat.parse(effectiveStartDate);
                effectiveStartDate = outputFormat.format(startDate);


                SimpleDateFormat inputFormatEnd =
                    new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat outputFormatEnd =
                    new SimpleDateFormat("dd-MMM-yyyy");
                Date endDate = inputFormatEnd.parse(effectiveEndDate);
                effectiveEndDate = outputFormatEnd.format(endDate);

                query = "SELECT \"PE\".\"ELEMENT_TYPE_ID\",\n" +
                        "\"PE\".\"CURRENCY_CODE\",\n" +
                        "\"PE\".\"PROCESSING_TYPE\",\n" +
                        "\"PE\".\"ELEMENT_NAME\",\n" +
                        "\"PE\".\"EFFECTIVE_START_DATE\",\n" +
                        "\"PE\".\"EFFECTIVE_END_DATE\",\n" +
                        "\"PE\".\"PROCESSING_PRIORITY\",\n" +
                        "\"PE\".\"OBJECT_VERSION_NUMBER\",\n" +
                        "\"PE\".\"CLASSIFICATION_ID\",\n" +
                        "\"CL\".\"BASE_CLASSIFICATION_NAME\"\n" +
                        "\n" +
                        "FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"PE\",\n" +
                        "\"PAYROLL_TEST\".\"PAY_ELE_CLASSIFICATIONS\" \"CL\" \n" +
                        "\n" +
                        "WHERE \"PE\".\"ELEMENT_NAME\" = ? AND \"PE\".\"PROCESSING_TYPE\" = ?  AND \"PE\".\"EFFECTIVE_START_DATE\" = ?  AND \"PE\".\"EFFECTIVE_END_DATE\" = ? AND \"PE\".\"PROCESSING_PRIORITY\" = ? AND \"PE\".\"CURRENCY_CODE\" = ? AND \"CL\".\"CLASSIFICATION_ID\" = ?";
                System.out.println("\nExecuting query: " + query);
                ps = connection.prepareStatement(query);
                ps.setString(1, elementName);
                ps.setString(2, proccessType);
                ps.setString(3, effectiveStartDate);
                ps.setString(4, effectiveEndDate);
                ps.setString(5, priority);
                ps.setString(6, currency);
                ps.setString(7, classificationId);
                rs = ps.executeQuery();
                while (rs.next()) {
                    ElementFormBean elementFormBean = new ElementFormBean();
                    elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                    //                String startTime = rs.getString("EFFECTIVE_START_DATE");
                    //                SimpleDateFormat inputFormat =
                    //                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
                    //                SimpleDateFormat outputFormat =
                    //                    new SimpleDateFormat("yyyy-MM-dd");
                    //                Date startDate = inputFormat.parse(startTime);
                    //                elementFormBean.setEffectiveStartDate(outputFormat.format(startDate));
                    //
                    //                String endTime = rs.getString("EFFECTIVE_END_DATE");
                    //                SimpleDateFormat inputFormatEnd =
                    //                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
                    //                SimpleDateFormat outputFormatEnd =
                    //                    new SimpleDateFormat("yyyy-MM-dd");
                    //                Date endDate = inputFormatEnd.parse(endTime);
                    //                elementFormBean.setEffectiveEndDate(outputFormatEnd.format(endDate));
                    elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                    elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                    //                    elementFormBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                    elementFormBean.setCurrency(rs.getString("CURRENCY_CODE"));
                    elementFormBean.setClassificationId(rs.getString("CLASSIFICATION_ID"));
                    elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                    elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                    //                    elementFormBean.setReportingName(rs.getString("REPORTING_NAME"));
                    //                    elementFormBean.setDescription(rs.getString("DESCRIPTION"));
                    elementFormBean.setPriority(rs.getString("PROCESSING_PRIORITY"));
                    elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                    elementFormBeanList.add(elementFormBean);
                }
            } else {
                SimpleDateFormat inputFormat =
                    new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat outputFormat =
                    new SimpleDateFormat("dd-MMM-yyyy");
                Date startDate = inputFormat.parse(effectiveStartDate);
                effectiveStartDate = outputFormat.format(startDate);

                query = "SELECT \"PE\".\"ELEMENT_TYPE_ID\",\n" +
                        "\"PE\".\"CURRENCY_CODE\",\n" +
                        "\"PE\".\"PROCESSING_TYPE\",\n" +
                        "\"PE\".\"ELEMENT_NAME\",\n" +
                        "\"PE\".\"EFFECTIVE_START_DATE\",\n" +
                        "\"PE\".\"EFFECTIVE_END_DATE\",\n" +
                        "\"PE\".\"PROCESSING_PRIORITY\",\n" +
                        "\"PE\".\"OBJECT_VERSION_NUMBER\",\n" +
                        "\"PE\".\"CLASSIFICATION_ID\",\n" +
                        "\"CL\".\"BASE_CLASSIFICATION_NAME\"\n" +
                        "\n" +
                        "FROM \"PAYROLL_TEST\".\"PAY_ELEMENT_TYPES_F\" \"PE\",\n" +
                        "\"PAYROLL_TEST\".\"PAY_ELE_CLASSIFICATIONS\" \"CL\" \n" +
                        "\n" +
                        "WHERE \"PE\".\"ELEMENT_NAME\" = ? AND \"PE\".\"PROCESSING_TYPE\" = ?  AND \"PE\".\"EFFECTIVE_START_DATE\" = ?  AND \"PE\".\"PROCESSING_PRIORITY\" = ? AND \"PE\".\"CURRENCY_CODE\" = ? AND \"CL\".\"CLASSIFICATION_ID\" = ?";
                System.out.println("\nExecuting query: " + query);
                ps = connection.prepareStatement(query);
                ps.setString(1, elementName);
                ps.setString(2, proccessType);
                ps.setString(3, effectiveStartDate);
                ps.setString(4, priority);
                ps.setString(5, currency);
                ps.setString(6, classificationId);
                rs = ps.executeQuery();
                while (rs.next()) {
                    ElementFormBean elementFormBean = new ElementFormBean();
                    elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
                    //                String startTime = rs.getString("EFFECTIVE_START_DATE");
                    //                SimpleDateFormat inputFormat =
                    //                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
                    //                SimpleDateFormat outputFormat =
                    //                    new SimpleDateFormat("yyyy-MM-dd");
                    //                Date startDate = inputFormat.parse(startTime);
                    //                elementFormBean.setEffectiveStartDate(outputFormat.format(startDate));
                    //
                    //                String endTime = rs.getString("EFFECTIVE_END_DATE");
                    //                SimpleDateFormat inputFormatEnd =
                    //                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
                    //                SimpleDateFormat outputFormatEnd =
                    //                    new SimpleDateFormat("yyyy-MM-dd");
                    //                Date endDate = inputFormatEnd.parse(endTime);
                    //                elementFormBean.setEffectiveEndDate(outputFormatEnd.format(endDate));
                    elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                    elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                    //                    elementFormBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
                    elementFormBean.setCurrency(rs.getString("CURRENCY_CODE"));
                    elementFormBean.setClassificationId(rs.getString("CLASSIFICATION_ID"));
                    elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                    elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                    //                    elementFormBean.setReportingName(rs.getString("REPORTING_NAME"));
                    //                    elementFormBean.setDescription(rs.getString("DESCRIPTION"));
                    elementFormBean.setPriority(rs.getString("PROCESSING_PRIORITY"));
                    elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
                    elementFormBeanList.add(elementFormBean);
                }
            }
        }

        catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return elementFormBeanList;
    }

    public String getCurrency() {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            BIReportModel biModel = new BIReportModel();
            JSONObject json =
                biModel.runReport(BIReportModel.REPORT_NAME.Currency_US_Report.getValue(),
                                  paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");
            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    //    public List<ElementFormBean> searchElement(String elementName,
    //                                               String proccessType,
    //                                               String effectiveStartDate,
    //                                               String priority,
    //                                               String currency) {
    //        List<ElementFormBean> elementFormBeanList =
    //            new ArrayList<ElementFormBean>();
    //
    //        try {
    //            connection = AppsproConnection.getConnection();
    //            String query = null;
    //            query =
    //                    "SELECT * FROM PAYROLL_TEST.PAY_ELEMENT_TYPES_F WHERE ELEMENT_NAME = ? AND PROCESSING_TYPE = ?  AND EFFECTIVE_START_DATE = ? AND PROCESSING_PRIORITY = ? AND CURRENCY_CODE = ?";
    //            System.out.println("\nExecuting query: " + query);
    //            ps = connection.prepareStatement(query);
    //            ps.setString(1, elementName);
    //            ps.setString(2, proccessType);
    //            ps.setString(3, effectiveStartDate);
    //            ps.setString(4, priority);
    //            ps.setString(5, currency);
    //            rs = ps.executeQuery();
    //            while (rs.next()) {
    //                ElementFormBean elementFormBean = new ElementFormBean();
    //                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
    //                //                String startTime = rs.getString("EFFECTIVE_START_DATE");
    //                //                SimpleDateFormat inputFormat =
    //                //                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
    //                //                SimpleDateFormat outputFormat =
    //                //                    new SimpleDateFormat("yyyy-MM-dd");
    //                //                Date startDate = inputFormat.parse(startTime);
    //                //                elementFormBean.setEffectiveStartDate(outputFormat.format(startDate));
    //                //
    //                //                String endTime = rs.getString("EFFECTIVE_END_DATE");
    //                //                SimpleDateFormat inputFormatEnd =
    //                //                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
    //                //                SimpleDateFormat outputFormatEnd =
    //                //                    new SimpleDateFormat("yyyy-MM-dd");
    //                //                Date endDate = inputFormatEnd.parse(endTime);
    //                //                elementFormBean.setEffectiveEndDate(outputFormatEnd.format(endDate));
    //                elementFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
    //                elementFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
    //                elementFormBean.setBusinessGroupId(rs.getString("BUSINESS_GROUP_ID"));
    //                elementFormBean.setCurrency(rs.getString("CURRENCY_CODE"));
    //                elementFormBean.setClassificationId(rs.getString("CLASSIFICATION_ID"));
    //                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
    //                elementFormBean.setReportingName(rs.getString("REPORTING_NAME"));
    //                elementFormBean.setDescription(rs.getString("DESCRIPTION"));
    //                elementFormBean.setPriority(rs.getString("PROCESSING_PRIORITY"));
    //                elementFormBean.setProcessingType(rs.getString("PROCESSING_TYPE"));
    //                elementFormBeanList.add(elementFormBean);
    //            }
    //        } catch (Exception e) {
    //            System.out.println(e);
    //            e.printStackTrace();
    //        } finally {
    //            closeResources(connection, ps, rs);
    //        }
    //        return elementFormBeanList;
    //    }

    public static void main(String[] args) {
        ElementEnrtyFormDAO elementEnrtyFormDAO = new ElementEnrtyFormDAO();
        elementEnrtyFormDAO.getAllElement();
    }

}
