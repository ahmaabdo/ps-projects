package com.appspro.payroll.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.payroll.bean.ElementFormBean;

import com.appspro.payroll.bean.ObjectGroupFormBean;

import common.restHelper.RestHelper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracle.jdbc.OracleTypes;

import oracle.sql.ArrayDescriptor;

import org.json.JSONArray;
import org.json.JSONObject;

public class ObjectGroupFormDAO extends AppsproConnection {


    Connection connection;
    PreparedStatement ps;
    CallableStatement cstmt = null;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    static String date;

    public List<ObjectGroupFormBean> insertProcedure(String insertElement) {

        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            ObjectGroupFormBean objectGroupFormBean =
                new ObjectGroupFormBean();
            JSONObject json = new JSONObject(insertElement);
            objectGroupFormBean.setElementSetName(json.optString("elementSetName"));
            objectGroupFormBean.setElementSetType(json.optString("elementSetType"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                objectGroupFormBean.setEffectiveEndDate(endDate);
            } else {
                objectGroupFormBean.setEffectiveEndDate("31-Dec-4721");
            }
            objectGroupFormBean.setEffectiveStartDate(startDate);

            JSONArray innerJson = new JSONArray();
            innerJson = json.optJSONArray("element");

            //  objectGroupFormBean.setInclusionStatus(innerJson.optString("inclusionStatus"));
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call INSERT_ELEMENT_OBJECT_GROUP(?,?,?,?,?)}";
            System.out.println("\nExecuting query: " + query);
            //            ArrayDescriptor desc =
            //                ArrayDescriptor.createDescriptor("SIMPLEARRAY", connection);
            cstmt = connection.prepareCall(query);
            cstmt.setString(1, objectGroupFormBean.getElementSetName());
            cstmt.setString(2, objectGroupFormBean.getElementSetType());
            cstmt.setString(3, objectGroupFormBean.getEffectiveStartDate());
            cstmt.setString(4, objectGroupFormBean.getEffectiveEndDate());
            cstmt.registerOutParameter(5, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            int generatedId = cstmt.getInt(5);
            objectGroupFormBean.setElementSetId(generatedId);
            for (int i = 0; i < innerJson.length(); i++) {
                query = "{call INSERT_ELEMENT_OBJECT_GROUP_DETAILS(?,?,?,?)}";
                System.out.println("\nExecuting query: " + query);
                cstmt = connection.prepareCall(query);
                cstmt.setInt(1, objectGroupFormBean.getElementSetId());
                String elementId =
                    innerJson.optJSONObject(i).optString("elementId");
                objectGroupFormBean.setElementTypeId(elementId);

                cstmt.setString(2, objectGroupFormBean.getElementTypeId());

                String classificationId =
                    innerJson.optJSONObject(i).optString("classificationId");
                objectGroupFormBean.setClassificationId(classificationId);

                cstmt.setString(3, objectGroupFormBean.getClassificationId());
                
                cstmt.registerOutParameter(4, OracleTypes.NUMBER);
                cstmt.executeUpdate();
            }
            objectGroupFormBeanList.add(objectGroupFormBean);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return objectGroupFormBeanList;
    }

    //    public List<ObjectGroupFormBean> getAllObjectGroup() {
    //        List<ObjectGroupFormBean> objectGroupFormBeanList =
    //            new ArrayList<ObjectGroupFormBean>();
    //        List<ElementFormBean> elementFormBeanList =
    //            new ArrayList<ElementFormBean>();
    //        try {
    //            // Load SQL Server JDBC driver and establish connection.
    //            connection = AppsproConnection.getConnection();
    //            String query = null;
    //            query = "SELECT \"PES\".\"ELEMENT_SET_ID\",\n" +
    //                    "\"PES\".\"EFFECTIVE_START_DATE\",\n" +
    //                    "\"PES\".\"EFFECTIVE_END_DATE\",\n" +
    //                    "\"PES\".\"ELEMENT_SET_NAME\",\n" +
    //                    "\"PES\".\"ELEMENT_SET_TYPE\",\n" +
    //                    "\"PESM\".\"ELEMENT_SET_MEMBER_ID\",\n" +
    //                    "\"PESM\".\"ELEMENT_TYPE_ID\",\n" +
    //                    "\"PETF\".\"ELEMENT_NAME\",\n" +
    //                    "\"PEC\".\"BASE_CLASSIFICATION_NAME\"\n" +
    //                    "FROM \"PAY_ELEMENT_SETS\" \"PES\",\n" +
    //                    "\"PAY_ELEMENT_SET_MEMBERS\" \"PESM\",\n" +
    //                    "\"PAY_ELEMENT_TYPES_F\" \"PETF\",\n" +
    //                    "\"PAY_ELE_CLASSIFICATIONS\" \"PEC\"\n" +
    //                    "WHERE \"PES\".\"ELEMENT_SET_ID\" = \"PESM\".\"ELEMENT_SET_ID\"\n" +
    //                    "AND \"PESM\".\"ELEMENT_TYPE_ID\" = \"PETF\".\"ELEMENT_TYPE_ID\"\n" +
    //                    "AND \"PETF\".\"CLASSIFICATION_ID\" = \"PEC\".\"CLASSIFICATION_ID\"\n" +
    //                    "AND TO_DATE(\"PES\".\"EFFECTIVE_END_DATE\", 'dd-MM-yyyy') > TO_DATE(SYSDATE, 'dd-MM-yyyy')";
    //            System.out.println("\nExecuting query: " + query);
    //            ps = connection.prepareStatement(query);
    //            rs = ps.executeQuery();
    //            while (rs.next()) {
    //                ObjectGroupFormBean objectGroupFormBean =
    //                    new ObjectGroupFormBean();
    //                ElementFormBean elementFormBean = new ElementFormBean();
    //                objectGroupFormBean.setElementSetId(rs.getInt("ELEMENT_SET_ID"));
    //                objectGroupFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
    //                objectGroupFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
    //                objectGroupFormBean.setElementSetName(rs.getString("ELEMENT_SET_NAME"));
    //                objectGroupFormBean.setElementSetType(rs.getString("ELEMENT_SET_TYPE"));
    //                elementFormBean.setElementTypeId(rs.getInt("ELEMENT_TYPE_ID"));
    //                elementFormBean.setElementName(rs.getString("ELEMENT_NAME"));
    //                elementFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
    //                elementFormBeanList.add(elementFormBean);
    //                objectGroupFormBean.setElementFormBeanList(elementFormBeanList);
    //                objectGroupFormBeanList.add(objectGroupFormBean);
    //            }
    //
    //        } catch (Exception e) {
    //            System.out.println(e);
    //            e.printStackTrace();
    //        } finally {
    //            closeResources(connection, ps, rs);
    //        }
    //        return objectGroupFormBeanList;
    //    }

    public List<ObjectGroupFormBean> getAllObjectGroup() {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            // Load SQL Server JDBC driver and establish connection.
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * FROM \"PAY_ELEMENT_SETS\" \"PES\" WHERE TO_DATE(\"PES\".\"EFFECTIVE_END_DATE\", 'dd-MM-yyyy') > TO_DATE(SYSDATE, 'dd-MM-yyyy')";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ObjectGroupFormBean objectGroupFormBean =
                    new ObjectGroupFormBean();
                objectGroupFormBean.setElementSetId(rs.getInt("ELEMENT_SET_ID"));
                objectGroupFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                objectGroupFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                objectGroupFormBean.setElementSetName(rs.getString("ELEMENT_SET_NAME"));
                objectGroupFormBean.setElementSetType(rs.getString("ELEMENT_SET_TYPE"));
                objectGroupFormBeanList.add(objectGroupFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return objectGroupFormBeanList;
    }

    public List<ObjectGroupFormBean> getObjectGroupHeaderById(int id) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT \"PES\".\"ELEMENT_SET_ID\",\n" +
                    "\"PES\".\"EFFECTIVE_START_DATE\",\n" +
                    "\"PES\".\"EFFECTIVE_END_DATE\",\n" +
                    "\"PES\".\"ELEMENT_SET_NAME\",\n" +
                    "\"PES\".\"ELEMENT_SET_TYPE\"\n" +
                    "FROM \"PAY_ELEMENT_SETS\" \"PES\"\n" +
                    "WHERE \"PES\".\"ELEMENT_SET_ID\" = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ObjectGroupFormBean objectGroupFormBean =
                    new ObjectGroupFormBean();
                objectGroupFormBean.setElementSetId(rs.getInt("ELEMENT_SET_ID"));
                objectGroupFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                objectGroupFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                objectGroupFormBean.setElementName(rs.getString("ELEMENT_SET_NAME"));
                objectGroupFormBean.setElementType(rs.getString("ELEMENT_SET_TYPE"));
                objectGroupFormBeanList.add(objectGroupFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return objectGroupFormBeanList;
    }


    public List<ObjectGroupFormBean> getObjectGroupDetailsById(int id) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT \"PES\".\"ELEMENT_SET_ID\",\n" +
                    "\"PESM\".\"ELEMENT_SET_MEMBER_ID\",\n" +
                    "\"PESM\".\"ELEMENT_TYPE_ID\",\n" +
                    "\"PETF\".\"ELEMENT_NAME\",\n" +
                    "\"PEC\".\"BASE_CLASSIFICATION_NAME\"\n" +
                    "FROM \"PAY_ELEMENT_SETS\" \"PES\",\n" +
                    "\"PAY_ELEMENT_SET_MEMBERS\" \"PESM\",\n" +
                    "\"PAY_ELEMENT_TYPES_F\" \"PETF\",\n" +
                    "\"PAY_ELE_CLASSIFICATIONS\" \"PEC\"\n" +
                    "WHERE \"PES\".\"ELEMENT_SET_ID\" = \"PESM\".\"ELEMENT_SET_ID\"\n" +
                    "AND \"PESM\".\"ELEMENT_TYPE_ID\" = \"PETF\".\"ELEMENT_TYPE_ID\"\n" +
                    "AND \"PETF\".\"CLASSIFICATION_ID\" = \"PEC\".\"CLASSIFICATION_ID\"\n" +
                    "AND \"PES\".\"ELEMENT_SET_ID\" = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ObjectGroupFormBean objectGroupFormBean =
                    new ObjectGroupFormBean();
                objectGroupFormBean.setElementSetId(rs.getInt("ELEMENT_SET_ID"));
                objectGroupFormBean.setElementTypeId(rs.getString("ELEMENT_TYPE_ID"));
                objectGroupFormBean.setElementName(rs.getString("ELEMENT_NAME"));
                objectGroupFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                objectGroupFormBeanList.add(objectGroupFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return objectGroupFormBeanList;
    }

    public List<ObjectGroupFormBean> getObjectGroupDetailsClassiById(int id) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT PES.ELEMENT_SET_ID,\n" +
                    "PEC.BASE_CLASSIFICATION_NAME,\n" +
                    "PEC.CLASSIFICATION_ID\n" +
                    "FROM PAY_ELEMENT_SETS PES,\n" +
                    "PAY_ELEMENT_SET_MEMBERS PESM,\n" +
                    "PAY_ELE_CLASSIFICATIONS PEC\n" +
                    "WHERE PES.ELEMENT_SET_ID = PESM.ELEMENT_SET_ID\n" +
                    "AND PESM.CLASSIFICATION_ID = PEC.CLASSIFICATION_ID\n" +
                    "AND PES.ELEMENT_SET_ID = ?";
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ObjectGroupFormBean objectGroupFormBean =
                    new ObjectGroupFormBean();
                objectGroupFormBean.setElementSetId(rs.getInt("ELEMENT_SET_ID"));
                objectGroupFormBean.setClassificationId(rs.getString("CLASSIFICATION_ID"));
                objectGroupFormBean.setElementType(rs.getString("BASE_CLASSIFICATION_NAME"));
                objectGroupFormBeanList.add(objectGroupFormBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return objectGroupFormBeanList;
    }

    public List<ObjectGroupFormBean> getObjectGroupByKeyword(String param) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            if (!isDateValid(param)) {
                System.out.println("Not Date");
            } else {
                SimpleDateFormat inputFormat =
                    new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat outputFormat =
                    new SimpleDateFormat("dd-MMM-yyyy");
                Date startDate = inputFormat.parse(param);
                date = outputFormat.format(startDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            if (date != null) {
                query = "SELECT \"PES\".\"ELEMENT_SET_ID\",\n" +
                        "\"PES\".\"EFFECTIVE_START_DATE\",\n" +
                        "\"PES\".\"EFFECTIVE_END_DATE\",\n" +
                        "\"PES\".\"ELEMENT_SET_NAME\",\n" +
                        "\"PES\".\"ELEMENT_SET_TYPE\"\n" +
                        "FROM \"PAY_ELEMENT_SETS\" \"PES\"\n" +
                        "WHERE TO_DATE(\"PES\".\"EFFECTIVE_END_DATE\", 'dd-MM-yyyy') > TO_DATE(SYSDATE, 'dd-MM-yyyy')\n" +
                        "AND '" + date +
                        "' IN (\"PES\".\"ELEMENT_SET_NAME\",\"PES\".\"ELEMENT_SET_TYPE\", \"PES\".\"EFFECTIVE_START_DATE\", \"PES\".\"EFFECTIVE_END_DATE\")";
            } else {
                query = "SELECT \"PES\".\"ELEMENT_SET_ID\",\n" +
                        "\"PES\".\"EFFECTIVE_START_DATE\",\n" +
                        "\"PES\".\"EFFECTIVE_END_DATE\",\n" +
                        "\"PES\".\"ELEMENT_SET_NAME\",\n" +
                        "\"PES\".\"ELEMENT_SET_TYPE\"\n" +
                        "FROM \"PAY_ELEMENT_SETS\" \"PES\"\n" +
                        "WHERE TO_DATE(\"PES\".\"EFFECTIVE_END_DATE\", 'dd-MM-yyyy') > TO_DATE((SYSDATE, 'dd-MM-yyyy')\n" +
                        "AND '" + param +
                        "' IN (\"PES\".\"ELEMENT_SET_NAME\",\"PES\".\"ELEMENT_SET_TYPE\")";
            }
            System.out.println("\nExecuting query: " + query);
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ObjectGroupFormBean objectGroupFormBean =
                    new ObjectGroupFormBean();
                objectGroupFormBean.setElementSetId(rs.getInt("ELEMENT_SET_ID"));
                objectGroupFormBean.setEffectiveStartDate(rs.getString("EFFECTIVE_START_DATE"));
                objectGroupFormBean.setEffectiveEndDate(rs.getString("EFFECTIVE_END_DATE"));
                objectGroupFormBean.setElementSetName(rs.getString("ELEMENT_SET_NAME"));
                objectGroupFormBean.setElementSetType(rs.getString("ELEMENT_SET_TYPE"));
                objectGroupFormBeanList.add(objectGroupFormBean);
            }
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return objectGroupFormBeanList;
    }

    public List<ObjectGroupFormBean> updateProcedure(int id,
                                                     String updateObjectGroup) {

        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();
        try {
            ObjectGroupFormBean objectGroupFormBean =
                new ObjectGroupFormBean();
            JSONObject json = new JSONObject(updateObjectGroup);
            objectGroupFormBean.setElementSetName(json.optString("elementSetName"));
            objectGroupFormBean.setElementSetType(json.optString("elementSetType"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(json.optString("effectiveStartDate"));
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String startDate = sdf.format(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            String effectiveEndDate = json.optString("effectiveEndDate");
            if (!effectiveEndDate.isEmpty()) {
                Date endDateEff = sdf2.parse(effectiveEndDate);
                sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
                String endDate = sdf2.format(endDateEff);
                objectGroupFormBean.setEffectiveEndDate(endDate);
            } else {
                objectGroupFormBean.setEffectiveEndDate("");
            }
            objectGroupFormBean.setEffectiveStartDate(startDate);
            JSONArray innerJson = new JSONArray();
            innerJson = json.optJSONArray("element");

            connection = AppsproConnection.getConnection();
            String query = null;
            query = "{call UPDATE_OBJECT_GROUP(?,?,?,?,?,?)}";
            System.out.println("\nExecuting query: " + query);

            cstmt = connection.prepareCall(query);
            cstmt.setInt(1, id);
            cstmt.setString(2, objectGroupFormBean.getEffectiveStartDate());
            cstmt.setString(3, objectGroupFormBean.getEffectiveEndDate());
            cstmt.setString(4, objectGroupFormBean.getElementSetName());
            cstmt.setString(5, objectGroupFormBean.getElementSetType());
            cstmt.registerOutParameter(6, OracleTypes.NUMBER);
            cstmt.executeUpdate();
            int generatedId = cstmt.getInt(6);
            objectGroupFormBean.setElementSetId(generatedId);
            for (int i = 0; i < innerJson.length(); i++) {
                query = "{call INSERT_ELEMENT_OBJECT_GROUP_DETAILS(?,?,?,?)}";
                System.out.println("\nExecuting query: " + query);
                cstmt = connection.prepareCall(query);
                cstmt.setInt(1, objectGroupFormBean.getElementSetId());
                String elementId =
                    innerJson.optJSONObject(i).optString("elementId");
                objectGroupFormBean.setElementTypeId(elementId);

                cstmt.setString(2, objectGroupFormBean.getElementTypeId());

                String classificationId =
                    innerJson.optJSONObject(i).optString("classificationId");
                objectGroupFormBean.setClassificationId(classificationId);

                cstmt.setString(3, objectGroupFormBean.getClassificationId());

                cstmt.registerOutParameter(4, OracleTypes.NUMBER);
                cstmt.executeUpdate();
                objectGroupFormBeanList.add(objectGroupFormBean);
            }

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs, cstmt);
        }
        return objectGroupFormBeanList;
    }

    public List<ObjectGroupFormBean> deleteObjectGroup(Integer id) {
        List<ObjectGroupFormBean> objectGroupFormBeanList =
            new ArrayList<ObjectGroupFormBean>();

        objectGroupFormBeanList = getObjectGroupHeaderById(id);
        String check = new JSONArray(objectGroupFormBeanList).toString();
        if (!check.equals("[]")) {

            try {

                connection = AppsproConnection.getConnection();
                String query = null;
                query =
                        "UPDATE PAYROLL_TEST.PAY_ELEMENT_SETS SET EFFECTIVE_END_DATE =sysdate WHERE ELEMENT_SET_ID = ?";
                System.out.println("\nExecuting query: " + query);
                ps = connection.prepareStatement(query);
                ps.setInt(1, id);
                ps.executeUpdate();

                return objectGroupFormBeanList;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
        } else {
            ObjectGroupFormBean objectGroupFormBean =
                new ObjectGroupFormBean();
            objectGroupFormBean.setCheck("There is no id in DB");
            objectGroupFormBeanList.add(objectGroupFormBean);
            return objectGroupFormBeanList;
        }
        return objectGroupFormBeanList;
    }


    public static boolean isDateValid(String date) {
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }


}
