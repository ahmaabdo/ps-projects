package com.appsproExceptionn;

import com.appspro.db.AppsproConnection;
import com.appspro.payroll.bean.LogsBean;

import javax.servlet.GenericServlet;

//import com.appspro.receipt.dao.LogsDAO;


public class AppsProException extends Exception {


    public AppsProException(Throwable throwable) {
        super(throwable);
        //Insert database log
//        AppsproConnection.LOG.error("ERROR", throwable);
        throwable.printStackTrace();
    }

    public AppsProException(String message, Throwable throwable) {
        super(throwable);
//        AppsproConnection.LOG.error(message, throwable);
        throwable.printStackTrace();
    }

    public AppsProException(String message) {
        super(message);
    }

    public AppsProException() {
        super();
    }
    

}
