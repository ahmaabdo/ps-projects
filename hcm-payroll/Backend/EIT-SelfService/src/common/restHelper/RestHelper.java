package common.restHelper;

import com.appspro.db.CommonConfigReader;
import com.appspro.payroll.bean.ElementFormBean;
import com.appspro.payroll.dao.CallProcedure;
import com.appspro.payroll.dao.ElementEnrtyFormDAO;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.security.cert.CertificateException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.joda.time.Chronology;
import org.joda.time.LocalDate;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;


public class RestHelper {

    private static RestHelper instance = null;

    public static RestHelper getInstance() {
        if (instance == null)
            instance = new RestHelper();
        return instance;
    }
    private String InstanceUrl =
        CommonConfigReader.getValue("InstanceUrl"); //;"https://ejcp-test.fa.em2.oraclecloud.com:443";
    private String orgnizationUrl =
        "//hcmCoreSetupApi/resources/latest/organizations";
    private String biReportUrl =
        CommonConfigReader.getValue("biReportUrl"); //"https://ejcp-test.fa.em2.oraclecloud.com/xmlpserver/services/PublicReportService";
    private String SecurityService =
        CommonConfigReader.getValue("SecurityService");
    private String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private String instanceName =
        CommonConfigReader.getValue("instanceName"); //"ejcp-test.fa.em2.oraclecloud.com";
    private String gradeUrl = "/hcmCoreSetupApi/resources/latest/grades";
    private String locationUrl = "/hcmCoreSetupApi/resources/latest/locations";
    private String positionUrl = "/hcmCoreSetupApi/resources/latest/positions";
    private String jobsUrl = "/hcmCoreSetupApi/resources/latest/jobs";
    private String absenceUrl = "/hcmRestApi/resources/11.13.17.11/absences/";
    private String departmentUrl =
        "/hcmRestApi/resources/11.13.18.05/departmentsLov";
    private String employeeAnalysisUrl =
        "https://hhajcs-a562419.java.em2.oraclecloudapps.com/HHA-OTL/EmployeeTimeAnalysisService";
    public static String USER_NAME =
        CommonConfigReader.getValue("USER_NAME"); //"mahmoud.essam@appspro-me.com";
    public static String PASSWORD = CommonConfigReader.getValue("PASSWORD");
    public static String Schema_Name =
        CommonConfigReader.getValue("Schema_Name");
    public final static String SAAS_URL =
        "https://" + CommonConfigReader.getValue("instanceName");
    public final static String STATIC_INSTANCE_NAME =
        "fa-epqq-test-saasfaprod1.fa.ocs.oraclecloud.com";


    public static String getSchema_Name() {
        return Schema_Name;
    }


    public String getAbsenceUrl() {
        return absenceUrl;
    }

    public void setAbsenceUrl(String absenceUrl) {
        this.absenceUrl = absenceUrl;
    }

    public String getJobUrl() {
        return this.jobsUrl;
    }

    public String getPositionUrl() {
        return positionUrl;
    }

    public String getLocationUrl() {
        return this.locationUrl;
    }

    public String getGradeUrl() {
        return this.gradeUrl;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public String getEmployeeUrl() {
        return InstanceUrl + employeeServiceUrl;
    }

    public String getEmployeeRestEndPointUrl() {
        return employeeServiceUrl;
    }

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getOrgnizationUrl() {
        return orgnizationUrl;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }


    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            //           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            //           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    //                   e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
                }
            }
        }

        return sb.toString();

    }

    public String convertToHijri(String d) throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");

        java.util.Date dateNew = format1.parse(d);
        String formatedDate = format2.format(dateNew);
        String[] parts = formatedDate.split("/");
        String year = parts[2]; // 004
        String month = parts[1]; // 034556
        String day = parts[0];
        int addDay = Integer.parseInt(day);
        int addMonth = Integer.parseInt(month);
        int addYear = Integer.parseInt(year);
        int number_Of_DaysInMonth = 0;
        switch (addMonth) {
        case 1:
            number_Of_DaysInMonth = 31;
            break;
        case 2:
            if ((addYear % 400 == 0) ||
                ((addYear % 4 == 0) && (addYear % 100 != 0))) {
                number_Of_DaysInMonth = 29;
            } else {
                number_Of_DaysInMonth = 28;
            }

            break;
        case 3:
            number_Of_DaysInMonth = 31;
            break;
        case 4:
            number_Of_DaysInMonth = 30;
            break;
        case 5:
            number_Of_DaysInMonth = 31;
            break;
        case 6:
            number_Of_DaysInMonth = 30;
            break;
        case 7:
            number_Of_DaysInMonth = 31;
            break;
        case 8:
            number_Of_DaysInMonth = 31;
            break;
        case 9:
            number_Of_DaysInMonth = 30;
            break;
        case 10:
            number_Of_DaysInMonth = 31;
            break;
        case 11:
            number_Of_DaysInMonth = 30;
            break;
        case 12:
            number_Of_DaysInMonth = 31;
            break;
        default:
            number_Of_DaysInMonth = 0;
            break;

        }

        if (number_Of_DaysInMonth == 28 || number_Of_DaysInMonth == 29) {
            if (addDay <= 27) {

                addDay += 1;

            } else {
                addDay = 0;
                addMonth += 1;
                addDay += 1;
            }

        }
        if (number_Of_DaysInMonth == 30 || number_Of_DaysInMonth == 31) {
            if (addDay <= 29) {

                addDay += 1;

            } else {
                addDay = 0;
                addMonth += 1;
                addDay += 1;
            }

        }
        day = String.valueOf(addDay);

        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();

        LocalDate todayIso =
            new LocalDate(Integer.parseInt(year), Integer.parseInt(month),
                          Integer.parseInt(day), iso);
        LocalDate todayHijri =
            new LocalDate(todayIso.toDateTimeAtStartOfDay(), hijri);
        return todayHijri.toString();
    }

    public String convertToGregorian(String d) {
        d = d.replace("-", "/");
        String[] parts = d.split("/");
        String year = parts[0]; // 004
        String month = parts[1]; // 034556
        //        AppsproConnection.LOG.info(month);
        String day = parts[2];
        int addDay = Integer.parseInt(day);
        addDay += 1;
        day = String.valueOf(addDay);

        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        //1434-05-18
        LocalDate todayHijri =
            new LocalDate(Integer.parseInt(year), Integer.parseInt(month),
                          Integer.parseInt(day), hijri);
        //        AppsproConnection.LOG.info("todayHijri" + todayHijri);
        LocalDate todayIso =
            new LocalDate(todayHijri.toDateTimeAtStartOfDay(), iso);

        return todayIso.toString();
    }

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public String callSaaS(String serverUrl, String restFrameWorkVersion) {
        String newurl = serverUrl.replaceAll(" ", "%20");
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        try {
            URL url =
                new URL(null, newurl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            //  connection.setRequestProperty("REST-Framework-Version", "3");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());
            connection.setRequestProperty("REST-Framework-Version", "2");
            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
            //            AppsproConnection.LOG.info(response.toString());
            return response.toString();

        } catch (Exception e) {
            //           e.printStackTrace();// AppsproConnection.LOG.error("ERROR", e);
        }
        return "";
    }

    //    public String callSaaS2(String serverURL, String restFrameWorkVersion) {
    //        SSLContext ctx = null;
    //        System.setProperty("DUseSunHttpHandler", "true");
    //        TrustManager[] trustAllCerts =
    //            new X509TrustManager[] { new X509TrustManager() {
    //                public X509Certificate[] getAcceptedIssuers() {
    //                    return null;
    //                }
    //
    //                public void checkClientTrusted(X509Certificate[] certs,
    //                                               String authType) {
    //                }
    //
    //                public void checkServerTrusted(X509Certificate[] certs,
    //                                               String authType) {
    //                }
    //            } };
    //        try {
    //            ctx = SSLContext.getInstance("TLSv1.2");
    //            ctx.init(null, trustAllCerts, null);
    //        } catch (NoSuchAlgorithmException e) {
    ////            AppsproConnection.LOG.info("Error loading ssl context {}" +
    ////                               e.getMessage());
    //        } catch (KeyManagementException e) {
    //           e.printStackTrace();
    //           //AppsproConnection.LOG.error("ERROR", e);
    //        }
    //
    //        SSLContext.setDefault(ctx);
    //
    //        ClientConfig def = new DefaultClientConfig();
    //
    //        def.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
    //                                new HTTPSProperties(null, ctx));
    //
    //        Client client = Client.create(def);
    //
    //        client.addFilter(new HTTPBasicAuthFilter(RestHelper.USER_NAME,
    //                                                 RestHelper.PASSWORD));
    //        //  client.resource(serverURL).header("REST-Framework-Version", "2");
    //
    //        // resource.header("REST-Framework-Version", "2");
    //        serverURL = serverURL.replaceAll("\\s", "%20");
    //        WebResource resource = client.resource(serverURL);
    //        resource.header("REST-Framework-Version", "2");
    //
    //        return resource.accept(MediaType.APPLICATION_JSON_TYPE,
    //                               MediaType.APPLICATION_XML_TYPE).header("REST-Framework-Version",
    //                                                                      "2").get(String.class);
    //    }

    public Document httpPost(String destUrl,
                             String postData) throws Exception {
        //        AppsproConnection.LOG.info(postData);
        try{
//            System.out.println(postData);
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        //        AppsproConnection.LOG.info("connection status: " + http.getResponseCode() +
        //                           "; connection response: " +
        //                           http.getResponseMessage());
        InputStream in;
            System.err.println(http.getResponseCode());
        if (http.getResponseCode() == 200) {
             in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            //  AppsproConnection.LOG.info("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);


            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();


            return doc;
        } else {
            //            AppsproConnection.LOG.info("Failed");
            
            in = http.getErrorStream();
                if (in != null) {
                                InputStreamReader iReader = new InputStreamReader(in);
                                BufferedReader bReader = new BufferedReader(iReader);
                                String line;
                                String response = "";
                                while ((line = bReader.readLine()) != null) {
                                    response += line;
                                }
                                
                                System.out.println(response);
                                }
            }}catch(Exception e){
            e.printStackTrace();
            }


        return null;
    }

    public static String getAuth() {
        byte[] message = (USER_NAME + ":" + PASSWORD).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }
    //    public static void main (String [] args ){
    //    }

    public void setSecurityService(String SecurityService) {
        this.SecurityService = SecurityService;
    }

    public String getSecurityService() {
        return SecurityService;
    }

    public String getEmailBody(String reqType, String approvalName,
                               String reqName, String BeneficiaryName) {
        String body = "";
        if (reqType.equals("FYI")) {
            body =
"<h4> Dear " + approvalName + ",</h4>" + " <p>Kindly review for your information the below details: </p>" +
 "<p>Request Name: " + reqName + ",</p><p>  Beneficiary: " + BeneficiaryName +
 "</p>";

        } else if (reqType.equals("FYA")) {
            body =
"<h4> Dear " + approvalName + ", </h4>" + "<p>Your Action required for the  following:-  </p>" +
 "<p>Request Name :  " + reqName + "</p> <p> Beneficiary: " + BeneficiaryName +
 " ,</p> <p>Please connect to <a href=\\\"https://HCM.HHA.com.sa/\\\">https://HCM.HHA.com.sa </a></p>";
        }
        return body;
    }

    public String getEmailSubject(String reqType, String reqName,
                                  String BeneficiaryName) {
        String subject = "";
        if (reqType.equals("FYI")) {
            subject =
                    "FYI  Approval of  " + reqName + "for  " + BeneficiaryName;
        } else if (reqType.equals("FYA")) {
            subject =
                    "Action Required   Approval of  " + reqName + "for  " + BeneficiaryName;
        }
        return subject;
    }

    public void setDepartmentUrl(String departmentUrl) {
        this.departmentUrl = departmentUrl;
    }

    public String getDepartmentUrl() {
        return departmentUrl;
    }

    public void setEmployeeAnalysisUrl(String employeeAnalysisUrl) {
        this.employeeAnalysisUrl = employeeAnalysisUrl;
    }

    public String getEmployeeAnalysisUrl() {
        return employeeAnalysisUrl;
    }

}
