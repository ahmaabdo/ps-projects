/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.rest;

/**
 *
 * @author Hp
 */
import com.appspro.bsbc.getSAASData.RetrivePurchaseOrder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import org.json.JSONArray;

@Path("/appsProPaasRest")
public class AppsProPaasRest {

    @GET
    @Path("/getPurchaseOrder")
    public String getPurchaseOrder(@QueryParam("serviceContractNumber") String serviceContractNumber,
            @QueryParam("serviceRequestNumber") String serviceRequestNumber,
            @QueryParam("workOrderNumber") String workOrderNumber,
            @Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString) {

        RetrivePurchaseOrder retrivePurchaseOrder = new RetrivePurchaseOrder();

        JSONArray resposne = retrivePurchaseOrder.getPOData(serviceContractNumber, serviceRequestNumber, workOrderNumber);
        return resposne.toString();

    }

    @GET
    @Path("/getPORequistion")
    public String getPORequistionData(@QueryParam("serviceContractNumber") String serviceContractNumber,
            @QueryParam("serviceRequestNumber") String serviceRequestNumber,
            @QueryParam("workOrderNumber") String workOrderNumber,
            @Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @HeaderParam("Authorization") String authString) {

        RetrivePurchaseOrder retrivePurchaseOrder = new RetrivePurchaseOrder();

        JSONArray resposne = retrivePurchaseOrder.getPORequistionData(serviceContractNumber, serviceRequestNumber, workOrderNumber);
        return resposne.toString();

    }

}
