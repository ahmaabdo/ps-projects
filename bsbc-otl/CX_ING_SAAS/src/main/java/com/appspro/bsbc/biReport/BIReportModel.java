/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.biReport;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 *
 * @author CPBSLV
 */
public class BIReportModel extends RestHelper {

    public static enum REPORT_NAME {
        GET_PO_HEADER_LINES_REPORT("GET_PO_HEADER_LINES_REPORT"),
        PURCHASING_REQUISITION_REPORT("PURCHASING_REQUISITION_REPORT");
        private String value;

        private REPORT_NAME(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum ATTRIBUTE_TEMPLATE {

        ATTRIBUTE_TEMPLATE_XML("DEFAULT"),
        ATTRIBUTE_TEMPLATE_PDF("PDF");
        private String value;

        private ATTRIBUTE_TEMPLATE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum ATTRIBUTE_FORMAT {

        ATTRIBUTE_FORMAT_XML("xml"),
        ATTRIBUTE_FORMAT_PDF("pdf"); //keep in small caps
        private String value;

        private ATTRIBUTE_FORMAT(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum GET_PO_HEADER_LINES_REPORT_PARAM {
        SERVICE_CONTRACT_NUMBER("SERVICE_CONTRACT_NUMBER"),
        WORK_ORDER_NUMBER("WORK_ORDER_NUMBER"),
        SERVICE_REQUEST_NUMBER("SERVICE_REQUEST_NUMBER");
        
        private String value;

        private GET_PO_HEADER_LINES_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    private String reportAbsolutePath;
    private String attributeFormat;
    private String attributeTemplate;
    private String parameters;
    private Map<String, String> paramMap = new HashMap<String, String>();
    private String soapRequest = null;
    private String username = "scm.admin";
    private String password = "asdasd123";

    public void setParamMap(Map<String, String> paramMap) {
        this.paramMap = paramMap;
    }

    public Map<String, String> getParamMap() {
        return paramMap;
    }

    public void setReportAbsolutePath(String reportAbsolutePath) {
        this.reportAbsolutePath = reportAbsolutePath;
    }

    public String getReportAbsolutePath() {
        return reportAbsolutePath;
    }

    public String getParameters() {
        Iterator it = paramMap.entrySet().iterator();
        parameters = "";
        while (it.hasNext()) {
            Map.Entry parameter = (Map.Entry) it.next();
            parameters = parameters + "             <pub:item>\n"
                    + "                <pub:name>" + parameter.getKey()
                    + "</pub:name>\n"
                    + "                 <pub:values>\n"
                    + "                    <pub:item>" + parameter.getValue()
                    + "</pub:item>\n"
                    + "                  </pub:values>\n"
                    + "             </pub:item>\n";
        }
        return parameters;
    }

    public String getSoapRequest() {
        this.soapRequest
                = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pub=\"http://xmlns.oracle.com/oxp/service/PublicReportService\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <pub:runReport>\n"
                + "         <pub:reportRequest>\n"
                + "            <pub:attributeLocale>en-US</pub:attributeLocale>\n"
                + "            <pub:attributeTemplate>"
                + this.attributeTemplate + "</pub:attributeTemplate>\n"
                + "            <pub:attributeFormat>" + this.attributeFormat
                + "</pub:attributeFormat>\n"
                + "            <pub:reportAbsolutePath>/Custom/SAAS Extension Reports/Reports/"
                + reportAbsolutePath + ".xdo</pub:reportAbsolutePath>\n"
                + "<pub:parameterNameValues>\n"
                + " \n"
                + getParameters() + "             </pub:parameterNameValues>\n"
                + " \n"
                + "         </pub:reportRequest>\n"
                + "         <pub:userID>" + this.username + "</pub:userID>\n"
                + "         <pub:password>" + this.password
                + "</pub:password>\n"
                + "      </pub:runReport>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public String executeReports() {
        String output = "";
        try {
            System.setProperty("DUseSunHttpHandler", "true");

            byte[] buffer = new byte[getSoapRequest().length()];
            buffer = getSoapRequest().getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url = new URL(null, this.getBiReportUrl(), new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https = (HttpsURLConnection) url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                //https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = "";
            //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            System.out.println(getSoapRequest());
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            InputStream is = http.getInputStream();
            String responseXML = getStringFromInputStream(is);

            DocumentBuilder builder
                    = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(responseXML));

            Document doc = builder.parse(src);
            String data
                    = doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            } else {
                output = data;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public void setAttributeFormat(String attributeFormat) {
        this.attributeFormat = attributeFormat;
    }

    public void setAttributeTemplate(String attributeTemplate) {
        this.attributeTemplate = attributeTemplate;
    }

    public static JSONObject runReport(String reportName,Map<String,String> paramMap) {
        BIReportModel biPReports = new BIReportModel();
        biPReports.setAttributeFormat(BIReportModel.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIReportModel.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
        biPReports.setReportAbsolutePath(reportName);
        
        biPReports.setParamMap(paramMap);
        
        String fresponse = biPReports.executeReports();
        System.out.print(fresponse);
        JSONObject xmlJSONObj
                = XML.toJSONObject(fresponse.toString());
        
        System.out.println(xmlJSONObj);
        return xmlJSONObj;
    }
    
    public static void main(String arp[]) {
        
    }

}
