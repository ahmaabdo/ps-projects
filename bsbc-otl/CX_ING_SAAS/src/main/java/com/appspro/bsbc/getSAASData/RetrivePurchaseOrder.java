/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc.getSAASData;

import com.appspro.bsbc.biReport.BIReportModel;
import static com.appspro.bsbc.biReport.BIReportModel.runReport;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Hp
 */
public class RetrivePurchaseOrder {

    public JSONArray getPOData(String SERVICE_CONTRACT_NUMBER,String SERVICE_REQUEST_NUMBER,String WORK_ORDER_NUMBER) {
        
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put(BIReportModel.GET_PO_HEADER_LINES_REPORT_PARAM.SERVICE_CONTRACT_NUMBER.getValue(), SERVICE_CONTRACT_NUMBER);
        paramMap.put(BIReportModel.GET_PO_HEADER_LINES_REPORT_PARAM.SERVICE_REQUEST_NUMBER.getValue(), SERVICE_REQUEST_NUMBER);
        paramMap.put(BIReportModel.GET_PO_HEADER_LINES_REPORT_PARAM.WORK_ORDER_NUMBER.getValue(), WORK_ORDER_NUMBER);
        //"Con-0000002-181022"
        
        JSONObject json = runReport(BIReportModel.REPORT_NAME.GET_PO_HEADER_LINES_REPORT.getValue(), paramMap);
        
        JSONObject dataDS = json.getJSONObject("DATA_DS");
        JSONArray g1 = dataDS.getJSONArray("G_1");
        
        return g1;
    }
    
     public JSONArray getPORequistionData(String SERVICE_CONTRACT_NUMBER,String SERVICE_REQUEST_NUMBER,String WORK_ORDER_NUMBER) {
        
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put(BIReportModel.GET_PO_HEADER_LINES_REPORT_PARAM.SERVICE_CONTRACT_NUMBER.getValue(), SERVICE_CONTRACT_NUMBER);
        paramMap.put(BIReportModel.GET_PO_HEADER_LINES_REPORT_PARAM.SERVICE_REQUEST_NUMBER.getValue(), SERVICE_REQUEST_NUMBER);
        paramMap.put(BIReportModel.GET_PO_HEADER_LINES_REPORT_PARAM.WORK_ORDER_NUMBER.getValue(), WORK_ORDER_NUMBER);
        
        JSONObject json = runReport(BIReportModel.REPORT_NAME.PURCHASING_REQUISITION_REPORT.getValue(), paramMap);
        
        JSONObject dataDS = json.getJSONObject("DATA_DS");
        Object obj = dataDS.get("G_1");
        
        if(obj instanceof JSONArray) {
            JSONArray g1 = dataDS.getJSONArray("G_1");
            return g1;
        } else {
            JSONObject g1 = dataDS.getJSONObject("G_1");
            JSONArray ja = new JSONArray();
            
            ja.put(g1);
            return ja;
        }
    }
}
