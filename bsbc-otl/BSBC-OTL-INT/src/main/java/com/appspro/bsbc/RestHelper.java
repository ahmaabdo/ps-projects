/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RestHelper {

    public final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    /*
     * Prod URL : ejdu.fa.em2.oraclecloud.com.
     * Dev URL : ejdu-dev1.fa.em2.oraclecloud.com. 
     */
    public final static String SAAS_URL_HTTP = "ehxm.fa.us6.oraclecloud.com";
    public final static String SAAS_URL_IP = "https://" + SAAS_URL_HTTP;
    public final static String SAAS_URL = "https://" + SAAS_URL_HTTP;

    /**
     *
     * @param serverUrl Target Server URL
     * @param jwttoken JWT TOKEN for autharization
     * @param requestMethod GET/POST/DELETE/PUT
     * @param contentType ex : APPLICATION/JSON
     *
     * @return response as String buffer
     */
    public StringBuffer getDataFromRestAPI(String serverUrl, String jwttoken,
            String requestMethod,
            String contentType) {
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = SAAS_URL_IP;
            connection = (HttpsURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestMethod(requestMethod);
            connection.setRequestProperty("Content-Type", contentType);
            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            connection.addRequestProperty("Authorization", "Basic aGNtdXNlcjpPcmFjbGVAMTIz");

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getLovName(StringBuffer respone, String fieldName) {
        JSONObject objDep;
        try {
            objDep = new JSONObject(respone.toString());
            JSONArray arrDep = objDep.getJSONArray("items");
            for (int i1 = 0; i1 < arrDep.length(); i1++) {
                return arrDep.getJSONObject(i1).getString(fieldName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts
                = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }
            }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String[] getPersonNumberByDFF(String dff) {
        try {

            String respones = callGetRest("https://ehxm.fa.us6.oraclecloud.com:443//hcmRestApi/resources/11.13.17.11/emps?onlyData=true&expand=assignments.assignmentDFF&q=assignments.assignmentDFF.machineEmployeeNo=\'" + dff + "\'", null);

            JSONObject emp = new JSONObject(respones);
            JSONArray items = emp.getJSONArray("items");
            String[] ar = new String[4];
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = (JSONObject) items.get(i);
                JSONArray assignments = item.getJSONArray("assignments");
                ar[0] = item.getString("PersonNumber");
                for (int j = 0; j < assignments.length(); j++) {
                    JSONObject assignment = (JSONObject) assignments.get(i);
                    JSONArray assignmentsDFF = assignment.getJSONArray("assignmentDFF");
                    for (int k = 0; k < assignmentsDFF.length(); k++) {
                        JSONObject assignmentDFF = (JSONObject) assignmentsDFF.get(k);
//                        ar[0] = assignmentDFF.getLong("machineEmployeeNo") + "";
                        if (assignmentDFF.has("exceptFromTimeAttendance") && !assignmentDFF.isNull("exceptFromTimeAttendance")) {
                            ar[1] = assignmentDFF.getString("exceptFromTimeAttendance");
                        } else {
                            ar[1] = "No";
                        }

                        if (assignmentDFF.has("exceptFromTimeAttendance_Display") && !assignmentDFF.isNull("exceptFromTimeAttendance_Display")) {
                            ar[2] = assignmentDFF.getString("exceptFromTimeAttendance_Display");
                        } else {
                            ar[2] = "No";
                        }

                        if (assignmentDFF.has("attendanceType") && !assignmentDFF.isNull("attendanceType")) {
                            ar[3] = assignmentDFF.getString("attendanceType");
                        } else {
                            ar[3] = "No";
                        }

//                        if (assignmentDFF.has("shiftWithoutStartTime") && !assignmentDFF.isNull("shiftWithoutStartTime")) {
//                            ar[4] = assignmentDFF.getString("shiftWithoutStartTime");
//                        } else {
//                            ar[4] = "No";
//                        }

                    }
                }
            }
            return ar;
        } catch (Exception e) {
            return null;
        }
    }

    public static String callPostRest(String serverUrl, String jwttoken,
            String contentType, String body) {
        HttpsURLConnection https = null;
        HttpURLConnection conn = null;
        try {
            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", contentType);
            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            conn.addRequestProperty("Authorization", "Basic aGNtdXNlcjpPcmFjbGVAMTIz");

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes());
            os.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output = br.readLine();
            System.out.println(output);

            conn.disconnect();

            return output;
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    public static String callPutRest(String serverUrl, String jwttoken,
            String contentType, String body) {

        HttpsURLConnection https = null;
        HttpURLConnection conn = null;
        try {

            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", contentType);

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes());
            os.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output = br.readLine();

            conn.disconnect();

            return output;
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public static String callGetRest(String serverUrl, String id) {
        HttpsURLConnection https = null;
        HttpURLConnection conn = null;
        try {

            URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.addRequestProperty("Authorization", "Basic aGNtdXNlcjpPcmFjbGVAMTIz");
            conn.addRequestProperty("REST-Framework-Version", "2");
//            conn.setRequestProperty("id", id);

            BufferedReader in
                    = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

}
