/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc;



import org.apache.log4j.Logger;
import org.quartz.Job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


/**
 *
 * @author Hp
 */
public class QuartzJob implements Job {

    private Logger log = Logger.getLogger(QuartzJob.class);

    @Override
    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
        
            ReadBuildEmployeeAttnds.readAndPostData();
            System.out.println("Hello From QuartzJob!!!!");
    }
}
