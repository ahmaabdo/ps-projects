/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.quartz.CronScheduleBuilder;
import org.quartz.DateBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Web application lifecycle listener.
 *
 * @author DELL
 */
public class ReadDailyListener implements ServletContextListener {

    private ServletContext context = null;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        System.out.println("----- listeners initiated");
        try {
            JobDetail jobDaily = JobBuilder.newJob(QuartzJob.class).build();
            JobDetail jobDailyAgain = JobBuilder.newJob(QuartzJob.class).build();
            JobDetail jobWeekly = JobBuilder.newJob(QuartzJobWeekly.class).build();
            //Trigger the job to run on the next round minute
            Trigger triggerDaily = TriggerBuilder.newTrigger()
                    .withIdentity("Deliy-Job")
                    .withSchedule(
                            CronScheduleBuilder.dailyAtHourAndMinute(19, 15))
                    .forJob(jobDaily)
                    .build();
            Trigger triggerDailyAgain = TriggerBuilder.newTrigger()
                    .withIdentity("Deliy-Job-Again")
                    .withSchedule(
                            CronScheduleBuilder.dailyAtHourAndMinute(20, 30))
                    .forJob(jobDailyAgain)
                    .build();
            Trigger triggerWeekly = TriggerBuilder.newTrigger()
                    .withIdentity("Weekly-Job")
                    .withSchedule(
                            CronScheduleBuilder.weeklyOnDayAndHourAndMinute(6, 4, 5))
                    .forJob(jobWeekly)
                    .build();
            Scheduler sch = StdSchedulerFactory.getDefaultScheduler();
            sch.start();
            sch.scheduleJob(jobDaily, triggerDaily);
            sch.scheduleJob(jobDailyAgain, triggerDailyAgain);
            sch.scheduleJob(jobWeekly, triggerWeekly);
        } catch (SchedulerException e) {
            System.out.println("Error" + e);
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
