/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.SequenceInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.Sequence;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author applaudteam
 */
public class ReadBuildWeeklyEmployeeAttnds {

    /**
     */
    public final static String SOURCE_ID = "BSBC_DEVICE";
    public final static String DEVICE_ID = "BSBC_DEVICE";
    public final static String REPORTER_ID_TYPE = "PERSON";
    public final static String NAME = "PayrollTimeType";
    public final static String VALUE = "Regular Hours";
    public final static String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'+03:00'";
    public final static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_PATTERN);

    public enum TradingDays {
        SATURDAY(Calendar.SATURDAY),
        SUNDAY(Calendar.SUNDAY),
        Monday(Calendar.MONDAY),
        Tuesday(Calendar.TUESDAY),
        Wednesday(Calendar.WEDNESDAY),
        Thursday(Calendar.THURSDAY),
        Friday(Calendar.FRIDAY);

        private int value;

        private TradingDays(int value) {
            this.value = value;
        }
    };

    public static void readAndPostDataTimeCard() {
        InputStream fstream = null;
        OutputStream outputStream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        String renamedFile = null;
        boolean check = true;
        try {

            try {
                String host = System.getProperty("http.proxyHost");
                String post = System.getProperty("http.proxyPort");
                session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("@BSbc123456");
                if (host != null || post != null) {
                    ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                    session.setProxy(proxy);
                }
                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect();
                sftpChannel = (ChannelSftp) channel;
                sftpChannel.cd("/upload/DailyOTL");
                Vector filelist = sftpChannel.ls("/upload/DailyOTL");
                List<String> fileNameList = new ArrayList<String>();
                List<File> listFile = new ArrayList<File>();
                List<String> renameFiles = new ArrayList<String>();
                String fileName = null;
                for (int i = 0; i < filelist.size(); i++) {
                    LsEntry entry = (LsEntry) filelist.get(i);
                    if (entry.getFilename().startsWith("BSBCATTLOG")
                            || entry.getFilename().startsWith("OTLBSBCATTLOG")) {
                        fileName = entry.getFilename();
                        fileNameList.add(fileName);
                    }

                }
                if (fileNameList != null && !fileNameList.isEmpty()) {
                    for (int i = 0; i < fileNameList.size(); i++) {
                        fileSFTP = sftpChannel.get("/upload/DailyOTL/" + fileNameList.get(i));
                        renamedFile = fileNameList.get(i);
                        renameFiles.add(renamedFile);
                        // Open the file
                        String filename = "OTL";
                        File file = File.createTempFile(filename, ".zip");

                        // write the inputStream to a FileOutputStream
                        outputStream
                                = new FileOutputStream(file);

                        int read = 0;
                        byte[] bytes = new byte[2056];

                        while ((read = fileSFTP.read(bytes)) != -1) {
                            outputStream.write(bytes, 0, read);
                        }
                        listFile.add(file);

                    }
                    fstream = collectStream(listFile);
                    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

                    readAndBuildJson(br);
                } else {
                    check = false;
                }

            } catch (ParseException ex) {
                Logger.getLogger(ReadBuildWeeklyEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadBuildWeeklyEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadBuildWeeklyEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException ex) {
//            Logger.getLogger(ReadBuildWeeklyEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        } finally {
            try {

                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                if (session != null) {
                    session.setTimeout(1500);
                    session.disconnect();
                }
                if (check & fstream != null) {
                    fstream.close();
                }

            } catch (IOException ex) {
                Logger.getLogger(ReadBuildWeeklyEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JSchException ex) {
                Logger.getLogger(ReadBuildWeeklyEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
            }
            renameFiles();
        }
    }

    public static InputStream collectStream(List<File> files) throws FileNotFoundException {
        if (files == null || files.isEmpty()) {
            return null;
        }
        if (files.size() == 1) {
            return new FileInputStream(files.get(0));
        }
        List<File> fileList = new ArrayList<File>();
        for (int i = 1; i < files.size(); i++) {
            fileList.add(files.get(i));
        }
        return new SequenceInputStream(new FileInputStream(files.get(0)), collectStream(fileList));

    }

    public static void renameFiles() {

        InputStream fstream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        String renamedFile = null;
        boolean check = true;
        try {

            String host = System.getProperty("http.proxyHost");
            String post = System.getProperty("http.proxyPort");
            session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword("@BSbc123456");
            if (host != null || post != null) {
                ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                session.setProxy(proxy);
            }
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftpChannel = (ChannelSftp) channel;
            sftpChannel.cd("/upload/DailyOTL");
            Vector filelist = sftpChannel.ls("/upload/DailyOTL");
            List<String> fileNameList = new ArrayList<String>();
            List<File> listFile = new ArrayList<File>();
            List<String> renameFiles = new ArrayList<String>();
            String fileName = null;
            for (int i = 0; i < filelist.size(); i++) {
                LsEntry entry = (LsEntry) filelist.get(i);
                if (entry.getFilename().startsWith("BSBCATTLOG")
                        || entry.getFilename().startsWith("OTLBSBCATTLOG")) {
                    fileName = entry.getFilename();
                    fileNameList.add(fileName);
                }

            }
            if (fileNameList != null && !fileNameList.isEmpty()) {
                for (int i = 0; i < fileNameList.size(); i++) {
                    fileSFTP = sftpChannel.get("/upload/DailyOTL/" + fileNameList.get(i));
                    renamedFile = fileNameList.get(i);
                    renameFiles.add(renamedFile);
                }
                for (String renameFile : renameFiles) {
                    System.out.println(renameFiles);
                    System.out.println("Start For Loop");
                    sftpChannel.rename("/upload/DailyOTL/" + renameFile,
                            "/upload/WeeklyOTL/" + renameFile);
                    System.out.println("End For Loop");
                }
                sftpChannel.cd("/upload/");
                System.out.println(java.time.LocalDate.now());
            } else {
                check = false;
            }

        } catch (JSchException e) {
            e.printStackTrace();
        } //        } catch (ParseException ex) {
        //            Logger.getLogger(ReadBuildWeeklyEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        catch (SftpException e) {
            e.printStackTrace();
        } finally {
            try {

                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                if (session != null) {
                    session.disconnect();
                }
                if (check & fstream != null) {
                    fstream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReadBuildWeeklyEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static String readAndBuildJson(BufferedReader br) throws IOException, ParseException {
        Map<String, List<String[]>> mapSorte = new TreeMap<String, List<String[]>>();
        String strLine;
        Map<String, String[]> personNumberMapping = new HashMap<String, String[]>();
        JSONObject body = new JSONObject();

        //String requestTimestamp = SIMPLE_DATE_FORMAT.format(new Date());
        //body.put("requestTimestamp", requestTimestamp);
        //Read File Line By Line
        int xx = 0;

        int counter = 0;
        String[] dff;
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        Map<String, Map<Date, String>> checkNullsDays = new HashMap<String, Map<Date, String>>();
        try {
            while ((strLine = br.readLine()) != null) {

                Map<Date, String> weekMap = new HashMap<Date, String>();

                now.set(Calendar.DAY_OF_WEEK, TradingDays.SUNDAY.value);
                weekMap.put(now.getTime(), "N");
                now.set(Calendar.DAY_OF_WEEK, TradingDays.Monday.value);
                weekMap.put(now.getTime(), "N");
                now.set(Calendar.DAY_OF_WEEK, TradingDays.Tuesday.value);
                weekMap.put(now.getTime(), "N");
                now.set(Calendar.DAY_OF_WEEK, TradingDays.Wednesday.value);
                weekMap.put(now.getTime(), "N");
                now.set(Calendar.DAY_OF_WEEK, TradingDays.Thursday.value);
                weekMap.put(now.getTime(), "N");
                now.set(Calendar.DAY_OF_WEEK, TradingDays.Friday.value);
                weekMap.put(now.getTime(), "N");
                now.set(Calendar.DAY_OF_WEEK, TradingDays.SATURDAY.value);
                weekMap.put(now.getTime(), "N");

                if (strLine != null && strLine.contains("datetime to date")) {
                    continue;
                }
                if (!strLine.contains("~")) {
                    continue;
                }
                String[] parts = strLine.split("~");
                if (parts.length <= 0) {
                    continue;
                }
                String key = parts[0];

                if (mapSorte.get(key) != null) {
                    List<String[]> x = mapSorte.get(key);
                    if (!x.isEmpty()) {
                        String[] compareParts = x.get(x.size() - 1);
                        if (compareParts[1].equals(parts[1])) {
                            x.add(parts);
                        } else {
                            if (x.size() % 2 == 0) {
                                x.add(parts);
                            } else {
                                String[] emptyParts = new String[parts.length];
                                emptyParts[0] = parts[0];
                                x.add(emptyParts);
                                x.add(parts);
                            }
                        }

                    }

                } else {
                    mapSorte.put(key, new ArrayList<String[]>());
                    List<String[]> x = mapSorte.get(key);
                    x.add(parts);
                    checkNullsDays.put(key, weekMap);
                }
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date date = sdf.parse(parts[1]);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                if (checkNullsDays.get(key).get(cal.getTime()) != null) {
                    checkNullsDays.get(key).put(cal.getTime(), "Y");
                }

            }

            for (Map.Entry<String, List<String[]>> entry : mapSorte.entrySet()) {
                body.put("processInline", "Y");
                body.put("processMode", "TIME_SUBMIT");
                JSONArray timeEventsArray = new JSONArray();
                System.out.println(entry.getKey() + " => " + entry.getValue());
                List<String[]> x = entry.getValue();
                for (int index = 0; index < x.size(); index += 2) {
                    String[] parts = x.get(index);
                    String[] parts2 = null;
                    if ((index + 1) < x.size()) {
                        parts2 = x.get(index + 1);
                    }

                    if (personNumberMapping.get(parts[0].replaceAll("\"", "")) != null) {
                        dff = personNumberMapping.get(parts[0].replaceAll("\"", ""));
                    } else {
                        dff = RestHelper.getPersonNumberByDFF(parts[0].replaceAll("\"", ""));
                        personNumberMapping.put(parts[0].replaceAll("\"", ""), dff);
                    }

                    if (dff != null) {
                        if ("No".equalsIgnoreCase(dff[2])) {

                            JSONObject timeEventsBody = new JSONObject();
                            timeEventsBody.put("timeRecordId", "");
                            timeEventsBody.put("timeRecordVersion", "");
                            timeEventsBody.put("operationType", "ADD");
//                        String uuid = UUID.randomUUID().toString().replace("-", "");
//                        System.out.println("uuid = " + uuid);
//                        timeEventsBody.put("timeRecordEventRequestId", dff[0] + "" + ((int)(Math.random() * 101)));
                            timeEventsBody.put("startTime", SIMPLE_DATE_FORMAT.format(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(parts[1].replaceAll("\"", "") + " " + parts[2].replaceAll("\"", ""))));
//                        System.out.println(parts2);
                            if (parts2 != null && parts2[1] != null) {
                                timeEventsBody.put("stopTime", SIMPLE_DATE_FORMAT.format(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(parts2[1].replaceAll("\"", "") + " " + parts2[2].replaceAll("\"", ""))));
                            } else {
                                timeEventsBody.put("measure", 0);
                            }
//                        if (checkInMap.get(personNumber) != null) {
//                            if (checkInMap.get(personNumber) % 2 == 0) {
//                                checkInMap.put(personNumber, checkInMap.get(personNumber) + 1);
//                                timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_IN");
//                            } else {
//                                checkInMap.put(personNumber, checkInMap.get(personNumber) + 1);
//                                timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_OUT");
//                            }
//
//                        } else {
//                            checkInMap.put(personNumber, 1);
//                            timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_IN");
//                        }

                            timeEventsBody.put("reporterId", dff[0]);
                            timeEventsBody.put("reporterIdType", REPORTER_ID_TYPE);
//                        timeEventsBody.put("measure", 3);

                            JSONArray timeEventAttributesArray = new JSONArray();
                            JSONObject timeEventAttributesBody = new JSONObject();
                            timeEventAttributesBody.put("attributeName", NAME);
                            timeEventAttributesBody.put("attributeValue", VALUE);

                            timeEventAttributesArray.put(timeEventAttributesBody);

                            timeEventsBody.put("timeRecordEventAttribute", (Object) timeEventAttributesArray);
                            timeEventsArray.put(timeEventsBody);
                        }
                    }

//                xx++;
//                if(xx > 2) {
//                    break;
//                }
                }   //Close the input stream
                body.put(
                        "timeRecordEvent", (Object) timeEventsArray);
                System.out.println(body.toString());
                RestHelper.callPostRest("https://ehxm.fa.us6.oraclecloud.com:443/hcmRestApi/resources/latest/timeRecordEventRequests", null, "application/json", body.toString());
            }

            for (Map.Entry<String, Map<Date, String>> entry : checkNullsDays.entrySet()) {
                body.put("processInline", "Y");
                body.put("processMode", "TIME_SUBMIT");
                JSONArray timeEventsArray = new JSONArray();
                System.out.println(entry.getKey() + " => " + entry.getValue());
                Map<Date, String> x = entry.getValue();
//            for (int index = 0; index < x.size(); index += 2) {
                for (Map.Entry<Date, String> weekdates : x.entrySet()) {
                    if (!weekdates.getValue().equals("Y")) {
                        x.put(weekdates.getKey(), "T");
                        if (personNumberMapping.get(entry.getKey().replaceAll("\"", "")) != null) {
                            dff = personNumberMapping.get(entry.getKey().replaceAll("\"", ""));
                        } else {
                            dff = RestHelper.getPersonNumberByDFF(entry.getKey().replaceAll("\"", ""));
                            personNumberMapping.put(entry.getKey().replaceAll("\"", ""), dff);
                        }

                        if (dff != null) {
                            if ("No".equalsIgnoreCase(dff[2])) {

                                JSONObject timeEventsBody = new JSONObject();
                                timeEventsBody.put("timeRecordId", "");
                                timeEventsBody.put("timeRecordVersion", "");
                                timeEventsBody.put("operationType", "ADD");
//                        String uuid = UUID.randomUUID().toString().replace("-", "");
//                        System.out.println("uuid = " + uuid);
//                        timeEventsBody.put("timeRecordEventRequestId", dff[0] + "" + ((int)(Math.random() * 101)));
                                timeEventsBody.put("startTime", SIMPLE_DATE_FORMAT.format(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(weekdates.getKey().getTime()))));

//                        if (checkInMap.get(personNumber) != null) {
//                            if (checkInMap.get(personNumber) % 2 == 0) {
//                                checkInMap.put(personNumber, checkInMap.get(personNumber) + 1);
//                                timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_IN");
//                            } else {
//                                checkInMap.put(personNumber, checkInMap.get(personNumber) + 1);
//                                timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_OUT");
//                            }
//
//                        } else {
//                            checkInMap.put(personNumber, 1);
//                            timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_IN");
//                        }
                                timeEventsBody.put("reporterId", dff[0]);
                                timeEventsBody.put("reporterIdType", REPORTER_ID_TYPE);
                                timeEventsBody.put("measure", 0);

//                                timeEventsBody.put("referenceDate", );
                                JSONArray timeEventAttributesArray = new JSONArray();
                                JSONObject timeEventAttributesBody = new JSONObject();
                                timeEventAttributesBody.put("attributeName", NAME);
                                timeEventAttributesBody.put("attributeValue", VALUE);

                                timeEventAttributesArray.put(timeEventAttributesBody);

                                timeEventsBody.put("timeRecordEventAttribute", (Object) timeEventAttributesArray);
                                timeEventsArray.put(timeEventsBody);
                            }
                        }

//                xx++;
//                if(xx > 2) {
//                    break;
//                }
//                    }
                    }
                }   //Close the input stream
                body.put(
                        "timeRecordEvent", (Object) timeEventsArray);
                System.out.println(body.toString());
                RestHelper.callPostRest("https://ehxm.fa.us6.oraclecloud.com:443/hcmRestApi/resources/latest/timeRecordEventRequests", null, "application/json", body.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            br.close();
        }

        return body.toString();
    }

    public static void main(String[] args) throws Exception {
     //   readAndPostDataTimeCard();
    }
}
