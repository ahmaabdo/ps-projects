/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bsbc;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.SequenceInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.Sequence;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author applaudteam
 */
public class ReadBuildEmployeeAttnds {

    /**
     */
    public final static String SOURCE_ID = "BSBC_DEVICE";
    public final static String DEVICE_ID = "BSBC_DEVICE";
    public final static String REPORTER_ID_TYPE = "PERSON";
    public final static String NAME = "PayrollTimeType";
    public final static String VALUE = "Regular Hours";
    public final static String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'+03:00'";
    public final static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_PATTERN);

    public enum TradingDays {
        SATURDAY(Calendar.SATURDAY),
        SUNDAY(Calendar.SUNDAY),
        Monday(Calendar.MONDAY),
        Tuesday(Calendar.TUESDAY),
        Wednesday(Calendar.WEDNESDAY),
        Thursday(Calendar.THURSDAY),
        Friday(Calendar.FRIDAY);

        private int value;

        private TradingDays(int value) {
            this.value = value;
        }
    };

    public static void readAndPostData(){
        FileInputStream fstream = null;
        OutputStream outputStream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        String renamedFile = null;
        boolean check = true;
        try {

            try {
                String host = System.getProperty("http.proxyHost");
                String post = System.getProperty("http.proxyPort");
                session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("@BSbc123456");
                if (host != null || post != null) {
                    ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                    session.setProxy(proxy);
                }
                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect();
                sftpChannel = (ChannelSftp) channel;
                sftpChannel.cd("/upload");
                Vector filelist = sftpChannel.ls("/upload");
                List<String> fileNameList = new ArrayList<String>();
                String fileName = null;
                for (int i = 0; i < filelist.size(); i++) {
                    LsEntry entry = (LsEntry) filelist.get(i);
                    if (entry.getFilename().startsWith("BSBCATTLOG")
                            || entry.getFilename().startsWith("TestOTL")) {
                        fileName = entry.getFilename();
                        fileNameList.add(fileName);
                    }

                }
                if (fileNameList != null && !fileNameList.isEmpty()) {
                    for (int i = 0; i < fileNameList.size(); i++) {
                        fileSFTP = sftpChannel.get("/upload/" + fileNameList.get(i));
                        renamedFile = fileNameList.get(i);

                        // Open the file
                        String filename = "OTL";
                        File file = File.createTempFile(filename, ".zip");

                        // write the inputStream to a FileOutputStream
                        outputStream
                                = new FileOutputStream(file);

                        int read = 0;
                        byte[] bytes = new byte[2056];

                        while ((read = fileSFTP.read(bytes)) != -1) {
                            outputStream.write(bytes, 0, read);
                        }

                        fstream = new FileInputStream(file);
                        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

                        readAndBuildJsonTimeEvent(br);

                    }

                } else {
                    check = false;
                }

            } catch (ParseException ex) {
                Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException ex) {
//            Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        } finally {
            try {
                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                if (session != null) {
                    session.setTimeout(1500);
                    session.disconnect();
                }
                if (check & fstream != null) {
                    fstream.close();
                }

            } catch (IOException ex) {
                Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JSchException ex) {
                Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
            }
//            catch (InterruptedException ex) {
//                Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
//            }
            renameFile();
        }
    }

    public static void renameFile() {

        FileInputStream fstream = null;
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        InputStream fileSFTP = null;
        String renamedFile = null;
        boolean check = true;
        try {

            String host = System.getProperty("http.proxyHost");
            String post = System.getProperty("http.proxyPort");
            session = jsch.getSession("a5B8ZPYY", "sftp.us2.cloud.oracle.com", 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword("@BSbc123456");
            if (host != null || post != null) {
                ProxyHTTP proxy = new ProxyHTTP(host, Integer.parseInt(post));
                session.setProxy(proxy);
            }
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftpChannel = (ChannelSftp) channel;
            sftpChannel.cd("/upload");
            Vector filelist = sftpChannel.ls("/upload");
            List<String> fileNameList = new ArrayList<String>();
            String fileName = null;
            for (int i = 0; i < filelist.size(); i++) {
                LsEntry entry = (LsEntry) filelist.get(i);
                if (entry.getFilename().startsWith("BSBCATTLOG")
                        || entry.getFilename().startsWith("TestOTL")) {
                    fileName = entry.getFilename();
                    fileNameList.add(fileName);
                }

            }
            if (fileNameList != null && !fileNameList.isEmpty()) {
                for (int i = 0; i < fileNameList.size(); i++) {
                    fileSFTP = sftpChannel.get("/upload/" + fileNameList.get(i));
                    renamedFile = fileNameList.get(i);

                    sftpChannel.rename("/upload/" + renamedFile,
                            "/upload/DailyOTL/" + renamedFile);
                    sftpChannel.cd("/upload/");
                }

            } else {
                check = false;
            }

        } catch (JSchException e) {
            e.printStackTrace();
        } //        } catch (ParseException ex) {
        //            Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
        catch (SftpException e) {
            e.printStackTrace();
        } finally {
            try {

                if (sftpChannel != null) {
                    sftpChannel.exit();
                }
                if (session != null) {
                    session.disconnect();
                }
                if (check & fstream != null) {
                    fstream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReadBuildEmployeeAttnds.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static String readAndBuildJsonTimeEvent(BufferedReader br) throws IOException, ParseException {
        Map<String, List<String[]>> mapSorte = new TreeMap<String, List<String[]>>();
        String strLine;
        Map<String, String[]> personNumberMapping = new HashMap<String, String[]>();

        String[] dff;
        try {
            while ((strLine = br.readLine()) != null) {

                if (strLine != null && strLine.contains("datetime to date")) {
                    continue;
                }
                if (!strLine.contains("~")) {
                    continue;
                }
                String[] parts = strLine.split("~");
                if (parts.length <= 0) {
                    continue;
                }
                String key = parts[0];
                System.out.println(parts[1]);

//                String shift = checkShift(key);
//                System.out.println("Hellloooo" + "  " + shift);
                if (mapSorte.get(key) != null) {
                    List<String[]> x = mapSorte.get(key);

                    x.add(parts);
                } else {
                    mapSorte.put(key, new ArrayList<String[]>());
                    List<String[]> x = mapSorte.get(key);
                    x.add(parts);

                }

            }
            for (Map.Entry<String, List<String[]>> entry : mapSorte.entrySet()) {
                System.out.println(entry.getKey() + " => " + entry.getValue());
                List<String[]> x = entry.getValue();
                List<String> addPunch = new ArrayList<String>();
                JSONObject body = new JSONObject();
                final String uuid = UUID.randomUUID().toString().replace("-", "");
                System.out.println("uuid = " + uuid);
                body.put("requestNumber", uuid);
                body.put("sourceId", SOURCE_ID);
                String requestTimestamp = SIMPLE_DATE_FORMAT.format(new Date());

                body.put("requestTimestamp", requestTimestamp);
                //String requestTimestamp = SIMPLE_DATE_FORMAT.format(new Date());
                String shift = checkShift(entry.getKey());
                //body.put("requestTimestamp", requestTimestamp);
                //Read File Line By Line
                JSONArray timeEventsArray = new JSONArray();
                for (int index = 0; index < x.size(); index++) {
                    String[] parts = x.get(index);

                    if (shift != null) {
                        if (shift.equalsIgnoreCase("shift Without StartTime")) {
//                            addPunch.add(entry.getKey());
//                            addPunch.add(parts[1]);
//                            addPunch.add("8:30:5");
//                            addPunch.add(parts[3]);
//                            String[] stringArray = addPunch.toArray(new String[0]);
//                            x.add(0, stringArray);
//                            shift = null;
                            addPunch.add(entry.getKey());
                            addPunch.add(parts[1]);
//                            String StartTimes = "8:30:5";
//                            String EndTimes = parts[2];
//                            String startTimeParse[] = StartTimes.split(":");
//                            String endTimeParse[] = EndTimes.split(":");
//                            int firstHour = Integer.parseInt(startTimeParse[0]);
//                            int firstMinute = Integer.parseInt(startTimeParse[1]);
//                            int secondHour = Integer.parseInt(endTimeParse[0]);
//                            int secondMinute = Integer.parseInt(endTimeParse[1]);
//                            if (firstHour < secondHour) {
//                                addPunch.add("8:30:5");
//                                addPunch.add(parts[3]);
//                                String[] stringArray = addPunch.toArray(new String[0]);
//                                x.add(0, stringArray);
//                            } 
//                            else if (firstHour == secondHour) {
//                                if (firstMinute <= secondMinute) {
//                                    addPunch.add("8:30:5");
//                                    addPunch.add(parts[3]);
//                                    String[] stringArray = addPunch.toArray(new String[0]);
//                                    x.add(0, stringArray);
//                                }
//                            }
                            addPunch.add("8:30:5");
                            addPunch.add(parts[3]);
                            String[] stringArray = addPunch.toArray(new String[0]);
                            x.add(0, stringArray);
                            shift = null;
                        } else if (shift.equalsIgnoreCase("shift Without EndTime") && index == x.size() - 1) {
                            addPunch.add(entry.getKey());
                            addPunch.add(parts[1]);
//                            String StartTimes = "17:30:55";
//                            String EndTimes = parts[2];
//                            String startTimeParse[] = StartTimes.split(":");
//                            String endTimeParse[] = EndTimes.split(":");
//                            int firstHour = Integer.parseInt(startTimeParse[0]);
//                            int firstMinute = Integer.parseInt(startTimeParse[1]);
//                            int secondHour = Integer.parseInt(endTimeParse[0]);
//                            int secondMinute = Integer.parseInt(endTimeParse[1]);
//                            if (firstHour > secondHour) {
//                                addPunch.add("17:30:55");
//                                addPunch.add(parts[3]);
//                                String[] stringArray = addPunch.toArray(new String[0]);
//                                x.add(x.size(), stringArray);
//                            }
//                            else if (firstHour == secondHour) {
//                                if (firstMinute >= secondMinute) {
//                                    addPunch.add("17:30:55");
//                                    addPunch.add(parts[3]);
//                                    String[] stringArray = addPunch.toArray(new String[0]);
//                                    x.add(x.size(), stringArray);
//                                }
//                            }
                            addPunch.add("17:30:55");
                            addPunch.add(parts[3]);
                            String[] stringArray = addPunch.toArray(new String[0]);
                            x.add(x.size(), stringArray);
                            shift = null;
                        }
                    }
//                String[] parts2 = null;
//                if ((index + 1) < x.size()) {
//                    parts2 = x.get(index + 1);
//                }
                    parts = x.get(index);

                    if (personNumberMapping.get(parts[0].replaceAll("\"", "")) != null) {
                        dff = personNumberMapping.get(parts[0].replaceAll("\"", ""));
                    } else {
                        dff = RestHelper.getPersonNumberByDFF(parts[0].replaceAll("\"", ""));
                        personNumberMapping.put(parts[0].replaceAll("\"", ""), dff);
                    }
                    System.out.println(dff[0]);
                    System.out.println(dff[2]);
                    System.out.println(dff[3]);
                    if (dff != null) {
                        if ("No".equalsIgnoreCase(dff[2])) {
                            JSONObject timeEventsBody = new JSONObject();
                            timeEventsBody.put("deviceId", DEVICE_ID);
                            timeEventsBody.put("eventDateTime", SIMPLE_DATE_FORMAT.format(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(parts[1].replaceAll("\"", "") + " " + parts[2].replaceAll("\"", ""))));
                            if (index == 0) {
                                timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_IN");
                            } else if (index == x.size() - 1) {
                                timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_OUT");
                            }

                            timeEventsBody.put("reporterId", dff[0]);
                            timeEventsBody.put("reporterIdType", REPORTER_ID_TYPE);

//                        if (checkInMap.get(personNumber) != null) {
//                            if (checkInMap.get(personNumber) % 2 == 0) {
//                                checkInMap.put(personNumber, checkInMap.get(personNumber) + 1);
//                                timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_IN");
//                            } else {
//                                checkInMap.put(personNumber, checkInMap.get(personNumber) + 1);
//                                timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_OUT");
//                            }
//
//                        } else {
//                            checkInMap.put(personNumber, 1);
//                            timeEventsBody.put("supplierDeviceEvent", "BSBC_DEVICE_IN");
//                        }
                            timeEventsBody.put("reporterId", dff[0]);
                            timeEventsBody.put("reporterIdType", REPORTER_ID_TYPE);
//                        timeEventsBody.put("measure", 3);

                            JSONArray timeEventAttributesArray = new JSONArray();
                            JSONObject timeEventAttributesBody = new JSONObject();
                            timeEventAttributesBody.put("name", NAME);
                            timeEventAttributesBody.put("value", VALUE);

                            timeEventAttributesArray.put(timeEventAttributesBody);

                            timeEventsBody.put("timeEventAttributes", (Object) timeEventAttributesArray);
                            timeEventsArray.put(timeEventsBody);
                        }
                    }

//                xx++;
//                if(xx > 2) {
//                    break;
//                }
                }   //Close the input stream
                body.put(
                        "timeEvents", (Object) timeEventsArray);
                System.out.println(body.toString());
                RestHelper.callPostRest("https://ehxm.fa.us6.oraclecloud.com:443/hcmRestApi/resources/latest/timeEventRequests", null, "application/json", body.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        br.close();

        return null;
    }

    public static String checkShift(String key) {
        String[] dff = RestHelper.getPersonNumberByDFF(key.replaceAll("\"", ""));
        if (dff[0] != null) {
            if (dff[3].equalsIgnoreCase("No") || dff[3].equalsIgnoreCase("Normal")) {
                return "shift Without EndTime or StartTime";
            } else if (dff[3].equalsIgnoreCase("No Out")) {
                return "shift Without EndTime";
            } else if (dff[3].equalsIgnoreCase("No IN")) {
                return "shift Without StartTime";
            }

        }
        return null;
    }

    public static void main(String[] args) throws Exception {

       // readAndPostData();
//        String StartTimes = "13:00";
//        String EndTimes = "01:00:55";
//        String startTimeParse[] = StartTimes.split(":");
//        String endTimeParse[] = EndTimes.split(":");
//        int firstHour = Integer.parseInt(startTimeParse[0]);
//        int firstMinute = Integer.parseInt(startTimeParse[1]);
//        int x = firstHour + firstMinute;
//        int secondHour = Integer.parseInt(endTimeParse[0]);
//        int secondMinute = Integer.parseInt(endTimeParse[1]);
//        int y = secondHour + secondMinute;
//        if (x > y) {
//            System.out.println("True");
//        } else {
//            System.out.println("False");
//        }
    }

}
