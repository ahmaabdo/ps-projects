package com.appspro.nhcReceipt.dao;

import biPReports.RestHelper;

import com.appspro.nhcReceipt.bean.EmployeeBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class EmployeesDAO extends RestHelper {

    public JSONArray getAllEmployees() throws MalformedURLException,
                                              IOException {

        JSONArray responseArr = new JSONArray();
        JSONObject returnresponse = new JSONObject();

        String serverUrl = getInstanceUrl() + getAllEmployeesUrl();
        
        returnresponse =  httpGet(serverUrl);

        JSONArray arr = returnresponse.getJSONArray("items");
        for (int i = 0; i < arr.length(); i++) {
            
            JSONObject reponseObj = new JSONObject();
            JSONObject projectObj = arr.getJSONObject(i);
            reponseObj.put("DisplayName", projectObj.getString("DisplayName"));
            reponseObj.put("PersonNumber", projectObj.getString("PersonNumber"));
            reponseObj.put("PersonId", projectObj.getLong("PersonId"));
            reponseObj.put("UserName", projectObj.has("UserName") ? projectObj.get("UserName").toString() : null);

            responseArr.put(reponseObj);
        }

        return responseArr;
    }

    public static JSONObject getEmployeeDetails(String username) throws Exception {
        EmployeeBean obj = new EmployeeBean();
        JSONObject resp = null;

        String payload =
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/types/\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <typ:findUserDetailsByUsername>\n" +
            "         <typ:Username>" + username + "</typ:Username>\n" +
            "      </typ:findUserDetailsByUsername>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";

        Document doc =  new RestHelper().httpPost(new RestHelper().getInstanceUrl() +  "/hcmService/UserDetailsServiceV2?invoke=", payload);

        doc.getDocumentElement().normalize();
        Element root = doc.getDocumentElement();
        if (root.getElementsByTagName("ns1:Value").getLength() != 0) {
            obj.setPersonId(String.valueOf(root.getElementsByTagName("ns1:PersonId").item(0).getTextContent()));
            obj.setUserName(root.getElementsByTagName("ns1:Username").item(0).getTextContent());
            obj.setDisplayName(root.getElementsByTagName("ns1:DisplayName").item(0).getTextContent());
            obj.setPersonNumber(root.getElementsByTagName("ns1:PersonNumber").item(0).getTextContent());

            resp = new JSONObject(obj);

        } else {
            resp = new JSONObject(obj);
        }

        return resp;
    }
}
