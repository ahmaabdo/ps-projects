package com.appspro.filter;

import com.appspro.db.CommonConfigReader;

import java.lang.reflect.Method;

import java.util.List;
import io.jsonwebtoken.Jwts;

import java.util.StringTokenizer;

import javax.annotation.Priority;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;

import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;


@Provider
@JWTTokenNeeded
@Priority(Priorities.AUTHENTICATION)
public class RestAuthenticationFilter implements ContainerRequestFilter {


//    @Context
//    private ResourceInfo resourceInfo;
    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Basic";
    private static final Response ACCESS_DENIED =
        Response.status(Response.Status.UNAUTHORIZED).build();
    private static final Response ACCESS_FORBIDDEN =
        Response.status(Response.Status.FORBIDDEN).build();

    public void filter(ContainerRequestContext containerRequestContext) {

            //Get request headers
            final MultivaluedMap<String, String> headers =
                containerRequestContext.getHeaders();
        
            //Fetch authorization header
            final List<String> authorization =
                headers.get(AUTHORIZATION_PROPERTY);
            ;
            if (containerRequestContext.getMethod().equals("OPTIONS")) {
                return;
            }
            ;
            //If no authorization information present; block access
            if (authorization == null || authorization.isEmpty()) {
                containerRequestContext.abortWith(ACCESS_DENIED);
                return;
            }

            //Get encoded username and password
            final String encodedUserPassword =
                authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ",
                                                  "");
            
            System.out.println(encodedUserPassword);
            
            //Decode username and password
            String usernameAndPassword =
                new String(Base64.decodeBase64(encodedUserPassword));

            //Split username and password tokens
            System.out.println(usernameAndPassword);
            
            final StringTokenizer tokenizer =
            new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();
            
        System.out.println(username);
        System.out.println(password);
            if (!isUserAllowed(username, password)) {
                containerRequestContext.abortWith(ACCESS_DENIED);
                return;
            } else {
                List<String> jwtList = headers.get("xxx_data_decode");
                try {


                    if (jwtList == null || jwtList.isEmpty()) {
                        containerRequestContext.abortWith(ACCESS_DENIED);
                        return;
                    }


                    // Validate the token
                    Jwts.parser().setSigningKey("P@$$w0rdOW".getBytes()).parseClaimsJws(jwtList.get(0));

                    System.out.println("#### valid token : " + jwtList.get(0));
                    ;
                } catch (Exception e) {
                    System.out.println("#### invalid token : " +
                                       jwtList.get(0));
                    containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                }
            }
        
        


    }


    private boolean isUserAllowed(final String username,
                                  final String password) {
        boolean isAllowed = false;
        if (username.equals(CommonConfigReader.getValue("USER_NAME")) &&
            password.equals(CommonConfigReader.getValue("PASSWORD"))) {
          
            isAllowed = true;
        } else {
            isAllowed = false;
        }
        return isAllowed;
    }
    
    public static void main(String args[]){
        
        
            byte[] decoded = Base64.decodeBase64("Ui5haG1lZEBuaGMuc2E6QXBwc0AxMjM");
            String decodedString = new String(decoded);
           // return decodedString;
            
            String usernameAndPassword =
                new String(DatatypeConverter.parseBase64Binary("Ui5haG1lZEBuaGMuc2E6QXBwc0AxMjM"));

            //Split username and password tokens
            System.out.println(decodedString);
        }
}


