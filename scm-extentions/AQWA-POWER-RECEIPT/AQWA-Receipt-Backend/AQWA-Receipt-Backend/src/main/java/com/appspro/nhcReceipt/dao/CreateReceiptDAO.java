package com.appspro.nhcReceipt.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.nhcReceipt.bean.CreateReceiptBean;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.URL;

import java.text.SimpleDateFormat;

import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class CreateReceiptDAO extends RestHelper {
  
    public JSONObject cerateReceipt (CreateReceiptBean bean) throws Exception {
        
        JSONObject responseObj = new JSONObject();
        String res = "";
                
        String payload = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/receiving/receiptsInterface/transactions/processorServiceV2/types/\" xmlns:proc=\"http://xmlns.oracle.com/apps/scm/receiving/receiptsInterface/transactions/processorServiceV2/\">\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:processor>\n" + 
        "         <typ:Receipt>\n" + 
        "            <proc:ASNType>STD</proc:ASNType>\n" + 
        "            <proc:ReceiptNumber>"+bean.getReceiptNumber()+"</proc:ReceiptNumber>\n" + 
        "            <proc:ReceiptSourceCode>VENDOR</proc:ReceiptSourceCode>\n" + 
        "            <proc:TransactionType>NEW</proc:TransactionType>\n" + 
        "            <proc:VendorName>"+bean.getVendorName()+"</proc:VendorName>\n" + 
        "            <proc:VendorSiteCode>"+bean.getVendorSiteCode()+"</proc:VendorSiteCode>\n" + 
        "            <proc:BusinessUnit>"+bean.getBusinessUnit()+"</proc:BusinessUnit>\n" + 
        "            <proc:EmployeeId>"+bean.getEmployeeId()+"</proc:EmployeeId>\n" + 
        "            <!--Zero or more repetitions:-->\n";
        String footerPostData =                 
        "         </typ:Receipt>\n" + 
        "      </typ:processor>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>\n" ;
        
        StringBuilder postData = new StringBuilder();
        postData.append(payload);
        
        for (int i = 0 ; i < bean.getLines().length() ; i++){
            String linePostData =
            "            <proc:StagedReceivingTransaction>\n" + 
            "               <proc:TransactionType>RECEIVE</proc:TransactionType>\n" + 
            "               <proc:AutoTransactCode>DELIVER</proc:AutoTransactCode>\n" + 
            "               <proc:SourceDocumentCode>PO</proc:SourceDocumentCode>\n" + 
            "               <proc:DocumentNumber>"+bean.getLines().getJSONObject(i).get("documentNumber")+"</proc:DocumentNumber>\n" +
            "               <proc:POHeaderId>"+bean.getLines().getJSONObject(i).get("poHeaderId")+"</proc:POHeaderId>\n" +
            "               <proc:DocumentLineNumber>"+bean.getLines().getJSONObject(i).get("documentLineNumber")+"</proc:DocumentLineNumber>\n" + 
            "               <proc:Amount currencyCode=\"SAR\">"+bean.getLines().getJSONObject(i).get("amount")+"</proc:Amount>\n" + 
            "               <proc:Quantity unitCode=\"\">"+bean.getLines().getJSONObject(i).get("quantity")+"</proc:Quantity>\n" + 
            "               <proc:Comments>"+bean.getLines().getJSONObject(i).get("comment")+"</proc:Comments>\n" + 
            "               <proc:ToOrganizationCode>"+bean.getLines().getJSONObject(i).get("toOrganizationCode")+"</proc:ToOrganizationCode>\n" + 
            "               <proc:TransactionDate>"+currentOracleDate()+"</proc:TransactionDate>\n" +
            "            </proc:StagedReceivingTransaction>\n";
            postData.append(linePostData);
        }
        
        postData.append(footerPostData);
        System.out.println(postData);
        Document doc = httpPost(getInstanceUrl() + getCreateReceipt(), postData.toString());
        
        try {
            NodeList nl = doc.getElementsByTagName("ns1:Status");
            if(doc.getElementsByTagName("ns1:Status").item(0).getTextContent().equals("SUCCESS")){
                res = doc.getElementsByTagName("ns1:Status").item(0).getTextContent();
                responseObj.put("Status",res );
                responseObj.put("ReceiptNumber",bean.getReceiptNumber());
            }
            else{
                res = doc.getElementsByTagName("ns1:ErrorMessage").item(0).getTextContent();
                responseObj.put("Error",res );
                responseObj.put("Status","ERROR" );
                responseObj.put("ReceiptNumber",bean.getReceiptNumber());
            }
            System.out.println(res);
        } catch (NullPointerException e) {
            e.printStackTrace();
            res = e.getMessage();
        }
        return responseObj;
    }
    
    
    public JSONArray cerateReceiptRest (String  body) throws Exception {
        String restURL = getInstanceUrl() + getCreateReceiptRest();
        JSONObject obj = new JSONObject();
        JSONArray resArr = new JSONArray();
        
        obj = httpPostRest(restURL, body);
                
        String status = obj.getJSONObject("data").get("ReturnStatus").toString();
        if(status.equals("ERROR")){
            JSONArray arr = obj.getJSONObject("data").getJSONArray("lines");
            for(int i = 0 ; i < arr.length() ; i++){
                JSONObject lineObj = arr.getJSONObject(i);
                JSONArray linksArr = lineObj.getJSONArray("links");
                for(int j = 0 ; j < linksArr.length() ; j++){
                    JSONObject linksObj = linksArr.getJSONObject(j);
                    if(linksObj.get("name".toString()).equals("processingErrors")){
                        JSONObject ob = new JSONObject();
                        String errorUrl = linksObj.get("href").toString();
                        JSONObject errorObj = httpGet(errorUrl);
                        String errorMessage = errorObj.getJSONArray("items").getJSONObject(0).get("ErrorMessage").toString();
                        ob.put("errorMessage",errorMessage);
                        resArr.put(ob);
                    }
                }
            }
        }
        else{
            String receiptNumber = obj.getJSONObject("data").get("ReceiptNumber").toString();
            JSONObject ob = new JSONObject();
            ob.put("receiptNumber",receiptNumber);
            resArr.put(ob);
        }
        return resArr;    
    }
    
    public JSONArray uploadFileUCM (JSONArray attachmentArr) throws Exception {
        JSONArray documentArr = new JSONArray();
//        JSONArray attachmentArr = new JSONArray(bodyArr);
        String destUrl = getInstanceUrl() + getFinanceUrl();
                
        for (int i = 0 ; i < attachmentArr.length() ; i++){
            JSONObject docObj = new JSONObject();
            JSONObject attchObj = attachmentArr.getJSONObject(i);
            String postData =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/types/\" xmlns:fin=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <typ:uploadFileToUcm>\n" +
                "         <typ:document>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:Content>" + attchObj.get("content").toString() + "</fin:Content>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:FileName>"+ attchObj.get("title").toString() +"</fin:FileName>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:DocumentTitle>Sample File1</fin:DocumentTitle>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:DocumentAuthor>SCM Team</fin:DocumentAuthor>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:DocumentSecurityGroup>FAFusionImportExport</fin:DocumentSecurityGroup>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:DocumentAccount>scm/receivingReceipt/import$</fin:DocumentAccount>\n" +
                "         </typ:document>\n" +
                "      </typ:uploadFileToUcm>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>\n";
            
                Document doc = httpPost(destUrl, postData);
                
                docObj.put("url","https://eghj-test.fa.em2.oraclecloud.com/cs/idcplg?IdcService=GET_FILE&dID=" + doc.getElementsByTagName("result").item(0).getTextContent());
                docObj.put("poNum",attchObj.get("poNum").toString());
                docObj.put("lineNum",attchObj.get("lineNum").toString());
                documentArr.put(docObj);
            }
                       
        return documentArr;
    }
}
