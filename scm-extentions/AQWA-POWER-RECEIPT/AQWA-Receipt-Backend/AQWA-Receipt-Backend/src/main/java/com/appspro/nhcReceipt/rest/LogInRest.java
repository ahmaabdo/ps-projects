/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.nhcReceipt.rest;

import biPReports.RestHelper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.appspro.login.Login;

import java.io.IOException;

import java.security.NoSuchAlgorithmException;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Anas Alghawi
 */
@Path("/login")
public class LogInRest {

       // @Path("/login2")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response getEmpDetails(String body, @Context
        HttpServletRequest request) throws NoSuchAlgorithmException {
        String jsonInString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JSONObject resultResponse = new JSONObject();
            JsonNode actualObj = mapper.readTree(body);
            String userName = actualObj.get("userName").asText();
            String password = actualObj.get("password").asText();
            Login login = new Login();
            boolean result = login.validateLogin(userName, password);
            jsonInString = "{\"result\":" + result + "}";
            System.out.println(jsonInString);
            if(result){
                   String token = issueToken(RestHelper.decodeBase64(userName));
                    resultResponse.put("JWT", token); 
                    resultResponse.put("result", result); 
                }
            return Response.ok(resultResponse.toString()).build();
        } catch (Exception e) {
            e.printStackTrace(); 
            return Response.status(500).entity(e.getMessage()).build();
        }

    }

    public LogInRest() {
        super();
    }
    @POST
        @Consumes("application/json")
        @Path("/generatetoken")
        public Response generateToken(String body, @Context
            HttpServletRequest request, @Context
            HttpServletResponse response, @HeaderParam("Authorization")  String authString) {
            JSONObject json = new JSONObject();
            try {
                JSONObject jwtJson = new JSONObject(body);
                String saasJWT = jwtJson.getString("jwt");
                String[] split_string = saasJWT.split("\\.");
                String base64EncodedBody = split_string[1];
                String tokenBody = RestHelper.decodeBase64(base64EncodedBody);
                System.out.println("JWT Body : " + tokenBody);
                JSONObject paasTokernJSON = new JSONObject(tokenBody);
                String username = paasTokernJSON.getString("sub");
                String token = issueToken(username);
                json.put("jwt", token);
                return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
            } catch (Exception e) {
                e.printStackTrace();
                return Response.status(500).entity(e.getMessage()).build();
            }
        }
    
    private static String issueToken(String username) throws NoSuchAlgorithmException {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.MINUTE, 30);
            String jwtToken = Jwts.builder().setSubject(username).setIssuer("Abdullah Oweidi").setIssuedAt(new Date()).setExpiration(calendar.getTime()).signWith(SignatureAlgorithm.HS512,"P@$$w0rdOW".getBytes()).compact();
            System.out.println("#### generating token for a key : " + jwtToken);
            
            System.out.println( "=================");
            System.out.println( jwtToken);
            return jwtToken;
        }
    
    
    public static void main(String [] args){


        try {
            
            System.out.println(issueToken("Loay"));
            ;
        } catch (NoSuchAlgorithmException e) {
        }
    }

}

