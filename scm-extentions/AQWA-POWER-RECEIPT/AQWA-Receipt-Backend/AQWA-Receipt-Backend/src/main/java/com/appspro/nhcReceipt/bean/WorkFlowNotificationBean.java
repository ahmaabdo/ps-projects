package com.appspro.ttcLG.bean;

public class WorkFlowNotificationBean {
    private int id;
    private String empId;
    private String empName;
    private String approvalStatus;
    private String cancel;
    private String lgStatus;
    private String amount;
    private String contractAmount;
    private String contractNumber;
    private String supplier;
    private String contractStartDate;
    private String contractEndDate;
    private String region;
    private String description;
    private String lgEndDate;
    private String lgType;
    private String lgNumFromBank;
    private int documentNumber;
    private String createdBy;
    private String lgStartDate;
    private String negotiationNumber;
    private String negotiationTitle;
    private String negotiationAmount;
    

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getCancel() {
        return cancel;
    }

    public void setLgStatus(String lgStatus) {
        this.lgStatus = lgStatus;
    }

    public String getLgStatus() {
        return lgStatus;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setContractAmount(String contractAmount) {
        this.contractAmount = contractAmount;
    }

    public String getContractAmount() {
        return contractAmount;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setContractStartDate(String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getContractStartDate() {
        return contractStartDate;
    }

    public void setContractEndDate(String contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public String getContractEndDate() {
        return contractEndDate;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setLgEndDate(String lgEndDate) {
        this.lgEndDate = lgEndDate;
    }

    public String getLgEndDate() {
        return lgEndDate;
    }

    public void setLgType(String lgType) {
        this.lgType = lgType;
    }

    public String getLgType() {
        return lgType;
    }

    public void setLgNumFromBank(String lgNumFromBank) {
        this.lgNumFromBank = lgNumFromBank;
    }

    public String getLgNumFromBank() {
        return lgNumFromBank;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setDocumentNumber(int documentNumber) {
        this.documentNumber = documentNumber;
    }

    public int getDocumentNumber() {
        return documentNumber;
    }

    public void setLgStartDate(String lgStartDate) {
        this.lgStartDate = lgStartDate;
    }

    public String getLgStartDate() {
        return lgStartDate;
    }

    public void setNegotiationNumber(String negotiationNumber) {
        this.negotiationNumber = negotiationNumber;
    }

    public String getNegotiationNumber() {
        return negotiationNumber;
    }

    public void setNegotiationTitle(String negotiationTitle) {
        this.negotiationTitle = negotiationTitle;
    }

    public String getNegotiationTitle() {
        return negotiationTitle;
    }

    public void setNegotiationAmount(String negotiationAmount) {
        this.negotiationAmount = negotiationAmount;
    }

    public String getNegotiationAmount() {
        return negotiationAmount;
    }
}
