package com.appspro.nhcReceipt.bean;

import org.json.JSONArray;

public class CreateReceiptBean {
   
    private String asnType;
    private String receiptSourceCode;
    private String transactionType;
    private String vendorName;
    private String vendorSiteCode;
    private String businessUnit;
    private String employeeName;
    private String employeeId;
    private String lineTransactionType;
    private String autoTransactCode;
    private String sourceDocumentCode;
    private String documentNumber;
    private String documentLineNumber;
    private String quantity;
    private String comments;
    private String toOrganizationCode;
    private String amount;
    private String receiptNumber;
    private String uom;
    private String transactionDate;
    private String parentTransactionId;
    private JSONArray lines;

    public void setAsnType(String asnType) {
        this.asnType = asnType;
    }

    public String getAsnType() {
        return asnType;
    }

    public void setReceiptSourceCode(String receiptSourceCode) {
        this.receiptSourceCode = receiptSourceCode;
    }

    public String getReceiptSourceCode() {
        return receiptSourceCode;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorSiteCode(String vendorSiteCode) {
        this.vendorSiteCode = vendorSiteCode;
    }

    public String getVendorSiteCode() {
        return vendorSiteCode;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setLineTransactionType(String lineTransactionType) {
        this.lineTransactionType = lineTransactionType;
    }

    public String getLineTransactionType() {
        return lineTransactionType;
    }

    public void setAutoTransactCode(String autoTransactCode) {
        this.autoTransactCode = autoTransactCode;
    }

    public String getAutoTransactCode() {
        return autoTransactCode;
    }

    public void setSourceDocumentCode(String sourceDocumentCode) {
        this.sourceDocumentCode = sourceDocumentCode;
    }

    public String getSourceDocumentCode() {
        return sourceDocumentCode;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentLineNumber(String documentLineNumber) {
        this.documentLineNumber = documentLineNumber;
    }

    public String getDocumentLineNumber() {
        return documentLineNumber;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public void setToOrganizationCode(String toOrganizationCode) {
        this.toOrganizationCode = toOrganizationCode;
    }

    public String getToOrganizationCode() {
        return toOrganizationCode;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setLines(JSONArray lines) {
        this.lines = lines;
    }

    public JSONArray getLines() {
        return lines;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUom() {
        return uom;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setParentTransactionId(String parentTransactionId) {
        this.parentTransactionId = parentTransactionId;
    }

    public String getParentTransactionId() {
        return parentTransactionId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeId() {
        return employeeId;
    }
}
