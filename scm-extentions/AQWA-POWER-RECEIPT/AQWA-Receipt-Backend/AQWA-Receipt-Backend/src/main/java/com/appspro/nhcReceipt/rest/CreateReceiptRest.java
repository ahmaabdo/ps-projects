package com.appspro.nhcReceipt.rest;

import com.appspro.nhcReceipt.bean.CreateReceiptBean;
import com.appspro.nhcReceipt.dao.CreateReceiptDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/receipt")
public class CreateReceiptRest {

    CreateReceiptDAO receipt = new CreateReceiptDAO();

    @POST
    @Path("createReceipt")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String createReceipt(String body) {

        JSONObject response = null;
        JSONObject returnReponse = new JSONObject();
        JSONArray bodyArr = new JSONArray(body);
        JSONArray receiptResArr = new JSONArray();
        CreateReceiptBean bean = new CreateReceiptBean();

        try {

            for (int i = 0; i < bodyArr.length(); i++) {
                response = new JSONObject();
                JSONObject obj = bodyArr.getJSONObject(i);
                bean.setReceiptNumber(obj.get("receiptNumber").toString());
                bean.setVendorName(obj.get("vendorName").toString());
                bean.setVendorSiteCode(obj.get("vendorSite").toString());
                bean.setBusinessUnit(obj.get("businessUnit").toString());
                bean.setEmployeeId(obj.get("employeeId").toString());
                bean.setLines(obj.getJSONArray("receiptLines"));

                returnReponse = receipt.cerateReceipt(bean);

                if (!returnReponse.get("Status").toString().equalsIgnoreCase("SUCCESS")) {
                    response.put("Status", "ERROR");
                    response.put("Data", returnReponse);
                }

                receiptResArr.put(returnReponse);
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.put("Status", "ERROR");
            response.put("Data", e.getMessage());
        }

        return receiptResArr.toString();
    }


    @POST
    @Path("createReceiptRest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String createReceiptRest(String body) {
        JSONObject response = new JSONObject();
        JSONArray responseArr = new JSONArray();
        try {
            responseArr = receipt.cerateReceiptRest(body);
            if (responseArr.getJSONObject(0).has("errorMessage")) {
                response.put("Status", "ERROR");
            }
            else{
                response.put("Status", "OK");
            }
            response.put("Data", responseArr);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("Status", "ERROR");
            response.put("Data", e.getMessage());
        }
        return response.toString();
    }
    
    @POST
    @Path("/receiptAttachment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response receiptAttachment (String body){
        
        JSONArray bodyArr = new JSONArray(body);
        JSONObject responseObj = new JSONObject();

        try {
            responseObj.put("Status","OK");
            responseObj.put("Data",receipt.uploadFileUCM(bodyArr));

        } catch (Exception e) {
            e.printStackTrace();
            responseObj.put("Status","ERROR");
            responseObj.put("Data",e.getMessage());
        }
        return  Response.ok(responseObj.toString()).build();
    }
}
