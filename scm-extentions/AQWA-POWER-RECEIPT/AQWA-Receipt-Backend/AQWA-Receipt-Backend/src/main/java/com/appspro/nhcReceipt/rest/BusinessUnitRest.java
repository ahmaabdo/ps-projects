package com.appspro.nhcReceipt.rest;


import com.appspro.nhcReceipt.dao.BusinessUnitDAO;

import java.io.IOException;

import java.net.MalformedURLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/businessUnit")
public class BusinessUnitRest {

    @GET
    @Path("allBusinessUnit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allBusinessUnit() {
        
        JSONObject response = new JSONObject();
        try {
            JSONObject obj = new JSONObject();
            BusinessUnitDAO bus = new BusinessUnitDAO();
            JSONArray arr = new JSONArray();
            
            obj = bus.businessUnit();
            
            response.put("Status", "OK");
            response.put("Data", obj.getJSONArray("items"));
            
        } catch (MalformedURLException e) {
            response.put("Status", "ERROR");
            response.put("Data", e.getMessage());
        } catch (IOException e) {
            response.put("Status", "ERROR");
            response.put("Data", e.getMessage());
        }
        
        return response.toString();

    }

}
