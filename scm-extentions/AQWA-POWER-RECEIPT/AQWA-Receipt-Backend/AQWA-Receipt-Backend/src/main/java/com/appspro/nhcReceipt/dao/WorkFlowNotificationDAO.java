package com.appspro.ttcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.ttcLG.bean.WorkFlowNotificationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class WorkFlowNotificationDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    PreparedStatement ps2;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public String insertIntoWorkflow(WorkFlowNotificationBean bean) throws SQLException {
        try {         
            connection = AppsproConnection.getConnection();
            String query = null;
            query ="insert into XXX_LG_NOTIFICATION(EMP_ID) select ? from dual where not exists(select emp_id  from XXX_LG_NOTIFICATION where emp_id = ?) ";
            ps2 = connection.prepareStatement(query);
            ps2.setString(1, bean.getEmpId());
            ps2.setString(2, bean.getEmpId());
            ps2.executeUpdate();

            String query2 = " insert into XXX_LG_NOTIFICATION_DETAILS (STATUS,CANCEL,DOC_NUM,LG_NUM_FROM_BANK,CONTRACT_NUMBER,CONTRACT_AMOUNT,LG_STATUS,AMOUNT,SUPPLIER,LG_END_DATE,LG_TYPE,EMP_ID,REGION,DESCRIPTION,CREATED_BY,LG_START_DATE,NEGOTIATION_NUM,NEGOTIATION_TITLE,NEGOTIATION_AMOUNT) " + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query2);
            ps.setString(1, bean.getApprovalStatus());
            ps.setString(2, bean.getCancel());
            ps.setInt(3, bean.getDocumentNumber());
            ps.setString(4, bean.getLgNumFromBank());
            ps.setString(5, bean.getContractNumber());
            ps.setString(6, bean.getContractAmount());
            ps.setString(7, bean.getLgStatus());
            ps.setString(8, bean.getAmount());
            ps.setString(9, bean.getSupplier());
            ps.setString(10, bean.getLgEndDate());
            ps.setString(11, bean.getLgType());
            ps.setString(12, bean.getEmpId());
//            ps.setString(13, bean.getContractStartDate());
//            ps.setString(14, bean.getContractEndDate());
            ps.setString(13, bean.getRegion());
            ps.setString(14, bean.getDescription());
            ps.setString(15, bean.getCreatedBy());
            ps.setString(16, bean.getLgStartDate());
            ps.setString(17, bean.getNegotiationNumber());
            ps.setString(18, bean.getNegotiationTitle());
            ps.setString(19, bean.getNegotiationAmount());


            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }

        return "Notification added Successfully";
    }
    
    public String updateWorkflow(WorkFlowNotificationBean bean) throws SQLException {
        try {     
            connection = AppsproConnection.getConnection();
            String query = null;
            query = " update  XXX_LG_NOTIFICATION_DETAILS set LG_NUM_FROM_BANK = ?, CONTRACT_NUMBER = ?,CONTRACT_AMOUNT = ?,AMOUNT = ?,SUPPLIER = ?,LG_END_DATE = ?,DESCRIPTION = ?,NEGOTIATION_NUM = ?,NEGOTIATION_TITLE = ?, NEGOTIATION_AMOUNT =?, STATUS=?,LG_STATUS=? where DOC_NUM=? and LG_TYPE =?";
            ps = connection.prepareStatement(query);
            
            ps.setString(1, bean.getLgNumFromBank());
            ps.setString(2, bean.getContractNumber());
            ps.setString(3, bean.getContractAmount());
            ps.setString(4, bean.getAmount());
            ps.setString(5, bean.getSupplier());
            ps.setString(6, bean.getLgEndDate());
            ps.setString(7, bean.getDescription());
            ps.setString(8, bean.getNegotiationNumber());
            ps.setString(9, bean.getNegotiationTitle());
            ps.setString(10, bean.getNegotiationAmount());
            ps.setString(11, bean.getApprovalStatus());
            ps.setString(12, bean.getLgStatus());
            ps.setInt(13, bean.getDocumentNumber());
            ps.setString(14, bean.getLgType());
            ps.executeUpdate();
            
            String query2 = " update  XXX_LG_NUMBER set APPROVAL_STATUS=? ,STATUS=? where DOCUMENT_NUMBER=? and LG_TYPE =?";
            ps2 = connection.prepareStatement(query2);
            ps2.setString(1, bean.getApprovalStatus());
            ps2.setString(2, bean.getLgStatus());
            ps2.setInt(3, bean.getDocumentNumber());
            ps2.setString(4, bean.getLgType());
            ps2.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }

        return "Notification updated Successfully";
    }
    
    public String updateNotifyStatus(WorkFlowNotificationBean bean) throws SQLException {
        try {     
            connection = AppsproConnection.getConnection();
            String query = null;
            query = " update  XXX_LG_NOTIFICATION_DETAILS set STATUS=?,LG_STATUS=? where DOC_NUM=? and LG_TYPE =?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getApprovalStatus());
            ps.setString(2, bean.getLgStatus());
            ps.setInt(3, bean.getDocumentNumber());
            ps.setString(4, bean.getLgType());
            ps.executeUpdate();
            
            String query2 = " update  XXX_LG_NUMBER set APPROVAL_STATUS=? ,STATUS=? where DOCUMENT_NUMBER=? and LG_TYPE =?";
            ps2 = connection.prepareStatement(query2);
            ps2.setString(1, bean.getApprovalStatus());
            ps2.setString(2, bean.getLgStatus());
            ps2.setInt(3, bean.getDocumentNumber());
            ps2.setString(4, bean.getLgType());
            ps2.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }

        return "Notification updated Successfully";
    }
    public JSONArray getNotificationByEmpId(String empId) throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<WorkFlowNotificationBean> list = new ArrayList<WorkFlowNotificationBean>();
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_NOTIFICATION_DETAILS WHERE EMP_ID = ?  ";
            ps = connection.prepareStatement(query);
            ps.setString(1, empId);
            rs = ps.executeQuery();
            while (rs.next()) {
                WorkFlowNotificationBean bean = new WorkFlowNotificationBean();

                bean.setId(rs.getInt("ID"));
                bean.setApprovalStatus(rs.getString("STATUS"));
                bean.setCancel(rs.getString("CANCEL"));
                bean.setDocumentNumber(rs.getInt("DOC_NUM"));
                bean.setLgNumFromBank(rs.getString("LG_NUM_FROM_BANK"));
                bean.setContractNumber(rs.getString("CONTRACT_NUMBER"));
                bean.setContractAmount(rs.getString("CONTRACT_AMOUNT"));
                bean.setLgStatus(rs.getString("LG_STATUS"));
                bean.setAmount(rs.getString("AMOUNT"));
                bean.setSupplier(rs.getString("SUPPLIER"));
                bean.setLgEndDate(rs.getString("LG_END_DATE"));
                bean.setLgType(rs.getString("LG_TYPE"));
                bean.setEmpId(rs.getString("EMP_ID"));
//                bean.setContractStartDate(rs.getString("CONTRACT_START_DATE"));
//                bean.setContractEndDate(rs.getString("CONTRACT_END_DATE"));
                bean.setRegion(rs.getString("REGION"));
                bean.setDescription(rs.getString("DESCRIPTION"));
                bean.setCreatedBy(rs.getString("CREATED_BY"));
                bean.setLgStartDate(rs.getString("LG_START_DATE")); 
                bean.setNegotiationNumber(rs.getString("NEGOTIATION_NUM"));
                bean.setNegotiationTitle(rs.getString("NEGOTIATION_TITLE"));
                bean.setNegotiationAmount(rs.getString("NEGOTIATION_AMOUNT"));
                
                list.add(bean);
            }
            arr = new JSONArray(list);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }
}
