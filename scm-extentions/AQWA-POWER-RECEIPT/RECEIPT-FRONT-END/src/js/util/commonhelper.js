define([], function () {

    function commonHelper() {
        var self = this;

        self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };

        self.businessLov = "businessUnit/allBusinessUnit";
        self.purchaseOrders = "report/getPurchaseOrders"

        self.getInternalRest = function() {

            if (window.location.hostname == "132.145.43.71") {
//                var host = "https://132.145.43.71/NHC-RECEIPT-BACKEND-TEST/webapi/";
            } else {
                var host = "http://127.0.0.1:7101/AQWA-RECEIPT-BACKEND-TEST/webapi/";
            }

            return host;
        };
    }

    return new commonHelper();
});