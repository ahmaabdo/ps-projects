function showEmplDetails(oj,ko, services, app, mainData, commonhelper) {
    var rootViewModel = app;
  var  xx ="https://132.145.43.71/NHC-RECEIPT/?username=r.ahmed@nhc.sa&host=https://eghj-test.fa.em2.oraclecloud.com&lang=en&jwt=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsIng1dCI6IlgyUUwyMWxMWUhyVFZsaDlqLWthY3J3Z2tKVSIsImtpZCI6InRydXN0c2VydmljZSJ9.eyJleHAiOjE1OTg5NzU0MzYsInN1YiI6InIuYWhtZWRAbmhjLnNhIiwiaXNzIjoid3d3Lm9yYWNsZS5jb20iLCJwcm4iOiJyLmFobWVkQG5oYy5zYSIsImlhdCI6MTU5ODk2MTAzNn0.DSrD7SFw94kBkugtl7qvDaRvIMXfn57zOx-ZoyCAPHxU2ZQUK43C0AkD9--3YAls9O7VDBJxHIPmPik5kH9O8yebbXjy900YTt18DzTCWNRREYqkFC_SJUTIkPyH82_oE6--xtLy3fkDr65d0iz8NASiQhVuyoFQNJUuwCzHEfT_AtnqoLwSscK_hrmSCORU3ZTmyXXA0NqFCSro76ZWlFXjpo0u1f8fvtfM30OagEIS7hKbJ_RAsT0WC-a06WCWjQfuFSRLgxtKKTgb_fd2v1qAGx8CdqFKPTdFLGst0HH4af5TOzQ91ewp8xQJcU3tOYfRG4_ahDbuwPdA9NXceA";
     var url = new URL(window.location.href); //xx window.location.href//
    var jwt;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var deviceAndBrowserDetails = navigator.userAgent; //browser and device details 
    var LogindateTime;
    var isJWTNotValid = false;
    if (url.searchParams.get("jwt")) {
        if (rootViewModel.hostUrl() == null || rootViewModel.hostUrl().length == 0) {
            hosturl = url.searchParams.get("host");
            rootViewModel.hostUrl(hosturl);
        }
        if (rootViewModel.jwt() == null || rootViewModel.jwt().length == 0) {
            jwt = url.searchParams.get("jwt");
            getToken(services,app,jwt);
            rootViewModel.jwt(jwt);
        }
        if (rootViewModel.userNameJWT() == null || rootViewModel.userNameJWT().length == 0) {
            try {
                jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
                userNameJWT = jwtJSON.sub;
                isJWTExpred = Math.round(Date.now() / 1000) > jwtJSON.exp;
                var isHasJwt = localStorage.jwt && localStorage.jwt == jwt;
                if (isJWTExpred) { //used before
                    isJWTNotValid = true;
                } else {
                    localStorage.setItem("userName", userNameJWT);
                    rootViewModel.userNameJWT(userNameJWT);
                    rootViewModel.userName(userNameJWT);
                }
            } catch (e) {};
        }
        if (url.searchParams.get("lang") && !isJWTNotValid) {
            var sessionLang = url.searchParams.get("lang");
            if (sessionLang == 'ar') {
                rootViewModel.setLocale('ar');
                localStorage.setItem("selectedLanguage", "ar");
            } else if (sessionLang == 'en') {
                rootViewModel.setLocale("en-US");
                localStorage.setItem("selectedLanguage", "en-US");
            }
        }
        if (!isJWTNotValid) { //its mean is JWT is vaild
            rootViewModel.loginVaild(true);
            $(".apLoginBtn").addClass("loading");
            rootViewModel.disOnLogin(true);
            LogindateTime = date + ' ' + time;
            rootViewModel.loginDateFromSass(LogindateTime);
            var payloadLoginHistory = {};
            $.getJSON("https://api.ipify.org/?format=json", function(e) {
                deviceIp = e.ip;
                payloadLoginHistory = {
                    "personNumber": userNameJWT,
                    "loginDate": LogindateTime,
                    "browserAndDeviceDetails": deviceAndBrowserDetails,
                    "deviceIp": deviceIp
                };
                sessionStorage.setItem("SShrLOginHistroy", JSON.stringify(payloadLoginHistory));
                localStorage.jwt = [];
                localStorage.jwt = jwt; //save jwt into local storage to be checked next time
            });
        } else { //if jwt token not vaild 
            rootViewModel.JWTExpired(true);
        }
    } else {
        if (localStorage.getItem("jwt") != null) {
            jwtJSON = jQuery.parseJSON(atob(localStorage.getItem("jwt").split('.')[1]));
            isJWTExpred = Math.round(Date.now() / 1000) > jwtJSON.exp;
            if (!isJWTExpred) {
                rootViewModel.loginVaild(true);
                $(".apLoginBtn").addClass("loading");
                rootViewModel.disOnLogin(true);
            }
        }
    }
    function getToken(services, app, userName) {
    var jwt = {"jwt" : userName};
    var getTokenData = function (data) {
        var jwt = data.jwt;
        document.cookie = "JSESSIONSID=" + jwt;
        console.log(document.cookie);
    };
    services.postGeneric("login/generatetoken", jwt).then(getTokenData, app.failCbFn);
    }
}
