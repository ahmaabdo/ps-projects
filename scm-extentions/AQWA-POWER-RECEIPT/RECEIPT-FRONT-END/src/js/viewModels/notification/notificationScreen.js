/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * notificationScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, commonhelper, DataCollectionEditUtils) {
    /**
     * The view model for the main content view template
     */
    function notificationScreenContentViewModel() {
        var self = this;
        self.customsModel = {
            columnArray: ko.observableArray([]),
            paymentTableArr: ko.observableArray([]),
            notificationArr:ko.observableArray([]),
            rowSelected:ko.observable(),
            isDisabled:ko.observable(true),
          
        };
        self.label = ko.observable();
        
        self.isLastApproved= ko.observable("appspro.fin");
        self.dataprovider = ko.observable();
        self.dataprovider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.paymentTableArr)));
        self.connected = function () {
        };
                  
            //--------------------------approve Action --------------------------
        self.approveBtnAction = function () {
            
            var headers = {
                "MSG_TITLE":  self.customsModel.rowSelected.msgTitle,
                "MSG_BODY":  self.customsModel.rowSelected.msgBody,
                "TRS_ID":  self.customsModel.rowSelected.requestId,
                "PERSON_ID": app.userName(),
                "RESPONSE_CODE": "APPROVED",
                "ssType":  self.customsModel.rowSelected.selfType,
                "PERSON_NAME":  self.customsModel.rowSelected.personName
            };
            app.loading(true);
            var approvalActionCBFN = function (data) {
                
                app.messagesDataProvider.push(app.createMessage('confirmation','Approved'));
                
                if (self.isLastApproved() == app.userName()) {
                    app.loading(true);
                    var payload = {
                        paymentId: self.customsModel.rowSelected.paymentId.toString(),
                        username: app.userName(),
                        transactionType:"MT101"
                    };
                    var paymnetSucess = function (data) {
                        self.customsModel.paymentTableArr([]); 
                        self.getAllPayments();
                        self.customsModel.isDisabled(true);

                      //  app.messagesDataProvider.push(app.createMessage('information', `${data.bankResponse.message[0]}`))
                        if (data.bankResponse.message[0] == "success")
                        {
                            app.loading(false);
                            app.messagesDataProvider.push(app.createMessage('confirmation', `Payment Posted Successfully`));
                        } else {
                            app.loading(false);
                            app.messagesDataProvider.push(app.createMessage('error', `${data.bankResponse.message[0]}`));
                        }
                    };
                    var paymentFail = function (fail) {
                        app.loading(false)
                        app.messagesDataProvider.push(app.createMessage('error', `${fail.statusText}`));
                    };
                    services.postGeneric(commonhelper.createPayment + self.customsModel.rowSelected.paymentOperation, payload, {"Authorization": commonhelper.authorization}).then(paymnetSucess, paymentFail);
                } else {
                    app.loading(false)
                    self.customsModel.paymentTableArr([]);
                    self.getAllPayments();
                    self.customsModel.isDisabled(true);
                }
            };
            var failCbFn = function () {

            };
            services.postGeneric("workFlowNotification", headers).then(approvalActionCBFN, failCbFn);
        };
        //--------------Reject Action --------------------------
        self.rejectBtnAction = function(){
             var headers = {
                "MSG_TITLE":  self.customsModel.rowSelected.msgTitle,
                "MSG_BODY":  self.customsModel.rowSelected.msgBody,
                "TRS_ID":  self.customsModel.rowSelected.requestId,
                "PERSON_ID": app.userName(),
                "RESPONSE_CODE": "REJECTED",
                "ssType":  self.customsModel.rowSelected.selfType,
                "PERSON_NAME": self.customsModel.rowSelected.personName
            };
            app.loading(true)
            var approvalActionCBFN = function (data) {
                app.loading(false)
                  app.messagesDataProvider.push(app.createMessage('confirmation','Rejected'));
                self.customsModel.paymentTableArr([]);
                self.getAllPayments();
                  self.customsModel.isDisabled(true);
            };
            var failCbFn = function (fail) {
                app.loading(false)
              app.messagesDataProvider.push(app.createMessage('error', `${fail.statusText}`));
            };
            services.postGeneric("workFlowNotification", headers).then(approvalActionCBFN, failCbFn);
        };
        //-----------------Selection Action --------------------------
        
//                $("body").delegate("#table .oj-table-body-row", "click", (e) => {
//            var index = $(e.currentTarget).find(".hiddenkeyHolder").prevObject[0].rowIndex;
//            self.customsModel.rowSelected = self.customsModel.paymentTableArr()[index - 1];
//            self.customsModel.payDisable(false);
//
//        });
        self.tableSelectionListener = function(event){
            var data = event.detail;
            var currentRow = data.currentRow;
            if (currentRow)
                self.customsModel.rowSelected = self.customsModel.paymentTableArr()[[currentRow['rowIndex']]];
               self.customsModel.selectedRowKey(self.customsModel.paymentTableArr()[[currentRow['rowIndex']]].requestId)
                self.customsModel.isDisabled(false);
        };
        
        //     --------------------------------------- view approval function button ------------------------ 

        
           //------------------View Button Actions --------------------------
      
         //------------------Translation --------------------------
          var getTransaltion = oj.Translations.getTranslatedString;
         function initTransaltion() {
              self.label({
                paymentNumberLbl: getTransaltion("paymentDetails.paymentNumberLbl"),
                paymentStatusLbl: getTransaltion("paymentDetails.paymentStatusLbl"),
                SupplierNumberLbl: getTransaltion("paymentDetails.SupplierNumberLbl"),
                paymentAmountLbl: getTransaltion("paymentDetails.paymentAmountLbl"),
                paymentCurrancyLbl: getTransaltion("paymentDetails.paymentCurrancyLbl"),
                paymentDescriptionLbl: getTransaltion("paymentDetails.paymentDescriptionLbl"),
                paymentDateLbl: getTransaltion("paymentDetails.paymentDateLbl"),
                paymentMethodLbl: getTransaltion("paymentDetails.paymentMethodLbl"),
                paymentTypeLbl: getTransaltion("paymentDetails.paymentTypeLbl"),
                createdByLbl: getTransaltion("paymentDetails.createdByLbl"),
                paymentIdLbl: getTransaltion("paymentDetails.paymentIdLbl"),
                operationTypesLbl: getTransaltion("paymentDetails.operationTypesLbl"),
                approvalscreenLbl: getTransaltion("paymentDetails.approvalscreenLbl"),
                approveLbl: getTransaltion("common.approveLbl"),
                rejectLbl: getTransaltion("common.rejectLbl"),
                viewLbl: getTransaltion("paymentDetails.viewLbl"),
                screenLbl: getTransaltion("paymentDetails.screenLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                viewApprovalLabel: getTransaltion("common.viewApprovalLabel"),
               
                });
                
                self.customsModel.columnArray([

                {
                    "headerText": "Message Title",
                    "field": "paymentDate"
                },
                {
                    "headerText": "Message Body",
                    "field": "paymentId"
                },

                {
                    "headerText": "Notification Type",
                    "field": "paymentStatus"
                },
              
            ]);
         }
         initTransaltion();
    }
    
    return notificationScreenContentViewModel;
});
