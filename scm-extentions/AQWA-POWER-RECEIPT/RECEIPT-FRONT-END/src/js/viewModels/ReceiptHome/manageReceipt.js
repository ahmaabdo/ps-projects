
define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'ojs/ojpagingdataproviderview', 'ojs/ojarraydataprovider', 'ojs/ojcollapsible', 'ojs/ojselectcombobox',
    'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojselectsingle', 'ojs/ojformlayout', 'ojs/ojbutton', 'ojs/ojtable',
    'ojs/ojpagingcontrol', 'ojs/ojlabelvalue', 'ojs/ojarraydataprovider', 'ojs/ojcheckboxset', 'ojs/ojpagingtabledatasource', 'ojs/ojarraytabledatasource'
], function (oj, ko, app, services, PagingDataProviderView, ArrayDataProvider) {

    function manageReceiptContentViewModel() {
        var self = this;
        var seq = 0;
        self.label = ko.observable();
        self.customsModel = {
            requestersArr: ko.observableArray([]),
            purchaseOrdersArr: ko.observableArray([]),
            businessUnitArr: ko.observableArray([]),
            rowSelectedArr: ko.observableArray([]),
            searchArr: ko.observableArray([]),
            placeHolder: ko.observable("Please Select"),
            rowSelected: ko.observable(),
            purchaseOrderVal: ko.observable(),
            businessUnitVal: ko.observable(),
            receiptVal: ko.observable(),
            isDisabled: ko.observable(true)
        };

        self.tableData = ko.observableArray([]);
        self.columnArray = ko.observableArray();
        self.selectedItems = ko.observableArray([]);
        self.pagingDataProvider = ko.observable();
        self.pagingDataProvider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tableData, { idAttribute: 'seq' })));

        self.connected = function () {
            initTranslations();

        };

        ko.computed(function () {
            self.customsModel.businessUnitArr(app.businessUnitArr());
            self.customsModel.purchaseOrdersArr(app.purchaseOrdersArr());

        });

        self.getAllRecieptLines = function (receiptNum, po_num, bu) {
            app.loading(true);
            var payload = {
                RECEIPT_NUM: receiptNum ? receiptNum : "",
                PO_NUM: po_num ? po_num : "",
                BU: bu ? bu : "",
                reportName: "Receipt_line_Report"
            };
            SuccessCbFn = function (data) {
                self.tableData([]);

                if (Array.isArray(data)) {
                    data = data.filter(el => el.CREATED_BY === app.personDetails()[0].userName);
                }
                else {
                    data = [data].filter(el => el.CREATED_BY === app.personDetails()[0].userName);
                }

                if (data.length > 0) {
                    var x = data.map(el => {
                        if (Array.isArray(el.Items)) {
                            el.Items = el.Items;
                        } else {
                            el.Items = [el.Items];
                        }
                        el.Items.forEach(e => {
                            e.BU_NAME = el.BU_NAME;
                            e.RECEIPT_DATE = el.RECEIPT_DATE;
                            e.RECEIPT_NUMBER = el.RECEIPT_NUMBER;
                        });
                        return el.Items;
                    });
                    var y = [];
                    x.forEach(el => {
                        el.forEach(e => {
                            e.seq = ++seq;
                            y.push(e);
                        });
                    });
                    self.tableData(y);
                } else {
                    app.messagesDataProvider.push(app.createMessage('warning', 'No Data to display', '2000'));
                }
                app.loading(false);
                self.pagingDataProvider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tableData, { idAttribute: 'seq' })));
            };
            failCbFn = function () {
                app.loading(false);
            };
            services.postGeneric("report/commonbireport", payload).then(SuccessCbFn, failCbFn);
        };

        self.returnBtnAction = function () {
            oj.Router.rootInstance.go('returnAndCorrect');
            oj.Router.rootInstance.store('return');
            localStorage.setItem("returnRowData", JSON.stringify(self.customsModel.rowSelectedArr()))
        };

        self.correctBtnAction = function () {
            oj.Router.rootInstance.go('returnAndCorrect');
            oj.Router.rootInstance.store('correct');
            localStorage.setItem("correctRowData", JSON.stringify(self.customsModel.rowSelectedArr()))
        };

        self.searchBtnAction = function () {
            self.customsModel.rowSelectedArr([])
            if (!self.customsModel.businessUnitVal() && !self.customsModel.purchaseOrderVal() && !self.customsModel.receiptVal()
            ) {
                self.tableData([]);
                app.messagesDataProvider.push(app.createMessage('warning', 'Please Select Value!', '2000'));
                return;
            }
            if (self.customsModel.businessUnitVal() && !self.customsModel.purchaseOrderVal()) {
                app.messagesDataProvider.push(app.createMessage('warning', 'Please Select Requisitioning BU and Purchase Order!', '2000'));
                return;
            }
            self.getAllRecieptLines(self.customsModel.receiptVal(), self.customsModel.purchaseOrderVal(), self.customsModel.businessUnitVal());
            self.customsModel.isDisabled(true);

        };

        self.resetBtnAction = function () {
            self.customsModel.purchaseOrderVal('');
            self.customsModel.businessUnitVal('');
            self.customsModel.receiptVal('');
            self.tableData(self.customsModel.searchArr());
            self.customsModel.rowSelectedArr([]);
            self.customsModel.isDisabled(true);
        };

        //-------------------------Delegation  Action----------------------------
        $("body").delegate("#table .oj-table-body-row", "click", (e) => {
            var index = $(e.currentTarget).find(".hiddenkeyHolder").val();
            self.customsModel.rowSelected = self.tableData()[index];

            if (e.target.type == 'checkbox') {
                if (self.customsModel.rowSelected) {
                    if (e.target.checked) {
                        self.customsModel.rowSelectedArr().push(self.customsModel.rowSelected);
                        self.customsModel.isDisabled(false);

                    } else {
                        self.customsModel.rowSelectedArr(self.customsModel.rowSelectedArr().filter(el => el.seq != self.customsModel.rowSelected.seq))
                        if (self.customsModel.rowSelectedArr().length == 0) {
                            self.customsModel.isDisabled(true);
                        }
                    }
                }
            }
        });

        //-------------------------cheack Box handelers  Action----------------------------
        self.getIndexInSelectedItems = function (id) {
            for (var i = 0; i < self.selectedItems().length; i++) {
                var range = self.selectedItems()[i];
                var startIndex = range.startIndex.row;
                var endIndex = range.endIndex.row;
                if (id >= startIndex && id <= endIndex) {
                    return i;
                }
            }
            return -1;
        };

        self.handleCheckbox = function (id, srN) {
            var isChecked = self.getIndexInSelectedItems(id) != -1 || (srN ? self.customsModel.rowSelectedArr().find(e => e.seq === srN) : false);
            return isChecked ? ['checked'] : [];
        };

        var getTranslation = oj.Translations.getTranslatedString;

        function initTranslations() {
            self.label({
                businessUnitLbl: getTranslation("receiptScreen.businessUnitLbl"),
                POLbl: getTranslation("receiptScreen.POLbl"),
                recepitLbl: getTranslation("receiptScreen.recepitLbl"),
                searchLbl: getTranslation("common.searchLbl"),
                resetLbl: getTranslation("common.resetLbl"),
                returnLbl: getTranslation("receiptScreen.returnLbl"),
                correctLbl: getTranslation("receiptScreen.correctLbl"),
                mangeReceiptScreen: getTranslation("receiptScreen.mangeReceiptScreen"),
                uom: getTranslation("receiptScreen.uom"),
                itemDescriptionLbl: getTranslation("receiptScreen.itemDescriptionLbl"),
                supplierName: getTranslation("receiptScreen.supplierName"),
                POlineNum: getTranslation("receiptScreen.POlineNum"),
                needByDate: getTranslation("receiptScreen.needByDate"),
                qtyDelivered: getTranslation("receiptScreen.qtyDelivered"),
                qtyReceived: getTranslation("receiptScreen.qtyReceived"),
                qtyShipped: getTranslation("receiptScreen.qtyShipped"),
                receiptDateLbl: getTranslation("receiptScreen.receiptDateLbl"),
                selectPlaceholder: getTranslation("common.selectPlaceholder"),
                amountReceivedLbl: getTranslation("receiptScreen.amountReceivedLbl"),
                currencyLbl: getTranslation("receiptScreen.currencyLbl"),
            })
            self.columnArray([
                {
                    "headerText": "Select",
                    "template": 'checkTemplate'
                },
                {
                    "headerText": "NO.",
                    "template": 'seqTemplate'
                },
                {
                    "headerText": self.label().businessUnitLbl, "field": "BU_NAME"
                },
                {
                    "headerText": self.label().recepitLbl, "field": "RECEIPT_NUMBER"
                },
                {
                    "headerText": self.label().receiptDateLbl, "field": "RECEIPT_DATE"
                },

                {
                    "headerText": self.label().itemDescriptionLbl, "field": "ITEM_DESCRIPTION"
                },
                {
                    "headerText": self.label().supplierName, "field": "VENDOR_NAME"
                },
                {
                    "headerText": self.label().POLbl, "field": "PO_NUMBER"
                },
                {
                    "headerText": self.label().POlineNum, "field": "LINE_NUM"
                },
                {
                    "headerText": self.label().qtyDelivered, "field": "QUANTITY_DELIVERED"
                },
                {
                    "headerText": self.label().qtyReceived, "field": "QUANTITY_RECEIVED"
                },
                {
                    "headerText": self.label().uom, "field": "UNIT_OF_MEASURE"
                },
                {
                    "headerText": self.label().amountReceivedLbl, "field": "AMOUNT_RECEIVED"
                },
                {
                    "headerText": self.label().currencyLbl, "field": "CURRENCY_CODE"
                }
                // {
                //     "headerText": self.label().qtyShipped, "field": "QUANTITY_SHIPPED"
                // },

            ])
        }
    }

    return manageReceiptContentViewModel;
});
