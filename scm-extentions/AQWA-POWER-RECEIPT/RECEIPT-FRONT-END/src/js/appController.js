/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmodule-element-utils', 'ojs/ojknockouttemplateutils', 'ojs/ojrouter', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojarraydataprovider',
    'ojs/ojoffcanvas', 'ojs/ojmodule-element', 'ojs/ojknockout', 'ojs/ojinputtext', 'ojs/ojmessages'],
        function (oj, ko, app, services, commonhelper, moduleUtils, KnockoutTemplateUtils, Router, ResponsiveUtils, ResponsiveKnockoutUtils, ArrayDataProvider, OffcanvasUtils) {
            function ControllerViewModel() {
                var self = this;
                this.KnockoutTemplateUtils = KnockoutTemplateUtils;
                // Handle announcements sent when pages change, for Accessibility.
                self.manner = ko.observable('polite');
                self.message = ko.observable();
                self.waitForAnnouncement = false;
                self.navDrawerOn = false;
                self.isUserLoggedIn = ko.observable(false);
                self.formVisible = ko.observable(true);
                self.windowSize = ko.observable();
                self.screenRange = oj.ResponsiveKnockoutUtils.createScreenRangeObservable();
                self.userName = ko.observable();
                self.password = ko.observable();
                self.Usernametext = ko.observable("Username");
                self.passwordLabel = ko.observable("Password");
                self.loginLabel = ko.observable("Login");
                self.disOnLogin = ko.observable(false);
                self.loginDisable = ko.observable(false);
                self.messagesDataProvider = ko.observableArray([]);
                self.language = ko.observable();
                self.loginWelcomeText = ko.observable();
                self.languagelable = ko.observable("عربي");
                var storage = window.localStorage;
                self.localeLogin = ko.observable();
                self.UsernameLabel = ko.observable();
                self.passwordLabel = ko.observable();
                self.loginword = ko.observable();
                self.forgotPasswordLabel = ko.observable();
                self.loading = ko.observable(false);
                self.businessUnitArr = ko.observableArray([]);
                self.purchaseOrdersArr = ko.observableArray([]);
                self.personDetails = ko.observableArray([]);
                self.AllemployeesArr = ko.observableArray([]);
                self.loginVaild = ko.observable();
                self.hostUrl = ko.observable();
                self.jwt = ko.observable();
                self.JWTExpired = ko.observable();
                self.userNameJWT = ko.observable();
                self.setLocale = ko.observable();
                self.loginDateFromSass = ko.observable();

                //----------------------------------------  loading function ------------------------ 

                self.loading = function (load) {
                    if (load) {
                        $('#loading').show();
                    } else {
                        $('#loading').hide();
                    }
                };
                //----------------------------------------  businessUnitLOV function ------------------------ 
                self.businessUnitLOV = function () {
                    var SuccessCbFn = function (data) {
                        self.businessUnitArr(data.Data.map(e => ({label: e.BusinessUnitName, value: e.BusinessUnitName})))
                    };
                    var failCbFn = function () {
                        self.messagesDataProvider.push(self.createMessage('error', 'An error accourred !', '2000'));
                    };
                    services.getGeneric(commonhelper.businessLov).then(SuccessCbFn, failCbFn)
                };
                //----------------------------------------  purchaseOrdersLOVReport function ------------------------ 
                self.purchaseOrdersLOVReport = function () {
                    var SuccessCbFn = function (data) {
                        var resData = JSON.parse(data)
                        self.purchaseOrdersArr(resData.map(e => ({label: e.PONUMBER, value: e.PONUMBER})))
                    };
                    var failCbFn = function () {
                        self.messagesDataProvider.push(self.createMessage('error', 'An error accourred !', '2000'));
                    };
                    services.getGeneric("report/Purchase_Order_Report").then(SuccessCbFn, failCbFn)
                };
                //----------------------------------------  login function ------------------------ 

                self.getEmployeeDetails = function (userName) {
                    var SuccessCbFn = function (data) {
                        self.personDetails().push(data.Data)
                    };
                    var failCbFn = function () {
                    };
                    services.getGeneric("empolyees/" + userName).then(SuccessCbFn, failCbFn)
                }
                //----------------------------------------  login function ------------------------ 

                self.getAllEmployeeDetails = function () {
                    var SuccessCbFn = function (data) {
                        self.AllemployeesArr(data.Data.map(e => ({label: e.DisplayName, value: e.UserName})))
                    };
                    var failCbFn = function () {

                    };
                    services.getGeneric("empolyees/AllEmployees").then(SuccessCbFn, failCbFn)
                };
                
                //----------------------------------------  login function ------------------------ 
                self.onLogin = function () {
                  
                   
                    if (!self.userName() && !self.password()) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter username and password', '2000'));
                        return;
                    } else if (!self.userName()) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter user name', '2000'));
                        return;
                    } else if (!self.password()) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter password', '2000'));
                        return;
                    } 
                     $(".apLoginBtn").addClass("loading");
                     self.loginDisable(true);
                    var loginSuccessCbFn = function () {
                        self.loading(false);
                        self.getAllEmployeeDetails();
                        self.businessUnitLOV();
                        self.purchaseOrdersLOVReport();
                        self.messagesDataProvider([]);
                        self.isUserLoggedIn(true);
                        oj.Router.rootInstance.go('myReceipts')
                    };

                    var loginFailCbFn = function () {
                        self.loading(false);
                        $(".apLoginBtn").removeClass("loading");
                        self.loginDisable(false);
                        self.isUserLoggedIn(false);
                        self.messagesDataProvider.push(self.createMessage('error', 'Wrong user name or passowrd', '2000'));
                    };

                    function authinticate(data) {
                        var result = data;
                        var check = data.result;
                        var jwt = data.JWT;
                        document.cookie = "JSESSIONSID=" + jwt;
                        if (check) {
                            loginSuccessCbFn();
                        } else {
                            loginFailCbFn();
                        }
                        $(".apLoginBtn").removeClass("loading");
                        self.disOnLogin(false);

                    }


                    self.getEmployeeDetails(self.userName())
                    var payload = {
                        "userName": self.userName(),
                        "password": self.password(),
                    };
                    services.authenticate(payload).then(authinticate, self.failCbFn);
                };
                //----------------------------------------  log out function ------------------------ 

                self.logOutBtn = function (event) {
                    self.isUserLoggedIn(false);
                    location.reload();
                };

                //----------------------------------------  login function ------------------------ 

               
                document.getElementById('globalBody').addEventListener('announce', announcementHandler, false);
                /*
                 @waitForAnnouncement - set to true when the announcement is happening.
                 If the nav-drawer is ON, it is reset to false in 'ojclose' event handler of nav-drawer.
                 If the nav-drawer is OFF, then the flag is reset here itself in the timeout callback.
                 */
                function announcementHandler(event) {
                    self.waitForAnnouncement = true;
                    setTimeout(function () {
                        self.message(event.detail.message);
                        self.manner(event.detail.manner);
                        if (!self.navDrawerOn) {
                            self.waitForAnnouncement = false;
                        }
                    }, 200);
                }
                ;
                self.createMessage = function (type, summary, time) {
                    return {
                        severity: type,
                        summary: summary,
                        autoTimeout: time
                    };
                }

                /*********************************Navigation Router*********************************************** */
                self.myReceiptsBtnAction = function () {
                    oj.Router.rootInstance.go('myReceipts')
                }

                self.ManageReceiptBtnAction = function () {
                    oj.Router.rootInstance.go('ManageReceipt')
                }

                self.createReceiptBtnAction = function () {
                    oj.Router.rootInstance.go('createReceipt')
                }

                self.returnAndCorrectBtnAction = function () {
                    oj.Router.rootInstance.go('returnAndCorrect')
                }

                self.NotificationBtnAction = function () {
                    oj.Router.rootInstance.go('Notification')
                }
                /************************************************************************************************* */

                self.drawerParams = {
                    displayMode: 'push',
                    selector: '#navDrawer',
                    content: '#pageContent'
                };
                // Called by navigation drawer toggle button and after selection of nav drawer item
                self.toggleDrawer = function () {
                    self.navDrawerOn = true;
                    return OffcanvasUtils.toggle(self.drawerParams);
                }

                // Media queries for repsonsive layouts
                var smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                self.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                var mdQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
                self.mdScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
                self.mdScreen.subscribe(function () {
                    OffcanvasUtils.close(self.drawerParams);
                });
                // Router setup
                self.router = Router.rootInstance;
                self.router.configure({

                    'myReceipts': {label: 'MyReceipts', value: "ReceiptHome/receiptHomePage", title: 'My Receipts ', isDefault: true},
                    'ManageReceipt': {label: 'ManageReceipt', value: "ReceiptHome/manageReceipt", title: 'Manage Receipts '},
                    'createReceipt': {label: 'createReceipt', value: "ReceiptHome/receiptCreateScreen", title: 'My Receipts'},
                    'returnAndCorrect': {label: 'returnAndCorrect', value: "ReceiptHome/returnAndCorrect", title: 'Manage Receipts'},
                    'Notification': {label: 'Notification', value: "notification/notificationScreen", title: 'Notification'}
                });
                Router.defaults['urlAdapter'] = new Router.urlParamAdapter();
                self.loadModule = function () {
                    self.moduleConfig = ko.pureComputed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        return moduleUtils.createConfig({
                            viewPath: viewPath,
                            viewModelPath: modelPath, params: {parentRouter: self.router}
                        });
                    });
                };
                // Navigation setup
                var navData = [                
                    {
                        name: 'MyReceipts', id: 'myReceipts',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-info-icon-24'
                    },
                    {
                        name: 'ManageReceipt', id: 'ManageReceipt',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'
                    },
                    {
                        name: 'Notification', id: 'Notification',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'
                    },
                ];
                self.navDataProvider = new ArrayDataProvider(navData, {keyAttributes: 'id'});
                // Drawer
                // Close offcanvas on medium and larger screens
                self.mdScreen.subscribe(function () {
                    OffcanvasUtils.close(self.drawerParams);
                });
                self.drawerParams = {
                    displayMode: 'push',
                    selector: '#navDrawer',
                    content: '#pageContent'
                };
                self.toggleDrawer = function () {
                    self.navDrawerOn = true;
                    return OffcanvasUtils.toggle(self.drawerParams);
                }
                document.getElementById('navDrawer').addEventListener("ojclose", onNavDrawerClose);
               

                self.changeLanguage = function () {
                    if (self.language() === 'english') {
                        self.language("arabic");
                        self.loginLabel("تسجيل دخول");
                        self.loginWelcomeText("مرحبا بعودتك! يرجى تسجيل الدخول إلى حسابك")
                        self.setPreferedLanguage(self.language());
                        self.languagelable("English");
                        $('.login-label').toggleClass('user-label-right');
                        $('.login-label').removeClass('user-label-left');
                        $('.input100').toggleClass('box-input-text');
                        $('.wrap-input100').toggleClass('box-input-text');
                    } else if (self.language() === 'arabic') {
                        self.language("english");
                        self.loginLabel("LOGIN")
                        self.loginWelcomeText("Welcome back! Please login to your account")
                        self.setPreferedLanguage(self.language());
                        self.languagelable("عربي");
                        $('.login-label').toggleClass('user-label-left');
                        $('.login-label').removeClass('user-label-right');
                        $('.input100').removeClass('box-input-text');
                        $('.wrap-input100').removeClass('box-input-text');
                    }
                };
                self.setPreferedLanguage = function (lang) {
                    var selecedLanguage = lang;
                    storage.setItem('setLang', selecedLanguage);
                    storage.setItem('setTrue', true);
                    preferedLanguage = storage.getItem('setLang');

                    // Call function to convert arabic
                    changeToArabic(preferedLanguage);
                };

                $(function () {
                    storage.setItem('setLang', 'english');
                    //                    storage.setItem('setLang', 'arabic');
                    preferedLanguage = storage.getItem('setLang');
                    self.language(preferedLanguage);
                    changeToArabic(preferedLanguage);
                });
                function changeToArabic(preferedLanguage) {
                    if (preferedLanguage == 'arabic') {

                        oj.Config.setLocale('ar-sa',
                                function () {
                                    $('html').attr({'lang': 'ar-sa', 'dir': 'rtl'});
                                    self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                    self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                    self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                    self.loginword(oj.Translations.getTranslatedString('login'));
                                    self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                                }
                        );

                    } else if (preferedLanguage == 'english') {
                        selectLanguage();
                    }
                }
                function selectLanguage() {
                    oj.Config.setLocale('english',
                            function () {
                                $('html').attr({'lang': 'en-us', 'dir': 'ltr'});
                                self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                self.loginword(oj.Translations.getTranslatedString('login'));
                                self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                            }
                    );
                }
                var newFunction = function () {
                    $.when(showEmplDetails(self)).done(function () {
                        if (self.loginVaild()) {

                            self.isUserLoggedIn(true)
                            self.messagesDataProvider([]);
                            self.disOnLogin(false);
                            self.loading(false);
                            self.router.go('myReceipts');

                        } else {
                            self.isUserLoggedIn(false);
                        }
                    });
                };
                newFunction();
                
                function onNavDrawerClose(event) {
                    self.navDrawerOn = false;
                    if (!self.waitForAnnouncement) {
                        document.getElementById('drawerToggleButton').focus();
                        return;
                    }

                    setTimeout(function () {
                        document.getElementById('drawerToggleButton').focus();
                        self.waitForAnnouncement = false;
                    }, 2500);
                }

                // Header
                // Application Name used in Branding Area
                self.appName = ko.observable(" NHC-Receipt");
                // User Info used in Global Navigation area
                self.userLogin = ko.observable();
                // Footer
              
            }

            return new ControllerViewModel();
        }
);
