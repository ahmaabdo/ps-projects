/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your application specific code will go here
 */
define(['knockout', 'ojs/ojmodule-element-utils', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils',
    'ojs/ojrouter', 'ojs/ojarraydataprovider', 'ojs/ojknockouttemplateutils', 'config/services', 'ojs/ojmodule-element',
    'ojs/ojknockout', 'ojs/ojknockout-validation', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojdialog'],
        function (ko, moduleUtils, ResponsiveUtils, ResponsiveKnockoutUtils,
                Router, ArrayDataProvider, KnockoutTemplateUtils, services) {
            function ControllerViewModel() {
                const self = this;

                self.KnockoutTemplateUtils = KnockoutTemplateUtils;
                const getTranslation = oj.Translations.getTranslatedString;
                // Handle announcements sent when pages change, for Accessibility.
                self.manner = ko.observable('polite');
                self.message = ko.observable();
                self.isUserLoggedIn = ko.observable(false);
                self.Usernametext = ko.observableArray();
                self.SignIn = ko.observable();
                self.forgetPassword = ko.observable();
                self.signoLbl = ko.observable();
                self.changeLbl = ko.observable();
                self.passwordLabel = ko.observable();
                self.userName = ko.observable('');
                self.password = ko.observable('');
                self.loginFailureText = ko.observable();
                self.loginLabel = ko.observable();
                self.windowSize = ko.observable();
                self.screenRange = oj.ResponsiveKnockoutUtils.createScreenRangeObservable();
                var pageBody = $('*');
                self.disOnLogin = ko.observable(false);
                self.isUserLoggedIn = ko.observable();
                self.tracker = ko.observable();
                self.languageSwitch_lng = ko.observable();
                self.confirmMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.loading = ko.observable(false);
                self.refreshViewForLanguage = ko.observable(false);
                self.showDashboard = ko.observable();
                //******************************//
                self.hostUrl = ko.observable('');
                self.jwt = ko.observable('');
                self.userName = ko.observable('');
                self.loginDateFromSass = ko.observable('');
                self.JWTExpired = ko.observable(false);
                self.loginVaild = ko.observable(false);
                /*/************************/

                // Media queries for repsonsive layouts
                var smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                self.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);

                // Router setup
                self.router = Router.rootInstance;
                self.router.configure({
                    'uploadScreen': {label: 'Upload', value: 'main/uploadScreen', isDefault: true}
                });
                Router.defaults['urlAdapter'] = new Router.urlParamAdapter();

                self.displaySreenRange = ko.computed(function () {
                    var range = self.screenRange();
                    if (range === oj.ResponsiveUtils.SCREEN_RANGE.SM) {
                        self.windowSize('SM');
                    } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.MD) {
                        self.windowSize('MD');
                    } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.LG) {
                        self.windowSize('LG');
                    } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.XL) {
                        self.windowSize('XL');
                    }
                });

                self.getLocale = function () {
                    return oj.Config.getLocale();

                };
                self.setLocale = function (lang) {

                    oj.Config.setLocale(lang,
                            function () {
                                self.refreshViewForLanguage(false);
                                if (lang === 'ar') {
                                    $("html").attr("lang", lang);
                                    $("html").attr("dir", "rtl");
                                } else {
                                    $("html").attr("lang", lang);
                                    $("html").attr("dir", "ltr");
                                }
                                initTranslations();
                                //self.getBusinessUnits();

                                self.refreshViewForLanguage(true);
                            }
                    );
                };

                self.loadModule = function () {
                    self.moduleConfig = ko.pureComputed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        return moduleUtils.createConfig({viewPath: viewPath,
                            viewModelPath: modelPath, params: {parentRouter: self.router}});
                    });
                };

                // Navigation setup
                var navData = [{}];
                self.navDataProvider = new ArrayDataProvider(navData, {keyAttributes: 'id'});


                // change language functions
                self.switchl = function () {
                    document.querySelector("#switch").open();
                };
                self.switchclose = function () {
                    document.querySelector("#switch").close();
                };
                self.switchLanguage = function () {
                    document.querySelector("#switch").close();
                    if (self.getLocale() === "ar") {
                        self.setLocale("en-US");
                        localStorage.setItem("selectedLanguage", "en-US");
                    } else if (self.getLocale() === "en-US") {
                        self.setLocale("ar");
                        localStorage.setItem("selectedLanguage", "ar");
                    }

                };

                self._showComponentValidationErrors = function (trackerObj) {
                    if (trackerObj !== undefined) {
                        trackerObj.showMessages();
                        if (trackerObj.focusOnFirstInvalid())
                            return false;
                    }
                    return true;
                };
                // check user authentication
                self.onLogin = function () {
                    if (!self.userName() || !self.password())
                        return;
                    //self.loading(true);
                    self.disOnLogin(true);
                    $(".apLoginBtn").addClass("loading");
                    pageBody.css('cursor', 'wait');
                    // Validating the components
                    self.loginFailureText("");
                    var trackerObj = ko.utils.unwrapObservable(self.tracker);
                    //                    
                    if (!this._showComponentValidationErrors(trackerObj)) {
                        return;
                    }
                    var loginSuccessCbFn = function () {
                        self.loading(true);
                        //end user login histiry
                        $(".apLoginBtn").removeClass("loading");
                        //self.loading(false);
                        self.disOnLogin(false);
                        self.router.go('uploadScreen');
                        self.isUserLoggedIn(true);
                        self.loginFailureText("");
                     
                    };
                    var loginFailCbFn = function () {
                        //self.loading(false);
                        $(".apLoginBtn").removeClass("loading");
                        self.disOnLogin(false);
                        //self.hidePreloader();
                        self.isUserLoggedIn(false);
                        self.loginFailureText("error");
                        self.loginFailureText(getTranslation("login.loginFailureText"));
                    };
                    //                    self.showPreloader();
                    function authinticate(data) {

                        var result = data.result;
                        console.log(result)
                        if (result) {
                            loginSuccessCbFn();
                        } else {
                            loginFailCbFn();
                        }
                        $(".apLoginBtn").removeClass("loading");
                        self.disOnLogin(false);

                    }
                    services.authenticate(self.userName(), self.password()).then(authinticate, self.failCbFn);
                    $(".apLoginBtn").removeClass("loading");
                };


                var newFunction = function () {
                    $.when(jwt(self)).done(function () {
                        
                        if (self.loginVaild())
                        {
                            self.loading(true);
                            self.disOnLogin(false);
                            self.router.go('uploadScreen');
                            self.isUserLoggedIn(true);
                        } else {
                            self.disOnLogin(false);
                            self.isUserLoggedIn(false);
                        }
                    });
                };
               newFunction();
                function initTranslations() {
                    self.Usernametext(getTranslation("login.userName"));
                    self.passwordLabel(getTranslation("login.Password"));
                    self.loginLabel(getTranslation("login.loginLabel"));
                    self.SignIn(getTranslation("login.SignIn"));
                    self.forgetPassword(getTranslation("login.forgetPassword"));
                    self.changeLbl(getTranslation("labels.changeLang"));
                    self.signoLbl(getTranslation("labels.signOut"));
                    if (self.loginFailureText() !== undefined && self.loginFailureText() !== "") {
                        self.loginFailureText(getTranslation("login.loginFailureText"));
                    }
                    self.languageSwitch_lng(getTranslation("common.switchLang"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                }
                initTranslations();

            }

            return new ControllerViewModel();
        }
);
