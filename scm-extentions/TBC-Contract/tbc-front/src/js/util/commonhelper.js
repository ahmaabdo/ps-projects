define([], function () {

    function commonHelper() {
        var self = this;

        self.purchaseOrdersURL = 'upload';
        
         self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };

        self.getInternalRest = function () {

            var host = "http://127.0.0.1:7101/tbc-backend-test/rest/";
            // var host = "https://132.145.47.152/tbc-backend-test/resources/";

            return host;
        };

        self.isConnected = function () {
            //show toast
            if (navigator.connection && navigator.connection.type) {
                if (navigator.connection.type == 'none') {
                    if (window.plugins && window.plugins.toast)
                        window.plugins.toast.showShortCenter('No Internet Connection!', () => {
                        }, () => {
                        });
                    return false;
                }
            }
            return true;
        };
    }

    return new commonHelper();
});