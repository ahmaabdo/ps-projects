/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * uploadScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'appController','ojs/ojpagingdataproviderview'
, 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol', 'ojs/ojtable', 'xlsx', 'ojs/ojknockout', 'ojs/ojbutton',
    'ojs/ojmessages', 'ojs/ojinputtext', 'ojs/ojfilepicker', 'ojs/ojdialog'
], function (oj, ko, $, services, app,PagingDataProviderView, ArrayDataProvider) {
    /**
     * The view model for the main content view template
     */
    function uploadScreenContentViewModel() {
        var self = this;
        var selected_file = null;
        var getTranslation = oj.Translations.getTranslatedString;
        self.labels = ko.observableArray([]);
        self.messages = ko.observableArray();
        self.messagesDataprovider = new oj.ArrayDataProvider(self.messages);
        self.acceptArr = ko.observableArray([]);
        self.acceptArr = ko.pureComputed(function () {
            var accept = ".xls,.xlsx";
            return accept ? accept.split(",") : [];
        }, self);
        self.headersData = ko.observableArray();
        self.linesData = ko.observableArray();
        self.dialogMsg = ko.observable();
        self.tableColumns2 = ko.observableArray([]);
        self.tableArray2 = ko.observableArray([]);
        self.dataSource = ko.observable(new PagingDataProviderView(new ArrayDataProvider(self.tableArray2, { idAttribute: 'contractNumber' })));
        self.uploadDisable = ko.observable(false);
        self.pickerDisable = ko.observable(false);

        self.selectListener = function (event) {
            var files = event.detail.files;
            var reader = new FileReader();
            selected_file = files[0];
            self.labels.picker_text(selected_file.name);
            self.headersData([]);
            self.linesData([]);
            reader.onload = function (e) {
                var data = e.target.result;
                var workbook = XLSX.read(data, { type: 'binary' ,cellDates: true});
                var data1 = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
                var data2 = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[1]]);
                for (var i = 0; i < data1.length; i++) {
                    data1[i].__EMPTY = typeof data1[i].__EMPTY !=='undefined'?self.formatDate(new Date(data1[i].__EMPTY)):null;
                    data1[i].__EMPTY_1 =typeof data1[i].__EMPTY_1 !=='undefined'? self.formatDate(new Date(data1[i].__EMPTY_1)):null;
                    self.headersData.push(data1[i]);
                }
                for (var j = 0; j < data2.length; j++) {
                    data2[j].__EMPTY = typeof data2[j].__EMPTY !=='undefined'?self.formatDate(new Date(data2[j].__EMPTY)):null;
                    data2[j].__EMPTY_1 = typeof data2[j].__EMPTY_1 !=='undefined'? self.formatDate(new Date(data2[j].__EMPTY_1)):null;
                    self.linesData.push(data2[j]);
                }
                console.log(self.headersData());
                console.log(self.linesData());
            };
            reader.onerror = function (ex) {
            };
            reader.readAsBinaryString(files[0]);
        }

        self.uploadAction = function (event) {
            self.tableArray2([]);

            if (selected_file != null) {
                self.uploadDisable(true);
                self.pickerDisable(true);
                self.labels.picker_text(getTranslation("uploadScreen.uploadingandprocessingdata"));
                var dataToSend = {
                    headers: self.headersData(),
                    lines: self.linesData()
                };
                console.log(dataToSend);
                setTimeout(() => {
                    services.addGeneric("contract/upload", JSON.stringify(dataToSend))
                        .then(e => {
                            try { e = JSON.parse(e) } catch (ignored) { }
                            self.labels.picker_text(getTranslation("uploadScreen.click_or_dragdrop"));
                            selected_file = null;
                            console.log(e.length)
                            if(e.status=='FileError'){
                            self.dialogMsg(e.message);
                            document.getElementById("messageDialog2").open();
                            }else{
                                self.tableArray2(e);
                                document.getElementById("dialog1").open();
                            }
                            self.uploadDisable(false);
                            self.pickerDisable(false);
                        });
                }, 100);
            } else {
                self.messages.push({ severity: "error", summary: getTranslation("uploadScreen.upload_file_not_selected"), autoTimeout: 0 });//default time out
            }
            return true;
        };

        self.downloadSample=function(event){
            //download("Contract File TBC - Upload.xlsx",'css/Contract File TBC - Upload.xlsx');
            var link = document.createElement("a");
            link.download = "Contract File TBC - Upload.xlsx";
            link.href = "css/Contract File TBC - Upload.xlsx";
            link.click();
        };
        // for translation
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });
        self.formatDate = function (date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        self.connected = function () {
            initTranslations();
            app.loading(false);

        };

        self.closeDialog = function () {
            document.getElementById("dialog1").close();
            self.uploadDisable(false);
            self.pickerDisable(false);
        }
        self.closeMessageDialog = function () {
            document.getElementById("messageDialog1").close();
        };
        self.closeMessageDialog2 = function () {
            document.getElementById("messageDialog2").close();
        };

        function initTranslations() {
            self.labels = {
                title: ko.observable(getTranslation('uploadScreen.title')),
                upload: ko.observable(getTranslation('uploadScreen.upload')),
                picker_text: ko.observable(getTranslation('uploadScreen.click_or_dragdrop')),
                download:ko.observable(getTranslation('uploadScreen.download'))
            }
            self.tableColumns2([
                {
                    "headerText": "#",
                    "field": "count"
                },
                {
                    "headerText": "Contract Number",
                    "field": "contractNumber"
                },
                {
                    "headerText": "Status",
                    "field": "status"
                },
                {
                    "headerText": "Message",
                    "field": "message"
                }
            ]);
        }
    }

    return uploadScreenContentViewModel;
});
