package common.restHelper;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author user
 */
public class CommonConfigReader {

    private static ResourceBundle bundle = null;

    public static String getValue(String key) {
        if (bundle == null) {
            Locale locale = new Locale("en", "");
             bundle = ResourceBundle.getBundle("connNew", locale);
        }
        return bundle.getString(key);
    }
}
