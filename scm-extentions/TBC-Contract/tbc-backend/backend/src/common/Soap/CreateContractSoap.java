package common.Soap;

import common.restHelper.RestHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;

import java.io.UnsupportedEncodingException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import oracle.i18n.net.URLDecoder;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

public class CreateContractSoap {
    
    RestHelper restHelper = new RestHelper();
    
    private  HashMap<String, ArrayList<String>> headerValues =
        new HashMap<String, ArrayList<String>>();
    
    private  HashMap<String, ArrayList<String>> linesValues =
            new HashMap<String, ArrayList<String>>();
    
    private  String[] header =
    { "Contract Number (Isnaad)", "Contract Name",
      "Contract Detailed Description", " Contract Value ", "Customer Name",
      "__EMPTY_1"/* start date */, "__EMPTY_2"/* finish date */, "Bill-to Site", "Contract Administrartor", "Ship-to Account Number",
      "Ship-to Site number", "Bill-to Account Number",
      "Transaction Type", "Contract Organization", "Invoice Method",
      "Billing Cycle", "Billing Currency Type", "Labor Format",
      "Nonlabor Format	", "Event Format	", "Payment Terms", "Revenue Method","Contract Type","Business Unit","Legal Entity"};
    

    private  String[] lines =
    { "Line Type", "Contract Number ","__EMPTY_1", "__EMPTY_2",
      "Contract Name", "Line Name", "Line description", " Line amount ",
      " Funded Amount ", "Invoice Method", "Revenue Method", "Project Number",
      "Project Name ", "Task Name", "Task Number" };

    public String createContract(String data) {
        String response ="";
        try {
            JSONObject objData=new JSONObject(data);
            JSONArray headersList = new JSONArray(objData.get("headers").toString());
            JSONArray linesList = new JSONArray(objData.get("lines").toString());
            for (int j = 0; j < header.length; j++) {
                ArrayList<String> array=new ArrayList<String>();
                for (int i = 1; i < headersList.length(); i++) {
                    JSONObject obj = new JSONObject(headersList.get(i).toString());
                    array.add(obj.get(header[j]).toString());
                }
                headerValues.put(header[j],array);
            }
            ////////////////////////
            for (int j = 0; j < lines.length; j++) {
                ArrayList<String> array=new ArrayList<String>();
                for (int i = 1; i < linesList.length(); i++) {
                    JSONObject obj = new JSONObject(linesList.get(i).toString());
                    array.add(obj.get(lines[j]).toString());
                   
                }
                linesValues.put(lines[j],array);
            }
            response = createSoapRequest(headerValues,linesValues);
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject obj=new JSONObject();
            obj.put("status", "FileError");
            obj.put("message", "This Excel file is wrong please download sample one and add your data in it");
            response=obj.toString();
        }
        return response; //error while try later
    }
    public String createSoapRequest(HashMap<String, ArrayList<String>> headerValues,HashMap<String, ArrayList<String>> linesValues) {
       
        ArrayList<JSONObject> responseArray=new  ArrayList<JSONObject>();

        for(int i = 0; i < headerValues.get("Contract Name").size(); i++){
                  JSONObject response = new JSONObject();
                  StringBuilder contractLines = new StringBuilder();
                  
                  for(int x = 0; x < linesValues.get("Contract Name").size(); x++){
                      if(headerValues.get("Contract Number (Isnaad)").get(i).equals(linesValues.get("Contract Number ").get(x))){
                          contractLines.append(
                          "            <con:ContractLine>\n" +
                          "               <con:ItemNameTxt>"+linesValues.get("Line Name").get(x)+"</con:ItemNameTxt>\n" +
                          "               <con:ItemDescription>"+linesValues.get("Line description").get(x)+"</con:ItemDescription>\n" +
                          "               <con:StartDate>"+linesValues.get("__EMPTY_1").get(x).toString()+"</con:StartDate>\n" +
                          "               <con:EndDate>"+linesValues.get("__EMPTY_2").get(x).toString()+"</con:EndDate>\n" +
                          "               <con:LineAmount currencyCode=\"SAR\">"+(int)Double.parseDouble(linesValues.get(" Line amount ").get(x).replaceAll(",", ""))+"</con:LineAmount>\n" +
                          "               <con:LineValueAmount currencyCode=\"SAR\">"+(int)Double.parseDouble(linesValues.get(" Line amount ").get(x).replaceAll(",", ""))+"</con:LineValueAmount>\n" +
                          "               <con:LineTypeName>"+linesValues.get("Line Type").get(x)+"</con:LineTypeName>\n" +
                          "               <con:AssociatedProject>\n" +
                          "                  <con1:ProjectNumber>"+linesValues.get("Project Number").get(x)+"</con1:ProjectNumber>\n" +
                          "                  <con1:TaskNumber>"+linesValues.get("Task Number").get(x)+"</con1:TaskNumber>\n" +
                          "                  <con1:FundingAmount currencyCode=\"SAR\">"+(int)Double.parseDouble(linesValues.get(" Funded Amount ").get(x).replaceAll(",", ""))+"</con1:FundingAmount>\n" +
                          "               </con:AssociatedProject>\n" +
                          "            </con:ContractLine>\n");
                      }
                  }
                  
                  String soapRequest =
                        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\" xmlns:con=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/\" xmlns:head=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/header/flex/header/\" xmlns:par=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/partyContact/\" xmlns:par1=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/party/\" xmlns:lin=\"http://xmlns.oracle.com/apps/flex/contracts/coreAuthoring/lines/\" xmlns:con1=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractPublicService/\" xmlns:con2=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractBillingService/\" xmlns:bil=\"http://xmlns.oracle.com/apps/projects/billing/contracts/flex/BilllPlanDff/\">\n" +
                                   "   <soapenv:Header/>\n" +
                                   "   <soapenv:Body>\n" +
                                   "      <typ:createContract>\n" +
                                   "         <typ:contractHeader>\n" +
                                   "            <con:ExternalSource>FUSION</con:ExternalSource>\n" +
                                   "            <con:ContractNumber>"+headerValues.get("Contract Number (Isnaad)").get(i)+"</con:ContractNumber>\n" +
                                   "            <con:Cognomen>"+headerValues.get("Contract Name").get(i).toString()+"</con:Cognomen>\n" +
                                   "            <con:StartDate>"+headerValues.get("__EMPTY_1").get(i).toString()+"</con:StartDate>\n" +
                                   "            <con:EndDate>"+headerValues.get("__EMPTY_2").get(i).toString()+"</con:EndDate>\n" +
                                   "            <con:BillToSiteUseLocation>"+headerValues.get("Bill-to Site").get(i)+"</con:BillToSiteUseLocation>\n" +
                                   "            <con:BillToAccountNumber>"+headerValues.get("Bill-to Account Number").get(i)+"</con:BillToAccountNumber>\n" +
                                   "            <con:ShipToAccountNumber>"+headerValues.get("Ship-to Account Number").get(i)+"</con:ShipToAccountNumber>\n" +
                                   "            <con:ShipToSiteUseLocation>"+headerValues.get("Ship-to Site number").get(i)+"</con:ShipToSiteUseLocation>\n" +
                                   "            <con:Description>"+headerValues.get("Contract Detailed Description").get(i)+"</con:Description>\n" +
        //                             "            <con:CreatedBy>"+headerValues.get("Contract Administrartor").get(i)+"</con:CreatedBy>\n" +
                                   "            <con:LegalEntityName>"+headerValues.get("Legal Entity").get(i)+"</con:LegalEntityName>\n" +
                                   "            <con:OrgName>"+headerValues.get("Business Unit").get(i)+"</con:OrgName>\n" +
                                   "            <con:OwningOrgName>"+headerValues.get("Contract Organization").get(i)+"</con:OwningOrgName>\n" +
                                   "            <con:ContractTypeName>Revenue Contract</con:ContractTypeName>\n" +
                                   "            <con:TransactionTypeName>"+headerValues.get("Transaction Type").get(i)+"</con:TransactionTypeName>\n" +
                                   "            <con:ContractHeaderFlexfieldVA>\n" +
                                   "            </con:ContractHeaderFlexfieldVA>\n"

                                                  + contractLines +

                                   "            <con:ContractParty>\n" +
                                   "               <con:PartyRoleCode>CUSTOMER</con:PartyRoleCode>\n" +
                                   "               <con:Role>Customer</con:Role>\n" +
                                   "               <con:PartyName>"+headerValues.get("Customer Name").get(i)+"</con:PartyName>\n" +
                                   "               <con:ExternalSource>FUSION:1</con:ExternalSource>\n" +
                                   "            </con:ContractParty>\n" +
                                   "            <con:BillPlan>\n" +
                                   "               <con1:BillMethodName>"+headerValues.get("Invoice Method").get(i)+"</con1:BillMethodName>\n" +
                                   "               <con1:BillPlanName>"+headerValues.get("Invoice Method").get(i)+"</con1:BillPlanName>\n" +
                                   "               <con1:InvoiceCurrencyOptCode>CONTRACT</con1:InvoiceCurrencyOptCode>\n" +
                                   "               <con1:BillSetNumber>1</con1:BillSetNumber>\n" +
                                   "               <con1:BillingCurrencyType>"+headerValues.get("Billing Currency Type").get(i)+"</con1:BillingCurrencyType>\n" +
                                   "               <con1:PaymentTerms>"+headerValues.get("Payment Terms").get(i)+"</con1:PaymentTerms>\n" +
                                   "               <con1:BillToAccountNumber>"+headerValues.get("Bill-to Account Number").get(i)+"</con1:BillToAccountNumber>\n" +
                                   "               <con1:BillToSite>"+headerValues.get("Bill-to Site").get(i)+"</con1:BillToSite>\n" +
                                   "               <con1:BillingCycle>"+headerValues.get("Billing Cycle").get(i)+"</con1:BillingCycle>\n" +
                                   "               <con1:LaborInvoiceFormat>"+headerValues.get("Labor Format").get(i)+"</con1:LaborInvoiceFormat>\n" +
                                   "               <con1:NonlaborInvoiceFormat>"+headerValues.get("Nonlabor Format	").get(i)+"</con1:NonlaborInvoiceFormat>\n" +
                                   "               <con1:EventInvoiceFormat>"+headerValues.get("Event Format	").get(i)+"</con1:EventInvoiceFormat>\n" +
                                   "               <con1:InvoiceComment>Manual Event</con1:InvoiceComment>\n" +
                                   "            </con:BillPlan>\n" +
                                   "            <con:RevenuePlan>\n" +
                                   "               <con1:BillMethodName>"+headerValues.get("Revenue Method").get(i)+"</con1:BillMethodName>\n" +
                                   "               <con1:BillPlanName>POC Revenue</con1:BillPlanName>\n" +
                                   "            </con:RevenuePlan>\n" +
                                   "         </typ:contractHeader>\n" +
                                   "      </typ:createContract>\n" +
                                   "   </soapenv:Body>\n" +
                                   "</soapenv:Envelope>";
        
                  // Invoking contract creation SOAP web service
                  System.out.println("Inserting Contract Number: " + headerValues.get("Contract Number (Isnaad)").get(i));
                  response=  importData(soapRequest);
                  response.put("contractNumber",headerValues.get("Contract Number (Isnaad)").get(i));
                  response.put("count",i+1);
                  System.out.println(response.toString());
                  responseArray.add(response);
        }
        return new JSONArray(responseArray).toString() ;
    }
    
    public JSONObject importData(String soapRequest){
        JSONObject response = new JSONObject();
           Document doc=null;
           try {
               doc = restHelper.httpPost(restHelper.getInstanceUrl()+restHelper.getContractURLSoap(), soapRequest);
               
               if (doc.getElementsByTagName("faultstring").getLength() > 0) {
                   int lastMsg=doc.getElementsByTagName("tns:message").getLength()-1;
                   String message=doc.getElementsByTagName("tns:message").item(lastMsg).getTextContent();
                   response.put("status", "Error");
                   message=message.replaceAll("</?[A-Za-z]+>", "");
                   message=message.replaceAll("JBO-OKC:::OKC_INVALID_CON_UNIQUE_KEY: ", "");
                   message=message.replaceAll("JBO-OKC:::OKC_OWNER_ROLE_SRC_MISMATCH: ", "");
                   message=message.replaceAll("JBO-OKC:::OKC_AUTH_CLN_INV_STDT_HDRDT: ", "");
                   if(message.contains("combination") && message.contains("contract") && message.contains("number")){
                       message="This contract found please change number of this contract.";
                   }else if(message.contains("LOV_PartyName_WS") && message.contains("matching")){
                       message="Customer name is wrong please check and edit it.";
                   }else if(message.contains("LOV_BillToAcctNumber_WS") && message.contains("matching")){
                       message="Bill to account number is wrong please check and edit it.";
                   }else if(message.contains("LOV_ShipToAcctNumber") && message.contains("matching")){
                       message="Ship to account number is wrong please check and edit it.";
                   }else if(message.contains("LOV_BillToLocation") && message.contains("matching")){
                       message="Bill to location is wrong please check and edit it.";
                   }else if(message.contains("ShipToLocation") && message.contains("matching")){
                       message="Ship to location is wrong please check and edit it.";
                   }
                   response.put("message", message);
               } else {
                   StringBuilder contractLine = new StringBuilder();
                   //Building contract line
                    NodeList nList  = doc.getElementsByTagName("ns1:ContractLine");
                   for (int i = 0; i < nList.getLength(); ++i) {
                       NamedNodeMap map = nList.item(i).getAttributes();
                       for (int j = 0; j < map.getLength(); j++) {
                           Node eNode = nList.item(i);
                           Element name = (Element)eNode;
                           contractLine.append("<con:ContractLine>\n" +
                                   "<con:ExternalSource>FUSION</con:ExternalSource>\n" +
                                   "<con:ExternalKey>" + name.getElementsByTagName("ns1:ExternalKey").item(0).getTextContent() + "</con:ExternalKey>\n" +
                                   "<con:BillPlan>Amount Based Invoice</con:BillPlan>\n" +
                                   "<con:RevenuePlan>POC Revenue</con:RevenuePlan>\n" +
                                   "</con:ContractLine>\n");
                       }
                   }
                   
                   String updateContractPayload = 
                       "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\" xmlns:con=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/\" xmlns:head=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/header/flex/header/\" xmlns:par=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/partyContact/\" xmlns:par1=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/party/\" xmlns:lin=\"http://xmlns.oracle.com/apps/flex/contracts/coreAuthoring/lines/\" xmlns:con1=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractPublicService/\" xmlns:con2=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractBillingService/\" xmlns:bil=\"http://xmlns.oracle.com/apps/projects/billing/contracts/flex/BilllPlanDff/\">\n" +
                       "   <soapenv:Header/>\n" +
                       "   <soapenv:Body>\n" +
                       "      <typ:updateContract>\n" +
                       "         <typ:contractHeader>\n" +
                       "            <con:Id>"+doc.getElementsByTagName("ns1:Id").item(0).getTextContent()+"</con:Id>\n" +
                       "            <con:MajorVersion>"+doc.getElementsByTagName("ns1:MajorVersion").item(0).getTextContent()+"</con:MajorVersion>\n" +
                       "            <con:ExternalSource>FUSION</con:ExternalSource>\n" +
                       "            <con:ExternalKey>"+doc.getElementsByTagName("ns1:Id").item(0).getTextContent()+"</con:ExternalKey>\n" 
                                       + contractLine +
                       "         </typ:contractHeader>\n" +
                       "      </typ:updateContract>\n" +
                       "   </soapenv:Body>\n" +
                       "</soapenv:Envelope>";
                   // Update contract SOAP web service    
                  response= updatedContract(updateContractPayload);
               }
           } catch (Exception e) {
               e.printStackTrace();
               System.err.println("Update Contract Error...");
           }
           return response;
       }
    
    public JSONObject updatedContract(String soapRequest) {
            
            Document doc = null;
            JSONObject response=new JSONObject();
            try {
                doc = restHelper.httpPost(restHelper.getInstanceUrl()+restHelper.getContractURLSoap(), soapRequest);
                
                if (doc.getElementsByTagName("faultstring").getLength() > 0) {
                    int lastMsg=doc.getElementsByTagName("tns:message").getLength()-1;
                    String message=doc.getElementsByTagName("tns:message").item(lastMsg).getTextContent();
                    response.put("status", "Error");
                    message=message.replaceAll("</?[A-Za-z]+>", "");
                   message=message.replaceAll("JBO-OKC:::OKC_INVALID_CON_UNIQUE_KEY: OKC-195788", "");
                   message=message.replaceAll("JBO-OKC:::OKC_OWNER_ROLE_SRC_MISMATCH: OKC-196156", "");
                    response.put("message", message);
                } else {
                    response.put("status", "Successs");
                    response.put("message", "Success in creating contract");  
                }
                return response;
            } catch (Exception e) {
                System.err.println("Error while inserting Contract");
                e.printStackTrace();
            }
            return response;
        }

    public static void main(String[] args) {

    }

}
