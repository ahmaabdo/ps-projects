package com.appspro.tbc.rest;

import common.Soap.CreateContractSoap;

import java.io.PrintStream;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;



@Path("contract")

public class ContractRest extends CreateContractSoap {
    
    @POST
    @Path("/upload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response upload(String body) {
        try {
            String res = new CreateContractSoap().createContract(body.toString());
            return Response.ok(res).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ok("{status:error , message:Internal server error}").build();
        }
    }
}
