package common.Soap;

import common.restHelper.RestHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;

import java.io.UnsupportedEncodingException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

public class CreateContractSoap {
    
    RestHelper restHelper = new RestHelper();
    
    private  HashMap<String, ArrayList<String>> headerValues =
        new HashMap<String, ArrayList<String>>();
    
    private  HashMap<String, ArrayList<String>> linesValues =
            new HashMap<String, ArrayList<String>>();
    
    private  String[] header =
    { "Contract Number (Isnaad)", "Contract Name",
      "Contract Detailed Description", "Contract Value", "Customer Name",
      "__EMPTY"/* start date */, "__EMPTY_1"/* finish date */, "Bill-to Site", "Contract Administrartor",
      "Ship-to Account Number",
      "Ship-to Site number", "Bill-to Account Number",
      "Transaction Type", "Contract Organization", "Invoice Method",
      "Billing Cycle", "Billing Currency Type", "Labor Format",
      "Nonlabor Format", "Event Format", "Payment Terms", "Revenue Method","Contract Type","Business Unit","Legal Entity"};
    

    private  String[] lines =
    { "Line Type", "Contract Number","__EMPTY", "__EMPTY_1",
      "Contract Name", "Line Name", "Line description", "Line amount",
      "Funded Amount", "Invoice Method", "Revenue Method", "Project Number",
      "Project Name", "Task Name", "Task Number" };

    public String createContract(String data) {
        String response ="";
        JSONObject obj=new JSONObject();
        try {
            JSONObject objData=new JSONObject(data);
            JSONArray headersList = new JSONArray(objData.get("headers").toString());
            JSONArray linesList = new JSONArray(objData.get("lines").toString());
            for (int j = 0; j < header.length; j++) {
                ArrayList<String> array=new ArrayList<String>();
                for (int i = 1; i < headersList.length(); i++) {
                    JSONObject obj2 = new JSONObject(headersList.get(i).toString());
                    array.add(obj2.has(header[j])?obj2.get(header[j]).toString():"");
                }
                headerValues.put(header[j],array);
            }
            ////////////////////////
            for (int j = 0; j < lines.length; j++) {
                ArrayList<String> array=new ArrayList<String>();
                for (int i = 1; i < linesList.length(); i++) {
                    JSONObject obj3 = new JSONObject(linesList.get(i).toString());
                    array.add(obj3.has(lines[j])?obj3.get(lines[j]).toString():"");
                   
                }
                linesValues.put(lines[j],array);
            }
            response = createSoapRequest(headerValues,linesValues);
        } catch (Exception e) {
            e.printStackTrace();
            obj.put("status", "FileError");
            String message=e.getMessage().replaceFirst("JSONObject", "")+" or "+" Cann't be empty ";
            obj.put("message", message);
            response=obj.toString();
        }
        return response; //error while try later
    }
    public String createSoapRequest(HashMap<String, ArrayList<String>> headerValues,HashMap<String, ArrayList<String>> linesValues) {
       
        ArrayList<JSONObject> responseArray=new  ArrayList<JSONObject>();

        for(int i = 0; i < headerValues.get("Contract Name").size(); i++){
                  JSONObject response = new JSONObject();
                  StringBuilder contractLines = new StringBuilder();
                  StringBuilder contractSaop = new StringBuilder();
                  
                  for(int x = 0; x < linesValues.get("Contract Name").size(); x++){
                      if(headerValues.get("Contract Number (Isnaad)").get(i).equals(linesValues.get("Contract Number").get(x).toString())){
                          contractLines.append("<con:ContractLine><con:ItemNameTxt>");
                          contractLines.append(linesValues.get("Line Name").get(x).toString());
                          contractLines.append("</con:ItemNameTxt><con:ItemDescription>");
                          contractLines.append(linesValues.get("Line description").get(x).toString());
                          contractLines.append("</con:ItemDescription><con:StartDate>");
                          contractLines.append(linesValues.get("__EMPTY").get(x).toString());
                          contractLines.append("</con:StartDate>\n");
                          if(!"null".equalsIgnoreCase(linesValues.get("__EMPTY_1").get(x).toString()) &&
                             !linesValues.get("__EMPTY_1").get(x).toString().isEmpty()&&
                              linesValues.get("__EMPTY_1").get(x).toString() !=null){
                          contractLines.append("<con:EndDate>");
                          contractLines.append(linesValues.get("__EMPTY_1").get(x).toString());
                          contractLines.append("</con:EndDate>\n");
                          }
                          contractLines.append("<con:LineAmount currencyCode=\"SAR\">");
                          contractLines.append((int)Double.parseDouble(linesValues.get("Line amount").get(x).replaceAll(",", "").toString()));
                          contractLines.append("</con:LineAmount> <con:LineValueAmount currencyCode=\"SAR\">");
                          contractLines.append((int)Double.parseDouble(linesValues.get("Line amount").get(x).replaceAll(",", "").toString()));
                          contractLines.append("</con:LineValueAmount><con:LineTypeName>");
                          contractLines.append(linesValues.get("Line Type").get(x).toString());
                          contractLines.append("</con:LineTypeName>\n");
                          contractLines.append("<con:AssociatedProject><con1:ProjectNumber>");
                          contractLines.append(linesValues.get("Project Number").get(x).toString().trim());
                          contractLines.append("</con1:ProjectNumber>\n");
                          if(!"null".equalsIgnoreCase(linesValues.get("Task Number").get(x).toString()) &&
                             !linesValues.get("Task Number").get(x).toString().isEmpty()&&
                              linesValues.get("Task Number").get(x).toString() !=null){
                          contractLines.append("<con1:TaskNumber>");
                          contractLines.append(linesValues.get("Task Number").get(x).toString());
                          contractLines.append("</con1:TaskNumber>\n");
                              }
                          contractLines.append("<con1:FundingAmount currencyCode=\"SAR\">");
                          contractLines.append((int)Double.parseDouble(linesValues.get("Funded Amount").get(x).replaceAll(",", "")));
                    contractLines.append("</con1:FundingAmount></con:AssociatedProject></con:ContractLine>\n");
                }
            }

// contract body 
            contractSaop.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\" xmlns:con=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/\" xmlns:head=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/header/flex/header/\" xmlns:par=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/partyContact/\" xmlns:par1=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/party/\" xmlns:lin=\"http://xmlns.oracle.com/apps/flex/contracts/coreAuthoring/lines/\" xmlns:con1=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractPublicService/\" xmlns:con2=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractBillingService/\" xmlns:bil=\"http://xmlns.oracle.com/apps/projects/billing/contracts/flex/BilllPlanDff/\">\n");
            contractSaop.append("<soapenv:Header/><soapenv:Body> <typ:createContract><typ:contractHeader> <con:ExternalSource>FUSION</con:ExternalSource>");
            contractSaop.append("<con:ContractNumber>");
            contractSaop.append(headerValues.get("Contract Number (Isnaad)").get(i).toString());
            contractSaop.append("</con:ContractNumber><con:Cognomen>");
            contractSaop.append(headerValues.get("Contract Name").get(i).toString());
            contractSaop.append("</con:Cognomen><con:StartDate>" );
            contractSaop.append(headerValues.get("__EMPTY").get(i).toString());
            contractSaop.append("</con:StartDate>\n");
            if(!"null".equalsIgnoreCase(headerValues.get("__EMPTY_1").get(i).toString()) &&
               !headerValues.get("__EMPTY_1").get(i).toString().isEmpty()&&
                headerValues.get("__EMPTY_1").get(i).toString() !=null){
            contractSaop.append( "<con:EndDate>");
            contractSaop.append(headerValues.get("__EMPTY_1").get(i).toString());
            contractSaop.append("</con:EndDate>\n");
                }
            contractSaop.append("<con:BillToSiteUseLocation>");
            contractSaop.append(headerValues.get("Bill-to Site").get(i));
            contractSaop.append("</con:BillToSiteUseLocation><con:BillToAccountNumber>");
            contractSaop.append(headerValues.get("Bill-to Account Number").get(i).toString());
            contractSaop.append("</con:BillToAccountNumber><con:ShipToAccountNumber>");
            contractSaop.append(headerValues.get("Ship-to Account Number").get(i).toString());
            contractSaop.append("</con:ShipToAccountNumber><con:ShipToSiteUseLocation>");
            contractSaop.append(headerValues.get("Ship-to Site number").get(i).toString());
            contractSaop.append("</con:ShipToSiteUseLocation>\n");
            if(!"null".equalsIgnoreCase(headerValues.get("Contract Detailed Description").get(i).toString()) &&
               !headerValues.get("Contract Detailed Description").get(i).toString().isEmpty()&&
                headerValues.get("Contract Detailed Description").get(i).toString() !=null){
            contractSaop.append("<con:Description>");
            contractSaop.append(headerValues.get("Contract Detailed Description").get(i).toString());
            contractSaop.append("</con:Description>\n");
                }
            contractSaop.append("<con:LegalEntityName>");
            contractSaop.append(headerValues.get("Legal Entity").get(i).toString());
            contractSaop.append("</con:LegalEntityName><con:OrgName>");
            contractSaop.append(headerValues.get("Business Unit").get(i).replaceAll("&", "&amp;").toString());
            contractSaop.append("</con:OrgName> <con:OwningOrgName>");
            contractSaop.append(headerValues.get("Contract Organization").get(i).toString());
            contractSaop.append("</con:OwningOrgName><con:ContractTypeName>Revenue Contract</con:ContractTypeName>");
            contractSaop.append("<con:TransactionTypeName>");
            contractSaop.append(headerValues.get("Transaction Type").get(i).toString());
            contractSaop.append("</con:TransactionTypeName><con:ContractHeaderFlexfieldVA></con:ContractHeaderFlexfieldVA>\n");
            contractSaop.append(contractLines.toString());
            contractSaop.append("<con:ContractParty><con:PartyRoleCode>CUSTOMER</con:PartyRoleCode><con:Role>Customer</con:Role><con:PartyName>");
            contractSaop.append(headerValues.get("Customer Name").get(i).toString());
            contractSaop.append("</con:PartyName><con:ExternalSource>FUSION:1</con:ExternalSource></con:ContractParty> <con:BillPlan><con1:BillMethodName>");
            contractSaop.append(headerValues.get("Invoice Method").get(i).toString());
            contractSaop.append("</con1:BillMethodName> <con1:BillPlanName>");
            contractSaop.append(headerValues.get("Invoice Method").get(i).toString());
            contractSaop.append("</con1:BillPlanName><con1:InvoiceCurrencyOptCode>CONTRACT</con1:InvoiceCurrencyOptCode><con1:BillSetNumber>1</con1:BillSetNumber><con1:BillingCurrencyType>");
            contractSaop.append(headerValues.get("Billing Currency Type").get(i).toString());
            contractSaop.append("</con1:BillingCurrencyType> <con1:PaymentTerms>");
            contractSaop.append(headerValues.get("Payment Terms").get(i).toString());
            contractSaop.append("</con1:PaymentTerms><con1:BillToAccountNumber>");
            contractSaop.append(headerValues.get("Bill-to Account Number").get(i).toString());
            contractSaop.append("</con1:BillToAccountNumber> <con1:BillToSite>");
            contractSaop.append(headerValues.get("Bill-to Site").get(i).toString());
            contractSaop.append("</con1:BillToSite><con1:BillingCycle>");
            contractSaop.append(headerValues.get("Billing Cycle").get(i).toString());
            contractSaop.append("</con1:BillingCycle> <con1:LaborInvoiceFormat>");
            contractSaop.append(headerValues.get("Labor Format").get(i).toString());
            contractSaop.append("</con1:LaborInvoiceFormat><con1:NonlaborInvoiceFormat>");
            contractSaop.append(headerValues.get("Nonlabor Format").get(i).toString());
            contractSaop.append("</con1:NonlaborInvoiceFormat><con1:EventInvoiceFormat>");
            contractSaop.append(headerValues.get("Event Format").get(i).toString());
            contractSaop.append("</con1:EventInvoiceFormat> <con1:InvoiceComment>Manual Event</con1:InvoiceComment></con:BillPlan><con:RevenuePlan><con1:BillMethodName>");
            contractSaop.append(headerValues.get("Revenue Method").get(i).toString());
            contractSaop.append("</con1:BillMethodName><con1:BillPlanName>POC Revenue</con1:BillPlanName></con:RevenuePlan></typ:contractHeader></typ:createContract></soapenv:Body></soapenv:Envelope>");
                  // Invoking contract creation SOAP web service
                  System.out.println("Inserting Contract Number: " + headerValues.get("Contract Number (Isnaad)").get(i).toString());
                  response=  importData(contractSaop.toString());
                  response.put("contractNumber",headerValues.get("Contract Number (Isnaad)").get(i).toString());
                  response.put("count",i+1);
                  System.out.println(response.toString());
                  responseArray.add(response);
        }
        return new JSONArray(responseArray).toString() ;
    }
    
    public JSONObject importData(String soapRequest){
        JSONObject response = new JSONObject();
           Document doc=null;
           try {
               doc = restHelper.httpPost(restHelper.getInstanceUrl()+restHelper.getContractURLSoap(), soapRequest);
               
               if (doc.getElementsByTagName("faultstring").getLength() > 0) {
               int lastMsg=doc.getElementsByTagName("tns:message").getLength() >0 ?
                   doc.getElementsByTagName("tns:message").getLength()-1 : doc.getElementsByTagName("tns:message").getLength();
                   String message=doc.getElementsByTagName("tns:message").item(lastMsg).getTextContent();
                   response.put("status", "Error");
                   message=message.replaceAll("</?[A-Za-z]+>", "");
                   message=message.replaceAll("JBO-OKC:::OKC_INVALID_CON_UNIQUE_KEY: ", "");
                   message=message.replaceAll("JBO-OKC:::OKC_OWNER_ROLE_SRC_MISMATCH: ", "");
                   message=message.replaceAll("JBO-OKC:::OKC_AUTH_CLN_INV_STDT_HDRDT: ", "");
                   if(message.contains("combination") && message.contains("contract") && message.contains("number")){
                       message="This contract found please change number of this contract.";
                   }else if(message.contains("LOV_PartyName_WS") && message.contains("matching")){
                       message="Customer name is wrong please check and edit it.";
                   }else if(message.contains("LOV_BillToAcctNumber_WS") && message.contains("matching")){
                       message="Bill to account number is wrong please check and edit it.";
                   }else if(message.contains("LOV_ShipToAcctNumber") && message.contains("matching")){
                       message="Ship to account number is wrong please check and edit it.";
                   }else if(message.contains("LOV_BillToLocation") && message.contains("matching")){
                       message="Bill to location is wrong please check and edit it.";
                   }else if(message.contains("ShipToLocation") && message.contains("matching")){
                       message="Ship to location is wrong please check and edit it.";
                   }
                   response.put("message", message);
               } else {
                   StringBuilder contractLine = new StringBuilder();
                   //Building contract line
                    NodeList nList  = doc.getElementsByTagName("ns1:ContractLine");
                   for (int i = 0; i < nList.getLength(); ++i) {
                       NamedNodeMap map = nList.item(i).getAttributes();
                       for (int j = 0; j < map.getLength(); j++) {
                           Node eNode = nList.item(i);
                           Element name = (Element)eNode;
                           contractLine.append("<con:ContractLine>\n" +
                                   "<con:ExternalSource>FUSION</con:ExternalSource>\n" +
                                   "<con:ExternalKey>" + name.getElementsByTagName("ns1:ExternalKey").item(0).getTextContent() + "</con:ExternalKey>\n" +
                                   "<con:BillPlan>Amount Based Invoice</con:BillPlan>\n" +
                                   "<con:RevenuePlan>POC Revenue</con:RevenuePlan>\n" +
                                   "</con:ContractLine>\n");
                       }
                   }
                   
                   String updateContractPayload = 
                       "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\" xmlns:con=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/\" xmlns:head=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/header/flex/header/\" xmlns:par=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/partyContact/\" xmlns:par1=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/party/\" xmlns:lin=\"http://xmlns.oracle.com/apps/flex/contracts/coreAuthoring/lines/\" xmlns:con1=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractPublicService/\" xmlns:con2=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractBillingService/\" xmlns:bil=\"http://xmlns.oracle.com/apps/projects/billing/contracts/flex/BilllPlanDff/\">\n" +
                       "   <soapenv:Header/>\n" +
                       "   <soapenv:Body>\n" +
                       "      <typ:updateContract>\n" +
                       "         <typ:contractHeader>\n" +
                       "            <con:Id>"+doc.getElementsByTagName("ns1:Id").item(0).getTextContent()+"</con:Id>\n" +
                       "            <con:MajorVersion>"+doc.getElementsByTagName("ns1:MajorVersion").item(0).getTextContent()+"</con:MajorVersion>\n" +
                       "            <con:ExternalSource>FUSION</con:ExternalSource>\n" +
                       "            <con:ExternalKey>"+doc.getElementsByTagName("ns1:Id").item(0).getTextContent()+"</con:ExternalKey>\n" 
                                       + contractLine +
                       "         </typ:contractHeader>\n" +
                       "      </typ:updateContract>\n" +
                       "   </soapenv:Body>\n" +
                       "</soapenv:Envelope>";
                   // Update contract SOAP web service    
                  response= updatedContract(updateContractPayload);
               }
           } catch (Exception e) {
               e.printStackTrace();
               System.err.println("Update Contract Error...");
           }
           return response;
       }
    
    public JSONObject updatedContract(String soapRequest) {
            
            Document doc = null;
            JSONObject response=new JSONObject();
            try {
                doc = restHelper.httpPost(restHelper.getInstanceUrl()+restHelper.getContractURLSoap(), soapRequest);
                
                if (doc.getElementsByTagName("faultstring").getLength() > 0) {
                    int lastMsg=doc.getElementsByTagName("tns:message").getLength()-1;
                    String message=doc.getElementsByTagName("tns:message").item(lastMsg).getTextContent();
                    response.put("status", "Error");
                    message=message.replaceAll("</?[A-Za-z]+>", "");
                   message=message.replaceAll("JBO-OKC:::OKC_INVALID_CON_UNIQUE_KEY: OKC-195788", "");
                   message=message.replaceAll("JBO-OKC:::OKC_OWNER_ROLE_SRC_MISMATCH: OKC-196156", "");
                    response.put("message", message);
                } else {
                    response.put("status", "Successs");
                    response.put("message", "Success in creating contract");  
                }
                return response;
            } catch (Exception e) {
                System.err.println("Error while inserting Contract");
                e.printStackTrace();
            }
            return response;
        }

    public static void main(String[] args) {

    }

}
