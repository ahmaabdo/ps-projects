package biPReports;


import com.appspro.db.CommonConfigReader;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.ws.rs.core.MediaType;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;

import org.json.JSONObject;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;


public class RestHelper {
    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    private String InstanceUrl =  CommonConfigReader.getValue("InstanceUrl"); 
    private String biReportUrl = CommonConfigReader.getValue("biReportUrl"); 
    private String SecurityService = CommonConfigReader.getValue("SecurityService");
    private String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";
    private String allEmployeesUrl = "/hcmRestApi/resources/11.13.18.05/emps?limit=200&onlyData=true";
    private String instanceName = CommonConfigReader.getValue("instanceName"); 
    private String supplierUrl = "/fscmRestApi/resources/11.13.18.05/suppliers?limit=500";
    private String purchaseOrderUrl = "/fscmRestApi/resources/11.13.18.05/purchaseOrders";
    private String businessUnitUrl = "/fscmRestApi/resources/11.13.18.05/finBusinessUnitsLOV?onlyData=true";
    private String createReceipt = "/fscmService/ProcessorServiceV2";
    private String createReceiptRest = "/fscmRestApi/resources/11.13.18.05/receivingReceiptRequests";
    private final String financeUrl="/fscmService/FinancialUtilService";

    public static String USER_NAME = CommonConfigReader.getValue("USER_NAME"); 
    public static String PASSWORD = CommonConfigReader.getValue("PASSWORD");
    public static String Schema_Name = CommonConfigReader.getValue("Schema_Name");
    public String protocol = "https";

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public static Document httpPost(String destUrl, String postData) throws Exception {
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        
        java.net.URL url = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https = (HttpsURLConnection)url.openConnection();
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + http.getResponseCode() + "; connection response: " +  http.getResponseMessage());
        
        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response = response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") + 15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);
            System.out.println(response);

            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();

            return doc;
        } else {
            System.out.println("Failed");
            InputStream in = http.getErrorStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response = response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") + 15);
            
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);

            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();
            
            System.out.println(response);
            return doc;
        }

    }
    
    public static JSONObject httpPostRest(String restApi,String body) throws MalformedURLException,
                                                             IOException {
        JSONObject res = new JSONObject();
        System.out.println(restApi);
        
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        URL url = new URL(null,restApi,new sun.net.www.protocol.https.Handler());

        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String soapAction = null;
        connection.setRequestMethod("POST");
        connection.setReadTimeout(6000000);
        connection.setRequestProperty("content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("SoapAction", soapAction);
        connection.setRequestProperty("Authorization", "Basic " + getAuth());
        
        if (body != null && body.length() > 0) {
            connection.setDoOutput(true);
            OutputStream os = connection.getOutputStream();
            byte[] input = body.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        
        int code = connection.getResponseCode();
        InputStream is;
        if (code >= 400) {
            is = connection.getErrorStream();
            res.put("status", "error");
        } else {
            res.put("status", "done");
            is = connection.getInputStream();
        }
        
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        
        try {
            res.put("data", new JSONObject(response.toString()));
         }catch (Exception e) {
            res.put("data", response.toString());
        }
        
        System.out.print("code:" + code);
        if (code >= 400)
            System.out.println(",Error:" + res.get("data").toString());
        else
            System.out.println();
       
         return res;
    }
    
    public static JSONObject httpGet (String restUrl) throws MalformedURLException, IOException {
       
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        URL url = new URL(null,restUrl,new sun.net.www.protocol.https.Handler());
        
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String soapAction = null ;
        connection.setRequestMethod("GET");
        connection.setReadTimeout(6000000);
        connection.setRequestProperty("content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("SoapAction", soapAction);
        connection.setRequestProperty("Authorization", "Basic " + getAuth());
       
        BufferedReader in = new BufferedReader( new InputStreamReader (connection.getInputStream(),"utf-8"));
        String input = null;
        StringBuffer response = new StringBuffer();
        
        while((input = in.readLine()) != null) {
            response.append(input);
        }
        
        JSONObject obj = new JSONObject(response.toString());
       
        return obj;
    }
    
    

    public static String getAuth() {
        byte[] message = (USER_NAME + ":" + PASSWORD).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }
    
    public static String currentOracleDate(){
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");  
        SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm:ss"); 
        Date date = new Date(System.currentTimeMillis() - 10800 * 1000);  
        String currentSystemDate = formatter.format(date)+"T"+formatterTime.format(date);
        
        return currentSystemDate;
    }
    public static String decodeBase64(String encoded) {

          byte[] decoded = Base64.decodeBase64(encoded);
          String decodedString = new String(decoded);
          return decodedString;
      }

    public void setSupplierUrl(String supplierUrl) {
        this.supplierUrl = supplierUrl;
    }

    public String getSupplierUrl() {
        return supplierUrl;
    }

    public void setPurchaseOrderUrl(String purchaseOrderUrl) {
        this.purchaseOrderUrl = purchaseOrderUrl;
    }

    public String getPurchaseOrderUrl() {
        return purchaseOrderUrl;
    }
    
    public static String getSchema_Name() {
        return Schema_Name;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public String getEmployeeUrl() {
        return InstanceUrl + employeeServiceUrl;
    }

    public String getEmployeeRestEndPointUrl() {
        return employeeServiceUrl;
    }

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }
    
    public void setSecurityService(String SecurityService) {
        this.SecurityService = SecurityService;
    }

    public String getSecurityService() {
        return SecurityService;
    }

    public void setBusinessUnitUrl(String businessUnitUrl) {
        this.businessUnitUrl = businessUnitUrl;
    }

    public String getBusinessUnitUrl() {
        return businessUnitUrl;
    }

    public void setAllEmployeesUrl(String allEmployeesUrl) {
        this.allEmployeesUrl = allEmployeesUrl;
    }

    public String getAllEmployeesUrl() {
        return allEmployeesUrl;
    }

    public void setCreateReceipt(String createReceipt) {
        this.createReceipt = createReceipt;
    }

    public String getCreateReceipt() {
        return createReceipt;
    }

    public String getCreateReceiptRest() {
        return createReceiptRest;
    }

    public String getFinanceUrl() {
        return financeUrl;
    }
}
