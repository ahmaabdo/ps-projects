package com.appspro.tbcReceipt.rest;


import biPReports.BIPReports;

import biPReports.BIReportModel;

import com.appspro.filter.JWTTokenNeeded;
import com.appspro.tbcReceipt.bean.SummaryReportBean;

import com.appspro.tbcReceipt.dao.SummaryReportDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.XML;


@Path("/report")
public class BIReportService {

    SummaryReportDAO summaryDao = new SummaryReportDAO();

    @POST
    @JWTTokenNeeded
    @Path("/commonbireport")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getBiReport(String incomingData, @Context HttpServletRequest request) {
        
        JSONObject jObject = null; // json
        JSONArray reportListJSON = null;
        String fresponse = null;
        String reportName = null;
      
            if (incomingData != null && !incomingData.isEmpty()) {

                Object jsonTokner = new JSONTokener(incomingData).nextValue();
                if (jsonTokner instanceof JSONObject) {
                    jObject = new JSONObject(incomingData);
                    reportName = jObject.getString("reportName");
                } else {
                    reportListJSON = new JSONArray(incomingData);
                }


                BIPReports biPReports = new BIPReports();
                biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
                biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

                String paramName;
                SummaryReportDAO summary = new SummaryReportDAO();
                SummaryReportBean reportBeans = new SummaryReportBean();

                ArrayList arr = summary.getReportParamSummary(reportName);
                if (!arr.isEmpty()) {
                    reportBeans = (SummaryReportBean)arr.get(0);
                }

                if (reportBeans.getParameter1() != null) {
                    paramName = reportBeans.getParameter1();
                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getParameter2() != null) {
                    paramName = reportBeans.getParameter2();

                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getParameter3() != null) {
                    paramName = reportBeans.getParameter3();
                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getParameter4() != null) {
                    paramName = reportBeans.getParameter4();
                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getParameter5() != null) {
                    paramName = reportBeans.getParameter5();
                    if (jObject.has(paramName)) {
                        biPReports.getParamMap().put(paramName,
                                                     jObject.get(paramName).toString());
                    } else {
                        biPReports.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getReportTypeVal() != null) {
                    if (reportBeans.getReportTypeVal().equals("xml")) {

                        biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                      //  biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                        System.out.println(reportBeans.getReport_Name());
                        System.out.println("Loggggggggggggggggggggggg");
                       // System.out.println(biPReports.executeReports());
                        fresponse = biPReports.executeReports();
                        JSONObject xmlJSONObj = XML.toJSONObject(fresponse.toString());

                        if (!xmlJSONObj.isNull("DATA_DS") && !xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1"))
                        {
                            if (xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) 
                            {
                                return Response.ok("[]", MediaType.TEXT_PLAIN).build();
                                
                            } else 
                            {
                                return Response.ok(xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("G_2","Items"),MediaType.APPLICATION_JSON).build();
                            }
                        } else 
                        {
                            return Response.ok("[]",MediaType.TEXT_PLAIN).build();
                        }
                    } else if (reportBeans.getReportTypeVal().equals("pdf")) {

                        biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                        byte[] reportBytes = biPReports.executeReportsPDF();

                        fresponse =  javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                        return Response.ok("{" + "\"REPORT\":" + "\"" +
                                           fresponse + "\"" + "}",
                                           MediaType.APPLICATION_JSON).build();
                    }
                }
            }
        
        else {
            List<BIReportModel> threadReport = new ArrayList<BIReportModel>();

            JSONArray reportResponeArray = new JSONArray();
            SummaryReportBean reportBeans = new SummaryReportBean();

            SummaryReportDAO service = new SummaryReportDAO();
            Map<String, SummaryReportBean> mapReportParam = new HashMap<String, SummaryReportBean>();
            mapReportParam = service.getAllSummaryMapped();
            
            for (int in = 0; in < reportListJSON.length(); in++) {
                BIPReports biPReportsThread = new BIPReports();
                biPReportsThread.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
                biPReportsThread.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());

                JSONObject reportDetails = reportListJSON.getJSONObject(in).getJSONObject("nameSeg");
                reportName = reportDetails.getString("reportName");
                String paramName;

                reportBeans = mapReportParam.get(reportName);
                if (reportBeans == null) {
                    continue;
                }

                if (reportBeans.getParameter1() != null) {
                    paramName = reportBeans.getParameter1();
                    if (reportDetails.has(paramName)) {
                        biPReportsThread.getParamMap().put(paramName,
                                                           reportDetails.get(paramName).toString());
                    } else {
                        biPReportsThread.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getParameter2() != null) {
                    paramName = reportBeans.getParameter2();

                    if (reportDetails.has(paramName)) {
                        biPReportsThread.getParamMap().put(paramName,
                                                           reportDetails.get(paramName).toString());
                    } else {
                        biPReportsThread.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getParameter3() != null) {
                    paramName = reportBeans.getParameter3();
                    if (reportDetails.has(paramName)) {
                        biPReportsThread.getParamMap().put(paramName,
                                                           reportDetails.get(paramName).toString());
                    } else {
                        biPReportsThread.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getParameter4() != null) {
                    paramName = reportBeans.getParameter4();
                    if (reportDetails.has(paramName)) {
                        biPReportsThread.getParamMap().put(paramName,
                                                           reportDetails.get(paramName).toString());
                    } else {
                        biPReportsThread.getParamMap().put(paramName, "");
                    }
                }
                
                if (reportBeans.getParameter5() != null) {
                    paramName = reportBeans.getParameter5();
                    if (reportDetails.has(paramName)) {
                        biPReportsThread.getParamMap().put(paramName,
                                                           reportDetails.get(paramName).toString());
                    } else {
                        biPReportsThread.getParamMap().put(paramName, "");
                    }
                }

                if (reportBeans.getReportTypeVal() != null) {
                    if (reportBeans.getReportTypeVal().equals("xml")) {

                        biPReportsThread.setReportAbsolutePath(reportBeans.getReport_Name());

                        biPReportsThread.setSegmentName(reportDetails.getString("segmentName"));
                        threadReport.add(biPReportsThread);

                    } 
                    else if (reportBeans.getReportTypeVal().equals("pdf")) 
                    {
                        BIPReports biPReports = new BIPReports();
                        biPReports.setReportAbsolutePath(reportBeans.getReport_Name());
                        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
                        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
                        byte[] reportBytes = biPReports.executeReportsPDF();

                        fresponse =  javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));

                        return Response.ok("{" + "\"REPORT\":" + "\"" +
                                           fresponse + "\"" + "}",
                                           MediaType.APPLICATION_JSON).build();
                    }
                }
            }

            try {
                BIReportModel.THREAD_REPORT = threadReport;
                List<String> responseList =  new BIReportModel().runMultipleReports();

                for (int t = 0; t < responseList.size(); t++) {
                    String[] responeArray = responseList.get(t).split("SPLIT");
                    fresponse = responeArray[1];
                    JSONObject xmlJSONObj =  XML.toJSONObject(fresponse.toString());

                    if (xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1") || xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) 
                    {
                        reportResponeArray.put(new JSONObject().put("[]", new JSONObject()));

                    } else {
                        JSONObject jsonRespone = new JSONObject(xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                                                "value").replaceAll("LABEL",
                                                                                                                                    "label"));
                        reportResponeArray.put(new JSONObject().put(responeArray[0],jsonRespone));
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();

            } catch (ExecutionException e) {
                e.printStackTrace();
            } finally {
                BIReportModel.THREAD_REPORT = new ArrayList<BIReportModel>();
                BIReportModel.THREAD_REPORT_COUNTER = -1;
                BIReportModel.THREAD_REPORT_RESPONSE = new ArrayList<String>();
            }
            return Response.ok(reportResponeArray.toString(), MediaType.APPLICATION_JSON).build();
        }
        return Response.ok("".toString(),MediaType.APPLICATION_JSON).build();
    }
  
    //this function to get data from report with get method for specfic report name
    
    @GET
    @Path("{reportName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response callReportService (@PathParam("reportName") String reportName) {
            
        return Response.ok(summaryDao.callingReport(reportName),MediaType.TEXT_PLAIN).build();

    }
    
}
