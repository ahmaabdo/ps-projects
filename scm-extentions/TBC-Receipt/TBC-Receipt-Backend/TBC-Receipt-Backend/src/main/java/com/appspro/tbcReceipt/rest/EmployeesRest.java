package com.appspro.tbcReceipt.rest;

import com.appspro.tbcReceipt.bean.EmployeeBean;
import com.appspro.tbcReceipt.dao.EmployeesDAO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/empolyees")

public class EmployeesRest {

    @Path("/AllEmployees")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEmployees()  {

        EmployeesDAO emp = new EmployeesDAO();
        JSONObject resObj = new JSONObject();
        JSONArray allEmployees;
        
        try {
            allEmployees = emp.getAllEmployees();
            resObj.put("Status", " OK");
            resObj.put("Data", allEmployees);
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
            resObj.put("Status", " ERROR");
            resObj.put("Data", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            resObj.put("Status", " ERROR");
            resObj.put("Data", e.getMessage());
        }
        return resObj.toString();
    }

    @GET
    @Path("{username}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getDataFromLookup(@PathParam("username")  String userName) {

        EmployeesDAO empDao = new EmployeesDAO();
        JSONObject resObj = new JSONObject();
        JSONObject returnData = new JSONObject();

        if (userName != null && !userName.isEmpty()) {
            try {
                returnData = empDao.getEmployeeDetails(userName);
                if(returnData.length() == 0){
                    resObj.put("Data", "Invalid UserName");
                    resObj.put("Status", "ERROR");
                }
                else{
                    resObj.put("Data", returnData);
                    resObj.put("Status", "OK");
                }
            } catch (Exception e) {
                resObj.put("Status", "ERROR");
                resObj.put("Data", e.getMessage());
            }            
        } 
        return resObj.toString();
    }
}
