/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.db;

/**
 *
 * @author user
 */
public class SessionLocaleBean {

    private String userSessionLocale;

    public SessionLocaleBean() {
    }

    public void setUserSessionLocale(String userSessionLocale) {
        this.userSessionLocale = userSessionLocale;
    }

    public String getUserSessionLocale() {
        return userSessionLocale;
    }
}
