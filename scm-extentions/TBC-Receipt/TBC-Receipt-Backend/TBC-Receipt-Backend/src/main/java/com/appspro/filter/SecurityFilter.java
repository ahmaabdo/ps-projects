//package com.appspro.filter;
//
//import java.io.IOException;
//import java.util.List;
//import java.util.StringTokenizer;
//import javax.ws.rs.container.ContainerRequestContext;
//import javax.ws.rs.container.ContainerRequestFilter;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.ext.Provider;
//
//import org.glassfish.jersey.internal.util.Base64;
//
//
//    
//    @Provider
//    public class SecurityFilter implements ContainerRequestFilter {
//        private static final String AUTHORIZATION_HEADER_KEY = "Authorization";
//        private static final String AUTHORIZATION_HEADER_PREFIX = "Basic ";
//        private static final String SUPPLIER_URL_PREFIX = "suppliers";
//        private static final String CONTRACT_URL_PREFIX = "suppliers";
//
//        @Override
//        
//        public void filter(ContainerRequestContext requestContext)  {
//            
//            if (requestContext.getUriInfo().getPath().contains(SUPPLIER_URL_PREFIX) && requestContext.getUriInfo().getPath().contains(CONTRACT_URL_PREFIX) ) {
//
//                List<String> authHeader = requestContext.getHeaders().get(AUTHORIZATION_HEADER_KEY);
//                System.out.println("auth"+authHeader);
//                if (authHeader != null && authHeader.size() > 0) {
//                    String authToken = authHeader.get(0);
//                    System.out.println("auth"+authToken);
//                    authToken = authToken.replaceFirst(AUTHORIZATION_HEADER_PREFIX, "");
//                    String decodeString = Base64.decodeAsString(authToken);
//                    StringTokenizer tokenizer = new StringTokenizer(decodeString, ":");
//                    String username = tokenizer.nextToken();
//                    String password = tokenizer.nextToken();
//                    if ("PaaSAdmin".equals(username) && "Appspro@123".equals(password)) {
//                        return  ;
//                    }
//                }
//                Response authorizedStatus = Response
//                        .status(Response.Status.UNAUTHORIZED).entity("{\n"
//                        + "\"errorCode\":\"401\",\n"
//                        + "\"errorMessage\":\"Unauthorized\"\n"
//                        + "}").type(MediaType.APPLICATION_JSON).build();
//                requestContext.abortWith(authorizedStatus);
//            }
//        }
//    }
//
