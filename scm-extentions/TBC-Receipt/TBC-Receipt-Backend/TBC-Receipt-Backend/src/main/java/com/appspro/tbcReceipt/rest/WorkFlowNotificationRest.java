package com.appspro.tbcReceipt.rest;

import com.appspro.tbcReceipt.bean.WorkFlowNotificationBean;
import com.appspro.tbcReceipt.dao.WorkFlowNotificationDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("notification")

public class WorkFlowNotificationRest {
    WorkFlowNotificationDAO serivces = new WorkFlowNotificationDAO();
    WorkFlowNotificationBean bean = new WorkFlowNotificationBean();
    JSONObject resposne = new JSONObject();
    JSONArray arr = new JSONArray();
    String status;

   
    @POST
    @Path("addReceiptRequest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertRceeiptReq(String body) {

        JSONObject obj = new JSONObject(body);
        try {
            resposne.put("Status", "OK");
            resposne.put("Data", serivces.insertRceeiptReq(obj));
        } catch (SQLException e) {
            e.printStackTrace();
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
    @GET
    @Path("getNotification/{personId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getNotificationById (@PathParam ("personId") String personId){

        try {
            resposne.put("Status", "OK");
            resposne.put("Data", serivces.getNotiyById(personId));
        } catch (SQLException e) {
            e.printStackTrace();
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        
        return resposne.toString();        
    }
    
    @POST
    @Path("updateNotification")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateNotificationStatus (String body ){

        JSONObject objBody = new JSONObject(body);
        try {
            resposne.put("Status", "OK");
            resposne.put("Data", serivces.updateNotifcation(objBody));
        } catch (SQLException e) {
            e.printStackTrace();
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        
        return resposne.toString();        
    }
    
    
    
}
