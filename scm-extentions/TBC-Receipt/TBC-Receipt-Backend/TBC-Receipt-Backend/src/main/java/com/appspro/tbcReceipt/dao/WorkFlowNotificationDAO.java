package com.appspro.tbcReceipt.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.tbcReceipt.bean.WorkFlowNotificationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class WorkFlowNotificationDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    ResultSet rs2;
    RestHelper rh = new RestHelper();


    public JSONObject insertRceeiptReq(JSONObject payload) throws SQLException {
        String refNo = "";
        JSONObject refObj = new JSONObject();
        JSONArray payloadArr = payload.getJSONArray("REQUESTES");

        try {

            connection = AppsproConnection.getConnection();

            String query = "SELECT REFERENCE_NO.NEXTVAL AS NEXT_ID FROM dual";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                refNo = rs.getString("NEXT_ID");
            }

            String query3 = "INSERT INTO XXX_RECEIPT_APPROVAL(RECEIPT_CREATION_DATE,PO_NUMBER,REFERENCE_NUM,ATTACHMENT_NUM,REQUESTER_NAME,REQUEST_TYPE,APPROVAL_STATUS,MANAGER_ID) VALUES (?,?,?,?,?,?,?,?)";
            ps = connection.prepareStatement(query3);
            ps.setString(1, payload.get("RECEIPT_CREATION_DATE").toString());
            ps.setString(2, payload.get("PO_NUMBER").toString());
            ps.setString(3, refNo);
            ps.setString(4, payload.get("ATTACHMENT_NUM").toString());
            ps.setString(5, payload.get("REQUESTER_NAME").toString());
            ps.setString(6, payload.get("REQUEST_TYPE").toString());
            ps.setString(7, "Pending Approval");
            ps.setString(8, payload.get("MANAGER_ID").toString());

            ps.executeUpdate();

            for (int i = 0; i < payloadArr.length(); i++) {
                JSONObject obj = payloadArr.getJSONObject(i);
                int request_id = 0;
                String query2 = "Select NVL(MAX(ID),0)+1 as request_id from XXX_RECEIPT_REQUEST";
                ps = connection.prepareStatement(query2);
                rs = ps.executeQuery();
                while (rs.next()) {
                    request_id = rs.getInt("request_id");
                }

                String query4 =
                    "INSERT INTO XXX_RECEIPT_REQUEST(ID,REQUISITION_BU,REQUISITION,ITEM_DESCRIPTION,SUPPLIER_NAME , NEED_BY_DATE,QUANTITY_DELIVERED,UOM,CURRENCY,QUANTITY,AVAILABLE,PURCHASE_ORDER,PURCHASE_ORDER_LINE," +
                    "REFERENCE_NUM,ORGANIZATION_CODE,PURCHASE_BASIS,LEGAL_ENTITY_NAME,VENDOR_NUMBER,VENDOR_SITE_CODE,AMOUNT,REQUESTER_ID,REQUESTER_NAME,MANAGER_ID,RECEIPT_STATUS) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query4);

                ps.setInt(1, request_id);
                ps.setString(2, obj.get("BU_NAME").toString());
                ps.setString(3, obj.get("REQUISITION_NUMBER").toString());
                ps.setString(4, obj.get("ITEM_DESCRIPTION").toString());
                ps.setString(5, obj.get("VENDOR_NAME").toString());
                ps.setString(6, obj.get("NEED_BY_DATE").toString());
                ps.setString(7, obj.get("QUANTITY_DELIVERED").toString());
                ps.setString(8, obj.has("UNIT_OF_MEASURE") ? obj.get("UNIT_OF_MEASURE").toString() : "");
                ps.setString(9, obj.get("CURRENCY_CODE").toString());
                ps.setString(10, obj.has("quantity") ? obj.get("quantity").toString() : "");
                ps.setString(11, obj.get("AVILABLE").toString());
                ps.setString(12, obj.get("SEGMENT1").toString());
                ps.setString(13, obj.get("LINE_NUMBER").toString());
                ps.setString(14, refNo);
                ps.setString(15, obj.get("ORGANIZATION_CODE").toString());
                ps.setString(16, obj.get("PURCHASE_BASIS").toString());
                ps.setString(17, obj.get("LEGAL_ENTITY_NAME").toString());
                ps.setString(18, obj.get("VENDOR_NUMBER").toString());
                ps.setString(19, obj.get("VENDOR_SITE_CODE").toString());
                ps.setString(20, obj.has("quantity") ? obj.get("quantity").toString() : "");
                ps.setString(21, obj.get("PO_HEADER_ID").toString());
                ps.setString(22, obj.get("REQUESTER_NAME").toString());
                ps.setString(23, obj.get("MANAGER_ID").toString());
                ps.setString(24, "Pending Approval");

                ps.executeUpdate();
            }
            refObj.put("referneceNumber", refNo);

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return refObj;
    }

    public JSONArray getNotiyById(String personId) throws SQLException {
        JSONArray resArr = new JSONArray();
        String referenceNum = "";

        try {
            connection = AppsproConnection.getConnection();

            String query = "SELECT * FROM XXX_RECEIPT_APPROVAL WHERE MANAGER_ID   = ? AND APPROVAL_STATUS = 'Pending Approval'";
            ps = connection.prepareStatement(query);
            ps.setString(1, personId);
            rs2 = ps.executeQuery();
            while (rs2.next()) {
                JSONObject bodyObj = new JSONObject();
                JSONArray arr = new JSONArray();

                referenceNum = rs2.getString("REFERENCE_NUM");
                bodyObj.put("PO_NUMBER", rs2.getString("PO_NUMBER"));
                bodyObj.put("REQUESTER_NAME", rs2.getString("REQUESTER_NAME"));
                bodyObj.put("REQUEST_TYPE", rs2.getString("REQUEST_TYPE"));
                bodyObj.put("RECEIPT_CREATION_DATE", rs2.getString("RECEIPT_CREATION_DATE"));
                bodyObj.put("REFERENCE_NUM", rs2.getString("REFERENCE_NUM"));
                bodyObj.put("APPROVAL_STATUS", rs2.getString("APPROVAL_STATUS"));

                String query2 = "SELECT * FROM XXX_RECEIPT_REQUEST WHERE MANAGER_ID = ? AND REFERENCE_NUM = ?";
                ps = connection.prepareStatement(query2);
                ps.setString(1, personId);
                ps.setString(2, rs2.getString("REFERENCE_NUM"));

                rs = ps.executeQuery();

                while (rs.next()) {

                    JSONObject objLine = new JSONObject();
                    objLine.put("BU_NAME", rs.getString("REQUISITION_BU"));
                    objLine.put("REQUISITION_NUMBER", rs.getString("REQUISITION"));
                    objLine.put("ITEM_DESCRIPTION", rs.getString("ITEM_DESCRIPTION"));
                    objLine.put("VENDOR_NAME", rs.getString("SUPPLIER_NAME"));
                    objLine.put("NEED_BY_DATE", rs.getString("NEED_BY_DATE"));
                    objLine.put("QUANTITY_DELIVERED", rs.getString("QUANTITY_DELIVERED"));
                    objLine.put("UNIT_OF_MEASURE", rs.getString("UOM"));
                    objLine.put("CURRENCY_CODE", rs.getString("CURRENCY"));
                    //            objLine.put("QUANTITY_ORDERED",rs.getString("QUANTITY_ORDERED"));
                    objLine.put("AVILABLE", rs.getString("AVAILABLE"));
                    objLine.put("SEGMENT1", rs.getString("PURCHASE_ORDER"));
                    objLine.put("LINE_NUMBER", rs.getString("PURCHASE_ORDER_LINE"));
                    objLine.put("REFERENCE_NUM", rs.getString("REFERENCE_NUM"));
                    objLine.put("ORGANIZATION_CODE", rs.getString("ORGANIZATION_CODE"));
                    objLine.put("PURCHASE_BASIS", rs.getString("PURCHASE_BASIS"));
                    objLine.put("LEGAL_ENTITY_NAME", rs.getString("LEGAL_ENTITY_NAME"));
                    objLine.put("VENDOR_NUMBER", rs.getString("VENDOR_NUMBER"));
                    objLine.put("VENDOR_SITE_CODE", rs.getString("VENDOR_SITE_CODE"));
                    objLine.put("QUANTITY", rs.getString("QUANTITY"));
                    //            objLine.put("PO_HEADER_ID",rs.getString("PO_HEADER_ID"));
                    objLine.put("REQUESTER_NAME", rs.getString("REQUESTER_NAME"));
                    objLine.put("MANAGER_ID", rs.getString("MANAGER_ID"));
                    objLine.put("RECEIPT_STATUS", rs.getString("RECEIPT_STATUS"));

                    arr.put(objLine);
                    bodyObj.put("lines", arr);
                }
                resArr.put(bodyObj);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(rs2);
        }
        return resArr;
    }

    public JSONArray updateNotifcation(JSONObject body) throws SQLException {
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();
        String query =  "UPDATE  XXX_RECEIPT_APPROVAL SET APPROVAL_STATUS = ? WHERE REFERENCE_NUM = ? AND PO_NUMBER = ?";

        try {
            connection = AppsproConnection.getConnection();

            ps = connection.prepareStatement(query);
            ps.setString(1, body.get("action").toString());
            ps.setString(2, body.get("refNum").toString());
            ps.setString(3, body.get("poNumber").toString());
            ps.executeUpdate();

            if (body.get("action").toString().equalsIgnoreCase("Approved")) {
                String query2 =  "SELECT * FROM   XXX_RECEIPT_ATTACHMENTS WHERE REFERENCE_NUM = ? AND PO_NUMBER = ? ";
                ps = connection.prepareStatement(query2);
                ps.setString(1, body.get("refNum").toString());
                ps.setString(2, body.get("poNumber").toString());
                rs = ps.executeQuery();

                while (rs.next()) {
                    JSONObject attach = new JSONObject();
                    attach.put("Title", rs.getString("ATTACH_TITLE"));
                    attach.put("Url", rs.getString("ATTACH_URL"));
                    attach.put("POLine", rs.getString("PO_LINE")); 
                    attach.put("CategoryName", "MISC");
                    attach.put("Description", "test file upload");
                    attach.put("DatatypeCode", "WEB_PAGE");
                    arr.put(attach);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }
}



//        for ( int i = 0 ; i < arr.length() ; i++){
//            String query2 = "UPDATE  XXX_RECEIPT_REQUEST SET RECEIPT_STATUS = ? WHERE REFERENCE_NUM = ? AND SEGMENT1 = ? AND LINE_NUMBER = ?";
//            
//            ps = connection.prepareStatement(query2);
//            ps.setString(1, actionType);
//            ps.setString(2, refNum);
//            ps.setString(3, body.get("DocumentNumber").toString());
//            ps.setString(4, body.get("DocumentLineNumber").toString());
//            ps.executeUpdate();
//        }



//        if (actionType.equalsIgnoreCase("Approved")) {
//            JSONArray arr = body.getJSONArray("lines");
//
//            String query3 = "SELECT * FROM   XXX_RECEIPT_ATTACHMENTS WHERE REFERENCE_NUM = ? AND SEGMENT1 = ? ";
//            ps = connection.prepareStatement(query3);
//            ps.setString(1, refNum);
//            ps.setString(2, body.get("DocumentNumber").toString());
//            rs = ps.executeQuery();
//
//            while (rs.next()) {
//                JSONObject attach = new JSONObject();
//                attach.put("Title", rs.getString("UEL"));
//                attach.put("Url", rs.getString("TITLE"));
//                attach.put("CategoryName", "MISC");
//                attach.put("Description", "test file upload");
//                attach.put("DatatypeCode", "WEB_PAGE");
//
//                for (int i = 0; i < arr.length(); i++) {
//                    JSONObject lineOb = arr.getJSONObject(i);
//                    if (lineOb.get("DocumentLineNumber").toString().equalsIgnoreCase(rs.getString("TITLE"))) {
//                        JSONArray transAttachment = lineOb.getJSONArray("transactionAttachments");
//                        transAttachment.put(attach);
//                    }
//                }
//            }
//
//            System.out.println(body);
//        }
