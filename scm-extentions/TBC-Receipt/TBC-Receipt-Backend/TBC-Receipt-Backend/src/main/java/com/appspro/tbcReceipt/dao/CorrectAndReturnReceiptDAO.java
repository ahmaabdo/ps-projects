package com.appspro.tbcReceipt.dao;

import biPReports.RestHelper;

import com.appspro.tbcReceipt.bean.CreateReceiptBean;

import java.text.SimpleDateFormat;

import java.util.Date;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;

import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class CorrectAndReturnReceiptDAO extends RestHelper {
    
    public JSONObject correctAndReturn (CreateReceiptBean bean) throws Exception {
       
       String res = "";
       String payload = "";
       JSONObject responseObj = new JSONObject();
       if(bean.getTransactionType().equalsIgnoreCase("CORRECT")){
           payload = "" +
           "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/receiving/receiptsInterface/transactions/processorServiceV2/types/\" xmlns:proc=\"http://xmlns.oracle.com/apps/scm/receiving/receiptsInterface/transactions/processorServiceV2/\">\n" + 
           "   <soapenv:Body>\n" + 
           "      <typ:processor>\n" + 
           "         <typ:Receipt>\n" +                 
           "             <proc:StagedReceivingTransaction>\n" + 
           "                    <proc:TransactionType>"+bean.getTransactionType()+"</proc:TransactionType>\n" + 
           "                    <proc:ParentTransactionId>"+bean.getParentTransactionId()+"</proc:ParentTransactionId>\n" + 
           "                    <proc:ReceiptSourceCode>VENDOR</proc:ReceiptSourceCode>\n" + 
           "                    <proc:SourceDocumentCode>PO</proc:SourceDocumentCode>\n" + 
           "                    <proc:DocumentNumber>"+bean.getDocumentNumber()+"</proc:DocumentNumber>\n" + 
           "                    <proc:DocumentLineNumber>"+bean.getDocumentLineNumber()+"</proc:DocumentLineNumber>\n" + 
           "                    <proc:Amount currencyCode=\"SAR\">"+bean.getAmount()+"</proc:Amount>\n"+
           "                    <proc:Quantity>"+bean.getQuantity()+"</proc:Quantity>\n" + 
           "                    <proc:UnitOfMeasure>"+bean.getUom()+"</proc:UnitOfMeasure>\n" + 
           "                    <proc:TransactionDate>"+currentOracleDate()+"</proc:TransactionDate>\n" + 
           "                </proc:StagedReceivingTransaction>\n" + 
           "                <proc:StagedReceivingTransaction>\n" + 
            "                    <proc:TransactionType>"+bean.getTransactionType()+"</proc:TransactionType>\n" + 
            "                    <proc:ParentTransactionId>"+bean.getReceiveTransactionId()+"</proc:ParentTransactionId>\n" + 
            "                    <proc:ReceiptSourceCode>VENDOR</proc:ReceiptSourceCode>\n" + 
            "                    <proc:SourceDocumentCode>PO</proc:SourceDocumentCode>\n" + 
            "                    <proc:DocumentNumber>"+bean.getDocumentNumber()+"</proc:DocumentNumber>\n" + 
            "                    <proc:DocumentLineNumber>"+bean.getDocumentLineNumber()+"</proc:DocumentLineNumber>\n" + 
           "                     <proc:Amount currencyCode=\"SAR\">"+bean.getAmount()+"</proc:Amount>\n"+
            "                    <proc:Quantity>"+bean.getQuantity()+"</proc:Quantity>\n" + 
            "                    <proc:UnitOfMeasure>"+bean.getUom()+"</proc:UnitOfMeasure>\n" + 
            "                    <proc:TransactionDate>"+currentOracleDate()+"</proc:TransactionDate>\n" + 
            "                </proc:StagedReceivingTransaction>\n" + 
           "         </typ:Receipt>\n" + 
           "      </typ:processor>\n" + 
           "   </soapenv:Body>\n" + 
           "</soapenv:Envelope>";
       }else{
           payload = "" +
           "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/receiving/receiptsInterface/transactions/processorServiceV2/types/\" xmlns:proc=\"http://xmlns.oracle.com/apps/scm/receiving/receiptsInterface/transactions/processorServiceV2/\">\n" + 
           "   <soapenv:Body>\n" + 
           "      <typ:processor>\n" + 
           "         <typ:Receipt>\n" +                 
           "             <proc:StagedReceivingTransaction>\n" + 
           "                    <proc:TransactionType>"+bean.getTransactionType()+"</proc:TransactionType>\n" + 
           "                    <proc:ParentTransactionId>"+bean.getParentTransactionId()+"</proc:ParentTransactionId>\n" + 
           "                    <proc:ReceiptSourceCode>VENDOR</proc:ReceiptSourceCode>\n" + 
           "                    <proc:SourceDocumentCode>PO</proc:SourceDocumentCode>\n" + 
           "                    <proc:DocumentNumber>"+bean.getDocumentNumber()+"</proc:DocumentNumber>\n" + 
           "                    <proc:DocumentLineNumber>"+bean.getDocumentLineNumber()+"</proc:DocumentLineNumber>\n" + 
           "                    <proc:Quantity>"+bean.getQuantity()+"</proc:Quantity>\n" + 
           "                    <proc:UnitOfMeasure>"+bean.getUom()+"</proc:UnitOfMeasure>\n" + 
           "                    <proc:TransactionDate>"+currentOracleDate()+"</proc:TransactionDate>\n" + 
           "                </proc:StagedReceivingTransaction>\n" + 
           "         </typ:Receipt>\n" + 
           "      </typ:processor>\n" + 
           "   </soapenv:Body>\n" + 
           "</soapenv:Envelope>";
       }
       
       System.out.println(payload);
       
        Document doc = httpPost(getInstanceUrl() + getCreateReceipt(), payload);
        try {
            NodeList nl = doc.getElementsByTagName("ns1:Status");
            if(doc.getElementsByTagName("ns1:Status").item(0).getTextContent().equals("SUCCESS")){
                res = doc.getElementsByTagName("ns1:Status").item(0).getTextContent();
                responseObj.put("Status",res );
                responseObj.put("TransactionId",bean.getParentTransactionId());
            }
            else{
                res = doc.getElementsByTagName("ns1:ErrorMessage").item(0).getTextContent();
                responseObj.put("Error",res );
                responseObj.put("Status","ERROR" );
                responseObj.put("TransactionId",bean.getParentTransactionId());
            }
            System.out.println(res);
            
        } catch (NullPointerException e) {
            e.printStackTrace();
            res = e.getMessage();
        }
        return responseObj;                                            
    }
}
