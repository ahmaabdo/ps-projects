package com.appspro.tbcReceipt.rest;

import com.appspro.tbcReceipt.bean.CreateReceiptBean;
import com.appspro.tbcReceipt.dao.CorrectAndReturnReceiptDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/receipt")

public class CorrectAndReturnReceiptRest {


    @POST
    @Path("corretAndReturn")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String correctAndReturn(String body) {

        JSONArray receiptResArr = new JSONArray();
        JSONObject response = null;

        try {
            JSONArray bodyArr = new JSONArray(body);
            JSONObject returnReponse = new JSONObject();
            CorrectAndReturnReceiptDAO receipt = new CorrectAndReturnReceiptDAO();
            CreateReceiptBean bean = new CreateReceiptBean();

            for (int i = 0; i < bodyArr.length(); i++) {
                response = new JSONObject();
                JSONObject obj = bodyArr.getJSONObject(i);
                bean.setTransactionType(obj.get("transactionType").toString());
                bean.setParentTransactionId(obj.get("parentTransactionId").toString());
                bean.setReceiveTransactionId(obj.get("receiveTransactionId").toString());
                bean.setDocumentNumber(obj.get("DocumentNumber").toString());
                bean.setDocumentLineNumber(obj.get("DocumentLineNumber").toString());
                bean.setAmount(obj.get("Amount").toString());
                bean.setQuantity(obj.get("Quantity").toString());
                bean.setUom(obj.get("UnitOfMeasure").toString());

                returnReponse = receipt.correctAndReturn(bean);

                if (!returnReponse.get("Status").toString().equalsIgnoreCase("SUCCESS")) {
                    response.put("Status", "ERROR");
                    response.put("Data", returnReponse);
                }
                receiptResArr.put(returnReponse);

            }

        } catch (Exception e) {
            e.printStackTrace();
            response.put("Status", "ERROR");
            response.put("Data", e.getMessage());
        }

        return receiptResArr.toString();
    }
}
