package com.appspro.tbcReceipt.dao;

import biPReports.RestHelper;
import java.io.IOException;
import java.net.MalformedURLException;
import org.json.JSONObject;

public class BusinessUnitDAO extends RestHelper {
  
    public JSONObject businessUnit() throws MalformedURLException, IOException {
        
        JSONObject res = new JSONObject();
        res = httpGet(getInstanceUrl()+getBusinessUnitUrl());
        
        return res;
    }
}
