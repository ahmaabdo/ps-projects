
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset', 'ojs/ojvalidationgroup', 'ojs/ojdialog'
], function (oj, ko, $, app, services, commonhelper) {
    function notificationScreenContentViewModel() {
        var self = this;
        self.label = ko.observable();

        self.customsModel = {
            columnArray: ko.observableArray([]),
            detailsColumnArray: ko.observableArray([]),
            receiptTableArr: ko.observableArray([]),
            receiptTableDetailsArr: ko.observableArray([]),
            notificationArr: ko.observableArray([]),
            rowSelected: ko.observable(),
            selectedRowKey: ko.observable(),
            isDisabled: ko.observable(true),
        };

        self.dataprovider = ko.observable();
        self.dataprovider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.receiptTableArr)));

        self.detailsDataProvider = ko.observable();
        self.detailsDataProvider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.receiptTableDetailsArr)));

        self.connected = function () {
            self.getNotificationById();
        };

        self.getNotificationById = function () {
            app.loading(true);
            self.customsModel.receiptTableArr([]);
            services.getGeneric("notification/getNotification/" + app.personDetails()[0].personId).then(data => {
                if (!Array.isArray(data.Data)) 
                    resData = [data.Data];
                else {
                    resData = data.Data;
                }
                resData.forEach(el => {
                    self.customsModel.receiptTableArr().push(el)
                })
                self.dataprovider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.receiptTableArr)));
                app.loading(false);
            }, error => {
                app.loading(false);
            })
        };

        //--------------------------approve Action --------------------------

        self.approveBtnAction = function () {
            document.querySelector("#receiptLineDialog").close();
            app.loading(true);
            var payload = {
                "action": "Approved",
                "refNum": self.customsModel.rowSelected.lines[0].REFERENCE_NUM,
                "poNumber": self.customsModel.rowSelected.lines[0].SEGMENT1
            }

            services.postGeneric("notification/updateNotification", payload).then(data => {
                app.loading(false);
                self.createReceiptFun(data.Data);
                self.getNotificationById();
            }, error => {
                app.loading(false);
            });
        };

        //--------------Reject Action --------------------------
        self.rejectBtnAction = function () {
            document.querySelector("#receiptLineDialog").close();
            app.loading(true);
            var payload = {
                "action": "Rejected",
                "refNum": self.customsModel.rowSelected.lines[0].REFERENCE_NUM,
                "poNumber": self.customsModel.rowSelected.lines[0].SEGMENT1
            }

            services.postGeneric("notification/updateNotification", payload).then(data => {
                app.loading(false);
                self.getNotificationById();
            }, error => {
                app.loading(false);
            });
        };

        //-----------------Selection Action --------------------------
        self.createReceiptFun = function (data) {
            var receiptLines = [];
            self.customsModel.rowSelected.lines.forEach(e =>
                receiptLines.push(
                    {
                        "ReceiptSourceCode": "VENDOR",
                        "SourceDocumentCode": "PO",
                        "TransactionType": "RECEIVE",
                        "AutoTransactCode": "DELIVER",
                        "OrganizationCode": e.ORGANIZATION_CODE,
                        "ItemDescription": e.ITEM_DESCRIPTION.split('_')[0],
                        "DocumentNumber": e.SEGMENT1.toString(),
                        "DocumentLineNumber": e.LINE_NUMBER,
                        "Quantity": e.PURCHASE_BASIS == "GOODS" ? e.QUANTITY : "",
                        "UnitOfMeasure": e.PURCHASE_BASIS == "GOODS" ? e.UNIT_OF_MEASURE : "",
                        "Amount": e.PURCHASE_BASIS == "SERVICES" ? e.QUANTITY : "",
                        "CurrencyCode": e.PURCHASE_BASIS == "SERVICES" ? e.CURRENCY_CODE : "",
                        "SoldtoLegalEntity": e.LEGAL_ENTITY_NAME,
                        "transactionAttachments": data.length > 0 ? data.filter(el => el.POLine == e.LINE_NUMBER).map(e => ({
                            "Url": e.Url,
                            "CategoryName": e.CategoryName,
                            "Title": e.Title,
                            "Description": e.Description,
                            "DatatypeCode": e.DatatypeCode
                        })
                        ) : []
                    }
                )
            );
            var createpayload = [{
                "ReceiptSourceCode": "VENDOR",
                "TransactionDate": new Date().toISOString().split("T")[0],
                "OrganizationCode": self.customsModel.rowSelected.lines[0].ORGANIZATION_CODE,
                "VendorNumber": self.customsModel.rowSelected.lines[0].VENDOR_NUMBER,
                "BusinessUnit": self.customsModel.rowSelected.lines[0].BU_NAME,
                "EmployeeId": app.personDetails()[0].personId,
                "lines": receiptLines,
            }];

            var successCreateCbFn = function (data) {
                app.loading(false)
                if (data.Status == "OK") {
                    for (var i in data.Data) {
                        app.messagesDataProvider.push(app.createMessage('confirmation', `Reciept Created with Number ${data.Data[i].receiptNumber}`, '6000'));
                    }
                }

                if (data.Status == "ERROR") {
                    for (var i in data.Data) {
                        app.messagesDataProvider.push(app.createMessage('error', ` ${data.Data[i].errorMessage} `, '6000'));
                    }
                }
            };

            var failCreateCbFn = function (err) {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', ` ${err} `, '6000'));
            };
            services.postGeneric("receipt/createReceiptRest", createpayload[0]).then(successCreateCbFn, failCreateCbFn)
        };
        //-----------------Selection Action --------------------------
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            if (currentRow)
                self.customsModel.rowSelected = self.customsModel.receiptTableArr()[[currentRow['rowIndex']]];

            self.customsModel.selectedRowKey(self.customsModel.receiptTableArr()[[currentRow['rowIndex']]].requestId)
            self.customsModel.isDisabled(false);
        };

        //     --------------------------------------- view approval function button ------------------------ 

        self.DownloadFileFn = function (){

        };
        //------------------View Button Actions --------------------------
        self.requestDetailsBtnAction = function () {
            self.customsModel.receiptTableDetailsArr(self.customsModel.rowSelected.lines);
            console.log(self.customsModel.rowSelected.lines)
            self.detailsDataProvider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.receiptTableDetailsArr)));
            document.querySelector("#receiptLineDialog").open();
        };

        //------------------Translation --------------------------
        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                paymentNumberLbl: getTransaltion("paymentDetails.paymentNumberLbl"),
                paymentStatusLbl: getTransaltion("paymentDetails.paymentStatusLbl"),
                SupplierNumberLbl: getTransaltion("paymentDetails.SupplierNumberLbl"),
                paymentAmountLbl: getTransaltion("paymentDetails.paymentAmountLbl"),
                paymentCurrancyLbl: getTransaltion("paymentDetails.paymentCurrancyLbl"),
                paymentDescriptionLbl: getTransaltion("paymentDetails.paymentDescriptionLbl"),
                paymentDateLbl: getTransaltion("paymentDetails.paymentDateLbl"),
                paymentMethodLbl: getTransaltion("paymentDetails.paymentMethodLbl"),
                paymentTypeLbl: getTransaltion("paymentDetails.paymentTypeLbl"),
                createdByLbl: getTransaltion("paymentDetails.createdByLbl"),
                paymentIdLbl: getTransaltion("paymentDetails.paymentIdLbl"),
                operationTypesLbl: getTransaltion("paymentDetails.operationTypesLbl"),
                approvalscreenLbl: getTransaltion("paymentDetails.approvalscreenLbl"),
                approveLbl: getTransaltion("common.approveLbl"),
                rejectLbl: getTransaltion("common.rejectLbl"),
                viewLbl: getTransaltion("paymentDetails.viewLbl"),
                screenLbl: getTransaltion("paymentDetails.screenLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                viewApprovalLabel: getTransaltion("common.viewApprovalLabel"),
                businessUnitLbl: getTransaltion("receiptScreen.businessUnitLbl"),
                POLbl: getTransaltion("receiptScreen.POLbl"),
                requisitionLbl: getTransaltion("receiptScreen.requisitionLbl"),
                itemDescriptionLbl: getTransaltion("receiptScreen.itemDescriptionLbl"),
                supplierName: getTransaltion("receiptScreen.supplierName"),
                needByDate: getTransaltion("receiptScreen.needByDate"),
                qtyDelivered: getTransaltion("receiptScreen.qtyDelivered"),
                qtyOrdered: getTransaltion("receiptScreen.qtyOrdered"),
                uom: getTransaltion("receiptScreen.uom"),
                currencyLbl: getTransaltion("receiptScreen.currencyLbl"),
                avilableLbl: getTransaltion("receiptScreen.avilableLbl"),
                POlineNum: getTransaltion("receiptScreen.POlineNum"),
            });

            self.customsModel.columnArray([
                {
                    "headerText": "Requester Name",
                    "field": "REQUESTER_NAME"
                },
                {
                    "headerText": "Requester Type",
                    "field": "REQUEST_TYPE"
                },
                {
                    "headerText": "Creation Date",
                    "field": "RECEIPT_CREATION_DATE"
                },
                {
                    "headerText": "Purchase Order Number",
                    "field": "PO_NUMBER"
                },
                {
                    "headerText": "Approval Status",
                    "field": "APPROVAL_STATUS"
                },
                {
                    "headerText": "Reference Number",
                    "field": "REFERENCE_NUM"
                }

            ]);

            self.customsModel.detailsColumnArray([
                {
                    "headerText": "NO", "template": "seqTemplate"
                },
                {
                    "headerText": self.label().businessUnitLbl, "field": "BU_NAME"
                },
                {
                    "headerText": self.label().requisitionLbl, "field": "REQUISITION_NUMBER"
                },
                {
                    "headerText": self.label().itemDescriptionLbl, "field": "ITEM_DESCRIPTION"
                },
                {
                    "headerText": self.label().supplierName, "field": "VENDOR_NAME"
                },
                {
                    "headerText": self.label().needByDate, "field": "NEED_BY_DATE"
                },
                {
                    "headerText": self.label().qtyDelivered, "field": "QUANTITY_DELIVERED"
                },
                {
                    "headerText": self.label().qtyOrdered, "field": "QUANTITY_ORDERED"
                },
                {
                    "headerText": "Quantity", "field": "QUANTITY"
                },
                {
                    "headerText": self.label().uom, "field": "UNIT_OF_MEASURE"
                },
                {
                    "headerText": self.label().currencyLbl, "field": "CURRENCY_CODE"
                },
                {
                    "headerText": self.label().avilableLbl, "field": "AVILABLE"
                },
                {
                    "headerText": self.label().POLbl, "field": "SEGMENT1"
                },
                {
                    "headerText": self.label().POlineNum, "field": "LINE_NUMBER"
                },
                {
                    "headerText": "Refrence Number", "field": "REFERENCE_NUM"
                },
                {
                    "headerText": "Attachments", "template": "downTemplate"
                }

            ]);
        }
        initTransaltion();
    }

    return notificationScreenContentViewModel;
});
