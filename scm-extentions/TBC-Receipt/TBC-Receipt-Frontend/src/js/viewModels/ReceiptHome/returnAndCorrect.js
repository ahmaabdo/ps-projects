define(['ojs/ojcore', 'knockout', 'ojs/ojbootstrap', 'appController', 'config/services',
    'ojs/ojconverter-number', 'ojs/ojconverter-datetime', 'ojs/ojconverterutils-i18n', 'ojs/ojvalidator-numberrange', 'ojs/ojdatacollection-utils',
    'ojs/ojarraydataprovider', 'ojs/ojcollapsible', 'ojs/ojselectcombobox', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojselectsingle', 'ojs/ojformlayout', 'ojs/ojbutton', 'ojs/ojtable',
    'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojlabelvalue', 'ojs/ojarraydataprovider',
], function (oj, ko, Bootstrap, app, services,
    NumberConverter, DateTimeConverter, ConverterUtils, NumberRangeValidator, DataCollectionEditUtils,
    ArrayDataProvider) {

    function returnAndCorrectContentViewModel() {
        var self = this;
        self.dataSource = ko.observable();
        self.tableData = ko.observableArray([]);
        self.columnArray = ko.observableArray();
        self.dataprovider = new ArrayDataProvider(self.tableData, { keyAttributes: 'seq' });
        self.retrieved = ko.observable();
        self.pageLbl = ko.observable();
        self.label = ko.observable();
        self.receiptNumberVal = ko.observable();
        self.supplierNameVal = ko.observable();
        
        self.connected = function () {
            initTranslations();
            self.retrieved(oj.Router.rootInstance.retrieve());

            if (self.retrieved() == "return") {
                self.pageLbl(self.label().returnScreenLbl);
                self.columnArray().push(
                    {
                        "field": "newQty",
                        "headerText": "Return Quantity",
                        "headerStyle": "min-width: 12em; max-width: 12em; width: 12em",
                        "headerClassName": "oj-helper-text-align-end",
                        "style": "min-width: 12em; max-width: 12em; width: 12em",
                        "className": "oj-helper-text-align-end",
                        "template": "locIdTemplate"
                    },
                    {
                        "headerText": "Action",
                        "headerStyle": "min-width: 10em; max-width: 10em; width: 10em; text-align: center;",
                        "style": "min-width: 10em; max-width: 10em; width: 10em; padding-top: 0px; padding-bottom: 0px; text-align: center;",
                        "template": "actionTemplate"
                    },
                );
                var retrivedData = JSON.parse(localStorage.getItem("returnRowData"))
                self.receiptNumberVal(retrivedData[0].RECEIPT_NUMBER)
                self.supplierNameVal(retrivedData[0].VENDOR_NAME)
                self.tableData(retrivedData)
                self.dataprovider = new ArrayDataProvider(self.tableData, { keyAttributes: 'seq' });
            } else {
                self.pageLbl(self.label().correctScreenLbl);
                self.columnArray().push(
                    {
                        "field": "newQty",
                        "headerText": "Correct Quantity",
                        "headerStyle": "min-width: 12em; max-width: 12em; width: 12em",
                        "headerClassName": "oj-helper-text-align-end",
                        "style": "min-width: 12em; max-width: 12em; width: 12em",
                        "className": "oj-helper-text-align-end",
                        "template": "locIdTemplate"
                    },
                    {
                        "headerText": "Action",
                        "headerStyle": "min-width: 10em; max-width: 10em; width: 10em; text-align: center;",
                        "style": "min-width: 10em; max-width: 10em; width: 10em; padding-top: 0px; padding-bottom: 0px; text-align: center;",
                        "template": "actionTemplate"
                    },
                );
                var retrivedData = JSON.parse(localStorage.getItem("correctRowData"))
                self.receiptNumberVal(retrivedData[0].RECEIPT_NUMBER)
                self.supplierNameVal(retrivedData[0].VENDOR_NAME)
                self.tableData(retrivedData)
                self.dataprovider = new ArrayDataProvider(self.tableData, { keyAttributes: 'seq' });
            }
        };

        self.tableSelectionListener = function () {

        };

        self.submitBtnAction = function () {
            var isQtyAdd = self.tableData().every(el => el.newQty);
            if (isQtyAdd) {
                app.loading(true)
                var payload = [];
                for (var i in self.tableData()) {
                    payload.push({
                        "transactionType": self.retrieved() == "return" ? "RETURN TO VENDOR" : "CORRECT",
                        "parentTransactionId": self.tableData()[i].TRANSACTION_ID,
                        "DocumentNumber": self.tableData()[i].PO_NUMBER,
                        "DocumentLineNumber": self.tableData()[i].LINE_NUM,
                        "Quantity": self.tableData()[i].QUANTITY_DELIVERED && self.retrieved() == "return" ? self.tableData()[i].newQty : self.tableData()[i].QUANTITY_DELIVERED ? self.tableData()[i].newQty - self.tableData()[i].QUANTITY_DELIVERED : "",
                        "UnitOfMeasure": self.tableData()[i].UNIT_OF_MEASURE ? self.tableData()[i].UNIT_OF_MEASURE : "",
                        "Amount": self.tableData()[i].AMOUNT_RECEIVED && self.retrieved() == "return" ? self.tableData()[i].newQty : self.tableData()[i].AMOUNT_RECEIVED ? self.tableData()[i].newQty - self.tableData()[i].AMOUNT_RECEIVED : "",
                        "receiveTransactionId" : self.tableData()[i].RECEIVE_TRANS_ID 
                    });
                };

                var successCbFn = function (data) {
                    app.loading(false)
                    for (var i in data) {
                        if (data[i].Status == "ERROR") {
                            app.messagesDataProvider.push(app.createMessage('error', `${data[i].Error}`, '6000'));
                        } else {
                            app.messagesDataProvider.push(app.createMessage('confirmation', 'Operation Success', '2000'));
                            oj.Router.rootInstance.go("ManageReceipt")
                        }
                    }
                };
                var failCbFn = function () {
                    app.loading(false)
                };
                services.postGeneric("receipt/corretAndReturn", payload).then(successCbFn, failCbFn)
            } else {
                app.messagesDataProvider.push(app.createMessage('error', 'Please Add Quantity!', '2000'));
            }
        };
        self.cancelBtnAction = function () {
            oj.Router.rootInstance.go('ManageReceipt');
        };

        //----------------------table editable------------
        this.numberConverter = new NumberConverter.IntlNumberConverter();
        this.dateConverter = new DateTimeConverter.IntlDateTimeConverter({ year: '2-digit', month: '2-digit', day: '2-digit' });
        this.editRow = ko.observable();

        this.beforeRowEditListener = function (event, context) {
            var key = event.detail.rowContext.status.rowKey;
            this.dataprovider.fetchByKeys({ keys: [key] }).then(function (fetchResult) {
                this.rowData = {};
                Object.assign(this.rowData, fetchResult.results.get(key).data);
                var rangeValidator = new NumberRangeValidator({ min: 0, max: context.rowData.QuantityReceived });
                this.validators = [rangeValidator];
            }.bind(this));
        }.bind(this);

        this.beforeRowEditEndListener = function (event, context) {
            var detail = event.detail;
            if (detail.cancelEdit == true) {
                return;
            }
            if (DataCollectionEditUtils.basicHandleRowEditEnd(event, detail) === false) {
                event.preventDefault();
            } else {
                self.tableData().splice(detail.rowContext.status.rowIndex, 1, this.rowData);
                // document.getElementById('rowDataDump').value = (JSON.stringify(this.rowData));
            }
        }.bind(this);
        this.handleUpdate = function (event, context) {
            this.editRow({ rowKey: context.key });
        }.bind(this);

        this.handleDone = function (event, context) {

            this.editRow({ rowKey: null });
        }.bind(this);

        var getTranslation = oj.Translations.getTranslatedString;
        function initTranslations() {
            self.label({
                businessUnitLbl: getTranslation("receiptScreen.businessUnitLbl"),
                POLbl: getTranslation("receiptScreen.POLbl"),
                recepitLbl: getTranslation("receiptScreen.recepitLbl"),
                searchLbl: getTranslation("common.searchLbl"),
                resetLbl: getTranslation("common.resetLbl"),
                submitLbl: getTranslation("common.submitLbl"),
                cancelLbl: getTranslation("common.cancelLbl"),
                saveLbl: getTranslation("common.saveLbl"),
                returnLbl: getTranslation("receiptScreen.returnLbl"),
                correctLbl: getTranslation("receiptScreen.correctLbl"),
                mangeReceiptScreen: getTranslation("receiptScreen.mangeReceiptScreen"),
                uom: getTranslation("receiptScreen.uom"),
                itemDescriptionLbl: getTranslation("receiptScreen.itemDescriptionLbl"),
                supplierName: getTranslation("receiptScreen.supplierName"),
                POlineNum: getTranslation("receiptScreen.POlineNum"),
                needByDate: getTranslation("receiptScreen.needByDate"),
                qtyDelivered: getTranslation("receiptScreen.qtyDelivered"),
                qtyReceived: getTranslation("receiptScreen.qtyReceived"),
                qtyShipped: getTranslation("receiptScreen.qtyShipped"),
                receiptDateLbl: getTranslation("receiptScreen.receiptDateLbl"),
                selectPlaceholder: getTranslation("common.selectPlaceholder"),
                actionTypeRLbl: getTranslation("receiptScreen.actionTypeRLbl"),
                returnScreenLbl: getTranslation("receiptScreen.returnScreenLbl"),
                correctScreenLbl: getTranslation("receiptScreen.correctScreenLbl"),
            })
            self.columnArray([
                {
                    "headerText": self.label().businessUnitLbl, "field": "BU_NAME"
                },
                {
                    "headerText": self.label().recepitLbl, "field": "RECEIPT_NUMBER"
                },
                {
                    "headerText": self.label().receiptDateLbl, "field": "RECEIPT_DATE"
                },

                {
                    "headerText": self.label().itemDescriptionLbl, "field": "ITEM_DESCRIPTION"
                },
                {
                    "headerText": self.label().supplierName, "field": "VENDOR_NAME"
                },
                {
                    "headerText": self.label().POLbl, "field": "PO_NUMBER"
                },
                {
                    "headerText": self.label().POlineNum, "field": "LINE_NUM"
                },
                {
                    "headerText": self.label().qtyDelivered, "field": "QUANTITY_DELIVERED"
                },
                {
                    "headerText": self.label().qtyReceived, "field": "QUANTITY_RECEIVED"
                },
                {
                    "headerText": self.label().uom, "field": "UNIT_OF_MEASURE"
                },
                {
                    "headerText": "Amount Received", "field": "AMOUNT_RECEIVED"
                },
            

            ]);
        }
    }

    return returnAndCorrectContentViewModel;
});
