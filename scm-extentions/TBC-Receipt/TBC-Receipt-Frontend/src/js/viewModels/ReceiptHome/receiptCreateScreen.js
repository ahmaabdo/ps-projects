
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbootstrap', 'appController', 'config/services', 'ojs/ojconverter-number', 'ojs/ojvalidator-numberrange', 'ojs/ojdatacollection-utils', 'ojs/ojarraydataprovider', 'ojs/ojcollapsible', 'ojs/ojselectcombobox',
    'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojselectsingle', 'ojs/ojformlayout', 'ojs/ojbutton', 'ojs/ojtable', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojdialog', 'ojs/ojlabelvalue', 'ojs/ojarraydataprovider', 'ojs/ojtrain', 'ojs/ojfilepicker'
], function (oj, ko, $, Bootstrap, app, services, NumberConverter, NumberRangeValidator, DataCollectionEditUtils, ArrayDataProvider) {
    function receiptCreateScreenContentViewModel() {
        var self = this;
        var index;
        self.dataSource = ko.observable();
        self.screenChangedLbl = ko.observable("Receipt Page");
        self.tableData = ko.observableArray([]);
        self.reviewData = ko.observableArray();
        self.maxReceiptNumber = ko.observable();
        self.columnArray = ko.observableArray();
        self.dataSourceReview = new oj.ArrayDataProvider(self.reviewData);
        self.isDisabled = ko.observable(true);
        self.selectedStepValue = ko.observable('stp1');
        self.selectedStepLabel = ko.observable('Step One');
        self.stepArray = ko.observableArray(
            [{ label: ' Receipt', id: 'stp1', },
            { label: 'Evaluate ', id: 'stp3' }
            ]);
        self.fileNames = ko.observableArray([]);
        self.rowData = ko.observable();
        self.label = ko.observable();
        self.dataprovider = new ArrayDataProvider(self.tableData, { keyAttributes: 'seq' });

        self.customsModel = {
            rowSelected: ko.observable(),
            contentData: ko.observable(),
            attTitle: ko.observable(),
            attName: ko.observable(),
            attLength: ko.observable(),
            attType: ko.observable(),
            fileData: ko.observable(),
            attachmentArr: ko.observableArray([]),
            attachmentArrHeader: ko.observableArray([]),
            payloadAttach: ko.observableArray([]),
            attachmentDisable: ko.observable(true),
            removeDisable: ko.observable(false),
            selectedItemsAttachment: ko.observableArray([]),
            selectedItemsAttachmentHeader: ko.observableArray([]),
        };

        self.dataProviderAttachment = ko.observable()
        self.dataProviderAttachment(new oj.ArrayDataProvider(self.customsModel.attachmentArr))
        self.dataProviderAttachmentHeader = ko.observable()
        self.dataProviderAttachmentHeader(new oj.ArrayDataProvider(self.customsModel.attachmentArrHeader))

        self.connected = function () {
            initTranslations();
            var retrivedData = JSON.parse(localStorage.getItem("rowData"));
            self.tableData(retrivedData);
            console.log(self.tableData())
            self.dataprovider = new ArrayDataProvider(self.tableData, { keyAttributes: 'seq' });
        };

        //----------------------------table Editable--------------------------------------
        this.numberConverter = new NumberConverter.IntlNumberConverter();
        this.editRow = ko.observable();
        this.beforeRowEditListener = function (event, context) {
            var key = event.detail.rowContext.status.rowKey;
            this.dataprovider.fetchByKeys({ keys: [key] }).then(function (fetchResult) {
                this.rowData = {};
                Object.assign(this.rowData, fetchResult.results.get(key).data);
                var rangeValidator = new NumberRangeValidator({ min: 1, max: context.rowData.AVILABLE });
                this.validators = [rangeValidator];
            }.bind(this));
        }.bind(this);

        this.beforeRowEditEndListener = function (event, context) {
            var detail = event.detail;
            if (detail.cancelEdit == true) {
                return;
            }
            if (DataCollectionEditUtils.basicHandleRowEditEnd(event, detail) === false) {
                event.preventDefault();
            } else {
                self.tableData().splice(detail.rowContext.status.rowIndex, 1, this.rowData);
                // document.getElementById('rowDataDump').value = (JSON.stringify(this.rowData));
            }
        }.bind(this);

        this.handleUpdate = function (event, context) {

            this.editRow({ rowKey: context.key });
            context.row.quantity = context.row.AVILABLE
        }.bind(this);

        this.handleDone = function (event, context) {
            this.editRow({ rowKey: null });
        }.bind(this);
        //----------------------------------Train buttons--------------------------------

        self.updateLabelText = function (event) {
            var train = document.getElementById('train');
            self.selectedStepLabel(train.getStep(event.detail.value).label);
            self.selectedStepValue(train.getStep(event.detail.value).id);
            self.screenChangedLbl(train.getStep(event.detail.value).label + " Page")
        };

        self.ReviewBtnAction = function () {
            var train = document.getElementById('train');
            var next = train.getNextSelectableStep();
            if (next != null) {
                self.selectedStepValue(next);
                self.selectedStepLabel(train.getStep(next).label);
            }
        };

        self.backBtnAction = function () {
            var train = document.getElementById('train');
            var prev = train.getPreviousSelectableStep();
            if (prev != null) {
                self.selectedStepValue(prev);
                self.selectedStepLabel(train.getStep(prev).label);
            }
        };

        self.createInitiativeBtnAction = function () {
            var train = document.getElementById('train');
            var next = train.getNextSelectableStep();
            if (next != null) {
                self.selectedStepValue(next);
                self.selectedStepLabel(train.getStep(next).label);
            }
        };

        self.cancelBtnAction = function () {
            oj.Router.rootInstance.go('myReceipts');
        };

        //--------------------------------Create Receipt Method -------------------------------
        self.createReceiptBtnAction = function () {
            var isQtyAdd = self.tableData().every(el => el.quantity)

            if (isQtyAdd) {
                app.loading(true);
                var payload = []
                self.tableData().forEach(e =>
                    e.attachment ? payload.push(...e.attachment) : []
                );
                self.insertReceiptDb(payload);
            } else {
                app.messagesDataProvider.push(app.createMessage('error', 'Please Add Quantity!', '2000'));
            }
        };

        self.insertReceiptDb = function (attachmentPayload) {
            self.tableData().forEach(el => {
                el.REQUESTER_NAME = app.personDetails()[0].displayName,
                el.MANAGER_ID = app.costCenterId()
            })

            var payload = {
                "RECEIPT_CREATION_DATE": self.currentDate(),
                "PO_NUMBER": self.tableData()[0].SEGMENT1,
                "ATTACHMENT_NUM": "",
                "REQUESTER_NAME": app.personDetails()[0].displayName,
                "REQUEST_TYPE": "Receiving PO",
                "MANAGER_ID" : app.costCenterId(),
                "REQUESTES": self.tableData()
            }

            services.postGeneric("notification/addReceiptRequest", payload).then(data => {
                attachmentPayload.forEach(el => {
                    el.refernceNum = data.Data.referneceNumber
                })
                if (attachmentPayload.length > 0) {
                    services.postGeneric("receipt/receiptAttachment", attachmentPayload).then(data => {
                        app.loading(false)
                    }, error => {
                        app.loading(false)
                    });
                }
                app.loading(false);
                oj.Router.rootInstance.go('myReceipts');
            }, error => {
                app.loading(false);
            })
            console.log(payload)
        }

        //--------------------------------Max Receipt Method --------------------------------
        var getMaxReceiptNumber = function () {
            app.loading(true);
            var successCbFn = function (data) {
                var resData = JSON.parse(data)
                self.maxReceiptNumber(resData.RECEIPT_NUMBER)
                app.loading(false);
                return self.maxReceiptNumber()
            };
            var failCbFn = function () {
                app.loading(false)
            };
            services.getGeneric("report/Max_Receipt_Num_Report").then(successCbFn, failCbFn)
        };

        //---------------------------------Table Selection-------------------------------------

        $("body").delegate("#tablestp1 .oj-table-body-row", "click", (e) => {
            index = $(e.currentTarget).find(".hiddenkeyHolder").val();
            self.customsModel.rowSelected(self.tableData()[index]);
            if (e.target.type == 'button') {
                if (self.customsModel.rowSelected()) {
                    document.querySelector("#attachDialog").open();
                    var rowAttachment = self.customsModel.rowSelected().attachment ? self.customsModel.rowSelected().attachment : []
                    self.customsModel.attachmentArr.push(...rowAttachment);
                    self.dataProviderAttachment(new oj.ArrayDataProvider(self.customsModel.attachmentArr))
                }
            }
        });

        //------------------------------ Select attachment Function-------------------------------------
        self.addFileSelectListener = function (event) {
            var x = {};
            var file = event.detail.files[0]
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (evt) {
                //                if(file.size >2000000){
                //               app.messagesDataProvider.push(app.createMessage('error', 'The maximum size is 2 mb'));
                //                    return;
                //                }
                self.customsModel.contentData(evt.target.result);
                self.customsModel.fileData(event.detail.files);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name)
                self.customsModel.attLength(self.customsModel.fileData()[0].size)
                self.customsModel.attType(self.customsModel.fileData()[0].type)
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0])
                x = {
                    "title": self.customsModel.attTitle(),
                    "content": self.customsModel.contentData().split(",")[1],
                    "poNum": self.customsModel.rowSelected().SEGMENT1,
                    "lineNum": self.customsModel.rowSelected().LINE_NUMBER,
                };
                if (x) {

                    self.customsModel.attachmentArr.push(x);
                    x = {};
                    self.dataProviderAttachment(new oj.ArrayDataProvider(self.customsModel.attachmentArr))
                }
            }
        };

        //------------------------------ remove selected attachment from list-------------------------------------
        self.removeSelectedAttachment = function () {
            if (self.customsModel.selectedItemsAttachment().length < 1) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select an item'));
                return;
            }
            var x = self.customsModel.attachmentArr().findIndex((el => el.FileName == self.customsModel.selectedItemsAttachment()[0][1]))
            self.customsModel.attachmentArr().splice(x, 1);
            $('#filePicker input').val(null)
            self.dataProviderAttachment(new oj.ArrayDataProvider(self.customsModel.attachmentArr))
            if (self.customsModel.attachmentArr().length == 0) {
            }
        };
        //------------------------------ Submit Attachments lines function-------------------------------------

        self.saveAttachmentBtn = function () {
            self.tableData()[index].attachment = self.customsModel.attachmentArr();
            self.dataprovider = new ArrayDataProvider(self.tableData, { keyAttributes: 'seq' });
            document.querySelector("#attachDialog").close();
            self.customsModel.attachmentArr([])

        };
        //------------------------------ cancel line dialog function------------------------------------- 
        self.canceldialogBtnAction = function () {
            document.querySelector("#attachDialog").close();
            self.customsModel.attachmentArr([]);
        };

        //------------------------------ Header attachments function-------------------------------------

        self.headerAttachments = function () {
            document.querySelector("#headerAttachments").open();
        };

        //------------------------------ Header attachment Actions-------------------------------------
        self.addFileSelectListenerHeader = function (event) {
            var x = {};
            var file = event.detail.files[0]
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (evt) {
                if (file.size > 2000000) {
                    app.messagesDataProvider.push(app.createMessage('error', 'The maximum size is 2 mb'));
                    return;
                }
                self.customsModel.contentData(evt.target.result);
                self.customsModel.fileData(event.detail.files);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name)
                self.customsModel.attLength(self.customsModel.fileData()[0].size)
                self.customsModel.attType(self.customsModel.fileData()[0].type)
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0])
                x = {
                    "DatatypeCode": "FILE",
                    "Title": self.customsModel.attTitle(),
                    "Description": "TEXT UPLOAD from REST",
                    "FileName": self.customsModel.attTitle(),
                    "UploadedFileContentType": self.customsModel.attType(),
                    "CategoryName": "MISC",
                    "FileContents": self.customsModel.contentData().split(",")[1]
                };
                if (x) {

                    self.customsModel.attachmentArrHeader.push(x);
                    x = {};
                    self.dataProviderAttachmentHeader(new oj.ArrayDataProvider(self.customsModel.attachmentArrHeader))
                }
            }
        };

        //------------------------------ remove selected attachment from list-------------------------------------
        self.removeSelectedAttachmentHeader = function () {
            if (self.customsModel.selectedItemsAttachmentHeader().length < 1) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select an item'));
                return;
            }
            var x = self.customsModel.attachmentArrHeader().findIndex((el => el.FileName == self.customsModel.selectedItemsAttachmentHeader()[0][1]))
            self.customsModel.attachmentArrHeader().splice(x, 1);
            $('#filePicker input').val(null)
            self.dataProviderAttachmentHeader(new oj.ArrayDataProvider(self.customsModel.attachmentArrHeader))
            if (self.customsModel.attachmentArrHeader().length == 0) {
            }
        };

        self.attachmentHeaderSubmit = function () {
            document.querySelector("#headerAttachments").close();
        };

        self.attachmentHeaderCancel = function () {
            document.querySelector("#headerAttachments").close();
        };

        self.currentDate = function () {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = mm + '/' + dd + '/' + yyyy;
            return today;
        }

        //------------------------------ Translation function-------------------------------------

        var getTranslation = oj.Translations.getTranslatedString;

        function initTranslations() {
            self.label({
                AttachmentLbl: getTranslation("receiptScreen.AttachmentLbl"),
                addFileLbl: getTranslation("receiptScreen.addFileLbl"),
                attachmentsDialogLbl: getTranslation("receiptScreen.attachmentsDialogLbl"),
                okLbl: getTranslation("common.okLbl"),
                cancelLbl: getTranslation("common.cancelLbl"),
                businessUnitLbl: getTranslation("receiptScreen.businessUnitLbl"),
                POLbl: getTranslation("receiptScreen.POLbl"),
                requisitionLbl: getTranslation("receiptScreen.requisitionLbl"),
                itemDescriptionLbl: getTranslation("receiptScreen.itemDescriptionLbl"),
                supplierName: getTranslation("receiptScreen.supplierName"),
                needByDate: getTranslation("receiptScreen.needByDate"),
                qtyDelivered: getTranslation("receiptScreen.qtyDelivered"),
                qtyOrdered: getTranslation("receiptScreen.qtyOrdered"),
                uom: getTranslation("receiptScreen.uom"),
                currencyLbl: getTranslation("receiptScreen.currencyLbl"),
                avilableLbl: getTranslation("receiptScreen.avilableLbl"),
                POlineNum: getTranslation("receiptScreen.POlineNum"),
                attachments: getTranslation("receiptScreen.attachments"),
                cancelLbl: getTranslation("common.cancelLbl"),
                createReceiptLbl: getTranslation("receiptScreen.createReceiptLbl"),
                actionTypeLbl: getTranslation("receiptScreen.actionTypeLbl"),
                saveLbl: getTranslation("common.saveLbl"),
                screenLbl: getTranslation("receiptScreen.screenLbl"),

            });

            self.columnArray([

                {
                    "headerText": "NO", "template": "seqTemplate"
                },
                {
                    "headerText": self.label().businessUnitLbl, "field": "BU_NAME"
                },
                {
                    "headerText": self.label().requisitionLbl, "field": "REQUISITION_NUMBER"
                },
                {
                    "headerText": self.label().itemDescriptionLbl, "field": "ITEM_DESCRIPTION"
                },
                {
                    "headerText": self.label().supplierName, "field": "VENDOR_NAME"
                },
                {
                    "headerText": self.label().needByDate, "field": "NEED_BY_DATE"
                },
                {
                    "headerText": self.label().qtyDelivered, "field": "QUANTITY_DELIVERED"
                },
                {
                    "headerText": self.label().qtyOrdered, "field": "QUANTITY_ORDERED"
                },
                {
                    "headerText": self.label().uom, "field": "UNIT_OF_MEASURE"
                },
                {
                    "headerText": self.label().currencyLbl, "field": "CURRENCY_CODE"
                },
                {
                    "field": "quantity",
                    "headerText": "Quantity",
                    "headerStyle": "min-width: 12em; max-width: 12em; width: 12em",
                    "headerClassName": "oj-helper-text-align-end",
                    "style": "min-width: 12em; max-width: 12em; width: 12em",
                    "className": "oj-helper-text-align-end",
                    "template": "locIdTemplate"
                },
                {
                    "headerText": "Action",
                    "headerStyle": "min-width: 10em; max-width: 10em; width: 10em; text-align: center;",
                    "style": "min-width: 10em; max-width: 10em; width: 10em; padding-top: 0px; padding-bottom: 0px; text-align: center;",
                    "template": "actionTemplate"
                },

                {
                    "headerText": self.label().avilableLbl, "field": "AVILABLE"
                },
                ,
                {
                    "headerText": self.label().POLbl, "field": "SEGMENT1"
                },
                {
                    "headerText": self.label().POlineNum, "field": "LINE_NUMBER"
                },
                {
                    "headerText": self.label().attachments, "template": "attachmentTemplate"
                }

            ])

        }

    }

    return receiptCreateScreenContentViewModel;
});
