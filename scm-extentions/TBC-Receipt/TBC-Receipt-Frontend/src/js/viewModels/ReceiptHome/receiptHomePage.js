
define(['ojs/ojcore', 'knockout', 'ojs/ojbootstrap', 'appController', 'config/services', 'ojs/ojarraydataprovider', 'ojs/ojcollapsible', 'ojs/ojselectcombobox',
    'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojselectsingle', 'ojs/ojformlayout', 'ojs/ojbutton', 'ojs/ojtable',
    'ojs/ojpagingcontrol', 'ojs/ojlabelvalue', 'ojs/ojarraydataprovider', 'ojs/ojcheckboxset', 'ojs/ojpagingtabledatasource', 'ojs/ojarraytabledatasource'


], function (oj, ko, Bootstrap, app, services, ArrayDataProvider) {

    function receiptHomePageContentViewModel() {

        var self = this;
        self.tableData = ko.observableArray([]);
        self.headerCheckStatus = ko.observable();
        self.selectedItems = ko.observableArray([]);
        self.columnArray = ko.observableArray();
        self.checkHeaderDisable = ko.observable(false);
        self.label = ko.observable();

        self.customsModel = {
            requestersArr: ko.observableArray([]),
            purchaseOrdersArr: ko.observableArray([]),
            businessUnitArr: ko.observableArray([]),
            placeHolder: ko.observable(),
            reportDataArr: ko.observableArray([]),
            rowSelectedArr: ko.observableArray([]),
            businessUnitVal: ko.observable(),
            purchaseOrderVal: ko.observable(),
            requesterVal: ko.observable(),
            EnterByVal: ko.observable(),
            requisitionVal: ko.observable(),
            receiveDisable: ko.observable(true),
        };

        self.pagingDataProvider = ko.observable();
        self.pagingDataProvider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tableData, { idAttribute: 'seq' })));

        //----------------------------------connected Action------------------------------------
        self.connected = function () {
            initTranslations();
        };

        ko.computed(function () {
            self.customsModel.businessUnitArr(app.businessUnitArr())
            self.customsModel.purchaseOrdersArr(app.purchaseOrdersArr())
            self.customsModel.requestersArr(app.AllemployeesArr())

        });

        //-----------------------------receive BtnAction Action---------------------------------------
        self.receiveBtnAction = function () {
            localStorage.setItem("rowData", JSON.stringify(self.customsModel.rowSelectedArr()))
            oj.Router.rootInstance.go('createReceipt');
        };

        //-------------------------------Search Action------------------------------------------------
        self.searchBtnAction = function () {
            if (!self.customsModel.businessUnitVal() && !self.customsModel.purchaseOrderVal() && !self.customsModel.requesterVal()
                && !self.customsModel.EnterByVal() && !self.customsModel.requisitionVal()) {
                self.tableData([])
                app.messagesDataProvider.push(app.createMessage('warning', 'Please Select Value!', '2000'));
                return;
            }
            if (self.customsModel.businessUnitVal() && !self.customsModel.purchaseOrderVal()) {
                app.messagesDataProvider.push(app.createMessage('warning', 'Please Select Requisitioning BU and Purchase Order!', '2000'));
                return;
            }
            self.getRecieptCreationData(self.customsModel.requesterVal(), self.customsModel.EnterByVal(), self.customsModel.requisitionVal(),
                self.customsModel.businessUnitVal(), self.customsModel.purchaseOrderVal())
        };

        //-------------------------Get All Data Action----------------------------
        self.getRecieptCreationData = function (requester, enteredBy, requisition, businessUnit, purchaseOrder) {
            app.loading(true);
            var payload = {
                P_REQUESTER: requester ? requester : "",
                P_CREATED_BY: enteredBy ? enteredBy : "",
                P_REQUISITION: requisition ? requisition : "",
                P_BU: businessUnit ? businessUnit : "",
                P_PO: purchaseOrder ? purchaseOrder : "",
                reportName: "receiving_Lines_Report"
            };

            var SuccessCbFn = function (data) {
                app.loading(false);
                var seq = 0;
                var resdata;
                data = data.filter(e=> e.REQUISITION_CREATED_BY == app.personDetails()[0].userName || e.Requester == app.personDetails()[0].userName)
                if (Array.isArray(data)) {
                    if (data.length > 0) {
                        resdata = data
                        resdata.forEach(e => {e.seq = ++seq , e.ITEM_DESCRIPTION = e.ITEM_DESCRIPTION.split('_')[0] , e.avilableFlag = e.hasOwnProperty('AVILABLE') ? "true" : "false"})
                        console.log(resdata)
                    } else {
                        resdata = [];
                        app.messagesDataProvider.push(app.createMessage('warning', 'No Data to display', '2000'));
                    }
                } else {
                    if (data != "[]") {
                        resdata = [data];
                        resdata.forEach(e => {e.seq = ++seq , e.ITEM_DESCRIPTION = e.ITEM_DESCRIPTION.split('_')[0], e.avilableFlag = e.hasOwnProperty('AVILABLE') ? "true" : "false"})
                    } else {
                        resdata = [];
                        app.messagesDataProvider.push(app.createMessage('warning', 'No Data to display', '2000'));
                    }
                }
                self.tableData(resdata);
                self.pagingDataProvider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tableData, { idAttribute: 'seq' })));
            };

            var failCbFn = function () {
                app.messagesDataProvider.push(app.createMessage('error', 'Error', '2000'));
                app.loading(false);
            };
            services.postGeneric("report/commonbireport", payload).then(SuccessCbFn, failCbFn)
        };

        //----------------------------------Delegation  Action-------------------------------------
        $("body").delegate("#table .oj-table-body-row", "click", (e) => {
            var index = $(e.currentTarget).find(".hiddenkeyHolder").val();
            self.customsModel.rowSelected = self.tableData()[index];
            if (e.target.type == 'checkbox') {
                if (self.customsModel.rowSelected) {
                    if (e.target.checked) {
                        self.customsModel.rowSelectedArr().push(self.customsModel.rowSelected);
                        self.customsModel.receiveDisable(false)

                    } else {
                        self.customsModel.rowSelectedArr(self.customsModel.rowSelectedArr().filter(el => el.seq != self.customsModel.rowSelected.seq))
                        if (self.customsModel.rowSelectedArr().length == 0) {
                            self.customsModel.receiveDisable(true)
                        }
                    }
                }
            }
        });

        //-------------------------cheack Box handelers  Action----------------------------
        self.getIndexInSelectedItems = function (id) {
            for (var i = 0; i < self.selectedItems().length; i++) {
                var range = self.selectedItems()[i];
                var startIndex = range.startIndex.row;
                var endIndex = range.endIndex.row;
                if (id >= startIndex && id <= endIndex) {
                    return i;
                }
            }
            return -1;
        };

        self.handleCheckbox = function (id, srN) {

            var isChecked = self.getIndexInSelectedItems(id) != -1 || (srN ? self.customsModel.rowSelectedArr().find(e => e.seq === srN) : false);
            return isChecked ? ['checked'] : [];
        };

        self.resetBtnAction = function () {
            self.customsModel.businessUnitVal('');
            self.customsModel.purchaseOrderVal('');
            self.customsModel.requesterVal('');
            self.customsModel.EnterByVal('');
            self.customsModel.requisitionVal('');
            self.tableData([]);
            self.customsModel.rowSelectedArr([])
            self.customsModel.receiveDisable(true);
        };

        //-------------------------Translations  Action----------------------------
        var getTranslation = oj.Translations.getTranslatedString;

        function initTranslations() {
            self.label({
                requesterLbl: getTranslation("receiptScreen.requesterLbl"),
                enteredByLbl: getTranslation("receiptScreen.enteredByLbl"),
                requisitionLbl: getTranslation("receiptScreen.requisitionLbl"),
                businessUnitLbl: getTranslation("receiptScreen.businessUnitLbl"),
                POLbl: getTranslation("receiptScreen.POLbl"),
                searchLbl: getTranslation("common.searchLbl"),
                resetLbl: getTranslation("common.resetLbl"),
                receiveLbl: getTranslation("receiptScreen.receiveLbl"),
                receivingScreenLbl: getTranslation("receiptScreen.receivingScreenLbl"),
                selectPlaceholder: getTranslation("common.selectPlaceholder"),
                itemDescriptionLbl: getTranslation("receiptScreen.itemDescriptionLbl"),
                supplierName: getTranslation("receiptScreen.supplierName"),
                needByDate: getTranslation("receiptScreen.needByDate"),
                qtyDelivered: getTranslation("receiptScreen.qtyDelivered"),
                qtyOrdered: getTranslation("receiptScreen.qtyOrdered"),
                uom: getTranslation("receiptScreen.uom"),
                currencyLbl: getTranslation("receiptScreen.currencyLbl"),
                avilableLbl: getTranslation("receiptScreen.avilableLbl"),
                POlineNum: getTranslation("receiptScreen.POlineNum"),
            });
            self.columnArray([
                {
                    "headerText": "Select",
                    "template": 'checkTemplate'
                },
                {
                    "headerText": "NO.",
                    "template": 'seqTemplate'
                },
                {
                    "headerText": self.label().businessUnitLbl, "field": "BU_NAME"
                },
                {
                    "headerText": self.label().requisitionLbl, "field": "REQUISITION_NUMBER"
                },

                {
                    "headerText": self.label().itemDescriptionLbl, "field": "ITEM_DESCRIPTION"
                },

                {
                    "headerText": self.label().supplierName, "field": "VENDOR_NAME"
                },
                {
                    "headerText": self.label().needByDate, "field": "NEED_BY_DATE"
                },
                {
                    "headerText": self.label().qtyDelivered, "field": "QUANTITY_DELIVERED"
                },
                {
                    "headerText": self.label().qtyOrdered, "field": "QUANTITY_ORDERED"
                },
                {
                    "headerText": self.label().uom, "field": "UNIT_OF_MEASURE"
                },
                {
                    "headerText": self.label().currencyLbl, "field": "CURRENCY_CODE"
                },
                {
                    "headerText": self.label().avilableLbl, "field": "AVILABLE"
                },
                ,
                {
                    "headerText": self.label().POLbl, "field": "SEGMENT1"
                },
                {
                    "headerText": self.label().POlineNum, "field": "LINE_NUMBER"
                },
            ])

        }
    }

    return receiptHomePageContentViewModel;
});
