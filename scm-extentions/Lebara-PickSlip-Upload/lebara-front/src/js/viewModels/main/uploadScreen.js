/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * uploadScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'util/commonhelper', 'appController', 'xlsx', 'ojs/ojknockout', 'ojs/ojbutton',
    'ojs/ojmessages', 'ojs/ojinputtext', 'ojs/ojfilepicker', 'ojs/ojdialog'
], function (oj, ko, $, services, commonhelper, app) {
    /**
     * The view model for the main content view template
     */
    function uploadScreenContentViewModel() {
        var self = this;
        var selected_file = null;
        var getTranslation = oj.Translations.getTranslatedString;
        self.labels = ko.observableArray([]);
        self.messages = ko.observableArray();
        self.messagesDataprovider = new oj.ArrayDataProvider(self.messages);
        self.acceptArr = ko.observableArray([]);
        self.acceptArr = ko.pureComputed(function () {
            var accept = ".xls,.xlsx";
            return accept ? accept.split(",") : [];
        }, self);
        self.excelData = ko.observableArray();
        self.dialogMsg = ko.observable();
        self.messagesData = ko.observableArray();
        self.status = ko.observable();
        self.fileDisabled = ko.observable(false);
        self.btnDisabled = ko.observable(false);
        var timeCount = 0;

        self.selectListener = function (event) {
            var files = event.detail.files;
            var reader = new FileReader();
            selected_file = files[0];
            self.labels.picker_text(selected_file.name);
            var dataString;
            self.excelData([]);
            reader.onload = function (e) {
                var data = e.target.result;
                var workbook = XLSX.read(data, { type: 'binary' });
                var data2 = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
                console.log(data2);
                for (var i = 0; i < data2.length; i++) {
                    self.excelData.push(data2[i]);
                }
            };
            reader.onerror = function (ex) {
            };
            reader.readAsBinaryString(files[0]);
        }
        self.uploadAction = function (event) {
            self.fileDisabled(true);
            self.btnDisabled(true);
            self.labels.picker_text(getTranslation("uploadScreen.uploadingandprocessingdata"));
            if (selected_file != null) {
                self.messagesData([]);
                document.getElementById("demo1").innerHTML = "";
                console.log(self.excelData());
                var success = function (data) {
                    if (data.status == "Done" || data.message == "Gateway Time-out") {
                        self.messagesData.push(
                            {
                                message: 'Pick confirmed Successfully',
                                status:'Done' //data.status
                            });
                    } else {
                        self.messagesData.push(
                            {
                                message: data.message,
                                status: data.status
                            });
                    }
                    if (self.messagesData().length == self.excelData().length) {
                        var target = document.getElementById("demo1");
                        for(var i=0 ; i<self.messagesData().length ; i++){
                            if (self.messagesData()[i].status == 'Done') {
                                var oImg = document.createElement("img");
                                oImg.setAttribute('src', 'css/images/success.png');
                                oImg.setAttribute('height', '15px');
                                oImg.setAttribute('width', '15px');
                                var target = document.getElementById("demo1");
                                var node = document.createElement("LI");                 // Create a <li> node
                                var textnode = document.createTextNode(' '+self.messagesData()[i].message);         // Create a text node
                                node.append(oImg);
                                node.appendChild(textnode);
                                target.appendChild(node);
                            } else {
                                var oImg = document.createElement("img");
                                oImg.setAttribute('src', 'css/images/error.png');
                                oImg.setAttribute('height', '15px');
                                oImg.setAttribute('width', '15px');
                                var target = document.getElementById("demo1");
                                var node = document.createElement("LI");                 // Create a <li> node
                                var textnode = document.createTextNode(' '+self.messagesData()[i].message);         // Create a text node
                                node.append(oImg);
                                node.appendChild(textnode);
                                target.appendChild(node);
                            }

                        }
                        setTimeout(function () {
                            document.getElementById("messageDialog1").open();
                            self.labels.picker_text(getTranslation("uploadScreen.click_or_dragdrop"));
                            self.fileDisabled(false);
                            self.btnDisabled(false);
                        },self.messagesData().length * 300000);
                    }
                };
                var fail = function (data) {
                    console.log('fail  ' + data);
                };
                for (var i = 0; i < self.excelData().length; i++) {
                    console.log(self.excelData()[i]);
                    services.addGeneric("pick/upload", JSON.stringify(self.excelData()[i]), {}).then(success, fail);
                }
            } else {
                self.labels.picker_text(getTranslation("uploadScreen.click_or_dragdrop"));
                self.fileDisabled(false);
                self.btnDisabled(false);
                self.messages.push({ severity: "error", summary: getTranslation("uploadScreen.upload_file_not_selected"), autoTimeout: 0 });//default time out
            }
        };

        // for translation
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });


        self.connected = function () {
            initTranslations();
            app.loading(false);

        };

        self.closeMessageDialog = function () {
            document.getElementById("messageDialog1").close();
        };
        self.closeMessageDialog2 = function () {
            document.getElementById("messageDialog2").close();
        };

        function initTranslations() {
            self.labels = {
                title: ko.observable(getTranslation('uploadScreen.title')),
                upload: ko.observable(getTranslation('uploadScreen.upload')),
                picker_text: ko.observable(getTranslation('uploadScreen.click_or_dragdrop'))
            }
        }
    }

    return uploadScreenContentViewModel;
});
