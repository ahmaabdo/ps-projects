package common.restHelper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;


public class RestHelper {
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    private static RestHelper instance = null;

    public static RestHelper getInstance() {
        if (instance == null)
            instance = new RestHelper();
        return instance;
    }

    private String SecurityService =
        CommonConfigReader.getValue("SecurityService");
    private String InstanceUrl = CommonConfigReader.getValue("InstanceUrl");

    private String biReportUrl = CommonConfigReader.getValue("biReportUrl");

    public String protocol = "https";
    private String instanceName = CommonConfigReader.getValue("instanceName");

    public static String USER_NAME = CommonConfigReader.getValue("USER_NAME");
    public static String PASSWORD = CommonConfigReader.getValue("PASSWORD");

    public final static String SAAS_URL =
        "https://" + CommonConfigReader.getValue("instanceName");
    public final static String STATIC_INSTANCE_NAME =
        "embf-dev1.fa.em3.oraclecloud.com";
    private String pickURLSoap = "/fscmService/PickConfirmService";
    private String pickURLRest ="/fscmRestApi/resources/11.13.18.05/pickTransactions";

    public String getInstanceName() {
        return instanceName;
    }

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }

    public static String getAuth() {
        byte[] message = (USER_NAME + ":" + PASSWORD).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String httpPostUrl(String hostUrl, String data) {
        JSONObject obj = new JSONObject();
        String returnedString = "";
        try {
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url =
                new URL(null, hostUrl, new sun.net.www.protocol.https.Handler());
            System.out.println("URL --> " + url);
            System.out.println(data);
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setDoOutput(true);
            http.setRequestMethod("POST");
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("Accept", "application/json");
            http.setConnectTimeout(3000000);
            try (OutputStream os = http.getOutputStream()) {
                byte[] input = data.toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());
            if (http.getResponseCode() == 200 ||
                http.getResponseCode() == 201) {
                BufferedReader in =
                    new BufferedReader(new InputStreamReader(http.getInputStream(),
                                                             "UTF-8"));
                StringBuffer response = new StringBuffer();
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                returnedString = response.toString();
                System.out.println(returnedString);
                in.close();
            } else {
                obj.put("ReturnStatus", "Error");
                obj.put("ErrorExplanation", http.getResponseMessage());
                returnedString = obj.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            obj.put("ReturnStatus", "Error");
            obj.put("ErrorExplanation ",
                    "Error in connecting to the server please try again later");
            returnedString = obj.toString();
        } catch (IOException e) {
            e.printStackTrace();
            obj.put("ReturnStatus", "Error");
            obj.put("ErrorExplanation ",
                    "Error in sending data to the server");
            returnedString = obj.toString();
        }
        return returnedString;
    }


    public Document httpPost(String destUrl, String postData) {

        System.out.println("************* soap data *************");
        System.out.println(postData);
        try {
            System.setProperty("DUseSunHttpHandler", "true");
            byte[] buffer = new byte[postData.length()];
            buffer = postData.getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url =
                new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
            System.out.println("RestHelperdesUrl  " + destUrl);
            java.net.HttpURLConnection http;

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }

            String SOAPAction = "";
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());
            InputStream in;
            if (http.getResponseCode() == 200 ||
                http.getResponseCode() == 201) {
                in = http.getInputStream();
            } else {
                in = http.getErrorStream();
            }
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));
            Document doc =
                builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));

            XML.toJSONObject(response).toString();
            System.out.println("response " + response);
            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }


    public void setSecurityService(String SecurityService) {
        this.SecurityService = SecurityService;
    }

    public String getSecurityService() {
        return SecurityService;
    }

    public static String getSTATIC_INSTANCE_NAME() {
        return STATIC_INSTANCE_NAME;
    }

    public String getPickURLSoap() {
        return pickURLSoap;
    }

    public String getPickURLRest() {
        return pickURLRest;
    }
}
