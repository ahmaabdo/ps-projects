package com.appspro.lebara.rest;

import common.Soap.CreatePickSoap;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("pick")

public class CreatePicktRest extends CreatePickSoap {


    @POST
    @Path("/upload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response upload(String body) {
        try {
//            System.out.println(new JSONArray(body).toString());
            JSONObject res =new CreatePickSoap().createExcelPick(body);
            return Response.ok(res.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ok("-1").build();
        }
    }
}
