package com.appspro.biReport;


import com.appspro.db.CommonConfigReader;

import com.appspro.restHelper.RestHelper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;


/**
 *
 * @author CPBSLV
 */
public class BIReportModel extends RestHelper {

   RestHelper helper=new RestHelper();

    public static enum ATTRIBUTE_TEMPLATE {

        ATTRIBUTE_TEMPLATE_HTML("html"),
        ATTRIBUTE_TEMPLATE_XML("xml"),
        ATTRIBUTE_TEMPLATE_PDF("PDF");
        private String value;

        private ATTRIBUTE_TEMPLATE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum ATTRIBUTE_FORMAT {

        ATTRIBUTE_FORMAT_XML("xml"),
        ATTRIBUTE_FORMAT_HTML("html"),
        ATTRIBUTE_FORMAT_PDF("pdf"); //keep in small caps
        private String value;

        private ATTRIBUTE_FORMAT(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    private String reportAbsolutePath;
    private String reportPath;
    private String attributeFormat;
    private String attributeTemplate;
    private String parameters;
    private Map<String, String> paramMap = new HashMap<String, String>();
    private String soapRequest = null;
    
    private String username = helper.getUserName();
    private String password = helper.getPassword();

    public void setParamMap(Map<String, String> paramMap) {
        this.paramMap = paramMap;
    }

    public Map<String, String> getParamMap() {
        return paramMap;
    }

    public void setReportAbsolutePath(String reportAbsolutePath) {
        this.reportAbsolutePath = reportAbsolutePath;
    }

    public String getReportAbsolutePath() {
        return reportAbsolutePath;
    }
     public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public String getReportPath() {
        return reportPath;
    }
    
    public void setAttributeFormat(String attributeFormat) {
        this.attributeFormat = attributeFormat;
    }

    public void setAttributeTemplate(String attributeTemplate) {
        this.attributeTemplate = attributeTemplate;
    }

    public String getParameters() {
        Iterator it = paramMap.entrySet().iterator();
        parameters = "";
        while (it.hasNext()) {
            Map.Entry parameter = (Map.Entry)it.next();
            System.out.println(parameter.getKey() + ">>>>>>>." +parameter.getValue());
            parameters = parameters + "<pub:item><pub:name>" + parameter.getKey() +
                    "</pub:name><pub:values><pub:item>" + parameter.getValue() +
                    "</pub:item></pub:values></pub:item>\n";
        }
        return parameters;
    }

    public String getSoapRequest() {
        
        this.soapRequest =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pub=\"http://xmlns.oracle.com/oxp/service/PublicReportService\">\n" +
                "<soapenv:Header/>\n" +
                "<soapenv:Body>\n" +
                "<pub:runReport>\n" +
                "<pub:reportRequest>\n" +
                "<pub:attributeLocale>en-US</pub:attributeLocale>\n" +
                "<pub:attributeTemplate>" +
                this.attributeTemplate + "</pub:attributeTemplate>\n" +
                "<pub:attributeFormat>" + this.attributeFormat +
                "</pub:attributeFormat><pub:flattenXML>false</pub:flattenXML>\n" +
                "<pub:reportAbsolutePath>/Custom/SAAS Extension Reports/Reports/" + 
                reportAbsolutePath + ".xdo</pub:reportAbsolutePath>\n" +
                "<pub:parameterNameValues>\n" + getParameters() + "</pub:parameterNameValues>\n" +
                "</pub:reportRequest>\n" +
                "<pub:userID>" + this.username+ "</pub:userID>\n" +
                "<pub:password>" + this.password +"</pub:password>\n" +
                "</pub:runReport>\n" +
                "</soapenv:Body>\n" +
                "</soapenv:Envelope>";
        return soapRequest;
    }

    public String executeReports() {
        String output = "";
        try {
            System.setProperty("DUseSunHttpHandler", "true");
            System.setProperty("https.protocols", "TLSv1.2");

            byte[] buffer = new byte[getSoapRequest().length()];
            buffer = getSoapRequest().getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL domain = new URL(getInstanceUrl());
            java.net.URL url = new URL(null, domain + this.getBiReportUrl(), new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https = (HttpsURLConnection)url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";
            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setConnectTimeout(3600000);
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            InputStream is = http.getInputStream();
            String responseXML = getStringFromInputStream(is );

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(responseXML ));

            Document doc = builder.parse(src);
            String data = doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            if (BIReportModel.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
                output = StringUtils.newStringUtf8(Base64.decodeBase64(data));
            } else {
                output = data;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
   
    public static String runReport(String reportName, String reportPath,Map<String, String> paramMap ) {
        BIReportModel biPReports = new BIReportModel();
        biPReports.setAttributeFormat(BIReportModel.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIReportModel.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
        biPReports.setReportAbsolutePath(reportName);
        biPReports.setReportPath(reportPath);

        biPReports.setParamMap(paramMap);

        String fresponse = biPReports.executeReports();

        return fresponse;
    }

}

