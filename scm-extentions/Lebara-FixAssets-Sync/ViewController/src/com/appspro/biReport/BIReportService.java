package com.appspro.biReport;


import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.json.XML;

public class BIReportService {
    
    
    public JSONObject runReportXML(String reportName){
        String fresponse ="";
        BIPReports biPReports = new BIPReports();
        JSONObject responseObject = new JSONObject();
        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
        biPReports.setReportAbsolutePath(reportName);
        fresponse = biPReports.executeReports();
        JSONObject xmlJSONObj = XML.toJSONObject(fresponse.toString());

        if (!xmlJSONObj.isNull("DATA_DS") && !xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1"))
        {
            if (xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) 
            {                 
                 return responseObject;

            } else 
            {
                responseObject = xmlJSONObj.getJSONObject("DATA_DS");
            }
        }
        return responseObject;
    }
    
    public JSONObject inventoryItemReport () {
        String reportName = "assetInventoryItemReport";
        JSONObject obj =runReportXML(reportName);
        return obj;
    }
    
    public JSONObject assetNumberReport () {
        String reportName = "assetNumberValueSetReport";
        JSONObject obj =runReportXML(reportName);
        return obj;
    }
    
    public JSONObject salesOrderNumberReport () {
        String reportName = "salesOrderAssetNumReport";
        JSONObject obj =runReportXML(reportName);
        return obj;
    }
    
    public static void main(String[] args) {
        JSONObject obj=new JSONObject();
        BIReportService ser = new BIReportService ();
        obj= ser.inventoryItemReport();
        System.out.println(obj.toString());
       
    }
}
