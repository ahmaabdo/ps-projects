package com.appspro.lebara.sync;

import com.appspro.biReport.BIReportService;
import com.appspro.db.CommonConfigReader;
import com.appspro.lebara.bean.AssetInventoryBean;
import com.appspro.lebara.dao.AssetInventoryDAO;
import com.appspro.lebara.createExcelAndUpload.CreateExcelFile;
import com.appspro.restHelper.RestHelper;
import java.io.IOException;
import java.io.StringWriter;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.ArrayList;
import org.apache.commons.codec.binary.Base64;
import com.appspro.lebara.createExcelAndUpload.uploadFile;


public class InventoryMassAdditional extends RestHelper {

    RestHelper helper = new RestHelper();
    CreateExcelFile excel=new CreateExcelFile();
    uploadFile upload=new uploadFile();

    public JSONArray getAllInventoryItems (){
        JSONArray respArr = new JSONArray();
        JSONArray arr = new JSONArray();
        ArrayList<AssetInventoryBean> assetList = new ArrayList<AssetInventoryBean>();
        BIReportService report = new BIReportService();
        AssetInventoryDAO inventoryDao = new AssetInventoryDAO();
        JSONObject objectData= report.inventoryItemReport();
        ArrayList<JSONObject> respList = new ArrayList<JSONObject>();        
        if(objectData.has("G_1")){
            arr = objectData.getJSONArray("G_1");
        }
        // get saved items in DB
        assetList =  inventoryDao.getAllItemsByDepartment();
        if(assetList.size() == 0){
            respArr.put(arr.toString());
        }else{
            for (int i = 0 ; i < arr.length() ; i++){
                JSONObject obj = arr.getJSONObject(i);
                if(!checkItemExistance(assetList,obj)){
                    respList.add(obj);
                }
            }
        }
        respArr.put(respList);
        
        System.out.println(" new Items to Add = " + respList.size());

        return new JSONArray(respArr.get(0).toString());   
    }
    
    public boolean checkItemExistance (ArrayList<AssetInventoryBean> assetList, JSONObject obj){
        for(int j = 0 ; j < assetList.size() ; j++ ){
            AssetInventoryBean bean = assetList.get(j);
            if(((obj.get("INVENTORY_ITEM_ID").toString().equalsIgnoreCase(bean.getItemId())) &&
                (obj.get("ITEM_NUMBER").toString().equalsIgnoreCase(bean.getItemNumber())) &&
                (obj.get("ORGANIZATION_ID").toString().equalsIgnoreCase(bean.getOrganizationId())) &&
                (obj.get("TRANSACTION_ID").toString().equalsIgnoreCase(bean.getTransactionId())))) {
                return true;
            }
        }
        return false;
    }

    public JSONObject createMassAdditionCSV(JSONArray inventoryArr) throws IOException{
        
        JSONObject obj = new JSONObject();
        StringWriter s = new StringWriter();

        // check if the instance is dev or test to get book type of asset
        String assetBookType = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            assetBookType = "LEBARA_FIFTH_BOOK";
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            assetBookType = "LEBARA_CORP_BOOK";
        }

        int x = 1;
        for (int i = 0; i < inventoryArr.length(); i++) {
            JSONObject rowobj = inventoryArr.getJSONObject(i);
            String ItemNumber =rowobj.get("ITEM_NUMBER").toString();  
            String itemDescription =rowobj.get("DESCRIPTION").toString();
            String poNumber =rowobj.get("SEGMENT1").toString();
            String vendorName =rowobj.get("VENDOR_NAME").toString();
            String vendorNumber =rowobj.get("VENDOR_NUMBER").toString();
            String TRANSACTION_QUANTITY = rowobj.get("TRANSACTION_QUANTITY").toString();
            String unitPrice=rowobj.get("UNIT_PRICE").toString();
//            String assessableValue =rowobj.get("ASSESSABLE_VALUE").toString();
            String assessableValue =String.valueOf(Float.parseFloat(unitPrice)*Float.parseFloat(TRANSACTION_QUANTITY));
//CAPITALIZED
            excel.writeLine(s,Arrays.asList(String.valueOf(x++),assetBookType,"","",ItemNumber+" - "+itemDescription,"","","","","CIP",assessableValue,"2020/01/01","",TRANSACTION_QUANTITY,"","","","","","","","NEW","","","","","","","","","","","","","","","","","","","","","","","","","","YES","","","","","","","","","","LEBARA STL","60","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",vendorName,vendorNumber,poNumber,"","","","","","","","","","","","","END"));
        }
        s.flush();
        s.close();

        byte[] bytes = s.toString().getBytes("UTF-8");

        obj.put("fileName", "FaMassAdditions.csv");
        obj.put("fileContent", s.toString());
        String encoded1 = Base64.encodeBase64String(bytes);
        return obj;
    }

    public JSONObject createMassDistributionsCSV(JSONArray inventoryArr) throws IOException {
        JSONObject obj = new JSONObject();
        StringWriter s = new StringWriter();
        int x = 1;
        for (int i = 0; i < inventoryArr.length(); i++) {
            JSONObject rowobj = inventoryArr.getJSONObject(i);
            String TRANSACTION_QUANTITY = rowobj.get("TRANSACTION_QUANTITY").toString();        
            excel.writeLine(s,Arrays.asList(String.valueOf(x++),TRANSACTION_QUANTITY,"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","END"));
        }

        s.flush();
        s.close();

        byte[] bytes = s.toString().getBytes("UTF-8");
        obj.put("fileName", "FaMassaddDistributions.csv");
        obj.put("fileContent", s.toString());
        String fileEncode = Base64.encodeBase64String(bytes);

        return obj;
    }

    public JSONObject createMassRatesCSV() throws IOException {
        JSONObject obj = new JSONObject();
        StringWriter s = new StringWriter();
        excel.writeLine(s, Arrays.asList(""));

        s.flush();
        s.close();

        byte[] bytes = s.toString().getBytes("UTF-8");
        obj.put("fileName", "FaMcMassRates.csv");
        obj.put("fileContent", s.toString());
        String fileEncode = Base64.encodeBase64String(bytes);

        return obj;
    }

    public void runInventoryAsset (){
        InventoryMassAdditional inv = new InventoryMassAdditional();
        ArrayList<JSONObject> zipAllFiles = new ArrayList<JSONObject>();
        JSONArray insertInventoryArr = new JSONArray();
        try {
            insertInventoryArr = inv.getAllInventoryItems();
            if(insertInventoryArr.length() != 0){
                
                zipAllFiles.add(inv.createMassAdditionCSV(insertInventoryArr));
                zipAllFiles.add(inv.createMassDistributionsCSV(insertInventoryArr));

                byte[] bytes = excel.zipBytes(zipAllFiles);
                String fileEncode = Base64.encodeBase64String(bytes);
                            
                upload.uploadFileToUCM(fileEncode,insertInventoryArr,"FaMassAddition.zip");
            }
            
        } catch (IOException e) {
        } catch (Exception e) {
        }
    
    }

    public static void main(String[] args) {

       new InventoryMassAdditional().runInventoryAsset();
       
    }
}
