package com.appspro.lebara.sync;

import com.appspro.biReport.BIReportService;
import com.appspro.lebara.bean.AssetInventoryBean;
import com.appspro.lebara.bean.SalesOrderBean;
import com.appspro.lebara.createExcelAndUpload.CreateExcelFile;
import com.appspro.lebara.createExcelAndUpload.uploadFile;
import com.appspro.lebara.dao.AssetInventoryDAO;
import com.appspro.lebara.dao.SalesOrderDAO;

import com.appspro.restHelper.RestHelper;

import com.fasterxml.jackson.databind.ser.std.DateSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import java.text.SimpleDateFormat;  

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;

public class AssetSalesOrder extends RestHelper{
    
    RestHelper rs = new RestHelper();
    CreateExcelFile excel=new CreateExcelFile();
    uploadFile upload=new uploadFile();
                                 
    public JSONArray getAllSalesOrder(){
        JSONArray respArr = new JSONArray();
        JSONArray arr = new JSONArray();
        ArrayList<SalesOrderBean> salesOrderList = new ArrayList<SalesOrderBean>();
        BIReportService ser = new BIReportService();
        SalesOrderDAO salesDao = new SalesOrderDAO();
        JSONObject objectData= ser.salesOrderNumberReport();
        ArrayList<JSONObject> respList = new ArrayList<JSONObject>();        
        if(objectData.has("G_1")){
            if(objectData.get("G_1") instanceof JSONObject){
                arr.put(objectData.get("G_1"));
            }
            else{
                arr = objectData.getJSONArray("G_1");
            }
        }
        salesOrderList =  salesDao.getAllSalesOrder();
        if(salesOrderList.size() == 0){
            respArr.put(arr.toString());
        }else{
            for (int i = 0 ; i < arr.length() ; i++){
                JSONObject obj = arr.getJSONObject(i);
                if(!checkItemExistance(salesOrderList,obj)){
                    respList.add(obj);
                }
            }
        }
        
        respArr.put(respList);
        System.out.println("new Items to add = "+respList.size());
        return new JSONArray(respArr.get(0).toString());   
    }
    
    public boolean checkItemExistance (ArrayList<SalesOrderBean> assetList, JSONObject obj){
        for(int j = 0 ; j < assetList.size() ; j++ ){
            SalesOrderBean bean = assetList.get(j);
            if(((obj.get("ITEM_NUMBER").toString().equalsIgnoreCase(bean.getItemNumber())) &&
                (obj.get("ORGANIZATION_ID").toString().equalsIgnoreCase(bean.getOrganizationId())) &&
                (obj.get("SALES_ORDER_NUMBER").toString().equalsIgnoreCase(bean.getSalesOrderNumber())) &&
                (obj.get("ATTRIBUTE_CHAR1").toString().equalsIgnoreCase(bean.getAssetNumber())))) {
                return true;
            }
        }
        return false;
    }
    
    public JSONObject faTransferCSV(JSONArray salesArr) throws IOException {
        AssetSalesOrder ob = new AssetSalesOrder();
        JSONObject obj = new JSONObject();
        StringWriter s = new StringWriter();
        int x = 1;
        for (int i = 0; i < salesArr.length(); i++) {
            JSONObject rowobj = salesArr.getJSONObject(i);
            String assetNumber = rowobj.get("ATTRIBUTE_CHAR1").toString();
            String bookTypeCode = rowobj.get("BOOK_TYPE_CODE").toString();
            excel.writeLine(s,Arrays.asList("ORACLE TRANSFER",bookTypeCode,assetNumber,"TRANSFER","POST","transfer","2020/01/01","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","END"));
        }

        s.flush();
        s.close();

        byte[] bytes = s.toString().getBytes("UTF-8");

        obj.put("fileName", "FaTransfersT.csv");
        obj.put("fileContent", s.toString());
        String encoded1 = Base64.encodeBase64String(bytes);

        return obj;
    }
    
    public JSONObject faTransferDistCSV(JSONArray salesArr) throws IOException {
        JSONObject obj = new JSONObject();
        StringWriter s = new StringWriter();
        int x = 1;
        for (int i = 0; i < salesArr.length(); i++) {
            JSONObject rowobj = salesArr.getJSONObject(i);
            String assetNumber = rowobj.get("ATTRIBUTE_CHAR1").toString();
            String LoSegment1 = rowobj.get("LO_SEGMENT1").toString();
            String LoSegment2 = rowobj.get("LO_SEGMENT2").toString();
            String LoSegment3 = rowobj.get("LO_SEGMENT3").toString();
            String LoSegment4 = rowobj.get("LO_SEGMENT4").toString();
            String bookTypeCode = rowobj.get("BOOK_TYPE_CODE").toString(); 
            String glSegment1 = rowobj.get("GL_SEGMENT1").toString();
            String[] glSegment1Parts = glSegment1.split("_");
            String expSegment1 = glSegment1Parts[0];
            String glSegment2 = rowobj.get("GL_SEGMENT2").toString();
            String[] glSegment2Parts = glSegment2.split("_");
            String expSegment2 = glSegment2Parts[0];
            String glSegment3 = rowobj.get("GL_SEGMENT3").toString();
            String[] glSegment3Parts = glSegment3.split("_");
            String expSegment3 = glSegment3Parts[0];
            String glSegment4 = rowobj.get("GL_SEGMENT4").toString();
            String[] glSegment4Parts = glSegment4.split("_");
            String expSegment4 = glSegment4Parts[0];
            String glSegment5 = rowobj.get("GL_SEGMENT5").toString();
            String[] glSegment5Parts = glSegment5.split("_");
            String expSegment5 = glSegment5Parts[0];
            String glSegment6 = rowobj.get("GL_SEGMENT6").toString();
            String[] glSegment6Parts = glSegment6.split("_");
            String expSegment6 = glSegment6Parts[0];
            String glSegment7 = rowobj.get("GL_SEGMENT7").toString();
            String[] glSegment7Parts = glSegment7.split("_");
            String expSegment7 = glSegment7Parts[0];
            String glSegment8 = rowobj.get("GL_SEGMENT8").toString();
            String[] glSegment8Parts = glSegment8.split("_");
            String expSegment8 = glSegment8Parts[0];
            String employeeName = "noMail".equals(rowobj.get("EMAIL").toString())?"":rowobj.get("EMAIL").toString();
            String transactionQuantity = rowobj.get("CURRENT_UNITS").toString();
            excel.writeLine(s,Arrays.asList("ORACLE TRANSFER",bookTypeCode,assetNumber,"TRANSFER","-"+transactionQuantity,"",LoSegment1,LoSegment2,LoSegment3,LoSegment4,"","","",expSegment1,expSegment2,expSegment3,expSegment4,expSegment5,expSegment6,expSegment7,expSegment8,"","","","","","","","","","","","","","","","","","","","","","","END"));
            excel.writeLine(s,Arrays.asList("ORACLE TRANSFER",bookTypeCode,assetNumber,"TRANSFER",transactionQuantity,employeeName,"","","","","","","",expSegment1,expSegment2,expSegment3,expSegment4,expSegment5,expSegment6,expSegment7,expSegment8,"","","","","","","","","","","","","","","","","","","","","","","END"));     
            }
//      xxhossam.ragaban@xxx.com
        s.flush();
        s.close();

        byte[] bytes = s.toString().getBytes("UTF-8");

        obj.put("fileName", "FaTransferDistsT.csv");
        obj.put("fileContent", s.toString());
        String encoded1 = Base64.encodeBase64String(bytes);
        return obj;
    }
    
    public JSONObject FaAdjustmentsT(JSONArray salesArr) throws IOException {
        JSONObject obj = new JSONObject();
        StringWriter s = new StringWriter();
        int x = 1;
        for (int i = 0; i < salesArr.length(); i++) {
            JSONObject rowobj = salesArr.getJSONObject(i);
            String assetNumber = rowobj.get("ATTRIBUTE_CHAR1").toString();
            String bookTypeCode = rowobj.get("BOOK_TYPE_CODE").toString();
            String servicesDate = rowobj.get("TRANSACTION_DATE").toString();
            String assetType=rowobj.get("ASSET_TYPE").toString();
            if("CIP".equals(assetType)){
                excel.writeLine(s,Arrays.asList("ADJUSTMENT",bookTypeCode,assetNumber,"ADJUSTMENT","POST","","","","",servicesDate,"","YES","","","","","","","","","","LEBARA STL","60","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","CAPITALIZED","END"));  
            }
        }
        s.flush();
        s.close();

        byte[] bytes = s.toString().getBytes("UTF-8");

        obj.put("fileName", "FaAdjustmentsT.csv");
        obj.put("fileContent", s.toString());
        String encoded1 = Base64.encodeBase64String(bytes);

        return obj;
    } 
          
    public JSONObject FaAdjSrcLinesT(JSONArray salesArr) throws IOException {
        JSONObject obj = new JSONObject();
        StringWriter s = new StringWriter();
        int x = 1;
        for (int i = 0; i < salesArr.length(); i++) {
            JSONObject rowobj = salesArr.getJSONObject(i);
            String assetNumber = rowobj.get("ATTRIBUTE_CHAR1").toString();
            String bookTypeCode = rowobj.get("BOOK_TYPE_CODE").toString();
            excel.writeLine(s,Arrays.asList("BMRX",bookTypeCode,assetNumber,"INVOICE ADDITION","","MACHINERY,ORACLE PAYABLES","-2500.00","7500.00","","","","","","","","","","","","","","","","","","","","","","","","","","","","","01","000","1570","0000","000","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","END"));
        }

        s.flush();
        s.close();

        byte[] bytes = s.toString().getBytes("UTF-8");

        obj.put("fileName", "FaAdjSrcLinesT.csv");
        obj.put("fileContent", s.toString());
        String encoded1 = Base64.encodeBase64String(bytes);
        return obj;
    }
    
    public void runSalesAsset () {
        AssetSalesOrder aso = new AssetSalesOrder();
        ArrayList<JSONObject> zipAllFiles = new ArrayList<JSONObject>();
        ArrayList<JSONObject> adjZipAllFiles = new ArrayList<JSONObject>();
        JSONArray insertsalesOrderArr = new JSONArray();

        try {
            insertsalesOrderArr = aso.getAllSalesOrder();
            if(insertsalesOrderArr.length() != 0) {
                zipAllFiles.add(aso.faTransferCSV(insertsalesOrderArr));
                zipAllFiles.add(aso.faTransferDistCSV(insertsalesOrderArr));

                byte[] bytes = excel.zipBytes(zipAllFiles);
                String fileEncode = Base64.encodeBase64String(bytes);
                    
                upload.uploadFileToUCM(fileEncode,insertsalesOrderArr,"FaMassTransfer.zip");
                
                adjZipAllFiles.add(aso.FaAdjustmentsT(insertsalesOrderArr));
                byte[] adjBytes = excel.zipBytes(adjZipAllFiles);
                String adjFileEncode = Base64.encodeBase64String(adjBytes);
                
                upload.uploadFileToUCM(adjFileEncode,insertsalesOrderArr,"FaMassAdjustment.zip");
            }
        }
        catch (IOException e) {
        }
        catch (Exception e) {
        }
    }
    public static void main(String[] args) {

        AssetSalesOrder ob = new AssetSalesOrder();
        
        ob.runSalesAsset();
    }

}
