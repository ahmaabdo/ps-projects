package com.appspro.lebara.sync;

import com.appspro.lebara.job.FixAssetJobScheduler;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class Listener1 implements ServletContextListener,
                                  ServletContextAttributeListener,
                                  HttpSessionListener,
                                  HttpSessionAttributeListener {
    private ServletContext context = null;
    private String name = null;
    private Object value = null;
    private HttpSession session = null;

    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
    }

    public void contextDestroyed(ServletContextEvent event) {
//        context = event.getServletContext();
        System.out.println("Context Destroyed");
        FixAssetJobScheduler valueSetJobScheduler = new FixAssetJobScheduler();
        
        valueSetJobScheduler.stopScheduler();
        
    }

    public void attributeAdded(ServletContextAttributeEvent event) {
        name  = event.getName();
        value = event.getValue();
    }

    public void attributeRemoved(ServletContextAttributeEvent event) {
        name  = event.getName();
        value = event.getValue();
    }

    public void attributeReplaced(ServletContextAttributeEvent event) {
        name  = event.getName();
        value = event.getValue();
    }

    public void sessionCreated(HttpSessionEvent event) {
        session  = event.getSession();
    }

    public void sessionDestroyed(HttpSessionEvent event) {
        session  = event.getSession();
    }

    public void attributeAdded(HttpSessionBindingEvent event) {
        name  = event.getName();
        session = event.getSession();
    }

    public void attributeRemoved(HttpSessionBindingEvent event) {
        name  = event.getName();
        session = event.getSession();
    }

    public void attributeReplaced(HttpSessionBindingEvent event) {
        name  = event.getName();
        session = event.getSession();
    }
}
