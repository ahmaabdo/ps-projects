package com.appspro.lebara.valueSets;

import com.appspro.lebara.sync.InventoryMassAdditional;
import com.appspro.restHelper.RestHelper;

import java.io.IOException;

import java.net.MalformedURLException;

import org.json.JSONObject;

public class ValueSets {
    
    RestHelper helper=new RestHelper();
    
    private final String MainValueSetId="Asset%20Number%20and%20Description%20Value%20Set";
    
    public void DeleteValueSet(String assetNumber){
        
        String valueSetUrl=helper.getInstanceUrl()+helper.getValueSetsUrl()+MainValueSetId+"/child/values?q=Value="+assetNumber;
        
        JSONObject response;
        JSONObject editResponse;
        try {
            response = helper.httpGet(valueSetUrl);
            JSONObject dataObj=new JSONObject(response.get("data").toString()).getJSONArray("items").getJSONObject(0);
            String valueSetId=dataObj.get("ValueId").toString();
            System.out.println("valueId= "+valueSetId);
            
            String body = " {\n"
                    + "      \"Value\": \"" + assetNumber + "\",\n"
                    + "      \"EnabledFlag\": \"N\",\n"
                    + "      \"SummaryFlag\": \"N\",\n"
                    + "      \"DetailPostingAllowed\": \"N\",\n"
                    + "      \"DetailBudgetingAllowed\": \"N\"\n"
                    + " }";
            String editValueSetUrl=helper.getInstanceUrl()+helper.getValueSetsUrl()+MainValueSetId+"/child/values/"+valueSetId;
            editResponse=helper.httpPatch(editValueSetUrl,body);
            System.out.println("edit= "+editResponse);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {

       new ValueSets().DeleteValueSet("1000001");
       
    }
    
}
