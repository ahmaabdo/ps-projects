package com.appspro.lebara.createExcelAndUpload;

import com.appspro.lebara.dao.AssetInventoryDAO;
import com.appspro.lebara.dao.SalesOrderDAO;
import com.appspro.restHelper.RestHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;

public class uploadFile{
   
    RestHelper helper = new RestHelper();
    
    public JSONObject uploadFileToUCM(String fileContent,JSONArray arr ,String fileName) throws Exception {
            System.out.println("========= upload to ucm =========");
            JSONObject obj = new JSONObject();
            String destUrl = helper.getInstanceUrl()+helper.getFinanceUrl();
            String postData =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/types/\" xmlns:fin=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <typ:uploadFileToUcm>\n" +
                "         <typ:document>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:Content>" + fileContent + "</fin:Content>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:FileName>"+fileName+"</fin:FileName>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:ContentType>zip</fin:ContentType>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:DocumentTitle>Sample File1</fin:DocumentTitle>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:DocumentAuthor>PAAS Team</fin:DocumentAuthor>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:DocumentSecurityGroup>FAFusionImportExport</fin:DocumentSecurityGroup>\n" +
                "            <!--Optional:-->\n" +
                "            <fin:DocumentAccount>fin$/assets$/import$</fin:DocumentAccount>\n" +
                "         </typ:document>\n" +
                "      </typ:uploadFileToUcm>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>\n";

            Document doc = helper.httpPost(destUrl, postData);

            try {
                obj.put("result",doc.getElementsByTagName("result").item(0).getTextContent());
            } catch (NullPointerException e) {
                obj.put("result", "");
            }
            System.out.println("file name = "+fileName);
            
            int jobNumber;
            if(fileName.equalsIgnoreCase("FaMassTransfer.zip")) {
                 jobNumber = 11;
            }else if(fileName.equalsIgnoreCase("FaMassAddition.zip")){
                jobNumber = 9;
            }else{
                jobNumber = 12;
            }
            submitStatus(obj.getString("result"),arr,jobNumber);
            return obj;
        }
    
    public JSONObject submitStatus(String paramList,JSONArray receivedArr,int jobNumber) {
        System.out.println("========= submit status =========");
        JSONObject obj = new JSONObject();
        String destUrl =  helper.getInstanceUrl()+helper.getFinanceUrl();
        String postData =
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/types/\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <typ:submitESSJobRequest>\n" +
            "         <typ:jobPackageName>/oracle/apps/ess/financials/commonModules/shared/common/interfaceLoader</typ:jobPackageName>\n" +
            "         <typ:jobDefinitionName>InterfaceLoaderController</typ:jobDefinitionName>\n" +
            "         <!--Zero or more repetitions:-->\n" +
            "         <typ:paramList>"+jobNumber+"</typ:paramList>\n" +
            "         <typ:paramList>" + paramList + "</typ:paramList>\n" +
            "      </typ:submitESSJobRequest>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";
        
        try {
             Document doc = helper.httpPost(destUrl, postData);
        } catch (Exception e) {
        }
        
        
        
        if(jobNumber == 12){
            SalesOrderDAO salesDao = new SalesOrderDAO();
            salesDao.insertOrUpdateSalesOrder(receivedArr);
        } else if(jobNumber == 9){
            AssetInventoryDAO inventoryDao = new AssetInventoryDAO();
            inventoryDao.insertOrUpdateItems(receivedArr);
        }
        return obj;
    }
    
}
