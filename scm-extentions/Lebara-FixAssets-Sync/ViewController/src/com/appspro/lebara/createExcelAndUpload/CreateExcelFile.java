package com.appspro.lebara.createExcelAndUpload;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.json.JSONObject;

public class CreateExcelFile {
 
    private static final char DEFAULT_SEPARATOR = ',';
    
    private static String followCVSformat(String value) {
        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;
    }
 
    public void writeLine(StringWriter stringWriter, List<String> values, char separators,char customQuote) {
        boolean first = true;
        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String value : values) {
            if (!first) {
                stringBuilder.append(separators);
            }
            if (customQuote == ' ') {
                stringBuilder.append(followCVSformat(value));
            } else {
                stringBuilder.append(customQuote).append(followCVSformat(value)).append(customQuote);
            }
            first = false;
        }
        stringBuilder.append("\n");
        stringWriter.append(stringBuilder.toString());
    }
    
    public void writeLine(StringWriter w,List<String> values) throws IOException {
        writeLine(w, values, DEFAULT_SEPARATOR, ' ');
    }
    
    
    public static byte[] zipBytes(List<JSONObject> files) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);

        for (int i = 0; i < files.size(); i++) {
            JSONObject obj = files.get(i);
            byte[] bytes = obj.getString("fileContent").getBytes();
            ZipEntry entry = new ZipEntry(obj.getString("fileName"));
            entry.setSize(bytes.length);
            zos.putNextEntry(entry);
            zos.write(bytes);
        }
        zos.closeEntry();
        zos.close();

        return baos.toByteArray();
    }
    
}
