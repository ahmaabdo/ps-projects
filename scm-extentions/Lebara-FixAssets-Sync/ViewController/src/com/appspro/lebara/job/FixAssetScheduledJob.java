package com.appspro.lebara.job;

import com.appspro.lebara.sync.AssetSalesOrder;
import com.appspro.lebara.sync.InventoryMassAdditional;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 *
 * @author HP
 */
public class FixAssetScheduledJob implements Job {
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {

        AssetSalesOrder sales = new AssetSalesOrder();
        sales.runSalesAsset();
        
        InventoryMassAdditional inv = new InventoryMassAdditional();
        inv.runInventoryAsset();
        
        System.out.println("test");
    }
}
