package com.appspro.lebara.dao;

import com.appspro.db.AppsproConnection;

import com.appspro.lebara.bean.AssetInventoryBean;

import java.util.ArrayList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;


public class AssetInventoryDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    ResultSet rs;

    public ArrayList<AssetInventoryBean> getAllItemsByDepartment() {

        ArrayList<AssetInventoryBean> assetList = new ArrayList<AssetInventoryBean>();
        AssetInventoryBean bean = null;

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XX_ASSET_INVENTORY_ITEM";
            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new AssetInventoryBean();
                bean.setItemId(rs.getString("ITEM_ID"));
                bean.setOrganizationId(rs.getString("ORGANIZATION_ID"));
                bean.setItemNumber(rs.getString("ITEM_NUMBER"));
                bean.setTransactionId(rs.getString("TRANSACTION_ID"));
                assetList.add(bean);
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return assetList;
    }

    public String insertOrUpdateItems(JSONArray arr) {
        System.out.println("data to be inserted in DB = "+arr.toString());
        try {
            connection = AppsproConnection.getConnection();
            for(int i = 0 ; i < arr.length() ; i++) {
                JSONObject obj = arr.getJSONObject(i);
                String query = "INSERT INTO XX_ASSET_INVENTORY_ITEM (ITEM_ID,ORGANIZATION_ID,ITEM_NUMBER,TRANSACTION_ID) VALUES(?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, obj.get("INVENTORY_ITEM_ID").toString());
                ps.setString(2, obj.get("ORGANIZATION_ID").toString());
                ps.setString(3, obj.get("ITEM_NUMBER").toString());
                ps.setString(4, obj.get("TRANSACTION_ID").toString());
                ps.executeUpdate();
            }

        } catch (SQLException e) {
        }
        finally {
            closeResources(connection, ps, rs);
        }
        return "data saved";
    }
}
