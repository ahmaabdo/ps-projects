package com.appspro.lebara.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.lebara.bean.AssetInventoryBean;

import com.appspro.lebara.bean.SalesOrderBean;

import com.appspro.lebara.valueSets.ValueSets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class SalesOrderDAO extends AppsproConnection {
   
    Connection connection;
    PreparedStatement ps;
    ResultSet rs;

    public ArrayList<SalesOrderBean> getAllSalesOrder() {

        ArrayList<SalesOrderBean> assetList = new ArrayList<SalesOrderBean>();
        SalesOrderBean bean = null;

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XX_SALES_ORDER_ITEM";
            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new SalesOrderBean(); 
                bean.setItemNumber(rs.getString("ITEM_NUMBER"));
                bean.setOrganizationId(rs.getString("ORGANIZATION_ID"));
                bean.setAssetNumber(rs.getString("ASSET_NUMBER"));
                bean.setSalesOrderNumber(rs.getString("SALE_ORDER_NUMBER"));
                assetList.add(bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return assetList;
    }

    public String insertOrUpdateSalesOrder(JSONArray arr) {
        System.out.println("data to be inserted in DB = "+arr.toString());
        try {
            connection = AppsproConnection.getConnection();
            for(int i = 0 ; i < arr.length() ; i++) {
                JSONObject obj = arr.getJSONObject(i);
                String query = "INSERT INTO XX_SALES_ORDER_ITEM (ITEM_NUMBER,ORGANIZATION_ID,ASSET_NUMBER,SALE_ORDER_NUMBER) VALUES(?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, obj.get("ITEM_NUMBER").toString());
                ps.setString(2, obj.get("ORGANIZATION_ID").toString());
                ps.setString(3, obj.get("ATTRIBUTE_CHAR1").toString());
                ps.setString(4, obj.get("SALES_ORDER_NUMBER").toString());
                ps.executeUpdate();
                
                // delete this asset from lov in saas
                new ValueSets().DeleteValueSet(obj.get("ATTRIBUTE_CHAR1").toString()); 
            }

        } catch (SQLException e) {
        }
        finally {
            closeResources(connection, ps, rs);
        }
        return "data saved";
    }
}
