package com.appspro.restHelper;


import com.appspro.db.CommonConfigReader;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.cert.CertificateException;

import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONObject;

import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;

import sun.misc.BASE64Decoder;

public class RestHelper {

    private final String orgnizationUrl = "//hcmCoreSetupApi/resources/latest/organizations";
    private final String biReportUrl = "/xmlpserver/services/PublicReportService";
    private final String employeeServiceUrl =  "/hcmCoreApi/resources/latest/emps/";
    private final String getinvoices = "/fscmRestApi/resources/11.13.18.05/invoices";
    private final String financeUrl="/fscmService/FinancialUtilService";
    private final String valueSetsUrl="/fscmRestApi/resources/11.13.18.05/valueSets/";
    private String createInvoice = "/fscmRestApi/resources/11.13.18.05/invoices/";
    public String protocol = "https";
    
    public String getInstanceUrl() {
        String url="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            url= CommonConfigReader.getValue("TestInstanceUrl");
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            url= CommonConfigReader.getValue("DevInstanceUrl");
        }
        return url;
    }

    public String getInstanceName() {
        String name="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            name= CommonConfigReader.getValue("TestInstanceName");
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            name= CommonConfigReader.getValue("DevInstanceName");
        }
        return name;
    }
    
    public String getUserName() {
        String userName="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            userName= CommonConfigReader.getValue("TEST_USER_NAME");
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            userName= CommonConfigReader.getValue("DEV_USER_NAME");
        }
        return userName;
    }
    
    public String getPassword() {
        String password="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            password= CommonConfigReader.getValue("TEST_PASSWORD");
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            password= CommonConfigReader.getValue("DEV_PASSWORD");
        }
        return password;
    }
    
    public String getGetinvoices() {
        return getinvoices;
    }

    public String getCreateInvoice() {
        return createInvoice;
    }
    
    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getOrgnizationUrl() {
        return orgnizationUrl;
    }

    public String getEmployeeServiceUrl() {
        return employeeServiceUrl;
    }
    
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }
    
    public String getFinanceUrl() {
        return financeUrl;
    }
    
    public String getValueSetsUrl() {
        return valueSetsUrl;
    }
    
    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    public  void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
    
    public JSONObject httpGet (String restUrl) throws MalformedURLException, IOException {
        JSONObject resObj = new JSONObject();
        
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        URL url = new URL(null,restUrl,new sun.net.www.protocol.https.Handler());

        
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String soapAction = null ;
        connection.setRequestMethod("GET");
        connection.setReadTimeout(6000000);
        connection.setRequestProperty("content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("SoapAction", soapAction);
        connection.setRequestProperty("Authorization", "Basic " + getAuth());
       
        int responseCode = connection.getResponseCode();
        InputStream inputStream;
        if (responseCode >= 400) {
            inputStream = connection.getErrorStream();
            resObj.put("status", "error");
        } else {
            resObj.put("status", "done");
            inputStream = connection.getInputStream();
        } 
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        resObj.put("data",response.toString());
        return resObj;
        
    }
    
    public Document httpPost(String destUrl, String postData) throws Exception {
        
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
     
        java.net.URL url = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        System.out.println("====== instance url "+url);
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String SOAPAction = "";
        connection.setRequestProperty("Content-Length", String.valueOf(b.length));
        connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        connection.setRequestProperty("SOAPAction", SOAPAction);
        connection.setRequestProperty("Authorization", "Basic " + getAuth()); 
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        OutputStream out = connection.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + connection.getResponseCode() + "; connection response: " + connection.getResponseMessage());

        if (connection.getResponseCode() == 200) {
            InputStream in = connection.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in,"UTF-8");
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            if (response.indexOf("<?xml") > 0)
                response =response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +15);
            
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));
            
            XML.toJSONObject(response).toString();
            
            iReader.close();
            bReader.close();
            in.close();
            connection.disconnect();
            return doc;
        } 
        else {
            System.out.println("error"+connection.getResponseMessage());
            InputStream in = connection.getErrorStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            System.out.println("error2"+connection.getErrorStream());
            System.out.println("Failed"+response);
            if (response.indexOf("<?xml") > 0)
                response =response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +15);
            
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));
            
            XML.toJSONObject(response).toString();
            
            iReader.close();
            bReader.close();
            in.close();
            connection.disconnect();
            
            return doc;
        }
    }
    
    public JSONObject httpPatch (String destUrl,String body){
           JSONObject res = new JSONObject();
           
        try{
           
             HttpsURLConnection https = null;
             HttpURLConnection connection = null;
             URL url;

             url = new URL(null,destUrl,new sun.net.www.protocol.https.Handler());
      
             System.out.println(url);
             if(url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                connection = https;
             }
             else{
                connection = (HttpsURLConnection)url.openConnection();
             }
            
            String soapAction = null;
            connection.setReadTimeout(6000000);
            connection.setRequestProperty("content-type", "application/json");
            connection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("charset", "UTF-8");
            connection.setRequestProperty("SoapAction", soapAction);
            connection.setRequestProperty("Authorization", "Basic " + getAuth());
            
            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            
            int code = connection.getResponseCode();
            InputStream is;
            if (code >= 400) {
                is = connection.getErrorStream();
                res.put("status", "error");
            } else {
                res.put("status", "done");
                is = connection.getInputStream();
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                res.put("data", response.toString());
            }

            System.out.print("code:" + code);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", ioe.getMessage());
        }

             return res;
    }

    public  String callPostRest(String destUrl, String jwttoken,
                                      String contentType,
                                      String postData) throws Exception {
        try {

            System.setProperty("DUseSunHttpHandler", "true");
            byte[] buffer = new byte[postData.length()];
            buffer = postData.getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url =
                new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                //            System.setProperty("DUseSunHttpHandler", "true");
                //https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";
            //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", contentType);
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);

            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }
    public  String getAuth() {

        byte[] message = (getUserName() + ":" + getPassword()).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }


   
}
