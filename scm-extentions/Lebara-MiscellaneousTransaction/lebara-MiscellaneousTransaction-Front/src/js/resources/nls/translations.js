define({
    // Root bundle
    "root": {
        "common": {
            "switchLang": "العربية"
        },
        "login": {
            "userName": "User Name: ",
            "Password": "Password: ",
            "loginLabel": "Login",
            "resetLabel": "Reset",
            "loginFailureText": "Invalid User Name or Password",
            "SignOut": "Sign Out",
            "Name": "Employee Name: "
        },
        "labels": {
             "changeLang": "Change language?",
            "signOut": "Are you sure you wish to logout?",
            "confirmMessage": "Confirm Message"
        },
        "others": {
              "yes": "Yes",
            "no": "No",
            back:"Back"
        },
        "uploadScreen":{
          "title":"Please select file to upload",
          "upload":"Send File",
          "upload_file_not_selected": "You didn't select a file to upload!",
          "upload_file_done": "File uploaded successfully",
          "upload_file_error": "Failed to upload the file",
          "uploadingandprocessingdata": "Uploading and Processing Data ...",
          "click_or_dragdrop": "Click or drag a file and drop here",
          "transactionType":"TransactionType",
          "placeHolder":"Please Select Value",
          "download":"Download Sample File",
          "selectTransactionMsg":"Please Select Transaction Type First",
          "organizationName":"Organization Name"
                
        }
    },
    "ar": true
});