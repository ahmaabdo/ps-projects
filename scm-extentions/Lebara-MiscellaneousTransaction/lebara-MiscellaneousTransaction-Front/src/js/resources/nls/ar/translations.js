define({
    // Arabic bundle
    "common": {
        "switchLang": "English"



    },
    "login": {
        "userName": "اسم المستخدم",
        "Password": "كلمة المرور",
        "loginLabel": "تسجيل الدخول",
        "resetLabel": "إلغاء",
        "loginFailureText": "اسم مستخدم غير صحيح أو كلمة سر غير صحيحة",
        "SignOut": "تسجيل الخروج",
        "Name": "الاسم: "

    },
    "labels": {
        "changeLang": "هل انت متأكد من تغير اللغة؟",
        "signOut": "هل تود تسجيل الخروج؟",
        "confirmMessage": "رسالة تاكيد"
    },
    "others": {
        "yes": "موافق",
        "no": "رجوع",
        "back": "رجوع"
    },
    "uploadScreen": {
        "title": "من فضلك قم باختيار ملف لرفعه",
        "upload": "إرسال الملف",
        "upload_file_not_selected": "لم تقم بإختيار ملف",
        "upload_file_done": "تم تحميل الملف بنجاح",
        "upload_file_error": "حدثت مشكلة أثناء رفع الملف",
        "uploadingandprocessingdata": "جارى رفع ومعالجة البيانات... ",
        "click_or_dragdrop": "أضغط أو أسحب وأسقط هنا",
        "transactionType":"نوع المعاملة",
        "placeHolder":"يرجى تحديد القيمة",
        "download":"تنزيل ملف عينة",
        "selectTransactionMsg":"يرجى تحديد نوع المعاملة أولاً",
        "organizationName":"أسم المنظمة"

    }

});