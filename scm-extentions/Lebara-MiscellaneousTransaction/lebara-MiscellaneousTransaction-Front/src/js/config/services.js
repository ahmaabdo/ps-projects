/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['config/serviceconfig', 'util/commonhelper'],
        function (serviceConfig, commonHelper) {

            /**
             * The view model for managing all services
             */
            function services() {

                var self = this;

                var restPath = commonHelper.getInternalRest();
                var appServiceHost = "";
                var biReportServletPath = commonHelper.getBiReportServletPath();
                
                 self.authenticate = function (userName, password) {
                    var serviceURL = restPath + "login/validateLogin";
                    var payload = {
                        "userName": userName, "password": password, "mainData": 'true'
                    };

                    var headers = {
                        // "Authorization":"Basic amphZG9vbkBoaGEuY29tLnNhOmhoYUAxMjMkTUlT"
                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                self.getGeneric = function (serviceName, headers) {
                    var serviceURL = restPath + serviceName;
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.getGenericAsync = function (serviceName, headers) {
                    var serviceURL = restPath + serviceName;
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
                };
                self.editGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };
                    return serviceConfig.callPutApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.addGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;

                    var headers = {
                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };

                //  add delete Generic function
                self.deleteGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };

                    return serviceConfig.callDeleteService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                
                  self.getGenericReport = function (payload) {
                    var serviceURL =appServiceHost+ restPath + biReportServletPath;
                   var headers = {};
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true,headers);
                };

                self.addFile = function (serviceName, payload) {
                    var headers = {
                    };
                    var serviceURL = restPath + serviceName;
                    return serviceConfig.callAddFileService(serviceURL, payload, headers);
                };

            }
            ;

            return new services();
        });