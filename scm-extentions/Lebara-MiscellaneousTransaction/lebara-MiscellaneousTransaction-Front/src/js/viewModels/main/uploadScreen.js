/**
 * uploadScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'util/commonhelper', 'appController',
    'ojs/ojpagingdataproviderview', 'ojs/ojarraydataprovider', 'xlsx', 'ojs/ojknockout', 'ojs/ojbutton',
    'ojs/ojmessages', 'ojs/ojinputtext', 'ojs/ojfilepicker', 'ojs/ojdialog', 'ojs/ojselectsingle',
    'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojpagingcontrol', 'ojs/ojlabelvalue'
], function (oj, ko, $, services, commonhelper, app, PagingDataProviderView, ArrayDataProvider) {

    function uploadScreenContentViewModel() {
        var self = this;
        var selected_file = null;
        var getTranslation = oj.Translations.getTranslatedString;
        self.labels = ko.observableArray([]);
        self.messages = ko.observableArray();
        self.messagesDataprovider = new ArrayDataProvider(self.messages);
        self.acceptArr = ko.observableArray([]);
        self.acceptArr = ko.pureComputed(function () {
            var accept = ".xls,.xlsx";
            return accept ? accept.split(",") : [];
        }, self);
        self.excelData = ko.observableArray();
        self.dialogMsg = ko.observable();
        self.messagesData = ko.observableArray();
        self.status = ko.observable();
        self.fileDisabled = ko.observable(false);
        self.btnDisabled = ko.observable(false);
        self.transactionTypesArray = ko.observableArray([]);
        self.transactionTypesListProvider = new ArrayDataProvider(self.transactionTypesArray, { keyAttributes: 'value' });
        self.organizationArray=ko.observableArray([]);
        self.organizationListProvider = new ArrayDataProvider(self.organizationArray, { keyAttributes: 'value' });
        self.selectedTransactionType = ko.observable();
        self.selectedOrganization = ko.observable();
        self.finalRestult = ko.observable();
        self.accountsArray = ko.observableArray([]);
        self.tableArray = ko.observableArray([]);
        self.tableDataProvider = new PagingDataProviderView(new ArrayDataProvider(self.tableArray, { idAttributes: 'count' }));
        self.showContent = ko.observable(false);
        //--------------- file picker actions -----------------//

        self.selectListener = function (event) {
            self.finalRestult('');
            self.tableArray([]);
            if (self.selectedTransactionType() == null ||
                typeof self.selectedTransactionType() == 'undefined') {

                self.messages.push({ severity: "error", summary: getTranslation("uploadScreen.selectTransactionMsg"), autoTimeout: 0 });

            } else {
                app.loading(true);
                var files = event.detail.files;
                var reader = new FileReader();
                self.failMessage=ko.observable();
                selected_file = files[0];
                self.labels.picker_text(selected_file.name);
                self.excelData([]);
                reader.onload = function (e) {
                    var data = e.target.result;
                    var workbook = XLSX.read(data, { type: 'binary' });
                    var data2 = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
                    var transactionType = self.transactionTypesArray().filter((data) => data.value == self.selectedTransactionType());
                    try {
                        for (var i = 0; i < data2.length; i++) {
                            data2[i].TransactionTypeId = transactionType[0].value;
                            data2[i].TransactionActionId = transactionType[0].actionId;
                            data2[i].TransactionSourceTypeId = transactionType[0].sourceId;

                            var org=self.accountsArray().find(obj => obj.OrganizationName == data2[i]['*OrganizationName']);
                            var account=self.accountsArray().find(obj => obj.account == data2[i]['*Account']);
                            if (typeof org == 'undefined') {
                                self.failMessage('Organization is wrong please check it');
                            } else if (typeof account == 'undefined') {
                                self.failMessage('Account is wrong please check the account');
                            }

                            var lineData = {
                                OrganizationId: org.OrganizationId,
                                Item: data2[i]['*Item'].trimEnd(),
                                Subinventory: data2[i]['*Subinventory'].trimEnd(),
                                TransactionTypeId: data2[i]['TransactionTypeId'].trimEnd(),
                                TransactionUnitOfMeasure: data2[i]['*TransactionUnitOfMeasure'].trimEnd(),
                                TransactionQuantity: data2[i]['*TransactionQuantity'].trimEnd(),
                                DistributionAccountId:account.id,
                                TransactionDate: self.formatDate(new Date()),
                                TransactionActionId: data2[i]['TransactionActionId'].trimEnd(),
                                TransactionSourceTypeId: data2[i]['TransactionSourceTypeId'].trimEnd()
                            };

                            
                            var serials = {
                                FromSerialNumber: data2[i]['*From Serial Number'].trimEnd(),
                                ToSerialNumber: data2[i]['*To Serial Number'].trimEnd(),
                            };
                            var serialArr = [serials];
                            lineData.serialItemSerials = serialArr;
                            var finalData = [lineData];
                            var sendedData = {};
                            sendedData.transactionLines = finalData;
                            self.excelData.push(sendedData);
                        }
                    } catch (e) {
                        console.log(e);
                        self.labels.picker_text(getTranslation("uploadScreen.click_or_dragdrop"));
                        self.fileDisabled(false);
                        self.btnDisabled(false);
                        self.messages.push({ severity: "error", summary: self.failMessage()});
                        app.loading(false);
                    }
                    console.log(self.excelData());
                    app.loading(false);
                };
                reader.onerror = function (ex) {
                    app.loading(false);
                };
                reader.readAsBinaryString(files[0]);
            }
        };
        //---------****************-----------------//
        //---------- buttons action ----------------//
        self.uploadAction = function (event) {
            console.log(selected_file)
            if (selected_file == null ||
                typeof selected_file == 'undefined') {
                self.labels.picker_text(getTranslation("uploadScreen.click_or_dragdrop"));
                self.fileDisabled(false);
                self.btnDisabled(false);
                self.messages.push({ severity: "error", summary: getTranslation("uploadScreen.upload_file_not_selected"), autoTimeout: 0 });//default time out
            } else if (self.selectedTransactionType() == null ||
                typeof self.selectedTransactionType() == 'undefined') {
                self.messages.push({ severity: "error", summary: getTranslation("uploadScreen.selectTransactionMsg"), autoTimeout: 0 });
            } else if (self.excelData().length <= 0) {
                self.messages.push({ severity: "error", summary: getTranslation("Wrong excel file please download simple file and fill it"), autoTimeout: 0 });
            } else if (self.selectedOrganization() == null ||
            typeof self.selectedOrganization() == 'undefined') {
                self.messages.push({ severity: "error", summary: getTranslation("please select organization first"), autoTimeout: 0 });
            } else {
                self.fileDisabled(true);
                self.btnDisabled(true);
                self.labels.picker_text(getTranslation("uploadScreen.uploadingandprocessingdata"));
                self.messagesData([]);
                document.getElementById("demo1").innerHTML = "";
                console.log(self.excelData());
                var success = function (data) {
                    self.messagesData(data);
                    var successCount = 0;
                    var failCount = 0;
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].status == 'Done') {
                                successCount++;
                            } else {
                                failCount++;
                                var tableData = {};
                                tableData.count = i + 1;
                                tableData.message = data[i].message;
                                self.tableArray().push(tableData);
                            }

                        }
                        self.finalRestult('Success : ' + successCount + '   Fails : ' + failCount + '   and the table contains fails requests');
                    } else {
                        self.finalRestult(' No Result returns please try to upload again');
                    }
                    self.showContent(true);
                    self.labels.picker_text(getTranslation("uploadScreen.click_or_dragdrop"));
                    self.fileDisabled(false);
                    self.btnDisabled(false);
                    selected_file = null;
                };
                var fail = function (data) {
                    self.fileDisabled(false);
                    self.btnDisabled(false);
                    console.log('fail  ' + data);
                };
                services.addGeneric("MiscellaneousTransaction/upload", JSON.stringify(self.excelData()), {}).then(success, fail);

            }
        };
        // downloading sample file
        self.downloadSample = function () {
            var link = document.createElement("a");
            link.download = "Lebara Miscellaneous Transaction.xlsx";
            link.href = "css/Lebara Miscellaneous Transaction.xlsx";
            link.click();
        };

        //-------------******************------------------//
        // for translation
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        self.connected = function () {
            initTranslations();
            app.loading(false);
            self.getTransactionTypeList();
            self.getOrganizationList();
        };

        self.valueChangedHandler=function(event){
         var value=event.detail.value;
         console.log(value);
         if(value !=null && typeof value!='undefined'){
            self.getAccountList(value);
         }
         
        };

        //gettransaction types
        self.getTransactionTypeList = function () {
            var success = function (data) {
                self.transactionTypesArray(data);
                app.loading(false);
            };
            var fail = function () {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage("error", 'Fail to get transaction list'));
            };
            services.getGeneric('MiscellaneousTransaction/TransactionList').then(success, fail);
        };
         //get all organizations types
         self.getOrganizationList = function () {
            var success = function (data) {
                self.organizationArray(data);
                app.loading(false);
            };
            var fail = function () {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage("error", 'Fail to get transaction list'));
            };
            services.getGeneric('MiscellaneousTransaction/OrganizationList').then(success, fail);
        };
        // get all accounts 
        self.getAccountList = function (orgCode) {
            app.loading(true);
            var success = function (data) {
                self.accountsArray(data);
                app.loading(false);
            };
            var fail = function () {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage("error", 'Fail to get accounts'));
            };
            services.getGenericAsync('MiscellaneousTransaction/accountList/'+orgCode).then(success, fail);
        };
        // close dialog
        self.closeMessageDialog = function () {
            document.getElementById("messageDialog1").close();
        };

        //format date
        self.formatDate = function (date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };
        /*************** translations***************************/
        function initTranslations() {
            self.labels = {
                title: ko.observable(getTranslation('uploadScreen.title')),
                upload: ko.observable(getTranslation('uploadScreen.upload')),
                picker_text: ko.observable(getTranslation('uploadScreen.click_or_dragdrop')),
                transactionType: ko.observable(getTranslation('uploadScreen.transactionType')),
                placeHolder: ko.observable(getTranslation('uploadScreen.placeHolder')),
                download: ko.observable(getTranslation('uploadScreen.download')),
                organizationName:ko.observable(getTranslation('uploadScreen.organizationName')),
                columnArray: ko.observableArray([
                    {
                        "headerText": "#", "field": "count"
                    },
                    {
                        "headerText": 'Message', "field": "message"
                    }
                ])

            }
        }
    }

    return uploadScreenContentViewModel;
});
