package com.appspro.lebara.rest;

import com.appspro.lebara.dao.MiscellaneousTransactionDAO;


import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("MiscellaneousTransaction")

public class MiscellaneousTransaction {

    MiscellaneousTransactionDAO mtDao=new MiscellaneousTransactionDAO();
    
    @GET
    @Path("/TransactionList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTransactionList() {
       ArrayList<JSONObject> trans=new ArrayList<JSONObject>();
        try {
            trans =mtDao.getTransactionTypesList();
          return  new JSONArray(trans).toString();
        } catch (Exception e) {
            e.printStackTrace();
          return  e.getMessage();
        }
    }
    
    @GET
    @Path("/OrganizationList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getOrganizationList() {
       ArrayList<JSONObject> trans=new ArrayList<JSONObject>();
        try {
            trans =mtDao.getOrganizationList();
          return  new JSONArray(trans).toString();
        } catch (Exception e) {
            e.printStackTrace();
          return  e.getMessage();
        }
    }
    @GET
    @Path("/accountList/{orgCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAccountList(@PathParam("orgCode") String orgCode) {
        ArrayList<JSONObject> accounts=new ArrayList<JSONObject>();
        try {
            accounts=mtDao.getAccountsList(orgCode);
            return new JSONArray(accounts).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
    
    @POST
    @Path("/upload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response upload(String body) {
        try {
            String res = mtDao.createMiscellaneousTransaction(body);
            return Response.ok(res.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ok(e.getMessage()).build();
        }
    }
}
