package com.appspro.lebara.dao;

import common.restHelper.RestHelper;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

public class MiscellaneousTransactionDAO extends RestHelper {

    public ArrayList<JSONObject> getTransactionTypesList() {

        ArrayList<JSONObject> transactionList = new ArrayList<JSONObject>();
        int offset = 0;
        int limit = 500;
        boolean hasMore = true;
        try {
            while (hasMore) {
                String serverUrl =
                    getInstanceUrl() + getTransactionTypesURL() +
                    "?q=TransactionSourceTypeName=Inventory" + "&limit=" +
                    limit + "&offset=" + offset;
                String response = new RestHelper().callSaaS(serverUrl);

                JSONObject obj = new JSONObject(response.toString());
                if (obj.getBoolean("hasMore") == true) {
                    hasMore = true;
                    offset = offset + limit;
                    limit = limit + 500;
                } else {
                    hasMore = false;
                }
                if (obj.has("items")) {
                    JSONArray arr = obj.getJSONArray("items");
                    if (arr.length() > 0) {
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject object = arr.getJSONObject(i);
                            JSONObject dataObject = new JSONObject();
                            if (object.has("TransactionAction")) {

                                if ("Issue from stores".equalsIgnoreCase(object.get("TransactionAction").toString()) ||
                                    "Receipt into stores".equalsIgnoreCase(object.get("TransactionAction").toString())) {
                                    dataObject.put("label",
                                                   object.get("TransactionTypeName").toString());
                                    dataObject.put("value",
                                                   object.get("TransactionTypeId").toString());
                                    dataObject.put("actionId",
                                                   object.get("TransactionActionId").toString());
                                    dataObject.put("sourceId",
                                                   object.get("TransactionSourceTypeId").toString());
                                    transactionList.add(dataObject);
                                }

                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return transactionList;
    }

    public ArrayList<JSONObject> getOrganizationList() {

        ArrayList<JSONObject> organizationList = new ArrayList<JSONObject>();
        int offset = 0;
        int limit = 500;
        boolean hasMore = true;
        try {
            while (hasMore) {
                String serverUrl =getInstanceUrl() + getInventoryOrganizationURL() +
                    "?limit=" +limit + "&offset=" + offset;
                String response = new RestHelper().callSaaS(serverUrl);

                JSONObject obj = new JSONObject(response.toString());
                if (obj.getBoolean("hasMore") == true) {
                    hasMore = true;
                    offset = offset + limit;
                    limit = limit + 500;
                } else {
                    hasMore = false;
                }
                if (obj.has("items")) {
                    JSONArray arr = obj.getJSONArray("items");
                    if (arr.length() > 0) {
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject object = arr.getJSONObject(i);
                            JSONObject dataObject = new JSONObject();
                            dataObject.put("label",
                                                   object.get("OrganizationName").toString());
                            dataObject.put("value",
                                                   object.get("OrganizationCode").toString());
                            organizationList.add(dataObject);
                                
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return organizationList;
    }
    
    public ArrayList<JSONObject> getAccountsList(String organization) {
        ArrayList<JSONObject> accountList = new ArrayList<JSONObject>();
        int offset = 0;
        int limit = 500;
        boolean hasMore = true;
        try {
            while (hasMore) {
                String serverUrl =
                    getInstanceUrl() + getInventoryTransactionAccounts() + "" +
                    "?q=OrganizationCode="+organization+"&limit=" + limit + "&offset=" + offset;
                String response = new RestHelper().callSaaS(serverUrl);

                JSONObject obj = new JSONObject(response.toString());
                JSONArray arr = obj.getJSONArray("items");
                if (arr.length()>0) {
                    hasMore = true;
                    offset = offset + limit;
                    limit = limit + 500;
                } else {
                    hasMore = false;
                }
                if (obj.has("items")) {
                    
                    if (arr.length() > 0) {
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject object = arr.getJSONObject(i);
                            JSONObject dataObject = new JSONObject();
                            if (object.has("CodeCombinationId")) {
                                dataObject.put("id",
                                               object.get("CodeCombinationId").toString());
                                dataObject.put("account",
                                               object.get("Account").toString());
                                dataObject.put("OrganizationName",
                                               object.get("OrganizationName").toString());
                                dataObject.put("OrganizationId",
                                               object.get("OrganizationId").toString());
                                accountList.add(dataObject);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accountList;
    }

    public String createMiscellaneousTransaction(String incomingData) {
        JSONArray objArray = new JSONArray(incomingData);
        ArrayList<JSONObject> responseArray = new ArrayList<JSONObject>();
        System.out.println("incoming array ----------->" + objArray);
        /***** read data incomingData ***************/
        for (int i = 0; i < objArray.length(); i++) {
            System.out.println("row number ----------->" + i);
            JSONObject response = new JSONObject();
            JSONObject dataToSend = objArray.getJSONObject(i);
            JSONObject returnedData =
                httpPostUrl(getInstanceUrl() + getInventoryTransactionsURL(),
                            dataToSend.toString());
            System.out.println("row response -----------> " + returnedData);
            if (returnedData.has("ReturnStatus")) {
                if ("ERROR".equalsIgnoreCase(returnedData.get("ReturnStatus").toString())) {
                    response.put("status", "Error");
                    if ("Bad Request".equalsIgnoreCase(returnedData.get("ErrorExplanation").toString())) {
                        response.put("message",
                                     "Excel data is wrong please check the data again");
                    } else {
                        response.put("message",
                                     returnedData.get("ErrorExplanation").toString());
                    }
                } else {
                    response.put("status", "Done");
                    response.put("message",
                                 "Success in performing transaction ");
                }
            } else {
                response.put("status", "Error");
                response.put("message", "Internal Server Error");
            }
            responseArray.add(response);
        }
        return new JSONArray(responseArray).toString();
    }

    public String getAccountIdByAccountNumber(String accountNumber) {

        String accountId = null;
        try {
            String serverUrl =
                getInstanceUrl() + getInventoryTransactionAccounts() +
                "?q=Account=" + accountNumber;
            String response = new RestHelper().callSaaS(serverUrl);

            JSONObject obj = new JSONObject(response.toString());
            if (obj.has("items")) {
                JSONArray arr = obj.getJSONArray("items");
                if (arr.length() > 0) {
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject object = arr.getJSONObject(i);
                        if (object.has("CodeCombinationId")) {
                            accountId =
                                    object.get("CodeCombinationId").toString();
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return accountId;
    }
}
