package common.restHelper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;


public class RestHelper {

    private final String transactionTypesURL =
        "/fscmRestApi/resources/11.13.18.05/inventoryTransactionTypes";
    private final String inventoryTransactionsURL =
        "/fscmRestApi/resources/11.13.18.05/inventoryTransactions";
    private final String inventoryTransactionAccounts =
        "/fscmRestApi/resources/11.13.18.05/inventoryTransactionAccounts";
    private final String protocol = "https";
    private final String SecurityService =
        "/xmlpserver/services/v2/SecurityService";
    private final String inventoryOrganizationUrl="/fscmRestApi/resources/11.13.18.05/inventoryOrganizations";

    public String getInstanceUrl() {
        String instanceUrl = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            instanceUrl = CommonConfigReader.getValue("TestInstanceUrl");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            instanceUrl = CommonConfigReader.getValue("ProdInstanceUrl");
        }
        return instanceUrl;
    }

    public String getInstanceName() {
        String instanceName = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            instanceName = CommonConfigReader.getValue("TestInstanceName");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            instanceName = CommonConfigReader.getValue("ProdInstanceName");
        }
        return instanceName;
    }

    public String getUserName() {
        String userName = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            userName = CommonConfigReader.getValue("TEST_USER_NAME");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            userName = CommonConfigReader.getValue("PROD_USER_NAME");
        }
        return userName;
    }

    public String getPassword() {
        String password = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            password = CommonConfigReader.getValue("TEST_PASSWORD");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            password = CommonConfigReader.getValue("PROD_PASSWORD");
        }
        return password;
    }

    public String getSecurityService() {
        return SecurityService;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getTransactionTypesURL() {
        return transactionTypesURL;
    }

    public String getInventoryTransactionsURL() {
        return inventoryTransactionsURL;
    }

    public String getInventoryTransactionAccounts() {
        return inventoryTransactionAccounts;
    }

    public String getInventoryOrganizationURL() {
        return inventoryOrganizationUrl;
    }
    
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public String getAuth() {
        byte[] message = (getUserName() + ":" + getPassword()).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject httpPostUrl(String hostUrl, String data) {
        JSONObject obj = new JSONObject();
        try {
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url =
                new URL(null, hostUrl, new sun.net.www.protocol.https.Handler());
            System.out.println("URL ------> " + url);
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setDoOutput(true);
            http.setRequestMethod("POST");
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("Accept", "application/json");
            try (OutputStream os = http.getOutputStream()) {
                byte[] input = data.toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());
            InputStream inStream = null;
            if (http.getResponseCode() == 200 ||
                http.getResponseCode() == 201) {
                inStream = http.getInputStream();
            } else {
                inStream = http.getErrorStream();
            }
            BufferedReader in =
                new BufferedReader(new InputStreamReader(inStream, "UTF-8"));
            StringBuffer response = new StringBuffer();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            try {
                System.out.println("res ------------> " + response.toString());
                obj = new JSONObject(response.toString());
            } catch (Exception e) {
                obj.put("ReturnStatus", "Error");
                obj.put("ErrorExplanation", response.toString());
            }
            in.close();
            http.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            obj.put("ReturnStatus", "Error");
            obj.put("ErrorExplanation ",
                    "Error in connecting to the server please try again later");
        } catch (IOException e) {
            e.printStackTrace();
            obj.put("ReturnStatus", "Error");
            obj.put("ErrorExplanation ",
                    "Error in sending data to the server");
        }
        return obj;
    }


    public Document httpPost(String destUrl, String postData) {

        System.out.println("************* soap data *************");
        System.out.println(postData);
        try {
            System.setProperty("DUseSunHttpHandler", "true");
            byte[] buffer = new byte[postData.length()];
            buffer = postData.getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url =
                new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
            System.out.println("RestHelperdesUrl  " + destUrl);
            java.net.HttpURLConnection http;

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }

            String SOAPAction = "";
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());
            InputStream in;
            if (http.getResponseCode() == 200 ||
                http.getResponseCode() == 201) {
                in = http.getInputStream();
            } else {
                in = http.getErrorStream();
            }
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));
            Document doc =
                builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));

            XML.toJSONObject(response).toString();
            System.out.println("response " + response);
            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String callSaaS(String serverUrl) {
        String newurl = serverUrl.replaceAll(" ", "%20");
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        try {
            URL url =
                new URL(null, newurl, new sun.net.www.protocol.https.Handler());
            System.out.println("Instance url ----------> "+url);
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            //  connection.setRequestProperty("REST-Framework-Version", "3");
            //                connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());
//            connection.setRequestProperty("REST-Framework-Version", "1");
                        System.out.println("connection status: " +
                               connection.getResponseCode() +
                               "; connection response: " +
                               connection.getResponseMessage());
            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONObject obj = new JSONObject(response.toString());
            connection.disconnect();
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

}
