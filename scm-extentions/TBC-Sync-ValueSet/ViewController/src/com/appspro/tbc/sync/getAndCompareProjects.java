package com.appspro.tbc.sync;

import com.appspro.tbc.RestApiCall;
import com.appspro.tbc.biReport.BIReportModel;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class getAndCompareProjects {
    public static void main(String[] args) {


        Map<String, String> paramMap = new HashMap<String, String>();

        String resp =
            BIReportModel.runReport("Projects_For_Sync", "/Custom/Projects/Reports/",
                                    paramMap);
        Document doc = RestApiCall.convertStringToXMLDocument(resp);


        NodeList G_1 = doc.getElementsByTagName("G_1");


        for (int i = 0; i < 1; i++) {

            Element element = (Element)G_1.item(i);

            System.out.println(element.getElementsByTagName("PROJECT_NUMBER").item(0).getTextContent());
            
            RestApiCall rs = new RestApiCall();
            JSONObject js =
                rs.createValueSetREST(element.getElementsByTagName("PROJECT_NUMBER").item(0).getTextContent(),
                                      element.getElementsByTagName("PROJECT_NAME").item(0).getTextContent());
            System.out.println(js);
            

        }


    }


    public void compareAndPostNewProjects() {
        Map<String, String> paramMap = new HashMap<String, String>();

        String resp =
            BIReportModel.runReport("Projects_For_Sync", "/Custom/Projects/Reports/",
                                    paramMap);
        Document doc = RestApiCall.convertStringToXMLDocument(resp);


        NodeList G_1 = doc.getElementsByTagName("G_1");


        for (int i = 0; i < G_1.getLength(); i++) {

            Element element = (Element)G_1.item(i);

            
            
            
            if(element.getElementsByTagName("PROJECT_NUMBER").item(0).getTextContent().toString().length() == 6){
                
                    RestApiCall rs = new RestApiCall();
                    JSONObject js =
                        rs.createValueSetREST(element.getElementsByTagName("PROJECT_NUMBER").item(0).getTextContent(),
                                              element.getElementsByTagName("PROJECT_NAME").item(0).getTextContent());
                    System.out.println(js);
                
            }

            
        }


    }
}
