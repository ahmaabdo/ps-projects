package com.appspro.tbc;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;

import java.util.ArrayList;

import java.util.Collections;
import java.util.HashMap;

import java.util.HashSet;

import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;

/**
 *
 * @author HP
 */
public class RestApiCall {

    public static String callGetRestProjects(String serverUrl) throws IOException {
        System.setProperty("https.protocols", "TLSv1.2");
        StringBuffer response = null;
        try {

            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https
                        = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = "";
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("charset", "UTF-8");

            byte[] message = ("appspro.projects" + ":" + "123456789").getBytes();

            String encoded = DatatypeConverter.printBase64Binary(message);
            http.setRequestProperty("Authorization",
                    "Basic " + encoded);
            http.setDoOutput(true);
            
            http.setRequestMethod("GET");
            http.setRequestProperty("", "");
            

            BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream(), "UTF-8"));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

//            JSONObject reportResponse = new JSONObject(response.toString());
        } catch (MalformedURLException e) {

        }
        return response.toString();
    }
    
    
    public static String getValueSetRest(String serverUrl) throws IOException {
        System.setProperty("https.protocols", "TLSv1.2");
        StringBuffer response = null;
        try {

            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https
                        = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = "";
            http.setRequestProperty("SOAPAction", SOAPAction);

            byte[] message = ("hcmuser" + ":" + "Oracle@123").getBytes();

            String encoded = DatatypeConverter.printBase64Binary(message);
            http.setRequestProperty("Authorization",
                    "Basic " + encoded);
            http.setDoOutput(true);
            http.setRequestMethod("GET");
            http.setRequestProperty("", "");
            

            BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream(), "UTF-8"));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

    //            JSONObject reportResponse = new JSONObject(response.toString());
        } catch (MalformedURLException e) {

        }
        return response.toString();
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts
                = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }
            }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {

        }
    }
    
    
    public static JSONObject createValueSetREST(String projectNumber, String projectName)  {
        
        System.setProperty("https.protocols", "TLSv1.2");

        String body = " {\n"
                + "\n"
                + "      \"IndependentValue\": null,\n"
                + "      \"IndependentValueNumber\": null,\n"
                + "      \"IndependentValueDate\": null,\n"
                + "      \"IndependentValueTimestamp\": null,\n"
                + "      \"Value\": \"" + projectNumber + "\",\n"
                + "      \"ValueNumber\": null,\n"
                + "      \"ValueDate\": null,\n"
                + "      \"ValueTimestamp\": null,\n"
                + "      \"TranslatedValue\": null,\n"
                + "      \"Description\": \"" + projectName + "\",\n"
                + "      \"EnabledFlag\": \"Y\",\n"
                + "      \"StartDateActive\": null,\n"
                + "      \"EndDateActive\": null,\n"
                + "      \"SortOrder\": null,\n"
                + "      \"SummaryFlag\": \"N\",\n"
                + "      \"DetailPostingAllowed\": \"Y\",\n"
                + "      \"DetailBudgetingAllowed\": \"Y\",\n"
                + "      \"AccountType\": null,\n"
                + "      \"ControlAccount\": null,\n"
                + "      \"ReconciliationFlag\": null,\n"
                + "      \"FinancialCategory\": null,\n"
                + "      \"ExternalDataSource\": null\n"
                + "\n"
                + " }";

        System.out.println(body);

        JSONObject res = new JSONObject();

        try {
            String restApi = "https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/valueSets/Project%20TBC/child/values";

            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            URL url = new URL(null, restApi, new sun.net.www.protocol.https.Handler());

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                connection = https;
            } else {
                connection = (HttpsURLConnection) url.openConnection();
            }

            String soapAction = null;
            connection.setRequestMethod("POST");
            connection.setReadTimeout(6000000);
            connection.setRequestProperty("content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("charset", "UTF-8");
            connection.setRequestProperty("SoapAction", soapAction);
//        connection.setRequestProperty("Authorization", "Basic " + "YWJkdWxsYWgub3dlaWRpQGFwcHNwcm8tbWUuY29tOlBhYXNAMTIz");

            byte[] message = ("hcmuser" + ":" + "Oracle@123").getBytes();

            String encoded = DatatypeConverter.printBase64Binary(message);
            connection.setRequestProperty("Authorization",
                    "Basic " + encoded);

            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            int code = connection.getResponseCode();
            InputStream is;
            if (code >= 400) {
                is = connection.getErrorStream();
                res.put("status", "error");

            } else {
                res.put("status", "done");
                is = connection.getInputStream();
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                res.put("data", response.toString());
            }

            System.out.print("code:" + code);
            if (code >= 400) {
                System.out.println(",Error:" + res.get("data").toString());
            } else {
                System.out.println();
            }

        } catch (Exception ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", "Internal server error");
        }

        return res;

    }
    public static Document httpPost(String destUrl, String soapRequest) throws  Exception {
        
        System.setProperty("https.protocols", "TLSv1.2");
        System.setProperty("DUseSunHttpHandler", "true");
    //        System.out.println(soapRequest);
//        System.setProperty("https.proxyHost", "proxy.stc.com.sa");
    //        System.setProperty("https.proxyPort", "8080");
    //        System.setProperty("https.proxyUser", "aosalman.c@stc.com.sa");
//        System.setProperty("https.proxyPassword", "oracle@1234");
        byte[] buffer = new byte[soapRequest.length()];
        buffer = soapRequest.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url
                = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https
                    = (HttpsURLConnection) url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection) url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
    //        System.out.println("Auth = " + getAuth());
        byte[] message = ("appspro.projects" + ":" + "123456789").getBytes();

        String encoded = DatatypeConverter.printBase64Binary(message);
        http.setRequestProperty("Authorization",
                "Basic " + encoded);
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);
//        String myResponse = http.getResponseCode() + " " + http.getResponseMessage();
            System.out.println("connection status: " + http.getResponseCode()
                    + "; connection response: "
                    + http.getResponseMessage());
        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);
            String line;
            String response = "";
    //            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                
                
                response += line;
            }
            
//            System.out.println(response);
//            if (response.indexOf("<?xml") > 0) {
//                response
//                        = response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>")
//                                + 15);
//            }
    //            System.out.println(response);
            
    Document doc2 = convertStringToXMLDocument( response );
     
    //Verify XML document is build correctly
    System.out.println(doc2.getFirstChild().getNodeName());
            System.out.println(doc2.toString());
            
            
            
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder
                    = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));
            Document doc = builder.parse(src);
            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();
            return doc;
        } else {
            return null;
    //            System.out.println("Failed");
        }
    //        return null;
    }
    
    
    public static Document convertStringToXMLDocument(String xmlString) {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            Document doc =
                builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    
//    public static void main(String [] args){
//        
//          String projectsResp;
//          String valueSetResp;
//
//
//        try {
//            projectsResp = callGerRest("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projects?q=ProjectStatus=Active&limit=1000");
//            
//            valueSetResp = callGerRest("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/valueSets/Project%20TBC/child/values?limit=1000");
//
//    //            System.out.println(valueSetResp);
//    //
//    //
//    //
//    //
//            JSONObject projectJson = new JSONObject(projectsResp);
//           
//            JSONArray jSONArray = projectJson.getJSONArray("items");
//            
//            
//            
////            ArrayList<String> listOne = new ArrayList<String> ();
//            
//            HashMap<Integer, String> mapOne = new HashMap<>();
//            
//            
//            for (int i = 0; i < jSONArray.length(); i++) {
//                
//                
//                    JSONObject tc = jSONArray.getJSONObject(i);
//                
//    //                    System.out.println(tc.get("ProjectNumber"));
//                
////                    listOne.add(tc.get("ProjectNumber").toString());
//                
//                    mapOne.put(Integer.parseInt(tc.get("ProjectNumber").toString()), tc.get("ProjectName").toString());
//                
//                
//                
//                }
//            
//            
//            
//            
//            
//            
//            JSONObject valueSetJson = new JSONObject(valueSetResp);
//            
//            JSONArray valueSetjSONArray = valueSetJson.getJSONArray("items");
//            
//            
//            
////            ArrayList<String> listTwo = new ArrayList<String> ();
//            
//            HashMap<Integer, String> mapTwo = new HashMap<>();
//            
//            
//            for (int i = 0; i < valueSetjSONArray.length(); i++) {
//                
//                
//                    JSONObject tc = valueSetjSONArray.getJSONObject(i);
//                
//            //                    System.out.println(tc.get("ProjectNumber"));
//                
////                    listTwo.add(tc.get("Value").toString());
//                
//                    mapTwo.put(Integer.parseInt(tc.get("Value").toString()) , tc.get("Description").toString());
//                
//                
//                
//                }
//            
//            
//            
//            
//            
////            HashSet<Integer> unionKeys = new HashSet<>(mapOne.keySet());
////            unionKeys.addAll(mapTwo.keySet());
//             
//            mapOne.keySet().removeAll(mapTwo.keySet());
//             
//            System.out.println(mapOne.size());
//            
//            for (Map.Entry<Integer, String> entry : mapOne.entrySet()) {
//                                System.out.println(entry.getKey() + " = " + entry.getValue());
//                            }
//            
//            
//            
//        } catch (IOException e) {
//        }
//    }


    public static void compareValueSetWithProjects() {

        String projectsResp;
        String valueSetResp;


        try {
            projectsResp =
                    callGetRestProjects("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/projects?q=ProjectStatus=Active&limit=1000");

            valueSetResp =
                    callGetRestProjects("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/valueSets/Project%20TBC/child/values?limit=1000");

            //            System.out.println(valueSetResp);
            //
            //
            //
            //
            JSONObject projectJson = new JSONObject(projectsResp);

            JSONArray jSONArray = projectJson.getJSONArray("items");


            ArrayList<Integer> listOne = new ArrayList<Integer>();
            HashMap<Integer, String> mapTwo = new HashMap<>();

            for (int i = 0; i < jSONArray.length(); i++) {


                JSONObject tc = jSONArray.getJSONObject(i);

                //                    System.out.println(tc.get("ProjectNumber"));

                listOne.add(Integer.parseInt(tc.get("ProjectNumber").toString()));
                
                mapTwo.put(Integer.parseInt(tc.get("ProjectNumber").toString()), tc.get("ProjectName").toString());
                
                


            }


            JSONObject valueSetJson = new JSONObject(valueSetResp);

            JSONArray valueSetjSONArray = valueSetJson.getJSONArray("items");


            ArrayList<Integer> listTwo = new ArrayList<Integer>();


            for (int i = 0; i < valueSetjSONArray.length(); i++) {


                JSONObject tc = valueSetjSONArray.getJSONObject(i);

                //                    System.out.println(tc.get("ProjectNumber"));

                listTwo.add(Integer.parseInt(tc.get("Value").toString()));


            }




            System.out.println("after difff=================================================");
            System.out.println("size1 = " + listOne.size());
            System.out.println("size2 = " + listTwo.size());


            System.out.println();
            System.out.println("after difff=================================================");
            Collections.sort(listOne);
            Collections.sort(listTwo);

            listOne.removeAll(listTwo);



            for (int i = 0; i < listOne.size(); i++) {

                System.out.println(listOne.get(i));
                
                System.out.println(mapTwo.get(listOne.get(i)));

            }

            System.out.println("size = " + listOne.size());


        } catch (IOException e) {
        }
    }
    
    
    public static void main(String [] args){
        
//            compareValueSetWithProjects();
        
                String projectNumber = "195001";

        String projectName = "Test2";

        JSONObject js = createValueSetREST(projectNumber, projectName);

        System.out.println(js);
            
            
        
        }
    
    

//    public static void main(String[] args) throws IOException {
//
//        String projectNumber = "134001";
//
//        String projectName = "Educational buildings rehabilitation project, stage 12 isnad 077/2018";
//
//        JSONObject js = createValueSetREST(projectNumber, projectName);
//
//        System.out.println(js);
//        
//        
////        String url = "https://eoay-test.fa.em2.oraclecloud.com:443/fscmService/ProjectDefinitionPublicServiceV2";
////            String soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/projects/foundation/projectDefinition/publicService/maintainProjectV2/types/\">\n" + 
////            "   <soapenv:Header/>\n" + 
////            "   <soapenv:Body>\n" + 
////            "      <typ:getProject>\n" + 
////            "         <typ:projectId>300000001502154</typ:projectId>\n" + 
////            "      </typ:getProject>\n" + 
////            "   </soapenv:Body>\n" + 
////            "</soapenv:Envelope>";
//        
//        
//        
////    Document response;
////
////        try {
////            response = httpPost(url,soapRequest);
////            
//////            System.out.println(response);
//////            
//////            Document doc2 = convertStringToXMLDocument( response );
////             
////            //Verify XML document is build correctly
////            System.out.println(response.getElementsByTagName("ns1:LastUpdateDate").item(0).getTextContent());
////                    System.out.println(response.toString());
////            
////            
////            
////            
////        } catch (Exception e) {
////        }
//    }

}

