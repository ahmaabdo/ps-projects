package com.appspro.tbc.job;

import com.appspro.tbc.sync.getAndCompareProjects;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 *
 * @author HP
 */
public class ValueSetScheduledJob implements Job {
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        getAndCompareProjects getCompareProjects = new getAndCompareProjects();
        getCompareProjects.compareAndPostNewProjects();
    }
}
