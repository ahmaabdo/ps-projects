package com.appspro.nhcCommittee.rest;

import com.appspro.nhcCommittee.bean.AttachmentBean;
import com.appspro.nhcCommittee.bean.CommitteeActivtiesBean;

import com.appspro.nhcCommittee.bean.EmployeeBean;
import com.appspro.nhcCommittee.dao.AttachmentDAO;
import com.appspro.nhcCommittee.dao.CommitteeActivitiesDAO;

import com.appspro.nhcCommittee.dao.EmployeeCommitteeDAO;

import java.sql.SQLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/activities")
public class CommitteeActivitiesRest {
    JSONObject resposne = new JSONObject();
    JSONObject obj = new JSONObject();
    JSONObject objAct = new JSONObject();

    CommitteeActivtiesBean bean = new CommitteeActivtiesBean();
    EmployeeBean empBean = new EmployeeBean();
    AttachmentBean attachBean = new AttachmentBean();
    ArrayList<CommitteeActivtiesBean> list =new ArrayList();
    CommitteeActivitiesDAO serivces = new CommitteeActivitiesDAO();
    EmployeeCommitteeDAO empSerivces = new EmployeeCommitteeDAO();
    AttachmentDAO attachSerivces = new AttachmentDAO();
    JSONArray arr = new JSONArray();
    JSONArray arrEmp = new JSONArray();
    JSONArray arrAttach = new JSONArray();
    String status;
    
    
    @GET
    @Path("/getNegData/{negNum}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getNegotiations(@PathParam("negNum") String negNum) {

        try {
            arrEmp = empSerivces.getEmployees(negNum);
            arrAttach = attachSerivces.getAttachmentByDocNum(negNum);
            obj.put("negNum",negNum);
            obj.put("attachment",arrAttach);
            obj.put("employees",arrEmp);
            resposne.put("Status", "OK");
            resposne.put("Data", obj);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }
    
    @POST
    @Path("/insertActivity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addNegotiation(String body) {
        try {
            status = serivces.addNegotiation(body);
            resposne.put("Status", "OK");
            resposne.put("Data", status);

        }

        catch (SQLException e) {

            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();

    }
    
    @GET
    @Path("/getActivities/{negNum}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getNegotiationData(@PathParam("negNum") String negNum) {

        try {
            arr = serivces.getNegotiation(negNum);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }
}
