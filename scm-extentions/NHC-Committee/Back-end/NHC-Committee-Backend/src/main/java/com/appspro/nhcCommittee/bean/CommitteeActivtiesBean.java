package com.appspro.nhcCommittee.bean;

public class CommitteeActivtiesBean {
    private int id;
    private String negNum;
    private String negTitle;
    private String sepplierName;
    private int total;
    private int docNum;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setNegNum(String negNum) {
        this.negNum = negNum;
    }

    public String getNegNum() {
        return negNum;
    }

    public void setNegTitle(String negTitle) {
        this.negTitle = negTitle;
    }

    public String getNegTitle() {
        return negTitle;
    }

    public void setSepplierName(String sepplierName) {
        this.sepplierName = sepplierName;
    }

    public String getSepplierName() {
        return sepplierName;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setDocNum(int docNum) {
        this.docNum = docNum;
    }

    public int getDocNum() {
        return docNum;
    }
}
