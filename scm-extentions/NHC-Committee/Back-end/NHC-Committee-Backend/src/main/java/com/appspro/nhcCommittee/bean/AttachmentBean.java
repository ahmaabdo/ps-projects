package com.appspro.nhcCommittee.bean;

public class AttachmentBean {
    private int id;
    private int docNum;
    private String name;
    private String title;
    private String content;
    private String negTitle;
    private String negNumber;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setDocNum(int docNum) {
        this.docNum = docNum;
    }

    public int getDocNum() {
        return docNum;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNegTitle(String negTitle) {
        this.negTitle = negTitle;
    }

    public String getNegTitle() {
        return negTitle;
    }

    public void setNegNumber(String negNumber) {
        this.negNumber = negNumber;
    }

    public String getNegNumber() {
        return negNumber;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
