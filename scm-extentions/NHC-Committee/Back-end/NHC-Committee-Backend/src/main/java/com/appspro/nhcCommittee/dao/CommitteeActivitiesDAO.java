package com.appspro.nhcCommittee.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.nhcCommittee.bean.AttachmentBean;

import com.appspro.nhcCommittee.bean.CommitteeActivtiesBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class CommitteeActivitiesDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public JSONArray getNegotiation(String negNum) throws SQLException {
        JSONArray arr = new JSONArray();
        try {
            ArrayList<CommitteeActivtiesBean> list =
                new ArrayList<CommitteeActivtiesBean>();
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM XXX_COMMITTEE_ACTIVITIES WHERE neg_num=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, negNum);
            rs = ps.executeQuery();

            while (rs.next()) {
                CommitteeActivtiesBean bean = new CommitteeActivtiesBean();

                bean.setId(rs.getInt("id"));
                bean.setTotal(rs.getInt("total"));
                bean.setSepplierName(rs.getString("SUPPLIER_NAME"));
                bean.setNegTitle(rs.getString("NEG_TITLE"));
                bean.setNegNum(rs.getString("NEG_NUM"));
                bean.setDocNum(rs.getInt("DOC_NUM"));
              
                list.add(bean);
            }
            arr = new JSONArray(list);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }


    public String addNegotiation(String  neg) throws SQLException {
        connection = AppsproConnection.getConnection();

        JSONArray arr = new JSONArray(neg);
        JSONObject obj = new JSONObject();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        for (int i = 0; i < arr.length(); i++) {
            obj = arr.getJSONObject(i);
            beanList.add(obj);
        }
        try {
            String query =
                "INSERT INTO XXX_COMMITTEE_ACTIVITIES(SUPPLIER_NAME,TOTAL,NEG_TITLE,NEG_NUM,doc_num) VALUES(?,?,?,?,?)";
            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
                ps.setInt(2, bean.getInt("total"));
                ps.setString(1, bean.getString("supplierName"));
                ps.setString(3, bean.getString("negTitle"));
                ps.setString(4, bean.getString("negNum"));
                ps.setInt(5, bean.getInt("docNum"));
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return "Negotiation add successfuly";
    }


}
