package com.appspro.nhcCommittee.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.nhcCommittee.bean.AttachmentBean;
import com.appspro.nhcCommittee.bean.EmployeeBean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;

public class EmployeesDAO extends RestHelper {
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public ArrayList<EmployeeBean> getAllEmployees() {
        ArrayList<EmployeeBean> employeesList = new ArrayList<EmployeeBean>();

        boolean hasMore = false;
        int offset = 0;
        do{
            
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getAllEmployeesUrl()+"&limit=200";
            System.out.println(serverUrl);
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
                
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization", "Basic " + "Ui5haG1lZEBuaGMuc2E6QXBwc0AxMjM=");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            
            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
            for (int i = 0; i < arr.length(); i++) {
                EmployeeBean bean = new EmployeeBean();
                JSONObject projectObj = arr.getJSONObject(i);
                bean.setDisplayName(projectObj.getString("DisplayName"));
                bean.setPersonNumber(projectObj.getString("PersonNumber"));
                bean.setPersonID(projectObj.getLong("PersonId"));
                bean.setUserName(projectObj.getString("UserName"));

                employeesList.add(bean);
            }
            
            
//              hasMore = obj.getBoolean("hasMore");
             offset += 200;
            

          } catch (Exception e) {
          }
        }while (hasMore);
        return employeesList;
    }
    public ArrayList<EmployeeBean> getEmployee(String userName) {
        ArrayList<EmployeeBean> employeesList = new ArrayList<EmployeeBean>();

        boolean hasMore = false;
        int offset = 0;
        do{
            
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getEmployeeUrl()+"q=UserName="+userName+"&expand=roles&onlyData=true";
            System.out.println(serverUrl);
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
                
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization", "Basic " + "Ui5haG1lZEBuaGMuc2E6QXBwc0AxMjM=");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            
            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
              for (int i = 0; i < arr.length(); i++) {
                EmployeeBean bean = new EmployeeBean();
                JSONObject projectObj = arr.getJSONObject(i);
                bean.setDisplayName(projectObj.getString("DisplayName"));
                bean.setPersonNumber(projectObj.getString("PersonNumber"));
                bean.setPersonID(projectObj.getLong("PersonId"));
                bean.setUserName(projectObj.getString("UserName"));
                bean.setRoles(projectObj.getJSONArray("roles").toString());

                employeesList.add(bean);
              }
          } catch (Exception e) {
          }
        }while (hasMore);
        return employeesList;
    }
}
