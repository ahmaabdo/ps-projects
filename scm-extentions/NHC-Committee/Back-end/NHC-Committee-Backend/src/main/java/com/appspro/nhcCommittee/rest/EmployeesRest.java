package com.appspro.nhcCommittee.rest;

import com.appspro.nhcCommittee.bean.AttachmentBean;
import com.appspro.nhcCommittee.bean.EmployeeBean;
import com.appspro.nhcCommittee.dao.AttachmentDAO;
import com.appspro.nhcCommittee.dao.EmployeeCommitteeDAO;
import com.appspro.nhcCommittee.dao.EmployeesDAO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/Empolyees")

public class EmployeesRest {
    JSONObject resposne = new JSONObject();

    EmployeeBean bean = new EmployeeBean();
    ArrayList<EmployeeBean> list =new ArrayList();
    EmployeeCommitteeDAO serivces = new EmployeeCommitteeDAO();
    JSONArray arr = new JSONArray();
    String status;
  
    @Path("/AllEmployees")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmployees(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        EmployeeBean obj = new EmployeeBean();
        EmployeesDAO emp = new EmployeesDAO();

        ArrayList<EmployeeBean> empList = emp.getAllEmployees();
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(empList);
        return Response.ok(new String(jsonInString.getBytes(), "UTF-8"), MediaType.APPLICATION_JSON).build();

    }
    
    @Path("/getEmployee/{userName}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmployee(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString,@PathParam("userName") String userName) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        EmployeeBean obj = new EmployeeBean();
        EmployeesDAO emp = new EmployeesDAO();

        ArrayList<EmployeeBean> empList = emp.getEmployee(userName);
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(empList);
        return Response.ok(new String(jsonInString.getBytes(), "UTF-8"), MediaType.APPLICATION_JSON).build();

    }
    
    @GET
    @Path("/getEmps/{negNum}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAEmployees(@PathParam("negNum") String negNum) {

        try {
            arr = serivces.getEmployees(negNum);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }
    
    @POST
    @Path("/insertEmp")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addEmployee(String body) {
        try {
            status = serivces.addEmployee(body);
            resposne.put("Status", "OK");
            resposne.put("Data", status);

        }

        catch (SQLException e) {

            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();

    }
}
