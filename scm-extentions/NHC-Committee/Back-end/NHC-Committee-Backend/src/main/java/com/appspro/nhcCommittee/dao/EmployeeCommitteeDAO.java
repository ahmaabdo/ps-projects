package com.appspro.nhcCommittee.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.nhcCommittee.bean.EmployeeBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class EmployeeCommitteeDAO extends AppsproConnection {
    Connection connection1;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public JSONArray getEmployees(String negNum) throws SQLException {
        JSONArray arr = new JSONArray();
        try {
            ArrayList<EmployeeBean> list =
                new ArrayList<EmployeeBean>();
            connection1 = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM XXX_COMMITTEE_EMPLOYEE where neg_num=?";
            ps = connection1.prepareStatement(query);
            ps.setString(1, negNum);
            rs = ps.executeQuery();
            while (rs.next()) {
                EmployeeBean bean = new EmployeeBean();
                bean.setFULL_NAME(rs.getString("EMPLOYEE_NAME"));
                bean.setId(rs.getInt("id"));
                bean.setPersonNumber(rs.getString("EMPLOYEE_NUMBER"));
                bean.setNegotiationNumber(rs.getString("NEG_NUM"));
                bean.setDocumentNumber(rs.getString("DOC_NUM"));
                bean.setCommDate(rs.getString("comm_date"));
                bean.setEMAIL_ADDRESS(rs.getString("email"));
              
                list.add(bean);
            }
            arr = new JSONArray(list);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection1, ps, rs);
        }

        return arr;
    }


    public String addEmployee(String  employee) throws SQLException {
        connection1 = AppsproConnection.getConnection();

        JSONArray arr = new JSONArray(employee);
        JSONObject obj = new JSONObject();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        for (int i = 0; i < arr.length(); i++) {
            obj = arr.getJSONObject(i);
            beanList.add(obj);
        }
        try {
            String query =
                "INSERT INTO XXX_COMMITTEE_EMPLOYEE(DOC_NUM,NEG_NUM,EMPLOYEE_NUMBER,EMPLOYEE_NAME,comm_date,email) VALUES(?,?,?,?,?,?)";
            ps = connection1.prepareStatement(query);
            for (JSONObject bean : beanList) {
                ps.setInt(1, bean.getInt("docNum"));
                ps.setString(2, bean.getString("negNum"));
                ps.setInt(3, bean.getInt("PERSON_NUMBER"));
                ps.setString(4, bean.getString("displayName"));
                ps.setString(5, bean.getString("date"));
                ps.setString(6, bean.getString("email"));

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection1, ps, rs);
        }

        return "Employee add successfuly";
    }
}
