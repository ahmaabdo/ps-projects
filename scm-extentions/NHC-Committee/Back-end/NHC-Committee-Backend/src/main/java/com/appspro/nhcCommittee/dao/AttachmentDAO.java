package com.appspro.nhcCommittee.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.nhcCommittee.bean.AttachmentBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class AttachmentDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public JSONArray getAttachmentByDocNum(String negNum) throws SQLException {
        JSONArray arr = new JSONArray();
        try {
            ArrayList<AttachmentBean> list =
                new ArrayList<AttachmentBean>();
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM XXX_COMMITTEE_ATTACHMENT WHERE  neg_num=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, negNum);
            rs = ps.executeQuery();

            while (rs.next()) {
                AttachmentBean attachmentBean = new AttachmentBean();

                attachmentBean.setTitle(rs.getString("TITLE"));
                attachmentBean.setDocNum(rs.getInt("DOC_NUM"));
                attachmentBean.setId(rs.getInt("id"));
                attachmentBean.setContent(rs.getString("CONTENT"));
                attachmentBean.setName(rs.getString("NAME"));
                attachmentBean.setNegTitle(rs.getString("NEG_TITLE"));
                attachmentBean.setNegNumber(rs.getString("NEG_NUM"));
              
                list.add(attachmentBean);
            }
            arr = new JSONArray(list);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }
    
    public JSONArray getAttachmentMaster(String negNum) throws SQLException {
        JSONArray arr = new JSONArray();
        try {
            ArrayList<AttachmentBean> list =
                new ArrayList<AttachmentBean>();
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM XXX_COMMITTEE_ATTACHMENT WHERE  neg_num=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, negNum);
            rs = ps.executeQuery();

            while (rs.next()) {
                AttachmentBean attachmentBean = new AttachmentBean();

                attachmentBean.setDocNum(rs.getInt("DOC_NUM"));
                attachmentBean.setContent(rs.getString("CONTENT"));
                attachmentBean.setName(rs.getString("NAME"));              
                list.add(attachmentBean);
            }
            arr = new JSONArray(list);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }


    public String addAttachment(String  attachment) throws SQLException {
        connection = AppsproConnection.getConnection();

        JSONArray arr = new JSONArray(attachment);
        JSONObject obj = new JSONObject();
        List<JSONObject> beanList = new ArrayList<JSONObject>();
        for (int i = 0; i < arr.length(); i++) {
            obj = arr.getJSONObject(i);
            beanList.add(obj);
        }
        try {
            String query =
                "INSERT INTO XXX_COMMITTEE_ATTACHMENT(DOC_NUM,TITLE,NAME,CONTENT,NEG_TITLE,NEG_NUM) VALUES(?,?,?,?,?,?)";
            ps = connection.prepareStatement(query);
            for (JSONObject bean : beanList) {
                ps.setInt(1, bean.getInt("docNum"));
                ps.setString(2, bean.getString("title"));
                ps.setString(3, bean.getString("name"));
                ps.setString(4, bean.getString("content"));
                ps.setString(5, bean.getString("negTitle"));
                ps.setString(6, bean.getString("negNum"));
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return "Attachment add successfuly";
    }


        
    public String deleteAttachment(int attachment) throws SQLException {

        try{
            connection = AppsproConnection.getConnection();
            String query;
      
                query = "DELETE FROM XXX_COMMITTEE_ATTACHMENT  WHERE id=?";
                ps= connection.prepareStatement(query);

                ps.setInt(1, attachment);
                 ps.executeUpdate();
            }
        
        catch (SQLException e){
            throw e;
        }
        finally {
            closeResources(connection, ps, rs);
            ps.close();
        }
        
        return "attachment Deleted Successfully";
    }
}
