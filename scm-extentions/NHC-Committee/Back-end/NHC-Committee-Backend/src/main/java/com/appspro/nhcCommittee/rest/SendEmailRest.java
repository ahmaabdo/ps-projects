package com.appspro.nhcCommittee.rest;

import com.appspro.mail.CredentialStoreClassMail;
import com.appspro.nhcCommittee.bean.EmployeeBean;
import com.appspro.nhcCommittee.dao.EmployeeCommitteeDAO;
import com.appspro.nhcCommittee.dao.EmployeesDAO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;

import java.sql.SQLException;

import java.util.ArrayList;

import javax.mail.MessagingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Path("email")
public class SendEmailRest {
    CredentialStoreClassMail mailObj = new CredentialStoreClassMail();

    @POST
    @Path("/sendEmail")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendEmail(String body) {
        JSONObject bodyObj = new JSONObject(body);
        JSONObject responseObj = new JSONObject();
        JSONArray empArr = bodyObj.getJSONArray("emps");
        JSONArray attachmentsArr = bodyObj.getJSONArray("attachments");
        for (int i = 0; i < empArr.length(); i++) {
            JSONObject obj = empArr.getJSONObject(i);
            obj.put("attachments", attachmentsArr);
            try {
                  
                mailObj.SEND_MAIL_LOCAL(obj.get("empEmail").toString()," Committee Extension", "Hello Dear," +
                "<body>"+"<br/>"+ "Kindly review for your negotaition details : " +"<br/>"+
                "<b>RFX Number :</b> "+bodyObj.get ("rfxNum").toString()+" , " +
                "<b>RFX Title :</b> "+bodyObj.get ("rfxTitle").toString()+" , " +
                ""+"<b>Procurement BU :</b> "+bodyObj.get ("bu").toString()+" , " +
                ""+"<b>Buyer :</b> "+bodyObj.get ("buyer").toString()+" , " +
                ""+"<b>Negotaition Status :</b> "+bodyObj.get ("NegoStatus").toString()+" , " +                              
                ""+"<b>Negotiation Style :</b> "+ bodyObj.get ("negoStyle").toString()+ 
                "</body>",
                obj.getJSONArray("attachments"));
                responseObj.put("Status", "OK");
                responseObj.put("Data", "Mail sent !!!");
            } catch (UnsupportedEncodingException e) {
                responseObj.put("Status", "ERROR");
                responseObj.put("Data", e.getMessage());
            } catch (MessagingException e) {
                responseObj.put("Status", "ERROR");
                responseObj.put("Data", e.getMessage());
            }
            catch (JSONException e) {
                 responseObj.put("Status", "ERROR");
                 responseObj.put("Data", e.getMessage());
            }
        }
        return Response.ok(responseObj.toString()).build();
    }
}

