package com.appspro.nhcCommittee.rest;

import com.appspro.nhcCommittee.bean.AttachmentBean;

import com.appspro.nhcCommittee.dao.AttachmentDAO;

import java.sql.SQLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/attachment")
public class AttachmentRest {
    JSONObject resposne = new JSONObject();

    AttachmentBean attachmentBean = new AttachmentBean();
    ArrayList<AttachmentBean> list =new ArrayList();
    AttachmentDAO serivces = new AttachmentDAO();
    JSONArray arr = new JSONArray();
    String status;


    @GET
    @Path("/getAttachment/{negNum}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAttatchment(@PathParam("negNum")
        String negNum) {

        try {
            arr = serivces.getAttachmentByDocNum(negNum);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }


    @POST
    @Path("/uploadAttachment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addNewAttachment(String body) {
        try {
            status = serivces.addAttachment(body);
            resposne.put("Status", "OK");
            resposne.put("Data", status);

        }

        catch (SQLException e) {

            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();

    }


    @GET
    @Path("/deleteAttachment/{docNum}")
    public String deleteAttachment(@PathParam("docNum")
        int docNum) {

        try {

            serivces.deleteAttachment(docNum);

            resposne.put("Status", "OK");
            resposne.put("Data", "attachment Deleted Successfully");

        } catch (Exception e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
}
