package com.appspro.mail;

import biPReports.RestHelper;

import com.sun.istack.internal.ByteArrayDataSource;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.activation.DataSource;

import org.apache.commons.codec.binary.Base64;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.JSONArray;
import org.json.JSONObject;


public class CredentialStoreClassMail extends RestHelper {
    static private String smtpAuthUserName =
        "fatmaemad105@gmail.com"; // gmail username
    static private String smtpAuthPassword = "commuter"; // gmail password
    static private String emailFrom =
        "fatmaemad105@gmail.com"; // gmail username

    public static void SEND_MAIL(String emailToEmp, String subject,
                                 String body) throws MessagingException,
                                                     UnsupportedEncodingException {
        String emailTo = emailToEmp;
        String emailBcc = "sherif.alaa@appspro-me.com";
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(smtpAuthUserName,
                                                  smtpAuthPassword);
            }
        };
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.host", "smtp.gmail.com");
        properties.setProperty("mail.smtp.port", "587");
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.ssl.enable", "false");
        properties.setProperty("mail.debug", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        Session session = Session.getInstance(properties, authenticator);
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(emailFrom, "NHC Commttiee"));
        InternetAddress[] to = { new InternetAddress(emailTo) };
        message.setRecipients(Message.RecipientType.TO, to);
        InternetAddress[] bcc = { new InternetAddress(emailBcc) };
        message.setRecipients(Message.RecipientType.BCC, bcc);
        message.setContent(body, "text/html; charset=utf-8");
        message.setSubject(subject);
        Transport.send(message);
    }

    public static void SEND_MAIL_LOCAL(String emailToEmp, String subject,
                                       String body,
                                       JSONArray attachments) throws MessagingException,
                                                                     UnsupportedEncodingException {
        String emailTo = emailToEmp;
        String emailBcc = "fatma.emad@appspro-me.com";
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(smtpAuthUserName,
                                                  smtpAuthPassword);
            }
        };
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.host", "smtp.gmail.com");
        properties.setProperty("mail.smtp.port", "587");
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.ssl.enable", "false");
        properties.setProperty("mail.debug", "true");
        Session session = Session.getInstance(properties, authenticator);
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(emailFrom, "NHC Committee"));
        InternetAddress[] to = { new InternetAddress(emailTo) };
        message.setRecipients(Message.RecipientType.TO, to);
        InternetAddress[] bcc = { new InternetAddress(emailBcc) };
        message.setRecipients(Message.RecipientType.BCC, bcc);
        message.setContent(body, "text/html; charset=utf-8");
        message.setSubject(subject);
        Multipart multipart = new MimeMultipart();
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        MimeBodyPart textBodyPart = new MimeBodyPart();
        textBodyPart.setContent(body, "text/html; charset=utf-8");
        multipart.addBodyPart(textBodyPart);
        for (int i = 0; i < attachments.length(); i++) {
            JSONObject ob = attachments.getJSONObject(i);
            String string =
                ob.getString("content").substring(ob.getString("content").indexOf("data:"),
                                                  ob.getString("content").indexOf("base64,") -
                                                  1);
            String contentType = string.substring(5);
            String base64Data =
                ob.getString("content").substring(ob.getString("content").lastIndexOf("base64,") +
                                                  7);
            byte[] imgBytes =
                Base64.decodeBase64(base64Data.getBytes(StandardCharsets.UTF_8));
            attachFile(imgBytes, contentType, ob.getString("title"), multipart,
                       new MimeBodyPart());
        }
        message.setContent(multipart);
        Transport.send(message);
    }

    public static void attachFile(byte[] imgBytes, String contentType,
                                  String filename, Multipart multipart,
                                  MimeBodyPart messageBodyPart) throws MessagingException {
        DataSource source = new ByteArrayDataSource(imgBytes, contentType);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);
        multipart.addBodyPart(messageBodyPart);
    }
}
