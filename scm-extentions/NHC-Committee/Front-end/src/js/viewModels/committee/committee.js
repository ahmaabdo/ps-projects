define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojcollapsible', 'ojs/ojselectcombobox',
    'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojselectsingle', 'ojs/ojbutton', 'ojs/ojtable', 'ojs/ojfilepicker',
     'ojs/ojpagingcontrol', 'ojs/ojpagingdataproviderview', 'ojs/ojvalidationgroup', 'ojs/ojdatetimepicker', 'ojs/ojlabelvalue', 'ojs/ojarraydataprovider'
], function (oj, ko, app, services, commonHelper) {

    function committeeContentViewModel() {
        var self = this;
        self.label = ko.observable();
        self.customsModel = {
            status: ko.observable(),
            negotiationStyle: ko.observable(),
            buyer: ko.observable(),
            procurementBU: ko.observable(),
            outcome: ko.observable(),
            afterNext: ko.observable(false),
            nextDisable: ko.observable(true),
            nextVisible: ko.observable(true),
            afterSubmitOpen: ko.observable(false),
            submitDisableOpen: ko.observable(true),
            submitDisableTech: ko.observable(true),
            submitDisableFin: ko.observable(true),
            submitDisableAward: ko.observable(true),
            defaultMemberDisableOpen: ko.observable(false),
            defaultMemberDisableTech: ko.observable(false),
            defaultMemberDisableFin: ko.observable(false),
            defaultMemberDisableAward: ko.observable(false),
            afterSubmitTech: ko.observable(false),
            afterSubmitFin: ko.observable(false),
            afterSubmitAward: ko.observable(false),
            openVisible: ko.observable(false),
            techVisible: ko.observable(false),
            finVisible: ko.observable(false),
            awardVisible: ko.observable(false),
            disRemoveTechBtn: ko.observable(false),
            resetDis: ko.observable(true),
            rfxTitle: ko.observable(),
            rfxColumns: ko.observableArray([]),
            rfxNumber: ko.observable(),
            rfxNumberArr: ko.observableArray([]),
            rfxArr: ko.observableArray([]),
            dateOpen: ko.observable(),
            minDate: ko.observable(formatDate(new Date())),
            employeeOpen: ko.observable(),
            employeeArr: ko.observableArray([]),
            memberColumnsOpen: ko.observableArray([]),
            membersArrOpen: ko.observableArray(),
            attachmentColumnsOpen: ko.observableArray([]),
            attachmentArrOpen: ko.observableArray([]),
            dateTech: ko.observable(),
            employeeTech: ko.observable(),
            memberColumnsTech: ko.observableArray([]),
            membersArrTech: ko.observableArray(),
            attachmentColumnsTech: ko.observableArray([]),
            attachmentArrTech: ko.observableArray([]),
            dateFin: ko.observable(),
            employeeFin: ko.observable(),
            memberColumnsFin: ko.observableArray([]),
            membersArrFin: ko.observableArray(),
            attachmentColumnsFin: ko.observableArray([]),
            attachmentArrFin: ko.observableArray([]),
            dateAward: ko.observable(),
            employeeAward: ko.observable(),
            memberColumnsAward: ko.observableArray([]),
            membersArrAward: ko.observableArray(),
            attachmentColumnsAward: ko.observableArray([]),
            attachmentArrAward: ko.observableArray([]),
            fileData: ko.observable(),
            attTitle: ko.observable(),
            attName: ko.observable(),
            contentData: ko.observable(),
            payloadAttachOpen: ko.observableArray([]),
            payloadAttachTech: ko.observableArray([]),
            payloadAttachFin: ko.observableArray([]),
            payloadAttachAward: ko.observableArray([]),
            selectedFiles: ko.observableArray(),
            rowAttachSelectedOpen: ko.observable(),
            rowAttachSelectedTech: ko.observable(),
            rowAttachSelectedFin: ko.observable(),
            rowAttachSelectedAward: ko.observable()
        };
        self.groupValid = ko.observable();
        self.dataProviderRfx = ko.observable();
        self.dataProviderRfx(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.rfxArr)));

        self.dataProviderMembersOpen = ko.observable();
        self.dataProviderMembersOpen(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.membersArrOpen)));
        self.dataProviderAttachmentOpen = ko.observable();
        self.dataProviderAttachmentOpen(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrOpen)));

        self.dataProviderMembersTech = ko.observable();
        self.dataProviderMembersTech(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.membersArrTech)));
        self.dataProviderAttachmentTech = ko.observable();
        self.dataProviderAttachmentTech(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrTech)));

        self.dataProviderMembersFin = ko.observable();
        self.dataProviderMembersFin(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.membersArrFin)));
        self.dataProviderAttachmentFin = ko.observable();
        self.dataProviderAttachmentFin(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrFin)));

        self.dataProviderMembersAward = ko.observable();
        self.dataProviderMembersAward(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.membersArrAward)));
        self.dataProviderAttachmentAward = ko.observable();
        self.dataProviderAttachmentAward(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrAward)));

        self.defaultMembersOpen = ko.observableArray([]);
        self.defaultMembersTech = ko.observableArray([]);
        self.defaultMembersFin = ko.observableArray([]);
        self.defaultMembersAward = ko.observableArray([]);
        self.connected = function () {
            initTranslations();
        };

        ko.computed(_ => {
            if (app.employeeArray()) {
                app.employeeArray().forEach(e => self.customsModel.employeeArr.push({ label: e.displayName, value: e.displayName }));
            }
            if (app.dataNegotiation()) {
                app.dataNegotiation().forEach(e => self.customsModel.rfxNumberArr.push({ label: e.DOCUMENT_NUMBER, value: e.DOCUMENT_NUMBER }));
            }
        });

        self.rfxNumberValueChange = function (event) {
            var rfxNumber = event.detail.value;
            app.dataNegotiation().forEach(obj => {
                obj.G_5 = [{FULL_NAME: "Ibrahim Aldossari", EMAIL_ADDRESS: "i.aldossari@nhc.sa", PERSON_NUMBER: "1823"},
                    {FULL_NAME: "Abdulelah Aldhahri", EMAIL_ADDRESS: "a.aldhahri@nhc.sa", PERSON_NUMBER: "1874"},
                    {FULL_NAME: "Muath Alanazi", EMAIL_ADDRESS: "mh.alanazi@nhc.sa", PERSON_NUMBER: "1916"},
                    {FULL_NAME: "Khalid AlMughaysib", EMAIL_ADDRESS: "k.almughaysib@nhc.sa", PERSON_NUMBER: "1058"}];

                if (obj.DOCUMENT_NUMBER === rfxNumber) {
                    self.customsModel.rfxTitle(obj.AUCTION_TITLE);
                    self.customsModel.status(obj.AUCTION_STATUS);
                    self.customsModel.negotiationStyle(obj.STYLE_NAME);
                    self.customsModel.buyer(obj.BUYER_NAME);
                    self.customsModel.procurementBU(obj.NAME);
                    self.customsModel.outcome(obj.DISPLAY_NAME);
                    if (obj.G_4.length > 0) { // need to check if obj.G4 is object not array.
                        self.customsModel.rfxArr([]);
                        self.customsModel.rfxArr(obj.G_4);
                    } // need to check if items is object not array.
                    self.defaultMembersOpen(self.checkDataArr([obj][0].Items, "Open Committee - اعضاء اللجنة المالية"));
                    self.defaultMembersTech(self.checkDataArr([obj][0].Items, "Technical Committee - اعضاء اللجنة الفنية"));
                    self.defaultMembersFin(self.checkDataArr([obj][0].Items, "Financial Committee - اعضاء اللجنة المالية"));
                    self.defaultMembersAward(self.checkDataArr([obj][0].Items, "Commercial Committee - اعضاء اللجنة المالية"));
                    
//                    obj.Items.forEach(e => {
//                        if (!e.G_3) {
//                            self.defaultMembersOpen.push(e);
//                            self.defaultMembersTech.push(e);
//                            self.defaultMembersFin.push(e);
//                            self.defaultMembersAward.push(e);
//                        }
//                    });

                    if (obj.G_5.length > 0) {
                        obj.G_5.forEach(e => {
                            self.defaultMembersOpen.push(e);
                            self.defaultMembersTech.push(e);
                            self.defaultMembersFin.push(e);
                            self.defaultMembersAward.push(e);
                        });
                    }
                }
            });

            if (self.customsModel.rfxNumber() !== null) {
                self.customsModel.resetDis(false);
                self.customsModel.nextDisable(false);
            };
        };

        self.checkDataArr = function (arr, checkValue) {
            return arr.filter(el => {
                let map = Array.isArray(el.G_3) ? el.G_3 : [el.G_3]
                return map.find(a => a.TEAM_NAME === checkValue)
            });
        };

        self.getNegotiationActivity = function () {
            app.loading(true);
            services.getGeneric(commonHelper.getNegData + self.customsModel.rfxNumber().split('/').join('-')).then(data => {
                if (data.Data) {
                    if (data.Data.attachment.length > 0) {
                        self.customsModel.selectedFiles(data.Data.attachment);
                        var dataOpen = self.customsModel.selectedFiles().filter(e => e.docNum === 1);
                        var dataTech = self.customsModel.selectedFiles().filter(e => e.docNum === 2);
                        var dataFin = self.customsModel.selectedFiles().filter(e => e.docNum === 3);
                        var dataAward = self.customsModel.selectedFiles().filter(e => e.docNum === 4);
                        if (dataOpen.length > 0 || dataTech.length > 0 || dataFin.length > 0 || dataAward.length > 0) {
                            dataOpen.forEach(e => self.customsModel.attachmentArrOpen.push(e));
                            dataTech.forEach(e => self.customsModel.attachmentArrTech.push(e));
                            dataFin.forEach(e => self.customsModel.attachmentArrFin.push(e));
                            dataAward.forEach(e => self.customsModel.attachmentArrAward.push(e));
                        }
                    }

                    if (data.Data.employees.length > 0) {
                        self.customsModel.membersArrOpen(data.Data.employees.filter(e => e.documentNumber === "1"));
                        self.customsModel.membersArrTech(data.Data.employees.filter(e => e.documentNumber === "2"));
                        self.customsModel.membersArrFin(data.Data.employees.filter(e => e.documentNumber === "3"));
                        self.customsModel.membersArrAward(data.Data.employees.filter(e => e.documentNumber === "4"));
                        self.customsModel.dateOpen(data.Data.employees.filter(e => e.documentNumber === "1")[0] ? data.Data.employees.filter(e => e.documentNumber === "1")[0].commDate : "");
                        self.customsModel.dateTech(data.Data.employees.filter(e => e.documentNumber === "2")[0] ? data.Data.employees.filter(e => e.documentNumber === "2")[0].commDate : "");
                        self.customsModel.dateFin(data.Data.employees.filter(e => e.documentNumber === "3")[0] ? data.Data.employees.filter(e => e.documentNumber === "3")[0].commDate : "");
                        self.customsModel.dateAward(data.Data.employees.filter(e => e.documentNumber === "4")[0] ? data.Data.employees.filter(e => e.documentNumber === "4")[0].commDate : "");
                    }
                    
                    if (app.roleNumber() === "1") {
                        self.customsModel.openVisible(true);
                        if (self.customsModel.membersArrOpen().length > 0) {
                            self.customsModel.afterSubmitOpen(true);
                            self.customsModel.defaultMemberDisableOpen(true);
                        } else {
                            self.customsModel.afterSubmitOpen(false);
                            self.customsModel.defaultMemberDisableOpen(false);
                        }
                    } else if (app.roleNumber() === "2") {
                        if (self.customsModel.membersArrOpen().length > 0) {
                            self.customsModel.openVisible(true);
                            self.customsModel.techVisible(true);
                            self.customsModel.afterSubmitOpen(true);
                            self.customsModel.defaultMemberDisableOpen(true);
                            if (self.customsModel.membersArrTech().length > 0) {
                                self.customsModel.defaultMemberDisableTech(true);
                                self.customsModel.afterSubmitTech(true);
                                self.customsModel.disRemoveTechBtn(true);
                            } else {
                                self.customsModel.defaultMemberDisableTech(false);
                                self.customsModel.afterSubmitTech(false);
                                self.customsModel.disRemoveTechBtn(false);
                            }
                        } else {
                            self.customsModel.openVisible(false);
                        }
                    } else if (app.roleNumber() === "3") {
                        if (self.customsModel.membersArrOpen().length > 0) {
                            self.customsModel.openVisible(true);
                            self.customsModel.afterSubmitOpen(true);
                            self.customsModel.defaultMemberDisableOpen(true);
                            if (self.customsModel.membersArrTech().length > 0) {
                                self.customsModel.techVisible(true);
                                self.customsModel.finVisible(true);
                                self.customsModel.defaultMemberDisableTech(true);
                                self.customsModel.afterSubmitTech(true);
                                self.customsModel.disRemoveTechBtn(true);
                                if (self.customsModel.membersArrFin().length > 0) {
                                    self.customsModel.defaultMemberDisableFin(true);
                                    self.customsModel.afterSubmitFin(true);
                                } else {
                                    self.customsModel.defaultMemberDisableFin(false);
                                    self.customsModel.afterSubmitFin(false);
                                }
                            } else {
                                self.customsModel.techVisible(false);
                            }
                        } else {
                            self.customsModel.openVisible(false);
                        }
                    } else if (app.roleNumber() === "4") {
                        if (self.customsModel.membersArrOpen().length > 0) {
                            self.customsModel.openVisible(true);
                            self.customsModel.afterSubmitOpen(true);
                            self.customsModel.defaultMemberDisableOpen(true);
                            if (self.customsModel.membersArrTech().length > 0) {
                                self.customsModel.techVisible(true);
                                self.customsModel.defaultMemberDisableTech(true);
                                self.customsModel.afterSubmitTech(true);
                                self.customsModel.disRemoveTechBtn(true);
                                if (self.customsModel.membersArrFin().length > 0) {
                                    self.customsModel.finVisible(true);
                                    self.customsModel.awardVisible(true);
                                    self.customsModel.defaultMemberDisableFin(true);
                                    self.customsModel.afterSubmitFin(true);
                                    if (self.customsModel.membersArrAward().length > 0) {
                                        self.customsModel.afterSubmitAward(true);
                                        self.customsModel.defaultMemberDisableAward(true);
                                    } else {
                                        self.customsModel.afterSubmitAward(false);
                                        self.customsModel.defaultMemberDisableAward(false);
                                    }
                                } else {
                                    self.customsModel.finVisible(false);
                                }
                            } else {
                                self.customsModel.techVisible(false);
                            }
                        } else {
                            self.customsModel.openVisible(false);
                        }
                    } else {
                        self.customsModel.openVisible(false);
                    }
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
            });
        };

        self.nextBtn = function () {
            if (!self.customsModel.rfxNumber()) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select an item'));
                return;
            }
            self.customsModel.afterNext(true);
            self.customsModel.nextDisable(true);
            self.getNegotiationActivity();
        };

        self.resetBtn = function () {
            self.resetFunction();
        };

        self.resetFunction = function () {
            self.customsModel.rfxNumber('');
            self.customsModel.rfxTitle('');
            self.customsModel.rfxArr([]);
            self.customsModel.nextDisable(true);
            self.customsModel.openVisible(false);
            self.customsModel.techVisible(false);
            self.customsModel.finVisible(false);
            self.customsModel.awardVisible(false);
            self.customsModel.afterNext(false);
            self.customsModel.membersArrOpen([]);
            self.customsModel.membersArrTech([]);
            self.customsModel.membersArrFin([]);
            self.customsModel.membersArrAward([]);
            self.customsModel.attachmentArrOpen([]);
            self.customsModel.attachmentArrTech([]);
            self.customsModel.attachmentArrFin([]);
            self.customsModel.attachmentArrAward([]);
            self.customsModel.payloadAttachOpen([]);
            self.customsModel.payloadAttachTech([]);
            self.customsModel.payloadAttachFin([]);
            self.customsModel.payloadAttachAward([]);
            self.customsModel.status('');
            self.customsModel.negotiationStyle('');
            self.customsModel.buyer('');
            self.customsModel.procurementBU('');
            self.customsModel.outcome('');
        };

        self.addEmployee = function (empArr) {
            app.loading(true);
            services.postGeneric(commonHelper.insertEmployee, empArr).then(data => {
                app.loading(false);
            }, error => { app.loading(false); });
        };

        var payloadEmailOpen = {};


        self.emailPayloadOpen = ko.observableArray([]);
        self.attachPayloadOpen = ko.observableArray([]);
        self.emailPayloadTech = ko.observableArray([]);
        self.attachPayloadTech = ko.observableArray([]);
        self.emailPayloadFin = ko.observableArray([]);
        self.attachPayloadFin = ko.observableArray([]);
        self.emailPayloadAward = ko.observableArray([]);
        self.attachPayloadAward = ko.observableArray([]);

        self.sendMail = function (payload) {
            app.loading(true);
            services.postGeneric(commonHelper.sendMail, payload).then(data => {
                app.loading(false);
            }, error => { app.loading(false); });
        };
        
        self.notifyBtnOpen = function () {
            var tracker = document.getElementById("tracker1");
            if (tracker.valid === "valid") {
                self.customsModel.defaultMemberDisableOpen(true);
                self.customsModel.afterSubmitOpen(true);
                self.customsModel.submitDisableOpen(true);
                self.emailPayloadOpen(self.customsModel.membersArrOpen().map(e => ({ empEmail: "ibrahim.hassan@appspro-me.com" })));
                self.attachPayloadOpen(self.customsModel.attachmentArrOpen().map(e => ({ title: e.title, content: e.content })));

                payloadEmailOpen = {
                    rfxNum: self.customsModel.rfxNumber(),
                    rfxTitle: self.customsModel.rfxTitle(),
                    NegoStatus: self.customsModel.status(),
                    buyer: self.customsModel.buyer(),
                    negoStyle: self.customsModel.negotiationStyle(),
                    bu: self.customsModel.procurementBU(),
                    emps: self.emailPayloadOpen(),
                    attachments: self.attachPayloadOpen()
                };
                var payloadOpen = [];
                if (self.customsModel.membersArrOpen().length > 0) {
                    self.customsModel.membersArrOpen().forEach(e => payloadOpen.push({
                            negNum: self.customsModel.rfxNumber().split('/').join('-'),
                            docNum: 1,
                            displayName: e.FULL_NAME,
                            PERSON_NUMBER: e.PERSON_NUMBER,
                            date: self.customsModel.dateOpen(),
                            email: e.EMAIL_ADDRESS
                        }));
                    if (self.customsModel.payloadAttachOpen().length > 0) {
                        notifyAndSubmit(payloadOpen, self.customsModel.payloadAttachOpen(), payloadEmailOpen);
                    }else{
                        app.messagesDataProvider.push(app.createMessage('error', 'Please Add Attachment !!'));
                        self.customsModel.defaultMemberDisableOpen(false);
                        self.customsModel.afterSubmitOpen(false);
                        self.customsModel.submitDisableOpen(false);
                    }
                }else{
                    app.messagesDataProvider.push(app.createMessage('error', 'Please Add Members !!'));
                    self.customsModel.defaultMemberDisableOpen(false);
                    self.customsModel.afterSubmitOpen(false);
                    self.customsModel.submitDisableOpen(false);
                }
                return true;
            } else {
                app.messagesDataProvider.push(app.createMessage('error', 'Please Enter Date'));
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return false;
            }
        };

        self.uploadContarctAttachment = function (payload) {
            app.loading(true);
            services.postGeneric(commonHelper.insertAttachment, payload).then(data => {
                app.loading(false);
            }, error => { app.loading(false); });
        };

        self.addMemberBtnOpen = function () {
            if (self.customsModel.employeeOpen()) {
                self.customsModel.membersArrOpen.push({
                    EMAIL_ADDRESS: app.employeeArray().find(e => e.displayName === self.customsModel.employeeOpen()).userName, FULL_NAME: self.customsModel.employeeOpen()
                    , negotiationNumber: self.customsModel.rfxNumber().split('/').join('-'), documentNumber: "1", PERSON_NUMBER: app.employeeArray().find(e => e.displayName === self.customsModel.employeeOpen()).personNumber
                });
            }
        };
        self.defaultMemberBtnOpen = function () {
            self.customsModel.membersArrOpen([]);
            if (self.defaultMembersOpen().length > 0) {
                self.customsModel.membersArrOpen(self.defaultMembersOpen());
            } else {
                app.messagesDataProvider.push(app.createMessage('warning', 'No default Members'));
                //                self.customsModel.defaultMemberDisableOpen(false);
            }
            //            self.customsModel.defaultMemberDisableOpen(true);
        };

        self.saveForLaterOpenBtn = function () {

        };

        self.resetBtnOpen = function () {
            self.customsModel.membersArrOpen([]);
            self.customsModel.employeeOpen('');
            self.customsModel.dateOpen('');
            self.customsModel.defaultMemberDisableOpen(false);
        };

        self.addFileSelectListenerOpen = function (event) {
            var x = {};
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function (evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name);
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0]);
                x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "negNum": self.customsModel.rfxNumber().split('/').join('-'),
                    "negTitle": self.customsModel.rfxTitle(),
                    "content": self.customsModel.contentData(),
                    "docNum": 1
                };
                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name) {
                            self.customsModel.payloadAttachOpen().push(x);
                            x = {};
                            self.customsModel.attachmentArrOpen(self.customsModel.payloadAttachOpen());
                            self.dataProviderAttachmentOpen(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrOpen)));
                        }
                    }
                });
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
            self.customsModel.selectedFiles(self.customsModel.fileData());
        };

        self.attachmentselectListenerOpen = function (event) {
            var data = event.detail;
            if (!data)
                return;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowAttachSelectedOpen(self.customsModel.attachmentArrOpen()[[currentRow['rowIndex']]]);
            }
        };

        self.removeFileFnOpen = function () {
            removeFile(self.customsModel.attachmentArrOpen(),self.customsModel.rowAttachSelectedOpen());
            self.dataProviderAttachmentOpen(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrOpen)));
        };

        self.notifyBtnTech = function () {
            var tracker = document.getElementById("tracker2");
            if (tracker.valid === "valid") {
                self.customsModel.afterSubmitTech(true);
                self.customsModel.submitDisableTech(true);
                self.customsModel.defaultMemberDisableTech(true);
                self.customsModel.disRemoveTechBtn(true);
                self.emailPayloadTech(self.customsModel.membersArrTech().map(e => ({ empEmail: "ibrahim.hassan@appspro-me.com" })));
                self.attachPayloadTech(self.customsModel.attachmentArrTech().map(e => ({ title: e.title, content: e.content })));

                var payloadEmailTech = {
                    rfxNum: self.customsModel.rfxNumber(),
                    rfxTitle: self.customsModel.rfxTitle(),
                    NegoStatus: self.customsModel.status(),
                    buyer: self.customsModel.buyer(),
                    negoStyle: self.customsModel.negotiationStyle(),
                    bu: self.customsModel.procurementBU(),
                    emps: self.emailPayloadTech(),
                    attachments: self.attachPayloadTech()
                };

                var payloadTech = [];
                if (self.customsModel.membersArrTech().length > 0) {
                    self.customsModel.membersArrTech().forEach(e => payloadTech.push({
                            negNum: self.customsModel.rfxNumber().split('/').join('-'),
                            docNum: 2,
                            displayName: e.FULL_NAME,
                            PERSON_NUMBER: e.PERSON_NUMBER,
                            date: self.customsModel.dateTech(),
                            email: e.EMAIL_ADDRESS
                        }));
                    if (self.customsModel.payloadAttachTech().length > 0) {
                        notifyAndSubmit(payloadTech, self.customsModel.payloadAttachTech(), payloadEmailTech);
                    } else {
                        app.messagesDataProvider.push(app.createMessage('error', 'Please Add Attachment !!'));
                        self.customsModel.afterSubmitTech(false);
                        self.customsModel.submitDisableTech(false);
                        self.customsModel.defaultMemberDisableTech(false);
                        self.customsModel.disRemoveTechBtn(false);
                    }
                } else {
                    app.messagesDataProvider.push(app.createMessage('error', 'Please Add Members !!'));
                    self.customsModel.afterSubmitTech(false);
                    self.customsModel.submitDisableTech(false);
                    self.customsModel.defaultMemberDisableTech(false);
                    self.customsModel.disRemoveTechBtn(false);
                }
                return true;
            } else {
                app.messagesDataProvider.push(app.createMessage('error', 'Please Enter Date'));
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return false;
            }
        };

        self.saveForLaterTechBtn = function () {

        };

        self.resetBtnTech = function () {
            self.customsModel.membersArrTech([]);
            self.customsModel.employeeTech('');
            self.customsModel.dateTech('');
            self.customsModel.defaultMemberDisableTech(false);
        };

        self.addMemberBtnTech = function () {
            if (self.customsModel.employeeTech())
                self.customsModel.membersArrTech.push({
                    EMAIL_ADDRESS: app.employeeArray().find(e => e.displayName == self.customsModel.employeeTech()).userName, FULL_NAME: self.customsModel.employeeTech()
                    , negotiationNumber: self.customsModel.rfxNumber().split('/').join('-'), documentNumber: "2", PERSON_NUMBER: app.employeeArray().find(e => e.displayName === self.customsModel.employeeTech()).personNumber
                });
        };
        self.defaultMemberBtnTech = function () {
            self.customsModel.membersArrTech([]);
            if (self.defaultMembersTech().length > 0) {
                self.customsModel.membersArrTech(self.defaultMembersTech());
            } else {
                app.messagesDataProvider.push(app.createMessage('warning', 'No default Members'));
                //                self.customsModel.defaultMemberDisableTech(false);
            }
            //            self.customsModel.defaultMemberDisableTech(true);
        };
        self.addFileSelectListenerTech = function (event) {
            var x = {};
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function (evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name);
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0]);
                x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "negNum": self.customsModel.rfxNumber().split('/').join('-'),
                    "negTitle": self.customsModel.rfxTitle(),
                    "content": self.customsModel.contentData(),
                    "docNum": 2
                };
                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name) {
                            self.customsModel.payloadAttachTech().push(x);
                            x = {};
                            self.customsModel.attachmentArrTech(self.customsModel.payloadAttachTech());
                            self.dataProviderAttachmentTech(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrTech)));
                        }
                    }
                });
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
            self.customsModel.selectedFiles(self.customsModel.fileData());
        };
        self.attachmentselectListenerTech = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowAttachSelectedTech(self.customsModel.attachmentArrTech()[[currentRow['rowIndex']]]);
            }
        };

        self.removeFileFnTech = function () {
            removeFile(self.customsModel.attachmentArrTech(),self.customsModel.rowAttachSelectedTech());
            self.dataProviderAttachmentTech(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrTech)));
        };

        self.notifyBtnFin = function () {
            var tracker = document.getElementById("tracker3");
            if (tracker.valid === "valid") {
                self.customsModel.afterSubmitFin(true);
                self.customsModel.submitDisableFin(true);
                self.customsModel.defaultMemberDisableFin(true);
                var payloadFin = [];
                self.emailPayloadFin(self.customsModel.membersArrFin().map(e => ({ empEmail: "ibrahim.hassan@appspro-me.com" })));
                self.attachPayloadFin(self.customsModel.attachmentArrFin().map(e => ({ title: e.title, content: e.content })));

                var payloadEmailFin = {
                    rfxNum: self.customsModel.rfxNumber(),
                    rfxTitle: self.customsModel.rfxTitle(),
                    NegoStatus: self.customsModel.status(),
                    buyer: self.customsModel.buyer(),
                    negoStyle: self.customsModel.negotiationStyle(),
                    bu: self.customsModel.procurementBU(),
                    emps: self.emailPayloadFin(),
                    attachments: self.attachPayloadFin()
                };
                if (self.customsModel.membersArrFin().length > 0) {
                    self.customsModel.membersArrFin().forEach(e => payloadFin.push({
                            negNum: self.customsModel.rfxNumber().split('/').join('-'),
                            docNum: 3,
                            displayName: e.FULL_NAME,
                            PERSON_NUMBER: e.PERSON_NUMBER,
                            date: self.customsModel.dateFin(),
                            email: e.EMAIL_ADDRESS
                        }));
                    if (self.customsModel.payloadAttachFin().length > 0) {
                        notifyAndSubmit(payloadFin, self.customsModel.payloadAttachFin(), payloadEmailFin);
                    } else {
                        app.messagesDataProvider.push(app.createMessage('error', 'Please Add Attachment !!'));
                        self.customsModel.afterSubmitFin(false);
                        self.customsModel.submitDisableFin(false);
                        self.customsModel.defaultMemberDisableFin(false);
                    }
                } else {
                    app.messagesDataProvider.push(app.createMessage('error', 'Please Add Members !!'));
                    self.customsModel.afterSubmitFin(false);
                    self.customsModel.submitDisableFin(false);
                    self.customsModel.defaultMemberDisableFin(false);
                }
                return true;
            } else {
                app.messagesDataProvider.push(app.createMessage('error', 'Please Enter Date'));
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return false;
            }
        };

        self.saveForLaterFinBtn = function () {

        };

        self.resetBtnFin = function () {
            self.customsModel.membersArrFin([]);
            self.customsModel.employeeFin('');
            self.customsModel.dateFin('');
            self.customsModel.defaultMemberDisableTech(false);
        };

        self.addMemberBtnFin = function () {
            if (self.customsModel.employeeFin())
                self.customsModel.membersArrFin.push({
                    EMAIL_ADDRESS: app.employeeArray().find(e => e.displayName == self.customsModel.employeeFin()).userName, FULL_NAME: self.customsModel.employeeFin()
                    , negotiationNumber: self.customsModel.rfxNumber().split('/').join('-'), documentNumber: "3", PERSON_NUMBER: app.employeeArray().find(e => e.displayName === self.customsModel.employeeFin()).personNumber
                });
        };
        self.defaultMemberBtnFin = function () {
            self.customsModel.membersArrFin([]);
            if (self.defaultMembersFin().length > 0) {
                self.customsModel.membersArrFin(self.defaultMembersFin());
            } else {
                app.messagesDataProvider.push(app.createMessage('warning', 'No default Members'));
                //                self.customsModel.defaultMemberDisableFin(false);
            }
            //            self.customsModel.defaultMemberDisableFin(true);
        };
        self.addFileSelectListenerFin = function (event) {
            var x = {};
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function (evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name);
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0]);
                x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "negNum": self.customsModel.rfxNumber().split('/').join('-'),
                    "negTitle": self.customsModel.rfxTitle(),
                    "content": self.customsModel.contentData(),
                    "docNum": 3
                };
                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name) {
                            self.customsModel.payloadAttachFin().push(x);
                            x = {};
                            self.customsModel.attachmentArrFin(self.customsModel.payloadAttachFin());
                            self.dataProviderAttachmentFin(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrFin)));
                        }
                    }
                });
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
            self.customsModel.selectedFiles(self.customsModel.fileData());
        };
        self.attachmentselectListenerFin = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowAttachSelectedFin(self.customsModel.attachmentArrFin()[[currentRow['rowIndex']]]);
            }
        };

        self.removeFileFnFin = function () {
            removeFile(self.customsModel.attachmentArrFin(),self.customsModel.rowAttachSelectedFin());
            self.dataProviderAttachmentFin(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrFin)));
        };

        self.notifyBtnAward = function () {
            var tracker = document.getElementById("tracker4");
            if (tracker.valid === "valid") {
                self.customsModel.submitDisableAward(true);
                self.customsModel.afterSubmitAward(true);
                self.customsModel.defaultMemberDisableAward(true);
                var payloadAward = [];
                self.emailPayloadAward(self.customsModel.membersArrAward().map(e => ({ empEmail: "ibrahim.hassan@appspro-me.com" })));
                self.attachPayloadAward(self.customsModel.attachmentArrAward().map(e => ({ title: e.title, content: e.content })));

                var payloadEmailAward = {
                    rfxNum: self.customsModel.rfxNumber(),
                    rfxTitle: self.customsModel.rfxTitle(),
                    NegoStatus: self.customsModel.status(),
                    buyer: self.customsModel.buyer(),
                    negoStyle: self.customsModel.negotiationStyle(),
                    bu: self.customsModel.procurementBU(),
                    emps: self.emailPayloadAward(),
                    attachments: self.attachPayloadAward()
                };
                if (self.customsModel.membersArrAward().length > 0) {
                    self.customsModel.membersArrAward().forEach(e => payloadAward.push({
                            negNum: self.customsModel.rfxNumber().split('/').join('-'),
                            docNum: 4,
                            displayName: e.FULL_NAME,
                            PERSON_NUMBER: e.PERSON_NUMBER,
                            date: self.customsModel.dateAward(),
                            email: e.EMAIL_ADDRESS
                        }));
                    if (self.customsModel.payloadAttachAward().length > 0) {
                        notifyAndSubmit(payloadAward, self.customsModel.payloadAttachAward(), payloadEmailAward);
                    } else {
                        app.messagesDataProvider.push(app.createMessage('error', 'Please Add Attachment !!'));
                        self.customsModel.afterSubmitAward(false);
                        self.customsModel.submitDisableAward(false);
                        self.customsModel.defaultMemberDisableAward(false);
                    }
                } else {
                    app.messagesDataProvider.push(app.createMessage('error', 'Please Add Members !!'));
                    self.customsModel.afterSubmitAward(false);
                    self.customsModel.submitDisableAward(false);
                    self.customsModel.defaultMemberDisableAward(false);
                }
                return true;
            } else {
                app.messagesDataProvider.push(app.createMessage('error', 'Please Enter Date'));
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return false;
            }
        };

        self.saveForLaterAwardBtn = function () {

        };

        self.resetBtnAward = function () {
            self.customsModel.membersArrAward([]);
            self.customsModel.employeeAward('');
            self.customsModel.dateAward('');
            self.customsModel.defaultMemberDisableAward(false);
        };

        self.addMemberBtnAward = function () {
            if (self.customsModel.employeeAward())
                self.customsModel.membersArrAward.push({
                    EMAIL_ADDRESS: app.employeeArray().find(e => e.displayName == self.customsModel.employeeAward()).userName, FULL_NAME: self.customsModel.employeeAward()
                    , negotiationNumber: self.customsModel.rfxNumber().split('/').join('-'), documentNumber: "4", PERSON_NUMBER: app.employeeArray().find(e => e.displayName === self.customsModel.employeeAward()).personNumber
                });
        };
        self.defaultMemberBtnAward = function () {
            self.customsModel.membersArrAward([]);
            if (self.defaultMembersAward().length > 0) {
                self.customsModel.membersArrAward(self.defaultMembersAward());
            }
            else {
                app.messagesDataProvider.push(app.createMessage('warning', 'No default Members'));
                //                self.customsModel.defaultMemberDisableAward(false);
            }
            //            self.customsModel.defaultMemberDisableAward(true);
        };
        self.addFileSelectListenerAward = function (event) {
            var x = {};
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function (evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name);
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0]);
                x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "negNum": self.customsModel.rfxNumber().split('/').join('-'),
                    "negTitle": self.customsModel.rfxTitle(),
                    "content": self.customsModel.contentData(),
                    "docNum": 4
                };
                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name) {
                            self.customsModel.payloadAttachAward().push(x);
                            x = {};
                            self.customsModel.attachmentArrAward(self.customsModel.payloadAttachAward());
                            self.dataProviderAttachmentAward(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.payloadAttachAward)));
                        }
                    }
                });
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
            self.customsModel.selectedFiles(self.customsModel.fileData());
        };
        self.attachmentselectListenerAward = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowAttachSelectedAward(self.customsModel.attachmentArrAward()[[currentRow['rowIndex']]]);
            }
        };

        self.removeFileFnAward = function () {
            removeFile(self.customsModel.attachmentArrAward(),self.customsModel.rowAttachSelectedAward());
            self.dataProviderAttachmentAward(new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.attachmentArrAward)));
        };
        
        function notifyAndSubmit(empArr,attachArr,mailArr){
            self.addEmployee(empArr);
            self.uploadContarctAttachment(attachArr);
            self.sendMail(mailArr);  
        };

        function removeFile (arr,row){
            if (row.length < 1) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select an item'));
                return;
            }
            var x = arr.find((el => el.name === row.name));
            arr.splice(x, 1);        
        };

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1), day = '' + date.getDate(), year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        var getTranslations = oj.Translations.getTranslatedString;

        function initTranslations() {
            self.label({
                screenLbl: getTranslations("committee.screenLbl"),
                rfxTitle: getTranslations("committee.rfxTitle"),
                openCommitteeLbl: getTranslations("committee.openCommitteeLbl"),
                technicalEvaluationLbl: getTranslations("committee.technicalEvaluationLbl"),
                financialEvaluationLbl: getTranslations("committee.financialEvaluationLbl"),
                awardDecisionLbl: getTranslations("committee.awardDecisionLbl"),
                rfxNumberLbl: getTranslations("committee.rfxNumberLbl"),
                rfxTitleLbl: getTranslations("committee.rfxTitleLbl"),
                placeHolder: getTranslations("common.placeHolder"),
                searchLbl: getTranslations("common.searchLbl"),
                nextLbl: getTranslations("common.nextLbl"),
                dateLbl: getTranslations("committee.dateLbl"),
                employeeLbl: getTranslations("committee.employeeLbl"),
                addMemberLbl: getTranslations("committee.addMemberLbl"),
                defaultMemberLbl: getTranslations("committee.defaultMemberLbl"),
                membersLbl: getTranslations("committee.membersLbl"),
                employeeNumberLbl: getTranslations("committee.employeeNumberLbl"),
                employeeEmailLbl: getTranslations("committee.employeeEmailLbl"),
                employeeNameLbl: getTranslations("committee.employeeNameLbl"),
                supplierNameLbl: getTranslations("committee.supplierNameLbl"),
                supplierNumberLbl: getTranslations("committee.supplierNumberLbl"),
                supplierTotalLbl: getTranslations("committee.supplierTotalLbl"),
                supplierSiteLbl: getTranslations("committee.supplierSiteLbl"),
                supplierContactLbl: getTranslations("committee.supplierContactLbl"),
                notifyLbl: getTranslations("common.notifyLbl"),
                submitLbl: getTranslations("common.submitLbl"),
                addFileLbl: getTranslations("committee.addFileLbl"),
                attachmentsLbl: getTranslations("committee.attachmentsLbl"),
                titleLbl: getTranslations("committee.titleLbl"),
                nameLbl: getTranslations("committee.nameLbl"),
                removeLbl: getTranslations("common.removeLbl"),
                saveForLaterLbl: getTranslations("committee.saveForLaterLbl"),
                resetLbl: getTranslations("common.resetLbl"),
                statusLbl: getTranslations("committee.statusLbl"),
                negotiationStyleLbl: getTranslations("committee.negotiationStyleLbl"),
                buyerLbl: getTranslations("committee.buyerLbl"),
                procurementBULbl: getTranslations("committee.procurementBULbl"),
                outcomeLbl: getTranslations("committee.outcomeLbl")

            });
            self.customsModel.rfxColumns([
                {
                    "headerText": self.label().supplierNameLbl,
                    "field": "PARTY_NAME"
                },
                {
                    "headerText": self.label().supplierNumberLbl,
                    "field": "PARTY_NUMBER"
                }
            ]);

            self.customsModel.memberColumnsOpen([
                {
                    "headerText": self.label().employeeNameLbl,
                    "field": "FULL_NAME"
                },
                {
                    "headerText": self.label().employeeEmailLbl,
                    "field": "EMAIL_ADDRESS"
                }
            ]);
            self.customsModel.attachmentColumnsOpen([
                {
                    "headerText": self.label().titleLbl,
                    "field": "title"
                },
                {
                    "headerText": self.label().nameLbl,
                    "field": "name"
                },
                {
                    "headerText": self.label().removeLbl,
                    "template": 'removeTemplate'
                }
            ]);

            self.customsModel.memberColumnsTech([
                {
                    "headerText": self.label().employeeNameLbl,
                    "field": "FULL_NAME"
                },
                {
                    "headerText": self.label().employeeEmailLbl,
                    "field": "EMAIL_ADDRESS"
                }
            ]);
            self.customsModel.attachmentColumnsTech([
                {
                    "headerText": self.label().titleLbl,
                    "field": "title"
                },
                {
                    "headerText": self.label().nameLbl,
                    "field": "name"
                },
                {
                    "headerText": self.label().removeLbl,
                    "template": 'removeTemplate'
                }
            ]);

            self.customsModel.memberColumnsFin([
                {
                    "headerText": self.label().employeeNameLbl,
                    "field": "FULL_NAME"
                },
                {
                    "headerText": self.label().employeeEmailLbl,
                    "field": "EMAIL_ADDRESS"
                }
            ]);
            self.customsModel.attachmentColumnsFin([
                {
                    "headerText": self.label().titleLbl,
                    "field": "title"
                },
                {
                    "headerText": self.label().nameLbl,
                    "field": "name"
                },
                {
                    "headerText": self.label().removeLbl,
                    "template": 'removeTemplate'
                }
            ]);

            self.customsModel.memberColumnsAward([
                {
                    "headerText": self.label().employeeNameLbl,
                    "field": "FULL_NAME"
                },
                {
                    "headerText": self.label().employeeEmailLbl,
                    "field": "EMAIL_ADDRESS"
                }
            ]);
            self.customsModel.attachmentColumnsAward([
                {
                    "headerText": self.label().titleLbl,
                    "field": "title"
                },
                {
                    "headerText": self.label().nameLbl,
                    "field": "name"
                },
                {
                    "headerText": self.label().removeLbl,
                    "template": 'removeTemplate'
                }
            ]);
        }
    }

    return committeeContentViewModel;
});
