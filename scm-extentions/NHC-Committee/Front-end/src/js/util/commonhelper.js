define([], function () {

    function commonHelper() {
        var self = this;
        
        self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };
        self.insertAttachment = "attachment/uploadAttachment";
        self.getAttachment = "attachment/getAttachment/";
        self.getSupplierNegotiation = "report/getAllSupplierNegotiation";
        self.getAllEmployee = "Empolyees/AllEmployees";
        self.getEmployees = "Empolyees/getEmps/";
        self.insertEmployee = "Empolyees/insertEmp";
        self.getNegAct = "activities/getActivities/";
        self.getNegData = "activities/getNegData/";
        self.addNegAct = "activities/insertActivity";
        self.sendMail = "email/sendEmail"

        self.getInternalRest = function() {

            if (window.location.hostname == "132.145.43.71") {
                var host = "https://132.145.43.71/nhcCommitteeContextTest/webapi/";
            } else {
                var host = "http://127.0.0.1:7101/nhcCommitteeContextTest/webapi/";
            }

            return host;
        };

    }

    return new commonHelper();
});