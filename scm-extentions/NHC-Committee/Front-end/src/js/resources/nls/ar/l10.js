define({

    "login": {
        "userName": "User Name: ",
        "Password": "Password: ",
        "loginLabel": "Login",
        "resetLabel": "Reset",
        "loginFailureText": "Invalid User Name or Password",
        "SignOut": "Sign Out",
        "Name": "Employee Name: "
    },

    "common": {
        "placeHolder": "اختر قيمة",
        "nextLbl": "التالى",
        "searchLbl": "بحث",
        "submitLbl": "إضافه",
        "logoutLbl": "تسجيل خروج",
        "notifyLbl":"أبلغ و أضف",
        "changeLang": "Change Language",
        "removeLbl":"حذف",
        "resetLbl":"إعادة"

    },"committee":{
            "screenLbl": "أنشطة اللجنة",
            "rfxNumberLbl":"رقم RFX",
            "rfxTitleLbl":"عنوان RFX",
            "dateLbl":"تاريخ",
            "employeeLbl":"موظف",
            "addMemberLbl":"إضافة عضو",
            "defaultMemberLbl":"الأعضاء الافتراضيون",
            "membersLbl":"أفراد",
            "employeeNameLbl":"اسم الموظف",
            "employeeNumberLbl":"رقم الموظف",
            "supplierNameLbl":"اسم المورد",
            "supplierNumberLbl":"رقم المورد",
            "supplierSiteLbl":"جهة اتصال المورد ",
            "supplierContactLbl":"اسم المورد",
            "supplierTotalLbl":"الإجمالى",
            "attachmentLbl":"المرفق",
            "attachmentsLbl":"المرفقات",
            "addFileLbl":"اضف ملف",
            "titleLbl":"عنوان",
            "nameLbl":"اسم",
            "rfxTitle":"معلومات عامة عن RFX",
            "openCommitteeLbl":"تقرير لجنة فتح المظاريف",
            "technicalEvaluationLbl":"محضر لجنة التقييم الفني ",
            "financialEvaluationLbl":"محضر لجنة التقييم المالي",
            "awardDecisionLbl":"محضر قرار الترسية ",
            "saveForLaterLbl":"احفظ لوقت لاحق",
            "statusLbl":"الحالة",
            "negotiationStyleLbl":"أسلوب التفاوض",
            "buyerLbl":"مشتر",
            "procurementBULbl":"المشتريات BU",
            "outcomeLbl":"النتيجة",
            "employeeEmailLbl":"البريد الإلكتروني للموظف"
        }


});