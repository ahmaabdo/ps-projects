define({
    "root": {

        "login": {
            "userName": "User Name: ",
            "Password": "Password: ",
            "loginLabel": "Login",
            "resetLabel": "Reset",
            "loginFailureText": "Invalid User Name or Password",
            "SignOut": "Sign Out",
            "Name": "Employee Name: "
        },
        "common": {
            "switchLang": "العربية",
            "inputPlaceholder": "Please enter value",
            "searchLbl": "Search",
            "nextLbl": "Next",
            "submitLbl": "submit",
            "notifyLbl":"Notify & Submit",
            "addFileLbl": "Add File +",
            "changeLang": "Change Language",
            "placeHolder": "Please Select value",
            "removeLbl":"Remove",
            "resetLbl":"Reset"
        },
        "committee":{
            "screenLbl": "Committee Activites",
            "rfxNumberLbl":"RFX Number",
            "rfxTitleLbl":"RFX Title",
            "dateLbl":"Date",
            "employeeLbl":"Employee",
            "addMemberLbl":"Add Member",
            "defaultMemberLbl":"Default Members",
            "membersLbl":"Members",
            "employeeNameLbl":"Employee Name",
            "employeeNumberLbl":"Employee Number",
            "supplierNameLbl":"Supplier Name",
            "supplierNumberLbl":"Supplier Number",
            "supplierSiteLbl":"Supplier Site",
            "supplierContactLbl":"Supplier Contact",
            "supplierTotalLbl":"Total",
            "attachmentsLbl":"Attachments",
            "addFileLbl":"Add File Attachment",
            "titleLbl":"Title",
            "nameLbl":"Name",
            "rfxTitle":"RFX General Information",
            "openCommitteeLbl":"Opening Committee",
            "technicalEvaluationLbl":"Technical Evaluation",
            "financialEvaluationLbl":"Financial Evaluation",
            "awardDecisionLbl":"Award Decision",
            "saveForLaterLbl":"Save For Later",
            "statusLbl":"Status",
            "negotiationStyleLbl":"Negotiation Style",
            "buyerLbl":"Buyer",
            "procurementBULbl":"Procurement BU",
            "outcomeLbl":"Outcome",
            "employeeEmailLbl":"Employee Email"
        }

    },
    "ar": true,
    "cs": true,
    "id": true
});