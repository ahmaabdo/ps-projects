/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmodule-element-utils', 'ojs/ojknockouttemplateutils', 'ojs/ojrouter', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojarraydataprovider',
    'ojs/ojoffcanvas', 'ojs/ojmodule-element', 'ojs/ojknockout', 'ojs/ojinputtext', 'ojs/ojmessages'],
    function (oj, ko, app, services, commonHelper, moduleUtils, KnockoutTemplateUtils, Router, ResponsiveUtils, ResponsiveKnockoutUtils, ArrayDataProvider, OffcanvasUtils) {
        function ControllerViewModel() {
            var self = this;
            this.KnockoutTemplateUtils = KnockoutTemplateUtils;
            // Handle announcements sent when pages change, for Accessibility.
            self.manner = ko.observable('polite');
            self.message = ko.observable();
            self.waitForAnnouncement = false;
            self.navDrawerOn = false;
            self.isUserLoggedIn = ko.observable(false);
            self.formVisible = ko.observable(true);
            self.windowSize = ko.observable();
            self.screenRange = oj.ResponsiveKnockoutUtils.createScreenRangeObservable();
            self.employeeInfo = ko.observable();
            self.employeeRole = ko.observableArray([]);
            self.userName = ko.observable();
            self.roleNumber = ko.observable();
            self.password = ko.observable();
            self.Usernametext = ko.observable("Username");
            self.passwordLabel = ko.observable("Password");
            self.loginLabel = ko.observable("Login");
            self.disOnLogin = ko.observable(false);
            self.messagesDataProvider = ko.observableArray([]);
            self.language = ko.observable();
            self.loginWelcomeText = ko.observable("Welcome back! Please login to your account")
            self.languagelable = ko.observable("عربي");
            var storage = window.localStorage;
            self.localeLogin = ko.observable();
            self.UsernameLabel = ko.observable();
            self.passwordLabel = ko.observable();
            self.loginword = ko.observable();
            self.loginVaild = ko.observable();
            self.hostUrl = ko.observable();
            self.jwt = ko.observable();
            self.userNameJWT = ko.observable();
            self.loginDateFromSass = ko.observable();
            self.forgotPasswordLabel = ko.observable();
            self.setLocale = ko.observable();
            self.suppliersArr = ko.observableArray();
            self.allSuppliersArr = ko.observableArray();
            self.employeeArray = ko.observableArray();
            self.dataNegotiation = ko.observableArray();
            // ----------------------------------------  loading function ------------------------ 

            self.getAllEmployees = function () {
                self.loading(true);
                services.getGeneric(commonHelper.getAllEmployee).then(data => {
                    self.loading(false);
                    if (data.length > 0) {
                        self.employeeArray(data);
                    }
                }, error => {
                });
            };

            // ----------------------------------------  loading function ------------------------ 
            
            self.getAllSupplierNegotiation = function () {
                self.loading(true);
                    var SuccessCbFn = function (data) {
                        self.loading(false);
                        if (data !== null) {
                            self.dataNegotiation(data);
                        };
                    };
                    var failCbFn = function () {
                        self.loading(false);
                    };
                    services.getGeneric(commonHelper.getSupplierNegotiation).then(SuccessCbFn, failCbFn)
            };
        // ----------------------------------------  loading function ------------------------ 
            self.loading = function (load) {
                if (load) {
                    $('#loading').show();
                } else {
                    $('#loading').hide();
                }
            };

            //     ----------------------------------------  Login function ------------------------ 
            self.onLogin = function () {
                self.disOnLogin(true);
                $(".apLoginBtn").addClass("loading");
                if (!self.userName() || !self.password())
                    return;
                var loginSuccessCbFn = function () {
                    self.getEmployee();
                    self.getAllEmployees();
                    self.getAllSupplierNegotiation();
                    self.messagesDataProvider([]);
                    self.isUserLoggedIn(true);
                    oj.Router.rootInstance.go('committee');

                };

                var loginFailCbFn = function () {
                    self.loading(false);
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);
                    self.isUserLoggedIn(false);
                    self.messagesDataProvider.push(self.createMessage('error', 'Wrong user name or passowrd'));

                };

                function authinticate(data) {
                    if (data.split(':')[1].split('}')[0] == 'true') {
                        loginSuccessCbFn();
                    } else {
                        loginFailCbFn();
                    }
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);
                }
                ;
                self.failCbFn = function (error) {
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);
                    self.isUserLoggedIn(false);
                    // self.loginFailureText("error");
                    self.messagesDataProvider.push(self.createMessage('error', `${error.statusText}`))
                };

                var payload = {
                    "userName": self.userName(),
                    "password": self.password(),
                    "mainData": ""
                };
                services.authenticate(payload).then(authinticate, self.failCbFn);


            };
            self.getEmployee = function () {
                services.getGeneric('Empolyees/getEmployee/' + self.userName()).then(data => {
                        if (data.length > 0) {
                            self.employeeInfo(data[0]);
                            self.employeeRole(JSON.parse(data[0].roles));
                            if (self.employeeRole().find(e => e.RoleCommonName === "NHC-Opening-Committee")) {
                                self.roleNumber("1");
                            } else if (self.employeeRole().find(e => e.RoleCommonName === "NHC-Technical-Evaluation")) {
                                self.roleNumber("2");
                            } else if (self.employeeRole().find(e => e.RoleCommonName === "NHC-Financial-Evaluation")) {
                                self.roleNumber("3");
                            } else if (self.employeeRole().find(e => e.RoleCommonName === "NHC-commercial-evaluation")) {
                                self.roleNumber("4");
                            } else {
                                self.roleNumber("0");
                            }
                        }
                }, error => {
                });
            };
            //     ----------------------------------------  Logout function ------------------------
            self.menuItemAction = function (event) {
                console.log(event.target.value)
                if (event.target.value === "out") {
                    self.isUserLoggedIn(false);
                    location.reload();

                } else {

                }
            };

            self.logOutBtn = function () {
                self.isUserLoggedIn(false);
                location.reload();
            }

            // document.getElementById('globalBody').addEventListener('announce', announcementHandler, false);
            function announcementHandler(event) {
                self.waitForAnnouncement = true;
                setTimeout(function () {
                    self.message(event.detail.message);
                    self.manner(event.detail.manner);
                    if (!self.navDrawerOn) {
                        self.waitForAnnouncement = false;
                    }
                }, 200);
            }
            ;
            self.createMessage = function (type, summary) {
                return {
                    severity: type,
                    summary: summary,
                    autoTimeout: '2000'
                };
            };
            self.displaySreenRange = ko.computed(function () {
                var range = self.screenRange();
                if (range === oj.ResponsiveUtils.SCREEN_RANGE.SM) {
                    self.windowSize('SM');
                } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.MD) {
                    self.windowSize('MD');
                } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.LG) {
                    self.windowSize('LG');
                } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.XL) {
                    self.windowSize('XL');
                }
            });
            // Media queries for repsonsive layouts
            var smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
            self.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
            var mdQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
            self.mdScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
            // Router setup
            self.router = Router.rootInstance;
            self.router.dispose();
            self.router.configure({
                'committee': { label: 'Committee', value: "committee/committee", title: 'Committee ', isDefault: true },
            });

            Router.defaults['urlAdapter'] = new Router.urlParamAdapter();
            self.loadModule = function () {
                self.moduleConfig = ko.pureComputed(function () {
                    var name = self.router.moduleConfig.name();
                    var viewPath = 'views/' + name + '.html';
                    var modelPath = 'viewModels/' + name;
                    return moduleUtils.createConfig({
                        viewPath: viewPath,
                        viewModelPath: modelPath, params: { parentRouter: self.router }
                    });
                });
            };
            /*********************************Navigation Router*********************************************** */
            
            /************************************************************************************************* */
            // Close offcanvas on medium and larger screens
            self.mdScreen.subscribe(function () {
                OffcanvasUtils.close(self.drawerParams);
            });
            self.drawerParams = {
                displayMode: 'push',
                selector: '#navDrawer',
                content: '#pageContent'
            };
            // Called by navigation drawer toggle button and after selection of nav drawer item
            self.toggleDrawer = function () {
                self.navDrawerOn = true;
                return OffcanvasUtils.toggle(self.drawerParams);
            }
            self.changeLanguage = function () {
                //storage.setItem('setLang', 'arabic');
                // document.querySelector("#switch").close();
                if (self.language() === 'english') {
                    self.language("arabic");
                    self.loginLabel("تسجيل دخول");
                    self.loginWelcomeText("مرحبا بعودتك! يرجى تسجيل الدخول إلى حسابك")
                    self.setPreferedLanguage(self.language());
                    self.languagelable("English");
                    $('.login-label').toggleClass('user-label-right');
                    $('.login-label').removeClass('user-label-left');
                    $('.input100').toggleClass('box-input-text');
                    $('.wrap-input100').toggleClass('box-input-text');
                } else if (self.language() === 'arabic') {
                    self.language("english");
                    self.loginLabel("LOGIN")
                    self.loginWelcomeText("Welcome back! Please login to your account")
                    self.setPreferedLanguage(self.language());
                    self.languagelable("عربي");
                    $('.login-label').toggleClass('user-label-left');
                    $('.login-label').removeClass('user-label-right');
                    $('.input100').removeClass('box-input-text');
                    $('.wrap-input100').removeClass('box-input-text');
                }
            };
            self.setPreferedLanguage = function (lang) {
                var selecedLanguage = lang;
                storage.setItem('setLang', selecedLanguage);
                storage.setItem('setTrue', true);
                preferedLanguage = storage.getItem('setLang');

                // Call function to convert arabic
                changeToArabic(preferedLanguage);
            };

            $(function () {
                storage.setItem('setLang', 'english');
                //                    storage.setItem('setLang', 'arabic');
                preferedLanguage = storage.getItem('setLang');
                self.language(preferedLanguage);
                changeToArabic(preferedLanguage);
                //document.addEventListener("deviceready", selectLanguage, false);
            });
            function changeToArabic(preferedLanguage) {
                if (preferedLanguage == 'arabic') {

                    oj.Config.setLocale('ar-sa',
                        function () {
                            $('html').attr({ 'lang': 'ar-sa', 'dir': 'rtl' });
                            self.localeLogin(oj.Translations.getTranslatedString('Login'));
                            self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                            self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                            self.loginword(oj.Translations.getTranslatedString('login'));
                            self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                        }
                    );

                } else if (preferedLanguage == 'english') {
                    selectLanguage();
                }
            }
            function selectLanguage() {
                oj.Config.setLocale('english',
                    function () {
                        $('html').attr({ 'lang': 'en-us', 'dir': 'ltr' });
                        self.localeLogin(oj.Translations.getTranslatedString('Login'));
                        self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                        self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                        self.loginword(oj.Translations.getTranslatedString('login'));
                        self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                    }
                );
            }
            function onNavDrawerClose(event) {
                self.navDrawerOn = false;
                if (!self.waitForAnnouncement) {
                    document.getElementById('drawerToggleButton').focus();
                    return;
                }

                setTimeout(function () {
                    document.getElementById('drawerToggleButton').focus();
                    self.waitForAnnouncement = false;
                }, 2500);
            }

            // Header
            // Application Name used in Branding Area
            self.appName = ko.observable(" NHC-Committee");
            // User Info used in Global Navigation area
            self.userLogin = ko.observable();

            var newFunction = function () {
                $.when(showEmplDetails(self)).done(function () {
                    if (self.loginVaild()) {
                        self.isUserLoggedIn(true)
                        self.messagesDataProvider([]);
                        self.disOnLogin(false);
                        self.loading(false);
                        self.router.go('committee');
                    } else {
                        self.isUserLoggedIn(false);
                    }
                });
            };
            newFunction();
            // Footer
            function footerLink(name, id, linkTarget) {
                this.name = name;
                this.linkId = id;
                this.linkTarget = linkTarget;
            }
            self.footerLinks = ko.observableArray([
                new footerLink('About Oracle', 'aboutOracle', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
                new footerLink('Contact Us', 'contactUs', 'http://www.oracle.com/us/corporate/contact/index.html'),
                new footerLink('Legal Notices', 'legalNotices', 'http://www.oracle.com/us/legal/index.html'),
                new footerLink('Terms Of Use', 'termsOfUse', 'http://www.oracle.com/us/legal/terms/index.html'),
                new footerLink('Your Privacy Rights', 'yourPrivacyRights', 'http://www.oracle.com/us/legal/privacy/index.html')
            ]);
        }

        return new ControllerViewModel();
    }
);
