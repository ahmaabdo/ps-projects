package com.appspro.lebara.biReport;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.cert.CertificateException;

import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONObject;

import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;

import sun.misc.BASE64Decoder;

public class RestHelper {

    private final String InstanceUrl = "https://embf-test.fa.em3.oraclecloud.com:443";
    private final String devInstanceUrl = "https://embf-dev1.fa.em3.oraclecloud.com:443";
    private final String biReportUrl = "/xmlpserver/services/PublicReportService";
    private final String instanceName =  "https://embf-test.fa.em3.oraclecloud.com";
    public String protocol = "https";
    
    public String getInstanceUrl() {
        return InstanceUrl;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public String getDevInstanceUrl() {
        return devInstanceUrl;
    }
    
    public String getBiReportUrl() {
        return biReportUrl;
    }

    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public static void trustAllHosts() {
        TrustManager[] trustAllCerts =  new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }
    
    public JSONObject httpGet (String restUrl) throws MalformedURLException, IOException {
       
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        URL url = new URL(null,restUrl,new sun.net.www.protocol.https.Handler());

        
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String soapAction = null ;
        connection.setRequestMethod("GET");
        connection.setReadTimeout(6000000);
        connection.setRequestProperty("content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("SoapAction", soapAction);
        connection.setRequestProperty("Authorization", "Basic " + "U0NNX1RNTTpPcmFjbGVAMTIz");
       
        BufferedReader in = new BufferedReader( new InputStreamReader (connection.getInputStream(),"utf-8"));
        String input = null;
        StringBuffer response = new StringBuffer();
        
        while((input = in.readLine()) != null) {
            response.append(input);
        }
        
        JSONObject obj = new JSONObject(response.toString());
               
        return obj;
        
    }
    
    public Document httpPostSoap(String destUrl, String postData) throws Exception {
        
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
     
        java.net.URL url = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String SOAPAction = "";
        connection.setRequestProperty("Content-Length", String.valueOf(b.length));
        connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        connection.setRequestProperty("SOAPAction", SOAPAction);
        connection.setRequestProperty("Authorization", "Basic " + "U0NNX1RNTTpPcmFjbGVAMTIz");
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        OutputStream out = connection.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + connection.getResponseCode() + "; connection response: " + connection.getResponseMessage());

        if (connection.getResponseCode() == 200) {
            InputStream in = connection.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in,"UTF-8");
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            if (response.indexOf("<?xml") > 0)
                response =response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +15);
            
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));
            
            XML.toJSONObject(response).toString();
            
            iReader.close();
            bReader.close();
            in.close();
            connection.disconnect();
            return doc;
        } 
        else {
            System.out.println("error"+connection.getResponseMessage());
            InputStream in = connection.getErrorStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            System.out.println("error2"+connection.getErrorStream());
            System.out.println("Failed"+response);
            if (response.indexOf("<?xml") > 0)
                response =response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +15);
            
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));
            
            XML.toJSONObject(response).toString();
            
            iReader.close();
            bReader.close();
            in.close();
            connection.disconnect();
            
            return doc;
        }
    }

    public static String callPostRest(String destUrl, String contentType, String postData) throws Exception {
        try {

            System.setProperty("DUseSunHttpHandler", "true");
            byte[] buffer = new byte[postData.length()];
            buffer = postData.getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https = (HttpsURLConnection)url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";
            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Type", contentType);
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);

            System.out.println("connection status: " + http.getResponseCode() + "; connection response: " + http.getResponseMessage());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getAuth() {

        byte[] message = ("appspro.projects" + ":" + "123456789").getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

}
