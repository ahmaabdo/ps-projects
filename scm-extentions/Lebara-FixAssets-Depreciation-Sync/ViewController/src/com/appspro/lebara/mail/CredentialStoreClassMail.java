package com.appspro.lebara.mail;

import com.appspro.lebara.biReport.RestHelper;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class CredentialStoreClassMail extends RestHelper {
  
    public static void SEND_MAIL3(String subject, String body) throws AddressException,  MessagingException {
        
        final String smtpAuthUserName = "sherif.alaa.017@gmail.com";
        final String smtpAuthPassword = "";
        String emailFrom = "sherif.alaa.017@gmail.com";
        String emailTo = "nasir.iqbal@appspro-me.com";
        String emailcc = "sherif.alaa@appspro-me.com";
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(smtpAuthUserName,
                        smtpAuthPassword);
            }
        };
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.host", "smtp.gmail.com");
        properties.setProperty("mail.smtp.port", "587");
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.ssl.enable", "false");
        properties.setProperty("mail.debug", "true");
        properties.setProperty("mail.smtp.sendpartial", "true"); 
        properties.setProperty("mail.transport.protocol", "smtp");
        trustAllHosts();
                     
        String certificatesTrustStorePath = "C:\\Program Files\\Java\\jdk1.8.0_231\\jre\\lib\\security\\cacerts";
        System.setProperty("javax.net.ssl.trustStore", certificatesTrustStorePath);
                 trustAllHosts();

        Session session = Session.getInstance(properties, authenticator);
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(emailFrom));
        InternetAddress[] to = {new InternetAddress(emailTo)};
        message.setRecipients(Message.RecipientType.TO, to);
        InternetAddress[] cc = {new InternetAddress(emailcc)};
        message.setRecipients(Message.RecipientType.CC, cc);
        message.setSubject(subject);
        message.setContent(body,"text/html; charset=utf-8");
        Transport.send(message);
        System.out.println("The mail sent success!!");

    }
}
