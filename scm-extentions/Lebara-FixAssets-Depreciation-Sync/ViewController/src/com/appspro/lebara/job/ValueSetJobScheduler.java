package com.appspro.lebara.job;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
/**
 *
 * @author HP
 */
public class ValueSetJobScheduler {
    public static  void runJobScheduler() {
        try {
            // specify the job' s details..
            JobDetail job = JobBuilder.newJob(ValueSetScheduledJob.class).withIdentity("Value Set Job")
                    .build();
            // specify the running period of the job
            Trigger trigger = TriggerBuilder.newTrigger()
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withIntervalInMinutes(2)
                            .repeatForever())
                    .build();
            //schedule the job
            //      SchedulerFactory schFactory = new StdSchedulerFactory();
            Scheduler sch = StdSchedulerFactory.getDefaultScheduler();
            sch.start();
            sch.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            System.out.println(e);
        }
    }
    
    
    public void stopScheduler() {
//        log.warn("Stopping scheduler (pid:" + ServerUtil.getCurrentPid() + ")");
        try {
            // Grab the Scheduler instance from the Factory
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.shutdown();

        } catch (SchedulerException se) {
//            log.error("Scheduler fail to stop", se);
        }
    }
}
