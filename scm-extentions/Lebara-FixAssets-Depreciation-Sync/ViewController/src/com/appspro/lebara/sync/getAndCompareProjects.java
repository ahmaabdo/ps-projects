package com.appspro.lebara.sync;

import com.appspro.lebara.biReport.BIReportService;
import com.appspro.lebara.mail.CredentialStoreClassMail;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.mail.MessagingException;
import org.json.JSONArray;
import org.json.JSONObject;


public class getAndCompareProjects {

    public void compareAndPostNewFixAsset() {

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        CredentialStoreClassMail mailObj = new CredentialStoreClassMail();
        Date date = new Date();  
        BIReportService ser = new BIReportService();
        JSONObject assetObj = new JSONObject();
        assetObj = ser.assetNumberReport();

        JSONArray arr = new JSONArray();
        
        if (assetObj.has("G_1")) {
            if(assetObj.get("G_1") instanceof JSONArray){
                arr = assetObj.getJSONArray("G_1");
            }
            else{
                System.out.println("test2");
                arr.put(assetObj.getJSONObject("G_1"));
            }
        }
        
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            int numberOfMonths = obj.getInt("LIFE_IN_MONTHS");
            String servicesDate = obj.get("DATE_PLACED_IN_SERVICE").toString();
            String assetNumber = obj.get("ASSET_NUMBER").toString();
            String description = obj.get("DESCRIPTION").toString();
            String categorySeg1 = obj.get("SEGMENT1").toString();
            String categorySeg2 = obj.get("SEGMENT2").toString();
            String categorySeg3 = obj.get("SEGMENT3").toString();
            
            int year = Integer.valueOf(servicesDate.split("\\-")[0]); 
            int month = Integer.valueOf(servicesDate.split("\\-")[1]); 
            int day = Integer.valueOf(servicesDate.split("\\-")[2]); 
            
            Calendar dateConverted = Calendar.getInstance(); 
            dateConverted.set(year, month, day);
            dateConverted.add(Calendar.MONTH, numberOfMonths); 

            String dateString = formatter.format(dateConverted.getTime());
                        
            try {
                String s = formatter.format(date);
                Date sysDate = formatter.parse(s);
                Date date1 = formatter.parse(dateString);


                if((formatter.format(date1)).compareTo(formatter.format(sysDate)) == 0){
                    System.out.println("Date1 is equal to Date2");

                try {
                     mailObj.SEND_MAIL3("Asset Depreciated",
                                       "Dear User,"+"<br/>"+
                                       "<body>"+"<br/>"+ "Please note, the following Asset has been fully depreciated: " +"<br/>"+"\n" +  "<br/>"+
                                           "<b>Asset Number : </b> "+assetNumber+"  " +"\n"+ "<br/>"+
                                           ""+"<b>Description : </b> "+description+"  " +"\n"+ "<br/>"+
                                           ""+"<b>Categories :</b> "+categorySeg1+" , " +"\n"+ "<br/>"+
                                           ""+"<b>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;</b> "+categorySeg2+" , " +"\n"+ "<br/>"+
                                           ""+"<b>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;</b> "+categorySeg3+"\n" +"<br/>" +
                                       "</body>"
                                       );
                    } catch (MessagingException e) {
                    }
                }
            } catch (ParseException e) {
            }
        }

    }
    
    public static void main(String [] args){
    
        getAndCompareProjects OB = new getAndCompareProjects ();
        OB.compareAndPostNewFixAsset();
        CredentialStoreClassMail mailObj = new CredentialStoreClassMail();

//        
//        Calendar cal = Calendar.getInstance(); 
//        cal.add(Calendar.MONTH, 60);
//        System.out.println();
//        
          DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        Calendar now = Calendar.getInstance(); 
//        now.add(Calendar.MONTH, 60); 
//        String dateString = formatter.format(now.getTime());
//        System.out.println(dateString);
        
//           String textfile = "2015-04-18"; 
//           int year = Integer.valueOf(textfile.split("\\-")[0]); 
//           int month = Integer.valueOf(textfile.split("\\-")[1]); 
//           int day = Integer.valueOf(textfile.split("\\-")[2]); 
//       
//           Calendar now = Calendar.getInstance(); 
//           now.set(year, month, day);
//
//            now.add(Calendar.MONTH, 60); 
//            String dateString = formatter.format(now.getTime());
//            System.out.println(dateString);


//        try {
//            mailObj.SEND_MAIL3("Asset Depreciated",
//                                       "Dear User,"+"<br/>"+
//                                       "<body>"+"<br/>"+ "Please note, the following Asset has been fully depreciated: " +"<br/>"+"\n" +  "<br/>"+
//                                           "<b>Asset Number : </b> 1000544  " +"\n"+ "<br/>"+
//                                           ""+"<b>Description : </b> Cork Board , " +"\n"+ "<br/>"+
//                                           ""+"<b>Categories :</b> Major Category - Office - Tangible , " +"\n"+ "<br/>"+
//                                           ""+"<b> &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;</b> Minor Category - Computers and Office Equipment "+"\n" + "<br/>"+
//                                           ""+"<b>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;</b> Sub-Minor Category - General"+"\n" +
//                                       "</body>"
//                                       );
//        } catch (AddressException e) {
//        } catch (MessagingException e) {
//        }
    }

}
