package com.appspro.lebara;

import com.appspro.lebara.biReport.BIReportService;

import com.appspro.lebara.biReport.RestHelper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;

import java.util.ArrayList;

import java.util.Collections;
import java.util.HashMap;

import java.util.HashSet;

import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;

/**
 *
 * @author HP
 */
public class RestApiCall {

    public static void trustAllHosts() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }
            }};

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {

        }
    }
    
    
    public static JSONObject createValueSetREST(String assetNumber, String assetNumberDescription)  {
        
        System.setProperty("https.protocols", "TLSv1.2");

        String body = " {\n"
                + "\n"
                + "      \"IndependentValue\": null,\n"
                + "      \"IndependentValueNumber\": null,\n"
                + "      \"IndependentValueDate\": null,\n"
                + "      \"IndependentValueTimestamp\": null,\n"
                + "      \"Value\": \"" + assetNumber + "\",\n"
                + "      \"ValueNumber\": null,\n"
                + "      \"ValueDate\": null,\n"
                + "      \"ValueTimestamp\": null,\n"
                + "      \"TranslatedValue\": null,\n"
                + "      \"Description\": \"" + assetNumberDescription + "\",\n"
                + "      \"EnabledFlag\": \"Y\",\n"
                + "      \"StartDateActive\": null,\n"
                + "      \"EndDateActive\": null,\n"
                + "      \"SortOrder\": null,\n"
                + "      \"SummaryFlag\": \"N\",\n"
                + "      \"DetailPostingAllowed\": \"Y\",\n"
                + "      \"DetailBudgetingAllowed\": \"Y\",\n"
                + "      \"AccountType\": null,\n"
                + "      \"ControlAccount\": null,\n"
                + "      \"ReconciliationFlag\": null,\n"
                + "      \"FinancialCategory\": null,\n"
                + "      \"ExternalDataSource\": null\n"
                + "\n"
                + " }";

        System.out.println(body);

        String restApi = "/fscmRestApi/resources/11.13.18.05/valueSets/Asset%20Number%20and%20Description%20Value%20Set/child/values";
        JSONObject res = new RestHelper().httpPost(restApi, body);

       return res;
    }

    public static void main(String [] args){
        String projectNumber = "195001";
        String projectName = "Test2";
        JSONObject js = createValueSetREST(projectNumber, projectName);
        System.out.println(js);
            
        }
}

