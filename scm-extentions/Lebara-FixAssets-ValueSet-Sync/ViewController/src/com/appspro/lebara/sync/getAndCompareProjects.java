package com.appspro.lebara.sync;

import com.appspro.lebara.RestApiCall;
import com.appspro.lebara.biReport.BIReportModel;

import com.appspro.lebara.biReport.BIReportService;

import java.lang.reflect.Array;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class getAndCompareProjects {

    public void compareAndPostNewFixAsset() {

        BIReportService ser = new BIReportService();
        JSONObject assetObj = new JSONObject();
        assetObj = ser.assetNumberReport();
        System.out.println(assetObj);

        JSONArray arr = new JSONArray();
        
        if (assetObj.has("G_1")) {
            if(assetObj.get("G_1") instanceof JSONArray){
                System.out.println("test");
                arr = assetObj.getJSONArray("G_1");
            }
            else{
                System.out.println("test2");
                arr.put(assetObj.getJSONObject("G_1"));
            }
        }
        
        System.out.println(arr);
        
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            RestApiCall rs = new RestApiCall();
            JSONObject js = rs.createValueSetREST(obj.get("ASSET_NUMBER").toString(), obj.get("DESCRIPTION").toString());
            System.out.println(js);
        }

    }
    
    public static void main(String [] arg){
     new getAndCompareProjects().compareAndPostNewFixAsset();
    }

}
