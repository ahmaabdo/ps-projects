package com.appspro.lebara.biReport;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.json.XML;

public class BIReportService {
    
    public JSONObject assetNumberReport () {
        String fresponse ="";
        String reportName = "assetNumberValueSetReport";
        BIPReports biPReports = new BIPReports();
        JSONObject obj = new JSONObject();
        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
        
        biPReports.setReportAbsolutePath(reportName);
        System.out.println(reportName);
        fresponse = biPReports.executeReports();
        JSONObject xmlJSONObj = XML.toJSONObject(fresponse.toString());

        if (!xmlJSONObj.isNull("DATA_DS") && !xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1"))
        {
            if (xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) 
            {                 
                 return obj;

            } else 
            {
                obj = xmlJSONObj.getJSONObject("DATA_DS");
            }
        } else 
        {
            return obj;
        }   
        
        return obj;
    }
    
    public static void main(String[] args) {
        
        BIReportService ser = new BIReportService ();
        ser.assetNumberReport();
    }
}
