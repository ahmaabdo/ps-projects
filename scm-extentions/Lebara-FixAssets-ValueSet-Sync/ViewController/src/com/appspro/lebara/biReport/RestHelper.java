package com.appspro.lebara.biReport;


import com.appspro.db.CommonConfigReader;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.cert.CertificateException;

import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONObject;

import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;

import sun.misc.BASE64Decoder;

public class RestHelper {

    public String protocol = "https";
    private final String biReportUrl = "/xmlpserver/services/PublicReportService";
    
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getInstanceUrl() {
        String url = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            url = CommonConfigReader.getValue("TestInstanceUrl");
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            url = CommonConfigReader.getValue("DevInstanceUrl");
        }
        return url;
    }

    public String getInstanceName() {
        String name = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            name = CommonConfigReader.getValue("TestInstanceName");
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            name = CommonConfigReader.getValue("DevInstanceName");
        }
        return name;
    }

    public String getUserName() {
        String userName = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            userName = CommonConfigReader.getValue("TEST_USER_NAME");
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            userName = CommonConfigReader.getValue("DEV_USER_NAME");
        }
        return userName;
    }

    public String getPassword() {
        String password = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            password = CommonConfigReader.getValue("TEST_PASSWORD");
        } else if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            password = CommonConfigReader.getValue("DEV_PASSWORD");
        }
        return password;
    }

    public String getBiReportUrl() {
           return biReportUrl;
       }
    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public static void trustAllHosts() {
        TrustManager[] trustAllCerts =  new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
    
    public JSONObject httpGet (String restUrl) throws MalformedURLException, IOException {
       
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        URL url = new URL(null,restUrl,new sun.net.www.protocol.https.Handler());

        
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String soapAction = null ;
        connection.setRequestMethod("GET");
        connection.setReadTimeout(6000000);
        connection.setRequestProperty("content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("SoapAction", soapAction);
        connection.setRequestProperty("Authorization", "Basic " +getAuth());
       
        BufferedReader in = new BufferedReader( new InputStreamReader (connection.getInputStream(),"utf-8"));
        String input = null;
        StringBuffer response = new StringBuffer();
        
        while((input = in.readLine()) != null) {
            response.append(input);
        }
        
        JSONObject obj = new JSONObject(response.toString());
               
        return obj;
        
    }
    
    public JSONObject httpPost(String destUrl, String body)  {
        JSONObject res = new JSONObject();
        
        try {
            HttpsURLConnection https = null;    
            HttpURLConnection connection = null;
            URL url = new URL(null, getInstanceUrl()+destUrl, new sun.net.www.protocol.https.Handler());

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection) url.openConnection();
                connection = https;
            } else {
                connection = (HttpsURLConnection) url.openConnection();
            }

            String soapAction = null;
            connection.setRequestMethod("POST");
            connection.setReadTimeout(6000000);
            connection.setRequestProperty("content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("charset", "UTF-8");
            connection.setRequestProperty("SoapAction", soapAction);
            connection.setRequestProperty("Authorization", "Basic " + getAuth()); 

            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            int code = connection.getResponseCode();
            InputStream is;
            if (code >= 400) {
                is = connection.getErrorStream();
                res.put("status", "error");
                res.put("data", is);
            } else {
                res.put("status", "done");
                is = connection.getInputStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(is));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                res.put("data", new JSONObject(response.toString()));
            }

            System.out.print("code:" + code);
            if (code >= 400) {
                System.out.println(",Error:" + res.get("data").toString());
            } else {
                System.out.println();
            }

        } catch (Exception ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", "Internal server error");
        }
        return res;
    }


    public String getAuth() {

        byte[] message = (getUserName() + ":" + getPassword()).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

}
