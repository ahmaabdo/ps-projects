/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * uploadScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'util/commonhelper', 'appController', 'ojs/ojconverterutils-i18n', 'ojs/ojpagingdataproviderview', 'ojs/ojarraydataprovider', 'xlsx',
    'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojinputtext', 'ojs/ojfilepicker', 'ojs/ojdialog',
    'ojs/ojlabelvalue', 'ojs/ojarraydataprovider', 'ojs/ojpagingcontrol',
    'ojs/ojselectcombobox', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojtable', 'bignumber'
], function (oj, ko, $, services, commonhelper, app, ConverterUtilsI18n, PagingDataProviderView, ArrayDataProvider) {
    /**
     * The view model for the main content view template
     */
    function uploadScreenContentViewModel() {
        var self = this;
        var selected_file = null;
        var getTranslation = oj.Translations.getTranslatedString;
        self.labels = ko.observableArray([]);
        self.messages = ko.observableArray();
        self.messagesDataprovider = new oj.ArrayDataProvider(self.messages);
        self.excelData = ko.observableArray();
        self.dialogMsg = ko.observable();
        self.messagesData = ko.observableArray();
        self.status = ko.observable();
        self.fileDisabled = ko.observable(false);
        self.btnDisabled = ko.observable(false);
        self.searchData = ko.observable();
        self.receiptNumber = ko.observable();
        self.poNumber = ko.observable();
        self.placeholder = ko.observable('Select type of search');
        self.selectedValue = ko.observable();
        self.batchNumberSelected = ko.observable();
        self.listValues = ko.observableArray([
            {
                label: getTranslation('putAwayScreen.receiptNumber'),
                value: 'receiptNumber'
            }, {
                label: getTranslation('putAwayScreen.poNumber'),
                value: 'poNumber'
            }, {
                label: getTranslation('putAwayScreen.rma'),
                value: 'poNumber2'
            }, {
                label: getTranslation('putAwayScreen.interShip'),
                value: 'interShip'
            }
        ]);
        self.subInvenList = ko.observableArray([]);
        self.subInvenFilteredList = ko.observableArray([]);
        self.deptArray = ko.observableArray([]);
        self.dataprovider = ko.observable(new PagingDataProviderView(new ArrayDataProvider(self.deptArray, { idAttribute: 'parentTransactionId' })));
        self.typeData = ko.observable();
        self.selectedData = ko.observable();
        self.subInvenSelected = ko.observable();
        self.fromSerial = ko.observable();
        self.toSerial = ko.observable();
        self.transDate = ko.observable(ConverterUtilsI18n.IntlConverterUtils.dateToLocalIso(new Date()));
        self.fromSerialDisabled = ko.observable(true);


        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            if (currentRow != null) {
                self.selectedData(self.deptArray[currentRow['rowIndex']]);
                self.fromSerialDisabled(false);
            } else {
                self.fromSerialDisabled(true);
            }
        };

        self.submitAction = function (event) {
            if (self.selectedData() == null || self.selectedData() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please select at least one raw data to submit'
                });
            } else if (self.fromSerial() == null || self.fromSerial() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please enter from serial'
                });
            } else if (self.toSerial() == null || self.toSerial() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please enter to serial'
                });
            } else if (self.subInvenSelected() == null || self.subInvenSelected() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please select subInventory'
                });
            } else if (self.transDate() == null || self.transDate() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please select date for transaction'
                });
            } else if (self.batchNumberSelected() == null || self.batchNumberSelected() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please enter batchNumber'
                });
            } else {
                console.log(self.fromSerial())
                self.selectedData().fromSerial = self.fromSerial();
                self.selectedData().toSerial = self.toSerial();
                self.selectedData().submitDate = self.transDate();
                self.selectedData().subInvenSelected = self.subInvenSelected();
                self.selectedData().batchNumber = self.batchNumberSelected();
                console.log(self.selectedData())

                success = function (data) {
                    if (data.length !== 0) {
                        if (data.status == 'Error') {
                            if (data.message == 'Gateway Timeout') {
                                self.messages.push({
                                    severity: 'confirmation', summary: 'Success In Submiting Put Away'
                                });
                                self.deptArray = [];
                                self.dataprovider(new PagingDataProviderView(new ArrayDataProvider(self.deptArray, { idAttribute: 'parentTransactionId' })));
                            } else {
                                self.messages.push({
                                    severity: 'error', summary: data.message
                                });
                            }

                        } else {
                            self.messages.push({
                                severity: 'confirmation', summary: data.message
                            });
                            self.deptArray = [];
                            self.dataprovider(new PagingDataProviderView(new ArrayDataProvider(self.deptArray, { idAttribute: 'parentTransactionId' })));
                        }

                    }
                    app.loading(false);
                };
                var fail = function (data) {
                    self.messages.push({
                        severity: 'error', summary: data.responseText
                    });
                    app.loading(false);
                };
                app.loading(true);
                services.addGeneric(commonhelper.submitPutAwayURL, self.selectedData()).then(success, fail);

            }
        };

        self.search = function (event) {
            self.deptArray = [];
            self.dataprovider(new PagingDataProviderView(new ArrayDataProvider(self.deptArray, { idAttribute: 'parentTransactionId' })));
            success = function (data) {
                if (data.length !== 0) {
                    self.deptArray = data;
                    self.deptArray.sort((a, b) => (a.parentTransactionId - b.parentTransactionId));
                    for (var i = 0; i < self.deptArray.length; i++) {
                        self.deptArray[i].count = i + 1;
                    }

                    self.dataprovider(new PagingDataProviderView(new ArrayDataProvider(self.deptArray, { idAttribute: 'parentTransactionId' })));
                    self.dataprovider();
                    //// filtering sub inventory
                    for (var i = 0; i < self.subInvenFilteredList().length; i++) {
                        if (self.subInvenFilteredList()[i].org == data[0].organizationCode) {
                            self.subInvenList.push(self.subInvenFilteredList()[i]);
                        }
                    }
                } else {
                    self.messages.push({
                        severity: 'error', summary: 'No put away lines for this order'
                    });
                }
                console.log(self.deptArray);
                app.loading(false);
            };
            var fail = function (data) {
                self.messages.push({
                    severity: 'error', summary: data.responseText
                });
                app.loading(false);
            };

            if (self.selectedValue() == null || self.selectedValue() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please choose which type you want to search using it'
                });
            } else if (self.typeData() == null || self.typeData() == '') {
                var d;
                for (var i = 0; i < self.listValues().length; i++) {
                    if (self.selectedValue() == self.listValues()[i].value) {
                        d = self.listValues()[i].label;
                    }
                }
                self.messages.push({
                    severity: 'error', summary: 'Please enter the value of ' + d
                });
            } else {
                self.sendData = ko.observable();
                if (self.selectedValue() == 'receiptNumber') {
                    self.searchData('ReceiptNumber=' + self.typeData());
                    self.sendData({
                        first: self.searchData()
                    });
                } else if (self.selectedValue() == 'poNumber') {
                    self.searchData('DocumentNumber=' + self.typeData());
                    self.sendData({
                        first: self.searchData(),
                        sec: 'PO'
                    });
                } else if (self.selectedValue() == 'poNumber2') {
                    self.searchData('DocumentNumber=' + self.typeData());
                    self.sendData({
                        first: self.searchData(),
                        sec: 'RMA'
                    });
                } else if (self.selectedValue() == 'interShip') {
                    self.searchData('DocumentNumber=' + self.typeData());
                    self.sendData({
                        first: self.searchData(),
                        sec: 'INVENTORY'
                    });
                }
                console.log(JSON.stringify(self.sendData()));
                app.loading(true);
                services.addGeneric(commonhelper.searchPutAwayURL, JSON.stringify(self.sendData())).then(success, fail);
            }
        };

        self.fromSerialChanged = function (event) {
            var listEvent = event['detail'];
            var fromSerial = listEvent.value;
            var e = new BigNumber(fromSerial);
            var d=e.add(self.selectedData().quantity);
            var final=d.subtract(1);
            console.log(d.valueOf());
            self.toSerial(final.valueOf());
        };
        // for translation
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });


        self.connected = function () {
            initTranslations();

            var success = function (data) {
                if (data.length !== 0) {
                    self.subInvenFilteredList(data);
                    app.loading(false);
                }
            };
            var fail = function (data) {
                self.messages.push({
                    severity: 'error', summary: data.responseText
                });
                app.loading(false);
            };
            services.getGeneric(commonhelper.subInventoryURL).then(success, fail);
        };

        self.closeMessageDialog = function () {
            document.getElementById("messageDialog1").close();
        };
        self.closeMessageDialog2 = function () {
            document.getElementById("messageDialog2").close();
        };

        function initTranslations() {
            self.labels = {
                title: ko.observable(getTranslation('uploadScreen.title')),
                upload: ko.observable(getTranslation('uploadScreen.upload')),
                picker_text: ko.observable(getTranslation('uploadScreen.click_or_dragdrop')),
                receiptNumberLbl: ko.observable(getTranslation('putAwayScreen.receiptNumber')),
                poNumberLbl: ko.observable(getTranslation('putAwayScreen.poNumber')),
                searchLbl: ko.observable(getTranslation('putAwayScreen.search')),
                submitLbl: ko.observable(getTranslation('putAwayScreen.submit'))
            }
            self.tableColumns = ko.observableArray([
                {
                    "headerText": "#", "field": "count"
                },
                {
                    "headerText": "Organization", "field": "organizationCode"
                },
                {
                    "headerText": "Item Number", "field": "itemNumber"
                },
                {
                    "headerText": "Item Desc", "field": "itemDesc"
                },
                {
                    "headerText": "Receipt Number", "field": "receiptNumber"
                },
                {
                    "headerText": "Quantity", "field": "quantity"
                },
                {
                    "headerText": "Document Number", "field": "documentNumber"
                },
                {
                    "headerText": "Document Line Number", "field": "docLineNumber"
                },
                {
                    "headerText": "Supplier", "field": "vendorName"
                }
            ]);


        }
    }

    return uploadScreenContentViewModel;
});
