define({
    // Root bundle
    "root": {
        "common": {
            "switchLang": "العربية"
        },
        "login": {
            "userName": "User Name: ",
            "Password": "Password: ",
            "loginLabel": "Login",
            "resetLabel": "Reset",
            "loginFailureText": "Invalid User Name or Password",
            "SignOut": "Sign Out",
            "Name": "Employee Name: "
        },
        "labels": {
             "changeLang": "Change language?",
            "signOut": "Are you sure you wish to logout?",
            "confirmMessage": "Confirm Message"
        },
        "others": {
              "yes": "Yes",
            "no": "No",
            back:"Back"
        },
        "uploadScreen":{
          "title":"Submit Put Away",
          "upload":"Send File",
          "upload_file_not_selected": "You didn't select a file to upload!",
          "upload_file_done": "File uploaded successfully",
          "upload_file_error": "Failed to upload the file",
          "uploadingandprocessingdata": "Uploading and Processing Data ...",
          "click_or_dragdrop": "Click or drag a file and drop here",
                
        },
        "putAwayScreen":{
            "receiptNumber":"Receipt Number ",
            "poNumber":"Purchase Order",
            "search":"Search",
            "submit":"Submit",
            "rma":"RMA",
            "interShip":"In-Transit Shipment"
        }
    },
    "ar": true
});