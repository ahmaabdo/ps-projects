package com.appspro.lebara.dao;

import com.appspro.lebara.bean.linesToReceiveBean;

import common.restHelper.RestHelper;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class linesToReceiveDAO extends RestHelper {

    RestHelper rs = new RestHelper();

    public String getlinesToReceive(linesToReceiveBean bean) throws MalformedURLException,
                                                                        IOException {

        ReceiptsDAO receiptsDAO = new ReceiptsDAO();
        JSONObject receiptObj = new JSONObject();
        RestHelper resthelper = new RestHelper();
        JSONArray arr = new JSONArray();
        String restApi =
            resthelper.getInstanceUrl() + resthelper.getLinesToReceive() +
            "?q=DocumentNumber='" +
            bean.getDocumentNumber().replaceAll("\\s", "%20") + "' and DocumentLineNumber= "+bean.getDocumentLineNumber();
        String response = resthelper.httpGet(restApi);
        JSONObject obj = new JSONObject(response.toString());
        
        if (obj.has("items")) {
            arr = obj.getJSONArray("items");
            if (arr.length() > 0) {
                JSONObject lineObj = arr.getJSONObject(0);

                System.out.println(arr);

                JSONObject obLines = new JSONObject();
                JSONObject ob1 = new JSONObject();
                JSONObject ob2 = new JSONObject();

                obLines.put("ItemNumber", lineObj.getString("ItemNumber"));
                obLines.put("DocumentLineNumber",lineObj.getString("DocumentLineNumber"));
                obLines.put("DocumentNumber",lineObj.getString("DocumentNumber"));
                obLines.put("UOMCode", lineObj.getString("UOMCode"));
                obLines.put("UnitOfMeasure",lineObj.getString("UnitOfMeasure"));
                obLines.put("ReceiptSourceCode",lineObj.getString("ReceiptSourceCode"));
                obLines.put("SourceDocumentCode", lineObj.getString("SourceDocumentCode"));
                obLines.put("Quantity", lineObj.getInt("OrderedQuantity"));
                obLines.put("OrganizationCode", lineObj.getString("ToOrganizationCode"));
                obLines.put("DestinationTypeCode",lineObj.getString("DestinationTypeCode"));
                obLines.put("AutoTransactCode", "RECEIVE");
                obLines.put("TransactionType", "RECEIVE");
                obLines.put("TransactionType", "RECEIVE");
                obLines.put("EmployeeName", bean.getEmployeeName());
//                obLines.put("Subinventory",bean.getSubinventory());

                JSONArray arr3 = new JSONArray();
                arr3.put(obLines);
                JSONArray arr2 = new JSONArray();
                arr2.put(ob2);

                ob2.put("OrganizationCode",lineObj.getString("ToOrganizationCode"));
                if(lineObj.get("VendorName").toString() != "null")
                ob2.put("VendorName", lineObj.opt("VendorName").toString());
                if(lineObj.get("VendorSiteCode").toString() != "null")
                ob2.put("VendorSiteCode", lineObj.opt("VendorSiteCode").toString());
                ob2.put("ReceiptSourceCode",lineObj.getString("ReceiptSourceCode"));
                ob2.put("receiptLines", arr3);
                ob1.put("receiptHeaders", arr2);
                if("CUSTOMER".equalsIgnoreCase(lineObj.get("ReceiptSourceCode").toString())){
                    ob2.put("CustomerId", lineObj.get("CustomerId").toString());  
                    ob2.put("EmployeeName", bean.getEmployeeName());
                }
                
               String res = receiptsDAO.receipts(ob1.toString());
               receiptObj=new JSONObject(res);
               
            } else {
                receiptObj.put("ReturnStatus", "Error");
                receiptObj.put("ErrorExplanation", "No items found for this doc number");
            }
        } else {
            receiptObj.put("ReturnStatus", "Error");
            receiptObj.put("ErrorExplanation",
                           "No items found for this doc number");
        }

        if (receiptObj.has("ReturnStatus")) {
            if ("ERROR".equalsIgnoreCase(receiptObj.get("ReturnStatus").toString())) {
                receiptObj.put("status", "Error");
                receiptObj.put("message",
                               receiptObj.get("ErrorExplanation").toString());
            } else {
                receiptObj.put("status", "Done");
                receiptObj.put("message","Receipt are successfully created");
            }
        } else {
            receiptObj.put("status", "Error");
            receiptObj.put("message", "No data returned from server ");
        }
        return receiptObj.toString();
    }
}
