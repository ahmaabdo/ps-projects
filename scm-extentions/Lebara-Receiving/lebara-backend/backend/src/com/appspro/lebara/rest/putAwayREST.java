package com.appspro.lebara.rest;

import com.appspro.lebara.dao.putAwayDAO;

import common.Soap.SubmitPutAwaySOAP;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

@Path("/searchPutAwayLines")
public class putAwayREST {
  
    @POST
    @Path("/getLines")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPutAwayLines(String body) {
        try {
            String res = new putAwayDAO().getPutAwayLines(body);
            return Response.ok(res.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ok("-1").build();
        }
    }
        @GET
        @Path("/sunbInventories")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public Response subInventories() {
            try {
                String res = new putAwayDAO().subInventories();
                return Response.ok(res.toString()).build();
            } catch (Exception e) {
                e.printStackTrace();
                return Response.ok("-1").build();
            }
        }   
        
        @POST
        @Path("/submitPutAway")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public Response submitPutAway(String body) {
            try {
                String res = new SubmitPutAwaySOAP().createPutAway(body);
                return Response.ok(res.toString()).build();
            } catch (Exception e) {
                e.printStackTrace();
                JSONObject responseObj=new JSONObject();
                responseObj.put("status","Error");
                responseObj.put("message","Error in submit Put Away");
                return Response.ok(responseObj.toString()).build();
            }
        } 
    
    }
