package com.appspro.lebara.bean;

public class linesToReceiveBean {
   
   private String documentNumber;
   private String documentLineNumber;
   private String destinationTypeCode;
   private String itemNumber;
   private String orderedQuantity;
   private String uOMCode;
   private String unitOfMeasure;
   private String receiptSourceCode;
   private String sourceDocumentCode;
    private String receiptNumber;
    private String quantity;
    private String subinventory;
    private String fromSerial;
    private String toSerial;
    private String employeeName;

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentLineNumber(String documentLineNumber) {
        this.documentLineNumber = documentLineNumber;
    }

    public String getDocumentLineNumber() {
        return documentLineNumber;
    }

    public void setDestinationTypeCode(String destinationTypeCode) {
        this.destinationTypeCode = destinationTypeCode;
    }

    public String getDestinationTypeCode() {
        return destinationTypeCode;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setOrderedQuantity(String orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public String getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setUOMCode(String uOMCode) {
        this.uOMCode = uOMCode;
    }

    public String getUOMCode() {
        return uOMCode;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setReceiptSourceCode(String receiptSourceCode) {
        this.receiptSourceCode = receiptSourceCode;
    }

    public String getReceiptSourceCode() {
        return receiptSourceCode;
    }

    public void setSourceDocumentCode(String sourceDocumentCode) {
        this.sourceDocumentCode = sourceDocumentCode;
    }

    public String getSourceDocumentCode() {
        return sourceDocumentCode;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setSubinventory(String subinventory) {
        this.subinventory = subinventory;
    }

    public String getSubinventory() {
        return subinventory;
    }

    public void setFromSerial(String fromSerial) {
        this.fromSerial = fromSerial;
    }

    public String getFromSerial() {
        return fromSerial;
    }

    public void setToSerial(String toSerial) {
        this.toSerial = toSerial;
    }

    public String getToSerial() {
        return toSerial;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }
}
