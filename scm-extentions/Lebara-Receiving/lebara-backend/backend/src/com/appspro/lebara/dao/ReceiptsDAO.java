package com.appspro.lebara.dao;

import common.restHelper.RestHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ResponseCache;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

import utils.system;

public class ReceiptsDAO {
  
  RestHelper rest=new RestHelper();
        public String receipts(String body) throws MalformedURLException,IOException {
            String res ;
            String restApi;
            restApi = rest.getInstanceUrl()+rest.getReceipt();
            res =rest.httpPostUrl(restApi, body);
            return res;
        }
}
