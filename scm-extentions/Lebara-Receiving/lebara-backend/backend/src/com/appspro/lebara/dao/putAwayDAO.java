package com.appspro.lebara.dao;

import com.appspro.lebara.bean.putAwayBean;
import common.restHelper.RestHelper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class putAwayDAO {
    RestHelper resthelper = new RestHelper();
    
    
    public String getPutAwayLines(String searchData){
        
        
        JSONObject objData=new JSONObject(searchData);
        
        String returnedRes="";
        String restApi =
            resthelper.getInstanceUrl() + resthelper.getPutAwayLinesRest() +"?q="+objData.get("first").toString().replaceAll("\\s", "%20");
        String response = resthelper.httpGet(restApi);
        
        JSONObject responseObj=new JSONObject(response);
        ArrayList<putAwayBean> putAwayList=new ArrayList<putAwayBean>();
        
        if (responseObj.has("items")) {
            JSONArray array=new  JSONArray(responseObj.get("items").toString());
            if(array.length()>0){
            for(int i=0 ; i < array.length() ; i++){
              JSONObject obj=new JSONObject(array.get(i).toString());
                putAwayBean bean =new putAwayBean();
                bean.setDocLineNumber(obj.get("DocumentLineNumber").toString());
                bean.setDocumentNumber(obj.get("DocumentNumber").toString());
                bean.setEmployeeName(obj.get("EmployeeName").toString());
                bean.setItemDesc(obj.get("ItemDescription").toString());
                bean.setItemNumber(obj.get("ItemNumber").toString());
                bean.setOrganizationCode(obj.get("ToOrganizationCode").toString());
                bean.setParentTransactionId(obj.get("ParentTransactionId").toString());
                bean.setQuantity(obj.get("AvailableQuantity").toString());
                bean.setReceiptNumber(obj.get("ReceiptNumber").toString());
                bean.setSourceDocumentCode(obj.get("SourceDocumentCode").toString());
                bean.setUomCode(obj.get("UOMCode").toString());
                bean.setVendorName(obj.get("VendorName").toString()==null 
                                   ||obj.get("VendorName").toString().isEmpty()
                                   ||obj.get("VendorName").toString()=="null"?"":obj.get("VendorName").toString());
                if(!objData.has("sec") ||
                   obj.get("SourceDocumentCode").toString().equalsIgnoreCase(objData.get("sec").toString())){
                    putAwayList.add(bean);  
                }
            }
                returnedRes=new JSONArray(putAwayList).toString();
            }else{
                returnedRes="No Put Away Lines Found";   
            }
            
        }else{
            returnedRes="No Put Away Lines Found";
        }
        System.out.println(returnedRes);
        return returnedRes;
    }
   
   
    public String subInventories(){
        String returnedRes="";
        String restApi =
            resthelper.getInstanceUrl() + resthelper.getSubinventoryRest();
        String response = resthelper.httpGet(restApi);
        
        JSONObject responseObj=new JSONObject(response);
        ArrayList<JSONObject> subInventiryList=new ArrayList<JSONObject>();
        
        if (responseObj.has("items")) {
            JSONArray array=new  JSONArray(responseObj.get("items").toString());
            if(array.length()>0){
            for(int i=0 ; i < array.length() ; i++){
              JSONObject obj=new JSONObject(array.get(i).toString());
                
                JSONObject obj2=new JSONObject();
                obj2.put("label",obj.get("SecondaryInventoryName").toString());
                obj2.put("value",obj.get("SecondaryInventoryName").toString());
                obj2.put("org",obj.get("OrganizationCode").toString());
                subInventiryList.add(obj2);
            }
                returnedRes=new JSONArray(subInventiryList).toString();
            }else{
                returnedRes="No SubInventroy Found";   
            }
            
        }else{
            returnedRes="No SubInventroy Found";
        }
        System.out.println(returnedRes);
        return returnedRes;
    }
    public static void main(String[] args) throws SAXException, IOException {
        
   
    }
}
