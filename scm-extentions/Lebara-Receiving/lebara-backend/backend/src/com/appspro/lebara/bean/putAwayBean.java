package com.appspro.lebara.bean;

public class putAwayBean {
    
    
    private String receiptNumber;
    private String organizationCode;
    private String vendorName;
    private String employeeName;
    private String subInventory;
    private String parentTransactionId;
    private String sourceDocumentCode;
    private String documentNumber;
    private String docLineNumber;
    private String itemNumber;
    private String quantity;
    private String uomCode;
    private String frmSerial;
    private String toSerial;
    private String itemDesc;
    private String count;


    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setSubInventory(String subInventory) {
        this.subInventory = subInventory;
    }

    public String getSubInventory() {
        return subInventory;
    }

    public void setParentTransactionId(String parentTransactionId) {
        this.parentTransactionId = parentTransactionId;
    }

    public String getParentTransactionId() {
        return parentTransactionId;
    }

    public void setSourceDocumentCode(String sourceDocumentCode) {
        this.sourceDocumentCode = sourceDocumentCode;
    }

    public String getSourceDocumentCode() {
        return sourceDocumentCode;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocLineNumber(String docLineNumber) {
        this.docLineNumber = docLineNumber;
    }

    public String getDocLineNumber() {
        return docLineNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setUomCode(String uomCode) {
        this.uomCode = uomCode;
    }

    public String getUomCode() {
        return uomCode;
    }

    public void setFrmSerial(String frmSerial) {
        this.frmSerial = frmSerial;
    }

    public String getFrmSerial() {
        return frmSerial;
    }

    public void setToSerial(String toSerial) {
        this.toSerial = toSerial;
    }

    public String getToSerial() {
        return toSerial;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCount() {
        return count;
    }
}
