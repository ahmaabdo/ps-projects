package com.appspro.lebara.rest;

import com.appspro.lebara.bean.linesToReceiveBean;

import com.appspro.lebara.dao.linesToReceiveDAO;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/linesToReceive")

public class linesToReceiveRest {
  
    @POST
    @Path("/getLinesToReceive")
    @Consumes("application/json")
    @Produces("application/json")
    
    public Response lineToReceive (String body) throws MalformedURLException,IOException {
        
        linesToReceiveDAO linesDao = new linesToReceiveDAO();
                linesToReceiveBean bean = new linesToReceiveBean();
                
                JSONObject obj = new JSONObject(body);
                JSONObject respOgj = new JSONObject();
                String   res;
                try{
//                    bean.setReceiptNumber(obj.get("*Receipt Number").toString().trim());
                    bean.setDocumentLineNumber(obj.get("*Document Line").toString().trim());
                    bean.setQuantity(obj.get("*Quantity").toString().trim());
//                    bean.setSubinventory(obj.get("*Subinventory").toString().trim());
//                    bean.setFromSerial(obj.get("*From Serial").toString().trim());
//                    bean.setToSerial(obj.get("*To Serial").toString().trim());
                    bean.setEmployeeName(obj.get("*Employee Name").toString().trim());
                    bean.setDocumentNumber(obj.get("*Document Number").toString().trim());
                }catch(Exception e){
                    respOgj.put("status","Error");
                    respOgj.put("message",e.getMessage());
                    res=respOgj.toString();
                }
                res = linesDao.getlinesToReceive(bean);
               
                return  Response.ok(res.toString()).build();
    }
    
}
