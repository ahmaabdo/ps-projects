package common.Soap;

import common.restHelper.RestHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import java.util.TimeZone;

import org.json.JSONObject;

import org.w3c.dom.Document;

public class SubmitPutAwaySOAP {
    
    RestHelper resthelper = new RestHelper();
   
    public String createPutAway(String body){
        
        JSONObject dataObj=new JSONObject(body);
        StringBuilder soapBuilder=new StringBuilder();
        JSONObject response = new JSONObject();
        
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+00:00'");
        Date datef = new Date();
        sdf.setTimeZone(TimeZone.getTimeZone("GTM+3"));
            String date = sdf.format(datef);
        
        soapBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/receiving/receiptsInterface/transactions/processorServiceV2/types/\" xmlns:proc=\"http://xmlns.oracle.com/apps/scm/receiving/receiptsInterface/transactions/processorServiceV2/\" xmlns:rcv=\"http://xmlns.oracle.com/apps/flex/scm/receiving/common/protectedUiModel/rcvTxnIntfInvStripingDFF/\">\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:processor>\n" + 
        "         <typ:Receipt>\n" + 
        "            <proc:TransactionType>NEW</proc:TransactionType>\n" + 
        "            <proc:ReceiptNumber>");
        soapBuilder.append(dataObj.get("receiptNumber").toString());
        soapBuilder.append("</proc:ReceiptNumber>\n" + 
        "            <proc:ASNType>STD</proc:ASNType>\n" + 
        "            <proc:ShipToOrganizationCode>");
        soapBuilder.append(dataObj.get("organizationCode").toString());
        soapBuilder.append("</proc:ShipToOrganizationCode>\n" + 
        "            <proc:VendorName>");
        soapBuilder.append(dataObj.get("vendorName").toString());
        soapBuilder.append("</proc:VendorName>\n" + 
        "            <proc:EmployeeName>");
        soapBuilder.append(dataObj.get("employeeName").toString());
        soapBuilder.append("</proc:EmployeeName>\n" + 
        "            <proc:StagedReceivingTransaction>\n" );
        soapBuilder.append("<proc:Attribute1>");
        soapBuilder.append(dataObj.get("batchNumber").toString());
        soapBuilder.append("</proc:Attribute1> <proc:Subinventory>");
        soapBuilder.append(dataObj.get("subInvenSelected").toString());
        soapBuilder.append("</proc:Subinventory>\n" + 
        "             <proc:ParentTransactionId>");
        soapBuilder.append(dataObj.get("parentTransactionId").toString());
        soapBuilder.append("</proc:ParentTransactionId>\n" +
        "               <proc:SourceDocumentCode>");
        soapBuilder.append(dataObj.get("sourceDocumentCode").toString());
        soapBuilder.append("</proc:SourceDocumentCode>\n" + 
        "               <proc:TransactionType>DELIVER</proc:TransactionType>\n" + 
        "               <proc:AutoTransactCode>DELIVER</proc:AutoTransactCode>\n" + 
        "               <proc:TransactionDate>");
        soapBuilder.append(date);
        soapBuilder.append("</proc:TransactionDate>\n" + 
        "               <proc:DocumentNumber>");
        soapBuilder.append(dataObj.get("documentNumber").toString());
        soapBuilder.append("</proc:DocumentNumber>\n" + 
        "               <proc:DocumentLineNumber>");
        soapBuilder.append(dataObj.get("docLineNumber").toString());
        soapBuilder.append("</proc:DocumentLineNumber>\n" + 
        "               <proc:ItemNumber>");
        soapBuilder.append(dataObj.get("itemNumber").toString());
        soapBuilder.append("</proc:ItemNumber>\n" + 
        "               <proc:Quantity unitCode=\""+dataObj.get("uomCode").toString()+"\">");
        soapBuilder.append(dataObj.get("quantity").toString());
        soapBuilder.append("</proc:Quantity>\n" + 
        "               <proc:UOMCode>");
        soapBuilder.append(dataObj.get("uomCode").toString());
        soapBuilder.append("</proc:UOMCode>\n" + 
        "               <proc:StagedTransactionSerial>\n" + 
        "                  <proc:FmSerialNumber>");
        soapBuilder.append(dataObj.get("fromSerial").toString());
        soapBuilder.append("</proc:FmSerialNumber>\n" + 
        "                  <proc:ToSerialNumber>");
        soapBuilder.append(dataObj.get("toSerial").toString());
        soapBuilder.append("</proc:ToSerialNumber>\n" + 
        "               </proc:StagedTransactionSerial>\n" + 
        "            </proc:StagedReceivingTransaction>\n" + 
        "         </typ:Receipt>\n" + 
        "      </typ:processor>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>");
        
        Document doc = null;
            doc = resthelper.httpPost(resthelper.getInstanceUrl() + resthelper.getSubmitInventory(),soapBuilder.toString());
        try{
            if (doc.getElementsByTagName("faultstring").getLength() > 0) {
            String message=doc.getElementsByTagName("faultstring").item(0).getTextContent();
                response.put("status", "Error");
                response.put("message", message);
            } else {
                if ("WARNING".equalsIgnoreCase(doc.getElementsByTagName("ns1:Status").item(0).getTextContent())) {
                    response.put("status", "Error");
                    System.out.println("----"+doc.getElementsByTagName("ns1:ErrorMessage").getLength());
                    response.put("message",
                                 doc.getElementsByTagName("ns1:ErrorMessage").getLength() >
                                 0 ?
                                 doc.getElementsByTagName("ns1:ErrorMessage").item(doc.getElementsByTagName("ns1:ErrorMessage").getLength()-1).getTextContent() :
                                 "Data is incorrect please chect it again");
                } else {
                    response.put("status", "Done");
                    response.put("message", "Success In Submiting Put Away");
                }
            }
        } catch (Exception e) {
            response.put("status", "Error");
            response.put("message","Your data is incorrect please check them and try again");
        }
            
            
        System.out.println(soapBuilder.toString());
        System.out.println("****"+response);
        return response.toString();
    }
}
