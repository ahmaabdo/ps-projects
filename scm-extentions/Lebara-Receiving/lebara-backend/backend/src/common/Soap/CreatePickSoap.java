package common.Soap;

import common.restHelper.RestHelper;
import java.util.ArrayList;
import java.util.HashMap;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;

public class CreatePickSoap {
    private static HashMap<String, ArrayList<String>> dataValuesMulti =
        new HashMap<String, ArrayList<String>>();
    private static String[] header =
    { "*Pick Slip", "*Line", "*Picked Quantity", "* Subinventory",
      "*From Serial Number", "*To Serial Number" };

    public static JSONObject createExcelPick(String data) {
        JSONObject response = new JSONObject();
        try {
            JSONArray arr = new JSONArray(data);
            for (int j = 0; j < header.length; j++) {
                ArrayList<String> array=new ArrayList<String>();
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = new JSONObject(arr.get(i).toString());
                    array.add(obj.get(header[j]).toString());
                }
                dataValuesMulti.put(header[j],array);
            }
            System.out.println(dataValuesMulti);
              response = createRestApiRequest(dataValuesMulti);
//            response = createSoapRequestMulti(dataValuesMulti);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "Error");
            response.put("message", e.getMessage().replaceAll("JSONObject", ""));
        }
        return response; //error while try later
    }
    public static JSONObject createRestApiRequest(HashMap<String, ArrayList<String>> data) {
        RestHelper restHelper = new RestHelper();
        JSONObject response = new JSONObject();


        JSONArray lines = new JSONArray();
        JSONObject dataObject = new JSONObject();
        String body;
        /***** read data from hashMap ***************/
        for(int i=0 ;i<data.get("*From Serial Number").size();i++){
            JSONObject serialObject = new JSONObject();
            JSONArray serials = new JSONArray();
            JSONObject lineObject = new JSONObject();
            serialObject.put("FromSerialNumber", data.get("*From Serial Number").get(i).trim());
            serialObject.put("ToSerialNumber", data.get("*To Serial Number").get(i).trim());
            serials.put(serialObject);
            ///////
            lineObject.put("PickSlip", data.get("*Pick Slip").get(i).trim());
            lineObject.put("PickSlipLine", data.get("*Line").get(i).trim());
            lineObject.put("PickedQuantity", data.get("*Picked Quantity").get(i).trim());
            lineObject.put("SubinventoryCode", data.get("* Subinventory").get(i).trim());
            lineObject.put("serialItemSerials", serials);
            ///////
            lines.put(lineObject);  
        }
        
        dataObject.put("pickLines", lines);
        body = dataObject.toString();
        /*****************************************/
        /******** pick Rest****************/
        String data2=restHelper.httpPostUrl(restHelper.getInstanceUrl() + restHelper.getPickURLRest(),body);
                    JSONObject returnedData=new JSONObject(data2);
                    if (returnedData.has("ReturnStatus") && "Error".equalsIgnoreCase(returnedData.get("ReturnStatus").toString())) {
                        response.put("status", "Error");
                        response.put("message",returnedData.get("ErrorExplanation").toString());
                    }else{
                        response.put("status", "Done");
                        response.put("message", "Success in pick confirmation");
                    }
        /******************************/
        return response;
    }

    public static JSONObject createSoapRequestMulti(HashMap<String, ArrayList<String>> data) {
        RestHelper restHelper = new RestHelper();
        JSONObject response = new JSONObject();
    
        StringBuilder pickLines = new StringBuilder(); 
        StringBuilder soapBuilder=new StringBuilder();
        
        for(int i=0 ;i<data.get("*From Serial Number").size();i++){
           pickLines.append("<pic:PickSlipPick>\n" + 
           "               <pic1:PickedQuantity unitCode=\"EA\">"+data.get("*Picked Quantity").get(i)+"</pic1:PickedQuantity>\n" + 
           "               <pic1:PickSlip>"+data.get("*Pick Slip").get(i)+"</pic1:PickSlip>\n" + 
           "               <pic1:Line>"+data.get("*Line").get(i)+"</pic1:Line>\n" + 
           "               <pic1:SourceSubinventory>"+data.get("* Subinventory").get(i)+"</pic1:SourceSubinventory>\n" + 
           "               <pic1:TransactionDate>"+new DateTime()+"</pic1:TransactionDate>\n" + 
           "               <pic1:PickSerial>\n" + 
           "                  <pic1:Quantity unitCode=\"EA\">"+data.get("*Picked Quantity").get(i)+"</pic1:Quantity>\n" + 
           "                  <pic1:FromSerialNumber>"+data.get("*From Serial Number").get(i)+"</pic1:FromSerialNumber>\n" + 
           "                  <pic1:ToSerialNumber>"+data.get("*To Serial Number").get(i)+"</pic1:ToSerialNumber>\n" + 
           "               </pic1:PickSerial>\n" + 
           "            </pic:PickSlipPick>"); 
        }
        
        soapBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/inventory/picking/pickConfirm/pickConfirmService/types/\" xmlns:pic=\"http://xmlns.oracle.com/apps/scm/inventory/picking/pickConfirm/pickConfirmService/\" xmlns:pic1=\"http://xmlns.oracle.com/apps/scm/inventory/picking/pickConfirm/pickConfirmService\">​\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:confirmPicks>\n" + 
        "         <typ:Commit>Y</typ:Commit>\n" + 
        "         <typ:TransactionMode>1</typ:TransactionMode>\n" + 
        "         <typ:PickSlipLines>");
        soapBuilder.append(pickLines);
        soapBuilder.append("</typ:PickSlipLines>\n" + 
        "      </typ:confirmPicks>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>");
        
        System.out.println(soapBuilder.toString());
        Document doc = null;
            doc = restHelper.httpPost(restHelper.getInstanceUrl() + restHelper.getPickURLSoap(),soapBuilder.toString());
            try{
            if (doc.getElementsByTagName("tns:message").getLength() > 0) {
            String message=doc.getElementsByTagName("tns:message").item(0).getTextContent();
            message=message.replaceAll("JBO-FND:::FND_CMN_PROC_INTERNAL_ERROR: <MESSAGE><NUMBER>FND-3</NUMBER><TEXT>", "");
            message=message.replaceAll("</TEXT><CAUSE></CAUSE><ACTION></ACTION><DETAILS></DETAILS><INCIDENT></INCIDENT></MESSAGE>", "");
            System.out.println();
                response.put("status", "Error");
                response.put("message", message);
            } else {
                if ("E".equalsIgnoreCase(doc.getElementsByTagName("ns2:ReturnStatus").item(0).getTextContent())) {
                    response.put("status", "Error");
                    response.put("message",
                                 doc.getElementsByTagName("ns1:MsgData").getLength() >
                                 0 ?
                                 doc.getElementsByTagName("ns1:MsgData").item(0).getTextContent() :
                                 "Data is incorrect please chect it again");
                } else {
                    response.put("status", "Done");
                    response.put("message", "Success in pick confirmation");
                }
            }
        } catch (Exception e) {
            response.put("status", "Error");
            response.put("message","Your data is incorrect please check them and try again");
        }
        return response;
    }

    public static void main(String[] args) {
        String EXCEL_SHEET_PATH =
            "C:/Users/Eslam/Desktop/Lebara Confirm Pick Slips.xlsx";

                  createExcelPick("[{\"*Pick Slip\":\"6009\",\"*Line\":\"3\",\"*Picked Quantity\":\"50\",\"* Subinventory\":\"IT-STG-SI\",\"*From Serial Number\":\"1000000000\",\"*To Serial Number\":\"1000000049 \"},{\"*Pick Slip\":\"6009\",\"*Line\":\"4\",\"*Picked Quantity\":\"500\",\"* Subinventory\":\"IT-STG-SI\",\"*From Serial Number\":\"200114000800300\",\"*To Serial Number\":\"200114000800799 \"}]");
    }

}
