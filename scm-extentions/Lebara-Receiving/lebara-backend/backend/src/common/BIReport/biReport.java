package common.BIReport;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;


public class biReport extends ReportModel {


    public String getReportCall(String reportName,
                                Map<String, String> reportParams) {
        try {
            if (reportName != null) {
                JSONObject xmlJSONObj = runReport(reportName, reportParams);
                System.out.println("*************"+xmlJSONObj.toString());
                String jsonString =
                    xmlJSONObj.getJSONObject("DATA_DS").get("G_1").toString().replaceAll("VALUE",
                                                                                         "value").replaceAll("LABEL",
                                                                                                             "label");
                return jsonString;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
