package common.Soap;

import com.appspro.nhc.bean.ContractLinesBean;
import com.appspro.nhc.bean.CreateContractBean;

import common.restHelper.CommonConfigReader;
import common.restHelper.RestHelper;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class updateContractSoap {

    RestHelper restHelper = new RestHelper();
    String desUrl = restHelper.getInstanceUrl() + restHelper.getContractUrl();

// make contract under amendment if it status is active
    public JSONObject amendContract(CreateContractBean contractBean) {
        JSONObject objResponse = new JSONObject();
        StringBuilder updatedLineBuilder = new StringBuilder();
        try{
            updatedLineBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\" xmlns:con=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/\">\n" ); 
            updatedLineBuilder.append("<soapenv:Header/> <soapenv:Body><typ:amendContract><typ:amendContractParams>\n"); 
            updatedLineBuilder.append("<con:Id>");
            updatedLineBuilder.append(contractBean.getId());
            updatedLineBuilder.append("</con:Id><con:ExternalSourceKey>");
            updatedLineBuilder.append(contractBean.getId());
            updatedLineBuilder.append("</con:ExternalSourceKey><con:VersionFlag>true</con:VersionFlag>");
            updatedLineBuilder.append("</typ:amendContractParams></typ:amendContract></soapenv:Body></soapenv:Envelope>");
            Document doc = restHelper.httpPostNew(desUrl, updatedLineBuilder.toString());
            if(doc.getElementsByTagName("ns0:amendContractResponse").getLength()>0){
                 objResponse.put("status", "success");
                 objResponse.put("message","Contract with number have been amended successfully");
            }else{
                Element element2=(Element)doc.getElementsByTagName("detail").item(0);   
                String message=element2.getElementsByTagName("tns:message").item(0).getTextContent(); 
                message=message.replaceAll("</?[A-Za-z]+>", "");
                message=message.replaceFirst("JBO-OKC:::OKC_AUTH_CLN_INT_VAL", "");
                message=message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_END_ERR", "");
                objResponse.put("status", "error");
                objResponse.put("message",message);
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            objResponse.put("status", "error");
            objResponse.put("message",e.getMessage());   
        }
        return objResponse;
    }
    
    // update Contract
    public JSONObject updateContract(CreateContractBean contractBean,int updateType){
        JSONObject objResponse=new JSONObject();
        StringBuilder updatedSoapBuilder = new StringBuilder();
        StringBuilder newLinesSoapBuilder = new StringBuilder();
        try{
            updatedSoapBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\" xmlns:con=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/\" xmlns:head=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/header/flex/header/\" xmlns:par=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/partyContact/\" xmlns:par1=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/party/\" xmlns:lin=\"http://xmlns.oracle.com/apps/flex/contracts/coreAuthoring/lines/\" xmlns:con1=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractPublicService/\" xmlns:con2=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractBillingService/\" xmlns:bil=\"http://xmlns.oracle.com/apps/projects/billing/contracts/flex/BilllPlanDff/\">\n"); 
            updatedSoapBuilder.append("<soapenv:Header/><soapenv:Body><typ:updateContract><typ:contractHeader>\n" );
            updatedSoapBuilder.append("<con:ExternalKey>");
            updatedSoapBuilder.append(contractBean.getId());// id of the contract
            updatedSoapBuilder.append("</con:ExternalKey><con:ExternalSource>FUSION</con:ExternalSource>\n");
            updatedSoapBuilder.append("<con:FreightTerms>");
            updatedSoapBuilder.append(contractBean.getFreightTerms());// freight terms
            updatedSoapBuilder.append("</con:FreightTerms><con:FOB>");
            updatedSoapBuilder.append(contractBean.getFOB());// fob
            updatedSoapBuilder.append("</con:FOB>\n" );
            updatedSoapBuilder.append("<con:StartDate>");
            updatedSoapBuilder.append(contractBean.getStartDate());
            updatedSoapBuilder.append("</con:StartDate>\n" );
            updatedSoapBuilder.append("<con:EndDate>");
            updatedSoapBuilder.append(contractBean.getEndDate());
            updatedSoapBuilder.append("</con:EndDate>\n" );
            // if contract is under amendment then put effective date
            if("UNDER_AMENDMENT".equalsIgnoreCase(contractBean.getStatus())){
                updatedSoapBuilder.append("<con:AmendmentEffectiveDate>");
                updatedSoapBuilder.append(contractBean.getUpdateDate());// date of contract start
                updatedSoapBuilder.append("</con:AmendmentEffectiveDate>\n" );  
            }
            updatedSoapBuilder.append("<con:ContractHeaderFlexfieldVA>");
            
            if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
                updatedSoapBuilder.append("<head:poNumber>");
                updatedSoapBuilder.append(contractBean.getOrderNumber());
                updatedSoapBuilder.append("</head:poNumber>");
            } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
                updatedSoapBuilder.append("<head:relatedPoNumber>");
                updatedSoapBuilder.append(contractBean.getOrderNumber());
                updatedSoapBuilder.append("</head:relatedPoNumber>");
            }            
            if(Double.parseDouble(contractBean.getVat()) >0){
                updatedSoapBuilder.append("<head:vat>");
                updatedSoapBuilder.append(contractBean.getVat());
                updatedSoapBuilder.append("</head:vat>");
                updatedSoapBuilder.append("<head:totalWithVat>");
                updatedSoapBuilder.append(contractBean.getTotalWithVat());
                updatedSoapBuilder.append("</head:totalWithVat>");
            }else{
            updatedSoapBuilder.append("<head:vat>0</head:vat>");
            updatedSoapBuilder.append("<head:totalWithVat>");
            updatedSoapBuilder.append(contractBean.getTotalWithVat());
            updatedSoapBuilder.append("</head:totalWithVat>"); 
        }
           
            updatedSoapBuilder.append("</con:ContractHeaderFlexfieldVA>\n");
            for(int i=0 ; i<contractBean.getLines().size() ; i++){
                StringBuilder updatedLineBuilder=new  StringBuilder();
                StringBuilder newLinesBuilder=new StringBuilder();
                ContractLinesBean Linesbean = contractBean.getLines().get(i);
                String text = Linesbean.getDeliveryDate();
                
                /************** new lines to add *********************/
                if(Linesbean.getLineId() ==null ||
                    "null".equalsIgnoreCase(Linesbean.getLineId()) ||
                    " ".equalsIgnoreCase(Linesbean.getLineId())) {

                    if (Linesbean.getPrice().equals("")) {
                        newLinesBuilder.append("<con:ContractLine>\n");
                        newLinesBuilder.append(" <con:ItemNameTxt>new line</con:ItemNameTxt>");
                        newLinesBuilder.append("<con:ItemDescription>");
                        newLinesBuilder.append(Linesbean.getDescription());
                        newLinesBuilder.append("</con:ItemDescription>\n");
                        newLinesBuilder.append("<con:PurchasingCategory>");
                        newLinesBuilder.append(Linesbean.getCategory().replaceAll("&","&amp;").toString());
                        newLinesBuilder.append("</con:PurchasingCategory>\n");
                        newLinesBuilder.append("<con:RecInvOrgName>");
                        newLinesBuilder.append(Linesbean.getShipToOrganizationName());
                        newLinesBuilder.append("</con:RecInvOrgName>\n");
                        newLinesBuilder.append("<con:PaymentTerms>");
                        newLinesBuilder.append(contractBean.getPaymentTerms());
                        newLinesBuilder.append("</con:PaymentTerms>\n");
                        newLinesBuilder.append("<con:ShipToOrgLocation>");
                        newLinesBuilder.append(Linesbean.getShipToLocation());
                        newLinesBuilder.append("</con:ShipToOrgLocation>\n");
                        newLinesBuilder.append("<con:StartDate>");
                        newLinesBuilder.append(contractBean.getStartDate());
                        newLinesBuilder.append("</con:StartDate>\n");
                        newLinesBuilder.append("<con:DeliveryDate>");
                        newLinesBuilder.append(text);
                        newLinesBuilder.append("</con:DeliveryDate>");
                        newLinesBuilder.append("<con:EndDate>");
                        newLinesBuilder.append(text);
                        newLinesBuilder.append("</con:EndDate>\n");
                        newLinesBuilder.append("<con:FixedPriceServiceYn>Y</con:FixedPriceServiceYn>\n");
                        newLinesBuilder.append("<con:RecInvOrgName>");
                        newLinesBuilder.append(Linesbean.getShipToOrganizationName());
                        newLinesBuilder.append("</con:RecInvOrgName>\n");
                        newLinesBuilder.append("<con:LineValueAmount currencyCode=\"");
                        newLinesBuilder.append(Linesbean.getOrderedCurrency());
                        newLinesBuilder.append("\">");
                        newLinesBuilder.append(Linesbean.getOrdered());
                        newLinesBuilder.append("</con:LineValueAmount>\n");
                        newLinesBuilder.append("<con:LineAmount currencyCode=\"");
                        newLinesBuilder.append(Linesbean.getOrderedCurrency());
                        newLinesBuilder.append("\">");
                        newLinesBuilder.append(Linesbean.getOrdered());
                        newLinesBuilder.append("</con:LineAmount>\n");
                        newLinesBuilder.append("</con:ContractLine>\n");

                    } else if(Double.parseDouble(Linesbean.getQuantity()) >0){
                        newLinesBuilder.append("<con:ContractLine>\n");
                        newLinesBuilder.append(" <con:ItemNameTxt>new line</con:ItemNameTxt>");
                        newLinesBuilder.append("<con:ItemDescription>");
                        newLinesBuilder.append(Linesbean.getDescription());
                        newLinesBuilder.append("</con:ItemDescription>\n");
                        newLinesBuilder.append("<con:PurchasingCategory>");
                        newLinesBuilder.append(Linesbean.getCategory().replaceAll("&","&amp;").toString());
                        newLinesBuilder.append("</con:PurchasingCategory>\n");
                        newLinesBuilder.append("<con:RecInvOrgName>");
                        newLinesBuilder.append(Linesbean.getShipToOrganizationName());
                        newLinesBuilder.append("</con:RecInvOrgName>\n");
                        newLinesBuilder.append("<con:PaymentTerms>");
                        newLinesBuilder.append(contractBean.getPaymentTerms());
                        newLinesBuilder.append("</con:PaymentTerms>\n");
                        newLinesBuilder.append("<con:ShipToOrgLocation>");
                        newLinesBuilder.append(Linesbean.getShipToLocation());
                        newLinesBuilder.append("</con:ShipToOrgLocation>\n");
                        newLinesBuilder.append("<con:StartDate>");
                        newLinesBuilder.append(contractBean.getStartDate());
                        newLinesBuilder.append("</con:StartDate>\n");
                        newLinesBuilder.append("<con:DeliveryDate>");
                        newLinesBuilder.append(text);
                        newLinesBuilder.append("</con:DeliveryDate>");
                        newLinesBuilder.append("<con:EndDate>");
                        newLinesBuilder.append(text);
                        newLinesBuilder.append("</con:EndDate>\n");
                        newLinesBuilder.append("<con:UOMCode>");
                        newLinesBuilder.append(Linesbean.getUOM());
                        newLinesBuilder.append("</con:UOMCode><con:FixedPriceServiceYn>N</con:FixedPriceServiceYn>  \n");
                        newLinesBuilder.append("<con:ItemQuantity unitCode=\"");
                        newLinesBuilder.append(Linesbean.getUOM());
                        newLinesBuilder.append("\">");
                        newLinesBuilder.append(Linesbean.getQuantity());
                        newLinesBuilder.append("</con:ItemQuantity> \n");
                        newLinesBuilder.append("<con:UnitPrice currencyCode=\"");
                        newLinesBuilder.append(Linesbean.getPriceCurrency());
                        newLinesBuilder.append("\">");
                        newLinesBuilder.append(Linesbean.getPrice());
                        newLinesBuilder.append("</con:UnitPrice> \n");
                        newLinesBuilder.append("</con:ContractLine>\n");

                    }
                   
                    newLinesSoapBuilder.append(newLinesBuilder);
                } /************** lines to update **********************/
                else {

                    if (Linesbean.isDelete() == true) {
                    
                        updatedLineBuilder.append("<con:ContractLine>\n");
                        updatedLineBuilder.append("<con:LineId>");
                        updatedLineBuilder.append(Linesbean.getLineId());
                        updatedLineBuilder.append("</con:LineId>\n");
                        
                        if ("".equalsIgnoreCase(Linesbean.getPrice())||
                            "null".equalsIgnoreCase(Linesbean.getPrice()) ||
                             Linesbean.getPrice().isEmpty()) {

                            updatedLineBuilder.append("<con:LineAmount currencyCode=\"");
                            updatedLineBuilder.append(Linesbean.getOrderedCurrency());
                            updatedLineBuilder.append("\">0.00</con:LineAmount>\n");

                        } else {
                            
                            updatedLineBuilder.append("<con:UnitPrice currencyCode=\"");
                            updatedLineBuilder.append(Linesbean.getPriceCurrency());
                            updatedLineBuilder.append("\">0</con:UnitPrice> \n");
                            updatedLineBuilder.append("<con:ItemQuantity unitCode=\"");
                            updatedLineBuilder.append(Linesbean.getOrderedCurrency());
                            updatedLineBuilder.append("\">");
                            updatedLineBuilder.append(Linesbean.getQuantity());
                            updatedLineBuilder.append("</con:ItemQuantity> \n");
                            
                        }
                    } else {

                        updatedLineBuilder.append("<con:ContractLine>\n");
                        updatedLineBuilder.append("<con:LineId>");
                        updatedLineBuilder.append(Linesbean.getLineId());
                        updatedLineBuilder.append("</con:LineId>\n");
                        updatedLineBuilder.append("<con:ItemDescription>");
                        updatedLineBuilder.append(Linesbean.getDescription());
                        updatedLineBuilder.append("</con:ItemDescription>\n");
                        updatedLineBuilder.append("<con:PurchasingCategory>");
                        updatedLineBuilder.append(Linesbean.getCategory().replaceAll("&","&amp;").toString());
                        updatedLineBuilder.append("</con:PurchasingCategory>\n");
                        updatedLineBuilder.append("<con:PaymentTerms>");
                        updatedLineBuilder.append(contractBean.getPaymentTerms());
                        updatedLineBuilder.append("</con:PaymentTerms>\n");
                        updatedLineBuilder.append("<con:StartDate>");
                        updatedLineBuilder.append(contractBean.getStartDate());
                        updatedLineBuilder.append("</con:StartDate>\n");
                        updatedLineBuilder.append("<con:DeliveryDate>");
                        updatedLineBuilder.append(text);
                        updatedLineBuilder.append("</con:DeliveryDate>\n");
                        updatedLineBuilder.append("<con:EndDate>");
                        updatedLineBuilder.append(text);
                        updatedLineBuilder.append("</con:EndDate>\n");

                        if (Linesbean.getPrice().equals("")) {
                        
                            updatedLineBuilder.append("<con:FixedPriceServiceYn>Y</con:FixedPriceServiceYn>\n");
                            updatedLineBuilder.append("<con:LineValueAmount currencyCode=\"");
                            updatedLineBuilder.append(Linesbean.getOrderedCurrency());
                            updatedLineBuilder.append("\">");
                            updatedLineBuilder.append(Linesbean.getOrdered());
                            updatedLineBuilder.append("</con:LineValueAmount>\n");
                            updatedLineBuilder.append("<con:LineAmount currencyCode=\"");
                            updatedLineBuilder.append(Linesbean.getOrderedCurrency());
                            updatedLineBuilder.append("\">");
                            updatedLineBuilder.append(Linesbean.getOrdered());
                            updatedLineBuilder.append("</con:LineAmount>\n");
                            
                        } else if(Double.parseDouble(Linesbean.getQuantity()) >0){
                            
                            updatedLineBuilder.append("<con:UOMCode>");
                            updatedLineBuilder.append(Linesbean.getUOM());
                            updatedLineBuilder.append("</con:UOMCode> \n");
                            updatedLineBuilder.append("<con:FixedPriceServiceYn>N</con:FixedPriceServiceYn>  \n");
                            updatedLineBuilder.append("<con:ItemQuantity unitCode=\"");
                            updatedLineBuilder.append(Linesbean.getUOM());
                            updatedLineBuilder.append("\">");
                            updatedLineBuilder.append(Linesbean.getQuantity());
                            updatedLineBuilder.append("</con:ItemQuantity> \n");
                            updatedLineBuilder.append("<con:UnitPrice currencyCode=\"");
                            updatedLineBuilder.append(Linesbean.getPriceCurrency());
                            updatedLineBuilder.append("\">");
                            updatedLineBuilder.append(Linesbean.getPrice());
                            updatedLineBuilder.append("</con:UnitPrice> \n");
                            
                        }

                    }
                    updatedLineBuilder.append("</con:ContractLine>\n");
                    updatedSoapBuilder.append(updatedLineBuilder);

                }       
                /**************************************/
            }
            updatedSoapBuilder.append("</typ:contractHeader></typ:updateContract></soapenv:Body></soapenv:Envelope>");
            Document doc = restHelper.httpPostNew(desUrl, updatedSoapBuilder.toString());
            if(doc.getElementsByTagName("ns1:ContractNumber").getLength()>0){
                 objResponse.put("status", "success");
                 objResponse.put("message","Contract with number "+doc.getElementsByTagName("ns1:ContractNumber").item(0).getTextContent()+" have been updated successfully");
                 if(updateType==2){
                 objResponse=addNewLinesToContract(newLinesSoapBuilder,contractBean);
                 }
            }else{
                Element element2=(Element)doc.getElementsByTagName("detail").item(0);   
                String message=element2.getElementsByTagName("tns:message").item(element2.getElementsByTagName("tns:message").getLength()-1).getTextContent(); 
                message=message.replaceAll("</?[A-Za-z]+>", "");
                message=message.replaceFirst("JBO-OKC:::OKC_AUTH_CLN_INT_VAL", "");
                message=message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_END_ERR", "");
                message=message.replaceFirst(": OKC-", "");
                message=message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_START_ERR195449", "");
                message=message.replaceFirst("The need-by date", "Promised Delivery Date of Po Lines  ");
                objResponse.put("status", "error");
                objResponse.put("message",message);
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            objResponse.put("status", "error");
            objResponse.put("message",e.getMessage());   
        }
        return objResponse;
    }
    
    public JSONObject addNewLinesToContract(StringBuilder linesBuilder,CreateContractBean contractBean){
        JSONObject objResponse=new JSONObject();
        StringBuilder soapBuilder = new StringBuilder();
        soapBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\" xmlns:con=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/\" xmlns:head=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/header/flex/header/\" xmlns:par=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/partyContact/\" xmlns:par1=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/party/\" xmlns:lin=\"http://xmlns.oracle.com/apps/flex/contracts/coreAuthoring/lines/\" xmlns:con1=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractPublicService/\" xmlns:con2=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractBillingService/\" xmlns:bil=\"http://xmlns.oracle.com/apps/projects/billing/contracts/flex/BilllPlanDff/\">\n" );
        soapBuilder.append("<soapenv:Header/><soapenv:Body><typ:mergeContractInAllStatus><typ:contractHeader><con:ContractId>");
        soapBuilder.append(contractBean.getContractId());
        soapBuilder.append("</con:ContractId>\n"); 
        soapBuilder.append("<con:ContractTypeId>");
        soapBuilder.append(contractBean.getContractTypeId());
        soapBuilder.append("</con:ContractTypeId>\n"); 
        soapBuilder.append("<con:CurrencyCode>");
        soapBuilder.append(contractBean.getCurrencyCode());
        soapBuilder.append("</con:CurrencyCode>\n"); 
        soapBuilder.append("<con:LegalEntityId>");
        soapBuilder.append(contractBean.getLegalEntity());
        soapBuilder.append("</con:LegalEntityId>\n");
        soapBuilder.append("<con:BuyOrSell>B</con:BuyOrSell>\n");
        soapBuilder.append("<con:OrgId>");
        soapBuilder.append(contractBean.getOrgId());
        soapBuilder.append("</con:OrgId>");
        soapBuilder.append("<con:StartDate>");
        soapBuilder.append(contractBean.getStartDate());// fob
        soapBuilder.append("</con:StartDate>\n" );
        soapBuilder.append("<con:EndDate>");
        soapBuilder.append(contractBean.getEndDate());
        soapBuilder.append("</con:EndDate>\n" );
        soapBuilder.append(linesBuilder);
        soapBuilder.append(" </typ:contractHeader><typ:targetStatusCode/><typ:closeDate/><typ:closeReasonCode/>\n");
        soapBuilder.append("<typ:cancelReasonCode/><typ:versionFlag>true</typ:versionFlag></typ:mergeContractInAllStatus>\n"); 
        soapBuilder.append("</soapenv:Body></soapenv:Envelope>");
        Document doc = restHelper.httpPostNew(desUrl, soapBuilder.toString());
        if(doc.getElementsByTagName("ns1:ContractNumber").getLength()>0){
             objResponse.put("status", "success");
             objResponse.put("message","Contract with number "+doc.getElementsByTagName("ns1:ContractNumber").item(0).getTextContent()+" have been updated successfully");
        }else{
            Element element2=(Element)doc.getElementsByTagName("detail").item(0);   
            String message=element2.getElementsByTagName("tns:message").item(element2.getElementsByTagName("tns:message").getLength()-1).getTextContent(); 
            message=message.replaceAll("</?[A-Za-z]+>", "");
            message=message.replaceFirst("JBO-OKC:::OKC_AUTH_CLN_INT_VAL", "");
            message=message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_END_ERR", "");
            message=message.replaceFirst(": OKC-", "");
            objResponse.put("status", "error");
            objResponse.put("message",message);
        } 
        
    return objResponse;    
    }
}
