package common.Soap;

import com.appspro.nhc.bean.PurchaseOrderBean;
import com.appspro.nhc.bean.PurchaseOrderLineBean;

import org.w3c.dom.Document;

import common.restHelper.RestHelper;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PurchaseOrderSoap extends RestHelper {
    public String getPurchaseOrderByHeaderId(String PoHeaderId) throws Exception {

        String soap =
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/prc/po/editDocument/purchaseOrderServiceV2/types/\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <typ:getPurchaseOrder>\n" +
            "         <typ:poHeaderId>" + PoHeaderId + "</typ:poHeaderId>\n" +
            "      </typ:getPurchaseOrder>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";

        String desUrl = getInstanceUrl() + getPurchaseOrderUrl();
        Document doc = httpPost(desUrl, soap);
        PurchaseOrderBean orderBean = new PurchaseOrderBean();
        try {
            orderBean.setPOHeaderId(doc.getElementsByTagName("ns0:POHeaderId").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setPOHeaderId(null);
        }
        try {
            orderBean.setOrderNumber(doc.getElementsByTagName("ns0:OrderNumber").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setOrderNumber(null);
        }
        try {
            orderBean.setProcurementBU(doc.getElementsByTagName("ns0:ProcurementBusinessUnit").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setProcurementBU(null);
        }
        try {
            orderBean.setProcurementBUId(doc.getElementsByTagName("ns0:ProcurementBUId").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setProcurementBUId(null);
        }
        try {
            orderBean.setSoldToLegalEntityId(doc.getElementsByTagName("ns0:SoldToLegalEntityId").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSoldToLegalEntityId(null);
        }
        try {
            orderBean.setSoldToLegalEntity(doc.getElementsByTagName("ns0:SoldToLegalEntity").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSoldToLegalEntity(null);
        }
        try {
            orderBean.setBillToLocation(doc.getElementsByTagName("ns0:BillToLocationCode").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setBillToLocation(null);
        }
        try {
            orderBean.setStatus(doc.getElementsByTagName("ns0:DocumentStatus").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setStatus(null);
        }
        try {
            orderBean.setBuyer(doc.getElementsByTagName("ns0:BuyerName").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setBuyer(null);
        }
        try {
            orderBean.setCreationDate(doc.getElementsByTagName("ns0:CreationDate").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setCreationDate(null);
        }
        try {
            orderBean.setSupplierId(doc.getElementsByTagName("ns0:SupplierId").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSupplierId(null);
        }
        try {
            orderBean.setSupplier(doc.getElementsByTagName("ns0:Supplier").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSupplier(null);
        }
        try {
            orderBean.setSupplierContact(doc.getElementsByTagName("ns0:SupplierContactName").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSupplierContact(null);
        }
        try {
            orderBean.setSupplierSite(doc.getElementsByTagName("ns0:SupplierSiteCode").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSupplierSite(null);
        }
        try {
            orderBean.setCurrency(doc.getElementsByTagName("ns0:Currency").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setCurrency(null);
        }
        try {
            orderBean.setCurrencyCode(doc.getElementsByTagName("ns0:CurrencyCode").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setCurrencyCode(null);
        }
        try {
            Element element = (Element) doc.getElementsByTagName("ns0:OrderedAmount").item(0) ;
            orderBean.setOrdered(element.getTextContent());
        } catch (NullPointerException e) {
            orderBean.setOrdered(null);
        }
        try {
            Element element = (Element) doc.getElementsByTagName("ns0:TotalTaxAmount").item(0) ;
            orderBean.setTotalVat(element.getTextContent());
        } catch (NullPointerException e) {
            orderBean.setTotalVat(null);
        }
        try {
            Element element = (Element) doc.getElementsByTagName("ns0:TotalAmount").item(0) ;
            orderBean.setTotal(element.getTextContent());
            orderBean.setTotalCurrancy(element.getAttribute("currencyCode"));
        } catch (NullPointerException e) {
            orderBean.setTotal(null);
            orderBean.setTotalCurrancy(null);
        }
        try {
            orderBean.setDescription(doc.getElementsByTagName("ns0:DocumentDescription").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setDescription(null);
        }
        try {
            orderBean.setFundsStatus(doc.getElementsByTagName("ns0:FundsStatus").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setFundsStatus(null);
        }
        try {
            orderBean.setDescription(doc.getElementsByTagName("ns0:DocumentDescription").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setDescription(null);
        }
        try {
            orderBean.setDelayedPenalties(doc.getElementsByTagName("ns6:delayedPenalties").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setDelayedPenalties(null);
        }
        try {
            orderBean.setChargePercentage(doc.getElementsByTagName("ns6:chargePercentageWorkingDays").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setChargePercentage(null);
        }
        try {
            orderBean.setSavingAmount(doc.getElementsByTagName("ns6:savingAmount").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSavingAmount(null);
        }
        try {
            orderBean.setContractValue(doc.getElementsByTagName("ns6:contractValue").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setContractValue(null);
        }
        try {
            orderBean.setAdvancedPayment(doc.getElementsByTagName("ns6:__FLEX_Context").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setAdvancedPayment(null);
        }
        try {
            orderBean.setNoteToSupplier(doc.getElementsByTagName("ns0:NoteToSupplier").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setNoteToSupplier(null);
        }
        try {
            orderBean.setNoteToReceiver(doc.getElementsByTagName("ns0:NoteToReceiver").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setNoteToReceiver(null);
        }
        try {
            orderBean.setRequisition(doc.getElementsByTagName("ns0:Requisition").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setRequisition(null);
        }
        try {
            orderBean.setSourceAgreement(doc.getElementsByTagName("ns0:SourceAgreement").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSourceAgreement(null);
        }
        try {
            orderBean.setSupplierOrder(doc.getElementsByTagName("ns0:SupplierOrder").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSupplierOrder(null);
        }
        try {
            orderBean.setShipToLocation(doc.getElementsByTagName("ns0:ShipToLocationCode").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setShipToLocation(null);
        }
        try {
            orderBean.setEmail(doc.getElementsByTagName("ns0:SupplierContactEmail").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setEmail(null);
        }
        try {
            orderBean.setCommunicationMethod(doc.getElementsByTagName("ns0:SupplierCommunicationMethod").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setCommunicationMethod(null);
        }
        try {
            orderBean.setSite(doc.getElementsByTagName("ns0:SupplierSiteCode").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSite(null);
        }
        try {
            orderBean.setRequisitioningBU(doc.getElementsByTagName("ns0:RequisitioningBusinessUnit").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setRequisitioningBU(null);
        }
        try {
            orderBean.setRequiredAcknowledgment(doc.getElementsByTagName("ns0:RequiredAcknowledgment").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setRequiredAcknowledgment(null);
        }
        try {
            orderBean.setPaymentTerms(doc.getElementsByTagName("ns0:PaymentTerms").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setPaymentTerms(null);
        }
        try {
            orderBean.setShippingMethod(doc.getElementsByTagName("ns0:ShippingMethod").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setShippingMethod(null);
        }
        try {
            orderBean.setFreightTerms(doc.getElementsByTagName("ns0:FreightTermsCode").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setFreightTerms(null);
        }
        try {
            orderBean.setFOB(doc.getElementsByTagName("ns0:FOBCode").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setFOB(null);
        }
        try {
            orderBean.setTermsAndConditions(doc.getElementsByTagName("ns0:TermsAndConditions").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setTermsAndConditions(null);
        }
        try {
            orderBean.setSupplierSiteId(doc.getElementsByTagName("ns0:SupplierSiteId").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setSupplierSiteId(null);
        }
        try {
            orderBean.setPaymentTermsId(doc.getElementsByTagName("ns0:PaymentTermsId").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setPaymentTermsId(null);
        }
        try {
            orderBean.setDescription(doc.getElementsByTagName("ns0:DocumentDescription").item(0).getTextContent());
        } catch (NullPointerException e) {
            orderBean.setDescription(null);
        }
        /******************get purcahse order lines***************************/
        ArrayList<PurchaseOrderLineBean> lineList = new ArrayList<>();
        NodeList nList = doc.getElementsByTagName("ns0:PurchaseOrderLine");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date maxDate=formatter.parse("1940-12-01");
        Date minDate=formatter.parse(formatter.format(new Date()));
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element element = (Element)node;

                PurchaseOrderLineBean lineBean = new PurchaseOrderLineBean();
                Date date2=formatter.parse(element.getElementsByTagName("ns0:PromisedDate").item(0).getTextContent().toString());
                if(date2.after(maxDate)){
                    maxDate=date2;
                }else if(minDate.after(date2)){
                    minDate=date2;  
                }
                orderBean.setMaxEndDate(formatter.format(maxDate).toString());
                orderBean.setMinEndDate(formatter.format(minDate).toString());
                try {
                    lineBean.setLineNumber(element.getElementsByTagName("ns0:LineNumber").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setLineNumber(null);
                }
                try {
                    lineBean.setDescription(element.getElementsByTagName("ns0:ItemDescription").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setDescription(null);
                }
                try {
                    lineBean.setDescriptionCode(element.getElementsByTagName("ns0:ItemDescription").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setDescriptionCode(null);
                }
                try {
                    lineBean.setCategoryId(element.getElementsByTagName("ns0:CategoryId").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setCategoryId(null);
                }
                try {
                    lineBean.setCategoryName(element.getElementsByTagName("ns0:CategoryName").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setCategoryName(null);
                }
                try {
                    lineBean.setLineStatus(element.getElementsByTagName("ns0:LineStatus").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setLineStatus(null);
                }
                try {
                    Element elementLine = (Element) element.getElementsByTagName("ns0:OrderedAmount").item(0) ;
                    lineBean.setOrdered(elementLine.getTextContent());
                    lineBean.setOrderedCurrency(elementLine.getAttribute("currencyCode"));
                } catch (NullPointerException e) {
                    lineBean.setOrdered(null);
                    lineBean.setOrderedCurrency(null);
                }
                try {
                    Element elementLine = (Element) element.getElementsByTagName("ns0:TotalTaxAmount").item(0) ;
                    lineBean.setTotalTaxAmount(elementLine.getTextContent());
                    lineBean.setTotalTaxCurrency(elementLine.getAttribute("currencyCode"));
                } catch (NullPointerException e) {
                    lineBean.setTotalTaxAmount(null);
                }
                try {
                    Element elementLine = (Element) element.getElementsByTagName("ns0:TotalAmount").item(0) ;
                    lineBean.setTotalAmount(elementLine.getTextContent()+" "+elementLine.getAttribute("currencyCode"));
                } catch (NullPointerException e) {
                    lineBean.setTotalAmount(null);
                }
                try {
                    orderBean.setFundsStatus(element.getElementsByTagName("ns0:FundsStatus").item(0).getTextContent());
                } catch (NullPointerException e) {
                    orderBean.setFundsStatus(null);
                }
                try {
                    Element elementLine = (Element) element.getElementsByTagName("ns0:Price").item(0) ;
                    lineBean.setPrice(elementLine.getTextContent());
                    lineBean.setPriceCurrency(elementLine.getAttribute("currencyCode"));
                } catch (NullPointerException e) {
                    lineBean.setPrice(null);
                    lineBean.setPriceCurrency(null);
                }
                try {
                    lineBean.setLocation(element.getElementsByTagName("ns0:ShipToLocationCode").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setLocation(null);
                }
                try {
                    lineBean.setOrganization(element.getElementsByTagName("ns0:ShipToOrganizationName").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setOrganization(null);
                }
                try {

                    lineBean.setFundsStatus(element.getElementsByTagName("ns0:FundsStatus").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setFundsStatus(null);
                }
                try {
                    lineBean.setRequesterName(element.getElementsByTagName("ns0:RequesterName").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setRequesterName(null);
                }
                try {
                    lineBean.setQuantity(element.getElementsByTagName("ns0:Quantity").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setQuantity(null);
                }
                try {
                    lineBean.setUOM(element.getElementsByTagName("ns0:UnitOfMeasureCode").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setUOM(null);
                }
                try {
                    lineBean.setUomTitle(element.getElementsByTagName("ns0:UnitOfMeasure").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setUomTitle(null);
                }
                try {
                    lineBean.setShipToOrganizationName(element.getElementsByTagName("ns0:ShipToOrganizationName").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setShipToOrganizationName(null);
                }
                try {
                    lineBean.setRequestedDeliveryDate(element.getElementsByTagName("ns0:PromisedDate").item(0).getTextContent());
                } catch (NullPointerException e) {
                    lineBean.setRequestedDeliveryDate(null);
                }

                lineList.add(lineBean);

        }
        ArrayList<JSONObject> contractTypes=new ArrayList<JSONObject>();
        contractTypes=getContractTypes();
        orderBean.setContractTypeList(contractTypes);
        orderBean.setLineList(lineList);
        return new JSONObject(orderBean).toString();
    }
    
    public ArrayList<JSONObject> getContractTypes(){
        
        ArrayList<JSONObject> values=new ArrayList<JSONObject>();
        boolean hasMoreTypes = false;
        int brandOffset = 0;
        do {
                    String typesURL = getInstanceUrl()+getContractTypesURL() + "?limit=500&offset=" + brandOffset;
                    try {
                        String data = httpGet(typesURL);
                        JSONObject objData=new JSONObject(data);
                        hasMoreTypes = objData.getBoolean("hasMore");
                        JSONArray items = objData.getJSONArray("items");
                        brandOffset += 500;
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject obj = new JSONObject(items.get(i).toString());
                            if(obj.has("Value")){
                                JSONObject obj2=new JSONObject();
                                obj2.put("value", obj.get("Value").toString());
                                obj2.put("label", obj.get("Value").toString());
                                if("Y".equalsIgnoreCase(obj.get("EnabledFlag").toString())){
                                    values.add(obj2);  
                                }
                                
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } while (hasMoreTypes);
        return values;
    }
    
    public static void main(String [] args){
//        ArrayList<JSONObject> arrayList = new PurchaseOrderSoap().getContractTypes();
//        System.out.println(new JSONArray(arrayList).toString());
        
        String s="<soapenv:Header/>\n" + 
        "<soapenv:Body>\n" + 
        "<typ:mergeContractInAllStatus>\n" + 
        "<typ:contractHeader>\n" + 
        "<con:Id>300000016171986</con:Id>\n" + 
        "<con:MajorVersion>2</con:MajorVersion>\n" + 
        "<con:AmendmentEffectiveDate>2015-03-03</con:AmendmentEffectiveDate>\n" + 
        "<con:ContractLine>\n" + 
        "<con:LineNumber>2</con:LineNumber>\n" + 
        "<con:ItemDescription>Standard Desktop - update</con:ItemDescription>\n" + 
        "<con:UOMCode>zzy</con:UOMCode>\n" + 
        "</con:ContractLine>\n" + 
        "</typ:contractHeader>\n" + 
        "<typ:targetStatusCode></typ:targetStatusCode>\n" + 
        "<typ:closeDate></typ:closeDate>\n" + 
        "<typ:closeReasonCode></typ:closeReasonCode>\n" + 
        "<typ:cancelReasonCode></typ:cancelReasonCode>\n" + 
        "<typ:versionFlag>true</typ:versionFlag>\n" + 
        "</typ:mergeContractInAllStatus>\n" + 
        "</soapenv:Body>";
        RestHelper rest=new RestHelper();
       String desUrl=rest.getInstanceUrl()+rest.getContractUrl();
        rest.httpPostNew(desUrl, s);
    }
}
