package common.Soap;

import com.appspro.nhc.bean.ContractLinesBean;
import com.appspro.nhc.bean.CreateContractBean;

import common.restHelper.CommonConfigReader;
import common.restHelper.RestHelper;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

import java.nio.charset.CharsetDecoder;

import java.nio.charset.CharsetEncoder;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreateSoap extends RestHelper{
    public JSONObject CreateContract(CreateContractBean bean ,String desUrl) throws Exception {
        
        StringBuilder relatedPoNumber=new  StringBuilder();
        StringBuilder headerBuilder = new StringBuilder();
        StringBuilder linesBuilder = new StringBuilder();

        relatedPoNumber.append("<con:ContractHeaderFlexfieldVA>");

        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            relatedPoNumber.append("<head:poNumber>");
            relatedPoNumber.append(bean.getOrderNumber());
            relatedPoNumber.append("</head:poNumber>");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            relatedPoNumber.append("<head:relatedPoNumber>");
            relatedPoNumber.append(bean.getOrderNumber());
            relatedPoNumber.append("</head:relatedPoNumber>");
        }
            
        if(Double.parseDouble(bean.getVat()) >0){
            relatedPoNumber.append("<head:vat>");
            relatedPoNumber.append(bean.getVat());
            relatedPoNumber.append("</head:vat>");
            relatedPoNumber.append("<head:totalWithVat>");
            relatedPoNumber.append(bean.getTotalWithVat());
            relatedPoNumber.append("</head:totalWithVat>");
        }else{
            relatedPoNumber.append("<head:totalWithVat>");
            relatedPoNumber.append(bean.getTotalWithVat());
            relatedPoNumber.append("</head:totalWithVat>"); 
        }
       relatedPoNumber.append("</con:ContractHeaderFlexfieldVA>");
        // soap header
        headerBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\" xmlns:con=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/\" xmlns:head=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/header/flex/header/\" xmlns:par=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/partyContact/\" xmlns:par1=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/parties/flex/party/\" xmlns:lin=\"http://xmlns.oracle.com/apps/flex/contracts/coreAuthoring/lines/\" xmlns:con1=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractPublicService/\" xmlns:con2=\"http://xmlns.oracle.com/apps/projects/billing/contracts/contractBillingService/\" xmlns:bil=\"http://xmlns.oracle.com/apps/projects/billing/contracts/flex/BilllPlanDff/\">\n");
        headerBuilder.append("<soapenv:Header/>\n");
        headerBuilder.append("<soapenv:Body><typ:createContract><typ:contractHeader><con:ExternalSource>FUSION</con:ExternalSource>\n");
        headerBuilder.append("<con:ContractTypeId>");
        headerBuilder.append(bean.getContractTypeId());
        headerBuilder.append("</con:ContractTypeId>");
        headerBuilder.append("<con:OrgId>");
        headerBuilder.append(bean.getOrgId());
        headerBuilder.append("</con:OrgId>\n");
        headerBuilder.append("<con:PartyName>");
        headerBuilder.append(bean.getSupplier());
        headerBuilder.append("</con:PartyName><con:CurrencyCode>");
        headerBuilder.append(bean.getCurrencyCode());
        headerBuilder.append("</con:CurrencyCode><con:SupplierId>");
        headerBuilder.append(bean.getSupplierId());
        headerBuilder.append("</con:SupplierId><con:LegalEntityName>");
        headerBuilder.append(bean.getLegalEntity());
        headerBuilder.append("</con:LegalEntityName><con:ApTermsId>");
        headerBuilder.append(bean.getPaymentTermsId());
        headerBuilder.append("</con:ApTermsId><con:FreightTerms>");
        headerBuilder.append(bean.getFreightTerms());
        headerBuilder.append("</con:FreightTerms><con:FOB>");
        headerBuilder.append(bean.getFOB());
        headerBuilder.append("</con:FOB>");
//        headerBuilder.append("<con:OrderNumber>");
//        headerBuilder.append(bean.getOrderNumber());
//        headerBuilder.append("</con:OrderNumber>");
        headerBuilder.append("<con:EndDate>");
        headerBuilder.append(bean.getEndDate());
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            headerBuilder.append("</con:EndDate><con:InvOrgName>NHC Item Master </con:InvOrgName>\n");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            headerBuilder.append("</con:EndDate><con:InvOrgName>NHC Master Item</con:InvOrgName>\n");
        }
        headerBuilder.append(relatedPoNumber.toString());
        //    headerBuilder.append("<con:TaxAmount currencyCode=\"");
        //    headerBuilder.append(bean.getCurrencyCode());
        //    headerBuilder.append("\">");
        //    headerBuilder.append(relatedPoNumber.toString());
        //    headerBuilder.append("</con:TaxAmount>");
        
        ArrayList<ContractLinesBean> Lineslist =
            new ArrayList<ContractLinesBean>(bean.getLines());
        int count=1;
        for (int i = 0; i < Lineslist.size(); i++) {
            System.out.println(count);
            ContractLinesBean Linesbean = Lineslist.get(i);
            String deliveryDate = Linesbean.getDeliveryDate();
            
            if (Linesbean.getPrice().equals("")) {
                linesBuilder.append("<con:ContractLine>\n");
                if (bean.isLineNumberRequired() == true) {
                    linesBuilder.append("<con:LineNumber>");
                    linesBuilder.append(count);
                    linesBuilder.append("</con:LineNumber>\n");
                }
                linesBuilder.append("<con:ItemNameTxt> line ");
                linesBuilder.append(count);
                linesBuilder.append("</con:ItemNameTxt><con:ItemDescription>");
                linesBuilder.append(Linesbean.getDescription().replaceAll("<","&lt;").toString().replaceAll(">", "&gt;").toString());
                linesBuilder.append("</con:ItemDescription><con:PurchasingCategory>");
                linesBuilder.append(Linesbean.getCategory().replaceAll("&","&amp;").toString());
                linesBuilder.append("</con:PurchasingCategory><con:FixedPriceServiceYn>Y</con:FixedPriceServiceYn>\n");
                linesBuilder.append("<con:LineValueAmount currencyCode=\"");
                linesBuilder.append(Linesbean.getOrderedCurrency());
                linesBuilder.append("\">");
                linesBuilder.append(Linesbean.getOrdered());
                linesBuilder.append("</con:LineValueAmount><con:LineAmount currencyCode=\"");
                linesBuilder.append(Linesbean.getOrderedCurrency());
                linesBuilder.append("\">");
                linesBuilder.append(Linesbean.getOrdered());
                linesBuilder.append("</con:LineAmount>");
                linesBuilder.append("<con:RecInvOrgName>");
                linesBuilder.append(Linesbean.getShipToOrganizationName());
                linesBuilder.append("</con:RecInvOrgName>");
                linesBuilder.append("<con:PaymentTerms>");
                linesBuilder.append(bean.getPaymentTerms());
                linesBuilder.append("</con:PaymentTerms><con:SupplierSiteId>");
                linesBuilder.append(bean.getSupplierSite());
                linesBuilder.append("</con:SupplierSiteId><con:ShipToOrgLocation>");
                linesBuilder.append(Linesbean.getShipToLocation());
                linesBuilder.append("</con:ShipToOrgLocation><con:DeliveryDate>");
                linesBuilder.append(deliveryDate);
                linesBuilder.append("</con:DeliveryDate><con:EndDate>");
                linesBuilder.append(deliveryDate);
                linesBuilder.append("</con:EndDate>\n");
                linesBuilder.append("</con:ContractLine>\n");
                count++;
            } else if(Double.parseDouble(Linesbean.getQuantity()) >0) {
                linesBuilder.append("<con:ContractLine>\n");
                if (bean.isLineNumberRequired() == true) {
                    linesBuilder.append("<con:LineNumber>");
                    linesBuilder.append(count);
                    linesBuilder.append("</con:LineNumber>\n");
                }
                linesBuilder.append("<con:ItemNameTxt> line ");
                linesBuilder.append(count);
                linesBuilder.append("</con:ItemNameTxt><con:ItemDescription>");
                linesBuilder.append(Linesbean.getDescription().replaceAll("<","&lt;").toString().replaceAll(">", "&gt;").toString());
                linesBuilder.append("</con:ItemDescription><con:PurchasingCategory>");
                linesBuilder.append(Linesbean.getCategory().replaceAll("&","&amp;").toString());
                linesBuilder.append("</con:PurchasingCategory><con:UOMCode>");
                linesBuilder.append(Linesbean.getUOM());
                linesBuilder.append("</con:UOMCode><con:FixedPriceServiceYn>N</con:FixedPriceServiceYn>\n");
                linesBuilder.append("<con:ItemQuantity unitCode=\"");
                linesBuilder.append(Linesbean.getUOM());
                linesBuilder.append("\">");
                linesBuilder.append(Linesbean.getQuantity());
                linesBuilder.append("</con:ItemQuantity><con:UnitPrice currencyCode=\"");
                linesBuilder.append(Linesbean.getPriceCurrency());
                linesBuilder.append("\">");
                linesBuilder.append(Linesbean.getPrice());
                linesBuilder.append("</con:UnitPrice>");
                linesBuilder.append("<con:RecInvOrgName>");
                linesBuilder.append(Linesbean.getShipToOrganizationName());
                linesBuilder.append("</con:RecInvOrgName>");
                linesBuilder.append("<con:PaymentTerms>");
                linesBuilder.append(bean.getPaymentTerms());
                linesBuilder.append("</con:PaymentTerms><con:SupplierSiteId>");
                linesBuilder.append(bean.getSupplierSite());
                linesBuilder.append("</con:SupplierSiteId><con:ShipToOrgLocation>");
                linesBuilder.append(Linesbean.getShipToLocation());
                linesBuilder.append("</con:ShipToOrgLocation><con:DeliveryDate>");
                linesBuilder.append(deliveryDate);
                linesBuilder.append("</con:DeliveryDate><con:EndDate>");
                linesBuilder.append(deliveryDate);
                linesBuilder.append("</con:EndDate>\n");
                linesBuilder.append("</con:ContractLine>\n");
                count++;
            }
        }
        headerBuilder.append(linesBuilder);
        headerBuilder.append("</typ:contractHeader></typ:createContract></soapenv:Body></soapenv:Envelope>");

        System.out.println("builder" + headerBuilder.toString());
        desUrl = getInstanceUrl() + getContractUrl();
        System.out.println("desUrl  " + desUrl);
        Document doc = httpPostNew(desUrl, headerBuilder.toString());
        JSONObject response = new JSONObject();
        if (doc.getElementsByTagName("ns1:ContractNumber").getLength() > 0) {
            response.put("status", "success");
            response.put("message", "Contract created sucessfully with number " +doc.getElementsByTagName("ns1:ContractNumber").item(0).getTextContent());
        } else {
            Element element =(Element)doc.getElementsByTagName("tns:ServiceErrorMessage").item(0);
            System.out.println(element.getElementsByTagName("tns:message").getLength());
            try {
                if (element.getElementsByTagName("tns:message").getLength() >
                    0) {
                    String message =element.getElementsByTagName("tns:message").item(element.getElementsByTagName("tns:message").getLength()-1).getTextContent();

                    if (message.contains("LineNumber") &&
                        message.contains("required")) {
                        System.out.println("Line Number Required");
                        bean.setLineNumberRequired(true);
                        JSONObject objectContract =new CreateSoap().CreateContract(bean,getInstanceName() +getContractUrl());
                        response = objectContract;
                    } else {
                        if (message.contains("LOV_ContractTypeName_WS") &&
                            message.contains("no matching")) {
                            message ="Contract type name is wrong please choose anthor one";
                        } else if (message.contains("OKC_KPROJ_NEED_START_ERR") && message.contains("no matching")) {
                            message ="DeliveryDate is less than the contract start date (" +new SimpleDateFormat("dd-MM-yyyy").format(new Date())+")";
                        } else if (message.contains("LOV_ShipToOrgLocation") &&message.contains("no matching")) {
                            message ="Ship to location is not vaild please check it first";
                        } else {
                            message = message.replaceAll("</?[A-Za-z]+>", "");
                            message = message.replaceFirst("JBO-OKC:::OKC_AUTH_CLN_INT_VAL","");
                            message =message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_END_ERR","");
                            message = message.replaceFirst(": OKC-", "");
                            message =message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_START_ERR195449","");
                            message =message.replaceFirst("The need-by date", "Promised Delivery Date of Po Lines  ");
                        }
                        response.put("status", "error");
                        response.put("message", message);
                    }

                } else {
                    Element element2 =
                        (Element)doc.getElementsByTagName("tns:detail").item(0);
                    String message =
                        element2.getElementsByTagName("tns:message").item(0).getTextContent();
                    message = message.replaceAll("</?[A-Za-z]+>", "");
                    message =message.replaceFirst("JBO-OKC:::OKC_AUTH_CLN_INT_VAL","");
                    message =message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_END_ERR","");
                    message = message.replaceFirst(": OKC-", "");
                    message =message.replaceAll("JBO-OKC:::OKC_KPROJ_NEED_START_ERR195449","");
                    message =message.replaceFirst("The need-by date", "Promised Delivery Date of Po Lines  ");
                    response.put("status", "error");
                    response.put("message", message);
                }
            } catch (NullPointerException e) {
                response.put("status", "error");
                response.put("message", e.getMessage());
            }

        }
        
    return response;
}
    
    public JSONObject CloseContract(String data){
        JSONObject objResponse=new JSONObject();
        JSONObject objData=new JSONObject(data);
        StringBuilder soapBuilder=new StringBuilder();
        Document doc=null;
        
        try{
        soapBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\">\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:closeContract>\n" + 
        "         <typ:contractId>");
        soapBuilder.append(objData.get("id").toString());
        soapBuilder.append("</typ:contractId>\n" + 
        "         <typ:headerExternalSourceKey>");
        soapBuilder.append(objData.get("id").toString());
        soapBuilder.append("</typ:headerExternalSourceKey>\n" + 
        "         <typ:closedDate>");
        soapBuilder.append(objData.get("closeDate").toString());
        soapBuilder.append("</typ:closedDate>\n" + 
        "         <typ:closeReasonCode>");
        soapBuilder.append(objData.get("closeReason").toString());
        soapBuilder.append("</typ:closeReasonCode>\n" + 
        "      </typ:closeContract>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>");
        }catch(Exception e){
            objResponse.put("status", "error");
            objResponse.put("message", e.getMessage());
        }
        
        try{
            String desUrl=getInstanceUrl()+getContractUrl();
            doc=httpPostNew(desUrl,soapBuilder.toString());
            if(doc.getElementsByTagName("ns2:result").getLength()>0){
                 objResponse.put("status", "success");
                 objResponse.put("message","Contract with number "+objData.get("number").toString()+" have been closed successfully");
            }else{
                Element element2=(Element)doc.getElementsByTagName("detail").item(0);   
                String message=element2.getElementsByTagName("tns:message").item(0).getTextContent(); 
                message=message.replaceAll("</?[A-Za-z]+>", "");
                message=message.replaceFirst("JBO-OKC:::OKC_AUTH_CLN_INT_VAL", "");
                message=message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_END_ERR", "");
                message=message.replaceFirst(": OKC-", "");
                objResponse.put("status", "error");
                objResponse.put("message",message);
            }
        }catch(NullPointerException e){
            objResponse.put("status", "error");
            objResponse.put("message",e.getMessage());   
        }
        
     return objResponse;   
    }
    
    public JSONObject CancelContract(String data){
        JSONObject objResponse=new JSONObject();
        JSONObject objData=new JSONObject(data);
        StringBuilder soapBuilder=new StringBuilder();
        Document doc=null;
        try{
        soapBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/contracts/coreAuthoring/contractService/types/\">\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:cancelContract>\n" + 
        "         <typ:contractId>");
        soapBuilder.append(objData.get("id").toString());
        soapBuilder.append("</typ:contractId>\n" + 
        "         <typ:reasonCode>");
        soapBuilder.append(objData.get("closeReason").toString());
        soapBuilder.append("</typ:reasonCode>\n" + 
        "         <typ:externalSourceKey>");
        soapBuilder.append(objData.get("id").toString());
        soapBuilder.append("</typ:externalSourceKey>\n" + 
        "      </typ:cancelContract>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>");
        }catch(Exception e){
            objResponse.put("status", "error");
            objResponse.put("message", e.getMessage());
        }
        
        try{
            String desUrl=getInstanceUrl()+getContractUrl();
            doc=httpPostNew(desUrl,soapBuilder.toString());
            if(doc.getElementsByTagName("ns2:result").getLength()>0){
                 objResponse.put("status", "success");
                 objResponse.put("message","Contract with number "+objData.get("number").toString()+" have been canceled successfully");
            }else{
                Element element2=(Element)doc.getElementsByTagName("detail").item(0);   
                String message=element2.getElementsByTagName("tns:message").item(0).getTextContent(); 
                message=message.replaceAll("</?[A-Za-z]+>", "");
                message=message.replaceFirst("JBO-OKC:::OKC_AUTH_CLN_INT_VAL", "");
                message=message.replaceFirst("JBO-OKC:::OKC_KPROJ_NEED_END_ERR", "");
                message=message.replaceFirst(": OKC-", "");
                objResponse.put("status", "error");
                objResponse.put("message",message);
            }
        }catch(NullPointerException e){
            objResponse.put("status", "error");
            objResponse.put("message",e.getMessage());   
        } 
        return objResponse;
    }

}
