package common.restHelper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.cert.CertificateException;


import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;


public class RestHelper {

    public final String protocol = "https";
    private final String purchaseOrdersURL =
        "/fscmRestApi/resources/11.13.18.05/purchaseOrders";
    private final String supplierURL =
        "/fscmRestApi/resources/11.13.18.05/suppliers/";
    private final String contractUrl = "/fscmService/ContractServiceV2";
    private final String purchaseOrderUrl =
        "/fscmService/PurchaseOrderServiceV2";
    private final String contractTypesURL =
        "/fscmRestApi/resources/11.13.18.05/valueSets/Contract%20Type/child/values";
    private final String contractsRestURL =
        "/fscmRestApi/resources/11.13.18.05/contracts";
    private final String biReportUrl =
        "/xmlpserver/services/PublicReportService";
    private final String SecurityService =
        "/xmlpserver/services/v2/SecurityService";

    public String getPurchaseOrdersURL() {
        return purchaseOrdersURL;
    }

    public String getSupplierURL() {
        return supplierURL;
    }

    public String getContractUrl() {
        return contractUrl;
    }

    public String getPurchaseOrderUrl() {
        return purchaseOrderUrl;
    }

    public String getContractTypesURL() {
        return contractTypesURL;
    }

    public String getContractsRestURL() {
        return contractsRestURL;
    }
    
    public String getProtocol() {
        return protocol;
    }

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getSecurityService() {
        return SecurityService;
    }

    public String getInstanceUrl() {
        String instanceUrl = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            instanceUrl = CommonConfigReader.getValue("TestInstanceUrl");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            instanceUrl = CommonConfigReader.getValue("ProdInstanceUrl");
        }
        return instanceUrl;
    }

    public String getInstanceName() {
        String instanceName = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            instanceName = CommonConfigReader.getValue("TestInstanceName");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            instanceName = CommonConfigReader.getValue("ProdInstanceName");
        }
        return instanceName;
    }

    public String getUserName() {
        String userName = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            userName = CommonConfigReader.getValue("TEST_USER_NAME");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            userName = CommonConfigReader.getValue("PROD_USER_NAME");
        }
        return userName;
    }

    public String getPassword() {
        String password = "";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            password = CommonConfigReader.getValue("TEST_PASSWORD");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
            password = CommonConfigReader.getValue("PROD_PASSWORD");
        }
        return password;
    }


    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    private static RestHelper instance = null;

    public static RestHelper getInstance() {
        if (instance == null)
            instance = new RestHelper();
        return instance;
    }


    public String getAuth() {
        byte[] message = (getUserName() + ":" + getPassword()).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String httpGet(String hostUrl) {

        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        StringBuffer response = new StringBuffer();

        try {
            URL url =
                new URL(null, hostUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }

            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());

            // print status and response
            System.out.println("connection status: " +
                               connection.getResponseCode() +
                               "; connection response: " +
                               connection.getResponseMessage());

            if ("Not Found".equalsIgnoreCase(connection.getResponseMessage()) ||
                "Internal Server Error".equalsIgnoreCase(connection.getResponseMessage())) {
                response = new StringBuffer("error in getting data");
            } else {
                try (BufferedReader in =
                     new BufferedReader(new InputStreamReader(connection.getInputStream(),
                                                              "UTF-8"))) {
                    String inputLine;
                    response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                }
            }
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    public Document httpPost(String destUrl, String data) {

        System.out.println("************* soap data *************");
        System.out.println(data);

        java.net.URL url;

        try {
            url =
new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
            System.out.println("RestHelperdesUrl  " + destUrl);
            java.net.HttpURLConnection http;

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";
            http.setRequestProperty("Content-Type", "text/xml");
            http.setRequestProperty("Accept", "text/xml");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            try (OutputStream os = http.getOutputStream()) {
                byte[] input = data.toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            ;
            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());

            InputStream in = null;
            if (http.getResponseCode() == 200 ||
                http.getResponseCode() == 201) {
                in = http.getInputStream();
            } else {
                System.out.println("error" + http.getResponseMessage());
                in = http.getErrorStream();
            }
            if (in != null) {
                InputStreamReader iReader = new InputStreamReader(in, "UTF-8");
                BufferedReader bReader = new BufferedReader(iReader);

                String line;
                String response = "";
                System.out.println("==================Service response: ================ ");
                while ((line = bReader.readLine()) != null) {
                    response += line;
                }
                if (response.indexOf("<?xml") > 0)
                    response =
                            response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                               15);
                DocumentBuilderFactory fact =
                    DocumentBuilderFactory.newInstance();
                fact.setNamespaceAware(true);
                DocumentBuilder builder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource src = new InputSource();
                src.setCharacterStream(new StringReader(response));
                Document doc =
                    builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));

                XML.toJSONObject(response).toString();

                iReader.close();
                bReader.close();
                in.close();
                http.disconnect();
                return doc;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Document httpPostNew(String destUrl, String data) {

        System.out.println("************* soap data *************");
        System.out.println(data);

        java.net.URL url;

        try {
            url =
new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
            System.out.println("RestHelperdesUrl  " + destUrl);
            java.net.HttpURLConnection http;

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }

            http.setRequestProperty("Content-Type", "text/xml");
            http.setRequestProperty("Accept", "text/xml");
            http.setRequestProperty("Authorization", "Basic " + getAuth());
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            try (OutputStream os = http.getOutputStream()) {
                byte[] input = data.toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            ;
            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());

            InputStream in = null;
            if (http.getResponseCode() == 200 ||
                http.getResponseCode() == 201) {
                in = http.getInputStream();
            } else {
                System.out.println("error" + http.getResponseMessage());
                in = http.getErrorStream();
            }
            if (in != null) {
                InputStreamReader iReader = new InputStreamReader(in);
                BufferedReader bReader = new BufferedReader(iReader);

                String line;
                String response = "";
                System.out.println("==================Service response: ================ ");
                while ((line = bReader.readLine()) != null) {
                    response += line;
                }
                if (response.indexOf("<?xml") > 0)
                    response =
                            response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                               15);
                DocumentBuilderFactory fact =
                    DocumentBuilderFactory.newInstance();
                fact.setNamespaceAware(true);
                DocumentBuilder builder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource src = new InputSource();
                src.setCharacterStream(new StringReader(response));
                Document doc =
                    builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));

                XML.toJSONObject(response).toString();

                iReader.close();
                bReader.close();
                in.close();
                http.disconnect();
                return doc;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();

    }

   
}
