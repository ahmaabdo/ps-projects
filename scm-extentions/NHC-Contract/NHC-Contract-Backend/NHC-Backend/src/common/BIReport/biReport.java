package common.BIReport;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;


public class biReport extends ReportModel {


    public JSONObject getReportCall(String reportName,
                                Map<String, String> reportParams) {
        try {
            if (reportName != null) {
                JSONObject xmlJSONObj = runReport(reportName, reportParams);
                JSONObject jsonString =
                    xmlJSONObj.getJSONObject("DATA_DS");
                return jsonString;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
