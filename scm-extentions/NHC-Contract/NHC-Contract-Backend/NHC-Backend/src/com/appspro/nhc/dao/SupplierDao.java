package com.appspro.nhc.dao;

import com.appspro.nhc.bean.SupplierBean;
import common.restHelper.RestHelper;
import org.json.JSONObject;

public class SupplierDao extends RestHelper {


    public String getSupplierDetails(String supplierId) {

        String serverUrl = getInstanceUrl() + getSupplierURL() + supplierId;
        SupplierBean supplierBean=new SupplierBean();
        String resp = httpGet(serverUrl);
        if (resp != null) {
            
            JSONObject  returnedData = new JSONObject(resp.toString());
            supplierBean.setSupplierId(returnedData.get("SupplierId").toString()=="null"?"":returnedData.get("SupplierId").toString());
            supplierBean.setAlternateName(returnedData.get("AlternateName").toString()=="null"?"":returnedData.get("AlternateName").toString());
            supplierBean.setSupplierSite(returnedData.get("SupplierPartyId").toString()=="null"?"":returnedData.get("SupplierPartyId").toString());
            supplierBean.setSupplierName(returnedData.get("Supplier").toString()=="null"?"":returnedData.get("Supplier").toString());
            supplierBean.setTaxRegistrationNumber(returnedData.get("TaxRegistrationNumber").toString()=="null"?"":returnedData.get("TaxRegistrationNumber").toString());
            
        }
        return new JSONObject(supplierBean).toString();
    }
}
