package com.appspro.nhc.rest;

import com.appspro.nhc.dao.PurchaseOrdersDao;

import common.Soap.PurchaseOrderSoap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("purchaseOrder")
public class PurchaseOrdersRest extends PurchaseOrderSoap{
    PurchaseOrdersDao purchaseOrderDao = new PurchaseOrdersDao();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPurchaseOrders() {
        return purchaseOrderDao.getPurchaseOrders();
    }
    
    @Path("/getPurchaseOrderDetails/{poHeaderId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getPurchaseOrderLines(@PathParam("poHeaderId") String poHeaderId) throws Exception {
        // check for supplier id
        if("".equalsIgnoreCase(poHeaderId)||"null".equalsIgnoreCase(poHeaderId)||poHeaderId==null){
            return "Po Header id not found";
        }else{
            String objectData =
                getPurchaseOrderByHeaderId(poHeaderId);
            return objectData;   
        } 
    }
}
