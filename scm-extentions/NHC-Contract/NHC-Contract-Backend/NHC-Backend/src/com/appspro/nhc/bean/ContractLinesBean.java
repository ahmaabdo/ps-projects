package com.appspro.nhc.bean;

public class ContractLinesBean {
    private String LineNumber;
    private String Description;
    private String Category;
    private String UOM;
    private String Quantity;
    private String Price;
    private String priceCurrency;
    private String Ordered;
    private String OrderedCurrency;
    private String shipToOrganizationName ;
    private String shipToLocation;
    private String deliveryDate;
    private String lineId;
    private String totalAmount;
    private String totalTaxAmount;
    private String totalTaxCurrency;
    private boolean delete;
    

    public void setLineNumber(String LineNumber) {
        this.LineNumber = LineNumber;
    }

    public String getLineNumber() {
        return LineNumber;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getDescription() {
        return Description;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public String getCategory() {
        return Category;
    }

    public void setUOM(String UOM) {
        this.UOM = UOM;
    }

    public String getUOM() {
        return UOM;
    }

    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getPrice() {
        return Price;
    }

    public void setOrdered(String Ordered) {
        this.Ordered = Ordered;
    }

    public String getOrdered() {
        return Ordered;
    }
    public void setShipToOrganizationName(String shipToOrganizationName) {
        this.shipToOrganizationName = shipToOrganizationName;
    }

    public String getShipToOrganizationName() {
        return shipToOrganizationName;
    }

    public void setShipToLocation(String shipToLocation) {
        this.shipToLocation = shipToLocation;
    }

    public String getShipToLocation() {
        return shipToLocation;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
    }

    public String getPriceCurrency() {
        return priceCurrency;
    }

    public void setOrderedCurrency(String OrderedCurrency) {
        this.OrderedCurrency = OrderedCurrency;
    }

    public String getOrderedCurrency() {
        return OrderedCurrency;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getLineId() {
        return lineId;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalTaxAmount(String totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public String getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxCurrency(String totalTaxCurrency) {
        this.totalTaxCurrency = totalTaxCurrency;
    }

    public String getTotalTaxCurrency() {
        return totalTaxCurrency;
    }
}
