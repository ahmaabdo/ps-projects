package com.appspro.nhc.bean;

public class SupplierBean {
    
    private String SupplierId;
    private String SupplierName;
    private String SupplierSite;
    private String AlternateName;
    private String TaxRegistrationNumber;

    public void setSupplierId(String SupplierId) {
        this.SupplierId = SupplierId;
    }

    public String getSupplierId() {
        return SupplierId;
    }

    public void setSupplierName(String SupplierName) {
        this.SupplierName = SupplierName;
    }

    public String getSupplierName() {
        return SupplierName;
    }

    public void setSupplierSite(String SupplierSite) {
        this.SupplierSite = SupplierSite;
    }

    public String getSupplierSite() {
        return SupplierSite;
    }

    public void setAlternateName(String AlternateName) {
        this.AlternateName = AlternateName;
    }

    public String getAlternateName() {
        return AlternateName;
    }

    public void setTaxRegistrationNumber(String TaxRegistrationNumber) {
        this.TaxRegistrationNumber = TaxRegistrationNumber;
    }

    public String getTaxRegistrationNumber() {
        return TaxRegistrationNumber;
    }
}
