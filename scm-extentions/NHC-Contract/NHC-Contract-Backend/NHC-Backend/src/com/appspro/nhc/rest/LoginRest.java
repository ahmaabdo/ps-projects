package com.appspro.nhc.rest;

import com.appspro.nhc.dao.LoginDao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.bind.DatatypeConverter;


@Path("/login")
public class LoginRest {
    
    
    @POST
    @Consumes("application/json")
    @Path("/validateLogin")
    public Response getEmpDetails(String body, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {
        String jsonInString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(body);
            String userName = actualObj.get("userName").asText();
            String password = actualObj.get("password").asText();
            boolean mainData = actualObj.get("mainData").asBoolean();
            LoginDao login = new LoginDao();
            boolean result = login.validateLogin(userName, password);
            jsonInString = "{\"result\":" + result + "}";
            if (result) {
                String encoding =
                    DatatypeConverter.printBase64Binary((userName + ":" +
                                                         password).getBytes("UTF-8"));

                HttpSession session = request.getSession();
                session.setAttribute("authorization", "Basic " + encoding);
            }
            return Response.ok(jsonInString,
                               MediaType.APPLICATION_JSON).build();
        } catch (JsonProcessingException e) {
           e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        } catch (IOException e) {
           e.printStackTrace();
        }
        return Response.ok(jsonInString, MediaType.APPLICATION_JSON).build();

    }
    public LoginRest() {
        super();
    }
}
