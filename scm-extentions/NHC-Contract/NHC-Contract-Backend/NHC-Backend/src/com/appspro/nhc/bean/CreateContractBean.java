package com.appspro.nhc.bean;

import java.util.ArrayList;


public class CreateContractBean {
    private String ContractNumber;
    private String LegalEntity;
    private String CurrencyCode;
    private String OrderNumber;
    private String PaymentTermsId;
    private String PaymentTerms;
    private String FreightTerms;
    private String FOB; 
    private String SupplierId;
    private String SupplierSite;
    private String Supplier;
    private String amount;
    private String amountCurrency;
    private String orgId;
    private String contractType;
    private String contractTypeId;
    private String relatedPoNumber;
    private String poVersionNumber;
    private boolean lineNumberRequired;
    private String contractId;
    private String id;
    private String majorVersion;
    private String status;
    private String startDate;
    private String bayOrSell;
    private String updateDate;
    private String endDate;
    private String vat;
    private String totalWithVat;
    

    private ArrayList<ContractLinesBean> lines;

    public void setLegalEntity(String LegalEntity) {
        this.LegalEntity = LegalEntity;
    }

    public String getLegalEntity() {
        return LegalEntity;
    }
    public void setOrderNumber(String OrderNumber) {
        this.OrderNumber = OrderNumber;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }
    public void setPaymentTerms(String PaymentTerms) {
        this.PaymentTerms = PaymentTerms;
    }

    public String getPaymentTerms() {
        return PaymentTerms;
    }

    public void setFreightTerms(String FreightTerms) {
        this.FreightTerms = FreightTerms;
    }

    public String getFreightTerms() {
        return FreightTerms;
    }

    public void setFOB(String FOB) {
        this.FOB = FOB;
    }

    public String getFOB() {
        return FOB;
    }
    public void setSupplierId(String SupplierId) {
        this.SupplierId = SupplierId;
    }

    public String getSupplierId() {
        return SupplierId;
    }

    public void setSupplier(String Supplier) {
        this.Supplier = Supplier;
    }

    public String getSupplier() {
        return Supplier;
    }

    public void setSupplierSite(String SupplierSite) {
        this.SupplierSite = SupplierSite;
    }

    public String getSupplierSite() {
        return SupplierSite;
    }
    public void setContractNumber(String ContractNumber) {
        this.ContractNumber = ContractNumber;
    }

    public String getContractNumber() {
        return ContractNumber;
    }

    public void setLines(ArrayList<ContractLinesBean> lines) {
        this.lines = lines;
    }

    public ArrayList<ContractLinesBean> getLines() {
        return lines;
    }

    public void setCurrencyCode(String CurrencyCode) {
        this.CurrencyCode = CurrencyCode;
    }

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setPaymentTermsId(String PaymentTermsId) {
        this.PaymentTermsId = PaymentTermsId;
    }

    public String getPaymentTermsId() {
        return PaymentTermsId;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractType() {
        return contractType;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setAmountCurrency(String amountCurrency) {
        this.amountCurrency = amountCurrency;
    }

    public String getAmountCurrency() {
        return amountCurrency;
    }

    public void setContractTypeId(String contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getContractTypeId() {
        return contractTypeId;
    }

    public void setRelatedPoNumber(String relatedPoNumber) {
        this.relatedPoNumber = relatedPoNumber;
    }

    public String getRelatedPoNumber() {
        return relatedPoNumber;
    }

    public void setPoVersionNumber(String poVersionNumber) {
        this.poVersionNumber = poVersionNumber;
    }

    public String getPoVersionNumber() {
        return poVersionNumber;
    }

    public void setLineNumberRequired(boolean lineNumberRequired) {
        this.lineNumberRequired = lineNumberRequired;
    }

    public boolean isLineNumberRequired() {
        return lineNumberRequired;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setMajorVersion(String majorVersion) {
        this.majorVersion = majorVersion;
    }

    public String getMajorVersion() {
        return majorVersion;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setBayOrSell(String bayOrSell) {
        this.bayOrSell = bayOrSell;
    }

    public String getBayOrSell() {
        return bayOrSell;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getVat() {
        return vat;
    }

    public void setTotalWithVat(String totalWithVat) {
        this.totalWithVat = totalWithVat;
    }

    public String getTotalWithVat() {
        return totalWithVat;
    }
}
