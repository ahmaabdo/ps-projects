package com.appspro.nhc.rest;


import common.BIReport.biReport;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;


@Path("/report")
public class ReportService {
    biReport biReport = new biReport();

    @POST
    @Path("/commonbireport")
    @Produces(MediaType.APPLICATION_JSON)
    public String getBiReport(String incomingData) {

        String reportName = null;
        Map<String, String> reportParams = new HashMap<String, String>();

        if (incomingData != null && !incomingData.isEmpty()) {
            // put incoming string into json object
            JSONObject reportData = new JSONObject(incomingData);
            reportName = reportData.get("reportName").toString();
            JSONObject questionMark = reportData.getJSONObject("params");
            Iterator keys = questionMark.keys();
            while (keys.hasNext()) {
                // loop to get the dynamic key
                String currentDynamicKey = (String)keys.next();
                // get the value of the dynamic key
                String currentDynamicValue =
                    questionMark.get(currentDynamicKey).toString();
                // do something here with the value...
                reportParams.put(currentDynamicKey, currentDynamicValue);
            }
            System.out.println("data params:- " + reportParams);

            JSONObject returnData = biReport.getReportCall(reportName, reportParams);

            return returnData.toString();
        } else {
            return "error in data check again";
        }
    }
}
