package com.appspro.nhc.bean;

public class PurchaseOrderLineBean {
    
    private String lineNumber;
    private String description;
    private String descriptionCode;
    private String categoryId;
    private String categoryName;
    private String orderedAmount;              
    private String UOM;
    private String quantity;
    private String price;
    private String priceCurrency;
    private String ordered;
    private String orderedCurrency;
    private String totalTaxAmount;
    private String totalAmount;
    private String lineStatus;
    private String location;
    private String organization;
    private String requesterName;
    private String fundsStatus;
    private String shipToOrganizationName;
    private String requestedDeliveryDate;
    private String totalTaxCurrency;
    private String uomTitle;
    


    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setOrderedAmount(String orderedAmount) {
        this.orderedAmount = orderedAmount;
    }

    public String getOrderedAmount() {
        return orderedAmount;
    }

    public void setUOM(String UOM) {
        this.UOM = UOM;
    }

    public String getUOM() {
        return UOM;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setOrdered(String ordered) {
        this.ordered = ordered;
    }

    public String getOrdered() {
        return ordered;
    }

    public void setTotalTaxAmount(String totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public String getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setLineStatus(String lineStatus) {
        this.lineStatus = lineStatus;
    }

    public String getLineStatus() {
        return lineStatus;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getOrganization() {
        return organization;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public void setFundsStatus(String fundsStatus) {
        this.fundsStatus = fundsStatus;
    }

    public String getFundsStatus() {
        return fundsStatus;
    }

    public void setShipToOrganizationName(String shipToOrganizationName) {
        this.shipToOrganizationName = shipToOrganizationName;
    }

    public String getShipToOrganizationName() {
        return shipToOrganizationName;
    }

    public void setDescriptionCode(String descriptionCode) {
        this.descriptionCode = descriptionCode;
    }

    public String getDescriptionCode() {
        return descriptionCode;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
    }

    public String getPriceCurrency() {
        return priceCurrency;
    }

    public void setOrderedCurrency(String orderedCurrency) {
        this.orderedCurrency = orderedCurrency;
    }

    public String getOrderedCurrency() {
        return orderedCurrency;
    }

    public void setTotalTaxCurrency(String totalTaxCurrency) {
        this.totalTaxCurrency = totalTaxCurrency;
    }

    public String getTotalTaxCurrency() {
        return totalTaxCurrency;
    }

    public void setUomTitle(String uomTitle) {
        this.uomTitle = uomTitle;
    }

    public String getUomTitle() {
        return uomTitle;
    }
}
