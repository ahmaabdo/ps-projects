package com.appspro.nhc.rest;

import com.appspro.nhc.dao.PurchaseOrdersDao;

import com.appspro.nhc.dao.SupplierDao;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("supplier")
public class SupplierRest {
    SupplierDao supplierDao=new SupplierDao();

    @Path("/getSupplier/{supplierId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPurchaseOrders(@PathParam("supplierId") String supplierId) {
        // check for supplier id
        if("".equalsIgnoreCase(supplierId)||"null".equalsIgnoreCase(supplierId)||supplierId==null){
            return "supplier id not found";
        }else{
            return supplierDao.getSupplierDetails(supplierId);   
        }
    }
}
