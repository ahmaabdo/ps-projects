package com.appspro.nhc.rest;

import com.appspro.nhc.bean.CreateContractBean;

import com.appspro.nhc.bean.ContractLinesBean;

import common.Soap.CreateSoap;

import java.util.ArrayList;

import javax.json.Json;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("Contract")

public class CreateContractRest extends CreateSoap {


    @POST
    @Path("/createContract")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createContract(String incomingData ) throws Exception {
        ArrayList<ContractLinesBean> Lineslist =
            new ArrayList<ContractLinesBean>();
        JSONObject objectData = new JSONObject(incomingData);
        System.out.println(objectData);
        CreateContractBean bean = new CreateContractBean();
        bean.setLegalEntity(objectData.get("legalEntity").toString());
        bean.setCurrencyCode(objectData.get("currencyCode").toString());
        bean.setOrderNumber(objectData.get("orderNumber").toString());
        bean.setPaymentTermsId(objectData.get("paymentTermsId").toString());
        bean.setPaymentTerms(objectData.get("paymentTerms").toString());
        bean.setFreightTerms(objectData.get("freightTerms").toString());
        bean.setFOB(objectData.get("FOB").toString());
        bean.setSupplierId(objectData.get("supplierId").toString());
        bean.setSupplierSite(objectData.get("supplierSite").toString());
        bean.setSupplier(objectData.get("supplier").toString());
//        bean.setContractType(objectData.get("contractType").toString());
        bean.setContractTypeId(objectData.get("contractType").toString());
        bean.setAmount(objectData.get("amount").toString());
        bean.setAmountCurrency(objectData.get("amountCurrency").toString());
        bean.setOrgId(objectData.get("orgId").toString());
        bean.setRelatedPoNumber(objectData.has("relatedPoNumber") ? objectData.get("relatedPoNumber").toString():"no");
        bean.setEndDate(objectData.get("endDate").toString());
        bean.setVat(objectData.get("vat").toString().replace("SAR", "").trim());
        bean.setTotalWithVat(objectData.get("totalWithVat").toString());
        JSONArray linesArray=new JSONArray(objectData.get("linesArray").toString());
        for (int i = 0; i < linesArray.length(); i++) {

            JSONObject lineData = linesArray.getJSONObject(i);
            ContractLinesBean Linesbean = new ContractLinesBean();
            Linesbean.setLineNumber(lineData.get("lineNumber").toString());
            Linesbean.setCategory(lineData.get("category").toString());
            Linesbean.setDescription(lineData.get("description").toString().replaceAll("&", "&amp;"));
            Linesbean.setQuantity(lineData.has("quantity")?lineData.get("quantity").toString():null);
            Linesbean.setUOM(lineData.has("UOM")?lineData.get("UOM").toString():"");
            Linesbean.setPrice(lineData.has("price")?lineData.get("price").toString():"");
            Linesbean.setPriceCurrency(lineData.has("priceCurrency")?lineData.get("priceCurrency").toString():"");
            Linesbean.setOrdered(lineData.get("ordered").toString());
            Linesbean.setOrderedCurrency(lineData.get("orderedCurrency").toString());
            Linesbean.setShipToOrganizationName(lineData.get("shipToOrg").toString());
            Linesbean.setShipToLocation(objectData.get("shipToLoc").toString());
            Linesbean.setDeliveryDate(lineData.get("deliveryDate").toString());
            Linesbean.setTotalAmount(lineData.get("totalAmount").toString());
            Linesbean.setTotalTaxAmount(lineData.get("totalTaxAmount").toString());
            Linesbean.setTotalTaxCurrency(lineData.get("totalTaxCurrency").toString());
            Lineslist.add(Linesbean);
        }
        bean.setLines(Lineslist);
        System.out.println("data sent    "+new JSONObject(bean).toString());
        JSONObject objectContract =
            CreateContract(bean, getInstanceName() + getContractUrl());

        return Response.ok(objectContract.toString() ,
                           MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/closeContract")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response closeContract(String incomingData ) throws Exception {
        
        JSONObject objectContract =CloseContract(incomingData);

        return Response.ok(objectContract.toString() ,
                           MediaType.APPLICATION_JSON).build(); 
    }
    
    @POST
    @Path("/cancelContract")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelContract(String incomingData ) throws Exception {
        
        JSONObject objectContract =CancelContract(incomingData);

        return Response.ok(objectContract.toString() ,
                           MediaType.APPLICATION_JSON).build();
    }

}
