package com.appspro.nhc.rest;

import com.appspro.nhc.dao.AddLinesToExistingContract;
import com.appspro.nhc.dao.updateContractDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;

@Path("updateContract")
public class updateContract {
    
    @POST
    @Path("/updateExistingContract")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateContract(String incomingData ) throws Exception {
        updateContractDAO updateContractDao=new updateContractDAO();
        JSONObject objectContract =updateContractDao.updateContract(incomingData);
        return Response.ok(objectContract.toString() ,
                           MediaType.APPLICATION_JSON).build(); 
    }
    
    @POST
    @Path("/addLinesToContract")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addLinesToContract(String incomingData) throws Exception {
        AddLinesToExistingContract addLinesDao=new AddLinesToExistingContract();
        JSONObject objectContract =addLinesDao.fillContractBean(incomingData);
        return Response.ok(objectContract.toString(),MediaType.APPLICATION_JSON).build(); 
    }
}
