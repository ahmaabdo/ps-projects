package com.appspro.nhc.bean;

import java.util.ArrayList;

import org.json.JSONObject;

public class PurchaseOrderBean {

    private String POHeaderId;
    private String OrderNumber;
    private String SoldToLegalEntity;
    private String CreationDate;
    private String Status;
    private String ProcurementBU;
    private String Buyer;
    private String SupplierId;
    private String Supplier;
    private String SupplierSite;
    private String SupplierContact;
    private String BillToLocation;
    private String CurrencyCode;
    private String Currency;
    private String PaymentTerms;
    private String FOB;
    private String NoteToSupplier;
    private String NoteToReceiver;
    private String ProcurementBUId;
    private String SoldToLegalEntityId;
    private String PaymentTermsId;
    private String contractValue;
    private String ordered;
    private String totalVat;
    private String total;
    private String description;
    private String fundsStatus;
    private String delayedPenalties;
    private String chargePercentage;
    private String savingAmount;
    private String advancedPayment;
    private String requisition;
    private String sourceAgreement;
    private String supplierOrder;
    private String shipToLocation;
    private String email;
    private String communicationMethod;
    private String site;
    private String requisitioningBU;
    private String requiredAcknowledgment;
    private String paymentTerms;
    private String shippingMethod;
    private String freightTerms;
    private String termsAndConditions;
    private String supplierSiteId;
    private String taxRegistrationNumber;
    private String alternateName;
    private String addressId;
    private String addressName;
    private String contractType;
    private String contractTypeCode;
    private String contractTypeId;
    private String creationDate;
    private String totalCurrancy;
    private String versionNumber;
    private String relatedPoNumber;
    private String orderWithoutVersion;
    private String approvedDate;
    private String maxEndDate;
    private String minEndDate;
    private String changeOrderDesc;
    ArrayList<CreateContractBean> contractList;
    ArrayList<PurchaseOrderLineBean> lineList;
    ArrayList<JSONObject> contractTypeList;

    public void setPOHeaderId(String POHeaderId) {
        this.POHeaderId = POHeaderId;
    }

    public String getPOHeaderId() {
        return POHeaderId;
    }

    public void setOrderNumber(String OrderNumber) {
        this.OrderNumber = OrderNumber;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setSoldToLegalEntity(String SoldToLegalEntity) {
        this.SoldToLegalEntity = SoldToLegalEntity;
    }

    public String getSoldToLegalEntity() {
        return SoldToLegalEntity;
    }

    public void setCreationDate(String CreationDate) {
        this.CreationDate = CreationDate;
    }

    public String getCreationDate() {
        return CreationDate;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getStatus() {
        return Status;
    }

    public void setProcurementBU(String ProcurementBU) {
        this.ProcurementBU = ProcurementBU;
    }

    public String getProcurementBU() {
        return ProcurementBU;
    }

    public void setBuyer(String Buyer) {
        this.Buyer = Buyer;
    }

    public String getBuyer() {
        return Buyer;
    }

    public void setSupplierId(String SupplierId) {
        this.SupplierId = SupplierId;
    }

    public String getSupplierId() {
        return SupplierId;
    }

    public void setSupplier(String Supplier) {
        this.Supplier = Supplier;
    }

    public String getSupplier() {
        return Supplier;
    }

    public void setSupplierSite(String SupplierSite) {
        this.SupplierSite = SupplierSite;
    }

    public String getSupplierSite() {
        return SupplierSite;
    }

    public void setSupplierContact(String SupplierContact) {
        this.SupplierContact = SupplierContact;
    }

    public String getSupplierContact() {
        return SupplierContact;
    }

    public void setBillToLocation(String BillToLocation) {
        this.BillToLocation = BillToLocation;
    }

    public String getBillToLocation() {
        return BillToLocation;
    }

    public void setCurrency(String Currency) {
        this.Currency = Currency;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setPaymentTerms(String PaymentTerms) {
        this.PaymentTerms = PaymentTerms;
    }

    public String getPaymentTerms() {
        return PaymentTerms;
    }

    public void setFOB(String FOB) {
        this.FOB = FOB;
    }

    public String getFOB() {
        return FOB;
    }

    public void setNoteToSupplier(String NoteToSupplier) {
        this.NoteToSupplier = NoteToSupplier;
    }

    public String getNoteToSupplier() {
        return NoteToSupplier;
    }

    public void setNoteToReceiver(String NoteToReceiver) {
        this.NoteToReceiver = NoteToReceiver;
    }

    public String getNoteToReceiver() {
        return NoteToReceiver;
    }

    public void setProcurementBUId(String ProcurementBUId) {
        this.ProcurementBUId = ProcurementBUId;
    }

    public String getProcurementBUId() {
        return ProcurementBUId;
    }

    public void setSoldToLegalEntityId(String SoldToLegalEntityId) {
        this.SoldToLegalEntityId = SoldToLegalEntityId;
    }

    public String getSoldToLegalEntityId() {
        return SoldToLegalEntityId;
    }

    public void setPaymentTermsId(String PaymentTermsId) {
        this.PaymentTermsId = PaymentTermsId;
    }

    public String getPaymentTermsId() {
        return PaymentTermsId;
    }

    public void setCurrencyCode(String CurrencyCode) {
        this.CurrencyCode = CurrencyCode;
    }

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setContractValue(String contractValue) {
        this.contractValue = contractValue;
    }

    public String getContractValue() {
        return contractValue;
    }

    public void setLineList(ArrayList<PurchaseOrderLineBean> lineList) {
        this.lineList = lineList;
    }

    public ArrayList<PurchaseOrderLineBean> getLineList() {
        return lineList;
    }

    public void setOrdered(String ordered) {
        this.ordered = ordered;
    }

    public String getOrdered() {
        return ordered;
    }

    public void setTotalVat(String totalVat) {
        this.totalVat = totalVat;
    }

    public String getTotalVat() {
        return totalVat;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setFundsStatus(String fundsStatus) {
        this.fundsStatus = fundsStatus;
    }

    public String getFundsStatus() {
        return fundsStatus;
    }

    public void setDelayedPenalties(String delayedPenalties) {
        this.delayedPenalties = delayedPenalties;
    }

    public String getDelayedPenalties() {
        return delayedPenalties;
    }

    public void setChargePercentage(String chargePercentage) {
        this.chargePercentage = chargePercentage;
    }

    public String getChargePercentage() {
        return chargePercentage;
    }

    public void setSavingAmount(String savingAmount) {
        this.savingAmount = savingAmount;
    }

    public String getSavingAmount() {
        return savingAmount;
    }

    public void setAdvancedPayment(String advancedPayment) {
        this.advancedPayment = advancedPayment;
    }

    public String getAdvancedPayment() {
        return advancedPayment;
    }

    public void setRequisition(String requisition) {
        this.requisition = requisition;
    }

    public String getRequisition() {
        return requisition;
    }

    public void setSourceAgreement(String sourceAgreement) {
        this.sourceAgreement = sourceAgreement;
    }

    public String getSourceAgreement() {
        return sourceAgreement;
    }

    public void setSupplierOrder(String supplierOrder) {
        this.supplierOrder = supplierOrder;
    }

    public String getSupplierOrder() {
        return supplierOrder;
    }

    public void setShipToLocation(String shipToLocation) {
        this.shipToLocation = shipToLocation;
    }

    public String getShipToLocation() {
        return shipToLocation;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setCommunicationMethod(String communicationMethod) {
        this.communicationMethod = communicationMethod;
    }

    public String getCommunicationMethod() {
        return communicationMethod;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getSite() {
        return site;
    }

    public void setRequisitioningBU(String requisitioningBU) {
        this.requisitioningBU = requisitioningBU;
    }

    public String getRequisitioningBU() {
        return requisitioningBU;
    }

    public void setRequiredAcknowledgment(String requiredAcknowledgment) {
        this.requiredAcknowledgment = requiredAcknowledgment;
    }

    public String getRequiredAcknowledgment() {
        return requiredAcknowledgment;
    }

    public void setPaymentTerms1(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public String getPaymentTerms1() {
        return paymentTerms;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setFreightTerms(String freightTerms) {
        this.freightTerms = freightTerms;
    }

    public String getFreightTerms() {
        return freightTerms;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setSupplierSiteId(String supplierSiteId) {
        this.supplierSiteId = supplierSiteId;
    }

    public String getSupplierSiteId() {
        return supplierSiteId;
    }

    public void setTaxRegistrationNumber(String taxRegistrationNumber) {
        this.taxRegistrationNumber = taxRegistrationNumber;
    }

    public String getTaxRegistrationNumber() {
        return taxRegistrationNumber;
    }

    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public String getAlternateName() {
        return alternateName;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setContractList(ArrayList<CreateContractBean> contractList) {
        this.contractList = contractList;
    }

    public ArrayList<CreateContractBean> getContractList() {
        return contractList;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractType() {
        return contractType;
    }

    public void setCreationDate1(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate1() {
        return creationDate;
    }

    public void setContractTypeCode(String contractTypeCode) {
        this.contractTypeCode = contractTypeCode;
    }

    public String getContractTypeCode() {
        return contractTypeCode;
    }

    public void setTotalCurrancy(String totalCurrancy) {
        this.totalCurrancy = totalCurrancy;
    }

    public String getTotalCurrancy() {
        return totalCurrancy;
    }

    public void setContractTypeId(String contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getContractTypeId() {
        return contractTypeId;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setRelatedPoNumber(String relatedPoNumber) {
        this.relatedPoNumber = relatedPoNumber;
    }

    public String getRelatedPoNumber() {
        return relatedPoNumber;
    }

    public void setOrderWithoutVersion(String orderWithoutVersion) {
        this.orderWithoutVersion = orderWithoutVersion;
    }

    public String getOrderWithoutVersion() {
        return orderWithoutVersion;
    }

    public void setContractTypeList(ArrayList<JSONObject> contractTypeList) {
        this.contractTypeList = contractTypeList;
    }

    public ArrayList<JSONObject> getContractTypeList() {
        return contractTypeList;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setMaxEndDate(String maxEndDate) {
        this.maxEndDate = maxEndDate;
    }

    public String getMaxEndDate() {
        return maxEndDate;
    }

    public void setMinEndDate(String minEndDate) {
        this.minEndDate = minEndDate;
    }

    public String getMinEndDate() {
        return minEndDate;
    }

    public void setChangeOrderDesc(String changeOrderDesc) {
        this.changeOrderDesc = changeOrderDesc;
    }

    public String getChangeOrderDesc() {
        return changeOrderDesc;
    }
}
