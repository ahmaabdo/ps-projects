package com.appspro.nhc.dao;

import com.appspro.nhc.bean.ContractLinesBean;
import com.appspro.nhc.bean.CreateContractBean;

import common.Soap.updateContractSoap;

import common.restHelper.RestHelper;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class AddLinesToExistingContract extends RestHelper{

   public JSONObject fillContractBean(String data) {

        JSONObject objResponse = new JSONObject();
        JSONObject objData = new JSONObject(data);
        ArrayList<ContractLinesBean> Lineslist =
            new ArrayList<ContractLinesBean>();
        CreateContractBean contractBean = new CreateContractBean();

        try {
            /********* set contract header data ************/
            contractBean.setRelatedPoNumber(objData.has("orderNumber") ?objData.get("orderNumber").toString() :"null");
            contractBean.setOrderNumber(objData.get("orderNumber").toString());
            contractBean.setContractNumber(objData.get("contractNumber").toString());
            contractBean.setFOB(objData.get("FOB").toString());
            contractBean.setFreightTerms(objData.get("freightTerms").toString());
            contractBean.setPaymentTerms(objData.get("paymentTerms").toString());
            contractBean.setSupplierSite(objData.get("supplierSite").toString());
            contractBean.setUpdateDate(objData.get("updateDate").toString());
            contractBean.setStartDate(objData.get("contractStartDate").toString());
            contractBean.setEndDate(objData.get("contractEndDate").toString());
            contractBean.setVat(objData.get("vat").toString().replace("SAR", "").trim());
            contractBean.setTotalWithVat(objData.get("totalWithVat").toString());
            /********** set contract line data ***********/
            JSONArray linesArray =
                new JSONArray(objData.get("linesArray").toString());
            for (int i = 0; i < linesArray.length(); i++) {
                JSONObject lineData = linesArray.getJSONObject(i);
                ContractLinesBean Linesbean = new ContractLinesBean();
                Linesbean.setLineNumber(lineData.get("lineNumber").toString());
                Linesbean.setCategory(lineData.get("category").toString());
                Linesbean.setDescription(lineData.get("descriptionCode").toString().replaceAll("&","&amp;"));
                Linesbean.setQuantity(lineData.has("quantity") ?lineData.get("quantity").toString() :"0");
                Linesbean.setUOM(lineData.has("UOM")?lineData.get("UOM").toString() : "");
                Linesbean.setPrice(lineData.has("price")?lineData.get("price").toString() : "");
                Linesbean.setPriceCurrency(lineData.has("priceCurrency")?lineData.get("priceCurrency").toString() :"");
                Linesbean.setOrdered(lineData.get("ordered").toString());
                Linesbean.setOrderedCurrency(lineData.get("orderedCurrency").toString());
                Linesbean.setShipToOrganizationName(lineData.get("shipToOrg").toString());
                Linesbean.setShipToLocation(objData.get("shipToLoc").toString());
                Linesbean.setDeliveryDate(lineData.get("deliveryDate").toString());
                Linesbean.setTotalAmount(lineData.get("totalAmount").toString());
                Linesbean.setTotalTaxAmount(lineData.get("totalTaxAmount").toString());
                Linesbean.setTotalTaxCurrency(lineData.get("totalTaxCurrency").toString());
                Lineslist.add(Linesbean);
            }
            contractBean.setLines(Lineslist);
        } catch (Exception e) {
            e.printStackTrace();
            objResponse.put("status", "Error");
            objResponse.put("message", e.getMessage());
        }

        /*********** get contract details *************/
        try {
            objResponse = getContractDetails(contractBean);
        } catch (Exception e) {
            e.printStackTrace();
            objResponse.put("status", "Error");
            objResponse.put("message", e.getMessage());
        }

        return objResponse;
    }

    public JSONObject getContractDetails(CreateContractBean contractBean) {
        JSONObject objResponse = new JSONObject();
        String restApi = getInstanceUrl() + getContractsRestURL() +"?q=ContractNumber=" + contractBean.getContractNumber();
        String response = httpGet(restApi);
        JSONArray arr = new JSONArray();
        JSONObject obj = new JSONObject(response.toString());
        if (obj.has("items")) {
            arr = obj.getJSONArray("items");
            if (arr.length() > 0) {
                for (int i = 0; i < arr.length(); i++) {
                    String dffApi = getInstanceUrl() + getContractsRestURL()+"/"+
                                    arr.getJSONObject(i).get("ContractId").toString()+"/child/ContractHeaderFlexfieldVA";
                    String dffResponse = httpGet(dffApi);
                    JSONObject objData = new JSONObject(dffResponse.toString());
                    double vat=0,totalWithVat=0;
                    JSONObject vatObj=objData.getJSONArray("items").getJSONObject(0);
                    String objVat="null".equalsIgnoreCase(vatObj.get("vat").toString()) ? "0": vatObj.get("vat").toString();
                    String objTvat="null".equalsIgnoreCase(vatObj.get("totalWithVat").toString()) ? "0": vatObj.get("totalWithVat").toString();
                    vat =Double.parseDouble(contractBean.getVat())+ Double.parseDouble(objVat);  
                    totalWithVat=Double.parseDouble(contractBean.getTotalWithVat())+Double.parseDouble(objTvat);    
                    contractBean.setVat(String.valueOf(vat));
                    contractBean.setTotalWithVat(String.valueOf(totalWithVat));
                    if ("UNDER_AMENDMENT".equalsIgnoreCase(arr.getJSONObject(i).get("StsCode").toString())) {
                        System.out.println("------------>1");
                        contractBean.setContractId(arr.getJSONObject(i).get("ContractId").toString());
                        contractBean.setId(arr.getJSONObject(i).get("Id").toString());
                        contractBean.setContractTypeId(arr.getJSONObject(i).get("ContractTypeId").toString());
                        contractBean.setOrgId(arr.getJSONObject(i).get("OrgId").toString());
                        contractBean.setCurrencyCode(arr.getJSONObject(i).get("CurrencyCode").toString());
                        contractBean.setLegalEntity(arr.getJSONObject(i).get("LegalEntityId").toString());
                        contractBean.setBayOrSell("B");
                        contractBean.setStatus("UNDER_AMENDMENT");
                        contractBean.setStartDate(arr.getJSONObject(i).get("StartDate").toString());
                        objResponse = addLinesToContract(contractBean);
                        break;
                    } else if ("DRAFT".equalsIgnoreCase(arr.getJSONObject(i).get("StsCode").toString())) {
                        System.out.println("------------>2");
                        contractBean.setContractId(arr.getJSONObject(i).get("ContractId").toString());
                        contractBean.setId(arr.getJSONObject(i).get("Id").toString());
                        contractBean.setContractTypeId(arr.getJSONObject(i).get("ContractTypeId").toString());
                        contractBean.setOrgId(arr.getJSONObject(i).get("OrgId").toString());
                        contractBean.setCurrencyCode(arr.getJSONObject(i).get("CurrencyCode").toString());
                        contractBean.setLegalEntity(arr.getJSONObject(i).get("LegalEntityId").toString());
                        contractBean.setBayOrSell("B");
                        contractBean.setStatus("DRAFT");
                        contractBean.setStartDate(arr.getJSONObject(i).get("StartDate").toString());
                        objResponse = addLinesToContract(contractBean);
                        break;
                    } else if("ACTIVE".equalsIgnoreCase(arr.getJSONObject(i).get("StsCode").toString())){
                        System.out.println("------------>3");
                        contractBean.setId(arr.getJSONObject(i).get("Id").toString());
                        contractBean.setStartDate(arr.getJSONObject(i).get("StartDate").toString());
                            objResponse =new updateContractSoap().amendContract(contractBean);
                            if ("success".equalsIgnoreCase(objResponse.get("status").toString())) {
                                objResponse = getContractDetails(contractBean);
                            }
                    }
                }

            } else {
                objResponse.put("status", "Error");
                objResponse.put("message","No details found for the contract " +contractBean.getContractNumber());
            }
        } else {
            objResponse.put("status", "Error");
            objResponse.put("message","No details found for the contract " + contractBean.getContractNumber());
        }
        return objResponse;
    }

    
    public JSONObject addLinesToContract(CreateContractBean contractBean){
        JSONObject objResponse = new JSONObject();
        objResponse= new updateContractSoap().updateContract(contractBean,2);
        
        return objResponse;
    }
}
