package com.appspro.nhc.dao;

import com.appspro.nhc.bean.ContractLinesBean;

import com.appspro.nhc.bean.CreateContractBean;

import common.Soap.updateContractSoap;

import common.restHelper.RestHelper;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class updateContractDAO {
    RestHelper resthelper = new RestHelper();
    public JSONObject updateContract(String data) {
        JSONObject objResponse = new JSONObject();
        JSONObject objData = new JSONObject(data);
        ArrayList<ContractLinesBean> Lineslist =
            new ArrayList<ContractLinesBean>();
        CreateContractBean contractBean = new CreateContractBean(); 

        try{
            /********* set contract header data ************/
            contractBean.setRelatedPoNumber(objData.has("orderNumber")? objData.get("orderNumber").toString():"null");
            contractBean.setOrderNumber(objData.get("orderNumber").toString());
            contractBean.setContractNumber(objData.get("contractNumber").toString().trim());
            contractBean.setFOB(objData.get("FOB").toString());
            contractBean.setFreightTerms(objData.get("freightTerms").toString());
            contractBean.setPaymentTerms(objData.get("paymentTerms").toString());
            contractBean.setSupplierSite(objData.get("supplierSite").toString());
            contractBean.setUpdateDate(objData.get("updateDate").toString());
            contractBean.setStartDate(objData.get("contractStartDate").toString());
            contractBean.setEndDate(objData.get("contractEndDate").toString());
            contractBean.setVat(objData.get("vat").toString().replace("SAR", "").trim());
            contractBean.setTotalWithVat(objData.get("totalWithVat").toString());
            /********** set contract line data ***********/
            JSONArray linesArray=new JSONArray(objData.get("linesArray").toString());
            for (int i = 0; i <linesArray.length(); i++) {
                JSONObject lineData = linesArray.getJSONObject(i);
                ContractLinesBean Linesbean = new ContractLinesBean();
                Linesbean.setLineNumber(lineData.get("lineNumber").toString());
                Linesbean.setCategory(lineData.get("category").toString());
                Linesbean.setDescription(lineData.get("descriptionCode").toString().replaceAll("&", "&amp;"));
                Linesbean.setQuantity(lineData.has("quantity")?lineData.get("quantity").toString():"0");
                Linesbean.setUOM(lineData.has("UOM")?lineData.get("UOM").toString():"");
                Linesbean.setPrice(lineData.has("price")?lineData.get("price").toString():"");
                Linesbean.setPriceCurrency(lineData.has("priceCurrency")?lineData.get("priceCurrency").toString():"");
                Linesbean.setOrdered(lineData.get("ordered").toString());
                Linesbean.setOrderedCurrency(lineData.get("orderedCurrency").toString());
                Linesbean.setShipToOrganizationName(lineData.get("shipToOrg").toString());
                Linesbean.setShipToLocation(objData.get("shipToLoc").toString());
                Linesbean.setDeliveryDate(lineData.get("deliveryDate").toString());
                Linesbean.setTotalAmount(lineData.get("totalAmount").toString());
                Linesbean.setTotalTaxAmount(lineData.get("totalTaxAmount").toString());
                Linesbean.setTotalTaxCurrency(lineData.get("totalTaxCurrency").toString());
                Lineslist.add(Linesbean);
            }
            contractBean.setLines(Lineslist);
           ////////////////////////////// 
        }catch(Exception e){
            e.printStackTrace();
            objResponse.put("status", "Error");
            objResponse.put("message",e.getMessage());  
        }
        /*********** get contract details *************/
        try{
          objResponse= new updateContractDAO().getContractDetails(contractBean);
        }catch(Exception e){
            e.printStackTrace();
            objResponse.put("status", "Error");
            objResponse.put("message",e.getMessage());  
        }
        return objResponse;
    }
    
    public JSONObject getContractDetails(CreateContractBean contractBean) {
        JSONObject objResponse = new JSONObject();
        String restApi =resthelper.getInstanceUrl() + resthelper.getContractsRestURL() +
            "?q=ContractNumber=" + contractBean.getContractNumber();
        String response = resthelper.httpGet(restApi);
        JSONArray arr = new JSONArray();
        JSONObject obj = new JSONObject(response.toString());
        if (obj.has("items")) {
            arr = obj.getJSONArray("items");
            if (arr.length() > 0) {
                for (int i = 0; i < arr.length(); i++) {
                    if ("UNDER_AMENDMENT".equalsIgnoreCase(arr.getJSONObject(i).get("StsCode").toString())) {
                        System.out.println("------------>1");
                        contractBean.setContractId(arr.getJSONObject(i).get("ContractId").toString());
                        contractBean.setId(arr.getJSONObject(i).get("Id").toString());
                        contractBean.setContractTypeId(arr.getJSONObject(i).get("ContractTypeId").toString());
                        contractBean.setOrgId(arr.getJSONObject(i).get("OrgId").toString());
                        contractBean.setCurrencyCode(arr.getJSONObject(i).get("CurrencyCode").toString());
                        contractBean.setLegalEntity(arr.getJSONObject(i).get("LegalEntityId").toString());
                        contractBean.setBayOrSell("B");
                        contractBean.setStatus("UNDER_AMENDMENT");
                        contractBean.setStartDate(arr.getJSONObject(i).get("StartDate").toString());
                        objResponse = getContractLines(contractBean);
                        break;
                    } else if ("DRAFT".equalsIgnoreCase(arr.getJSONObject(i).get("StsCode").toString())) {
                        System.out.println("------------>2");
                        contractBean.setContractId(arr.getJSONObject(i).get("ContractId").toString());
                        contractBean.setId(arr.getJSONObject(i).get("Id").toString());
                        contractBean.setContractTypeId(arr.getJSONObject(i).get("ContractTypeId").toString());
                        contractBean.setOrgId(arr.getJSONObject(i).get("OrgId").toString());
                        contractBean.setCurrencyCode(arr.getJSONObject(i).get("CurrencyCode").toString());
                        contractBean.setLegalEntity(arr.getJSONObject(i).get("LegalEntityId").toString());
                        contractBean.setBayOrSell("B");
                        contractBean.setStatus("DRAFT");
                        contractBean.setStartDate(arr.getJSONObject(i).get("StartDate").toString());
                        objResponse = getContractLines(contractBean);
                        break;
                    } else if("ACTIVE".equalsIgnoreCase(arr.getJSONObject(i).get("StsCode").toString())){
                        System.out.println("------------>3");
                        contractBean.setId(arr.getJSONObject(i).get("Id").toString());
                        contractBean.setStartDate(arr.getJSONObject(i).get("StartDate").toString());
                            objResponse =new updateContractSoap().amendContract(contractBean);
                            if ("success".equalsIgnoreCase(objResponse.get("status").toString())) {
                                objResponse = getContractDetails(contractBean);
                            }
                    }
                }

            } else {
                objResponse.put("status", "Error");
                objResponse.put("message","No details found for the contract "+contractBean.getContractNumber());     
            }
        }else{
            objResponse.put("status", "Error");
            objResponse.put("message","No details found for the contract "+contractBean.getContractNumber());    
        }
        return objResponse;
    }
    
    public JSONObject getContractLines(CreateContractBean contractBean){
        JSONObject objResponse = new JSONObject();
        
        // contractBean.getLines() ------> order lines
        /************ get lines data for contracts ****************/
        try {
           
            JSONArray incomingContractLines = new JSONArray();
            String restApi =resthelper.getInstanceUrl() + resthelper.getContractsRestURL()+"/" +contractBean.getContractId() + "/child/ContractLine";
           System.out.println(restApi);
            String response = resthelper.httpGet(restApi);
            JSONObject obj = new JSONObject(response.toString());
            if (obj.has("items")) {
                incomingContractLines = obj.getJSONArray("items");
                if(incomingContractLines.length()== contractBean.getLines().size()){
                    System.out.println("******1");
                    /********** order lines equals contract lines *************/
                    for (int i = 0; i < incomingContractLines.length(); i++) {
                        JSONObject lineObj =
                            incomingContractLines.getJSONObject(i);
                        for (int j = 0; j < contractBean.getLines().size();
                             j++) {
                            if (lineObj.get("LineNumber").toString().equalsIgnoreCase(contractBean.getLines().get(j).getLineNumber())) {
                                contractBean.getLines().get(j).setLineId(lineObj.get("LineId").toString());
                                contractBean.getLines().get(j).setLineNumber(lineObj.get("LineNumber").toString());
                                if (!"".equalsIgnoreCase(contractBean.getLines().get(j).getPrice()) &&
                                    Double.parseDouble(contractBean.getLines().get(j).getQuantity()) ==0) {
                                    contractBean.getLines().get(j).setQuantity("1");
                                    contractBean.getLines().get(j).setPrice("0");
                                }
                                contractBean.getLines().get(j).setDelete(false);
                            }
                        }
                    }
                   objResponse= updateContractSoap(1, contractBean);
                }else if(contractBean.getLines().size() > incomingContractLines.length()) {
                    System.out.println("******2"+contractBean.getLines().size());
                    /******* order lines is bigger than contract lines ************/
                    for (int j = 0; j < contractBean.getLines().size(); j++) {
                        for (int i = 0; i < incomingContractLines.length();i++) {
                            JSONObject lineObj =incomingContractLines.getJSONObject(i);
                            if (lineObj.get("LineNumber").toString().equalsIgnoreCase(contractBean.getLines().get(j).getLineNumber())) {
                                contractBean.getLines().get(j).setLineId(lineObj.get("LineId").toString());
                                contractBean.getLines().get(j).setLineNumber(lineObj.get("LineNumber").toString());
                                contractBean.getLines().get(j).setDelete(false);
                                if (!"".equalsIgnoreCase(contractBean.getLines().get(j).getPrice()) &&
                                    Double.parseDouble(contractBean.getLines().get(j).getQuantity()) == 0) {
                                    contractBean.getLines().get(j).setQuantity("1");
                                    contractBean.getLines().get(j).setPrice("0");
                                }
                            }
                        }
                    } 
                   objResponse= updateContractSoap(2, contractBean);
                }else{
                    System.out.println("******3");
                    /********** order lines less than contract lines *************/
                    
                    ArrayList<ContractLinesBean> linesList=new ArrayList<ContractLinesBean>();
                   
                    for(int i=0 ; i<incomingContractLines.length() ;i++){
                        ContractLinesBean Linesbean = new ContractLinesBean();
                        boolean found=false;
                        JSONObject lineObj = incomingContractLines.getJSONObject(i); 
                        System.out.println(new JSONObject(lineObj).toString());
                        for(int j=0 ; j<contractBean.getLines().size() ; j++){
                            
                            if(lineObj.get("LineNumber").toString().
                            equalsIgnoreCase(contractBean.getLines().get(j).getLineNumber())){
                                Linesbean.setLineId(lineObj.get("LineId").toString());
                                Linesbean.setLineNumber(contractBean.getLines().get(j).getLineNumber());
                                Linesbean.setCategory(contractBean.getLines().get(j).getCategory());
                                Linesbean.setDescription(contractBean.getLines().get(j).getDescription());
                                Linesbean.setQuantity(contractBean.getLines().get(j).getQuantity());
                                Linesbean.setUOM(contractBean.getLines().get(j).getUOM());
                                Linesbean.setPrice(contractBean.getLines().get(j).getPrice());
                                Linesbean.setPriceCurrency(contractBean.getLines().get(j).getPriceCurrency());
                                Linesbean.setOrdered(contractBean.getLines().get(j).getOrdered());
                                Linesbean.setOrderedCurrency(contractBean.getLines().get(j).getOrderedCurrency());
                                Linesbean.setShipToOrganizationName(contractBean.getLines().get(j).getShipToOrganizationName());
                                Linesbean.setShipToLocation(contractBean.getLines().get(j).getShipToLocation());
                                Linesbean.setDeliveryDate(contractBean.getLines().get(j).getDeliveryDate());
                                Linesbean.setTotalAmount(contractBean.getLines().get(j).getTotalAmount());
                                Linesbean.setTotalTaxAmount(contractBean.getLines().get(j).getTotalTaxAmount());
                                Linesbean.setTotalTaxCurrency(contractBean.getLines().get(j).getTotalTaxCurrency());
                                Linesbean.setDelete(false);
                                linesList.add(Linesbean);
                                found=true;
                                if(!"".equalsIgnoreCase(Linesbean.getPrice()) && Double.parseDouble(Linesbean.getQuantity())==0){
                                    Linesbean.setQuantity("1"); 
                                    Linesbean.setPrice("0");
                                }
                            }    
                        } 
                       
                        if (found==false) {
                            Linesbean.setPrice(lineObj.get("UnitPrice").toString());
                            Linesbean.setLineId(lineObj.get("LineId").toString());
                            Linesbean.setUOM(contractBean.getCurrencyCode());
                            Linesbean.setQuantity(lineObj.get("ItemQuantity").toString());
                            Linesbean.setPriceCurrency(contractBean.getCurrencyCode());
                            Linesbean.setOrderedCurrency(contractBean.getCurrencyCode());
                            Linesbean.setDelete(true);
                            linesList.add(Linesbean);
                        }
                    }
                    contractBean.setLines(linesList);
                    
                  objResponse=  updateContractSoap(3, contractBean); 
                }
              
            } else {
                /******* order lines is bigger than contract lines *******/
                updateContractSoap(2, contractBean);
            }

        } catch (Exception e) {
            e.printStackTrace();
            objResponse.put("status", "Error");
            objResponse.put("message",e.getMessage());
        } 
        return objResponse;
    }
    
    public JSONObject updateContractSoap(int linesUpdateType,CreateContractBean contractBean){
       JSONObject objResponse=new JSONObject(); 
       /************ contract has amended or the status is draft ***************/
       objResponse=new updateContractSoap().updateContract(contractBean,linesUpdateType);  
       return objResponse;
    }

}
