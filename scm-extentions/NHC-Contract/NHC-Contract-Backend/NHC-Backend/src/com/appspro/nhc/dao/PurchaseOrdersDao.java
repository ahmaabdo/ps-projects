package com.appspro.nhc.dao;

import com.appspro.nhc.bean.CreateContractBean;

import common.restHelper.RestHelper;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.appspro.nhc.bean.PurchaseOrderBean;

import common.BIReport.biReport;

import java.math.BigDecimal;

import java.io.UnsupportedEncodingException;

import java.util.HashMap;

public class PurchaseOrdersDao extends RestHelper {

    // get all purchase orders


    public String getPurchaseOrders() {

        ArrayList<PurchaseOrderBean> purchaseOrderList = new ArrayList<>();
        ArrayList<CreateContractBean> contractList = new ArrayList<>();
        // calling report to get list of POs
        JSONObject reportData =
            new biReport().getReportCall("getAllPurchaseOrdersReport",
                                         new HashMap<String, String>());
        System.out.println(reportData);
        if (reportData != null) {
            String contractsTypeList =
                getContractTypeList(reportData.get("G_4").toString());
            String data = reportData.get("G_1").toString();

            Object json = new JSONTokener(data).nextValue();
            // check returned data id object or array
            if (json instanceof JSONObject) {
                //you have an object
                // convert data to json object
                JSONObject objectData = new JSONObject(data);
                try {
                    PurchaseOrderBean purchaseBean = new PurchaseOrderBean();
                    purchaseBean.setSupplierId(objectData.get("SUPPLIERID").toString());
                    purchaseBean.setContractValue(objectData.get("CONTRACTVALUE").toString());
                    purchaseBean.setPOHeaderId(objectData.get("POHEADERID").toString());
                    purchaseBean.setStatus(objectData.get("STATUS").toString());
                    purchaseBean.setSoldToLegalEntity(objectData.get("LEGALENTITYNAME").toString());
                    purchaseBean.setRelatedPoNumber(objectData.has("RELATEDPONUMBER") ?
                                                    objectData.get("RELATEDPONUMBER").toString() :
                                                    null);
                    purchaseBean.setVersionNumber(objectData.has("VERSIONNUMBER") ?
                                                  objectData.get("VERSIONNUMBER").toString() :
                                                  null);
                    purchaseBean.setApprovedDate(objectData.has("APPROVED_DATE") ?
                                                 objectData.get("APPROVED_DATE").toString() :
                                                 null);
                    purchaseBean.setChangeOrderDesc(objectData.has("CHANGEORDERDESC") ?
                                                    objectData.get("CHANGEORDERDESC").toString() :
                                                    null);
                    if (purchaseBean.getVersionNumber() != null &&
                        Integer.parseInt(purchaseBean.getVersionNumber()) >
                        0) {
                        purchaseBean.setOrderNumber(objectData.get("PONUMBER").toString() +
                                                    "-" +
                                                    purchaseBean.getVersionNumber());
                    } else {
                        purchaseBean.setOrderNumber(objectData.get("PONUMBER").toString());
                    }
                    purchaseBean.setTotal(String.valueOf(new BigDecimal(objectData.get("ORDERAMOUNT").toString())));
                    purchaseBean.setOrderWithoutVersion(objectData.get("PONUMBER").toString());
                    purchaseBean.setTaxRegistrationNumber(objectData.has("TAXREGISTRATIONNUMBER") ?
                                                          objectData.get("TAXREGISTRATIONNUMBER").toString() :
                                                          null);
                    purchaseBean.setAlternateName(objectData.has("ALTERNATENAME") ?
                                                  new String(objectData.get("ALTERNATENAME").toString().getBytes(),
                                                             "UTF-8") : null);
                    purchaseBean.setAddressId(objectData.has("ADDRESSID") ?
                                              objectData.get("ADDRESSID").toString() :
                                              null);
                    purchaseBean.setAddressName(objectData.has("ADDRESSNAME") ?
                                                objectData.get("ADDRESSNAME").toString() :
                                                null);
                    purchaseBean.setCreationDate(objectData.has("CREATIONDATE") ?
                                                 objectData.get("CREATIONDATE").toString() :
                                                 null);
                    purchaseBean.setContractType(objectData.has("CONTRACTTYPE") ?
                                                 new String(objectData.get("CONTRACTTYPE").toString().getBytes(),
                                                            "UTF-8") : null);
                    purchaseBean.setContractTypeCode(objectData.has("CONTRACTTYPE") ?
                                                     objectData.get("CONTRACTTYPE").toString() :
                                                     null);
                    // get current history and contracts for po
                    if (objectData.has("G_2")) {
                        ArrayList<JSONObject> poHistory =
                            new ArrayList<JSONObject>();
                        if (objectData.has("G_3")) {
                            poHistory =
                                    getPoHistory(objectData.get("G_3").toString());
                        }
                        // check for contracts of this order
                        contractList =
                                getContracts(objectData.get("G_2").toString(),
                                             poHistory, purchaseBean);
                        purchaseBean.setContractList(contractList);
                    }
                    purchaseBean.setContractTypeId(contractsTypeList);
                    return new JSONObject(purchaseBean).toString();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return "Error";
                }

            } else if (json instanceof JSONArray) {
                //you have an array
                // convert data to json object
                JSONArray arrayData = new JSONArray(data);

                for (int i = 0; i < arrayData.length(); i++) {
                    JSONObject objectData = arrayData.getJSONObject(i);
                    try {
                        PurchaseOrderBean purchaseBean =
                            new PurchaseOrderBean();
                        purchaseBean.setSupplierId(objectData.get("SUPPLIERID").toString());
                        purchaseBean.setContractValue(objectData.get("CONTRACTVALUE").toString());
                        purchaseBean.setPOHeaderId(objectData.get("POHEADERID").toString());
                        purchaseBean.setStatus(objectData.get("STATUS").toString());
                        purchaseBean.setSoldToLegalEntity(objectData.get("LEGALENTITYNAME").toString());
                        purchaseBean.setRelatedPoNumber(objectData.has("RELATEDPONUMBER") ?
                                                        objectData.get("RELATEDPONUMBER").toString() :
                                                        null);
                        purchaseBean.setVersionNumber(objectData.has("VERSIONNUMBER") ?
                                                      objectData.get("VERSIONNUMBER").toString() :
                                                      null);
                        purchaseBean.setApprovedDate(objectData.has("APPROVED_DATE") ?
                                                     objectData.get("APPROVED_DATE").toString() :
                                                     null);
                        purchaseBean.setChangeOrderDesc(objectData.has("CHANGEORDERDESC") ?
                                                        objectData.get("CHANGEORDERDESC").toString() :
                                                        null);
                        if (purchaseBean.getVersionNumber() != null &&
                            Integer.parseInt(purchaseBean.getVersionNumber()) >
                            0) {
                            purchaseBean.setOrderNumber(objectData.get("PONUMBER").toString() +
                                                        "-" +
                                                        purchaseBean.getVersionNumber());
                        } else {
                            purchaseBean.setOrderNumber(objectData.get("PONUMBER").toString());
                        }
                        purchaseBean.setTotal(String.valueOf(new BigDecimal(objectData.get("ORDERAMOUNT").toString())));
                        purchaseBean.setOrderWithoutVersion(objectData.get("PONUMBER").toString());
                        purchaseBean.setTaxRegistrationNumber(objectData.has("TAXREGISTRATIONNUMBER") ?
                                                              objectData.get("TAXREGISTRATIONNUMBER").toString() :
                                                              null);
                        purchaseBean.setAlternateName(objectData.has("ALTERNATENAME") ?
                                                      new String(objectData.get("ALTERNATENAME").toString().getBytes(),
                                                                 "UTF-8") :
                                                      null);
                        purchaseBean.setAddressId(objectData.has("ADDRESSID") ?
                                                  objectData.get("ADDRESSID").toString() :
                                                  null);
                        purchaseBean.setAddressName(objectData.has("ADDRESSNAME") ?
                                                    objectData.get("ADDRESSNAME").toString() :
                                                    null);
                        purchaseBean.setCreationDate(objectData.has("CREATIONDATE") ?
                                                     objectData.get("CREATIONDATE").toString() :
                                                     null);
                        purchaseBean.setContractType(objectData.has("CONTRACTTYPE") ?
                                                     new String(objectData.get("CONTRACTTYPE").toString().getBytes(),
                                                                "UTF-8") :
                                                     null);
                        purchaseBean.setContractTypeCode(objectData.has("CONTRACTTYPE") ?
                                                         objectData.get("CONTRACTTYPE").toString() :
                                                         null);
                        // get current history and contracts for po
                        if (objectData.has("G_2")) {
                            ArrayList<JSONObject> poHistory =
                                new ArrayList<JSONObject>();
                            if (objectData.has("G_3")) {
                                poHistory =
                                        getPoHistory(objectData.get("G_3").toString());
                            }
                            // check for contracts of this order
                            contractList =
                                    getContracts(objectData.get("G_2").toString(),
                                                 poHistory, purchaseBean);
                            purchaseBean.setContractList(contractList);
                        }
                        purchaseBean.setContractTypeId(contractsTypeList);
                        purchaseOrderList.add(purchaseBean);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "Error";
                    }
                }
                return new JSONArray(purchaseOrderList).toString();
            }
        }
        return "No data found";
    }

    public ArrayList<CreateContractBean> getContracts(String data,
                                                      ArrayList<JSONObject> poHistory,
                                                      PurchaseOrderBean bean) {
        ArrayList<CreateContractBean> contractList = new ArrayList<>();
        Object contractJson = new JSONTokener(data).nextValue();
        // check returned data id object or array
        if (contractJson instanceof JSONObject) {
            //you have an object
            // convert data to json object
            JSONObject contractObjectData = new JSONObject(data);
            CreateContractBean contractBean = new CreateContractBean();
            contractBean.setContractNumber(contractObjectData.get("CONTRACT_NUMBER").toString().trim());
            contractBean.setAmount(contractObjectData.has("ESTIMATED_AMOUNT") ?
                                   String.valueOf(new BigDecimal(contractObjectData.get("ESTIMATED_AMOUNT").toString())) :
                                   "0");
            contractBean.setContractId(contractObjectData.get("CONTRACT_ID").toString());
            contractBean.setId(contractObjectData.get("ID").toString());
            contractBean.setContractTypeId(contractObjectData.get("CONTRACT_TYPE_ID").toString());
            contractBean.setMajorVersion(contractObjectData.get("MAJOR_VERSION").toString());
            contractBean.setStatus(contractObjectData.get("STS_CODE").toString());
            contractBean.setPoVersionNumber(getPurchaseOrderVersion(bean,
                                                                    contractBean.getAmount(),
                                                                    poHistory));
            contractList.add(contractBean);
        } else if (contractJson instanceof JSONArray) {
            //you have an array
            // convert data to json object
            JSONArray contractArrayData = new JSONArray(data);
            for (int i = 0; i < contractArrayData.length(); i++) {
                JSONObject contractObjectData =
                    contractArrayData.getJSONObject(i);
                CreateContractBean contractBean = new CreateContractBean();
                contractBean.setContractNumber(contractObjectData.get("CONTRACT_NUMBER").toString().trim());
                contractBean.setAmount(contractObjectData.has("ESTIMATED_AMOUNT") ?
                                       String.valueOf(new BigDecimal(contractObjectData.get("ESTIMATED_AMOUNT").toString())) :
                                       "0");
                contractBean.setContractId(contractObjectData.get("CONTRACT_ID").toString());
                contractBean.setId(contractObjectData.get("ID").toString());
                contractBean.setContractTypeId(contractObjectData.get("CONTRACT_TYPE_ID").toString());
                contractBean.setMajorVersion(contractObjectData.get("MAJOR_VERSION").toString());
                contractBean.setStatus(contractObjectData.get("STS_CODE").toString());
                contractBean.setPoVersionNumber(getPurchaseOrderVersion(bean,
                                                                        contractBean.getAmount(),
                                                                        poHistory));
                contractList.add(contractBean);
            }
        }
        return contractList;
    }

    public String getContractTypeList(String data) {
        String returnedData = null;
        Object json = new JSONTokener(data).nextValue();
        // check returned data id object or array
        if (json instanceof JSONObject) {
            //you have an object
            // convert data to json object
            JSONObject objectData = new JSONObject(data);
            JSONObject dataObj = new JSONObject();
            try {
                dataObj.put("label",
                            new String(objectData.get("CONTRACTTYPENAME").toString().getBytes(),
                                       "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            dataObj.put("value", objectData.get("CONTRACTTYPEID").toString());
            returnedData = dataObj.toString();
        } else if (json instanceof JSONArray) {
            //you have an array
            // convert data to json object
            JSONArray arrayData = new JSONArray(data);
            ArrayList dataArray = new ArrayList();
            for (int i = 0; i < arrayData.length(); i++) {
                JSONObject objectData = arrayData.getJSONObject(i);
                JSONObject dataObj = new JSONObject();
                try {
                    dataObj.put("label",
                                new String(objectData.get("CONTRACTTYPENAME").toString().getBytes(),
                                           "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                dataObj.put("value",
                            objectData.get("CONTRACTTYPEID").toString());
                dataArray.add(dataObj);
            }
            returnedData = dataArray.toString();
        }
        return returnedData;
    }

    public ArrayList<JSONObject> getPoHistory(String data) {

        ArrayList<JSONObject> returnedData = new ArrayList<JSONObject>();
        Object json = new JSONTokener(data).nextValue();
        // check returned data id object or array
        if (json instanceof JSONObject) {
            //you have an object
            // convert data to json object
            JSONObject objectData = new JSONObject(data);
            returnedData.add(objectData);
        } else if (json instanceof JSONArray) {
            //you have an array
            // convert data to json object
            JSONArray arrayData = new JSONArray(data);
            for (int i = 0; i < arrayData.length(); i++) {
                JSONObject objectData = arrayData.getJSONObject(i);
                returnedData.add(objectData);
            }
        }
        return returnedData;
    }

    public String getPurchaseOrderVersion(PurchaseOrderBean bean,
                                          String contractAmount,
                                          ArrayList<JSONObject> poHistory) {
        String versionNumber = "";
        double contractValue = Double.parseDouble(contractAmount);
        double poValue = Double.parseDouble(bean.getTotal());
        if (poHistory.size() > 0) {
            for (JSONObject objData : poHistory) {

                double changeAmount =
                    Double.parseDouble(objData.get("AMOUNT").toString());
                if ("E".equalsIgnoreCase(objData.get("CHANGE_ORDER_TYPE").toString())) {
                    changeAmount = poValue - changeAmount;
                } else {
                    changeAmount = poValue + changeAmount;
                }

                if (contractValue == changeAmount) {
                    versionNumber = objData.get("VERSION_NUMBER").toString();
                } else {
                    int vn = Integer.parseInt(bean.getVersionNumber());
                    versionNumber = String.valueOf(vn > 0 ? vn-1 : 0);
                }
            }
        } else {
            if (contractValue == poValue) {
                versionNumber = bean.getVersionNumber();
            } else {
                int vn = Integer.parseInt(bean.getVersionNumber());
                versionNumber = String.valueOf(vn > 0 ? vn-1 : 0);
            }
        }
        return versionNumber;
    }

    public static void main(String[] args) {
        new PurchaseOrdersDao().getPurchaseOrders();
    }
}
