/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * PoSummary module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'config/services', 'util/commonhelper', 'appController'
    , 'ojs/ojarraydataprovider', 'ojs/ojpagingdataproviderview', 'ojs/ojknockout', 'ojs/ojpagingcontrol',
    'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojinputtext', 'ojs/ojdialog', 'ojs/ojpopup', 'ojs/ojtable'
    , 'ojs/ojpagingtabledatasource', 'ojs/ojarraytabledatasource'

], function (oj, ko, $, services, commonhelper, app, ArrayDataProvider, PagingDataProviderView) {
    /**
     * The view model for the main content view template
     */
    function PoSummaryContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
        self.labels = ko.observableArray([]);
        self.tableArray = ko.observableArray([]);
        self.tableSelectedIndex = ko.observable();
        self.selectedData = ko.observable();
        self.messages = ko.observableArray();
        self.messagesDataprovider = new oj.ArrayDataProvider(self.messages);
        self.filterOrder = ko.observable();
        self.filterContract = ko.observable();
        self.OrderEvent = ko.observable();
        self.ContractEvent = ko.observable();
        self.contractNumber = ko.observableArray([]);
        self.contractNumberArr = ko.observableArray([])
        self.tableColumns = ko.observableArray([]);
        self.tableColumns2 = ko.observableArray([]);
        self.filteredArray = ko.observableArray([]);
        self.contractsArray = ko.observableArray([]);
        self.otherContractsArray = ko.observableArray([]);
        self.historyDisabled = ko.observable(false);
        self.historyMessage = ko.observable();
        self.tableArray2 = ko.observableArray([]);
        self.dataSource = ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tableArray2, { idAttribute: 'contractNumber' })));
        self.table2Visible = ko.observable(false);

        /********* input for order number filter ***********/
        self.handleValueChangedOrder = function (event) {
            self.OrderEvent(event.detail.value);
            self.filterOrder(document.getElementById('filter1').rawValue);
        };

        /********* input for contract number filter ***********/
        self.handleValueChanged = function (event) {
            self.ContractEvent(event.detail.value);
            var conNumber = document.getElementById('filter2').rawValue;
            if (document.getElementById('filter2').rawValue) {
                for (var i = 0; i < self.tableArray().length; i++) {
                    if (typeof self.tableArray()[i].contracts != 'undefined') {
                        for (var j = 0; j < self.tableArray()[i].contracts.length; j++) {
                            var newData = self.tableArray()[i].contracts[j];
                            if (newData == document.getElementById('filter2').rawValue) {
                                self.tableArray()[i].ContractNumber = newData;
                                self.filterContract(self.tableArray()[i].orderNumber);
                            }
                        }
                    }

                }
            } else {
                self.filterContract('');
            }
        };

        /*********** provider for table ************/
        self.dataprovider = ko.computed(function () {
            var filterNumber;

            if (self.filterOrder()) {
                filterNumber = self.filterOrder();
            } else if (typeof self.ContractEvent() !== 'undefined') {
                if (self.filterContract()) {
                    filterNumber = self.filterContract();
                } else {
                    filterNumber = self.ContractEvent();
                }
            }
            if (typeof filterNumber !== 'undefined') {
                var filtered = self.tableArray().filter((data) => JSON.stringify(data.orderNumber).toLowerCase().includes(filterNumber.toLowerCase()));
            }
            if (typeof filtered != 'undefined') {
                self.filteredArray(filtered);
                return new ArrayDataProvider(filtered, { keyAttributes: 'orderNumber' });
            } else {
                console.log(self.tableArray());
                self.filteredArray(self.tableArray());
                return new ArrayDataProvider(self.tableArray(), { keyAttributes: 'orderNumber' });
            }
        });

        /********************/
        self.clearClick = function (event) {
            self.filterOrder('');
            self.filterContract('');
            return true;
        };
        /******** highlight filtered order ********/
        self.highlightingCellRenderer = function (context) {
            var field = null;
            var filterString = "";
            if (context.columnIndex === 2) {
                field = 'orderNumber';
            }
            var data = context.row[field].toString();
            if (self.filterOrder()) {
                filterString = self.filterOrder();
            } else {
                filterString = self.filterContract();
            }
            if (filterString && filterString.length > 0) {
                var index = data.toLowerCase().indexOf(filterString.toLowerCase());
                if (index > -1) {
                    var highlightedSegment = data.substr(index, filterString.length);
                    data = data.substr(0, index) + '<b>' + highlightedSegment + '</b>' + data.substr(index + filterString.length, data.length - 1);
                }
            }
            // eslint-disable-next-line no-param-reassign
            context.cellContext.parentElement.innerHTML = data;
        };

        // for translation
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        /*********** selection listener for table ************/
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.tableSelectedIndex(currentRow['rowIndex']);
            self.selectedData(self.filteredArray()[self.tableSelectedIndex()]);
            console.log(self.selectedData());
        };

        /*******************/
        self.connected = function () {
            var n = "";
            initTranslations();
            var success = function (data) {
                app.loading(false);
                self.otherContractsArray([]);
                for (var i = 0; i < data.length; i++) {
                    data[i].approvedDate=self.formatDate(new Date(data[i].approvedDate));
                    self.contractsArray([])
                    if (typeof data[i].contractList !== 'undefined') {
                        for (var j = 0; j < data[i].contractList.length; j++) {
                            var contractNumber = data[i].contractList[j].contractNumber;
                            var contractAmount = data[i].contractList[j].amount;
                            var poVersion = data[i].contractList[j].poVersionNumber;
                            if (Math.round(contractAmount) == Math.round(data[i].total)) {
                                self.contractsArray().push(contractNumber);
                            } else {
                                var d = {
                                    contractNumber: data[i].contractList[j].contractNumber,
                                    contractAmount: data[i].contractList[j].amount,
                                    poVersion: data[i].contractList[j].poVersionNumber,
                                    orderNumber: data[i].orderWithoutVersion
                                };
                                self.otherContractsArray().push(d);
                            }
                        }

                        self.contractsArray.sort((a, b) => (a - b));
                        data[i].contracts = self.contractsArray();
                    }
                    self.tableArray().push(data[i]);
                }
                self.filterOrder('');
                console.log(self.tableArray())
                console.log(self.otherContractsArray());
            };
            var fail = function (data) {
                console.log('fail');
                app.loading(false);
            };
            services.getGeneric(commonhelper.purchaseOrdersURL, {}).then(success, fail);
        };
        
        self.viewPo = function (event) {

            if (self.tableSelectedIndex() == null || typeof self.tableSelectedIndex() == 'undefined') {
                self.messages.push({
                    severity: 'error', summary: self.labels.errorMessage(), detail: self.labels.errorDetails(),
                    autoTimeout: (5000)
                });
            } else {
               
                    app.loading(true);
                    localStorage.setItem('storedPoData', JSON.stringify(self.selectedData()));
                    //                oj.Router.rootInstance.store(self.selectedData());
                    oj.Router.rootInstance.go('purchaseOrder');
            }

        };

        /******** show history dialog *********/
        self.history = function () {
            if (self.tableSelectedIndex() == null || typeof self.tableSelectedIndex() == 'undefined') {
                self.messages.push({
                    severity: 'error', summary: self.labels.errorMessage(),
                    autoTimeout: (5000)
                });
            } else {

                if (typeof self.selectedData().contractList != 'undefined') {
                    self.table2Visible(true);
                    self.tableArray2([]);
                    for (var i = 0; i < self.otherContractsArray().length; i++) {
                        if (self.selectedData().orderWithoutVersion == self.otherContractsArray()[i].orderNumber) {
                            var data = {
                                orderVersion: self.otherContractsArray()[i].poVersion,
                                contractAmount: self.otherContractsArray()[i].contractAmount,
                                contractNumber: self.otherContractsArray()[i].contractNumber
                            };

                            self.tableArray2.push(data);

                        }
                    }
                    console.log(self.tableArray2());
                    self.tableArray2.sort((a, b) => (a.contractNumber - b.contractNumber));
                    self.dataSource(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tableArray2,
                        {
                            idAttribute: 'contractNumber'
                        })));

                    changeRowColor();
                    document.getElementById('pagingTable2').onclick = function mouseEvent() {
                        changeRowColor();
                        console.log('mouse click')
                    };
                    document.getElementById('pagingTable2').onkeyup = function mouseEvent() {
                        changeRowColor();
                        console.log('button click')
                    };


                } else {
                    self.table2Visible(false);
                    self.historyMessage('No Contract history for this Order');
                }
                if (self.tableArray2().length <= 0) {
                    self.table2Visible(false);
                    self.historyMessage('No Contract history for this Order');
                }
                document.querySelector('#dialog1').open();
            }
        };

        /********* function for changing color of each row in history table *********/
        function changeRowColor() {
            $("#table2").ready(function () {
                for (var t = 0; t < self.tableArray2().length; t++) {
                    var randomColor = ['#339933', '#ff9966', '#999966', '#3399ff', '#99ccff', '#00cc66', '#6666ff', '#00cc99', '#ff9933',
                        '#ffff99', '#ffcccc', '#ffffff', '#ccff99', '#66ff66', '#339966', '#009999', '#6699ff', '#339933',
                        '#666699', '#993366', '#ff9999', '#ff9999', '#ccff66', '#669900', '#009933', '#9966ff', '#ff0066',
                        '#ff66ff', '#99ff99', '#9999ff', '#66ffcc', '#6666ff', '#ffcc66', '#33cc33', '#0099cc', '#009900'];
                    $("#table2 td").filter(function (index) {
                        return $(this).html() == self.tableArray2()[t].orderVersion;
                    }).parent().css("background", randomColor[parseInt(self.tableArray2()[t].orderVersion)]);
                }
            });
        };


        self.closeDialog = function () {
            $('.custom-tb tbody tr').addClass('importantRule')
            document.querySelector('#dialog1').close();
        };

        /********** action for refresh view ************/
        self.refresh = function () {
            app.loading(true);
            var success = function (data) {
                self.tableArray([]);
                app.loading(false);
                self.otherContractsArray([]);
                for (var i = 0; i < data.length; i++) {
                    data[i].approvedDate=self.formatDate(new Date(data[i].approvedDate));
                    self.contractsArray([])
                    if (typeof data[i].contractList !== 'undefined') {
                        for (var j = 0; j < data[i].contractList.length; j++) {
                            var contractNumber = data[i].contractList[j].contractNumber;
                            var contractAmount = data[i].contractList[j].amount;
                            var poVersion = data[i].contractList[j].poVersionNumber;
                            if (Math.round(contractAmount) == Math.round(data[i].total)) {
                                self.contractsArray().push(contractNumber);
                            } else {
                                var d = {
                                    contractNumber: data[i].contractList[j].contractNumber,
                                    contractAmount: data[i].contractList[j].amount,
                                    poVersion: data[i].contractList[j].poVersionNumber,
                                    orderNumber: data[i].orderWithoutVersion
                                };
                                self.otherContractsArray().push(d);
                            }
                        }
                        data[i].contracts = self.contractsArray();
                    }
                    // data[i].contracts = self.contractsArray();
                    self.tableArray().push(data[i]);
                }
            };
            var fail = function (data) {
                console.log('fail');
            };
            services.getGeneric(commonhelper.purchaseOrdersURL, {}).then(success, fail);
        };
        self.formatDate = function (date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };
        function initTranslations() {
            self.labels = {
                listOfPo: ko.observable(getTranslation('purchaseOrder.listOfPo')),
                viewPo: ko.observable(getTranslation('purchaseOrder.viewPo')),
                errorMessage: ko.observable(getTranslation('purchaseOrder.errorMessage')),
                errorDetails: ko.observable(getTranslation('purchaseOrder.errorDetails')),
                contractTypeError: ko.observable(getTranslation('purchaseOrder.contractTypeError')),
                orderNumberLbl: ko.observable(getTranslation('purchaseOrder.orderNumberLbl') + '....'),
                contractNumberLbl: ko.observable(getTranslation('purchaseOrder.contractNumber') + '....'),
                history: ko.observable(getTranslation('purchaseOrder.history')),
                close: ko.observable(getTranslation('purchaseOrder.close'))
            }
            self.tableColumns([{
                "headerText": "Sold-to Legal Entity",
                "field": "soldToLegalEntity"
            },
            {
                "headerText": "Status",
                "field": "status"
            },
            {
                "headerText": "Order",
                "renderer": self.highlightingCellRenderer
            },
            {
                "headerText": "Approved Date",
                "field": "approvedDate"
            },
            {
                "headerText": "Total",
                "field": "total"
            },
            // {
            //     "headerText": "Related Po Number",
            //     "field": "relatedPoNumber"
            // },
            {
                "headerText": "Contract Value",
                "field": "contractValue"
            },
            {
                "headerText": "Contract Type",
                "field": "contractType"
            },
            {
                "headerText": "Contract Number",
                "field": "contracts"
            }

            ]);
            self.tableColumns2([
                {
                    "headerText": "Contract Number",
                    "field": "contractNumber"
                }, {
                    "headerText": "Contract Amount",
                    "field": "contractAmount"
                }, {
                    "headerText": "Order Version",
                    "field": "orderVersion"
                }
            ]);
        }
    }

    return PoSummaryContentViewModel;
});
