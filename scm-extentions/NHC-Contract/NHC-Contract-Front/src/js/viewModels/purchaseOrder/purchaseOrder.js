/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * purchaseOrder module
 */
define(['ojs/ojcore', 'knockout', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojarraytabledatasource', 'ojs/ojknockout', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojarraydataprovider', 'ojs/ojtable', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojformlayout',
    'ojs/ojcollapsible', 'ojs/ojinputnumber', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojswitcher', 'ojs/ojarraydataprovider', 'ojs/ojmessages', 'ojs/ojdialog', 'ojs/ojpopup',
    'ojs/ojselectcombobox', 'ojs/ojradioset', 'ojs/ojinputnumber'
], function (oj, ko, app, services, commonhelper) {

    function purchaseOrderContentViewModel() {
        var self = this;
        var getTranslation = oj.Translations.getTranslatedString;
        self.labels = ko.observableArray([]);
        self.retrieve = ko.observable();
        self.returndata = ko.observableArray([]);
        self.selectedItem = ko.observable('terms');
        self.edge = ko.observable('top');
        self.lineArray = ko.observableArray();
        self.payloadLines = ko.observableArray([]);
        self.supplierArray = ko.observableArray([]);
        self.messages = ko.observableArray();
        self.dataprovider = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.lineArray, { idAttribute: 'lineNumber' }));
        self.messagesDataprovider = new oj.ArrayDataProvider(self.messages);
        self.messagesContract = ko.observable();
        self.dialogMsg = ko.observable();
        self.contractTypeArray = ko.observableArray();
        self.contractUpdateTypeSelecion = ko.observable('list');
        self.selectedContractNumber = ko.observable();
        self.contractNumberList = ko.observableArray([]);
        self.contractCancelTypeSelecion = ko.observable('allContracts');
        self.activeContracts = ko.observableArray([]);
        self.draftContracts = ko.observableArray([]);
        self.ableToCancel = ko.observable(true);
        self.placeHolder = ko.observable('Please select value');
        self.updateContractVisible = ko.observable(true);
        self.orderModel = {
            poHeaderId: ko.observable(),
            total: ko.observable(),
            delayedPenalties: ko.observable(),
            legalEntity: ko.observable(),
            currency: ko.observable(),
            legalEntityId: ko.observable(),
            description: ko.observable(),
            procurementBUId: ko.observable(),
            savingAmount: ko.observable(),
            buyer: ko.observable(),
            supplier: ko.observable(),
            totalVat: ko.observable(),
            currancyCode: ko.observable(),
            supplierId: ko.observable(),
            billToLocation: ko.observable(),
            status: ko.observable(),
            supplierContact: ko.observable(),
            procurementBU: ko.observable(),
            fundsStatus: ko.observable(),
            creationDate: ko.observable(),
            ordered: ko.observable(),
            orderNumber: ko.observable(),
            contractValue: ko.observable(),
            noteToSupplier: ko.observable(),
            noteToReceiver: ko.observable(),
            requisition: ko.observable(),
            sourceAgreement: ko.observable(),
            supplierOrder: ko.observable(),
            shipToLocation: ko.observable(),
            email: ko.observable(),
            communicationMethod: ko.observable(),
            site: ko.observable(),
            requisitioningBU: ko.observable(),
            requiredAcknowledgment: ko.observable(),
            paymentTerms: ko.observable(),
            paymentTermsId: ko.observable(),
            shippingMethod: ko.observable(),
            freightTerms: ko.observable(),
            FOB: ko.observable(),
            chargePercentage: ko.observable(),
            advancedPayment: ko.observable(),
            termsAndConditions: ko.observable(),
            supplierAlternativeName: ko.observable(),
            taxRegistrationNumber: ko.observable(),
            poNumber: ko.observable(),
            attachment: ko.observable(),
            supplierSiteId: ko.observable(),
            contractType: ko.observable(),
            contractTypeCode: ko.observable(),
            contractTypeId: ko.observable(),
            totalCurrency: ko.observable(),
            relatedPoNumber: ko.observable('no'),
            contractEndDate: ko.observable(),
            changeOrderDesc: ko.observable(),
            minEndDate: ko.observable(),
            vat: ko.observable('no'),
            totalWithVat: ko.observable('no')
        };

        // for translation
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });
        //-----------------------------------------------------------------------
        self.tableColumns = [
            { "headerText": 'Line', "field": 'lineNumber' },
            { "headerText": "Category", "field": "category" },
            { "headerText": "Description", "field": "description" },
            { "headerText": "Quantity", "field": "quantity" },
            { "headerText": "UOM", "field": "uomTitle" },
            { "headerText": "Price", "field": "priceLbl" },
            { "headerText": "Ordered", "field": "orderedLbl" }];
        //   ----------------------------------functions section--------------------------------------------

        self.connected = function () {
            app.loading(true);
            initTranslations();
            self.retrieve(JSON.parse(localStorage.getItem('storedPoData')));
            self.orderModel.changeOrderDesc(self.retrieve().changeOrderDesc);
            if (typeof self.retrieve().contractList != 'undefined' && self.retrieve().contractList.length > 0) {
                var num = (self.retrieve().contracts.length) - 1;
                if (self.retrieve().contracts.length > 1) {
                    self.messagesContract(self.labels.haveAnthorContract1() + self.retrieve().contracts.length + ' ' + self.labels.haveAnthorContract2() + self.retrieve().contracts[num]);
                } else {
                    self.messagesContract(self.labels.haveOneContract() + self.retrieve().contracts[num]);
                }
                console.log(self.retrieve());
                for (var i = 0; i < self.retrieve().contractList.length; i++) {
                    var data = {
                        value: self.retrieve().contractList[i].contractNumber,
                        label: self.retrieve().contractList[i].contractNumber
                    };
                    self.contractNumberList.push(data);
                }
            } else {
                self.messagesContract(self.labels.noContractFound());
            }
            var d = JSON.parse(self.retrieve().contractTypeId);
            self.contractTypeArray(d);

            for (var i in self.contractTypeArray()) {
                if (self.contractTypeArray()[i].label.includes(self.retrieve().contractType)) {
                    self.orderModel.contractType(self.contractTypeArray()[i].value);
                }
            }
            self.getPurchaseOrderDetails();
        };

        self.sendPoToContractBtn = function () {
            document.querySelector('#contractTypeDialog').open();
        };
        self.closeMessageDialog = function (event) {
            document.getElementById('messageDialog1').close();
        };
        self.closeMessageDialog2 = function (event) {
            document.getElementById('messageDialog2').close();
        };
        self.closeUpdateContractDialog = function (event) {
            document.querySelector('#contractUpdateDailog').close();
        };
        self.backBtn = function () {
            app.loading(true);
            oj.Router.rootInstance.go('poSummary');
        };
        //--------------------------------------------get Lines & suppler data from po functions-------------------------------------
        self.getPurchaseOrderDetails = function () {
            console.log(self.retrieve().contractList)
            self.getData = function (data) {
                if (data) {
                    self.orderModel.poHeaderId(data.POHeaderId);
                    self.orderModel.total(numberWithCommas(data.total));
                    self.orderModel.delayedPenalties(data.delayedPenalties);
                    self.orderModel.legalEntity(data.soldToLegalEntity);
                    self.orderModel.currency(data.currency);
                    self.orderModel.legalEntityId(data.soldToLegalEntityId);
                    self.orderModel.description(data.description);
                    self.orderModel.procurementBUId(data.procurementBUId);
                    self.orderModel.savingAmount(data.savingAmount);
                    self.orderModel.buyer(data.buyer);
                    self.orderModel.supplier(data.supplier);
                    self.orderModel.totalVat(numberWithCommas(data.totalVat) + ' ' + data.totalCurrancy);
                    self.orderModel.currancyCode(data.currencyCode);
                    self.orderModel.supplierId(data.supplierId);
                    self.orderModel.billToLocation(data.billToLocation);
                    self.orderModel.status(data.status);
                    self.orderModel.supplierContact(data.supplierContact);
                    self.orderModel.procurementBU(data.procurementBU);
                    self.orderModel.fundsStatus(data.fundsStatus);
                    self.orderModel.creationDate(self.formatDate(new Date(data.creationDate)));
                    self.orderModel.ordered(numberWithCommas(data.ordered) + ' ' + data.totalCurrancy);
                    self.orderModel.orderNumber(data.orderNumber);
                    self.orderModel.contractValue(data.contractValue);
                    self.orderModel.noteToSupplier(data.noteToSupplier);
                    self.orderModel.noteToReceiver(data.noteToReceiver);
                    self.orderModel.requisition(data.requisition);
                    self.orderModel.sourceAgreement(data.sourceAgreement);
                    self.orderModel.supplierOrder(data.supplierOrder);
                    self.orderModel.shipToLocation(data.shipToLocation);
                    self.orderModel.email(data.email);
                    self.orderModel.communicationMethod(data.communicationMethod);
                    self.orderModel.site(data.supplierSite);
                    self.orderModel.requisitioningBU(data.requisitioningBU);
                    self.orderModel.poNumber(self.labels.orderNumber() + self.retrieve().orderNumber);
                    self.orderModel.requiredAcknowledgment(data.requiredAcknowledgment);
                    self.orderModel.paymentTerms(data.paymentTerms);
                    self.orderModel.paymentTermsId(data.paymentTermsId);
                    self.orderModel.shippingMethod(data.shippingMethod);
                    self.orderModel.freightTerms(data.freightTerms);
                    self.orderModel.FOB(data.FOB);
                    self.orderModel.chargePercentage(data.chargePercentage);
                    self.orderModel.savingAmount(data.savingAmount);
                    self.orderModel.advancedPayment(data.advancedPayment);
                    self.orderModel.termsAndConditions(data.termsAndConditions);
                    self.orderModel.supplierSiteId(data.supplierSiteId);
                    self.orderModel.supplierAlternativeName(self.retrieve().alternateName);
                    self.orderModel.taxRegistrationNumber(self.retrieve().taxRegistrationNumber);
                    self.orderModel.totalCurrency(data.totalCurrancy);
                    // self.orderModel.contractType(self.retrieve().contractType);
                    self.orderModel.contractEndDate(data.maxEndDate);
                    self.orderModel.minEndDate(data.minEndDate);
                    self.orderModel.vat(data.totalVat);
                    self.orderModel.totalWithVat(data.total);
                    // self.contractTypeArray(data.contractTypeList);

                    self.orderModel.relatedPoNumber(self.retrieve().relatedPoNumber);
                    for (var i = 0; i < data.lineList.length; i++) {
                        var dataObject = data.lineList;
                        var lineData = {
                            lineNumber: dataObject[i].lineNumber,
                            category: dataObject[i].categoryName,
                            descriptionCode: dataObject[i].descriptionCode,
                            description: dataObject[i].description,
                            quantity: dataObject[i].quantity,
                            UOM: dataObject[i].UOM,
                            uomTitle: dataObject[i].uomTitle,
                            price: dataObject[i].price,
                            priceCurrency: dataObject[i].priceCurrency,
                            priceLbl: dataObject[i].price + ' ' + dataObject[i].priceCurrency,
                            ordered: dataObject[i].ordered,
                            orderedCurrency: dataObject[i].orderedCurrency,
                            orderedLbl: dataObject[i].ordered + ' ' + dataObject[i].orderedCurrency,
                            itemQuantity: dataObject[i].quantity,
                            unitPrice: dataObject[i].price,
                            categoryId: dataObject[i].categoryId,
                            shipToOrg: dataObject[i].shipToOrganizationName,
                            deliveryDate: dataObject[i].requestedDeliveryDate,
                            totalTaxAmount: dataObject[i].totalTaxAmount,
                            totalAmount: dataObject[i].totalAmount,
                            totalTaxCurrency: dataObject[i].totalTaxCurrency
                        };

                        self.lineArray.push(lineData);
                    }
                    self.lineArray.sort((a, b) => (a.lineNumber - b.lineNumber));

                }
                app.loading(false);
            };
            var fail = function (data) {
                console.log('fail');
                app.loading(false);
            };
            services.getGeneric(commonhelper.allLineURL + self.retrieve().POHeaderId, {}).then(self.getData, fail);
        };
        // --------------------------------------------------Create Contract Funtions-------------------------------------      

        /********* action for create new contract *************/
        self.createNewContract = function () {
            if (self.orderModel.contractType() == null || self.orderModel.contractType() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please select contract type first'
                });
            } else {
                self.CreateContract();
            }

        };
        /********* action for update existing contract *************/
        self.updateContract = function () {
            if (self.orderModel.contractType() == null || self.orderModel.contractType() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please select contract type first'
                });
            } else {
                document.querySelector('#contractTypeDialog').close();
                document.querySelector('#contractUpdateDailog').open();
            }

        };
        /********* create new contract ***********/
        self.CreateContract = function () {
            document.querySelector('#contractTypeDialog').close();
            app.loading(true);
            var payload = {
                "currencyCode": self.orderModel.currancyCode(),
                "legalEntity": self.orderModel.legalEntity(),
                "orderNumber": self.orderModel.orderNumber(),
                "freightTerms": self.orderModel.freightTerms(),
                "FOB": self.orderModel.FOB(),
                "supplierId": self.orderModel.supplierId(),
                "paymentTerms": self.orderModel.paymentTerms(),
                "supplierSiteId": self.orderModel.supplierSiteId(),
                "supplier": self.orderModel.supplier(),
                "shipToLoc": self.orderModel.shipToLocation(),
                "supplierSite": self.retrieve().addressId,
                "paymentTermsId": self.orderModel.paymentTermsId(),
                "contractType": self.orderModel.contractType(),
                "amount": self.orderModel.total(),
                "amountCurrency": self.orderModel.totalCurrency(),
                "orgId": self.orderModel.procurementBUId(),
                "relatedPoNumber": self.orderModel.relatedPoNumber(),
                "updateDate": self.retrieve().approvedDate,
                "endDate": self.orderModel.contractEndDate(),
                "vat": self.orderModel.vat(),
                "totalWithVat": self.orderModel.totalWithVat(),
                "linesArray": self.lineArray()
            };
            console.log(JSON.stringify(payload));
            var creatContractCbFn = function (data) {
                if (data.status == 'success') {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog1').open();
                } else {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog2').open();
                }
                app.loading(false);
            };
            var failCbFn = function (data) {
                if (data.status == 'success') {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog1').open();
                } else {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog2').open();
                }
                app.loading(false);
            };
            services.addGeneric("Contract/createContract", JSON.stringify(payload)).then(creatContractCbFn, failCbFn);
        };
        /******** update existing contract *********/
        self.updateExistingContract = function () {
            document.querySelector('#contractTypeDialog').close();
            console.log('ggg');
            console.log(self.selectedContractNumber())
            var payload = {
                "contractNumber": self.selectedContractNumber(),
                "currencyCode": self.orderModel.currancyCode(),
                "legalEntity": self.orderModel.legalEntity(),
                "orderNumber": self.orderModel.orderNumber(),
                "freightTerms": self.orderModel.freightTerms(),
                "FOB": self.orderModel.FOB(),
                "contractEndDate": self.orderModel.contractEndDate(),
                "contractStartDate": self.orderModel.minEndDate(),
                "supplierId": self.orderModel.supplierId(),
                "paymentTerms": self.orderModel.paymentTerms(),
                "supplierSiteId": self.orderModel.supplierSiteId(),
                "supplier": self.orderModel.supplier(),
                "shipToLoc": self.orderModel.shipToLocation(),
                "supplierSite": self.retrieve().addressId,
                "paymentTermsId": self.orderModel.paymentTermsId(),
                "contractType": self.orderModel.contractType(),
                "amount": self.orderModel.total(),
                "amountCurrency": self.orderModel.totalCurrency(),
                "orgId": self.orderModel.procurementBUId(),
                "relatedPoNumber": self.orderModel.relatedPoNumber(),
                "updateDate": self.retrieve().approvedDate,
                "vat": self.orderModel.vat(),
                "totalWithVat": self.orderModel.totalWithVat(),
                "linesArray": self.lineArray()
            };
            var creatContractCbFn = function (data) {
                if (data.status == 'success') {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog1').open();
                } else {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog2').open();
                }
                app.loading(false);
            };
            var failCbFn = function (data) {
                if (data.status == 'success') {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog1').open();
                } else {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog2').open();
                }
                app.loading(false);
            };

            if (self.selectedContractNumber() != null
                && self.selectedContractNumber() != ''
                && typeof self.selectedContractNumber() != 'undefined') {
                document.querySelector('#contractUpdateDailog').close();
                app.loading(true);
                console.log(JSON.stringify(payload))
                services.addGeneric("updateContract/updateExistingContract", JSON.stringify(payload)).then(creatContractCbFn, failCbFn);
            } else {
                self.messages.push({
                    severity: 'error', summary: 'Please enter contract number to update'
                });
            }

        };
        /******** cancel contract relate to this order ********/
        self.cancelContractBtnAction = function () {
            document.querySelector('#contractcancelDailog').open();
        };
        self.closeCancelContractDialog = function (event) {
            document.querySelector('#contractcancelDailog').close();
        };
        self.closeCancelMessageDialog = function () {
            document.querySelector('#cancelMessageDialog').close();
        };
        self.cancelContractAction = async function () {
            if (self.contractCancelTypeSelecion() == null || self.contractCancelTypeSelecion() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please Select which contract you want to cancel'
                });
            } else if (typeof self.retrieve().contractList != 'undefined' && self.retrieve().contractList.length > 0) {
                self.draftContracts([]);
                self.activeContracts([]);
                app.loading(true);
                document.querySelector('#contractcancelDailog').close();
                var target = document.getElementById("demo");
                target.textContent = '';
                if (self.contractCancelTypeSelecion() == 'allContracts') {
                    console.log(self.contractNumberList());
                    console.log(self.retrieve().contractList);
                    for (var i = 0; i < self.retrieve().contractList.length; i++) {
                        if (self.retrieve().contractList[i].status == 'ACTIVE' || self.retrieve().contractList[i].status == 'HOLD') {
                            self.activeContracts.push(self.retrieve().contractList[i]);
                            var node = document.createElement("LI");                 // Create a <li> node
                            var textnode = document.createTextNode('The contract ' + self.retrieve().contractList[i].contractNumber
                                + ' have status ' + self.retrieve().contractList[i].status + ' and it can be closed.');         // Create a text node
                            node.appendChild(textnode);
                            node.style.backgroundColor = "red";
                            target.appendChild(node);
                        }
                        else if (self.retrieve().contractList[i].status == 'DRAFT') {
                            self.draftContracts.push(self.retrieve().contractList[i]);
                            var node = document.createElement("LI");                 // Create a <li> node
                            var textnode = document.createTextNode('This contract ' + self.retrieve().contractList[i].contractNumber
                                + ' have status ' + self.retrieve().contractList[i].status + ' and it can be canceled.');           // Create a text node
                            node.appendChild(textnode);
                            node.style.backgroundColor = "yellow";
                            target.appendChild(node);
                        } else {
                            self.ableToCancel(false);
                            var node = document.createElement("LI");                 // Create a <li> node
                            var textnode = document.createTextNode('This contract ' + self.retrieve().contractList[i].contractNumber
                                + ' have status ' + self.retrieve().contractList[i].status + ' and you cann\'t do any thing for it.');           // Create a text node
                            node.appendChild(textnode);
                            node.style.backgroundColor = "gray";
                            target.appendChild(node);
                        }
                    }

                    console.log(self.draftContracts());
                    console.log(self.draftContracts().length);
                    if (self.draftContracts().length > 0 || self.activeContracts().length > 0) {
                        self.ableToCancel(true);
                    }
                    document.querySelector('#cancelMessageDialog').open();
                    app.loading(false);
                } else {
                    if (self.selectedContractNumber() == null || self.selectedContractNumber() == '') {
                        self.messages.push({
                            severity: 'error', summary: 'Please Select which contract you want to cancel'
                        });
                    } else {
                        var id = '', number = '', type = '', response = '';
                        for (var e = 0; e < self.retrieve().contractList.length; e++) {
                            if (self.selectedContractNumber() == self.retrieve().contractList[e].contractNumber) {
                                id = self.retrieve().contractList[e].id;
                                number = self.retrieve().contractList[e].contractNumber;
                                type = self.retrieve().contractList[e].status;
                            }
                        };
                        if (type == 'DRAFT') {
                            var payload = {
                                "id": id,
                                "number": number,
                                "closeReason": 'CANCELED_PRICING'
                            };
                            response = await cancelContractFNC(payload);
                            console.log(response)
                        } else {
                            var payload = {
                                "id": id,
                                "number": number,
                                "closeReason": 'TERMINATED_NON_COMPLIANCE',
                                "closeDate": self.formatDate(new Date())
                            };
                            response = await closeContractFNC(payload);
                            console.log(response.status)
                        }
                        console.log(response)
                        if (response.status == 'success') {
                            app.loading(false);
                            self.dialogMsg(response.message);
                            document.getElementById('messageDialog1').open();
                        } else {
                            app.loading(false);
                            self.dialogMsg(response.message);
                            document.getElementById('messageDialog2').open();
                        }
                    }
                }
            } else {
                self.messages.push({
                    severity: 'error', summary: 'Their is no contracts to cancel'
                });
            }
        };

        self.addLinesToContract = function () {
            self.updateContractVisible(false);
            if (self.orderModel.contractType() == null || self.orderModel.contractType() == '') {
                self.messages.push({
                    severity: 'error', summary: 'Please select contract type first'
                });
            } else {
                document.querySelector('#contractTypeDialog').close();
                document.querySelector('#contractUpdateDailog').open();
            }
        };

        self.addLinesContract = function () {
            document.querySelector('#contractTypeDialog').close();
            console.log(self.selectedContractNumber())
            var payload = {
                "contractNumber": self.selectedContractNumber(),
                "currencyCode": self.orderModel.currancyCode(),
                "legalEntity": self.orderModel.legalEntity(),
                "orderNumber": self.orderModel.orderNumber(),
                "freightTerms": self.orderModel.freightTerms(),
                "FOB": self.orderModel.FOB(),
                "contractEndDate": self.orderModel.contractEndDate(),
                "contractStartDate": self.orderModel.minEndDate(),
                "supplierId": self.orderModel.supplierId(),
                "paymentTerms": self.orderModel.paymentTerms(),
                "supplierSiteId": self.orderModel.supplierSiteId(),
                "supplier": self.orderModel.supplier(),
                "shipToLoc": self.orderModel.shipToLocation(),
                "supplierSite": self.retrieve().addressId,
                "paymentTermsId": self.orderModel.paymentTermsId(),
                "contractType": self.orderModel.contractType(),
                "amount": self.orderModel.total(),
                "amountCurrency": self.orderModel.totalCurrency(),
                "orgId": self.orderModel.procurementBUId(),
                "relatedPoNumber": self.orderModel.relatedPoNumber(),
                "updateDate": self.retrieve().approvedDate,
                "vat": self.orderModel.vat(),
                "totalWithVat": self.orderModel.totalWithVat(),
                "linesArray": self.lineArray()
            };
            var creatContractCbFn = function (data) {
                if (data.status == 'success') {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog1').open();
                } else {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog2').open();
                }
                app.loading(false);
            };
            var failCbFn = function (data) {
                if (data.status == 'success') {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog1').open();
                } else {
                    self.dialogMsg(data.message);
                    document.getElementById('messageDialog2').open();
                }
                app.loading(false);
            };

            if (self.selectedContractNumber() != null
                && self.selectedContractNumber() != ''
                && typeof self.selectedContractNumber() != 'undefined') {
                document.querySelector('#contractUpdateDailog').close();
                app.loading(true);
                console.log(JSON.stringify(payload))
                services.addGeneric("updateContract/addLinesToContract", JSON.stringify(payload)).then(creatContractCbFn, failCbFn);
            } else {
                self.messages.push({
                    severity: 'error', summary: 'Please enter contract number to update'
                });
            }

        };

        self.submitCancelAction = async function () {
            app.loading(true);
            document.querySelector('#cancelMessageDialog').close();
            console.log(self.activeContracts());
            console.log(self.draftContracts());
            var response;
            var target = document.getElementById("demo");
            target.textContent = '';
            for (var a = 0; a < self.activeContracts().length; a++) {
                console.log(self.activeContracts()[a].id)
                var payload = {
                    "id": self.activeContracts()[a].id,
                    "number": self.activeContracts()[a].contractNumber,
                    "closeReason": 'TERMINATED_NON_COMPLIANCE',
                    "closeDate": self.formatDate(new Date())
                };
                response = await closeContractFNC(payload);
                console.log(response)
                var node = document.createElement("LI");                 // Create a <li> node
                var textnode = document.createTextNode(response.message + ' Contract Number : ' + self.activeContracts()[a].contractNumber);           // Create a text node
                node.appendChild(textnode);
                node.style.backgroundColor = "red";
                target.appendChild(node);
            };
            for (var d = 0; d < self.draftContracts().length; d++) {
                var payload = {
                    "id": self.draftContracts()[d].id,
                    "number": self.draftContracts()[d].contractNumber,
                    "closeReason": 'CANCELED_PRICING'
                };
                response = await cancelContractFNC(payload);
                console.log(response)
                var node = document.createElement("LI");                 // Create a <li> node
                var textnode = document.createTextNode(response.message + ' Contract Number : ' + self.draftContracts()[d].contractNumber);           // Create a text node
                node.appendChild(textnode);
                node.style.backgroundColor = "yellow";
                target.appendChild(node);
            };

            app.loading(false);
            console.log(h);
            self.ableToCancel(false);
            document.querySelector('#cancelMessageDialog').open();

        };

        async function closeContractFNC(payload) {
            var f;
            var creatContractCbFn = function (data) {
                f = data;
            };
            var failCbFn = function (data) {
                f = data;
            };

            await services.addGeneric("Contract/closeContract", JSON.stringify(payload)).then(creatContractCbFn, failCbFn);
            return f;
        };

        async function cancelContractFNC(payload) {
            var f;
            var creatContractCbFn = function (data) {
                f = data;
            };
            var failCbFn = function (data) {
                f = data;
            };
            await services.addGeneric("Contract/cancelContract", JSON.stringify(payload)).then(creatContractCbFn, failCbFn);
            return f;
        }
        /***************************/
        self.formatDate = function (date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        function numberWithCommas(value) {
            value = parseFloat(value.toString().includes(',') ? value.replace(',', '') : value).toFixed(2);
            var sep = ',';
            // check if it needs formatting
            if (value.toString() === value.toLocaleString()) {
                // split decimals
                var parts = value.toString().split('.')
                // format whole numbers
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, sep);
                // put them back together
                value = parts[1] ? parts.join('.') : parts[0];
            } else {
                value = value.toLocaleString();
            }
            return value;
        }

        function parseStringToFloat(stringValue) {
            stringValue = stringValue.trim();
            var result = stringValue.replace(/[^0-9]/g, '');
            if (/[,\.]\d{2}$/.test(stringValue)) {
                result = result.replace(/(\d{2})$/, '.$1');
            }
            return parseFloat(result);
        }
        // ---------------------------------------------------------------------------------------------------------------------------    
        function initTranslations() {
            self.labels = {
                SendContract: ko.observable(getTranslation('purchaseOrder.SendContract')),
                procurementBU: ko.observable(getTranslation('purchaseOrder.procurementBU')),
                requisitioningBU: ko.observable(getTranslation('purchaseOrder.requisitioningBU')),
                legalEntity: ko.observable(getTranslation('purchaseOrder.legalEntity')),
                billToBU: ko.observable(getTranslation('purchaseOrder.billToBU')),
                order: ko.observable(getTranslation('purchaseOrder.order')),
                status: ko.observable(getTranslation('purchaseOrder.status')),
                fundsStatus: ko.observable(getTranslation('purchaseOrder.fundsStatus')),
                buyer: ko.observable(getTranslation('purchaseOrder.buyer')),
                creationDate: ko.observable(getTranslation('purchaseOrder.creationDate')),
                supplier: ko.observable(getTranslation('purchaseOrder.supplier')),
                site: ko.observable(getTranslation('purchaseOrder.site')),
                supplierContact: ko.observable(getTranslation('purchaseOrder.supplierContact')),
                communicationMethod: ko.observable(getTranslation('purchaseOrder.communicationMethod')),
                email: ko.observable(getTranslation('purchaseOrder.email')),
                billToLocation: ko.observable(getTranslation('purchaseOrder.billToLocation')),
                shipToLocation: ko.observable(getTranslation('purchaseOrder.shipToLocation')),
                ordered: ko.observable(getTranslation('purchaseOrder.ordered')),
                totalVat: ko.observable(getTranslation('purchaseOrder.totalVat')),
                total: ko.observable(getTranslation('purchaseOrder.total')),
                description: ko.observable(getTranslation('purchaseOrder.description')),
                changeOrderDescription: ko.observable(getTranslation('purchaseOrder.changeOrderDescription')),
                requisition: ko.observable(getTranslation('purchaseOrder.requisition')),
                sourceAgreement: ko.observable(getTranslation('purchaseOrder.sourceAgreement')),
                supplierOrder: ko.observable(getTranslation('purchaseOrder.supplierOrder')),
                requiredAcknowledgment: ko.observable(getTranslation('purchaseOrder.requiredAcknowledgment')),
                paymentTerms: ko.observable(getTranslation('purchaseOrder.paymentTerms')),
                shippingMethod: ko.observable(getTranslation('purchaseOrder.shippingMethod')),
                freightTerms: ko.observable(getTranslation('purchaseOrder.freightTerms')),
                FOB: ko.observable(getTranslation('purchaseOrder.FOB')),
                noteToSupplier: ko.observable(getTranslation('purchaseOrder.noteToSupplier')),
                noteToReceiver: ko.observable(getTranslation('purchaseOrder.noteToReceiver')),
                delayedPenalties: ko.observable(getTranslation('purchaseOrder.delayedPenalties')),
                chargePercentage: ko.observable(getTranslation('purchaseOrder.chargePercentage')),
                savingAmount: ko.observable(getTranslation('purchaseOrder.savingAmount')),
                advancedPayment: ko.observable(getTranslation('purchaseOrder.advancedPayment')),
                termsAndConditions: ko.observable(getTranslation('purchaseOrder.termsAndConditions')),
                contractValue: ko.observable(getTranslation('purchaseOrder.contractValue')),
                supplierAlternativeName: ko.observable(getTranslation("purchaseOrder.supplierAlternativeName")),
                taxRegistrationNumber: ko.observable(getTranslation("purchaseOrder.taxRegistrationNumber")),
                createContract: ko.observable(getTranslation("purchaseOrder.createContract")),
                orderNumber: ko.observable(getTranslation("purchaseOrder.orderNumber")),
                back: ko.observable(getTranslation("others.back")),
                noContractFound: ko.observable(getTranslation("purchaseOrder.noContractFound")),
                haveOneContract: ko.observable(getTranslation("purchaseOrder.haveOneContract")),
                haveAnthorContract1: ko.observable(getTranslation("purchaseOrder.haveAnthorContract1")),
                haveAnthorContract2: ko.observable(getTranslation("purchaseOrder.haveAnthorContract2")),
                createContractLbl: ko.observable(getTranslation("purchaseOrder.createContractConfirm")),
                yes: ko.observable(getTranslation("others.yes")),
                no: ko.observable(getTranslation("others.no")),
                contractType: ko.observable(getTranslation('purchaseOrder.contractType')),
                relatedPoNumber: ko.observable(getTranslation('purchaseOrder.relatedPoNumber')),
                createNewContract: ko.observable(getTranslation('purchaseOrder.createNewContract')),
                updateExisting: ko.observable(getTranslation('purchaseOrder.updateContract')),
                contractMessage: ko.observable(getTranslation('purchaseOrder.contractMessage')),
                updateContractMsg: ko.observable(getTranslation('purchaseOrder.updateContractMsg')),
                close: ko.observable(getTranslation('purchaseOrder.close')),
                cancelOrder: ko.observable(getTranslation('purchaseOrder.cancelOrder')),
                submit: ko.observable(getTranslation('purchaseOrder.submit')),
                addLinesToContract: ko.observable(getTranslation('purchaseOrder.addLinesToContract'))
            };
        }
    }

    return purchaseOrderContentViewModel;
});
