define({
    // Arabic bundle
    "common": {
        "switchLang": "English"



    },
    "login": {
        "userName": "اسم المستخدم",
        "Password": "كلمة المرور",
        "loginLabel": "تسجيل الدخول",
        "resetLabel": "إلغاء",
        "loginFailureText": "اسم مستخدم غير صحيح أو كلمة سر غير صحيحة",
        "SignOut": "تسجيل الخروج",
        "Name": "الاسم: "

    },
    "labels": {
        "changeLang": "هل انت متأكد من تغير اللغة؟",
        "signOut": "هل تود تسجيل الخروج؟",
        "confirmMessage": "رسالة تاكيد"
    },
    "others": {
        "yes": "موافق",
        "no": "رجوع",
        "back":"رجوع"
    },
    "purchaseOrder":{
           SendContract: "إنشاء عقد : ",
                procurementBU: "Procurement BU : ",
                requisitioningBU:"Requisitioning BU : ",
                legalEntity:"Sold-to Legal Entity : ",
                billToBU:"Bill-to BU : ",
                order: "امر  : ",
                status: "الحالة : ",
                fundsStatus:"حالة الأموال : ",
                buyer: "المشتري : ",
                creationDate: "تاريخ الانشاء : ",
                supplier: "المورد : ",
                site: "موقع المورد : ",
                supplierContact: "اتصال امورد : ",
                communicationMethod:"طريقة الاتصال : ",
                email:"البريد الإلكتروني : ",
                billToLocation: "فواتير إلى الموقع : ",
                shipToLocation:"الشحن الى الموقعn : ",
                ordered:"أمر : ",
                totalVat: " Vat  مجموع: ",
                total: "مجموع : ",
                description:"وصف : ",
                requisition:"طلب: ",
                sourceAgreement	:"اتفاق المصدر : ",
                supplierOrder	:"طلب المورد : ",
                requiredAcknowledgment:"طلبات المعرفة : ",
                paymentTerms:"شروط الدفع : ",
                shippingMethod:"طريقة الشحن : ",
                freightTerms:"شروط الشحن : ",
                FOB:"FOB : ",
                noteToSupplier:"ملاحظة إلى المورد : ",
                noteToReceiver:"ملاحظة إلى المتلقي: ",
                delayedPenalties:"تأخر العقوبات : ",
                chargePercentage:"نسبة الشحن: ",
                savingAmount: "القيمة الموفرة : ",
                advancedPayment: "دفع مسبق : ",
                termsAndConditions:"الأحكام والشروط : ",
                contractValue: "قيمة العقد : ",
                supplierAlternativeName:"اسم المورد البديل : ",
                taxRegistrationNumber:"رقم التسجيل الضريبي : ",
                createContract:"إنشاء العقد",
                listOfPo:"قائمة أوامر الشراء",
                viewPo:"عرض امر الشراء",
                errorMessage:"من فضلك اختر امر شراء اولا",
                errorDetails:"لايمكنك الذهاب الي صفحة امر الشراء",
                orderNumber:"أمر شراء رقم ",
                orderNumberLbl:"رقم أمر الشراء",
                contractNumber:"رقم العقد",
                noContractFound:"أمر الشراء هذا لايوجد له عقود سابقة ",
                haveOneContract:"أمر الشراء هذا له عقد مسبق برقم ",
                haveAnthorContract1:"أمر الشراء هذل له  ",
                haveAnthorContract2:"من العقود المسبقة والعقد الاخير برقم   ",
                createContractConfirm:"هل تريد إنشاء عقد أخر ؟",
                contractType:"نوع العقد : ",
                contractTypeError:"لا يحتوي امر الشراء هذا علي نوع العقد من فضلك قم بإضافته اولا ثم حاول مرة اخري.",
                relatedPoNumber:"رقم امر الشراء المرتبط بهذا : ",
                history:"History",
                close:"إغلاق",
                createNewContract:"إنشاء عقد جديد",
                updateContract:"تحديث عقد موجود",
                contractMessage:"أختر نوع العقد الذي تريد إنشائه ؟",
                updateContractMsg:"تحديث العقد",
                cancelOrder:"إلغاء العقد ",
                submit:"إرسال",
                changeOrderDescription:"تغيير وصف الطلب : ",
                addLinesToContract:"إضافة قيم جديدة للعقد"
        }

});