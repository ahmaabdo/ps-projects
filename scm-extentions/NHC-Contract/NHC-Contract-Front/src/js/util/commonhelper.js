define([], function () {

    function commonHelper() {
        var self = this;

        self.purchaseOrdersURL = 'purchaseOrder';
        self.allLineURL = 'purchaseOrder/getPurchaseOrderDetails/';
        self.allSupplierURL = 'supplier/getSupplier/';
        
         self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };

        self.getInternalRest = function () {

            var host = "http://127.0.0.1:7101/NHC-Contract-Backend-Test/rest/";
        //    var host = "https://132.145.43.71/NHC-Contract-Backend-Test/resources/";

            return host;
        };

        self.isConnected = function () {
            //show toast
            if (navigator.connection && navigator.connection.type) {
                if (navigator.connection.type == 'none') {
                    if (window.plugins && window.plugins.toast)
                        window.plugins.toast.showShortCenter('No Internet Connection!', () => {
                        }, () => {
                        });
                    return false;
                }
            }
            return true;
        };
    }

    return new commonHelper();
});