package com.appspro.nhc.job;

import com.appspro.nhc.sync.InactiveSupplier;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 *
 * @author HP
 */
public class ValueSetScheduledJob implements Job {
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        InactiveSupplier supplier = new InactiveSupplier();
        supplier.inactiveSupplier();
    }
}
