package com.appspro.nhc;

import com.appspro.nhc.biReport.BIReportService;

import com.appspro.nhc.biReport.RestHelper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;



/**
 *
 * @author HP
 */
public class RestApiCall extends RestHelper {
    
    public static JSONObject updateInactiveDateSupp(String supplierId, String addressId, String newDate)  {
       
        trustAllHosts();
        System.setProperty("https.protocols", "TLSv1.2");
        JSONObject payloadObj = new JSONObject();
        payloadObj.put("InactiveDate",newDate.toString());
       
        String restApi = "https://eghj-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/suppliers/"+supplierId+"/child/addresses/"+addressId;
        System.out.println(restApi);
        JSONObject obj =  httpPatch(restApi,payloadObj.toString());
            
        return obj;
    }
}

