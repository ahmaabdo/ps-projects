package com.appspro.nhc.sync;

import com.appspro.nhc.RestApiCall;

import com.appspro.nhc.biReport.BIReportService;

import com.appspro.nhc.biReport.RestHelper;

import java.lang.reflect.Array;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class InactiveSupplier extends RestHelper {

    public void inactiveSupplier() {

        BIReportService ser = new BIReportService();
        JSONObject suppliers = new JSONObject();
        suppliers = ser.inactiveSupplier();

        JSONArray arr = new JSONArray();

        if (suppliers.has("G_1")) {
            if (suppliers.get("G_1") instanceof JSONArray) {
                arr = suppliers.getJSONArray("G_1");
            } else {
                arr.put(suppliers.getJSONObject("G_1"));
            }
        }

        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            System.out.println(obj);
            RestApiCall rs = new RestApiCall();
            
            JSONArray arr2 = new JSONArray();
            JSONObject dffObj = new JSONObject();
            dffObj.put("inactiveStatus","updated");
            arr2.put(dffObj);
            JSONObject dffPayload = new JSONObject();
            dffPayload.put("DFF",arr2);
            
            String dffSupplierRestApi = "https://eghj-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/suppliers/"+obj.get("SUPPLIER_ID").toString();
            System.out.println(dffSupplierRestApi);

            JSONObject obj2 =  httpPatch(dffSupplierRestApi,dffPayload.toString());
            
            if(obj.has("COMMERCIAL_REGISTRATION_NUMBER")){
                System.out.println("if1");

                try {
                    String newDate = obj.get("COMMERCIAL_REGISTRATION_NUMBER").toString();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar c = Calendar.getInstance();
                    c.setTime(sdf.parse(newDate));
                    c.add(Calendar.DATE, 60); 
                    newDate = sdf.format(c.getTime());
                    System.out.println(newDate);
                    if(obj.get("G_2") instanceof JSONArray){
                        for (int j = 0 ; j < obj.getJSONArray("G_2").length() ; j++){
                            JSONObject addArr = obj.getJSONArray("G_2").getJSONObject(j);
                            JSONObject js = rs.updateInactiveDateSupp(obj.get("SUPPLIER_ID").toString(), addArr.get("PARTY_SITE_ID").toString(), newDate );
                        }
                    }
                    else{
                        JSONObject js = rs.updateInactiveDateSupp(obj.get("SUPPLIER_ID").toString(), obj.getJSONObject("G_2").get("PARTY_SITE_ID").toString(), newDate );
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
