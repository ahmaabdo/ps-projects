package com.appspro.nhc.biReport;


import org.json.JSONObject;
import org.json.XML;

public class BIReportService {
    
    public JSONObject inactiveSupplier () {
        String fresponse ="";
        String reportName = "Inactive_Supplier_Report";
        BIPReports biPReports = new BIPReports();
        JSONObject obj = new JSONObject();
        biPReports.setAttributeFormat(BIPReports.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
        biPReports.setAttributeTemplate(BIPReports.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue());
        
        biPReports.setReportAbsolutePath(reportName);
        fresponse = biPReports.executeReports();
        JSONObject xmlJSONObj = XML.toJSONObject(fresponse.toString());

        if (!xmlJSONObj.isNull("DATA_DS") && !xmlJSONObj.getJSONObject("DATA_DS").isNull("G_1"))
        {
            if (xmlJSONObj.getJSONObject("DATA_DS").opt("G_1").toString().isEmpty()) 
            {                 
                 return obj;

            } else 
            {
                obj = xmlJSONObj.getJSONObject("DATA_DS");
            }
        } else 
        {
            return obj;
        }   
        return obj;
    }
}
