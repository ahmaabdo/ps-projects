package common.restHelper;


import com.appspro.db.CommonConfigReader;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;


public class RestHelper {

    private final String biReportUrl="/xmlpserver/services/PublicReportService";
    private final String SecurityService="/xmlpserver/services/v2/SecurityService";
    private final String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";
    public  final String protocol = "https";
    private final String departmentsServiceUrl ="/hcmRestApi/resources/11.13.18.05/departmentsLov";
    private final String currenciesListURL ="/fscmRestApi/resources/11.13.18.05/currenciesLOV";
    
    public String getSchema_Name(){
        String schemaName="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        schemaName= CommonConfigReader.getValue("Test_Schema_Name");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        schemaName= CommonConfigReader.getValue("Prod_Schema_Name");
        }
        return schemaName;
    }
    
    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getSecurityService() {
        return SecurityService;
    }

    public String getEmployeeServiceUrl() {
        return employeeServiceUrl;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getDepartmentsServiceUrl() {
        return departmentsServiceUrl;
    }

    public String getCurrenciesListURL() {
        return currenciesListURL;
    }
    
    public String getInstanceUrl(){
        String instanceUrl="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        instanceUrl= CommonConfigReader.getValue("TestInstanceUrl");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        instanceUrl= CommonConfigReader.getValue("ProdInstanceUrl");
        }
        return instanceUrl;
    }
    
    public String getInstanceName(){
        String instanceName="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        instanceName= CommonConfigReader.getValue("TestInstanceName");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        instanceName= CommonConfigReader.getValue("ProdInstanceName");
        }
        return instanceName;  
    }
    
    public String getUserName(){
        String userName="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        userName= CommonConfigReader.getValue("TEST_USER_NAME");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        userName= CommonConfigReader.getValue("PROD_USER_NAME");
        }
        return userName;
    }
    
    public String getPassword(){
        String password="";
        if ("test".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        password= CommonConfigReader.getValue("TEST_PASSWORD");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP_TYPE"))) {
        password= CommonConfigReader.getValue("PROD_PASSWORD");
        }
        return password;
    }


    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
           e.printStackTrace(); 
        }
    }

    public String callSaaS(String serverUrl) {
        String newurl = serverUrl.replaceAll(" ", "%20");
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        try {
            URL url =
                new URL(null, newurl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            //  connection.setRequestProperty("REST-Framework-Version", "3");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());
            connection.setRequestProperty("REST-Framework-Version", "1");
            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
                      while ((inputLine = in.readLine()) != null) {
                                      response.append(inputLine);
                                  }
                                  in.close();
            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
            return response.toString();

        } catch (Exception e) {
           e.printStackTrace();
        }
        return "";
    }

    public String callSaaS2(String serverURL) {
           SSLContext ctx = null;
           System.setProperty("DUseSunHttpHandler", "true");
           TrustManager[] trustAllCerts =
               new X509TrustManager[] { new X509TrustManager() {
                   public X509Certificate[] getAcceptedIssuers() {
                       return null;
                   }

                   public void checkClientTrusted(X509Certificate[] certs,
                                                  String authType) {
                   }

                public void checkServerTrusted(X509Certificate[] certs,
                                               String authType) {
                }
            } };
        try {
            ctx = SSLContext.getInstance("TLSv1.2");
            ctx.init(null, trustAllCerts, null);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
           e.printStackTrace(); 
        }

           SSLContext.setDefault(ctx);

        ClientConfig def = new DefaultClientConfig();

        def.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
                                new HTTPSProperties(null, ctx));

        Client client = Client.create(def);

        client.addFilter(new HTTPBasicAuthFilter(this.getUserName(),this.getPassword()));
        //  client.resource(serverURL).header("REST-Framework-Version", "2");

        // resource.header("REST-Framework-Version", "2");
        serverURL = serverURL.replaceAll("\\s", "%20");
        WebResource resource = client.resource(serverURL);
        resource.header("REST-Framework-Version", "2");

        return resource.accept(MediaType.APPLICATION_JSON_TYPE,
                               MediaType.APPLICATION_XML_TYPE).header("REST-Framework-Version",
                                                                      "2").get(String.class);
    }

    public Document httpPost(String destUrl,
                             String postData) throws Exception {
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response =response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);


            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();


            return doc;
        }


        return null;
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
           e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                   e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
    
    public String getAuth() {
        byte[] message = (getUserName() + ":" + getPassword()).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }


}
