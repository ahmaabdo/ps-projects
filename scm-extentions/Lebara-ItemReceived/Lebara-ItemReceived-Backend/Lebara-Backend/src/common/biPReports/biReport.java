package common.biPReports;


import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;


public class biReport extends ReportModel {


    public String getReportCall(String reportName,Map<String, String> reportParams,String reportType) {
        try {
            String jsonString =null;
            if (reportName != null) {
                    JSONObject xmlJSONObj = runReport(reportName, reportParams,reportType);
                    System.out.println("*************"+xmlJSONObj.toString());
                    if( xmlJSONObj.has("DATA_DS")){
                        JSONObject objectData = xmlJSONObj.getJSONObject("DATA_DS");
                        if(objectData.has("G_1")){
                           jsonString = objectData.get("G_1").toString().replaceAll("VALUE", "value").replaceAll("LABEL","label");
                        }else if(objectData.has("G_2")){
                           jsonString = objectData.get("G_2").toString().replaceAll("VALUE", "value").replaceAll("LABEL","label");
                        }
                        
                    }else{
                     jsonString =xmlJSONObj.toString();   
                    }
                   
                return jsonString;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
