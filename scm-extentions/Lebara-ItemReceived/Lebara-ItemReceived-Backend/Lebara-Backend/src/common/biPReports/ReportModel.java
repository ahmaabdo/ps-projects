package common.biPReports;

import com.appspro.db.AppsproConnection;
import com.appspro.db.CommonConfigReader;

import common.restHelper.RestHelper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;

import org.xml.sax.InputSource;

public class ReportModel extends RestHelper {
    private String reportName;
    private String attributeFormat;
    private String attributeTemplate;
    private String parameters;
    private Map<String, String> paramMap = new HashMap<String, String>();
    private String soapRequest = null;
    private String username = getUserName();
    private String password = getPassword();


    public static enum ATTRIBUTE_FORMAT {

        ATTRIBUTE_FORMAT_XML("xml"),
        ATTRIBUTE_FORMAT_PDF("pdf"); //keep in small caps
        private String value;

        private ATTRIBUTE_FORMAT(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum ATTRIBUTE_TEMPLATE {

        ATTRIBUTE_TEMPLATE_XML("DEFAULT"),
        ATTRIBUTE_TEMPLATE_PDF("PDF");
        private String value;

        private ATTRIBUTE_TEMPLATE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public static enum REPORT_NAME {
        EIT_REPORT("EIT Report");

        private String value;

        private REPORT_NAME(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum EIT_REPORT_PARAM {
        //        Structure_Code("Structure_Code"),
        LANG("LANG");

        private String value;

        private EIT_REPORT_PARAM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public void setParamMap(Map<String, String> paramMap) {
        this.paramMap = paramMap;
    }

    public Map<String, String> getParamMap() {
        return paramMap;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportName() {
        return reportName;
    }

    public String getParameters() {
        Iterator it = paramMap.entrySet().iterator();
        parameters = "";
        while (it.hasNext()) {
            Map.Entry parameter = (Map.Entry)it.next();
            parameters = parameters + "             <pub:item>\n" +
                    "                <pub:name>" + parameter.getKey() +
                    "</pub:name>\n" +
                    "                 <pub:values>\n" +
                    "                    <pub:item>" + parameter.getValue() +
                    "</pub:item>\n" +
                    "                  </pub:values>\n" +
                    "             </pub:item>\n";
        }
        return parameters;
    }

    public String getSoapRequest() {
        this.soapRequest =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pub=\"http://xmlns.oracle.com/oxp/service/PublicReportService\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <pub:runReport>\n" +
                "         <pub:reportRequest>\n" +
                "            <pub:attributeLocale>en-US</pub:attributeLocale>\n" +
                "            <pub:attributeTemplate>" +
                this.attributeTemplate + "</pub:attributeTemplate>\n" +
                "            <pub:attributeFormat>" + this.attributeFormat +
                "</pub:attributeFormat>\n" +
                "            <pub:reportAbsolutePath>/Custom/SAAS Extension Reports/Reports/" +
                this.reportName + ".xdo</pub:reportAbsolutePath>\n" +
                "<pub:parameterNameValues>\n" +
                " \n" +
                getParameters() + "             </pub:parameterNameValues>\n" +
                " \n" +
                "         </pub:reportRequest>\n" +
                "         <pub:userID>" + this.username + "</pub:userID>\n" +
                "         <pub:password>" + this.password +
                "</pub:password>\n" +
                "      </pub:runReport>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        return soapRequest;
    }

    public static JSONObject runReport(String reportName,Map<String, String> paramMap,String reportType) {
        ReportModel biPReports = new ReportModel();
        biPReports.setReportName(reportName);
        biPReports.setParamMap(paramMap);
        JSONObject xmlJSONObj = new JSONObject();
        if("pdf".equalsIgnoreCase(reportType)){
            biPReports.setAttributeFormat(ReportModel.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_PDF.getValue());
            biPReports.setAttributeTemplate(ReportModel.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_PDF.getValue());
            byte[] reportBytes =biPReports.executePdfReports();
           String fresponse =javax.xml.bind.DatatypeConverter.printBase64Binary(new Base64().decode(reportBytes));
            xmlJSONObj.put("report", fresponse);
        }else{
            biPReports.setAttributeFormat(ReportModel.ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue());
            biPReports.setAttributeTemplate(ReportModel.ATTRIBUTE_TEMPLATE.ATTRIBUTE_TEMPLATE_XML.getValue()); 
            String fresponse = biPReports.executeReports();
            try {
                xmlJSONObj = XML.toJSONObject(fresponse.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        System.out.println(xmlJSONObj);

        return xmlJSONObj;
    }


    public String executeReports() {
        String output = "";
        try {
            Document doc =new RestHelper().httpPost(getInstanceUrl()+getBiReportUrl(), getSoapRequest());
            System.out.println(getSoapRequest());
            doc.getDocumentElement().normalize();
            String data =
                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            
            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
                output = new String(Base64.decodeBase64(data));
            } else {
                output = data;
            }
            return output;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
    
    public byte[] executePdfReports() {
        String output = "";
        try {
            System.setProperty("DUseSunHttpHandler", "true");
            System.out.println("************* soap data *************");
            System.out.println(getSoapRequest());
            byte[] buffer = new byte[getSoapRequest().length()];
            buffer = getSoapRequest().getBytes();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            java.net.URL url =
                new URL(null, getInstanceUrl()+getBiReportUrl(), new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https =
                    (HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                //https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
            } else {
                http = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "";
            //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
            http.setRequestProperty("Content-Length",
                                    String.valueOf(b.length));
            http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);
            OutputStream out = http.getOutputStream();
            out.write(b);
            System.out.println("connection status: " + http.getResponseCode() +
                               "; connection response: " +
                               http.getResponseMessage());
            InputStream is = http.getInputStream();
            String responseXML = getStringFromInputStream(is);

            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(responseXML));

            Document doc = builder.parse(src);
            String data =
                doc.getElementsByTagName("reportBytes").item(0).getTextContent();
            if (ATTRIBUTE_FORMAT.ATTRIBUTE_FORMAT_XML.getValue().equals(attributeFormat)) {
                output = new String(Base64.decodeBase64(data));
            } else {
                output = data;
            }
            System.out.println("aaaa"+output);
        } catch (Exception e) {
           e.printStackTrace();
        }
        return output.getBytes();
    }


    public void setAttributeFormat(String attributeFormat) {
        this.attributeFormat = attributeFormat;
    }

    public String getAttributeFormat() {
        return attributeFormat;
    }

    public void setAttributeTemplate(String attributeTemplate) {
        this.attributeTemplate = attributeTemplate;
    }

    public String getAttributeTemplate() {
        return attributeTemplate;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public static void main(String[] args) {
        Map<String, String> paramMAp = new HashMap<String, String>();
        paramMAp.put("orderNumber", "300000003940957");
        runReport("getPoReport", paramMAp,"xml");
        System.out.println("Hello");
    }
}
