package com.appspro.lebara.rest;


import com.appspro.lebara.dao.AddtionalDataDAO;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;

@Path("addtionlData")
public class addtionalData {
  
    AddtionalDataDAO addtionalDao=new AddtionalDataDAO();
    
    @GET
    @Path("getDepartmentList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getDepartmentList() {
        ArrayList<JSONObject> departmentList = new ArrayList<JSONObject>();
        try {
            departmentList = addtionalDao.getDepartmentList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray(departmentList).toString();
    }
    
    @GET
    @Path("getCurrencyList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getCurrencyList() {
        ArrayList<JSONObject> currencyList = new ArrayList<JSONObject>();
        try {
            currencyList = addtionalDao.getCurrenciesList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray(currencyList).toString();
    }
    
}
