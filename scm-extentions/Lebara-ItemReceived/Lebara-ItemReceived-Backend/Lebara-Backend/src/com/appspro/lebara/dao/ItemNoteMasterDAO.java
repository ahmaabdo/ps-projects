package com.appspro.lebara.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.lebara.bean.ApprovalListBean;
import com.appspro.lebara.bean.ApprovalSetupBean;
import com.appspro.lebara.bean.ItemNoteMasterBean;
import com.appspro.lebara.bean.WorkFlowNotificationBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;
import common.biPReports.biReport;

import org.json.JSONArray;

public class ItemNoteMasterDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    AttachmentDAO attachmentDao=new AttachmentDAO();
    ItemNoteDetailsDAO detailsDao=new ItemNoteDetailsDAO();

    public String getNextItemNumber() {
        String itemNumber = null;
        String nextItemNumber = null;
        StringBuilder query = new StringBuilder();
        try {
            connection = AppsproConnection.getConnection();
            query.append("select max(ITEM_NUMBER) as  ITEM_NUMBER \n from ");
            query.append(getSchemaName());
            query.append(".XX_ITEM_NOTE_MASTER ORDER BY ID DESC ");
            ps = connection.prepareStatement(query.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                itemNumber = rs.getString("ITEM_NUMBER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        if (itemNumber != null) {
            String dayNumber =
                itemNumber.substring(itemNumber.length() - 2, itemNumber.length());
            String monthNumber = itemNumber.substring(4, 6);
            String yearNumber = itemNumber.substring(0, 4);
            System.out.println(dayNumber + "--" + monthNumber + "--" +
                               yearNumber);
            if ("31".equalsIgnoreCase(dayNumber)) {
                if ("12".equalsIgnoreCase(monthNumber)) {
                    monthNumber = "01";
                    yearNumber =
                            String.valueOf(Integer.parseInt(yearNumber) + 1);
                } else {
                    monthNumber =
                            String.valueOf(Integer.parseInt(monthNumber) + 1);
                }
                dayNumber = "01";
            } else {
                dayNumber = String.valueOf(Integer.parseInt(dayNumber) + 1);
            }
            if (dayNumber.length() < 2) {
                dayNumber = "0" + dayNumber;
            }
            if (monthNumber.length() < 2) {
                monthNumber = "0" + monthNumber;
            }
            System.out.println(dayNumber + "--" + monthNumber + "--" +
                               yearNumber);
            nextItemNumber = yearNumber + monthNumber + "0" + dayNumber;
        } else {
            Date today = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            String dayNumber ="01"; // 17
            String monthNumber =
                String.valueOf(cal.get(Calendar.MONTH) + 1); // 169
            String yearNumber = String.valueOf(cal.get(Calendar.YEAR));

            if (dayNumber.length() < 2) {
                dayNumber = "0" + dayNumber;
            }
            if (monthNumber.length() < 2) {
                monthNumber = "0" + monthNumber;
            }
            System.out.println(dayNumber + "--" + monthNumber + "--" +
                               yearNumber);
            nextItemNumber = yearNumber + monthNumber + "0" + dayNumber;

        }

        return nextItemNumber;
    }

    public ArrayList<ItemNoteMasterBean> searchItem(String body){
        JSONObject objData=new JSONObject(body);
        ArrayList<ItemNoteMasterBean> itemsList = new ArrayList<ItemNoteMasterBean>();
        ItemNoteMasterBean bean= null;
        String dateSelection="";
        String receivingDepartment=objData.has("selectedDepartment")?objData.get("selectedDepartment").toString():null;
        String itemNumber=objData.has("noteNumber")?objData.get("noteNumber").toString():null;
        String status=objData.has("SelectedStatus")?objData.get("SelectedStatus").toString():null;
        String fromDate=objData.has("fromDate")?objData.get("fromDate").toString():null;
        String toDate =objData.has("toDate") ? objData.get("toDate").toString() : null;
        String department=objData.get("department").toString().contains("Finance Department")?"":"AND EMPLOYEE_DEPARTMENT = '"+objData.get("department").toString()+"'";
        if ((fromDate != null && !fromDate.isEmpty()) &&
            (toDate != null && !toDate.isEmpty())) {

            dateSelection ="AND CREATION_DATE BETWEEN to_date('" + objData.get("fromDate").toString() +
                    "','YYYY-MM-DD')" + " AND to_date('" +objData.get("toDate").toString() + "','YYYY-MM-DD')";

        } else if (fromDate != null && !fromDate.isEmpty()) {

            dateSelection = "AND CREATION_DATE >= to_date('" + objData.get("fromDate").toString() +"','YYYY-MM-DD')";

        } else if (toDate != null && !toDate.isEmpty()) {

            dateSelection ="AND CREATION_DATE <= to_date('" + objData.get("toDate").toString() +"','YYYY-MM-DD')";

        }
        System.out.println(dateSelection);
        StringBuilder query = new StringBuilder();
        try {
            connection = AppsproConnection.getConnection();
            query.setLength(0);
            query.append("select * from ");
            query.append(getSchemaName());
            query.append(".XX_ITEM_NOTE_MASTER  where  (RECEIVED_DEPARTMENT = ? or ? is null) AND (ITEM_NUMBER = ? or ? is null) ");
            query.append(dateSelection);
            query.append(department);
            query.append(" AND (STATUS = ? or ? is null) ");
            query.append(" ORDER BY ID DESC");

            ps = connection.prepareStatement(query.toString());
            ps.setString(1, receivingDepartment);
            ps.setString(2, receivingDepartment);
            ps.setString(3, itemNumber);
            ps.setString(4, itemNumber);
            ps.setString(5, status);
            ps.setString(6, status);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new ItemNoteMasterBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setItemNumber(rs.getString("ITEM_NUMBER"));
                bean.setCreatedBy(rs.getString("CREATED_BY"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedBy(rs.getString("UPDATED_BY"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPersonId(rs.getString("PERSON_ID"));
                bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                bean.setRejectReason(rs.getString("REJECT_REASON"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                bean.setReceivingDepartment(rs.getString("RECEIVED_DEPARTMENT"));
                bean.setReceivingDepartmentLbl(rs.getString("RECEIVING_DEPARTMENT_LBL"));
                itemsList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return itemsList;
    };
    
    public ArrayList<ItemNoteMasterBean> getAllItemsByDepartment(String departmentName) {
        ArrayList<ItemNoteMasterBean> itemList = new ArrayList<ItemNoteMasterBean>();
        ItemNoteMasterBean bean = null;
        StringBuilder query = new StringBuilder();
        
        String department=departmentName.contains("Finance Department")?"":"where EMPLOYEE_DEPARTMENT = '"+departmentName+"'";
        try {
            connection = AppsproConnection.getConnection();
            query.setLength(0);
            query.append("select * from ");
            query.append(getSchemaName());
            query.append(".XX_ITEM_NOTE_MASTER   ");
            query.append(department);
            query.append(" ORDER BY ID DESC ");
            
            System.out.println(query.toString());
            ps = connection.prepareStatement(query.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ItemNoteMasterBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setItemNumber(rs.getString("ITEM_NUMBER"));
                bean.setCreatedBy(rs.getString("CREATED_BY"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedBy(rs.getString("UPDATED_BY"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPersonId(rs.getString("PERSON_ID"));
                bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                bean.setRejectReason(rs.getString("REJECT_REASON"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                bean.setReceivingDepartment(rs.getString("RECEIVED_DEPARTMENT"));
                bean.setReceivingDepartmentLbl(rs.getString("RECEIVING_DEPARTMENT_LBL"));
                bean.setFinalStatus(rs.getString("FINAL_STATUS"));
                itemList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return itemList;
    }

    public ItemNoteMasterBean getItemById(String requestId) {
        ItemNoteMasterBean bean = null;
        StringBuilder query = new StringBuilder();
        try {
            connection = AppsproConnection.getConnection();
            query.setLength(0);
            query.append("select * from ");
            query.append(getSchemaName());
            query.append(".XX_ITEM_NOTE_MASTER  where ID = ? ");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, requestId);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new ItemNoteMasterBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
                bean.setItemNumber(rs.getString("ITEM_NUMBER"));
                bean.setCreatedBy(rs.getString("CREATED_BY"));
                bean.setCreationDate(rs.getString("CREATION_DATE"));
                bean.setUpdatedBy(rs.getString("UPDATED_BY"));
                bean.setUpdatedDate(rs.getString("UPDATED_DATE"));
                bean.setStatus(rs.getString("STATUS"));
                bean.setPersonId(rs.getString("PERSON_ID"));
                bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                bean.setRejectReason(rs.getString("REJECT_REASON"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                bean.setReceivingDepartment(rs.getString("RECEIVED_DEPARTMENT"));
                bean.setReceivingDepartmentLbl(rs.getString("RECEIVING_DEPARTMENT_LBL"));
                bean.setFinalStatus(rs.getString("FINAL_STATUS"));
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ItemNoteMasterBean insertOrUpdateItems(ItemNoteMasterBean bean, String transactionType,
                                        String approvalCode) {
        StringBuilder query = new StringBuilder();
        try {
            connection = AppsproConnection.getConnection();
           
            if ("ADD".equalsIgnoreCase(transactionType)) {
                bean.setId(getMaxId(connection));
                query.setLength(0);
                query.append("insert into  ");
                query.append(getSchemaName());
                query.append(".XX_ITEM_NOTE_MASTER (ID,ITEM_NUMBER,CREATED_BY,CREATION_DATE,STATUS,PERSON_ID,PERSON_NUMBER,EMPLOYEE_DEPARTMENT,EMPLOYEE_NAME,RECEIVED_DEPARTMENT,RECEIVING_DEPARTMENT_LBL,FINAL_STATUS)");
                query.append("values(?,?,?,sysdate,?,?,?,?,?,?,?,?)");

                ps = connection.prepareStatement(query.toString());
                ps.setInt(1, bean.getId());
                ps.setString(2, bean.getItemNumber());
                ps.setString(3, bean.getPersonId());
                ps.setString(4, bean.getStatus());
                ps.setString(5, bean.getPersonId());
                ps.setString(6, bean.getPersonNumber());
                ps.setString(7, bean.getEmployeeDepartment());
                ps.setString(8, bean.getPersonName());
                ps.setString(9, bean.getReceivingDepartment());
                ps.setString(10, bean.getReceivingDepartmentLbl());
                ps.setString(11, "Open");
                ps.executeUpdate();
                detailsDao.insertOrUpdateDetails(bean.getDetailsList(), bean);
                insertApproval(bean, approvalCode,connection);      
            } else if ("EDIT".equalsIgnoreCase(transactionType)) {
                query.setLength(0);
                query.append("update  ");
                query.append(getSchemaName());
                query.append(".XX_ITEM_NOTE_MASTER set ");
                query.append("ITEM_NUMBER=NVL(?,ITEM_NUMBER),");
                query.append("UPDATED_BY=NVL(?,UPDATED_BY),");
                query.append("RECEIVED_DEPARTMENT=NVL(?,RECEIVED_DEPARTMENT),");
                query.append("RECEIVING_DEPARTMENT_LBL=NVL(?,RECEIVING_DEPARTMENT_LBL),");
                query.append("UPDATED_DATE=sysdate,STATUS=NVL(?,STATUS) , PERSON_NUMBER=NVL(?,PERSON_NUMBER) where id=?");
                ps = connection.prepareStatement(query.toString());
                ps.setString(1, bean.getItemNumber());
                ps.setString(2, bean.getPersonId());
                ps.setString(3, bean.getReceivingDepartment());
                ps.setString(4, bean.getReceivingDepartmentLbl());
                ps.setString(5,"REJECTED".equalsIgnoreCase(bean.getStatus())?"Pending Approval": 
                                "APPROVED".equalsIgnoreCase(bean.getStatus()) ?"Pending Approval":bean.getStatus());
                ps.setString(6, bean.getPersonNumber());
                ps.setInt(7, bean.getId());
                ps.executeUpdate();
                detailsDao.insertOrUpdateDetails(bean.getDetailsList(), bean);
                if("REJECTED".equalsIgnoreCase(bean.getStatus())){
                    approvalAfterReject(bean,connection);
                }
            }else if ("RMI".equalsIgnoreCase(transactionType)) {
                query.setLength(0);
                query.append("update  ");
                query.append(getSchemaName());
                query.append(".XX_ITEM_NOTE_MASTER set ");
                query.append("ITEM_NUMBER=NVL(?,ITEM_NUMBER),");
                query.append("UPDATED_BY=NVL(?,UPDATED_BY),");
                query.append("RECEIVED_DEPARTMENT=NVL(?,RECEIVED_DEPARTMENT),");
                query.append("RECEIVING_DEPARTMENT_LBL=NVL(?,RECEIVING_DEPARTMENT_LBL),");
                query.append("UPDATED_DATE=sysdate,STATUS=NVL(?,STATUS) , PERSON_NUMBER=NVL(?,PERSON_NUMBER) where id=?");
                ps = connection.prepareStatement(query.toString());
                ps.setString(1, bean.getItemNumber());
                ps.setString(2, bean.getPersonId());
                ps.setString(3, bean.getReceivingDepartment());
                ps.setString(4, bean.getReceivingDepartmentLbl());
                ps.setString(5,"Pending Approval");
                ps.setString(6, bean.getPersonNumber());
                ps.setInt(7, bean.getId());
                ps.executeUpdate();
                detailsDao.insertOrUpdateDetails(bean.getDetailsList(), bean);
                approvalForRequestMoreInfo(bean,approvalCode,connection);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    public void updateStatus(String status, String rejectReason, int id) {
        StringBuilder query = new StringBuilder();
        try {
            connection = AppsproConnection.getConnection();
            query.append("update  ");
            query.append(getSchemaName());
            query.append(".XX_ITEM_NOTE_MASTER set ");
            query.append("STATUS=NVL(?,STATUS),");
            query.append("UPDATED_DATE=sysdate,");
            query.append("REJECT_REASON=? where id=? ");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, status);
            ps.setString(2, rejectReason);
            ps.setInt(3, id);
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    
    public void updateFinalStatus(String status, int id) {
        StringBuilder query = new StringBuilder();
        try {
            connection = AppsproConnection.getConnection();
            query.append("update  ");
            query.append(getSchemaName());
            query.append(".XX_ITEM_NOTE_MASTER set ");
            query.append("FINAL_STATUS=NVL(?,FINAL_STATUS),");
            query.append("UPDATED_DATE=sysdate where id=? ");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    
    public void updateStatus(Connection connection, String status, String rejectReason, int id) {
        StringBuilder query = new StringBuilder();
        try {
            query.append("update  ");
            query.append(getSchemaName());
            query.append(".XX_ITEM_NOTE_MASTER set ");
            query.append("STATUS=NVL(?,STATUS),");
            query.append("UPDATED_DATE=sysdate,");
            query.append("REJECT_REASON=? where id=? ");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, status);
            ps.setString(2, rejectReason);
            ps.setInt(3, id);
            ps.executeUpdate();

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        }
    }
    
    public synchronized int getMaxId(Connection connection) {
          int id = 0;
          try {
              StringBuilder query=new StringBuilder();
              query.append("SELECT  ");
              query.append(getSchemaName());
              query.append(".XX_ITEM_NOTE_MASTER_SEQ.NEXTVAL AS NEXT_ID FROM dual");
              ps = connection.prepareStatement(query.toString());
              rs = ps.executeQuery();
              while (rs.next()) {
                  id = Integer.parseInt(rs.getString("NEXT_ID"));
              }
          } catch (Exception e) {
             e.printStackTrace();
          }
          return id;
      }
    
    public String getPdfReport(String body){
        
        JSONObject masterObj=new JSONObject(body);
        biReport bIReportDAO = new biReport();
        
        // call report to get details for employee and his manager
        Map<String, String> reportParam=new HashMap<String, String>();
        reportParam.put("personNumber",masterObj.get("personNumber").toString());
        reportParam.put("receivingDepartment",masterObj.get("receivingDepartmentLbl").toString());
        JSONObject jsonObj =new JSONObject(bIReportDAO.getReportCall("getEmployeeApproverReport",reportParam,"xml"));
        JSONObject cheifObj=new JSONObject(jsonObj.get("G_2").toString());
        // call report to get pdf report details
        Map<String, String> pdfReportParam=new HashMap<String, String>();
        
        JSONArray details=new JSONArray(masterObj.get("detailsArray").toString());
        
        for(int i=0 ; i<details.length() ;i++){
            JSONObject dataObj=details.getJSONObject(i);
            JSONArray attachmentArray=new JSONArray(dataObj.get("attachment").toString());
            StringBuilder attachments=new StringBuilder();
//            for(int j=0;j<attachmentArray.length();j++){
//                attachments.append(attachmentArray.getJSONObject(j).get("name").toString() );
//                attachments.append(" - ");
//            }
            reportParam.put("invoiceCurrency"+i,dataObj.get("invoiceCurrency").toString());
//            reportParam.put("poDate"+i,dataObj.get("contractOrOrderDate").toString());
//            reportParam.put("totalAmountSAR"+i,dataObj.get("totalAmountInSAR").toString());
            reportParam.put("invoiceTotalAmount"+i,dataObj.get("invoiceTotalAmount").toString());
            reportParam.put("poNumber"+i,dataObj.get("contractOrOrderNumber").toString());
            reportParam.put("invoiceNumber"+i,dataObj.get("invoiceNumber").toString());
            reportParam.put("supplierName"+i,dataObj.get("supplierName").toString());
//            reportParam.put("exchangeRate"+i,dataObj.get("exchangeRate").toString());
            reportParam.put("invoiceDate"+i,dataObj.get("invoiceDate").toString());
//            reportParam.put("attachmentName"+i,attachments.toString());  
        }
        reportParam.put("itemNumber",masterObj.get("itemNumber").toString());
        reportParam.put("requesterName",jsonObj.get("EMPLOYEE_NAME").toString());
        reportParam.put("requesterPosition",cheifObj.get("EMPLOYEE_POSITION_NAME").toString());
        reportParam.put("requestDate",masterObj.get("requestDate").toString());
        
        if("APPROVED".equalsIgnoreCase(masterObj.get("status").toString())){
            reportParam.put("approverName",jsonObj.get("CHIEF_NAME").toString());
            reportParam.put("approverPosition",cheifObj.get("CHEIF_POSITION_NAME").toString());
            reportParam.put("approveDate",masterObj.get("approveDate").toString());
        }
       
        JSONObject report=new JSONObject(bIReportDAO.getReportCall("itemReceived",reportParam,"pdf"));
        
        return report.toString();
    }
    
    public synchronized void approvalAfterReject(ItemNoteMasterBean bean,Connection connection){
       // get rejected request from approvalList
       ApprovalListDAO approvalDao = new ApprovalListDAO();
       ApprovalListBean approvalBean=approvalDao.getRejectedApprovalByServiceAndTransaction("XX_ITEM_RECEIVED",String.valueOf(bean.getId()),connection);
       // update approvalList which rejected
//       approvalDao.updateApprovalList(null,String.valueOf(bean.getId()),"2","XX_ITEM_RECEIVED","",connection);
       approvalDao.updateApprovalList(null, approvalBean.getTransActionId(), approvalBean.getStepLeval(), approvalBean.getServiceType(), "",connection);
       // re open notification to be approved
       WorkflowNotificationDAO workDao=new WorkflowNotificationDAO();
       workDao.updateWorkflownotification(approvalBean.getRoleId(), "OPEN", approvalBean.getTransActionId(), approvalBean.getServiceType(),connection);
//       workDao.deleteWorkflownotification(String.valueOf(bean.getId()),"XX_ITEM_RECEIVED",connection);
    }
    
    public synchronized void approvalForRequestMoreInfo(ItemNoteMasterBean bean,String approvalCode,Connection connection){
        System.out.println(new JSONObject(bean).toString());
        
        ArrayList<ApprovalSetupBean> approvalSetupList =new ArrayList<ApprovalSetupBean>();
        ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
        ApprovalListBean approvalBean = new ApprovalListBean();
        ApprovalListDAO approvalDao = new ApprovalListDAO();
        WorkflowNotificationDAO  workDao =new WorkflowNotificationDAO();
        WorkFlowNotificationBean workBean=new WorkFlowNotificationBean();
        boolean isInsertApprovalList = false;
        // get approval setup
        approvalSetupList =approvalSetup.getApprovalByServiceCodeAndConditionCode("XX_ITEM_RECEIVED",approvalCode,connection);
        // get max step level for approval list
        String stpLevel =approvalDao.getMaxStepLevalRMI(String.valueOf(bean.getId()),"XX_ITEM_RECEIVED",connection);
        String maxStpLvl=String.valueOf(Integer.parseInt(stpLevel)-1);
        // update approval list for this employee
        approvalDao.updateApprovalList("RESUBMIT",String.valueOf(bean.getId()),maxStpLvl,"XX_ITEM_RECEIVED",null,connection);
        
        ApprovalSetupBean ItratorBean;
        Iterator<ApprovalSetupBean> approvalSetupListIterator =approvalSetupList.iterator();
        
        // start insert into approval list
        while (approvalSetupListIterator.hasNext()) {
          ApprovalListBean  approvalBean2=new ApprovalListBean();
            ItratorBean = approvalSetupListIterator.next();
            System.out.println(new JSONObject(ItratorBean).toString());
            String stpLevel2 =approvalDao.getMaxStepLevalRMI(String.valueOf(bean.getId()),"XX_ITEM_RECEIVED",connection);
            System.out.println(stpLevel2);
            // get max step level for approval list
            approvalBean2.setStepLeval(stpLevel2);
            approvalBean2.setServiceType("XX_ITEM_RECEIVED");
            approvalBean2.setTransActionId(Integer.toString(bean.getId()));
            approvalBean2.setWorkflowId("");
            approvalBean2.setRolrType(ItratorBean.getApprovalType());
            approvalBean2.setRoleId("");
            System.out.println(new JSONObject(approvalBean2).toString());
            
                if ("EMP".equalsIgnoreCase(approvalBean2.getRolrType())) {
                    approvalBean2.setRoleId(bean.getPersonId());
                    approvalBean2.setRoleName(bean.getPersonName());
                } 
                
                else if ("LINE_MANAGER".equalsIgnoreCase(approvalBean2.getRolrType()) &&
                           bean.getLineManager() != null &&
                           !bean.getLineManager().isEmpty()) {
                    
                    approvalBean2.setRoleId(bean.getLineManager());
                    approvalBean2.setRoleName(bean.getManagerName());

                }
                
                else if ("LINE_MANAGER+1".equalsIgnoreCase(approvalBean2.getRolrType()) &&
                           bean.getManagerOfManager() !=null &&
                           !bean.getManagerOfManager().isEmpty()) {
                    
                    approvalBean2.setRoleId(bean.getManagerOfManager());
                    approvalBean2.setRoleName(bean.getManagerOfManagerName());
                }
                
                else if ("Special_Case".equalsIgnoreCase(approvalBean.getRolrType())) {
                //Call Dynamic Report Validation -- arg
                biReport bIReportDAO = new biReport();
                Map<String, String> reportParam=new HashMap<String, String>();
                reportParam.put("str", ItratorBean.getSpecialCase());
                JSONObject jsonObj =new JSONObject(bIReportDAO.getReportCall("DynamicValidationReport",reportParam,"xml"));
                approvalBean2.setRoleId(jsonObj.get("value").toString());
            }
                
                else if ("chief_officer".equalsIgnoreCase(approvalBean2.getRolrType())) {
                approvalBean2.setRoleId(bean.getApproverId());
                approvalBean2.setRoleName(bean.getApproverName());
            }
            
            approvalBean2.setNotificationType(ItratorBean.getNotificationType());
            
            if (approvalBean2.getRoleId() != null && !approvalBean2.getRoleId().isEmpty()) {
                approvalDao.insertIntoApprovalList(approvalBean2,connection);
                isInsertApprovalList = true;
            }
        }
        
        if(isInsertApprovalList){  
            workDao.updateWorkflownotification(bean.getPersonId(), "CLOSED",String.valueOf(bean.getId()), "XX_ITEM_RECEIVED",connection);
            
            // insert notification for next user
            workBean.setMsgTitle("Resubmit Items Received Note Request");
            workBean.setMsgBody("Resubmit Items Received Note / " +bean.getItemNumber());
            ApprovalListBean rolesBean =approvalDao.getRoles(Integer.toString(bean.getId()),"XX_ITEM_RECEIVED",connection);
            System.out.println("Notification Type :" +rolesBean.getNotificationType());
            if (rolesBean.getNotificationType().equals("FYI")) {
                this.updateStatus("APPROVED", "",bean.getId());
            }
            workBean.setReceiverType(rolesBean.getRolrType());
            workBean.setReceiverId(rolesBean.getRoleId());
            workBean.setType(rolesBean.getNotificationType());
            workBean.setRequestId(Integer.toString(bean.getId()));
            workBean.setStatus(rolesBean.getNotificationType().equals("FYI")?"CLOSED":"OPEN");
            workBean.setSelfType("XX_ITEM_RECEIVED");
            workBean.setPersonName(bean.getPersonName());
            workBean.setResponsePersonId(rolesBean.getRoleId());
            workDao.insertIntoWorkflow(workBean,connection);
            
        }
             
    }
    
    public WorkFlowNotificationBean requestMoreInfo(ItemNoteMasterBean bean){
        
        WorkFlowNotificationBean workBean=new WorkFlowNotificationBean();
        
        try{
        connection = AppsproConnection.getConnection();
        int maxStepLevel;
        ArrayList<ApprovalSetupBean> approvalSetupList =new ArrayList<ApprovalSetupBean>();
        ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
        ApprovalListBean approvalBean = new ApprovalListBean();
        ApprovalListDAO approvalDao = new ApprovalListDAO();
        
        WorkflowNotificationDAO  workDao =new WorkflowNotificationDAO();

        // get approval setup
        approvalSetupList =approvalSetup.getApprovalByServiceCodeAndConditionCode("XX_ITEM_RECEIVED","Default",connection);
        
        // delete all approval list which response code is null
        maxStepLevel=approvalDao.deleteApprovalList(bean.getId(), "XX_ITEM_RECEIVED",connection);
        System.out.println(new JSONObject(bean).toString());
        // insert into approvalList for the Employee to resubmit the request
        approvalBean.setStepLeval(String.valueOf(maxStepLevel));
        approvalBean.setServiceType("XX_ITEM_RECEIVED");
        approvalBean.setTransActionId(Integer.toString(bean.getId()));
        approvalBean.setWorkflowId("");
        approvalBean.setRolrType("EMP");
        approvalBean.setRoleId(bean.getPersonId());
        approvalBean.setPersonName(bean.getPersonName());
        approvalBean.setNotificationType("FYA");
        approvalBean.setRoleName(bean.getPersonName());
        approvalBean.setRejectReason(bean.getRejectReason());
        // insert into approval list
        approvalDao.insertIntoApprovalList(approvalBean,connection);
        
        // delete pending notification first
        workDao.deleteWorkflownotification(String.valueOf(bean.getId()),"XX_ITEM_RECEIVED",connection);
        workBean.setMsgTitle("Resubmit Items Received Note Request");
        workBean.setMsgBody("Resubmit Items Received Note"+ " / " +bean.getItemNumber());
        workBean.setReceiverType("EMP");
        workBean.setReceiverId(bean.getPersonId());
        workBean.setType("FYA");
        workBean.setRequestId(Integer.toString(bean.getId()));
        workBean.setStatus("OPEN");
        workBean.setSelfType("XX_ITEM_RECEIVED");
        workBean.setPersonName(bean.getPersonName());
        workBean.setResponsePersonId(bean.getPersonId());
        workDao.insertIntoWorkflow(workBean,connection);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return workBean;
 
    }
    
    public synchronized void insertApproval(ItemNoteMasterBean bean, String approvalCode,Connection connection) {

        ArrayList<ApprovalSetupBean> approvalSetupList =new ArrayList<ApprovalSetupBean>();
        ApprovalSetupDAO approvalSetup = new ApprovalSetupDAO();
        ApprovalListBean approvalBean = new ApprovalListBean();
        ApprovalListDAO approvalDao = new ApprovalListDAO();
        WorkFlowNotificationBean workBean=new WorkFlowNotificationBean();
        WorkflowNotificationDAO  workDao =new WorkflowNotificationDAO();
        boolean isInsertApprovalList = false;
        // get approval setup
        approvalSetupList =approvalSetup.getApprovalByServiceCodeAndConditionCode("XX_ITEM_RECEIVED",approvalCode,connection);
        
        approvalBean.setStepLeval("0");
        approvalBean.setServiceType("XX_ITEM_RECEIVED");
        approvalBean.setTransActionId(Integer.toString(bean.getId()));
        approvalBean.setWorkflowId("");
        approvalBean.setRolrType("EMP");
        approvalBean.setRoleId(bean.getPersonId());
        approvalBean.setPersonName(bean.getPersonName());
        approvalBean.setResponseCode("SUBMIT");
        approvalBean.setNotificationType("FYI");
        approvalBean.setRoleName(bean.getPersonName());
        // insert into approval list
        approvalDao.insertIntoApprovalList(approvalBean,connection);

        approvalBean = null;
        ApprovalSetupBean ItratorBean;
        Iterator<ApprovalSetupBean> approvalSetupListIterator =approvalSetupList.iterator();
        
        // start insert into approval list
        while (approvalSetupListIterator.hasNext()) {
            approvalBean=new ApprovalListBean();
            ItratorBean = approvalSetupListIterator.next();
            // get max step level for approval list
            String stpLevel =approvalDao.getMaxStepLeval(String.valueOf(bean.getId()),"XX_ITEM_RECEIVED",connection);
            approvalBean.setStepLeval(stpLevel.equalsIgnoreCase("null") ?ItratorBean.getApprovalOrder() :stpLevel);
            approvalBean.setServiceType("XX_ITEM_RECEIVED");
            approvalBean.setTransActionId(Integer.toString(bean.getId()));
            approvalBean.setWorkflowId("");
            approvalBean.setRolrType(ItratorBean.getApprovalType());
            approvalBean.setRoleId("");
            if (approvalBean.getRoleId().isEmpty() ||
                approvalBean.getRoleId().equals(null) || 
                approvalBean.getRoleId() == "") {
                
                if (approvalBean.getRolrType().equals("EMP")) {
                    approvalBean.setRoleId(bean.getPersonId());
                    approvalBean.setRoleName(bean.getPersonName());
                } else if (approvalBean.getRolrType().equals("LINE_MANAGER") &&
                           bean.getLineManager() != null &&
                           !bean.getLineManager().isEmpty()) {
                    
                    approvalBean.setRoleId(bean.getLineManager());
                    approvalBean.setRoleName(bean.getManagerName());

                } else if (approvalBean.getRolrType().equals("LINE_MANAGER+1") &&
                           bean.getManagerOfManager() !=null &&
                           !bean.getManagerOfManager().isEmpty()) {
                    
                    approvalBean.setRoleId(bean.getManagerOfManager());
                    approvalBean.setRoleName(bean.getManagerOfManagerName());
                }
            }
            // set for special case
            if (approvalBean.getRolrType().equals("Special_Case")) {
                //Call Dynamic Report Validation -- arg
                biReport bIReportDAO = new biReport();
                Map<String, String> reportParam=new HashMap<String, String>();
                reportParam.put("str", ItratorBean.getSpecialCase());
                JSONObject jsonObj =new JSONObject(bIReportDAO.getReportCall("DynamicValidationReport",reportParam,"xml"));
                approvalBean.setRoleId(jsonObj.get("value").toString());
            }
            // set for cheif officer
            if (approvalBean.getRolrType().equalsIgnoreCase("chief_officer")) {
                approvalBean.setRoleId(bean.getApproverId());
                approvalBean.setRoleName(bean.getApproverName());
            }
            
            approvalBean.setNotificationType(ItratorBean.getNotificationType());
            
            if (approvalBean.getRoleId() != null && !approvalBean.getRoleId().isEmpty()) {
                approvalDao.insertIntoApprovalList(approvalBean,connection);
                isInsertApprovalList = true;
            }
            approvalBean = null; 
        }
        
        // insert into workflow notifications
        if (isInsertApprovalList) {
            workBean.setMsgTitle("Items Received Note Request");
            workBean.setMsgBody("Items Received Note"+ " / " +bean.getItemNumber());
            ApprovalListBean rolesBean =approvalDao.getRoles(Integer.toString(bean.getId()),"XX_ITEM_RECEIVED",connection);
            System.out.println("Notification Type :" +rolesBean.getNotificationType());
            if (rolesBean.getNotificationType().equals("FYI")) {
                this.updateStatus("APPROVED", "",bean.getId());
            }
            workBean.setReceiverType(rolesBean.getRolrType());
            workBean.setReceiverId(rolesBean.getRoleId());
            workBean.setType(rolesBean.getNotificationType());
            workBean.setRequestId(Integer.toString(bean.getId()));
            workBean.setStatus(rolesBean.getNotificationType().equals("FYI")?"CLOSED":"OPEN");
            workBean.setSelfType("XX_ITEM_RECEIVED");
            workBean.setPersonName(bean.getPersonName());
            workBean.setResponsePersonId(rolesBean.getRoleId());
            workDao.insertIntoWorkflow(workBean,connection);
        }else{
            this.updateStatus("APPROVED", "",bean.getId());
        }
    }

}
