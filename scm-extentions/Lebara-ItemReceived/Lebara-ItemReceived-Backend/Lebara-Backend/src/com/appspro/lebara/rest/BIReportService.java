package com.appspro.lebara.rest;

import com.appspro.db.AppsproConnection;

import common.biPReports.biReport;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


@Path("/report")
public class BIReportService {
    
    @POST
    @Path("/commonbireport")
    @Produces(MediaType.APPLICATION_JSON)
    public String getBiReport(String incomingData) {

        String reportName = null;
        Map<String, String> reportParams = new HashMap<String, String>();

        if (incomingData != null && !incomingData.isEmpty()) {
            // put incoming string into json object
            JSONObject reportData = new JSONObject(incomingData);
            reportName = reportData.get("reportName").toString();
            JSONObject questionMark = reportData.getJSONObject("params");
            Iterator keys = questionMark.keys();
            while (keys.hasNext()) {
                // loop to get the dynamic key
                String currentDynamicKey = (String)keys.next();
                // get the value of the dynamic key
                String currentDynamicValue =
                    questionMark.get(currentDynamicKey).toString();
                // do something here with the value...
                reportParams.put(currentDynamicKey, currentDynamicValue);
            }
            System.out.println("data params:- " + reportParams);

            String returnData =new biReport().getReportCall(reportName, reportParams,"xml");

            return returnData;
        } else {
            return "error in data check again";
        }
    }
}
