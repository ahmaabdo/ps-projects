package com.appspro.lebara.rest;

import com.appspro.lebara.bean.WorkFlowNotificationBean;

import com.appspro.lebara.dao.AddtionalDataDAO;
import com.appspro.lebara.dao.WorkflowNotificationDAO;
import common.biPReports.biReport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;


@Path("/workflowNotification")
public class WorkflowNotification {

    WorkflowNotificationDAO workflowDao = new WorkflowNotificationDAO();
    AddtionalDataDAO addtionalDao=new AddtionalDataDAO();

    @POST
    @Path("getNotificationsCount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getNotificationsCount(String body) {
        JSONObject data=new JSONObject();
        ArrayList<WorkFlowNotificationBean> notificationList =new ArrayList<WorkFlowNotificationBean>();
        Map<String, String> reportParams = new HashMap<String, String>();
        String reportData="";
        String reportName="getEmployeeApproverReport";
        try {

            JSONObject obj=new JSONObject(body);
            reportParams.put("personNumber", obj.get("personNumber").toString());
            notificationList = workflowDao.getAllNotification(body);
            reportData =new biReport().getReportCall(reportName, reportParams,"xml");
            data.put("notifications", new JSONArray(notificationList).toString());
            data.put("reportData", reportData);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
       
        return data.toString();
    }

    @POST
    @Path("getAllNotifications")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllNotifications(String body) {
        ArrayList<WorkFlowNotificationBean> notificationList =new ArrayList<WorkFlowNotificationBean>();
        try {
            
            notificationList = workflowDao.getAllNotification(body);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
       
        return new JSONArray(notificationList).toString();
    }
    @POST
    @Path("workflowAction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String worwflowApproval(String body) {
        String bean =null;
        try {
          bean = workflowDao.workflowAction(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;

    }
    
    @POST
    @Path("searchNotification")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchNotification(String body) {
        ArrayList<WorkFlowNotificationBean> notificationList =
            new ArrayList<WorkFlowNotificationBean>();
        try {
          notificationList = workflowDao.serachNotification(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray(notificationList).toString();
    }
}
