package com.appspro.lebara.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.lebara.bean.ItemNoteDetailsBean;
import com.appspro.lebara.bean.ItemNoteMasterBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class ItemNoteDetailsDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    AttachmentDAO attachmentDao=new AttachmentDAO();
    
    public ArrayList<ItemNoteDetailsBean> getItemNoteDetailsByMasterId(int masterId) {
        ArrayList<ItemNoteDetailsBean> eitsList = new ArrayList<ItemNoteDetailsBean>();
        ItemNoteDetailsBean bean = null;
        StringBuilder query = new StringBuilder();
        try {
            connection = AppsproConnection.getConnection();
            query.setLength(0);
            query.append("select * from ");
            query.append(getSchemaName());
            query.append(".XX_ITEM_NOTE_DETAILS   ");
            query.append("Where ITEM_NOTE_MASTER_ID = ");
            query.append(masterId);
            query.append(" ORDER BY ID DESC ");
            
            System.out.println(query.toString());
            ps = connection.prepareStatement(query.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ItemNoteDetailsBean();
                bean.setId(Integer.parseInt(rs.getString("ID")));
//                bean.setReceivingDepartment(rs.getString("RECEIVED_DEPARTMENT"));
                bean.setSupplierName(rs.getString("SUPPLIER_NAME"));
                bean.setContractOrOrderNumber(rs.getString("CONTRACT_PO_NUMBER"));
                bean.setContractOrOrderDate(rs.getString("CONTRACT_PO_DATE"));
                bean.setInvoiceNumber(rs.getString("INVOICE_NUMBER"));
                bean.setInvoiceDate(rs.getString("INVOICE_DATE"));
                bean.setInvoiceTotalAmount(rs.getString("INVOICE_TOTAL_AMOUNT"));
                bean.setInvoiceCurrency(rs.getString("INVOICE_CURRENCY"));
                bean.setExchangeRate(rs.getString("EXCHANGE_RATE"));
                bean.setTotalAmountInSAR(rs.getString("TOTAL_AMOUNT_SAR"));
//                bean.setReceivingDepartmentLbl(rs.getString("RECEIVING_DEPARTMENT_LBL"));
                bean.setAttachment(attachmentDao.getAttachment(connection,bean.getId(),"XX_ITEM_NOTE_DETAILS"));
                eitsList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return eitsList;
    }

    public ItemNoteMasterBean insertOrUpdateDetails(ArrayList<ItemNoteDetailsBean> detailsList,ItemNoteMasterBean masterBean) {
        StringBuilder query = new StringBuilder();
        try {
            connection = AppsproConnection.getConnection();
            
                query.setLength(0);
                query.append("delete from ");
                query.append(getSchemaName());
                query.append(".XX_ITEM_NOTE_DETAILS where ITEM_NOTE_MASTER_ID = ");
                query.append(masterBean.getId());
                ps = connection.prepareStatement(query.toString());
                ps.executeUpdate();
                attachmentDao.deleteAttachment(String.valueOf(masterBean.getId()), "XX_ITEM_NOTE_DETAILS", connection);
            
            for(int i=0 ; i<detailsList.size() ;i++){
                ItemNoteDetailsBean bean=detailsList.get(i);
                bean.setId(getMaxId(connection));
                query.setLength(0);
                query.append("insert into  ");
                query.append(getSchemaName());
                query.append(".XX_ITEM_NOTE_DETAILS (ID,ITEM_NOTE_MASTER_ID,SUPPLIER_NAME,CONTRACT_PO_NUMBER,CONTRACT_PO_DATE,INVOICE_NUMBER,INVOICE_DATE,INVOICE_TOTAL_AMOUNT,INVOICE_CURRENCY,EXCHANGE_RATE,TOTAL_AMOUNT_SAR) ");
                query.append(" values(?,?,?,?,?,?,?,?,?,?,?)");
                ps = connection.prepareStatement(query.toString());
               
                System.out.println(bean.getId());
                ps.setInt(1, bean.getId());
                ps.setInt(2, masterBean.getId());
//                ps.setString(3, bean.getReceivingDepartment());
                ps.setString(3, bean.getSupplierName());
                ps.setString(4, bean.getContractOrOrderNumber());
                ps.setString(5, bean.getContractOrOrderDate());
                ps.setString(6, bean.getInvoiceNumber());
                ps.setString(7, bean.getInvoiceDate());
                ps.setString(8, bean.getInvoiceTotalAmount());
                ps.setString(9, bean.getInvoiceCurrency());
                ps.setString(10, bean.getExchangeRate());
                ps.setString(11, bean.getTotalAmountInSAR());
//                ps.setString(13, bean.getReceivingDepartmentLbl());
                ps.executeUpdate();
                attachmentDao.insertOrUpdateAttachments(bean,"XX_ITEM_NOTE_DETAILS",masterBean.getId(),connection);  
            }
 
           
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return masterBean;
    }
    
    public synchronized int getMaxId(Connection connection) {
          int id = 0;
          try {
              StringBuilder query=new StringBuilder();
              query.append("SELECT  ");
              query.append(getSchemaName());
              query.append(".XX_ITEM_NOTE_DETAILS_SEQ.NEXTVAL AS NEXT_ID FROM dual");
              ps = connection.prepareStatement(query.toString());
              rs = ps.executeQuery();
              while (rs.next()) {
                  id = Integer.parseInt(rs.getString("NEXT_ID"));
              }
          } catch (Exception e) {
             e.printStackTrace();
          }
          return id;
      } 

}
