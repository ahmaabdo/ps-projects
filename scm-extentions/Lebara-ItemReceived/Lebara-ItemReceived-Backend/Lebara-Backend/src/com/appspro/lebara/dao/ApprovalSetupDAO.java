package com.appspro.lebara.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.lebara.bean.ApprovalSetupBean;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Eslam
 */
public class ApprovalSetupDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;

    public ApprovalSetupBean insertOrUpdateApprovalSetup(ApprovalSetupBean bean,
                                                         String transactionType) {
        try {
            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            if (transactionType.equals("ADD")) {
                query.setLength(0);
                query.append("INSERT INTO  " + getSchemaName() +
                             ".XX_APPROVALS_SETUP(");
                query.append("SERVICE_CODE,");
                query.append("APPROVAL_ORDER,");
                query.append("APPROVAL_TYPE,");
                query.append("ROLE_NAME,");
                query.append("SPECIALCASE,");
                query.append("NOTIFICATION_TYPE,");
                query.append("CREATEDBY,");
                query.append("CREATEDDATE,");
                query.append("APPROVAL_CODE,");
                query.append("OPERATION,");
                query.append("MANAGER_LEVAL)\n");
                query.append("VALUES ( ?, ?, ?, ?, ?, ?, ?,SYSDATE, ?, ?, ?)");

                ps = connection.prepareStatement(query.toString());
                ps.setString(1, bean.getServiceCode());
                ps.setString(2, bean.getApprovalOrder());
                ps.setString(3, bean.getApprovalType());
                if (bean.getApprovalType().equals("ROLES")) {
                    ps.setString(4, bean.getRoleName());
                } else if (bean.getApprovalType().equals("POSITION")) {
                    ps.setString(4, bean.getPosition());
                } else {
                    ps.setString(4, "");
                }
                ps.setString(5, bean.getSpecialCase());
                ps.setString(6, bean.getNotificationType());
                ps.setString(7, bean.getCreatedBy());
                ps.setString(8, bean.getApprovalCode());
                ps.setString(9, bean.getOperation());
                ps.setString(10, bean.getManagerLeval());
                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {
                query.setLength(0);
                query.append("UPDATE " + getSchemaName() +
                             ".XX_APPROVALS_SETUP SET ");
                query.append("SERVICE_CODE =? ,");
                query.append("APPROVAL_ORDER = ? ,");
                query.append("APPROVAL_TYPE = ? ,");
                query.append("ROLE_NAME = ? ,");
                query.append("SPECIALCASE = ? ,");
                query.append("NOTIFICATION_TYPE= ? ,");
                query.append("APPROVAL_CODE=? ,");
                query.append("OPERATION=? ,");
                query.append("MANAGER_LEVAL=? ,");
                query.append("UPDATED_BY=? ,");
                query.append("UPDATED_DATE = SYSDATE ");
                query.append("WHERE id = ? ");

                System.out.println(query.toString());

                ps = connection.prepareStatement(query.toString());
                ps.setString(1, bean.getServiceCode());
                ps.setString(2, bean.getApprovalOrder());
                ps.setString(3, bean.getApprovalType());
                if (bean.getApprovalType().equals("ROLES")) {
                    ps.setString(4, bean.getRoleName());
                } else if (bean.getApprovalType().equals("POSITION")) {
                    ps.setString(4, bean.getPosition());
                } else {
                    ps.setString(4, "");
                }
                ps.setString(5, bean.getSpecialCase());
                ps.setString(6, bean.getNotificationType());
                ps.setString(7, bean.getApprovalCode());
                ps.setString(8, bean.getOperation());
                ps.setString(9, bean.getManagerLeval());
                ps.setString(10, bean.getUpdatedBy());
                ps.setString(11, bean.getId());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public ArrayList<ApprovalSetupBean> getApprovalByServiceCode(String serviceCode) {
        ArrayList<ApprovalSetupBean> approvalSetupList =
            new ArrayList<ApprovalSetupBean>();
        ApprovalSetupBean approvalSetupObject = null;
        connection = AppsproConnection.getConnection();

        StringBuilder query = new StringBuilder();
        query.append("select  * from  " + getSchemaName());
        query.append(".XX_APPROVALS_SETUP  where SERVICE_CODE = ? ORDER BY approval_order ASC");
        try {
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, serviceCode.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                approvalSetupObject = new ApprovalSetupBean();
                approvalSetupObject.setId(rs.getString("ID"));
                approvalSetupObject.setServiceCode(rs.getString("SERVICE_CODE"));
                approvalSetupObject.setApprovalOrder(rs.getString("APPROVAL_ORDER"));
                approvalSetupObject.setApprovalType(rs.getString("APPROVAL_TYPE"));
                approvalSetupObject.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupObject.setSpecialCase(rs.getString("SPECIALCASE"));
                approvalSetupObject.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                approvalSetupObject.setCreatedBy(rs.getString("CREATEDBY"));
                approvalSetupObject.setCreationDate(rs.getString("CREATEDDATE"));
                approvalSetupObject.setApproval(rs.getString("APPROVAL_CODE"));
                approvalSetupObject.setApprovalCode(rs.getString("APPROVAL_CODE"));
                approvalSetupObject.setOperation(rs.getString("OPERATION"));
                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                approvalSetupList.add(approvalSetupObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }

    public void deleteApproval(int ID, String service_code,
                               String approval_order) {
        connection = AppsproConnection.getConnection();
        StringBuilder query = new StringBuilder();
        query.append("DELETE FROM  ");
        query.append(getSchemaName());
        query.append(".XX_APPROVALS_SETUP WHERE ID = ?");
        int store_order = Integer.parseInt(approval_order);
        int order = 0;
        try {
            ps = connection.prepareStatement(query.toString());
            ps.setInt(1, ID);
            ps.executeUpdate();

            query.setLength(0);
            query.append("SELECT ID,APPROVAL_Order FROM  ");
            query.append(getSchemaName());
            query.append(".xx_approvals_setup WHERE SERVICE_CODE = ?");

            ps = connection.prepareStatement(query.toString());
            ps.setString(1, service_code);
            rs = ps.executeQuery();

            while (rs.next()) {
                ID = rs.getInt("ID");
                order = Integer.parseInt(rs.getString("APPROVAL_Order"));
                if (order > store_order) {
                    order = store_order;
                }
                query.setLength(0);
                query.append("UPDATE  ");
                query.append(getSchemaName());
                query.append(".XX_APPROVALS_SETUP SET APPROVAL_ORDER = ? WHERE ID = ? ");;
                ps = connection.prepareStatement(query.toString());
                ps.setString(1, String.valueOf(order));
                ps.setInt(2, ID);
                ps.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }

    public int getMAxApprovalOrder(String service_code, String approvalCode) {
        connection = AppsproConnection.getConnection();
        int max_Order = 0;
        StringBuilder query = new StringBuilder();
        query.append("SELECT MAX(APPROVAL_ORDER) AS Max_Approval_Order FROM  ");
        query.append(getSchemaName());
        query.append(".XX_APPROVALS_SETUP WHERE SERVICE_CODE = ? AND APPROVAL_CODE=?");
        try {
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, service_code);
            ps.setString(2, approvalCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                max_Order = rs.getInt("Max_Approval_Order");
            }
            if (max_Order > 0) {
                max_Order = max_Order;
            } else {
                max_Order = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return max_Order;

    }


    public String getLastNotificationType(String serviceCode,
                                          String approvalCode) {
        connection = AppsproConnection.getConnection();
        StringBuilder query = new StringBuilder();
        query.append("select NOTIFICATION_TYPE from  ");
        query.append(getSchemaName());
        query.append(".XX_APPROVALS_SETUP where id=(select max(id) from  ");
        query.append(getSchemaName());
        query.append(".XX_APPROVALS_SETUP WHERE SERVICE_CODE =? And APPROVAL_CODE=?)");

        String notificationType = "";
        try {
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, serviceCode);
            ps.setString(2, approvalCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                notificationType = rs.getString("NOTIFICATION_TYPE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return notificationType;
    }

    public int getApprovalForPositionValidation(String SERVICE_CODE,
                                                String Role_Name,
                                                String Approval_Type,
                                                String Notification_Type,
                                                String approval) {
        connection = AppsproConnection.getConnection();
        int countApproval = 0;
        try {
            if ((!Role_Name.equals("")) && (!Role_Name.equals("null"))) {
                String query =
                    "SELECT * FROM  " + " " + getSchemaName() + ".XX_APPROVALS_SETUP WHERE SERVICE_CODE = ? AND ROLE_NAME = ? AND APPROVAL_TYPE = ? AND NOTIFICATION_TYPE = ? AND APPROVAL_CODE=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, SERVICE_CODE.toString());
                ps.setString(2, Role_Name.toString());
                ps.setString(3, Approval_Type.toString());
                ps.setString(4, Notification_Type.toString());
                ps.setString(5, approval.toString());
                rs = ps.executeQuery();

                while (rs.next()) {
                    countApproval++;

                }
            } else {
                String query =
                    "SELECT * FROM  " + " " + getSchemaName() + ".XX_APPROVALS_SETUP WHERE SERVICE_CODE = ? AND APPROVAL_TYPE = ? AND NOTIFICATION_TYPE = ? AND APPROVAL_CODE=?";
                ps = connection.prepareStatement(query);
                ps.setString(1, SERVICE_CODE.toString());
                ps.setString(2, Approval_Type.toString());
                ps.setString(3, Notification_Type.toString());
                ps.setString(4, approval.toString());
                rs = ps.executeQuery();
                while (rs.next()) {
                    countApproval++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return countApproval;
    }


    public ArrayList<ApprovalSetupBean> getApprovalByServiceCodeAndApprovalCode(String serviceCode,
                                                                                String approvalCode) {
        ArrayList<ApprovalSetupBean> approvalSetupList =
            new ArrayList<ApprovalSetupBean>();
        ApprovalSetupBean approvalSetupObject = null;
        connection = AppsproConnection.getConnection();

        String query =
            "select  * from  " + " " + getSchemaName() + ".XX_APPROVALS_SETUP  where SERVICE_CODE = ? AND APPROVAL_CODE =?  ORDER BY approval_order ASC";

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, serviceCode.toString());
            ps.setString(2, approvalCode.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                approvalSetupObject = new ApprovalSetupBean();
                approvalSetupObject.setId(rs.getString("ID"));
                approvalSetupObject.setServiceCode(rs.getString("SERVICE_CODE"));
                approvalSetupObject.setApprovalOrder(rs.getString("APPROVAL_ORDER"));
                approvalSetupObject.setApprovalType(rs.getString("APPROVAL_TYPE"));
                approvalSetupObject.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupObject.setSpecialCase(rs.getString("SPECIALCASE"));
                approvalSetupObject.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                approvalSetupObject.setCreatedBy(rs.getString("CREATEDBY"));
                approvalSetupObject.setCreationDate(rs.getString("CREATEDDATE"));
                approvalSetupObject.setApproval(rs.getString("APPROVAL_CODE"));
                approvalSetupObject.setOperation(rs.getString("OPERATION"));
                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                approvalSetupList.add(approvalSetupObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }


    public ArrayList<ApprovalSetupBean> getApprovalByServiceCodeAndConditionCode(String serviceCode,
                                                                                 String conditionCode,Connection connection) {
        ArrayList<ApprovalSetupBean> approvalSetupList =
            new ArrayList<ApprovalSetupBean>();
        ApprovalSetupBean approvalSetupObject = null;
//        connection = AppsproConnection.getConnection();

        StringBuilder query =new StringBuilder();
        query.append("select  * from  ");
        query.append(getSchemaName());
        query.append(".XX_APPROVALS_SETUP  where SERVICE_CODE = ? AND APPROVAL_CODE = ? ORDER BY approval_order ASC");
        try {
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, serviceCode.toString());
            ps.setString(2, conditionCode.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                approvalSetupObject = new ApprovalSetupBean();
                approvalSetupObject.setId(rs.getString("ID"));
                approvalSetupObject.setServiceCode(rs.getString("SERVICE_CODE"));
                approvalSetupObject.setApprovalOrder(rs.getString("APPROVAL_ORDER"));
                approvalSetupObject.setApprovalType(rs.getString("APPROVAL_TYPE"));
                approvalSetupObject.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupObject.setSpecialCase(rs.getString("SPECIALCASE"));
                approvalSetupObject.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                approvalSetupObject.setCreatedBy(rs.getString("CREATEDBY"));
                approvalSetupObject.setCreationDate(rs.getString("CREATEDDATE"));
                approvalSetupObject.setApproval(rs.getString("APPROVAL_CODE"));
                approvalSetupObject.setOperation(rs.getString("OPERATION"));
                approvalSetupObject.setManagerLeval(rs.getString("MANAGER_LEVAL"));
                approvalSetupList.add(approvalSetupObject);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } 
        return approvalSetupList;

    }
}

