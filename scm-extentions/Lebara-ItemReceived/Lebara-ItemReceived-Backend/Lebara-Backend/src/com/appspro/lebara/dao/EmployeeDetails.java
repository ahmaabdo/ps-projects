/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.lebara.dao;


import com.appspro.lebara.bean.EmployeeBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import common.restHelper.RestHelper;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.json.JSONObject;


public class EmployeeDetails extends RestHelper {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public EmployeeDetails() {
        super();
    }


    public EmployeeBean getEmpDetails(String username) {

        EmployeeBean emp = new EmployeeBean();


        String serverUrl =getInstanceUrl()+getEmployeeServiceUrl() + "?q=UserName=" + username;
         System.out.println(serverUrl);
        try {
            String response = new RestHelper().callSaaS(serverUrl);
            JSONObject mainData=new JSONObject(response);
            JSONObject empObj=new JSONObject(mainData.getJSONArray("items").get(0).toString());
            
            emp.setDisplayName(empObj.get("DisplayName").toString());
            emp.setFirstName(empObj.get("FirstName").toString());
            emp.setLastName(empObj.get("LastName").toString());
            emp.setEmail(empObj.get("WorkEmail").toString());
            emp.setPersonNumber(empObj.get("PersonNumber").toString());
            emp.setPersonId(empObj.get("PersonId").toString());
            
        } catch (Exception e) {
           e.printStackTrace();
        }
        return emp;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        EmployeeBean emp = new EmployeeDetails().getEmpDetails("hassan.elhassan@lebara.sa");

    }


}
