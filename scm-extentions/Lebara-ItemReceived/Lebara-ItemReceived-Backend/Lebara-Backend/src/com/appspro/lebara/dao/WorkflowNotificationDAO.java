package com.appspro.lebara.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.lebara.bean.ApprovalListBean;
import com.appspro.lebara.bean.EmployeeBean;
import com.appspro.lebara.bean.WorkFlowNotificationBean;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.json.JSONObject;
/**
 *
 * @author Eslam
 */
public class WorkflowNotificationDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    EmployeeDetails employeeDetails = new EmployeeDetails();
    EmployeeBean emp = null;

    public WorkFlowNotificationBean insertIntoWorkflow(WorkFlowNotificationBean bean,Connection connection) {
        try {
//            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append(" insert into  ");
            query.append(getSchemaName());
            query.append(".XX_WORKFLOW_NOTIFICATION (MSG_TITLE,MSG_BODY,CREATION_DATE,RECEIVER_TYPE,RECEIVER_ID,RESPONSE_PERSON_ID,TYPE,RESPONSE_DATE,WORKFLOW_ID,REQUEST_ID,STATUS,SELF_TYPE,PERSON_NAME,NATIONALIDENTITY) ");
            query.append("VALUES(?,?,SYSDATE,?,?,?,?,?,?,?,?,?,?,?) ");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, bean.getMsgTitle());
            ps.setString(2, bean.getMsgBody());
            ps.setString(3, bean.getReceiverType());
            ps.setString(4, bean.getReceiverId());
            ps.setString(5, bean.getResponsePersonId());
            ps.setString(6, bean.getType());
            ps.setString(7, bean.getResponseDate());
            ps.setString(8, bean.getWorkflowId());
            ps.setString(9, bean.getRequestId());
            ps.setString(10, bean.getStatus());
            ps.setString(11, bean.getSelfType());
            ps.setString(12, bean.getPersonName());
            ps.setString(13, bean.getNationalIdentity());
            ps.executeUpdate();
 
        } catch (Exception e) {
           e.printStackTrace();
        }
        return bean;
    }
    
    public ArrayList getAllNotification(String body) {

            JSONObject objData=new JSONObject(body);
            String MANAGER_ID=objData.has("managerId")? objData.get("managerId").toString() :null ;
            String managerOfManager=objData.has("managerOfManager")? objData.get("managerOfManager").toString(): null;
            String emp=objData.has("personId")?objData.get("personId").toString() :null ;
          ArrayList<WorkFlowNotificationBean> notificationList = new ArrayList<WorkFlowNotificationBean>();
          try {
              connection = AppsproConnection.getConnection();
              
              StringBuilder query = new StringBuilder();
              query.append("SELECT * FROM ( SELECT NT.*,PERI.PERSON_ID,PERI.PERSON_NUMBER,PERI.STATUS AS REQUEST_STATUS  FROM ");
              query.append(getSchemaName());
              query.append(".XX_WORKFLOW_NOTIFICATION NT, ");
              query.append(getSchemaName());
              query.append(".XX_ITEM_NOTE_MASTER PERI WHERE NT.STATUS = 'OPEN' ");
              query.append("AND NT.REQUEST_ID = PERI.ID ");
              query.append("AND ((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = ?) OR  ");
              query.append("(NT.RECEIVER_TYPE = 'LINE_MANAGER+1' AND NT.RECEIVER_ID = ?) OR ");
              query.append("(NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = ?)OR ");
              query.append("(NT.RECEIVER_TYPE = 'Special_Case' AND NT.RECEIVER_ID =?) OR ");
              query.append("(NT.RECEIVER_TYPE = 'chief_officer' AND NT.RECEIVER_ID =?)) ");
              query.append("UNION SELECT NT.*,PERI.PERSON_ID,PERI.PERSON_NUMBER,PERI.STATUS AS REQUEST_STATUS FROM ");
              query.append(getSchemaName());
              query.append(".XX_WORKFLOW_NOTIFICATION NT,");
              query.append(getSchemaName());
              query.append(".XX_ITEM_NOTE_MASTER PERI  WHERE NT.STATUS = 'CLOSED' ");
              query.append("AND NT.REQUEST_ID = PERI.ID ");
              query.append("AND (NT.TYPE = 'FYI' OR( NT.TYPE = 'FYA' and NT.STATUS = 'OPEN')) ");
              query.append("AND ((NT.RECEIVER_TYPE = 'LINE_MANAGER' AND NT.RECEIVER_ID = ?) OR ");
              query.append("(NT.RECEIVER_TYPE = 'LINE_MANAGER+1' AND NT.RECEIVER_ID = ?) OR ");
              query.append("(NT.RECEIVER_TYPE = 'Special_Case' AND NT.RECEIVER_ID = ?) OR ");
              query.append("(NT.RECEIVER_TYPE = 'chief_officer' AND NT.RECEIVER_ID =?) OR ");
              query.append("(NT.RECEIVER_TYPE = 'EMP' AND NT.RECEIVER_ID = ?))) ORDER BY CREATION_DATE DESC");

              ps = connection.prepareStatement(query.toString());
              ps.setString(1, MANAGER_ID);
              ps.setString(2, managerOfManager);
              ps.setString(3, emp);
              ps.setString(4, emp);
              ps.setString(5, emp);
              ps.setString(6, MANAGER_ID);
              ps.setString(7, managerOfManager);
              ps.setString(8, emp);
              ps.setString(9, emp);
              ps.setString(10, emp);
              rs = ps.executeQuery();

              while (rs.next()) {
                  WorkFlowNotificationBean bean = new WorkFlowNotificationBean();
                  
                  bean.setID(rs.getString("ID"));
                  bean.setMsgTitle(rs.getString("MSG_TITLE"));
                  bean.setMsgBody(rs.getString("MSG_BODY"));
                  bean.setCreationDate(rs.getString("CREATION_DATE"));
                  bean.setReceiverType(rs.getString("RECEIVER_TYPE"));
                  bean.setReceiverId(rs.getString("RECEIVER_ID"));
                  bean.setResponsePersonId(rs.getString("RESPONSE_PERSON_ID"));
                  bean.setType(rs.getString("TYPE"));
                  bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                  bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                  bean.setRequestId(rs.getString("REQUEST_ID"));
                  bean.setStatus(rs.getString("STATUS"));
                  bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                  bean.setSelfType(rs.getString("SELF_TYPE"));
                  bean.setPersonName(rs.getString("PERSON_NAME"));
                  
                  notificationList.add(bean);
              }

          } catch (Exception e) {
              e.printStackTrace();
          } finally {
              closeResources(connection, ps, rs);
          }
          return notificationList;
      }
    
    public void updateWorkflownotification(String PERSON_ID, String STATUS,
                                              String TRS_ID, String ssType,Connection connection) {
           try {
//               connection = AppsproConnection.getConnection();
               StringBuilder query = new StringBuilder();
               query.append("UPDATE  ");
               query.append(getSchemaName());
               query.append(".XX_WORKFLOW_NOTIFICATION SET RESPONSE_PERSON_ID = ?, RESPONSE_DATE = SYSDATE,");
               query.append("STATUS = ? WHERE REQUEST_ID = ? AND SELF_TYPE = ?");

               ps = connection.prepareStatement(query.toString());
               ps.setString(1, PERSON_ID);
               ps.setString(2, STATUS);
               ps.setString(3, TRS_ID);
               ps.setString(4, ssType);
               ps.executeUpdate();
               
           } catch (Exception e) {
              e.printStackTrace();
           }
       }
    
    public void deleteWorkflownotification(String requestId, String selfType,Connection connection) {
           try {
//               connection = AppsproConnection.getConnection();
               StringBuilder query = new StringBuilder();
               query.append("DELETE from  ");
               query.append(getSchemaName());
               query.append(".XX_WORKFLOW_NOTIFICATION where REQUEST_ID=? and SELF_TYPE=?");

               ps = connection.prepareStatement(query.toString());
               ps.setString(1, requestId);
               ps.setString(2, selfType);
               ps.executeUpdate();
               
           } catch (Exception e) {
              e.printStackTrace();
           }
       }
    public String workflowAction(String action) {
            ItemNoteMasterDAO itemDao=new ItemNoteMasterDAO();
            JSONObject jsonObj = new JSONObject(action);
            String v_check = "YES";
            boolean v_check2 = true;
            String V_STEP_LEVEL;
            ApprovalListBean approvalListBean = new ApprovalListBean();
            ApprovalListDAO approvalDao = new ApprovalListDAO();
            try {
                connection = AppsproConnection.getConnection();
                
                V_STEP_LEVEL =approvalDao.getMinStepLeval(jsonObj.getString("TRS_ID"), jsonObj.getString("ssType"),connection);
                
                approvalListBean =approvalDao.getNextApproval(jsonObj.getString("TRS_ID"), V_STEP_LEVEL,jsonObj.getString("ssType"),connection);

                approvalDao.updateApprovalList(jsonObj.getString("RESPONSE_CODE"),
                                               jsonObj.getString("TRS_ID"),
                                               V_STEP_LEVEL,
                                               jsonObj.getString("ssType"),
                                               jsonObj.has("rejectReason")?jsonObj.getString("rejectReason"):null,connection);
                
                this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                                "CLOSED",
                                                jsonObj.getString("TRS_ID"),
                                                jsonObj.getString("ssType"),connection);
                
                if (jsonObj.getString("RESPONSE_CODE").equals("REJECTED")) {
                    
                   itemDao.updateStatus("REJECTED",
                                        jsonObj.getString("rejectReason"),
                                        Integer.parseInt(jsonObj.getString("TRS_ID")));
                   
                }
        
                V_STEP_LEVEL =approvalDao.getMinStepLeval(jsonObj.getString("TRS_ID"),
                                                    jsonObj.getString("ssType"),connection);
                approvalListBean =approvalDao.getNextApproval(jsonObj.getString("TRS_ID"),
                                                    V_STEP_LEVEL,
                                                    jsonObj.getString("ssType"),connection);

                if (approvalListBean.getId() == null) {
                    v_check = "NO";
                }
                if (!v_check.equalsIgnoreCase("NO")) {
                    if (jsonObj.getString("RESPONSE_CODE").equals("APPROVED")) {
                        if (approvalListBean.getNotificationType().equals("FYI")) {
                            while (v_check2) {
                                if (approvalListBean.getNotificationType().equals("FYI")) {
                                    WorkFlowNotificationBean wfnBean = new WorkFlowNotificationBean();
                                    wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                                    wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                                    wfnBean.setReceiverType(approvalListBean.getRolrType());
                                    wfnBean.setReceiverId(approvalListBean.getRoleId());
                                    wfnBean.setType(approvalListBean.getNotificationType());
                                    wfnBean.setRequestId(approvalListBean.getTransActionId());
                                    wfnBean.setStatus("OPEN");
                                    wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                                    wfnBean.setSelfType(approvalListBean.getServiceType());
                                    this.insertIntoWorkflow(wfnBean,connection);
                                    approvalDao.updateReqeustDate(approvalListBean.getId());
                                    approvalDao.getMinStepLeval(approvalListBean.getTransActionId(),
                                                                approvalListBean.getServiceType(),connection);
                                    approvalDao.updateApprovalList("Delivered",
                                                                   approvalListBean.getTransActionId(),
                                                                   approvalListBean.getStepLeval(),
                                                                   approvalListBean.getServiceType(),
                                                                 "",connection  );
                                    this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                                                    "CLOSED",
                                                                    jsonObj.getString("TRS_ID"),
                                                                    jsonObj.getString("ssType"),connection);
                                    V_STEP_LEVEL =
                                            approvalDao.getMinStepLeval(approvalListBean.getTransActionId(),
                                                                        approvalListBean.getServiceType(),connection);
                                    approvalListBean =
                                            approvalDao.getNextApproval(approvalListBean.getTransActionId(),
                                                                        V_STEP_LEVEL,
                                                                        approvalListBean.getServiceType(),connection);
       
                                    if (approvalListBean.getId() == null) {
                                        v_check2 = false;
                                    }
                                    
                                } else {
                                    v_check2 = false;
                                    WorkFlowNotificationBean wfnBean =
                                        new WorkFlowNotificationBean();
                                    wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                                    wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                                    wfnBean.setReceiverType(approvalListBean.getRolrType());
                                    wfnBean.setReceiverId(approvalListBean.getRoleId());
                                    wfnBean.setType(approvalListBean.getNotificationType());
                                    wfnBean.setRequestId(approvalListBean.getTransActionId());
                                    wfnBean.setStatus("OPEN");
                                    wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                                    wfnBean.setSelfType(approvalListBean.getServiceType());
                                    this.insertIntoWorkflow(wfnBean,connection);
                                    approvalDao.updateReqeustDate(approvalListBean.getId());
                                }
                            }
                        } else {
                            WorkFlowNotificationBean wfnBean =
                                new WorkFlowNotificationBean();
                            wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                            wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                            wfnBean.setReceiverType(approvalListBean.getRolrType());
                            wfnBean.setReceiverId(approvalListBean.getRoleId());
                            wfnBean.setType(approvalListBean.getNotificationType());
                            wfnBean.setRequestId(approvalListBean.getTransActionId());
                            wfnBean.setStatus("OPEN");
                            wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                            wfnBean.setSelfType(approvalListBean.getServiceType());
                            this.insertIntoWorkflow(wfnBean,connection);
                            approvalDao.updateReqeustDate(approvalListBean.getId());

                        }
                    }
                }

            } catch (Exception e) {
               e.printStackTrace();
            } finally {
                closeResources(connection, ps, rs);
            }
          return new JSONObject(approvalListBean).toString();
        }

    public ArrayList serachNotification(String body) {
          JSONObject objData=new JSONObject(body);
          String status=objData.has("status")?objData.get("status").toString():null;
          String personId=objData.has("personId")?objData.get("personId").toString():null;
          String itemNumber=objData.has("itemNumber")?objData.get("itemNumber").toString():null;
          String type=objData.has("type")?objData.get("type").toString():null;
          ArrayList<WorkFlowNotificationBean> notificationList = new ArrayList<WorkFlowNotificationBean>();
          try {
              connection = AppsproConnection.getConnection();
              
              StringBuilder query = new StringBuilder();
              query.append("SELECT * FROM ( SELECT NT.*,PERI.PERSON_ID,PERI.PERSON_NUMBER,PERI.STATUS AS REQUEST_STATUS  FROM ");
              query.append(getSchemaName());
              query.append(".XX_WORKFLOW_NOTIFICATION NT, ");
              query.append(getSchemaName());
              query.append(".XX_ITEM_NOTE_MASTER PERI WHERE (NT.STATUS = :status or :status is null) ");
              query.append("AND NT.REQUEST_ID = PERI.ID  AND NT.RECEIVER_ID = ? AND (PERI.ITEM_NUMBER = ? or ? is null)");
              query.append("AND (NT.TYPE = :type or :type is null)");
              query.append("UNION SELECT NT.*,PERI.PERSON_ID,PERI.PERSON_NUMBER,PERI.STATUS AS REQUEST_STATUS FROM ");
              query.append(getSchemaName());
              query.append(".XX_WORKFLOW_NOTIFICATION NT,");
              query.append(getSchemaName());
              query.append(".XX_ITEM_NOTE_MASTER PERI  WHERE (NT.STATUS = :status or :status is null) ");
              query.append("AND NT.REQUEST_ID = PERI.ID AND NT.RECEIVER_ID = ?");
              query.append("AND (NT.TYPE = :type or :type is null)");
              query.append(" AND (PERI.ITEM_NUMBER = ? or ? is null)) ORDER BY CREATION_DATE DESC");

              ps = connection.prepareStatement(query.toString());
              ps.setString(1, status);
              ps.setString(2, status);
              ps.setString(3, personId);
              ps.setString(4, itemNumber);
              ps.setString(5, itemNumber);
              ps.setString(6, type);
              ps.setString(7, type);
              ps.setString(8, status);
              ps.setString(9, status);
              ps.setString(10, personId);
              ps.setString(11, type);
              ps.setString(12, type);
              ps.setString(13, itemNumber);
              ps.setString(14, itemNumber);
              
              rs = ps.executeQuery();

              while (rs.next()) {
                  WorkFlowNotificationBean bean = new WorkFlowNotificationBean();
                  
                  bean.setID(rs.getString("ID"));
                  bean.setMsgTitle(rs.getString("MSG_TITLE"));
                  bean.setMsgBody(rs.getString("MSG_BODY"));
                  bean.setCreationDate(rs.getString("CREATION_DATE"));
                  bean.setReceiverType(rs.getString("RECEIVER_TYPE"));
                  bean.setReceiverId(rs.getString("RECEIVER_ID"));
                  bean.setResponsePersonId(rs.getString("RESPONSE_PERSON_ID"));
                  bean.setType(rs.getString("TYPE"));
                  bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                  bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                  bean.setRequestId(rs.getString("REQUEST_ID"));
                  bean.setStatus(rs.getString("STATUS"));
                  bean.setPersonNumber(rs.getString("PERSON_NUMBER"));
                  bean.setSelfType(rs.getString("SELF_TYPE"));
                  bean.setPersonName(rs.getString("PERSON_NAME"));
                  
                  notificationList.add(bean);
              }

          } catch (Exception e) {
              e.printStackTrace();
          } finally {
              closeResources(connection, ps, rs);
          }
          return notificationList;
      }
    
}

