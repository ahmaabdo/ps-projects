package com.appspro.lebara.rest;

import com.appspro.lebara.bean.ItemNoteDetailsBean;
import com.appspro.lebara.bean.ItemNoteMasterBean;
import com.appspro.lebara.bean.WorkFlowNotificationBean;
import com.appspro.lebara.dao.ItemNoteDetailsDAO;
import com.appspro.lebara.dao.ItemNoteMasterDAO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

@Path("ItemReceived")
public class ItemNoteReceived {

    private ItemNoteMasterDAO itemDao = new ItemNoteMasterDAO();
    private ItemNoteDetailsDAO detailsDao = new ItemNoteDetailsDAO();

    @GET
    @Path("getNextItemNumber")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getNextItemNumber() {
        String nextNumber=null;
        try {
            nextNumber = itemDao.getNextItemNumber();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nextNumber;
    }
    
    @GET
    @Path("getItemesByDepartment/{departmentName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getItemesByPersonId(@PathParam("departmentName")
        String departmentName) {
        ArrayList<ItemNoteMasterBean> peiList = new ArrayList<ItemNoteMasterBean>();
        try {
            peiList = itemDao.getAllItemsByDepartment(departmentName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray(peiList).toString();
    }

    @GET
    @Path("getItemById/{requestId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getItemById(@PathParam("requestId")
        String requestId) {
        ItemNoteMasterBean peiBean = new ItemNoteMasterBean();
        try {
            peiBean = itemDao.getItemById(requestId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(peiBean).toString();
    }

    @POST
    @Path("/createOrUpdateItem/{transactionType}/{approvalCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateItem(String body, @PathParam("transactionType")
        String transactionType,@PathParam("approvalCode") String approvalCode) {
        ItemNoteMasterBean returnedBean = new ItemNoteMasterBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ItemNoteMasterBean bean = mapper.readValue(body, ItemNoteMasterBean.class);
            returnedBean =itemDao.insertOrUpdateItems(bean, transactionType,approvalCode);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONObject(returnedBean).toString();
    }
    
    @GET
    @Path("updateStatus/{requestId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateStatus(@PathParam("requestId")
        int requestId) {
        try {
             itemDao.updateStatus("APPROVED",null,requestId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Done";
    }
    
    
    @POST
    @Path("pdfReport")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getPdfReport(String body) {
        String returnData=null;
        try {
            returnData= itemDao.getPdfReport(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnData;
    }
    
    @POST
    @Path("search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String searchItem(String body) {
        ArrayList<ItemNoteMasterBean> list=new ArrayList<ItemNoteMasterBean>();
        try {
            list= itemDao.searchItem(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray(list).toString();
    }
    
    @GET
    @Path("getDetailsByMasterId/{masterItemId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getDetailsByMasterId(@PathParam("masterItemId") int masterItemId) {
        ArrayList<ItemNoteDetailsBean> list=new ArrayList<ItemNoteDetailsBean>();
        try {
            list= detailsDao.getItemNoteDetailsByMasterId(masterItemId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray(list).toString();
    }
    
    @POST
    @Path("requestMoreInfo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String requestMoreInfo(String body) {
        WorkFlowNotificationBean notificationBean=new WorkFlowNotificationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ItemNoteMasterBean bean = mapper.readValue(body, ItemNoteMasterBean.class);
            notificationBean=itemDao.requestMoreInfo(bean);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(notificationBean).toString();
    }
    
    @GET
    @Path("updateFinalStatus/{requestId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateFinalStatus(@PathParam("requestId")
        int requestId) {
        try {
             itemDao.updateFinalStatus("Closed",requestId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Done";
    }

}
