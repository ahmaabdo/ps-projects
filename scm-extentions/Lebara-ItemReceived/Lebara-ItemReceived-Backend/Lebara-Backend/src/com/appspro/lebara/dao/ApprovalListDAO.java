package com.appspro.lebara.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.lebara.bean.ApprovalListBean;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ApprovalListDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;

    public ArrayList<ApprovalListBean> getLastStepForApproval(String transactionId,
                                                              String serviceType) {
        ArrayList<ApprovalListBean> approvalSetupList =
            new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        connection = AppsproConnection.getConnection();

        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM  ");
        query.append(getSchemaName() );
        query.append(".XX_APPROVAL_LIST WHERE transaction_id = ? AND service_type = ? AND notification_type = 'FYA'\n");
        query.append(" AND step_level = (SELECT MAX(step_level) FROM  ");
        query.append(getSchemaName());
        query.append(".XX_APPROVAL_LIST WHERE transaction_id = ? AND service_type = ? AND notification_type = 'FYA')");
        try {
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, transactionId.toString());
            ps.setString(2, serviceType.toString());
            ps.setString(3, transactionId.toString());
            ps.setString(4, serviceType.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                approvalSetupList.add(bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;

    }

    public ApprovalListBean insertIntoApprovalList(ApprovalListBean bean,Connection connection) {
        try {
//            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append( "insert into  ");
            query.append(getSchemaName());
            query.append(".XX_APPROVAL_LIST (STEP_LEVEL,SERVICE_TYPE,TRANSACTION_ID,WORKFLOW_ID,ROLE_TYPE,ROLE_ID,REQUEST_DATE,RESPONSE_CODE,NOTIFICATION_TYPE,NOTE,LINE_MANAGER_NAME,EMPLOYEE_NAME,RESPONSE_DATE,ROLE_NAME,REJECT_REASON)");
            query.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,sysDate,?,?)");
            
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, bean.getStepLeval());
            ps.setString(2, bean.getServiceType());
            ps.setString(3, bean.getTransActionId());
            ps.setString(4, bean.getWorkflowId());
            ps.setString(5, bean.getRolrType());
            ps.setString(6, bean.getRoleId());
            ps.setString(7, bean.getRequestDatt());
            ps.setString(8, bean.getResponseCode());
            ps.setString(9, bean.getNotificationType());
            ps.setString(10, bean.getNote());
            ps.setString(11, bean.getLineManagerName());
            ps.setString(12, bean.getPersonName());
            ps.setString(13, bean.getRoleName());
            ps.setString(14, bean.getRejectReason());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }

    public ApprovalListBean getRoles(String ID, String ServiceType,Connection connection) {
        ApprovalListBean bean = new ApprovalListBean();
        try {
//            connection = AppsproConnection.getConnection();
            
            StringBuilder query = new StringBuilder();
            query.append("select ROLE_TYPE, ROLE_ID , NOTIFICATION_TYPE from  ");
            query.append(getSchemaName());
            query.append(".XX_APPROVAL_LIST Where TRANSACTION_ID =? And STEP_LEVEL = 1 And SERVICE_TYPE =?");
            
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, ID);
            ps.setString(2, ServiceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        }
        return bean;
    }

    public String getMinStepLeval(String transactionId, String serviceType,Connection connection) {
        String minStepLeval = "";
        try {
//            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append("SELECT MIN(STEP_LEVEL) AS MIN_ID  FROM  ");
            query.append(getSchemaName());
            query.append(".XX_APPROVAL_LIST WHERE TRANSACTION_ID = ? AND RESPONSE_CODE IS NULL AND SERVICE_TYPE = ?");
            
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, transactionId);
            ps.setString(2, serviceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                minStepLeval = rs.getString("MIN_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return minStepLeval;
    }

    public ApprovalListBean getNextApproval(String transactionId,
                                            String minStepLeval,
                                            String serviceType,Connection connection) {
        ApprovalListBean bean = new ApprovalListBean();
        try {
//            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append("SELECT * FROM  ");
            query.append(getSchemaName());
            query.append( ".XX_APPROVAL_LIST WHERE TRANSACTION_ID = ? AND RESPONSE_CODE IS NULL AND STEP_LEVEL = ? AND SERVICE_TYPE = ?");
            
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, transactionId);
            ps.setString(2, minStepLeval);
            ps.setString(3, serviceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));

            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        }

        return bean;
    }

    public void updateApprovalList(String RESPONSE_CODE, String transactionId,
                                   String stepLeval, String serviceType,
                                   String reason,Connection connection) {
        try {
            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append("UPDATE  ");
            query.append(getSchemaName());
            query.append(".XX_APPROVAL_LIST SET  RESPONSE_DATE =SYSDATE , RESPONSE_CODE = ?   , REJECT_REASON =NVL(?,REJECT_REASON)  ");
            query.append(" WHERE TRANSACTION_ID = ? " + "AND STEP_LEVEL = ? ");
            query.append(" AND SERVICE_TYPE = ?");

            ps = connection.prepareStatement(query.toString());
            ps.setString(1, RESPONSE_CODE);
            ps.setString(2, reason);
            ps.setString(3, transactionId);
            ps.setString(4, stepLeval);
            ps.setString(5, serviceType);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ApprovalListBean> approvalType(String serviceType) {
        ArrayList<ApprovalListBean> approvalSetupList =
            new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        try {
            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append(" SELECT * FROM  ");
            query.append(getSchemaName());
            query.append(".XX_APPROVAL_LIST WHERE SERVICE_TYPE = ?");
   
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, serviceType);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }

    public ArrayList<ApprovalListBean> getApprovalByServiceAndTransaction(String serviceType,String transaction_id) {
        ArrayList<ApprovalListBean> approvalSetupList =
            new ArrayList<ApprovalListBean>();
        ApprovalListBean bean = null;
        try {
            connection = AppsproConnection.getConnection();
            
            StringBuilder query = new StringBuilder();
            query.append(" SELECT * FROM  ");
            query.append(getSchemaName() );
            query.append(".XX_APPROVAL_LIST WHERE SERVICE_TYPE = ? AND TRANSACTION_ID = ? ORDER BY ID ");
            
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, serviceType);
            ps.setString(2, transaction_id);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                bean.setRejectReason(rs.getString("REJECT_REASON"));
                bean.setRoleName(rs.getString("ROLE_NAME"));
                approvalSetupList.add(bean);
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
        return approvalSetupList;
    }
    
    public ApprovalListBean getRejectedApprovalByServiceAndTransaction(String serviceType,String transaction_id,Connection connection) {
        ApprovalListBean bean =new ApprovalListBean();
        try {
//            connection = AppsproConnection.getConnection();
            
            StringBuilder query = new StringBuilder();
            query.append(" SELECT * FROM  ");
            query.append(getSchemaName() );
            query.append(".XX_APPROVAL_LIST WHERE SERVICE_TYPE = ? AND TRANSACTION_ID = ? And RESPONSE_CODE='REJECTED' ORDER BY ID ");
            
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, serviceType);
            ps.setString(2, transaction_id);
            rs = ps.executeQuery();

            while (rs.next()) {
                bean = new ApprovalListBean();
                bean.setId(rs.getString("ID"));
                bean.setStepLeval(rs.getString("STEP_LEVEL"));
                bean.setServiceType(rs.getString("SERVICE_TYPE"));
                bean.setTransActionId(rs.getString("TRANSACTION_ID"));
                bean.setWorkflowId(rs.getString("WORKFLOW_ID"));
                bean.setRolrType(rs.getString("ROLE_TYPE"));
                bean.setRoleId(rs.getString("ROLE_ID"));
                bean.setRequestDatt(rs.getString("REQUEST_DATE"));
                bean.setResponseCode(rs.getString("RESPONSE_CODE"));
                bean.setNotificationType(rs.getString("NOTIFICATION_TYPE"));
                bean.setNote(rs.getString("NOTE"));
                bean.setResponseDate(rs.getString("RESPONSE_DATE"));
                bean.setLineManagerName(rs.getString("LINE_MANAGER_NAME"));
                bean.setPersonName(rs.getString("EMPLOYEE_NAME"));
                bean.setRejectReason(rs.getString("REJECT_REASON"));
                bean.setRoleName(rs.getString("ROLE_NAME"));
            }
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        }
        return bean;
    }

    public void updateReqeustDate(String ID) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    " UPDATE  " + " " + getSchemaName() + ".XX_APPROVAL_LIST SET REQUEST_DATE = SYSDATE WHERE ID = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, ID);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }

    public String getMaxStepLeval(String transactionId, String serviceType,Connection connection) {
        String maxStepLeval = "";
        int returned = 0;
        try {
//            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append("SELECT MAX(STEP_LEVEL) AS MAX_ID  FROM  ");
            query.append(getSchemaName());
            query.append(".XX_APPROVAL_LIST WHERE TRANSACTION_ID = ? AND RESPONSE_CODE IS NULL AND SERVICE_TYPE = ?");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, transactionId);
            ps.setString(2, serviceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("MAX_ID") == null) {
                    maxStepLeval = "1";
                } else {
                    returned = Integer.parseInt(rs.getString("MAX_ID")) + 1;
                    maxStepLeval = String.valueOf(returned);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxStepLeval;
    }
    
    public String getMaxStepLevalRMI(String transactionId, String serviceType,Connection connection) {
        String maxStepLeval = "";
        int returned = 0;
        try {
    //            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append("SELECT MAX(STEP_LEVEL) AS MAX_ID  FROM  ");
            query.append(getSchemaName());
            query.append(".XX_APPROVAL_LIST WHERE TRANSACTION_ID = ? AND SERVICE_TYPE = ?");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, transactionId);
            ps.setString(2, serviceType);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("MAX_ID") == null) {
                    maxStepLeval = "1";
                } else {
                    returned = Integer.parseInt(rs.getString("MAX_ID")) + 1;
                    maxStepLeval = String.valueOf(returned);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxStepLeval;
    }
    
    public int deleteApprovalList(int transId,String servicCode,Connection connection){
        
        int maxStepLeval=0;
        try {
//            connection = AppsproConnection.getConnection();
            StringBuilder query = new StringBuilder();
            query.append(" delete from  ");
            query.append( getSchemaName());
            query.append(".XX_APPROVAL_LIST where TRANSACTION_ID = ? and  SERVICE_TYPE = ? and RESPONSE_CODE is null");
            ps = connection.prepareStatement(query.toString());
            ps.setInt(1, transId);
            ps.setString(2, servicCode);
            ps.executeUpdate();
            
            query.setLength(0);
            query.append("SELECT MAX(STEP_LEVEL) AS MAX_ID  FROM  ");
            query.append(getSchemaName());
            query.append(".XX_APPROVAL_LIST WHERE TRANSACTION_ID = ? AND SERVICE_TYPE = ?");
            ps = connection.prepareStatement(query.toString());
            ps.setInt(1, transId);
            ps.setString(2, servicCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("MAX_ID") == null) {
                    maxStepLeval = 1;
                } else {
                   int returned = Integer.parseInt(rs.getString("MAX_ID")) + 1;
                    maxStepLeval = returned;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxStepLeval;
    }
}
