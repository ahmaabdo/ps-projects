package com.appspro.lebara.rest;

import com.appspro.lebara.bean.ApprovalSetupBean;
import com.appspro.lebara.dao.ApprovalSetupDAO;

import com.fasterxml.jackson.databind.ObjectMapper;
import common.restHelper.RestHelper;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;


/**
 *
 * @author user
 */
@Path("/approvalSetup")
public class ApprovalSetup extends RestHelper {

    ApprovalSetupDAO service = new ApprovalSetupDAO();

    @GET
    @Path("/{serviceCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllWorkNatureAllowance(@PathParam("serviceCode")
        String service_code) {
        ArrayList<ApprovalSetupBean> list = new ArrayList<ApprovalSetupBean>();
        list = service.getApprovalByServiceCode(service_code);
        return new JSONArray(list).toString();
    }
    
    @POST
    @Path("/deleteApproval/{ID}/{service_code}/{approval_Order}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void getDeleteApproval(@PathParam("ID")
        String ID, @PathParam("service_code")
        String service_code,@PathParam("approval_Order") String approval_order) {
        int IDN = Integer.parseInt(ID);
        service.deleteApproval(IDN, service_code,approval_order);
    }
    
    @GET
    @Path("/maxOrder/{serviceCode}/{approvalCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMaxApprovalOrder(@PathParam("serviceCode")
        String service_code,@PathParam("approvalCode") String approvalCode) {
        int list = 0;
        list = service.getMAxApprovalOrder(service_code,approvalCode);
        JSONObject json = new JSONObject();
        json.append("count", list);
        return Response.ok("{" + "\"count\":" + new Integer(list) + "}",MediaType.APPLICATION_JSON).build();
    }
    
    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateJob(String body, @PathParam("transactionType")
        String transactionType) {
        ApprovalSetupBean list = new ApprovalSetupBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ApprovalSetupBean bean =mapper.readValue(body, ApprovalSetupBean.class);
            list = service.insertOrUpdateApprovalSetup(bean, transactionType);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            return new JSONObject(list).toString();
        }
    }
    
    @GET
    @Path("/lastNotificationType/{serviceCode}/{approvalCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNotificationType(@PathParam("serviceCode")
        String service_code,@PathParam("approvalCode")
        String approvalCode) {
        String list = "";
        list = service.getLastNotificationType(service_code,approvalCode);
        return Response.ok("{" + "\"notificationType\":" + "\"" + list + "\"" +
                           "}", MediaType.APPLICATION_JSON).build();

    }
    
    @POST
    @Path("/vaildation/")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getJobCount(@FormParam("eid_code")
        String eid_code, @FormParam("role_name")
        String role_name, @FormParam("approval_type")
        String approval_type, @FormParam("notification_type")
        String notification_type,@FormParam("approval")
         String approval   ) {
        int count = -1;
        count =
                service.getApprovalForPositionValidation(eid_code, role_name, approval_type,
                                                         notification_type,approval);
        JSONObject obj = new JSONObject();
        obj.put("count", count);

        try {
            return new String(obj.toString().getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }

        return obj.toString();
        //generalGroup
    }

    @GET
    @Path("approvalType/{serviceCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getApprovalTypeByEIT_CODE(@PathParam("serviceCode")
        String service_code) {
        ArrayList<ApprovalSetupBean> list = new ArrayList<ApprovalSetupBean>();
        list = service.getApprovalByServiceCode(service_code);
        return new JSONArray(list).toString();
    }
    
    @GET
    @Path("approvalType/{serviceCode}/{approvalCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getApprovalTypeByEIT_CODEAndApprovalCode(@PathParam("serviceCode")
        String service_code ,@PathParam("approvalCode") String approvalCode) {
        ArrayList<ApprovalSetupBean> list = new ArrayList<ApprovalSetupBean>();
        list = service.getApprovalByServiceCodeAndApprovalCode(service_code , approvalCode);
        return new JSONArray(list).toString();
    }
    

    public static void main(String[] args) {
        ApprovalSetup obj = new ApprovalSetup();

        //(obj.getApprovalTypeByEIT_CODE("XXX_ABS_CREATE_DECREE"));
    }
}

