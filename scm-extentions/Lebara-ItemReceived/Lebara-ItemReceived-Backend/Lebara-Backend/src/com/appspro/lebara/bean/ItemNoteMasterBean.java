package com.appspro.lebara.bean;

import java.util.ArrayList;

public class ItemNoteMasterBean {

    int id;
    private String itemNumber;
    private String createdBy;
    private String creationDate;
    private String updatedBy;
    private String updatedDate;
    private String status;
    private String personId;
    private String personName;
    private String lineManager;
    private String managerName;
    private String managerOfManager;
    private String managerOfManagerName;
    private String personNumber;
    private String rejectReason;
    private String employeeDepartment;
    private String employeePosition;
    private String approverId;
    private String approverName;
    private String approverPosition;
    private ArrayList<ItemNoteDetailsBean> detailsList;
    private String receivingDepartment;
    private String receivingDepartmentLbl;
    private String finalStatus;
        
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemNumber() {
        return itemNumber;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setLineManager(String lineManager) {
        this.lineManager = lineManager;
    }

    public String getLineManager() {
        return lineManager;
    }

    public void setManagerOfManager(String managerOfManager) {
        this.managerOfManager = managerOfManager;
    }

    public String getManagerOfManager() {
        return managerOfManager;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerOfManagerName(String managerOfManagerName) {
        this.managerOfManagerName = managerOfManagerName;
    }

    public String getManagerOfManagerName() {
        return managerOfManagerName;
    }

    public void setPersonNumber(String perssonNumber) {
        this.personNumber = perssonNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }
    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getRejectReason() {
        return rejectReason;
    }
    public void setEmployeeDepartment(String employeeDepartment) {
        this.employeeDepartment = employeeDepartment;
    }

    public String getEmployeeDepartment() {
        return employeeDepartment;
    }

    public void setEmployeePosition(String employeePosition) {
        this.employeePosition = employeePosition;
    }

    public String getEmployeePosition() {
        return employeePosition;
    }

    public void setApproverId(String approverId) {
        this.approverId = approverId;
    }

    public String getApproverId() {
        return approverId;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverPosition(String approverPosition) {
        this.approverPosition = approverPosition;
    }

    public String getApproverPosition() {
        return approverPosition;
    }

    public void setDetailsList(ArrayList<ItemNoteDetailsBean> detailsList) {
        this.detailsList = detailsList;
    }

    public ArrayList<ItemNoteDetailsBean> getDetailsList() {
        return detailsList;
    }

    public void setReceivingDepartment(String receivingDepartment) {
        this.receivingDepartment = receivingDepartment;
    }

    public String getReceivingDepartment() {
        return receivingDepartment;
    }

    public void setReceivingDepartmentLbl(String receivingDepartmentLbl) {
        this.receivingDepartmentLbl = receivingDepartmentLbl;
    }

    public String getReceivingDepartmentLbl() {
        return receivingDepartmentLbl;
    }

    public void setFinalStatus(String finalStatus) {
        this.finalStatus = finalStatus;
    }

    public String getFinalStatus() {
        return finalStatus;
    }
}
