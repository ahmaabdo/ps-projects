package com.appspro.lebara.dao;

import com.appspro.db.AppsproConnection;
import com.appspro.lebara.bean.AttachmentBean;
import com.appspro.lebara.bean.ItemNoteDetailsBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class AttachmentDAO extends AppsproConnection{
   
    Connection connection;
    PreparedStatement ps;
    ResultSet rs;
    
    public synchronized void insertOrUpdateAttachments(ItemNoteDetailsBean bean,String serviceName,int masterId,Connection connection){
        StringBuilder query = new StringBuilder();
//         connection = AppsproConnection.getConnection();
        try {
           
            
            // add attachments in table
            System.out.println(new JSONObject(bean).toString());
            JSONArray array=new JSONArray(bean.getAttachment());
           
            for(int i=0 ; i< array.length() ; i++){
                System.out.println("nnnn"+array.length());
                JSONObject obj=array.getJSONObject(i);
                query.setLength(0);
                query.append("insert into ");
                query.append(getSchemaName());
                query.append(".XX_ATTACHMENTS (NAME,DATA,REQUEST_ID,SERVICE_CODE,MASTER_ID,ID) values (?,?,?,?,?,?)");
                
//                Blob blob = connection.createBlob();
//                BASE64Decoder decoder = new BASE64Decoder();
//                 byte[] decodeByte  = decoder.decodeBuffer(obj.get("data").toString());
//                blob.setBytes(1,decodeByte);
                int maxId=getMaxId(connection);
                ps = connection.prepareStatement(query.toString());
                ps.setString(1, obj.get("name").toString());
                ps.setString(2,  obj.get("data").toString());
                ps.setString(3,String.valueOf(bean.getId()));
                ps.setString(4,serviceName);
                ps.setInt(5,masterId);
                ps.setInt(6, maxId);
                ps.executeUpdate();  
            }
            
        } catch (Exception e) {
           e.printStackTrace();
        }
    } 
    
    public synchronized void deleteAttachment(String requestId,String serviceName,Connection connection){
     
        try {
            StringBuilder query=new StringBuilder();
            // delete from attachments table 
                query.append("delete from  ");
                query.append(getSchemaName());
                query.append(".XX_ATTACHMENTS where MASTER_ID= ? and SERVICE_CODE = ? ");
                ps = connection.prepareStatement(query.toString());
                ps.setString(1,requestId);
                ps.setString(2, serviceName);
                ps.executeUpdate();  
        } catch (Exception e) {
           e.printStackTrace();
        }
    } 
    
    public String getAttachment(Connection connection,int requestId,String serviceName){
        ArrayList<AttachmentBean> attachmentList = new ArrayList<AttachmentBean>();
        try {
            StringBuilder query=new StringBuilder();
            query.append("SELECT * from ");
            query.append(getSchemaName());
            query.append(".XX_ATTACHMENTS where REQUEST_ID= ? and SERVICE_CODE = ? ");
            ps = connection.prepareStatement(query.toString());
            ps.setString(1, String.valueOf(requestId));
            ps.setString(2, serviceName);
            rs = ps.executeQuery();
            while (rs.next()) {
                AttachmentBean bean=new AttachmentBean();
                bean.setId(rs.getInt("ID"));
                bean.setName(rs.getString("NAME"));
                bean.setData(rs.getString("DATA"));
                attachmentList.add(bean);
            }
        } catch (Exception e) {
           e.printStackTrace();
        }
        return new JSONArray(attachmentList).toString();
    }
    
    public synchronized int getMaxId(Connection connection) {
          int id = 0;
          try {
              StringBuilder query=new StringBuilder();
              query.append("SELECT  ");
              query.append(getSchemaName());
              query.append(".XX_ATTACHMENTS_SEQ.NEXTVAL AS NEXT_ID FROM dual");
              ps = connection.prepareStatement(query.toString());
              rs = ps.executeQuery();
              while (rs.next()) {
                  id = Integer.parseInt(rs.getString("NEXT_ID"));
              }
          } catch (Exception e) {
             e.printStackTrace();
          }
          return id;
      } 
}
