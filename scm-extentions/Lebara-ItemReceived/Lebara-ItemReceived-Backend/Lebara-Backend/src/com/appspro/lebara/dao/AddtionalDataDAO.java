package com.appspro.lebara.dao;

import common.restHelper.RestHelper;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;


public class AddtionalDataDAO extends RestHelper{

    public ArrayList<JSONObject> getDepartmentList() {

        ArrayList<JSONObject> departmentList = new ArrayList<JSONObject>();
        int offset = 0;
        int limit = 100;
        boolean hasMore = true;
        try {
            while (hasMore) {
                String serverUrl =getInstanceUrl() + getDepartmentsServiceUrl() +
                    "?limit=" + limit + "&offset=" + offset;
                String response = new RestHelper().callSaaS(serverUrl);
                JSONObject obj = new JSONObject(response.toString());
                if (obj.getBoolean("hasMore") == true) {
                    hasMore = true;
                    offset = offset + limit;
                    limit = limit + 100;
                }else{
                    hasMore=false;
                }
                if (obj.has("items")) {
                    JSONArray arr = obj.getJSONArray("items");
                    if (arr.length() > 0) {
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject object = arr.getJSONObject(i);
                            JSONObject dataObject = new JSONObject();
                            dataObject.put("label",
                                           object.get("Name").toString());
                            dataObject.put("value",
                                           object.get("OrganizationId").toString());
                            departmentList.add(dataObject);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return departmentList;
    }
    
    public ArrayList<JSONObject> getCurrenciesList() {

        ArrayList<JSONObject> currencyList = new ArrayList<JSONObject>();
        int offset = 0;
        int limit = 100;
        boolean hasMore = true;
        try {
            while (hasMore) {
                String serverUrl =getInstanceUrl() + getCurrenciesListURL() +
                    "?limit=" + limit + "&offset=" + offset;
                String response = new RestHelper().callSaaS(serverUrl);
                JSONObject obj = new JSONObject(response.toString());
                if (obj.getBoolean("hasMore") == true) {
                    hasMore = true;
                    offset = offset + limit;
                    limit = limit + 100;
                }else{
                    hasMore=false;
                }
                if (obj.has("items")) {
                    JSONArray arr = obj.getJSONArray("items");
                    if (arr.length() > 0) {
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject object = arr.getJSONObject(i);
                            JSONObject dataObject = new JSONObject();
                            dataObject.put("label", object.get("Name").toString());
                            dataObject.put("value",object.get("CurrencyCode").toString());
                            currencyList.add(dataObject);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currencyList;
    }
    
}
