package com.appspro.lebara.bean;

public class ItemNoteDetailsBean {

    int id;
//    private String receivingDepartment;
    private String supplierName;
    private String contractOrOrderNumber;
    private String contractOrOrderDate;
    private String invoiceNumber;
    private String invoiceDate;
    private String invoiceTotalAmount;
    private String invoiceCurrency;
    private String exchangeRate;
    private String totalAmountInSAR;
    private String attachment;
//    private String receivingDepartmentLbl;
        
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


//    public void setReceivingDepartment(String receivingDepartment) {
//        this.receivingDepartment = receivingDepartment;
//    }
//
//    public String getReceivingDepartment() {
//        return receivingDepartment;
//    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setContractOrOrderNumber(String contractOrOrderNumber) {
        this.contractOrOrderNumber = contractOrOrderNumber;
    }

    public String getContractOrOrderNumber() {
        return contractOrOrderNumber;
    }

    public void setContractOrOrderDate(String contractOrOrderDate) {
        this.contractOrOrderDate = contractOrOrderDate;
    }

    public String getContractOrOrderDate() {
        return contractOrOrderDate;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceTotalAmount(String invoiceTotalAmount) {
        this.invoiceTotalAmount = invoiceTotalAmount;
    }

    public String getInvoiceTotalAmount() {
        return invoiceTotalAmount;
    }

    public void setInvoiceCurrency(String invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    public String getInvoiceCurrency() {
        return invoiceCurrency;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setTotalAmountInSAR(String totalAmountInSAR) {
        this.totalAmountInSAR = totalAmountInSAR;
    }

    public String getTotalAmountInSAR() {
        return totalAmountInSAR;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachment() {
        return attachment;
    }

//    public void setReceivingDepartmentLbl(String receivingDepartmentLbl) {
//        this.receivingDepartmentLbl = receivingDepartmentLbl;
//    }
//
//    public String getReceivingDepartmentLbl() {
//        return receivingDepartmentLbl;
//    }
}
