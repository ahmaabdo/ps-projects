package com.appspro.lebara.rest;

import com.appspro.lebara.bean.ApprovalListBean;
import com.appspro.lebara.dao.ApprovalListDAO;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;


@Path("approvalList")
public class ApprovalList {

    ApprovalListDAO approvalListDao = new ApprovalListDAO();

    @GET
    @Path("approvalListByTransactionId/{service_type}/{transaction_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getapprovaltype(@PathParam("service_type")
        String service_type, @PathParam("transaction_id")
        String transaction_id) {

        ArrayList<ApprovalListBean> approvalList =
            new ArrayList<ApprovalListBean>();
        try {

            approvalList =approvalListDao.getApprovalByServiceAndTransaction(service_type, transaction_id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray(approvalList).toString();
    }
    
    @GET
       @Path("lastStep/{transactionId}/{serviceType}")
       @Consumes(MediaType.APPLICATION_JSON)
       @Produces(MediaType.APPLICATION_JSON)
       public String getAllWorkNatureAllowance(@PathParam("transactionId") String transactionId,
                                               @PathParam("serviceType") String serviceType) {
        
           ArrayList<ApprovalListBean> list = new ArrayList<ApprovalListBean>();
           
           try{
               list = approvalListDao.getLastStepForApproval(transactionId, serviceType);   
           }catch(Exception e){
               e.printStackTrace();
           }

            return  new  JSONArray(list).toString();
       }
    
}
