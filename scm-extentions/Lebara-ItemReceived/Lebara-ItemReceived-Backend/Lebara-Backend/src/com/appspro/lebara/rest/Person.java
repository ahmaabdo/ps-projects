/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.lebara.rest;


import com.appspro.db.AppsproConnection;
import com.appspro.lebara.bean.EmployeeBean;
import com.appspro.lebara.dao.EmployeeDetails;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;


/**
 *
 * @author user
 */
@Path("/emp")
public class Person {

    @GET
    @Path("/info")
    public Response getEmpDetails(@QueryParam("username2") String username2, @Context
        HttpServletRequest request) {

        EmployeeBean obj = new EmployeeBean();
        EmployeeDetails det = new EmployeeDetails();
        if (username2 != null && !username2.isEmpty()) {
                obj = det.getEmpDetails(username2);
            } else {
                return Response.ok("", MediaType.APPLICATION_JSON).build();

            }
return Response.ok(new JSONObject(obj).toString(),
                               MediaType.APPLICATION_JSON).build();
        }

    @GET
    @Path("/logout")
    public Response logoutUser(@QueryParam("username2")
        String username2, @QueryParam("token")
        String token, @QueryParam("hostURL")
        String hostURL, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        HttpSession session = request.getSession();


        session.invalidate();
        // session.removeAttribute("EmpDetails");

        String jsonInString = "";

        try {
            return Response.ok(new String(jsonInString.getBytes(), "UTF-8"),
                               MediaType.APPLICATION_JSON).build();
        } catch (UnsupportedEncodingException ex) {
            return Response.status(500).entity(ex.getMessage()).build();
        }
    }
}
