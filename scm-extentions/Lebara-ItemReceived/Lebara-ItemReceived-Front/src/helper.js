function jwt(app,services, mainData, commonhelper) {
    console.log('helper');
        var  rootViewModel = app;
        var jwt;
        var date;
        var time;
        var isJWTNotValid = false;
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var url = new URL(window.location.href);

        // check jwt existance
        if (url.searchParams.get("jwt")) {
            rootViewModel.loading(true);
            // check for the host
            if (rootViewModel.hostUrl() == null || rootViewModel.hostUrl().length == 0) {
                hosturl = url.searchParams.get("host");
                rootViewModel.hostUrl(hosturl);
            }
            // check for jwt 
            if (rootViewModel.jwt() == null || rootViewModel.jwt().length == 0) {
                jwt = url.searchParams.get("jwt");
                rootViewModel.jwt(jwt);
            }
            // check user name
            if (rootViewModel.username() == null || rootViewModel.username().length == 0) {
                // check vaildation jwt
                try {
                    jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
                    username = jwtJSON.sub;
                    isJWTExpred = Math.round(Date.now() / 1000) > jwtJSON.exp;
                    console.log('session Expired : ' + isJWTExpred);
                    //                var isHasJwt = localStorage.jwt && localStorage.jwt == jwt;
                    if (/*isHasJwt ||*/ isJWTExpred) {//used before
                        isJWTNotValid = true;
                    } else {
                        rootViewModel.username(username);
                    }

                } catch (e) {
                };
            }
            if (url.searchParams.get("lang") && !isJWTNotValid) {

                var sessionLang = url.searchParams.get("lang");
                if (sessionLang == 'ar') {
                    // rootViewModel.setLocale('ar');
                    localStorage.setItem("selectedLanguage", "ar");
                } else if (sessionLang == 'en') {
                    // rootViewModel.setLocale("en-US");
                    localStorage.setItem("selectedLanguage", "en-US");
                }
            }
            if (!isJWTNotValid) {
                // getEmployeeData(services,false,rootViewModel);
                rootViewModel.loginVaild(true);
                rootViewModel.isUserLoggedIn(true);
                // rootViewModel.disOnLogin(true);
                LogindateTime = date + ' ' + time;
                // rootViewModel.loginDateFromSass(LogindateTime);
                var payloadLoginHistory = {};
                // get device ip and store logIn info 
                $.getJSON("https://api.ipify.org/?format=json", function (e) {
                    deviceIp = e.ip;
                    payloadLoginHistory = {
                        "personNumber": username,
                        "loginDate": LogindateTime,
                        "browserAndDeviceDetails": ''/*deviceAndBrowserDetails*/,
                        "deviceIp": deviceIp
                    };
                    sessionStorage.setItem("SShrLOginHistroy", JSON.stringify(payloadLoginHistory));
                    localStorage.jwt = jwt;//save jwt into local storage to be checked next time
                });
            } else {
                rootViewModel.loginVaild(false);
                rootViewModel.isUserLoggedIn(false);
                rootViewModel.loading(false);
            }
        } 
}



