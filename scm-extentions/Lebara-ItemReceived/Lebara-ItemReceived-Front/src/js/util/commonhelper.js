define([], function () {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function () {
            var host = "http://192.168.1.118:7101/EBSMobile/rest";
            return host;
        };

        self.getPaaSHost = function () {
            var host = "https://apex-hcuk.db.em2.oraclecloudapps.com/apex/xx_selfService/";
            return host;
        };

        self.getBiReportServletPath = function () {
            var host = "report/commonbireport";
            return host;
        };



     

        self.getInternalRest = function () {
                //  var host = "http://127.0.0.1:7101/Lebara-ItemReceived-Backend-Test/rest/";
                 var host = "https://140.238.93.48/Lebara-ItemReceived-Backend-Test/resources/"
            return host;
        };

    }

    return new commonHelper();
});