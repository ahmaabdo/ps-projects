define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojarraydataprovider', 'ojs/ojknockout-keyset',
    'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojtable',
    'ojs/ojlabelvalue',],
    function (oj, ko, $, ArrayDataProvider, keySet, app, services, commonhelper) {

        function homeViewModel() {

            var self = this;
            self.dataLoaded = ko.observable(false);
            
            self.getEmployeeApprover = function () {
                var payload = {
                    'managerId': app.personDetails().managerId,
                    'managerOfManager': app.personDetails().managerOfManager,
                    'personId': app.personDetails().personId,
                    'personNumber': app.personDetails().personNumber
                };

                var success = function (data) {
                    if (typeof data !== 'undefined') {
                        var notifications = JSON.parse(data.notifications);
                        app.notiCount(notifications.length);
                        app.approverDetails(JSON.parse(data.reportData));
                        self.dataLoaded(true);
                    } else {
                        app.messagesDataProvider.push(app.createMessage("error", "cann't get approver details please login with anthor user"));
                        app.isUserLoggedIn(false);
                        app.loading(false);

                    }
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get departments data'));
                };
                // get notifications and employee approvers
                services.addGeneric('workflowNotification/getNotificationsCount', payload).then(success, fail);
            };

            getEmployeeData=function() {
                var getEmpDetails = function (data) {
                    if (data) {
                        app.personDetails(data); 
                    }
                };
            
                var failCbFn = function () {
                     app.loading(false);
                     app.createMessage('error','This user have no data try anthor one');
                     app.loginVaild(false);
                     app.isUserLoggedIn(false);
                    return;
                };
                services.getEmpDetailsAsync(app.username(), app.jwt(), app.hostUrl()).then(getEmpDetails, failCbFn);
                
            };
            getEmployeeData();

            ko.computed(function () {
                
                if (self.dataLoaded() == false && typeof app.personDetails().personNumber !== 'undefined') {
                    self.getEmployeeApprover();
                } else if (self.dataLoaded() == true && typeof app.personDetails().personNumber !== 'undefined') {
                    oj.Router.rootInstance.go('itemReceivedSummary');
                }
            });


        }

        return homeViewModel;


    })