define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojarraydataprovider', 'ojs/ojknockout-keyset',
    'appController', 'config/services', 'ojs/ojconverter-datetime', 'ojs/ojknockout', 'ojs/ojtable',
    'ojs/ojlabelvalue', 'ojs/ojdialog', 'ojs/ojdatetimepicker', 'ojs/ojselectsingle'],
    function (oj, ko, $, ArrayDataProvider, keySet, app, services, DateTimeConverter) {

        function itemReceivedSummaryViewModel() {

            var self = this;
            var getTransaltion = oj.Translations.getTranslatedString;
            self.labels = ko.observable();
            self.tableArray = ko.observableArray([]);
            self.tableSelectedRowKey = ko.observable();
            self.tableSelectedIndex = ko.observable();
            self.oprationdisabled = ko.observable(true);
            self.selectedDataTable = ko.observable();
            self.datacrad = ko.observableArray([]);
            self.editBtnDisabled = ko.observable(true);
            self.departmentList = ko.observableArray();
            self.infoBtnVisible = ko.observable(false);
            self.infoDisabled = ko.observable(true);
            self.reason=ko.observable();
            self.statusArray = ko.observableArray([
                {
                    label: 'Pending Approval',
                    value: 'Pending Approval'
                },
                {
                    label: 'Approved',
                    value: 'APPROVED'
                },
                {
                    label: 'Rejected',
                    value: 'REJECTED'
                }
            ])

            self.tableDataProvider = new ArrayDataProvider(self.tableArray, { idAttributes: 'id' });
            self.receivingDepartmentListProvider = new ArrayDataProvider(self.departmentList, { keyAttributes: 'value' });
            self.statusListProvider = new ArrayDataProvider(self.statusArray, { keyAttributes: 'value' });
            self.dateConverter = ko.observable(new DateTimeConverter.IntlDateTimeConverter({
                pattern: 'dd/MM/yyyy'
            }));

            //-------------- refresh for language --------------//
            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });
            self.getAllItemsReceived = function () {
                var success = function (data) {
                    app.loading(false);
                    self.tableArray(data);
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get data'));
                    app.loading(false);
                };
                if (typeof app.personDetails().personId !== 'undefined') {
                    app.loading(true);
                    services.getGeneric('ItemReceived/getItemesByDepartment/' + app.approverDetails().EMPLOYEE_DEPART_NAME).then(success, fail);
                }
            };
            self.getDepartmentList=  function(){
                var success = function (data) {
                    app.departmentList(data);
                };
                var fail = function () {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get department list'));
                    app.loading(false);
                };
                if (typeof app.personDetails().personId !== 'undefined') {
                    services.getGeneric('addtionlData/getDepartmentList').then(success, fail);
                }  
            }
            //------------***************----------------//

            //------------- search model ----------------//
            self.searchModel = ko.observable();
            self.searchModel = {
                selectedDepartment: ko.observable(),
                noteNumber: ko.observable(),
                fromDate: ko.observable(),
                toDate: ko.observable(),
                SelectedStatus: ko.observable(),
                department: ko.observable()
            };

            //-------------*****************----------//
            self.tableSelectionListener = function (event) {
                var data = event.detail;
                var currentRow = data.currentRow;
                if (currentRow !== null) {
                    self.tableSelectedRowKey(currentRow['rowKey']);
                    //store current index to pass it to other form when update or view
                    self.tableSelectedIndex(currentRow['rowIndex']);
                    self.oprationdisabled(false);
                    self.selectedDataTable(self.tableArray()[self.tableSelectedIndex()]);
                    console.log(self.selectedDataTable());
                    var departmentName = app.approverDetails().EMPLOYEE_DEPART_NAME;
                    console.log(departmentName.includes('Finance Department'))

                    if(self.selectedDataTable().status == 'APPROVED' &&
                    departmentName.includes('Finance Department')==false ){
                        self.editBtnDisabled(true);  
                    }else if(self.selectedDataTable().status !== 'APPROVED' &&
                    self.selectedDataTable().personId == app.personDetails().personId){
                        self.editBtnDisabled(false);
                    }

                    if (departmentName.includes('Finance Department')==true &&
                    self.selectedDataTable().personId !== app.personDetails().personId) {
                        self.infoDisabled(false);
                        self.editBtnDisabled(true);
                    } else if(self.selectedDataTable().status !== 'APPROVED' && 
                    self.selectedDataTable().personId == app.personDetails().personId){
                        self.infoDisabled(true);
                        self.editBtnDisabled(false);
                    }    
                }
            };

            //------------ buttons action ------------------------//
            self.addActionBtn = function (event) {
                app.loading(true);
                var data = { operation: 'ADD' };
                // oj.Router.rootInstance.store(data);
                localStorage.setItem('itemData', JSON.stringify(data));
                oj.Router.rootInstance.go('itemReceivedOperation');
            };

            self.editActionBtn = function (event) {
                var dataToSend = { operation: 'EDIT', data: self.selectedDataTable() };
                localStorage.setItem('itemData', JSON.stringify(dataToSend));
                // oj.Router.rootInstance.store(dataToSend);
                oj.Router.rootInstance.go('itemReceivedOperation');
                app.loading(true);
            };

            self.viewActionBtn = function (event) {
                var dataToSend = { operation: 'VIEW', data: self.selectedDataTable() };
                localStorage.setItem('itemData', JSON.stringify(dataToSend));
                // oj.Router.rootInstance.store(dataToSend);
                oj.Router.rootInstance.go('itemReceivedOperation');
                app.loading(true);
            };

            self.approvalViewActionBtn = async function (event) {
                app.loading(true);
                self.positionName = ko.observable();
                self.rejectReason = ko.observable();
                self.datacrad([]);
                var serviceCode = "XX_ITEM_RECEIVED";
                var success = function (data) {
                    app.loading(false);
                    console.log(data);
                    if (data.length != 0) {

                        for (var i = 0; i < data.length; i++) {
                            var notificationStatus, bodyCardStatus, cardStatus, rejectReason, approvalStatus;
                            // check for notification type
                            if (data[i].notificationType === "FYA") {
                                notificationStatus = 'app-type-a';
                            } else {
                                cardStatus = 'badge-secondary';
                                notificationStatus = 'app-type-i';
                                bodyCardStatus = 'app-crd-bdy-border-ntre';
                            }
                            // check for response code
                            approvalStatus = data[i].responseCode;
                            if (!data[i].responseCode) {
                                cardStatus = 'badge-warning';
                                bodyCardStatus = 'app-crd-bdy-border-pen';
                                data[i].responseDate = '';
                                approvalStatus = "PENDING";
                            } else if (data[i].responseCode === 'APPROVED') {
                                cardStatus = 'badge-success';
                                bodyCardStatus = 'app-crd-bdy-border-suc';
                            } else if (data[i].responseCode === 'REJECTED') {
                                cardStatus = 'badge-danger';
                                bodyCardStatus = 'app-crd-bdy-border-fail';
                            }else if (data[i].responseCode === 'RESUBMIT') {
                                cardStatus = 'badge-secondary';
                                bodyCardStatus = 'app-crd-bdy-border-ntre';
                            }
                            // check for reject reason
                            if (data[i].rejectReason) {
                                rejectReason = data[i].rejectReason;
                            } else {
                                rejectReason = "There is no comment";
                            }
                            // push for approval list
                            self.datacrad.push(
                                {
                                    personName: data[i].roleName,
                                    FYA: data[i].notificationType,
                                    Approved: approvalStatus,
                                    date: data[i].responseDate,
                                    status: notificationStatus,
                                    bodyStatus: bodyCardStatus,
                                    appCardStatus: cardStatus,
                                    rejectReason: rejectReason
                                }
                            );
                        }
                    } else {
                        self.datacrad.push(
                            {
                                personName: 'There is no approval list to this request',
                                FYA: '',
                                Approved: '',
                                date: '',
                                status: '',
                                bodyStatus: '',
                                appCardStatus: '',
                                rejectReason: ''
                            });
                    }
                    document.getElementById('approvalDialog').open();
                };

                var fail = function () {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get approval list'));
                };

                await services.getGenericAsync("approvalList/approvalListByTransactionId/" + serviceCode + "/" + self.selectedDataTable().id).then(success, fail);
            };

            self.moreInfoActionBtn = function () {
                document.getElementById('requestMoreInfoDialog').open();
            };

            self.closeActionBtn=function(){
                document.getElementById('requestCloseDialog').open(); 
            };

            self.approvalDialogCloseAction = function () {
                document.getElementById('approvalDialog').close();
            };

            self.searchActionBtn = function () {
                self.editBtnDisabled(true);
                self.oprationdisabled(true);
                self.infoDisabled(true);
                self.searchModel.department(app.approverDetails().EMPLOYEE_DEPART_NAME);
                app.loading(true);
                var payload = ko.toJSON(self.searchModel);
                var success = function (data) {
                    app.loading(false);
                    self.tableArray(data);
                }
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", "cann't search for any item"));
                    app.loading(false);
                }
                services.addGeneric("ItemReceived/search", payload).then(success, fail);
            };

            self.resetActionBtn = function () {
                self.editBtnDisabled(true);
                self.oprationdisabled(true);
                self.infoDisabled(true);
                self.searchModel.SelectedStatus('');
                self.searchModel.fromDate('');
                self.searchModel.toDate('');
                self.searchModel.selectedDepartment('');
                self.getAllItemsReceived();
            };

            //------------ request more info --------//
            self.requestMoreInfoActionBtn=function(){
                app.loading(true);
                document.getElementById('requestMoreInfoDialog').close();
                self.selectedDataTable().rejectReason=self.reason();
                var payload = ko.toJSON(self.selectedDataTable());
                var success = function (data) {
                    app.loading(false);
                    console.log(data);
                };

                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", "cann't make request"));
                    app.loading(false);
                };

                services.addGeneric("ItemReceived/requestMoreInfo", payload).then(success, fail);
            };

            self.requestMoreInfoDialogClose=function(){
             document.getElementById('requestMoreInfoDialog').close();
            };

            //--------- request close ------------//
            self.requestCloseActionBtn=function(){
                app.loading(true);
                document.getElementById('requestCloseDialog').close();
                var success = function (data) {
                    app.loading(false);
                    console.log(data);
                    self.getAllItemsReceived();
                };

                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", "cann't make request"));
                    app.loading(false);
                };

                services.getGeneric("ItemReceived/updateFinalStatus/"+self.selectedDataTable().id).then(fail,success);
            };

            self.requestCloseDialogClose=function(){
                document.getElementById('requestCloseDialog').close();
            };

            //--------------***********------------------//
            self.connected = function () {
                self.getAllItemsReceived();
                self.getDepartmentList();
                if (typeof app.approverDetails() !== 'undefined') {
                    var departmentName = app.approverDetails().EMPLOYEE_DEPART_NAME;
                    if (departmentName.includes('Finance Department')) {
                        self.infoBtnVisible(true);
                    } else {
                        self.infoBtnVisible(false);
                    }
                }
            };

            ko.computed(function () {
                if (self.departmentList().length <= 0) {
                    self.departmentList(app.departmentList());
                }
            });

            //--------------- translations -------------------//
            function initTranslations() {
                self.labels({
                    addLbl: ko.observable(getTransaltion("common.add")),
                    editLbl: ko.observable(getTransaltion("common.edit")),
                    viewLbl: ko.observable(getTransaltion("common.view")),
                    homeScreenLbl: ko.observable(getTransaltion("itemReceived.title")),
                    approvalLbl: ko.observable(getTransaltion("common.viewApproval")),
                    ok: ko.observable(getTransaltion("common.ok")),
                    receivingDepartment: ko.observable(getTransaltion("itemReceived.receivingDepartment")),
                    noteNumber: ko.observable(getTransaltion("itemReceived.itemNumber")),
                    fromDate: ko.observable(getTransaltion("itemReceived.fromDate")),
                    toDate: ko.observable(getTransaltion("itemReceived.toDate")),
                    statusLbl: ko.observable(getTransaltion("common.status")),
                    searchLbl: ko.observable(getTransaltion("common.search")),
                    resetLbl: ko.observable(getTransaltion("common.reset")),
                    placeHolder: ko.observable(getTransaltion("common.placeHolder")),
                    requestMoreInfo: ko.observable(getTransaltion("itemReceived.requestMoreInfo")),
                    requestMoreInfoMsg:ko.observable(getTransaltion("itemReceived.requestMoreInfoMsg")),
                    reason:ko.observable(getTransaltion("common.reason")),
                    yes:ko.observable(getTransaltion("common.yes")),
                    no:ko.observable(getTransaltion("common.no")),
                    close:ko.observable(getTransaltion("common.close")),
                    closeRequest:ko.observable(getTransaltion("itemReceived.closeRequestTitle")),
                    closeRequestMsg:ko.observable(getTransaltion("itemReceived.closeRequestMsg")),
                    columnArray: ko.observableArray([
                        {
                            "headerText": getTransaltion('itemReceived.receivingDepartment'), "field": "receivingDepartmentLbl"
                        },
                        {
                            "headerText": getTransaltion('itemReceived.itemNumber'), "field": "itemNumber"
                        },
                        {
                            "headerText": getTransaltion('itemReceived.creationDate'), "field": "creationDate"
                        },
                        // {
                        //     "headerText": getTransaltion('itemReceived.supplierName'), "field": "supplierName"
                        // },
                        {
                            "headerText": getTransaltion('common.status'), "field": "status"
                        },
                        {
                            "headerText": getTransaltion('itemReceived.finalStatus'), "field": "finalStatus"
                        },
                        // ,
                        // {
                        //     "headerText": getTransaltion('itemReceived.totalAmountInSAR'), "field": "totalAmountInSAR"
                        // }
                    ])
                });

            }
            initTranslations();
        }

        return itemReceivedSummaryViewModel;


    })