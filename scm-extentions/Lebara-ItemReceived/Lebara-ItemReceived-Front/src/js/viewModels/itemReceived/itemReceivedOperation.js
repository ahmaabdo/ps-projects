define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout-keyset',
    'appController', 'config/services', 'util/commonhelper', 'ojs/ojconverter-datetime', 
    'ojs/ojarraydataprovider','ojs/ojknockout', 'ojs/ojtable', 'ojs/ojarraydataprovider', 'ojs/ojlabelvalue',
    'ojs/ojformlayout','ojs/ojvalidationgroup', 'ojs/ojtrain', 'ojs/ojinputtext', 'ojs/ojdialog',
    'ojs/ojvalidationgroup','ojs/ojlabel', 'ojs/ojdatetimepicker', 'ojs/ojselectsingle', 
    'ojs/ojdatetimepicker','ojs/ojtimezonedata', 'ojs/ojfilepicker', 'ojs/ojlistview', 'ojs/ojinputnumber'],
    function (oj, ko, $, keySet, app, services, commonhelper, DateTimeConverter, ArrayDataProvider) {

        function itemReceivedOperationViewModel() {
            var self = this;
            var getTransaltion = oj.Translations.getTranslatedString;
            self.labels = ko.observable();
            self.approvalsBtnVisible = ko.observable(false);
            self.previousBtnVisible = ko.observable(false);
            self.nextBtnVisible = ko.observable(true);
            self.submitBtnVisible = ko.observable(false);
            self.currentStepValue = ko.observable('stp1');
            self.trainVisibility = ko.observable(true);
            self.fieldsDisabled = ko.observable(false);
            self.departmentList = ko.observableArray(app.departmentList());
            self.currencyList = ko.observableArray([]);
            self.currenciesProvider = new ArrayDataProvider(self.currencyList, { keyAttributes: 'value' });
            self.departmentsProvider = new ArrayDataProvider(self.departmentList, { keyAttributes: 'value' });
            self.pageMode = ko.observable();
            self.disableSubmit = ko.observable();
            self.rejectReason = ko.observable('');
            self.EmbedPDFLink = ko.observable();
            self.attachmentNames = ko.observableArray();
            // details dialog
            self.detailsTrainVisibility = ko.observable(true);
            self.detailsCurrentStepValue = ko.observable('stp1');
            self.detailsBtnDisabled = ko.observable(true);
            self.detailsPreviousBtnVisible = ko.observable(false);
            self.detailsNextBtnVisible = ko.observable(true);
            self.detailsSubmitBtnVisible = ko.observable(false);
            self.detailsViewBtnDisabled = ko.observable(true);
            self.detailsAddBtnDisabled = ko.observable(false);
            self.tableArray = ko.observableArray([]);
            self.tableDataProvider = new ArrayDataProvider(self.tableArray, { idAttributes: 'id' });
            self.tableSelectedRowKey = ko.observable();
            self.tableSelectedIndex = ko.observable();
            self.selectedDataTable = ko.observable();
            var rowId = 1;
            // file picker
            self.attachmentId = 0;
            self.koArray = ko.observableArray([]);
            self.selectedItemsAttachment = ko.observableArray([]);
            self.attachmentViewerData = ko.observable();
            self.koAttachmentArray = ko.observableArray([]);
            self.koArrayRemoved = ko.observableArray([]);
            self.dataProviderAttachment = new ArrayDataProvider(self.koArray, { keyAttributes: 'id' });

            self.dateConverter = ko.observable(new DateTimeConverter.IntlDateTimeConverter({
                pattern: 'dd/MM/yyyy'
            }));

            //---------- connected action ------------------//
            self.connected = function () {
                self.retrieve = JSON.parse(localStorage.getItem('itemData')); //oj.Router.rootInstance.retrieve();
                console.log(self.retrieve)
                if (typeof app.personDetails().personId != 'undefined') {
                    self.getCurrencyList();
                }
                if (self.retrieve.operation == 'ADD') {
                    self.pageMode('ADD');
                    if (typeof app.approverDetails() !== 'undefined') {
                        self.itemModel.employeeDepartment(app.approverDetails().EMPLOYEE_DEPART_NAME);
                        self.itemModel.employeePosition(app.approverDetails().G_2.EMPLOYEE_POSITION_NAME);
                        self.itemModel.approverId(app.approverDetails().G_2.CHEIF_ID);
                        self.itemModel.approverName(app.approverDetails().CHIEF_NAME);
                        self.itemModel.approverPosition(app.approverDetails().G_2.CHEIF_POSITION_NAME);
                        self.itemModel.receivingDepartment(app.approverDetails().EMPLOYEE_ORGANIZATION_ID);
                        self.itemModel.receivingDepartmentLbl(app.approverDetails().EMPLOYEE_DEPART_NAME);
                        self.getNextItemNumber();
                        self.detailsAddBtnDisabled(false);
                    }
                } else if (self.retrieve.operation == 'EDIT') {
                    self.pageMode('EDIT');
                    var data = self.retrieve.data;
                    self.itemModel.id(data.id);
                    self.itemModel.itemNumber(data.itemNumber);
                    self.itemModel.creationDate(data.creationDate);
                    self.itemModel.updatedDate(data.updatedDate);
                    self.itemModel.personNumber(data.personNumber);
                    self.itemModel.status(data.status);
                    self.itemModel.receivingDepartment(data.receivingDepartment);
                    self.itemModel.receivingDepartmentLbl(data.receivingDepartmentLbl);
                    self.detailsAddBtnDisabled(false);
                } else if (self.retrieve.operation == 'VIEW') {
                    self.pageMode('VIEW');
                    self.currentStepValue('stp2');
                    self.trainVisibility(false);
                    self.nextBtnVisible(false);
                    self.submitBtnVisible(false);
                    self.previousBtnVisible(false);
                    self.detailsViewBtnDisabled(false);
                    self.detailsBtnDisabled(true);
                    self.detailsAddBtnDisabled(true);
                    var data = self.retrieve.data;
                    self.itemModel.id(data.id);
                    self.itemModel.itemNumber(data.itemNumber);
                    self.itemModel.creationDate(data.creationDate);
                    self.itemModel.updatedDate(data.updatedDate);
                    self.itemModel.personNumber(data.personNumber);
                    self.itemModel.status(data.status);
                    self.itemModel.receivingDepartment(data.receivingDepartment);
                    self.itemModel.receivingDepartmentLbl(data.receivingDepartmentLbl);

                } else if (self.retrieve.operation == 'REVIEW') {
                    app.loading(true);
                    self.pageMode('REVIEW');
                    self.currentStepValue('stp2');
                    self.trainVisibility(false);
                    self.nextBtnVisible(false);
                    self.submitBtnVisible(false);
                    self.previousBtnVisible(false);
                    self.detailsViewBtnDisabled(false);
                    self.detailsBtnDisabled(true);
                    self.detailsAddBtnDisabled(true);
                    var data = self.retrieve.data;
                    if (data.type == 'FYA') {
                        self.approvalsBtnVisible(true);
                    }
                    self.getItemById(data.requestId);
                    app.getLastApprover(data.requestId, data.selfType);
                }else if(self.retrieve.operation == 'RMI'){
                    if (typeof app.approverDetails() !== 'undefined') {
                        self.itemModel.employeeDepartment(app.approverDetails().EMPLOYEE_DEPART_NAME);
                        self.itemModel.employeePosition(app.approverDetails().G_2.EMPLOYEE_POSITION_NAME);
                        self.itemModel.approverId(app.approverDetails().G_2.CHEIF_ID);
                        self.itemModel.approverName(app.approverDetails().CHIEF_NAME);
                        self.itemModel.approverPosition(app.approverDetails().G_2.CHEIF_POSITION_NAME);
                        self.itemModel.receivingDepartment(app.approverDetails().EMPLOYEE_ORGANIZATION_ID);
                        self.itemModel.receivingDepartmentLbl(app.approverDetails().EMPLOYEE_DEPART_NAME);
                    }
                    app.loading(true);
                    self.pageMode('RMI');
                    self.currentStepValue('stp1');
                    self.approvalsBtnVisible(false);
                    var data = self.retrieve.data;
                    self.getItemById(data.requestId);
                }
            };

            //---------**************-----------------//

            self.getItemById = function (id) {
                app.loading(true);
                var success = function (data) {
                    app.loading(false);
                    self.itemModel.id(data.id);
                    self.itemModel.itemNumber(data.itemNumber);
                    self.itemModel.creationDate(data.creationDate);
                    self.itemModel.updatedDate(data.updatedDate);
                    self.itemModel.personNumber(data.personNumber);
                    self.itemModel.status(data.status);
                    self.itemModel.receivingDepartment(data.receivingDepartment);
                    self.itemModel.receivingDepartmentLbl(data.receivingDepartmentLbl);
                    self.getDetailsById();
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get departments data'));
                    app.loading(false);
                };
                services.getGeneric('ItemReceived/getItemById/' + id).then(success, fail);

            };

            // get details for the item
            self.getDetailsById = function () {
                app.loading(true);
                if (typeof self.itemModel.id() != 'undefined') {
                    var success = function (data) {
                        if (data) {
                            self.tableArray(data);
                        }
                        app.loading(false);
                    };

                    var fail = function (data) {
                        app.messagesDataProvider.push(app.createMessage("error", 'Fail to get details data'));
                        app.loading(false);
                    };
                    services.getGeneric('ItemReceived/getDetailsByMasterId/' + self.itemModel.id()).then(success, fail);
                } else {
                    app.loading(false);
                }
            };

            self.getCurrencyList = function () {
                app.loading(true);
                var success = function (data) {
                    app.loading(false);
                    self.currencyList(data);
                    if (self.pageMode() != 'ADD') {
                        self.getDetailsById();
                    }
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get currency data'));
                    app.loading(false);
                };
                services.getGeneric('addtionlData/getCurrencyList').then(success, fail);
            };

            self.getNextItemNumber = function () {
                app.loading(true);
                var success = function (data) {
                    app.loading(false);
                    self.itemModel.itemNumber(data);
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get next item'));
                    app.loading(false);
                };
                services.getGeneric('ItemReceived/getNextItemNumber').then(success, fail);
            };

            //--------------- page modele --------//
            //master model
            self.itemModel = ko.observable();
            self.itemModel = {
                id: ko.observable(),
                itemNumber: ko.observable(),
                creationDate: ko.observable(new Date()),
                updatedDate: ko.observable(),
                employeeDepartment: ko.observable(),
                employeePosition: ko.observable(),
                approverId: ko.observable(),
                approverName: ko.observable(),
                approverPosition: ko.observable(),
                status: ko.observable('Pending Approval'),
                personId: ko.observable(app.personDetails().personId),
                personName: ko.observable(app.personDetails().displayName),
                lineManager: ko.observable(app.personDetails().managerId),
                managerOfManager: ko.observable(app.personDetails().managerName),
                managerName: ko.observable(app.personDetails().managerOfManager),
                managerOfManagerName: ko.observable(app.personDetails().managerOfMnagerName),
                personNumber: ko.observable(app.personDetails().personNumber),
                detailsList: ko.observableArray([]),
                receivingDepartment: ko.observable(),
                receivingDepartmentLbl: ko.observable()
            };

            // details model
            self.itemDetailsModel = ko.observable();
            self.itemDetailsModel = {
                id: ko.observable(),
                // receivingDepartment: ko.observable(),
                supplierName: ko.observable(),
                contractOrOrderNumber: ko.observable(),
                contractOrOrderDate: ko.observable(),
                invoiceNumber: ko.observable(),
                invoiceDate: ko.observable(),
                invoiceTotalAmount: ko.observable(),
                invoiceCurrency: ko.observable(),
                exchangeRate: ko.observable(),
                totalAmountInSAR: ko.observable(),
                attachment: ko.observable(),
                // receivingDepartmentLbl: ko.observable()
            };

            //---**************************----/
            //---------------- Master All Actions-----------//

            //*********** master action buttons*************/
            self.backAction = function () {
                app.loading(true);
                if (self.pageMode() == 'REVIEW') {
                    oj.Router.rootInstance.go('notificationScreen');
                } else {
                    oj.Router.rootInstance.go('itemReceivedSummary');
                }
            };

            self.showReportActionBtn = function () {
                app.loading(true);
                var getReportCbFn = function (data) {
                    var base64pdf = 'data:application/pdf;base64,' + data.report;
                    self.EmbedPDFLink(base64pdf);
                    document.querySelector("#pdfViewer").open();
                    app.loading(false);
                };

                var fail = function () {
                    app.loading(true);
                    app.messagesDataProvider.push(app.createMessage('error', "cann't get report"));
                }
                // var currency = self.currencyList().filter((data) => data.value == self.itemModel.invoiceCurrency());
                // var department = self.departmentList().filter((data) => data.value == self.itemModel.receivingDepartment());
                self.attachmentNames([]);
                self.koArray().forEach(element => self.attachmentNames().push(element.name));
                var payLoad = {
                    "personNumber": self.itemModel.personNumber(),
                    "status": self.itemModel.status(),
                    "itemNumber": self.itemModel.itemNumber(),
                    "detailsArray":self.tableArray(),
                    "receivingDepartmentLbl":self.itemModel.receivingDepartmentLbl(),
                    // "invoiceCurrency":"itemNumber", //currency[0].label,
                    // "poDate":"itemNumber",  //self.formatDate(new Date(self.itemModel.contractOrOrderDate())),
                    // "totalAmountSAR": "itemNumber", //self.itemModel.totalAmountInSAR(),
                    // "invoiceTotalAmount": "itemNumber", //self.itemModel.invoiceTotalAmount(),
                    // "poNumber": "itemNumber", //self.itemModel.contractOrOrderNumber(),
                    // "invoiceNumber": "itemNumber", //self.itemModel.invoiceNumber(),
                    // "suppliername":"itemNumber",  //self.itemModel.supplierName(),
                    // "exchangeRate": "itemNumber", //self.itemModel.exchangeRate(),
                    // "receivingDepartment": "itemNumber", //department[0].label,
                    // "invoiceDate":"itemNumber",  //self.formatDate(new Date(self.itemModel.invoiceDate())),
                    // "attachmentName": "itemNumber", //self.attachmentNames(),
                    "requestDate": self.formatDate(new Date(self.itemModel.creationDate())),
                    "approveDate": self.formatDate(new Date(self.itemModel.updatedDate())),
                };

                services.addGeneric("ItemReceived/pdfReport", payLoad).then(getReportCbFn, fail);
            };

            self.openApproveDialog = function () {
                document.getElementById('approveDialog').open();
            };

            self.openRejectDialog = function () {
                document.getElementById('rejectDialog').open();
            };

            self.approveDialogClose = function () {
                document.getElementById('approveDialog').close();
            };

            self.rejectlDialogClose = function () {
                document.getElementById('rejectDialog').close();
            };

            self.approveActionBtn = function () {
                self.disableSubmit(true);
                app.loading(true);
                var data = self.retrieve.data;
                var headers = {
                    "MSG_TITLE": data.msgTitle,
                    "MSG_BODY": data.msgBody,
                    "TRS_ID": data.requestId,
                    "PERSON_ID": app.personDetails().personId,
                    "RESPONSE_CODE": "APPROVED",
                    "ssType": data.selfType,
                    "PERSON_NAME": data.personName,
                    "rejectReason": self.rejectReason()
                };

                var success = function () {
                    if (app.isLastApprover()) {
                        var updateStatusCBFN = function (data) {
                            oj.Router.rootInstance.go("notificationScreen");
                        };
                        services.getGeneric("ItemReceived/updateStatus/" + data.requestId, {}).then(fail, updateStatusCBFN);
                    };

                };

                var fail = function () {
                    app.messagesDataProvider.push(app.createMessage('error', 'fail in approve '));
                };

                document.getElementById('approveDialog').close();
                services.addGeneric("workflowNotification/workflowAction", headers).then(success, fail);
            };

            self.rejectActionBtn = function () {
                self.disableSubmit(true);
                app.loading(true);
                var data = self.retrieve.data;
                var headers = {
                    "MSG_TITLE": data.msgTitle,
                    "MSG_BODY": data.msgBody,
                    "TRS_ID": data.requestId,
                    "PERSON_ID": app.personDetails().personId,
                    "RESPONSE_CODE": "REJECTED",
                    "ssType": data.selfType,
                    "PERSON_NAME": data.personName,
                    "rejectReason": self.rejectReason()
                };

                var success = function () {
                    oj.Router.rootInstance.go("notificationScreen");
                };

                var fail = function () {
                    app.messagesDataProvider.push(app.createMessage('error', 'fail in approve '));
                };

                document.getElementById('rejectDialog').close();
                services.addGeneric("workflowNotification/workflowAction", headers).then(success, fail);
            };


            /*************** master train actions ******************/
            self.previousStep = function () {
                var train = document.getElementById('train');
                var prev = train.getPreviousSelectableStep();
                if (prev != null) {
                    self.submitBtnVisible(false);
                    self.nextBtnVisible(true);
                    self.previousBtnVisible(false);
                    self.detailsAddBtnDisabled(false);
                    self.currentStepValue(prev);
                    if (self.selectedDataTable()) {
                        self.detailsBtnDisabled(false);
                    }
                }
            };

            self.nextStep = function () {
                var train = document.getElementById('train');
                var next = train.getNextSelectableStep();
                var tracker = document.getElementById("tracker");
                if (tracker.valid != "valid") {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    return;
                } else if (self.tableArray().length == 0) {
                    app.messagesDataProvider.push(app.createMessage('error', self.labels().detailsMsg()));
                    return;
                } else if (next != null) {
                    self.currentStepValue(next);
                    self.detailsCurrentStepValue('stp2');
                    self.nextBtnVisible(false);
                    self.previousBtnVisible(true);
                    self.submitBtnVisible(true);
                    self.detailsBtnDisabled(true);
                    self.detailsAddBtnDisabled(true);
                }
            };

            self.stopSelectListener = function () {
                var tracker = document.getElementById("tracker");
                if (tracker.valid != "valid") {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    event.preventDefault();
                    return;
                } else if (self.tableArray().length == 0) {
                    app.messagesDataProvider.push(app.createMessage('error', self.labels().detailsMsg()));
                    return;
                } else {
                    if (self.currentStepValue() == 'stp1') {
                        self.nextStep();
                    } else {
                        self.previousStep();
                    }
                }
            };

            self.submitActionBtn = function () {
                document.getElementById('submitDialog').open();
            };

            self.submitYes = function () {
                app.loading(true);
                self.itemModel.detailsList(self.tableArray());
                document.getElementById('submitDialog').close();
                var payload = ko.toJSON(self.itemModel);
                console.log(payload)
                var success = function (data) {
                    oj.Router.rootInstance.go('itemReceivedSummary');
                    app.loading(false);
                }
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to insert into items'));
                    app.loading(false);
                }

                services.addGeneric("ItemReceived/createOrUpdateItem/" + self.pageMode() + '/Default', payload).then(success, fail);
            };

            self.submitNo = function () {
                document.getElementById('submitDialog').close();
            };
            //------********************-----------------//   
            //*------   details all actions ---------------// 
            //--------------- handler ------------------------//
            self.invoiceTotalAmountHandler = function (event) {
                var val = event.detail.value;
                if(!val.toString().includes(',')){
                    val=numberWithCommas(val);
                }
                self.itemDetailsModel.invoiceTotalAmount(val);
                if (typeof val !== 'undefined' && typeof self.itemDetailsModel.exchangeRate() !== 'undefined') {

                    var amount = parseFloat(self.itemDetailsModel.exchangeRate())
                        * parseFloat(parseStringToFloat(self.itemDetailsModel.invoiceTotalAmount()));

                    self.itemDetailsModel.totalAmountInSAR(numberWithCommas(amount));
                }
            };

            self.invoiceCurrencyHandler = function (event) {
                self.itemDetailsModel.invoiceCurrency('');
                var val = event.detail.value;
                if (typeof val !== 'undefined' && val !== '') {
                    $("#changeRateInput").find("input").addClass("loading");
                    self.itemDetailsModel.invoiceCurrency(val);
                    if (val == 'SAR') {
                        self.itemDetailsModel.exchangeRate(1);
                        var amount = parseFloat(parseStringToFloat(self.itemDetailsModel.invoiceTotalAmount()))
                            * parseFloat(self.itemDetailsModel.exchangeRate());
                        self.itemDetailsModel.totalAmountInSAR(numberWithCommas(amount));
                        $("#changeRateInput").find("input").removeClass("loading");
                    } else {
                        self.getChangeRate(val);
                    }
                }
            };

            self.departmentListHandler = function (event) {
                // self.itemDetailsModel.receivingDepartmentLbl('');
                // self.itemDetailsModel.receivingDepartment('');

                self.itemModel.receivingDepartmentLbl('');
                self.itemModel.receivingDepartment('');
                var val = event.detail.value;
                if (typeof val !== 'undefined' && val !== '') {
                    self.itemModel.receivingDepartment(val);
                    var department = self.departmentList().filter((data) => data.value == val);
                    self.itemModel.receivingDepartmentLbl(department[0].label);
                }
            };

            self.getChangeRate = function (currency) {
                var reportParams = {
                    "conversionDate": self.formatDate(new Date()),
                    "fromCurrency": currency
                };

                var payload = {
                    'reportName': 'getCurrencyRate',
                    'params': reportParams
                };

                var success = function (data) {
                    if (typeof data !== 'undefined') {
                        self.itemDetailsModel.exchangeRate(data[0].CONVERSION_RATE);
                        var amount = parseFloat(parseStringToFloat(self.itemDetailsModel.invoiceTotalAmount()))
                            * parseFloat(self.itemDetailsModel.exchangeRate());
                        self.itemDetailsModel.totalAmountInSAR(numberWithCommas(amount));
                    } else {
                        app.messagesDataProvider.push(app.createMessage("error", self.labels().changeRateError()));
                    }
                    $("#changeRateInput").find("input").removeClass("loading");
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get departments data'));
                };
                services.addGeneric('report/commonbireport', payload).then(success, fail);
            };
            //--*****************************--//
            //---------- details train buttons action --------//
            self.detailsStopSelectListener = function (event) {
                var tracker = document.getElementById("DetailsTracker");
                if (tracker.valid != "valid") {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    event.preventDefault();
                    return;
                } else if (self.koArray().length == 0) {
                    app.messagesDataProvider.push(app.createMessage('error', self.labels().attachmentMsg()));
                    return;
                } else if (typeof self.itemDetailsModel.exchangeRate() == 'undefined') {
                    app.messagesDataProvider.push(app.createMessage('error', self.labels().changeRateError()));
                    return;
                } else {
                    if (self.detailsCurrentStepValue() == 'stp1') {
                        self.detailsNextStep();
                    } else {
                        self.detailsPreviousStep();
                    }
                }
            };

            self.detailsPreviousStep = function () {
                var train = document.getElementById('detailsTrain');
                var prev = train.getPreviousSelectableStep();
                if (prev != null) {
                    self.detailsSubmitBtnVisible(false);
                    self.detailsNextBtnVisible(true);
                    self.detailsPreviousBtnVisible(false);
                    self.detailsCurrentStepValue(prev);
                    self.fieldsDisabled(false);
                }
            };

            self.detailsNextStep = function () {
                var train = document.getElementById('detailsTrain');
                var next = train.getNextSelectableStep();
                var tracker = document.getElementById("DetailsTracker");
                if (tracker.valid != "valid") {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    return;
                } else if (self.koArray().length == 0) {
                    app.messagesDataProvider.push(app.createMessage('error', self.labels().attachmentMsg()));
                    return;
                } else if (typeof self.itemDetailsModel.exchangeRate() == 'undefined') {
                    app.messagesDataProvider.push(app.createMessage('error', self.labels().changeRateError()));
                    return;
                } else if (next != null) {
                    self.fieldsDisabled(true);
                    self.detailsCurrentStepValue(next);
                    self.detailsNextBtnVisible(false);
                    self.detailsPreviousBtnVisible(true);
                    self.detailsSubmitBtnVisible(true);
                }
            };

            self.detailsSubmitActionBtn = function () {
                self.itemDetailsModel.attachment(self.koArray());
                if (self.itemDetailsModel.id()) {
                    var element = document.getElementById('lineTable');
                    var currentRow = element.currentRow;
                    if (currentRow != null) {
                        self.tableArray.splice(currentRow['rowIndex'], 1, {
                            // receivingDepartment: ko.toJS(self.itemDetailsModel.receivingDepartment()),
                            supplierName: ko.toJS(self.itemDetailsModel.supplierName()),
                            contractOrOrderNumber: ko.toJS(self.itemDetailsModel.contractOrOrderNumber()),
                            contractOrOrderDate: ko.toJS(self.itemDetailsModel.contractOrOrderDate()),
                            invoiceNumber: ko.toJS(self.itemDetailsModel.invoiceNumber()),
                            invoiceDate: ko.toJS(self.itemDetailsModel.invoiceDate()),
                            invoiceTotalAmount: ko.toJS(self.itemDetailsModel.invoiceTotalAmount()),
                            invoiceCurrency: ko.toJS(self.itemDetailsModel.invoiceCurrency()),
                            exchangeRate: ko.toJS(numberWithCommas(self.itemDetailsModel.exchangeRate())),
                            totalAmountInSAR: ko.toJS(numberWithCommas(self.itemDetailsModel.totalAmountInSAR())),
                            attachment: ko.toJS(JSON.stringify(self.itemDetailsModel.attachment())),
                            // receivingDepartmentLbl: ko.toJS(self.itemDetailsModel.receivingDepartmentLbl())
                        });
                    }
                } else {
                    self.itemDetailsModel.attachment(self.koArray());
                    self.itemDetailsModel.id(rowId);
                    rowId = parseInt(rowId) + 1;
                    self.tableArray.push({
                        id: ko.toJS(self.itemDetailsModel.id()),
                        // receivingDepartment: ko.toJS(self.itemDetailsModel.receivingDepartment()),
                        supplierName: ko.toJS(self.itemDetailsModel.supplierName()),
                        contractOrOrderNumber: ko.toJS(self.itemDetailsModel.contractOrOrderNumber()),
                        contractOrOrderDate: ko.toJS(self.itemDetailsModel.contractOrOrderDate()),
                        invoiceNumber: ko.toJS(self.itemDetailsModel.invoiceNumber()),
                        invoiceDate: ko.toJS(self.itemDetailsModel.invoiceDate()),
                        invoiceTotalAmount: ko.toJS(self.itemDetailsModel.invoiceTotalAmount()),
                        invoiceCurrency: ko.toJS(self.itemDetailsModel.invoiceCurrency()),
                        exchangeRate: ko.toJS(numberWithCommas(self.itemDetailsModel.exchangeRate())),
                        totalAmountInSAR: ko.toJS(numberWithCommas(self.itemDetailsModel.totalAmountInSAR())),
                        attachment: ko.toJS(JSON.stringify(self.itemDetailsModel.attachment())),
                        // receivingDepartmentLbl: ko.toJS(self.itemDetailsModel.receivingDepartmentLbl())
                    });
                }
                document.getElementById('detailsDialog').close();
                self.detailsPreviousStep();

            };

            //*************** table details action button*************/
            self.addActionBtn = function () {
                self.dailogStyleReset();
                self.detailsCurrentStepValue('stp1');
                self.emptyDeatilsModel();
                self.detailsTrainVisibility(true);
                self.detailsNextBtnVisible(true);
                self.fieldsDisabled(false);
                document.getElementById('detailsDialog').open();
            };

            self.editActionBtn = function () {
                var element = document.getElementById('lineTable');
                var currentRow = element.currentRow;
                var rowObj = document.getElementById('lineTable').getDataForVisibleRow(currentRow['rowIndex']);
                self.selectedDataTable(JSON.stringify(rowObj['data']));
                self.fillDetailsModel();
                self.detailsTrainVisibility(true);
                self.detailsNextBtnVisible(true);
                self.fieldsDisabled(false);
                self.detailsCurrentStepValue('stp1');
                document.getElementById('detailsDialog').open();
                
            };

            self.viewActionBtn = function () {
                var element = document.getElementById('lineTable');
                var currentRow = element.currentRow;
                var rowObj = document.getElementById('lineTable').getDataForVisibleRow(currentRow['rowIndex']);
                self.selectedDataTable(JSON.stringify(rowObj['data']));
                self.fillDetailsModel();
                document.getElementById('detailsDialog').open();
                self.detailsCurrentStepValue('stp2');
                self.detailsPreviousBtnVisible(false);
                self.detailsSubmitBtnVisible(false);
                self.detailsTrainVisibility(false);
                self.detailsNextBtnVisible(false);
                self.fieldsDisabled(true);

            };

            self.deleteActionBtn = function () {
                var element = document.getElementById('lineTable');
                var currentRow = element.currentRow;
                self.tableArray.splice(currentRow['rowIndex'], 1);
                self.detailsBtnDisabled(true);
            };

            self.tableSelectionListener = function (event) {
                var data = event.detail;
                var newCurrentRow = data.currentRow;
                if (newCurrentRow !== null) {
                    var rowObj = document.getElementById('lineTable').getDataForVisibleRow(newCurrentRow['rowIndex']);
                    self.selectedDataTable(JSON.stringify(rowObj['data']));
                    console.log(self.selectedDataTable());
                    self.detailsViewBtnDisabled(false);
                    if ((self.pageMode() !== 'VIEW' || self.pageMode() !== 'REVIEW') && (self.currentStepValue() == 'stp1')) {
                            self.detailsBtnDisabled(false);
                    }
                }
            };

            self.fillDetailsModel = function () {
                self.dailogStyleReset();
                var data = JSON.parse(self.selectedDataTable());
                self.itemDetailsModel.id(data.id);
                // self.itemDetailsModel.receivingDepartment(data.receivingDepartment);
                self.itemDetailsModel.supplierName(data.supplierName);
                self.itemDetailsModel.contractOrOrderNumber(data.contractOrOrderNumber);
                self.itemDetailsModel.contractOrOrderDate(data.contractOrOrderDate);
                self.itemDetailsModel.invoiceNumber(data.invoiceNumber);
                self.itemDetailsModel.invoiceDate(data.invoiceDate);
                self.itemDetailsModel.invoiceTotalAmount(data.invoiceTotalAmount);
                self.itemDetailsModel.invoiceCurrency(data.invoiceCurrency);
                self.itemDetailsModel.exchangeRate(data.exchangeRate);
                self.itemDetailsModel.totalAmountInSAR(data.totalAmountInSAR);
                self.itemDetailsModel.attachment(data.attachment);
                self.koArray(JSON.parse(self.itemDetailsModel.attachment()));
                // self.itemDetailsModel.receivingDepartmentLbl(data.receivingDepartmentLbl);
            }

            self.emptyDeatilsModel = function () {
                self.itemDetailsModel.id('');
                // self.itemDetailsModel.receivingDepartment('');
                self.itemDetailsModel.supplierName('');
                self.itemDetailsModel.contractOrOrderNumber('');
                self.itemDetailsModel.contractOrOrderDate('');
                self.itemDetailsModel.invoiceNumber('');
                self.itemDetailsModel.invoiceDate('');
                self.itemDetailsModel.invoiceTotalAmount(0);
                self.itemDetailsModel.invoiceCurrency('');
                self.itemDetailsModel.exchangeRate(1);
                self.itemDetailsModel.totalAmountInSAR(0);
                self.itemDetailsModel.attachment('');
                // self.itemDetailsModel.receivingDepartmentLbl('');
                self.koArray([]);
            }

            //*********** details attachments ************/
            self.attachmentSelectListener = function (event) {
                var files = event.detail.files;
                if (files.length > 0) {
                    //add the new files at the beginning of the list
                    var dataFiles = {};
                    for (var i = 0; i < files.length; i++) {
                        function getBase64(file) {
                            return new Promise((resolve, reject) => {
                                const reader = new FileReader();
                                reader.readAsDataURL(file);
                                reader.onload = () => resolve(reader.result);
                                reader.onerror = error => reject(error);
                            });
                        }
                        dataFiles.name = files[i].name;
                        getBase64(files[i]).then(function (data) {
                            dataFiles.data = (data);
                            self.attachmentId = self.attachmentId - 1;
                            dataFiles.id = (self.attachmentId);
                            self.koArray.push(dataFiles);
                        }
                        );
                    }
                }
            };

            self.removeSelectedAttachment = function (event) {
                if (typeof self.selectedItemsAttachment()[0] !== 'undefined')
                    self.selectedItemsAttachment()[0] < 0 ? '' : self.koArrayRemoved.push({ 'id': self.selectedItemsAttachment()[0] });
                $.each(self.selectedItemsAttachment(), function (index, value) {
                    self.koArray.remove(function (item) {
                        return (item.id == value);
                    });
                });
            };

            self.openAttachmentViewer = function () {
                console.log(self.koArray());
                //Option to download all attachments once the method fired
                for (var i = 0; i < self.koArray().length; i++) {
                    downloadURI(self.koArray()[i].data, self.koArray()[i].name);
                }
            };

            function downloadURI(uri, name) {
                var link = document.createElement("a");
                link.setAttribute('href', uri);
                link.setAttribute('download', name);
                link.style.display = 'none';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }

            self.dialogMaximize=function(){
                var w = window.innerWidth-20;
                var h = window.innerHeight-20;
                var dailog = document.getElementById('detailsDialog');
                dailog.style.height=h+'px';
                dailog.style.width=w+'px';
                dailog.style.top='0px';
                dailog.style.left='0px';
            };

            self.dialogMinimize=function(){
                var dailog = document.getElementById('detailsDialog');
                var h = window.innerHeight-50;
                dailog.style.top=h+'px';
                dailog.style.left='10px';
                dailog.style.width='40%';
                dailog.style.height='40px';

                var dailogHeader = dailog.getElementsByClassName('oj-dialog-header');
                dailogHeader[0].style.minHeight='40px';
            };

            self.dailogStyleReset=function(){
                var dailog = document.getElementById('detailsDialog');
                dailog.style.height='auto';
                dailog.style.width='80%';
                dailog.style.top='0';
                dailog.style.left='0';
            };

            //---**********************************-----//
            //----------- refresh for language ---------//
            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });
            //---------- other functions -------------//

            // convert blob to local image
            self.image = ko.observable();
            self.getAttachments = async function (attachment) {
                var attachments = JSON.parse(attachment);
                for (var i = 0; i < attachments.length; i++) {
                    var image = await arrayBufferToBase64(new Uint8Array(attachments[i].data)).then(imageData => {
                        return imageData.replace("data/", "data:").replace('/base64/', ';base64,');
                    });
                    var data = {
                        "name": attachments[i].name,
                        "id": attachments[i].id,
                        "data": image
                    };
                    self.koArray.push(data);
                };
            };
            //format date
            self.formatDate = function (date) {
                var month = '' + (date.getMonth() + 1),
                    day = '' + date.getDate(),
                    year = date.getFullYear();
                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;
                return [year, month, day].join('-');
            };
            // convert bytes to base64
            function arrayBufferToBase64(buffer) {
                return new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    var blob = new Blob([buffer]);
                    reader.readAsDataURL(blob);
                    reader.onload = () => resolve(reader.result.substr(reader.result.indexOf(',') + 1));
                    reader.onerror = error => reject(error);
                });
            }

            function numberWithCommas(value) {
                value = parseFloat(value.toString().includes(',') ? value.replace(',', '') : value).toFixed(2);
                var sep = ',';
                // check if it needs formatting
                if (value.toString() === value.toLocaleString()) {
                    // split decimals
                    var parts = value.toString().split('.')
                    // format whole numbers
                    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, sep);
                    // put them back together
                    value = parts[1] ? parts.join('.') : parts[0];
                } else {
                    value = value.toLocaleString();
                }
                return value;
            }

            function parseStringToFloat(stringValue) {
                stringValue = stringValue.trim();
                var result = stringValue.replace(/[^0-9]/g, '');
                if (/[,\.]\d{2}$/.test(stringValue)) {
                    result = result.replace(/(\d{2})$/, '.$1');
                }
                return parseFloat(result);
            }
            // -------------- translations -------------//
            function initTranslations() {
                self.labels({
                    addLbl: ko.observable(getTransaltion("common.add")),
                    saveLbl: ko.observable(getTransaltion("common.save")),
                    editLbl: ko.observable(getTransaltion("common.edit")),
                    viewLbl: ko.observable(getTransaltion("common.view")),
                    deleteLbl: ko.observable(getTransaltion("common.delete")),
                    homeScreenLbl: ko.observable(getTransaltion("itemReceived.title")),
                    noteDetails: ko.observable(getTransaltion("itemReceived.noteDetails")),
                    approvalLbl: ko.observable(getTransaltion("common.viewApproval")),
                    back: ko.observable(getTransaltion("common.back")),
                    approveLbl: ko.observable(getTransaltion("common.approve")),
                    rejectLbl: ko.observable(getTransaltion("common.reject")),
                    perviousLbl: ko.observable(getTransaltion("common.prev")),
                    nextLbl: ko.observable(getTransaltion("common.next")),
                    submitLbl: ko.observable(getTransaltion("common.submit")),
                    itemNumber: ko.observable(getTransaltion("itemReceived.itemNumber")),
                    receivingDepartment: ko.observable(getTransaltion("itemReceived.receivingDepartment")),
                    supplierName: ko.observable(getTransaltion("itemReceived.supplierName")),
                    contractOrOrderNumber: ko.observable(getTransaltion("itemReceived.cotractPoNumber")),
                    contractPoDate: ko.observable(getTransaltion("itemReceived.contractPoDate")),
                    invoiceNumber: ko.observable(getTransaltion("itemReceived.invoiceNumber")),
                    invoiceDate: ko.observable(getTransaltion("itemReceived.invoiceDate")),
                    invoiceTotalAmount: ko.observable(getTransaltion("itemReceived.invoiceTotalAmount")),
                    invoiceCurrency: ko.observable(getTransaltion("itemReceived.invoiceCurrency")),
                    exchangeRate: ko.observable(getTransaltion("itemReceived.exchangeRate")),
                    totalAmountInSAR: ko.observable(getTransaltion("itemReceived.totalAmountInSAR")),
                    relatedDocument: ko.observable(getTransaltion("itemReceived.relatedDocument")),
                    removeLbl: ko.observable(getTransaltion("common.remove")),
                    downloadLbl: ko.observable(getTransaltion("common.download")),
                    attachmentMsg: ko.observable(getTransaltion("itemReceived.attachmentError")),
                    submitTitle: ko.observable(getTransaltion("itemReceived.submitTitle")),
                    submitMessage: ko.observable(getTransaltion("itemReceived.submitMsg")),
                    yes: ko.observable(getTransaltion("common.yes")),
                    no: ko.observable(getTransaltion("common.no")),
                    placeHolder: ko.observable(getTransaltion("common.placeHolder")),
                    departmentMsg: ko.observable(getTransaltion("itemReceived.departmentMsg")),
                    invoiceCurrencyMsg: ko.observable(getTransaltion("itemReceived.invoiceCurrencyMsg")),
                    stepArray: ko.observableArray([{ label: getTransaltion("common.create"), id: 'stp1' },
                    { label: getTransaltion("common.review"), id: 'stp2' }]),
                    approveTitle: ko.observable(getTransaltion("common.approveTitle")),
                    approveMessage: ko.observable(getTransaltion("common.approveMsg")),
                    rejectTitle: ko.observable(getTransaltion("common.rejectTitle")),
                    rejectMessage: ko.observable(getTransaltion("common.rejectMsg")),
                    rejectReason: ko.observable(getTransaltion("common.rejectReason")),
                    showReportLbl: ko.observable(getTransaltion("common.showReport")),
                    changeRateError: ko.observable(getTransaltion("itemReceived.changeRateError")),
                    detailsMsg:ko.observable(getTransaltion("itemReceived.detailsMsg")),
                    columnArray: ko.observableArray([
                        // {
                        //     "headerText": getTransaltion('itemReceived.receivingDepartment'), "field": "receivingDepartmentLbl"
                        // },
                        {
                            "headerText": getTransaltion('itemReceived.supplierName'), "field": "supplierName"
                        },
                        {
                            "headerText": getTransaltion('itemReceived.cotractPoNumber'), "field": "contractOrOrderNumber"
                        },
                        {
                            "headerText": getTransaltion('itemReceived.invoiceNumber'), "field": "invoiceNumber"
                        },
                        {
                            "headerText": getTransaltion('itemReceived.invoiceCurrency'), "field": "invoiceCurrency"
                        }
                        // ,
                        // {
                        //     "headerText": getTransaltion('itemReceived.totalAmountInSAR'), "field": "totalAmountInSAR"
                        // }
                    ])

                });

            }
            initTranslations();
        }

        return itemReceivedOperationViewModel;


    })