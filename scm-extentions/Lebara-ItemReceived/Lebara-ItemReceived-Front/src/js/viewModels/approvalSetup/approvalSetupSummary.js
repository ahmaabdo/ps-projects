define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojarraydataprovider', 'ojs/ojknockout-keyset',
    'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojtable',
    'ojs/ojlabelvalue', 'ojs/ojselectsingle','ojs/ojdialog'],
    function (oj, ko, $, ArrayDataProvider, keySet, app, services, commonhelper) {

        function approvalSetupSummaryViewModel() {

            var self = this;
            var getTransaltion = oj.Translations.getTranslatedString;
            self.labels = ko.observable();
            self.tableArray = ko.observableArray([]);
            self.selectedServiceName = ko.observable();
            self.tableSelectedRowKey=ko.observable();
            self.oprationdisabled=ko.observable(true);
            self.selectedDataTable=ko.observable();
            self.tableSelectedIndex=ko.observable();
            self.servicesNameArray = ko.observableArray([
                {
                    label: 'Item received',
                    value: 'XX_ITEM_RECEIVED'
                }
            ])
            self.tableDataProvider = new ArrayDataProvider(self.tableArray, { keyAttributes: 'id' });
            self.serviceListProvider = new ArrayDataProvider(self.servicesNameArray, { keyAttributes: 'value' });
           
            //------------ refresh for language -------------//
            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });
            //------------************-----------------------//

            //--------------- service name handler -------------//
            self.serviceNameHandler = function (event) {
                var serviceName = event.detail.value;
                app.loading(true);
                self.selectedServiceName(serviceName);
                self.getApprovalSetup(serviceName);
            };

            self.getApprovalSetup=function(serviceName){
                var success = function (data) {
                    app.loading(false);
                    for (var i = 0; i < data.length; i++) {
                        let serviceNameLbl = self.servicesNameArray().filter(obj => obj.value == data[i].serviceCode);
                        data[i].serviceName = serviceNameLbl[0].label;
                    }
                    self.tableArray(data);
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error",'Fail to get data'));
                    app.loading(false);
                };
                services.getGeneric('approvalSetup/' + serviceName).then(success, fail);
            };
            //-----------------***********-------------------//

            //-------------- table selection -----------------//
            self.tableSelectionListener = function (event) {
                var data = event.detail;
                var currentRow = data.currentRow;
                if (currentRow !== null) {
                    self.tableSelectedRowKey(currentRow['rowKey']);
                    //store current index to pass it to other form when update or view
                    self.tableSelectedIndex(currentRow['rowIndex']);
                    self.oprationdisabled(false);
                    self.selectedDataTable(self.tableArray()[self.tableSelectedIndex()]);
                    console.log(self.selectedDataTable());
                };
            };
            //--------------****************---------------//

            //-------------- buttons action -------------//
            self.addActionBtn = function (event) {
                app.loading(true);
                var data = { operation: 'ADD' };
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('approvalSetupOperation');
            };

            self.editActionBtn = function (event) {
                var dataToSend = { operation: 'EDIT', data: self.selectedDataTable() };
                oj.Router.rootInstance.store(dataToSend);
                oj.Router.rootInstance.go('approvalSetupOperation');
                app.loading(true);
            };

            self.viewActionBtn = function (event) {
                var dataToSend = { operation: 'VIEW', data: self.selectedDataTable() };
                oj.Router.rootInstance.store(dataToSend);
                oj.Router.rootInstance.go('approvalSetupOperation');
                app.loading(true);
            };

            self.deleteActionBtn = function (event) {
                document.getElementById('deleteDialog').open();
            };

            self.submitYes=function(){
                document.getElementById('deleteDialog').close();
                app.loading(true);
                var success = function (data) {
                    // app.loading(false);
                    // self.tableArray.splice(self.tableSelectedIndex(), 1);
                    self.tableArray([]);
                    self.getApprovalSetup(self.selectedServiceName());
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error",'Fail to delete row'));
                    app.loading(false);
                };
                if(typeof self.selectedDataTable().id != 'undefiend'){
                    var serviceUrl=self.selectedDataTable().id+'/'+self.selectedDataTable().serviceCode+'/'+self.selectedDataTable().approvalOrder;
                    services.addGeneric('approvalSetup/deleteApproval/' + serviceUrl).then(success, fail);
                }else{
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage("error",'Select row first'));
                }
            };

            self.submitNo=function(){
                document.getElementById('deleteDialog').close();
            };

            //-------------**************-----------//

            self.connected = function () {
                app.loading(false);

            };

            //-------------- translations ----------------//
            function initTranslations() {
                self.labels({
                    addLbl: ko.observable(getTransaltion("common.add")),
                    editLbl: ko.observable(getTransaltion("common.edit")),
                    viewLbl: ko.observable(getTransaltion("common.view")),
                    homeScreenLbl: ko.observable(getTransaltion("approvalSetup.title")),
                    deleteLbl: ko.observable(getTransaltion("common.delete")),
                    placeHolder: ko.observable(getTransaltion("common.placeHolder")),
                    serviceName: ko.observable(getTransaltion("approvalSetup.serviceName")),
                    deleteTitle:ko.observable(getTransaltion("approvalSetup.deleteTitle")),
                    deleteMessage:ko.observable(getTransaltion("approvalSetup.deleteMessage")),
                    yes:ko.observable(getTransaltion("common.yes")),
                    no:ko.observable(getTransaltion("common.no")),
                    columnArray: ko.observableArray([
                        {
                            "headerText": getTransaltion('approvalSetup.serviceName'), "field": "serviceName"
                        },
                        {
                            "headerText": getTransaltion('approvalSetup.approvalType'), "field": "approvalType"
                        },
                        {
                            "headerText": getTransaltion('approvalSetup.approvalCode'), "field": "approvalCode"
                        },
                        {
                            "headerText": getTransaltion('approvalSetup.approvalOrder'), "field": "approvalOrder"
                        },
                        {
                            "headerText": getTransaltion('approvalSetup.notificationType'), "field": "notificationType"
                        },
                        {
                            "headerText": getTransaltion('approvalSetup.roleName'), "field": "roleNameLbl"
                        },
                        {
                            "headerText": getTransaltion('approvalSetup.specialCase'), "field": "specialCase"
                        }
                    ])
                });

            }
            initTranslations();
        }

        return approvalSetupSummaryViewModel;


    })