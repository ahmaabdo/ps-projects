define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper',
    'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata',
    'ojs/ojselectsingle', 'ojs/ojdialog', 'ojs/ojtrain', 'ojs/ojvalidationgroup', 'ojs/ojinputnumber'],
    function (oj, ko, $, app, services, commonhelper, ArrayDataProvider) {

        function approvalSetupOprationViewModel() {
            var self = this;
            var getTransaltion = oj.Translations.getTranslatedString;
            var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
            self.labels = ko.observable();
            self.pageMode = ko.observable();
            self.currentStepValue = ko.observable('stp1');
            self.submitBtnVisible = ko.observable(false);
            self.nextBtnVisible = ko.observable(true);
            self.previousBtnVisible = ko.observable(false);
            self.trainVisibility = ko.observable(true);
            self.fieldsDisabled = ko.observable(false);
            self.specialCaseVisible = ko.observable(false);
            self.lastNotificationType=ko.observable();
            self.serviceCodeArray = ko.observableArray([{
                label: 'Item Recieved',
                value: 'XX_ITEM_RECEIVED'
            }]);
            self.approvalCodeArray = ko.observableArray([{
                label: 'Default',
                value: 'Default'
            }]);
            self.approvalTypeArray = ko.observableArray([{
                label: 'Employee',
                value: 'EMP'
            }, {
                label: 'Special Case',
                value: 'Special_Case'
            }, {
                label: 'Line Manager',
                value: 'LINE_MANAGER'
            }, {
                label: 'Manger of Manager',
                value: 'LINE_MANAGER+1'
            }, {
                label: 'Chief Executive Officer',
                value: 'chief_officer'
            }]);
            self.notificationTypeArray = ko.observableArray([{
                label: 'For your information',
                value: 'FYI'
            }, {
                label: 'For your action',
                value: 'FYA'
            }]);
            self.serviceCodeProvider = new ArrayDataProvider(self.serviceCodeArray, { keyAttributes: 'value' });
            self.approvalCodeProvider = new ArrayDataProvider(self.approvalCodeArray, { keyAttributes: 'value' });
            self.approvalTypeProvider = new ArrayDataProvider(self.approvalTypeArray, { keyAttributes: 'value' });
            self.notificationTypeProvider = new ArrayDataProvider(self.notificationTypeArray, { keyAttributes: 'value' });


              //-------------------------Start Load Funcation------------------------------
              self.connected = function () {
                app.loading(false);
                self.retrieve = oj.Router.rootInstance.retrieve();
                if (self.retrieve.operation == 'ADD') {
                    self.pageMode('ADD');
                }else if (self.retrieve.operation == 'EDIT') {
                    self.pageMode('EDIT');
                    // self.getLastNotificationType();
                    var dataObj=self.retrieve.data;
                    self.approvalModel.id(dataObj.id);
                    self.approvalModel.serviceCode(dataObj.serviceCode);
                    self.approvalModel.approvalCode(dataObj.approvalCode);
                    self.approvalModel.approvalOrder(parseInt(dataObj.approvalOrder));
                    self.approvalModel.approvalType(dataObj.approvalType);
                    self.approvalModel.notificationType(dataObj.notificationType);
                    self.approvalModel.specialCase(dataObj.specialCase);
                    self.approvalModel.updatedBy(app.personDetails().personId);
                    app.loading(false);
                }else if (self.retrieve.operation == 'VIEW') {
                    self.pageMode('VIEW');
                    self.fieldsDisabled(true);
                    self.trainVisibility(false);
                    self.currentStepValue('stp2');
                    self.submitBtnVisible(false);
                    self.nextBtnVisible(false);
                    self.previousBtnVisible(false);
                    var dataObj=self.retrieve.data;
                    self.approvalModel.id(dataObj.id);
                    self.approvalModel.serviceCode(dataObj.serviceCode);
                    self.approvalModel.approvalCode(dataObj.approvalCode);
                    self.approvalModel.approvalOrder(parseInt(dataObj.approvalOrder));
                    self.approvalModel.approvalType(dataObj.approvalType);
                    self.approvalModel.notificationType(dataObj.notificationType);
                    self.approvalModel.specialCase(dataObj.specialCase);
                    self.approvalModel.updatedBy(app.personDetails().personId);
                    app.loading(false);
                }
            };
            //--------------************-----------------//
            //----------------- approval model -------------//

            self.approvalModel = ko.observable();
            self.approvalModel = {
                id:ko.observable(),
                serviceCode: ko.observable(),
                approvalCode: ko.observable(),
                approvalOrder: ko.observable(),
                approvalType: ko.observable(),
                notificationType: ko.observable(),
                specialCase:ko.observable(),
                createdBy:ko.observable(),
                updatedBy:ko.observable()
                
            };

            //------------*********************-------------//
            //----------------- buttons action -----------------

            self.backAction = function () {
                oj.Router.rootInstance.go('approvalSetupSummary');
            };

            //-----****************************************----//
            //-------------- train action buttons -------------//

            self.previousStep = function (event) {
                var train = document.getElementById('train');
                var prev = train.getPreviousSelectableStep();
                if (prev != null) {
                    self.submitBtnVisible(false);
                    self.nextBtnVisible(true);
                    self.previousBtnVisible(false);
                    self.currentStepValue(prev);
                    self.fieldsDisabled(false);
                }
            };

            self.nextStep = function (event) {
                var train = document.getElementById('train');
                var next = train.getNextSelectableStep();
                var tracker = document.getElementById("tracker");
                if (tracker.valid != "valid") {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    return;
                } else if (next != null) {
                    self.fieldsDisabled(true);
                    self.currentStepValue(next);
                    self.nextBtnVisible(false);
                    self.previousBtnVisible(true);
                    self.submitBtnVisible(true);
                }
            };

            self.stopSelectListener = function (event) {
                var tracker = document.getElementById("tracker");
                if (tracker.valid != "valid") {
                    tracker.showMessages();
                    tracker.focusOn("@firstInvalidShown");
                    event.preventDefault();
                    return;
                } else {
                    if (self.currentStepValue() == 'stp1') {
                        self.nextStep();
                    } else {
                        self.previousStep();
                    }
                }
            };

            self.submitActionBtn = function (event) {
                document.getElementById('submitDialog').open();
            };

            self.submitYes = function () {
                document.getElementById('submitDialog').close();
                app.loading(true);
                self.approvalModel.createdBy(app.personDetails().personId);
                var jsonData=ko.toJSON(self.approvalModel);
                var success = function () {
                    app.loading(false);
                    oj.Router.rootInstance.go('approvalSetupSummary');
                };

                var fail = function () {
                    app.messagesDataProvider.push(app.createMessage("error",'fail to insert approval'));
                    document.getElementById('submitDialog').close();
                };
                services.addGeneric('approvalSetup/' + self.pageMode(), jsonData).then(success, fail);
            };

            self.submitNo = function () {
                document.getElementById('submitDialog').close();
            };
            //-------------***********************------------//

            //-------------- handler change  --------------//
            self.serviceNameHandler = function (event) {
                $("#inputApprovalOrder").find("input").addClass("loading");
                self.MaxApprovalOrder();
                self.getLastNotificationType();
            };

            self.approvalCodeHandler = function (event) {
                $("#inputApprovalOrder").find("input").addClass("loading");
                self.MaxApprovalOrder();
                self.getLastNotificationType();
            };

            self.approvalTypeHandler = function (event) {
                var data = event.detail.value;
                if (data == 'Special_Case') {
                    self.specialCaseVisible(true);
                } else {
                    self.specialCaseVisible(false);
                }
            };

            self.MaxApprovalOrder = function () {
                var serviceCode = self.approvalModel.serviceCode();
                var approvalCode = self.approvalModel.approvalCode();
                var getMaxCBF = function (data) {
                    max_order = data.count + 1;
                    self.approvalModel.approvalOrder(max_order);
                    $("#inputApprovalOrder").find("input").removeClass("loading");
                    return data.count;
                };
                var failCbFn=function(){
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage("error",'error to get max order'));
                }
                services.getGeneric("approvalSetup/maxOrder/" + serviceCode + "/" + approvalCode).then(getMaxCBF, failCbFn);

            };

            self.getLastNotificationType = function () {
                var serviceCode = self.approvalModel.serviceCode();
                var approvalCode=self.approvalModel.approvalCode();
                var success = function (data) {
                    self.lastNotificationType(data.notificationType);
                };
                var fail=function(){
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage("error",'error to get notification order'));
                };

                services.getGeneric("approvalSetup/lastNotificationType/" + serviceCode+"/"+approvalCode).then(success, fail);
            };
            //-------------*****************--------------//
            //---------------------Translate Section ----------------------------------------

            function initTranslations() {
                self.labels(
                    {
                        screenLbl: ko.observable(getTransaltion("approvalSetup.title")),
                        back: ko.observable(getTransaltion("common.back")),
                        perviousLbl: ko.observable(getTransaltion("common.prev")),
                        nextLbl: ko.observable(getTransaltion("common.next")),
                        submitLbl: ko.observable(getTransaltion("common.submit")),
                        submitTitle: ko.observable(getTransaltion("approvalSetup.submitTitle")),
                        submitMessage: ko.observable(getTransaltion("approvalSetup.submitMsg")),
                        placeHolder: ko.observable(getTransaltion("common.placeHolder")),
                        yes: ko.observable(getTransaltion("common.yes")),
                        no: ko.observable(getTransaltion("common.no")),
                        stepArray: ko.observableArray([{ label: getTransaltion("common.create"), id: 'stp1' },
                        { label: getTransaltion("common.review"), id: 'stp2' }]),
                        serviceName: ko.observable(getTransaltion("approvalSetup.serviceName")),
                        approvalCode: ko.observable(getTransaltion("approvalSetup.approvalCode")),
                        approvalOrder: ko.observable(getTransaltion("approvalSetup.approvalOrder")),
                        approvalType: ko.observable(getTransaltion("approvalSetup.approvalType")),
                        notificationType: ko.observable(getTransaltion("approvalSetup.notificationType")),
                        specialCase: ko.observable(getTransaltion('approvalSetup.specialCase'))
                    }
                )

            }
            initTranslations();

        }

        return approvalSetupOprationViewModel;
    });
