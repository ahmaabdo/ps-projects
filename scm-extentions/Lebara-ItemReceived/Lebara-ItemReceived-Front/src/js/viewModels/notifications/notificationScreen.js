define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojarraydataprovider', 'ojs/ojknockout-keyset',
    'appController', 'config/services', 'util/commonhelper', 'ojs/ojknockout', 'ojs/ojtable',
    'ojs/ojlabelvalue', 'ojs/ojselectsingle', 'ojs/ojdialog','ojs/ojinputtext'],
    function (oj, ko, $, ArrayDataProvider, keySet, app, services, commonhelper) {

        function itemReceivedSummaryViewModel() {

            var self = this;
            var getTransaltion = oj.Translations.getTranslatedString;
            self.labels = ko.observable();
            self.tableArray = ko.observableArray([]);
            self.tableSelectedRowKey = ko.observable();
            self.tableSelectedIndex = ko.observable();
            self.oprationdisabled = ko.observable(true);
            self.selectedDataTable = ko.observable();
            self.serviceNameArray = ko.observableArray();
            self.notificationTypeArray = ko.observableArray();
            self.statusArray = ko.observableArray();
            self.tableDataProvider = new ArrayDataProvider(self.tableArray, { idAttributes: 'id' });
            self.serviceListProvider = new ArrayDataProvider(self.serviceNameArray, { keyAttributes: 'value' });
            self.notificationTypeListProvider = new ArrayDataProvider(self.notificationTypeArray, { keyAttributes: 'value' });
            self.statusListProvider = new ArrayDataProvider(self.statusArray, { keyAttributes: 'value' });
            self.selectedServiceName = ko.observable();
            self.selectedNotifiType = ko.observable();
            self.selectedStatus = ko.observable();
            self.approvalBtnDisabled=ko.observable(true);
            self.itemNumber=ko.observable();
            self.showStatusList=ko.observable(false);
            self.rejectReason=ko.observable('');
            //-------------- refresh for language --------------//
            self.refreshView = ko.computed(function () {
                if (app.refreshViewForLanguage()) {
                    initTranslations();
                }
            });
            //------------***************----------------//
            self.tableSelectionListener = function (event) {
                var data = event.detail;
                var currentRow = data.currentRow;
                if (currentRow !== null) {
                    self.tableSelectedRowKey(currentRow['rowKey']);
                    //store current index to pass it to other form when update or view
                    self.tableSelectedIndex(currentRow['rowIndex']);
                    self.oprationdisabled(false);
                    self.selectedDataTable(self.tableArray()[self.tableSelectedIndex()]);
                    if(self.selectedDataTable().type=='FYI' || self.selectedDataTable().status=='CLOSED' ||
                    (self.selectedDataTable().type=='FYA' && self.selectedDataTable().receiverType=='EMP')){
                     self.approvalBtnDisabled(true);
                    }else{
                     self.approvalBtnDisabled(false);
                    }
                    console.log(self.selectedDataTable());
                }
            };

            //------------ buttons action ------------------------//
            self.searchActionBtn = function (event) {
                app.loading(true);
                self.approvalBtnDisabled(true);
                var payload={
                      'status':self.selectedStatus(),
                      'type':self.selectedNotifiType(),
                      'itemNumber':self.itemNumber(),
                      'personId':app.personDetails().personId
                };
                var success = function (data) {
                    app.loading(false);
                    for(var i=0 ; i<data.length ; i++){
                        data[i].notificationCount=i+1;
                        data[i].employeeTitle=(data[i].personName+' / '+data[i].personNumber);
                    };
                    self.tableArray(data);
                };
                var fail = function () {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get data'));
                    app.loading(false);
                };
            
             services.addGeneric('workflowNotification/searchNotification',payload).then(success, fail);

            };

            self.resetActionBtn = function (event) {
                app.loading(true);
                self.approvalBtnDisabled(true);
                self.getAllNotifications();
            };

            self.viewActionBtn = function (event) {
                if(self.selectedDataTable().type=='FYA' && self.selectedDataTable().receiverType=='EMP'){
                    var dataToSend = { operation: 'RMI', data: self.selectedDataTable() };
                }else{
                    var dataToSend = { operation: 'REVIEW', data: self.selectedDataTable() };
                }
                
                localStorage.setItem('itemData', JSON.stringify(dataToSend));
                // oj.Router.rootInstance.store(dataToSend);
                oj.Router.rootInstance.go('itemReceivedOperation');
                app.loading(true);
            };

            //--------------*********----------------//
            //----------- approval area ----------//

            self.openApproveDialog = function () {
                app.getLastApprover(self.selectedDataTable().requestId, self.selectedDataTable().selfType);
                document.getElementById('approveDialog').open();
            };

            self.openRejectDialog = function () {
                document.getElementById('rejectDialog').open();
            };

            self.approveDialogClose = function () {
                document.getElementById('approveDialog').close();
            };

            self.rejectlDialogClose = function () {
                document.getElementById('rejectDialog').close();
            };

            self.approveActionBtn = function (event) {
                app.loading(true);
                self.approvalBtnDisabled(true);
                var data = self.selectedDataTable();
                var headers = {
                    "MSG_TITLE": data.msgTitle,
                    "MSG_BODY": data.msgBody,
                    "TRS_ID": data.requestId,
                    "PERSON_ID": app.personDetails().personId,
                    "RESPONSE_CODE": "APPROVED",
                    "ssType": data.selfType,
                    "PERSON_NAME": data.personName,
                    "rejectReason": self.rejectReason()
                };

                var success = function () {
                    if (app.isLastApprover()) {
                        var updateStatusCBFN = function (data) {
                            app.loading(false);
                            self.getAllNotifications();
                        };
                        services.getGeneric("ItemReceived/updateStatus/" + data.requestId, {}).then(fail, updateStatusCBFN);
                    }else{
                        app.loading(false);
                        self.getAllNotifications();
                    }

                };

                var fail = function () {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'fail in approve '));
                };

                document.getElementById('approveDialog').close();
                services.addGeneric("workflowNotification/workflowAction", headers).then(success, fail);
            };

            self.rejectActionBtn = function (event) {
                app.loading(true);
                self.approvalBtnDisabled(true);
                var data = self.selectedDataTable();
                var headers = {
                    "MSG_TITLE": data.msgTitle,
                    "MSG_BODY": data.msgBody,
                    "TRS_ID": data.requestId,
                    "PERSON_ID": app.personDetails().personId,
                    "RESPONSE_CODE": "REJECTED",
                    "ssType": data.selfType,
                    "PERSON_NAME": data.personName,
                    "rejectReason": self.rejectReason()
                };

                var success = function () {
                    app.loading(false);
                    self.getAllNotifications();
                };

                var fail = function () {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'fail in approve '));
                };

                document.getElementById('rejectDialog').close();
                services.addGeneric("workflowNotification/workflowAction", headers).then(success, fail);
            };
            //--------------***********------------------//
            //------------- handler ----------------//
            self.notificationTypeHandler=function(event){
             var val=event.detail.value;
             if(val=='FYA'){
                self.showStatusList(true);
             }else{
                self.showStatusList(false);
             }
            };
            //--------------*************------------//
            self.connected = function () {
                app.loading(true);
                self.getAllNotifications();
            };

            self.getAllNotifications=function(){
                var payload={
                'managerId':app.personDetails().managerId,
                'managerOfManager':app.personDetails().managerOfManager,
                'personId':app.personDetails().personId
                };

                var success = function (data) {
                    app.loading(false);
                    app.notiCount(data.length);
                    for(var i=0 ; i<data.length ; i++){
                        data[i].notificationCount=i+1;
                        data[i].employeeTitle=(data[i].personName+' / '+data[i].personNumber);
                    };
                    self.tableArray(data);
                };
                var fail = function (data) {
                    app.messagesDataProvider.push(app.createMessage("error", 'Fail to get data'));
                    app.loading(false);
                };
             services.addGeneric('workflowNotification/getAllNotifications',payload).then(success, fail);
            };

            //--------------- translations -------------------//
            function initTranslations() {
                self.labels({
                    notificationScreenLbl: ko.observable(getTransaltion("notifications.title")),
                    searchLbl: ko.observable(getTransaltion("common.search")),
                    resetLbl: ko.observable(getTransaltion("common.reset")),
                    viewLbl: ko.observable(getTransaltion("common.view")),
                    approveLbl: ko.observable(getTransaltion("common.approve")),
                    rejectLbl: ko.observable(getTransaltion("common.reject")),
                    serviceNameLbl: ko.observable(getTransaltion("approvalSetup.serviceName")),
                    notificationTypeLbl: ko.observable(getTransaltion("notifications.notificationType")),
                    statusLbl: ko.observable(getTransaltion("common.status")),
                    placeHolder: ko.observable(getTransaltion("common.placeHolder")),
                    itemNumber:ko.observable(getTransaltion("itemReceived.itemNumber")),
                    approveTitle: ko.observable(getTransaltion("common.approveTitle")),
                    approveMessage: ko.observable(getTransaltion("common.approveMsg")),
                    rejectTitle: ko.observable(getTransaltion("common.rejectTitle")),
                    rejectMessage: ko.observable(getTransaltion("common.rejectMsg")),
                    rejectReason: ko.observable(getTransaltion("common.rejectReason")),
                    yes: ko.observable(getTransaltion("common.yes")),
                    no: ko.observable(getTransaltion("common.no")),
                    columnArray: ko.observableArray([
                        {
                            "headerText": '#', "field": "notificationCount"
                        },
                        {
                            "headerText": getTransaltion('notifications.serviceNameWithEmployee'), "field": "msgBody"
                        },
                        {
                            "headerText": getTransaltion('notifications.employeeNumber'), "field": "employeeTitle"
                        }
                        ,
                        {
                            "headerText": getTransaltion('notifications.notificationType'), "field": "type"
                        },
                        {
                            "headerText": getTransaltion('notifications.creationDate'), "field": "creationDate"
                        }
                    ])
                });
                self.notificationTypeArray([
                    {
                        label: getTransaltion('notifications.fyi'),
                        value: 'FYI'
                    }, {
                        label: getTransaltion('notifications.fya'),
                        value: 'FYA'
                    }
                ]);

                self.statusArray([
                    {
                        label: getTransaltion('notifications.open'),
                        value: 'OPEN'
                    }, {
                        label: getTransaltion('notifications.closed'),
                        value: 'CLOSED'
                    }
                ]);

            }
            initTranslations();
        }

        return itemReceivedSummaryViewModel;


    })