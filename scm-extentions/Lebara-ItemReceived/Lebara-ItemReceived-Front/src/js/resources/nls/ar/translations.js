define({
    // --------------------- common items --------------------
    "common": {
        "add": "إضافه",
        "edit": "تعديل",
        "view": "عرض",
        "viewApproval": "قائمة الموافقات",
        "logOut": "تسجيل خروج",
        "next": "التالي",
        "prev": "السابق",
        "submit": "إرسال",
        "create": "إنشاء",
        "review": "مراجعه",
        "approve": "قبول",
        "reject": "رفض",
        "back": "رجوع",
        "status": "الحالة",
        "remove": "حذف",
        "download": "تحميل",
        "yes": "نعم",
        "no": "لا",
        "placeHolder": "من فضلك اختر قيمة",
        "search": "بحث",
        "reset": "إعادة",
        "approveTitle": "الموافقة علي الطلب",
        "approveMsg": "هل أنت متأكد أنك تريد الموافقة على هذا الطلب؟",
        "rejectTitle": "رفض الطلب",
        "rejectMsg": "هل أنت متأكد أنك تريد رفض هذا الطلب؟",
        "rejectReason": "سبب الرفض",
        "showReport": "إضهار التقرير",
        "save":"حفظ",
        "reason":"السبب",
        "close":"إغلاق"
    },
    // --------------------- login page --------------------
    "login":
    {
        "Username": "اسم المستخدم",
        "Password": "كلمة المرور",
        "enterName": "من فضلك ادخل اسم المستخدم",
        "enterPassword": "من فضلك ادخل كلمة المرور",
        "loginFail": "لا يمكن التحقق من صحة هذا المستخدم",
        "enterNameAndPassword": "من فضللك ادخل اسم المستخدم وكلمة السر",
        "languagelable": "English",
        "Login": "تسجيل دخول",
        "Forgot Password": "نسيت كلمة المرور"
    },
    // --------------------- home page --------------------
    "itemReceived":
    {
        "title": "ملاحظة البنود الواردة",
        "itemNumber": "رقم الملاحظة",
        "receivingDepartment": "قسم الاستلام",
        "supplierName": "اسم المورد",
        "cotractPoNumber": "رقم العقد او امر الشراء",
        "contractPoDate": "تاريخ العقد او رقم الشراء",
        "invoiceNumber": "رقم الفاتورة",
        "invoiceDate": "تاريخ الفاتورة",
        "invoiceTotalAmount": "إجمالي مبلغ الفاتورة",
        "invoiceCurrency": "عملة الفاتورة",
        "exchangeRate": "سعر الصرف",
        "totalAmountInSAR": "المبلغ الإجمالي بالريال السعودي",
        "relatedDocument": "الفاتورة والمستند الداعم ذي الصلة",
        "creationDate": "تاريخ الإنشاء",
        "attachmentError": "من فضلك قم بأختيار ملف الفاتورة او المستند الداعم لها ",
        "submitTitle": "إرسال ملاحظة استلام البند",
        "submitMsg": "هل انت متأكد انك تريد الإرسال ؟",
        "departmentMsg": "من فضلك اختر قسم الاستلام",
        "invoiceCurrencyMsg": "من فضلك اختر عملة الفاتورة",
        "changeRateError": "لم يتم العثور على سعر صرف يرجى تحديد عملة أخرى",
        "comments":"التعليقات",
        "fromDate":"من تاريخ",
        "toDate":"إلي تاريخ",
        "requestMoreInfo":"طلب المزيد من المعلومات",
        "noteDetails":"تفاصيل البند ",
        "requestMoreInfoMsg":"يرجى تقديم سبب إعادة تقديم هذا الطلب",
        "detailsMsg":"من فضلك قم بإضافة تفاصيل البند اولا",
        "closeRequestTitle":"إغلاق هذا الطلب",
        "closeRequestMsg":"هل أنت متأكد انك تريد غلق الطلب ؟",
        "finalStatus":"الحالة النهائية"

    },
    // --------------------- approval setup page --------------------
    "approvalSetup": {
        "title": "إعداد الموافقة",
        "serviceCode": "رمز الخدمة",
        "approvalType": "نوع الموافقة",
        "approvalCode": "رمز الموافقه",
        "approvalOrder": "أمر الموافقة",
        "notificationType": "نوع إعلام",
        "roleName": "اسم الدور",
        "specialCase": "حالة خاصة",
        "submitTitle": "إرسال إعداد الموافقة",
        "submitMsg": "هل أنت متأكد من أنك تريد إضافة إعداد الموافقة؟",
        "serviceName": "اسم الخدمة",
        "deleteTitle": "حذف إعداد الموافقة",
        "deleteMessage": "هل أنت متأكد أنك تريد حذف هذا الإعداد؟"
    },
    // --------------------- notification page --------------------
    "notifications":
    {
        "title": "شاشة الإخطارات",
        "serviceNameWithEmployee": "اسم الخدمة ورقم العنصر",
        "employeeNumber": "أسم الموظف و رقمة",
        "notificationType": "نوع إعلام",
        "creationDate": "تاريخ الإنشاء",
        "fyi": "لمعلوماتك",
        "fya": "للعمل الخاص بك",
        "pending": "في انتظار الموافقة",
        "approved": "وافق",
        "rejected": "مرفوض",
        "all": "الكل",
        "open": "المفتوحة",
        "closed": "المغلقة"
    }


});
