define({
    "root": {
        // --------------------- common items --------------------
        "common": {
            "add": "Add",
            "edit": "Edit",
            "view": "View",
            "viewApproval": "Approval List",
            "logOut": "Log Out",
            "next": "Next",
            "prev": "Previous",
            "submit": "Submit",
            "create": "Create",
            "review": "Review",
            "approve": "Approve",
            "reject": "Reject",
            "back": "Back",
            "status": "Status",
            "remove": "Remove",
            "delete":"Delete",
            "download": "Download",
            "yes": "Yes",
            "no": "No",
            "placeHolder": "Please select value",
            "search":"Search",
            "reset":"Reset",
            "ok":"Ok",
            "approveTitle":"Approve Request",
            "approveMsg":"Are You sure you want to approve this request?",
            "rejectTitle":"Reject Request",
            "rejectMsg":"Are you sure you want to reject this this request?",
            "rejectReason":"Reject Reason",
            "showReport":"Show Report",
            "save":"Save",
            "reason":"Reason",
            "close":"Close"
        },
        // --------------------- login page --------------------
        "login": {
            "Username": "UserName",
            "Password": "Password",
            "enterName": "Please enter user your name",
            "enterPassword": "Please enter your password",
            "enterNameAndPassword": "Please enter user name and password",
            "loginFail": "Cann't validate this user ",
            "languagelable": "عربي",
            "Login": "Login",
            "Forgot Password": "Forget Password ?"
        },
        // --------------------- home page --------------------
        "itemReceived":
        {
            "title": "Items Received Note",
            "itemNumber": "Note Number",
            "receivingDepartment": "Receiving Department",
            "supplierName": "Supplier Name",
            "cotractPoNumber": "Contrat or Purchase Order Number",
            "contractPoDate": "Contract or Purchase Order Date",
            "invoiceNumber": "Invoice Number",
            "invoiceDate": "Invoice Date",
            "invoiceTotalAmount": "Invoice Total Amount",
            "invoiceCurrency": "Invoice Currency",
            "exchangeRate": "Exchange Rate",
            "totalAmountInSAR": "Total Amount in SAR",
            "relatedDocument": "Invoice and related supporting document",
            "creationDate": "Creation Date",
            "attachmentError": "Please add invoice and related supporting document first",
            "submitTitle": "Submit Items Received Note",
            "submitMsg": "Are you sure you want to submit?",
            "departmentMsg": "Please select receiving department",
            "invoiceCurrencyMsg": "Please select invoice currency",
            "changeRateError":"No exchange rate found please select anthor currency",
            "comments":"Comments",
            "fromDate":"From Date",
            "toDate":"To Date",
            "requestMoreInfo":"Request More Information",
            "noteDetails":"Note Details",
            "requestMoreInfoMsg":"Please provide the information you need",
            "detailsMsg":"Please add items details first",
            "closeRequestTitle":"Close Item note received",
            "closeRequestMsg":"Sure to close this item ?",
            "finalStatus":"Final Status"

        },
       // --------------------- approval setup page --------------------
        "approvalSetup":{
            "title":"Approval Setup",
            "serviceCode":"Service Code",
            "approvalType":"Approval Type",
            "approvalCode":"Approval Code",
            "approvalOrder":"Approval Order",
            "notificationType":"Notification Type",
            "roleName":"Role Name",
            "specialCase":"Special Case",
            "submitTitle":"Approval Setup Submit",
            "submitMsg":"Are you sure you want to add approval setup ?",
            "serviceName":"Service Name",
            "deleteTitle":"Approval Setup Delete",
            "deleteMessage":"Are you sure you want to delete this setup ?"
        },
        // --------------------- notification page --------------------
        "notifications":
        {
            "title": "Notifications Screen",
            "serviceNameWithEmployee":"Service Name and Note Number",
            "employeeNumber":"Employee Name and Number",
            "notificationType":"Notification Type",
            "creationDate":"Creation Date",
            "fyi":"For Your Information",
            "fya":"For Your Action",
            "pending":"Pending for Approve",
            "approved":"Approved",
            "rejected":"Rejected",
            "all":"All",
            "open":"Open",
            "closed":"Closed"
        }

    },
    "ar": true,
    "cs": true,
    "id": true
});
