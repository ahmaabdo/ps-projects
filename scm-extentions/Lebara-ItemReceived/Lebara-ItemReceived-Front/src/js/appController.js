define(['knockout', 'ojs/ojmodule-element-utils', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils',
  'ojs/ojrouter', 'ojs/ojarraydataprovider', 'ojs/ojknockouttemplateutils', 'jquery', 'config/services',
  'util/commonhelper', 'ojs/ojconverterutils-i18n', 'ojs/ojmodule-element', 'ojs/ojknockout',
  'ojs/ojlabel', 'ojs/ojavatar', 'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojmessage'],
  function (ko, moduleUtils, ResponsiveUtils, ResponsiveKnockoutUtils, Router, ArrayDataProvider,
    KnockoutTemplateUtils, $, services, commonhelper, ConverterUtilsI18n) {
    function ControllerViewModel() {
      var self = this;
      self.KnockoutTemplateUtils = KnockoutTemplateUtils;
      // Media queries for repsonsive layouts
      var smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
      self.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
      var getTranslation = oj.Translations.getTranslatedString;
      var pageBody = $('*');
      self.username = ko.observable('');
      self.password = ko.observable('');
      self.userLogin = ko.observable();
      self.loginUser = ko.observable();
      self.loginFailureText = ko.observable();
      self.personDetails = ko.observableArray([]);
      self.personNumber = ko.observable();
      self.lastInvoiceNumber = ko.observable();
      self.agreementArr = ko.observableArray([]);
      self.receiptArr = ko.observableArray([]);
      self.requisitionArr = ko.observableArray([]);
      self.receiptLinesArr = ko.observableArray([]);
      self.labels = ko.observable();
      self.language = ko.observable();
      var storage = window.localStorage;
      self.jwt = ko.observable('');
      self.hostUrl = ko.observable('');
      self.isUserLoggedIn = ko.observable(false);
      self.loginVaild = ko.observable(false);
      self.avatarSize = ko.observable("xxs");
      self.initials = ko.observable();
      self.firstName = ko.observable();
      self.lastName = ko.observable();
      self.image = ko.observable();
      self.selectedMenuItem = ko.observable("");
      self.refreshViewForLanguage = ko.observable(false);
      self.notiCount = ko.observable(0);
      self.isLastApprover =ko.observable();
      self.approverDetails=ko.observable();
      self.departmentList=ko.observableArray([]);
      var selectedLanguage = localStorage.getItem("selectedLanguage");
      if (selectedLanguage !== null && (selectedLanguage === "en-US" || selectedLanguage === "ar")) {
        setLocale(selectedLanguage);
      }

      //--------------- define messages notification ---------------//
      self.messagesDataProvider = ko.observableArray();
      self.position =
      {
        "my": { "vertical": "top", "horizontal": "end" },
        "at": { "vertical": "top", "horizontal": "end" },
        "of": "window"
      };
      self.closeMessageHandler = function (event) {
        self.messagesDataProvider.remove(event.detail.message);
      };

      self.createMessage = function (type, summary) {
        return {
          severity: type,
          summary: summary,
          autoTimeout: 10000
        };
      };
      //------------------------------------------------------------//
      // Router setup
      self.router = Router.rootInstance;
      self.router.configure({
        'home': { label: 'Home', value: "home", title: "Home", isDefault: true },
        'itemReceivedSummary': { label: 'Items Received Note', value: "itemReceived/itemReceivedSummary", title: "Items Received Note" },
        'itemReceivedOperation': { label: 'Item Received Operation', value: "itemReceived/itemReceivedOperation", title: "Items Received Note" },
        'approvalSetupSummary': { label: 'Approval Setup', value: "approvalSetup/approvalSetupSummary", title: "Approval Setup" },
        'approvalSetupOperation': { label: 'Approval Setup Add', value: "approvalSetup/approvalSetupOperation", title: "Approval Setup Add" },
        'notificationScreen': { label: 'Notifications', value: "notifications/notificationScreen", title: "Notifications" }
      });
      Router.defaults['urlAdapter'] = new Router.urlParamAdapter();

      self.loadModule = function () {
        self.moduleConfig = ko.pureComputed(function () {
          var name = self.router.moduleConfig.name();
          var viewPath = 'views/' + name + '.html';
          var modelPath = 'viewModels/' + name;
          return moduleUtils.createConfig({
            viewPath: viewPath,
            viewModelPath: modelPath, params: { parentRouter: self.router }
          });
        });
      };


      //--------------- define messages notification ---------------//
      self.messagesDataProvider = ko.observableArray();
      self.position =
      {
        "my": { "vertical": "top", "horizontal": "end" },
        "at": { "vertical": "top", "horizontal": "end" },
        "of": "window"
      };
      self.closeMessageHandler = function (event) {
        self.messagesDataProvider.remove(event.detail.message);
      };

      self.createMessage = function (type, summary) {
        return {
          severity: type,
          summary: summary,
          autoTimeout: 10000
        };
      };
      //------------------------------------------------------------//
      //--------------------- language changing --------------------------
      function getLocale() {
        return oj.Config.getLocale();

      };
      function setLocale(lang) {
        oj.Config.setLocale(lang,
          function () {
            self.refreshViewForLanguage(false);
            if (lang === 'ar') {
              $("html").attr("lang", lang);
              $("html").attr("dir", "rtl");
            } else {
              $("html").attr("lang", lang);
              $("html").attr("dir", "ltr");
            }
            initTranslations();
            self.refreshViewForLanguage(true);
          }
        );
      };
      self.switchLanguage = function () {
        if (getLocale() === "ar") {
          setLocale("en-US");
          localStorage.setItem("selectedLanguage", "en-US");
        } else if (getLocale() === "en-US") {
          setLocale("ar");
          localStorage.setItem("selectedLanguage", "ar");
        }

      };

      /************** profile menu action **************/
      self.menuItemAction = function (event) {
        self.selectedMenuItem(event.target.value);
      };
      //     ----------------------------------------  loading function ------------------------ 

      self.loading = function (load) {
        if (load) {
          $('#loading').show();
        } else {
          $('#loading').hide();
        }
      };

      //     ---------------------------------------- enter to login  ------------------------ 
      $(function () {
        $('#userNameInput').keypress(function (ev) {
          var mobEvent = ev;
          var mobPressed = mobEvent.keyCode;
          if (mobPressed == 13) {
            ev.preventDefault();
            self.userName(document.getElementById("userNameInput").value);
            self.validateUser();
          }
          return true;
        });
      });

      $(function () {
        $('#passwordInput').keypress(function (ev) {
          var mobEvent = ev;
          var mobPressed = mobEvent.keyCode;
          if (mobPressed == 13) {
            ev.preventDefault();
            self.password(document.getElementById("passwordInput").value);
            self.validateUser();
          }
          return true;
        });
      });

      //     ---------------------------------------- login button function------------------------ 

      self.validateUser = function () {
        // self.loading(true);
        self.personDetails('');
        $("#loginButton").addClass("loader");
        self.loading() ? pageBody.css('cursor', 'wait') : pageBody.css('cursor', '');
        var username = self.username();
        var password = self.password();
        // Here we check null value and show error to fill
        if (!username && !password) {
          $("#errorDiv").addClass("fa fa-exclamation");
          self.loginFailureText(self.labels().userAndPassReq());
          $("#loginButton").removeClass("loader");
        } else if (!username) {
          $("#errorDiv").addClass("fa fa-exclamation");
          self.loginFailureText(self.labels().userNameRequired());
          $("#loginButton").removeClass("loader");
        } else if (!password) {
          $("#errorDiv").addClass("fa fa-exclamation");
          self.loginFailureText(self.labels().passwordRequired());
          $("#loginButton").removeClass("loader");
        } else {
          $('#loader-overlay').show();
          var loginCbFn = async function (data) {
            self.loginFailureText('');
            getEmployeeData(services,self);
            if (data.result == true) {
              self.loading(true);
              $("#loginButton").removeClass("loader");
              console.log(data)
              self.isUserLoggedIn(true);
              self.loginFailureText('');
              sessionStorage.setItem('username', self.isUserLoggedIn());
              sessionStorage.setItem('isUserLoggedIn', self.isUserLoggedIn());
              sessionStorage.setItem('Username', self.username());
              self.router.go('home');
             
              // getNotificationCount();
              $('#loader-overlay').hide();
            } else {
              $("#loginButton").removeClass("loader");
              // If authentication failed
              // alert(response.Error);
              self.loginFailureText('');
              $("#errorDiv").addClass("fa fa-exclamation");
              self.loginFailureText(self.labels().loginfail());
              self.loginUser(username);
              return;
            }
          };

          var loginFailCbFn = function () {
            $("#errorDiv").addClass("fa fa-exclamation");
            self.loginFailureText(self.labels().loginfail());
            $("#loginButton").removeClass("loader");
            return;
          };

          var payload = {
            "userName": username,
            "password": password
          };
          services.authenticate(payload).then(loginCbFn, loginFailCbFn);
          return;
        }
      };

      //     ---------------------------------------- home icon function------------------------ 

      $(document).on('click', '#homeIcon', function () {
        oj.Router.rootInstance.go('itemReceivedSummary');
      });

      //     ---------------------------------------- notification icon function ------------------------ 

      $(document).on('click', '#notificationIcon', function () {
        oj.Router.rootInstance.go('notificationScreen');
      });
      //     ---------------------------------------- approval Setup icon function ------------------------ 

      $(document).on('click', '#approvalSetup', function () {
        oj.Router.rootInstance.go('approvalSetupSummary');
      });
      //     ---------------------------------------- log out button function------------------------ 

      self.logOutBtn = function (event) {
        // if (event.target.value === 'out') {
        self.router.go('home');
        location.reload();
        self.isUserLoggedIn(false);
        self.validateUser(false);
        sessionStorage.setItem('username', '');
        sessionStorage.setItem('isUserLoggedIn', '');
        if (self.language() === 'english') {
          $('html').attr({ 'dir': 'ltr' });
          $('#login-formId').removeClass('login-formright');
          $('#login-formId').addClass('login-formleft');
        } else if (self.language() === 'arabic') {
          $('html').attr({ 'dir': 'rtl' });
          $('#login-formId').addClass('login-formright');
          $('#login-formId').removeClass('login-formleft');
        }

      };

      // --------------- check for last step approver -----------------------//
      self.getLastApprover = function (transationId, service_code) {
        //isOperationScreen : When isOperationScreen :  
        var roleType, roleId;
        var isLast;
        var getLastApprove = function (data) {
            if (data.length) {
                roleType = data[0].rolrType;
                roleId = data[0].roleId;
                if ((roleType == "LINE_MANAGER" || roleType == "LINE_MANAGER+1" || roleType == "EMP" || roleType == "chief_officer") && roleId == self.personDetails().personId) {
                    self.isLastApprover (true);
                }else if(roleType == "Special_Case" && roleId == self.personDetails().personId){
                  self.isLastApprover (true);
                }else{
                  self.isLastApprover (false);
                }
            } else {
              self.isLastApprover (true);
            }
        };

        var fail=function(){
          self.messagesDataProvider.push(self.createMessage('error', "Cann't get last approver"));
        };
        services.getGeneric('approvalList/lastStep/'+ transationId + "/" + service_code).then(getLastApprove, fail);
        
    };


      // --------------------------------------------------------------
      self.computeIconsArray = ko.computed(function () {
        if (self.personDetails().picBase64 === undefined || self.personDetails().picBase64 === null || self.personDetails().picBase64 === "") {
          self.image('');
        } else {
          self.image("data:image/png;base64," + self.personDetails().picBase64);

        }
        if (self.personDetails().picBase64) {
          if ($(".profilepic")[0]) {
            $(".profilepic")[0].src = "data:image/png;base64," + self.personDetails().picBase64;
          }
          if ($(".profilepic")[1]) {
            $(".profilepic")[1].src = "data:image/png;base64," + self.personDetails().picBase64;
          }
        } else {
                  var firstName=self.personDetails().firstName==null?'No':self.personDetails().firstName;
                  var lastName=self.personDetails().lastName==null?'Name':self.personDetails().lastName;
                  self.initials(ConverterUtilsI18n.IntlConverterUtils.getInitials(firstName,lastName));
        }
      });
      //     ---------------------------------------- call jwt from helper ------------------------ 

      self.refresh = function () {
        self.loading(true);
        console.log('refresh')
        self.personDetails('');
        $.when(jwt(self,services, false, commonhelper)).done(function () {
            if (self.loginVaild()) {
              self.router.go('home');
              
            } else {
              self.loading(false);
              self.isUserLoggedIn(false);
            }
          });
      
      };
      self.refresh();

      //-------------- translations 
      function initTranslations() {
        self.labels({
          localeLogin: ko.observable(getTranslation('login.Login')),
          UsernameLabel: ko.observable(getTranslation('login.Username')),
          passwordLabel: ko.observable(getTranslation('login.Password')),
          loginword: ko.observable(getTranslation('login.Login')),
          forgotPasswordLabel: ko.observable(getTranslation('login.Forgot Password')),
          loginBtnLbl: ko.observable(getTranslation('login.Login')),
          languagelable: ko.observable(getTranslation("login.languagelable")),
          userNameRequired: ko.observable(getTranslation("login.enterName")),
          passwordRequired: ko.observable(getTranslation("login.enterPassword")),
          loginfail: ko.observable(getTranslation("login.loginFail")),
          userAndPassReq: ko.observable(getTranslation("login.enterNameAndPassword")),
          // ---------------------- pages name --------------------------
          homePageLbl: ko.observable(getTranslation("home.title")),
          secondPageLbl: ko.observable(getTranslation("secondOne")),
          notificationPageLbl: ko.observable(getTranslation("notification.title")),
          logOutLbl: ko.observable(getTranslation("common.logOut"))
        });
        if (document.getElementById("userNameInput") != null) {
          document.getElementById("userNameInput").placeholder = getTranslation('login.Username');
          document.getElementById("passwordInput").placeholder = getTranslation('login.Password');
        }
      }
      initTranslations();
    }

    return new ControllerViewModel();
  }
);
