/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
    'ojs/ojoffcanvas'],
        function (oj, ko, moduleUtils) {
            function ControllerViewModel() {
                var self = this;
                
                self.localeProjects = ko.observable();
                self.localeDevices = ko.observable();
                
                self.titleName = ko.observable();

                // Media queries for repsonsive layouts
                var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
                self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);

                self.reloadlanguage = function () {
                    function reloadTranslatedString() {
                        self.localeProjects(oj.Translations.getTranslatedString("Projects"));
                        self.localeDevices(oj.Translations.getTranslatedString("Devices"));
                        
                        self.localeYear = ko.observable(oj.Translations.getTranslatedString("Year"));
                        self.localeQuarter = ko.observable(oj.Translations.getTranslatedString("Quarter"));
                        self.localeEntities = ko.observable(oj.Translations.getTranslatedString("Entities"));
                        self.localePrograms = ko.observable(oj.Translations.getTranslatedString("Projects"));
                        self.localeObjectives = ko.observable(oj.Translations.getTranslatedString("Objectives"));
                    }
                    localStorage.setItem('lang', "ar-sa");
                    localStorage.setItem('setLang', "arabic");
                    var newlang = localStorage.getItem('setLang');

                    if (!newlang)
                    {
                        localStorage.setItem('lang', newlang);
                    }
                    switch (newlang) {
                        case 'arabic':
                            newLang = 'ar-sa';
                            break;
                        default:
                            newLang = 'en-US';
                    }
                    oj.Config.setLocale(newLang,
                            function () {
                                $('html').attr('lang', newLang);
                                if (newLang === 'ar-sa') {
                                    $('html').attr('dir', 'rtl');
                                    reloadTranslatedString();

                                } else {
                                    $('html').attr('dir', 'ltr');
                                    reloadTranslatedString();
                                }
                            }
                    );
                    console.log(newlang);
                }

                self.reloadlanguage();
                // Router setup
                self.router = oj.Router.rootInstance;
                self.router.configure({
                    'home': {label: "Home", isDefault: true},
                    'projects': {label: self.localeProjects},
                    'devices': {label: self.localeDevices}
                });
                oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});

                self.loadModule = function () {
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath})
                        ]);
                        masterPromise.then(
                                function (values) {
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                },
                                function (reason) {}
                        );
                    });
                };

                // Navigation setup
                self.hideBackButton = ko.observable(false);
                var navData = [
                    {name: "الرئيسية", id: 'home',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24'}
//                    {name: self.localeProjects, id: 'projects',
//                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'},
//                    {name: self.localeDevices, id: 'devices',
//                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-fire-icon-24'}
                ];
                self.navDataSource = new oj.ArrayTableDataSource(navData, {idAttribute: 'id'});

                // Drawer
                // Close offcanvas on medium and larger screens
                self.mdScreen.subscribe(function () {
                    oj.OffcanvasUtils.close(self.drawerParams);
                });
                self.drawerParams = {
                    displayMode: 'push',
                    selector: '#navDrawer',
                    content: '#pageContent'
                };
                // Called by navigation drawer toggle button and after selection of nav drawer item
                self.toggleDrawer = function () {
                    return oj.OffcanvasUtils.toggle(self.drawerParams);
                }
                // Add a close listener so we can move focus back to the toggle button when the drawer closes
                $("#navDrawer").on("ojclose", function () {
                    $('#drawerToggleButton').focus();
                });

            }

            return new ControllerViewModel();
        }
);
