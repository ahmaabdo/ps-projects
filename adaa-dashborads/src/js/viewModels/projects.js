/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojknockout', 'ojs/ojchart', 'ojs/ojtoolbar', 'ojs/ojmasonrylayout', 'ojs/ojavatar', 'ojs/ojselectcombobox', 'ojs/ojbutton', 'ojs/ojdialog', 'ojs/ojtable', 'ojs/ojarraydataprovider', 'jet-composites/adaa-card/loader', 'ojs/ojtrain', 'ojs/ojcollapsible', 'ojs/ojpopup', 'ojs/ojswitch', 'ojs/ojgauge'],
        function (oj, ko, $, app) {

            function DashboardViewModel() {
                var self = this;
                self.localeYear = ko.observable(oj.Translations.getTranslatedString("Year"));
                self.localeQuarter = ko.observable(oj.Translations.getTranslatedString("Quarter"));
                self.localeEntities = ko.observable(oj.Translations.getTranslatedString("Entities"));
                self.localePrograms = ko.observable(oj.Translations.getTranslatedString("Projects"));
                self.localeObjectives = ko.observable(oj.Translations.getTranslatedString("Objectives"));
                self.yearSelectVal = ko.observable("2018");
                self.selectedYearValue = ko.observable("Q22018");
                self.quarterSelectVal = ko.observable("Q2");
                self.selectedStepValue = ko.observable('stp1');
                self.entitesNumbers = ko.observable("");
                self.objectiveNumbers = ko.observable("");
                self.inititivesNumbers = ko.observable("");
                self.KPINumbers = ko.observable("");
                self.KPITotal = ko.observable("");
                self.INITotal = ko.observable("");

                self.kpiCode = ko.observable();
                self.iniCode = ko.observable();
                self.kpiDetailsArray = ko.observableArray([]);
                self.INIArabicNameDialog = ko.observable("");
                self.KPIArabicNameDialog = ko.observable("");

                self.q1Array = ko.observableArray([]);
                self.q2Array = ko.observableArray([]);
                self.q3Array = ko.observableArray([]);
                self.q4Array = ko.observableArray([]);
                //ini header
                self.formInivName = ko.observable("لا توجد بيانات");
                self.formEntityName = ko.observable("لا توجد بيانات");
                self.formInivDescription = ko.observable("لا توجد بيانات");
                self.formV2030LevelDesc = ko.observable("لا توجد بيانات");
                self.formPlannedStartDate = ko.observable("لا توجد بيانات");
                self.formPlannedFinishDate = ko.observable("لا توجد بيانات");
                //first header
                self.formEntityCode = ko.observable("لا توجد بيانات");
                self.formCode = ko.observable("لا توجد بيانات");
                self.formName = ko.observable("لا توجد بيانات");
                self.formDesc = ko.observable("لا توجد بيانات");
                self.formKPIName = ko.observable("لا توجد بيانات");
                self.formKPIDesc = ko.observable("لا توجد بيانات");
                //second header
                self.formBaseLineValue = ko.observable("لا توجد بيانات");
                self.formBaseLineYear = ko.observable("لا توجد بيانات");
                self.formFrequency = ko.observable("لا توجد بيانات");
                self.formPolarity = ko.observable("لا توجد بيانات");
                self.formKMeasurementUnit = ko.observable("لا توجد بيانات");
                self.maxAxisData = ko.observable(null);
                self.formFormula = ko.observable("لا توجد بيانات");
                self.formDatasourceAr = ko.observable("لا توجد بيانات");
                self.formActual = ko.observable("لا توجد بيانات");
                self.formReportingDate = ko.observable("لا توجد بيانات");
                self.formDataAvailableDate = ko.observable("لا توجد بيانات");
                self.formRegionalValue = ko.observable("لا توجد بيانات");
                self.formRegionalCountry = ko.observable("لا توجد بيانات");
                self.formRegionalYear = ko.observable("لا توجد بيانات");
                self.formRegionalSource = ko.observable("لا توجد بيانات");
                self.formInterValue = ko.observable("لا توجد بيانات");
                self.formInterCountry = ko.observable("لا توجد بيانات");
                self.formInterYear = ko.observable("لا توجد بيانات");
                self.formInterSource = ko.observable("لا توجد بيانات");
                self.allLineChartsData = ko.observableArray([]);
                self.barGroupsValue = ko.observableArray([]);

                self.q1y17 = ko.observable();
                self.q1y18 = ko.observable();
                self.q1y19 = ko.observable();
                self.q1y20 = ko.observable();

                self.q2y17 = ko.observable();
                self.q2y18 = ko.observable();
                self.q2y19 = ko.observable();
                self.q2y20 = ko.observable();

                self.q3y17 = ko.observable();
                self.q3y18 = ko.observable();
                self.q3y19 = ko.observable();
                self.q3y20 = ko.observable();

                self.q4y16 = ko.observable();
                self.q4y17 = ko.observable();
                self.q4y18 = ko.observable();
                self.q4y19 = ko.observable();
                self.q4y20 = ko.observable();
                
                self.sum2016 = ko.observable();
                self.sum2017 = ko.observable();
                self.sum2018 = ko.observable();
                self.sum2019 = ko.observable();
                self.sum2020 = ko.observable();


                self.mvalue1 = ko.observable(20);
                self.referenceLines = [{value: 33, color: 'red'}, {value: 67, color: 'green'}];
                self.getTooltip = function (data) {
                    var tooltip;
                    if (data.componentElement.id == "gauge")
                        tooltip = "Value: " + data.label + "<br>Reference Lines: Low 33, Medium 67, High 100";
                    else
                        tooltip = "Value: " + data.label + "<br>Thresholds: Low 33, Medium 67, High 100";
                    return {insert: tooltip};
                };

                self.committeeHead = ko.observable("لا توجد بيانات");
                self.committeeDescAR = ko.observable("لا توجد بيانات");
                self.committeeProgramName = ko.observable("لا توجد بيانات");
                self.committeeHeadIcon = ko.observable("");
                self.programObject = ko.observable("");
                self.areaSeries = ko.observableArray([]);
                self.areaGroups = ko.observableArray([]);
                self.checkLineChart = ko.observable(false);
                
                self.objType = ko.observable(false);
                self.objType2 = ko.observable(false);
                self.objType3 = ko.observable(true);
                self.programCardNotNull = ko.observable(true);
                self.objTypeNull = ko.observable();
                self.toggleObjType = ko.computed(function () {
                    return (self.objType() ? "INI" : "KPI");
                }, this);

                self.pushAllLineChartsData = function () {
                    self.currentLevel('Object');
                    kpiToggleText();
                    query();
                };

                self.toggleCustomCharts = function () {
                    kpiToggleText()
                };


                function kpiToggleText() {
                    if (self.objType()) {
                        $('.swtich-ini-text').css({'color': '#333333', 'font-weight': '700', 'font-size': '14px'});
                        $('.swtich-kpi-text').css({'color': '#808185', 'font-weight': '300', 'font-size': '14px'});
                    } else {
                        $('.swtich-kpi-text').css({'color': '#333333', 'font-weight': '700', 'font-size': '14px'});
                        $('.swtich-ini-text').css({'color': '#808185', 'font-weight': '300', 'font-size': '14px'});
                    }

                    if (self.objType2()) {
                        $('.swtich-cards').css({'color': '#333333', 'font-weight': '700', 'font-size': '14px'});
                        $('.swtich-charts').css({'color': '#808185', 'font-weight': '300', 'font-size': '14px'});
                        $('#oweidi1').fadeIn();
                        $('oj-masonry-layout').fadeOut();
                    } else {
                        $('.swtich-charts').css({'color': '#333333', 'font-weight': '700', 'font-size': '14px'});
                        $('.swtich-cards').css({'color': '#808185', 'font-weight': '300', 'font-size': '14px'});
                        $('oj-masonry-layout').fadeIn();
                        $('#oweidi').fadeOut();
                        $('#oweidi1').fadeOut();

                    }

                    if (self.objType2()) {
                        if (self.objType3()) {
                            $('.swtich-Liker-charts').css({'color': '#333333', 'font-weight': '700', 'font-size': '14px'});
                            $('.swtich-Likert-charts').css({'color': '#808185', 'font-weight': '300', 'font-size': '14px'});
                            $('#oweidi').fadeIn();
                            $('#oweidi1').fadeOut();
                        } else {
                            $('.swtich-Likert-charts').css({'color': '#333333', 'font-weight': '700', 'font-size': '14px'});
                            $('.swtich-Liker-charts').css({'color': '#808185', 'font-weight': '300', 'font-size': '14px'});
                            $('#oweidi1').fadeIn();
                            $('#oweidi').fadeOut();
                        }
                    }

                }
                ;

                function kpiNullText() {
                    if (self.objTypeNull()) {
                        $('#switch-kpi-null').fadeIn();
                    } else {
                        $('#switch-kpi-null').fadeOut();
                    }
                }
                ;

                self.committeeDialogClosed = function () {
                    self.committeeHead("لا توجد بيانات");
                    self.committeeDescAR("لا توجد بيانات");
                    self.committeeProgramName("لا توجد بيانات");
                    self.committeeHeadIcon("");
                    // enableScroll();
                }
                self.inititiveDetailsClosed = function () {
                    // enableScroll();
                }
                self.kpiDetailsTableClosed = function () {
                    // enableScroll();
                }

                self.dialogOpen = function (event) {

                    $("#kpiDetailsTable_layer_overlay").click(function () {
                        document.querySelector("#kpiDetailsTable").close();
                    });
                    $("#kpiRowDetailsTable_layer_overlay").click(function () {
                        document.querySelector("#kpiRowDetailsTable").close();
                    });
                    $("#iniRowDetailsTable_layer_overlay").click(function () {
                        document.querySelector("#iniRowDetailsTable").close();
                    });

                    $("#inititiveDetailsTable_layer_overlay").click(function () {
                        document.querySelector("#inititiveDetailsTable").close();
                    });
                    $("#committeeDialog_layer_overlay").click(function () {
                        document.querySelector("#committeeDialog").close();
                    });

                };

                var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };
                var scrollWindow = document.getElementsByTagName("oj-dialog");

                function preventDefault(e) {
                    e = e || window.event;
                    if (e.preventDefault)
                        e.preventDefault();
                    e.returnValue = false;
                }

                function preventDefaultForScrollKeys(e) {
                    if (keys[e.keyCode]) {
                        preventDefault(e);
                        return false;
                    }
                }

                function disableScroll() {
                    if (window.addEventListener) // older FF
                        window.addEventListener('DOMMouseScroll', preventDefault, false);
                    window.onwheel = preventDefault; // modern standard
                    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
                    window.ontouchmove = preventDefault; // mobile
                    window.onkeydown = preventDefaultForScrollKeys;

                    //enable scrolling for dialog
                    if (scrollWindow.removeEventListener)
                        scrollWindow.removeEventListener('DOMMouseScroll', preventDefault, false);
                    scrollWindow.onmousewheel = document.onmousewheel = null;
                    scrollWindow.onwheel = null;
                    scrollWindow.ontouchmove = null;
                    scrollWindow.onkeydown = null;
                }

                function enableScroll() {
                    if (window.removeEventListener)
                        window.removeEventListener('DOMMouseScroll', preventDefault, false);
                    window.onmousewheel = document.onmousewheel = null;
                    window.onwheel = null;
                    window.ontouchmove = null;
                    window.onkeydown = null;
                }

                self.onValueChanged = function (event) {
                      if(event.detail.value.length > 0) {
                        self.kpiCode(event.detail.value[0].startKey.row[0]);
                    }
                    
                    self.q1Array([]);
                    self.q2Array([]);
                    self.q3Array([]);
                    self.q4Array([]);
                    self.areaSeries([]);
                    self.areaGroups([]);
                    self.checkLineChart(false);
                    var items = [];
                    var targets = [];
                    var areaGroups = [];
                    var areaSeries = [];
                    var nameKPI;


                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {

                            var query = 'SELECT Status,internationalValue,InternationalyearYear,InternationalSourceAr,InternationalCountryAr,RegionalValue,RegionalYear,SUBSTR(Data_Available_Date,0,11) as Data_Available_Date,regionalSourceAr,regionalCountryAr,ObejctiveCode,EntityCode,Code,ObjectiveNameArabic,SUBSTR(reportingYear,3,5)  ||  \'-\' || reportingquarter as area,ObjectiveDescriptionArabic,\' \' ,KPINameArabic,Description_Ar,BaselineValue,BaselineYear,Polarity,FrequencyArabic,MeasurementUnitAr,FormulaAr,DatasourceAr,Actual,ReportingYear,ReportingQuarter,[replace](ReportingDate, \' \', \'/\') as ReportingDate, Target FROM mytable where Code = \'' + self.kpiCode() + '\' order by reportingYear DESC,reportingquarter  DESC';

                            txn.executeSql(query, [], function (tx, res) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    Object.keys(res.rows.item(0)).forEach(function (key) {
                                        if (res.rows.item(0)[key] === 'NULL') {
                                            res.rows.item(0)[key] = 'لا توجد بيانات';
                                        }
                                    });
                                    self.formEntityCode(res.rows.item(0).ObejctiveCode);
                                    self.formCode(res.rows.item(0).Code);
                                    self.formName(res.rows.item(0).ObjectiveNameArabic);
                                    self.formDesc(res.rows.item(0).ObjectiveDescriptionArabic);
                                    self.formKPIName(res.rows.item(0).KPINameArabic);
                                    self.formKPIDesc(res.rows.item(0).Description_Ar);
                                    //second header
                                    self.formBaseLineValue(res.rows.item(0).BaselineValue);
                                    self.formBaseLineYear(res.rows.item(0).BaselineYear);
                                    self.formFrequency(res.rows.item(0).FrequencyArabic);
                                    self.formPolarity(res.rows.item(0).Polarity);
                                    self.formKMeasurementUnit(res.rows.item(0).MeasurementUnitAr);
                                    self.formFormula(res.rows.item(0).FormulaAr);
                                    self.formDatasourceAr(res.rows.item(0).DatasourceAr);
                                    self.formActual(res.rows.item(0).Actual);
                                    self.formReportingDate(res.rows.item(0).ReportingDate);
                                    self.formDataAvailableDate(res.rows.item(0).Data_Available_Date);
                                    self.formRegionalValue(res.rows.item(0).RegionalValue);
                                    self.formRegionalCountry(res.rows.item(0).regionalCountryAr);
                                    self.formRegionalYear(res.rows.item(0).RegionalYear);
                                    self.formRegionalSource(res.rows.item(0).regionalSourceAr);
                                    self.formInterValue(res.rows.item(0).internationalValue);
                                    self.formInterCountry(res.rows.item(0).InternationalCountryAr);
                                    self.formInterYear(res.rows.item(0).InternationalyearYear);
                                    self.formInterSource(res.rows.item(0).InternationalSourceAr);
                                    
                                    if(self.formKMeasurementUnit() === 'نسبة مئوية (%)'){
                                        self.formKMeasurementUnit('نسبة مئوية');
                                        self.maxAxisData(90);
                                    }else if(self.formKMeasurementUnit() === 'NULL'){
                                        self.formKMeasurementUnit('غير معرفة');
                                    }else{
                                        self.maxAxisData(null);
                                    }
                                }
                                for (var i = 0; i < res.rows.length; i++) {
                                    var itemsarray = {};
                                    var itemsarray2 = {};
                                    if (res.rows.item(i).Target === 'NULL' || res.rows.item(i).Target === 'لا توجد بيانات' )
                                        res.rows.item(i).Target = 0;
                                    if (res.rows.item(i).ReportingQuarter == "Q1") {
                                        self.q1Array().push({
                                            reportingYear: res.rows.item(i).ReportingYear,
                                            target: res.rows.item(i).Target
                                        });

                                    }
                                    if (res.rows.item(i).ReportingQuarter == "Q2") {
                                        self.q2Array().push({
                                            reportingYear: res.rows.item(i).ReportingYear,
                                            target: res.rows.item(i).Target
                                        });

                                    }
                                    ;
                                    if (res.rows.item(i).ReportingQuarter == "Q3") {
                                        self.q3Array().push({
                                            reportingYear: res.rows.item(i).ReportingYear,
                                            target: res.rows.item(i).Target
                                        });

                                    }
                                    ;
                                    if (res.rows.item(i).ReportingQuarter == "Q4") {
                                        self.q4Array().push({
                                            reportingYear: res.rows.item(i).ReportingYear,
                                            target: res.rows.item(i).Target
                                        });

                                    }
                                    ;

                                    self.kpiDetailsArray.push(res.rows.item(i));
                                    function loadData(){
                                        areaGroups.push(res.rows.item(i).area);
                                        itemsarray.y = res.rows.item(i).Actual;
                                        switch(res.rows.item(i).Status){
                                            case 'Yellow': itemsarray.color = '#FBd903'; break;
                                            case 'Green': itemsarray.color = '#009A84'; break;
                                            case 'Red': itemsarray.color = '#C00000'; break;
                                            case 'Gray': itemsarray.color = '#BABABA'; break;
                                            default: itemsarray.color = '#BABABA'; break;
                                        }
                                        items.push(itemsarray);
                                        itemsarray2.y = res.rows.item(i).Target;
                                        targets.push(itemsarray2);
                                        self.checkLineChart(true);
                                    }
                                    if (res.rows.item(i).ReportingYear == self.yearSelectVal()) {
                                        loadData();
                                    }
                                    if (res.rows.item(i).ReportingYear == (self.yearSelectVal() - 1)) {
                                        loadData();
                                    }
                                }
                                //   
                                if(self.q1Array().length < 4) {
                                    for (var fix=self.q1Array().length;fix<4;fix++) {
                                        self.q1Array().push({
                                                reportingYear: '',
                                                target: ''
                                            });
                                    }
                                }
                                
                                if(self.q2Array().length < 4) {
                                    for (var fix=self.q2Array().length;fix<4;fix++) {
                                        self.q2Array().push({
                                                reportingYear: '',
                                                target: ''
                                            });
                                    }
                                }
                                
                                if(self.q3Array().length < 4) {
                                    for (var fix=self.q3Array().length;fix<4;fix++) {
                                        self.q3Array().push({
                                                reportingYear: '',
                                                target: ''
                                            });
                                    }
                                }
                                
                                if(self.q4Array().length < 5) {
                                    for (var fix=self.q4Array().length;fix<45;fix++) {
                                        self.q4Array().push({
                                                reportingYear: '',
                                                target: ''
                                            });
                                    }
                                }
                                
                                self.q1y17(self.q1Array()[0].target);
                                self.q1y18(self.q1Array()[1].target);
                                self.q1y19(self.q1Array()[2].target);
                                self.q1y20(self.q1Array()[3].target);

                                self.q2y17(self.q2Array()[0].target);
                                self.q2y18(self.q2Array()[1].target);
                                self.q2y19(self.q2Array()[2].target);
                                self.q2y20(self.q2Array()[3].target);

                                self.q3y17(self.q3Array()[0].target);
                                self.q3y18(self.q3Array()[1].target);
                                self.q3y19(self.q3Array()[2].target);
                                self.q3y20(self.q3Array()[3].target);

                                self.q4y16(self.q4Array()[0].target);
                                self.q4y17(self.q4Array()[1].target);
                                self.q4y18(self.q4Array()[2].target);
                                self.q4y19(self.q4Array()[3].target);
                                self.q4y20(self.q4Array()[4].target);
                                
                                self.sum2016(self.q4y16());
                                self.sum2017(self.q1y17() + self.q2y17() + self.q3y17() + self.q4y17());
                                self.sum2018(self.q1y18() + self.q2y18() + self.q3y18() + self.q4y18());
                                self.sum2019(self.q1y19() + self.q2y19() + self.q3y19() + self.q4y19());
                                self.sum2020(self.q1y20() + self.q2y20() + self.q3y20() + self.q4y20());
                                if(self.checkLineChart()) {
                                    nameKPI = res.rows.item(0).KPINameArabic;
                                    areaSeries.push({
                                        name: nameKPI,
                                        items: ['none'],
                                        assignedToY2: "on",
                                        markerColor: 'none',
                                        color: 'none'
                                    },{
                                        name: 'المستهدف',
                                        items: targets,
                                        assignedToY2: "on",
                                        color: '#000000',
                                        markerColor: '#000000'
                                        },{
                                            name: 'الحالي',
                                            items: items,
                                            assignedToY2: "on",
                                            color: '#237BB1'
                                        });

                                    self.areaSeries(areaSeries);
                                    self.areaGroups(areaGroups);
                                }
                                
                            });

                        });

                    }).catch(function (err) {
                        // error! :( 
                    });
                    document.querySelector("#kpiRowDetailsTable").open();
                };


                self.onINIValueChanged = function (event) {
                    
                    if (event.detail.value.length > 0) {
                        self.iniCode(event.detail.value[0].startKey.row[0]);
                    }
                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {

                            var query = 'SELECT InivNameAr,EntityNameArabic,InivDescriptionAr,V2030Level3DescriptionArabic,[replace](PlannedStartDate, \' \', \'/\') as PlannedStartDate,[replace](PlannedFinishDate, \' \', \'/\') as PlannedFinishDate FROM mytable where Code = \'' + self.iniCode() + '\'';

                            txn.executeSql(query, [], function (tx, res) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    Object.keys(res.rows.item(0)).forEach(function (key) {
                                        if (res.rows.item(0)[key] === 'NULL') {
                                            res.rows.item(0)[key] = 'لا توجد بيانات';
                                        }
                                    });
                                    self.formInivName(res.rows.item(0).InivNameAr);
                                    self.formEntityName(res.rows.item(0).EntityNameArabic);
                                    self.formInivDescription(res.rows.item(0).InivDescriptionAr);
                                    self.formV2030LevelDesc(res.rows.item(0).V2030Level3DescriptionArabic);
                                    self.formPlannedStartDate(res.rows.item(0).PlannedStartDate);
                                    self.formPlannedFinishDate(res.rows.item(0).PlannedFinishDate);
                                }
                            });

                        });

                    }).catch(function (err) {
                        // error! :( 
                    });
                    document.querySelector("#iniRowDetailsTable").open();
                };


                self.inititiveDetailsTableColumnArray = [
                    {
                        "headerText": "المبادرات",
                        "field": "InivNameAr",
                        "renderer": oj.KnockoutTemplateUtils.getRenderer("ININameArabic", true)
                    },
                    {
                        "headerText": "نسبة الإكتمال الفعلية",
                        "field": "started",
                        "headerClassName": "oj-sm-only-hide",
                        "className": "oj-sm-only-hide",
                        "resizable": "enabled",
                        "style": "text-align: center;"
                    },
                    {
                        "headerText": "نسبة الإكتمال حسب الخطة",
                        "field": "Target",
                        "resizable": "enabled",
                        "style": "text-align: center;"
                    },
                    {
                        "headerText": "حالة التوثيق",
                        "field": "DocumentValidated",
                        "resizable": "enabled",
                        "style": "text-align: center;"
                    }];

                self.kpiDetailsTableColumnArray = [
                    {
                        "headerText": "مؤشرات الأداء الرئيسية",
                        "field": "KPINameArabic",
                        "renderer": oj.KnockoutTemplateUtils.getRenderer("KPINameArabicScript", true)
                    }, {
                        "headerText": "القيمة المستهدفة/الفعلية",
                        "field": "Actual",
                        "renderer": oj.KnockoutTemplateUtils.getRenderer("bulletChart", true)
                    },
                    {
                        "headerText": "فترة القياس",
                        "field": "Reporting_Period_Ar",
                        "resizable": "enabled",
                        "style": "text-align: center;"
                    },
                    {
                        "headerText": "حالة التوثيق",
                        "field": "DocumentValidated",
                        "resizable": "enabled",
                        "style": "text-align: center;"
                    }
                ];


                self.stepArray =
                        ko.observableArray([
                            {label: 'Q1', id: 'stp1', year: 2018},
                            {label: 'Q4', id: 'stp2', year: 2017},
                            {label: 'Q3', id: 'stp3', year: 2017},
                            {label: 'Q2', id: 'stp4', year: 2017},
                            {label: 'Q1', id: 'stp5', year: 2017},
                            {label: 'Q4', id: 'stp6', year: 2016}]);
                this.updateLabelText = function (event) {
                    var train = document.getElementById("filter-train");
                    self.yearSelectVal(train.getStep(event.detail.value).year);
                    self.quarterSelectVal(train.getStep(event.detail.value).label);

                    switch (self.yearSelectVal()) {
                        case 2018:
                            $('.train-2018').css('color', '#ffffff');
                            $('.train-2017, .train-2016').css('color', '#000000');
                            break;
                        case 2017:
                            $('.train-2017').css('color', '#ffffff');
                            $('.train-2018, .train-2016').css('color', '#000000');
                            break;
                        case 2016:
                            $('.train-2016').css('color', '#ffffff');
                            $('.train-2017, .train-2018').css('color', '#000000');
                            break;
                    }

                };

                self.currentLevel = ko.observable("All");
                self.currentProg = ko.observable('');
                self.currentEntity = ko.observable('');
                self.currentObjective = ko.observable('');
                self.currentProgName = ko.observable();
                self.currentEntityName = ko.observable();
                self.currentObjectiveName = ko.observable();

                self.programCardOnClick = function (event) {
                    if (self.programCardNotNull()) {
                        if (self.currentLevel() === 'All') {
                            var programObject = JSON.parse(JSON.stringify(event));
                            self.currentLevel("Prog");
                            $('.reset-all').css('color', '#65d1ba');
                            $('.reset-prog').css('color', '#ffffff').fadeIn();
                            self.currentProg(programObject.programNumber);
                            self.currentProgName(programObject.programNameAr);
                            query();
                            app.titleName(programObject.programNameAr);

                            getEntitesNumbersInProjects();
                            getObjectiveNumbersInProjects();
                            getINIStatusCount();
                            getKPIStatusCount();
                            getINICount();
                            getKPICount();
                        } else if (self.currentLevel() === 'Prog') {
                            var programObject = JSON.parse(JSON.stringify(event));
                            programObject2 = JSON.parse(JSON.stringify(event));
                            self.currentLevel("Entity");
                            $('.reset-entity').css('color', '#ffffff').fadeIn();
                            $('.reset-prog').css('color', '#65d1ba');
                            self.currentEntityName(programObject.entityNameArabic);
                            self.currentEntity(programObject.entityCode);
                            query();
                            app.titleName(programObject.entityNameArabic);
                        } else if (self.currentLevel() === 'Entity') {
                            self.cards.destroy(function (someItem) {
                                return someItem.currentArrayLevel === 'Entity';
                            });
                            var programObject = JSON.parse(JSON.stringify(event));
                            programObject2 = JSON.parse(JSON.stringify(event));
                            self.currentLevel("Object");
                            $('.reset-object').css('color', '#ffffff').fadeIn();
                            $('.reset-prog, .reset-entity').css('color', '#65d1ba');
                            self.currentObjectiveName(programObject.objectiveNameArabic);
                            self.currentObjective(programObject.objCode);

                            fillHeaderBarChartKPI(programObject.mo2shratG, programObject.mo2shratY, programObject.mo2shratR, programObject.mo2shratS, 0);
                            fillHeaderBarChartKPI(programObject.mobadratG, programObject.mobadratY, programObject.mobadratR, programObject.mobadratS, 1);
                            query();
                            app.titleName(programObject.objectiveNameArabic);
                        }
                    } else {
                        console.log("Error: Program Card NULL");
                        return;
                    }
                    $("*").animate({ scrollTop: 0 }, 500);
                };

                self.resetAll = function () {
                    chart.dispose();
                    chart = null;
                    chartKPI.dispose();
                    chartKPI = null;
                    yearChangedQuery = false;

                    history.go(-1);
                    self.currentLevel("All");
                    self.cards([]);
                    return false;
                };

                self.resetProg = function () {
                    $("*").animate({ scrollTop: 0 }, 500);
                    if (self.currentLevel() === "All")
                        return;

                    chart.dispose();
                    chart = null;

                    chartKPI.dispose();
                    chartKPI = null;

                    $('.reset-all').css({'color': '#ffffff'});
                    $('.reset-prog, .reset-entity, .reset-object').css('color', '#65d1ba').fadeOut();
                    self.cards([]);
                    self.currentLevel("All");
                    $("*").animate({ scrollTop: 0 }, 500);
                    query();
                };

                self.resetEntity = function () {
                    $("*").animate({ scrollTop: 0 }, 500);
                    if (self.currentLevel() === "Prog")
                        return;
                    $('.reset-prog').css({'color': '#ffffff'});
                    $('.reset-entity, .reset-object').css('color', '#65d1ba').fadeOut();
                    self.cards([]);
                    self.currentLevel("Prog");
                    $("*").animate({ scrollTop: 0 }, 500);
                    query();
                };
                self.resetObject = function () {
                    $("*").animate({ scrollTop: 0 }, 500);
                    if (self.currentLevel() === "Entity")
                        return;
                    self.allLineChartsData([]);
                    $('.reset-entity').css({'color': '#ffffff'});
                    $('.reset-object').css('color', '#65d1ba').fadeOut();
                    self.cards([]);
                    self.currentLevel("Entity");
                    $("*").animate({ scrollTop: 0 }, 500);
                    query();
                };

                //Open committeeHead Dialog
                self.showCommitteeHeadDialog = function (programCode) {
                    self.showCommitteeDialogDetails(programCode);
                    document.querySelector("#committeeDialog").open();
                    // disableScroll();
                };
                //Open Inititive Details Table
                self.showINIDetailsTable = function (programCode, entityCode, objCode, color, INICount, programNameAr) {
                    self.deptObservableArray([]);
                    self.dataprovider = new oj.ArrayTableDataSource(self.deptObservableArray,
                            {keyAttributes: 'Code', implicitSort: [{attribute: 'Code', direction: 'ascending'}]});

                    self.showDetails('INI', programCode, entityCode, objCode, color, INICount);
                    self.INIArabicNameDialog("المبادرات - "+programNameAr);
                    document.querySelector("#inititiveDetailsTable").open();
                    // disableScroll();

                };
                //close Inititive Details Table
                self.InititiveDetailsTableClose = function () {
                    self.deptObservableArray([]);
                    self.dataprovider = new oj.ArrayTableDataSource(self.deptObservableArray,
                            {keyAttributes: 'Code', implicitSort: [{attribute: 'Code', direction: 'ascending'}]});
                    document.querySelector("#inititiveDetailsTable").close();
                };
                //Open Kpi Details Table
                self.showKPIDetailsTable = function (programCode, entityCode, objCode, color, KPICount, programNameAr) {
                    self.deptObservableArray1([]);
                    self.dataprovider1 = new oj.ArrayTableDataSource(self.deptObservableArray1,
                            {keyAttributes: 'Code', implicitSort: [{attribute: 'Code', direction: 'ascending'}]});

                    self.showKPIDetails("KPI", programCode, entityCode, objCode, color, KPICount);
                    self.KPIArabicNameDialog("المؤشرات - "+programNameAr);
                    document.querySelector("#kpiDetailsTable").open();
                    // disableScroll();
                };
                //close Kpi Details Table
                self.KpiDetailsTableClose = function () {
                    self.deptObservableArray1([]);
                    self.dataprovider1 = new oj.ArrayTableDataSource(self.deptObservableArray1,
                            {keyAttributes: 'Code', implicitSort: [{attribute: 'Code', direction: 'ascending'}]});
                    document.querySelector("#kpiDetailsTable").close();
                };

                var deptArray = [];
                self.deptObservableArray = ko.observableArray([]);
                self.deptObservableArray1 = ko.observableArray([]);
                //                self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {keyAttributes: 'DepartmentId', implicitSort: [{attribute: 'DepartmentId', direction: 'ascending'}]});
                self.dataprovider = new oj.ArrayTableDataSource(self.deptObservableArray,
                        {keyAttributes: 'Code', implicitSort: [{attribute: 'Code', direction: 'ascending'}]});
                self.dataprovider1 = new oj.ArrayTableDataSource(self.deptObservableArray1,
                        {keyAttributes: 'Code', implicitSort: [{attribute: 'Code', direction: 'ascending'}]});


                self.showCommitteeDialogDetails = function (programCode) {
                    self.copyDatabaseFile('adaa.db').then(function () {
                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {
                            //programCode = res.rows.item(x).ProgramCode;
                            var query = 'select NameAr, CommitteeHead, DESC_AR as CommitteeDescAR FROM vision_table where Code = \'' + programCode + '\'';

                            txn.executeSql(query, [], function (tx, res) {
                                if (res.rows.item(0).CommitteeHead != 'undefined') {
                                    self.committeeHead(res.rows.item(0).CommitteeHead);
                                    self.committeeProgramName(res.rows.item(0).NameAr);
                                    self.committeeDescAR(res.rows.item(0).CommitteeDescAR);
                                    self.committeeHeadIcon("css/images/programsLogo/" + programCode + ".png");
                                }
                            });
                        });
                    }).catch(function (err) {
                        console.log(err);
                    });
                };


                self.showDetails = function (type, programCode, entityCode, objCode, color, INICount) {

                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {
                            var querycolor;

                            if (color === 'none') {
                                querycolor = 'status'
                            } else {
                                querycolor = '\'' + color + '\'';
                            }
                            self.INITotal(INICount);
                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select  Code,InivNameAr,status,(Target * 100) || \'%\' as Target,FormattedTargetAr,CASE WHEN started IN (1, \'Yes\') then (Actual * 100) || \'%\' ELSE \'لم تبدأ\' END as started,CASE DocumentValidated WHEN \'1\' THEN \'موثق\' WHEN \'0\' THEN \'غير موثق\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated,Actual,ProgramNameAr,ProgramCode,EntityNameArabic from mytable where ReportingYear = ' + self.yearSelectVal() + ' and ReportingQuarter = \'' + self.quarterSelectVal() + '\' and Type = \'' + type + '\' AND status = ' + querycolor + ' and programCode = \'' + programCode + '\'';

                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select  Code,InivNameAr,status,(Target * 100) || \'%\' as Target,FormattedTargetAr,CASE WHEN started IN (1, \'Yes\') then (Actual * 100) || \'%\' ELSE \'لم تبدأ\' END as started,CASE DocumentValidated WHEN \'1\' THEN \'موثق\' WHEN \'0\' THEN \'غير موثق\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated,Actual,ProgramNameAr,ProgramCode,EntityNameArabic from mytable where ReportingYear = ' + self.yearSelectVal() + ' and ReportingQuarter = \'' + self.quarterSelectVal() + '\' and Type = \'' + type + '\' AND status = ' + querycolor + ' and programCode = \'' + programCode + '\' and EntityCode = \'' + entityCode + '\'';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select  Code,InivNameAr,status,(Target * 100) || \'%\' as Target,FormattedTargetAr,CASE WHEN started IN (1, \'Yes\') then (Actual * 100) || \'%\' ELSE \'لم تبدأ\' END as started,CASE DocumentValidated WHEN \'1\' THEN \'موثق\' WHEN \'0\' THEN \'غير موثق\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated,Actual,ProgramNameAr,ProgramCode,EntityNameArabic from mytable where ReportingYear = ' + self.yearSelectVal() + ' and ReportingQuarter = \'' + self.quarterSelectVal() + '\' and Type = \'' + type + '\' AND status = ' + querycolor + ' and programCode = \'' + programCode + '\' and EntityCode = \'' + entityCode + '\' and ObejctiveCode = \'' + objCode + '\'';
                            }


                            txn.executeSql(query, [], function (tx, res) {

//                                self.INIArabicNameDialog('المبادرات')
//                                self.INIArabicNameDialog('مبادرات -  '+res.rows.item(0).ProgramNameAr);
                                for (var i = 0; i < res.rows.length; i++) {
                                    var old = res.rows.item(i);
                                    if (old.started === '0%') {
                                        old.started = "لم تبدأ";
                                    }
                                    console.log(old.started);
                                    self.deptObservableArray.push(old);
                                }

                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                    });
                };


                self.showKPIDetails = function (type, programCode, entityCode, objCode, color, KPICount) {

                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {

                            var querycolor;
                            if (color === 'none') {
                                querycolor = 'status'
                            } else {
                                querycolor = '\'' + color + '\'';
                            }
                            self.KPITotal(KPICount);

                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select Code, KPINameArabic,status ,Target , target2020 , Reporting_Period_Ar, Reporting_Period_En,FormattedActualAr || \'%\' as FormattedActualAr,FormattedTargetAr || \'%\' as FormattedTargetAr,CASE started WHEN 1 then \'بدأت\' WHEN \'0\' then \'لم تبدأ\' WHEN \'No\' then \'لم تبدأ\' WHEN \'Yes\' then \'بدأت\'            END as started,CASE measurable WHEN \'1\' THEN (SELECT CASE DocumentValidated WHEN \'1\' THEN \'موثق\' WHEN \'0\' THEN \'غير موثق\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated) WHEN \'0\' THEN \'لا يقاس حاليا\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated,Actual,ProgramNameAr,ProgramCode,EntityNameArabic from mytable where ReportingYear = ' + self.yearSelectVal() + ' and ReportingQuarter = \'' + self.quarterSelectVal() + '\' and Type = \'' + type + '\' AND status = ' + querycolor + ' and programCode = \'' + programCode + '\'';

                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select Code, KPINameArabic,status ,Target , target2020 , Reporting_Period_Ar, Reporting_Period_En,FormattedActualAr || \'%\' as FormattedActualAr,FormattedTargetAr || \'%\' as FormattedTargetAr,CASE started WHEN 1 then \'بدأت\' WHEN \'0\' then \'لم تبدأ\' WHEN \'No\' then \'لم تبدأ\' WHEN \'Yes\' then \'بدأت\'            END as started,CASE measurable WHEN \'1\' THEN (SELECT CASE DocumentValidated WHEN \'1\' THEN \'موثق\' WHEN \'0\' THEN \'غير موثق\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated) WHEN \'0\' THEN \'لا يقاس حاليا\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated,Actual,ProgramNameAr,ProgramCode,EntityNameArabic from mytable where ReportingYear = ' + self.yearSelectVal() + ' and ReportingQuarter = \'' + self.quarterSelectVal() + '\' and Type = \'' + type + '\' AND status = ' + querycolor + ' and programCode = \'' + programCode + '\' and EntityCode = \'' + entityCode + '\'';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select Code, KPINameArabic,status ,Target , target2020 , Reporting_Period_Ar, Reporting_Period_En,FormattedActualAr || \'%\' as FormattedActualAr,FormattedTargetAr || \'%\' as FormattedTargetAr,CASE started WHEN 1 then \'بدأت\' WHEN \'0\' then \'لم تبدأ\' WHEN \'No\' then \'لم تبدأ\' WHEN \'Yes\' then \'بدأت\'            END as started,CASE measurable WHEN \'1\' THEN (SELECT CASE DocumentValidated WHEN \'1\' THEN \'موثق\' WHEN \'0\' THEN \'غير موثق\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated) WHEN \'0\' THEN \'لا يقاس حاليا\' WHEN \'NULL\' THEN \'لا يقاس حاليا\' END as DocumentValidated,Actual,ProgramNameAr,ProgramCode,EntityNameArabic from mytable where ReportingYear = ' + self.yearSelectVal() + ' and ReportingQuarter = \'' + self.quarterSelectVal() + '\' and Type = \'' + type + '\' AND status = ' + querycolor + ' and programCode = \'' + programCode + '\' and ObejctiveCode = \'' + objCode + '\'';
                            }



                            txn.executeSql(query, [], function (tx, res) {

//                                self.KPIArabicNameDialog('المؤشرات');
//                                if (res.rows.item(0).ProgramNameAr !== 'NULL')
//                                    self.KPIArabicNameDialog('مؤشرات -  ' + res.rows.item(0).ProgramNameAr);
                                for (var i = 0; i < res.rows.length; i++) {
                                    var old = res.rows.item(i);
                                    if (old.Target <= '0' || old.Target === 'NULL') {
                                        old.Target = 0;
                                    }
                                    if (old.Actual <= '0' || old.Actual === 'NULL') {
                                        old.Actual = 0;
                                    }
                                    if (old.target2020 <= '0' || old.target2020 === 'NULL') {
                                        old.target2020 = 0;
                                    }
                                    self.deptObservableArray1.push(old);
                                }

                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });
                };

                self.copyDatabaseFile = function (dbName) {
                    var sourceFileName = cordova.file.applicationDirectory + 'www/db/' + dbName;
                    var targetDirName = cordova.file.dataDirectory;

                    return Promise.all([
                        new Promise(function (resolve, reject) {
                            resolveLocalFileSystemURL(sourceFileName, resolve, reject);
                        }),
                        new Promise(function (resolve, reject) {
                            resolveLocalFileSystemURL(targetDirName, resolve, reject);
                        })
                    ]).then(function (files) {
                        var sourceFile = files[0];
                        var targetDir = files[1];
                        return new Promise(function (resolve, reject) {
                            targetDir.getFile(dbName, {}, resolve, reject);
                        }).then(function () {
                            console.log("file already copied");
                        }).catch(function () {
                            console.log("file doesn't exist, copying it");
                            return new Promise(function (resolve, reject) {
                                sourceFile.copyTo(targetDir, dbName, resolve, reject);
                            }).then(function () {
                                console.log("database file copied");
                            });
                        });
                    });
                };

                self.avatarSize = ko.observable("md");
                self.firstName = 'Amy';
                self.lastName = 'Bartlett';
                self.initials = oj.IntlConverterUtils.getInitials(self.firstName, self.lastName);

                /* toggle button variables */
                self.stackValue = ko.observable('on');
                self.orientationValue = ko.observable('horizontal');
                self.stackLabelValue = ko.observable('on');
                self.legendRenderedValue = ko.observable('off');
                self.color1 = ko.observable('#009a84');
                self.color2 = ko.observable('#FBD903');
                self.color3 = ko.observable('#C00000');
                self.color4 = ko.observable('#BABABA');



                self.KPIGray = ko.observable("");
                self.KPIGreen = ko.observable("");
                self.KPIRed = ko.observable("");
                self.KPIYellow = ko.observable("");

                self.INIGray = ko.observable("");
                self.INIGreen = ko.observable("");
                self.INIRed = ko.observable("");
                self.INIYellow = ko.observable("");
                var yearChangedQuery = true;
                
                self.yearValueChangedHandler = function (event) {
                    self.yearSelectVal(self.selectedYearValue().substring(2, 6));
                    self.quarterSelectVal(self.selectedYearValue().substring(0, 2));
                    try {
                        chart.dispose();
                        chart = null;

                        chartKPI.dispose();
                        chartKPI = null;

                        self.cards([]);
                        yearChangedQuery = true;
                        query();
                    } catch (err) {
                        alert(err.message);
                    }
                };
                self.cards = ko.observableArray([]);
                self.cards([]);
                self.yearQuarterArray = ko.observableArray([]);
                fillYearQuarter();
                self.likertINI = ko.observableArray([]);
                self.likertKPI = ko.observableArray([]);

                /* chart data */
                self.mo2shratBarSeries = [
                    {name: "Series 1", items: [{y: 44, label: "44%"}, {y: 36, label: "36%"}], color: self.color1()},
                    {name: "Series 2", items: [{y: 9, label: "9%"}, {y: 25, label: "25%"}]},
                    {name: "Series 3", items: [{y: 42, label: "42%"}, {y: 26, label: "26%"}]},
                    {name: "Series 4", items: [{y: 5, label: "5%"}, {y: 13, label: "13%"}]}];

                self.mo2shratBarSeries[0]['color'] = self.color1();
                self.mo2shratBarSeries[1]['color'] = self.color2();
                self.mo2shratBarSeries[2]['color'] = self.color3();
                self.mo2shratBarSeries[3]['color'] = self.color4();

                self.mo2shratBarSeriesValue = ko.observableArray(self.mo2shratBarSeries);


                // if application running on device
                document.addEventListener("deviceready", onDeviceReady, false);
                function onDeviceReady() {
                }



                // Below are a set of the ViewModel methods invoked by the oj-module component.
                // Please reference the oj-module jsDoc for additional information.

                /**
                 * Optional ViewModel method invoked after the View is inserted into the
                 * document DOM.  The application can put logic that requires the DOM being
                 * attached here. 
                 * This method might be called multiple times - after the View is created 
                 * and inserted into the DOM and after the View is reconnected 
                 * after being disconnected.
                 */
                self.connected = function () {
                    // Implement if needed
                    $(".dashboard_info").on("click", function () {
                        $(".dashboard_info").css("overflow-y", "hidden").slideUp(function () {
                            $(".expand-dashboard-info-container").css("display", "block");
                        });
                    });
                    $(".expand-dashboard-info-container").on("click", function () {
                        $(".expand-dashboard-info-container").css("display", "none");
                        $(".dashboard_info").css("overflow-y", "unset").slideDown();
                    });
                    app.titleName("جميع البرامج");
                    $("header, footer").css("display", "block");
                    if(!yearChangedQuery){
                        query();
                    }
                };

                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                self.disconnected = function () {
                    // Implement if needed
                    app.hideBackButton(false);
                    $("#composite-container").html("");
                    $(".dashboard_info").on("click", function () {
                        $(".dashboard_info").css("overflow-y", "hidden").slideUp();
                    });
                    $(".expand-dashboard-info-container").on("click", function () {
                        $(".expand-dashboard-info-container").css("display", "none");
                        $(".dashboard_info").css("overflow-y", "unset").slideDown();
                    });
                    $("header, footer").css("display", "none");
                };

                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                self.transitionCompleted = function () {
                    // Implement if needed

                };

                function getEntitesNumbersInProjects() {

                    self.copyDatabaseFile('adaa.db').then(function () {
                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {

                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select count(distinct   EntityCode) count from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' ';
                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select count(distinct   EntityCode) count from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and ProgramCode = \'' + self.currentProg() + '\'';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select count(distinct   EntityCode) count from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and ProgramCode = \'' + self.currentProg() + '\'\n\
                                    and EntityCode = \'' + self.currentEntity() + '\'';
                            } else {
                                query = 'select count(distinct   EntityCode) count from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' ';
                            }

                            txn.executeSql(query, [], function (tx, res) {

                                self.entitesNumbers(res.rows.item(0).count);
                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });

                }

                function getObjectiveNumbersInProjects() {

                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {
                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select count(distinct ObejctiveCode) count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' ';
                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select count(distinct ObejctiveCode) count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and ProgramCode = \'' + self.currentProg() + '\'';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select count(distinct ObejctiveCode) count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\'  and ProgramCode = \'' + self.currentProg() + '\'\n\
                                        and EntityCode = \'' + self.currentEntity() + '\'';
                            } else {
                                query = 'select count(distinct ObejctiveCode) count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' ';
                            }

                            txn.executeSql(query, [], function (tx, res) {

                                // alert(res.rows.item(0));
                                self.objectiveNumbers(res.rows.item(0).count);

                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });

                }


                function getINICount() {

                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {
                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select   count(*)  count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'INI\' ';
                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select   count(*)  count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'INI\'  and ProgramCode = \'' + self.currentProg() + '\'';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select   count(*)  count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'INI\'   and ProgramCode = \'' + self.currentProg() + '\'\n\
                                        and EntityCode = \'' + self.currentEntity() + '\'';
                            } else {
                                query = 'select   count(*)  count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'INI\' ';
                            }
                            txn.executeSql(query, [], function (tx, res) {


                                self.inititivesNumbers(res.rows.item(0).count);


                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });
                }

                function getKPICount() {

                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {

                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select   count(*)  count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'KPI\' ';
                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select   count(*)  count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'KPI\'  and ProgramCode = \'' + self.currentProg() + '\'';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select   count(*)  count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'KPI\'   and ProgramCode = \'' + self.currentProg() + '\'\n\
                                        and EntityCode = \'' + self.currentEntity() + '\'';
                            } else {
                                query = 'select   count(*)  count  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'KPI\' ';
                            }
                            txn.executeSql(query, [], function (tx, res) {

                                self.KPINumbers(res.rows.item(0).count);

                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });
                }

                function getKPIStatusCount() {

                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {

                            var query = '';
                            var query;
                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select   count(*)  count,status  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'KPI\'  group by status';
                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select   count(*)  count,status  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'KPI\'  and ProgramCode = \'' + self.currentProg() + '\'  group by status';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select   count(*)  count,status  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'KPI\'   and ProgramCode = \'' + self.currentProg() + '\'\n\
                                        and EntityCode = \'' + self.currentEntity() + '\'  group by status';
                            } else {
                                query = 'select   count(*)  count,status  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'KPI\' group by status';
                            }
                            self.KPIGray(0);
                            self.KPIGreen(0);
                            self.KPIRed(0);
                            self.KPIYellow(0);
                            txn.executeSql(query, [], function (tx, res) {
                                for (var i = 0; i < res.rows.length; i++) {

                                    if (res.rows.item(i).Status === 'Gray') {
                                        self.KPIGray(res.rows.item(i).count);
                                    } else if (res.rows.item(i).Status === 'Green') {
                                        self.KPIGreen(res.rows.item(i).count);
                                    } else if (res.rows.item(i).Status === 'Red') {
                                        self.KPIRed(res.rows.item(i).count);
                                    } else if (res.rows.item(i).Status === 'Yellow') {
                                        self.KPIYellow(res.rows.item(i).count);
                                    }

                                }

                                if (res.rows.length == 0) {
                                    fillHeaderBarChartKPI(0, 0, 0, 0, 0)
                                } else {
                                    fillHeaderBarChartKPI(self.KPIGreen(), self.KPIYellow(), self.KPIRed(), self.KPIGray(), 0)
                                }


                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });
                }

                function getINIStatusCount() {

                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {
                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select   count(*)  count,status  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'INI\'  group by status';
                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select   count(*)  count,status  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'INI\'  and ProgramCode = \'' + self.currentProg() + '\'  group by status';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select   count(*)  count,status  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'INI\'   and ProgramCode = \'' + self.currentProg() + '\'\n\
                                        and EntityCode = \'' + self.currentEntity() + '\'  group by status';
                            } else {
                                query = 'select   count(*)  count,status  from mytable where ReportingYear ='
                                        + self.yearSelectVal() + ' and ReportingQuarter=\''
                                        + self.quarterSelectVal() + '\' and type=\'INI\' group by status';
                            }
                            self.INIGray(0);
                            self.INIGreen(0);
                            self.INIRed(0);
                            self.INIYellow(0);
                            txn.executeSql(query, [], function (tx, res) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    if (res.rows.item(i).Status === 'Gray') {
                                        self.INIGray(res.rows.item(i).count);
                                    } else if (res.rows.item(i).Status === 'Green') {
                                        self.INIGreen(res.rows.item(i).count);
                                    } else if (res.rows.item(i).Status === 'Red') {
                                        self.INIRed(res.rows.item(i).count);
                                    } else if (res.rows.item(i).Status === 'Yellow') {
                                        self.INIYellow(res.rows.item(i).count);
                                    }

                                }


                                if (res.rows.length == 0) {
                                    fillHeaderBarChartKPI(0, 0, 0, 0, 1)
                                } else {
                                    fillHeaderBarChartKPI(self.INIGreen(), self.INIYellow(), self.INIRed(), self.INIGray(), 1)
                                }
                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });
                }

                function fillYearQuarter() {
                    self.copyDatabaseFile('adaa.db').then(function () {
                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {
                            var query = "select distinct ReportingQuarter, ReportingYear from mytable order by ReportingYear DESC, ReportingQuarter DESC";
                            txn.executeSql(query, [], function (tx, res) {
                                for (var i = 0; i < res.rows.length; i++) {
                                    self.yearQuarterArray.push({
                                        value: res.rows.item(i).ReportingQuarter + res.rows.item(i).ReportingYear,
                                        label: res.rows.item(i).ReportingQuarter + ' / ' + res.rows.item(i).ReportingYear
                                    });
                                }
                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });
                }

                function fillHeaderBarChartKPI(green, yellow, red, gray, barIndex) {

                    var total = green + yellow + red + gray;
                    var greenValue = 0;
                    if (green != 0) {
                        greenValue = calculatePer(green, total);
                        self.mo2shratBarSeries[0].items[barIndex].y = greenValue;
                        self.mo2shratBarSeries[0].items[barIndex].label = greenValue + '%';
                    } else {
                        self.mo2shratBarSeries[0].items[barIndex].y = 0;
                        self.mo2shratBarSeries[0].items[barIndex].label = 0;
                    }


                    var yellowValue = 0;
                    if (yellow != 0) {
                        yellowValue = calculatePer(yellow, total);
                        self.mo2shratBarSeries[1].items[barIndex].y = yellowValue;
                        self.mo2shratBarSeries[1].items[barIndex].label = yellowValue + '%';
                    } else {
                        self.mo2shratBarSeries[1].items[barIndex].y = 0;
                        self.mo2shratBarSeries[1].items[barIndex].label = 0;
                    }



                    var redValue = 0;
                    if (red != 0) {
                        redValue = calculatePer(red, total);
                        self.mo2shratBarSeries[2].items[barIndex].y = redValue;
                        self.mo2shratBarSeries[2].items[barIndex].label = redValue + '%';
                    } else {
                        self.mo2shratBarSeries[2].items[barIndex].y = 0;
                        self.mo2shratBarSeries[2].items[barIndex].label = 0;
                    }




                    var grayValue = 0;
                    if (gray != 0) {
                        if (Number(greenValue) != 0 || Number(yellowValue) != 0 || Number(redValue) != 0) {
                            grayValue = 100 - (Number(greenValue) + Number(yellowValue) + Number(redValue));
                            if (grayValue != 0) {
                                self.mo2shratBarSeries[3].items[barIndex].y = grayValue;
                                self.mo2shratBarSeries[3].items[barIndex].label = grayValue + '%';
                            } else {
                                self.mo2shratBarSeries[3].items[barIndex].y = 0;
                                self.mo2shratBarSeries[3].items[barIndex].label = 0;
                            }

                        } else if (gray != 0) {
                            self.mo2shratBarSeries[3].items[barIndex].y = 100;
                            self.mo2shratBarSeries[3].items[barIndex].label = '100%';
                        } else {
                            self.mo2shratBarSeries[3].items[barIndex].y = 0;
                            self.mo2shratBarSeries[3].items[barIndex].label = 0;
                        }
                    } else {
                        self.mo2shratBarSeries[3].items[barIndex].y = 0;
                        self.mo2shratBarSeries[3].items[barIndex].label = 0;
                    }




                    self.mo2shratBarSeriesValue([]);

                    self.mo2shratBarSeriesValue(self.mo2shratBarSeries);
                }


                function calculatePer(val1, total) {
                    var perc;
                    if (isNaN(val1) || isNaN(total)) {
                        perc = " ";
                    } else {
                        perc = ((val1 / total) * 100).toFixed();
                    }

                    return perc;
                }

                anychart.licenseKey("appspro-me.com-880879da-e9510f2f");
                var chart = anychart.bar();
                var chartKPI = anychart.bar();
                function query() {

                    if (!chart) {
                        chart = anychart.bar();
                    }

                    if (!chartKPI) {
                        chartKPI = anychart.bar();
                    }

                    self.objType2(false);
                    self.programCardNotNull(false);


                    if (self.currentLevel() === 'All') {
                        getEntitesNumbersInProjects();
                        getObjectiveNumbersInProjects();
                        getINICount();
                        getKPICount();
                        getKPIStatusCount();
                        getINIStatusCount();


                    } else if (self.currentLevel() === 'Prog') {

                        getEntitesNumbersInProjects();
                        getObjectiveNumbersInProjects();
                        getINICount();
                        getKPICount();
                        getKPIStatusCount();
                        getINIStatusCount();

                    } else if (self.currentLevel() === 'Entity') {

                        getEntitesNumbersInProjects();
                        getObjectiveNumbersInProjects();
                        getINICount();
                        getKPICount();
                        getKPIStatusCount();
                        getINIStatusCount();
                    }
                    self.copyDatabaseFile('adaa.db').then(function () {

                        var db = sqlitePlugin.openDatabase('adaa.db');
                        db.readTransaction(function (txn) {
                            var query;
                            if (self.currentLevel() === 'All') {
                                query = 'select ProgramCode from mytable where ReportingYear =' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\'  and ProgramCode<> \'NULL\' group by ProgramCode order by COUNT(*) desc';
                            } else if (self.currentLevel() === 'Prog') {
                                query = 'select EntityCode,ProgramCode from mytable where ReportingYear =' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\' and ProgramCode=\'' + self.currentProg() + '\'  group by EntityCode order by COUNT(*) desc';
                            } else if (self.currentLevel() === 'Entity') {
                                query = 'select EntityCode,ProgramCode,ObejctiveCode from mytable  where ReportingYear =' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\' and ProgramCode=\'' + self.currentProg() + '\' and EntityCode = \'' + self.currentEntity() + '\' group by ObejctiveCode order by COUNT(*) desc';
                            } else if (self.currentLevel() === 'Object') {
                                kpiToggleText();
                                query = 'select EntityCode,ProgramCode,Target,ObejctiveCode,code from mytable  where ReportingYear =' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\' and ProgramCode=\'' + self.currentProg() + '\' and EntityCode = \'' + self.currentEntity() + '\' and ObejctiveCode = \'' + self.currentObjective() + '\' group by code order by code';
                            }

                            var count = 0;
                            txn.executeSql(query, [], function (tx, res1) {
                                self.allLineChartsData([]);

                                for (var x = 0; x < res1.rows.length; x++) {

                                    var programCode;
                                    var entityCode;
                                    var objectiveCode;
                                    var code;

                                    if (self.currentLevel() === 'All') {
                                        programCode = res1.rows.item(x).ProgramCode;
                                        query = 'select  count(*) count ,type , status , ProgramCode,Target,ProgramNameAr from mytable where  ReportingYear = ' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\'  and ProgramCode=\'' + programCode + '\' group by type, status ';

                                    } else if (self.currentLevel() === 'Prog') {
                                        entityCode = res1.rows.item(x).EntityCode;
                                        programCode = res1.rows.item(x).ProgramCode;
                                        query = 'select COUNT(*) count,status,type,EntityCode,ProgramCode,Target,EntityNameArabic,ProgramNameAr from mytable where ReportingYear =' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\'  and ProgramCode=\'' + programCode + '\' and entityCode = \'' + entityCode + '\' group by Type,status,EntityCode order by EntityCode';
                                    } else if (self.currentLevel() === 'Entity') {
                                        entityCode = res1.rows.item(x).EntityCode;
                                        programCode = res1.rows.item(x).ProgramCode;
                                        objectiveCode = res1.rows.item(x).ObejctiveCode;
                                        query = 'select COUNT(*) count,status,type,EntityCode,ProgramCode,Target,EntityNameArabic,ProgramNameAr,ObjectiveNameArabic,ObejctiveCode from mytable where ReportingYear =' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\'  and ProgramCode=\'' + programCode + '\' and entityCode = \'' + entityCode + '\' and ObejctiveCode = \'' + objectiveCode + '\' group by Type,status,ObejctiveCode order by ObejctiveCode';
                                    } else if (self.currentLevel() === 'Object') {
                                        entityCode = res1.rows.item(x).EntityCode;
                                        programCode = res1.rows.item(x).ProgramCode;
                                        objectiveCode = res1.rows.item(x).ObejctiveCode;
                                        code = res1.rows.item(x).Code;

                                        query = 'select type,Polarity,InivNameAr,KPINameArabic,reportingYear,Target,reportingquarter,SUBSTR(reportingYear,3,5)  ||  \'-\' || reportingquarter as area,Actual as series,MeasurementUnitAr,status from mytable where ProgramCode=\'' + programCode + '\' and entityCode = \'' + entityCode + '\' and ObejctiveCode = \'' + objectiveCode + '\' and code =\'' + code + '\' order by reportingYear DESC,reportingquarter DESC';
                                        console.log(query);
                                    }



                                    tx.executeSql(query, [], function (tx, res) {
                                        count++;
                                        if (self.currentLevel() !== 'Object') {
                                            var grayINICount = 0;
                                            var greenINICount = 0;
                                            var redINICount = 0;
                                            var yellowINICount = 0;

                                            var grayKPICount = 0;
                                            var greenKPICount = 0;
                                            var redKPICount = 0;
                                            var yellowKPICount = 0;
                                            var pCode = ko.observable(res.rows.item(0).ProgramCode);
                                            var ProgramNameAr = ko.observable(res.rows.item(0).ProgramNameAr);
                                            var currentArrayLevel = ko.observable(self.currentLevel());
                                            var firstValueToDestroy, SecValueToDestroy;
                                            var entityNameArabic;
                                            var eCode;
                                            var objectiveNameArabic;
                                            var objCode;
                                            var KPICount = 0;
                                            var INICount = 0;
                                            if (self.currentLevel() === 'Prog') {
                                                entityNameArabic = res.rows.item(0).EntityNameArabic;
                                                eCode = res.rows.item(0).EntityCode;
                                            } else if (self.currentLevel() === 'Entity') {
                                                entityNameArabic = res.rows.item(0).EntityNameArabic;
                                                eCode = res.rows.item(0).EntityCode;
                                                objectiveNameArabic = res.rows.item(0).ObjectiveNameArabic;
                                                objCode = res.rows.item(0).ObejctiveCode;
                                            }

                                            //Reload the array values without clear
                                            switch (self.currentLevel()) {
                                                case 'All':
                                                    firstValueToDestroy = 'Prog';
                                                    SecValueToDestroy = 'Entity';
                                                    break;
                                                case 'Prog':
                                                    firstValueToDestroy = 'All';
                                                    SecValueToDestroy = 'Entity';
                                                    break;
                                                case 'Entity':
                                                    firstValueToDestroy = 'All';
                                                    SecValueToDestroy = 'Prog';
                                                    break;
                                            }
                                            self.cards.destroy(function (someItem) {
                                                return someItem.currentArrayLevel === firstValueToDestroy;
                                            });
                                            self.cards.destroy(function (someItem) {
                                                return someItem.currentArrayLevel === SecValueToDestroy;
                                            });

                                            for (var i = 0; i < res.rows.length; i++) {

                                                if (res.rows.item(i).Status === 'Gray' && res.rows.item(i).Type === 'KPI') {
                                                    grayKPICount = res.rows.item(i).count;
                                                    KPICount = KPICount + grayKPICount;
                                                } else if (res.rows.item(i).Status === 'Green' && res.rows.item(i).Type === 'KPI') {
                                                    greenKPICount = res.rows.item(i).count;
                                                    KPICount = KPICount + greenKPICount;
                                                } else if (res.rows.item(i).Status === 'Red' && res.rows.item(i).Type === 'KPI') {
                                                    redKPICount = res.rows.item(i).count;
                                                    KPICount = KPICount + redKPICount;
                                                } else if (res.rows.item(i).Status === 'Yellow' && res.rows.item(i).Type === 'KPI') {
                                                    yellowKPICount = res.rows.item(i).count;
                                                    KPICount = KPICount + yellowKPICount;
                                                } else if (res.rows.item(i).Status === 'Gray' && res.rows.item(i).Type === 'INI') {
                                                    grayINICount = res.rows.item(i).count;
                                                    INICount = INICount + grayINICount;
                                                } else if (res.rows.item(i).Status === 'Green' && res.rows.item(i).Type === 'INI') {
                                                    greenINICount = res.rows.item(i).count;
                                                    INICount = INICount + greenINICount;
                                                } else if (res.rows.item(i).Status === 'Red' && res.rows.item(i).Type === 'INI') {
                                                    redINICount = res.rows.item(i).count;
                                                    INICount = INICount + redINICount;
                                                } else if (res.rows.item(i).Status === 'Yellow' && res.rows.item(i).Type === 'INI') {
                                                    yellowINICount = res.rows.item(i).count;
                                                    INICount = INICount + yellowINICount;
                                                }
                                            }
                                            self.cards.push({
                                                name: '1x1',
                                                sizeClass: 'oj-masonrylayout-tile-2x1',
                                                programNumber: pCode(),
                                                goalsNumber: 2,
                                                mo2shratNumber: 3,
                                                mobadratNumber: 4,
                                                mo2shratG: greenKPICount,
                                                mo2shratY: yellowKPICount,
                                                mo2shratR: redKPICount,
                                                mo2shratS: grayKPICount,
                                                mobadratG: greenINICount,
                                                mobadratY: yellowINICount,
                                                mobadratR: redINICount,
                                                mobadratS: grayINICount,
                                                avatarSize: 'md',
                                                initials: 'AF',
                                                entityCode: eCode,
                                                entityNameArabic: entityNameArabic,
                                                programNameAr: ProgramNameAr(),
                                                objectiveNameArabic: objectiveNameArabic,
                                                objCode: objCode,
                                                INICount: ': ' + INICount,
                                                KPICount: ': ' + KPICount,
                                                currentArrayLevel: currentArrayLevel()
                                            });

                                            if (self.currentLevel() === 'All') {

                                                var prooog = ProgramNameAr();

                                                self.likertINI.push({
                                                    "progname": prooog,
                                                    "green": greenINICount,
                                                    "yellow": yellowINICount,
                                                    "red": redINICount,
                                                    "gray": grayINICount

                                                });

                                                self.likertKPI.push({
                                                    "progname": prooog,
                                                    "green": greenKPICount,
                                                    "yellow": yellowKPICount,
                                                    "red": redKPICount,
                                                    "gray": grayKPICount

                                                });

                                                if (count === (res1.rows.length - 1)) {
                                                    var likertData = self.likertINI();
                                                    var likertKPIData = self.likertKPI();

                                                    var createSeries = function (name) {
                                                        var data = [];
                                                        for (var i = 0; i < likertData.length; i++) {
                                                            var yellow = likertData[i].yellow;
                                                            var red = likertData[i].red;
                                                            var green = likertData[i].green;
                                                            var gray = likertData[i].gray;
                                                            var total = green + yellow + red + gray;
                                                            var totalYello = 0, totalRed = 0, totalGreen = 0, totalGray = 0;
                                                            totalYello = calculatePer(yellow, total);
                                                            totalRed = calculatePer(red, total);
                                                            totalGreen = calculatePer(green, total);
                                                            totalGray = calculatePer(gray, total);
                                                            if (name == 'yellow') {
                                                                if (yellow == 0) {
                                                                    data.push({
                                                                        x: likertData[i].progname,
                                                                        low: 0,
                                                                        high: yellow,
                                                                        value: yellow,
                                                                        fill: '#FBD903',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": false
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    data.push({
                                                                        x: likertData[i].progname,
                                                                        low: 0,
                                                                        high: yellow,
                                                                        value: yellow,
                                                                        total: totalYello,
                                                                        fill: '#FBD903',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": true,
                                                                                "anchor": "center",
                                                                                "position": "center-top",
                                                                                "fontColor": "Black",
                                                                                "format": "{%total}%",
                                                                                "fontSize": 8,
                                                                                "offsetX": -12
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            } else if (name == 'green') {
                                                                if (green == 0) {

                                                                    data.push({
                                                                        x: likertData[i].progname,
                                                                        low: yellow,
                                                                        high: green + yellow,
                                                                        value: green,
                                                                        fill: '#009a84',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": false
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    data.push({
                                                                        x: likertData[i].progname,
                                                                        low: yellow,
                                                                        high: green + yellow,
                                                                        value: green,
                                                                        total: totalGreen,
                                                                        fill: '#009a84',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": true,
                                                                                "anchor": "center",
                                                                                "position": "center-top",
                                                                                "fontColor": "Black",
                                                                                "format": "{%total}%",
                                                                                "fontSize": 8,
                                                                                "offsetX": -12
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            } else if (name == 'red') {
                                                                if (red == 0) {
                                                                    data.push({
                                                                        x: likertData[i].progname,
                                                                        low: 0,
                                                                        high: 0,
                                                                        value: red,
                                                                        fill: '#C00000',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": false
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    data.push({
                                                                        x: likertData[i].progname,
                                                                        low: 0,
                                                                        high: -red,
                                                                        value: red,
                                                                        fill: '#C00000',
                                                                        total: totalRed,
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": true,
                                                                                "anchor": "center",
                                                                                "fontSize": 8,
                                                                                "position": "center-top",
                                                                                "fontColor": "Black",
                                                                                "format": "{%total}%",
                                                                                "offsetX": -12
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            } else if (name == 'gray') {
                                                                if (gray == 0) {
                                                                    data.push({
                                                                        x: likertData[i].progname,
                                                                        low: -red,
                                                                        high: -red - gray,
                                                                        value: gray,
                                                                        fill: '#BABABA',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": false
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    data.push({
                                                                        x: likertData[i].progname,
                                                                        low: -red,
                                                                        high: -red - gray,
                                                                        value: gray,
                                                                        fill: '#BABABA',
                                                                        total: totalGray,
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": true,
                                                                                "anchor": "center",
                                                                                "position": "center-top",
                                                                                "fontColor": "Black",
                                                                                "format": "{%total}%",
                                                                                "fontSize": 8,
                                                                                "offsetX": -12
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                            }
                                                        }
                                                        console.log(data);
                                                        var series = chart.rangeBar(data);
                                                        series.name(name)
                                                                .stroke('3 #fff 1')
                                                                .selectionMode('none');
                                                        series.hovered().stroke('3 #fff 1');
                                                        series.tooltip().title()
                                                                .useHtml(true)
                                                                .fontSize(12);

//                                                    // set labels position
//                                                    labels = series.labels();
//                                                    labels.offsetX(-30);
//                                                    labels.enabled(true);
//                                                    labels.position("center-top");
//                                                    labels.format("{%value}");
//                                                    chart.labels().fontColor("Black");
                                                    };

                                                    var createKPISeries = function (name) {
                                                        var data = [];
                                                        for (var i = 0; i < likertKPIData.length; i++) {
                                                            var yellow = likertKPIData[i].yellow;
                                                            var red = likertKPIData[i].red;
                                                            var green = likertKPIData[i].green;
                                                            var gray = likertKPIData[i].gray;
                                                            var total = green + yellow + red + gray;
                                                            var totalYello = 0, totalRed = 0, totalGreen = 0, totalGray = 0;
                                                            totalYello = calculatePer(yellow, total);
                                                            totalRed = calculatePer(red, total);
                                                            totalGreen = calculatePer(green, total);
                                                            totalGray = calculatePer(gray, total);
                                                            if (name == 'yellow') {
                                                                if (yellow == 0) {
                                                                    data.push({
                                                                        x: likertKPIData[i].progname,
                                                                        low: 0,
                                                                        high: yellow,
                                                                        value: yellow,
                                                                        fill: '#FBD903',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": false
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    data.push({
                                                                        x: likertKPIData[i].progname,
                                                                        low: 0,
                                                                        high: yellow,
                                                                        value: yellow,
                                                                        fill: '#FBD903',
                                                                        total: totalYello,
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": true,
                                                                                "anchor": "center",
                                                                                "position": "center-top",
                                                                                "fontColor": "Black",
                                                                                "format": "{%total}%",
                                                                                "fontSize": 8,
                                                                                "offsetX": -12
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                            } else if (name == 'green') {
                                                                if (green == 0) {

                                                                    data.push({
                                                                        x: likertKPIData[i].progname,
                                                                        low: yellow,
                                                                        high: green + yellow,
                                                                        value: green,
                                                                        fill: '#009a84',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": false
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    data.push({
                                                                        x: likertKPIData[i].progname,
                                                                        low: yellow,
                                                                        high: green + yellow,
                                                                        value: green,
                                                                        total: totalGreen,
                                                                        fill: '#009a84',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": true,
                                                                                "anchor": "center",
                                                                                "position": "center-top",
                                                                                "fontColor": "Black",
                                                                                "format": "{%total}%",
                                                                                "fontSize": 8,
                                                                                "offsetX": -12
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                            } else if (name == 'red') {
                                                                if (red == 0) {
                                                                    data.push({
                                                                        x: likertKPIData[i].progname,
                                                                        low: 0,
                                                                        high: -red,
                                                                        value: red,
                                                                        fill: '#C00000',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": false
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    data.push({
                                                                        x: likertKPIData[i].progname,
                                                                        low: 0,
                                                                        high: -red,
                                                                        value: red,
                                                                        fill: '#C00000',
                                                                        total: totalRed,
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": true,
                                                                                "anchor": "center",
                                                                                "fontSize": 8,
                                                                                "position": "center-top",
                                                                                "fontColor": "Black",
                                                                                "format": "{%total}%",
                                                                                "offsetX": -12
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                            } else if (name == 'gray') {
                                                                if (gray == 0) {
                                                                    data.push({
                                                                        x: likertKPIData[i].progname,
                                                                        low: -red,
                                                                        high: -red - gray,
                                                                        value: gray,
                                                                        fill: '#BABABA',
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": false
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    data.push({
                                                                        x: likertKPIData[i].progname,
                                                                        low: -red,
                                                                        high: -red - gray,
                                                                        value: gray,
                                                                        fill: '#BABABA',
                                                                        total: totalGray,
                                                                        normal: {
                                                                            label: {
                                                                                "enabled": true,
                                                                                "anchor": "center",
                                                                                "position": "center-top",
                                                                                "fontColor": "Black",
                                                                                "format": "{%total}%",
                                                                                "fontSize": 8,
                                                                                "offsetX": -12
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                            }
                                                        }
                                                        var seriesKPI = chartKPI.rangeBar(data);
                                                        seriesKPI.name(name)
                                                                .stroke('3 #fff 1')
                                                                .selectionMode('none');
                                                        seriesKPI.hovered().stroke('3 #fff 1');
                                                        seriesKPI.tooltip().title()
                                                                .useHtml(true)
                                                                .fontSize(12);

                                                        // set labels position
                                                        // var labelsKPI = seriesKPI.labels();
                                                        // labelsKPI.enabled(true);
                                                        // labelsKPI.position("center");
                                                        // labelsKPI.format("{%value}");
                                                        // chartKPI.labels().fontColor("White");
                                                    };

                                                    createSeries('gray');
                                                    createSeries('red');
                                                    createSeries('yellow');
                                                    createSeries('green');

                                                    createKPISeries('gray');
                                                    createKPISeries('red');
                                                    createKPISeries('yellow');
                                                    createKPISeries('green');



                                                    // changes palette for this sample
//                                                chart.palette(anychart.palettes.distinctColors().items(['#009a84', '#FBD903', '#C00000', '#BABABA']));
                                                    chart.palette(anychart.palettes.distinctColors().items(['#BABABA', '#C00000', '#FBD903', '#009a84']));
                                                    chartKPI.palette(anychart.palettes.distinctColors().items(['#BABABA', '#C00000', '#FBD903', '#009a84']));
                                                    chart.tooltip(false);
                                                    chartKPI.tooltip(false);

                                                    var legend = chart.legend();
                                                    var legendKPI = chartKPI.legend();

//                                            
//                                            chartKPI.legend()
//                                                    .enabled(true)
//                                                    .padding([20, 0, 10, 0])
//                                                    .position('center-bottom');// legend items events adjusting
//
//                                            chart.legend()
//                                                    .enabled(true)
//                                                    .padding([20, 0, 10, 0])
//                                                    .position('center-bottom');// legend items events adjusting
                                                    chartKPI.legend()
                                                            .enabled(false);

                                                    chart.legend()
                                                            .enabled(false);
                                                    // set the action to the click event
                                                    // legend items events adjusting
                                                    legend.listen("legendItemMouseDown", function (e) {
                                                        // stop reacting on this event
                                                        e.preventDefault();
                                                    });

                                                    // set the action to the click event
                                                    legend.listen("legendItemClick", function (e) {
                                                        e.preventDefault();
                                                    });

                                                    // legend items events adjusting
                                                    legendKPI.listen("legendItemMouseDown", function (e) {
                                                        // stop reacting on this event
                                                        e.preventDefault();
                                                    });

                                                    // set the action to the click event
                                                    legendKPI.listen("legendItemClick", function (e) {
                                                        e.preventDefault();
                                                    });

                                                    // creates stack bar chart from multi series chart
                                                    chart.yScale().stackMode('value');
                                                    chart.yAxis().enabled(false);

                                                    chart.lineMarker().value(0).stroke('#CECECE');
                                                    chart.container('oweidi');
                                                    chart.draw();

                                                    chartKPI.yScale().stackMode('value');
                                                    chartKPI.yAxis().enabled(false);

                                                    chartKPI.lineMarker().value(0).stroke('#CECECE');
                                                    chartKPI.container('oweidi1');
                                                    chartKPI.draw();
                                                }
                                            }
                                            self.objType2(false);
                                            self.programCardNotNull(true);
                                        } else {
                                            var items = [];
                                            var targets = [];
                                            var areaGroups = [];
                                            var areaSeries = [];
                                            var maxAxisData = null;
                                            var nameKPIINI,polarityKPIINI;
                                            var measurementUnitAr;
                                            var lastseries,lastseries2 = 0;

                                            for (var j = 0; j < res.rows.length; j++) {

                                                var itemsarray = {};
                                                var targetsarray = {};
                                                function loadData(){
                                                        
                                                    areaGroups.push(res.rows.item(j).area);
                                                    if (Number(res.rows.item(j).series) === 0) {
                                                        itemsarray.y = lastseries;
                                                        switch(res.rows.item(j).Status){
                                                            case 'Yellow': itemsarray.color = '#FBd903'; break;
                                                            case 'Green': itemsarray.color = '#009A84'; break;
                                                            case 'Red': itemsarray.color = '#C00000'; break;
                                                            case 'Gray': itemsarray.color = '#BABABA'; break;
                                                            default: itemsarray.color = '#BABABA'; break;
                                                        }
                                                        items.push(itemsarray);

                                                        targetsarray.y = lastseries2;
                                                        targets.push(targetsarray);

                                                    }else if (res.rows.item(j).series === 'NULL') {
                                                        itemsarray.y = 1;
                                                        switch(res.rows.item(j).Status){
                                                            case 'Yellow': itemsarray.color = '#FBd903'; break;
                                                            case 'Green': itemsarray.color = '#009A84'; break;
                                                            case 'Red': itemsarray.color = '#C00000'; break;
                                                            case 'Gray': itemsarray.color = '#BABABA'; break;
                                                            default: itemsarray.color = '#BABABA'; break;
                                                        }
                                                        items.push(itemsarray);

                                                    }else if(res.rows.item(j).Target === 'NULL'){
                                                        targetsarray.y = 1;
                                                        targets.push(targetsarray);

                                                    } else {
                                                        if(res.rows.item(j).Type === 'INI') {
                                                            itemsarray.y = Number(res.rows.item(j).series) * 100;
                                                            switch(res.rows.item(j).Status){
                                                                case 'Yellow': itemsarray.color = '#FBd903'; break;
                                                                case 'Green': itemsarray.color = '#009A84'; break;
                                                                case 'Red': itemsarray.color = '#C00000'; break;
                                                                case 'Gray': itemsarray.color = '#BABABA'; break;
                                                                default: itemsarray.color = '#BABABA'; break;
                                                            }
                                                            lastseries = Number(res.rows.item(j).series) * 100;

                                                            targetsarray.y = Number(res.rows.item(j).Target) * 100;
                                                            lastseries2 = Number(res.rows.item(j).Target) * 100;
                                                        } else {
                                                            itemsarray.y = res.rows.item(j).series;
                                                            switch(res.rows.item(j).Status){
                                                                case 'Yellow': itemsarray.color = '#FBd903'; break;
                                                                case 'Green': itemsarray.color = '#009A84'; break;
                                                                case 'Red': itemsarray.color = '#C00000'; break;
                                                                case 'Gray': itemsarray.color = '#BABABA'; break;
                                                                default: itemsarray.color = '#BABABA'; break;
                                                            }
                                                            lastseries = res.rows.item(j).series;

                                                            targetsarray.y = res.rows.item(j).Target;
                                                            lastseries2 = res.rows.item(j).Target;
                                                        }
                                                        
                                                        items.push(itemsarray);
                                                        targets.push(targetsarray);


                                                    }
                                                    if (self.toggleObjType() === 'INI') {
                                                        nameKPIINI = res.rows.item(j).InivNameAr;
                                                    } else {
                                                        nameKPIINI = res.rows.item(j).KPINameArabic;
                                                    }
                                                    // if(res.rows.item(j).Polarity === '+'){
                                                    //     polarityKPIINI = ' القطبية: موجب ';
                                                    // }else if(res.rows.item(j).Polarity === '-'){
                                                    //     polarityKPIINI = ' القطبية: سالب ';                               
                                                    // }else{
                                                    //     polarityKPIINI = '';
                                                    // }

                                                    measurementUnitAr = res.rows.item(j).MeasurementUnitAr;
                                                    if(measurementUnitAr === 'نسبة مئوية (%)'){
                                                        measurementUnitAr = 'نسبة مئوية';
                                                        maxAxisData = 90;
                                                    }else if(measurementUnitAr === 'NULL'){
                                                        measurementUnitAr = 'غير معرفة';
                                                        maxAxisData = null;
                                                    }else{
                                                        maxAxisData = null;
                                                    }
                                                    if(res.rows.item(j).Type === 'INI'){
                                                        measurementUnitAr = 'نسبة مئوية';
                                                        maxAxisData = 90;
                                                    }
                                                }
                                                if (res.rows.item(j).ReportingYear == self.yearSelectVal()) {
                                                    loadData();
                                                }
                                                if (res.rows.item(j).ReportingYear == (self.yearSelectVal() - 1)) {
                                                    loadData();
                                                }
                                            }
                                                areaSeries.push({
                                                    name: nameKPIINI,
                                                    items: ['none'],
                                                    assignedToY2: "on",
                                                    markerColor: 'none',
                                                    color: 'none'
                                                },{
                                                    name: 'المستهدف',
                                                    items: targets,
                                                    assignedToY2: "on",
                                                    color: '#000000',
                                                    markerColor: '#000000'
                                                    },{
                                                    name: 'الحالي',
                                                    items: items,
                                                    assignedToY2: "on",
                                                    color: '#237BB1'
                                                });
                                                if (nameKPIINI !== 'NULL') {
                                                    self.objTypeNull(false);
                                                    self.allLineChartsData.push({
                                                        areaSeries: areaSeries,
                                                        areaGroups: areaGroups,
                                                        maxAxisData: maxAxisData,
                                                        measurementUnitAr: measurementUnitAr
                                                    });
                                                } else {
                                                    if (self.allLineChartsData().length <= 0) {
                                                        self.objTypeNull(true);
                                                    } else {
                                                        self.objTypeNull(false);
                                                    }

                                                }
                                            
                                            kpiNullText();
                                        }
                                    });


                                }

                                var barGroup1 = 'المؤشرات ';
                                var barGroup = 'المبادرات ';
                                var barGroups = [barGroup1, barGroup];
                                self.barGroupsValue(barGroups);
                            });
                        });
                    }).catch(function (err) {
                        // error! :( 
                        console.log(err);
                    });

                }

            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new DashboardViewModel();
        }
);
