/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojknockout', 'ojs/ojchart'],
    function (oj, ko, $, app) {

        function IncidentsViewModel() {
            var self = this;
            self.localeEntities = ko.observable(oj.Translations.getTranslatedString("Entities"));
            self.localeObjectives = ko.observable(oj.Translations.getTranslatedString("Objectives"));
            self.yearSelectVal = ko.observable("2018");
            self.quarterSelectVal = ko.observable("Q1");
            self.entitesNumbers = ko.observable("22");
            self.objectiveNumbers = ko.observable("22");
            self.inititivesNumbers = ko.observable("22");
            self.KPINumbers = ko.observable("22");
            self.currentLevel = ko.observable("All");
            /* toggle button variables */
            self.stackValue = ko.observable('on');
            self.orientationValue = ko.observable('horizontal');
            self.stackLabelValue = ko.observable('on');
            self.legendRenderedValue = ko.observable('off');
            self.color1 = ko.observable('#009a84');
            self.color2 = ko.observable('#FBD903');
            self.color3 = ko.observable('#C00000');
            self.color4 = ko.observable('#BABABA');
            self.KPIGray = ko.observable("");
            self.KPIGreen = ko.observable("");
            self.KPIRed = ko.observable("");
            self.KPIYellow = ko.observable("");
            self.INIGray = ko.observable("");
            self.INIGreen = ko.observable("");
            self.INIRed = ko.observable("");
            self.INIYellow = ko.observable("");
            self.barGroupsValue = ko.observableArray([]);

            /* chart data */
            self.mo2shratBarSeries = [
                { name: "Series 1", items: [{ y: 44, label: "44%" }, { y: 36, label: "36%" }], color: self.color1() },
                { name: "Series 2", items: [{ y: 9, label: "9%" }, { y: 25, label: "25%" }] },
                { name: "Series 3", items: [{ y: 42, label: "42%" }, { y: 26, label: "26%" }] },
                { name: "Series 4", items: [{ y: 5, label: "5%" }, { y: 13, label: "13%" }] }];

            self.mo2shratBarSeries[0]['color'] = self.color1();
            self.mo2shratBarSeries[1]['color'] = self.color2();
            self.mo2shratBarSeries[2]['color'] = self.color3();
            self.mo2shratBarSeries[3]['color'] = self.color4();

            self.mo2shratBarSeriesValue = ko.observableArray(self.mo2shratBarSeries);

            //Go to programs page
            self.programsOnClick = function () {
                //           $('#image1').attr('src', 'css/images/LandingPageImages/devicesHomeCard.png');

                console.log(oj.Router.rootInstance.stateId());
//                document.getElementById("programsHomeCard").src = "css/images/LandingPageImages/programsHomeCardClicked.png";
                oj.Router.rootInstance.go('projects');

            };
            //Got to devices page
            self.devicesOnClick = function () {
                console.log(oj.Router.rootInstance.stateId());
//                document.getElementById("devicesHomeCard").src = "css/images/LandingPageImages/devicesHomeCardClicked.png";
                oj.Router.rootInstance.go('devices');
            };

            self.copyDatabaseFile = function (dbName) {
                var sourceFileName = cordova.file.applicationDirectory + 'www/db/' + dbName;
                var targetDirName = cordova.file.dataDirectory;

                return Promise.all([
                    new Promise(function (resolve, reject) {
                        resolveLocalFileSystemURL(sourceFileName, resolve, reject);
                    }),
                    new Promise(function (resolve, reject) {
                        resolveLocalFileSystemURL(targetDirName, resolve, reject);
                    })
                ]).then(function (files) {
                    var sourceFile = files[0];
                    var targetDir = files[1];
                    return new Promise(function (resolve, reject) {
                        targetDir.getFile(dbName, {}, resolve, reject);
                    }).then(function () {
                        console.log("file already copied");
                    }).catch(function () {
                        console.log("file doesn't exist, copying it");
                        return new Promise(function (resolve, reject) {
                            sourceFile.copyTo(targetDir, dbName, resolve, reject);
                        }).then(function () {
                            console.log("database file copied");
                        });
                    });
                });
            };

            // if application running on device
            document.addEventListener("deviceready", onDeviceReady, false);
            function onDeviceReady() {
                self.copyDatabaseFile('adaa.db');
                query();

            }

            // Below are a set of the ViewModel methods invoked by the oj-module component.
            // Please reference the oj-module jsDoc for additional information.

            /**
             * Optional ViewModel method invoked after the View is inserted into the
             * document DOM.  The application can put logic that requires the DOM being
             * attached here. 
             * This method might be called multiple times - after the View is created 
             * and inserted into the DOM and after the View is reconnected 
             * after being disconnected.
             */
            self.connected = function () {
                // Implement if needed
                app.titleName("الرئيسية");
            };

            /**
             * Optional ViewModel method invoked after the View is disconnected from the DOM.
             */
            self.disconnected = function () {
                // Implement if needed
                app.hideBackButton(true);
            };

            /**
             * Optional ViewModel method invoked after transition to the new View is complete.
             * That includes any possible animation between the old and the new View.
             */
            self.transitionCompleted = function () {
                // Implement if needed
            };

            function getentitesNumbersInProjects() {

                self.copyDatabaseFile('adaa.db').then(function () {
                    console.log('HERE  3' + self.currentLevel());
                    var db = sqlitePlugin.openDatabase('adaa.db');
                    db.readTransaction(function (txn) {

                        var query;
                        query = 'select count(distinct   EntityCode) count from mytable where ReportingYear ='
                            + self.yearSelectVal() + ' and ReportingQuarter=\''
                            + self.quarterSelectVal() + '\' ';
                        console.log(query);

                        txn.executeSql(query, [], function (tx, res) {

                            self.entitesNumbers(res.rows.item(0).count);
                        });
                    });
                }).catch(function (err) {
                    // error! :( 
                    console.log(err);
                });

            }

            function getobjectiveNumbersInProjects() {

                self.copyDatabaseFile('adaa.db').then(function () {

                    var db = sqlitePlugin.openDatabase('adaa.db');
                    db.readTransaction(function (txn) {
                        var query;
                        query = 'select count(distinct ObejctiveCode) count  from mytable where ReportingYear ='
                            + self.yearSelectVal() + ' and ReportingQuarter=\''
                            + self.quarterSelectVal() + '\' ';

                        txn.executeSql(query, [], function (tx, res) {

                            // alert(res.rows.item(0));
                            self.objectiveNumbers(res.rows.item(0).count);

                        });
                    });
                }).catch(function (err) {
                    // error! :( 
                    console.log(err);
                });

            }


            function getINICount() {

                self.copyDatabaseFile('adaa.db').then(function () {

                    var db = sqlitePlugin.openDatabase('adaa.db');
                    db.readTransaction(function (txn) {
                        var query;
                        query = 'select   count(*)  count  from mytable where ReportingYear ='
                            + self.yearSelectVal() + ' and ReportingQuarter=\''
                            + self.quarterSelectVal() + '\' and type=\'INI\' ';
                        txn.executeSql(query, [], function (tx, res) {
                            self.inititivesNumbers(res.rows.item(0).count);
                        });
                    });
                }).catch(function (err) {
                    // error! :( 
                    console.log(err);
                });
            }

            function getKPICount() {

                self.copyDatabaseFile('adaa.db').then(function () {

                    var db = sqlitePlugin.openDatabase('adaa.db');
                    db.readTransaction(function (txn) {

                        var query;
                        query = 'select   count(*)  count  from mytable where ReportingYear ='
                            + self.yearSelectVal() + ' and ReportingQuarter=\''
                            + self.quarterSelectVal() + '\' and type=\'KPI\' ';
                        txn.executeSql(query, [], function (tx, res) {

                            self.KPINumbers(res.rows.item(0).count);

                        });
                    });
                }).catch(function (err) {
                    // error! :( 
                    console.log(err);
                });
            }

            function getKPIStatusCount() {

                self.copyDatabaseFile('adaa.db').then(function () {

                    var db = sqlitePlugin.openDatabase('adaa.db');
                    db.readTransaction(function (txn) {

                        var query = 'select   count(*)  count ,status  from mytable where ReportingYear ='
                            + self.yearSelectVal() + ' and ReportingQuarter=\''
                            + self.quarterSelectVal() + '\' and type=\'KPI\' group by status ';

                        txn.executeSql(query, [], function (tx, res) {
                            for (var i = 0; i < res.rows.length; i++) {

                                if (res.rows.item(i).Status === 'Gray') {
                                    self.KPIGray(res.rows.item(i).count);
                                } else if (res.rows.item(i).Status === 'Green') {
                                    self.KPIGreen(res.rows.item(i).count);
                                } else if (res.rows.item(i).Status === 'Red') {
                                    self.KPIRed(res.rows.item(i).count);
                                } else if (res.rows.item(i).Status === 'Yellow') {
                                    self.KPIYellow(res.rows.item(i).count);
                                }

                            }


                            fillHeaderBarChartKPI(self.KPIGray(), self.KPIYellow(), self.KPIRed(), self.KPIGray(), 1)

                        });
                    });
                }).catch(function (err) {
                    // error! :( 
                    console.log(err);
                });
            }

            function getINIStatusCount() {

                self.copyDatabaseFile('adaa.db').then(function () {

                    var db = sqlitePlugin.openDatabase('adaa.db');
                    db.readTransaction(function (txn) {

                        var query = 'select   count(*)  count ,status  from mytable where ReportingYear ='
                            + self.yearSelectVal() + ' and ReportingQuarter=\''
                            + self.quarterSelectVal() + '\' and type=\'INI\' group by status ';
                        txn.executeSql(query, [], function (tx, res) {
                            for (var i = 0; i < res.rows.length; i++) {
                                if (res.rows.item(i).Status === 'Gray') {
                                    self.INIGray(res.rows.item(i).count);
                                } else if (res.rows.item(i).Status === 'Green') {
                                    self.INIGreen(res.rows.item(i).count);
                                } else if (res.rows.item(i).Status === 'Red') {
                                    self.INIRed(res.rows.item(i).count);
                                } else if (res.rows.item(i).Status === 'Yellow') {
                                    self.INIYellow(res.rows.item(i).count);
                                }

                            }
                            console.log(query);
                            fillHeaderBarChartKPI(self.INIGreen(), self.INIYellow(), self.INIRed(), self.INIGray(), 0)

                        });
                    });
                }).catch(function (err) {
                    // error! :( 
                    console.log(err);
                });
            }

            function fillHeaderBarChartKPI(green, yellow, red, gray, barIndex) {

                var total = green + yellow + red + gray;

                var greenValue = calculatePer(green, total);
                self.mo2shratBarSeries[0].items[barIndex].y = greenValue;
                self.mo2shratBarSeries[0].items[barIndex].label = greenValue + '%';

                var yellowValue = calculatePer(yellow, total);
                self.mo2shratBarSeries[1].items[barIndex].y = yellowValue;
                self.mo2shratBarSeries[1].items[barIndex].label = yellowValue + '%';

                var redValue = calculatePer(red, total);
                self.mo2shratBarSeries[2].items[barIndex].y = redValue;
                self.mo2shratBarSeries[2].items[barIndex].label = redValue + '%';



                var grayValue = 100 - (Number(greenValue) + Number(yellowValue) + Number(redValue));
                self.mo2shratBarSeries[3].items[barIndex].y = grayValue;
                self.mo2shratBarSeries[3].items[barIndex].label = grayValue + '%';

                self.mo2shratBarSeriesValue([]);

                self.mo2shratBarSeriesValue(self.mo2shratBarSeries);
            }


            function calculatePer(val1, total) {
                var perc;
                if (isNaN(val1) || isNaN(total)) {
                    perc = " ";
                } else {
                    perc = ((val1 / total) * 100).toFixed();
                }

                return perc;
            }


            function query() {

                if (self.currentLevel() === 'All') {
                    getentitesNumbersInProjects();
                    getobjectiveNumbersInProjects();
                    getINICount();
                    getKPICount();
                    getKPIStatusCount();
                    getINIStatusCount();
                }
                self.copyDatabaseFile('adaa.db').then(function () {

                    var db = sqlitePlugin.openDatabase('adaa.db');
                    db.readTransaction(function (txn) {
                        var query = 'select ProgramCode from mytable where ReportingYear =' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\'  and ProgramCode<> \'NULL\' group by ProgramCode order by COUNT(*) desc';

                        txn.executeSql(query, [], function (tx, res) {

                            for (var x = 0; x < res.rows.length; x++) {

                                var programCode;

                                programCode = res.rows.item(x).ProgramCode;
                                query = 'select  count(*) count ,type , status , ProgramCode,ProgramNameAr ,(SELECT v.CommitteeHead FROM vision_table v  where  v.Code = ProgramCode ) as CommitteeHead   from mytable where  ReportingYear = ' + self.yearSelectVal() + ' and ReportingQuarter=\'' + self.quarterSelectVal() + '\'  and ProgramCode=\'' + programCode + '\' group by type, status ';

                            }

                            var barGroup = 'المؤشرات ';
                            var barGroup1 = 'المبادرات ';
                            var barGroups = [barGroup, barGroup1];
                            self.barGroupsValue(barGroups);
                        });
                    });
                }).catch(function (err) {
                    console.log(err);
                });

            }

        }

        /*
         * Returns a constructor for the ViewModel so that the ViewModel is constructed
         * each time the view is displayed.  Return an instance of the ViewModel if
         * only one instance of the ViewModel is needed.
         */
        return new IncidentsViewModel();
    }
);
