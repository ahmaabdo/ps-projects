function addFuseModelToIndexedDB(services, app, rootViewModel, commonhelper) {
    var lang = '';
    
  
    if (localStorage.getItem("selectedLanguage") == 'ar') {
        lang = "AR";
    } else {
        lang = "US";
    }
  
   
//    services.getMasterDataReport(rootViewModel.userName(), lang).then(function (data) {
//              
//        var fuseData = jQuery.parseJSON(data.FUSEREPORT);
//        var allEitSegment = jQuery.parseJSON(data.ALLEITSEGMENT);
//        var eitName = jQuery.parseJSON(data.EITNAME);
//      
//        var userRole = jQuery.parseJSON(data.USERROLE);
//        //var personalDetails=jQuery.parseJSON(data.personalDetails);
//        
//        
//        sessionStorage.setItem("fuseData", JSON.stringify(fuseData));
//        sessionStorage.setItem("allEitSegment", JSON.stringify(allEitSegment));
//        sessionStorage.setItem("eitName", JSON.stringify(eitName));
//        sessionStorage.setItem("userRole", JSON.stringify(userRole));
//        //rootViewModel.personDetails().citizenship_name = personalDetails.CITIZENSHIP_NAME;
//        //rootViewModel.personDetails().legislation_code = personalDetails.LEGISLATION_CODE;
//        //rootViewModel.personDetails().legal_entity_name = personalDetails.LEGAL_ENTITY_NAME;
//        //console.log(JSON.parse(sessionStorage.getItem("userRole"))); 
//        rootViewModel.globalFuseModel([]);
//        //  rootViewModel.globalFuseModel(jQuery.parseJSON(data));
//        rootViewModel.globalFuseModel(fuseData);
//        rootViewModel.allEITArray(allEitSegment);
//        populateGlobalValues(rootViewModel, commonhelper);
//        papulateUserRole(rootViewModel, commonhelper, userRole);
//        populateEitName(rootViewModel, commonhelper, eitName);
//        rootViewModel.constructNavTopData();
//        rootViewModel.OpenReportSeesion();
//        //allListSegment
//    },
//            app.failCbFn);

}
function allListSegment (){
    var listSegment = []; 
    
}
function papulateUserRole(rootViewModel, commonhelper, userRole) {
    
     var  userRoles;
    rootViewModel.userRoleArr(userRole);
    console.log(rootViewModel.userRoleArr());
    if(userRole.length>1){
        
        userRoles = JSON.parse(sessionStorage.getItem("userRole"));
        for (var i = 0; i < userRole.length; i++) {
            if (userRole[i].ROLE_NAME === "HHA PAAS Employee Custom") {
                rootViewModel.EmployeeAuthentication(userRole[i].ROLE_NAME);
            } else if (userRole[i].ROLE_NAME === "Application Implementation Administrator") {
                rootViewModel.adminAuthentication(userRole[i].ROLE_NAME);
            } else if (userRole[i].ROLE_NAME === "HHA-Line Manager Custom V3") {
                rootViewModel.lineManager(userRole[i].ROLE_NAME);
            }
        }
    } else {
        var roleName = userRole.ROLE_NAME;
        if (roleName === "HHA PAAS Employee Custom") {
            rootViewModel.EmployeeAuthentication(roleName);
        } else if (roleName === "Application Implementation Administrator") {
            rootViewModel.adminAuthentication(roleName);
        } else if (roleName === "HHA-Line Manager Custom V3") {
            rootViewModel.lineManager(roleName);
        }
    }
}
//function populateEitName(rootViewModel, commonhelper, eitName) {
//    rootViewModel.globalEitNameReport([]);
//    for (var i = 0; i < eitName.length; i++) {
//
//        rootViewModel.globalEitNameReport.push({"value": eitName[i].DESCRIPTIVE_FLEX_CONTEXT_CODE, "label": eitName[i].DESCRIPTIVE_FLEX_CONTEXT_NAME});
//    }
//    rootViewModel.globalEitNameReportLength(rootViewModel.globalEitNameReport().length);
//    rootViewModel.globalEitNameReport.push({"value": 'XXX_RECRUITMENT_REQUEST', "label": 'Manager Recruitment Request'});
//    rootViewModel.globalEitNameReport.push({"value": 'XXX_HR_SPECIALIST_RECRUITMENT_REQUEST', "label": 'Recruitment Request - Initial Details Form'});
//   
//}
function populateGlobalValues(rootViewModel, commonhelper) {
    rootViewModel.globalHRCYesNo([]);
    rootViewModel.globalJobName([]);
    rootViewModel.globalJobCatagory([]);
    rootViewModel.globalGeneralGroup([]);
    rootViewModel.globalGroupType([]);
    rootViewModel.globalEFFUDT([]);
    rootViewModel.globalJobCodes([]);
    rootViewModel.globalHrGrades([]);
    rootViewModel.globalPositionsName([]);
    rootViewModel.globalPositionsAction([]);
    rootViewModel.globalEitNameReport([]);
    rootViewModel.globalJobsNames([]);
    rootViewModel.globalEITReportLookup([]);
    rootViewModel.globalEmployeeEITLookup([]);
    rootViewModel.globalEITDefultValueParamaterLookup([]);
    rootViewModel.globalSpecialistLookup([]);
    rootViewModel.globalManagerLookup([]);
    
        rootViewModel.globalNationalityLookup([]);
    rootViewModel.globalMartialStatusLookup([]);
    rootViewModel.globalNationalIdentifierTypeLookup([]);
    rootViewModel.globalEITDefultValueParamaterLookup([]);
    rootViewModel.globalProjectTaskLookup([]);
    rootViewModel.globalSalaryTypeLookup([]);
    rootViewModel.globalEmploymentStatusLookup([]);
    rootViewModel.globalHiringSourceLookup([]);
    rootViewModel.globalAllowneceTypeLookup([]);
    rootViewModel.globalPayrollListLookup([]);
    rootViewModel.globalContractTypeLookup([]);
    rootViewModel.globalQualifyingUnitsLookup([]);

    for (var i = 0; i < rootViewModel.globalFuseModel().length; i++) {
        if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HRC_YES_NO) {
            rootViewModel.globalHRCYesNo.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.EIT_Name) {
            rootViewModel.globalEitNameReport.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.jobName) {
            rootViewModel.globalJobName.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.employeeReqeustLookup) {
            rootViewModel.globalEmployeeEITLookup.push(rootViewModel.globalFuseModel()[i].LOOKUP_CODE);
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.XXX_HR_JOB_CATEGORY) {
            rootViewModel.globalJobCatagory.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.pdfReportLookup) {
            rootViewModel.globalPDFReport.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HRDYNAMICCOLUMNREPORT) {
            rootViewModel.globalEITReportLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
                , "description": rootViewModel.globalFuseModel()[i].DESCRIPTION
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.XXX_HR_SPECIALIST_REQUESTS) {
            rootViewModel.globalSpecialistLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
                , "description": rootViewModel.globalFuseModel()[i].DESCRIPTION
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.XXX_HR_MANAGER_REQUESTS) {
            rootViewModel.globalManagerLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
                , "description": rootViewModel.globalFuseModel()[i].DESCRIPTION
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.XXX_HR_JOB_GENERAL_GROUP) {
            rootViewModel.globalGeneralGroup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_DYNAMIC_COLUMN_REPORT) {
            rootViewModel.globalEITDefultValueParamaterLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].DESCRIPTION, "labelAr": rootViewModel.globalFuseModel()[i].DESCRIPTION
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.JOB_TYPE_GROUP) {
            rootViewModel.globalGroupType.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_JOB_CODES) {
            rootViewModel.globalJobCodes.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR, "description": rootViewModel.globalFuseModel()[i].DESCRIPTION
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_GRADE) {
            rootViewModel.globalHrGrades.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_POSITIONS_NAME) {
            rootViewModel.globalPositionsName.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_POSITION_ACTIONS) {
            rootViewModel.globalPositionsAction.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        } else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_QUALIFICATIONS) {
            rootViewModel.globalQualifications.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_NATIONALITY) {
            rootViewModel.globalNationalityLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_MAR_STATUS) {
            rootViewModel.globalMartialStatusLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.NATIONAL_IDENTIFIER_TYPE) {
            rootViewModel.globalNationalIdentifierTypeLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_CA_TASK) {
            rootViewModel.globalProjectTaskLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_SALARY_TYPE) {
            rootViewModel.globalSalaryTypeLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_EMPLOYMENT_STATUS) {
            rootViewModel.globalEmploymentStatusLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_HIRING_SOURCE) {
            rootViewModel.globalHiringSourceLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.QUALIFYING_UNITS) {
            rootViewModel.globalQualifyingUnitsLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.CONTRACT_TYPE) {
            rootViewModel.globalContractTypeLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.HR_PAYROLL_LIST) {
            rootViewModel.globalPayrollListLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.LOOKUP && rootViewModel.globalFuseModel()[i].LOOKUP_TYPE === commonhelper.ALLOWANCE_TYPE) {
            rootViewModel.globalAllowneceTypeLookup.push({
                "value": rootViewModel.globalFuseModel()[i].LOOKUP_CODE, "label": rootViewModel.globalFuseModel()[i].MEANING, "labelAr": rootViewModel.globalFuseModel()[i].MEANING_AR
            });
        }
        
        else if (rootViewModel.globalFuseModel()[i].DATA_TYPE === commonhelper.REPORT_DATA_TYPE.UDT && rootViewModel.globalFuseModel()[i].USER_TABLE_NAME === commonhelper.HR_EFF_SEARCH) {
            rootViewModel.globalEFFUDT.push({
                "displaySequence": rootViewModel.globalFuseModel()[i].DISPLAY_SEQUENCE, "rowLowRangeOrName": rootViewModel.globalFuseModel()[i].ROW_LOW_RANGE_OR_NAME,
                "rowName": rootViewModel.globalFuseModel()[i].ROW_NAME, "userColumnName": rootViewModel.globalFuseModel()[i].USER_COLUMN_NAME,
                "userTableName": rootViewModel.globalFuseModel()[i].USER_TABLE_NAME
            });


        }

    }
    rootViewModel.globalEitNameReportLength(rootViewModel.globalEitNameReport().length);
}
function drawAttachment(count) {
    //drawLabel(lblValue, div);


    var input = document.createElement("img");

    input.setAttribute('class', 'attClass' + count);
    input.setAttribute('alt', "Image preview...");
    input.setAttribute('height', "60");
    input.setAttribute('width', "60");
    var parent = document.getElementById("attachedfile");

    // return input ;
    parent.appendChild(input);
}

function drawAttachment2(count, baseStr64) {
    //drawLabel(lblValue, div);

    var input = document.createElement("img");

    input.setAttribute('class', 'attClass' + count);
    input.setAttribute('alt', "Image preview...");
    input.setAttribute('height', "60");
    input.setAttribute('width', "60");
    input.setAttribute('src', baseStr64);
    var parent = document.getElementById("attachedfile");

    // return input ;
    parent.appendChild(input);
}


var count = 0;
var checkPreviews = {};
var preview;
function prev(files) {

    drawAttachment(count);

    preview = document.querySelector('.attClass' + count);
    preview1 = document.querySelector('.attClass1');
    preview2 = document.querySelector('.attClass2');
    var xx = 0;

    checkPreviews['checkpreview' + count] = true;

    var checkpreview1 = true;
    var checkpreview2 = true;
    var checkpreview3 = true;
    for (var i = 0; i < files.length; i++) {
        if (checkPreviews['checkpreview' + count]) {
            checkPreviews['checkpreview' + count] = false;
            setupReader(files[i], preview);

            continue;
        }



    }
    count++;


}
;

function setupReader(file, preview) {
    var name = file.name;
    var reader = new FileReader();
    reader.onload = function (e) {
        // get file content  
        var text = e.target.result;
        preview.src = reader.result;

        if (file) {
            reader.readAsDataURL(file);
        }
    };
    reader.readAsText(file, "UTF-8");
}


//function showEmplDetails(ko, services, app, km, mainData, commonhelper) {
//    
//    var rootViewModel = app;
//    //app.loading(true);
//    var url = new URL(window.location.href);
//    var username;
//    var hosturl;
//    var jwt;
//    var deviceIp;
//    var today = new Date();
//    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
//    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
//    var deviceAndBrowserDetails = navigator.userAgent;
//    var LogindateTime;
//
//    if (url.searchParams.get("jwt")) {
//        if (rootViewModel.hostUrl() == null || rootViewModel.hostUrl().length == 0) {
//            hosturl = url.searchParams.get("host");
//            rootViewModel.hostUrl(hosturl);
//        }
//
//        if (rootViewModel.jwt() == null || rootViewModel.jwt().length == 0) {
//            jwt = url.searchParams.get("jwt");
//            ;
//            rootViewModel.jwt(jwt);
//        }
//
//        if (rootViewModel.userName() == null || rootViewModel.userName().length == 0) {
//            jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
//            username = jwtJSON.sub;
//
//            rootViewModel.userName(username);
//            //  displayname = userName.split('.')[0] + username.split('.')[1];
//        }
//        $(".apLoginBtn").addClass("loading");
//        rootViewModel.disOnLogin(true);
//        LogindateTime = date + ' ' + time;
//        rootViewModel.loginDateFromSass(LogindateTime);
//        var payloadLoginHistory = {};
//        $.getJSON("https://api.ipify.org/?format=json", function (e) {
//            deviceIp = e.ip;
//            payloadLoginHistory = {
//                "personNumber": username,
//                "loginDate": LogindateTime,
//                "browserAndDeviceDetails": deviceAndBrowserDetails,
//                "deviceIp": deviceIp
//            };
//
//            sessionStorage.setItem("SShrLOginHistroy", JSON.stringify(payloadLoginHistory));
//
//
//            var SShrLoginHistory = function (data) {
//
//            };
//
//
//            services.addGeneric("SShrLoginHistory/addSShrLoginHistory", sessionStorage.getItem("SShrLOginHistroy")).then(SShrLoginHistory, rootViewModel.failCbFn());
//        });
//    }
//
//    var getEmpDetails = function (data) {
//
//        if (!data) {
//            return false;
//        } else {
//
//            if (typeof $(".profilepic")[0] !== 'undefined' && typeof $(".profilepic")[1] !== 'undefined') {
//                if (rootViewModel.personDetails().picBase64 && rootViewModel.personDetails().picBase64 !== null) {
//                    $(".profilepic")[0].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
//                    $(".profilepic")[1].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
//                } else {
//                    $(".profilepic")[0].src = "css/images/avatar_24px.png";
//                    $(".profilepic")[1].src = "css/images/avatar_24px.png";
//                }
//            } 
//            rootViewModel.personDetails(data);
//
//            //-----to be Test  Effect ----------------------
//            rootViewModel.userAuthentication();
//            //  papulateUserRole(rootViewModel, commonhelper ,userRole);           
//            //-----to be Test  Effect ----------------------
//            rootViewModel.empName(rootViewModel.personDetails().displayName);
//            $(".infosecempname").text(rootViewModel.empName());
//            
//            rootViewModel.userLogin(rootViewModel.personDetails().displayName);
//            rootViewModel.isUserLoggedIn(true);
//
////                                showEmplDetails(ko, services, self, km, false);
//            //constructNavLeftData();
////                            constructNavTopData();
//            rootViewModel.navDataSource(new oj.ArrayTableDataSource(rootViewModel.NavIConsTopArr(), {idAttribute: 'id'}));
//
//            addFuseModelToIndexedDB(services, rootViewModel, rootViewModel, commonhelper);
//            getPaaSLookup(services, rootViewModel, rootViewModel, commonhelper);
//            getEITIcons(services, rootViewModel, rootViewModel, commonhelper);
//            getPersonNumber(services, rootViewModel, rootViewModel, commonhelper);
//            getApprovalCondition(services, rootViewModel, rootViewModel, commonhelper);
//            getAllPosition(services, rootViewModel, rootViewModel, commonhelper);
//            getRoles(services, rootViewModel, rootViewModel, commonhelper);
//            rootViewModel.getUserRoles();
//
//                var reportPaylod = {"reportName": "ROLES"};
//                var getRolesCBCF = function (data) {
//                    rootViewModel.allRoles(data);
//                };
//                services.getGenericReportAsync(reportPaylod).then(getRolesCBCF, app.failCbFn);
//            
//            //app.loading(false);
//
//            return true;
//        }
//    };
//    if (mainData === true) {
//        services.getEmpDetails(rootViewModel.userName(), rootViewModel.jwt(), rootViewModel.hostUrl()).then(getEmpDetails, app.failCbFn);
//    } else {
//        services.getEmpDetailsAsync(rootViewModel.userName(), rootViewModel.jwt(), rootViewModel.hostUrl()).then(getEmpDetails, app.failCbFn);
//    }
//
//
//}
function searchArray(nameKey, searchArray) {
    for (var i = 0; i < searchArray.length; i++) {
        if (searchArray[i].value === nameKey) {
            return searchArray[i].label;
        }
    }
}

function searchArrayValue(nameKey, searchArray) {
    for (var i = 0; i < searchArray.length; i++) {
        if (searchArray[i].label === nameKey) {
            return searchArray[i].value;
        }
    }
}
function searchArrayContainValue(nameKey, searchArray) {
    var arr = [];
    for (var i = 0; i < searchArray.length; i++) {
        if (searchArray[i].value === nameKey) {
            arr.push(searchArray[i].label);
        }
    }
    return arr;
}
function searchArrayContainDescription(nameKey, searchArray) {
    var arr = [];
    for (var i = 0; i < searchArray.length; i++) {
        if (searchArray[i].value === nameKey) {
            arr.push(searchArray[i].description);
        }
    }
    return arr;
}
//build addational details for job screens
function buildJobNavigationPage(oj) {
    var getTranslation = oj.Translations.getTranslatedString;
    var navDataLeft = [];

    navDataLeft = [
        {
            name: getTranslation("pages.job"), id: 'searchJob', iconClass: 'oj-navigationlist-item-icon fa fa-briefcase'
        },

        {
            name: getTranslation("job.additionalDetails"), id: 'additionalDetailsSummary', iconClass: 'oj-navigationlist-item-icon fa fa-pencil-square-o'
        },
        {
            name: getTranslation("others.validGrade"), id: 'validGradeSummary', iconClass: 'oj-navigationlist-item-icon fa fa-tasks'
        }
    ];
    return navDataLeft;
}
//build addational details for positions screens
function buildPositionNavigationPage(oj) {
    var getTranslation = oj.Translations.getTranslatedString;
    var navDataLeft = [];

    navDataLeft = [
        {
            name: getTranslation("pages.position"), id: 'searchPosition', iconClass: 'oj-navigationlist-item-icon fa fa-address-book-o'
        },

        {
            name: getTranslation("position.positionSummaryAdditionalDetails"), id: 'positionSummaryAdditionalDetails', iconClass: 'oj-navigationlist-item-icon fa fa-pencil-square-o'
        }
    ];
    return navDataLeft;
}
function getPaaSLookup(services, app, rootViewModel, commonhelper) {
    var getLookup = function (data) {
        rootViewModel.PaaSLookup([]);
        for (var i = 0; i < data.length; i++) {
            rootViewModel.PaaSLookup.push({
                "ID": data[i].id, "name": data[i].name, "code": data[i].code, "valuAr": data[i].valueArabic, "valuEn": data[i].valueEnglish

            });
        }
            

    };
    var getBankDetailsReport = function (data) {
                console.log(data);
                rootViewModel.bankNameArr([]);
                if(data.length > 1){
                for (var index = 0; index < data.length; index++) {
                    rootViewModel.bankNameArr.push({
                        label: data[index].label,
                        value: data[index].value
                    });
                }
            }
            else
            {
                rootViewModel.bankNameArr.push({
                        label: data.label,
                        value: data.value
                    });
            }
            };
            var payload = {"reportName": "XXX_HR_BANK"};
            services.getGenericReport(payload).then(getBankDetailsReport, app.failCbFn);



    services.getGenericAsync(commonhelper.getAllPaaSLookup).then(getLookup, app.failCbFn);

}


function getEITIcons(services, app, rootViewModel, commonhelper) {
    var getIcons = function (data) {
        rootViewModel.EITIcons([]);
        for (var i = 0; i < data.length; i++) {
            rootViewModel.EITIcons.push({
                "ID": data[i].id, "eitCode": data[i].eitCode, "icons": 'css/images/icons/' + data[i].icons ,"leftIcons":data[i].leftIcons

            });

        }
         

    };

    services.getGenericAsync(commonhelper.getIcons).then(getIcons, app.failCbFn);

}

function getPersonNumber(services, app, rootViewModel, commonhelper) {
    var getpersonNumber = function (data) {
        rootViewModel.PersonNumberArr([]);
        for (var i = 0; i < data.length; i++) {
            rootViewModel.PersonNumberArr.push({
                "value": data[i].PERSON_ID, "label": data[i].FULL_NAME

            });

        }


    };
    var reportPaylod = {"reportName": "employeeNameReport"};
     services.getGenericReport(reportPaylod).then(getpersonNumber, app.failCbFn);

}

function getPaaSDefultValue(eitCode, segmentName, paasDefultValue) {
    var query;
    //self.passReturnArr([]);
    for (var i = 0; i < paasDefultValue.length; i++) {
        if (paasDefultValue[i].eitCode == eitCode && paasDefultValue[i].segmentName == segmentName) {

            query = paasDefultValue[i].query;

        }
    }
    return query;

}
function getApprovalCondition(services, app, rootViewModel, commonhelper) {
    var getApprovalCondition = function (data) {
        rootViewModel.ApprovalCondition([]);
        for (var i = 0; i < data.length; i++) {
            rootViewModel.ApprovalCondition.push({
                "id": data[i].id, "firstKeyType": data[i].firstKeyType, "operation": data[i].operation, "secondKeyType": data[i].secondKeyType, "firstKeyValue": data[i].firstKeyValue,"secondKeyValue": data[i].secondKeyValue,"secondEitSegment": data[i].secondEitSegment,"firstEitSegment": data[i].firstEitSegment,"eitCode": data[i].eitCode,"approvalCode": data[i].approvalCode

            });
        }
    };

    services.getGenericAsync(commonhelper.getAllApprovalCondition).then(getApprovalCondition, app.failCbFn);

}
function getAllPosition(services, app, rootViewModel, commonhelper) {
    var getAllPosition = function (data) {
    
        rootViewModel.roleOptionType([]);
        for (var i = 0; i < data.length; i++) {
            rootViewModel.roleOptionType.push({
                "value": data[i].positionId, "label": data[i].name

            });
            
        }
      
    };

    services.getGenericAsync("saasposition/").then(getAllPosition, app.failCbFn);

}

function getRoles(services, app, rootViewModel, commonhelper) {
    var getRolesCBCF = function (data) {
    
        rootViewModel.rolesOption([]);
        for (var i = 0; i < data.length; i++) {
            rootViewModel.rolesOption.push({
                "value": data[i].ROLE_ID, "label": data[i].ROLE_NAME
            });            
        }      
    };
    var reportPaylod = {"reportName": "ROLES"};
   services.getGenericReport(reportPaylod).then(getRolesCBCF, app.failCbFn);
}


function getBankDetails(services, app, rootViewModel, commonhelper) {
            var getBankDetailsReport = function (data) {
                console.log(data);
                rootViewModel.bankNameArr([]);
                if(data.length > 1){
                for (var index = 0; index < data.length; index++) {
                    rootViewModel.bankNameArr.push({
                        label: data[index].label,
                        value: data[index].value
                    });
                }
            }
            else
            {
                rootViewModel.bankNameArr.push({
                        label: data.label,
                        value: data.value
                    });
            }
            };
            var payload = {"reportName": "XXX_HR_BANK"};
            services.getGenericReport(payload).then(getBankDetailsReport, app.failCbFn);
        };
