
define([], function () {
 function Screen() {
 var self = this;
self.buildScreen =  function buildScreen (EIT,formName ,model , isDisabled){
    var varName ;
    var req;
    var isDisabledName;
    
    for (var i =0;i<EIT.length;i++){   
            var div = document.createElement("div");
           div.setAttribute('class', 'oj-flex');
           var parent = document.getElementById("xx");          
            parent.appendChild(div);
            if(EIT[i].REQUIRED_FLAG =="Y"){
                req= true;
            }else{
                req =false;
            }
             
            
        //----------------End Div -------------------
       // drawLabel(EIT[i].END_USER_COLUMN_NAME);
        isDisabledName = 'isDisabledx.'+'isDisabled'+EIT[i].DESCRIPTION;       
       varName='model.'+EIT[i].DESCRIPTION;
        if(EIT[i].FLEX_value_SET_NAME=="100 Character No Validation"){                       
            drawInputText (varName,EIT[i].END_USER_COLUMN_NAME ,req ,div,isDisabledName);
        }else if(EIT[i].FLEX_value_SET_NAME=="10 Number"){
            drawInputNumber(varName,EIT[i].END_USER_COLUMN_NAME,req,div ,isDisabledName);    
        }else if(EIT[i].FLEX_value_SET_NAME=="HRC_STANDARD_DATE"){
           drawInputDate(varName,EIT[i].END_USER_COLUMN_NAME ,req,div,isDisabledName);
        }else {
              drawList(varName,EIT[i].END_USER_COLUMN_NAME ,req,div,isDisabledName);
        }
       
    }
    
    
    
};
function drawDiv(div){
     var divCreated = document.createElement("div");
           divCreated.setAttribute('class', 'oj-flex-item');
           var parent = div;           
            parent.appendChild(divCreated); 
            return divCreated;
}
function drawInputText (varName,lblValue ,required ,div,isDisabledName){
    //drawLabel(lblValue, div);
    var input = document.createElement("oj-input-text");
     input.setAttribute('disabled', '{{'+isDisabledName+'}}');
    input.setAttribute('value','{{'+varName+'}}');
    input.setAttribute('required',required); 
    var parent =    drawLabel(lblValue,div);
     // return input ; 
   parent.appendChild(input);
}
function drawInputNumber(varName,lblValue ,required,div,isDisabledName){
 // drawLabel(lblValue,div);
    var input = document.createElement("oj-input-number");
    input.setAttribute('disabled', '{{'+isDisabledName+'}}');
    input.setAttribute('value','{{'+varName+'}}'); 
    input.setAttribute('required',required); 
    var parent =    drawLabel(lblValue,div);
    //return input ; 
    parent.appendChild(input);
}
function drawLabel(lblValue ,div){
    var label = document.createElement("oj-label");
    
    label.innerHTML = lblValue;    
    var parent = drawDiv(div);    
    parent.appendChild(label);
   
   
     return parent;
}
function drawInputDate(varName,lblValue ,required,div,isDisabledName){
    //drawLabel(lblValue,div);
    var input = document.createElement("oj-input-date");
    input.setAttribute('disabled', '{{'+isDisabledName+'}}');
    input.setAttribute('value','{{'+varName+'}}'); 
    input.setAttribute('required',required); 
    var parent =   drawLabel(lblValue,div);;
    //  return input ; 
    parent.appendChild(input);
}
function drawList(varName,lblValue ,required,div,isDisabledName){
          
    var input = document.createElement("oj-select-one");
     input.setAttribute('disabled', '{{'+isDisabledName+'}}');
    input.setAttribute('value','{{'+varName+'}}');
  //  input.setAttribute('options','{{dataArray}}');
    input.setAttribute('required',false); 
    var parent =   drawLabel(lblValue,div);
    
    parent.appendChild(input);
           
}

 }
 return new Screen();
});
