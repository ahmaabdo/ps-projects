"use strict";
module.exports = {
    metadata: () => ({
            "name": "downPaymentValidation",
            "properties": {
                "downPayment": {"type": "double", "required": true},
                "keepTurn": {"type": "boolean", "required": false},
				"count": {"type": "int", "required": false}
            },
            "supportedActions": ["valid", "invalid","threeTriel"]
        }),
    invoke: (conversation, done) => {
        var downPayment = conversation.properties().downPayment;
        var keepTurn = conversation.properties().keepTurn;
        var lang = conversation.variable("lang");
		var count = conversation.properties().count;
		var financeAmount = conversation.variable("financeAmount");
		if(downPayment < financeAmount) {
			conversation.transition("valid");
		}else {
			if(count < 3){
				conversation.variable("count" , count+1);
				if (lang === 'en') {
					conversation.reply({text: 'Down payment should be less than finance amount.'});
				} else {
					conversation.reply({text: 'الدفعة الاولى يجب ان لا تتعدى قيمة مبلغ التمويل.'});
				}
				
				conversation.transition("invalid");
			}else if(count >= 3){
				if (lang === 'en') {
					conversation.reply({text: 'You entered 3 wrong trail.'});
				} else {
					conversation.reply({text: 'لقد ادخلت 3 محاولات خاطئة.'});
				}
				conversation.transition("threeTriel");
			}
		}
		done();
    }
};