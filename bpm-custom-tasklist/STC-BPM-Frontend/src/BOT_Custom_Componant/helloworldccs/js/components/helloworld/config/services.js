/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['config/serviceconfig', 'util/commonhelper'], function (serviceConfig, commonHelper) {

    /**
     * The view model for managing all services
     */
    function services() {

        var self = this;

        var servicesHost = commonHelper.getSaaSHost();
        var paasServiceHost = commonHelper.getPaaSHost();
        var appServiceHost = commonHelper.getAppHost();
        var biReportServletPath = commonHelper.getBiReportServletPath();
        var restPath = commonHelper.getInternalRest();
        // POST example
        self.authenticate = function (userName, password) {
            var serviceURL = restPath + "login/login2";
            var payload = {
                "userName": userName, "password": password
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.getNotification = function (headers) {
            var serviceURL = paasServiceHost + 'workflowApproval/';
            return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };
        self.workflowAction = function (payload) {
            var serviceURL = paasServiceHost + 'workflowApproval/';
            var headers = {
            };

            return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, headers, true);
        };
        self.getFuseModelReport = function () {
            var serviceURL = appServiceHost + restPath + biReportServletPath;
            console.log(serviceURL);
            var payload = {
                "reportName": "FuseReport2"
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        self.getEIT = function (Structure_Code) {
            var serviceURL = appServiceHost + restPath + biReportServletPath;
            var payload = {
                "Structure_Code" : Structure_Code, "reportName" : "EIT Report"
            };

            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };        
        self.getDynamicReport = function (str) {
            var serviceURL = appServiceHost + restPath + biReportServletPath;
            var payload = {
                "str" : str, "reportName" : "DynamicReport2"
            };

            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        self.getElementEntry = function () {
            var serviceURL = paasServiceHost + "elementEntry";
            var headers = {
            };

            return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);

        };
        self.addElementEntry = function (payload) {
            var serviceURL = paasServiceHost + "elementEntry";
            var headers = {
            };

            return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.editElementEntry = function (payload) {
            var serviceURL = paasServiceHost + "elementEntry";
            var headers = {
            };

            return serviceConfig.callPutApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.getElementEnteryById = function (id) {
            var serviceURL = paasServiceHost + "elementEntery/" + id;
            var headers = {
            };

            return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.getElementEntryValue = function () {
            var serviceURL = paasServiceHost + "elementEnteryValue";
            var headers = {
            };

            return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);

        };
        self.addElementEntryValue = function (payload) {
            var serviceURL = paasServiceHost + "elementEnteryValue";
            var headers = {
            };

            return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.editElementEntryValue = function (payload) {
            var serviceURL = paasServiceHost + "elementEnteryValue";
            var headers = {
            };

            return serviceConfig.callPutApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.getPersonFuseModelReport = function (personId) {
            var serviceURL = appServiceHost + restPath + biReportServletPath;
            var payload = {
                "person_id": personId, "reportName": "PersonFuseReport"
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        self.getEmpDetails = function (url) {

           // var serviceURL = restPath + "emp/info?username2=" + username2 + "&token=" + token + "&hostURL=" + hostURL;
		   var serviceURL = url;
            var headers = {
            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.getAllPositions = function () {
            var serviceURL = appServiceHost + restPath + biReportServletPath;
            var payload = {
                "reportName": "GetAllPositions"

            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        self.getEmpDetailsbyPersonId = function (personId, token, hostURL) {

            var serviceURL = restPath + "emp/infoById?personId=" + personId + "&token=" + token + "&hostURL=" + hostURL;
            var headers = {
            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.searchEmployees = function (name, nationalId, personNumber, managerId, effectiveAsof, token, hostURL) {

            var serviceURL = restPath + "emp/searchEmployees?name=" + name + "&nationalId=" + nationalId + "&personNumber=" + personNumber + "&managerId=" + managerId + "&effectiveAsof=" + effectiveAsof + "&token=" + token + "&hostURL=" + hostURL;
            var headers = {
            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.submitElementEntry = function (payload) {
            var serviceURL = appServiceHost + restPath + "upload/elementEntry";
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.getPersonElementEntry = function (personId, startDate) {
            var serviceURL = appServiceHost + restPath + biReportServletPath;
            var payload = {
                "P_PERSON_ID": personId, "P_START_DATE": startDate, "reportName": "PersonElementEntryReport"
            };
            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };
        self.getEmployees = function (userName, token, hostURL) {
            var serviceURL = restPath + "emp/getEmployees?username=" + name + "&token=" + token + "&hostURL=" + hostURL;
            var headers = {
            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };
        self.getHDLFiles = function (id, ssType, personNumber, effectiveDate, processedInPayroll, assigment) {
            var serviceURL = paasServiceHost + "HDL";
            var headers = {

            };

            if (id) {
                headers.id = id;
            }

            if (ssType) {
                headers.SS_TYPE = ssType;
            }

            if (effectiveDate) {
                headers.CREATION_DATE = effectiveDate;
            }

            if (personNumber) {
                headers.PERSON_NUMBER = personNumber;
            }

            if (processedInPayroll) {
                headers.STATUS = processedInPayroll;
            }

            if (assigment) {
                headers.ASSIGNMENT_STATUS_TYPE = assigment;
            }

            return serviceConfig.callGetApexService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.processElementEntry = function (payload) {
            var serviceURL = appServiceHost + restPath + "upload/elementEntry";
            var headers = {

            };
            return serviceConfig.callPutService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.getPayrollReport = function (personId) {
            var serviceURL = appServiceHost + restPath + biReportServletPath;
            var payload = {
                "reportName": "EmployeePrepaymentInPeriodReport"
            };

            var headers = {

            };
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.getGeneric = function (serviceName,headers) {
            var serviceURL = restPath + serviceName;
            
//          headers.Authorization = "Basic YW1yby5hbGZhcmVzQGFwcHNwcm8tbWUuY29tOkFwcHNwcm9AMTIzNA=="
             return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers);
        };
        self.editGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
            };
            return serviceConfig.callPutApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.addGeneric = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
            };
            return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.jobs = function (payload, type) {
            console.log(type);
            var serviceURL = "webapi/jobs/" + type;
            var headers = {
            };
            console.log(serviceConfig.contentTypeApplicationJSON);
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.searcheGeneric = function (serviceName,payload) {
            var serviceURL =restPath+serviceName;
            var headers = {
                 "content-type": "application/x-www-form-urlencoded"
            };
            
            return serviceConfig.callPostServiceUncodeed(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
        };                
        self.getJobByJobName = function () {
            var serviceURL = "webapi/jobs/search/66";
            console.log(serviceURL);

    
            var headers = {

            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers);
        };//PATCH
        self.genericPatch = function (serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
            };
            return serviceConfig.callPostApexService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

    };
     
    return new services();
});