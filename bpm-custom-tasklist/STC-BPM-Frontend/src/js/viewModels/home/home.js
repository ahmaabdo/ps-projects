define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojtrain', 'config/services',
    'util/commonhelper', 'knockout-mapping', 'ojs/ojmodel', 'ojs/ojknockout',
    'ojs/ojknockout-model', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox',
    'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojinputnumber', 'ojs/ojpopup',
    'ojs/ojknockout-validation', 'ojs/ojfilmstrip', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset',
    'ojs/ojradioset', 'ojs/ojlabel', 'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'jquery-calendar-picker',
    'ojs/ojinputtext', 'ojs/ojavatar'],
        function (oj, ko, $, app, ojtrain, services, commonhelper, km) {

            function homeViewModel() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;

                self.agreement = ko.observableArray();
                self.EitName = ko.observableArray()
                self.currentColor = ko.observable("red");
                self.nam = ko.observable();
                self.validationData = ko.observable();
                self.valueofsharethouts = ko.observable("Share thoughts with colleagues..");
                self.jobLbl = ko.observable();
                self.positionLbl = ko.observable();
                self.actualPositionLbl = ko.observable();
                self.notificationlbl = ko.observable();
                self.eitIconsLoaded = ko.observable(false);
                self.tempIcons = ko.observableArray([]);
                self.tempIcons().length = 15;
                self.icons = ko.observableArray([]);
                self.ProbationAndEvaluationlbl = ko.observable();
                self.employeeTimeAttandancelbl = ko.observable();
                self.personDetails = ko.observable(app.personDetails());
                self.followersLbl = ko.observable();
                self.followingLbl = ko.observable();
                self.employeeNewsLbl = ko.observable();
                self.myFlagsLbl = ko.observable();
                self.countPending = ko.observable();
                self.countApproved = ko.observable();
                self.countRejected = ko.observable();
                self.avatarSize = ko.observable("md");
                self.firstName = ko.observable();
                self.lastName = ko.observable();
                self.initials = ko.observable();
                self.recruitmnetLbl = ko.observable();
                self.hrrecruitmnetLbl = ko.observable();
                self.inquireAboutApprovedRecruitmentsLbl = ko.observable();
                self.loading = function () {
                    app.loading(true);
                };

                self.error = function () {
                    app.hasErrorOccured(false);
                    app.router.go("error");
                };

                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }
                });

                self.fixedEitIcon = ko.observableArray([
                    {icon: 'fa fa-money'},
                    {icon: 'fa fa-envelope'},
                    {icon: 'fa fa-money'},
                    {icon: 'fa fa-file-text'},
                    {icon: 'fa fa-file-text'},
                    {icon: 'fa fa-file-text'},
                    {icon: 'fa fa-pencil-square-o'},
                    {icon: 'fa fa-file-text'},
                    {icon: 'fa fa-money'},
                    {icon: 'fa fa-envelope'},
                    {icon: 'fa fa-money'},
                    {icon: 'fa fa-file-text'},
                    {icon: 'fa fa-file-text'},
                    {icon: 'fa fa-file-text'},
                    {icon: 'fa fa-pencil-square-o'},
                    {icon: 'fa fa-file-text'}
                ]);
                
                
                self.getSelfServicesWhenChangeLanguage=function(){
                    
                     self.icons([]);
                      if (typeof $(".profilepic")[0] !== 'undefined' && typeof $(".profilepic")[1] !== 'undefined') {
                        if (rootViewModel.personDetails().picBase64 && rootViewModel.personDetails().picBase64 !== null) {
                            $(".profilepic")[0].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                            $(".profilepic")[1].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                        } else {
                            $(".profilepic")[0].src = "css/images/avatar_24px.png";
                            $(".profilepic")[1].src = "css/images/avatar_24px.png";
                        }
                    }
                    self.firstName(app.personDetails().firstName);
                    self.lastName(app.personDetails().lastName);
                    self.initials(oj.IntlConverterUtils.getInitials(self.firstName(),self.lastName()));
                    self.personDetails(app.personDetails());
                    self.EitName(app.globalEitNameReport());
                    if (app.conutEitRequest()) {
                            self.countPending(app.conutEitRequest().countPendingApproved);
                            self.countApproved(app.conutEitRequest().countApproved);
                            self.countRejected(app.conutEitRequest().countRejected);
                        }

                    

                    if (self.validationData()) {
                       
                        var data = self.validationData();
                        if (app.globalEitNameReportLength() > 1) {
                            var fristValue;
                            var operation;
                            var secondValue;
                            var validEit = [];
                            var notValidEit = [];
                            var IconType=1;

                            for (var i = 0; i < data.length; i++) {
                                fristValue = app.personDetails()[data[i].personInfromation];
                                operation = data[i].operation;
                                secondValue = data[i].staticValue;


                                if (app.compare(fristValue, operation, secondValue)) {

                                    validEit.push(data[i].eitCode);

                                } else {

                                    notValidEit.push({eitcode: data[i].eitCode, operation: operation});
                                }
                            }

                            var colorPar = 0;
                            for (var i = 0; i < self.EitName().length; i++) {
                                var IconBGSetG = ['_RBGCG_0', '_RBGCG_1', '_RBGCG_2', '_RBGCG_3', '_RBGCG_4', '_RBGCG_5', '_RBGCG_6', '_RBGCG_7', '_RBGCG_8', '_RBGCG_9'];
                                var choosenColor = IconBGSetG[colorPar];

                                if (app.globalEmployeeEITLookup().indexOf(self.EitName()[i].value) != -1
                                        && validEit.indexOf(self.EitName()[i].value) == -1) {
                                   
                                    self.eitIconsLoaded(true);

                                    self.icons.push(
                                            {
                                                label: 'c',
                                                value: 'demo-icon-circle-a',
                                                router: 'dynamicSummary',
                                                iconType: app.getEITIcons(self.EitName()[i].value,"ImageIcone"),
                                                text: self.EitName()[i].label,
                                                visible: true,
                                                IconVarBG: choosenColor,
                                                code: self.EitName()[i].value},
                                            );
                                }


                                colorPar++;
                                if (colorPar == 10) {
                                    colorPar = 0;
                                }
                            }
                            if (notValidEit.length > 0) {
                                 
                                for (var j = 0; j < app.globalEitNameReport().length; j++) {

                                    for (var x = 0; x < notValidEit.length; x++) {

                                        if ( app.globalEmployeeEITLookup().indexOf(app.globalEitNameReport()[j].value) != -1 && app.globalEitNameReport()[j].value == notValidEit[x].eitcode && notValidEit[x].operation == "!=") {

                                          
                                            self.eitIconsLoaded(true);

                                            self.icons.push(
                                                    {
                                                        label: 'c',
                                                        value: 'demo-icon-circle-a',
                                                        router: 'dynamicSummary',
                                                        iconType: app.getEITIcons(notValidEit[x].eitcode,"ImageIcone"),
                                                        text: app.globalEitNameReport()[j].label,
                                                        visible: true,
                                                        IconVarBG: choosenColor,
                                                        code: notValidEit[x].eitcode},
                                                    );
                                        }
                                    }
                                }
                            }
                            if (app.userRoleArr()[0].ROLE_NAME === "Human Resource Specialist") {
                                self.icons.push(
                                        {
                                            label: 'c',
                                            value: 'demo-icon-circle-a',
                                            router: 'hrSpecialistRecruitmentRequestSummary',
                                            iconType: app.getEITIcons('XXX_HR_SPECIALIST_RECRUITMENT_REQUEST',"ImageIcone"),
                                            text: self.hrrecruitmnetLbl(),
                                            visible: true,
                                            IconVarBG: choosenColor,
                                            code: ''}
                                );

                                self.icons.push(
                                        {
                                            label: 'c',
                                            value: 'demo-icon-circle-a',
                                            router: 'inquireAboutApprovedRecruitments',
                                            iconType: app.getEITIcons('XXX_RECRUITMENT_REQUEST',"ImageIcone"),
                                            text: self.inquireAboutApprovedRecruitmentsLbl(),
                                            visible: true,
                                            IconVarBG: choosenColor,
                                            code: ''}
                                );
                            } else if (app.userRoleArr()[0].ROLE_NAME === "Line Manager")
                            {
                                console.log(app.userRoleArr()[0])
                                self.icons.push(
                                        {
                                            label: 'c',
                                            value: 'demo-icon-circle-a',
                                            router: 'recruitmentRequestSummary',
                                            iconType: app.getEITIcons('XXX_RECRUITMENT_REQUEST',"ImageIcone"),
                                            text: self.recruitmnetLbl(),
                                            visible: true,
                                            IconVarBG: choosenColor,
                                            code: ''}
                                );
                            }

                        }
                    }
                };
                
                
                
//
//                self.computeIconsArray = ko.computed(function () {
//                    if (typeof $(".profilepic")[0] !== 'undefined' && typeof $(".profilepic")[1] !== 'undefined') {
//                        if (rootViewModel.personDetails().picBase64 && rootViewModel.personDetails().picBase64 !== null) {
//                            $(".profilepic")[0].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
//                            $(".profilepic")[1].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
//                        } else {
//                            $(".profilepic")[0].src = "css/images/avatar_24px.png";
//                            $(".profilepic")[1].src = "css/images/avatar_24px.png";
//                        }
//                    }
//                    self.firstName(app.personDetails().firstName);
//                    self.lastName(app.personDetails().lastName);
//                    self.initials(oj.IntlConverterUtils.getInitials(self.firstName(),self.lastName()));
//                    self.personDetails(app.personDetails());
//                    self.EitName(app.globalEitNameReport());
//                    if (app.conutEitRequest()) {
//                            self.countPending(app.conutEitRequest().countPendingApproved);
//                            self.countApproved(app.conutEitRequest().countApproved);
//                            self.countRejected(app.conutEitRequest().countRejected);
//                        }
//
//                    if (self.eitIconsLoaded() == true) {
//                        return;
//                    }
//
//                    if (self.validationData()) {
//                       
//                        var data = self.validationData();
//                        if (app.globalEitNameReportLength() > 1) {
//                            var fristValue;
//                            var operation;
//                            var secondValue;
//                            var validEit = [];
//                            var notValidEit = [];
//                            var IconType=1;
//
//                            for (var i = 0; i < data.length; i++) {
//                                fristValue = app.personDetails()[data[i].personInfromation];
//                                operation = data[i].operation;
//                                secondValue = data[i].staticValue;
//
//
//                                if (app.compare(fristValue, operation, secondValue)) {
//
//                                    validEit.push(data[i].eitCode);
//
//                                } else {
//
//                                    notValidEit.push({eitcode: data[i].eitCode, operation: operation});
//                                }
//                            }
//
//                            var colorPar = 0;
//                            for (var i = 0; i < self.EitName().length; i++) {
//                                var IconBGSetG = ['_RBGCG_0', '_RBGCG_1', '_RBGCG_2', '_RBGCG_3', '_RBGCG_4', '_RBGCG_5', '_RBGCG_6', '_RBGCG_7', '_RBGCG_8', '_RBGCG_9'];
//                                var choosenColor = IconBGSetG[colorPar];
//
//                                if (app.globalEmployeeEITLookup().indexOf(self.EitName()[i].value) != -1
//                                        && validEit.indexOf(self.EitName()[i].value) == -1) {
//                                   
//                                    self.eitIconsLoaded(true);
//
//                                    self.icons.push(
//                                            {
//                                                label: 'c',
//                                                value: 'demo-icon-circle-a',
//                                                router: 'dynamicSummary',
//                                                iconType: app.getEITIcons(self.EitName()[i].value,"ImageIcone"),
//                                                text: self.EitName()[i].label,
//                                                visible: true,
//                                                IconVarBG: choosenColor,
//                                                code: self.EitName()[i].value},
//                                            );
//                                }
//
//
//                                colorPar++;
//                                if (colorPar == 10) {
//                                    colorPar = 0;
//                                }
//                            }
//                            if (notValidEit.length > 0) {
//                                 
//                                for (var j = 0; j < app.globalEitNameReport().length; j++) {
//
//                                    for (var x = 0; x < notValidEit.length; x++) {
//
//                                        if ( app.globalEmployeeEITLookup().indexOf(app.globalEitNameReport()[j].value) != -1 && app.globalEitNameReport()[j].value == notValidEit[x].eitcode && notValidEit[x].operation == "!=") {
//
//                                          
//                                            self.eitIconsLoaded(true);
//
//                                            self.icons.push(
//                                                    {
//                                                        label: 'c',
//                                                        value: 'demo-icon-circle-a',
//                                                        router: 'dynamicSummary',
//                                                        iconType: app.getEITIcons(notValidEit[x].eitcode,"ImageIcone"),
//                                                        text: app.globalEitNameReport()[j].label,
//                                                        visible: true,
//                                                        IconVarBG: choosenColor,
//                                                        code: notValidEit[x].eitcode},
//                                                    );
//                                        }
//                                    }
//                                }
//                            }
//                            self.icons.push(
//                                    {
//                                        label: 'c',
//                                        value: 'demo-icon-circle-a',
//                                        router: 'employeeTimeAttandanceSummary',
//                                        iconType: 'css/images/icons/overtime_assignment.png',
//                                        text: self.employeeTimeAttandancelbl(),
//                                        visible: true,
//                                        IconVarBG: choosenColor,
//                                        code: ''
//                                    });
//
//                        }
//                    }
//
//                });

                self.pagingModel = null;

                self.iconNavigation = function (router, code) {
                    app.loading(true);
                    oj.Router.rootInstance.store(code);
                    oj.Router.rootInstance.go(router);
                    return true;
                };

                getPagingModel = function () {
                    if (!self.pagingModel) {
                        var filmStrip = $("#filmStrip");
                        var pagingModel = filmStrip.ojFilmStrip("getPagingModel");
                        self.pagingModel = pagingModel;
                    }
                    return self.pagingModel;
                };

                //--------------------Start Validation------------------//
                self.summaryObservableArray = ko.observableArray();

                self.validationRole = async function () {
                    var getRoleValidationCbFn = function (data) {
                        self.validationData(data);
                       if(data.length>0){
                           
                          self.getSelfServicesWhenChangeLanguage(); 
                          
                       }
                    };

                     var serviceName = commonhelper.getAllRoleSetupValidation;
                    await services.getAllRoleValidation(serviceName).then(getRoleValidationCbFn, app.failCbFn);
                }
                //-------------------End Validation --------------------//


                self.handleAttached = function (info) {
//                    self.EitName(app.globalEitNameReport());
                    self.validationRole();
                    app.loading(false);
                    app.backBtnVis(false);
//                    self.getSelfServicesWhenChangeLanguage();
                    initTranslations();
                    if (typeof $(".profilepic")[0] !== 'undefined' && typeof $(".profilepic")[1] !== 'undefined') {
                        if (rootViewModel.personDetails().picBase64 && rootViewModel.personDetails().picBase64 !== null) {
                            $(".profilepic")[0].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                            $(".profilepic")[1].src = "data:image/png;base64," + rootViewModel.personDetails().picBase64;
                        } else {
                            $(".profilepic")[0].src = "css/images/avatar_24px.png";
                            $(".profilepic")[1].src = "css/images/avatar_24px.png";
                        }
                    }
                    if (!self.validationData())
                        self.validationRole();
                        

                };

                function initTranslations() {
                    self.nam(getTranslation("common.name"));
                    self.jobLbl(getTranslation("pages.job"));
                    self.positionLbl(getTranslation("pages.position"));
                    self.actualPositionLbl(getTranslation("pages.actualPosition"));
                    self.notificationlbl(getTranslation("notification.notificationTile"));
                    self.ProbationAndEvaluationlbl(getTranslation("pages.ProbationAndEvaluation"));
                    self.employeeTimeAttandancelbl(getTranslation("pages.employeeTimeAttandance"));
                    self.valueofsharethouts(getTranslation('others.valueofsharethouts'));
                    self.followingLbl(getTranslation('others.following'));
                    self.followersLbl(getTranslation('others.Followers'));
                    self.employeeNewsLbl(getTranslation('others.employeeNews'));
                    self.myFlagsLbl(getTranslation("others.myFlags"));
                    self.recruitmnetLbl(getTranslation("recruitmentRequest.recruitmentRequestScreen"));
                    self.hrrecruitmnetLbl(getTranslation("recruitmentRequest.hrSpecialistRecruitmentRequest"));
                    self.inquireAboutApprovedRecruitmentsLbl(getTranslation("recruitmentRequest.inquireAboutApprovedRecruitments"));
                }

                return true;
            }

            return new homeViewModel();
        });
