/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * workListScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'config/buildScreen', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojknockout-validation', 'ojs/ojlabel', 'ojs/ojtable'],
        function (oj, ko, $, app, commonUtil, services, buildScreen, ArrayDataProvider) {
            /**
             * The view model for the main content view template
             */
            function workListScreenContentViewModel() {
                var self = this;
                self.lang = "";
//                self.positionName = ko.observable();
                self.companyName = ko.observable();
                self.adminUser = ko.observable();
                self.adminPassword = ko.observable();
                self.hostName = ko.observable();
                self.backActionLbl = ko.observable("Back");
                self.addLbl = ko.observable("Add");
                self.editLbl = ko.observable("Edit");
                self.editMode = ko.observable(false);
                self.addMode = ko.observable(true);
                self.data = ko.observable();
                self.pagetype = ko.observable();
                self.Number = ko.observable();

                self.searchBTN = function () {
                    app.loading(true);

                    if (self.pagetype() == 'ADD') {
                        var payload = {
                            "companyName": self.companyName(),
                            "adminUsername": self.adminUser(),
                            "adminPassword": self.adminPassword(),
                            "hostName": self.hostName()

                        };
                        var getValidGradeCBF = function (data) {
                            oj.Router.rootInstance.go('SetupSummeryScreen');
                        };
                        var failCbFn = function () {
                        };
                        services.addGeneric("setup/" + self.pagetype(), payload).then(getValidGradeCBF, failCbFn);
                    } else if (self.pagetype() == 'EDIT') {
                        console.log(receivedData);

                        var payload = {
                            "id": receivedData.id,
                            "companyName": self.companyName(),
                            "adminUsername": self.adminUser(),
                            "adminPassword": self.adminPassword(),
                            "hostName": self.hostName()

                        };
                        var getValidGradeCBF = function (data) {
                            oj.Router.rootInstance.go('SetupSummeryScreen');
                        };
                        var failCbFn = function () {
                        };
                        services.addGeneric("setup/" + self.pagetype(), payload).then(getValidGradeCBF, failCbFn);
                    }

                };


                self.backAction = function () {
                    app.loading(true);
                    oj.Router.rootInstance.go('SetupSummeryScreen');
                };

                if (app.getLocale() == 'ar') {
                    self.lang = "AR";
                } else {
                    self.lang = "US";
                }

                self.handleAttached = function (info) {
                    var receivedData = oj.Router.rootInstance.retrieve();
                    console.log(receivedData);
                    self.pagetype(receivedData.type);
                    self.data(receivedData.row);
                    app.loading(false);

                    if (self.pagetype() == 'EDIT') {
                        self.companyName(receivedData.companyName);
                        self.adminUser(receivedData.adminUsername);
                        self.adminPassword(receivedData.adminPassword);
                        self.hostName(receivedData.hostName);
                        self.editMode(true);
                        self.addMode(false);
                    } else {
                        self.editMode(false);
                        self.addMode(true);
                    }

                };
                self.handleDetached = function (info) {
                };

            }
            return workListScreenContentViewModel;
        });