define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojarraydataprovider', 'appController',
    'util/commonhelper', 'config/services', 'ojs/ojknockout', 'ojs/ojlabel', 'ojs/ojmessages',
    'ojs/ojtable', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojarraytabledatasource', 'ojs/ojinputtext', 'ojs/ojdialog'],
        function (oj, ko, $, ArrayDataProvider, app, commonUtil, services) {
            /**
             * The view model for the main content view template
             */
            function taskLogTrackerContentViewModel() {
                var self = this;

                self.searchLbl = ko.observable();
                self.resendLbl = ko.observable();
                self.taskNumberLbl = ko.observable();
                self.clearLbl = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.resendShow = ko.observable(false);
                self.taskNumber = ko.observable();
                self.serviceActionType = ko.observableArray([]);
                self.selectedDetaildData = ko.observable();
                self.tableSelectedIndex = ko.observable();
                self.tableDataArray = ko.observableArray([]);
                self.tableDataSource = new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.tableDataArray));
                self.tableColumnArray = ko.observableArray();
                self.AttachmentData = ko.observableArray();
                self.koArray = ko.observableArray([]);
                self.encodedAttachment = ko.observable();
                self.contentType = ko.observable();
                self.secondKoArray = ko.observableArray();
                self.getAllDataByNumber = ko.observable();
                self.urlArray = ko.observableArray();
                self.commentDataTwo = ko.observableArray();
                self.employeeName = ko.observable();
// define messages
                self.messages = ko.observableArray();
                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };
                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };

                self.handleAttached = function (info) {
                    console.log('handle attach');
                    self.getTasksLog();
                    console.log(self.tableDataArray());
                };

                /***************** main functions actions ****************/
                self.searchBtn = function () {
                    console.log(self.taskNumber());
                    self.searchInTasksLog();
                };
                self.resendTaskBtn = function () {
                    document.querySelector("#yesNoDialog").open();
                };
                self.clearBtn = function () {
                    self.getTasksLog();
                };
                self.confirmResend = function () {
                    self.resendAction();
                    document.querySelector("#yesNoDialog").close();
                };
                self.cancelResend = function () {
                    document.querySelector("#yesNoDialog").close();
                };
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    var currentRow = data.currentRow;
                    self.tableSelectedIndex(currentRow['rowIndex']);
                    console.log(self.tableDataArray()[self.tableSelectedIndex()])
                    if (currentRow !== null &&
                            (self.tableDataArray()[self.tableSelectedIndex()].saasStatus == 'fail'
                                    || self.tableDataArray()[self.tableSelectedIndex()].inboxStatus == 'fail'
                                    || self.tableDataArray()[self.tableSelectedIndex()].mySqlStatus == 'fail')) {
                        self.resendShow(true);
                        self.getAllDataByNumber(self.tableDataArray()[self.tableSelectedIndex()]);
                        self.employeeName(self.tableDataArray()[self.tableSelectedIndex()].nextApprover);
                        self.taskNumber(self.tableDataArray()[self.tableSelectedIndex()].taskNumber);
                        console.log(self.employeeName()+'  '+ self.taskNumber());
                    } else {
                        self.resendShow(false);
                    }
                };
                /***************** others functions ****************/
                /*************** attachment ****************/
                // desktop attachment
                
                self.AttachOkBTNForApp =async function (event) {
                    console.log(event)
                    self.secondKoArray([]);
                    console.log(self.selectedDetaildData());
                    var data = JSON.parse(self.selectedDetaildData().serviceData)[0];
                    self.secondKoArray.push({
                        attachmentScope: 'TASK',
                        attachmentSize: 0,
                        encodedAttachment: data.encodedAttachment,
                        href: '',
                        rel: 'self',
                        title: data.title,
                        updatedBy: '',
                        updatedDate: '',
                        uri: {
                            href: '',
                            length: 0,
                            rel: 'stream'
                        },
                        userId: typeof data.userId == 'undefined' ? self.getAllDataByNumber().userName : data.userId,
                        taskId: typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId
                    });
                    var attachJsonString = JSON.stringify(self.secondKoArray());
                    var success002 = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Fail in resend ',
                                    autoTimeout: 0
                                });
                            } else {
                                self.messages.push({
                                    severity: 'confirmation',
                                    summary: 'Resend Sucess',
                                    autoTimeout: 0
                                });
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail002 = function (data) {
                        self.messages.push({
                            severity: 'error',
                            summary: 'Fail in resend ',
                            autoTimeout: 0
                        });
                    };
                    console.log(attachJsonString);
                    await services.addGeneric("bpmWorklist/addAttachment/"+self.employeeName()+"/"+self.taskNumber(), attachJsonString).then(success002, fail002);
                };
                // url attachment
                
                self.addAttachmentByUrlForApprove =async function () {
                    self.urlArray([]);
                    var data = JSON.parse(self.selectedDetaildData().serviceData)[0];
                    console.log(data)
                    self.urlArray.push({
                        attachmentScope: 'TASK',
                        attachmentSize: 0,
                        encodedAttachment: '',
                        href: data.href,
                        rel: 'self',
                        title: data.title,
                        updatedBy: '',
                        updatedDate: '',
                        uri: {
                            href: data.href,
                            length: 0,
                            rel: 'rel'
                        },
                        userId: typeof data.userId == 'undefined' ? self.getAllDataByNumber().userName : data.userId,
                        taskId: typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId
                    });
                    var attachmentUrlJsonString = JSON.stringify(self.urlArray());
                    console.log(attachmentUrlJsonString);
                    var success = function (data) {
                        console.log(data);
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Fail in resend ',
                                    autoTimeout: 0
                                });
                            } else {
                                self.messages.push({
                                    severity: 'confirmation',
                                    summary: 'Resend Sucess',
                                    autoTimeout: 0
                                });
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        self.messages.push({
                            severity: 'error',
                            summary: 'Fail in resend ',
                            autoTimeout: 0
                        });
                    };
                    await services.addGeneric("bpmWorklist/addAttachmentURL/" + self.employeeName() + "/" + self.taskNumber(), attachmentUrlJsonString).then(success, fail);
                };
                /*************** reject ***************/
                self.rejectRewaardRequst =async function () {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);
                    var payload = {"taskId": typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId,
                        "outcome": "REJECT",
                        "employeeName": self.employeeName(),
                        "taskNumber": self.taskNumber()
                    };
                    var success = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Fail in resend ',
                                    autoTimeout: 0
                                });
                            } else {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Resend Sucess',
                                    autoTimeout: 0
                                });
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        self.messages.push({
                            severity: 'error',
                            summary: 'Fail in resend ',
                            autoTimeout: 0
                        });
                    }
                    await services.addGeneric("bpmWorklist/updateTaskOutcome", payload).then(success, fail);
                };
                /**************** approve *****************/
                self.approveRequst =async function () {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);

                    var payload = {
                        "taskId": typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId,
                        "outcome": "APPROVE",
                        "employeeName": self.employeeName(),
                        "taskNumber": self.taskNumber()
                    };
                    var success = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Fail in resend ',
                                    autoTimeout: 0
                                });
                            } else {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Resend Sucess',
                                    autoTimeout: 0
                                });
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        self.messages.push({
                            severity: 'error',
                            summary: 'Fail in resend ',
                            autoTimeout: 0
                        });
                    };
                   await services.addGeneric("bpmWorklist/updateTaskOutcome", payload).then(success, fail);
                };
                /******************* add comment ********************/
                self.addComment =async function () {
                    self.commentDataTwo([]);
                    var data = JSON.parse(self.selectedDetaildData().serviceData)[0];
                    self.commentDataTwo.push({commentScope: '',
                        commentStr: data.commentStr,
                        updatedBy: '',
                        updateddDate: '',
                        userId: '',
                        taskId: typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId});
                    var commentJsonString = JSON.stringify(self.commentDataTwo());
                    console.log(commentJsonString);
                    var success001 = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Fail in resend ',
                                    autoTimeout: 0
                                });
                            } else {
                                self.messages.push({
                                    severity: 'confirmation',
                                    summary: 'Resend Sucess',
                                    autoTimeout: 0
                                });
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail001 = function (data) {
                        self.messages.push({
                            severity: 'error',
                            summary: 'Fail in resend ',
                            autoTimeout: 0
                        });
                    };
                       await services.addGeneric("bpmWorklist/addComment/"+self.employeeName()+"/"+self.taskNumber(), commentJsonString).then(success001, fail001);
                }
                ////////////// suspend ///////////

                self.suspendYES =async function () {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);
                    var payload = {"taskId": typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId};
                    var success = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Fail in resend ',
                                    autoTimeout: 0
                                });
                            } else {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Resend Sucess',
                                    autoTimeout: 0
                                });
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        self.messages.push({
                            severity: 'error',
                            summary: 'Fail in resend ',
                            autoTimeout: 0
                        });
                    };
                   await services.addGeneric("bpmWorklist/actionType/suspendTask/"+self.employeeName()+"/"+self.taskNumber(), payload).then(success, fail);
                };
                //////////////// withdraw //////////////////
                self.withdrawYES =async function () {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);
                    var payload = {"taskId": typeof userId.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId};
                    var success = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Fail in resend ',
                                    autoTimeout: 0
                                });
                            } else {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Resend Sucess',
                                    autoTimeout: 0
                                });
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        self.messages.push({
                            severity: 'error',
                            summary: 'Fail in resend ',
                            autoTimeout: 0
                        });
                    };
                   await services.addGeneric("bpmWorklist/actionType/withDrawTask/"+self.employeeName()+"/"+self.taskNumber(), payload).then(success, fail);
                };
                ///////////////// esclation ////////////////
                self.escalateYES =async function () {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);
                    var payload = {"taskId": typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId};
                    var success = function (data) {
                       data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Fail in resend ',
                                    autoTimeout: 0
                                });
                            } else {
                                self.messages.push({
                                    severity: 'error',
                                    summary: 'Resend Sucess',
                                    autoTimeout: 0
                                });
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        self.messages.push({
                            severity: 'error',
                            summary: 'Fail in resend ',
                            autoTimeout: 0
                        });
                    };
                   await services.addGeneric("bpmWorklist/actionType/escalateTask/"+self.employeeName()+"/"+self.taskNumber(), payload).then(success, fail);
                };

                /////////// reaasign group ////////////
                self.reassignOKGroup =async function () {
                    var data = JSON.parse(self.selectedDetaildData().serviceData)[0];
                    var payload = {"taskId": typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId,
                        "userName": data.userName,
                        "scope": data.scope};
                    var reassignPayloadStringForGrStr = JSON.stringify(payload);
                    var success = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                commentResponseForReject = 'fail';
                            } else {
                                commentResponseForReject = 'success';
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                commentResponseForReject = 'fail';
                            } else {
                                commentResponseForReject = 'success';
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                   await services.addGeneric("bpmWorklist/actionType/reassignTask/"+self.employeeName()+"/"+self.taskNumber(), reassignPayloadStringForGrStr).then(success, fail);

                };
                ///////// reassign user ///////////////
                self.reassignOKUser =async function () {
                    var data = JSON.parse(self.selectedDetaildData().serviceData)[0];
                    var payload = {"taskId": typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId,
                        "userName": data.userName,
                        "scope": data.scope};
                    var reassignPayloadString = JSON.stringify(payload);
                    var success = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                commentResponseForReject = 'fail';
                            } else {
                                commentResponseForReject = 'success';
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                commentResponseForReject = 'fail';
                            } else {
                                commentResponseForReject = 'success';
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    await services.addGeneric("bpmWorklist/actionType/reassignTask/"+self.employeeName()+"/"+self.taskNumber(), reassignPayloadString).then(success, fail);
                };
                ////////////// delwgate user /////////////////

                self.DelegateOKUser =async function () {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);
                    var payload = {"taskId": typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId,
                        "userName": data.userName};
                    var delegatePayloadString = JSON.stringify(payload);
                    var success = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                commentResponseForReject = 'fail';
                            } else {
                                commentResponseForReject = 'success';
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                    var fail = function (data) {
                        data = JSON.parse(data);
                        if (data.taskExist == 'Yes') {
                            if (data.status != 'Success') {
                                commentResponseForReject = 'fail';
                            } else {
                                commentResponseForReject = 'success';
                                self.updateDetaild();
                            }
                        } else {
                            self.messages.push({
                                severity: 'error',
                                summary: 'This task is no longer exist',
                                autoTimeout: 0
                            });
                        }
                    };
                   await services.addGeneric("bpmWorklist/DelegateTask/"+self.employeeName()+"/"+self.taskNumber(), delegatePayloadString).then(success, fail);
                };
                /********************* inbox insert *******************/
                self.insertInbox = function () {
                    var success = function (data) {
                        if (data != 'Success') {
                            data = 'fail';
                        } else {
                            self.updateDetaild();
                            data = 'success';
                        }
                    };
                    var fail = function (data) {
                    };
                    services.addGeneric("inbox/createNewIgateInbox", self.selectedDetaildData().serviceData).then(success, fail);

                };
                /*************************** inbox update **********************/
                self.updateInbox = function () {
                    var success = function (data) {
                        if (data != 'Success') {
                            data = 'fail';
                        } else {
                            self.updateDetaild();
                            data = 'success';
                        }
                    };
                    var fail = function (data) {
                    };
                    services.addGeneric("inbox/editIgateInbox", self.selectedDetaildData().serviceData).then(success, fail);
                };
                /************************* inbox delete ******************/
                self.inboxResend = function () {
                    var success002 = function (data) {
                        if (data != 'Success') {
                            data = 'fail';
                        } else {
                            self.updateDetaild();
                            data = 'success';
                        }
                    };
                    var fail002 = function (data) {
                    };
                    services.addGeneric("retryInbox/resendInbox", self.selectedDetaildData().serviceData).then(success002, fail002);
                };
                /****************************************************************/
                /********************* mySql insert *******************/
                self.insertMySql = function (event) {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);

                    var payload = {
                        taskNumber: typeof data.taskNumber == 'undefined' ? self.getAllDataByNumber().taskNumber : data.taskNumber,
                        taskId: typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId,
                        userName: typeof data.userName == 'undefined' ? self.getAllDataByNumber().taskNumber : data.userName,
                        taskStatus: typeof data.taskStatus == 'undefined' ? self.getAllDataByNumber().taskStatus : data.taskStatus,
                        creationDate: typeof data.creationDate == 'undefined' ? self.getAllDataByNumber().creationDate : data.creationDate,
                        creatorId: typeof data.creatorId == 'undefined' ? self.getAllDataByNumber().createdBy : data.creatorId,
                        updatedDate: typeof data.updatedDate == 'undefined' ? self.getAllDataByNumber().updatedDate : data.updatedDate,
                        updatedBy: typeof data.updatedBy == 'undefined' ? self.getAllDataByNumber().updatedBy : data.updatedBy,
                        personNumber: typeof data.personNumber == 'undefined' ? self.getAllDataByNumber().personNumber : data.personNumber
                    };
                    var success = function (data) {
                        if (typeof data.taskNumber == 'undefined') {
                            data = 'fail';
                        } else {
                            self.updateDetaild();
                            data = 'success';
                        }
                    };
                    var fail = function (data) {
                    };
                    //call mySql insert here
                    services.addGeneric("tasksLog/addTaskDetails", payload).then(success, fail);

                };
                /*************************** mySql update **********************/
                self.updateMySql = function (event) {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);
                    var payload = {
                        taskNumber: typeof data.taskNumber == 'undefined' ? self.getAllDataByNumber().taskNumber : data.taskNumber,
                        taskId: typeof data.taskId == 'undefined' ? self.getAllDataByNumber().taskId : data.taskId,
                        userName: typeof data.userName == 'undefined' ? self.getAllDataByNumber().taskNumber : data.userName,
                        taskStatus: typeof data.taskStatus == 'undefined' ? self.getAllDataByNumber().taskStatus : data.taskStatus,
                        creationDate: typeof data.creationDate == 'undefined' ? self.getAllDataByNumber().creationDate : data.creationDate,
                        creatorId: typeof data.creatorId == 'undefined' ? self.getAllDataByNumber().createdBy : data.creatorId,
                        updatedDate: typeof data.updatedDate == 'undefined' ? self.getAllDataByNumber().updatedDate : data.updatedDate,
                        updatedBy: typeof data.updatedBy == 'undefined' ? self.getAllDataByNumber().updatedBy : data.updatedBy,
                        personNumber: typeof data.personNumber == 'undefined' ? self.getAllDataByNumber().personNumber : data.personNumber
                    };
                    var success = function (data) {
                        if (typeof data.taskNumber == 'undefined') {
                            data = 'fail';
                        } else {
                            self.updateDetaild();
                            data = 'success';
                        }
                    };
                    var fail = function (data) {
                    };
                    //call mySql update here
                    services.addGeneric("tasksLog/updateTaskDetails", payload).then(success, fail);
                };
                /************************* mySql delete ******************/
                self.mySqlDelete = function (event) {
                    var data = JSON.parse(self.selectedDetaildData().serviceData);
                    var payload = {
                        taskNumber: typeof data.taskNumber == 'undefined' ? self.getAllDataByNumber().taskNumber : data.taskNumber,
                        userName: typeof data.userName == 'undefined' ? self.getAllDataByNumber().taskNumber : data.userName
                    };
                    var success = function (data) {
                        if (typeof data.taskNumber == 'undefined') {
                            data = 'fail';
                        } else {
                            self.updateDetaild();
                            data = 'success';
                        }
                    };
                    var fail = function (data) {
                    };
                    //call mySql delete here
                    services.addGeneric("tasksLog/deleteTaskDetails", payload).then(success, fail);
                };
                /****************************************************************/

                ///////////////////***** update ***/////////

                self.updateDetaild =async function () {
                    var payload = {
                        "id": self.selectedDetaildData().id,
                        "taskNumber": self.selectedDetaildData().taskNumber,
                        "serviceStatus": "success"
                    };
                    console.log(payload);
                    var success = function (data) {
                        console.log('success');
                        console.log(data);
                        self.getTasksLog();
                    };
                    var fail = function (data) {
                        console.log('faield');
                        console.log(data);
                        self.getTasksLog();
                    };
                   await services.addGeneric("tasksLog/editTaskLogDetails", payload).then(success, fail);
                };

                self.getTasksLog =async function () {
                    self.tableDataArray([]);
                    var success = function (data) {
                        for (var i = 0; i < data.length; i++) {
                            var saasStatus = 'No data found yet';
                            var inboxStatus = 'No data found yet';
                            var sqlStatus = 'No data found yet';
                            var saasSuccess, inboxSuccess, sqlSuccess;
                            self.serviceActionType([]);
                            for (var j = 0; j < data[i].taskDetails.length; j++) {
                                if (data[i].taskDetails[j].serviceName == 'saasService') {
                                    if (data[i].taskDetails[j].serviceStatus == 'fail') {
                                        saasStatus = 'fail';
                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    } else {
                                        saasSuccess = 'success';
//                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    }
                                } else if (data[i].taskDetails[j].serviceName == 'inboxService') {
                                    if (data[i].taskDetails[j].serviceStatus == 'fail') {
                                        inboxStatus = 'fail';
                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    } else {
                                        inboxSuccess = 'success';
//                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    }
                                } else if (data[i].taskDetails[j].serviceName == 'mySqlService') {
                                    if (data[i].taskDetails[j].serviceStatus == 'fail') {
                                        sqlStatus = 'fail';
                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    } else {
                                        sqlSuccess = 'success';
//                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    }
                                }
                            }
                            self.tableDataArray.push({
                                taskNumber: data[i].taskNumber,
                                systemName: data[i].systemName,
                                nextApprover: data[i].nextApprover,
                                companyName: data[i].companyName,
                                type: data[i].type,
                                previousApprover: data[i].previousApprover,
                                display_status: data[i].display_status,
                                status: data[i].status,
                                personNumber: data[i].personNumber,
                                saasStatus: saasStatus == 'fail' ? 'fail' : saasSuccess == 'success' ? 'success' : saasStatus,
                                serviceDetails: data[i].taskDetails,
                                failureType: self.serviceActionType(),
                                inboxStatus: inboxStatus == 'fail' ? 'fail' : inboxSuccess == 'success' ? 'success' : inboxStatus,
                                mySqlStatus: sqlStatus == 'fail' ? 'fail' : sqlSuccess == 'success' ? 'success' : sqlStatus,
                                inboxActionType: data[i].inboxActionType
                            });
                            saasSuccess='';
                            inboxSuccess='';
                            sqlSuccess='';
                        }
                    };
                    var fail = function () {
                        console.log('failed');
                    };
                   await services.getGeneric("tasksLog/getAllTaskslog").then(success, fail);
                }
                ;
                self.searchInTasksLog = function () {
                    self.tableDataArray([]);
                    var success = function (data) {
                        console.log(data.taskNumber)
                        if (typeof data.taskNumber !== 'undefined') {
                            var saasStatus = 'No data found yet';
                            var inboxStatus = 'No data found yet';
                            var sqlStatus = 'No data found yet';
                            var saasSuccess, inboxSuccess, sqlSuccess;
                            self.serviceActionType([]);
                            for (var j = 0; j < data.taskDetails.length; j++) {
                                if (data.taskDetails[j].serviceName == 'saasService') {
                                    if (data.taskDetails[j].serviceStatus == 'fail') {
                                        saasStatus = 'fail';
                                        self.serviceActionType.push(' ' + data.taskDetails[j].serviceActionType + ' ');
                                    } else {
                                        saasSuccess = 'success';
//                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    }
                                } else if (data.taskDetails[j].serviceName == 'inboxService') {
                                    if (data.taskDetails[j].serviceStatus == 'fail') {
                                        inboxStatus = 'fail';
                                        self.serviceActionType.push(' ' + data.taskDetails[j].serviceActionType + ' ');
                                    } else {
                                        inboxSuccess = 'success';
//                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    }
                                } else if (data.taskDetails[j].serviceName == 'mySqlService') {
                                    if (data.taskDetails[j].serviceStatus == 'fail') {
                                        sqlStatus = 'fail';
                                        self.serviceActionType.push(' ' + data.taskDetails[j].serviceActionType + ' ');
                                    } else {
                                        sqlSuccess = 'success';
//                                        self.serviceActionType.push(' ' + data[i].taskDetails[j].serviceActionType + ' ');
                                    }
                                }
                            }
                            self.tableDataArray.push({
                                taskNumber: data.taskNumber,
                                taskId: data.taskId,
                                userName: data.userName,
                                taskStatus: data.taskStatus,
                                creationDate: self.formatDate(new Date(data.creationDate)),
                                createdBy: data.createdBy,
                                updatedDate: self.formatDate(new Date(data.updatedDate)),
                                updatedBy: data.updatedBy,
                                personNumber: data.personNumber,
                                saasStatus: saasStatus == 'fail' ? 'fail' : saasSuccess == 'success' ? 'success' : saasStatus,
                                serviceDetails: data.taskDetails,
                                failureType: self.serviceActionType(),
                                inboxStatus: inboxStatus == 'fail' ? 'fail' : inboxSuccess == 'success' ? 'success' : inboxStatus,
                                mySqlStatus: sqlStatus == 'fail' ? 'fail' : sqlSuccess == 'success' ? 'success' : sqlStatus,
                                inboxActionType: data.inboxActionType
                            });
                        }
                    };
                    var fail = function () {
                        console.log('failed');
                    };
                    services.getGeneric("tasksLog/getTaskLog/" + self.taskNumber()).then(success, fail);
                }
                ;

                self.resendAction =async function () {
                    var data = self.tableDataArray()[self.tableSelectedIndex()];
                    console.log(data);

                    for (var i = 0; i < data.serviceDetails.length; i++) {
                        if (data.serviceDetails[i].serviceStatus == 'fail') {
                            console.log(data.serviceDetails[i])
                            self.selectedDetaildData(data.serviceDetails[i]);
                            switch (data.serviceDetails[i].serviceActionType) {
                                case 'attachmentByUrl':
                                    console.log('attachmentByUrl');
                                   await self.addAttachmentByUrlForApprove();
                                    break;
                                case 'addAttachmentDesktop':
                                    console.log('addAttachmentDesktop');
                                   await self.AttachOkBTNForApp();
                                    break;
                                case 'rejectAction':
                                   await self.rejectRewaardRequst();
                                    console.log('rejectAction');
                                    break;
                                case 'addComment':
                                   await self.addComment();
                                    console.log('addComment');
                                    break;
                                case 'approveAction':
                                   await self.approveRequst();
                                    console.log('approveAction');
                                    break;
                                case 'suspendAction':
                                   await self.suspendYES();
                                    console.log('suspendAction');
                                    break;
                                case 'withdrawAction':
                                   await self.withdrawYES();
                                    console.log('withdrawAction');
                                    break;
                                case 'escalateAction':
                                   await self.escalateYES();
                                    console.log('escalateAction');
                                    break;
                                case 'mySqlDelete':
                                   await self.mySqlDelete();
                                    console.log('mySqlDelete');
                                    break;
                                case 'mySqlUpdate':
                                   await self.updateMySql();
                                    console.log('mySqlUpdate');
                                    break;
                                case 'mySqlInsert':
                                   await self.insertMySql();
                                    console.log('mySqlInsert');
                                    break;
                                case 'inboxSend':
                                   await self.inboxResend();
                                    console.log('inboxResend');
                                    break;
                                case 'inboxUpdate':
                                    self.updateInbox();
                                    console.log('inboxUpdate');
                                    break;
                                case 'inboxInsert':
                                    self.insertInbox();
                                    console.log('inboxInsert');
                                    break;
                                default:
                                // code block
                            }
                        }
                    }
                };

                self.formatDate = function (date) {
                    //var date = new Date()
                    var month = '' + (date.getMonth() + 1),
                            day = '' + date.getDate(),
                            year = date.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                };
                /**************** translation section **********************/
                initTranslations();
                function initTranslations() {
                    self.resendLbl('Resend');
                    self.searchLbl('Search');
                    self.taskNumberLbl('Task Number');
                    self.clearLbl('Clear');
                    self.yes('Yes');
                    self.no('No');
                }
                ;

                self.tableColumnArray([
                    {"headerText": "Task Number",
                        "field": "taskNumber",
                        "resizable": "enabled"},
                    {"headerText": "previous Approver",
                        "field": "previousApprover",
                        "resizable": "enabled"},
                    {"headerText": "next Approver",
                        "field": "nextApprover",
                        "resizable": "enabled"},
                    {"headerText": "company Name",
                        "field": "companyName",
                        "resizable": "enabled"},
                    {"headerText": "display Status",
                        "field": "display_status",
                        "resizable": "enabled"},
//                    {"headerText": "service Name",
////                        "field": "taskDetails.serviceName",
////                        "resizable": "enabled"},
                    {"headerText": "saas Status",
                        "field": "saasStatus",
                        "resizable": "enabled"},
                    {"headerText": "Sql Status",
                        "field": "mySqlStatus",
                        "resizable": "enabled"},
                    {"headerText": "Inbox Status",
                        "field": "inboxStatus",
                        "resizable": "enabled"},
                    {"headerText": "Failure Type",
                        "field": "failureType",
                        "resizable": "enabled"}
                ]);
                return true;
                


            }

            return new taskLogTrackerContentViewModel();
        });

