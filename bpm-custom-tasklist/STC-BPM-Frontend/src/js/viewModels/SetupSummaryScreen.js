/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * homeministryJob module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojarraydataprovider',
    'ojs/ojselectcombobox', 'ojs/ojtable',
    'ojs/ojdialog'], function (oj, ko, $, app, services, commonhelper, ArrayDataProvider) {

    function positionSummaryAdditionalDetails() {
        var self = this;
        self.columnArray = ko.observableArray();
        self.placeholder = ko.observable();
        self.dataSourceTB2 = ko.observable();
        var deptArray = [];
        self.data = ko.observableArray([]);
        self.selctedStruct = ko.observable();
        self.isRequired = ko.observable(true);
        self.oprationdisabled = ko.observable(true);
        self.selectedRowKey = ko.observable();
        self.selectedIndex = ko.observable();
        self.nextPage = ko.observable();
        self.backActionLbl = ko.observable();
        self.deptObservableArray = ko.observableArray([]);
        var Url;
        var positionObj;
        self.checkResultSearch = ko.observable();
    self.dataprovider = new oj.ArrayDataProvider(self.deptObservableArray, {idAttribute: 'id'});
        function clearContent() {

        }

        self.EitName = ko.observableArray([]);
  

        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            self.oprationdisabled(false);
            self.selectedRowKey(currentRow['rowKey']);
            if (currentRow !== null) {
                self.selectedIndex(currentRow['rowIndex']);
            }

        };

        self.addAdditionalDetails = function () {
            app.loading(true);
            positionObj = {}
            positionObj.type = "ADD";
            oj.Router.rootInstance.store(positionObj);
            oj.Router.rootInstance.go('SetupScreen');
        };
        self.updateAdditionalDetails = function () {

            if (self.deptObservableArray()[self.selectedIndex()] != null) {
                app.loading(true);
                self.deptObservableArray()[self.selectedIndex()].type = "EDIT";
                oj.Router.rootInstance.store(self.deptObservableArray()[self.selectedIndex()]);
                oj.Router.rootInstance.go('SetupScreen');
            }
            else
            {
                $.notify(self.ValidationMessage(), "error");
            }
        };
        self.deleteAdditionalDetails = function () {
            if (self.deptObservableArray()[self.selectedIndex()] != null) {
                document.querySelector("#yesNoDialog").open();
            } else
            {
                $.notify(self.ValidationMessage(), "error");
            }

        };
        self.validationModel = {
            id: ko.observable(),
            eit_code: ko.observable(),
            approval_order: ko.observable()


        };
    
        var getSummaryCBFn = function (data) {
            console.log(data);
            if (data.length !== 0) {
                self.deptObservableArray(data);
                console.log(self.deptObservableArray());

            }
        };
        var headers = {};
        var serviceName = "setup/getAllSetup";

        services.getGeneric(serviceName, headers).then(getSummaryCBFn, self.failCbFn)

        self.globalDelete = function () {


            var element = document.getElementById('table');
            var currentRow = element.currentRow;
            self.deptObservableArray()[currentRow['rowIndex']];
            var id = self.deptObservableArray()[currentRow['rowIndex']].id;
            var eit_code = self.deptObservableArray()[currentRow['rowIndex']].eitCode;
            var approval_order = self.deptObservableArray()[currentRow['rowIndex']].approvalOrder;
            var jsonData = ko.toJSON(self.validationModel);
            var approval_code = self.deptObservableArray()[currentRow['rowIndex']].approvalCode;
            var getdeleteCBF = function () {
            };

            services.searcheGeneric(commonhelper.approvalSetupUrl + "delapproval/" + self.deptObservableArray()[currentRow['rowIndex']].id + "/" + eit_code + "/" + approval_code + "/" + approval_order).then(getdeleteCBF, app.failCbFn);
        
        };
        self.backAction = function () {
            app.loading(true);
            oj.Router.rootInstance.go('searchPosition');
        };
        self.handleAttached = function () {

            app.loading(false);
          
            var arr = [{value: "XXX_RECRUITMENT_REQUEST",
                    label: self.EmployeeProbationaryPeriodEvaluationLBL()}];
            var totalArr = [];
            totalArr = arr.concat(app.globalEitNameReport());


            self.EitName(totalArr);

        };
        //------------------Option Change Section ----------------------------
        /*
         * Tyis Function Load Date From Web Servise Dependant On Selected Structure Name 
         * And Set The Next Page To Direct So When Crate The Your Pape Set The True Value 
         * in Var self.nextPage 
         * 
         */
        self.structureChangedHandler = function (event, data) {
            self.getall(self.selctedStruct());
        };
        //------------------------End Of Section ----------------------------

        //--------------------------History Section --------------------------------- 
        self.historyDetails = ko.observable();
        self.closeDialog = function () {
            $("#modalDialog1").ojDialog("close");
        };

        //--------------------------End Of Section ----------------------------------
        self.commitRecord = function (data, event) {
            self.globalDelete();
            self.getall(self.selctedStruct());
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
        };

        self.cancelButtonValid = function () {
            document.querySelector("#ValidationDialog").close();
        };
        self.backAction = function () {
            oj.Router.rootInstance.go('setup');
        };
        //---------------------Translate Section ----------------------------------------
        self.ok = ko.observable();
        self.addLbl = ko.observable();
        self.editLbl = ko.observable();
        self.CompanyNameLbl = ko.observable();
        self.backLbl = ko.observable();
        self.placeholder = ko.observable();
        self.approvalTypeLbl = ko.observable();
        self.specialCaseLbl = ko.observable();
        self.AdminUserLbl = ko.observable();
        self.SetupScreenLbl = ko.observable();
        self.AdminPasswordLbl = ko.observable();
        self.HostNameLbl = ko.observable();
        self.yes = ko.observable();
        self.no = ko.observable();
        self.confirmMessage = ko.observable();
        self.oprationMessage = ko.observable();
        self.ValidationMessage = ko.observable();
        self.EmployeeProbationaryPeriodEvaluationLBL = ko.observable();
        var getTranslation = oj.Translations.getTranslatedString;
        self.refreshView = ko.computed(function () {
            if (app.refreshViewForLanguage()) {
                initTranslations();
            }
        });

        function initTranslations() {
            self.CompanyNameLbl(getTranslation("labels.CompanyName"));
            self.backLbl(getTranslation("others.back"));
            self.ok(getTranslation("others.ok"));
            self.addLbl(getTranslation("others.add"));
            self.editLbl(getTranslation("others.edit"));
            self.approvalTypeLbl(getTranslation("approvalScreen.approvalType"));
            self.specialCaseLbl(getTranslation("approvalScreen.specialCase"));
            self.SetupScreenLbl(getTranslation("labels.SetupScreen"));
            self.AdminUserLbl(getTranslation("labels.AdminUser"));
            self.AdminPasswordLbl(getTranslation("labels.AdminPassword"));
            self.HostNameLbl(getTranslation("labels.HostName"));
            self.confirmMessage(getTranslation("approvalScreen.confirmMessage"));
            self.oprationMessage(getTranslation("approvalScreen.oprationMessage"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.placeholder(getTranslation("labels.placeHolder"));
            self.ValidationMessage(getTranslation("labels.ValidationMessage"));
            self.backActionLbl(getTranslation("approvalScreen.backAction"));
            self.columnArray([
                {
                    "headerText": self.CompanyNameLbl(), "field": "companyName"
                },
                {
                    "headerText": self.AdminUserLbl(), "field": "adminUsername"
                },
                {
                    "headerText": self.AdminPasswordLbl(), "field": "adminPassword"
                },
                {
                    "headerText": self.HostNameLbl(), "field": "hostName"
                }
            ]);
        }
        initTranslations();
    }
    return positionSummaryAdditionalDetails;
});
