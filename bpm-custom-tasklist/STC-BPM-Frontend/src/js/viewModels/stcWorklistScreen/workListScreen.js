/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * workListScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'util/commonhelper',
    'config/services', 'config/buildScreen', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
    'ojs/ojknockout-validation', 'ojs/ojlabel', 'ojs/ojtable'],
        function (oj, ko, $, app, commonUtil, services, buildScreen, ArrayDataProvider) {
            /**
             * The view model for the main content view template
             */
            function workListScreenContentViewModel() {
                var self = this;
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                var getTranslation = oj.Translations.getTranslatedString;
                var positionObj;
                self.filterArr = ko.observableArray([]);
                self.placeholder = ko.observable("Please select value");
                //Observables
                self.lang = "";
                self.employeeName=ko.observable();
                self.filter = ko.observable();
                self.columnArray = ko.observableArray();
                self.selectedRowKey = ko.observable();
                self.messages = ko.observableArray([]);
                self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
                self.position =
                        {
                            "my": {"vertical": "top", "horizontal": "end"},
                            "at": {"vertical": "top", "horizontal": "end"},
                            "of": "window"
                        };
                self.closeMessageHandler = function (event) {
                    self.messages.remove(event.detail.message);
                };
                self.unknownErrMsg = ko.observable();
                self.selectedIndex = ko.observable();
                self.editLbl = ko.observable();

                self.arr = ko.observableArray([]);

                self.code = ko.observable('My');
                self.filterArr.push({
                    "value": "My",
                    "label": "Me"
                },
                        {
                            "value": "Group",
                            "label": "My Group"
                        },
                        {
                            "value": "My+Group",
                            "label": "Me & My Group"
                        },
                        {
                            "value": "My+Group+All",
                            "label": "Me & My Group All"
                        },
                        {
                            "value": "Previous",
                            "label": "Me (Previously)"
                        });

                self.changeValue = function (e) {
                    self.summaryObservableArray([]);
                    self.code(e.detail.value);
                    self.getWorkListByFilter();
                    console.log(e.detail.value);//['detail'].value
                };

                self.Number = ko.observable();
                self.searchBTN = function () {
                    //console.log('pressed');

                    if ((self.Number() != null) && (self.Number() != "undefined") && (self.Number().length != 0)) {
                        self.summaryObservableArray([]);
                        searchResultArray = self.arr().filter(item => item.taskNumber == self.Number());
                        console.log(searchResultArray);
                        self.summaryObservableArray(searchResultArray);
                        console.log("done");
                    } else {
                        console.log(self.arr());
                        self.summaryObservableArray(self.arr());
                    }

                };

                self.search = ko.observable();
                self.clear = ko.observable();
                self.tempSummaryObservableArray = [];
                self.summaryObservableArray = ko.observableArray([]);
                self.oprationdisabled = ko.observable(true);
                self.dataSource = new ko.observable(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.summaryObservableArray, {idAttribute: 'taskNumber'})));




//                self.btnShowTask = function () {
//
//                    oj.Router.rootInstance.go("worklistTaskDetails");
//
//                };
                self.btnShowTask = function () {
                    console.log(self.summaryObservableArray()[IndexORow]);
                    if (self.summaryObservableArray()[IndexORow] != null) {
                        console.log(self.summaryObservableArray()[IndexORow]);
                        self.summaryObservableArray()[self.selectedIndex()].type = "SHOW";
                        app.loading(true);
                        // you can open by new popup
//                        window.open("http://localhost:8000/?root=dynamicTaskDetails&taskNumber="+currentRow.rowKey+"&user="+self.employeeName(),"newPopup","width=700,height=500");
                        // open it in current tab
                        window.open("http://localhost:8000/?root=dynamicTaskDetails&taskNumber="+currentRow.rowKey+"&user="+self.employeeName(),"_self");
//localhost:8000/?root=dynamicTaskDetail
//                    window.open("http://10.21.196.229:8181/STC-BPM-Frontend/web/?root=dynamicTaskDetails&taskNumber="+currentRow.rowKey+"&user="+self.employeeName(),"_self");
                    
                    } else {
                        self.messages.push({
                                severity: 'error',
                                summary: "Can't find data",
                                autoTimeout: 5000
                            });
                    }
                };
                self.getWorkList = function () {
                    var getAllWorkListsCBF = function (data) {
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            var obj = {};
                            obj.assignedDate = data[i].assignedDate;
                            obj.updatedByType = data[i].updatedByType;
                            obj.creator = data[i].creator;
                            obj.createdDate = data[i].createdDate;
                            obj.state = data[i].state;
                            obj.taskNumber = data[i].taskNumber;
                            obj.updatedDate = data[i].updatedDate;
                            obj.priority = data[i].priority;
                            obj.title = data[i].title;
                            obj.updatedById = data[i].updatedById;
                            obj.taskId = data[i].taskId;
                            self.summaryObservableArray.push(obj);
                        }

                        self.arr(self.summaryObservableArray());
                        console.log(self.summaryObservableArray());
                    };
                    var failCbFn = function (data) {
                        console.log('ddddddd')
                        self.messages.push({
                                severity: 'error',
                                summary: data,
                                autoTimeout: 5000
                            });
                    };
                    var payload = {
                        "jwtToken": "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIiwiY3R5IjoiSldUIn0..SAyI1a25z5ldc3NHKZ_uug.72tofRwS0ujMwgcQDu8pJum8EwHFivAmG8Wdp0E33ysUQGwrJL7xYJ5E1rzIcuCCkbONI6cBhcNr_qMMUjVIQTBPpG-kGJ4uvUL4nbw7_w9b5mURfyIahUo7eNEgU-mIiM3UczJBqUXHAbTyn-LgoYZPcgIQpOuGuJl_sy9KLGcw5hhqbpt7uYhLfw2YYyYnq-b6zooCELVoKAy1EjYz12jDIqlLtw3-4Ffk-xKrm5dRqRMw7uq7vp8O59S2P9SA.UdQM9_7rBbSu_a0uBr6MqQ",
                        "language": "en",
                        "username": "shrkhan.c@stc.com.sa",
                        //added here
                        "AssignedFilter": "My",
                        "employeeName":self.employeeName()
                    };
                    services.addGeneric("bpmWorklist/asFilter/", payload).then(getAllWorkListsCBF, failCbFn);
                };
                
                var taskId ;
                var IndexORow;
                var currentRow;
                self.tableSelectionListener = function (event) {
                    var data = event.detail;
                    IndexORow = event.detail.currentRow.rowIndex;
                    console.log(IndexORow);
                    taskId = data.currentRow.rowKey;
                    console.log(taskId);
                    console.log(self.summaryObservableArray()[IndexORow]);
                    currentRow = data.currentRow;
                    self.selectedRowKey(currentRow['rowKey']);
                    self.selectedIndex(currentRow['rowIndex']);
                    self.oprationdisabled(false);

                };
                if (app.getLocale() == 'ar') {
                    self.lang = "AR";
                } else {
                    self.lang = "US";
                }

                self.handleAttached = function (info) {
                    var url = new URL(window.location.href);
                    console.log(url);
                    self.employeeName(url.searchParams.get("user"));
                    app.loading(false);
                    self.getWorkListByFilter();
                    console.log(self.jwtToken());
                };
                self.handleDetached = function (info) {
                    

                };

                self.getWorkListByFilter = function () {
                    var filter = self.code();
                    var getAllWorkListsByFilterCBF = function (data) {
                        console.log(payload);
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            var obj = {};
                            obj.title = data[i].title;
                            obj.assignedDate = data[i].assignedDate;
                            obj.createdDate = data[i].createdDate;
                            obj.creator = data[i].creator;
                            obj.displayName = data[i].displayName;
                            obj.priority = data[i].priority;
                            obj.state = data[i].state;
                            obj.taskId = data[i].taskId;
                            obj.taskNumber = data[i].taskNumber;
                            self.summaryObservableArray.push(obj);
                        }

                        self.arr(self.summaryObservableArray());
                        console.log(self.summaryObservableArray());
                    };
                    var failCbFn = function (data) {
                        console.log(data.responseText);
                        self.messages.push({
                                severity: 'error',
                                summary: data.responseText,
                                autoTimeout: 0
                            });
                    };
                    var payload = {
                        "jwtToken": "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIiwiY3R5IjoiSldUIn0..SAyI1a25z5ldc3NHKZ_uug.72tofRwS0ujMwgcQDu8pJum8EwHFivAmG8Wdp0E33ysUQGwrJL7xYJ5E1rzIcuCCkbONI6cBhcNr_qMMUjVIQTBPpG-kGJ4uvUL4nbw7_w9b5mURfyIahUo7eNEgU-mIiM3UczJBqUXHAbTyn-LgoYZPcgIQpOuGuJl_sy9KLGcw5hhqbpt7uYhLfw2YYyYnq-b6zooCELVoKAy1EjYz12jDIqlLtw3-4Ffk-xKrm5dRqRMw7uq7vp8O59S2P9SA.UdQM9_7rBbSu_a0uBr6MqQ",
                        "language": "en",
                        "username": "shrkhan.c@stc.com.sa",
                        //added here
                        "AssignedFilter": filter,
                        "employeeName":self.employeeName()
//                        All, My, Group, My+Group, My+Group+All, Reportees, Creator, Owner, Previous, Admin
                    };
                    console.log(payload);
//                    services.addGeneric("bpmWorklist/", payload).then(getAllWorkListsCBF, failCbFn);
                    services.addGeneric("bpmWorklist/asFilter/", payload).then(getAllWorkListsByFilterCBF, failCbFn);
                };





                self.refreshView = ko.computed(function () {
                    if (app.refreshViewForLanguage()) {
                        initTranslations();
                    }

                });

                function initTranslations() {
                    //New

                    self.editLbl(getTranslation("others.edit"));
                    self.unknownErrMsg(getTranslation("labels.ValidationMessage"));

                    self.columnArray([
                        {
                            "headerText": "Title", "field": "title"
                        },
                        {
                            "headerText": "Number", "field": "taskNumber"
                        },
                        {
                            "headerText": "Creator", "field": "creator"
                        },
                        {
                            "headerText": "Assigned", "field": "assignedDate"
                        },
                        {
                            "headerText": "Priority", "field": "priority"
                        }
                    ]);

                }
                
                var userName = "shrkhan.c@stc.com.sa";
                var jwtToken = "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIiwiY3R5IjoiSldUIn0..SAyI1a25z5ldc3NHKZ_uug.72tofRwS0ujMwgcQDu8pJum8EwHFivAmG8Wdp0E33ysUQGwrJL7xYJ5E1rzIcuCCkbONI6cBhcNr_qMMUjVIQTBPpG-kGJ4uvUL4nbw7_w9b5mURfyIahUo7eNEgU-mIiM3UczJBqUXHAbTyn-LgoYZPcgIQpOuGuJl_sy9KLGcw5hhqbpt7uYhLfw2YYyYnq-b6zooCELVoKAy1EjYz12jDIqlLtw3-4Ffk-xKrm5dRqRMw7uq7vp8O59S2P9SA.UdQM9_7rBbSu_a0uBr6MqQ";
                self.jwtToken = ko.observable(jwtToken);
                console.log(self.jwtToken());
                
                
                
                initTranslations();

            }

            return workListScreenContentViewModel;
        });