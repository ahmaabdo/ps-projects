/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * dynamicTaskDetails module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojarraydataprovider',
    'ojs/ojbootstrap', 'appController', 'util/commonhelper', 'config/services', 'ojs/ojknockout',
    'ojs/ojcollapsible', 'ojs/ojaccordion', 'ojs/ojradioset', 'ojs/ojlabel', 'ojs/ojdialog', 'ojs/ojvalidationgroup',
    'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojcheckboxset', 'promise', 'ojs/ojtable', 'ojs/ojbutton', 'ojs/ojtoolbar', 'ojs/ojmenu',
    'ojs/ojpagingtabledatasource', 'ojs/ojdefer', 'ojs/ojdefer', 'ojs/ojpopup',
    'ojs/ojdatasource-common', 'ojs/ojradioset', 'ojs/ojtable', 'ojs/ojfilepicker', 'ojs/ojcheckboxset'
], function (oj, ko, $, ArrayDataProvider, Bootstrap, app, commonUtil, services) {
    /**
     * The view model for the main content view template
     */
    function dynamicTaskDetailsContentViewModel() {
        var self = this;
        self.myPageVisibilty = ko.observable(true);
        var getTranslation = oj.Translations.getTranslatedString;
        self.isShown = ko.observable(false);
        self.approve = ko.observable("Approve");
        self.reject = ko.observable("Reject");
        self.dismiss = ko.observable("Dismiss");
        self.resume = ko.observable("Resume");
        self.actionBrows = ko.observableArray([]);
        self.codeOP = new ArrayDataProvider(self.actionBrows, {keyAttributes: 'value'});
        self.actionData = ko.observableArray([]);
        self.confirmMessage = ko.observable();
        self.approveMessage = ko.observable();
        self.unknownErrMsg = ko.observable();
        self.groupValid = ko.observable();
        self.groupValidForCommentInApprove = ko.observable();
        self.addCommentsTxtForApprove = ko.observable();
        self.AttachTypeRadioSetForApprove = ko.observable();
        self.secondGroupValidForApprove = ko.observable();
        self.attachNameUrlPlaceholder = ko.observable('Enter the attachment name');
        self.attachUrlUrlPlaceholder = ko.observable('Enter the attachment URL');
        self.removeBtnLbl = ko.observable("Remove");
        self.selectedItemsAttachmentForApp = ko.observableArray([]);
        self.koArray = ko.observableArray([]);
        self.AttachmentData = ko.observableArray();
        self.AttachmentDataprovider = new ArrayDataProvider(self.AttachmentData);
        self.secondKoArray = ko.observableArray();
        self.encodedAttachment = ko.observable();
        self.messagess = ko.observableArray();
        self.attachmentName = ko.observable();
        self.contentType = ko.observable();
        self.attachNameUrlForApprove = ko.observable();
        self.attachUrlUrlForApprove = ko.observable();
        self.urlArray = ko.observableArray();
        self.dataProviderAttachmentForApp = new oj.ArrayDataProvider(self.koArray, {keyAttributes: 'id'});
        self.yes = ko.observable();
        self.no = ko.observable();
        self.disableSubmit = ko.observable(false);
        self.CommentData = ko.observableArray([]);
        self.commentDataTwo = ko.observableArray();
        self.rejectMessage = ko.observable();
        self.addCommentsTxt = ko.observable();
        self.addCommentsTxtForApprove = ko.observable();
        self.historyData = ko.observableArray([]);
        self.historyDataprovider = new oj.ArrayDataProvider(self.historyData, {keyAttributes: 'userId'});
        self.historyTableSelectionListener = function (event) {};
        self.historyTableColumnArray = ko.observableArray();
        self.patternData = ko.observable();
        self.histroyNewStructure = ko.observable();
        self.userNameHistory = ko.observable();
        self.approveBtnVisiblty = ko.observable(true);
        self.rejectBtnVisiblty = ko.observable(true);
        self.actionBtnVisiblty = ko.observable(true);
        self.dismissBtnVisiblty = ko.observable(false);
        self.resumeBtnVisiblty = ko.observable(false);
        self.getAllDataByNumber = ko.observableArray();
        self.reassignCheckbox = ko.observable();
        self.value = ko.observable('user');
        self.reassignSelectValue = ko.observable('user');
        self.firstName = ko.observable('');
        self.lastName = ko.observable('');
        self.email = ko.observable('');
        self.id = ko.observable('');
        self.myTextArea = ko.observableArray();
        self.reassignTaskRadioSet = ko.observable('ReassignValue');
        self.reassignCheckbox = ko.observable();
        self.groupID = ko.observable();
        self.radioSetCurrentSelected = ko.observable();
        self.reassignData = ko.observableArray([]);
        self.RequestreassignData = ko.observableArray([]);
        self.errorMessage = ko.observable();
        self.historyStatus = ko.observable();
        var encodedAtt;
        var actionDataVar;
        var actionDataArray;
        self.delegatePayload = ko.observableArray();
        self.employeeName = ko.observable();
        self.isAction = ko.observable(false);
        self.taskExistStatus = ko.observable(true);
        // define messages
        self.messages = ko.observableArray();
        self.messagesDataProvider = new oj.ArrayDataProvider(self.messages);
        self.position =
                {
                    "my": {"vertical": "top", "horizontal": "end"},
                    "at": {"vertical": "top", "horizontal": "end"},
                    "of": "window"
                };
        self.closeMessageHandler = function (event) {
            self.messages.remove(event.detail.message);
        };
        self.dialogErrorMessage = ko.observable();
        self.showHistory = ko.observable(true);
        //////////// translations ///////////
        function initTranslations() {
            self.approve(getTranslation("others.approve"));
            self.reject(getTranslation("others.reject"));
            self.unknownErrMsg(getTranslation("labels.ValidationMessage"));
            self.rejectMessage(getTranslation("others.rejectMSG"));
            self.approveMessage(getTranslation("others.approvalMSG"));
            self.yes(getTranslation("others.yes"));
            self.no(getTranslation("others.no"));
            self.approve(getTranslation("others.approve"));
            self.reject(getTranslation("others.reject"));
        }
        initTranslations();
        self.getAllActionsForTask = function () {
            self.getComment();
            self.getAttachment();
            self.getActionDetails();
            self.dynamicPayloadData();
            self.getHistoryData();
            self.setPageTitle();
        };

        self.getWorkListByNumber = async function () {
            console.log(taskNumber);
            var payload = {
                "jwtToken": "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIiwiY3R5IjoiSldUIn0..SAyI1a25z5ldc3NHKZ_uug.72tofRwS0ujMwgcQDu8pJum8EwHFivAmG8Wdp0E33ysUQGwrJL7xYJ5E1rzIcuCCkbONI6cBhcNr_qMMUjVIQTBPpG-kGJ4uvUL4nbw7_w9b5mURfyIahUo7eNEgU-mIiM3UczJBqUXHAbTyn-LgoYZPcgIQpOuGuJl_sy9KLGcw5hhqbpt7uYhLfw2YYyYnq-b6zooCELVoKAy1EjYz12jDIqlLtw3-4Ffk-xKrm5dRqRMw7uq7vp8O59S2P9SA.UdQM9_7rBbSu_a0uBr6MqQ",
                "language": "en",
                "username": "shrkhan.c@stc.com.sa",
                "taskNumber": taskNumber,
                "employeeName": self.employeeName()
            };
            var success = function (myRowData) {
//                debugger
                console.log(myRowData);
                if (typeof myRowData.errorInTask != 'undefined') {
                    self.dialogErrorMessage(myRowData.errorInTask);
                    document.getElementById("errorDialog").open();
                    self.taskExistStatus(false);
                    app.loading(false);
                } else {
                    self.getAllDataByNumber(myRowData);
                    if (self.getAllDataByNumber().workflowPattern == 'FYI' && (self.getAllDataByNumber().state == 'FYI' || self.getAllDataByNumber().state == 'ASSIGNED')) {
                        self.approveBtnVisiblty(false);
                        self.rejectBtnVisiblty(false);
                        self.actionBtnVisiblty(false);
                        self.dismissBtnVisiblty(true);
                        self.getAllActionsForTask();
                    } else if (self.getAllDataByNumber().state == 'SUSPENDED') {
                        self.approveBtnVisiblty(false);
                        self.rejectBtnVisiblty(false);
                        self.actionBtnVisiblty(false);
                        self.dismissBtnVisiblty(false);
                        self.resumeBtnVisiblty(true);
                        self.getAllActionsForTask();
                    } else if (myRowData.state == "ASSIGNED") {
                        if (myRowData.assignee != self.employeeName()) {
                            self.dialogErrorMessage("This Task Can not be found");
                            document.getElementById("errorDialog").open();
                            self.taskExistStatus(true);
                            app.loading(false);
                        } else {
                            self.getAllActionsForTask();
                        }
                    } else if (self.getAllDataByNumber().state == 'WITHDRAWN' || self.getAllDataByNumber().state == 'COMPLETED') {
                        self.approveBtnVisiblty(false);
                        self.rejectBtnVisiblty(false);
                        self.actionBtnVisiblty(false);
                        self.dismissBtnVisiblty(false);
                        self.resumeBtnVisiblty(false);
                        self.getAllActionsForTask();
                    } else {
                        self.approveBtnVisiblty(true);
                        self.rejectBtnVisiblty(true);
                        self.actionBtnVisiblty(true);
                        self.dismissBtnVisiblty(false);
                        self.resumeBtnVisiblty(false);
                        self.getAllActionsForTask();
                    }

                }
            };
            var fail = function () {
                console.log('failed');
            };
            await services.addGeneric("bpmWorklist/taskNumber", payload).then(success, fail);
        };
        var taskNumber;
        self.handleAttached = function (info) {
            app.loading(true);
            var url = new URL(window.location.href);
            taskNumber = url.searchParams.get("taskNumber");
            self.employeeName(url.searchParams.get("user"));
            console.log('taskNumber=' + taskNumber + '::' + 'user=' + self.employeeName());
            self.getWorkListByNumber();
        };
        //////// add comment action ////////////
        self.addCommentBTN = function () {
            app.loading(true);
            var myComment = self.addCommentsTxtForApprove();
            self.commentDataTwo.push({commentScope: '',
                commentStr: self.addCommentsTxtForApprove(),
                updatedBy: '',
                updateddDate: '',
                userId: '',
                taskId: self.getAllDataByNumber().taskId});
            var commentJsonString = JSON.stringify(self.commentDataTwo());
            var success001 = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        commentResponseForApprove = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        commentResponseForApprove = 'success';
                        app.loading(false);
                        self.messages.push({
                            severity: 'confirmation',
                            summary: 'comment uploaded successfully',
                            autoTimeout: 0
                        });
                        document.getElementById("myHistory").innerHTML = "";
                        self.getHistoryData();
                        self.getActionDetails();
                        self.isAction(false);
                    }
                    self.getComment();
                    // store log details in log table mySql
                    self.getTaskLogByNumber('saasService', commentResponseForApprove, 'addComment', commentJsonString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail001 = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        commentResponseForApprove = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        commentResponseForApprove = 'success';
                        self.isAction(false);
                    }
                    self.getComment();
                    // store log details in log table mySql
                    self.getTaskLogByNumber('saasService', commentResponseForApprove, 'addComment', commentJsonString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            if (myComment != null) {
                services.addGeneric("bpmWorklist/addComment/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, commentJsonString).then(success001, fail001);
                document.querySelector("#addCommentModalDialog").close();
            } else {
                self.messages.push({
                    severity: 'error',
                    summary: 'Enter comment first',
                    autoTimeout: 0
                });
            }
        }
        ///// approve button /////
        self.submitButton = function () {
            document.querySelector("#yesNoDialog").open();
        };
        ///// reject button ////
        self.rejectButton = function () {
            document.querySelector("#rejectDialog").open();
        };
        ///// add attachment button in approve popup ////
        self.addAttchamentForApprove = function () {
            document.querySelector("#addAttachModalDialogForApprove").open();
            self.AttachTypeRadioSetForApprove('urlForApprove');
        };
        ///// close attachment popup inside approve popup //////
        self.cancelBehaviorForApprove = function () {
            self.attachNameUrlForApprove('');
            self.attachUrlUrlForApprove('');
            document.getElementById("attachNameUrlForApprove").reset();
            document.getElementById("attachUrlUrlForApprove").reset();
        };
        ///// ok button in attachment by url for approve popup ////
        self.UrlAttachOkBTNForApprove = function () {
            console.log(self.attachNameUrlForApprove());
            console.log(self.attachUrlUrlForApprove());
            if (isEmpty(self.attachNameUrlForApprove()) && isEmpty(self.attachUrlUrlForApprove())) {
                document.querySelector("#errorMessageinAttachment").open();

            } else if (isEmpty(self.attachNameUrlForApprove()) && !isEmpty(self.attachUrlUrlForApprove())) {
                document.getElementById("attachNameUrlForApprove").validate().then(function () {
                    var tracker = document.getElementById("secondTrackerForApprove");
                });
            } else if (!isEmpty(self.attachNameUrlForApprove()) && isEmpty(self.attachUrlUrlForApprove())) {
                document.getElementById("attachUrlUrlForApprove").validate().then(function () {
                    var tracker = document.getElementById("secondTrackerForApprove");
                });
            } else if (!isEmpty(self.attachNameUrlForApprove()) && !isEmpty(self.attachUrlUrlForApprove())) {
                self.addAttachmentByUrlForApprove();
                document.querySelector("#addAttachModalDialogForApprove").close();
                self.attachNameUrlForApprove('');
                self.attachUrlUrlForApprove('');
            }
        };
        ///// cancel button for attachment by url in approve popup ////
        self.UrlAttachCancelBTNForApprove = function () {
            self.attachNameUrlForApprove('');
            self.attachUrlUrlForApprove('');
            document.getElementById("attachNameUrlForApprove").reset();
            document.getElementById("attachUrlUrlForApprove").reset();
            document.querySelector("#addAttachModalDialogForApprove").close();
        };
        //////  selection lestiner in attachment popup for approve popup /////
        var files001;
        self.attachmentId = 0;
        var obj = {};
        self.attachmentSelectListenerForApp = function (event) {
            files001 = event.detail.files;
            console.log(files001);
            if (files001.length > 0) {
                for (var i = 0; i < files001.length; i++) {
                    obj.name = files001[i].name;
                    obj.id = i + 1;
                    getBase64(files001[i]).then(function (data) {
                        obj.data = data;
                        self.attachmentId = self.attachmentId + 1;
                        obj.id = (self.attachmentId);
                        self.koArray([]);
                        self.koArray.push(obj);
                        self.encodedAttachment(obj.data);
                        self.attachmentName(obj.name);
                        self.contentType(obj.name.split(".")[1]);
                    });
                }

            }
        };
        ///// remove button in attachment by desktop in approve popup /////
        self.removeSelectedAttachmentForApp = function (event) {
            $.each(self.selectedItemsAttachmentForApp(), function (index, value)
            {
                self.koArray.remove(function (item) {
                    return (item.id == value);
                });
            });
        };
        self.attachNameUrlChangeForApprove = function () {
            self.attachNameUrlForApprove('');
        };
        self.attachUrlUrlChangeForApprove = function () {
            self.attachUrlUrlForApprove('');
        };
        ////// ok button for attachment by desktop in approve popup /////
        var attachByDesktopResponse;
        self.AttachOkBTNForApp = function (event) {
            if (self.koArray() <= 0) {
                self.messagess.push({severity: 'error', summary: 'You Must Choose a File to Upload', detail: ""});
            } else {
                console.log(self.AttachmentData());
                encodedAtt = self.encodedAttachment();
                encodedAtt = encodedAtt.substring(encodedAtt.indexOf("64") + 1);
                encodedAtt = encodedAtt.substr(2);
                self.secondKoArray([]);
                self.secondKoArray.push({
                    attachmentScope: 'TASK',
                    attachmentSize: 0,
                    encodedAttachment: encodedAtt,
                    href: '',
                    rel: 'self',
                    title: self.attachmentName(),
                    updatedBy: '',
                    updatedDate: '',
                    uri: {
                        href: '',
                        length: 0,
                        rel: 'stream'
                    },
                    userId: self.getAllDataByNumber().assignee,
                    taskId: self.getAllDataByNumber().taskId
                });
                var attachJsonString = JSON.stringify(self.secondKoArray());
                console.log(attachJsonString);
                var success002 = function (data) {
                    data = JSON.parse(data);
                    if (data.taskExist == 'Yes') {
                        if (data.status != 'Success') {
                            attachByDesktopResponse = 'fail';
                            self.dialogErrorMessage(data.error);
                            document.getElementById("errorDialog").open();
                        } else {
                            attachByDesktopResponse = 'success';
                            self.messages.push({
                                severity: 'confirmation',
                                summary: 'Attachment uploaded successfully',
                                autoTimeout: 0
                            });
                            document.getElementById("myHistory").innerHTML = "";
                            self.getHistoryData();
                            self.getActionDetails();
                            self.isAction(false);
                        }
                        self.AttachmentData([]);
                        self.koArray([]);
                        self.getAttachment();
                        //save log details in mySql
                        self.getTaskLogByNumber('saasService', attachByDesktopResponse, 'addAttachmentDesktop', attachJsonString);
                    } else {
                        app.loading(false);
                        self.taskExistStatus(false);
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    }
                };
                var fail002 = function (data) {
                    data = JSON.parse(data);
                    if (data.taskExist == 'Yes') {
                        if (data.status != 'Success') {
                            attachByDesktopResponse = 'fail';
                            self.dialogErrorMessage(data.error);
                            document.getElementById("errorDialog").open();
                        } else {
                            attachByDesktopResponse = 'success';
                            self.isAction(fasle);
                        }
                        self.AttachmentData([]);
                        document.querySelector("#addAttachModalDialogForApprove").close();
                        self.koArray([]);
                        self.getAttachment();
                        //save log details in mySql
                        self.getTaskLogByNumber('saasService', attachByDesktopResponse, 'addAttachmentDesktop', attachJsonString);
                    } else {
                        app.loading(false);
                        self.taskExistStatus(false);
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    }
                };
                console.log(attachJsonString);
                services.addGeneric("bpmWorklist/addAttachment/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, attachJsonString).then(success002, fail002);
                document.querySelector("#addAttachModalDialogForApprove").close();
            }
            self.koArray([]);
        };
        self.getAttachment = function () {
            var payload = {};
            var success = function (AttachmentData) {
                if (AttachmentData == 'noTaskFound') {
                    self.messages.push({
                        severity: 'error',
                        summary: 'This task is no longer exist',
                        autoTimeout: 0
                    });
                } else {
                    self.AttachmentData(AttachmentData);
                }
            };
            var fail = function () {
                console.log('failed to get attach');
            };
            services.getGeneric("bpmWorklist/getAttachments/" + taskNumber + "/" + self.employeeName(), payload).then(success, fail);
        };
        function getBase64(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => resolve(reader.result);
                reader.onerror = error => reject(error);
            });
        }
        ;
        var attachmentUrlRespone;
        self.addAttachmentByUrlForApprove = function () {
            var newUrl;
            var attachUrlName;
            attachUrlName = self.attachNameUrlForApprove();
            newUrl = self.attachUrlUrlForApprove();
            self.urlArray.push({
                attachmentScope: 'TASK',
                attachmentSize: 0,
                encodedAttachment: '',
                href: newUrl,
                rel: 'self',
                title: self.attachNameUrlForApprove(),
                updatedBy: '',
                updatedDate: '',
                uri: {
                    href: newUrl,
                    length: 0,
                    rel: 'rel'
                },
                userId: self.getAllDataByNumber().assignee,
                taskId: self.getAllDataByNumber().taskId
            });
            console.log(self.urlArray());
            var attachmentUrlJsonString = JSON.stringify(self.urlArray());
            console.log(attachmentUrlJsonString);
            var success = function (data) {
                data = JSON.parse(data);
                self.urlArray([]);
                self.getAttachment();
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        attachmentUrlRespone = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        attachmentUrlRespone = 'success';
                        self.messages.push({
                            severity: 'confirmation',
                            summary: 'Attachment uploaded successfully',
                            autoTimeout: 0
                        });
                        document.getElementById("myHistory").innerHTML = "";
                        self.getHistoryData();
                        self.getActionDetails();
                        self.isAction(false);
                    }
                    console.log(attachmentUrlRespone);
                    console.log(self.attachNameUrlForApprove());
                    self.getTaskLogByNumber('saasService', attachmentUrlRespone, 'attachmentByUrl', attachmentUrlJsonString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        attachmentUrlRespone = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        attachmentUrlRespone = 'success';
                        self.isAction(false);
                    }
                    console.log(attachmentUrlRespone);
                    console.log(self.attachNameUrlForApprove());
                    self.getTaskLogByNumber('saasService', attachmentUrlRespone, 'attachmentByUrl', attachmentUrlJsonString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            services.addGeneric("bpmWorklist/addAttachmentURL/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, attachmentUrlJsonString).then(success, fail);
            document.getElementById("addAttachModalDialogForApprove").close();
        };
        /// cancel button 
        self.AttachCancelBTNForApp = function () {
            self.koArray([]);
            document.querySelector("#addAttachModalDialogForApprove").close();
        };
        function isEmpty(val) {
            return (val === undefined || val == null || val.length <= 0) ? true : false;
        }
        ;
        var commentResponseForApprove;
        var approveResponse;
        self.approveRequst = function (data, event) {
            var myComment;
            myComment = self.addCommentsTxtForApprove();
            console.log(self.addCommentsTxtForApprove());
            self.commentDataTwo.push({commentScope: '',
                commentStr: self.addCommentsTxtForApprove(),
                updatedBy: '',
                updateddDate: '',
                userId: '',
                taskId: self.getAllDataByNumber().taskId});
            var commentJsonString = JSON.stringify(self.commentDataTwo());
            console.log(commentJsonString);
            var success001 = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        commentResponseForApprove = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        commentResponseForApprove = 'success';
                        self.isAction(false);
                    }
                    self.getComment();
                    self.getTaskLogByNumber('saasService', commentResponseForApprove, 'addComment', commentJsonString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail001 = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        commentResponseForApprove = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        commentResponseForApprove = 'success';
                        self.isAction(false);
                    }
                    self.getComment();
                    self.getTaskLogByNumber('saasService', commentResponseForApprove, 'addComment', commentJsonString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            if (myComment != null)
                services.addGeneric("bpmWorklist/addComment/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, commentJsonString).then(success001, fail001);
//
            document.querySelector("#yesNoDialog").close();
            var payload = {
                "taskId": self.getAllDataByNumber().taskId,
                "outcome": "APPROVE",
                "employeeName": self.employeeName(),
                "taskNumber": self.getAllDataByNumber().taskNumber
            };
            var success = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        approveResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        approveResponse = 'success';
                        self.isAction(true);
                    }
                    var payloadString = JSON.stringify(payload);
                    // save log details for the task in DB
                    self.getTaskLogByNumber('saasService', approveResponse, 'approveAction', payloadString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        approveResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        approveResponse = 'success';
                        self.isAction(true);
                    }
                    var payloadString = JSON.stringify(payload);
                    // save log details for the task in DB
                    self.getTaskLogByNumber('saasService', approveResponse, 'approveAction', payloadString);
                } else {
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            services.addGeneric("bpmWorklist/updateTaskOutcome", payload).then(success, fail);
            app.loading(true);
        };
        self.getComment = function () {
            var payload = {};
            var success = function (CommentData) {
                if (CommentData == 'noTaskFound') {
                    self.messages.push({
                        severity: 'error',
                        summary: 'This task is no longer exist',
                        autoTimeout: 0
                    });
                } else {
                    self.CommentData(CommentData);
                    console.log(self.CommentData());
                }
            };
            var fail = function () {
                console.log('failed to get comments');
            };
            services.getGeneric("bpmWorklist/getComments/" + taskNumber + "/" + self.employeeName(), payload).then(success, fail);
        };
        self.cancelButton = function () {
            document.querySelector("#yesNoDialog").close();
        };
        var commentResponseForReject;
        var rejectResponse;
        self.rejectRewaardRequst = function (data, event) {
            var myComment;
            myComment = self.addCommentsTxt();
            self.commentDataTwo.push({commentScope: '',
                commentStr: self.addCommentsTxt(),
                updatedBy: '',
                updateddDate: '',
                userId: '',
                taskId: self.getAllDataByNumber().taskId});
            var commentJsonString = JSON.stringify(self.commentDataTwo());
            console.log(commentJsonString);
            var success001 = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        commentResponseForReject = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        commentResponseForReject = 'success';
                        self.isAction(false);
                    }
                    self.getComment();
                    //save log details for in mySql
                    self.getTaskLogByNumber('saasService', commentResponseForReject, 'addComment', commentJsonString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail001 = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        commentResponseForReject = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        commentResponseForReject = 'success';
                        self.isAction(false);
                    }
                    self.getComment();
                    //save log details in mySql
                    self.getTaskLogByNumber('saasService', commentResponseForReject, 'addComment', commentJsonString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            console.log(myComment);
            if (myComment != null)
                services.addGeneric("bpmWorklist/addComment/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, commentJsonString).then(success001, fail001);
            document.querySelector("#rejectDialog").close();
            var payload = {
                "taskId": self.getAllDataByNumber().taskId,
                "outcome": "REJECT",
                "employeeName": self.employeeName(),
                "taskNumber": self.getAllDataByNumber().taskNumber};
            var success = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        rejectResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        rejectResponse = 'success';
                        self.isAction(true);
                    }
                    var payloadString = JSON.stringify(payload);
                    //save log details in mySql
                    self.getTaskLogByNumber('saasService', rejectResponse, 'rejectAction', payloadString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        rejectResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        rejectResponse = 'success';
                        self.isAction(true);
                    }
                    var payloadString = JSON.stringify(payload);
                    //save log details in mySql
                    self.getTaskLogByNumber('saasService', rejectResponse, 'rejectAction', payloadString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            }
            services.addGeneric("bpmWorklist/updateTaskOutcome", payload).then(success, fail);
            app.loading(true);
        };
        self.rejectCancelButton = function () {
            document.querySelector("#rejectDialog").close();
        };
        /////////////// filter actions for every task \\\\\\\\\\\\\\\
        self.getActionDetails = function () {
            var success = function (data) {
                console.log(data.length);
                if (data.length < 5) {
                    self.actionBtnVisiblty(false);
                }
//                console.log(self.codeOP());
                self.actionData(data);
                console.log(self.actionData());
                actionDataVar = self.actionData();
                actionDataArray = actionDataVar.map(e => {
                    return {label: e.title, value: e.title};
                });
                for (var i = 0; i < actionDataArray.length; i++) {
                    if (actionDataArray[i].label == "PURGE" || actionDataArray[i].label == "SAVE" || actionDataArray[i].label === "SKIP_CURRENT_ASSIGNMENT"
                            || actionDataArray[i].label == "DELETE" || actionDataArray[i].label == "CREATE_TODO" || actionDataArray[i].label == "SUSPEND_TIMERS"
                            || actionDataArray[i].label == "VIEW_PROCESS_HISTORY" || actionDataArray[i].label == "VIEW_TASK" || actionDataArray[i].label == "CUSTOM"
                            || actionDataArray[i].label == "OVERRIDE_ROUTING_SLIP" || actionDataArray[i].label == "VIEW_TASK_HISTORY" || actionDataArray[i].label == "Complete"
                            || actionDataArray[i].label == "VIEW_SUB_TASKS" || actionDataArray[i].label == "DECOMPOSE_TASK" || actionDataArray[i].label == "Approve"
                            || actionDataArray[i].label == "Reject" || actionDataArray[i].label == "Discard" || actionDataArray[i].value == "DELEGATE") {
                        actionDataArray.splice(i, 1);
                        i--;
                    }
                    if (actionDataArray[i] != undefined) {
                        if (actionDataArray[i].label === "SKIP_CURRENT_ASSIGNMENT")
                            actionDataArray[i].label = "Skip Current Assignment"
                        else if (actionDataArray[i].label == "DELEGATE")
                            actionDataArray[i].label = "Delegate"
                        else if (actionDataArray[i].label == "RELEASE")
                            actionDataArray[i].label = "Release"
                        else if (actionDataArray[i].label == "SAVE")
                            actionDataArray[i].label = "Save"
                        else if (actionDataArray[i].label == "INFO_REQUEST")
                            actionDataArray[i].label = "Request More Information"
                        else if (actionDataArray[i].label == "WITHDRAW")
                            actionDataArray[i].label = "Withdraw"
                        else if (actionDataArray[i].label == "REASSIGN")
                            actionDataArray[i].label = "Reassign"
                        else if (actionDataArray[i].label == "ESCALATE")
                            actionDataArray[i].label = "Escalate"
                        else if (actionDataArray[i].label == "ADHOC_ROUTE")
                            actionDataArray[i].label = "Adhoc Route"
                        else if (actionDataArray[i].label == "SUSPEND")
                            actionDataArray[i].label = "Suspend"
                        else if (actionDataArray[i].label == "UPDATE_COMMENT")
                            actionDataArray[i].label = "Add Comment"
                        else if (actionDataArray[i].label == "UPDATE_ATTACHMENT")
                            actionDataArray[i].label = "Add Attachment"
                    }
                }
                self.actionBrows(actionDataArray);
            };
            var fail = function () {
                console.log('failed to get actions');
            };
            services.getGeneric("bpmWorklist/getActionList/" + taskNumber + "/" + self.employeeName()).then(success, fail);
        };
        var optionName;
        self.menuItemActionOne = function (event) {
            optionName = event.detail.originalEvent.srcElement.innerText;
            if (optionName == 'Reassign') {
                document.getElementById("ReassignTaskPopup").open();
            }
            if (optionName == 'Request More Information')
                document.getElementById("requestInfoPopup").open();
            if (optionName == 'Suspend')
                document.querySelector("#suspendYesNoDialog").open();
            if (optionName == 'Adhoc Route')
                document.getElementById("adhocRoutePopup").open();
            if (optionName == 'Suspend')
                document.querySelector("#suspendYesNoDialog").open();
            if (optionName == 'Withdraw')
                document.querySelector("#withdrawYesNoDialog").open();
            if (optionName == 'Escalate')
                document.querySelector('#escalateYesNoDialog').open();
            if (optionName == 'Add Attachment') {
                document.querySelector("#addAttachModalDialogForApprove").open();
                self.AttachTypeRadioSetForApprove('urlForApprove');
            }
            if (optionName == 'Release')
                document.querySelector("#releaseYesNoDialog").open();
            if (optionName == 'Add Comment')
                document.querySelector("#addCommentModalDialog").open();
            self.getActionDetails();
        };
        self.optionClick = function () {
        };
        //////////////////// Start get history with structure for history table \\\\\\\\\\\\\\\\\\\\\\\\\
        self.dynamicPayloadData = function () {
            var payload = {};
            var success = function (patternData) {
                if (patternData == 'fail') {
                    self.myPageVisibilty(false);
                    self.isShown(false);
                    app.loading(false);
                    self.dialogErrorMessage("We can not find any report for this task");
                    self.taskExistStatus(false);
                    document.getElementById("errorDialog").open();
                } else {
                    var returnedData = JSON.parse(patternData);
                    var myData = returnedData.report;
                    self.myPageVisibilty(true);
                    self.isShown(true);
                    app.loading(false);
                    self.errorMessage('');
                    self.setPageTitle(self.getAllDataByNumber().title)
                    if (returnedData.hasHistory == 'true') {
                        self.showHistory(true);
                    } else {
                        self.showHistory(false);
                    }

                    var substring001;
                    var substring002;
                    var newMyData;
                    if (myData.includes('Approval History')) {
                        substring001 = 'Approval History';
                        substring002 = myData.indexOf(substring001) + substring001.length;
                        newMyData = myData.substring(0, substring002);
                    } else if (myData.includes('Approvers')) {
                        substring001 = 'Approvers';
                        substring002 = myData.indexOf(substring001) + substring001.length;
                        newMyData = myData.substring(0, substring002);
                    } else if (myData.includes('Approvals')) {
                        substring001 = 'Approvals';
                        substring002 = myData.indexOf(substring001) + substring001.length;
                        newMyData = myData.substring(0, substring002);
                    } else {
                        newMyData = myData;
                    }
                    if (newMyData.includes('Â')) {
                        newMyData = newMyData.replace(/Â+/g, '');
                    }
                    document.getElementById('dynamicPayload').innerHTML = newMyData;
                }
            };
            var fail = function () {
            };
            services.getGeneric("bpmWorklist/segmentName/" + self.getAllDataByNumber().taskNamespace + "/taskNumber/" + self.getAllDataByNumber().taskNumber + "/" + self.employeeName(), payload).then(success, fail);
        };
        self.getTaskLogByNumber = function (serviceName, reponse, serviceActionType, serviceData) {
//            var success = function (data) {
//                console.log(data);
//                console.log(data.taskNumber);
//                if (typeof data.taskNumber == 'undefined')
//                {
                    self.addTaskLogForAction(serviceName, reponse, serviceActionType, serviceData);
//                } else {
//                    self.addTaskLogDetails(serviceName, reponse, serviceActionType, serviceData);
//                }
//            };
//            var fail = function (data) {
//                console.log('faield');
//                console.log(data);
//            };
//            services.getGeneric("tasksLog/getLogByTaskNumber/" + self.getAllDataByNumber().taskNumber).then(success, fail);
        };
        self.addTaskLogForAction = function (serviceName, reponse, serviceActionType, serviceData) {
            console.log(self.getAllDataByNumber());
            app.loading(false);
            var payload = {
                "previousApprover": self.getAllDataByNumber().creator,
                "nextApprover": self.getAllDataByNumber().assignee,
                "taskNumber": self.getAllDataByNumber().taskNumber,
                "companyName":"tawal",
                "status": self.getAllDataByNumber().state,
                "display_status": self.getAllDataByNumber().state,
                "taskDetails": [{
                        "taskNumber": self.getAllDataByNumber().taskNumber,
                        "serviceName": serviceName,
                        "serviceStatus": reponse,
                        "serviceActionType": serviceActionType,
                        "serviceData": serviceData
                    }]
            };
            var success = function (data) {
                console.log('success');
                console.log(data);
                if (self.isAction() == true) {
                    window.opener = null;
                    window.close();
                }
            };
            var fail = function (data) {
                console.log('faield');
                console.log(data);
                if (self.isAction() == true) {
                    window.opener = null;
                    window.close();
                }
            };
            services.addGeneric("tasksLog/addTaskLog", payload).then(success, fail);
        };
        self.addTaskLogDetails = function (serviceName, reponse, serviceActionType, serviceData) {
            console.log(self.getAllDataByNumber());
            app.loading(false);
            var payload = {
                "taskNumber": self.getAllDataByNumber().taskNumber,
                "serviceName": serviceName,
                "serviceStatus": reponse,
                "serviceActionType": serviceActionType,
                "serviceData": serviceData
            };
            var success = function (data) {
                console.log('success');
                console.log(data);
                if (self.isAction() == true) {
                    window.opener = null;
                    window.close();

                }
            };
            var fail = function (data) {
                console.log('faield');
                console.log(data);
                if (self.isAction() == true) {
                    window.opener = null;
                    window.close();
                }
            };
            services.addGeneric("tasksLog/addTaskLogDetails", payload).then(success, fail);
        };
        ////////////// suspend ///////////
        self.suspendYES = function () {
            var payload = {"taskId": self.getAllDataByNumber().taskId};
            var suspendPayload = JSON.stringify(payload);
            var suspendStatus;
            var success = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        suspendStatus = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        suspendStatus = 'success';
                        self.isAction(true);
                    }
                    // save log details in mySql
                    self.getTaskLogByNumber('saasService', suspendStatus, 'suspendAction', suspendPayload);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        suspendStatus = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        suspendStatus = 'success';
                        self.isAction(true);
                    }
                    // save log details in mySql
                    self.getTaskLogByNumber('saasService', suspendStatus, 'suspendAction', suspendPayload);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            services.addGeneric("bpmWorklist/actionType/suspendTask/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, payload).then(success, fail);
            document.querySelector("#suspendYesNoDialog").close();
            app.loading(true);
        };
        self.suspendNO = function () {
            document.querySelector("#suspendYesNoDialog").close();
        };
        //////////////// withdraw //////////////////
        self.withdrawYES = function () {
            var payload = {"taskId": self.getAllDataByNumber().taskId};
            var withdrawPayload = JSON.stringify(payload);
            var withdrawResponse;
            var success = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        withdrawResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        withdrawResponse = 'success';
                        self.isAction(true);
                    }
                    // save log details in mySql
                    self.getTaskLogByNumber('saasService', withdrawResponse, 'withdrawAction', withdrawPayload);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        withdrawResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        withdrawResponse = 'success';
                        self.isAction(true);
                    }
                    // save log Details in mySql
                    self.getTaskLogByNumber('saasService', withdrawResponse, 'withdrawAction', withdrawPayload);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            services.addGeneric("bpmWorklist/actionType/withDrawTask/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, payload).then(success, fail);
            document.querySelector("#withdrawYesNoDialog").close();
            app.loading(true);
        };
        self.withdrawNO = function () {
            document.querySelector("#withdrawYesNoDialog").close();
        };
        ///////////////// esclation ////////////////
        self.escalateYES = function () {
            var payload = {"taskId": self.getAllDataByNumber().taskId};
            console.log(payload);
            var escalatePayload = JSON.stringify(payload);
            var escalateResponse;
            console.log(escalatePayload);
            var success = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        escalateResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        escalateResponse = 'success';
                        self.isAction(true);
                    }
                    self.getTaskLogByNumber('saasService', escalateResponse, 'escalateAction', escalatePayload);
                } else {
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        escalateResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        escalateResponse = 'success';
                        // save log details in mySql
                        self.isAction(true);
                    }
                    self.getTaskLogByNumber('saasService', escalateResponse, 'escalateAction', escalatePayload);
                } else {
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            services.addGeneric("bpmWorklist/actionType/escalateTask/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, payload).then(success, fail);
            document.querySelector('#escalateYesNoDialog').close();
            app.loading(true);
        };
        self.escalateNO = function () {
            document.querySelector('#escalateYesNoDialog').close();
        };
        //******************* REASSIGN ***********************//
        self.usercolumnArray = ko.observableArray();
        self.usercolumnArray([{"template": "checkTemplate"}, {"headerText": "ID", "field": "UserName", "resizable": "enabled"}, {"headerText": "First Name", "field": "FirstName", "resizable": "enabled"}, {"headerText": "Last Name", "field": "LastName", "resizable": "enabled"}, {"headerText": "Email", "field": "WorkEmail", "resizable": "enabled"}, {"headerText": "Title", "field": "title", "resizable": "enabled"}, {"headerText": "Manager", "field": "ManagerId", "resizable": "enabled"}, {"headerText": "Organization", "field": "LegalEntityId", "resizable": "enabled"}]);
        self.reassignDataprovider = new oj.ArrayTableDataSource(self.RequestreassignData, {keyAttribute: 'UserName'});
        self.groupreassignDataprovider = new oj.ArrayTableDataSource(self.reassignData, {keyAttribute: 'id'});
        self.groupcolumnArray = ko.observableArray();
        self.groupcolumnArray([{"template": "checkTemplate"}, {"headerText": "Group ID", "field": "id", "resizable": "enabled"}]);
        self.searchDataprovider = new oj.ArrayTableDataSource(self.RequestreassignData, {keyAttributes: 'UserName'});
        self.searchColumnArray = ko.observableArray();
        self.searchColumnArray([{"template": "checkTemplate001"}, {"headerText": "ID", "field": "UserName", "resizable": "enabled"}, {"headerText": "First Name", "field": "FirstName", "resizable": "enabled"}, {"headerText": "Last Name", "field": "LastName", "resizable": "enabled"}, {"headerText": "Email", "field": "WorkEmail", "resizable": "enabled"}, {"headerText": "Title", "field": "title", "resizable": "enabled"}, {"headerText": "Manager", "field": "ManagerId", "resizable": "enabled"}, {"headerText": "Organization", "field": "LegalEntityId", "resizable": "enabled"}]);
        self.reassignSearch = function () {
            if (isEmpty(self.firstName()) && isEmpty(self.lastName()) && isEmpty(self.email()) && isEmpty(self.id())) {
                self.messagess.push({severity: 'error', summary: 'You Must Enter At Least One Value', detail: ""});
            } else {
                var fname001 = self.firstName();
                var fname002 = self.firstName().charAt(0).toUpperCase();
                var fname = fname002 + fname001.substring(1);

                var lname001 = self.lastName();
                var lname002 = self.lastName().charAt(0).toUpperCase();
                var lname = lname002 + lname001.substring(1);

                for (var i in self.RequestreassignData())
                    if (self.RequestreassignData()[i].selected() == true) {
                        self.RequestreassignData()[i].selected(false)
                    }
                self.RequestreassignData([]);
                var payload = {"firstName": fname, "lastName": lname, "userName": self.id(), "workMail": self.email(), "scope": self.value()};
                $(".searchbutton button").addClass("loading");
                var success = function (Data) {
                    console.log(Data);
                    $(".searchbutton button").removeClass("loading");
                    for (var i = 0; i < Data.items.length; i++) {
                        if (Data.items[i].UserName == null) {
                            Data.items.splice(i, 1);
                            i--;
                        }
                    }
                    Data.items.forEach(e => {
                        e.selected = ko.observable(false);
                    });
                    self.RequestreassignData(Data.items);
                };
                var fail = function () {};
                services.addGeneric("bpmWorklist/Search" + "/" + self.employeeName(), payload).then(success, fail);
            }

        };
        self.Array = ko.observableArray();
        self.reassignSearchforGR = function () {
            self.reassignData([]);
            var payload = {"firstName": self.firstName(), "lastName": self.lastName(), "userName": self.id(), "workMail": self.email(), "scope": "group"};
            $(".searchbutton button").addClass("loading");
            var success = function (data) {
                $(".searchbutton button").removeClass("loading");
                self.Array(data.items);
                var filterbyid = self.Array().filter(e => self.groupID() ? e.id == self.groupID() : true);
                filterbyid.forEach(e => {
                    e.selected = ko.observable(false);
                });
                self.reassignData(filterbyid);
            };
            var fail = function () {
            };
            services.addGeneric("bpmWorklist/Search", payload).then(success, fail);
        };
        self.reassignReset = function () {
            self.firstName("");
            self.lastName("");
            self.id("");
            self.email("");
            self.groupID("");
//            self.values([]);
            self.myTextArea([]);
            self.value('user');
            self.radioSetCurrentSelected('');
            for (var i in self.RequestreassignData())
                if (self.RequestreassignData()[i].selected() == true) {
                    self.RequestreassignData()[i].selected(false);
                }
            self.RequestreassignData([]);
            for (var i in self.reassignData())
                if (self.reassignData()[i].selected() == true) {
                    self.reassignData()[i].selected(false);
                }
            self.reassignData([]);
            self.reassignTaskRadioSet('ReassignValue');
        };
        self.reassignTableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            console.log(data, currentRow);
        };
        self.searchTableSelectionListener = function (event) {
            console.log(event.detail);
        };
        self.valueChangedHandler = function (event) {
            console.log(event.detail);
        };
        self.reassignSelectValueChange = function (event) {
            console.log(event.detail);
        };
        self.reassignModalDialogClose = function () {
            document.getElementById("ReassignSearchTaskPopup").close();
            self.reassignTaskRadioSet('ReassignValue');
        };
        self.closeReqSearch = function () {
            self.firstName('');
            self.lastName('');
            self.email('');
            self.id('');
            self.reassignReset();
        };
        self.ReassignPopUpClose = function () {
            self.firstName("");
            self.lastName("");
            self.id("");
            self.email("");
            self.groupID("");
//            self.values([]);
            self.value('user');
            self.radioSetCurrentSelected('');
            for (var i in self.RequestreassignData())
                if (self.RequestreassignData()[i].selected() == true) {
                    self.RequestreassignData()[i].selected(false);
                }
            self.RequestreassignData([]);
            for (var i in self.reassignData())
                if (self.reassignData()[i].selected() == true) {
                    self.reassignData()[i].selected(false);
                }
            self.reassignData([]);
            self.reassignTaskRadioSet('ReassignValue');
        };
        self.valueChanged = function (event) {
            self.reassignTaskRadioSet(event['detail'].value);
            if (event['detail'].value == 'DelegateValue')
                self.reassignReset();
            else
                self.reassignReset();
        };
        self.checkboxListenerReassGr = function (event) {
            if (event.detail && event.detail.value && self.radioSetCurrentSelected() && self.radioSetCurrentSelected()[2] == event.detail.value[2])
                return;

            if (event.detail != null) {
                var myevent = event.detail.value
                console.log(myevent);
                self.myTextArea(myevent[2]);
                self.radioSetCurrentSelected(myevent);
            }
        };
        self.printCurrentSelection = function () {
            var selectionTxt = "";
            var selected = self.selectedItemsInf();
            if (selected.isAddAll()) {
                var iterator = selected.deletedValues();
                iterator.forEach(function (key) {
                    selectionTxt = selectionTxt.length === 0 ? key : selectionTxt + ", " + key;
                });

                if (iterator.size > 0) {
                    selectionTxt = " except " + selectionTxt;
                }
                selectionTxt = "Everything selected" + selectionTxt;
            } else {
                selected.values().forEach(function (key) {
                    selectionTxt = selectionTxt.length === 0 ? key : selectionTxt + ", " + key;
                });
            }
        };
        self.checkboxListenerInf = function (event) {
            if (event.detail != null) {
                var value = event.detail.value;
                var key = (event.target.dataset.rowKey);
                if (selectedDataInf.indexOf(key) == -1)
                    selectedDataInf.push(key);
                else
                    selectedDataInf = selectedDataInf.filter(e => e != key);
                console.log(selectedDataInf);
                self.myTextArea(selectedDataInf);
                console.log(self.myTextArea());
            }
        };
        self.selectNone = function () {
            if (self.value() == 'group') {
                self.reassignData([]);
                self.reassignSearchforGR();
            } else {
                self.RequestreassignData([]);
                self.reassignSearch();
            }
            self.values([]);
            self.visibleselect(false);
        };
        self.reassignPayloadStringForGr = ko.observableArray();
        self.reassignOKGroup = function () {
            if (isEmpty(self.groupID())) {
                self.messagess.push({severity: 'error', summary: 'You Must Value For Search', detail: ""});
            } else {
                for (var i = 0; i < 1; i++) {
                    var payload = {"taskId": self.getAllDataByNumber().taskId, "userName": self.myTextArea(), "scope": self.value()};
                    self.reassignPayloadStringForGr.push(payload);
                }
                var reassignGrpResponse;
                var reassignPayloadStringForGrStr = JSON.stringify(self.reassignPayloadStringForGr());
                console.log(reassignPayloadStringForGrStr);
                var success = function (data) {
                    data = JSON.parse(data);
                    if (data.taskExist == 'Yes') {
                        if (data.status != 'Success') {
                            reassignGrpResponse = 'fail';
                            self.dialogErrorMessage(data.error);
                            document.getElementById("errorDialog").open();
                        } else {
                            reassignGrpResponse = 'success';
                            self.isAction(true);
                        }
                        // save log details in mySql
                        self.getTaskLogByNumber('saasService', reassignGrpResponse, 'reassignGroupAction', reassignPayloadStringForGrStr);
                    } else {
                        app.loading(false);
                        self.taskExistStatus(false);
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    }
                };
                var fail = function (data) {
                    data = JSON.parse(data);
                    if (data.taskExist == 'Yes') {
                        if (data.status != 'Success') {
                            reassignGrpResponse = 'fail';
                            self.dialogErrorMessage(data.error);
                            document.getElementById("errorDialog").open();
                        } else {
                            reassignGrpResponse = 'success';
                            self.isAction(true);
                        }
                        // save log detials in mySql
                        self.isAction(true);
                        self.getTaskLogByNumber('saasService', reassignGrpResponse, 'reassignGroupAction', reassignPayloadStringForGrStr);
                    } else {
                        app.loading(false);
                        self.taskExistStatus(false);
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    }
                };
                services.addGeneric("bpmWorklist/actionType/reassignTask/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, payload).then(success, fail);
                document.getElementById("ReassignTaskPopup").close();
                self.reassignReset();
                app.loading(true);
            }
        };
        self.reassignPayload = ko.observableArray();
        self.reassignOKUser = function () {
            var reassignUserResponse;
            if (self.myTextArea().length <= 0) {
                self.messagess.push({severity: 'error', summary: 'reassign not done', detail: ""});
            } else {
                for (var i = 0; i < self.myTextArea().length; i++) {
                    var payload = {"taskId": self.getAllDataByNumber().taskId, "userName": self.myTextArea()[i], "scope": self.value()};
                    self.reassignPayload.push(payload);
                    var reassignPayloadString = JSON.stringify(self.reassignPayload());
                }
                ;
                var success = function (data) {
                    data = JSON.parse(data);
                    if (data.taskExist == 'Yes') {
                        if (data.status != 'Success') {
                            reassignUserResponse = 'fail';
                            self.dialogErrorMessage(data.error);
                            document.getElementById("errorDialog").open();
                        } else {
                            reassignUserResponse = 'success';
                            self.isAction(true);
                        }
                        // save log details in mySql
                        self.getTaskLogByNumber('saasService', reassignUserResponse, 'reassignUserAction', reassignPayloadString);
                    } else {
                        app.loading(false);
                        self.taskExistStatus(false);
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    }
                };
                var fail = function (data) {
                    data = JSON.parse(data);
                    if (data.taskExist == 'Yes') {
                        if (data.status != 'Success') {
                            reassignUserResponse = 'fail';
                            self.dialogErrorMessage(data.error);
                            document.getElementById("errorDialog").open();
                        } else {
                            reassignUserResponse = 'success';
                            self.isAction(true);
                        }
                        // save log details in mySql
                        self.getTaskLogByNumber('saasService', reassignUserResponse, 'reassignUserAction', reassignPayloadString);
                    } else {
                        app.loading(false);
                        self.taskExistStatus(false);
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    }
                };
                console.log(reassignPayloadString);
                services.addGeneric("bpmWorklist/actionType/reassignTask/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, reassignPayloadString).then(success, fail);
                document.getElementById("ReassignTaskPopup").close();
                self.reassignReset();
                app.loading(true);
            }
        };
        self.FromTxt = ko.observable();
        self.DelegateOKUser = function () {
            if (self.myTextArea().length <= 0) {
                self.messagess.push({severity: 'error', summary: 'reassign not done', detail: ""});
            } else {
                for (var i = 0; i < self.myTextArea().length; i++) {
                    var payload = {"taskId": self.getAllDataByNumber().taskId, "userName": self.myTextArea()[i]};
                    self.delegatePayload.push(payload);
                    var delegatePayloadString = JSON.stringify(self.delegatePayload());
                }
                var delegateResponse;
                var success = function (data) {
                    data = JSON.parse(data);
                    if (data.taskExist == 'Yes') {
                        if (data.status != 'Success') {
                            delegateResponse = 'fail';
                            self.dialogErrorMessage(data.error);
                            document.getElementById("errorDialog").open();
                        } else {
                            delegateResponse = 'success';
                            self.isAction(true);
                        }
                        // save log details in mySql
                        self.getTaskLogByNumber('saasService', delegateResponse, 'delegateAction', delegatePayloadString);
                    } else {
                        app.loading(false);
                        self.taskExistStatus(false);
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    }
                };
                var fail = function (data) {
                    data = JSON.parse(data);
                    if (data.taskExist == 'Yes') {
                        if (data.status != 'Success') {
                            delegateResponse = 'fail';
                            self.dialogErrorMessage(data.error);
                            document.getElementById("errorDialog").open();
                        } else {
                            delegateResponse = 'success';
                            self.isAction(true);
                        }
                        // save log details in mySql
                        self.getTaskLogByNumber('saasService', delegateResponse, 'delegateAction', delegatePayloadString);
                    } else {
                        app.loading(false);
                        self.taskExistStatus(false);
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    }
                };
                services.addGeneric("bpmWorklist/actionType/DelegateTask/" + self.employeeName() + "/" + self.getAllDataByNumber().taskNumber, delegatePayloadString).then(success, fail);
                console.log(delegatePayloadString);
                document.getElementById("ReassignTaskPopup").close();
                self.reassignReset();
                self.FromTxt("");
                app.loading(true);
            }
        };
        self.closeErrorDialog = function () {
            document.getElementById("errorDialog").close();
            if (self.taskExistStatus() == false) {
                window.opener = null;
                window.close();
            }
        }
        self.reassignClose = function () {
            document.getElementById("ReassignTaskPopup").close();
            document.getElementById("RequestTaskPopup").close();
            self.reassignReset();
            self.FromTxt("");
        };
        var selectedDataInf = [];
        self.handleCheckboxInf = function (id) {
            var isChecked = selectedDataInf.indexOf(id) > -1;
            return isChecked ? ["checked"] : [];
        };
        self.cancelBehaviorForComment = function () {
            self.addCommentsTxtForApprove('');
        };
        self.getHistoryData = function () {
            self.getAttachment();
            var success = function (data) {
                console.log(data)
                if (data == 'fail') {
                    self.historyStatus('History not available');
                } else {
                    self.historyData([]);
                    self.historyStatus('');
                    self.historyData(data);
                    console.log(data);
                    console.log(self.AttachmentData());

//debugger
                    for (var i = self.historyData().length - 1; i >= 0; i--) {
                        if (self.historyData()[i].displayName == 'null') {
                            self.historyData().splice(i, 1);
                            i--;
                        }
                    }
                    for (var i = self.historyData().length - 1; i >= 0; i--) {
                        if ((self.historyData()[i].actionName == 'Assigned' && self.historyData()[i].reason != 'NOTANNUALLEAVE') || (self.historyData()[i].actionName == 'Assigned' && self.historyData().length == i)) {
                            if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (self.historyData()[i + 1] && self.historyData()[i + 2] && self.historyData()[i + 1].state == 'COMPLETED' && self.historyData()[i + 2].state == 'COMPLETED' && !self.historyData()[i + 3]) || (!self.historyData()[i + 1])) {
                                self.drawAssignedToSection(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, 'FCF1C3');
                                self.drawArrowUp(i);
                            } else {
                                self.drawAssignedToSection(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, 'F4F4F4');
                            }
                        }
                        //self.drawReassign = function (name, assignedFrom, date, index, reason, color)
                        if (self.historyData()[i].actionName == 'Reassigned') {
                            if (self.historyData()[i - 1].actionName == 'Comment Added') {
                                if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (!self.historyData()[i + 1])) {
                                    self.drawReassign(self.historyData()[i].displayName, self.historyData()[i - 1].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i - 1].reason, 'FCF1C3');
                                    self.drawArrowUp(i);
                                } else {
                                    self.drawReassign(self.historyData()[i].displayName, self.historyData()[i - 1].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i - 1].reason, 'F4F4F4');
                                }
                            } else {
                                if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (!self.historyData()[i + 1])) {
                                    self.drawReassign(self.historyData()[i].displayName, self.historyData()[i - 1].displayName, self.historyData()[i].updatedDate, i, '', 'FCF1C3');
                                    self.drawArrowUp(i);
                                } else {
                                    self.drawReassign(self.historyData()[i].displayName, self.historyData()[i - 1].displayName, self.historyData()[i].updatedDate, i, '', 'F4F4F4');
                                }

                            }
                        }
                        if (self.historyData()[i].actionName == 'Rejected' || self.historyData()[i].actionName == 'Task Completed - Rejected') {
                            if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (self.historyData()[i + 1] && self.historyData()[i + 2] && self.historyData()[i + 1].state == 'COMPLETED' && self.historyData()[i + 2].state == 'COMPLETED' && !self.historyData()[i + 3]) || (!self.historyData()[i + 1])) {
                                self.drawReject(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, 'FCF1C3');
                                self.drawArrowUp(i);
                            } else {
                                self.drawReject(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, 'F4F4F4');
                            }
                        }
                        if (self.historyData()[i].actionName == 'Completed') {
                            if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (!self.historyData()[i + 2])) {
                                self.drawCompleted(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, 'FCF1C3');
                                self.drawArrowUp(i);
                            } else {
                                self.drawCompleted(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, 'F4F4F4');
                            }
                        }
                        if (self.historyData()[i].actionName == 'Suspended') {
                            if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (self.historyData()[i + 1] && self.historyData()[i + 2] && self.historyData()[i + 1].state == 'COMPLETED' && self.historyData()[i + 2].state == 'COMPLETED' && !self.historyData()[i + 3]) || (!self.historyData()[i + 1])) {
                                self.drawSuspend(self.historyData()[i].updatedDate, i, 'FCF1C3');
                                self.drawArrowUp(i);
                            } else {
                                self.drawSuspend(self.historyData()[i].updatedDate, i, 'F4F4F4');
                            }
                        }
                        if (self.historyData()[i].actionName == 'Resumed') {
                            if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (self.historyData()[i + 1] && self.historyData()[i + 2] && self.historyData()[i + 1].state == 'COMPLETED' && self.historyData()[i + 2].state == 'COMPLETED' && !self.historyData()[i + 3]) || (!self.historyData()[i + 1])) {
                                self.drawResumed(self.historyData()[i].updatedDate, i, 'FCF1C3');
                                self.drawArrowUp(i);
                            } else {
                                self.drawResumed(self.historyData()[i].updatedDate, i, 'F4F4F4');
                            }
                        }
                        if (self.historyData()[i].actionName == 'Withdrawn') {
                            if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (self.historyData()[i + 1] && self.historyData()[i + 2] && self.historyData()[i + 1].state == 'COMPLETED' && self.historyData()[i + 2].state == 'COMPLETED' && !self.historyData()[i + 3]) || (!self.historyData()[i + 1])) {
                                self.drawWithdraw(self.historyData()[i].updatedDate, i, self.historyData()[i].displayName == '' ? self.getAllDataByNumber().creator : self.historyData()[i].displayName, 'FCF1C3');
                                self.drawArrowUp(i);
                            } else {
                                self.drawWithdraw(self.historyData()[i].updatedDate, i, self.historyData()[i].displayName == '' ? self.getAllDataByNumber().creator : self.historyData()[i].displayName, 'F4F4F4');
                            }
                        }


                        if (self.historyData()[i].actionName == 'Delegated') {
                            if (self.historyData()[i - 1].actionName == 'Comment Added') {
                                if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (!self.historyData()[i + 1])) {
                                    self.drawDelegation(self.historyData()[i].displayName, self.historyData()[i - 1].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i - 1].reason, 'FCF1C3')
                                    self.drawArrowUp(i);
                                } else {
                                    self.drawDelegation(self.historyData()[i].displayName, self.historyData()[i - 1].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i - 1].reason, 'F4F4F4');
                                }
                            } else {
                                if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (!self.historyData()[i + 1])) {
                                    self.drawDelegation(self.historyData()[i].displayName, self.historyData()[i - 1].displayName, self.historyData()[i].updatedDate, i, '', 'FCF1C3');
                                    self.drawArrowUp(i);
                                } else {
                                    self.drawDelegation(self.historyData()[i].displayName, self.historyData()[i - 1].displayName, self.historyData()[i].updatedDate, i, '', 'F4F4F4');
                                }

                            }
                        }

                        if (self.historyData()[i].actionName == 'Approved' || self.historyData()[i].actionName == 'Task Completed - Approved') {
                            if (self.historyData()[i - 1].actionName == 'Comment Added') {
                                if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (!self.historyData()[i + 1])) {
                                    self.drawApproved(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i - 1].reason, 'FCF1C3');
                                    self.drawArrowUp(i);
                                } else {
                                    self.drawApproved(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i - 1].reason, 'F4F4F4');
                                }
                            } else {
                                if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (self.historyData()[i + 1] && self.historyData()[i + 1].state == 'COMPLETED' && !self.historyData()[i + 2]) || (!self.historyData()[i + 1])) {
                                    self.drawApproved(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, '', 'FCF1C3');
                                    self.drawArrowUp(i);
                                } else {
                                    self.drawApproved(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, '', 'F4F4F4');
                                }

                            }
                        }

                        // if(historyData[i].actionName = 'Comment Added' && historyData[i+11].actionName !='Reassigned')
//                        debugger
                        if (self.historyData()[i].actionName == 'Comment Added') {
                            if (self.historyData().length > 1) {
                                console.log(self.historyData()[i]);
                                console.log(self.historyData()[i + 1]);
                                if ((self.historyData()[i + 1] && self.historyData()[i + 1].state != 'Future participant' && self.historyData()[i + 1].actionName != 'Reassigned')) {
                                    self.drawCommentsDetails(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i].reason, 'F4F4F4');
                                    ;
                                }

                                if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (!self.historyData()[i + 1])) {
                                    self.drawCommentsDetails(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i].reason, 'FCF1C3');
                                    self.drawArrowUp(i);
                                }
                            }
                        }
//                        debugger
                        if (self.historyData()[i].actionName == 'Attachment Added') {
                            for (var j = 0; j < self.AttachmentData().length; j++) {

                                if ((self.historyData()[i].reason == self.AttachmentData()[j].title) && (self.historyData()[i].updatedDate == self.AttachmentData()[j].updatedDate)) {

                                    if (self.AttachmentData()[j].uri.rel == 'rel') {
                                        if (self.historyData()[i + 1] && self.historyData()[i + 1].state != 'Future participant') {
                                            self.drawAttachmentDetails(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i].reason, self.AttachmentData()[j].uri.href, "target", "blank", "attachment-url-logo.png", 'F4F4F4');
                                        } else if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (!self.historyData()[i + 1])) {
                                            self.drawAttachmentDetails(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i].reason, self.AttachmentData()[j].uri.href, "target", "blank", "attachment-url-logo.png", 'FCF1C3');
                                            self.drawArrowUp(i);
                                        }
                                    } else {
                                        var decodingFormat = 'data:application/octet-stream;base64, ';
                                        if (self.historyData()[i + 1] && self.historyData()[i + 1].state != 'Future participant') {
                                            self.drawAttachmentDetails(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i].reason, decodingFormat + self.AttachmentData()[j].encodedAttachment, "download", self.AttachmentData()[j].title, "attachment-logo.png", 'F4F4F4');
                                        } else if ((self.historyData()[i + 1] && self.historyData()[i + 1].state == 'Future participant') || (!self.historyData()[i + 1])) {
                                            self.drawAttachmentDetails(self.historyData()[i].displayName, self.historyData()[i].updatedDate, i, self.historyData()[i].reason, decodingFormat + self.AttachmentData()[j].encodedAttachment, "download", self.AttachmentData()[j].title, "attachment-logo.png", 'FCF1C3');
                                            self.drawArrowUp(i);
                                        }
                                    }
                                }
                            }
                        }

                        if (self.historyData()[i].state == 'Future participant') {
                            self.drawNextApprovalSection(self.historyData()[i].displayName, i);
                            if (self.historyData().length > 1) {
                                if (self.historyData()[i - 1].state != 'Future participant')
                                    self.drawArrowUp(i);
                            }
                        }
                    }
                    var createdDate = self.getAllDataByNumber().createdDate.substring(0, self.getAllDataByNumber().updatedDate.length - 5)
                    createdDate = createdDate.replace('T', ' ');
                    self.drawSubmittedBySection(self.getAllDataByNumber().creator, createdDate);
                    console.log(data);
                    console.log(self.getAllDataByNumber());
                }
            }
            var fail = function () {
                self.historyStatus('History not available');
            }
            if (self.showHistory())
                services.getGeneric("bpmWorklist/getHistoryTest/" + self.getAllDataByNumber().taskNumber + "/" + self.employeeName()).then(success, fail);
        };
        self.drawWithdraw = function (date, index, name, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////

            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none;font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////

            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = name;
            secondP.innerText = "Withdrawn By ";
            firstImage.setAttribute("src", "css/images/icons/action-logo-stc.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(secondP);
            firstTD1.appendChild(firstP);
        };
        self.drawResumed = function (date, index, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////

            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none;font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////

            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = '';
            secondP.innerText = "Resumed";
            firstImage.setAttribute("src", "css/images/icons/action-logo-stc.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(secondP);
            firstTD1.appendChild(firstP);
        };
        self.drawApproved = function (name, date, index, reason, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////

            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none;font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////

            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = name;
            secondP.innerText = "Approved By ";
            firstImage.setAttribute("src", "css/images/icons/approved-mark.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(secondP);
            firstTD1.appendChild(firstP);

            /////////////// second Row /////////////////////
            var secondRow = document.createElement("tr");
            secondRow.setAttribute("id", "secondRow" + index);
            document.getElementById("myTable" + index).appendChild(secondRow);
            var secondTD = document.createElement("TD");
            var secondTD1 = document.createElement("TD");
            var secondTD2 = document.createElement("TD");
            secondTD2.setAttribute("id", "myTD2" + index);
            document.getElementById("secondRow" + index).appendChild(secondTD);
            document.getElementById("secondRow" + index).appendChild(secondTD1);
            document.getElementById("secondRow" + index).appendChild(secondTD2);
            var secondSpan = document.createElement("href");
            secondSpan = document.createTextNode(reason);
            secondTD1.setAttribute("style", "border:none;font-size:14px");
            secondTD1.appendChild(secondSpan);
        };
        self.drawReject = function (name, date, index, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////

            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none;font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////

            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = name;
            secondP.innerText = "Rejected By ";
            firstImage.setAttribute("src", "css/images/icons/reject-icon.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(secondP);
            firstTD1.appendChild(firstP);
        };
        self.drawCompleted = function (name, date, index, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////

            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none;font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////

            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = '';
            secondP.innerText = "Task Completed";
            firstImage.setAttribute("src", "css/images/icons/approved-mark.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(secondP);
            firstTD1.appendChild(firstP);
        };
        self.drawSuspend = function (date, index, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////

            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none;font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////

            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = '';
            secondP.innerText = "Suspend";
            firstImage.setAttribute("src", "css/images/icons/action-logo-stc.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(secondP);
            firstTD1.appendChild(firstP);
        };
        self.drawAttachmentDetails = function (name, date, index, title, link, action, dest, imageName, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////
            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none; font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////
            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = name + "  ";
            secondP.innerText = "    provided additional details";
            firstImage.setAttribute("src", "css/images/icons/action-logo-stc.png");
            firstTD.setAttribute("style", "border:none");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(firstP);
            firstTD1.appendChild(secondP);
            /////////////// second Row /////////////////////
            var secondRow = document.createElement("tr");
            secondRow.setAttribute("id", "secondRow" + index);
            document.getElementById("myTable" + index).appendChild(secondRow);
            var secondTD = document.createElement("TD");
            var secondTD1 = document.createElement("TD");
            var secondTD2 = document.createElement("TD");
            secondTD2.setAttribute("id", "myTD2" + index);
            var secondImage = document.createElement("img");
            secondImage.setAttribute("src", "css/images/icons/" + imageName);
            document.getElementById("secondRow" + index).appendChild(secondTD);
            document.getElementById("secondRow" + index).appendChild(secondTD1);
            document.getElementById("secondRow" + index).appendChild(secondTD2);
            secondTD1.setAttribute("style", "border:none");
            var anchor = document.createElement("a");
            anchor.setAttribute("href", link);
            anchor.setAttribute(action, dest);
            anchor.setAttribute("style", "font-size:14px");
            anchor.innerText = title;
            secondTD1.appendChild(secondImage);
            secondTD1.appendChild(anchor);
        };
        self.drawCommentsDetails = function (name, date, index, reason, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////
            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none; font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////
            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = name + "  ";
            secondP.innerText = "    provided additional details";
            firstImage.setAttribute("src", "css/images/icons/action-logo-stc.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(firstP);
            firstTD1.appendChild(secondP);
            /////////////// second Row /////////////////////
            var secondRow = document.createElement("tr");
            secondRow.setAttribute("id", "secondRow" + index);
            document.getElementById("myTable" + index).appendChild(secondRow);
            var secondTD = document.createElement("TD");
            var secondTD1 = document.createElement("TD");
            var secondTD2 = document.createElement("TD");
            secondTD2.setAttribute("id", "myTD2" + index);
            document.getElementById("secondRow" + index).appendChild(secondTD);
            document.getElementById("secondRow" + index).appendChild(secondTD1);
            document.getElementById("secondRow" + index).appendChild(secondTD2);
            var secondSpan = document.createElement("href");
            secondSpan = document.createTextNode(reason);
            secondTD1.setAttribute("style", "border:none;font-size:14px");
            secondTD1.appendChild(secondSpan);
        };
        self.drawSubmittedBySection = function (name, date) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + "-1");
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#f4f4f4");
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////

            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + "-1");
            document.getElementById("myTable" + "-1").appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + "-1").appendChild(zeroTD);
            document.getElementById("zeroRow" + "-1").appendChild(zeroTD1);
            document.getElementById("zeroRow" + "-1").appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none;font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////

            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + "-1");
            document.getElementById("myTable" + "-1").appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + "-1").appendChild(firstTD);
            document.getElementById("firstRow" + "-1").appendChild(firstTD1);
            document.getElementById("firstRow" + "-1").appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = name;
            secondP.innerText = "Submitted By ";
            firstImage.setAttribute("src", "css/images/icons/submittiedby-icon.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(secondP);
            firstTD1.appendChild(firstP);
        };
        self.drawAssignedToSection = function (name, date, index, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////

            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createElement("h4");
            zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none; font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////

            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + +index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            firstP.setAttribute("style", "font-weight:bold; display: inline");
            secondP.setAttribute("style", "display: inline");
            firstP.innerText = name;
            secondP.innerText = "Assigned To ";
            firstImage.setAttribute("src", "css/images/icons/assigned-logo-stc.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(secondP);
            firstTD1.appendChild(firstP);

        };
        self.drawNextApprovalSection = function (name, index) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "nextApprovalTable");
            myTable.setAttribute("style", "border: 0.5px dashed #9D9D9D;background-color:#fff");
            document.getElementById("myHistory").appendChild(myTable);

            /////////// draw first row //////

            var firstRow001 = document.createElement("tr");
            firstRow001.setAttribute("id", "firstRow001" + index);
            document.getElementById("myTable" + index).appendChild(firstRow001);
            var firstTD001 = document.createElement("TD");
            var firstTD1001 = document.createElement("TD");
            var firstTD2001 = document.createElement("TD");
            var firstTD3001 = document.createElement("TD");
            firstTD001.setAttribute("class", "myTD");
            document.getElementById("firstRow001" + index).appendChild(firstTD001);
            document.getElementById("firstRow001" + index).appendChild(firstTD1001);
            document.getElementById("firstRow001" + index).appendChild(firstTD2001);
            document.getElementById("firstRow001" + index).appendChild(firstTD3001);
            var firstImage001 = document.createElement("img");
            var firstSpan001 = document.createElement("span");
            var firstSpan1001 = document.createElement("span");
            firstTD2001.setAttribute("id", "myDate" + index);
            firstTD2001.setAttribute("style", "background-color:#fff");
            firstSpan001 = document.createTextNode(name);
            firstImage001.setAttribute("src", "css/images/icons/future-approvals-logo.png");
            firstTD001.setAttribute("style", "border:none");
            firstTD001.appendChild(firstImage001);
            firstTD1001.appendChild(firstSpan001);
            firstTD2001.appendChild(firstSpan1001);
        };
        self.drawReassign = function (name, assignedFrom, date, index, reason, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////
            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none; font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////
            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            firstTD1.setAttribute("class", "myTD");
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            secondP.setAttribute("style", "font-weight:bold; display: inline");
            firstP.setAttribute("style", "display: inline");
            firstP.innerText = assignedFrom + " reassigned to ";
            secondP.innerText = name;
            firstImage.setAttribute("src", "css/images/icons/action-logo-stc.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(firstP);
            firstTD1.appendChild(secondP);
            /////////////// second Row /////////////////////
            var secondRow = document.createElement("tr");
            secondRow.setAttribute("id", "secondRow" + index);
            document.getElementById("myTable" + index).appendChild(secondRow);
            var secondTD = document.createElement("TD");
            var secondTD1 = document.createElement("TD");
            var secondTD2 = document.createElement("TD");
            secondTD2.setAttribute("id", "myTD2" + index);
            document.getElementById("secondRow" + index).appendChild(secondTD);
            document.getElementById("secondRow" + index).appendChild(secondTD1);
            document.getElementById("secondRow" + index).appendChild(secondTD2);
            var secondSpan = document.createElement("href");
            secondSpan = document.createTextNode(reason);
            secondTD1.setAttribute("style", "border:none;font-size:14px");
            secondTD1.appendChild(secondSpan);
        };
        self.drawDelegation = function (name, delegatedFrom, date, index, reason, color) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "myTable" + index);
            myTable.setAttribute("class", "allTable");
            myTable.setAttribute("style", "border: 1px solid #9D9D9D;background-color:#" + color);
            document.getElementById("myHistory").appendChild(myTable);
            /////////////// zero row/////////////////
            var zeroRow = document.createElement("tr");
            zeroRow.setAttribute("id", "zeroRow" + index);
            document.getElementById("myTable" + index).appendChild(zeroRow);
            var zeroTD = document.createElement("TD");
            var zeroTD1 = document.createElement("TD");
            var zeroTD3 = document.createElement("TD");
            document.getElementById("zeroRow" + index).appendChild(zeroTD);
            document.getElementById("zeroRow" + index).appendChild(zeroTD1);
            document.getElementById("zeroRow" + index).appendChild(zeroTD3);
            var zeroSpan = document.createTextNode(date);
            zeroTD3.setAttribute("style", "border:none; font-size:13px");
            zeroTD3.appendChild(zeroSpan);

            /////////// draw first row //////
            var firstRow = document.createElement("tr");
            firstRow.setAttribute("id", "firstRow" + index);
            document.getElementById("myTable" + index).appendChild(firstRow);
            var firstTD = document.createElement("TD");
            var firstTD1 = document.createElement("TD");
            var firstTD2 = document.createElement("TD");
            firstTD1.setAttribute("class", "myTD");
            document.getElementById("firstRow" + index).appendChild(firstTD);
            document.getElementById("firstRow" + index).appendChild(firstTD1);
            document.getElementById("firstRow" + index).appendChild(firstTD2);
            var firstImage = document.createElement("img");
            var firstP = document.createElement("p");
            var secondP = document.createElement("p");
            secondP.setAttribute("style", "font-weight:bold; display: inline");
            firstP.setAttribute("style", "display: inline");
            firstP.innerText = delegatedFrom + " delegated to ";
            secondP.innerText = name;
            firstImage.setAttribute("src", "css/images/icons/action-logo-stc.png");
            firstTD.appendChild(firstImage);
            firstTD1.appendChild(firstP);
            firstTD1.appendChild(secondP);
            /////////////// second Row /////////////////////
            var secondRow = document.createElement("tr");
            secondRow.setAttribute("id", "secondRow" + index);
            document.getElementById("myTable" + index).appendChild(secondRow);
            var secondTD = document.createElement("TD");
            var secondTD1 = document.createElement("TD");
            var secondTD2 = document.createElement("TD");
            secondTD2.setAttribute("id", "myTD2" + index);
            document.getElementById("secondRow" + index).appendChild(secondTD);
            document.getElementById("secondRow" + index).appendChild(secondTD1);
            document.getElementById("secondRow" + index).appendChild(secondTD2);
            var secondSpan = document.createElement("href");
            secondSpan = document.createTextNode(reason);
            secondTD1.setAttribute("style", "border:none;font-size:14px");
            secondTD1.appendChild(secondSpan);
        };
        self.drawArrowUp = function (index) {
            var myTable = document.createElement("table");
            myTable.setAttribute("id", "arrowTable" + index);
            myTable.setAttribute("style", "border: none;background-color:#fff");
            document.getElementById("myHistory").appendChild(myTable);

            ////////////////////// draw first row //////////////////
            var myRow = document.createElement("tr");
            myRow.setAttribute("id", "myRow" + index);
            document.getElementById("arrowTable" + index).appendChild(myRow);
            var myTD = document.createElement("TD");
            document.getElementById("myRow" + index).appendChild(myTD);
            var myImage = document.createElement("img");
            myTD.setAttribute("id", "myDate" + index);
            myImage.setAttribute("src", "css/images/icons/arrow-up.png");
            myTD.setAttribute("style", "border:none");
            myTD.appendChild(myImage);
        };
        self.setPageTitle = function (title) {
            if (typeof title !== 'undefined') {
                var myHeader = document.createElement("p");
                myHeader.innerText = title;
                document.getElementById("myPageTitle").appendChild(myHeader);
            }
        };
        var dismissResponse;
        self.dismissBtn = function () {
            document.getElementById("dismissYesNoDialog").open();
        };
        self.dismissYES = function () {
            var payload = {
                "taskId": self.getAllDataByNumber().taskId,
                "outcome": "ACKNOWLEDGE",
                "employeeName": self.employeeName(),
                "taskNumber": self.getAllDataByNumber().taskNumber};
            var success = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        dismissResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        dismissResponse = 'success';
                        self.isAction(true);
                    }
                    var payloadString = JSON.stringify(payload);
                    //save log details in mySql
                    self.getTaskLogByNumber('saasService', dismissResponse, 'dismissAction', payloadString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        dismissResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        dismissResponse = 'success';
                        self.isAction(true);
                    }
                    var payloadString = JSON.stringify(payload);
                    //save log details in mySql
                    self.getTaskLogByNumber('saasService', dismissResponse, 'dismissAction', payloadString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            }
            services.addGeneric("bpmWorklist/updateTaskOutcome", payload).then(success, fail);
            document.getElementById("dismissYesNoDialog").close();
            app.loading(true);
        };
        self.dismissNO = function () {
            document.getElementById("dismissYesNoDialog").close();
        };
        self.resumeBTN = function () {
            document.getElementById("resumeYesNoDialog").open();
        };
        self.resumeYES = function () {
            app.loading(true);
            var resumeResponse;
            var payload = {
                "taskNumber": taskNumber,
                "taskId": self.getAllDataByNumber().taskId,
                "employeeName": self.employeeName()
            }
            var payloadString = JSON.stringify(payload);
            var success = function (data) {
                data = JSON.parse(data);
                if (data.status != 'Success') {
                    resumeResponse = 'fail';
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                } else {
                    app.loading(false);
                    resumeResponse = 'success';
                    self.isAction(true);
                }
                self.getTaskLogByNumber('saasService', resumeResponse, 'resumeAction', payloadString);
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.status != 'Success') {
                    resumeResponse = 'fail';
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                } else {
                    app.loading(false);
                    resumeResponse = 'success';
                    self.isAction(true);
                }
                self.getTaskLogByNumber('saasService', resumeResponse, 'resumeAction', payloadString);
            };
            services.addGeneric("bpmWorklist/resumeTask", payload).then(success, fail);
            document.getElementById("resumeYesNoDialog").close();
            app.loading(true);
        };
        self.resumeNO = function () {
            document.getElementById("resumeYesNoDialog").close();
        };
        self.releaseYES = function () {
            var releaseResponse;
            var payload = {
                "taskNumber": taskNumber,
                "taskId": self.getAllDataByNumber().taskId,
                "employeeName": self.employeeName()
            }
            var payloadString = JSON.stringify(payload);
            var success = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        releaseResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        releaseResponse = 'success';
                        self.isAction(true);
                    }
                    self.getTaskLogByNumber('saasService', releaseResponse, 'releaseAction', payloadString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            var fail = function (data) {
                data = JSON.parse(data);
                if (data.taskExist == 'Yes') {
                    if (data.status != 'Success') {
                        releaseResponse = 'fail';
                        self.dialogErrorMessage(data.error);
                        document.getElementById("errorDialog").open();
                    } else {
                        releaseResponse = 'success';
                        self.isAction(true);
                    }
                    self.getTaskLogByNumber('saasService', releaseResponse, 'releaseAction', payloadString);
                } else {
                    app.loading(false);
                    self.taskExistStatus(false);
                    self.dialogErrorMessage(data.error);
                    document.getElementById("errorDialog").open();
                }
            };
            services.addGeneric("bpmWorklist/releaseTask", payload).then(success, fail);
            document.getElementById("releaseYesNoDialog").close();
            app.loading(true);
        };
        self.releaseNO = function () {
            document.getElementById("releaseYesNoDialog").close();
        };
    }
    return dynamicTaskDetailsContentViewModel;
});